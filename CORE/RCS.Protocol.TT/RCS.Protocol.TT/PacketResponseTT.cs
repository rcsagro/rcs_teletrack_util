using System;
using TransitServisLib;

namespace RCS.Protocol.TT
{
  public class PacketResponseTT : PacketTT
  {
    /// <summary>
    /// �������� ���� ������
    /// </summary>
    public byte Cod_answ
    {
      get { return cod_answ; }
      set
      {
        if (value == 0 || value == 1)
        {
          cod_answ = value;
        }
      }
    }
    /// <summary>
    /// ��� ������
    /// </summary>
    byte cod_answ;

    public byte[] AssemblePacket(bool cod)
    {
      byte[] bytes = new byte[ATR_LENGTH + 1];
      //��������� �����
      RESP_TT.CopyTo(bytes, 0);
      BitConverter.GetBytes(cod).CopyTo(bytes, ATR_LENGTH);
      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      int pac_size = 5; //������ ������ Response = 5 ����
      try
      {
        cod_answ = incomPack[header_pos + 4];
        //�od_answ = BitConverter.ToInt32(incomPack, ATR_LENGTH + header_pos);
      }
      catch (Exception e)
      {
        MessageErrorEventArgs msg_except = new MessageErrorEventArgs();
        msg_except.Message = e.Message;
        msg_except.DTime = DateTime.Now;
        return -1;
      }
      return pac_size;
    }

    public override byte[] AssemblePacket()
    {
      byte[] bytes = new byte[0];
      return bytes;
    }
  }
}
