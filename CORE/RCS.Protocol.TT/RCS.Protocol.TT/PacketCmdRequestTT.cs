using System;

namespace RCS.Protocol.TT
{
  public class PacketCmdRequestTT : PacketTT
  {
    public override byte[] AssemblePacket()
    {
      byte[] bytes = new byte[0];
      return PacketTT.CMD_RE_TT;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      return 1;
    }
  }
}
