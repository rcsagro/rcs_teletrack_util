using System;
using System.Text;
using TransitServisLib;

namespace RCS.Protocol.TT
{
    public class PacketAuthTT : PacketTT
    {
        public PacketAuthTT()
        {
        }

        public PacketAuthTT(string log, string pass)
        {
            login = log;
            password = pass;
        }

        /// <summary>
        /// id_teletrack - �������� ��� (�����)
        /// </summary>
        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        protected string login;

        /// <summary>
        /// ������ pas
        /// </summary>
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        protected string password;

        /// <summary>
        /// �������� ����� Auth
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public byte[] AssemblePacket(string login, string pass)
        {
            byte[] log = Encoding.ASCII.GetBytes(login);
            //������� NUL_SIMB
            Array.Resize(ref log, log.Length + NUL_SIMB.Length);
            byte[] pas = Encoding.ASCII.GetBytes(pass);
            Array.Resize(ref pas, pas.Length + NUL_SIMB.Length);
            byte[] bytes = new byte[ATR_LENGTH + log.Length + pas.Length];
            //��������� �����
            PACK_AUTH_TT.CopyTo(bytes, 0);
            log.CopyTo(bytes, ATR_LENGTH);
            pas.CopyTo(bytes, (ATR_LENGTH + log.Length));

            return bytes;
        }

        public override byte[] AssemblePacket()
        {
            byte[] log = Encoding.ASCII.GetBytes(login);
            //������� NUL_SIMB
            Array.Resize(ref log, log.Length + NUL_SIMB.Length);
            byte[] pas = Encoding.ASCII.GetBytes(password);
            Array.Resize(ref pas, pas.Length + NUL_SIMB.Length);
            byte[] bytes = new byte[ATR_LENGTH + log.Length + pas.Length];
            //��������� �����
            PACK_AUTH_TT.CopyTo(bytes, 0);
            log.CopyTo(bytes, ATR_LENGTH);
            pas.CopyTo(bytes, (ATR_LENGTH + log.Length));

            return bytes;
        }

        public override int DisassemblePacket(byte[] incomPack, int header_pos)
        {
            //header_pos - �������� �� ������ incomPack �� �������� ������� ������
            int pac_size = -1;
            try
            {
                //pos1 ������� NUL_SIMB ����� ������
                int pos1 = FindBytes(incomPack, NUL_SIMB, ATR_LENGTH + header_pos);
                login = Encoding.ASCII.GetString(incomPack, ATR_LENGTH + header_pos, ATR_LENGTH);
                    //pos1 - (ATR_LENGTH + header_pos)
                //pos2 ������� NUL_SIMB ������� ������
                int pos2 = FindBytes(incomPack, NUL_SIMB, (pos1 + 1) /*+ NUL_SIMB.Length)*/+ header_pos);
                password = Encoding.ASCII.GetString(incomPack, (pos1 + 1) /*+ NUL_SIMB.Length)*/+ header_pos,
                    (pos2 - (pos1 + 1) /*+ NUL_SIMB.Length)*/+ header_pos));
                pac_size = login.Length + password.Length;
            }
            catch (Exception e)
            {
                MessageErrorEventArgs msg_except = new MessageErrorEventArgs();
                msg_except.Message = e.Message;
                msg_except.DTime = DateTime.Now;
                return -1;
            }
            return pac_size;
        }
    }
}
