using System;
using System.Text;

namespace RCS.Protocol.TT
{
  public class PacketA1TT : PacketTT
  {
    /// <summary>
    /// id_teletrack - �������� ��� (�����)
    /// </summary>
    public int ID_Teletrack
    {
      get { return id_teletrack; }
      set { id_teletrack = value; }
    }
    int id_teletrack;

    /// <summary>
    /// ������ ID_Command, ����������� � �� �������
    /// </summary>
    public int id_cmd;
    /// <summary>
    /// id ������� � �� �������
    /// </summary>
    public int id_cmd_cl;

    ///������ ���������� ��� ������ � ������ �1
    static readonly int A1_DATA_SIZE = 144;
    ///������ ���������� ��� �����
    static readonly int EMAIL_SIZE = 13;

    /// <summary>
    /// �������� ������ � comtext (��������)
    /// </summary>
    public byte[] ComText
    {
      get { return comtext; }
      set { comtext = value; }
    }
    /// <summary>
    /// ������� ��� ��������� �� ���������� � �������� ����
    /// </summary>
    private byte[] comtext;

    /// <summary>
    /// �����
    /// </summary>
    public string E_Mail
    {
      get { return e_mail; }
      set { e_mail = value; }
    }
    private string e_mail;

    /// <summary>
    /// ����� �������� �������� ������ � ����� (A1) ��������� Light
    /// </summary>
    /// <returns></returns>
    public byte[] AssemblePacket(string e_mail, byte[] com_text)
    {
      byte[] bytes = new byte[EMAIL_SIZE + A1_ATR_LENGTH + A1_DATA_SIZE];

      //��������� �����
      if (e_mail == "")
      {
        Array.Copy(new byte[EMAIL_SIZE], bytes, EMAIL_SIZE);
      }
      else
      {
        Encoding.ASCII.GetBytes(e_mail).CopyTo(bytes, 0);
      }
      Array.Copy(A1_TT, 0, bytes, EMAIL_SIZE, A1_ATR_LENGTH);
      Array.Copy(com_text, 0, bytes, EMAIL_SIZE + A1_ATR_LENGTH, com_text.Length);

      return bytes;
    }

    public override byte[] AssemblePacket()
    {
      byte[] bytes = new byte[0];
      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      int pack_size = -1;
      if (incomPack.Length >= (EMAIL_SIZE + 3 + A1_DATA_SIZE))
      {
        pack_size = incomPack.Length;
        PacketSize = pack_size;//��� ����������
        comtext = incomPack;
      }
      return pack_size;
    }
  }
}
