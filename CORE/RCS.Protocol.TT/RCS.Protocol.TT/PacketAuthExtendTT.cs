using System;
using System.Text;
using TransitServisLib;

namespace RCS.Protocol.TT
{
  public class PacketAuthExtendTT : PacketAuthTT
  {
    public PacketAuthExtendTT()
    {
    }

    public PacketAuthExtendTT(string ver, string log, string pass)
      : base(log, pass)
    {
      version = ver;
    }

    /// <summary>
    /// ������ ���������
    /// </summary>
    public string Version
    {
      get { return version; }
      set { version = value; }
    }
    string version;


    /// <summary>
    /// �������� ����� Auth
    /// </summary>
    /// <param name="login"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public override byte[] AssemblePacket()
    {

      byte[] ver = Encoding.ASCII.GetBytes(version);

      //Array.Copy(ver, 0, bytes, 0, ver.Length);

      byte[] log = Encoding.ASCII.GetBytes(login);

      //Array.Copy(log,0, bytes, ver.Length + 1, log.Length);

      byte[] pas = Encoding.ASCII.GetBytes(password);

      //Array.Copy(pas, 0, bytes, ver.Length + log.Length + 2, pas.Length);

      byte[] bytes = new byte[ATR_LENGTH + ver.Length + log.Length + pas.Length + 3];
      //��������� �����
      PACK_AUTH_TT.CopyTo(bytes, 0);

      ver.CopyTo(bytes, ATR_LENGTH);

      log.CopyTo(bytes, ATR_LENGTH + ver.Length + 1);
      pas.CopyTo(bytes, (ATR_LENGTH + ver.Length + log.Length + 2));

      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      //header_pos - �������� �� ������ incomPack �� �������� ������� ������
      int pac_size = -1;
      try
      {
        //pos1 ������� NUL_SIMB ����� ������
        int pos1 = FindBytes(incomPack, NUL_SIMB, ATR_LENGTH + header_pos);
        version = Encoding.ASCII.GetString(incomPack, ATR_LENGTH + header_pos, pos1 - (ATR_LENGTH + header_pos));

        int pos2 = FindBytes(incomPack, NUL_SIMB, (pos1 + 1));
        login = Encoding.ASCII.GetString(incomPack, pos1 + 1, pos2 - (pos1 + 1));
        //pos3 ������� NUL_SIMB ����� ������
        int pos3 = FindBytes(incomPack, NUL_SIMB, (pos2 + 1));
        password = Encoding.ASCII.GetString(incomPack, pos2 + 1, pos3 - (pos2 + 1));

        pac_size = login.Length + password.Length;

      }
      catch (Exception e)
      {
        MessageErrorEventArgs msg_except = new MessageErrorEventArgs();
        msg_except.Message = e.Message;
        msg_except.DTime = DateTime.Now;
        return -1;
      }
      return pac_size;
    }
  }
}
