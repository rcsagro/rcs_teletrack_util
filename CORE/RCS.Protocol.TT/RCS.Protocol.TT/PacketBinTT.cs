using System;
using System.Text;

namespace RCS.Protocol.TT
{
  /// <summary>
  /// ����� �������� ������ � ������ on-line
  /// (�� ����������� ��)
  /// </summary>
  public class PacketBinTT : PacketTT
  {
    /// <summary>
    /// id_teletrack - �������� ��� (�����)
    /// </summary>
    public string ID_Teletrack
    {
      get { return id_teletrack; }
      set { id_teletrack = value; }
    }
    string id_teletrack;

    /// <summary>
    /// ������ ID_Packet, ����������� ��� � �� �������
    /// </summary>
    public int ID_Packet
    {
      get { return id_packet; }
      set { id_packet = value; }
    }
    int id_packet;

    /// <summary>
    /// �������� ������� � ������ 
    /// </summary>
    public byte[] Packet
    {
      get { return packet; }
      set { packet = value; }
    }

    /// <summary>
    /// ����� (A1, Bin, MultiBin � �.�.) � �������� ����
    /// </summary>
    private byte[] packet = new byte[BIN_LENGTH];

    /// <summary>
    /// ����� �������� 
    /// </summary>
    /// <param name="message_id"></param>
    /// <param name="id_teletr"></param>
    /// <param name="id_packet"></param>
    /// <param name="inf_struct"></param>
    /// <returns></returns>
    public byte[] AssemblePacket(int message_id, int id_teletr, int id_packet, byte[] inf_struct)
    {
      byte[] bytes = Encoding.ASCII.GetBytes("12345AAAAAAAAAAAAAAAAAAAAAAAAAAA");
      return bytes;
    }

    public int DisassemblePacket(byte[] incomPack)
    {
      return BIN_LENGTH;
    }

    public override byte[] AssemblePacket()
    {
      byte[] bytes = new byte[0];
      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      int pac_size = incomPack.Length - header_pos;
      if (pac_size <= BIN_LENGTH)
      {
        Array.Copy(incomPack, header_pos, packet, 0, BIN_LENGTH);
        //PacketSize = BIN_LENGTH;//��� ����������
      }
      else
      {
        pac_size = -1;
      }
      return pac_size;
    }
  }
}
