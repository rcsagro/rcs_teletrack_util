using System;
using System.Text;

namespace RCS.Protocol.TT
{
  public class PacketAckTT : PacketTT
  {
    /// <summary>
    /// ������ ��� ������������� ����� �� ������
    /// </summary>
    public static readonly byte[] ACK_ERR_N = Encoding.ASCII.GetBytes("8");

    /// <summary>
    /// ��� ��, ������ ���
    /// </summary>
    public static readonly byte[] ACK_OK_N = Encoding.ASCII.GetBytes("0");

    /// <summary>
    /// ��� ��, ���� �������
    /// </summary>
    public static readonly byte[] ACK_OK_Y = Encoding.ASCII.GetBytes("4");

    /// <summary>
    /// ������ ��� ������������� ����� �� ������, ���� �������
    /// </summary>
    public static readonly byte[] ACK_ERR_Y = Encoding.ASCII.GetBytes("<");

    public override byte[] AssemblePacket()
    {
      byte[] bytes = new byte[0];
      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      int pac_size = incomPack.Length;
      return pac_size;
    }
  }
}
