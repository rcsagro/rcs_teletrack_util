using System;
using System.Collections;
using TransitServisLib;

namespace RCS.Protocol.TT
{
  public class PacketPKMultiBin : PacketMultiBinTT
  {
    /// <summary>
    /// ���� ������������� �������
    /// </summary>
    //public Stack<byte[]> stack = new Stack<byte[]>();

    public override byte[] AssemblePacket()
    {
      byte[] bytes = new byte[0];
      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      int pack_size = -1;
      try
      {
        //�������� �������� ����������� �����

        // 41 - ��� ��������: ���� �� ������� ������, ����������,
        //����� �������� ��������, ����� ���������� ������
        if (incomPack.Length >= ATR_LENGTH + header_pos + 1)
        {
          bin_count = incomPack[ATR_LENGTH + header_pos];
          pack_size = incomPack.Length - header_pos;
        }
        //ExecuteLogging.Log("Packet", "PacketMultiBin: " + BitConverter.ToString(incomPack));

        bool flag = true;

        if (incomPack.Length > header_pos + 41)
        {
          if (bin_count != 0)
          {
            Packet = incomPack; //��� �������� CRC

            byte[] bytes = new byte[BIN_LENGTH];
            Array.Copy(incomPack, (ATR_LENGTH + 1) + header_pos, bytes, 0, BIN_LENGTH);
            lst_newBin.Add(bytes);

            //ExecuteLogging.Log("Packet", "������ Bin: " + BitConverter.ToString(bytes));

            byte[] nextPack = (byte[])bytes.Clone();

            //stack.Push(bytes);

            int cur_pos = (ATR_LENGTH + 1) + 32 + header_pos;

            for (int i = 1; i < bin_count; i++)
            {
              if (incomPack.Length >= cur_pos + 4)
              {
                byte[] tmp_mask = new byte[4];
                Array.Copy(incomPack, cur_pos, tmp_mask, 0, 4);

                UInt32 m_int = (UInt32)(tmp_mask[3] + (tmp_mask[2] << 8) + (tmp_mask[1] << 16) + (tmp_mask[0] << 24));//BitConverter.ToInt32(tmp_mask, 0);

                //ExecuteLogging.Log("Packet", "�����: " + BitConverter.ToString(tmp_mask));

                cur_pos += 4;
                BitArray mask = new BitArray(tmp_mask);
                for (int j = 0; j < 32; j++)
                {
                  if ((m_int & (1 << (31 - j))) > 0)
                  {
                    cur_pos++;
                    if (incomPack.Length >= cur_pos)
                    {
                      nextPack[j] = incomPack[cur_pos - 1];
                      pack_size = cur_pos;
                    }
                    else { flag = false; break; }
                  }
                }

                if (flag)
                {
                  // bo.NumbLastRecInTT = (int)(data.pack[1] << 24 | data.pack[2] << 16 | data.pack[3] << 8 | data.pack[4]);//BitConverter.ToInt32(data.pack, 1); 
                  byte[] pk = (byte[])nextPack.Clone();
                  lst_newBin.Add(pk);
                  //ExecuteLogging.Log("Packet", "Bin: " + BitConverter.ToString(nextPack));
                }
                else break;
              }
            }
            if (flag && lst_newBin.Count == bin_count)
            {
              pack_size = incomPack.Length;
              //PacketSize = pack_size;//��� ����������
            }
            else
              pack_size = incomPack.Length + 1;//���������, ��� ����� �� ������
            //PacketTT.CountCRC();
          }
        }
        else pack_size = -1;
      }
      catch (Exception e)
      {
        MessageErrorEventArgs msg_except = new MessageErrorEventArgs();
        msg_except.Message = e.Message;
        msg_except.DTime = DateTime.Now;
        throw e;
      }
      return pack_size;
      //return bin_count;
    }
  }
}
