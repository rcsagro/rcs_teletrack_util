using System;
using System.Collections.Generic;
using System.Text;
using TransitServisLib;

namespace RCS.Protocol.TT
{
  public enum TeletrPKType
  {
    /// <summary>
    /// ����� �����������
    /// </summary>
    PACK_AUTH = 1,
    /// <summary>
    /// ����� MultiBin
    /// </summary>
    PACK_MBIN = 2,
    /// <summary>
    /// ����� PacketMultiBin
    /// </summary>
    PACK_PK_MBIN = 3,
    /// <summary>
    /// ����� CmdRequest
    /// </summary>
    PACK_CMD_REQUEST = 4,
    /// <summary>
    /// ����� Response
    /// </summary>
    PACK_RESPONSE = 5,
    /// <summary>
    /// ����� A1
    /// </summary>
    PACK_A1 = 6,
    /// <summary>
    /// ����� Bin
    /// </summary>
    PACK_BIN = 7,
    /// <summary>
    /// ����� Ack
    /// </summary>
    PACK_ACK = 8
  }

  public abstract class PacketTT : IPacket
  {
    /// <summary>
    /// ������ ��� ��������� ������
    /// </summary>
    public int PacketType
    {
      get { return packetType; }
      set { packetType = value; }
    }
    int packetType;

    /// <summary>
    /// ���������� ������ ��������� ������
    /// </summary>
    public int PacketSize
    {
      get { return packetSize; }
      set { packetSize = value; }
    }
    int packetSize = 0;

    /// <summary>
    /// ������� ������
    /// </summary>
    public string PacketAttr
    {
      get { return pack_attribute; }
      set { pack_attribute = value; }
    }
    string pack_attribute;

    /// <summary>
    /// ���������� id ������������� ������
    /// </summary>
    public int MessageID
    {
      get { return message_id; }
      set { message_id = value; }
    }
    int message_id;

    #region �������� �������
    /// <summary>
    /// ������, ������� ����� - ������ � ����� 0�00
    /// ��� ������ ��������� ������ ��������� ��� ������� "����� �� ���������" 
    /// </summary>
    public static readonly byte[] NUL_SIMB = new byte[1] { 0x00 };

    /// <summary>
    /// ������� ������ Auth
    /// </summary>
    public static readonly byte[] PACK_AUTH_TT = Encoding.ASCII.GetBytes("%%AU");

    /// <summary>
    /// ������� ������ Auth
    /// </summary>
    public static readonly byte[] PACK_AUTH_TT_EX = Encoding.ASCII.GetBytes("%%AE");

    /// <summary>
    /// ������� ������ A1
    /// </summary>
    public static readonly byte[] A1_TT = Encoding.ASCII.GetBytes(" %%");

    /// <summary>
    /// ������� ������ Response
    /// </summary>
    public static readonly byte[] RESP_TT = Encoding.ASCII.GetBytes("%%RE");

    /// <summary>
    /// ������� ������ MultiBin
    /// </summary>
    public static readonly byte[] MBIN_TT = Encoding.ASCII.GetBytes("%%MB");

    /// <summary>
    /// ������� ������ NewMultiBin
    /// </summary>
    public static readonly byte[] MBIN_64_TT = Encoding.ASCII.GetBytes("%%NB");
    /// <summary>
    /// ������� ������ PacketMultiBin
    /// </summary>
    public static readonly byte[] PK_MBIN_TT = Encoding.ASCII.GetBytes("%%PB");

    /// <summary>
    /// ������� ������ CmdRequest
    /// </summary>
    public static readonly byte[] CMD_RE_TT = Encoding.ASCII.GetBytes("%%CR");

    /// <summary>
    /// ����� ��������� ���������  
    /// </summary>
    public static readonly int ATR_LENGTH = 4; //4 �����

    /// <summary>
    /// ����� �������� ������ �1  
    /// </summary>
    public static readonly int A1_ATR_LENGTH = 3; //3 �����

    /// <summary>
    /// ����� ������ Bin  
    /// </summary>
    public static readonly int BIN_LENGTH = 32; //32 �����

    /// <summary>
    /// ����� ������ NewBin  
    /// </summary>
    public static readonly int BIN_64_LENGTH = 64; //64 �����

    ///������ ���������� ��� �����
    public static readonly int A1_EMAIL_SIZE = 13;
    #endregion

    private static object locker = new object();

    protected static readonly List<byte[]> attributesPack = new List<byte[]>();

    public abstract byte[] AssemblePacket();
    public abstract int DisassemblePacket(byte[] incomPack, int header_pos);

    static PacketTT()
    {
      attributesPack.Add(PACK_AUTH_TT);
      attributesPack.Add(PACK_AUTH_TT_EX);
      attributesPack.Add(A1_TT);
      attributesPack.Add(CMD_RE_TT);
      attributesPack.Add(MBIN_TT);
      attributesPack.Add(PK_MBIN_TT);
      attributesPack.Add(RESP_TT);
      attributesPack.Add(MBIN_64_TT);
      //���������� ������
      //������ � �������� ����������
      /*attributesPack.Add(PacketAckTT.ACK_OK_Y);
      attributesPack.Add(PacketAckTT.ACK_OK_N);
      attributesPack.Add(PacketAckTT.ACK_ERR_Y);
      attributesPack.Add(PacketAckTT.ACK_ERR_N);*/
    }

    //******************************************************************************************

    /// <summary>
    /// �������������� �������� �����
    /// </summary>
    /// <param name="incom_pack">�����</param>
    /// <returns>������� ������</returns>
    public static IPacket PacketIdentify(byte[] incom_pack, ref int pos)
    {
      byte[] attr = FindPacketHeader(incom_pack, ref pos);
      if (attr != null)
      {
        if (attr == PACK_AUTH_TT) return new PacketAuthTT();//����� Auth
        if (attr == PACK_AUTH_TT_EX) return new PacketAuthExtendTT();//����� Auth
        if (attr == MBIN_TT) return new PacketMultiBinTT();//����� MultiBin
        if (attr == PK_MBIN_TT) return new PacketPKMultiBin();//����� PacketMultiBin
        if (attr == CMD_RE_TT) return new PacketCmdRequestTT();//����� CmdRequest
        if (attr == RESP_TT) return new PacketResponseTT();//����� Response
        if (attr == MBIN_64_TT) return new Packet64MultiBinTT();//����� NewMultiBin
      }
      else
      {
        if (A1_FindPacketHeader(incom_pack, ref pos) != null)
        {
          return new PacketA1TT(); //����� �1
        }
        else
        {
          if (incom_pack.Length == BIN_LENGTH)
          {
            return new PacketBinTT(); //����� Bin
          }
        }
      }
      return null; //������
    }

    /// <summary>
    /// ������� ������� ������ �1
    /// </summary>
    /// <param name="bs">�����</param>
    /// <param name="res">�������</param>
    /// <returns>������� ������</returns>
    public static byte[] A1_FindPacketHeader(byte[] bs, ref int head_pos)
    {
      lock (locker)
      {
        byte[] bd = A1_TT;
        bool flag = false;

        if (bs.Length < A1_EMAIL_SIZE + A1_ATR_LENGTH)
        {
          return null;
        }

        for (int i = head_pos + A1_EMAIL_SIZE; i < (head_pos + A1_EMAIL_SIZE + A1_ATR_LENGTH); i++)
        {
          for (int j = 0; j < bd.Length; j++)
          {
            if (bs[i + j] == bd[j])
            {
              flag = true;
            }
            else
            {
              flag = false;
              break;
            }
          }
          if (flag)
          {
            //head_pos = i;
            return bd;
          }
        }
        return null;
      }
    }

    /// <summary>
    /// �������������� �������� �����
    /// </summary>
    /// <param name="bs">�����</param>
    /// <param name="res">�������</param>
    /// <returns>������� ������</returns>
    public static byte[] FindPacketHeader(byte[] bs, ref int head_pos)
    {
      //������� �������� �� �������� ���������
      //����� ���������� ������������ ������ lock(object)
      lock (locker)
      {
        int count = attributesPack.Count;
        int tmp = count;
        //head_pos = -1;
        byte[] bd = new byte[ATR_LENGTH];
        bool flag = false;

        if (bs.Length < bd.Length)
        {
          return null;
        }

        for (int i = head_pos; i < ATR_LENGTH; i++)
        {
          for (int k = 0; k < attributesPack.Count; k++)
          {
            bd = attributesPack[k];
            for (int j = 0; j < bd.Length; j++)
            {
              if (bs[i + j] == bd[j])
              {
                flag = true;
              }
              else
              {
                flag = false;
                break;
              }
            }
            if (flag)
            {
              head_pos = i;
              return bd;
            }
          }
        }
        return null;
      }
    }

    /// <summary>
    /// ��� ������ � PacketAuth
    /// </summary>
    /// <param name="bs">���</param>
    /// <param name="bd">����� ����� ����</param>
    /// <param name="start_pos">� ����� �������</param>
    /// <returns></returns>
    public int FindBytes(byte[] bs, byte[] bd, int start_pos)
    {
      int res = -1;
      bool flag = false;

      if (bs.Length < bd.Length)
      {
        return -1;
      }

      for (int i = start_pos; i < (bs.Length - (bd.Length - 1)); i++)
      {
        for (int j = 0; j < bd.Length; j++)
        {
          if (bs[i + j] == bd[j])
          {
            flag = true;
          }
          else
          {
            flag = false;
            break;
          }
        }
        if (flag)
        {
          return i;
        }
      }
      return res;
    }

    /// <summary>
    /// ����������
    /// </summary>
    /// <param name="bs"></param>
    /// <param name="bd"></param>
    /// <returns></returns>
    public int FindBytes(byte[] bs, byte[] bd)
    {
      int res = -1;
      bool flag = false;

      if (bs.Length < bd.Length)
      {
        return -1;
      }

      for (int i = 0; i < (bs.Length - (bd.Length - 1)); i++)
      {
        for (int j = 0; j < bd.Length; j++)
        {
          if (bs[i + j] == bd[j])
          {
            flag = true;
          }
          else
          {
            flag = false;
            break;
          }
        }

        if (flag)
        {
          return i;
        }
      }
      return res;
    }

    public static byte CountCRC(byte[] data)
    {
      byte ret = 0xee;
      byte[] CRC8_DATA = new byte[]{
        0x00, 0x5e, 0xbc, 0xe2, 0x61, 0x3f, 0xdd, 0x83,
        0xc2, 0x9c, 0x7e, 0x20, 0xa3, 0xfd, 0x1f, 0x41,
        0x9d, 0xc3, 0x21, 0x7f, 0xfc, 0xa2, 0x40, 0x1e,
        0x5f, 0x01, 0xe3, 0xbd, 0x3e, 0x60, 0x82, 0xdc,
        0x23, 0x7d, 0x9f, 0xc1, 0x42, 0x1c, 0xfe, 0xa0,
        0xe1, 0xbf, 0x5d, 0x03, 0x80, 0xde, 0x3c, 0x62,
        0xbe, 0xe0, 0x02, 0x5c, 0xdf, 0x81, 0x63, 0x3d,
        0x7c, 0x22, 0xc0, 0x9e, 0x1d, 0x43, 0xa1, 0xff,
        0x46, 0x18, 0xfa, 0xa4, 0x27, 0x79, 0x9b, 0xc5,
        0x84, 0xda, 0x38, 0x66, 0xe5, 0xbb, 0x59, 0x07,
        0xdb, 0x85, 0x67, 0x39, 0xba, 0xe4, 0x06, 0x58,
        0x19, 0x47, 0xa5, 0xfb, 0x78, 0x26, 0xc4, 0x9a,
        0x65, 0x3b, 0xd9, 0x87, 0x04, 0x5a, 0xb8, 0xe6,
        0xa7, 0xf9, 0x1b, 0x45, 0xc6, 0x98, 0x7a, 0x24,
        0xf8, 0xa6, 0x44, 0x1a, 0x99, 0xc7, 0x25, 0x7b,
        0x3a, 0x64, 0x86, 0xd8, 0x5b, 0x05, 0xe7, 0xb9,
        0x8c, 0xd2, 0x30, 0x6e, 0xed, 0xb3, 0x51, 0x0f,
        0x4e, 0x10, 0xf2, 0xac, 0x2f, 0x71, 0x93, 0xcd,
        0x11, 0x4f, 0xad, 0xf3, 0x70, 0x2e, 0xcc, 0x92,
        0xd3, 0x8d, 0x6f, 0x31, 0xb2, 0xec, 0x0e, 0x50,
        0xaf, 0xf1, 0x13, 0x4d, 0xce, 0x90, 0x72, 0x2c,
        0x6d, 0x33, 0xd1, 0x8f, 0x0c, 0x52, 0xb0, 0xee,
        0x32, 0x6c, 0x8e, 0xd0, 0x53, 0x0d, 0xef, 0xb1,
        0xf0, 0xae, 0x4c, 0x12, 0x91, 0xcf, 0x2d, 0x73,
        0xca, 0x94, 0x76, 0x28, 0xab, 0xf5, 0x17, 0x49,
        0x08, 0x56, 0xb4, 0xea, 0x69, 0x37, 0xd5, 0x8b,
        0x57, 0x09, 0xeb, 0xb5, 0x36, 0x68, 0x8a, 0xd4,
        0x95, 0xcb, 0x29, 0x77, 0xf4, 0xaa, 0x48, 0x16,
        0xe9, 0xb7, 0x55, 0x0b, 0x88, 0xd6, 0x34, 0x6a,
        0x2b, 0x75, 0x97, 0xc9, 0x4a, 0x14, 0xf6, 0xa8,
        0x74, 0x2a, 0xc8, 0x96, 0x15, 0x4b, 0xa9, 0xf7,
        0xb6, 0xe8, 0x0a, 0x54, 0xd7, 0x89, 0x6b, 0x35
        };

      data[31] = 0;
      //ret = CheckSummArr(s_buffer+1, position);
      //BYTE CheckSummArr(BYTE *pDat,BYTE Size)
      {
        byte i = 0;
        byte CRC = 0;
        for (i = 0; i < 32; i++)
        {
          CRC = CRC8_DATA[CRC ^ data[i]];
        }
        ret = CRC;
      }
      return ret;
    }
  }
}
