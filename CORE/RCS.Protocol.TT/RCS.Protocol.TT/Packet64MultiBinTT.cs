﻿using System;
using System.Collections.Generic;
using TransitServisLib;

namespace RCS.Protocol.TT
{
    public class Packet64MultiBinTT : PacketTT
    {
        /// <summary>
        /// id_teletrack - понятное имя (логин)
        /// </summary>
        public int ID_Teletrack
        {
            get { return id_teletrack; }
            set { id_teletrack = value; }
        }
        int id_teletrack;

        /// <summary>
        /// Хранит ID_Packet, присвоенный ему в БД сервера
        /// </summary>
        public int ID_Packet
        {
            get { return id_packet; }
            set { id_packet = value; }
        }
        int id_packet;

        /// <summary>
        /// Свойство доступа к пакету (A1, Bin, MultiBin и т.д.)
        /// </summary>
        public byte[] Packet
        {
            get { return packet; }
            set { packet = value; }
        }
        /// <summary>
        /// Пакет (A1, Bin, MultiBin и т.д.) в бинарном виде
        /// </summary>
        private byte[] packet;

        public Stack<byte[]> stack = new Stack<byte[]>();
        public List<byte[]> lst_newBin = new List<byte[]>();

        /// <summary>
        /// Хранит количество упакованных пакетов Bin (от 0 до 255)
        /// </summary>
        public byte Bin_Count
        {
            get { return bin_count; }
            set { bin_count = value; }
        }
        protected byte bin_count;

        /// <summary>
        /// Метод упаковки бинарных данных в пакет (NewMultiBin) протокола
        /// </summary>
        /// <returns></returns>
        public byte[] AssemblePacket(byte[] dat)
        {
            byte[] bytes = new byte[ATR_LENGTH + 1 + dat.Length];
            int num = dat.Length / 64;
            if (num < 256)
            {
                Bin_Count = (byte)num;
                //Формируем пакет
                MBIN_64_TT.CopyTo(bytes, 0);
                bytes[ATR_LENGTH] = Bin_Count;
                Array.Copy(dat, 0, bytes, ATR_LENGTH + 1, dat.Length);
            }
            return bytes;
        }

        public override byte[] AssemblePacket()
        {
            byte[] bytes = new byte[0];
            return bytes;
        }

        public override int DisassemblePacket(byte[] incomPack, int header_pos)
        {
            int pac_size = -1;
            try
            {
                bin_count = incomPack[ATR_LENGTH + header_pos];
                //см. структуру пакета
                pac_size = ATR_LENGTH + 1 + bin_count * BIN_64_LENGTH;

                if (bin_count != 0 && pac_size <= incomPack.Length)
                {
                    for (int i = 0; i < bin_count; i++)
                    {
                        byte[] bytes = new byte[BIN_64_LENGTH];
                        Array.Copy(incomPack, (ATR_LENGTH + 1 + i * BIN_64_LENGTH) + header_pos, bytes, 0, BIN_64_LENGTH);
                        lst_newBin.Add(bytes);
                        //stack.Push(bytes);
                    }
                }
            }
            catch (Exception e)
            {
                MessageErrorEventArgs msg_except = new MessageErrorEventArgs();
                msg_except.Message = e.Message;
                msg_except.DTime = DateTime.Now;
                return -1;
            }
            return pac_size;
        }
    }
}
