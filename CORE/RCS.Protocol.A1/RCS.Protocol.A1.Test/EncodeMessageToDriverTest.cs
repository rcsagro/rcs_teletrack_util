using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ��������")]
  public class EncodeMessageToDriverTest
  {
    [Test, Description("��������� �������� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void MessageToDriverNull()
    {
      string result = Encoder.EncodeMessageToDriver(null);
    }

    [Test, Description("������������ ������������ ��������� �������� - ����� ������� ���������")]
    [ExpectedException(typeof(A1Exception))]
    public void MessageToDriverBigText()
    {
      MessageToDriver config = new MessageToDriver();
      config.MessageID = 15;
      config.ShortID = "4938";
      config.Text = "AAAAAAAAAA bbbbbbbbbb tttttttttt jjjjjjjjjj uuuuuuuuuu 2222222222 zzzzzzzzzz mmmmmmmmmm";
      string result = Encoder.EncodeMessageToDriver(config);
    }

    [Test, Description("����������� ��������� ��������")]
    public void MessageToDriver()
    {
      MessageToDriver config = new MessageToDriver();
      config.MessageID = 15;
      config.ShortID = "4938";
      config.Text = "CMD=AT#SMTPPORT=202";

      string result = Encoder.EncodeMessageToDriver(config);
      const string dbMessage = "              %%AAi05z4rCADyQTRHPQVFIUTfVUUATUVLPMMJMAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXwXXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.AreEqual((int)Const.LEVEL0_PACKET_LENGTH, config.Source.Length);
      Assert.AreEqual((int)Const.LEVEL0_PACKET_LENGTH, config.Message.Length);
      Assert.AreEqual(dbMessage, result);
    }
  }
}
