using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ������������ ���������� �������")]
  public class EncodePhoneNumberConfigTest
  {
    [Test, Description("������������ ���������� ������� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void PhoneNumberConfigNull()
    {
      string result = Encoder.EncodePhoneNumberConfigSet(null);
    }

    [Test, Description("������������ ������������ ���������� ������� NumberAccept1")]
    [ExpectedException(typeof(A1Exception))]
    public void PhoneNumberConfigBigNumberAccept1()
    {
      PhoneNumberConfig config = new PhoneNumberConfig();
      config.NumberAccept1 = "+380637562678456789097";
      string result = Encoder.EncodePhoneNumberConfigSet(config);
    }

    [Test, Description("������������ ������������ ���������� ������� NumberAccept2")]
    [ExpectedException(typeof(A1Exception))]
    public void PhoneNumberConfigBigNumberAccept2()
    {
      PhoneNumberConfig config = new PhoneNumberConfig();
      config.NumberAccept2 = "+380637562678456789097";
      string result = Encoder.EncodePhoneNumberConfigSet(config);
    }

    [Test, Description("������������ ������������ ���������� ������� NumberAccept3")]
    [ExpectedException(typeof(A1Exception))]
    public void PhoneNumberConfigBigNumberAccept3()
    {
      PhoneNumberConfig config = new PhoneNumberConfig();
      config.NumberAccept3 = "+380637562678456789097";
      string result = Encoder.EncodePhoneNumberConfigSet(config);
    }

    [Test, Description("������������ ������������ ���������� ������� NumberDspt")]
    [ExpectedException(typeof(A1Exception))]
    public void PhoneNumberConfigBigNumberDspt()
    {
      PhoneNumberConfig config = new PhoneNumberConfig();
      config.NumberDspt = "+380637562678456789097";
      string result = Encoder.EncodePhoneNumberConfigSet(config);
    }

    [Test, Description("������������ ������������ ���������� ������� NumberSOS")]
    [ExpectedException(typeof(A1Exception))]
    public void PhoneNumberConfigBigNumberSOS()
    {
      PhoneNumberConfig config = new PhoneNumberConfig();
      config.NumberSOS = "+380637562678456789097";
      string result = Encoder.EncodePhoneNumberConfigSet(config);
    }

    [Test, Description("������������ ������������ ���������� ������� Name1")]
    [ExpectedException(typeof(A1Exception))]
    public void PhoneNumberConfigBigName1()
    {
      PhoneNumberConfig config = new PhoneNumberConfig();
      config.Name1 = "ALTUHOV23";
      string result = Encoder.EncodePhoneNumberConfigSet(config);
    }

    [Test, Description("������������ ������������ ���������� ������� Name2")]
    [ExpectedException(typeof(A1Exception))]
    public void PhoneNumberConfigBigName2()
    {
      PhoneNumberConfig config = new PhoneNumberConfig();
      config.Name2 = "ALTUHOV23";
      string result = Encoder.EncodePhoneNumberConfigSet(config);
    }

    [Test, Description("������������ ������������ ���������� ������� Name3")]
    [ExpectedException(typeof(A1Exception))]
    public void PhoneNumberConfigBigName3()
    {
      PhoneNumberConfig config = new PhoneNumberConfig();
      config.Name3 = "ALTUHOV23";
      string result = Encoder.EncodePhoneNumberConfigSet(config);
    }

    [Test, Description("����������� ������������ ���������� �������")]
    public void PhoneNumberConfig()
    {
      PhoneNumberConfig config = new PhoneNumberConfig();
      config.ShortID = "3913";
      config.MessageID = 115;
      config.NumberAccept1 = "80637562678";
      config.NumberAccept2 = "80637562677";
      config.NumberAccept3 = "80672452724";
      config.NumberDspt = "80637562677";
      config.NumberSOS = "80637562678";
      config.Name1 = "ALTUHOV";
      config.Name2 = "RCS1";
      config.Name3 = "Basis_KS";

      string result = Encoder.EncodePhoneNumberConfigSet(config);
      const string expected = "              %%AAiz5xzkDAcwgYdcYZje8AAAAAgAYdYnZf8HAAAAAgZwJUcoT8ABAAAAgYdcYZfe8AAAAAAAQTVBVSTxVAUiQUMfAAAAAQYYcac3XSU-gYdcYZje8AAAAAXwXXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }

    [Test, Description("������ ������������ ���������� ������� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void PhoneNumberConfigQueryNull()
    {
      string result = Encoder.EncodePhoneNumberConfigQuery(null);
    }

    [Test, Description("����������� ������� ������������ ���������� �������")]
    public void PhoneNumberConfigQuery()
    {
      SimpleQuery config = new SimpleQuery();
      config.ShortID = "1010";
      config.MessageID = 45;

      string result = Encoder.EncodePhoneNumberConfigQuery(config);
      const string expected = "              %%AAixwxwsIALQXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }
  }
}
