using System;
using NUnit.Framework;
using RCS.Protocol.A1;

namespace UnitTest
{
  [TestFixture, Description("��������� ������ �� ������ 4")]
  public class Level4ConverterTest
  {
    #region BytesToString

    [Test, Description("����� BytesToString ������ �������� source")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void BytesToStringNullParam()
    {
      Level4Converter.BytesToString(null, 0, 2);
    }

    [Test, Description("����� BytesToString �������� startIndex < 0")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void BytesToStringSmallStartIndex()
    {
      Level4Converter.BytesToString(new byte[] { 1, 2, 3, 4 }, -1, 2);
    }

    [Test, Description("����� BytesToString �������� startIndex > ����� �������")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void BytesToStringBigStartIndex()
    {
      Level4Converter.BytesToString(new byte[] { 1, 2, 3, 4 }, 5, 2);
    }

    [Test, Description("����� BytesToString �������� length <= 0")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void BytesToStringSmallLength()
    {
      Level4Converter.BytesToString(new byte[] { 1, 2, 3, 4 }, 0, 0);
    }

    [Test, Description("����� BytesToString �������� startIndex + length > ����� �������")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void BytesToStringBigStartIndexLength()
    {
      Level4Converter.BytesToString(new byte[] { 1, 2, 3, 4 }, 0, 10);
    }

    #endregion

    #region BytesToTelNumber

    [Test, Description("����� BytesToTelNumber ������ �������� source")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void BytesToTelNumberNullParam()
    {
      Level4Converter.BytesToTelNumber(null, 0, 2);
    }

    [Test, Description("����� BytesToTelNumber �������� startIndex < 0")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void BytesToTelNumberSmallStartIndex()
    {
      Level4Converter.BytesToTelNumber(new byte[] { 1, 2, 3, 4 }, -1, 2);
    }

    [Test, Description("����� BytesToTelNumber �������� startIndex > ����� �������")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void BytesToTelNumberBigStartIndex()
    {
      Level4Converter.BytesToTelNumber(new byte[] { 1, 2, 3, 4 }, 5, 2);
    }

    [Test, Description("����� BytesToTelNumber �������� length <= 0")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void BytesToTelNumberSmallLength()
    {
      Level4Converter.BytesToTelNumber(new byte[] { 1, 2, 3, 4 }, 0, 0);
    }

    [Test, Description("����� BytesToTelNumber �������� startIndex + length > ����� �������")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void BytesToTelNumberBigStartIndexLength()
    {
      Level4Converter.BytesToTelNumber(new byte[] { 1, 2, 3, 4 }, 0, 10);
    }

    #endregion

    #region GetPhoneNumberSymbol

    [Test, Description("����� GetPhoneNumberSymbol ������ �������� source")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void GetPhoneNumberSymbolNullParam()
    {
      Level4Converter.GetPhoneNumberSymbol(null, 0);
    }

    [Test, Description("����� GetPhoneNumberSymbol �������� Index < 0")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void GetPhoneNumberSymbolSmallIndex()
    {
      Level4Converter.GetPhoneNumberSymbol(new byte[] { 1, 2, 3, 4 }, -1);
    }

    [Test, Description("����� GetPhoneNumberSymbol �������� Index > source.Length * 2")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void GetPhoneNumberSymbolBigIndex()
    {
      Level4Converter.GetPhoneNumberSymbol(new byte[] { 1, 2, 3, 4 }, 10);
    }

    #endregion

    #region StringToBytes

    [Test, Description("����� StringToBytes ������ �������� value")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void StringToBytesNullParam()
    {
      Level4Converter.StringToBytes(null, 0);
    }

    [Test, Description("����� StringToBytes �������� maxLength < 0")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void StringToBytesSmallMaxLength()
    {
      Level4Converter.StringToBytes("ddd", -1);
    }

    #endregion

    #region TelNumberToBytes

    [Test, Description("����� TelNumberToBytes ������ �������� phoneNumber")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void TelNumberToBytesNullParam()
    {
      Level4Converter.TelNumberToBytes(null, 0);
    }

    [Test, Description("����� TelNumberToBytes �������� maxLength < 0")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void TelNumberToBytesSmallMaxLength()
    {
      Level4Converter.TelNumberToBytes("ddd", -1);
    }

    #endregion

    #region ToInt16

    [Test, Description("����� ToInt16 �������� source = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ToInt16NullParam()
    {
      Level4Converter.ToInt16(null, 0);
    }

    [Test, Description("����� ToInt16 ������ �������� source")]
    [ExpectedException(typeof(ArgumentException))]
    public void ToInt16EmptyParam()
    {
      Level4Converter.ToInt16(new byte[] { }, 0);
    }

    [Test, Description("����� ToInt16 �������� startIndex < 0")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void ToInt16SmallStartIndex()
    {
      Level4Converter.ToInt16(new byte[] { 1, 2, 3, 4 }, -1);
    }

    [Test, Description("����� ToInt16 �������� startIndex > ����� �������")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void ToInt16BigStartIndex()
    {
      Level4Converter.ToInt16(new byte[] { 1, 2, 3, 4 }, 5);
    }

    #endregion

    #region ToInt32

    [Test, Description("����� ToInt32 �������� source = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ToInt32NullParam()
    {
      Level4Converter.ToInt32(null, 0);
    }

    [Test, Description("����� ToInt32 ������ �������� source")]
    [ExpectedException(typeof(ArgumentException))]
    public void ToInt32EmptyParam()
    {
      Level4Converter.ToInt32(new byte[] { }, 0);
    }

    [Test, Description("����� ToInt32 �������� startIndex < 0")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void ToInt32SmallStartIndex()
    {
      Level4Converter.ToInt32(new byte[] { 1, 2, 3, 4 }, -1);
    }

    [Test, Description("����� ToInt32 �������� startIndex > ����� �������")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void ToInt32BigStartIndex()
    {
      Level4Converter.ToInt32(new byte[] { 1, 2, 3, 4 }, 5);
    }

    #endregion
  }
}
