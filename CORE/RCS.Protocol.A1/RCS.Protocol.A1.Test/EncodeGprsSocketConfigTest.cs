using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ������������ �������� Socket GPRS")]
  public class EncodeGprsSocketConfigTest
  {
    [Test, Description("������������ �������� Socket GPRS = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void GprsSocketConfigNull()
    {
      string result = Encoder.EncodeGprsSocketConfigSet(null);
    }

    [Test, Description("������������ ������������ �������� Socket GPRS Server")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsSocketConfigBigServer()
    {
      GprsSocketConfig config = new GprsSocketConfig();
      config.Server = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsSocketConfigSet(config);
    }

    [Test, Description("����������� ������������ �������� socket GPRS")]
    public void GprsSocketConfig()
    {
      GprsSocketConfig config = new GprsSocketConfig();
      config.MessageID = 38;
      config.ShortID = "8542";
      config.Server = "p314.net";
      config.Port = 9009;

      string result = Encoder.EncodeGprsSocketConfigSet(config);
      const string expected = "              %%AAi410y5NAJgcMMcNLboZdABAAAAAAAAAAAAAAIwMXX9XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }

    [Test, Description("������ ������������ �������� socket GPRS = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void GprsSocketQueryNull()
    {
      string result = Encoder.EncodeGprsSocketConfigQuery(null);
    }

    [Test, Description("����������� ������� �������� socket GPRS")]
    public void GprsSocketConfigQuery()
    {
      SimpleQuery config = new SimpleQuery();
      config.ShortID = "8542";
      config.MessageID = 45;

      string result = Encoder.EncodeGprsSocketConfigQuery(config);
      const string expected = "              %%AAi410y7SALQXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }
  }
}
