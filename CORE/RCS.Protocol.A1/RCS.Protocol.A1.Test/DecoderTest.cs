#region Using directives
using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;
#endregion

namespace UnitTest
{
  [TestFixture, Description("������������� ���������")]
  public class DecoderTest
  {
    [Test, Description("������ ���������")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void EmptyException()
    {
      const string message = "";
      CommonDescription command = Decoder.DecodeMessage(message);
    }

    [Test, Description("������ ��������� null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void EmptyNullException()
    {
      CommonDescription command = Decoder.DecodeMessage((string)null);
    }

    [Test, Description("��������� �����")]
    [ExpectedException(typeof(A1Exception))]
    public void LengthException()
    {
      const string message = "d500@p345.net %%..i01y0mKAHBZNMEMQcAMNNTLbZadAVgTAAAAAAAmeAMAAAAAAaAZNMQQQcAMNNTLbZadAowgZ38+8wIAm9gEogOZJUjcT8GuDY+I9EkAEAIAAAAANX";
      CommonDescription command = Decoder.DecodeMessage(message);
    }

    [Test, Description("������ CRC")]
    [ExpectedException(typeof(A1Exception))]
    public void CRCException()
    {
      const string message = "d500@p345.net %%..i01y0mKAHBZNMEMQcAMNNTLbZadAVgTAAAAAAAmeAMAAAAAAaAZNMQQQcAMNNTLbZadAowgZ38+8wIAm9gEogOZJUjcT8GuDY+I9EkAEAIAAAAANXwXXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      CommonDescription command = Decoder.DecodeMessage(message);
    }

    [Test, Description("����������� ������� ������ ���������")]
    [ExpectedException(typeof(A1Exception))]
    public void StartIndicatorMissingException()
    {
      const string message = "d500@p345.net ..i01y0mKAHBZNMEMQcAMNNTLbZadAVgTAAAAAAAmeAMAAAAAAaAZNMQQQcAMNNTLbZadAowgZ38+8wIAm9gEogOZJUjcT8GuDY+I9EkAEAIAAAAANXwXXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      CommonDescription command = Decoder.DecodeMessage(message);
    }

    [Test, Description("������� ������ ��������� �� �� ����� �������")]
    [ExpectedException(typeof(A1Exception))]
    public void StartIndicatorShiftException()
    {
      const string message = "d500@p345 %%..i01y0mKAHBZNMEMQcAMNNTLbZadAVgTAAAAAAAmeAMAAAAAAaAZNMQQQcAMNNTLbZadAowgZ38+8wIAm9gEogOZJUjcT8GuDY+I9EkAEAIAAAAANXwXXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      CommonDescription command = Decoder.DecodeMessage(message);
    }

    [Test, Description("������������� ������������ ��� �������")]
    public void SmsAddrConfig()
    {
      const string message = "d059@p314.net %%..i1035aKACxZMNQOQcBMMNHLbZadAAAAAAAATAAAAAAAAAAAAAAZMNQOQcBMMNHLbZadAowgYdcYZje8UMoYogPZJUjcT8GUMY6ogYzdYZ5f8UhMYX+XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(SmsAddrConfig));
      SmsAddrConfig config = (SmsAddrConfig)command;
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual(CommandDescriptor.SMSConfigAnswer, config.CommandID);
      Assert.AreEqual("5479", config.ShortID);
      Assert.AreEqual((ushort)11, config.MessageID);
      Assert.AreEqual("d059@p314.net", config.DsptEmailGprs);
      Assert.AreEqual("d059@p314.net", config.DsptEmailSMS);
      // ����� ������� �� ����� ����, �.� �� ����� ��������� 11 ��������.
      // ������ ����� ��� �������������� ��������� � �����-�� ������� ������
      // �� ������ �������� � ����� �� �����
      Assert.AreEqual("+380637562678", config.SmsCentre);
      Assert.AreEqual("+380672452724", config.SmsDspt);
      Assert.AreEqual("+380637562677", config.SmsEmailGate);
    }

    [Test, Description("������������� ������������ ���������� �������")]
    public void PhoneNumberConfig()
    {
      const string message = "d500@p345.net %%..iz5xzvFAcygYdcYZje9AAjAAgAYdYnZf93AAAIAgZwJUcoT9ANAAACgYdcYZfe+AtC8wACQTVBVSTxVAUiQUMfAgJoUQYZcac3XSU-gYdcYZje+AtC8wXyXXX-XXX-XXX-XXX-XXX-XXX-";
      //"              %%AAiz5xz3DAcwgYdcYZje9AAjAAgAYdYnZf93AAAIAgZwJUcoT9ANAAACgYdcYZfe+AtC8wACQTVBVSTxVAUiQUMfAgJoUQYZcac3XSU-gYdcYZje+AtC8wXyXXX-XXX-XXX-XXX-XXX-XXX-";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(PhoneNumberConfig));
      PhoneNumberConfig config = (PhoneNumberConfig)command;
      Assert.AreEqual(CommandDescriptor.PhoneConfigConfirm, config.CommandID);
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual("3913", config.ShortID);
      Assert.AreEqual((ushort)115, config.MessageID);
      Assert.AreEqual("80637562678", config.NumberAccept1);
      Assert.AreEqual("80637562677", config.NumberAccept2);
      Assert.AreEqual("80672452724", config.NumberAccept3);
      Assert.AreEqual("80637562677", config.NumberDspt);
      Assert.AreEqual("80637562678", config.NumberSOS);
      Assert.AreEqual("ALTUHOV", config.Name1);
      Assert.AreEqual("RCS1", config.Name2);
      Assert.AreEqual("Basis_KS", config.Name3);
    } // PhoneNumberConfig()

    [Test, Description("������������� ��������� ����������� �������")]
    public void EventConfig()
    {
      const string message = "d500@p345.net %%..iz5xz5FAdDAADzEiADyAAQABAEBAARAAAAAAAAAAAEAAARAAAEAAGBAGAAGAGAAEAREAAQAAAEAAABAAAEAAAAAAQA6+0GZKy9W90AAQABAHICAPAAAXX9XXX-XXX-XXX-XXX-XXX-XXX-";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(EventConfig));
      EventConfig config = (EventConfig)command;
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual(CommandDescriptor.EventConfigConfirm, config.CommandID);
      Assert.AreEqual("3913", config.ShortID);
      Assert.AreEqual((ushort)116, config.MessageID);
      Assert.AreEqual((sbyte)3, config.SpeedChange);
      Assert.AreEqual((ushort)15, config.CourseBend);
      Assert.AreEqual((ushort)5000, config.Distance1);
      Assert.AreEqual((ushort)200, config.Distance2);

      Assert.AreEqual((ushort)2, config.MinSpeed);
      Assert.AreEqual((ushort)7200, config.Timer1);
      Assert.AreEqual((ushort)60, config.Timer2);

      Assert.AreEqual((ushort)1, config.EventMask[0]);
      Assert.AreEqual((ushort)5, config.EventMask[1]);
      Assert.AreEqual((ushort)5, config.EventMask[2]);
      Assert.AreEqual((ushort)1, config.EventMask[3]);
      Assert.AreEqual((ushort)0, config.EventMask[4]);
      Assert.AreEqual((ushort)0, config.EventMask[5]);
      Assert.AreEqual((ushort)0, config.EventMask[6]);
      Assert.AreEqual((ushort)1, config.EventMask[7]);
      Assert.AreEqual((ushort)1, config.EventMask[8]);
      Assert.AreEqual((ushort)1, config.EventMask[9]);
      Assert.AreEqual((ushort)1, config.EventMask[10]);
      Assert.AreEqual((ushort)1, config.EventMask[11]);
      Assert.AreEqual((ushort)24, config.EventMask[12]);
      Assert.AreEqual((ushort)24, config.EventMask[13]);
      Assert.AreEqual((ushort)24, config.EventMask[14]);
      Assert.AreEqual((ushort)24, config.EventMask[15]);
      Assert.AreEqual((ushort)272, config.EventMask[16]);
      Assert.AreEqual((ushort)272, config.EventMask[17]);
      Assert.AreEqual((ushort)1, config.EventMask[18]);
      Assert.AreEqual((ushort)1, config.EventMask[19]);
      Assert.AreEqual((ushort)1, config.EventMask[20]);
      Assert.AreEqual((ushort)0, config.EventMask[21]);
      Assert.AreEqual((ushort)1, config.EventMask[22]);
      // ������� ������ ������������ ������ 23 ��������.
      // ���������� �������� ��� � ���� ������
    }


    [Test, Description("������������� ������������ ���������� ����������")]
    [Ignore("� ����� ������ �� ����� �� ������ ��������������� ���������")]
    public void UniqueConfig()
    {
      const string message = "";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(UniqueConfig));
      UniqueConfig config = (UniqueConfig)command;
      Assert.AreEqual(CommandDescriptor.UniqueConfigConfirm, config.CommandID);
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual("3913", config.ShortID);
      Assert.AreEqual((ushort)115, config.MessageID);

    }

    [Test, Description("������������� ������������ ����������������� ����������")]
    public void IdConfig()
    {
      const string message = "              %%..i01x2PLABDNNMUNNMONMMAMMVFVMMYQAABFs-z-------------------------MLvMOICIIIAIIIAIIIAIA-w-XX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(IdConfig));
      IdConfig config = (IdConfig)command;
      Assert.AreEqual(CommandDescriptor.IdConfigAnswer, config.CommandID);
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual("4516", config.ShortID);
      Assert.AreEqual((ushort)4, config.MessageID);
      Assert.AreEqual("4516", config.DevIdShort);
      Assert.AreEqual("7040011TT21A", config.DevIdLong);
      Assert.AreEqual("����", config.ModuleIdGps);
      Assert.AreEqual("����", config.ModuleIdGsm);
      //Assert.AreEqual("����", config.ModuleIdMm);
      Assert.AreEqual("����", config.ModuleIdRf);
      Assert.AreEqual("����", config.ModuleIdSs);
      Assert.AreEqual("3.28           ", config.VerProtocolLong);
      Assert.AreEqual("��", config.VerProtocolShort);
    }

    [Test, Description("������������� ������ �� ������� ������������ ����������� ���")]
    public void ZoneConfigConfirm()
    {
      const string message = "d119@p314.net %%..i410wpGAABAAWMqAAXXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(ZoneConfigConfirm));
      ZoneConfigConfirm config = (ZoneConfigConfirm)command;
      Assert.AreEqual(CommandDescriptor.ZoneConfigConfirm, config.CommandID);
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual("8540", config.ShortID);
      Assert.AreEqual((ushort)0, config.MessageID);
      Assert.AreEqual(219307, config.ZoneMsgID);
      Assert.AreEqual((byte)1, config.ZoneState);
      Assert.AreEqual((byte)1, config.Result);
    }

    [Test, Description("������������� ������ �� ������� ������� GPS ������")]
    public void DataGpsAnswer()
    {
      const string message = "d038@p314.net %%..iz1zwiMACBA-A-RPzF0ALjHgAOG5UjjHIQAANAmaAAAAIAAAAAAAAAAARQPzthALHYuAGx4Ki7HMAFANlweAAAAIAAAAAAAAAAARPUzsAALHhWAG4s1iHPKAABNleIAAAACAAAAAAAAAAA";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(DataGpsAnswer));
      DataGpsAnswer config = (DataGpsAnswer)command;
      Assert.AreEqual(CommandDescriptor.DataGpsAuto, config.CommandID);
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual("3530", config.ShortID);
      Assert.AreEqual((ushort)8, config.MessageID);
      Assert.AreEqual((ushort)1023, config.WhatWrite);
      Assert.AreEqual(3, config.DataGpsList.Count);

      Assert.AreEqual(13464, config.DataGpsList[0].LogID);
      Assert.AreEqual(1161678035, config.DataGpsList[0].Time);
      Assert.AreEqual(30224670, config.DataGpsList[0].Latitude);
      Assert.AreEqual(18279220, config.DataGpsList[0].Longitude);
      Assert.AreEqual((byte)140, config.DataGpsList[0].Altitude);
      Assert.AreEqual((byte)28, config.DataGpsList[0].Direction);
      Assert.AreEqual((byte)33, config.DataGpsList[0].Speed);
      Assert.AreEqual((byte)104, config.DataGpsList[0].Flags);
      Assert.AreEqual((uint)32, config.DataGpsList[0].Events);
      Assert.AreEqual(true, config.DataGpsList[0].Valid);
      Assert.AreEqual((byte)0, config.DataGpsList[0].Sensor1);
      Assert.AreEqual((byte)0, config.DataGpsList[0].Sensor2);
      Assert.AreEqual((byte)0, config.DataGpsList[0].Sensor3);
      Assert.AreEqual((byte)0, config.DataGpsList[0].Sensor4);
      Assert.AreEqual((byte)0, config.DataGpsList[0].Sensor5);
      Assert.AreEqual((byte)0, config.DataGpsList[0].Sensor6);
      Assert.AreEqual((byte)0, config.DataGpsList[0].Sensor7);
      Assert.AreEqual((byte)0, config.DataGpsList[0].Sensor8);

      Assert.AreEqual(13463, config.DataGpsList[1].LogID);
      Assert.AreEqual(1161678006, config.DataGpsList[1].Time);
      Assert.AreEqual(30222650, config.DataGpsList[1].Latitude);
      Assert.AreEqual(18276260, config.DataGpsList[1].Longitude);
      Assert.AreEqual((byte)139, config.DataGpsList[1].Altitude);
      Assert.AreEqual((byte)29, config.DataGpsList[1].Direction);
      Assert.AreEqual((byte)49, config.DataGpsList[1].Speed);
      Assert.AreEqual((byte)120, config.DataGpsList[1].Flags);
      Assert.AreEqual((uint)32, config.DataGpsList[1].Events);
      Assert.AreEqual(true, config.DataGpsList[1].Valid);
      Assert.AreEqual((byte)0, config.DataGpsList[1].Sensor1);
      Assert.AreEqual((byte)0, config.DataGpsList[1].Sensor2);
      Assert.AreEqual((byte)0, config.DataGpsList[1].Sensor3);
      Assert.AreEqual((byte)0, config.DataGpsList[1].Sensor4);
      Assert.AreEqual((byte)0, config.DataGpsList[1].Sensor5);
      Assert.AreEqual((byte)0, config.DataGpsList[1].Sensor6);
      Assert.AreEqual((byte)0, config.DataGpsList[1].Sensor7);
      Assert.AreEqual((byte)0, config.DataGpsList[1].Sensor8);

      Assert.AreEqual(13462, config.DataGpsList[2].LogID);
      Assert.AreEqual(1161678000, config.DataGpsList[2].Time);
      Assert.AreEqual(30222130, config.DataGpsList[2].Latitude);
      Assert.AreEqual(18275430, config.DataGpsList[2].Longitude);
      Assert.AreEqual((byte)139, config.DataGpsList[2].Altitude);
      Assert.AreEqual((byte)28, config.DataGpsList[2].Direction);
      Assert.AreEqual((byte)41, config.DataGpsList[2].Speed);
      Assert.AreEqual((byte)120, config.DataGpsList[2].Flags);
      Assert.AreEqual((uint)8, config.DataGpsList[2].Events);
      Assert.AreEqual(true, config.DataGpsList[2].Valid);
      Assert.AreEqual((byte)0, config.DataGpsList[2].Sensor1);
      Assert.AreEqual((byte)0, config.DataGpsList[2].Sensor2);
      Assert.AreEqual((byte)0, config.DataGpsList[2].Sensor3);
      Assert.AreEqual((byte)0, config.DataGpsList[2].Sensor4);
      Assert.AreEqual((byte)0, config.DataGpsList[2].Sensor5);
      Assert.AreEqual((byte)0, config.DataGpsList[2].Sensor6);
      Assert.AreEqual((byte)0, config.DataGpsList[2].Sensor7);
      Assert.AreEqual((byte)0, config.DataGpsList[2].Sensor8);
    }

    [Test, Description("������������� ������������ �������� �������� GPRS")]
    public void GprsBaseConfig()
    {
      const string message = "d119@p314.net %%..i410yaPAKAACd4ddLvdbY1LdYWAAAAAAAAAAAAAAAAAAAAAQU0SQAGAAAAAAWAWAAAAAAAAAOAMLMoNNLlNNLiMMAOAAAA--------------APaZdlAAAAAAAAAdYQcAACAAAAAXX8XXX-";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(GprsBaseConfig));
      GprsBaseConfig config = (GprsBaseConfig)command;
      Assert.AreEqual(CommandDescriptor.GprsBaseConfigConfirm, config.CommandID);
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual((ushort)10, config.Mode);
      Assert.AreEqual("8542", config.ShortID);
      Assert.AreEqual((ushort)40, config.MessageID);
      Assert.AreEqual("www.umc.ua", config.ApnServer);
      Assert.AreEqual("", config.ApnLogin);
      Assert.AreEqual("", config.ApnPassword);
      Assert.AreEqual("80.255.64.23", config.DnsServer);
      Assert.AreEqual("�����������", config.DialNumber);
      Assert.AreEqual("", config.GprsLogin);
      Assert.AreEqual("", config.GprsPassword);
    }

    [Test, Description("������������� ������������ �������� EMAIL GPRS")]
    public void GprsEmailConfig()
    {
      const string message = "d119@p314.net %%..i410yWPAJhcbdHcLcIMMNHLbZadAAAAAAAAAAAAAAAAAbgbbAKAAAAAAAAddd-AAAAAAAAbaZ0adLgcMMcNLboZdABAAAAAAAAAAAAAONQNMQIAZAEAASQdcV4ONNEMAACXXX-XXX-XXX-";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(GprsEmailConfig));
      GprsEmailConfig config = (GprsEmailConfig)command;
      Assert.AreEqual(CommandDescriptor.GprsEmailConfigConfirm, config.CommandID);
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual("8542", config.ShortID);
      Assert.AreEqual((ushort)38, config.MessageID);
      Assert.AreEqual("smtp.p314.net", config.SmtpServer);
      Assert.AreEqual("", config.SmtpLogin);
      Assert.AreEqual("", config.SmtpPassword);
      Assert.AreEqual("light.p314.net", config.Pop3Server);
      Assert.AreEqual("8542@", config.Pop3Login);
      Assert.AreEqual("ItrW8542", config.Pop3Password);
    }

    [Test, Description("������������� ������������ �������� FTP GPRS")]
    [Ignore("��� �� �����������. ��� ������ � ����.")]
    public void GprsFtpConfig()
    {
      const string message = "";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(GprsFtpConfig));
      GprsFtpConfig config = (GprsFtpConfig)command;
      Assert.AreEqual(CommandDescriptor.GprsFtpConfigConfirm, config.CommandID);
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual("8542", config.ShortID);
      Assert.AreEqual((ushort)38, config.MessageID);
      Assert.AreEqual("", config.Server);
      Assert.AreEqual("", config.Login);
      Assert.AreEqual("", config.Password);
      Assert.AreEqual("", config.ConfigPath);
      Assert.AreEqual("", config.PutPath);
    }

    [Test, Description("������������� ������������ �������� Socket GPRS")]
    [Ignore("��� ������ � ����.")]
    public void GprsSocketConfig()
    {
      const string message = "";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(GprsSocketConfig));
      GprsSocketConfig config = (GprsSocketConfig)command;
      Assert.AreEqual(CommandDescriptor.GprsSocketConfigConfirm, config.CommandID);
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual("8542", config.ShortID);
      Assert.AreEqual((ushort)38, config.MessageID);
      Assert.AreEqual("", config.Server);
      Assert.AreEqual((ushort)0, config.Port);
    }

    [Test, Description("������������� �������� ����������� � ���������� GPRS")]
    public void GprsProviderConfig()
    {
      const string message = "d119@p314.net %%..i410yGQAJAAVLQSTSZVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAbcLjcMMcNLboZdABAAAAAAAAAAAAAAAAXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      CommonDescription command = Decoder.DecodeMessage(message);
      Assert.AreEqual(command.GetType(), typeof(GprsProviderConfig));
      GprsProviderConfig config = (GprsProviderConfig)command;
      Assert.AreEqual(CommandDescriptor.GprsProviderConfigConfirm, config.CommandID);
      Assert.Greater(config.Message.Length, 0);
      Assert.Greater(config.Source.Length, 0);
      Assert.AreEqual("8542", config.ShortID);
      Assert.AreEqual((ushort)36, config.MessageID);
      Assert.AreEqual("", config.InitString);
      Assert.AreEqual("pop.p314.net", config.Domain);
    }
  }
}
