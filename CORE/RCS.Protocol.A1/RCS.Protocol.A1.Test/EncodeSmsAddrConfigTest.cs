using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ������������ ��� �������")]
  public class EncodeSmsAddrConfigTest
  {
    [Test, Description("������������ ��� ������� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void SmsAddrConfigNull()
    {
      string result = Encoder.EncodeSmsAddrConfigSet(null);
    }

    [Test, Description("������������ ������������ ��� ������� DsptEmailGprs")]
    [ExpectedException(typeof(A1Exception))]
    public void SmsAddrConfigBigDsptEmailGprs()
    {
      SmsAddrConfig config = new SmsAddrConfig();
      config.DsptEmailGprs = "d50089753fdfff@p345wwwwqwerty.net";
      string result = Encoder.EncodeSmsAddrConfigSet(config);
    }

    [Test, Description("������������ ������������ ��� ������� DsptEmailSMS")]
    [ExpectedException(typeof(A1Exception))]
    public void SmsAddrConfigBigDsptEmailSMS()
    {
      SmsAddrConfig config = new SmsAddrConfig();
      config.DsptEmailSMS = "d50089753fdfff@p345wwwwqwerty.net";
      string result = Encoder.EncodeSmsAddrConfigSet(config);
    }

    [Test, Description("������������ ������������ ��� ������� SmsCentre")]
    [ExpectedException(typeof(A1Exception))]
    public void SmsAddrConfigBigSmsCentre()
    {
      SmsAddrConfig config = new SmsAddrConfig();
      config.SmsCentre = "+3806724527244500986";
      string result = Encoder.EncodeSmsAddrConfigSet(config);
    }

    [Test, Description("������������ ������������ ��� ������� SmsDspt")]
    [ExpectedException(typeof(A1Exception))]
    public void SmsAddrConfigBigSmsDspt()
    {
      SmsAddrConfig config = new SmsAddrConfig();
      config.SmsDspt = "+3806724527244500986";
      string result = Encoder.EncodeSmsAddrConfigSet(config);
    }

    [Test, Description("������������ ������������ ��� ������� SmsEmailGate")]
    [ExpectedException(typeof(A1Exception))]
    public void SmsAddrConfigBigSmsEmailGate()
    {
      SmsAddrConfig config = new SmsAddrConfig();
      config.SmsEmailGate = "+3806724527244500986";
      string result = Encoder.EncodeSmsAddrConfigSet(config);
    }

    [Test, Description("����������� ������������ ��� �������")]
    public void SmsAddrConfig()
    {
      SmsAddrConfig config = new SmsAddrConfig();
      config.MessageID = 28;
      config.ShortID = "4938";
      config.DsptEmailGprs = "d500@p345.net";
      config.DsptEmailSMS = "d500@p345.net";
      config.SmsCentre = "+38067";
      config.SmsDspt = "80672452724";
      config.SmsEmailGate = "20";

      string result = Encoder.EncodeSmsAddrConfigSet(config);
      const string expected = "              %%AAi05z4DCAHDZNMEMQcAMNNTLbZadAAAAAAAAAAAAAAAAAAAAAAAZNMEMQcAMNNTLbZadAowgZ38AAAAAAAAAgZwJUcoT8ABAAAAI3AMAAAAAAAAAAXwXXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }

    [Test, Description("������ ������������ ��� ������� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void SmsAddrConfigQueryNull()
    {
      string result = Encoder.EncodeSmsAddrConfigQuery(null);
    }

    [Test, Description("����������� ������� ������������ ��� �������")]
    public void SmsAddrConfigQuery()
    {
      SimpleQuery config = new SimpleQuery();
      config.ShortID = "1010";
      config.MessageID = 45;

      string result = Encoder.EncodeSmsAddrConfigQuery(config);
      const string expected = "              %%AAixwxwtHALTXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }
  }
}
