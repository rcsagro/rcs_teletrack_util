using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ������������ �������� EMAIL GPRS")]
  public class EncodeGprsEmailConfigTest
  {
    [Test, Description("������������ �������� EMAIL GPRS = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void GprsEmailConfigNull()
    {
      string result = Encoder.EncodeGprsEmailConfigSet(null);
    }

    [Test, Description("������������ ������������ �������� EMAIL GPRS SmtpServer")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsEmailConfigBigSmtpServer()
    {
      GprsEmailConfig config = new GprsEmailConfig();
      config.SmtpServer = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsEmailConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� EMAIL GPRS SmtpLogin")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsEmailConfigBigSmtpLogin()
    {
      GprsEmailConfig config = new GprsEmailConfig();
      config.SmtpLogin = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsEmailConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� EMAIL GPRS SmtpPassword")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsEmailConfigBigSmtPassword()
    {
      GprsEmailConfig config = new GprsEmailConfig();
      config.SmtpPassword = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsEmailConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� EMAIL GPRS Pop3Server")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsEmailConfigBigPop3Server()
    {
      GprsEmailConfig config = new GprsEmailConfig();
      config.Pop3Server = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsEmailConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� EMAIL GPRS Pop3Login")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsEmailConfigBigPop3Login()
    {
      GprsEmailConfig config = new GprsEmailConfig();
      config.Pop3Login = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsEmailConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� EMAIL GPRS Pop3Password")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsEmailConfigBigPop3Password()
    {
      GprsEmailConfig config = new GprsEmailConfig();
      config.Pop3Password = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsEmailConfigSet(config);
    }

    [Test, Description("����������� ������������ �������� EMAIL GPRS")]
    public void GprsEmailConfig()
    {
      GprsEmailConfig config = new GprsEmailConfig();
      config.MessageID = 38;
      config.ShortID = "8542";
      config.SmtpServer = "smtp.p314.net";
      config.SmtpLogin = "";
      config.SmtpPassword = "";
      config.Pop3Server = "light.p314.net";
      config.Pop3Login = "8542@";
      config.Pop3Password = "ItrW8542";

      string result = Encoder.EncodeGprsEmailConfigSet(config);
      const string expected = "              %%AAi410yeMAJjcbdHcLcIMMNHLbZadAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAbaZ0adLgcMMcNLboZdABAAAAAAAAAAAAAONQNMQIAAAAAASQdcV4ONNEMAACXXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }

    [Test, Description("������ ������������ �������� EMAIL GPRS = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void GprsEmailQueryNull()
    {
      string result = Encoder.EncodeGprsEmailConfigQuery(null);
    }

    [Test, Description("����������� ������� ������������ �������� EMAIL GPRS")]
    public void GprsEmailConfigQuery()
    {
      SimpleQuery config = new SimpleQuery();
      config.ShortID = "8542";
      config.MessageID = 45;

      string result = Encoder.EncodeGprsEmailConfigQuery(config);
      const string expected = "              %%AAi410y7RALTXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }
  }
}
