using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ������������ ����������� ���")]
  public class EncodeZoneConfigTest
  {
    [Test, Description("������������ ����������� ��� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ZoneConfigNull()
    {
      string[] result = Encoder.EncodeZoneConfigSet(null);
    }

    [Test, Description("������������ ������������ ����������� ��� pointCount")]
    [ExpectedException(typeof(A1Exception))]
    public void ZoneConfigBigPointCount()
    {
      ZoneConfig config = new ZoneConfig();
      ZonePoint[] points = new ZonePoint[150];
      for (int i = 0; i < 150; i++)
      {
        points[i] = new ZonePoint(21 * 600000, (int)(45.2 * 600000));
      }
      config.AddZone(new Zone(new ZoneMask(1, 0, 1, 0), points));

      points = new ZonePoint[150];
      for (int i = 0; i < 150; i++)
      {
        points[i] = new ZonePoint(25 * 600000, (int)(48.7 * 600000));
      }
      config.AddZone(new Zone(new ZoneMask(0, 0, 1, 0), points));

      string[] result = Encoder.EncodeZoneConfigSet(config);
    }

    [Test, Description("����������� ������������ ����������� ���. ��������� ���������� - ��� �������� ��������� � ����� ������")]
    public void ZoneConfig()
    {
      ZoneConfig config = new ZoneConfig();
      config.ShortID = "1010";
      config.MessageID = 45;
      config.ZoneMsgID = 123323;

      ZonePoint[] points = new ZonePoint[4];
      for (int i = 0; i < 4; i++)
      {
        points[i] = new ZonePoint((21 + i) * 600000, (int)((45.2 + 1) * 600000));
      }
      config.AddZone(new Zone(new ZoneMask(1, 0, 1, 0), points));

      points = new ZonePoint[10];
      for (int i = 0; i < 10; i++)
      {
        points[i] = new ZonePoint((25 + i) * 600000, (int)((48.7 + 1) * 600000));
      }
      config.AddZone(new Zone(new ZoneMask(0, 0, 1, 0), points));

      points = new ZonePoint[5];
      for (int i = 0; i < 5; i++)
      {
        points[i] = new ZonePoint((20 + i) * 600000, (int)((41.1 + 1) * 600000));
      }
      config.AddZone(new Zone(new ZoneMask(0, 1, 0, 1), points));

      string[] result = Encoder.EncodeZoneConfigSet(config);
      Assert.IsNotNull(config.Source);
      Assert.AreEqual(25, result.Length);
      Assert.AreEqual(25, config.Source.Count);
      for (int i = 0; i < 25; i++)
      {
        Assert.IsTrue(config.Source[i].Length == Const.LEVEL0_PACKET_LENGTH);
        Assert.IsTrue(config.Message[i].Length == Const.LEVEL0_PACKET_LENGTH);
      }
    }
  }
}
