using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ������ gps ������")]
  public class EncodeDataGpsTest
  {
    [Test, Description("������ gps ������ = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void DataGpsQueryNull()
    {
      string result = Encoder.EncodeDataGpsQuery(null);
    }

    [Test, Description("������������ ������������ ������� gps ������ StartLongitude")]
    [ExpectedException(typeof(A1Exception))]
    public void DataGpsBigStartLongitude()
    {
      DataGpsQuery config = new DataGpsQuery();
      config.StartLongitude = Const.MAX_LONGITUDE + 10;
      string result = Encoder.EncodeDataGpsQuery(config);
    }

    [Test, Description("������������ ������������ ������� gps ������ EndLongitude")]
    [ExpectedException(typeof(A1Exception))]
    public void DataGpsBigEndLongitude()
    {
      DataGpsQuery config = new DataGpsQuery();
      config.EndLongitude = Const.MAX_LONGITUDE + 10;
      string result = Encoder.EncodeDataGpsQuery(config);
    }

    [Test, Description("������������ ������������ ������� gps ������ StartLatitude")]
    [ExpectedException(typeof(A1Exception))]
    public void DataGpsBigStartLatitude()
    {
      DataGpsQuery config = new DataGpsQuery();
      config.StartLatitude = Const.MAX_LATITUDE + 10;
      string result = Encoder.EncodeDataGpsQuery(config);
    }

    [Test, Description("������������ ������������ ������� gps ������ EndLatitude")]
    [ExpectedException(typeof(A1Exception))]
    public void DataGpsBigEndLatitude()
    {
      DataGpsQuery config = new DataGpsQuery();
      config.EndLatitude = Const.MAX_LATITUDE + 10;
      string result = Encoder.EncodeDataGpsQuery(config);
    }

    [Test, Description("����������� ������� gps ������")]
    public void DataGpsQuery()
    {
      DataGpsQuery config = new DataGpsQuery();
      config.MessageID = 14;
      config.ShortID = "9195";
      config.CheckMask = 1;
      config.Events = 0;
      config.LastRecords = 8;
      config.LastTimes = 0;
      config.MaxSmsNumber = 0;
      config.WhatSend = 1023;
      config.StartTime = -10800;
      config.StartSpeed = 0;
      config.StartLatitude = 0;
      config.StartLongitude = 0;
      config.StartAltitude = 0;
      config.StartDirection = 0;
      config.StartLogId = 0;
      config.EndTime = -10800;
      config.EndSpeed = 0;
      config.EndLatitude = 0;
      config.EndLongitude = 0;
      config.EndAltitude = 0;
      config.EndDirection = 0;
      config.EndLogId = 0;
      config.LogId1 = 0;
      config.LogId2 = 0;
      config.LogId3 = 0;
      config.LogId4 = 0;
      config.LogId5 = 0;

      string result = Encoder.EncodeDataGpsQuery(config);
      // ������� �� ����:
      //              %%AAi5x513JADgAAAAAAABAAAAAACA--1f0AAAAAAAAAAAAAAw----10ABAAAAAAAAAAAAAAAAAAAA--1f0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXX8XXX-XXX-XXX-
      // �������������� ���������:
      //              %%AAi5x51AJADgAAAAAAABAAAAAACAAAAAAAAAAAAAAAAAAAAw----10ABAAAAAAAAAAAAAAAAAAAA--1f0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXX8XXX-XXX-XXX-
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
    }
  }
}
