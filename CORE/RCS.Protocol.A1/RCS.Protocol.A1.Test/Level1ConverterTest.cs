#region Using directives
using System;
using NUnit.Framework;
using RCS.Protocol.A1;
#endregion

namespace UnitTest
{
  [TestFixture, Description("��������� ������ �� ������ 1 � 2")]
  public class Level1ConverterTest
  {
    [Test, Description("����� BytesToString ������ ��������")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void BytesToString()
    {
      Level1Converter.BytesToString(null);
    }

    [Test, Description("����� Decode6BitTo8 ������ ��������")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Decode6BitTo8NullParam()
    {
      Level1Converter.Decode6BitTo8(null);
    }

    [Test, Description("����� Decode6BitTo8 �������� ������������ �����")]
    [ExpectedException(typeof(ArgumentException))]
    public void Decode6BitTo8WrongParam()
    {
      Level1Converter.Decode6BitTo8(new byte[] { 1, 2, 3 });
    }

    [Test, Description("����� Encode8BitTo6 ������ ��������")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Encode8BitTo6NullParam()
    {
      Level1Converter.Encode8BitTo6(null);
    }

    [Test, Description("����� Encode8BitTo6 �������� ������������ �����")]
    [ExpectedException(typeof(ArgumentException))]
    public void Encode8BitTo6WrongParam()
    {
      Level1Converter.Encode8BitTo6(new byte[] { 1, 2, 3 });
    }
  }
}
