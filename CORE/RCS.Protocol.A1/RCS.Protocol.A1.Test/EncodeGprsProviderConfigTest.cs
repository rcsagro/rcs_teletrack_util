using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ������������ �������� ����������� � ���������� GPRS")]
  public class EncodeGprsProviderConfigTest
  {
    [Test, Description("������������ �������� ����������� � ���������� GPRS = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void GprsProviderConfigNull()
    {
      string result = Encoder.EncodeGprsProviderConfigSet(null);
    }

    [Test, Description("������������ ������������ �������� ����������� � ���������� GPRS InitString")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsProviderConfigBigInitString()
    {
      GprsProviderConfig config = new GprsProviderConfig();
      config.InitString = "aaaaaaaaaasssssssssddddddddddffffffffffgggggggggghhhhhhhhhh";
      string result = Encoder.EncodeGprsProviderConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� ����������� � ���������� GPRS Domain")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsProviderConfigBigDomain()
    {
      GprsProviderConfig config = new GprsProviderConfig();
      config.Domain = "aaaaaaaaaasssssssssddddddddddffffffffffgggggggggghhhhhhhhhh";
      string result = Encoder.EncodeGprsProviderConfigSet(config);
    }

    [Test, Description("����������� �������� ����������� � ���������� GPRS")]
    public void GprsProviderConfig()
    {
      GprsProviderConfig config = new GprsProviderConfig();
      config.MessageID = 36;
      config.ShortID = "8542";
      config.InitString = "";
      config.Domain = "pop.p314.net";

      string result = Encoder.EncodeGprsProviderConfigSet(config);
      const string expected = "              %%AAi410yqNAJCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcAbcLjcMMcNLboZdABAAAAAAAAAAAAAAAAXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }

    [Test, Description("������ ������������ �������� ����������� � ���������� GPRS = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void GprsProviderQueryNull()
    {
      string result = Encoder.EncodeGprsProviderConfigQuery(null);
    }

    [Test, Description("����������� ������� �������� ����������� � ���������� GPRS")]
    public void GprsProviderConfigQuery()
    {
      SimpleQuery config = new SimpleQuery();
      config.ShortID = "8542";
      config.MessageID = 45;

      string result = Encoder.EncodeGprsProviderConfigQuery(config);
      const string expected = "              %%AAi410y7SALSXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }
  }
}
