using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ������������ ���������� ����������")]
  public class EncodeUniqueConfigTest
  {
    [Test, Description("������������ ���������� ���������� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void UniqueConfigNull()
    {
      string result = Encoder.EncodeUniqueConfigSet(null);
    }

    [Test, Description("������������ ������������ ���������� ���������� DispatcherID")]
    [ExpectedException(typeof(A1Exception))]
    public void UniqueConfigBigEventMask()
    {
      UniqueConfig config = new UniqueConfig();
      config.DispatcherID = "55445";
      string result = Encoder.EncodeUniqueConfigSet(config);
    }

    [Test, Description("������������ ������������ ���������� ���������� Password")]
    [ExpectedException(typeof(A1Exception))]
    public void UniqueConfigBigPassword()
    {
      UniqueConfig config = new UniqueConfig();
      config.Password = "55445ddf45";
      string result = Encoder.EncodeUniqueConfigSet(config);
    }

    [Test, Description("������������ ������������ ���������� ���������� TmpPassword")]
    [ExpectedException(typeof(A1Exception))]
    public void UniqueConfigBigTmpPassword()
    {
      UniqueConfig config = new UniqueConfig();
      config.TmpPassword = "fff55ew445";
      string result = Encoder.EncodeUniqueConfigSet(config);
    }

    [Test, Description("����������� ������������ ���������� ����������")]
    public void UniqueConfig()
    {
      UniqueConfig config = new UniqueConfig();
      config.MessageID = 28;
      config.ShortID = "4938";
      config.DispatcherID = "5531";
      config.Password = "D45wwer";
      config.TmpPassword = "321";

      string result = Encoder.EncodeUniqueConfigSet(config);
      const string expected = "              %%AAi05z43DAHCNNM1MRNBNdd9ZcAJMMMbAAAAAAXwXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }

    [Test, Description("������ ������������ ���������� ���������� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void UniqueConfigQueryNull()
    {
      string result = Encoder.EncodeUniqueConfigQuery(null);
    }

    [Test, Description("����������� ������� ������������ ���������� ����������")]
    public void UniqueConfigQuery()
    {
      SimpleQuery config = new SimpleQuery();
      config.ShortID = "1010";
      config.MessageID = 45;

      string result = Encoder.EncodeUniqueConfigQuery(config);
      const string expected = "              %%AAixwxwtIALSXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }
  }
}
