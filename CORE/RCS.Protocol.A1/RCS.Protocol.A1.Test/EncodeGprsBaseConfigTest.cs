using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ������������ �������� �������� GPRS")]
  public class EncodeGprsBaseConfigTest
  {
    [Test, Description("������������ �������� �������� GPRS = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void GprsBaseConfigNull()
    {
      string result = Encoder.EncodeGprsBaseConfigSet(null);
    }

    [Test, Description("������������ ������������ �������� �������� GPRS ApnServer")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsBaseConfigBigApnServer()
    {
      GprsBaseConfig config = new GprsBaseConfig();
      config.ApnServer = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsBaseConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� �������� GPRS ApnLogin")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsBaseConfigBigApnLogin()
    {
      GprsBaseConfig config = new GprsBaseConfig();
      config.ApnLogin = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsBaseConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� �������� GPRS ApnPassword")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsBaseConfigBigApnPassword()
    {
      GprsBaseConfig config = new GprsBaseConfig();
      config.ApnPassword = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsBaseConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� �������� GPRS DnsServer")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsBaseConfigBigDnsServer()
    {
      GprsBaseConfig config = new GprsBaseConfig();
      config.DnsServer = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsBaseConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� �������� GPRS GprsLogin")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsBaseConfigBigGprsLogin()
    {
      GprsBaseConfig config = new GprsBaseConfig();
      config.GprsLogin = "eerewtretyrretfzcvftr";
      string result = Encoder.EncodeGprsBaseConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� �������� GPRS GprsPassword")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsBaseConfigBigGprsPassword()
    {
      GprsBaseConfig config = new GprsBaseConfig();
      config.GprsPassword = "eerewtretyrretfzcvftr";
      string result = Encoder.EncodeGprsBaseConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� �������� GPRS DialNumber")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsBaseConfigBigDialNumber()
    {
      GprsBaseConfig config = new GprsBaseConfig();
      config.DialNumber = "+3805635763567467876";
      string result = Encoder.EncodeGprsBaseConfigSet(config);
    }

    [Test, Description("����������� ������������ �������� �������� GPRS")]
    public void GprsBaseConfig()
    {
      GprsBaseConfig config = new GprsBaseConfig();
      config.MessageID = 40;
      config.ShortID = "8542";
      config.Mode = 10;
      config.ApnServer = "www.umc.ua";
      config.ApnLogin = "";
      config.ApnPassword = "";
      config.DnsServer = "80.255.64.23";
      config.DialNumber = "�����������";
      config.GprsLogin = "";
      config.GprsPassword = "";

      string result = Encoder.EncodeGprsBaseConfigSet(config);
      const string expected = "              %%AAi410yMMAKCACd4ddLvdbY1LdYWAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOAMLMoNNLlNNLiMMAOAAAA--------------APAAAAAAAAAAAAAAAAAAAAAAAAAXX8XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }

    [Test, Description("������ ������������ �������� �������� GPRS = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void GprsBaseQueryNull()
    {
      string result = Encoder.EncodeGprsBaseConfigQuery(null);
    }

    [Test, Description("����������� ������� ������������ �������� �������� GPRS")]
    public void GprsBaseConfigQuery()
    {
      SimpleQuery config = new SimpleQuery();
      config.ShortID = "8542";
      config.MessageID = 45;

      string result = Encoder.EncodeGprsBaseConfigQuery(config);
      const string expected = "              %%AAi410y7RALSXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }
  }
}
