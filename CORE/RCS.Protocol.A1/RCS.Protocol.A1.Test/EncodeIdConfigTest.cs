using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ������������ ����������������� ����������")]
  public class EncodeIdConfigTest
  {
    [Test, Description("������������ ����������������� ���������� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void IdConfigNull()
    {
      string result = Encoder.EncodeIdConfigSet(null);
    }

    [Test, Description("������������ ������������ ����������������� ���������� DevIdShort")]
    [ExpectedException(typeof(A1Exception))]
    public void IdConfigBigDevIdShort()
    {
      IdConfig config = new IdConfig();
      config.DevIdShort = "21232";
      string result = Encoder.EncodeIdConfigSet(config);
    }

    [Test, Description("������������ ������������ ����������������� ���������� DevIdLong")]
    [ExpectedException(typeof(A1Exception))]
    public void IdConfigBigDevIdLong()
    {
      IdConfig config = new IdConfig();
      config.DevIdLong = "7040011TT21A456r3";
      string result = Encoder.EncodeIdConfigSet(config);
    }

    [Test, Description("������������ ������������ ����������������� ���������� ModuleIdGps")]
    [ExpectedException(typeof(A1Exception))]
    public void IdConfigBigModuleIdGps()
    {
      IdConfig config = new IdConfig();
      config.ModuleIdGps = "70403";
      string result = Encoder.EncodeIdConfigSet(config);
    }

    [Test, Description("������������ ������������ ����������������� ���������� ModuleIdGsm")]
    [ExpectedException(typeof(A1Exception))]
    public void IdConfigBigModuleIdGsm()
    {
      IdConfig config = new IdConfig();
      config.ModuleIdGsm = "70403";
      string result = Encoder.EncodeIdConfigSet(config);
    }

    [Test, Description("������������ ������������ ����������������� ���������� ModuleIdMm")]
    [ExpectedException(typeof(A1Exception))]
    [Ignore("ModuleIdMm ���� �� ������������")]
    public void IdConfigBigModuleIdMm()
    {
      IdConfig config = new IdConfig();
      //config.ModuleIdMm = "70403";
      string result = Encoder.EncodeIdConfigSet(config);
    }

    [Test, Description("������������ ������������ ����������������� ���������� ModuleIdRf")]
    [ExpectedException(typeof(A1Exception))]
    public void IdConfigBigModuleIdRf()
    {
      IdConfig config = new IdConfig();
      config.ModuleIdRf = "70403";
      string result = Encoder.EncodeIdConfigSet(config);
    }

    [Test, Description("������������ ������������ ����������������� ���������� ModuleIdSs")]
    [ExpectedException(typeof(A1Exception))]
    public void IdConfigBigModuleIdSs()
    {
      IdConfig config = new IdConfig();
      config.ModuleIdSs = "70403";
      string result = Encoder.EncodeIdConfigSet(config);
    }

    [Test, Description("������������ ������������ ����������������� ���������� VerProtocolLong")]
    [ExpectedException(typeof(A1Exception))]
    public void IdConfigBigVerProtocolLong()
    {
      IdConfig config = new IdConfig();
      config.VerProtocolLong = "704034534654635209009";
      string result = Encoder.EncodeIdConfigSet(config);
    }

    [Test, Description("������������ ������������ ����������������� ���������� VerProtocolShort")]
    [ExpectedException(typeof(A1Exception))]
    public void IdConfigBigVerProtocolShort()
    {
      IdConfig config = new IdConfig();
      config.VerProtocolShort = "7.04";
      string result = Encoder.EncodeIdConfigSet(config);
    }

    [Test, Description("����������� ������������ ����������������� ����������")]
    public void IdConfig()
    {
      IdConfig config = new IdConfig();
      config.MessageID = 4;
      config.ShortID = "4516";
      config.DevIdShort = "4516";
      config.DevIdLong = "7040011TT21A";
      config.ModuleIdGps = "����";
      config.ModuleIdGsm = "����";
      //config.ModuleIdMm = "����";
      config.ModuleIdRf = "����";
      config.ModuleIdSs = "����";
      config.VerProtocolLong = "3.28           ";
      config.VerProtocolShort = "��";

      string result = Encoder.EncodeIdConfigSet(config);
      const string expected = "              %%AAi01x2zEABBNNMUNNMONMMAMMVFVMMYQAABAA-w---------AADAA-w-----5-35MLtMOICIIIAIIIAIIIAIA-w-AADXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }

    [Test, Description("������ ������������ ����������������� ���������� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void IdConfigQueryNull()
    {
      string result = Encoder.EncodeIdConfigQuery(null);
    }

    [Test, Description("����������� ������� ������������ ����������������� ����������")]
    public void IdConfigQuery()
    {
      SimpleQuery config = new SimpleQuery();
      config.ShortID = "1010";
      config.MessageID = 45;

      string result = Encoder.EncodeIdConfigQuery(config);
      const string expected = "              %%AAixwxwtJALRXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }
  }
}
