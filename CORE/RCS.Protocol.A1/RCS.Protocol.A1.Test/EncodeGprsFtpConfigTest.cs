using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ������������ �������� FTP GPRS")]
  public class EncodeGprsFtpConfigTest
  {
    [Test, Description("������������ �������� FTP GPRS = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void GprsFtpConfigNull()
    {
      string result = Encoder.EncodeGprsFtpConfigSet(null);
    }

    [Test, Description("������������ ������������ �������� FTP GPRS Server")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsFtpConfigBigServer()
    {
      GprsFtpConfig config = new GprsFtpConfig();
      config.Server = "www.eerewtretyrretfzcvftr.org";
      string result = Encoder.EncodeGprsFtpConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� FTP GPRS Login")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsFtpConfigBigLogin()
    {
      GprsFtpConfig config = new GprsFtpConfig();
      config.Login = "eerewtretyrretfzcvftr";
      string result = Encoder.EncodeGprsFtpConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� FTP GPRS Password")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsFtpConfigBigPassword()
    {
      GprsFtpConfig config = new GprsFtpConfig();
      config.Password = "eerewtretyrretfzcvftr";
      string result = Encoder.EncodeGprsFtpConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� FTP GPRS ConfigPath")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsFtpConfigBigConfigPath()
    {
      GprsFtpConfig config = new GprsFtpConfig();
      config.ConfigPath = @"www.eeeeee.org\root\usr\upload";
      string result = Encoder.EncodeGprsFtpConfigSet(config);
    }

    [Test, Description("������������ ������������ �������� FTP GPRS PutPath")]
    [ExpectedException(typeof(A1Exception))]
    public void GprsFtpConfigBigPutPath()
    {
      GprsFtpConfig config = new GprsFtpConfig();
      config.PutPath = @"www.eeeeee.org\root\usr\upload";
      string result = Encoder.EncodeGprsFtpConfigSet(config);
    }

    [Test, Description("����������� ������������ �������� FTP GPRS")]
    public void GprsFtpConfig()
    {
      GprsFtpConfig config = new GprsFtpConfig();
      config.MessageID = 38;
      config.ShortID = "8542";
      config.Server = "p314.net";
      config.Login = "";
      config.Password = "";
      config.PutPath = @"Upload\123";
      config.ConfigPath = @"Config\main";

      string result = Encoder.EncodeGprsFtpConfigSet(config);
      const string expected = "              %%AAi410yLNAJhcMMcNLboZdABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQbbvZaZ2XbYUabAJAAAAAAAAAAVQcbbwYZXBMMM5AAAAAAAAAAAAAXX8XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }

    [Test, Description("������ ������������ �������� FTP GPRS = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void GprsFtpQueryNull()
    {
      string result = Encoder.EncodeGprsFtpConfigQuery(null);
    }

    [Test, Description("����������� ������� ������������ �������� FTP GPRS")]
    public void GprsFtpConfigQuery()
    {
      SimpleQuery config = new SimpleQuery();
      config.ShortID = "8542";
      config.MessageID = 45;

      string result = Encoder.EncodeGprsFtpConfigQuery(config);
      const string expected = "              %%AAi410y7SALRXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }
  }
}
