using System;
using NUnit.Framework;
using RCS.Protocol.A1.Entity;

namespace UnitTest
{
  [TestFixture, Description("����� � ������������")]
  public class ZonePointTest
  {
    [Test, Description("������������ �������� latitude � ������������")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void WrongLatitudeParam()
    {
      new ZonePoint(300 * 600000, 600000);
    }

    [Test, Description("������������ �������� longitude � ������������")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void WrongLongitudeParam()
    {
      new ZonePoint(600000, 300 * 600000);
    }

    [Test, Description("M���� Equals (True)")]
    public void MethodEqualsTrue()
    {
      ZonePoint point1 = new ZonePoint(600000, 600000);
      ZonePoint point2 = new ZonePoint(600000, 600000);
      Assert.IsTrue(point1.Equals(point2));
    }

    [Test, Description("M���� Equals (False)")]
    public void MethodEqualsFalse()
    {
      ZonePoint point1 = new ZonePoint(600000, 600000);
      ZonePoint point2 = new ZonePoint(600000, 600001);
      Assert.IsFalse(point1.Equals(point2));
    }

    [Test, Description("M���� GetHashCode")]
    public void MethodGetHashCode()
    {
      ZonePoint point1 = new ZonePoint(600000, 600000);
      point1.GetHashCode();
    }

    [Test, Description("�������� != ")]
    public void OpInequality()
    {
      ZonePoint point1 = new ZonePoint(600000, 600000);
      ZonePoint point2 = new ZonePoint(600000, 600001);
      Assert.IsTrue(point1 != point2);
    }
  }
}
