using System;
using NUnit.Framework;
using RCS.Protocol.A1.Entity;

namespace UnitTest
{
  [TestFixture, Description("�������� ����")]
  public class ZoneTest
  {
    [Test, Description("������������ �������� mask = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CtorMask()
    {
      new Zone(null, new ZonePoint[] { new ZonePoint(600000, 600000) });
    }

    [Test, Description("������������ �������� points = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CtorPointsNull()
    {
      new Zone(new ZoneMask(1, 0, 1, 0), null);
    }

    [Test, Description("������������ �������� points")]
    [ExpectedException(typeof(ArgumentException))]
    public void CtorPoints()
    {
      ZonePoint[] points = new ZonePoint[257];
      for (int i = 0; i < 257; i++)
      {
        points[i] = new ZonePoint(600000, 45 * 600000);
      }
      new Zone(new ZoneMask(1, 0, 1, 0), points);
    }
  }
}
