#region Using directives
using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
#endregion

namespace UnitTest
{
  [TestFixture, Description("��������� �����")]
  public class test
  {
    [Test, Description("��� ��� ������")]
    [Ignore()]
    public void Miscellaneous()
    {
      DataGpsQuery config = new DataGpsQuery();
      config.MessageID = 45;
      config.ShortID = "5601";
      config.LastRecords = 100;
      config.LastTimes = 0;
      //config.LastMeters = 0;
      config.MaxSmsNumber = 10;
      config.StartLatitude = 0;
      config.StartLongitude = 0;
      config.StartAltitude = 0;
      config.EndLatitude = 0;
      config.EndLongitude = 0;
      config.EndAltitude = 0;
      config.StartTime = -10800;
      config.EndTime = -10800;
      config.StartSpeed = 0;
      config.StartDirection = 0;
      config.EndSpeed = 0;
      config.EndDirection = 0;
      config.StartLogId = 0;
      config.EndLogId = 0;
      config.WhatSend = 1023;
      config.CheckMask = 9;
      config.Events = 0;
      config.LogId1 = 0;
      config.LogId2 = 0;
      config.LogId3 = 0;
      config.LogId4 = 0;
      config.LogId5 = 0;

      string result = Encoder.EncodeDataGpsQuery(config);
    }

    /// <summary>
    /// Decoding
    /// </summary>
    [Test, Description("�������������")]
    [Ignore()]
    public void Decoding()
    {
      const string msg =
        "              %%..i:y22iKAACgYdcYZfe8AAiAAgAkMIrJf8nAAAIAgZwJUcoT8AJAAACgZJMUcTa8ANCxQAEUQU+MAABAAUgQUMvAd9gEUQ6UQALEuY8gYdcYZje8ANCxQX0XXX-XXX-XXX-XXX-XXX-XXX-";
      CommonDescription cmd = Decoder.DecodeMessage(msg);
      System.Console.WriteLine("ok");
    } // Decoding()

    [Test, Description("����������� ��� ������")]
    //[Ignore()]
    public void TelNumberTncoding()
    {
      byte[] tel1_byte = new byte[11];
      byte[] tel2_byte = new byte[11];
      tel1_byte = Level4Converter.TelNumberToBytes("80501112233", 11);
      tel2_byte = Level4Converter.TelNumberToBytes("+380501112233", 11);

      string tel1 = Level4Converter.BytesToTelNumber(tel1_byte, 0, 10);
      string tel2 = Level4Converter.BytesToTelNumber(tel2_byte, 0, 10);
      System.Console.WriteLine(tel1 + " " + tel2);
    }

    [Test, Description("�� ������")]
    public void FromFedor()
    {
      const string test =
        "              %%..i:y222KIAHAADzB0ADyAAQABAmAAARAAAEAAABACAECACRACAUGAGRAGAEGAGRAEAVEAAZAAAmAAAAAF-2---------+0Hq-KfQ90AAQABAAWKAPAAAXX9XXX-XXX-XXX-XXX-XXX-XXX-";

      CommonDescription cmd = Decoder.DecodeMessage(test);
      Console.WriteLine(cmd.GetDebugInfo());
    }
  }
}
