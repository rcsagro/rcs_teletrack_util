using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;

namespace UnitTest
{
  [TestFixture, Description("�������� ����")]
  public class CommonDescriptionTest
  {
    [Test, Description("����� MethodGetDebugInfo")]
    public void MethodGetDebugInfo()
    {
      CommonDescription descr = new CommonDescription();
      descr.ShortID = "1";
      descr.CommandID = CommandDescriptor.DataGpsAuto;
      descr.MessageID = 1;
      descr.MobitelID = 20;
      const string str = "���������� �� ������� ����: CommonDescription.\r\nShortID: 1; " +
        "MobitelID: 20; MessageID: 1; CommandID: DataGpsAuto; Source: ; Message: ; ";
      Assert.AreEqual(str, descr.GetDebugInfo());
    }
  }
}
