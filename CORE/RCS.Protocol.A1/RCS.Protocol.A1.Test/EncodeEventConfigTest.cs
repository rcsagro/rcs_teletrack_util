using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("����������� ��������� ������������ �������")]
  public class EncodeEventConfigTest
  {
    [Test, Description("������������ ������� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void EventConfigNull()
    {
      string result = Encoder.EncodeEventConfigSet(null);
    }

    [Test, Description("������������ ������������ ������� CourseBend")]
    [ExpectedException(typeof(A1Exception))]
    public void EventConfigSmallCourseBend()
    {
      EventConfig config = new EventConfig();
      config.CourseBend = 0;
      string result = Encoder.EncodeEventConfigSet(config);
    }

    [Test, Description("������������ ������������ ������� CourseBend")]
    [ExpectedException(typeof(A1Exception))]
    public void EventConfigBigCourseBend()
    {
      EventConfig config = new EventConfig();
      config.CourseBend = 500;
      string result = Encoder.EncodeEventConfigSet(config);
    }

    [Test, Description("������������ ������������ ������� Distance1")]
    [ExpectedException(typeof(A1Exception))]
    public void EventConfigSmallDistance1()
    {
      EventConfig config = new EventConfig();
      config.Distance1 = 10;
      string result = Encoder.EncodeEventConfigSet(config);
    }

    [Test, Description("������������ ������������ ������� Distance2")]
    [ExpectedException(typeof(A1Exception))]
    public void EventConfigSmallDistance2()
    {
      EventConfig config = new EventConfig();
      config.Distance2 = 40;
      string result = Encoder.EncodeEventConfigSet(config);
    }

    [Test, Description("������������ ������������ ������� MinSpeed")]
    [ExpectedException(typeof(A1Exception))]
    public void EventConfigBigMinSpeed()
    {
      EventConfig config = new EventConfig();
      config.MinSpeed = 300;
      string result = Encoder.EncodeEventConfigSet(config);
    }

    [Test, Description("������������ ������������ ������� Timer1")]
    [ExpectedException(typeof(A1Exception))]
    public void EventConfigSmallTimer1()
    {
      EventConfig config = new EventConfig();
      config.Timer1 = 0;
      string result = Encoder.EncodeEventConfigSet(config);
    }

    [Test, Description("������������ ������������ ������� Timer2")]
    [ExpectedException(typeof(A1Exception))]
    public void EventConfigSmallTimer2()
    {
      EventConfig config = new EventConfig();
      config.Timer2 = 0;
      string result = Encoder.EncodeEventConfigSet(config);
    }


    [Test, Description("����������� ��������� ����������� �������")]
    public void EventConfig()
    {
      EventConfig config = new EventConfig();
      config.ShortID = "3913";
      config.MessageID = 116;
      config.SpeedChange = 3;
      config.CourseBend = 15;
      config.Distance1 = 5000;
      config.Distance2 = 200;
      config.MinSpeed = 2;
      config.Timer1 = 7200;
      config.Timer2 = 60;
      config.EventMask[0] = 1;
      config.EventMask[1] = 5;
      config.EventMask[2] = 5;
      config.EventMask[3] = 1;
      config.EventMask[4] = 0;
      config.EventMask[5] = 0;
      config.EventMask[6] = 0;
      config.EventMask[7] = 1;
      config.EventMask[8] = 1;
      config.EventMask[9] = 1;
      config.EventMask[10] = 1;
      config.EventMask[11] = 1;
      config.EventMask[12] = 24;
      config.EventMask[13] = 24;
      config.EventMask[14] = 24;
      config.EventMask[15] = 24;
      config.EventMask[16] = 272;
      config.EventMask[17] = 272;
      config.EventMask[18] = 1;
      config.EventMask[19] = 1;
      config.EventMask[20] = 1;
      config.EventMask[21] = 0;
      config.EventMask[22] = 1;
      // ������� ������ ������������ ������ 23 ��������.
      // ���������� �������� ��� � ���� ������

      string message = Encoder.EncodeEventConfigSet(config);
      const string expected = "              %%AAiz5xzwDAdBAADzEiADyAAQABAEBAARAAAAAAAAAAAEAAARAAAEAAGBAGAAGAGAAEAREAAQAAAEAAABAAAEAAAAAAAAAAAAAAAAAAAAAAAAAHICAPAAAXX8XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, message);
    }

    [Test, Description("������ ������������ ������� = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void EventConfigQueryNull()
    {
      string result = Encoder.EncodeEventConfigQuery(null);
    }

    [Test, Description("����������� ������� ������������ �������")]
    public void EventConfigQuery()
    {
      SimpleQuery config = new SimpleQuery();
      config.ShortID = "1010";
      config.MessageID = 45;

      string result = Encoder.EncodeEventConfigQuery(config);
      const string expected = "              %%AAixwxwtIALRXXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX-";
      Assert.IsNotNull(config.Source);
      Assert.IsTrue(config.Source.Length == Const.LEVEL0_PACKET_LENGTH);
      Assert.Greater(config.Message.Length, 0);
      Assert.AreEqual(expected, result);
    }
  }
}
