#region Using directives
using System;
using NUnit.Framework;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
#endregion

namespace UnitTest
{
  [TestFixture, Description("����������� ������� ������������ c ������� �����������")]
  public class EncoderConfigQueryEmptyParamsTest
  {
    [Test, Description("����������� ������� ������������ c ������� �����������")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ConfigQueryEmptyParams()
    {
      SimpleQuery config = new SimpleQuery();
      string result = Encoder.EncodeSmsAddrConfigQuery(config);
    }
  }
}
