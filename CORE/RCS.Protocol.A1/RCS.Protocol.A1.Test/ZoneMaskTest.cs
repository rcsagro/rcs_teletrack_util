using System;
using NUnit.Framework;
using RCS.Protocol.A1.Entity;

namespace UnitTest
{
  [TestFixture, Description("�������� ����� ����")]
  public class ZoneMaskTest
  {
    [Test, Description("������������ �������� ZoneMask1")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void CtorZoneMask1()
    {
      new ZoneMask(2, 0, 0, 0);
    }

    [Test, Description("������������ �������� ZoneMask2")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void CtorZoneMask2()
    {
      new ZoneMask(1, 2, 0, 0);
    }

    [Test, Description("������������ �������� ZoneMask3")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void CtorZoneMask3()
    {
      new ZoneMask(1, 1, 2, 0);
    }

    [Test, Description("������������ �������� ZoneMask4")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void CtorZoneMask4()
    {
      new ZoneMask(0, 0, 0, 2);
    }
  }
}
