using System;
using NUnit.Framework;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace UnitTest
{
  [TestFixture, Description("�������� ����")]
  public class ZoneConfigTest
  {
    [Test, Description("������������ ������������ ����������� ��� Zone = null")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ZoneConfigNullZone()
    {
      ZoneConfig config = new ZoneConfig();
      config.AddZone(null);
    }

    [Test, Description("������������ ������������ ����������� ��� ZoneCount")]
    [ExpectedException(typeof(A1Exception))]
    public void ZoneConfigBigZoneCount()
    {
      ZoneConfig config = new ZoneConfig();
      for (int i = 0; i < 65; i++)
      {
        config.AddZone(new Zone(
          new ZoneMask(1, 0, 1, 0),
          new ZonePoint[] { new ZonePoint(600000, 600000) }));
      }
    }

    [Test, Description("������������ ������������ ����������� ��� AddSource(null)")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ZoneConfigAddSourceNull()
    {
      ZoneConfig config = new ZoneConfig();
      config.AddSource(null);
    }

    [Test, Description("������������ ������������ ����������� ��� AddSource")]
    [ExpectedException(typeof(A1Exception))]
    public void ZoneConfigAddSource()
    {
      ZoneConfig config = new ZoneConfig();
      for (int i = 0; i < 26; i++)
      {
        config.AddSource(new byte[] { 1, 4, 3 });
      }
    }

    [Test, Description("������������ ������������ ����������� ��� AddMessage(null)")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ZoneConfigAddMessageNull()
    {
      ZoneConfig config = new ZoneConfig();
      config.AddMessage(null);
    }

    [Test, Description("������������ ������������ ����������� ��� AddMessage")]
    [ExpectedException(typeof(A1Exception))]
    public void ZoneConfigAddMessage()
    {
      ZoneConfig config = new ZoneConfig();
      for (int i = 0; i < 26; i++)
      {
        config.AddMessage("%%986gyg");
      }
    }
  }
}
