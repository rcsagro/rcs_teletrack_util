﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: InternalsVisibleTo("UnitTest, PublicKey=" +
  "00240000048000009400000006020000002400005253413100040000010001007b04c829ae0138" +
  "85d0d3459b40ce07bbf0f38a5fad80849d3f89cf535784489509f999e8f7bce1a22745282a28e8" +
  "183136af8041dcbd4c362b8bfefa3c7ece2e01dcac6080077a0bd49aeb18849edeea97b909967a" +
  "01f31c60ee8d2987957908c1dc2702359a7f86481d3c6f8fb622226a688f62ef63662b85b4591d" +
  "d8e9c3ca")]

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("RCS.Protocol.A1")]
[assembly: AssemblyDescription("Протокол A1")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("RCS")]
[assembly: AssemblyProduct("RCS.Protocol.A1")]
[assembly: AssemblyCopyright("Copyright © 2008-2017 RCS")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7915c547-109e-475f-8884-6a6fc29e8ddb")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("3.5.0.0")]
[assembly: AssemblyFileVersion("3.5.0.0")]
