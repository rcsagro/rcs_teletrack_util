namespace RCS.Protocol.A1
{
  /// <summary>
  /// �������� ��������������� ������
  /// </summary>
  public enum CommandDescriptor
  {
    /// <summary>
    /// ����������� ������� 0
    /// </summary>
    Unknown = 0,
    /// <summary>
    /// Ping, ��� ������� �� ��������� ���������� 8
    /// </summary>
    Ping = 8,
    /// <summary>
    /// ��������� ���������� ��� ������� 
    /// SMS, E-MAIL � WEB SERVER (GPRS) ��������� 11
    /// </summary>
    SMSConfigSet = 11,
    /// <summary>
    /// ������������� ��������� ���������� ��� ������� 
    /// SMS, E-MAIL � WEB SERVER (GPRS) ��������� 21.
    /// </summary>
    SMSConfigConfirm = 21,
    /// <summary>
    /// ������ ���������� ��� ������� 
    /// SMS, E-MAIL � WEB SERVER (GPRS) ��������� 31.
    /// </summary>
    SMSConfigQuery = 31,
    /// <summary>
    /// ����� �� ������ ���������� ��� ������� 
    /// SMS, E-MAIL � WEB SERVER (GPRS) ��������� 41.
    /// </summary>
    SMSConfigAnswer = 41,
    /// <summary>
    /// ��������� ������������ ���������� ������� 12.
    /// </summary>
    PhoneConfigSet = 12,
    /// <summary>
    /// ������������� ��������� ������������ ���������� ������� 22. 
    /// </summary>
    PhoneConfigConfirm = 22,
    /// <summary>
    /// ������ ������������ ���������� ������� 32. 
    /// </summary>
    PhoneConfigQuery = 32,
    /// <summary>
    /// ����� �� ������ ������������ ���������� ������� 42. 
    /// </summary>
    PhoneConfigAnswer = 42,
    /// <summary>
    /// ��������� ������������ ������� 13.
    /// </summary>
    EventConfigSet = 13,
    /// <summary>
    /// ������������� ��������� ������������ ������� 23.
    /// </summary>
    EventConfigConfirm = 23,
    /// <summary>
    /// ������ ������������ ������� 33.
    /// </summary>
    EventConfigQuery = 33,
    /// <summary>
    /// ����� �� ������ ������������ ������� 43.
    /// </summary>
    EventConfigAnswer = 43,
    /// <summary>
    /// ��������� ������������ ���������� ���������� 14.
    /// </summary>
    UniqueConfigSet = 14,
    /// <summary>
    /// ������������� ��������� ������������ ���������� ���������� 24.
    /// </summary>
    UniqueConfigConfirm = 24,
    /// <summary>
    /// ������ ������������ ���������� ���������� 34.
    /// </summary>
    UniqueConfigQuery = 34,
    /// <summary>
    /// ����� �� ������ ������������ ���������� ���������� 44.
    /// </summary>
    UniqueConfigAnswer = 44,
    /// <summary>
    /// ��������� ������������ ����������������� ���������� 17.
    /// </summary>
    IdConfigSet = 17,
    /// <summary>
    /// ������������� ��������� ������������ ����������������� ���������� 27.
    /// </summary>
    IdConfigConfirm = 27,
    /// <summary>
    /// ������ ������������ ����������������� ���������� 37.
    /// </summary>
    IdConfigQuery = 37,
    /// <summary>
    /// ����� �� ������ ������������ ����������������� ���������� 47.
    /// </summary>
    IdConfigAnswer = 47,
    /// <summary>
    /// ��������� ������������ ����������� ��� 15. 
    /// </summary>
    ZoneConfigSet = 15,
    /// <summary>
    /// ������������� ��������� ������������ ����������� ��� 25.  
    /// </summary>
    ZoneConfigConfirm = 25,
    /// <summary>
    /// ������ GPS ������ 36. 
    /// </summary>
    DataGpsQuery = 36,
    /// <summary>
    /// ����� �� ������ GPS ������ 46. 
    /// </summary>
    DataGpsAnswer = 46,
    /// <summary>
    /// �������������� �������� GPS ������ 49. 
    /// </summary>
    DataGpsAuto = 49,
    /// <summary>
    /// ��������� ��������
    /// </summary>
    MessageToDriver = 10,
    /// <summary>
    /// ��������� �������� �������� GPRS 50.
    /// </summary>
    GprsBaseConfigSet = 50,
    /// <summary>
    /// ������������� ��������� �������� �������� GPRS 60.
    /// </summary>
    GprsBaseConfigConfirm = 60,
    /// <summary>
    /// ������ �������� �������� GPRS 70.
    /// </summary>
    GprsBaseConfigQuery = 70,
    /// <summary>
    /// ����� �� ������ �������� �������� GPRS 80.
    /// </summary>
    GprsBaseConfigAnswer = 80,
    /// <summary>
    /// ��������� �������� ����������� � ���������� GPRS 54. 
    /// </summary>
    GprsProviderConfigSet = 54,
    /// <summary>
    /// ������������� ��������� �������� ����������� � ���������� GPRS 64. 
    /// </summary>
    GprsProviderConfigConfirm = 64,
    /// <summary>
    /// ������ �������� ����������� � ���������� GPRS 74. 
    /// </summary>
    GprsProviderConfigQuery = 74,
    /// <summary>
    /// ����� �� ������ �������� ����������� � ���������� GPRS 84. 
    /// </summary>
    GprsProviderConfigAnswer = 84,
    /// <summary>
    /// ��������� �������� Email GPRS 51.
    /// </summary>
    GprsEmailConfigSet = 51,
    /// <summary>
    /// ������������� ��������� �������� Email GPRS 61.
    /// </summary>
    GprsEmailConfigConfirm = 61,
    /// <summary>
    /// ������ �������� Email GPRS 71.
    /// </summary>
    GprsEmailConfigQuery = 71,
    /// <summary>
    /// ����� �� ������ �������� Email GPRS 81.
    /// </summary>
    GprsEmailConfigAnswer = 81,
    /// <summary>
    /// ��������� �������� Socket GPRS 52.
    /// </summary>
    GprsSocketConfigSet = 52,
    /// <summary>
    /// ������������� ��������� �������� Socket GPRS 62.
    /// </summary>
    GprsSocketConfigConfirm = 62,
    /// <summary>
    /// ������ �������� Socket GPRS 72.
    /// </summary>
    GprsSocketConfigQuery = 72,
    /// <summary>
    /// ����� �� ������ �������� Socket GPRS 82.
    /// </summary>
    GprsSocketConfigAnswer = 82,
    /// <summary>
    /// ��������� �������� FTP GPRS 53.
    /// </summary>
    GprsFtpConfigSet = 53,
    /// <summary>
    /// ������������� ��������� �������� FTP GPRS 63.
    /// </summary>
    GprsFtpConfigConfirm = 63,
    /// <summary>
    /// ������ �������� FTP GPRS 73.
    /// </summary>
    GprsFtpConfigQuery = 73,
    /// <summary>
    /// ����� �� ������ �������� FTP GPRS 83.
    /// </summary>
    GprsFtpConfigAnswer = 83
  }
} 
