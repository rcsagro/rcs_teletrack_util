<?xml version="1.0" encoding="WINDOWS-1251" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head></head>
      <body>
        <xsl:for-each select="developerConceptualDocument/section/content/para">
          <br> <xsl:value-of select="."/> </br>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>