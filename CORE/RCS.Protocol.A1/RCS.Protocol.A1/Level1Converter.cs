#region Using directives
using System;
using System.Collections.Generic;
using System.Text;
#endregion

namespace RCS.Protocol.A1
{
  /// <summary>
  /// ��������� ������ �� ������ 1 � 2 ��������� A1
  /// </summary>
  public static class Level1Converter
  {
    /// <summary>
    /// �������������� Unicode ������ � ������ ����,
    /// � ������� ������ ������ ����������� ASCII ������.
    /// <para>������ ������� = ������� ������</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">�� �������� ������ source</exception>
    /// <param name="source">Source ������</param>
    /// <returns>������ ���� byte[]</returns>
    public static byte[] StringToByteArray(string source)
    {
      if (source == null)
        throw new ArgumentNullException("source", "�� �������� ������ source");

      byte[] result = new byte[source.Length];

      for (int i = 0; i < source.Length - 1; i++)
        result[i] = Convert.ToByte(source[i]);

      return result;
    }

    /// <summary>
    /// ������������ ������ �� ������� ���� ������ LEVEL1
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <param name="source">byte[]</param>
    /// <returns>������</returns>
    public static string BytesToString(byte[] source)
    {
      if (source == null)
        throw new ArgumentNullException("source", "�� ������� ������ ���� source");

      StringBuilder result = new StringBuilder();

      for (int i = 0; i < source.Length; i++)
        result.Append((char)SymbolToValue(source[i]));

      return result.ToString();
    }

    /// <summary>
    /// �������������� ����� ������ �� 6 ������ � 8 ������ ������� �� ������ LEVEL1.
    /// <para>����� ������������� ����� ������ ������ ���� ����� 136 ����
    /// (Data block �� ������ LEVEL1).</para>
    /// </summary>
    /// <exception cref="ArgumentException">����� ����������� ������� dataBlock �� ������������ ��������� �����</exception>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� dataBlock</exception>
    /// <param name="dataBlock">byte[136] Data block �� ������ LEVEL1</param>
    /// <returns>byte[102] ��������� 3-��� ������</returns>
    public static byte[] Decode6BitTo8(byte[] dataBlock)
    {
      if (dataBlock == null)
        throw new ArgumentNullException("dataBlock", "�� ������� ������ ���� dataBlock");
      if (dataBlock.Length != Const.LEVEL1_DATA_BLOCK_LENGHT)
        throw new ArgumentException("dataBlock", "����� ����������� ������� dataBlock (" +
          dataBlock.Length + ") �� ������������ ��������� ����� - " +
          Const.LEVEL1_DATA_BLOCK_LENGHT);

      byte[] result = new byte[Const.LEVEL3_PACKET_LENGHT];
      int blockCount = Const.LEVEL1_DATA_BLOCK_LENGHT >> 2;

      for (int i = 0; i < blockCount; i++)
      {
        byte sX, sY, sZ, sA, sB, sC, sD, AD, BD, CD;
        int ind = 4 * i;

        sA = SymbolToValue(dataBlock[ind]);
        sB = SymbolToValue(dataBlock[ind + 1]);
        sC = SymbolToValue(dataBlock[ind + 2]);
        sD = SymbolToValue(dataBlock[ind + 3]);

        sX = (byte)(sA << 2);
        sY = (byte)(sB << 2);
        sZ = (byte)(sC << 2);

        AD = (byte)(sD << 6);
        AD = (byte)(AD >> 6);

        BD = (byte)(sD << 4);
        BD = (byte)(BD >> 6);

        CD = (byte)(sD << 2);
        CD = (byte)(CD >> 6);

        sX = (byte)(sX + AD);
        sY = (byte)(sY + BD);
        sZ = (byte)(sZ + CD);

        result[i * 3] = (byte)(sX & 0xff);
        result[i * 3 + 1] = (byte)(sY & 0xff);
        result[i * 3 + 2] = (byte)(sZ & 0xff);
      }

      return result;
    }

    /// <summary>
    /// �������������� ����� ������ �� 8 ������ � 6 ������ �������. 
    /// <para>����� ����������� ����� = 102 �����(������ ������ �� ������ LEVEL3).</para>
    /// </summary>
    /// <exception cref="ArgumentException">������� ������ ������ ���� source</exception>
    /// <exception cref="ArgumentNullException">����� ������� source �� ����� 102</exception>
    /// <param name="source">�������� byte[102]</param>
    /// <returns>byte[136]</returns>
    public static byte[] Encode8BitTo6(byte[] source)
    {
      if (source == null)
        throw new ArgumentNullException("source", "�� ������� ������ ���� source");
      if (source.Length != Const.LEVEL3_PACKET_LENGHT)
        throw new ArgumentException("source", "����� ����������� ������� source (" +
          source.Length + ") �� ������������ ��������� ����� - " +
          Const.LEVEL3_PACKET_LENGHT);

      byte[] result = new byte[Const.LEVEL1_DATA_BLOCK_LENGHT];

      const int blocks = Const.LEVEL1_DATA_BLOCK_LENGHT >> 2;

      for (int i = 0; i < blocks; i++)
      {
        byte sX, sY, sZ, sA = 0, sB = 0, sC = 0, sD = 0;
        int ind = 3 * i;

        sX = source[ind];
        sY = source[ind + 1];
        sZ = source[ind + 2];

        sA = (byte)(sX >> 2);
        sB = (byte)(sY >> 2);
        sC = (byte)(sZ >> 2);

        sX = (byte)(sX << 6);
        sX = (byte)(sX >> 6);
        sY = (byte)(sY << 6);
        sY = (byte)(sY >> 4);
        sZ = (byte)(sZ << 6);
        sZ = (byte)(sZ >> 2);

        sD = (byte)(sX + sY + sZ);

        result[i * 4] = ValueToSymbol(sA);
        result[i * 4 + 1] = ValueToSymbol(sB);
        result[i * 4 + 2] = ValueToSymbol(sC);
        result[i * 4 + 3] = ValueToSymbol(sD);
      }
      return result;
    }

    /// <summary>
    /// �������������� ��������� �� ��������� ����� � ��� ASCII 
    /// � ������������ � �������� LEVEL1
    /// <para>������������ ��� �������������</para>
    /// </summary>
    /// <param name="symbolCode">byte</param>
    /// <returns>byte</returns>
    public static byte SymbolToValue(byte symbolCode)
    {
      byte value = 0;
      char symbol = Convert.ToChar(symbolCode);

      if ((symbol >= 'A') && (symbol <= 'Z'))
        value = (byte)(symbolCode - ASCII_CODE_A);

      else if ((symbol >= 'a') && (symbol <= 'z'))
        value = (byte)(symbolCode - Convert.ToByte('a') + 26);

      else if ((symbol >= '0') && (symbol <= '9'))
        value = (byte)(symbolCode - Convert.ToByte('0') + 52);

      else if (symbol == '+')
        value = 62;

      else if (symbol == '-')
        value = 63;

      else if ((symbolCode >= 58) && (symbolCode <= 63))
        value = (byte)(symbolCode - 58 + ASCII_CODE_A);

      return value;
    }

    private static readonly byte ASCII_CODE_A =
      Convert.ToByte('A');

    /// <summary>
    /// �������������� ASCII ��� � ��� ������� �1, 
    /// � �������� ����������� ���������,
    /// <para>� ������������ � �������� LEVEL1</para>
    /// </summary>
    /// <param name="value">byte</param>
    /// <returns>byte</returns>
    public static byte ValueToSymbol(byte value)
    {
      byte symbol = Convert.ToByte('.');
      if (value <= 25)
        symbol = (byte)(ASCII_CODE_A + value);

      else if ((value >= 26) && (value <= 51))
        symbol = (byte)(Convert.ToByte('a') + value - 26);

      else if ((value >= 52) && (value <= 61))
        symbol = (byte)(Convert.ToByte('0') + value - 52);

      else if (value == 62)
        symbol = Convert.ToByte('+');

      else if (value == 63)
        symbol = Convert.ToByte('-');

      else if ((value >= ASCII_CODE_A) && (value <= Convert.ToByte('F')))
        symbol = (byte)(value - ASCII_CODE_A + 58);

      return symbol;
    }
  }
}
