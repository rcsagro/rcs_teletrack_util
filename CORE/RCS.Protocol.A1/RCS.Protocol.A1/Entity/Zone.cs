#region Using directives
using System;
using System.Text;
using RCS.Protocol.A1.Error;
#endregion

namespace RCS.Protocol.A1.Entity
{
  /// <summary>
  /// �������� ����������� ����
  /// </summary>
  public class Zone
  {
    /// <summary>
    /// ����� ������-������
    /// </summary>
    public readonly ZoneMask Mask;

    /// <summary>
    /// ������ �����. 
    /// ������������ ���-�� ��������� = 256. 
    /// </summary>
    public readonly ZonePoint[] Points;

    /// <summary>
    /// �����������
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� mask ��� points</exception>
    /// <exception cref="ArgumentOutOfRangeException">������ points ����� 256</exception>
    /// <exception cref="A1Exception"></exception>
    /// <param name="mask">����� ������-������</param>
    /// <param name="points">������ �����</param>
    public Zone(ZoneMask mask, ZonePoint[] points)
    {
      if (mask == null)
        throw new ArgumentNullException("mask",
          "�� ������� �������� mask (����� ������-������)");
      if (points == null)
        throw new ArgumentNullException("points",
          "�� ������� �������� points (������ �����)");
      if (points.Length > 256)
        throw new ArgumentException("points",
          String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "points(������ �����)",
          "Zone(�������� ����������� ����)",
          256));
      if (points.Length < 3)
        throw new A1Exception(ErrorText.ZONE_MIN_POINT_COUNT);

      // �������� ������� ����� ������. 
      // � ���� �� ���� ����� ������ �� ������ ���� ������,
      // � ��������� ����������� ���� ������(��������� �������) 
      bool oneCloneFound = false;
      for (int i = 0; i < points.Length; i++)
      {
        for (int j = i + 1; i < points.Length; i++)
        {
          if (points[i] == points[j])
          {
            if ((points.Length == 3) || (oneCloneFound))
              throw new A1Exception(ErrorText.ZONE_BAD_DEFINED);

            oneCloneFound = true;
          }
        }
      }

      Mask = mask;
      Points = points;
    }
  }
}
