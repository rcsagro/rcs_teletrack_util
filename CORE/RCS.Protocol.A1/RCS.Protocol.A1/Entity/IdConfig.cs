using System;
using RCS.Protocol.A1.Error;

namespace RCS.Protocol.A1.Entity
{
  /// <summary>
  /// ������������ ����������������� ����������.
  /// �������������� ������: 17, 27, 47
  /// </summary>
  public class IdConfig : CommonDescription
  {
    /// <summary>
    /// �������� ������������� ���������. 
    /// �� ����� �������� ������������ �� ������ ��������� ������ ���������. 
    /// ������ ������������� ��������� � ��������� ������� ���������.
    /// �������� 4 �������.
    /// </summary>
    public string DevIdShort
    {
      get { return devIdShort; }
      set { devIdShort = value; }
    }
    private string devIdShort;

    /// <summary>
    /// �������� ����� ���������. �������� 16 ��������.
    /// </summary>
    public string DevIdLong
    {
      get { return devIdLong; }
      set { devIdLong = value; }
    }
    private string devIdLong;

    /// <summary>
    /// ������������� ������ GPS (�����������).
    /// �������� 4 �������.
    /// </summary>
    public string ModuleIdGps
    {
      get { return moduleIdGps; }
      set { moduleIdGps = value; }
    }
    private string moduleIdGps;

    /// <summary>
    /// ������������� ������ MultiMedia (�� ������������).
    /// �������� 4 �������.
    /// </summary>
    public string ModuleIdMm
    {
      get { return moduleIdMm; }
    }
    private string moduleIdMm;

    /// <summary>
    /// ������������� ����� �������� (�����������).
    /// </summary>
    public string ModuleIdSs
    {
      get { return moduleIdSs; }
      set { moduleIdSs = value; }
    }
    private string moduleIdSs;

    /// <summary>
    /// ������ ���������. ����������� ����������
    /// </summary>
    public string VerProtocolLong
    {
      get { return verProtocolLong; }
      set { verProtocolLong = value; }
    }
    private string verProtocolLong;

    /// <summary>
    /// ������ ���������. ������� ���������� 
    /// </summary>
    public string VerProtocolShort
    {
      get { return verProtocolShort; }
      set { verProtocolShort = value; }
    }
    private string verProtocolShort;

    /// <summary>
    /// ������������� ������ GSM (�����������). �� ������������.
    /// �������� 4 �������.
    /// </summary>
    public string ModuleIdGsm
    {
      get { return moduleIdGsm; }
      set { moduleIdGsm = value; }
    }
    private string moduleIdGsm;

    /// <summary>
    /// ������������� ������ RF (�����������). �� ������������.
    /// �������� 4 �������.
    /// </summary>
    public string ModuleIdRf
    {
      get { return moduleIdRf; }
      set { moduleIdRf = value; }
    }
    private string moduleIdRf;

    /// <summary>
    /// �����������
    /// </summary>
    public IdConfig()
    {
      devIdShort = "";
      devIdLong = "";
      moduleIdGps = "";
      moduleIdGsm = "";
      moduleIdMm = "";
      moduleIdRf = "";
      moduleIdSs = "";
      verProtocolLong = "";
      verProtocolShort = "";
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;

      if (DevIdShort.Length > 4)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "DevIdShort(�������� ������������� ���������)",
          "IdConfig(������������ ����������������� ����������)",
          4);
        return false;
      }
      if (DevIdLong.Length > 16)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "DevIdLong(�������� ����� ���������)",
          "IdConfig(������������ ����������������� ����������)",
          16);
        return false;
      }
      if (ModuleIdGps.Length > 4)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "ModuleIdGps(������������� ������ GPS)",
          "IdConfig(������������ ����������������� ����������)",
          4);
        return false;
      }
      if (ModuleIdGsm.Length > 4)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "ModuleIdGsm(������������� ������ GSM)",
          "IdConfig(������������ ����������������� ����������)",
          4);
        return false;
      }
      //if (ModuleIdMm.Length > 4)
      //{
      //  errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
      //    "ModuleIdMm(������������� ������ MultiMedia)",
      //    "IdConfig(������������ ����������������� ����������)",
      //    4);
      //  return false;
      //}
      if (ModuleIdRf.Length > 4)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "ModuleIdRf(������������� ������ RF)",
          "IdConfig(������������ ����������������� ����������)",
          4);
        return false;
      }
      if (ModuleIdSs.Length > 4)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "ModuleIdSs(������������� ����� ��������)",
          "IdConfig(������������ ����������������� ����������)",
          4);
        return false;
      }
      if (VerProtocolLong.Length > 16)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "VerProtocolLong(������ ��������� ������)",
          "IdConfig(������������ ����������������� ����������)",
          16);
        return false;
      }
      if (VerProtocolShort.Length > 2)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "VerProtocolShort(������ ��������� ������. ������� ����������)",
          "IdConfig(������������ ����������������� ����������)",
          2);
        return false;
      }
      return true;
    }
  }
}
