using System;
using RCS.Protocol.A1.Error;

namespace RCS.Protocol.A1.Entity
{
  /// <summary>
  /// �� ������������
  /// ������������ �������� GPRS. ��������� FTP.
  /// �������������� ������: 53, 63, 83
  /// </summary>
  public class GprsFtpConfig : CommonDescription
  {
    /// <summary>
    /// Ftp ������. �������� 25 ��������.
    /// </summary>
    public string Server
    {
      get { return server; }
      set { server = value; }
    }
    private string server;

    /// <summary>
    /// ����� ��� ������� �� Ftp ������, �������� 10 ��������.
    /// </summary>
    public string Login
    {
      get { return login; }
      set { login = value; }
    }
    private string login;

    /// <summary>
    /// ������ ��� ������� �� Ftp ������. �������� 10 ��������.
    /// </summary>
    public string Password
    {
      get { return password; }
      set { password = value; }
    }
    private string password;

    /// <summary>
    /// ���� � ������ � �����������. �������� 20 ��������.
    /// </summary>
    public string ConfigPath
    {
      get { return configPath; }
      set { configPath = value; }
    }
    private string configPath;

    /// <summary>
    /// ����, ���� ������� ���������� ����������. �������� 20 ��������.
    /// </summary>
    public string PutPath
    {
      get { return putPath; }
      set { putPath = value; }
    }
    private string putPath;

    /// <summary>
    /// �����������
    /// </summary>
    public GprsFtpConfig()
    {
      server = "";
      login = "";
      password = "";
      configPath = "";
      putPath = "";
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;

      if (server.Length > 25)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "server(FTP ������)",
          "GprsFtpConfig(��������� FTP GPRS)",
          25);
        return false;
      }
      if (login.Length > 10)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "login(����� ��� ������� �� Ftp ������)",
          "GprsFtpConfig(��������� FTP GPRS)",
          10);
        return false;
      }
      if (password.Length > 10)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "password(������ ��� ������� �� Ftp ������)",
          "GprsFtpConfig(��������� FTP GPRS)",
          10);
        return false;
      }
      if (configPath.Length > 20)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "configPath(���� � ������ � �����������)",
          "GprsFtpConfig(��������� FTP GPRS)",
          20);
        return false;
      }
      if (putPath.Length > 20)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "putPath(���� ��� ������������� ����������)",
          "GprsFtpConfig(��������� FTP GPRS)",
          20);
        return false;
      }
      return true;
    }
  }
} 
