using System;
using RCS.Protocol.A1.Error;

namespace RCS.Protocol.A1.Entity
{
  /// <summary>
  /// ������������ �������� GPRS. ��������� Email.
  /// ���� �������� ��������� ����������� � ����� �������� �� ����� 
  /// �����������������: ���������� ������, ��������� ������ �1 � �.�. 
  /// ������� ������������ ��� ��� ��������� ����������, 
  /// ���������� �� ���������� POP3/SMTP, ��� � �� SOCKET.
  /// �������������� ������: 51, 61, 81
  /// </summary>
  public class GprsEmailConfig : CommonDescription
  {
    /// <summary>
    /// Smtp ������ - ��� ��������� ������� �������� �����. 
    /// ������ ��� ����������, ���������� �� ���������� POP3/SMTP.
    /// �������� 25 ��������. 
    /// </summary>
    public string SmtpServer
    {
      get { return smtpServer; }
      set { smtpServer = value; }
    }
    private string smtpServer;

    /// <summary>
    /// ����� ��� �������� �����, �������� 10 ��������.
    /// ������ ��� ����������, ���������� �� ���������� POP3/SMTP.
    /// </summary>
    public string SmtpLogin
    {
      get { return smtpLogin; }
      set { smtpLogin = value; }
    }
    private string smtpLogin;

    /// <summary>
    /// ������ ��� �������� �����, �������� 10 ��������.
    /// ������ ��� ����������, ���������� �� ���������� POP3/SMTP.
    /// </summary>
    public string SmtpPassword
    {
      get { return smtpPassword; }
      set { smtpPassword = value; }
    }
    private string smtpPassword;

    /// <summary>
    /// Pop3 ������ ��� online ������ - ���������� ��� ��� IP �����. 
    /// ���� ������� ���������� ��� ������ ���� ������ IP ����� DNS �������
    /// �������� �50. 
    /// <para>�������� 25 ��������.</para>
    /// </summary>
    public string Pop3Server
    {
      get { return pop3Server; }
      set { pop3Server = value; }
    }
    private string pop3Server;

    /// <summary>
    /// � ����������� �� ������ ������ ������������ ����� ���� ��
    /// ���� ���������:
    /// <list type="bullet">
    ///   <item><description>����� ��� Pop3 - �������� 10 ��������.</description></item>
    ///   <item><description>����� ��� online ������� - �������� 4 �������.</description></item>
    /// </list>
    /// <para>��������!  ���� ������������� ��������, ���������� �� �������, 
    /// �� �������� ���� pop3un ������ ��� �������������� �� ������� ������ ���� 
    /// ���������� �������� ���� devIdShort ������������� ����������������� ���������� 
    /// (�������: � 17, 27, 47). ��� ������� � ���, ��� ����������� GPS ������ ����� 
    /// ���������������� ������ �� �������������������� ������ �����(������������ pop3un), 
    /// � �������-��������� ��������� �1 ����� �������� ������������� ���������, 
    /// ������� ������������� �� ���� devIdShort ������������� ����������������� ����������.</para>
    /// </summary>
    public string Pop3Login
    {
      get { return pop3Login; }
      set { pop3Login = value; }
    }
    private string pop3Login;

    /// <summary>
    /// ������ Pop3 ��� online �������. �������� 10 ��������.
    /// </summary>
    public string Pop3Password
    {
      get { return pop3Password; }
      set { pop3Password = value; }
    }
    private string pop3Password;

    /// <summary>
    /// �����������
    /// </summary>
    public GprsEmailConfig()
    {
      smtpServer = "";
      smtpLogin = "";
      smtpPassword = "";
      pop3Server = "";
      pop3Login = "";
      pop3Password = "";
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;

      if (smtpServer.Length > 25)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "smtpServer(SMTP ������)",
          "GprsEmailConfig(��������� Email GPRS)",
          25);
        return false;
      }
      if (smtpLogin.Length > 10)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "smtpLogin(SMTP �����)",
          "GprsEmailConfig(��������� Email GPRS)",
          10);
        return false;
      }
      if (smtpPassword.Length > 10)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "smtpPassword(SMTP ������)",
          "GprsEmailConfig(��������� Email GPRS)",
          10);
        return false;
      }
      if (pop3Server.Length > 25)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "pop3Server(POP3 ������)",
          "GprsEmailConfig(��������� Email GPRS)",
          25);
        return false;
      }
      if (pop3Login.Length > 10)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "pop3Login(POP3 �����)",
          "GprsEmailConfig(��������� Email GPRS)",
          10);
        return false;
      }
      if (pop3Password.Length > 10)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "pop3Password(POP3 ������)",
          "GprsEmailConfig(��������� Email GPRS)",
          10);
        return false;
      }
      return true;
    }
  }
}
