namespace RCS.Protocol.A1.Entity
{
  /// <summary>
  /// ������������� ��������� ������� ������������ ����������� ���. 
  /// �������������� ������: 25
  /// </summary>
  public class ZoneConfigConfirm : CommonDescription
  {
    /// <summary>
    /// ���� ������������� ��� ������� ����� � ������� ZoneConfig
    /// </summary>
    public int ZoneMsgID
    {
      get { return zoneMsgID; }
      set { zoneMsgID = value; }
    }
    private int zoneMsgID;

    /// <summary>
    /// ������� ��������� ����������� ��� (Busy, not busy)
    /// </summary>
    public byte ZoneState
    {
      get { return zoneState; }
      set { zoneState = value; }
    }
    private byte zoneState;

    /// <summary>
    /// ��������� ��������� ����������� ���.
    /// 1 - ����������� ���� ���� �������� ���������
    /// 0 - ��������� ������ 
    /// </summary>
    public byte Result
    {
      get { return result; }
      set { result = value; }
    }
    private byte result;
  }
}
