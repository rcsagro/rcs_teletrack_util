namespace RCS.Protocol.A1.Entity
{
  /// <summary>
  /// ��������� ������ GPS ������
  /// </summary>
  public class DataGps
  {
    /// <summary>
    /// ����� UnixTime
    /// </summary>
    public int Time
    {
      get { return time; }
      set { time = value; }
    }
    private int time;

    /// <summary>
    /// ������, ������� * 600000
    /// </summary>
    public int Latitude
    {
      get { return latitude; }
      set { latitude = value; }
    }
    private int latitude;

    /// <summary>
    /// �������, ������� * 600000
    /// </summary>
    public int Longitude
    {
      get { return longitude; }
      set { longitude = value; }
    }
    private int longitude;

    /// <summary>
    /// ������, �����
    /// </summary>
    public byte Altitude
    {
      get { return altitude; }
      set { altitude = value; }
    }
    private byte altitude;

    /// <summary>
    /// �����������, ������� / 1.5
    /// </summary>
    public byte Direction
    {
      get { return direction; }
      set { direction = value; }
    }
    private byte direction;

    /// <summary>
    /// ��������, ���� � ���
    /// </summary>
    public byte Speed
    {
      get { return speed; }
      set { speed = value; }
    }
    private byte speed;

    /// <summary>
    /// ������ � ����
    /// </summary>
    public int LogID
    {
      get { return logID; }
      set { logID = value; }
    }
    private int logID;

    /// <summary>
    /// ����� (����������, ���-�� ���������)
    /// </summary>
    public byte Flags
    {
      get { return flags; }
      set { flags = value; }
    }
    private byte flags;

    /// <summary>
    /// ���� ������������� ������ (����������).
    /// True - ������ ����� ��������
    /// </summary>
    public bool Valid
    {
      get { return valid; }
      set { valid = value; }
    }
    private bool valid;

    /// <summary>
    /// �������
    /// </summary>
    public uint Events
    {
      get { return events; }
      set { events = value; }
    }
    private uint events;

    /// <summary>
    /// ������ ���������� ������ ���������� � �������� 
    /// True - ������������ ���������� � ��������, ���������� � ��������� ����������
    /// False - ��� ���������� � ��������, ������������ ���������� � ���������
    /// </summary>
    public bool SensorsDataExists
    {
      get { return sensorsDataExists; }
      set { sensorsDataExists = value; }
    }
    private bool sensorsDataExists;

    /// <summary>
    /// ���������������. ������� 1
    /// </summary>
    public ushort Counter1
    {
      get { return counter1; }
      set { counter1 = value; }
    }
    private ushort counter1;

    /// <summary>
    /// ���������������. ������� 2 
    /// </summary>
    public ushort Counter2
    {
      get { return counter2; }
      set { counter2 = value; }
    }
    private ushort counter2;

    /// <summary>
    /// ���������������. ������� 3 
    /// </summary>
    public ushort Counter3
    {
      get { return counter3; }
      set { counter3 = value; }
    }
    private ushort counter3;

    /// <summary>
    /// ���������������. ������� 4
    /// </summary>
    public ushort Counter4
    {
      get { return counter4; }
      set { counter4 = value; }
    }
    private ushort counter4;

    /// <summary>
    /// ������ 1
    /// </summary>
    public byte Sensor1
    {
      get { return sensor1; }
      set { sensor1 = value; }
    }
    private byte sensor1;

    /// <summary>
    /// ������ 2
    /// </summary>
    public byte Sensor2
    {
      get { return sensor2; }
      set { sensor2 = value; }
    }
    private byte sensor2;

    /// <summary>
    /// ������ 3
    /// </summary>
    public byte Sensor3
    {
      get { return sensor3; }
      set { sensor3 = value; }
    }
    private byte sensor3;

    /// <summary>
    /// ������ 4
    /// </summary>
    public byte Sensor4
    {
      get { return sensor4; }
      set { sensor4 = value; }
    }
    private byte sensor4;

    /// <summary>
    /// ������ 5
    /// </summary>
    public byte Sensor5
    {
      get { return sensor5; }
      set { sensor5 = value; }
    }
    private byte sensor5;

    /// <summary>
    /// ������ 6
    /// </summary>
    public byte Sensor6
    {
      get { return sensor6; }
      set { sensor6 = value; }
    }
    private byte sensor6;

    /// <summary>
    /// ������ 7
    /// </summary>
    public byte Sensor7
    {
      get { return sensor7; }
      set { sensor7 = value; }
    }
    private byte sensor7;

    /// <summary>
    /// ������ 8
    /// </summary>
    public byte Sensor8
    {
      get { return sensor8; }
      set { sensor8 = value; }
    }
    private byte sensor8;
  }
}
