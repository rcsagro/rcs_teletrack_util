using System;
using RCS.Protocol.A1.Error;

// ������-��������� ������
namespace RCS.Protocol.A1.Entity
{
  /// <summary>
  /// ��������� ��������� ��� ��������. �������������� ������: 10
  /// </summary>
  public class MessageToDriver : CommonDescription
  {
    /// <summary>
    /// ��������� ���������, ������������ ������ 79 ��������.
    /// </summary>
    public string Text
    {
      get { return text; }
      set { text = value; }
    }
    private string text;

    /// <summary>
    /// �����������
    /// </summary>
    public MessageToDriver()
    {
      text = "";
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;
      if (Text.Length > 79)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "Text(��������� ���������)",
          "MessageToDriver(��������� ��������� ��� ��������)",
          79);
        return false;
      }
      return true;
    }
  }
}
