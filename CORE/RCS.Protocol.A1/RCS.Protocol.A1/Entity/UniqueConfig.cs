using System;
using RCS.Protocol.A1.Error;

namespace RCS.Protocol.A1.Entity
{
  /// <summary>
  /// �� ������������
  /// ������������ ���������� ����������.
  ///	�������������� ������: 14, 24, 44
  /// </summary>
  public class UniqueConfig : CommonDescription
  {
    /// <summary>
    /// ������������� �������������� ������. ������ � ����� 
    /// �������������� ������ �������� ����� ��������� ���������.
    /// �������� 4 �������.
    /// </summary>
    public string DispatcherID
    {
      get { return dispatcherID; }
      set { dispatcherID = value; }
    }
    private string dispatcherID;

    /// <summary>
    /// ���������� ������. �������� 8 ��������.
    /// </summary>
    public string Password
    {
      get { return password; }
      set { password = value; }
    }
    private string password;

    /// <summary>
    /// ��������� ������. �������� 8 ��������.
    /// </summary>
    public string TmpPassword
    {
      get { return tmpPassword; }
      set { tmpPassword = value; }
    }
    private string tmpPassword;

    /// <summary>
    /// �����������
    /// </summary>
    public UniqueConfig()
    {
      dispatcherID = "";
      password = "";
      tmpPassword = "";
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;

      if (DispatcherID.Length > 4)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "DispatcherID(������������� �������������� ������)",
          "UniqueConfig(������������ ���������� ����������)",
          4);
        return false;
      } // if (DsptEmailGprs.Length)
      if (Password.Length > 8)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "Password(���������� ������)",
          "UniqueConfig(������������ ���������� ����������)",
          8);
        return false;
      }
      if (TmpPassword.Length > 8)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "TmpPassword(��������� ������)",
          "UniqueConfig(������������ ���������� ����������)",
          8);
        return false;
      }
      return true;
    }
  }
}
