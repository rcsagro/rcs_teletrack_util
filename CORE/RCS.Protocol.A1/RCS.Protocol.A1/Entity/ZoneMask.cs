#region Using directives
using System;
using System.Text;
#endregion

namespace RCS.Protocol.A1.Entity
{
  /// <summary>
  /// �������� ����� ���� ������-������ � �������� ����������
  /// � ���� ���  ��� ����
  /// </summary>
  public class ZoneMask
  {
    /// <summary>
    /// ���� �������� ������ � ����
    /// </summary>
    public readonly byte EntryFlag;
    /// <summary>
    /// ���� �������� ������ �� ����
    /// </summary>
    public readonly byte ExitFlag;
    /// <summary>
    /// ���� �������� ���������� ������ ����
    /// </summary>
    public readonly byte InFlag;
    /// <summary>
    /// ���� �������� ���������� ������� ���� 
    /// </summary>
    public readonly byte OutFlag;

    /// <summary>
    /// Create zone mask
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException">��������� ����� ����� �������� ������ 0 ��� 1</exception>
    /// <param name="entryFlag">���� �������� ������ � ����: 0 ��� 1</param>
    /// <param name="exitFlag">���� �������� ������ �� ����: 0 ��� 1</param>
    /// <param name="inFlag">���� �������� ���������� ������ ����: 0 ��� 1</param>
    /// <param name="outFlag">���� �������� ���������� ������� ����: 0 ��� 1</param>
    public ZoneMask(byte entryFlag, byte exitFlag, byte inFlag, byte outFlag)
    {
      const string error = "�������� {0} ����� ����� �������� ������ 0 ��� 1";
      if (entryFlag > 1)
      {
        throw new ArgumentOutOfRangeException("entryFlag",
          String.Format(error, "entryFlag"));
      }
      if (exitFlag > 1)
      {
        throw new ArgumentOutOfRangeException("exitFlag",
          String.Format(error, "exitFlag"));
      }
      if (inFlag > 1)
      {
        throw new ArgumentOutOfRangeException("inFlag",
          String.Format(error, "inFlag"));
      }
      if (outFlag > 1)
      {
        throw new ArgumentOutOfRangeException("outFlag",
          String.Format(error, "outFlag"));
      }
      EntryFlag = entryFlag;
      ExitFlag = exitFlag;
      InFlag = inFlag;
      OutFlag = outFlag;
    }
  }
}
