using System;
using RCS.Protocol.A1.Error;

namespace RCS.Protocol.A1.Entity
{
  /// <summary>
  /// ������������ �������� GPRS. 
  /// ��������� ����������� � ����������.
  /// �������: 54, 64, 84
  /// </summary>
  public class GprsProviderConfig : CommonDescription
  {
    /// <summary>
    /// ������ ������������� GPRS �����������. 
    /// �������� 50 ��������.
    /// </summary>
    public string InitString
    {
      get { return initString; }
      set { initString = value; }
    }
    private string initString;

    /// <summary>
    /// ����� GPRS �����������. �������� 25 ��������.
    /// </summary>
    public string Domain
    {
      get { return domain; }
      set { domain = value; }
    }
    private string domain;

    /// <summary>
    /// �����������
    /// </summary>
    public GprsProviderConfig()
    {
      initString = "";
      domain = "";
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;

      if (InitString.Length > 50)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "InitString(������ ������������� GPRS �����������)",
          "GprsProviderConfig(��������� ����������� � ���������� GPRS)",
          50);
        return false;
      }
      if (Domain.Length > 25)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "Domain(����� GPRS �����������)",
          "GprsProviderConfig(��������� ����������� � ���������� GPRS)",
          25);
        return false;
      }
      return true;
    }
  }
}
