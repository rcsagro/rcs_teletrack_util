#region Using directives
using System.Collections.Generic;
#endregion

namespace RCS.Protocol.A1.Entity
{
  /// <summary>
  /// �������������� ������� GPS ������ ��� 
  /// ����� �� ������ ������ (������� DataGpsQuery).
  /// �������������� ������: 46, 49
  /// </summary>
  public class DataGpsAnswer : CommonDescription
  {
    /// <summary>
    /// ���� �������� ���������� � ���, ����� GPS ���������� ���������� � ���������.
    /// ����������� ������� ��� �����:
    /// 0 - �����
    /// 1 - ������
    /// 2 - �������
    /// 3 - ������
    /// 4 - �����������
    /// 5 - ��������
    /// 6 - ������ ���� (������ ����� 1)
    /// 7 - �����
    /// 8 - �������
    /// 9 - �������
    /// 10 - ��������
    /// </summary>
    public ushort WhatWrite
    {
      get { return whatWrite; }
      set { whatWrite = value; }
    }
    private ushort whatWrite;


    /// <summary>
    /// C����� ������� � GPS �������
    /// </summary>
    public List<DataGps> DataGpsList
    {
      get { return dataGpsList; }
    }
    private List<DataGps> dataGpsList;

    /// <summary>
    /// ������������ ������ DataGps
    /// </summary>
    /// <param name="data">Data</param>
    public void Add(DataGps data)
    {
      dataGpsList.Add(data);
    }

    /// <summary>
    /// �����������
    /// </summary>
    public DataGpsAnswer()
    {
      dataGpsList = new List<DataGps>();
    }
  }
}
