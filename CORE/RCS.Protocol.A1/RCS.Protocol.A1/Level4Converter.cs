#region Using directives
using System;
using System.Collections.Generic;
using System.Text;
#endregion

namespace RCS.Protocol.A1
{
  /// <summary>
  /// ��������� ������ �� ������ 4 ��������� A1
  /// </summary>
  public static class Level4Converter
  {
    /// <summary>
    /// �������������� ������� ���� � ushort
    /// </summary>
    /// <exception cref="ArgumentException">������� ������ ������ ���� source</exception>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <exception cref="ArgumentOutOfRangeException">startIndex ������ ���� ������ ��� ����� ���� � ������ ����� ������� source</exception>
    /// <param name="source">byte[] ����� ������ ��� ����� 1</param>
    /// <param name="startIndex">������ ������� � �������� ����� ������������� ��������������</param>
    /// <returns>ushort</returns>
    public static ushort BytesToUShort(byte[] source, int startIndex)
    {
      return (ushort)ToInt16(source, startIndex);
    }

    /// <summary>
    /// �������������� ������� ���� � short
    /// </summary>
    /// <exception cref="ArgumentException">������� ������ ������ ���� source</exception>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <exception cref="ArgumentOutOfRangeException">startIndex ������ ���� ������ ��� ����� ���� � ������ ����� ������� source</exception>
    /// <param name="source">byte[] ����� ������ ��� ����� 1</param>
    /// <param name="startIndex">������ ������� � �������� ����� ������������� ��������������</param>
    /// <returns>short</returns>
    public static short BytesToShort(byte[] source, int startIndex)
    {
      return (short)ToInt16(source, startIndex);
    }

    /// <summary>
    /// �������������� ������� ���� � short - ushort. 
    /// <para>�������� ������������ ��� int32.</para>
    /// </summary>
    /// <exception cref="ArgumentException">������� ������ ������ ���� source</exception>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <exception cref="ArgumentOutOfRangeException">startIndex ������ ���� ������ ��� ����� ���� � ������ ����� ������� source</exception>
    /// <param name="source">byte[] ����� ������ ��� ����� 1</param>
    /// <param name="startIndex">������ ������� � �������� ����� ������������� ��������������</param>
    /// <returns>Int</returns>
    internal static int ToInt16(byte[] source, int startIndex)
    {
      if (source == null)
        throw new ArgumentNullException("source", "�� ������� ������ ���� source");
      if (source.Length == 0)
        throw new ArgumentException("source", "������� ������ ������ ���� source");
      if ((startIndex < 0) || (startIndex >= source.Length))
        throw new ArgumentOutOfRangeException("startIndex", "startIndex ������ ���� ������ ��� ����� ���� � ������ ����� ������� source");

      int H = 0, L = 0;

      if (startIndex + 1 < source.Length)
      {
        H = source[startIndex];
        L = source[startIndex + 1];
      } // if (startIndex)
      else
      {
        L = source[startIndex];
      } // else
      return ((H << 8) | L);
    }

    /// <summary>
    /// �������������� ������� ���� � int
    /// </summary>
    /// <exception cref="ArgumentException">������� ������ ������ ���� source</exception>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <exception cref="ArgumentOutOfRangeException">startIndex ������ ���� ������ ��� ����� ���� � ������ ����� ������� source</exception>
    /// <param name="source">byte[] ����� ������ ��� ����� 1</param>
    /// <param name="startIndex">������ ������� � �������� ����� ������������� ��������������</param>
    /// <returns>int</returns>
    public static int BytesToInt(byte[] source, int startIndex)
    {
      return (int)ToInt32(source, startIndex);
    }

    /// <summary>
    /// �������������� ������� ���� � uint
    /// </summary>
    /// <exception cref="ArgumentException">������� ������ ������ ���� source</exception>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <exception cref="ArgumentOutOfRangeException">startIndex ������ ���� ������ ��� ����� ���� � ������ ����� ������� source</exception>
    /// <param name="source">byte[] ����� ������ ��� ����� 1</param>
    /// <param name="startIndex">������ ������� � �������� ����� ������������� ��������������</param>
    /// <returns>uint</returns>
    public static uint BytesToUInt(byte[] source, int startIndex)
    {
      return (uint)ToInt32(source, startIndex);
    }

    /// <summary>
    /// �������������� ������� ���� � int - uint. 
    /// �������� ������������ ��� long.
    /// </summary>
    /// <exception cref="ArgumentException">������� ������ ������ ���� source</exception>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <exception cref="ArgumentOutOfRangeException">startIndex ������ ���� ������ ��� ����� ���� � ������ ����� ������� source</exception>
    /// <param name="source">byte[] ����� ������ ��� ����� 1</param>
    /// <param name="startIndex">������ ������� � �������� ����� ������������� ��������������</param>
    /// <returns>long</returns>
    internal static long ToInt32(byte[] source, int startIndex)
    {
      if (source == null)
        throw new ArgumentNullException("source", "�� ������� ������ ���� source");
      if (source.Length == 0)
        throw new ArgumentException("source", "������� ������ ������ ���� source");
      if ((startIndex < 0) || (startIndex >= source.Length))
        throw new ArgumentOutOfRangeException("startIndex", "startIndex ������ ���� ������ ��� ����� ���� � ������ ����� ������� source");

      int H = 0, L = 0;

      if (startIndex + 2 < source.Length)
      {
        H = ToInt16(source, startIndex);
        L = ToInt16(source, startIndex + 2);
      }
      else
      {
        L = source[startIndex];
      }

      return ((H << 16) | (L & 0xFFFF));
    }


    /// <summary>
    /// �������������� ushort � ������ ���� byte[2]
    /// </summary>
    /// <param name="value">ushort value</param>
    /// <returns>byte[2]</returns>
    public static byte[] UShortToBytes(ushort value)
    {
      return new byte[2] { (byte)(value >> 8), (byte)value };
    }

    /// <summary>
    /// �������������� short � ������ ���� byte[2]
    /// </summary>
    /// <param name="value">short value</param>
    /// <returns>byte[2]</returns>
    public static byte[] ShortToBytes(short value)
    {
      return new byte[2] { (byte)(value >> 8), (byte)value };
    }

    /// <summary>
    /// �������������� int32 � ������ ���� byte[4]
    /// </summary>
    /// <param name="value">int32 value</param>
    /// <returns>byte[4]</returns>
    public static byte[] IntToBytes(int value)
    {
      byte[] highArray = UShortToBytes((ushort)(value >> 16));
      byte[] lowArray = UShortToBytes((ushort)value);

      return new byte[4] { 
        highArray[0], highArray[1], lowArray[0], lowArray[1] };
    }

    /// <summary>
    /// �������������� uint32 � ������ ���� byte[4]
    /// </summary>
    /// <param name="value">uint value</param>
    /// <returns>byte[4]</returns>
    public static byte[] UIntToBytes(uint value)
    {
      byte[] highArray = UShortToBytes((ushort)(value >> 16));
      byte[] lowArray = UShortToBytes((ushort)value);

      return new byte[4] { 
        highArray[0], highArray[1], lowArray[0], lowArray[1] };
    }

    /// <summary>
    /// ������������ ������ �� ������� ���� ������ LEVEL4
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <exception cref="ArgumentOutOfRangeException">�������� startIndex ��� length ����� �� ������� �����������</exception>
    /// <param name="source">byte[]</param>
    /// <param name="startIndex">��������� ������� � ������� source</param>
    /// <param name="length">���-�� ����, ����������� � ��������</param>
    /// <returns>������</returns>
    public static string BytesToString(byte[] source, int startIndex, int length)
    {
      if (source == null)
        throw new ArgumentNullException("source", "�� ������� ������ ���� source");
      if ((startIndex < 0) || (startIndex >= source.Length))
        throw new ArgumentOutOfRangeException("startIndex",
          "��� ��������� startIndex ������ ����������� �������: (startIndex >= 0) && (startIndex < source.Length)");

      int finishIndex = startIndex + length;

      if ((length <= 0) || (finishIndex >= source.Length))
        throw new ArgumentOutOfRangeException("length",
          "��� ��������� length ������ ����������� �������: (length > 0) && (startIndex + length < source.Length)");

      StringBuilder result = new StringBuilder();

      for (int i = startIndex; i < finishIndex; i++)
      {
        char ch = Util.ConvertAsciiWin1251ToChar(source[i]);
        if ((byte)ch == 0)
          break;

        result.Append(ch);
      }

      return result.ToString();
    }

    /// <summary>
    /// ������������� ������ � ������ ����.
    /// <para>������ ������� ����� ���-�� �������� ���������� ������, 
    /// �� �� ����� maxLength</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">�� �������� ������ value</exception>
    /// <exception cref="ArgumentOutOfRangeException">�������� maxLength ������ ���� ������ ����</exception>
    /// <param name="value">������</param>
    /// <param name="maxLength">������������ ���-�� ��������, ������� ����� ����������</param>
    /// <returns>byte[]</returns>
    public static byte[] StringToBytes(string value, int maxLength)
    {
      if (value == null)
        throw new ArgumentNullException("value", "�� �������� ������ value");
      if (maxLength <= 0)
        throw new ArgumentOutOfRangeException("maxLength", "�������� maxLength ������ ���� ������ ����");

      byte[] result = new byte[value.Length];

      for (int i = 0; i < Math.Min(value.Length, maxLength); i++)
        result[i] = Util.ConvertCharToAsciiWin1251(value[i]);

      return result;
    }

    /// <summary>
    /// ������������ ������ ����������� ������ �� ������� ���� ������ LEVEL4.
    /// <para>����� ���������� ��������, ������� ��� � ���� �������� �1.
    /// � ������ ����� ��������� 2 �������.</para> 
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <exception cref="ArgumentOutOfRangeException">�������� startIndex ��� length ����� �� ������� �����������</exception>
    /// <param name="source">byte[]</param>
    /// <param name="startIndex">��������� ������� � ������� source</param>
    /// <param name="length">���-�� ����, ����������� � ��������</param>
    /// <returns>������</returns>
    public static string BytesToTelNumber(byte[] source, int startIndex, int length)
    {
      if (source == null)
        throw new ArgumentNullException("source", "�� ������� ������ ���� source");
      if ((startIndex < 0) || (startIndex >= source.Length))
        throw new ArgumentOutOfRangeException("startIndex",
          "��� ��������� startIndex ������ ����������� �������: (startIndex >= 0) && (startIndex < source.Length)");

      int finishIndex = startIndex + length;

      if ((length <= 0) || (finishIndex >= source.Length))
        throw new ArgumentOutOfRangeException("length",
          "��� ��������� length ������ ����������� �������: (length > 0) && (startIndex + length < source.Length)");

      string result = "";
      for (int i = startIndex << 1; i < finishIndex << 1; i++)
      {
        byte code = GetPhoneNumberSymbol(source, i);
        if (code < 0x0d)
        {
          switch (code)
          {
            case 10:
              result += "+";
              break;
            case 11:
              result += "*";
              break;
            case 12:
              result += "#";
              break;
            default:
              result += code.ToString();
              break;
          }
        }
        else
        {
          break;
        }
      }
      return result;
    }

    /// <summary>
    /// ��������� ������ ����������� ������ �� ��������� ���������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <exception cref="ArgumentOutOfRangeException">�������� index ����� �� ������� �����������</exception>
    /// <param name="source">byte[]</param>
    /// <param name="index">index</param>
    /// <returns>Byte</returns>
    internal static byte GetPhoneNumberSymbol(byte[] source, int index)
    {
      if ((source == null) || (source.Length == 0))
        throw new ArgumentNullException("source", "�� ������� ������ ���� source");
      if ((index < 0) || (index >= source.Length << 1))
        throw new ArgumentOutOfRangeException("index",
          "��� ��������� index ������ ����������� �������: (index >= 0) && (index < source.Length)");

      if ((index & 0x01) != 0)
        return (byte)(source[index >> 1] & 0x0F);
      else
        return (byte)(source[index >> 1] >> 4);
    }


    /// <summary>
    /// ������������ ����������� ������ � ���� ������� ���� ������ LEVEL4
    /// �� ��������� �������� ������.
    /// <para>����� ���������� ��������, ������� ��� � ���� �������� �1.
    /// � ������ ����� ��������� 2 �������.</para> 
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ���������� ����� phoneNumber</exception>
    /// <exception cref="ArgumentOutOfRangeException">maxLength ������ ���� ������ ����</exception>
    /// <param name="phoneNumber">���������� ����� � ���� ������</param>
    /// <param name="maxLength">������������ ����� � ������</param>
    /// <returns>byte[]</returns>
    public static byte[] TelNumberToBytes(string phoneNumber, int maxLength)
    {
      if (phoneNumber == null)
        throw new ArgumentNullException("phoneNumber", "�� ������� ���������� �����");
      if (maxLength <= 0)
        throw new ArgumentOutOfRangeException("maxLength", "maxLength ������ ���� ������ ����");

      byte[] result = new byte[maxLength];

      int i = 0;
      int tmpLength = (maxLength << 1) - 2;
      while ((i < phoneNumber.Length) && (i < tmpLength))
      {
        WriteTelNumberSymbol(phoneNumber[i], result, i);
        i++;
      }
      WriteTelNumberSymbol(';', result, i);
      i++;
      WriteTelNumberSymbol(Const.ZERO_SYMBOL, result, i);

      return result;
    }

    /// <summary>
    /// ���������� ������ ����������� ������ � �������� ��������.
    /// </summary>
    /// <param name="symbol">char ������ ����������� ������</param>
    /// <param name="result">byte[] ������������� ������, � ������� �����</param>
    /// <param name="index">������</param>
    private static void WriteTelNumberSymbol(char symbol, byte[] result, int index)
    {
      byte res = 255;

      switch (symbol)
      {
        case Const.ZERO_SYMBOL:
          res = 0x0F;
          break;
        case '+':
          res = 0x0A;
          break;
        case '*':
          res = 0x0B;
          break;
        case '#':
          res = 0x0C;
          break;
        case ';':
          res = 0x0D;
          break;
        default:
          if (((byte)symbol >= 48) && ((byte)symbol <= 57))
            res = (byte)((byte)symbol - 48);
          break;
      }

      if (res < 16)
      {
        if ((index & 0x01) != 0)
          result[index >> 1] = (byte)((result[index >> 1] & 0xF0) | (res & 0x0F));
        else
          result[index >> 1] = (byte)((result[index >> 1] & 0x0F) | ((res & 0x0F) << 4));
      }
    }
  }
}
