using System;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.A1.Error;

namespace RCS.Protocol.A1
{
  /// <summary>
  /// ������������� ��������� ��������� �� ��������� �� ��������� A1
  /// </summary>
  public class Decoder
  {
    /// <summary>
    /// ������������� ���������.
    /// <para>� ���������� ���������� ������ �������� ������� ����� 
    /// CommonDescription ��������� ���������. 
    /// <para>����� ���������� ����� ��������� ������ ����� ������������ �������� 
    /// <para>CommandID ���������� (������������ ������� � CommandDescriptor)
    /// ��� Reflection.</para></para></para>
    /// <example> <code>
    /// // ����� ��������� �� ��������� � ���������� ����
    /// string source = "d500@p345.net %%..iz5xzvFAcygYdcYZje9AAjAAgAYdYnZf93AAAIAgZwJUcoT9ANAAACgYdcYZfe+AtC8wACQTVBVSTxVAUiQUMfAgJoUQYZcac3XSU-gYdcYZje+AtC8wXyXXX-XXX-XXX-XXX-XXX-XXX-";
    /// // �������������
    /// CommonDescription message = A1.Decoder.DecodeMessage(source);
    /// // ������ � ���������
    /// switch (message.CommandID)
    /// {
    ///   case A1.CommandDescriptor.SMSConfigConfirm:
    ///   case A1.CommandDescriptor.SMSConfigAnswer:
    ///     SaveConfigSms((SmsAddrConfig)message);
    ///     break;
    /// 
    ///   // � ��� �����
    /// 
    ///   default:
    ///     throw new ApplicationException("Unknown message type");
    /// } 
    /// </code></example>
    /// </summary>
    /// <exception cref="A1Exception">������ ����������� � �������� ��������</exception>
    /// <param name="message">��������� ��������� string 160 ��������</param>
    /// <returns>������� ����� CommonDescription ��� ���������� ���������.</returns>
    public static CommonDescription DecodeMessage(string message)
    {
      return DecodeMessage(Level1Converter.StringToByteArray(message));
    }

    /// <summary>
    /// ������������� ��������� ������ 160 ����.
    /// <para>� ���������� ���������� ������ �������� ������� ����� 
    /// CommonDescription ��������� ���������. 
    /// <para>����� ���������� ����� ��������� ������ ����� ������������ �������� 
    /// <para>CommandID ���������� (������������ ������� � CommandDescriptor)
    /// ��� ���������� � �����.</para></para></para>
    /// <example> <code>
    /// // ��������� �� ��������� � �������� ����
    /// byte[] source;
    /// // �������������
    /// CommonDescription message = A1.Decoder.DecodeMessage(source);
    /// // ������ � ���������
    /// switch (message.CommandID)
    /// {
    ///   case A1.CommandDescriptor.SMSConfigConfirm:
    ///   case A1.CommandDescriptor.SMSConfigAnswer:
    ///     SaveConfigSms((SmsAddrConfig)message);
    ///     break;
    /// 
    ///   // � ��� �����
    /// 
    ///   default:
    ///     throw new ApplicationException("Unknown message type");
    /// } 
    /// </code></example> 
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� message</exception> 
    /// <exception cref="A1Exception">������ ����������� � �������� ��������</exception>
    /// <param name="message">��������� ��������� byte[160]</param>
    /// <returns>������� ����� CommonDescription ��� ���������� ���������.</returns>
    public static CommonDescription DecodeMessage(byte[] message)
    {
      if ((message == null) || (message.Length == 0))
        throw new ArgumentNullException("message", "�� ������� ������ ���� message");
      if (message.Length != Const.LEVEL0_PACKET_LENGTH)
        throw new A1Exception(String.Format(ErrorText.LENGTH_PACKET,
          "LEVEL0", Const.LEVEL0_PACKET_LENGTH, message.Length));

      // 1. ����� ������� %%
      int startMessageIndex;
      if (!Util.FindMessageStart(message, out startMessageIndex))
        throw new A1Exception(ErrorText.START_INDICATOR_MISSING);

      if (startMessageIndex != Const.LEVEL0_START_INDICATOR_POSITION)
        throw new A1Exception(
          String.Format(ErrorText.START_INDICATOR_FLOW, startMessageIndex));

      // ���� ������ �� ������ LEVEL1
      byte[] packet = new byte[Const.LEVEL1_PACKET_LENGHT];
      Array.Copy(message, Const.LEVEL0_START_INDICATOR_POSITION + 2,
        packet, 0, Const.LEVEL1_PACKET_LENGHT);

      // 2. �������� ����� 
      int length = Level1Converter.SymbolToValue(
        packet[Const.LEVEL1_LENGTH_POSITION]) << 2;
      if (length != Const.LEVEL1_DATA_BLOCK_LENGHT)
        throw new A1Exception(String.Format(ErrorText.LENGTH_LEVEL1_UNEXPECTED,
          length, Const.LEVEL1_DATA_BLOCK_LENGHT));

      byte[] tmpVersion = new byte[2];
      Array.Copy(packet, 0, tmpVersion, 0, 2);
      string version = Level1Converter.BytesToString(tmpVersion);

      // 3. �������� CRC  
      byte crcSpecified = Level1Converter.SymbolToValue(packet[7]);
      byte crcCalculated = Util.CalculateLevel1CRC(packet, 0, length);
      if (crcSpecified != crcCalculated)
        throw new A1Exception(
          String.Format(ErrorText.CRC, crcSpecified, crcCalculated));

      // �������� ������������� ���������
      byte[] shortId = new byte[4];
      Array.Copy(packet, 3, shortId, 0, 4);

      // ���� ������ �� ������ LEVEL1 
      byte[] dataBlock = new byte[Const.LEVEL1_DATA_BLOCK_LENGHT];
      Array.Copy(packet, Const.LEVEL1_CRC_POSITION + 1,
        dataBlock, 0, Const.LEVEL1_DATA_BLOCK_LENGHT);

      CommonDescription result = DecodeLevel3Message(
        Level1Converter.Decode6BitTo8(dataBlock));

      result.ShortID = Level1Converter.BytesToString(shortId);
      result.Source = message;
      result.Message = Util.GetStringFromByteArray(message);
      return result;
    }


    /// <summary>
    /// ��������� ��������� �� 3-�� ������ LEVEL3
    /// </summary>
    /// <exception cref="A1Exception">A1Exception</exception>
    /// <param name="command">byte[102] ����� ������� LEVEL3</param>
    /// <returns>CommonDescription</returns>
    private static CommonDescription DecodeLevel3Message(byte[] command)
    {
      if (command.Length != Const.LEVEL3_PACKET_LENGHT)
        throw new A1Exception(String.Format(ErrorText.LENGTH_PACKET,
          "LEVEL3", Const.LEVEL3_PACKET_LENGHT, command.Length));

      byte commandId = command[0];
      ushort messageId = Level4Converter.BytesToUShort(command, Const.LEVEL3_MESSAGEID_POSITION);

      CommonDescription result;
      // ���� ������ �� ������ LEVEL3
      byte[] dataBlock = new byte[Const.LEVEL3_DATA_BLOCK_LENGTH];
      Array.Copy(command, Const.LEVEL3_DATA_BLOCK_POSITION,
        dataBlock, 0, Const.LEVEL3_DATA_BLOCK_LENGTH);

      switch (commandId)
      {
        case (byte)CommandDescriptor.SMSConfigConfirm:
        case (byte)CommandDescriptor.SMSConfigAnswer:
          result = GetSmsAddrConfig(dataBlock);
          break;

        case (byte)CommandDescriptor.PhoneConfigConfirm:
        case (byte)CommandDescriptor.PhoneConfigAnswer:
          result = GetPhoneNumberConfig(dataBlock);
          break;

        case (byte)CommandDescriptor.EventConfigConfirm:
        case (byte)CommandDescriptor.EventConfigAnswer:
          result = GetEventConfig(dataBlock);
          break;

        case (byte)CommandDescriptor.UniqueConfigConfirm:
        case (byte)CommandDescriptor.UniqueConfigAnswer:
          result = GetUniqueConfig(dataBlock);
          break;

        case (byte)CommandDescriptor.IdConfigConfirm:
        case (byte)CommandDescriptor.IdConfigAnswer:
          result = GetIdConfig(dataBlock);
          break;

        case (byte)CommandDescriptor.ZoneConfigConfirm:
          result = GetZoneConfigConfirm(dataBlock);
          break;

        case (byte)CommandDescriptor.DataGpsAnswer:
        case (byte)CommandDescriptor.DataGpsAuto:
          result = GetDataGps(dataBlock);
          break;

        case (byte)CommandDescriptor.GprsBaseConfigConfirm:
        case (byte)CommandDescriptor.GprsBaseConfigAnswer:
          result = GetGprsBaseConfig(dataBlock);
          break;

        case (byte)CommandDescriptor.GprsEmailConfigConfirm:
        case (byte)CommandDescriptor.GprsEmailConfigAnswer:
          result = GetGprsEmailConfig(dataBlock);
          break;

        case (byte)CommandDescriptor.GprsFtpConfigConfirm:
        case (byte)CommandDescriptor.GprsFtpConfigAnswer:
          result = GetGprsFtpConfig(dataBlock);
          break;

        case (byte)CommandDescriptor.GprsSocketConfigConfirm:
        case (byte)CommandDescriptor.GprsSocketConfigAnswer:
          result = GetGprsSocketConfig(dataBlock);
          break;

        case (byte)CommandDescriptor.GprsProviderConfigConfirm:
        case (byte)CommandDescriptor.GprsProviderConfigAnswer:
          result = GetGprsProviderConfig(dataBlock);
          break;

        default:
          throw new A1Exception(String.Format(
            ErrorText.COMMAND_UNKNOWN, commandId));
      }

      result.CommandID = (CommandDescriptor)commandId;
      result.MessageID = messageId;
      return result;
    }

    /// <summary>
    /// ������������� ��������� ����������� SMS ������� 11, 21, 41
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns>SmsAddrConfig ��������� ��� ������� SMS, E-MAIL � 
    /// WEB SERVER (GPRS) ���������.</returns>
    private static SmsAddrConfig GetSmsAddrConfig(byte[] command)
    {
      SmsAddrConfig result = new SmsAddrConfig();
      result.DsptEmailGprs = Level4Converter.BytesToString(command, 0, 30);
      result.DsptEmailSMS = Level4Converter.BytesToString(command, 30, 14);
      result.SmsCentre = Level4Converter.BytesToTelNumber(command, 44, 11);
      result.SmsDspt = Level4Converter.BytesToTelNumber(command, 55, 11);
      result.SmsEmailGate = Level4Converter.BytesToTelNumber(command, 66, 11);
      return result;
    }

    /// <summary>
    /// ������������� ��������� �����������  ���������� ������� 12, 22, 42
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns> ������������ ������� ���������</returns>
    private static PhoneNumberConfig GetPhoneNumberConfig(byte[] command)
    {
      PhoneNumberConfig result = new PhoneNumberConfig();
      result.NumberAccept1 = Level4Converter.BytesToTelNumber(command, 0, 11);
      result.NumberAccept2 = Level4Converter.BytesToTelNumber(command, 11, 11);
      result.NumberAccept3 = Level4Converter.BytesToTelNumber(command, 22, 11);
      result.NumberDspt = Level4Converter.BytesToTelNumber(command, 33, 11);
      result.Name1 = Level4Converter.BytesToString(command, 45, 8);
      result.Name2 = Level4Converter.BytesToString(command, 53, 8);
      result.Name3 = Level4Converter.BytesToString(command, 61, 8);
      result.NumberSOS = Level4Converter.BytesToTelNumber(command, 69, 11);
      return result;
    }

    /// <summary>
    /// ������������� ��������� ����������� ������� 13, 23, 43
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns>������������ �������</returns>
    private static EventConfig GetEventConfig(byte[] command)
    {
      EventConfig result = new EventConfig();
      result.SpeedChange = Convert.ToSByte(command[0]);
      result.CourseBend = Level4Converter.BytesToUShort(command, 1);
      result.Distance1 = Level4Converter.BytesToUShort(command, 3);
      result.Distance2 = Level4Converter.BytesToUShort(command, 5);
      for (int i = 0; i < 32; i++)
        result.EventMask[i] = Level4Converter.BytesToUShort(command, (i << 1) + 7);
      result.MinSpeed = Level4Converter.BytesToUShort(command, 71);
      result.Timer1 = Level4Converter.BytesToUShort(command, 73);
      result.Timer2 = Level4Converter.BytesToUShort(command, 75);
      return result;
    }

    /// <summary>
    /// ������������� ��������� ����������� ���������� ���������� 14, 24, 44
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns>������������ ���������� ����������</returns>
    private static UniqueConfig GetUniqueConfig(byte[] command)
    {
      UniqueConfig result = new UniqueConfig();
      result.DispatcherID = Level4Converter.BytesToString(command, 0, 4);
      result.Password = Level4Converter.BytesToString(command, 4, 8);
      result.TmpPassword = Level4Converter.BytesToString(command, 12, 8);
      return result;
    }

    /// <summary>
    /// ������������� ��������� ����������� ����������������� ���������� 17, 27, 47
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns>������������ ����������������� ����������</returns>
    private static IdConfig GetIdConfig(byte[] command)
    {
      IdConfig result = new IdConfig();
      result.DevIdShort = Level4Converter.BytesToString(command, 0, 4);
      result.DevIdLong = Level4Converter.BytesToString(command, 4, 16);
      result.ModuleIdGps = Level4Converter.BytesToString(command, 20, 4);
      result.ModuleIdGsm = Level4Converter.BytesToString(command, 24, 4);
      //result.ModuleIdMm = Level4Converter.BytesToString(command, 28, 4);
      result.ModuleIdRf = Level4Converter.BytesToString(command, 32, 4);
      result.ModuleIdSs = Level4Converter.BytesToString(command, 36, 4);
      result.VerProtocolLong = Level4Converter.BytesToString(command, 40, 16);
      result.VerProtocolShort = Level4Converter.BytesToString(command, 56, 2);
      return result;
    }

    /// <summary>
    /// ������������� ������������� ��������� ������� ����������� ����������� ��� 25
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns>������ �� ������� ����������� ����������� ���</returns>
    private static ZoneConfigConfirm GetZoneConfigConfirm(byte[] command)
    {
      ZoneConfigConfirm result = new ZoneConfigConfirm();
      result.ZoneMsgID = Level4Converter.BytesToInt(command, 0);
      result.ZoneState = command[4];
      result.Result = command[5];
      return result;
    }

    /// <summary>
    /// ������������� GPS ������ 46, 49
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns>GPS ������</returns>
    private static DataGpsAnswer GetDataGps(byte[] command)
    {
      DataGpsAnswer result = new DataGpsAnswer();
      short index = 0;
      result.WhatWrite = Level4Converter.BytesToUShort(command, index);
      index += 2;

      byte packetCount = command[index];
      index++;

      for (int i = 0; i < packetCount; i++)
      {
        DataGps data = new DataGps();
        if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.TIME))
        {
          data.Time = Level4Converter.BytesToInt(command, index);
          index += 4;
        }
        if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.LATITUDE))
        {
          data.Latitude = Level4Converter.BytesToInt(command, index) * 10;
          index += 4;
        }
        if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.LONGITUDE))
        {
          data.Longitude = Level4Converter.BytesToInt(command, index) * 10;
          index += 4;
        }
        if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.ALTITUDE))
        {
          data.Altitude = command[index];
          index++;
        }
        if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.DIRECTION))
        {
          data.Direction = command[index];
          index++;
        }
        if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.SPEED))
        {
          data.Speed = command[index];
          index++;
        }
        if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.LOGID))
        {
          data.LogID = Level4Converter.BytesToInt(command, index);
          index += 4;
        }
        if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.FLAGS))
        {
          data.Flags = command[index];
          index++;
        }
        if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.EVENTS))
        {
          data.Events = Level4Converter.BytesToUInt(command, index);
          index += 4;
        }
        if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.SENSORS))
        {
          data.Sensor1 = command[index];
          index++;
          data.Sensor2 = command[index];
          index++;
          data.Sensor3 = command[index];
          index++;
          data.Sensor4 = command[index];
          index++;
          data.Sensor5 = command[index];
          index++;
          data.Sensor6 = command[index];
          index++;
          data.Sensor7 = command[index];
          index++;
          data.Sensor8 = command[index];
          index++;
        }
        if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.COUNTERS))
        {
          data.Counter1 = Level4Converter.BytesToUShort(command, index);
          index += 2;
          data.Counter2 = Level4Converter.BytesToUShort(command, index);
          index += 2;
          data.Counter3 = Level4Converter.BytesToUShort(command, index);
          index += 2;
          data.Counter4 = Level4Converter.BytesToUShort(command, index);
          index += 2;
        } // if (Util.IsBitSetInMask)

        // ��������� ����������
        if ((Math.Abs(data.Longitude) > Const.MAX_LONGITUDE) ||
          (Math.Abs(data.Latitude) > Const.MAX_LATITUDE))
          data.Valid = false;
        else
          data.Valid = (data.Flags & 0x08) != 0;

        result.Add(data);
      }

      return result;
    }

    /// <summary>
    /// ������������� ��������� ����������� �������� �������� GPRS 50, 60, 80
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns>�������� ��������� GPRS</returns>
    private static GprsBaseConfig GetGprsBaseConfig(byte[] command)
    {
      GprsBaseConfig result = new GprsBaseConfig();
      result.Mode = Level4Converter.BytesToUShort(command, 0);
      result.ApnServer = Level4Converter.BytesToString(command, 2, 25);
      result.ApnLogin = Level4Converter.BytesToString(command, 27, 10);
      result.ApnPassword = Level4Converter.BytesToString(command, 37, 10);
      result.DnsServer = Level4Converter.BytesToString(command, 47, 16);
      result.DialNumber = Level4Converter.BytesToString(command, 63, 11);
      result.GprsLogin = Level4Converter.BytesToString(command, 74, 10);
      result.GprsPassword = Level4Converter.BytesToString(command, 84, 10);
      return result;
    }

    /// <summary>
    /// ������������� ��������� ����������� �������� Email GPRS 51, 61, 81. 
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns>��������� Email GPRS</returns>
    private static GprsEmailConfig GetGprsEmailConfig(byte[] command)
    {
      GprsEmailConfig result = new GprsEmailConfig();
      result.SmtpServer = Level4Converter.BytesToString(command, 0, 25);
      result.SmtpLogin = Level4Converter.BytesToString(command, 25, 10);
      result.SmtpPassword = Level4Converter.BytesToString(command, 35, 10);
      result.Pop3Server = Level4Converter.BytesToString(command, 45, 25);
      result.Pop3Login = Level4Converter.BytesToString(command, 70, 10);
      result.Pop3Password = Level4Converter.BytesToString(command, 80, 10);
      return result;
    }

    /// <summary>
    /// ������������� ��������� ����������� �������� FTP GPRS 53, 63, 83. 
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns>���������  FTP GPRS</returns>
    private static GprsFtpConfig GetGprsFtpConfig(byte[] command)
    {
      GprsFtpConfig result = new GprsFtpConfig();
      result.Server = Level4Converter.BytesToString(command, 0, 25);
      result.Login = Level4Converter.BytesToString(command, 25, 10);
      result.Password = Level4Converter.BytesToString(command, 35, 10);
      result.ConfigPath = Level4Converter.BytesToString(command, 45, 20);
      result.PutPath = Level4Converter.BytesToString(command, 65, 20);
      return result;
    }

    /// <summary>
    /// ������������� ��������� ����������� �������� Socket GPRS 52, 62, 82. 
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns>���������  Socket GPRS</returns>
    private static GprsSocketConfig GetGprsSocketConfig(byte[] command)
    {
      GprsSocketConfig result = new GprsSocketConfig();
      result.Server = Level4Converter.BytesToString(command, 0, 20);
      result.Port = Level4Converter.BytesToUShort(command, 20);
      return result;
    }

    /// <summary>
    /// ������������� ��������� �������� ����������� � ���������� GPRS 54, 64, 84. 
    /// </summary>
    /// <param name="command">���������</param>
    /// <returns>������������ ����������� � ���������� GPRS</returns>
    private static GprsProviderConfig GetGprsProviderConfig(byte[] command)
    {
      GprsProviderConfig result = new GprsProviderConfig();
      result.InitString = Level4Converter.BytesToString(command, 0, 50);
      result.Domain = Level4Converter.BytesToString(command, 50, 25);
      return result;
    }
  }
}
