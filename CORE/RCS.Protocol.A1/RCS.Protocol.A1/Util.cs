#region Using directives
using RCS.Protocol.A1.Entity;
using System;
using System.Text;
#endregion

namespace RCS.Protocol.A1
{
  /// <summary>
  /// �������� ��������������� ������� 
  /// </summary>
  public static class Util
  {
    /// <summary>
    /// ����� ������ ��������� (������� %%)
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <param name="source">�������� �������� byte[]</param>
    /// <param name="positionBegin">��������� ������� ������ ���������, ����� -1</param>
    /// <returns>true ���� ������� %% ���� �������</returns>
    public static bool FindMessageStart(byte[] source, out int positionBegin)
    {
      if (source == null)
        throw new ArgumentNullException("source", "�� ������� ������ ���� source");

      bool result = false;
      positionBegin = -1;
      for (int i = 0; i < source.Length - 1; i++)
      {
        if ((source[i] == Const.START_INDICATOR_SYMBOL) && (source[i + 1] == Const.START_INDICATOR_SYMBOL))
        {
          result = true;
          positionBegin = i;
          break;
        }
      }
      return result;
    }

    /// <summary>
    /// ������ CRC ��� ������ ��������� LEVEL1.
    /// <para>� ���� �� ��������� ���������� ������. CRC ��������� � 0-�� ����� �� 136-�� 
    /// <para>�������� �� ������������ 7 ����(��� ���� CRC), ��� ���� ��������� 8 ���� ����� ������ �������� �� ���������.
    /// <para>��� ����� ��������� � ������� ��� ��������.</para></para></para>
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� source</exception>
    /// <exception cref="ArgumentOutOfRangeException">�������� startIndex ��� length ����� �� ������� �����������</exception>
    /// <param name="source">byte[] ������� ��� ��������</param>
    /// <param name="startIndex">������, � �������� ���� �������� �������</param>
    /// <param name="length">���-�� �������� ����������� � ��������</param>
    /// <returns>byte CRC</returns>
    internal static byte CalculateLevel1CRC(byte[] source, int startIndex, int length)
    {
      if ((source == null) || (source.Length == 0))
        throw new ArgumentNullException("source", "�� ������� ������ ���� source");
      if ((startIndex < 0) || (startIndex >= source.Length))
        throw new ArgumentOutOfRangeException("startIndex",
          "��� ��������� startIndex ������ ����������� �������: (startIndex >= 0) && (startIndex < source.Length)");

      int finishIndex = startIndex + length;

      if ((length <= 0) || (finishIndex >= source.Length))
        throw new ArgumentOutOfRangeException("length",
          "��� ��������� length ������ ����������� �������: (length > 0) && (startIndex + length < source.Length)");

      int result = 0;
      for (int i = startIndex; i <= finishIndex; i++)
      {
        // ��������� �� ������������ ���� � ������� ������� CRC
        if (i != Const.LEVEL1_CRC_POSITION)
        {
          result += source[i];
          result &= 0xff;
        }
      }
      // ����� �� 4, ����� ��������� � 1 ����
      result >>= 2;
      result &= 0xff;
      return (byte)result;
    }


    /// <summary>
    /// �������������� ������� ���� � Unicode ������ (1 ���� = 1 �������)
    /// </summary>
    /// <param name="source">byte[]</param>
    /// <returns>string</returns>
    public static string GetStringFromByteArray(byte[] source)
    {
      StringBuilder result = new StringBuilder();

      for (int i = 0; i < source.Length; i++)
        result.Append((char)source[i]);

      return result.ToString();
    }

    /// <summary>
    /// ������ � ���� ������� ��������� �������� ������� � ������� startIndex.
    /// <para>����� �������� ������������� �� �������� alignLength, ���� ��� ���� 
    /// ������ �������� alignLength. 
    /// <para>� ������ ���������� ������� �������� �������� alignLength, 
    /// ��� ����� ����� ������� �� �������� alignLength.</para></para>
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������ ���� sourceAttribute ��� destinationCommand</exception>
    /// <exception cref="ArgumentOutOfRangeException">�������� startIndex ��� alignLength ����� �� ������� �����������</exception>
    /// <param name="sourceAttribute">byte[] ������� �������</param>
    /// <param name="destinationCommand">byte[] �������</param>
    /// <param name="startIndex">������ ������ �������� �������� � �������</param>
    /// <param name="alignLength">���-�� ���� ������� �������� ��� �������� ��������</param>
    internal static void FillCommandAttribute(byte[] sourceAttribute, byte[] destinationCommand, int startIndex, int alignLength)
    {
      if (sourceAttribute == null)
        throw new ArgumentNullException("sourceAttribute", "�� ������� ������ ���� sourceAttribute");
      if ((destinationCommand == null) && (destinationCommand.Length == 0))
        throw new ArgumentNullException("destinationCommand", "�� ������� ������ ���� destinationCommand");
      if ((startIndex < 0) || (startIndex >= destinationCommand.Length))
        throw new ArgumentOutOfRangeException("startIndex",
          "��� ��������� startIndex ������ ����������� �������: (startIndex >= 0) && (startIndex < destinationCommand.Length)");
      if ((alignLength <= 0) || (startIndex + alignLength >= destinationCommand.Length))
        throw new ArgumentOutOfRangeException("alignLength",
          "��� ��������� alignLength ������ ����������� �������: (alignLength > 0) && (startIndex + alignLength < destinationCommand.Length)");


      byte[] tmpArray = new byte[alignLength];
      // ������������ ��������
      Array.Copy(sourceAttribute, 0, tmpArray, 0, Math.Min(alignLength, sourceAttribute.Length));
      // ���������� �������
      Array.Copy(tmpArray, 0, destinationCommand, startIndex, alignLength);
    }

    /// <summary>
    /// �������������� ASCII ���� Win-1251 � ������ UNICODE
    /// </summary>
    /// <param name="code">ASCII ���</param>
    /// <returns>UNICODE ������</returns>
    public static char ConvertAsciiWin1251ToChar(byte code)
    {
      char result;
      if (code < 0xC0)
      {
        switch (code)
        {
          case 0xA8:
            result = '�';
            break;
          case 0xB8:
            result = '�';
            break;
          default:
            result = Convert.ToChar(code);
            break;
        }
      }
      else
      {
        result = Convert.ToChar(code + 0x410 - 0xc0); ;
      }
      return result;
    }

    /// <summary>
    /// �������������� UNICODE ������� � 8-�� ������ ASCII ��� Win-1251
    /// <para>����� �, � ����������������� � �,� ��������������, �.�. ��������
    /// ����-�� �� ������������ ���� ���� ��.</para>
    /// </summary>
    /// <param name="symbol">UNICODE ������</param>
    /// <returns>ASCII ���</returns>
    public static byte ConvertCharToAsciiWin1251(char symbol)
    {
      if (symbol == '�')
        symbol = '�';
      else if (symbol == '�')
        symbol = '�';

      byte result;
      if (symbol < 0x100)
      {
        result = Convert.ToByte(symbol);
      }
      else
      {
        if ((symbol >= 0x410) && (symbol <= 0x44f))
        {
          result = (byte)(0xc0 + symbol - 0x410);
        }
        else
        {
          result = Convert.ToByte('?');
        }
      }
      return result;
    }

    /// <summary>
    /// ������������ ����� ����������� ��� �� ������
    /// </summary>
    /// <param name="entryFlag">���� �������� ������ � ����</param>
    /// <param name="exitFlag">���� �������� ������ �� ����</param>
    /// <param name="inFlag">���� �������� ���������� ������ ����</param>
    /// <param name="OutFlag">���� �������� ���������� ������� ����</param>
    /// <returns>����� ����������� ����</returns>
    public static byte GetMaskForZone(byte entryFlag, byte exitFlag,
      byte inFlag, byte OutFlag)
    {
      byte result = 0;

      if (entryFlag != 0)
        result = 1;

      if (exitFlag == 0)
        result &= (1 << 1) ^ 0xFF;
      else
        result |= (1 << 1);

      if (inFlag == 0)
        result &= (1 << 2) ^ 0xFF;
      else
        result |= (1 << 2);

      if (OutFlag == 0)
        result &= (1 << 3) ^ 0xFF;
      else
        result |= (1 << 3);

      return result;
    }

    /// <summary>
    /// ������ ����������� ����� ��� ������������ ����������� ���
    /// </summary>
    /// <param name="zoneConfig">Zone config</param>
    /// <returns>Int ����������� �����</returns>
    internal static int CalculateZoneConfigCRC(ZoneConfig zoneConfig)
    {
      int result = 0;
      foreach (Zone zone in zoneConfig.ZoneList)
      {
        foreach (ZonePoint point in zone.Points)
        {
          result = (result + point.Latitude + point.Longitude) % 32001111;
        }
      }
      return result;
    }

    /// <summary>
    /// �������������� ����-������� DateTime � ������ UnixTime.
    /// </summary>
    /// <param name="time">DateTime"</param>
    /// <returns>����-����� � ������� UnixTime (int)</returns>
    public static int ToUnixTime(DateTime time)
    {
      DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
      TimeSpan diff = time - origin;
      // ������ ���������, ������ ��� � ������� ����-�����
      // ������������ ��� int
      return (int)Math.Floor(diff.TotalSeconds);
    }

    /// <summary>
    /// �������������� ����-������� UnixTime � DateTime.
    /// </summary>
    /// <param name="unixTime">����-����� � ������� UnixTime</param>
    /// <returns>DateTime</returns>
    public static DateTime FromUnixTime(int unixTime)
    {
      DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
      return origin.AddSeconds(unixTime);
    }

    /// <summary>
    /// �������� ���������� �� �������� ��� � �����
    /// </summary>
    /// <param name="mask">ushort �����</param>
    /// <param name="bitIndex">������ ����</param>
    /// <returns>true - �������� ���� � ������� �������� = 1</returns>
    public static bool IsBitSetInMask(ushort mask, byte bitIndex)
    {
      ushort value = (ushort)(1 << bitIndex);
      return (value & mask) != 0 ? true : false;
    }
  }
}
