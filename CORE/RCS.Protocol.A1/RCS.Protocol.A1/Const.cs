namespace RCS.Protocol.A1
{
  /// <summary>
  /// ��������� ������������� � ������ � ���������� �1
  /// </summary>
  public static class Const
  {
    /// <summary>
    /// ������ ������ �� ������ LEVEL0 = 160 ����
    /// </summary>
    public const byte LEVEL0_PACKET_LENGTH = 160;
    /// <summary>
    /// �����  Email = 13 ����
    /// </summary>
    public const byte LEVEL0_EMAIL_LENGTH = 13;
    /// <summary>
    /// ������� ������ �������� ������ �� ������ LEVEL0 - 14�� ����.
    /// ������� %% �������� 14 � 15 �����.
    /// </summary>
    public const byte LEVEL0_START_INDICATOR_POSITION = 14;
    /// <summary>
    /// ������� ����� ��������� ����� ����� ������ �� ������ LEVEL1 = 2-�� ����
    /// </summary>
    public const byte LEVEL1_LENGTH_POSITION = 2;
    /// <summary>
    /// ������� CRC �� ������ LEVEL1 = 7-�� ����
    /// </summary>
    public const byte LEVEL1_CRC_POSITION = 7;
    /// <summary>
    /// ����� ������ �� ������ LEVEL1 = 144 ����
    /// </summary>
    public const byte LEVEL1_PACKET_LENGHT = 144;
    /// <summary>
    /// ����� ����� ������ �� ������ LEVEL1 = 136 ����
    /// </summary>
    public const byte LEVEL1_DATA_BLOCK_LENGHT = 136;
    /// <summary>
    /// ����� ������ �� ������ LEVEL3 = 102 �����
    /// </summary>
    public const byte LEVEL3_PACKET_LENGHT = 102;
    /// <summary>
    /// ������� ������ MessageID �� ������ LEVEL3 = 1-�� ����
    /// </summary>
    public const byte LEVEL3_MESSAGEID_POSITION = 1;
    /// <summary>
    /// ������� ������ ����� ������ �� ������ LEVEL3 = 3-�� ����
    /// </summary>
    public const byte LEVEL3_DATA_BLOCK_POSITION = 3;
    /// <summary>
    /// ����� ����� ������ �� ������ LEVEL3 = 99 ����
    /// </summary>
    public const byte LEVEL3_DATA_BLOCK_LENGTH = 99;
    /// <summary>
    /// ������ � ����� 0
    /// </summary>
    public const char ZERO_SYMBOL = (char)0;
    /// <summary>
    /// ��������� ������. 
    /// ������� ������ ������ ������� �� ���� ��������� �������� %%
    /// </summary>
    public const char START_INDICATOR_SYMBOL = '%';


    /// <summary>
    /// ������������ ���������� �������� ������� = 108000000 (180)
    /// </summary>
    public const int MAX_LONGITUDE = 108000000;
    /// <summary>
    /// ������������ ���������� �������� ������ = 54000000 (90)
    /// </summary>
    public const int MAX_LATITUDE = 54000000;
  }
}
