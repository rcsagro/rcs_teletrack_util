using System;
using System.Net;
using System.Net.Sockets;
using RCS.Proxy;
using RCS.Proxy.Client;
using RCS.Sockets.Connection;

namespace RCS.Sockets.Clnt
{
    public class ClientSocket : SocketDecorator
    {
        /// <summary>
        /// ������������ ��� �������
        /// </summary>
        public ConnectionInfo Cl_Connection
        {
            get { return cl_connection; }
        }

        private volatile ConnectionInfo cl_connection;

        string IP = "";

        /// <summary>
        /// ������������� ���������� � �������� (������������ ��� �������).
        /// </summary>
        /// <exception cref="ApplicationException">���������� � �������� ��� ����������</exception>
        /// <param name="address">����� �������.</param>
        /// <param name="port">���� �������.</param>
        /// <returns>ConnectionInfo</returns>
        public ConnectionInfo SetupClientConnect(string address, int port)
        {
            ValidateEstablishedConnection(address, port);

            ConnectionInfo return_value = null;

            // aketner
            //throw new Exception( "++++++ adress: " + address + " : port: " + port.ToString() );

            IPEndPoint serverEndpoint = new IPEndPoint(IPAddress.Parse(address), port);
            IP = address;

            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(serverEndpoint);

            CreateConnectionInfo(socket);

            return_value = cl_connection;

            return cl_connection;
        }

        /// <summary>
        /// ������������� ���������� � �������� ����� Proxy.
        /// </summary>
        /// <exception cref="ApplicationException">���������� � �������� ��� ����������</exception>
        /// <param name="proxyCfg">��������� ����������� � Proxy-�������.</param>
        /// <param name="proxyType">��� ������ �������.</param>
        /// <param name="address">����� �������.</param>
        /// <param name="port">���� �������.</param>
        /// <returns>ConnectionInfo</returns>
        public ConnectionInfo SetupClientConnectionViaProxy(
            ProxyCfg proxyCfg, ProxyType proxyType, string address, int port)
        {
            ValidateEstablishedConnection(address, port);

            ConnectionInfo return_value = null;

            CreateConnectionInfo(new ProxyClientFactory().CreateProxyClient(proxyCfg, proxyType)
                .ConnectToHost(address, port).Client);

            return_value = cl_connection;

            return cl_connection;
        }

        private void CreateConnectionInfo(Socket socket)
        {
            cl_connection = new ConnectionInfo();
            cl_connection.socket = socket;
            cl_connection.socket.BeginReceive(
                cl_connection.IncomMsgBuff,
                0,
                cl_connection.IncomMsgBuff.Length,
                SocketFlags.None,
                new AsyncCallback(ReceiveCallback),
                cl_connection);
        }

        private void ValidateEstablishedConnection(string address, int port)
        {
            if (SocketConnected(cl_connection))
            {
                throw new ApplicationException(String.Format(
                    "������� ���������� ����������. ���������� � �������� {0}:{1} ��� ����������.",
                    address, port));
            }
        }

        /// <summary>
        /// ������ ���������� � ��������
        /// </summary>
        public void CloseClient()
        {
            if (SocketConnected(cl_connection))
            {
                cl_connection.socket.Shutdown(SocketShutdown.Both);
                cl_connection.socket.Close();
                //�������: ������ ����������
                OnConnectedStateMsg(false,
                    String.Format("CloseClient: ������ ���������� � ��������. �����: {0}", DateTime.Now),
                    cl_connection);
            }
            else
            {
                OnConnectedStateMsg(false,
                    String.Format("CloseClient: C��������� � �������� �����������. �����: {0}", DateTime.Now),
                    cl_connection);
            }
        }
    }
}
