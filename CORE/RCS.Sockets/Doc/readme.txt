������� ����������, ������������ ������
���� �� ����� ������������ .NET Framework 2.0 � ��������� ������� �������� ����������� ������� ��������. ��� ����������� �������� ������� ����������, ���������� � ��������, �� ��������� � ������ ���������.
� ������� System.Diagnostics ����������������� ����� �� ������ ������� ����������� � ������������ ���� System.Net �������� �������������� ������ (trace source), �� ������� �� ������ ������� (� ����� ���������� ���������� ������ �System.Net�, �System.Net.Sockets� � �System.Net.Cache�). � ����� ������ �� ����� ������������ ���� System.Net.Sockets. ������ ����������������� ����� ���������� ������� � �������� 5.

������� 5. ���������������� ����
<configuration>
  <system.diagnostics>
    <sources>
      <source name="System.Net.Sockets">
        <listeners>
          <add name="Sockets"/>
        </listeners>
      </source>
    </sources>
    <switches>
      <add name="System.Net.Sockets" value="31" />
    </switches>
    <sharedListeners>
    <add name="Sockets" type=
      "System.Diagnostics.TextWriterTraceListener"
      initializeData="Sockets.log"/>
    </sharedListeners>
    <trace autoflush="true" />
  </system.diagnostics>
</configuration>

������� ����������� ������� ��������, �� ������� �������� ����� ���������� ���������� � ����� ����������, ���������� � ��������. ������� value ��� ������� ����������� �� ������ System.Net � ��� ������� �����, ������ ��� ������� ������������ ������ ������� ����������������. ���� ���� �������: ������� ������ (0x10), ���� � ������ � ����� �� ��� (0x8), �������������� (0x4), ������ (0x2) � ����������� ������� (0x1). � ������� � �������� 5 �� ������������ ���, ��� ���������� ��������������� �������� 31. ����������� ����� � ������ � ������ �� ��� � ����� �������� ��������, ����������� ��������, ����� �� ��������� ������ ������ �� ������������ ���� System.Net. ��� �� ������ �������� ���������� ���������, �� � ��������� ����� ������ ���������� ������ API. ���� ����� ���������� ��������, ��� ����� ����� ���������������� � �������. ���� ��� ������ ������� ����������� ����� � ������ �� ������������ ������ BeginAccept; ��������, ��� �� ����� ������� ����� ���������� ����, ��� ����� ��� ������ � API. ��� ������, ����� BeginAccept ������ �������� AcceptOverlappedAsyncResult, � ������������� ������� � ������ ����� ������ ����� 48209832.
System.Net.Sockets Information: 0 :
 Socket#48285313::BeginAccept()
System.Net.Sockets Information: 0 :
 Socket#59817589::Socket(InterNetwork#2)
System.Net.Sockets Information: 0 :
 Exiting Socket#59817589::Socket()
System.Net.Sockets Information: 0 :
 Exiting Socket#48285313::BeginAccept()
-> AcceptOverlappedAsyncResult#48209832

������, ��� �� ��� ������ ������������ ������� �����������. ���, � ������� �� ������ Bind ����� Select � �������� ����������� �� ����������. ���������� � ���, ����� ������ ������������ �����������, ��. � ������������ (EN).
������ ������ �������� ������������ �������� ������ �������� �������. �� ����� �����: ��� ���� ������ ���������� ��� ���������� ������. �������, ��� � ������ ������������ ������ ����� ������� (��� ���� ������������ �������� ������������ ����� ������), ��� ����� ������� � �����������. ��������, ���� ���� � 512-������� ������ �������� ������ ��������� ������ ���� ����, � ������� �� ������� ���������� ����� ������. ����� �������������� � ���, ��� �����, �� ������� �� ��������, ����� �����, ��������� ��������, ������������ ������� �������� ��� ������ ������. ��������� �������������� ������ ���� ������������� �����������, ����������� ���� ���� ������ (������ �a�) ����� ����� 60504909:

System.Net.Sockets Information: 0 : Socket#60504909::Send()
System.Net.Sockets Verbose: 0 : 00000000 : 61 : a
System.Net.Sockets Information: 0 :
 Exiting Socket#60504909::Send()     ->
1#1

��� ������ ������ ���������������� (��������������, ������ � ����������� �������) ��������� �������� ���������� � ��������� ��������� � API. ��� ������ �� ���� ����� ���������������� � ������ ������������ ������� ���������������� � ��������� �� ������. ��������, ��� ��������� ������ Socket.GetSocketOptions ������ ����� ��������� ���:

System.Net.Sockets Error: 0 : Exception in the
Socket#48285313::GetSocketOption - An unknown, invalid, or
unsupported option or level was specified in a getsockopt
or setsockopt call


1. ���������� dll (References)
��� �������� � ������ � ��������� using TransitServisLib;

2. ��������� ������

ctrl = new ControlClass();//����������� ����� ��� ������ � dll
��� �������: 

        	      
	//���������� � �������� (�����,����)   
	ConnectionInfo cn = ctrl.ConnectToServer("SRVAVS", 30000);
        

//******************************************************************
��� �������:

	
	ctrl.ServerStart("",30000);

//******************************************************************
3. �������� ������:
	//ConnectionInfo cn - ������ ����������
	//������ �������� �� ConnectToServer("SRVAVS", 30000);
	//������ ����� �������� ��������� ������
	

	// ������ ��� ��������
	byte [] bytes = new byte[1] {5};
	//�������� ������    
	if (cn != null)
		int num_pack = ctrl.SendData(cn, bytes); //return ���������� ����� ������������� ������ 

//****************************************************************************************************
�������:
	//��������� ����������
	ctrl.�onnectedStateMsg += new ConnectedStateEventHandler(Ctrl_conn_state);
        void Ctrl_conn_state(string str)
        {
            WriteOutput(str);
        }
	//������� �����
	ctrl.unpack_data += new ControlClass.DisassemblePacketEventHandler(Ctrl_unpack_data);
        
	void Ctrl_unpack_data(ConnectionInfo conn)
        {	
		byte[] data = conn.QUnpackedMsg; //���������� ������
	}

	//������������ ������������ ������
	ctrl.�onfirm_packet += new ControlClass.�onfirmPacketEventHandler(Ctrl_�onfirm_packet);
	void Ctrl_�onfirm_packet(int id_packet_in)
	{
		id_packet_in //���������� ����� ������, ������������� �� ���������� ���� 
	}
	
	//������
	ctrl.error += new ControlClass.ErrorEventHandler(Ctrl_error);
        void Ctrl_error(object sender, MessageErrorEventArgs e)
        {
		string.Format("����������: {0} {1}, {2}", e.Message, e.IPaddress, e.DTime.ToLocalTime();
	}
//******************************************************************************************************
�������� �������:

��� �������:
ServerStop();

��� �������
ClientDisconnect();

