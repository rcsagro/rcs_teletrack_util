#region Using directives
using System;
using System.Net.Sockets;
using RCS.Sockets.Connection;
using RW_Log;
using TransitServisLib;

#endregion

namespace RCS.Sockets
{
    /// <summary>
    /// ����� ��� ������ � ��������.
    /// ������������� ������ ��� ������ �� ������ �������.
    /// </summary>
    public class SocketDecorator
    {
        /// <summary>
        /// ����� �������
        /// </summary>
        protected Socket socket;

        /// <summary>
        /// ������ ����������� ������ ������ ������� ������
        /// </summary>
        public int SockReceiveBufferSize
        {
            get { return sockReceiveBufferSize; }
            set { sockReceiveBufferSize = value; }
        }

        private int sockReceiveBufferSize = 1024;

        #region �������

        /// <summary>
        /// ������� IncomMessage
        /// </summary>
        public event IncomingMessageEventHandler IncomMessage;

        /// <summary>
        /// OutMessage
        /// </summary>
        public event OutMessageEventHandler OutMessage;

        /// <summary>
        /// MessageError
        /// </summary>
        public event MessageErrorEventHandler messageError;

        /// <summary>
        /// �onnectedStateMsg
        /// </summary>
        public event ConnectedStateEventHandler �onnectedStateMsg;

        /// <summary>
        /// ��������� ������� "����� ������" 
        /// </summary>
        /// <param name="e">��������� ������ � ������� byte [] ��������� ������</param>
        protected void OnIncomMessage(ConnectionInfo e)
        {
            if (IncomMessage != null)
            {
                IncomMessage(this, e);
            }
        }

        /// <summary>
        /// ������� - ��������� �����
        /// </summary>
        /// <param name="ip_address">����</param>
        /// <param name="sendMsg">���</param>
        protected void OnOutMessage(string ip_address, byte[] sendMsg)
        {
            if (OutMessage != null)
            {
                OutMessage(ip_address, sendMsg);
            }
        }

        /// <summary>
        /// ������� - ������
        /// </summary>
        /// <param name="e">����� �������� ������</param>
        protected void OnMessageError(MessageErrorEventArgs e)
        {
            if (messageError != null)
            {
                messageError(this, e);
            }
        }

        /// <summary>
        /// ������� - ������ ����������
        /// </summary>
        /// <param name="state"></param>
        /// <param name="msg">��������</param>
        /// <param name="conn">ConnectionInfo</param>
        protected void OnConnectedStateMsg(bool state, string msg, ConnectionInfo conn)
        {
            if (�onnectedStateMsg != null)
            {
                ConnectedStateEvArg con_state = new ConnectedStateEvArg();
                con_state.B_State = state;
                con_state.Message = msg;
                con_state.conn = conn;
                �onnectedStateMsg(con_state);
            }
        }

        #endregion

        protected static bool SocketConnected(ConnectionInfo connection)
        {
            return (connection != null) && (connection.socket != null) &&
                   (connection.socket.Connected);
        }

        /// <summary>
        /// ������������� �������� �������� �������� ������
        /// tm - ����� �������� ����� socket.Close()
        /// </summary>
        /// <param name="isOn">���/����. ��������</param>
        /// <param name="tm">����� ��������</param>
        /// <returns>���������</returns>
        public bool LingerOptionsOn(bool isOn, int tm)
        {
            if (socket == null)
            {
                return false;
            }
            else
            {
                LingerOption lingerOption = new LingerOption(isOn, tm);
                socket.SetSocketOption(
                    SocketOptionLevel.Socket,
                    SocketOptionName.Linger,
                    lingerOption);
            }
            return true;
        }

        /// <summary>
        /// ��������� ������
        /// </summary>
        /// <param name="result">IAsyncResult</param>
        protected void ReceiveCallback(IAsyncResult result)
        {
            ConnectionInfo connection = (ConnectionInfo) result.AsyncState;
            try
            {
                if (SocketConnected(connection))
                {
                    int num_read = connection.socket.EndReceive(result);
                    if (num_read != 0)
                    {
                        byte[] receive_packet = new byte[num_read];

                        Array.Copy(connection.IncomMsgBuff, receive_packet, num_read);

                        connection.EnQueueIncomPack(receive_packet);

                        //�������: �������� �����
                        OnIncomMessage(connection);
                        //������� ����� ������ 
                        Array.Clear(connection.IncomMsgBuff, 0, connection.IncomMsgBuff.Length);

                        connection.socket.BeginReceive(
                            connection.IncomMsgBuff,
                            0,
                            connection.IncomMsgBuff.Length,
                            SocketFlags.None,
                            new AsyncCallback(ReceiveCallback),
                            connection);
                    }
                    else
                    {
                        CloseSocket(connection);
                    }
                }
            }
            catch (SocketException socketException)
            {
                //WSAECONNRESET, the other side closed impolitely
                if (socketException.ErrorCode == 10054)
                {
                    CloseSocket(connection);
                }

                SendErrMessage(connection, socketException);
                //������ �������� ����������
            }
            catch (Exception exception)
            {
                CloseSocket(connection);
                SendErrMessage(connection, exception);
            }
        }

        /// <summary>
        /// �������� ������ 
        /// </summary>
        /// <param name="connection">ConnectionInfo</param>
        /// <param name="byteData">byte[]</param>
        public void Send(ConnectionInfo connection, byte[] byteData)
        {
            try
            {
                if (connection.socket != null)
                {
                    if (connection.socket.Connected)
                    {
                        connection.socket.Send(byteData);
                    }
                    else
                    {
                        ExecuteLogging.Log("BPLevelTT", String.Format(
                            "����� SocketCLClass.Send: ����� ��� ������. HashCode: {0}.",
                            connection.socket.GetHashCode()));

                        CloseSocket(connection);
                    }
                }
            }
            catch (Exception exception)
            {
                CloseSocket(connection);
                SendErrMessage(connection, exception);
            }
        }

        /// <summary>
        /// ����� ��������� ������ Send
        /// </summary>
        /// <param name="ar">IAsyncResult ar</param>
        protected void SendCallback(IAsyncResult ar)
        {
            ConnectionInfo connection = (ConnectionInfo) ar.AsyncState;
            if (SocketConnected(connection)) //���������
            {
                try
                {
                    connection.socket.EndSend(ar);
                }
                catch (Exception exception)
                {
                    CloseSocket(connection);
                    SendErrMessage(connection, exception);
                }
            }
            else
            {
                CloseSocket(connection);
            }
        }

        /// <summary>
        /// Close Socket
        /// </summary>
        /// <param name="connection">ConnectionInfo</param>
        public void CloseSocket(ConnectionInfo connection)
        {
            try
            {
                if (SocketConnected(connection))
                {
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "  ��������� socket. LocalEndPoint: {0}; RemoteEndPoint: {1}; HashCode: {2}",
                        connection.socket.LocalEndPoint,
                        connection.socket.RemoteEndPoint,
                        connection.socket.GetHashCode()));

                    connection.socket.Shutdown(SocketShutdown.Both);
                    connection.socket.Close();
                }
            }
            catch (SocketException socketException)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "  ���� ��� �������� socket socketException: {0}",
                    socketException.Message));
            }
            catch (Exception exception)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "  ���� ��� �������� socket exception: {0}",
                    exception.Message));
            }
            OnConnectedStateMsg(false, "������ ���������� ", connection);
        }

        /// <summary>
        /// ������� ����� �������� ������
        /// </summary>
        /// <param name="cn">ConnectionInfo</param>
        /// <param name="e">Exception</param>
        public void SendErrMessage(ConnectionInfo cn, Exception e)
        {
            SocketException se;
            MessageErrorEventArgs msg_except = new MessageErrorEventArgs();
            if (e is SocketException)
            {
                se = (SocketException) e;
                msg_except.ErrorCode = se.ErrorCode;
            }
            if ((cn != null) && (cn.RemoteAddress != null))
            {
                msg_except.Message = String.Format("{0}\r\n {1}\r\n Remote Address: {2}",
                    e.Message, e.StackTrace, cn.RemoteAddress);
                msg_except.IPaddress = cn.RemoteAddress.IpAddress;
            }
            else
            {
                msg_except.Message = String.Format("{0}\r\n {1}", e.Message, e.StackTrace);
            }
            msg_except.DTime = DateTime.Now;

            OnMessageError(msg_except);
        }

        /// <summary>
        /// ����������� �������� �� ����� ������� �������� ������.
        /// </summary>
        /// <param name="socket"></param>
        /// <returns></returns>
        public static bool IsRealConnected(Socket socket)
        {
            bool blockingState = socket.Blocking;
            bool result = false;
            try
            {
                byte[] tmp = new byte[1];

                socket.Blocking = false;
                socket.Send(tmp, 0, 0);
                socket.Receive(tmp, 1, 0);
                result = true;
            }
            catch (SocketException ex)
            {
                if (ex.NativeErrorCode.Equals(10035))
                {
                    // 10035 WSAEWOULDBLOCK Resource temporarily unavailable.
                    // This error is returned from operations on nonblocking sockets 
                    // that cannot be completed immediately, for example recv 
                    // when no data is queued to be read from the socket. 
                    // It is a nonfatal error, and the operation should be retried later.
                    result = true;
                }
            }
            finally
            {
                socket.Blocking = blockingState;
            }
            return result;
        }
    }
}
