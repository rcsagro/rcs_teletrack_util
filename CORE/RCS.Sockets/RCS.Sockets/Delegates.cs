using RCS.Sockets.Connection;

namespace RCS.Sockets
{
  /// <summary>
  /// ������� - ������ ����������
  /// </summary>
  /// <param name="str">��������</param>
  public delegate void ConnectedStateEventHandler(ConnectedStateEvArg con_state);

  /// <summary>
  /// ������� - ������ ����� (��� �������)
  /// </summary>
  /// <param name="obj">this</param>
  /// <param name="e">��������� ������ � ������� byte [] ��������� ������</param>
  public delegate void IncomingMessageEventHandler(object obj, ConnectionInfo e);

  /// <summary>
  /// ������� - ��������� �����
  /// </summary>
  /// <param name="ip_address">����</param>
  /// <param name="sendMsg">���</param>
  public delegate void OutMessageEventHandler(string ip_address, byte[] sendMsg);
}
