using System;

namespace RCS.Sockets.Connection
{
  /// <summary>
  /// ��������� ����� ����� -IP ����� � ����.
  /// </summary>
  public class HostAddress
  {
    /// <summary>
    /// IP �����.
    /// </summary>
    public string IpAddress
    {
      get { return _ipAddress; }
    }
    private string _ipAddress;

    /// <summary>
    /// ����.
    /// </summary>
    public int Port
    {
      get { return _port; }
    }
    private int _port;

    /// <summary>
    /// �����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">IP-����� �� ����� ���� ������.</exception>
    /// <exception cref="ArgumentOutOfRangeException">�������� ����� ������ ����
    /// � ��������� �� 0 �� 65535.</exception>
    /// <param name="ipAddress">IP �����.</param>
    /// <param name="port">����.</param>
    public HostAddress(string ipAddress, int port)
    {
      if (String.IsNullOrEmpty(ipAddress))
      {
        throw new ArgumentNullException("ipAddress", "IP-����� �� ����� ���� ������.");
      }
      if ((port <= 0) && (port > 65535))
      {
        throw new ArgumentOutOfRangeException("port", port,
          "�������� ����� ������ ���� � ��������� �� 0 �� 65535.");
      }

      _ipAddress = ipAddress;
      _port = port;
    }

    private HostAddress()
    {
    }

    /// <summary>
    /// ����� � ������� IP:port.
    /// </summary>
    /// <returns>�����.</returns>
    public override string ToString()
    {
      return String.Format("{0}:{1}", _ipAddress, _port);
    }
  }
}
