namespace RCS.Sockets.Connection
{
  public class ConnectedStateEvArg
  {
    public string Message
    {
      set { message = value; }
      get { return message; }
    }
    string message;

    public bool B_State
    {
      set { b_state = value; }
      get { return b_state; }
    }
    bool b_state;

    public ConnectionInfo conn;
  }
}
