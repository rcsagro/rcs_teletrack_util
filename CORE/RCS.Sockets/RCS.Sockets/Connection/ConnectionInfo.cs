#region Using directives
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
#endregion

namespace RCS.Sockets.Connection
{
    /// <summary>
    /// ����� ������� ����������
    /// </summary>
    public class ConnectionInfo
    {
        /// <summary>
        /// ����� ��������� �������� �������
        /// </summary>
        public IncompletePacket incompl_pack = new IncompletePacket();

        /// <summary>
        /// ������ ������ ��������� ������
        /// </summary>
        internal volatile byte[] remainderOfPacket = new byte[0];

        /// <summary>
        /// ������� ����������
        /// </summary>
        public Socket socket
        {
            get { return _socket; }
            set
            {
                _socket = value;
                if ((_remouteAddress == null) && (_socket != null) && (_socket.Connected))
                {
                    _remouteAddress = new HostAddress(
                        ((IPEndPoint) socket.RemoteEndPoint).Address.ToString(),
                        ((IPEndPoint) socket.RemoteEndPoint).Port);
                }
            }
        }

        private volatile Socket _socket;

        /// <summary>
        /// ����� ���������� ����������.
        /// </summary>
        public HostAddress RemoteAddress
        {
            get { return _remouteAddress; }
        }

        private HostAddress _remouteAddress;

        #region ������

        /// <summary>
        /// locker
        /// </summary>
        private object locker = new object();

        private object obj = new object();

        #endregion

        /// <summary>
        /// ������ ������ ������.
        /// </summary> 
        private static int BufferSize = 8192; //409600;//8388608;//32767;

        /// <summary>
        /// ����� ������ ������
        /// </summary>
        /// <returns></returns>
        public byte[] IncomMsgBuff
        {
            get { return incomingMessage; }
            set { incomingMessage = value; }
        }

        protected volatile byte[] incomingMessage = new byte[BufferSize];


        /// <summary>
        /// ������� ����������� ������� �� ���������
        /// </summary>
        private Queue<byte[]> queueIncomPack = new Queue<byte[]>();

        /// <summary>
        /// �������� ��� ������� � ������� ����������� �������
        /// </summary>
        /// <returns></returns>
        public byte[] DeQueueIncomPack()
        {
            lock (locker)
            {
                return queueIncomPack.Count != 0 ? queueIncomPack.Dequeue() : null;
            }
        }

        public void EnQueueIncomPack(byte[] bytes)
        {
            lock (locker)
            {
                queueIncomPack.Enqueue(bytes);
            }
        }

        /// <summary>
        /// ������� ����������� ������������� ������� 
        /// </summary>
        private Queue<byte[]> qUnpackedMsg = new Queue<byte[]>();

        /// <summary>
        /// �������� ��� ������� � ������� ������������� ������
        /// </summary>
        /// <returns></returns>
        public byte[] DeQUnpackedMsg()
        {
            lock (obj)
            {
                return qUnpackedMsg.Count != 0 ? qUnpackedMsg.Dequeue() : null;
            }
        }

        /// <summary>
        /// En q unpacked message
        /// </summary>
        /// <param name="bytes">Bytes</param>
        public void EnQUnpackedMsg(byte[] bytes)
        {
            lock (obj)
            {
                qUnpackedMsg.Enqueue(bytes);
            }
        }

        /// <summary>
        /// ��������� ����� ��������� ���������� ������
        /// </summary>
        public DateTime TimeIncomLastPack
        {
            get { return timeIncomLastPack; }
            set { timeIncomLastPack = value; }
        }

        private DateTime timeIncomLastPack = DateTime.Now;
    }
}
