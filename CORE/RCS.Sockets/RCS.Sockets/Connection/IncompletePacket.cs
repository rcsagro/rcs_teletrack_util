using System;

namespace RCS.Sockets.Connection
{
  /// <summary>
  /// ����� ����� ������� �� ������
  /// </summary>
  public class IncompletePacket
  {
    /// <summary>
    /// ������ ������ ��������� ������
    /// </summary>
    protected byte[] remainderOfPacket = new byte[0];

    /// <summary>
    /// ������������ ������ ������ ����� ������� 
    /// </summary>
    protected static int max_size = 10000000;

    /// <summary>
    /// ������ ������
    /// </summary>
    /// <returns>Int</returns>
    public int Len
    {
      get { return remainderOfPacket.Length; }
    }

    /// <summary>
    /// �������� ������� � ������
    /// </summary>
    private byte[] RemOfPac
    {
      get { return remainderOfPacket; }
      set { remainderOfPacket = value; }
    }

    /// <summary>
    /// ��������� ������ ������, 
    /// ���������� �� ���������� ��������
    /// </summary>
    /// <param name="pack">Pack</param>
    public byte[] AddHeadOfPacket(byte[] pack)
    {
      byte[] bytes;
      if (pack.Length < max_size)
      {
        bytes = new byte[Len + pack.Length];
        Array.Copy(RemOfPac, 0, bytes, 0, Len);
        Array.Copy(pack, 0, bytes, Len, pack.Length);
      }
      else
      {
        bytes = new byte[0];
      }
      CleanupRemeinderBuf();
      return bytes;
    }

    /// <summary>
    /// ������� ����� �� ��������� �������� �������
    /// ������������� ������� ������
    /// </summary>
    protected void CleanupRemeinderBuf()
    {
      Array.Resize(ref remainderOfPacket, 0);
    }

    /// <summary>
    /// ����� � ����� �������� �����
    /// </summary>
    public void PutBufIncompletePack(byte[] data, int pos)
    {
      CleanupRemeinderBuf();
      Array.Resize(ref remainderOfPacket, data.Length - pos);
      Array.Copy(data, pos, remainderOfPacket, 0, Len);
    }
  }
}
