using System;
using System.Net;
using System.Net.Sockets;
using RCS.Sockets.Connection;
using RW_Log;

namespace RCS.Sockets
{
    public class ServerSocket : SocketDecorator
    {
        private IAsyncResult iptr;

        /// <summary>
        /// ���� ����������� ������ �������� ���������� �� ������ �������
        /// </summary>
        public bool bEndOfAcceptConn = false;

        /// <summary>
        /// ������������� �������� �������� "�����������" ����������
        /// </summary>
        public bool KeepAliveOn()
        {
            ExecuteLogging.Log("BPLevelTT", "KeepAliveOn. ������������� �������� �������� ����������� ����������");

            if (socket == null)
            {
                return false;
            }
            else
            {
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
            }

            return true;
        }

        /// <summary>
        /// ������ ������������ ������������� �����
        /// </summary>
        /// <param name="t"></param>
        public void AsynchronousAcceptConnections(Type t)
        {
            try
            {
                ExecuteLogging.Log("BPLevelTT", "AsynchronousAcceptConnections. ������ ������������ ������������� �����");

                iptr = socket.BeginAccept(new AsyncCallback(AsyncAcceptCallback), t);
            }
            catch (Exception e)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format("{0} \r\n {1}", e.Message, e.StackTrace));
            }
        }

        /// <summary>
        /// �������� � �������� ������
        /// </summary>
        /// <param name="ip_address"></param>
        /// <param name="port"></param>
        /// <returns>�� - ���</returns>
        public bool SetupServerSocket(string ip_address, int port)
        {
            bool return_value = false;

            try
            {
                ExecuteLogging.Log("BPLevelTT", "SetupServerSocket. �������� � �������� ������: " + ip_address + ":" + port);

                //���� ���������� ��� �������� ������...
                IPAddress adr = IPAddress.Parse(ip_address);
                IPEndPoint myEndpoint = new IPEndPoint(adr, port);
                //�������� ������...
                socket = new Socket(
                    myEndpoint.Address.AddressFamily,
                    SocketType.Stream,
                    ProtocolType.Tcp);

                //�������� ������
                socket.Bind(myEndpoint);
                //������������ ��� ������ ���������� �������
                socket.Listen(1000);

                return_value = true; //���� ����� - ������� ����������
            }
            catch (Exception exception)
            {
                SendErrMessage(null, exception);
            }

            return return_value;
        }

        /// <summary>
        /// ������ ������������ ������������� �����
        /// </summary>
        public void AsynchronousAcceptConnections()
        {
            try
            {
                socket.BeginAccept(new AsyncCallback(AsyncAcceptCallback), null);
            }
            catch (Exception exception)
            {
                SendErrMessage(null, exception);
            }
        }

        /// <summary>
        /// ��������� ����������, ������ BeginReceive �� �������� ������ ����������
        /// </summary>
        /// <param name="result">IAsyncResult</param>
        public void AsyncAcceptCallback(IAsyncResult result)
        {
            ExecuteLogging.Log("BPLevelTT", "AsyncAcceptCallback. ��������� ����������, ������ BeginReceive �� �������� ������ ����������");

            Type type = (Type) result.AsyncState;
            ConnectionInfo connection = null;

            try
            {
                //����� ������ ����������
                object obj = Activator.CreateInstance(type);

                connection = (ConnectionInfo) obj;
                connection.socket = socket.EndAccept(result);

                ExecuteLogging.Log("BPLevelTT", String.Format(">>> AsyncAcceptCallback. ������� ���������� socket {0}(HashCode:{1}), ��������� �����: {2}.",
                    connection.socket.LocalEndPoint,
                    connection.socket.GetHashCode(),
                    connection.socket.RemoteEndPoint));

                connection.socket.BeginReceive(connection.IncomMsgBuff, 0,
                    connection.IncomMsgBuff.Length, SocketFlags.None,
                    new AsyncCallback(ReceiveCallback), connection);
            }
                #region Exception

            catch (ObjectDisposedException objSocketException)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "AsyncAcceptCallback. ������-socket ������� ������. {0} {1}",
                    objSocketException.Message,
                    objSocketException.StackTrace));
            }
            catch (SocketException socketException)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "AsyncAcceptCallback. ����������� ������ �� ����� ������ �������� ���������� \r\n{0} {1} {2}\r\n",
                    socketException.Message,
                    socketException.StackTrace,
                    socketException.ErrorCode));
                ExecuteLogging.Log("BPLevelTT", "AsyncAcceptCallbac. k��������� ������ ������ �������� ����������....");

                if (SocketConnected(connection))
                {
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "AsyncAcceptCallback  ��������� socket: {0}(HashCode:{1})",
                        connection.socket.LocalEndPoint,
                        connection.socket.GetHashCode()));

                    connection.socket.Shutdown(SocketShutdown.Both);
                    connection.socket.Close();
                }
            }
            catch (Exception e)
            {
                if (!bEndOfAcceptConn)
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "AsyncAcceptCallback ����������� ������ �� ����� ������ �������� ���������� \r\n{0} {1}",
                        e.Message, e.StackTrace));
            }
                #endregion

            finally
            {
                if (!bEndOfAcceptConn)
                {
                    //����������� ������ �� ������������� ������ �������
                    ExecuteLogging.Log("BPLevelTT", "AsyncAcceptCallback ����������� ������ �� ������������� ������ �������");
                    socket.BeginAccept(new AsyncCallback(AsyncAcceptCallback), type);
                }
                else
                {
                    ExecuteLogging.Log("BPLevelTT", "AsyncAcceptCallback ��������� ������ ������ �������� ����������....");
                }
            } 
        }

        /// <summary>
        /// ��������� ����� �������. 
        /// ������������ ������ � DirectOnline.
        /// </summary>
        public void CloseServer()
        {
            if (socket != null)
            {
                //Shutdown �� ����� - ��������.
                socket.Close();
                socket = null;

                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "CloseServer() ����� ������� ������. �����: {0}",
                    System.Threading.Thread.CurrentThread.GetHashCode()));
            }
        }
    }
}
