namespace RCS.SrvKernel.ErrorHandling
{
  /// <summary>
  /// ����������� ����� ���������� ��������� ����������� ���������� ����������
  /// </summary>
  public static class AppError
  {
    #region ���������� ����������. ������ Srv_1

    /// <summary>
    /// Srv_1. ����� {0} ������ {1} ������ ���� �������� � ��������. 
    /// �� ������� �������� ��� ��������
    /// </summary>
    public const string NOT_IMPLEMENTED =
      "Srv_1. ����� {0} ������ {1} ������ ���� �������� � ��������. �� ������� �������� ��� ��������.";
    /// <summary>
    /// Srv_2. � ����� {0} ������ {1} �� ������� �������� {2}
    /// </summary>
    public const string ARGUMENT_NULL =
      "Srv_2. � ����� {0} ������ {1} �� ������� �������� {2}.";
    /// <summary>
    /// Srv_3. ��������� ������������ ������(����� {0} ������ {1}).
    /// </summary>
    public const string BUFFER_OVERFlOW =
      "Srv_3. ��������� ������������ ������(����� {0} ������ {1}).";



    #endregion

    #region �����. ������ � Srv_15

    /// <summary>
    /// Srv_15. Do not use abstract class Logger directly.
    /// </summary>
    public const string LOGGER_ABSTRACT_USING =
      "Srv_15. Do not use abstract class Logger directly.";

    #endregion

    #region ���������� ������. ������ � Srv_20

    /// <summary>
    /// Srv_20. � ����������� ������� ErrorHandler �� �������� 
    /// ������ �� ��������� ILogger.  
    /// </summary>
    public const string ERROR_HANDLER_NULL_CTOR =
      "Srv_20. � ����������� ������� ErrorHandler �� �������� ������ �� ��������� ILogger.";
    /// <summary>
    /// Srv_21. � ����� Handle ������� ErrorHandler 
    /// �� ������� �������� ����������(Exception).
    /// </summary>
    public const string ERROR_HANDLER_NULL_HANDLE =
      "Srv_21. � ����� Handle ������� ErrorHandler �� ������� �������� ����������(Exception).";
    /// <summary>
    /// Srv_22. ���������� ������ ���� {0} ��������� ���������� �������� {1}.
    /// </summary>
    public const string ERROR_OVERFLOW_EX_COUNT =
      "Srv_22. ���������� ������ ���� {0} ��������� ���������� �������� {1}.";

    #endregion

    #region ������ � ������ ��������. ������ � Srv_30

    /// <summary>
    /// Srv_30. �� ������ ���� � ����������� {0}.
    /// </summary>
    public const string SETTINGS_FILE_NOT_FOUND =
      "Srv_30. �� ������ ���� � ����������� {0}.";
    /// <summary>
    /// Srv_31. �������� � ��������������� ���� �������� {0}\r\n {1}
    /// </summary>
    public const string SETTINGS_DESERIALIZE =
      "Srv_31. �������� � ��������������� ����� �������� {0}\r\n {1}";

    #endregion

    #region ������ � ��. ������ � Srv_40

    /// <summary>
    /// Srv_40. ��������� �������� � ������� {0} � 
    /// ����������� ������ ������ ����� ���������� ������� {1} ��������.
    /// </summary>
    public const string DB_NUMBER_COLUMNS =
      "Srv_40. ��������� �������� � ������� {0} � ����������� ������ ������ ����� ���������� ������� {1} ��������.";
    /// <summary>
    ///Srv_41. � ����������� ������� DbCommand �� ������� �������� ������ �����������.
    /// </summary>
    public const string DB_NULL_DATAMANAGER_CTOR =
      "Srv_41. � ����������� ������� SrvDbCommand �� ������� �������� ������ �����������.";
    /// <summary>
    /// Srv_42. � ����� ������������� (Init) ������� ������� ����� ������ �� ������� ������ ����� ������
    /// </summary>
    public const string DB_NULL_DATA_INSERT_COMMAND =
      "Srv_42. � ����� ������������� (Init) ������� ������� ����� ������ �� ������� ������ ����� ������";
    /// <summary>
    /// Srv_43. � ����� DataManager.InsertDataInBuffer �� ������� 
    /// ������������ �������� DataGpsStruct[] DataGPS"
    /// </summary>
    public const string DB_NULL_DATAGPS_PARAM =
      "Srv_43. � ����� DataManager.InsertDataInBuffer �� ������� ������������ �������� DataGpsStruct[] DataGPS.";
    /// <summary>
    /// Srv_44. ��������� �������� � ������ ������ ����������� �������: {0}, 
    /// � ��������� ����������: {1}, ��������
    /// </summary>
    public const string DB_NUMBER_COLUMNS_LOST_DATA =
      "Srv_44. ��������� �������� � ������ ������ ����������� �������: {0}, � ��������� ����������: {1}, ��������.";
    /// <summary>
    /// Srv_45. ���������� ������������ � ���� ������. ������ �����������: {0}
    /// </summary>
    public const string DB_CONNECTION =
      "Srv_45. ���������� ������������ � ���� ������. ������ �����������: {0}";
    /// <summary>
    /// Srv_47. �� ��������� ������ ������� {0}. ���������� ������ ��������� ����������
    /// </summary>
    public const string DB_CRITICAL_COMMAND =
      "Srv_47. �� ��������� ������ �������: {0}. ���������� ������ ��������� ����������.";
    /// <summary>
    /// Srv_48. ������� {0} � ����� Init ������� �������� object[] initObjects ������ {1}, 
    /// ��������� ������� ������ ������ {2}.
    /// </summary>
    public const string DB_WRONG_INIT_PARAMS_LENGTH =
      "Srv_48. ������� {0} � ����� Init ������� �������� object[] initObjects ������ {1}, ��������� ������� ������ ������ {2}.";
    /// <summary>
    /// Srv_49. ������ ���������� ������� {0}, ������ �����������: {1}. \r\n ��������� �� ������: {2} \r\n ���������������� ���������� ��. ����
    /// </summary>
    public const string DB_COMMAND_EXECUTING_FAILED =
      "Srv_49. ������ ���������� ������� {0}, ������ �����������: {1}. \r\n ��������� �� ������: {2} \r\n ���������������� ���������� ��. ����.";

    #endregion

    #region ���������� ������. ������ � Srv_60.

    /// <summary>
    /// � ����������� ������� {0} �� ������� ������������ �������� byte[] Params.
    /// </summary>
    public const string CMD_NULL_PARAM_CTOR =
      "Srv_60. � ����������� ������� {0} �� ������� ������������ �������� byte[] Params.";

    #endregion

    #region �������. ������ � Srv_80

    /// <summary>
    /// Srv_80. � ����� {0} ������������ ������ TransportPacketHandler 
    /// �� ������� �������� {1}.
    /// </summary>
    public const string PARSING_NULL_ARGUMENT =
      "Srv_80. � ����� {0} ������������ ������ TransportPacketHandler �� ������� �������� {1}.";
    /// <summary>
    /// Srv_81. ����� ������� ���� ���������� � ����� {0} 
    /// ������ ���� ������ ��� ����� {1}
    /// </summary>
    public const string PARSING_OUT_OF_RANGE_ARRAY =
      "Srv_81. ����� ������� ���� ���������� � ����� {0} ������ ���� <= {1}";
    /// <summary>
    /// Srv_82. ����� ������ ������� {0} �� ������������ ���������. 
    /// ��������� {1} ����, ������ {2} ����
    /// </summary>
    public const string PARSING_UNEXPECTED_LENGTH_COMMAND =
      "Srv_82. ����� ������ ������� {0} �� ������������ ���������. ��������� {1} ����, ������ {2} ����.";
    /// <summary>
    /// Srv_83. ����� ������ ������� ����� ����
    /// </summary>
    public const string PARSING_ZERO_LENGTH =
      "Srv_83. ����� ������ ������� ����� ����.";
    /// <summary>
    /// Srv_84. ���������� �������� {0} � ����� GetSubArray 
    /// ����� �� ������� ���������.
    /// </summary>
    public const string PARSING_OUT_OF_RANGE =
      "Srv_84. ���������� �������� {0} � ����� GetSubArray ����� �� ������� ���������.";
    /// <summary>
    /// Srv_85. ����������� �������
    /// </summary>
    public const string PARSING_UNKNOWN_COMMAND =
      "Srv_85. ����������� ������� {0}";
    /// <summary>
    /// Srv_86. ����� ���������-������� ������ {0} ������ {1} �� ������������ ���������. ��������� {2} ����, �������� {3} ����.
    /// </summary>
    public const string PARSING_UNEXPECTED_LENGTH_ARRAY =
      "Srv_86. ����� ���������-������� ������ {0} ������ {1} �� ������������ ���������. ��������� {2} ����, �������� {3} ����.";
    /// <summary>
    /// Srv_86. ����� ���������-������� ������ {0} ������ {1} �� ������������ ���������. ��������� {2} ����, �������� {3} ����.
    /// </summary>
    public const string LOG_ID_VERY_BIG_VALUE =
      "Srv_87. �������� MobitelId = {0}. ����� ������� �������� LogID = {1}, ���������� �������� LogID = {2}, �������� interval = {3}, ��� ������ ����������� ��������. ���� ����� ����� ��������.";

    #endregion

    #region �������������� � �����������. ������ � Srv_100

    /// <summary>
    /// Srv_100. � ����������� Communicator �� ������� �������� {0}
    /// </summary>
    public const string TRANSPORT_NULL_PARAM_CTOR =
      "Srv_100. � ����������� Communicator �� ������� �������� {0}.";
    /// <summary>
    /// Srv_101. � ����������� Communicator �������� ������������ �������� ����� {0} (�������� Port)
    /// </summary>
    public const string TRANSPORT_PORT_PARAM_CTOR =
      "Srv_101. � ����������� Communicator �������� ������������ �������� ����� {0} (�������� Port).";
    /// <summary>
    /// Srv_102. ���������� ������ ������������� ������: \r\n {0}. \r\n IPaddress: {1}
    /// </summary>
    public const string TRANSPORT_INTERNAL =
      "Srv_102. ���������� ������ ������������� ������: \r\n {0}.\r\n IpAddress: {1}.";

    #endregion

    #region Main Controller. ������ � Srv_130

    /// <summary>
    /// Srv_130. ������ ����������� �������: ��� {0}
    /// </summary>
    public const string MAINCTRL_COMMAND_NOT_IMPLEMENTED =
      "Srv_130. ��� ������� {0} �� ���������� ����������.";
    /// <summary>
    /// Srv_131. � ����� {0} ��������� ������� ������ �� ������� �������� {1}
    /// </summary>
    public const string MAINCTRL_QUEUE_NULL_ARGUMENT =
      "Srv_131. � ����� {0} ��������� ������� ������ �� ������� �������� {1}.";
    /// <summary>
    /// Srv_132. ���������� ��������� ������ ��. ���-����
    /// </summary>
    public const string MAINCTRL_START_FAILURE =
      "Srv_132. ���������� ��������� ������ ��. ���-����.";

    #endregion


    #region ��������� ���. ������ � Srv_150

    /// <summary>
    /// Srv_150. �� ������� ������ � ������� {0}, �������� ������: {1}
    /// </summary>
    public const string CACHE_ROW_NOT_FOUND =
      "Srv_150. �� ������� ������ � ������� {0}, �������� ������: {1}.";
    #endregion

    #region A1 ������ � Srv_170

    /// <summary>
    /// Srv_170. �������������� ��������� �������-��������� {0} �� ��������� {1}.
    /// </summary>
    public const string A1_DECODE_CMD_WRONG =
      "Srv_170. �������������� ��������� �������-��������� {0} �� ��������� {1}.";
    /// <summary>
    /// Srv_171. �������� � �������� ��������������� {0} �� ��������������� � ���� ������.
    /// </summary>
    public const string A1_TT_NOT_FOUND =
      "Srv_171. �������� � �������� ��������������� {0} �� ��������������� � ���� ������.";
    /// <summary>
    /// Srv_172.���������� {0} ����������� �� ��������� A1 �� �������������� � ������� ������.
    /// </summary>
    public const string A1_SAVE_NOT_IMPLEMENTED =
      "Srv_172. ���������� {0} ����������� �� ��������� A1 �� �������������� � ������� ������.";
    /// <summary>
    /// Srv_173. �������������� ��������� �������-��������� {0} ��� ��������� {1}.
    /// </summary>
    public const string A1_ENCODE_CMD_WRONG =
      "Srv_173. �������������� ��������� �������-��������� {0} ��� ��������� {1}.";
    /// <summary>
    /// Srv_174. ������������ ��������� {0} ��� ��������� (�������� A1) �� �������������� � ������� ������.
    /// </summary>
    public const string A1_LOAD_NOT_IMPLEMENTED =
      "Srv_174. ������������ ��������� {0} ��� ��������� (�������� A1) �� �������������� � ������� ������.";

    #endregion
  }
}
