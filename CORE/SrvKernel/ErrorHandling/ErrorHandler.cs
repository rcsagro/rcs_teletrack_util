// Project: SrvKernel, File: ErrorHandler.cs
// Namespace: RCS.SrvKernel.ErrorHandling, Class: ErrorHandler
// Path: D:\Development\Pop3Pump_Head\SrvKernel\ErrorHandling, Author: guschin
// Code lines: 185, Size of file: 5.77 KB
// Creation date: 8/18/2008 9:33 AM
// Last modified: 9/24/2008 4:58 PM
// Generated with Commenter by abi.exDream.com

#region Using directives
using RCS.SrvKernel.Logging;
using System;
using System.Data.Common;
using RemoteInteractionLib;
#endregion

// ������ ��������� ������
namespace RCS.SrvKernel.ErrorHandling
{
	/// <summary>
	/// ����� ���������� ������
	/// <para>�������� �������������� DbException.
	/// ��� �������� DbException ������������ ����������� �������.</para>
	/// </summary>
	public static class ErrorHandler
	{
		/// <summary>
		/// ������������ ����������� ���-�� ������ ��� ������ � �� = 10
		/// </summary>
		private const short MAX_DB_ERROR_ALLOW = 10;
		/// <summary>
		/// ������������ ����������� ���-�� ������ �� ��������� � ��  = 50
		/// </summary>
		private const short MAX_COMMON_ERROR_ALLOW = 50;
		/// <summary>
		/// ����� � ������� �������� ��������� ���������
		/// ������������� � ��������� MAX_DB_ERROR_ALLOW 
		/// ���-�� ������ = 1 ������
		/// </summary>
		private const int ALLOW_DB_ERRORS_TIMEOUT = 1 * 60 * 1000;

		/// <summary>
		/// ����� � ������� �������� ��������� ���������
		/// ������������� � ��������� MAX_COMMON_ERROR_ALLOW 
		/// ���-�� ������ = 10 �����
		/// </summary>
		private const int ALLOW_COMMON_ERRORS_TIMEOUT = 10 * 60 * 1000;

		/// <summary>
		/// ������� ������ ��������� � ��
		/// </summary>
		private static ExceptionCounter DbExcepionCounter;

		/// <summary>
		/// ������� ������ �� ��������� � ��
		/// </summary>
		private static ExceptionCounter CommonExcepionCounter;

		/// <summary>
		/// ������� ������������ ���������� ���������� ��������� � ��
		/// </summary>
		public static event CommonEventDelegate OverflowDBExceptionsEvent;

		/// <summary>
		/// ������� ������������ ���������� ���������� �� ��������� � ��
		/// </summary>
		public static event CommonEventDelegate OverflowCommonExceptionsEvent;

		/// <summary>
		/// Create error handler
		/// </summary>
		static ErrorHandler()
		{
			DbExcepionCounter = new ExceptionCounter(
				MAX_DB_ERROR_ALLOW, ALLOW_DB_ERRORS_TIMEOUT);
			DbExcepionCounter.OverflowEvent +=
        new CommonEventDelegate(OnOverflowDBException);

			CommonExcepionCounter = new ExceptionCounter(
				MAX_COMMON_ERROR_ALLOW, ALLOW_DB_ERRORS_TIMEOUT);
			CommonExcepionCounter.OverflowEvent +=
        new CommonEventDelegate(OnOverflowCommonException);
		}
		// ErrorHandler(Logger)

		private const string ERROR_MESSAGE =
			"\r\n ������ {0} \r\n ������: {1} \r\n �����: {2} \r\n StackTrace: \r\n {3} \r\n";
		private const string WARNING_MESSAGE =
			"\r\n ��������������: {0} \r\n";

		/// <summary>
		/// ��������� ������
		/// </summary>
		/// <param name="ex">Exception</param>
		public static void Handle(Exception ex)
		{
			Handle(ex, ErrorLevel.ERROR);
		}
		// Handle()

		/// <summary>
		/// ��������� ������, � ��������� ������ "�����������"
		/// </summary>
		/// <param name="ex">Exception</param>
		/// <param name="level">Level</param>
		public static void Handle(Exception ex, ErrorLevel level)
		{
			if (ex == null)
				throw new ArgumentNullException("ex", AppError.ERROR_HANDLER_NULL_HANDLE);

			string msg = AssembleErrorMessage(ex, level);
			Logger.Write(msg);
			// ����������� ������ � ����������� ��� ������ �������������
			RepositOfStat.AddInErrorLst(String.Concat(DateTime.Now.ToString(), msg));

			// �������� ������� ������
			if (level == ErrorLevel.ERROR) {
				if (ex is DbException)
					DbExcepionCounter.Increment();
				else
					CommonExcepionCounter.Increment();
			}
		}

		/// <summary>
		/// ������� ������ � ����������� �� ������ ��� ������ 
		/// </summary>
		/// <param name="ex">Exception</param>
		/// <param name="level">Level</param>
		/// <returns>������ � �����������</returns>
		private static string AssembleErrorMessage(Exception ex, ErrorLevel level)
		{
			string result;
			switch (level) {
				case ErrorLevel.ERROR:
					result = String.Format(ERROR_MESSAGE,
						ex.Message, ex.Source, ex.TargetSite, ex.StackTrace);
					break;
				case ErrorLevel.WARNING:
					result = String.Format(WARNING_MESSAGE, ex.Message);
					break;
				default:
					result = "";
					break;
			} // switch
			return result;
		}

		/// <summary>
		/// ���������� ������� "������������" ���������� DBException
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">E</param>
		private static void OnOverflowDBException(object sender, EventArgsEx args)
		{
			string message = String.Format(AppError.ERROR_OVERFLOW_EX_COUNT,
				                    "DbException", Convert.ToString(MAX_DB_ERROR_ALLOW));

			// ����������� � ����
			Logger.Write(message);

			if (OverflowDBExceptionsEvent != null) {
				args.Message = message;
				OverflowDBExceptionsEvent(null, args);
			}
		}

		/// <summary>
		/// ���������� ������� "������������" ���������� Exception
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">E</param>
		private static void OnOverflowCommonException(object sender, EventArgsEx args)
		{
			string message = String.Format(AppError.ERROR_OVERFLOW_EX_COUNT,
				                    "Exception", Convert.ToString(MAX_DB_ERROR_ALLOW));

			// ����������� � ����
			Logger.Write(message);

			if (OverflowCommonExceptionsEvent != null) {
				args.Message = message;
				OverflowCommonExceptionsEvent(null, args);
			}
		}
	}
}
