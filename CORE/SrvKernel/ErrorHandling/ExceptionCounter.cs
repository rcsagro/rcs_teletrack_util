// Project: SrvLib, File: ExceptionCounter.cs
// Namespace: RCS.SrvLib.ErrorHandling, Class: ExceptionCounter
// Path: D:\Development\SrvService\SrvLib\ErrorHandling, Author: guschin
// Code lines: 92, Size of file: 2.65 KB
// Creation date: 4/1/2008 3:56 PM
// Last modified: 4/2/2008 12:13 PM
// Generated with Commenter by abi.exDream.com

#region Using directives
using System.Timers;
#endregion

namespace RCS.SrvKernel.ErrorHandling
{
  /// <summary>
  /// ������� ������.
  /// ��������� ����������� ������������� 
  /// ���-�� ������ �� ������� �������. ���� ���-��
  /// ������ ��������� ���������� ����� ����� 
  /// ������������� ������� - �� � ���� ������ ���������
  /// ������� � ������� ���-�� ������.
  /// </summary>
  class ExceptionCounter
  {
    private static object syncObject = new object();
    /// <summary>
    /// ������������ ����������� ���-�� ������
    /// </summary>
    private readonly int StandardErrorCount;
    /// <summary>
    /// ������� ���������� ��������� ������
    /// </summary>
    private int errorCount;

    private Timer timer;

    /// <summary>
    /// ������� ������������
    /// </summary>
    public event CommonEventDelegate OverflowEvent;

    /// <summary>
    /// Create exception counter
    /// </summary>
    /// <param name="allowErrorCount">����������� ���-�� ������ �� ����� TimeOut</param>
    /// <param name="timeOut">����� � ��</param>
    /// <param name="ExceptionTypeName">������������ ���� ������������� ������</param>
    public ExceptionCounter(int allowErrorCount, int timeOut)
    {
      StandardErrorCount = allowErrorCount;

      timer = new Timer();
      timer.AutoReset = false;
      timer.Interval = timeOut;
      timer.Elapsed += new ElapsedEventHandler(OnElapsedEvent);
      timer.Start();
    } // ExceptionCounter(AllowErrorCount, TimeOut)

    private void OnElapsedEvent(object source, ElapsedEventArgs e)
    {
      lock (syncObject)
      {
        if (errorCount < StandardErrorCount)
          errorCount = 0;

        timer.Start();
      }
    }

    /// <summary>
    /// ���������� �������� �� �������
    /// </summary>
    public void Increment()
    {
      lock (syncObject)
      {
        errorCount++;

        if ((errorCount >= StandardErrorCount) && (OverflowEvent != null))
        {
          timer.Stop();
          errorCount = 0;
          EventArgsEx args = new EventArgsEx();
          OverflowEvent(this, args);
          timer.Start();
        }
      }
    }
  }
}
