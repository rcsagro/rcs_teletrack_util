
namespace RCS.SrvKernel.SrvSystem.Messages
{
  /// <summary>
  /// ��������� ����������� ���������
  /// </summary>
  public interface IMsgDispatcher
  {
    /// <summary>
    /// ��������� ���������
    /// </summary>
    /// <param name="message">MessageSpec ���������</param>
    /// <returns>������ ���� ��������� ���������� � ������� �� ���������</returns>
    bool Post(MessageSpec message);
  }
}
