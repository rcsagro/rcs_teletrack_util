
namespace RCS.SrvKernel.SrvSystem.Messages
{
  /// <summary>
  /// ������-���������
  /// </summary>
  public class MessageSpec
  {
    /// <summary>
    /// ������������� ���������
    /// </summary>
    public MessageType msgType;
    /// <summary>
    /// �����������
    /// </summary>
    public object Sender;
    /// <summary>
    /// ���������
    /// </summary>
    public object Params;

    public MessageSpec()
    {
      msgType = MessageType.UNDEFINED;
    }
  }
}
