// Project: TDataMngrLib, File: MessageType.cs
// Namespace: RCS.TDataMngrLib.Base, Class: 
// Path: D:\Development\TDataManager_Head\TDataMngrLib\Base, Author: guschin
// Code lines: 11, Size of file: 157 Bytes
// Creation date: 7/22/2008 2:59 PM
// Last modified: 7/22/2008 3:36 PM
// Generated with Commenter by abi.exDream.com

namespace RCS.SrvKernel.SrvSystem.Messages
{

  /// <summary>
  /// ���� ���������
  /// </summary>
  public enum MessageType
  {
    /// <summary>
    /// ��������������� ���
    /// </summary>
    UNDEFINED = -2,
    /// <summary>
    /// ������ ���������(����� ����������� ��� �����)
    /// </summary>
    DUMMY = -1,
    /// <summary>
    /// ������� �����  
    /// </summary>
    CMD_START,
    /// <summary>
    /// ������ ���������
    /// (����� �� CMD_START)
    /// </summary>
    STARTED,
    /// <summary>
    /// ������� ���������
    /// </summary>
    CMD_STOP,
    /// <summary>
    /// ��������� ���������
    /// (����� �� CMD_STOP)
    /// </summary>
    STOPPED,
    /// <summary>
    /// ��������� ������ ��������� ���������
    /// </summary>
    STOP_EXECUTING_Thread,
    /// <summary>
    /// ���������� ��������
    /// </summary>
    CONNECTIONS_AVAILABLE,
    /// <summary>
    /// ������� ���������� ������
    /// </summary>
    ERROR_OVERFLOW,
    /// <summary>
    /// ������� ����� �����
    /// </summary>
    PACKET_RECEIVED,
    /// <summary>
    /// ��������� ���������� ��������� 
    /// ����� � ������� ���������
    /// </summary>
    STATE_EXECUTING_DONE,
    /// <summary>
    /// ���������� ��������  
    /// TODO: ������ REFRESH_SETTINGS
    /// </summary>
    SETTINGS_REFRESH,
    /// <summary>
    /// ���������� ��������� �����
    /// </summary>
    CHECK_MAIL,
    /// <summary>
    /// Windows ��������� ��������
    /// </summary>
    MESSAGE_READ,
    /// <summary>
    /// �������� ��������� 
    /// TODO: ������� � �������� �� SETTINGS_REFRESH(��� ����������)
    /// </summary>
    REFRESH_SETTINGS,
    /// <summary>
    /// ������ Hasp-���� � ������ ���������
    /// </summary>
    HL_FOUND,
    /// <summary>
    /// ������� ��������� ���������� 
    /// </summary>
    CONNECT
  }
}
