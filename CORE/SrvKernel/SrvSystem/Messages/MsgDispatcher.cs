// Project: SrvKernel, File: MsgDispatcher.cs
// Namespace: RCS.SrvKernel.SrvSystem.Messages, Class: MsgDispatcher
// Path: D:\Development\TDataManager_Head\SrvKernel\SrvSystem\Messages, Author: guschin
// Code lines: 185, Size of file: 5.11 KB
// Creation date: 7/24/2008 12:56 PM
// Last modified: 7/28/2008 2:47 PM
// Generated with Commenter by abi.exDream.com

#region Using directives
using System;
using System.Collections.Generic;
using System.Threading;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.ErrorHandling;
#endregion

namespace RCS.SrvKernel.SrvSystem.Messages
{

    /// <summary>
    /// ���������� ���������
    /// </summary>
    /// <param name="message">MessageSpec</param>
    public delegate void HandleMessageDelegate(MessageSpec message);

    /// <summary>
    /// ������� �����, ����������� �������� ���������
    /// ���������. 
    /// <para> ���������� ����� ��������� ������������
    /// � ����� � ����������� ����������� �������������
    /// �� ��������� ������ ������ � ������.
    /// <para> ������������� ����� ��������� ���������
    /// ����������� ��������� � ��������� ������ ������
    /// �� �������� � ���, ��� ������ ���� �� ������ �������
    /// </para></para>
    /// </summary>
    public class MsgDispatcher : IMsgDispatcher
    {
        /// <summary>
        /// ������ �������������
        /// </summary>
        private static volatile object syncObject = new object();

        /// <summary>
        /// ������� ����� �c�������� ������
        /// </summary>
        private Thread workThread;
        private volatile EventWaitHandle waitEvent;

        /// <summary>
        /// ������� ���������
        /// </summary>
        private Queue<MessageSpec> messageQueue;

        /// <summary>
        /// ���������� ����������
        /// </summary>
        private volatile bool shouldStop;


        /// <summary>
        /// ������� ��������� ���������
        /// </summary>
        public event HandleMessageDelegate EventHandleMessage;


        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="messageThreadName">�������� ��� ������ �������</param>
        public MsgDispatcher(string messageThreadName)
        {
            messageQueue = new Queue<MessageSpec>();
            waitEvent = new AutoResetEvent(false);

            workThread = new Thread(this.Run);
            workThread.Name = String.Concat("MessageLoopThread.", messageThreadName);
            workThread.Start();
        }

        /// <summary>
        /// ������������� ��������������(��� ����� ���������)
        /// </summary>
        protected virtual void Initialize() { }

        /// <summary>
        /// �������� ����� ���������
        /// </summary>
        private void Run()
        {
            Initialize();
            MessageSpec currentMessage;

            while (true)
            {
                currentMessage = null;

                if (shouldStop)
                {
                    messageQueue.Clear();
                    waitEvent.Close();
                    break;
                }

                // ���������� ��������� �� ������� 
                lock (syncObject)
                {
                    if (messageQueue.Count > 0)
                        currentMessage = messageQueue.Dequeue();
                } // lock

                if ((currentMessage != null) &&
                  (currentMessage.msgType != MessageType.UNDEFINED))
                {
                    if (SrvSettings.DebugMode)
                    {
                        Logger.WriteDebug("msg", String.Format(
                          "{0} ���������� ��������� {1} �� ���������",
                          workThread.Name, currentMessage.msgType));
                    } // if (SrvSettings.DebugMode)

                    HandleMessage(currentMessage);

                } // if (currentMessage)
                else
                {
                    waitEvent.WaitOne();
                } // else
            } // while (true)

        } // Run()

        /// <summary>
        /// ��������� ���������
        /// </summary>
        /// <param name="message">MessageSpec</param>
        protected virtual void HandleMessage(MessageSpec message)
        {
            if (EventHandleMessage != null)
                EventHandleMessage(message);
        }

        /// <summary>
        /// ������� ���������
        /// </summary>
        /// <param name="message">MessageSpec ���������</param>
        /// <returns>������ ���� ��������� ���������� � ������� �� ���������</returns>
        public bool Post(MessageSpec message)
        {
            lock (syncObject)
            {

                if (SrvSettings.DebugMode)
                {
                    Logger.WriteDebug("msg", String.Format(
                      "{0} ������� ��������� {1}", workThread.Name, message.msgType));
                } // if (SrvSettings.DebugMode)

                if (message.msgType == MessageType.STOP_EXECUTING_Thread)
                {
                    shouldStop = true;
                    waitEvent.Set();
                    return true;
                } // if (message.msgType)

                if (messageQueue.Count <= int.MaxValue)
                {
                    messageQueue.Enqueue(message);
                    waitEvent.Set();
                    return true;
                } // if (messageQueue.Count)
                else
                {
                    ErrorHandler.Handle(new ApplicationException(String.Format(
                      AppError.BUFFER_OVERFlOW, "Post", this.GetType().Name)));
                    return false;
                } // else
            }
        }

    }// END CLASS DEFINITION MsgObject
}
