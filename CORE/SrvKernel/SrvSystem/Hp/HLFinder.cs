// Project: SrvKernel, File: HaspLicenceFinder.cs
// Namespace: RCS.SrvKernel.SrvSystem.Hasp, Class: HaspLicenceFinder
// Path: D:\Development\DirectOnline_Head\SrvKernel\SrvSystem\Hasp, Author: guschin
// Code lines: 103, Size of file: 3,07 KB
// Creation date: 03.03.2009 17:40
// Last modified: 04.03.2009 11:51
// Generated with Commenter by abi.exDream.com

#region Using directives
using RCS.HLibrary;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvSystem.Messages;
using System;
using System.Threading;
#endregion

namespace RCS.SrvKernel.SrvSystem.Hp
{
  /// <summary>
  /// ������������ ����� ����� Hasp �� ��� ���, ����
  /// ���� � ��������� ��������� �� ����� ������. 
  /// ������������� ��������� � ��������� ������.
  /// <para>����� ���� ��� ���� ��������� - ������������ ���������</para>
  /// </summary>
  class HLFinder : IHLFinder
  {
    /// <summary>
    /// ������ �������� ����� 10 ������
    /// </summary>
    private const int INSPECTION_PERIOD = 10 * 1000;
    /// <summary>
    /// ������� �� ��������� ������
    /// </summary>
    private volatile bool haveToStop;
    /// <summary>
    /// ����� �������� ��� ������
    /// </summary>
    private volatile int lNumber;
    /// <summary>
    /// ��������, ������� ����� ������������ ��������� ������� �����
    /// </summary>
    private volatile IMsgDispatcher owner;

    /// <summary>
    /// Create hasp licence finder
    /// </summary>
    /// <param name="lNumber">����� �������� � �����</param>
    /// <param name="owner">��������, ������� ����� ������������ ��������� ������� �����</param>
    public HLFinder(int lNumber, IMsgDispatcher owner)
    {
      this.lNumber = lNumber;
      this.owner = owner;
    } // HaspLicenceFinder(licenceNumber, owner)

    /// <summary>
    /// �������� ������� Hasp-����� � ������ ��������. ����� ����������� �� ��� ���, ����
    /// �� ����� ������ ���� � ��������� ���������.
    /// <para>����� ���� ��� ���� ��������� � � ��� ������� �������� - ������������ ���������</para>
    /// ������������� ��������� � ��������� ������. 
    /// </summary>
    public void Inspect()
    {
      haveToStop = false;

      while (!haveToStop)
      {
        try
        {
          new CHp(lNumber);

          if (!haveToStop)
          {
            // ���� �� ���� ���������� - ������ ��� � �������,
            // ��������� ������
            haveToStop = true;

            MessageSpec message = new MessageSpec();
            message.msgType = MessageType.HL_FOUND;
            owner.Post(message);
          } // if ()
        }
        catch (Exception Ex)
        {
          // ����-����� ���������� �� �����������
          ErrorHandler.Handle(new ApplicationException(Ex.Message));

          if (!haveToStop)
            Thread.Sleep(INSPECTION_PERIOD);
        }
      }
    } // Inspect(licenceNumber, owner)

    /// <summary>
    /// ���������� ��������
    /// </summary>
    public void Stop()
    {
      haveToStop = true;
    } // Stop()
  }
}
