// Project: GPSControl, File: HaspLibrary.cs
// Namespace: RCS.HLibrary, Class: CHp
// Path: D:\MyProject\GPSControl\GPSControl\Hasp, Author: Ivanov
// Code lines: 301, Size of file: 8,95 KB
// Creation date: 21.07.2008 16:52
// Last modified: 21.07.2008 17:38


#region Using directives
using Aladdin.HASP;
using System;
using System.Xml;
#endregion

namespace RCS.HLibrary
{
  /// <summary>
  /// CHasp
  /// </summary>
  public class CHp
  {

    protected Hasp hp;

    private byte[] mainBytes;
    public byte[] MainBytes
    {
      get { return mainBytes; }
    }

    protected int hpid;
    public int Hpid
    {
      get { return hpid; }
    }

    /// <summary>
    /// Feature options
    /// </summary>
    private FeatureOptions options;

    /// <summary>
    /// Create c hasp
    /// </summary>
    public CHp()
    {
      options = FeatureOptions.Default;
      Log();
      mainBytes = GetMain();
      hpid = Convert.ToInt32(SessionInfo("hasp_info/keyspec/hasp/haspid"));
    } // CHasp()

    /// <summary>
    /// Create c hasp
    /// </summary>
    /// <param name="num">����� ��������</param>
    public CHp(int num)
    {
      options = FeatureOptions.Default;
      Log(num);
      mainBytes = GetMain();
      hpid = Convert.ToInt32(SessionInfo("hasp_info/keyspec/hasp/haspid"));
    } // CHasp()

    /// <summary>
    /// Login
    /// </summary>
    public void Log()
    {
      HaspFeature feature = HaspFeature.ProgNumDefault;
      feature.SetOptions(options, FeatureOptions.Default);

      hp = new Hasp(feature);

      HaspStatus status = hp.Login(VendorCode.Code);
      if (status != HaspStatus.StatusOk)
        throw new Exception("�� ������ ����");

      //Read(hasp, HaspFiles.Main);
      //SessionInfoDemo(hasp);

    } // Login()

    /// <summary>
    /// Login
    /// </summary>
    /// <param name="num">����� ��������</param>
    public void Log(int num)
    {
      HaspFeature feature = HaspFeature.FromProgNum(num);//.ProgNumDefault;
      feature.SetOptions(options, FeatureOptions.Default);

      hp = new Hasp(feature);

      HaspStatus status = hp.Login(VendorCode.Code);
      if (status != HaspStatus.StatusOk)
      {
        if (status == HaspStatus.FeatureNotFound)
          throw new Exception(String.Concat("�� ������� �����", "\r\n", status.ToString()));

        throw new Exception(String.Concat("�� ������ ����", "\r\n", status.ToString()));
      } // if (status)

      //Read(hasp, HaspFiles.Main);
      //SessionInfoDemo(hasp);

    } // Login()

    /// <summary>
    /// Session info
    /// </summary>
    /// <returns>String</returns>
    protected string SessionInfo(string nodename)
    {
      // firstly we will retrieve the key info.
      string info = null;
      HaspStatus status = hp.GetSessionInfo(Hasp.KeyInfo, ref info);
      XmlDocument xmlInfo = new System.Xml.XmlDocument();
      xmlInfo.LoadXml(info);
      return xmlInfo.SelectSingleNode(nodename).InnerText;
    } // SessionInfo()

    /// <summary>
    /// Get main
    /// </summary>
    /// <returns>byte[]</returns>
    public byte[] GetMain()
    {
      // sanity check
      if ((null == hp) || !hp.IsLoggedIn())
        return null;
      HaspFile file = hp.GetFile(HaspFiles.Main);
      if (!file.IsLoggedIn())
      {
        // Not logged into a key - nothing left to do.
        return null;
      } // if ()
      // get the file size
      int size = 0;
      HaspStatus status = file.FileSize(ref size);
      // read the contents of the file into a buffer
      byte[] bytes = new byte[size];
      status = file.Read(bytes, 0, bytes.Length);
      return bytes;
    } // GetMain()

    /// <summary>
    /// Read write demo
    /// </summary>
    /// <param name="hasp">Hasp</param>
    /// <param name="fileId">File id</param>
    //public void Read(Hasp hasp, HaspFiles fileId)
    //{
    //  // sanity check
    //  if ((null == hasp) || !hasp.IsLoggedIn())
    //    return;

    //  // firstly get a file object to a key's file.
    //  // please note: the file object is tightly connected
    //  // to its key object. logging out from a key also
    //  // invalidates the file object.
    //  // doing the following will result in an invalid
    //  // file object:
    //  // hasp.login(...)
    //  // HaspFile file = hasp.GetFile();
    //  // hasp.logout();
    //  // Debug.Assert(file.IsValid()); will assert
    //  HaspFile file = hasp.GetFile(fileId);
    //  if (!file.IsLoggedIn())
    //  {
    //    // Not logged into a key - nothing left to do.
    //    System.Diagnostics.Trace.WriteLine("Failed to get file object\r\n");
    //    return;
    //  } // if ()

    //  Trace.WriteLine("Reading contents of file: " + file.FileId.ToString());

    //  Trace.WriteLine("Retrieving the size of the file");

    //  // get the file size
    //  int size = 0;
    //  HaspStatus status = file.FileSize(ref size);
    //  //ReportStatus(status);

    //  if (HaspStatus.StatusOk != status)
    //  {
    //    Trace.WriteLine("");
    //    return;
    //  } // if (HaspStatus.StatusOk)

    //  Trace.WriteLine("Size of the file is: " + size.ToString() + " Bytes");

    //  // read the contents of the file into a buffer
    //  byte[] bytes = new byte[size];

    //  Trace.WriteLine("Reading data");
    //  status = file.Read(bytes, 0, bytes.Length);
    //  //ReportStatus(status);

    //  if (HaspStatus.StatusOk != status)
    //  {
    //    Trace.WriteLine("");
    //    return;
    //  } // if (HaspStatus.StatusOk)
    //  int firstBlock = bytes[0];
    //  int secondBlock = bytes[firstBlock+1];
    //  Trace.WriteLine("������ ������� ����� " + firstBlock.ToString());
    //  Trace.WriteLine("������ ������� ����� " + secondBlock.ToString());
    //  ArrayList listCodeMap = new ArrayList();
    //  for (int i = 0; i < secondBlock / 4; i++)
    //  {
    //    int codeMap = bytes[firstBlock + i*4+2];
    //    codeMap += bytes[firstBlock + i * 4+3] << 8;
    //    codeMap += bytes[firstBlock + i * 4+4] << 16;
    //    listCodeMap.Add(codeMap);
    //  } // for (int)
    //  foreach(int code in listCodeMap)
    //  {
    //    Trace.WriteLine(code.ToString("x"));
    //  } // foreach (code)

    //  //DumpBytes(bytes);
    //  foreach (byte b in bytes)
    //  {
    //    Trace.WriteLine(b.ToString("x"));
    //  } // foreach
    //} // Read(hasp, fileId)

    /// <summary>
    /// Session info demo
    /// </summary>
    /// <param name="hasp">Hasp</param>
    //  public void SessionInfoDemo(Hasp hasp)
    //  {
    //    // sanity check
    //    if ((null == hasp) || !hasp.IsLoggedIn())
    //      return;

    //    Trace.WriteLine("Get Session Information Demo");

    //    Trace.WriteLine("Retrieving Key Information");

    //    // firstly we will retrieve the key info.
    //    string info = null;
    //    HaspStatus status = hasp.GetSessionInfo(Hasp.KeyInfo,
    //                                            ref info);

    //    System.Xml.XmlDocument xmlInfo = new System.Xml.XmlDocument();
    //    xmlInfo.LoadXml(info);
    //    System.Xml.XmlNode node = xmlInfo.SelectSingleNode("hasp_info/keyspec/hasp/haspid");
    //    Trace.WriteLine(node.InnerText);
    //    int  haspid = Convert.ToInt32(node.InnerText);

    //    //ReportStatus(status);
    //    if (HaspStatus.StatusOk == status)
    //    {
    //      Trace.WriteLine("Key Information:");
    //      Trace.WriteLine(info.Replace("\r\n", "\r\n          "));
    //    } // if (HaspStatus.StatusOk)
    //    else
    //      Trace.WriteLine("");

    //    Trace.WriteLine("Retrieving Session Information");

    //    // next the session info.
    //    status = hasp.GetSessionInfo(Hasp.SessionInfo, ref info);
    //    //ReportStatus(status);
    //    if (HaspStatus.StatusOk == status)
    //    {
    //      Trace.WriteLine("Session Information:");
    //      Trace.WriteLine(info.Replace("\r\n", "\r\n          "));
    //    } // if (HaspStatus.StatusOk)
    //    else
    //      Trace.WriteLine("");

    //    Trace.WriteLine("Retrieving Update Information");

    //    // last the update information.
    //    status = hasp.GetSessionInfo(Hasp.UpdateInfo, ref info);
    //    //ReportStatus(status);
    //    if (HaspStatus.StatusOk == status)
    //    {
    //      Trace.WriteLine("Update Information:");
    //      Trace.WriteLine(info.Replace("\r\n", "\r\n          "));
    //    }
    //      // if (HaspStatus.StatusOk)
    //    else
    //      Trace.WriteLine("");
    //  } // SessionInfoDemo(hasp)
  } // class CHasp
}
