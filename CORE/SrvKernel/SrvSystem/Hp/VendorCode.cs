////////////////////////////////////////////////////////////////////
//                Aladdin Knowledge Systems (Deutschland) GmbH
//                    (c) AKS Ltd., All rights reserved
//
//
// $Id: haspvendorcode.cs,v 1.1 2004/03/09 16:16:06 dieter Exp $
////////////////////////////////////////////////////////////////////
using System;
using System.Text;


namespace RCS.HLibrary
{
  /// <summary>
  /// Class for holding the demoma vendor code.
  /// This vendor code shall be used within the sample.
  /// </summary>
  public class VendorCode
  {
    /// <summary>
    /// The Base64 encoded vendor code for a demoma key.
    /// </summary>
    private const string vendorCodeString =
      "BFz2gF7MZAPoDKKIagtyI67kHws+K2gJKzdawd56VhqYoIvR" +
      "vIfMjb2h7OCU+Lj9nPxyGJaUDSZUuB8iP1t/2st6NHUONcGK" +
      "X8sNiRcFQPAVP46F2cQvlLAszPb4Z+Ua0WwHck7F4FmMD9jf" +
      "L+PtR5vS4L//lV5E9+2fKMwNThea/lx2+zEDbN31TcSd2NLC" +
      "gtbazHWFSZJk5ScaOyWBd0hbnKATi3JY7bQAetGPIhNbD4hx" +
      "FGq9b2PxoWeBihLQMUrdunkUHlBR9BZyQjthCQepiaqZUQwD" +
      "ppj5JTFbp8nwM/i3ytBH44mbanclQl44e/DLCx2ozux5IAvH" +
      "HRbA9l5z1KAbQqXGvk5m8PYnJR2Y0MuWg8dN46LCg+6AIGL9" +
      "0MnjcRorKoFem+vBxJwTAshKFGBi4/fQS+10ipiiqHWCGOiJ" +
      "F/yMyWJ4squO6nEAYI2NDndiyVYnpgKXOGkHsMCf1QMjdOAZ" +
      "oeOFj5XQNtNsEntf7B31aaXgDaAk+Y1O68GuEWvrCnbft9KD" +
      "KA5iLDIpziiuAmci5CWI08LOGm5+KneSpm3cCP1ZWPPphjjF" +
      "ymjaRUMzfUetcI4xYFNxOX9HIYIp41bYc+4PajOeraIUCZQA" +
      "lcnYs9daCxe+Eu3OJmQDTHjb8Nbwmw2U0S6SqelPJikZuonu" +
      "DAk6ACuL5CjyMB29ILnN8KWjVJTOdKeNSX1n75T/0dXzXimX" +
      "2HwHBnZY103g";

    /// <summary>
    /// Constructor - does nothing by default
    /// </summary>
    public VendorCode() { }

    /// <summary>
    /// Returns the vendor code as a byte array.
    /// </summary>
    static public byte[] Code
    {
      get
      {
        byte[] code = ASCIIEncoding.Default.GetBytes(vendorCodeString);
        return code;
      }
    }
  }
}
