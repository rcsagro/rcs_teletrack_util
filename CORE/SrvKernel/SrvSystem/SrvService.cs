#region Using directives
using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvSystem.Hp;
using RCS.SrvKernel.SrvSystem.Messages;
using RemoteInteractionLib;
#endregion

namespace RCS.SrvKernel.SrvSystem
{
    /// <summary>
    /// ���� ��� ������ ���������� ����������� ������ ������ �� ����������. 
    /// 
    /// TODO: �������� ��������� IMsgDispatcher ������ �� ������� ����� - 
    ///   ��������������, ��� ��� �� �����.
    /// </summary>
    public class SrvService : ServiceBase, IMsgDispatcher
    {
        /// <summary>
        /// �������������� ����� ����������� ���
        /// ������ � ���������� ������ - 3 ������
        /// </summary>
        private const int ADDITIONAL_TIME = 3 * 60 * 1000;
        /// <summary>
        /// ��� ���������� ������ 1066, ���������� � ������������� ���
        /// ������� ���������� ������
        /// </summary>
        private const int ERROR_SERVICE_SPECIFIC_ERROR = 1066;

        /// <summary>
        /// ������� �������� ��������� �����������
        /// </summary>
        private volatile EventWaitHandle waitControllerStopEvent;

        /// <summary>
        /// ��������� ���������
        /// </summary>
        protected IMsgDispatcher iMsgDispatcher;

        /// <summary>
        /// ������� ����������
        /// </summary>
        protected IMainController mainController;

        /// <summary>
        /// ������������ ����� ����� Hasp � �������� � ���
        /// </summary>
        private IHLFinder �lFinder;
        /// <summary>
        /// ����� �c�������� ������
        /// </summary>
        private Thread �lFinderThread;

        /// <summary>
        /// ��� ������ � ������ ���������� ��������� ��� ����������
        /// </summary>
        protected string RealServiceName
        {
            get
            {
                return String.IsNullOrEmpty(SrvSettings.ServiceName) ?
                  ServiceName : SrvSettings.ServiceName;
            }
        }

        public SrvService()
            : base()
        {
            MsgDispatcher obj = new MsgDispatcher(GetType().Name);
            obj.EventHandleMessage += new HandleMessageDelegate(OnHandleMessage);
            iMsgDispatcher = (IMsgDispatcher)obj;

            waitControllerStopEvent = new AutoResetEvent(false);
        }

#if DEBUG
        /// <summary>
        /// ��� ������� � ������.
        /// </summary>
        public void RunDebug()
        {
            LoadSettings();
            StartMainController();
        }
#endif

        #region �������� ���������

        /// <summary>
        /// ��������� ���������
        /// </summary>
        /// <param name="message">MessageSpec</param>
        private void OnHandleMessage(MessageSpec message)
        {
            HandleMessage(message);
        }

        /// <summary>
        /// �������� ���������. ������������� � �������� ��� 
        /// ������������� ��� ������� ��������� ���������.
        /// </summary>
        /// <param name="message">MessageSpec</param>
        protected virtual void HandleMessage(MessageSpec message)
        {
            switch (message.msgType)
            {
                case MessageType.STARTED:
                    // ������� ���������� ����������
                    HandleControllerStarted();
                    break;
                case MessageType.STOPPED:
                    // ������� ���������� �����������
                    HandleControllerStopped();
                    break;
                case MessageType.HL_FOUND:
                    HandleHL();
                    break;
                default:
                    throw new ApplicationException(String.Format(
                      "��������� ��������� {0} ������� �� �������������.", message.msgType));
            }
        }

        /// <summary>
        /// �������� ���������
        /// </summary>
        /// <param name="message">MessageSpec</param>
        /// <returns>������ ���� ��������� ���������� � ������� �� ���������</returns>
        public bool Post(MessageSpec message)
        {
            bool result = iMsgDispatcher.Post(message);
            return result;
        }

        /// <summary>
        /// ���������� ������������ �������� � ��������� ������ ����������
        /// ���������
        /// </summary>
        private void StopMessageThread()
        {
            MessageSpec message = new MessageSpec();
            message.msgType = MessageType.STOP_EXECUTING_Thread;
            Post(message);
        }

        #endregion

        #region HASP

        /// <summary>
        /// ����� �������� ��� ������ � Hasp-�����.
        /// ������������� � ���������� ��������, ���� 
        /// ��������� �������� ��������.
        /// </summary>
        /// <returns>Int</returns>
        protected virtual int GetLnumber()
        {
            throw new Exception(String.Format(
              AppError.NOT_IMPLEMENTED, "GetLogLevel", "SrvService"));
        }

        /// <summary>
        /// ������ ������ ������ Hasp ����� � ��������� 
        /// </summary>
        private void StartHlThread()
        {
            �lFinder = new HLFinder(GetLnumber(), this);
            �lFinderThread = new Thread(�lFinder.Inspect);
            �lFinderThread.Name = "HlThread";
            �lFinderThread.Start();
        }

        /// <summary>
        /// ���������� ��������� ��������� ����� � ��������
        /// </summary>
        private void HandleHL()
        {
            DisposeHlObjects();
            StartMainController();
        }

        /// <summary>
        /// ������������� ����� ������ Hasp ����� � ���������
        /// </summary>
        private void DisposeHlObjects()
        {
            if ((�lFinder != null) && (�lFinderThread != null))
            {
                �lFinder.Stop();
                �lFinderThread.Join();
            }

            �lFinder = null;
            �lFinderThread = null;
        }

        #endregion

        #region IPC �����

        /// <summary>
        /// �������� IPC ������
        /// </summary>
        protected virtual void OpenIpcChannel()
        {
            ChanelInteraction.OpenServerChanel(RealServiceName);
        }

        /// <summary>
        /// �������� IPC ������
        /// </summary>
        protected virtual void CloseIpcChannel()
        {
            ChanelInteraction.CloseServerChanel(RealServiceName);
        }

        #endregion

        /// <summary>
        /// �����
        /// </summary>
        /// <param name="args">string[]</param>
        protected override void OnStart(string[] args)
        {
            // ���������� � ���, ��� ��� ��������� �������������� ����� ��� ���� �����
            // ��������� ������ 
            RequestAdditionalTime(ADDITIONAL_TIME);

            // �������������� ���������� ���������� � ���.
            // ���-���� ������� � ���� �� ������ - ������ ����������� � ����� ������.
            AppDomain.CurrentDomain.UnhandledException +=
              new UnhandledExceptionEventHandler(UnhandledExceptionHandler);

            try
            {
                RepositOfStat.ConnectionCount = 0;
                LoadSettings();
                OpenIpcChannel();
                Logger.Write(GetStartMessage());
                Logger.Write(System.Reflection.Assembly.GetCallingAssembly().FullName);
            }
            catch (Exception Ex)
            {
                ShowException(String.Format("{0} \r\n {1}", Ex.Message, Ex.StackTrace), true);
                OnStop();
                throw Ex;
            }

            // �������� ������� ����� ���������� ������ � �������
            // ������������ ������� ������������...
#if (PROTECT)
      StartHlThread();
#else
            StartMainController();
#endif

            base.OnStart(args);
        }

        /// <summary>
        /// ����� �������� ����������� 
        /// </summary>
        private void StartMainController()
        {
            try
            {
                mainController = CreateMainController();
                Logger.Write(GetRunningMessage());
                SendStartMessage();
            }
            catch (Exception Ex)
            {
                ShowException(String.Format("{0} \r\n {1}", Ex.Message, Ex.StackTrace), true);
                OnStop();
                throw Ex;
            }
        }

        /// <summary>
        /// �������� ��������. ������� ����������� ���
        /// �������� ����� ������������� �������
        /// </summary>
        protected virtual void LoadSettings()
        {
            SrvSettings.LoadSettings();
        }

        /// <summary>
        /// �������� �������� �����������
        /// </summary>
        /// <returns>IMainController</returns>
        protected virtual IMainController CreateMainController()
        {
            throw new Exception(String.Format(
              AppError.NOT_IMPLEMENTED, "CreateMainController", "SrvService"));
        }

        /// <summary>
        /// ���������
        /// </summary>
        protected override void OnStop()
        {
            // ���������� � ���, ��� ��� ��������� �������������� ����� ��� ���� �����
            // ��������� ������ 
            RequestAdditionalTime(ADDITIONAL_TIME);

            InnerStop();
            base.OnStop();

            CloseIpcChannel();
            FinallOperations();
        }

        /// <summary>
        /// ���� ������ � ���������� �����������
        /// </summary>
        private void FinallOperations()
        {
            Logger.Write(GetStopMessage());
        }

        /// <summary>
        /// ��������� ���������� ��������
        /// </summary>
        private void InnerStop()
        {
            try
            {
                DisposeHlObjects();
                StopServer();
                // ��������� ����� ��������� ���������
                StopMessageThread();
                waitControllerStopEvent.Close();
            }
            catch (Exception Ex)
            {
                ShowException(Ex.Message, true);
            }
        }

        /// <summary>
        /// ����� �� �������
        /// </summary>
        protected override void OnShutdown()
        {
            if (mainController != null)
            {
                Logger.Write(
                  " OS SHUTDOWN (������� ���������� ������ ������������ �������)");
            }

            InnerStop();
            FinallOperations();
            base.OnShutdown();
        }

        /// <summary>
        /// Stop server
        /// </summary>
        private void StopServer()
        {
            if (mainController != null)
            {
                SendStopMessage();
            }
        }

        /// <summary>
        /// ���������� ������������� ����������
        /// </summary>
        /// <param name="Sender">object</param>
        /// <param name="ExArgs">UnhandledExceptionEventArgs</param>
        private void UnhandledExceptionHandler(object Sender, UnhandledExceptionEventArgs ExArgs)
        {
            ShowException(String.Format("�������������� ��������: {0} \r\n {1}",
              ((Exception)ExArgs.ExceptionObject).Message,
              ((Exception)ExArgs.ExceptionObject).StackTrace),
              true);
        }

        private delegate DialogResult ShowMsgDelegate(
          string text,
          string caption,
          MessageBoxButtons buttons,
          MessageBoxIcon icon,
          MessageBoxDefaultButton defaultButton,
          MessageBoxOptions options);

        /// <summary>
        /// ���������� ����������� ������
        /// </summary>
        /// <param name="message">���������</param>
        private void ShowException(string message)
        {
            ShowException(message, false);
        }

        /// <summary>
        /// ���������� ������ � ����������� ������� � ������� ������ ������ �������:
        /// ����������� ��� ���������
        /// ���������� ��������� � ����������� ������, � ����� � ��� ���
        /// ��� ������ �� ��������� ������� ����� ��������� ��� ����� ���������� 
        /// �� ������������ ������� �����, � �� �� ���������, � ��� ����� ���������
        /// � 3�� �������� �������� ��� ������ � ������ ������������� ������ -
        /// MessageBox.Show ������������ � ��������� ������.
        /// </summary>
        /// <param name="message">���������</param>
        /// <param name="asyncM�de">True - ��������� ������������ � ������������ ������</param>
        private void ShowException(string message, bool asyncM�de)
        {
            string errorMessage = String.Format("����������� ������! {0}", message);

            // ��������� ��� ������ "������������� ������"
            // 1066L ERROR_SERVICE_SPECIFIC_ERROR - 
            // The service has returned a service-specific error code.
            // ���������� �� serviceSpecificExitCode �� �������, �.�.
            // ��� �������� ������.
            ExitCode = ERROR_SERVICE_SPECIFIC_ERROR;

            EventLog.WriteEntry(errorMessage, EventLogEntryType.Error);

            ErrorHandler.Handle(new ApplicationException(message));

            if (asyncM�de)
            {
                new ShowMsgDelegate(MessageBox.Show).BeginInvoke(
                  errorMessage, String.Format("������ {0}", ServiceName),
                  MessageBoxButtons.OK,
                  MessageBoxIcon.Error,
                  MessageBoxDefaultButton.Button1,
                  MessageBoxOptions.ServiceNotification,
                  null, null);
            }
            else
            {
                MessageBox.Show(
                  errorMessage, String.Format("������ {0}", ServiceName),
                  MessageBoxButtons.OK,
                  MessageBoxIcon.Error,
                  MessageBoxDefaultButton.Button1,
                  MessageBoxOptions.ServiceNotification);
            }
        }

        /// <summary>
        /// �������� ��������� ����������� � ������
        /// </summary>
        protected virtual void SendStartMessage()
        {
            MessageSpec message = new MessageSpec();
            message.msgType = MessageType.CMD_START;
            ((IMsgDispatcher)mainController).Post(message);
        }

        /// <summary>
        /// �������� ��������� ����������� �� ���������
        /// </summary>
        protected virtual void SendStopMessage()
        {
            Logger.Write(GetStoppingMessage());

            MessageSpec message = new MessageSpec();
            message.msgType = MessageType.CMD_STOP;
            ((IMsgDispatcher)mainController).Post(message);

            waitControllerStopEvent.WaitOne(ADDITIONAL_TIME - 10000, false);

            mainController = null;
            Logger.Write(GetStoppedMessage());
        }

        /// <summary>
        /// ��������� ������� ������� �����������
        /// </summary>
        protected virtual void HandleControllerStarted()
        {
            Logger.Write(GetRunMessage());
        }

        /// <summary>
        /// ��������� ������� ��������� �����������
        /// </summary>
        protected virtual void HandleControllerStopped()
        {
            waitControllerStopEvent.Set();
        }

        #region ����� � ��� ���������

        /// <summary>
        /// \r\n *** ������ {0}({1}) ����������� *** \r\n
        /// </summary>
        private const string START_MESSAGE =
          "\r\n *** ������ {0}({1}) ����������� *** \r\n";

        /// <summary>
        /// ��������� ������ �������� ������ ������
        /// </summary>
        /// <returns>String</returns>
        protected virtual string GetStartMessage()
        {
            return String.Format(START_MESSAGE, ServiceName, SrvSettings.ServiceName);
        }

        /// <summary>
        /// \r\n *** ������ {0}({1}) ����������� *** \r\n
        /// </summary>
        private const string STOP_MESSAGE =
          "\r\n *** ������ {0}({1}) ����������� *** \r\n";

        /// <summary>
        /// ��������� �� ��������� ������
        /// </summary>
        /// <returns>String</returns>
        protected virtual string GetStopMessage()
        {
            return String.Format(STOP_MESSAGE, ServiceName, SrvSettings.ServiceName);
        }

        /// <summary>
        /// \r\n ---------------------\r\n ������ �����������... \r\n ---------------------\r\n
        /// </summary>
        private const string SRV_RUNNING_MESSAGE =
          "\r\n ---------------------\r\n ������ �����������... \r\n ---------------------\r\n";
        /// <summary>
        /// \r\n\r\n ������ ������� ��������� \r\n
        /// </summary>
        private const string SRV_RUN_MESSAGE = "\r\n\r\n ������ ������� ��������� \r\n";

        /// <summary>
        /// ��������� ������ �������� ������ �������
        /// </summary>
        /// <returns>String</returns>
        protected virtual string GetRunningMessage()
        {
            return SRV_RUNNING_MESSAGE;
        }

        /// <summary>
        /// ��������� � ������ � ������� ����� �������
        /// </summary>
        /// <returns>String</returns>
        protected virtual string GetRunMessage()
        {
            return SRV_RUN_MESSAGE;
        }

        /// <summary>
        /// \r\n\r\n ������ ���������������...\r\n
        /// </summary>
        private const string SRV_STOPPING_MESSAGE = "\r\n\r\n ������ ���������������...\r\n";
        /// <summary>
        /// \r\n ---------------------\r\n ������ ���������� \r\n ---------------------\r\n
        /// </summary>
        private const string SRV_STOPPED_MESSAGE =
          "\r\n ---------------------\r\n ������ ���������� \r\n ---------------------\r\n";

        /// <summary>
        /// ��������� ������ �������� ��������� �������
        /// </summary>
        /// <returns>String</returns>
        protected virtual string GetStoppingMessage()
        {
            return SRV_STOPPING_MESSAGE;
        }

        /// <summary>
        /// ��������� �� ��������� �������
        /// </summary>
        /// <returns>String</returns>
        protected virtual string GetStoppedMessage()
        {
            return SRV_STOPPED_MESSAGE;
        }

        private void InitializeComponent()
        {
            this.CanShutdown = true;
        }

        #endregion
    }
}
