// Project: SrvKernel, File: MsgObject.cs
// Namespace: RCS.SrvKernel.SrvSystem, Class: MsgObject
// Path: D:\Development\RfDbServer_1_1\SrvKernel\SrvSystem, Author: guschin
// Code lines: 90, Size of file: 2.50 KB
// Creation date: 7/23/2008 3:23 PM
// Last modified: 7/24/2008 9:00 AM
// Generated with Commenter by abi.exDream.com

#region Using directives
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvSystem.Messages;
using System;
using System.Threading;
#endregion

namespace RCS.SrvKernel.SrvSystem
{
    /// <summary>
    /// Базовый класс инкапсулирующий механизм 
    /// обработки сообщений. От него наследуются
    /// классы которым требуется этот механизм.
    /// </summary>
    public class MsgObject : IMsgDispatcher
    {
        /// <summary>
        /// внутреннее имя объекта
        /// </summary>
        protected string Name
        {
            get { return GetName(); }
        }

        /// <summary>
        /// Перекрывается наследниками для определения
        /// имени объекта
        /// </summary>
        /// <returns>String</returns>
        protected virtual string GetName()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "GetName", "MsgObject"));
        }

        /// <summary>
        /// Диспетчер сообщений
        /// </summary>
        protected IMsgDispatcher iMsgDispatcher;

        public MsgObject()
        {
            MsgDispatcher obj = new MsgDispatcher(Name);
            obj.EventHandleMessage += new HandleMessageDelegate(OnHandleMessage);
            iMsgDispatcher = (IMsgDispatcher) obj;
        }

        /// <summary>
        /// Обработка сообщений
        /// </summary>
        /// <param name="message">MessageSpec</param>
        private void OnHandleMessage(MessageSpec message)
        {
            HandleMessage(message);
        }

        /// <summary>
        /// Обрабока сообщений. Перекрывается в потомках для 
        /// спецефической для потомка обработки сообщений.
        /// </summary>
        /// <param name="message">MessageSpec</param>
        protected virtual void HandleMessage(MessageSpec message)
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "HandleMessage", "MsgObject"));
        }

        /// <summary>
        /// Выполнение остановочных операций в контексте потока выполнения
        /// сообщений
        /// </summary>
        protected void StopMessageThread()
        {
            MessageSpec message = new MessageSpec();
            message.msgType = MessageType.STOP_EXECUTING_Thread;
            Post(message);
        }

        #region IMsgDispatcher Members

        /// <summary>
        /// Отправить сообщение
        /// </summary>
        /// <param name="message">MessageSpec сообщение</param>
        /// <returns>истина если сообщение поставлено в очередь на обработку</returns>
        public virtual bool Post(MessageSpec message)
        {
            if (message.msgType == MessageType.CMD_STOP)
                SrvSettings.StopProcess = true;

            bool result = iMsgDispatcher.Post(message);
            return result;
        }

        #endregion
    }
}
