using RCS.Sockets.Connection;
using RCS.SrvKernel.SrvCommand.Sending;

namespace RCS.SrvKernel.SrvCommand
{
  /// <summary>
  /// ��������� ����� ��������� �������
  /// </summary>
  public interface ISrvCommand
  {
    /// <summary>
    /// ��� �������
    /// </summary>
    int CommandID { get; }
    /// <summary>
    /// ���������� ��������� �������
    /// </summary>
    string CommandDescr { get; }
    /// <summary>
    /// ��������� �� ������� �����
    /// </summary>
    bool NeedResponse { get; }
    /// <summary>
    /// �������� �����������
    /// </summary>
    ConnectionInfo ConnectionContext { get; }
    /// <summary>
    /// ���������� �� ������� ������� ����� ������
    /// </summary>
    bool IsInsertFunc();
    /// <summary>
    /// ��������� �������
    /// </summary>
    SendingInfo Execute();
  }
} 
