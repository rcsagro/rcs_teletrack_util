using RCS.Sockets.Connection;

namespace RCS.SrvKernel.SrvCommand.Sending
{
  /// <summary>
  /// ��� ����������� ���������� ��� ������
  /// </summary>
  public class SendingInfo
  {
    /// <summary>
    /// ��� ���������� �������
    /// </summary>
    public int CommandID;
    /// <summary>
    /// ������������� ������(���������� �����)
    /// </summary>
    public int PacketID;
    /// <summary>
    /// ���������� ������������� �������
    /// </summary>
    public string CommandName;
    /// <summary>
    /// ���(�����) ��������
    /// </summary>
    public SendingType Mode = SendingType.SINGLE;
    /// <summary>
    /// ������ ������
    /// </summary>
    //TODO: ResponseStatus (������ ������) ��� �� �����
    public ExecutionStatus Status;
    /// <summary>
    /// "����" ������
    /// </summary>
    public byte[] Body;
    /// <summary>
    /// ���������� � ��������� �����������
    /// </summary>
    public ConnectionInfo ConnectionCtx;
  }
}
