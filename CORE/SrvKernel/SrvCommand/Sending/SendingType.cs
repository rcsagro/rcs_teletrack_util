// Project: SrvLib, File: ResponseTypes.cs
// Namespace: RCS.SrvLib.Response, Class: 
// Path: D:\Development\SrvService\SrvLib\Response, Author: guschin
// Code lines: 90, Size of file: 2.28 KB
// Creation date: 2/29/2008 11:58 AM
// Last modified: 4/15/2008 12:15 PM
// Generated with Commenter by abi.exDream.com

namespace RCS.SrvKernel.SrvCommand.Sending
{

  #region Тип отправки

  /// <summary>
  /// Тип отправки
  /// </summary>
  public enum SendingType
  {
    /// <summary>
    /// Отправка конкретному получателю
    /// </summary>
    SINGLE,
    /// <summary>
    /// Отправка всем зарегистрированным клиентам
    /// </summary>
    BROADCAST
  }

  #endregion

  #region Статус ответа

  /// <summary>
  /// Статус обработки данных
  /// </summary>
  public enum ExecutionStatus
  {
    /// <summary>
    /// Все впорядке
    /// </summary>
    OK = 0,
    /// <summary>
    /// Ошибка при работе с БД
    /// </summary>
    ERROR_DB = 1,
    /// <summary>
    /// присутсвуют ошибочные значения в полях
    /// или др. ошибки
    /// </summary>
    ERROR_DATA = 2
  }

  #endregion
} 
