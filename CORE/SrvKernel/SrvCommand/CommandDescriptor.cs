// Project: SrvKernel, File: CommandDescriptor.cs
// Namespace: RCS.SrvKernel.SrvCommand, Class: CommandDescriptor
// Path: D:\Development\SrvService_0_3\SrvKernel\SrvCommand, Author: guschin
// Code lines: 65, Size of file: 1.81 KB
// Creation date: 6/2/2008 5:27 PM
// Last modified: 6/4/2008 3:25 PM
// Generated with Commenter by abi.exDream.com

#region Using directives
using System.Collections.Generic;
#endregion

namespace RCS.SrvKernel.SrvCommand
{
  /// <summary>
  /// ��������� ������������������ ������.
  /// <para>� ���������� �������� ���������
  /// ����� ��������� MainController'�,
  /// � ������� ������������ ������������ �������
  /// </para>
  /// </summary>
  public static class CommandDescriptor
  {
    private static Dictionary<int, string> commands;

      public static Protocol.A1.CommandDescriptor ZoneConfigSet
      {
          get { return Protocol.A1.CommandDescriptor.ZoneConfigSet; }
      }

      /// <summary>
    /// ������������� ����������� � MainController'� ���������� �������� 
    /// </summary>
    /// <param name="cmdDescription">Dictionary(int, string) � ������������������� ���������</param>
    public static void Init(Dictionary<int, string> cmdDescription)
    {
      commands = cmdDescription;
    }
  
    /// <summary>
    /// ���������������� �� �������
    /// </summary>
    /// <param name="cmdCode">��� �������</param>
    /// <returns>Bool</returns>
    public static bool IsCommandExist(int cmdCode)
    {
      if (commands.ContainsKey(cmdCode))
        return true;
      else
        return false;
    }

    /// <summary>
    /// ���������� ��������� �������
    /// </summary>
    /// <param name="cmdCode">Cmd code</param>
    /// <returns>String</returns>
    public static string GetDescription(int cmdCode)
    {
      if (commands.ContainsKey(cmdCode))
        return commands[cmdCode];
      else
        return "UNKNOWN COMMAND";
    }
  }
}
