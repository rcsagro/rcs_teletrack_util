using System;

namespace RCS.SrvKernel.SrvCommand
{
  /// <summary>
  /// ���������� ���������� �������
  /// </summary>
  struct Statistics
  {
    /// <summary>
    /// ����� ������ ���������� �������
    /// </summary>
    public DateTime StartTime;
    /// <summary>
    /// ����� ��������� ���������� �������
    /// </summary>
    public DateTime FinishTime;
  }
}
