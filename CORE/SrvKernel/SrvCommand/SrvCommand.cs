#region Using directives
using System;
using RCS.Sockets.Connection;
using RCS.SrvKernel.DataManagement;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand.Sending;
using RCS.SrvKernel.Util;
#endregion

namespace RCS.SrvKernel.SrvCommand
{
	public static class LogIdUtil
	{
		public static UInt32 logIdPreviouse = 0;
		public static bool flagFirstLogIdSave = false;
	}
	
	/// <summary>
	/// Базовый класс команд
	/// </summary>
	public class SrvCommand : ISrvCommand
	{
		/// <summary>
		/// Требуется ли формировать ответ
		/// </summary>
		/// <returns>Bool</returns>
		public bool NeedResponse {
			get { return GetNeedResponse(); }
		}

		protected virtual bool GetNeedResponse()
		{
			return true;
		}

		/// <summary>
		/// Статистика
		/// </summary>
		private Statistics statistics;

		/// <summary>
		/// Код команды 
		/// </summary>
		public int CommandID {
			get { return GetCommandID(); }
		}

		protected virtual int GetCommandID()
		{
			throw new NotImplementedException(String.Format(
				AppError.NOT_IMPLEMENTED, "GetCommandID", "SrvCommand"));
		}

		/// <summary>
		/// Символьный описатель команды
		/// </summary>
		public string CommandDescr {
			get { return GetCommandDescr(); }
		}

		protected virtual string GetCommandDescr()
		{
			throw new NotImplementedException(String.Format(
				AppError.NOT_IMPLEMENTED, "GetCommandDescr", "SrvCommand"));
		}

		#region Ответ

		/// <summary>
		/// Код ответа
		/// </summary>
		protected virtual int GetResponseCode()
		{
			throw new NotImplementedException(String.Format(
				AppError.NOT_IMPLEMENTED, "GetResponseCode", "SrvCommand"));
		}

		/// <summary>
		/// Статус ответа
		/// </summary>
		protected ExecutionStatus responseStatus;

		/// <summary>
		/// Объект подключения
		/// </summary>
		public ConnectionInfo ConnectionContext {
			get { return connectionContext; }
		}
		private ConnectionInfo connectionContext;

		#endregion

		private SrvCommand()
		{
		}

		public SrvCommand(BaseDataManager dataMngr, ConnectionInfo connectionContext)
		{
			SetDataManager(dataMngr);

			this.connectionContext = connectionContext;

			statistics = new Statistics();
			statistics.StartTime = DateTime.Now;
		}

		private BaseDataManager dataManager;

		/// <summary>
		/// Обязательная установка менеджера данных
		/// </summary>
		/// <param name="dataMngr">BaseDataManager</param>
		protected virtual void SetDataManager(BaseDataManager dataMngr)
		{
			this.dataManager = dataMngr;
		}

		/// <summary>
		/// Выполнить команду
		/// </summary>
		/// <returns>Response info</returns>
		public SendingInfo Execute()
		{
			SendingInfo result = null;

			try {
				InternalExecute();
				if (NeedResponse) {
					result = CreateResponse();
				}
				FinallCommit();
			} catch (Exception Ex) {
				ErrorHandler.Handle(Ex);
			}

			return result;
		}

		/// <summary>
		/// Производит ли команда вставку новых данных
		/// </summary>
		/// <returns>Bool</returns>
		public virtual bool IsInsertFunc()
		{
			return false;
		}

		/// <summary>
		/// Основная обработка выполнения команды
		/// </summary>
		protected virtual void InternalExecute()
		{
			throw new NotImplementedException(String.Format(
				AppError.NOT_IMPLEMENTED, "InternalExecute", "SrvCommand"));
		}

		/// <summary>
		/// Формирование ответа
		/// </summary>
		/// <returns>Bool</returns>
		private SendingInfo CreateResponse()
		{
			SendingInfo result = new SendingInfo();
			result.CommandID = GetResponseCode();
			result.CommandName = GetCommandDescr();
			result.Mode = GetSendingType();
			// TODO: responseStatus устарел
			result.Status = responseStatus;
			result.Body = GetResponseBody();
			result.ConnectionCtx = ConnectionContext;
			return result;
		}

		/// <summary>
		/// Получить тело ответа
		/// </summary>
		/// <returns>byte[]</returns>
		protected virtual byte[] GetResponseBody()
		{
			throw new NotImplementedException(String.Format(
				AppError.NOT_IMPLEMENTED, "GetResponseBody", "SrvCommand"));
		}

		/// <summary>
		/// Получить режим отправки: конкретный получатель или всем кто на связи
		/// </summary>
		/// <returns>SendingType</returns>
		protected virtual SendingType GetSendingType()
		{
			return SendingType.SINGLE;
		}

		/// <summary>
		/// Последние действия 
		/// </summary>
		protected virtual void FinallCommit()
		{
			statistics.FinishTime = DateTime.Now;
		}

		/// <summary>
		/// Парсинг строки новых данных
		/// </summary>
		/// <param name="mobitelID">Mobitel ID</param>
		/// <param name="packet">Пакет DataGps - массив байт</param>
		/// <param name="devIdShort">короткий идентификатор - для дебага</param>
		/// <param name="extFields">Дополнительные поля, которые надо пристыковать к строке DataGps</param>
		/// <param name="parsedRow">распарсенная строка</param>
		/// <returns>True если все в порядке</returns>
		protected bool GetParsedRow(int mobitelID, byte[] packet, string devIdShort, string[] extFields, out string parsedRow)
		{
			if (packet == null) {
				throw new ArgumentNullException(String.Format(AppError.ARGUMENT_NULL,
					"GetParsedRow", "SrvCommand", "byte[] packet"));
			}
			if (String.IsNullOrEmpty(devIdShort)) {
				throw new ArgumentNullException(String.Format(AppError.ARGUMENT_NULL,
					"GetParsedRow", "SrvCommand", "string devIdShort"));
			}
			bool result = true;
			parsedRow = "";
			string tmp = "";
			DataGpsInfo64 dataGps64 = null;
			
			try {
				if (packet.Length == ParserServices.DATA_GPS_BYTE_COUNT_64) {
					dataGps64 = DataGpsParser64.GetDataGpsRow(mobitelID, packet);
					parsedRow = dataGps64.GetAssembledRow();
					tmp = dataGps64.GetDebugRow();
				} else {
					DataGpsInfo dataGps = DataGpsParser.GetDataGpsRow(mobitelID, packet);
					parsedRow = dataGps.GetAssembledRow();
					tmp = dataGps.GetDebugRow();
				}

				// Добавление дополнительных полей
				if ((extFields != null) && (extFields.Length > 0)) {
					foreach (string field in extFields) {
						parsedRow = String.Concat(parsedRow, DataGpsInfo.DELIMITER, field);
					}
				}
				if (SrvSettings.DebugMode) {
					const string received_msg = "Пакет от телетрека {0}({1}) принят для сохранения в БД {2}: \r\n  {3}";
					// Добавление дополнительных полей
					if ((extFields != null) && (extFields.Length > 0)) {
						foreach (string field in extFields) {
							tmp = String.Concat(tmp, "; ", field);
						}
					}
                
					Logger.Write(String.Format(received_msg, mobitelID, devIdShort,
						dataManager.ConnectionStringBase, tmp));
				}
				
				// Если значение LogId текущего пакета отличается на очень большую величину от LogId предыдущего пакета, 
				// то такой пакет отбрасываем.
//				if (LogIdUtil.flagFirstLogIdSave) {
//					Int32 resultLogId = (Int32)(Convert.ToUInt32(dataGps64.DataGps32.LogID) - LogIdUtil.logIdPreviouse);
//					if (resultLogId > 100000) {
//						throw new Exception(String.Format(AppError.LOG_ID_VERY_BIG_VALUE, mobitelID, dataGps64.DataGps32.LogID, LogIdUtil.logIdPreviouse, resultLogId));
//					} 
//				} 
//				else {
//					LogIdUtil.flagFirstLogIdSave = true;
//				}
//					
//				LogIdUtil.logIdPreviouse = Convert.ToUInt32(dataGps64.DataGps32.LogID);

			} catch (Exception Ex) {
				result = false;
				ErrorHandler.Handle(Ex);
			}
			
			return result;
		}
	}
} 
