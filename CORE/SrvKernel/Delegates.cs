// Project: SrvLib, File: Delegates.cs
// Namespace: RCS.SrvLib, Class: EventArgsEx
// Path: D:\Development\SrvService\SrvLib, Author: guschin
// Code lines: 39, Size of file: 1.00 KB
// Creation date: 2/20/2008 2:19 PM
// Last modified: 4/2/2008 12:08 PM
// Generated with Commenter by abi.exDream.com

#region Using directives
using System;
using RCS.SrvKernel.SrvCommand.Sending;
#endregion

namespace RCS.SrvKernel
{
  /// <summary>
  /// �������� ������ �������� ����������� ��� 
  /// ���������� ���������
  /// </summary>
  /// <param name="parameter">object</param>
  public delegate void HandleCommandDelegate(object parameter);
  /// <summary>
  /// �������� ��������� ������������
  /// </summary>
  /// <param name="SResponse">ResponseInfo</param>
  delegate void SendDelegate(SendingInfo response);
  /// <summary>
  /// ���������� ������� � ����������� ������  
  /// </summary>
  /// <returns>ResponseInfo</returns>
  delegate SendingInfo ExecuteDelegate();
  /// <summary>
  /// ����� �������
  /// </summary>
  /// <param name="sender">object</param>
  /// <param name="e">EventArgsEx</param>
  public delegate void CommonEventDelegate(object sender, EventArgsEx args);
  /// <summary>
  /// ������� ��� ������������ �������� �������
  /// </summary>
  public delegate void SimpleEventDelegate();


  /// <summary>
  /// Event arguments exception
  /// </summary>
  public class EventArgsEx : EventArgs
  {
    public string Message = "";
  }
}
