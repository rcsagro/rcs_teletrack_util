using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� �� �� �������� ���������� ����������
    /// </summary>
    class A1SelectConfigUnique : SrvDbCommand
    {
        private const string QUERY =
            "SELECT COALESCE(dsptId, '') AS DispatcherID, " +
            " COALESCE(pswd, '') AS Password, " +
            " COALESCE(tmpPswd, '') AS TmpPassword " +
            "FROM ConfigUnique " +
            "WHERE Message_ID = ?MessageId " +
            "LIMIT 1";

        private const string msQUERY =
            "SELECT TOP 1 COALESCE(dsptId, '') AS DispatcherID, " +
            " COALESCE(pswd, '') AS Password, " +
            " COALESCE(tmpPswd, '') AS TmpPassword " +
            "FROM ConfigUnique " +
            "WHERE Message_ID = @MessageId ";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1SelectConfigUnique(DriverDb connection)
            : base(connection)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
                db.NewSqlParameter("?MessageId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
                db.NewSqlParameter("@MessageId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1SelectConfigUnique", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigUnique",
                    initObjects.Length, 1), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "MessageId", (int) initObjects[0]);
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            // ((MySqlConnection) Connection).Open();
            //db.CommandConnectionOpen();
            return db.CommandExecuteReader(); // MyDbCommand.ExecuteReader();
        }
    }
}
