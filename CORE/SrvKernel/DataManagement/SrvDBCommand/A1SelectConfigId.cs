using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� �� �� �������� ����������������� ����������
    /// </summary>
    class A1SelectConfigId : SrvDbCommand
    {
        private const string QUERY =
            "SELECT * FROM InternalMobitelConfig WHERE Message_ID = ?MessageId";

        private const string msQUERY =
            "SELECT * FROM InternalMobitelConfig WHERE Message_ID = @MessageId";

        DriverDb connect;
        DriverDb commdb = new DriverDb();

        public A1SelectConfigId(DriverDb connection)
            : base(connection)
        {
            connect = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.Text);
            commdb.CommandText("");
            commdb.CommandParametersClear();
            connect.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                connect.CommandText(QUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
                connect.NewSqlParameter("?MessageId", connect.GettingInt32());
                connect.CommandParametersAdd(connect.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                connect.CommandText(msQUERY);
                connect.NewSqlParameter("@MessageId", connect.GettingInt32());
                connect.CommandParametersAdd(connect.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1SelectConfigId", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigId",
                    initObjects.Length, 1), "object[] initObjects");

            connect.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
            connect.CommandParametersValue(connect.ParamPrefics + "MessageId", (int) initObjects[0]);
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = (MySqlConnection)Connection;
            connect.CommandSqlConnection(connect.SqlConnection);
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            return connect.CommandExecuteReader(); // MyDbCommand.ExecuteReader();
        }
    }
}
