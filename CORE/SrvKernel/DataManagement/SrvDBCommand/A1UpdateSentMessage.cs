using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ���������� ����� ������� Messages �������� ������ ����� �������� ���������
    /// </summary>
    internal class A1UpdateSentMessage : SrvDbCommand
    {
        private const string QUERY =
            "UPDATE Messages " +
            "SET IsNew = 0, IsSend = 1, Time3 = Unix_Timestamp(?Time), Source_ID = ?SourceId " +
            "WHERE Message_ID = ?MessageId";

        private const string msQUERY =
            "UPDATE Messages " +
            "SET IsNew = 0, IsSend = 1, Time3 = dbo.UNIX_TIMESTAMPS(@Time), Source_ID = @SourceId " +
            "WHERE Message_ID = @MessageId";

        private DriverDb db;
        private DriverDb commdb = new DriverDb();

        public A1UpdateSentMessage(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandText("");
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
                db.NewSqlParameter("?MessageId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?SourceId", MySqlDbType.Int32));
                db.NewSqlParameter("?SourceId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?Time", MySqlDbType.DateTime));
                db.NewSqlParameter("?Time", db.GettingDateTime());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(msQUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
                db.NewSqlParameter("@MessageId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?SourceId", MySqlDbType.Int32));
                db.NewSqlParameter("@SourceId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?Time", MySqlDbType.DateTime));
                db.NewSqlParameter("@Time", db.GettingDateTime());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// 2. SourceId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1UpdateSentMessage", "object[] initObjects"));
            if (initObjects.Length != 2)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1UpdateSentMessage",
                    initObjects.Length, 2), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "MessageId", (int) initObjects[0]);
            //MyDbCommand.Parameters["?SourceId"].Value = (int)initObjects[1];
            db.CommandParametersValue(db.ParamPrefics + "SourceId", (int) initObjects[1]);
            //MyDbCommand.Parameters["?Time"].Value = DateTime.Now;
            db.CommandParametersValue(db.ParamPrefics + "Time", DateTime.Now);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
