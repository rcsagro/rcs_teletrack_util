using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� ���������� ����������
    /// </summary>
    class A1InsertConfigUnique : SrvDbCommand
    {
        private const string QUERY =
            "INSERT INTO ConfigUnique (Name, Descr, Message_ID, pswd, ID) " +
            "VALUES (?Name, ?Descr, ?Message_ID, ?pswd, " +
            " (COALESCE((SELECT ConfigUnique_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1)))";

        private const string msQUERY =
            "INSERT INTO ConfigUnique (Name, Descr, Message_ID, pswd, ID) " +
            "VALUES (@Name, @Descr, @Message_ID, @pswd, " +
            " (COALESCE((SELECT TOP 1 ConfigUnique_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1)))";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1InsertConfigUnique(DriverDb connection) : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
                db.NewSqlParameter("?Name", db.GettingString());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
                db.CommandParametersAdd(db.GetSqlParameter);
                db.NewSqlParameter("?Descr", db.GettingString());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
                db.CommandParametersAdd(db.GetSqlParameter);
                db.NewSqlParameter("?Message_ID", db.GettingInt32());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
                db.CommandParametersAdd(db.GetSqlParameter);
                db.NewSqlParameter("?MobitelId", db.GettingInt32());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
                db.CommandParametersAdd(db.GetSqlParameter);
                db.NewSqlParameter("?pswd", db.GettingString());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?pswd", MySqlDbType.String));
                db.CommandParametersAdd(db.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
                db.NewSqlParameter("@Name", db.GettingString());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
                db.CommandParametersAdd(db.GetSqlParameter);
                db.NewSqlParameter("@Descr", db.GettingString());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
                db.CommandParametersAdd(db.GetSqlParameter);
                db.NewSqlParameter("@Message_ID", db.GettingInt32());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
                db.CommandParametersAdd(db.GetSqlParameter);
                db.NewSqlParameter("@MobitelId", db.GettingInt32());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
                db.CommandParametersAdd(db.GetSqlParameter);
                db.NewSqlParameter("@pswd", db.GettingString());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?pswd", MySqlDbType.String));
                db.CommandParametersAdd(db.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigUnique_ID)
        /// 3. ������
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1InsertConfigUnique", "object[] initObjects"));
            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigUnique",
                    initObjects.Length, 3), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;

            //MyDbCommand.Parameters["?Name"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Name", "");
            //MyDbCommand.Parameters["?Descr"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Descr", "");
            //MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "Message_ID", (int) initObjects[0]);
            //MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];
            db.CommandParametersValue(db.ParamPrefics + "MobitelId", (int) initObjects[1]);
            // MyDbCommand.Parameters["?pswd"].Value = (string)initObjects[2];
            db.CommandParametersValue(db.ParamPrefics + "pswd", (string) initObjects[2]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
