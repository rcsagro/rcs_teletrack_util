#region Using directives
using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.Protocol.A1.Entity;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

#endregion

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� SMS �������
    /// </summary>
    class A1InsertConfigSms : SrvDbCommand
    {
        private const string QUERY = "INSERT Into configsms (Name, Descr, Message_ID, " +
                                     "smsCenter_ID, smsDspt_ID, smsEmailGate_ID, dsptEmail, dsptGprsEmail, ID) " +
                                     "VALUES (?Name, ?Descr, ?Message_ID, " +
                                     "(COALESCE((SELECT SmsNumber_ID FROM SmsNumbers WHERE smsNumber = ?SmsCenterNumber LIMIT 1), -1)), " +
                                     "(COALESCE((SELECT SmsNumber_ID FROM SmsNumbers WHERE smsNumber = ?SmsDsptNumber LIMIT 1), -1)), " +
                                     "(COALESCE((SELECT SmsNumber_ID FROM SmsNumbers WHERE smsNumber = ?SmsEmailGateNumber LIMIT 1), -1)), " +
                                     "?dsptEmail, ?dsptGprsEmail, " +
                                     "(COALESCE((SELECT ConfigSms_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1)))";

        private const string msQUERY = "INSERT Into configsms (Name, Descr, Message_ID, " +
                                       "smsCenter_ID, smsDspt_ID, smsEmailGate_ID, dsptEmail, dsptGprsEmail, ID) " +
                                       "VALUES (@Name, @Descr, @Message_ID, " +
                                       "(COALESCE((SELECT TOP 1 SmsNumber_ID FROM SmsNumbers WHERE smsNumber = @SmsCenterNumber), -1)), " +
                                       "(COALESCE((SELECT TOP 1 SmsNumber_ID FROM SmsNumbers WHERE smsNumber = @SmsDsptNumber), -1)), " +
                                       "(COALESCE((SELECT TOP 1 SmsNumber_ID FROM SmsNumbers WHERE smsNumber = @SmsEmailGateNumber), -1)), " +
                                       "@dsptEmail, @dsptGprsEmail, " +
                                       "(COALESCE((SELECT TOP 1 ConfigSms_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1)))";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1InsertConfigSms(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
            }

            //MyDbCommand.Parameters.Add( new MySqlParameter( "?Name", MySqlDbType.String ) );
            db.NewSqlParameter("?Name", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);

            //MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
            db.NewSqlParameter("?Descr", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
            db.NewSqlParameter("?Message_ID", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
            db.NewSqlParameter("?MobitelId", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?SmsCenterNumber", MySqlDbType.String));
            db.NewSqlParameter("?SmsCenterNumber", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?SmsDsptNumber", MySqlDbType.String));
            db.NewSqlParameter("?SmsDsptNumber", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?SmsEmailGateNumber", MySqlDbType.String));
            db.NewSqlParameter("?SmsEmailGateNumber", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?dsptEmail", MySqlDbType.String));
            db.NewSqlParameter("?dsptEmail", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?dsptGprsEmail", MySqlDbType.String));
            db.NewSqlParameter("?dsptGprsEmail", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigSms_ID)
        /// 3. SmsAddrConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1InsertConfigSms", "object[] initObjects"));
            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigSms",
                    initObjects.Length, 3), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;

            //MyDbCommand.Parameters["?Name"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Name", "");
            //MyDbCommand.Parameters["?Descr"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Descr", "");
            //MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "Message_ID", (int) initObjects[0]);
            //MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];
            db.CommandParametersValue(db.ParamPrefics + "MobitelId", (int) initObjects[1]);

            SmsAddrConfig config = (SmsAddrConfig) initObjects[2];
            //MyDbCommand.Parameters["?SmsCenterNumber"].Value = config.SmsCentre;
            db.CommandParametersValue(db.ParamPrefics + "SmsCenterNumber", config.SmsCentre);
            //MyDbCommand.Parameters["?SmsDsptNumber"].Value = config.SmsDspt;
            db.CommandParametersValue(db.ParamPrefics + "SmsDsptNumber", config.SmsDspt);
            //MyDbCommand.Parameters["?SmsEmailGateNumber"].Value = config.SmsEmailGate;
            db.CommandParametersValue(db.ParamPrefics + "SmsEmailGateNumber", config.SmsEmailGate);
            //MyDbCommand.Parameters["?dsptEmail"].Value = config.DsptEmailSMS;
            db.CommandParametersValue(db.ParamPrefics + "dsptEmail", config.DsptEmailSMS);
            //MyDbCommand.Parameters["?dsptGprsEmail"].Value = config.DsptEmailGprs;
            db.CommandParametersValue(db.ParamPrefics + "dsptGprsEmail", config.DsptEmailGprs);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
