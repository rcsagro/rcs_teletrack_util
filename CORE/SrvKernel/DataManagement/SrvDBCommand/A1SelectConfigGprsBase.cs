using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� �� �� �������� �������� GPRS
    /// </summary>
    class A1SelectConfigGprsBase : SrvDbCommand
    {
        private const string QUERY =
            "SELECT Mode, Apnserv, Apnun, Apnpw, " +
            " Dnsserv1, Dialn1, Ispun, Isppw " +
            "FROM ConfigGprsMain " +
            "WHERE Message_ID = ?MessageId";

        private const string msQUERY =
            "SELECT Mode, Apnserv, Apnun, Apnpw, " +
            " Dnsserv1, Dialn1, Ispun, Isppw " +
            "FROM ConfigGprsMain " +
            "WHERE Message_ID = @MessageId";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1SelectConfigGprsBase(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
                db.NewSqlParameter("?MessageId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
            else
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(msQUERY);
                //MyDbCommand.Parameters.Add( new MySqlParameter( "?MessageId", MySqlDbType.Int32 ) );
                db.NewSqlParameter("@MessageId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1SelectConfigGprsBase", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigGprsBase",
                    initObjects.Length, 1), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "MessageId", (int) initObjects[0]);
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            db.CommandConnectionOpen();
            return db.CommandExecuteReader(); // MyDbCommand.ExecuteReader();
        }
    }
}
