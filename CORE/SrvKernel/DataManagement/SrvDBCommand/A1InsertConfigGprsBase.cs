using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.Protocol.A1.Entity;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� �������� GPRS 
    /// </summary>
    class A1InsertConfigGprsBase : SrvDbCommand
    {
        private const string QUERY =
            "INSERT INTO ConfigGprsMain (Name, Descr, Message_ID, " +
            " Mode, Apnserv, Apnun, Apnpw, Dnsserv1, Dialn1, Ispun, Isppw, ID)  " +
            "VALUES (?Name, ?Descr, ?Message_ID,  " +
            " ?Mode, ?Apnserv, ?Apnun, ?Apnpw, ?Dnsserv1, ?Dialn1, ?Ispun, ?Isppw, " +
            " COALESCE((SELECT ConfigGprsMain_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

        private const string msQUERY =
            "INSERT INTO ConfigGprsMain (Name, Descr, Message_ID, " +
            " Mode, Apnserv, Apnun, Apnpw, Dnsserv1, Dialn1, Ispun, Isppw, ID)  " +
            "VALUES (@Name, @Descr, @Message_ID,  " +
            " @Mode, @Apnserv, @Apnun, @Apnpw, @Dnsserv1, @Dialn1, @Ispun, @Isppw, " +
            " COALESCE((SELECT TOP 1 ConfigGprsMain_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1))";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1InsertConfigGprsBase(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.CommandType = CommandType.Text;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(msQUERY);
            }

            //MyDbCommand.Parameters.Add( new MySqlParameter( "?Name", MySqlDbType.String ) );
            db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Descr", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Message_ID", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "MobitelId", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Mode", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Mode", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Apnserv", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Apnserv", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Apnun", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Apnun", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Apnpw", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Apnpw", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Dnsserv1", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Dnsserv1", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Dialn1", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Dialn1", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Ispun", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Ispun", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Isppw", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Isppw", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigGprsMain_ID)
        /// 3. GprsBaseConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1InsertConfigGprsBase", "object[] initObjects"));
            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigGprsBase",
                    initObjects.Length, 3), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;

            // MyDbCommand.Parameters["?Name"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Name", "");
            //MyDbCommand.Parameters["?Descr"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Descr", "");
            //MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "Message_ID", (int) initObjects[0]);
            //MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];
            db.CommandParametersValue(db.ParamPrefics + "MobitelId", (int) initObjects[1]);

            GprsBaseConfig config = (GprsBaseConfig) initObjects[2];
            //MyDbCommand.Parameters["?Mode"].Value = config.Mode;
            db.CommandParametersValue(db.ParamPrefics + "Mode", config.Mode);
            // MyDbCommand.Parameters["?Apnserv"].Value = config.ApnServer;
            db.CommandParametersValue(db.ParamPrefics + "Apnserv", config.ApnServer);
            //MyDbCommand.Parameters["?Apnun"].Value = config.ApnLogin;
            db.CommandParametersValue(db.ParamPrefics + "Apnun", config.ApnLogin);
            // MyDbCommand.Parameters["?Apnpw"].Value = config.ApnPassword;
            db.CommandParametersValue(db.ParamPrefics + "Apnpw", config.ApnPassword);
            // MyDbCommand.Parameters["?Dnsserv1"].Value = config.DnsServer;
            db.CommandParametersValue(db.ParamPrefics + "Dnsserv1", config.DnsServer);
            // MyDbCommand.Parameters["?Dialn1"].Value = config.DialNumber;
            db.CommandParametersValue(db.ParamPrefics + "Dialn1", config.DialNumber);
            // MyDbCommand.Parameters["?Ispun"].Value = config.GprsLogin;
            db.CommandParametersValue(db.ParamPrefics + "Ispun", config.GprsLogin);
            // MyDbCommand.Parameters["?Isppw"].Value = config.GprsPassword;
            db.CommandParametersValue(db.ParamPrefics + "Isppw", config.GprsPassword);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
