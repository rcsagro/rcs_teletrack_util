using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.Protocol.A1.Entity;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� GPRS Email
    /// </summary>
    class A1InsertConfigGprsEmail : SrvDbCommand
    {
        private const string QUERY =
            "INSERT INTO ConfigGprsEmail (Name, Descr, Message_ID, " +
            " Smtpserv, Smtpun, Smtppw, Pop3serv, Pop3un, Pop3pw, ID) " +
            "VALUES (?Name, ?Descr, ?Message_ID,  " +
            " ?Smtpserv, ?Smtpun, ?Smtppw, ?Pop3serv, ?Pop3un, ?Pop3pw,  " +
            " COALESCE((SELECT ConfigGprsEmail_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

        private const string msQUERY =
            "INSERT INTO ConfigGprsEmail (Name, Descr, Message_ID, " +
            " Smtpserv, Smtpun, Smtppw, Pop3serv, Pop3un, Pop3pw, ID) " +
            "VALUES (@Name, @Descr, @Message_ID,  " +
            " @Smtpserv, @Smtpun, @Smtppw, @Pop3serv, @Pop3un, @Pop3pw,  " +
            " COALESCE((SELECT TOP 1 ConfigGprsEmail_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1))";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1InsertConfigGprsEmail(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
            }

            //MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Descr", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Message_ID", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "MobitelId", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Smtpserv", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Smtpserv", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Smtpun", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Smtpun", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Smtppw", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Smtppw", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Pop3serv", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Pop3serv", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Pop3un", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Pop3un", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Pop3pw", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Pop3pw", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigGprsEmail_ID)
        /// 3. GprsEmailConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1InsertConfigGprsEmail", "object[] initObjects"));
            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigGprsEmail",
                    initObjects.Length, 3), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;

            // MyDbCommand.Parameters["?Name"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Name", "");
            //MyDbCommand.Parameters["?Descr"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Descr", "");
            // MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "Message_ID", (int) initObjects[0]);
            //MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];
            db.CommandParametersValue(db.ParamPrefics + "MobitelId", (int) initObjects[1]);

            GprsEmailConfig config = (GprsEmailConfig) initObjects[2];
            // MyDbCommand.Parameters["?Smtpserv"].Value = config.SmtpServer;
            db.CommandParametersValue(db.ParamPrefics + "Smtpserv", config.SmtpServer);
            // MyDbCommand.Parameters["?Smtpun"].Value = config.SmtpLogin;
            db.CommandParametersValue(db.ParamPrefics + "Smtpun", config.SmtpLogin);
            // MyDbCommand.Parameters["?Smtppw"].Value = config.SmtpPassword;
            db.CommandParametersValue(db.ParamPrefics + "Smtppw", config.SmtpPassword);
            // MyDbCommand.Parameters["?Pop3serv"].Value = config.Pop3Server;
            db.CommandParametersValue(db.ParamPrefics + "Pop3serv", config.Pop3Server);
            // MyDbCommand.Parameters["?Pop3un"].Value = config.Pop3Login;
            db.CommandParametersValue(db.ParamPrefics + "Pop3un", config.Pop3Login);
            // MyDbCommand.Parameters["?Pop3pw"].Value = config.Pop3Password;
            db.CommandParametersValue(db.ParamPrefics + "Pop3pw", config.Pop3Password);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            // MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
