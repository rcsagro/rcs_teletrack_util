// Project: SrvKernel, File: A1InsertMessagesDbCommand.cs
// Namespace: RCS.SrvKernel.DataManagement.SrvDBCommand, Class: A1InsertMessagesDbCommand
// Path: D:\Development\TDataManager_Head\SrvKernel\DataManagement\SrvDBCommand, Author: guschin
// Code lines: 20, Size of file: 629 Bytes
// Creation date: 10.11.2008 15:25
// Last modified: 10.11.2008 15:36
// Generated with Commenter by abi.exDream.com

using System;
using System.Data;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using MySql.Data.MySqlClient;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� ������ � ������� Messages
    /// </summary>
    class A1InsertMessages : SrvDbCommand
    {
        private const string QUERY = "INSERT INTO Messages " +
                                 "(Time1, Time2, isNew, Direction, Command_ID, Source_ID, " +
                                 "Address, DataFromService, Mobitel_ID, crc, MessageCounter) " +
                                 "VALUES (?Time1, ?Time2, ?isNew, ?Direction, ?Command_ID, ?Source_ID, " +
                                 "?Address, ?DataFromService, ?Mobitel_ID, ?crc, ?MessageCounter)";

        private const string msQUERY = "INSERT INTO Messages " +
                                   "(Time1, Time2, isNew, Direction, Command_ID, Source_ID, " +
                                   "Address, DataFromService, Mobitel_ID, crc, MessageCounter) " +
                                   "VALUES (@Time1, @Time2, @isNew, @Direction, @Command_ID, @Source_ID, " +
                                   "@Address, @DataFromService, @Mobitel_ID, @crc, @MessageCounter)";

        /// <summary>
        /// �������� ���������� ����� Message_ID ����� ������,
        /// ������� ������ ��� ���������
        /// </summary>
        private int newMessageId;

        /// <summary>
        /// �������� ���������� ����� Message_ID ����� ������,
        /// ������� ������ ��� ���������
        /// </summary>
        public int NewMessageId
        {
            get { return newMessageId; }
        }

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1InsertMessages(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand; 

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
            }

            // MyDbCommand.Parameters.Add(new MySqlParameter("?Time1", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Time1", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Time2", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Time2", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?isNew", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "isNew", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Direction", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Direction", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Command_ID", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Command_ID", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Source_ID", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Source_ID", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Address", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Address", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?DataFromService", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "DataFromService", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Mobitel_ID", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Mobitel_ID", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?crc", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "crc", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageCounter", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "MessageCounter", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Time1 
        /// 2. Command_ID
        /// 3. Source_ID
        /// 4. Address(Email)
        /// 5. Mobitel_ID
        /// 6. MessageCounter
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1InsertMessages", "object[] initObjects"));
            if (initObjects.Length != 6)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertMessages",
                    initObjects.Length, 6), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;
            newMessageId = 0;
            // MyDbCommand.Parameters["?Time1"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "Time1", (int)initObjects[0]);
            // MyDbCommand.Parameters["?Time2"].Value = 0;
            db.CommandParametersValue(db.ParamPrefics + "Time2", 0);
            //  MyDbCommand.Parameters["?isNew"].Value = 1;
            db.CommandParametersValue(db.ParamPrefics + "isNew", 1);
            // MyDbCommand.Parameters["?Direction"].Value = 0;
            db.CommandParametersValue(db.ParamPrefics + "Direction", 0);
            //  MyDbCommand.Parameters["?Command_ID"].Value = (int)initObjects[1];
            db.CommandParametersValue(db.ParamPrefics + "Command_ID", (int)initObjects[1]);
            // MyDbCommand.Parameters["?Source_ID"].Value = (int)initObjects[2];
            db.CommandParametersValue(db.ParamPrefics + "Source_ID", (int)initObjects[2]);
            // MyDbCommand.Parameters["?Address"].Value = (string)initObjects[3];
            db.CommandParametersValue(db.ParamPrefics + "Address", (string)initObjects[3]);
            // MyDbCommand.Parameters["?DataFromService"].Value = "Data";
            db.CommandParametersValue(db.ParamPrefics + "DataFromService", "Data");
            //  MyDbCommand.Parameters["?Mobitel_ID"].Value = (int)initObjects[4];
            db.CommandParametersValue(db.ParamPrefics + "Mobitel_ID", (int)initObjects[4]);
            //  MyDbCommand.Parameters["?crc"].Value = 1;
            db.CommandParametersValue(db.ParamPrefics + "crc", 1);
            //  MyDbCommand.Parameters["?MessageCounter"].Value = (int)initObjects[5];
            db.CommandParametersValue(db.ParamPrefics + "MessageCounter", (int)initObjects[5]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            //newMessageId = (int)MyDbCommand.LastInsertedId;
            newMessageId = (int)db.CommandLastInsertedId();

            return ExecutionStatus.OK;
        }
    }
}
