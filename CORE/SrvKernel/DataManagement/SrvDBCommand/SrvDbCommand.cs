#region Using directives

using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Text;
using System.Threading;
using RCS.SrvKernel.SrvCommand.Sending;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RWLog.DriverDataBase;

#endregion

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// Базовый класc команд работающих с базой данных  
    /// </summary>
    public class SrvDbCommand : ISrvDbCommand
    {
        /// <summary>
        /// Время ожидания перед повторным выполнением команды 30 секунд
        /// </summary>
        private const int SLEEPING_TIME = 30000;

        /// <summary>
        /// Строка подключения
        /// </summary>
        protected DriverDb connectionS;

        //private MySqlConnection connection;
        private object connection = null; // постоянно включенное глобальное соединение
        //protected DriverDb db = new DriverDb();

        /// <summary>
        /// Коннекшен устанавливается = null 
        /// в методе CloseConnection()
        /// </summary>
        //protected MySqlConnection Connection

        protected object Connection
        {
            get
            {
                if (connection == null)
                {
                    //connection = new MySqlConnection(ConnectionString);
                    //connection = db.NewSqlConnection(ConnectionString);
                    connection = connectionS.GetConnection();
                }

                return connection;
            }
        }

        private static object syncObject = new object();

        //protected MySqlCommand MyDbCommand;
        //protected object MyDbCommand;

        protected SrvDbCommand()
        {
        }

        public SrvDbCommand(DriverDb connection)
        {
            if ((connection == null) || (connection.CS.Length == 0))
            {
                throw new ArgumentNullException(
                    "connectionString", AppError.DB_NULL_DATAMANAGER_CTOR);
            }

            connectionS = connection;

            //MyDbCommand = new MySqlCommand();
            //connectionS.NewSqlCommand();
            //connectionS.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            //MyDbCommand = connectionS.GetSqlCommand();
            //MyDbCommand.CommandTimeout = SrvSettings.DataBaseCommandTimeout;
        }

        /// <summary>
        /// Инициализация команды перед выполнением
        /// </summary>
        /// <param name="initObjects">массив объектов инициализации</param>
        public virtual void Init(params object[] initObjects)
        {
        }

        /// <summary>
        /// Закрыть соединение, если требуется.
        /// Обязательно закрывать после использования reader
        /// </summary>
        public void CloseConnection()
        {
            ConnectionState stateConn = ConnectionState.Closed;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                stateConn = ((MySqlConnection) Connection).State;
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                stateConn = ((SqlConnection) Connection).State;
            }

            if (stateConn != ConnectionState.Closed)
            {
                try
                {
                    if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
                    {
                        ((MySqlConnection) Connection).Close();
                    }
                    else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
                    {
                        ((SqlConnection) Connection).Close();
                    }
                }
                catch (Exception Ex)
                {
                    ErrorHandler.Handle(Ex);
                }

                connection = null;

                if (SrvSettings.DebugMode)
                {
                    TraceCloseConnection();
                }
            }
        }

        private void InitExecuting()
        {
            isBusy = true;
            if (SrvSettings.DebugMode)
            {
                TraceStartCommand();
            }
        }

        /// <summary>
        /// Требуется ли повторное выполнение команды
        /// после первого "провала"
        /// </summary>
        /// <returns>Bool</returns>
        protected virtual bool NeedExecuteAfterFailure()
        {
            return true;
        }

        /// <summary>
        /// Выполнить команду.
        /// <para>
        /// При возникновении исключения тихо его обрабатывает,
        /// регистрирует
        /// </para>
        /// </summary>
        public ExecutionStatus Execute()
        {
            lock (syncObject)
            {
                InitExecuting();
                ExecutionStatus result = ExecutionStatus.OK;

                try
                {
                    result = InternalExecute();
                }
                catch (Exception Ex)
                {
                    HandleExecutingException(Ex);
                    result = ExecutionStatus.ERROR_DB;

                    if (NeedExecuteAfterFailure())
                    {
                        // Пробуем повторно запустить 
                        // через небольшой интервал времени
                        try
                        {
                            Thread.Sleep(SLEEPING_TIME);
                            result = TryExecute();
                        }
                        catch (Exception ExInner)
                        {
                            HandleExecutingException(ExInner);
                        }
                    }
                }
                finally
                {
                    //CloseConnection();
                    Logger.Write("Execute: Connection with server attempt clousing!");
                    isBusy = false;
                }

                return result;
            }
        }

        private ExecutionStatus TryExecute()
        {
            InitExecuting();
            return InternalExecute();
        }

        /// <summary>
        /// Выполнить команду и возвращает DataReader и ExecutionStatus
        /// </summary>
        /// <param name="status">ResponseStatus</param>
        /// <returns>MySqlDataReader</returns>
        public object ExecuteReader(out ExecutionStatus status)
        {
            lock (syncObject)
            {
                status = ExecutionStatus.OK;
                object result = null;
                try
                {
                    result = TryExecuteReader();
                }
                catch (Exception Ex)
                {
                    HandleExecutingException(Ex);
                    status = ExecutionStatus.ERROR_DB;

                    if (NeedExecuteAfterFailure())
                    {
                        // Пробуем повторно запустить
                        // через небольшой интервал времени
                        try
                        {
                            Thread.Sleep(SLEEPING_TIME);
                            result = TryExecuteReader();
                            status = ExecutionStatus.OK;
                        }
                        catch (Exception ExInner)
                        {
                            HandleExecutingException(ExInner);
                            status = ExecutionStatus.ERROR_DB;
                        }
                    }
                }
                finally
                {
                    isBusy = false;
                }

                return result;
            }
        }

        /// <summary>
        /// Запись в лог информации об ошибках и закрытие соединения
        /// </summary>
        /// <param name="ExInner">Exception</param>
        private void HandleExecutingException(Exception ExInner)
        {
            string keyMassage = String.Format(AppError.DB_COMMAND_EXECUTING_FAILED,
                this.GetType().Name, connectionS.CS, ExInner.Message);
            ErrorHandler.Handle(new ApplicationException(keyMassage));
            ErrorHandler.Handle(ExInner);
            //CloseConnection();
            Logger.Write("HandleExecutingException: Connection with server attempt clousing!");
        }

        /// <summary>
        /// Try execute reader
        /// </summary>
        /// <returns>My sql data reader</returns>
        private object TryExecuteReader()
        {
            InitExecuting();
            return InternalExecuteReader();
        }

        /// <summary>
        /// Здесь скрыта специфика реализации 
        /// выполнения соответствующей команды
        /// </summary>
        protected virtual ExecutionStatus InternalExecute()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "InternalExecute", "DbCommand"));
        }

        protected virtual object InternalExecuteReader()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "InternalExecuteReader", "DbCommand"));
        }

        private bool isBusy;

        public bool IsBusy
        {
            get { return isBusy; }
        }

        /// <summary>
        /// Информация о команде которая будет исполнена
        /// </summary>
        protected virtual void TraceStartCommand()
        {
            const string strParamsInfo = "\r\n  Параметр: {0}, значение: {1}";

            StringBuilder paramsInfo = new StringBuilder("");

            //foreach (MySqlParameter parameter in MyDbCommand.Parameters)
            //{
            // paramsInfo.Append(String.Format(strParamsInfo,
            //   parameter.ParameterName,
            //   Convert.ToString(parameter.Value)));
            //  }

            for (int i = 0; i < connectionS.GetParametersCount; i++)
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlParameter parameter = (MySqlParameter) connectionS.GetParameters(i);
                    paramsInfo.Append(String.Format(strParamsInfo, parameter.ParameterName,
                        Convert.ToString(parameter.Value)));
                }
                else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
                {
                    SqlParameter parameter = (SqlParameter) connectionS.GetParameters(i);
                    paramsInfo.Append(String.Format(strParamsInfo, parameter.ParameterName,
                        Convert.ToString(parameter.Value)));
                }
            }

            //WriteDebug(String.Format(
            //  "Команда: {0}, строка подключения: {1}.\r\n Текст команды: {2}\r\n Тип команды: {3}\r\n Параметры:{4}",
            //  this.GetType().Name,
            //  ConnectionString, MyDbCommand.CommandText, MyDbCommand.CommandType.ToString(), paramsInfo.ToString()));

            WriteDebug(String.Format(
                "Команда: {0}, строка подключения: {1}.\r\n Текст команды: {2}\r\n Тип команды: {3}\r\n Параметры:{4}",
                this.GetType().Name,
                connectionS.CS, connectionS.GetCommandText, connectionS.GetCommandType, paramsInfo));
        }

        /// <summary>
        /// Информация о закрытии соединения командой
        /// </summary>
        private void TraceCloseConnection()
        {
            WriteDebug(String.Format("Команда {0} закрыла соединение с базой {1} \r\n",
                this.GetType().Name, connectionS.CS));
        }

        protected void WriteDebug(string message)
        {
            Logger.WriteDebug("$  ", message);
        }
    }
}
