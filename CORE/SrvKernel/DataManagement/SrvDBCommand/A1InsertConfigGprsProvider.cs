using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.Protocol.A1.Entity;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� GPRS Provider
    /// </summary>
    class A1InsertConfigGprsProvider : SrvDbCommand
    {
        private const string QUERY =
            "INSERT INTO ConfigGprsInit (Name, Descr, Message_ID, Domain, InitString, ID) " +
            "VALUES (?Name, ?Descr, ?Message_ID, ?Domain, ?InitString, " +
            " COALESCE((SELECT ConfigGprsInit_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

        private const string msQUERY =
            "INSERT INTO ConfigGprsInit (Name, Descr, Message_ID, Domain, InitString, ID) " +
            "VALUES (@Name, @Descr, @Message_ID, @Domain, @InitString, " +
            " COALESCE((SELECT TOP 1 ConfigGprsInit_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1))";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1InsertConfigGprsProvider(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;
            
            //MyDbCommand.CommandType = CommandType.Text;
            db.CommandType(CommandType.Text);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
            }
            else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(msQUERY);
            }

            //MyDbCommand.Parameters.Add(new MySqlParameter(db.ParamPrefics + "Name", db.GettingString()));
            db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add( new MySqlParameter( db.ParamPrefics + "Descr", db.GettingString() ) );
            db.NewSqlParameter(db.ParamPrefics + "Descr", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add( new MySqlParameter( db.ParamPrefics + "Message_ID", db.GettingInt32() ) );
            db.NewSqlParameter(db.ParamPrefics + "Message_ID", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add( new MySqlParameter( db.ParamPrefics + "MobitelId", db.GettingInt32() ) );
            db.NewSqlParameter(db.ParamPrefics + "MobitelId", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add( new MySqlParameter( db.ParamPrefics + "Domain", db.GettingString() ) );
            db.NewSqlParameter(db.ParamPrefics + "Domain", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add( new MySqlParameter( db.ParamPrefics + "InitString", db.GettingString() ) );
            db.NewSqlParameter(db.ParamPrefics + "InitString", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigGprsInit_ID)
        /// 3. GprsProviderConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1InsertConfigGprsProvider", "object[] initObjects"));
            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigGprsProvider",
                    initObjects.Length, 3), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;

            //MyDbCommand.Parameters["?Name"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Name", "");
            //MyDbCommand.Parameters["?Descr"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Descr", "");
            //MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "Message_ID", (int) initObjects[0]);
            //MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];
            db.CommandParametersValue(db.ParamPrefics + "MobitelId", (int) initObjects[1]);

            GprsProviderConfig config = (GprsProviderConfig) initObjects[2];
            //MyDbCommand.Parameters["?Domain"].Value = config.Domain;
            db.CommandParametersValue(db.ParamPrefics + "Domain", config.Domain);
            // MyDbCommand.Parameters["?InitString"].Value = config.Message;
            db.CommandParametersValue(db.ParamPrefics + "InitString", config.Message);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
