using System.Data;
using MySql.Data.MySqlClient;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� ��������������� ���������� � ��������� �����
    /// ���������-������ ��������������� ��� ��������� ����������
    /// </summary>
    class A1SelectMobitelsInNewMessages : SrvDbCommand
    {
        private const string QUERY =
            "SELECT DISTINCT msg.Mobitel_ID AS MobitelId " +
            "FROM Messages msg JOIN Mobitels m ON m.Mobitel_id = msg.Mobitel_id " +
            " JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID) " +
            "WHERE (msg.isNew = 1) AND (msg.Direction = 1) AND " +
            " (c.InternalMobitelConfig_ID = ( " +
            "   SELECT intConf.InternalMobitelConfig_ID " +
            "   FROM internalmobitelconfig intConf " +
            "   WHERE (intConf.ID = c.ID)  " +
            "   ORDER BY intConf.InternalMobitelConfig_ID DESC " +
            "   LIMIT 1)) AND " +
            " (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')";

        private const string msQUERY =
            "SELECT DISTINCT msg.Mobitel_ID AS MobitelId " +
            "FROM Messages msg JOIN Mobitels m ON m.Mobitel_id = msg.Mobitel_id " +
            " JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID) " +
            "WHERE (msg.isNew = 1) AND (msg.Direction = 1) AND " +
            " (c.InternalMobitelConfig_ID = ( " +
            "   SELECT TOP 1 intConf.InternalMobitelConfig_ID " +
            "   FROM internalmobitelconfig intConf " +
            "   WHERE (intConf.ID = c.ID)  " +
            "   ORDER BY intConf.InternalMobitelConfig_ID DESC " +
            "   )) AND " +
            " (c.devIdShort IS NOT NULL) AND (c.devIdShort <> '')";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1SelectMobitelsInNewMessages(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
            }
        }

        protected override object InternalExecuteReader()
        {
            db.SetNewCommand = commdb.GetCommand;

            //MyDbCommand.Connection = (MySqlConnection)Connection;
            db.CommandSqlConnection(db.SqlConnection);

            // cmd = (MySqlConnection)Connection;
            //Connection.Open();
            //cmd.Open();
            //db.CommandConnectionOpen();
            return db.CommandExecuteReader(); //MyDbCommand.ExecuteReader();
        }
    }
}
