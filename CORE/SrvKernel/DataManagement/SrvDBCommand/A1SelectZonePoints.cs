using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� �� �� �������� ����� ����������� ����
    /// </summary>
    class A1SelectZonePoints : SrvDbCommand
    {
        private const string QUERY =
            "SELECT Latitude, Longitude FROM Points WHERE Zone_ID = ?ZoneId";

        private const string msQUERY =
            "SELECT Latitude, Longitude FROM Points WHERE Zone_ID = @ZoneId";

        DriverDb connect;
        DriverDb commdb = new DriverDb();

        public A1SelectZonePoints(DriverDb connection)
            : base(connection)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            connect = connection;
            commdb.NewSqlCommand();
            commdb.CommandText("");
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            connect.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                connect.CommandParametersClear();
                //MyDbCommand.CommandText = QUERY;
                connect.CommandText(QUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?ZoneId", MySqlDbType.Int32));
                connect.NewSqlParameter("?ZoneId", connect.GettingInt32());
                connect.CommandParametersAdd(connect.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                connect.CommandParametersClear();
                connect.CommandText(msQUERY);
                connect.NewSqlParameter("?ZoneId", connect.GettingInt32());
                connect.CommandParametersAdd(connect.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. ZoneId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1SelectZonePoints", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectZonePoints",
                    initObjects.Length, 1), "object[] initObjects");

            connect.SetNewCommand = commdb.GetCommand;
            // MyDbCommand.Parameters["?ZoneId"].Value = (int)initObjects[0];
            connect.CommandParametersValue(connect.ParamPrefics + "ZoneId", (int) initObjects[0]);
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = (MySqlConnection)Connection;
            connect.CommandSqlConnection(connect.SqlConnection);
            //((MySqlConnection)Connection).Open();
            //connect.CommandConnectionOpen();
            return connect.CommandExecuteReader(); // MyDbCommand.ExecuteReader();
        }
    }
}
