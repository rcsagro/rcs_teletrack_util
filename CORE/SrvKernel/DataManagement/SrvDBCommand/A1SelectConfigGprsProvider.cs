using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� �� �� �������� GPRS Provider
    /// </summary>
    class A1SelectConfigGprsProvider : SrvDbCommand
    {
        private const string QUERY =
            "SELECT Domain, InitString FROM ConfigGprsInit WHERE Message_ID = ?MessageId";

        private const string msQUERY =
            "SELECT Domain, InitString FROM ConfigGprsInit WHERE Message_ID = @MessageId";

        DriverDb connect;
        DriverDb commdb = new DriverDb();

        public A1SelectConfigGprsProvider(DriverDb connection)
            : base(connection)
        {
            connect = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            connect.SetNewCommand = commdb.GetCommand;

            //MyDbCommand.CommandText = QUERY;
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                connect.CommandText(QUERY);
                connect.NewSqlParameter("?MessageId", connect.GettingInt32());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
                connect.CommandParametersAdd(connect.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                connect.CommandText(msQUERY);
                connect.NewSqlParameter("@MessageId", connect.GettingInt32());
                connect.CommandParametersAdd(connect.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1SelectConfigGprsProvider", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigGprsProvider",
                    initObjects.Length, 1), "object[] initObjects");

            connect.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
            connect.CommandParametersValue(connect.ParamPrefics + "MessageId", (int) initObjects[0]);
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = (MySqlConnection)Connection;
            connect.CommandSqlConnection(connect.SqlConnection);
            // (( MySqlConnection ) Connection).Open();
            //db.CommandConnectionOpen();
            return connect.CommandExecuteReader(); // MyDbCommand.ExecuteReader();
        }
    }
}
