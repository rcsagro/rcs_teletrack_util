using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� ���-�� ����� �������������� ��������� ������ ��� ����������� ����������
    /// </summary>
    class A1SelectNewMessageCount : SrvDbCommand
    {
        private const string QUERY =
            "SELECT COUNT(msg.Message_ID) " +
            "FROM Messages msg JOIN Mobitels m ON m.Mobitel_id = msg.Mobitel_id " +
            " JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID) " +
            "WHERE (msg.isNew = 1) AND (msg.Direction = 1) AND " +
            " (c.InternalMobitelConfig_ID = ( " +
            "   SELECT intConf.InternalMobitelConfig_ID " +
            "   FROM internalmobitelconfig intConf " +
            "   WHERE (intConf.ID = c.ID)  " +
            "   ORDER BY intConf.InternalMobitelConfig_ID DESC " +
            "   LIMIT 1)) AND " +
            " (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')";

        private const string msQUERY =
            "SELECT COUNT(msg.Message_ID) " +
            "FROM Messages msg JOIN Mobitels m ON m.Mobitel_id = msg.Mobitel_id " +
            " JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID) " +
            "WHERE (msg.isNew = 1) AND (msg.Direction = 1) AND " +
            " (c.InternalMobitelConfig_ID = ( " +
            "   SELECT TOP 1 intConf.InternalMobitelConfig_ID " +
            "   FROM internalmobitelconfig intConf " +
            "   WHERE (intConf.ID = c.ID)  " +
            "   ORDER BY intConf.InternalMobitelConfig_ID DESC " +
            "   )) AND " +
            " (c.devIdShort IS NOT NULL) AND (c.devIdShort <> '')";

        /// <summary>
        /// ���-�� ���������
        /// </summary>
        private int count;

        /// <summary>
        /// ���-�� ���������
        /// </summary>
        public int Count
        {
            get { return count; }
        }

        DriverDb globDb; // ���������� ����������
        DriverDb localDb = new DriverDb(); // ��������� ����������
        DriverDb commdb = new DriverDb(); // ��������� ������� ��� ��

        private int WaitForEndOfRead()
        {
            int cnt = 0;
            int ok = 0;
            int err = -1;

            ConnectionState State = ConnectionState.Fetching;

            while (State == ConnectionState.Fetching)
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    State = ((MySqlConnection) localDb.SqlConnection).State;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    State = ((SqlConnection) localDb.SqlConnection).State;
                }

                Thread.Sleep(1000);
                cnt++;
                if (cnt == 10)
                {
                    return err;
                }
            }

            return ok;
        }

        /// <summary>
        /// Create A1 select new messages
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public A1SelectNewMessageCount(DriverDb connection)
            : base(connection)
        {
            //globDb = connection;
            localDb.SqlConnection = connection.SqlConnection;
            //localDb.NewSqlConnection();
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            localDb.SetNewCommand = commdb.GetCommand;

            //MyDbCommand.CommandText = QUERY;
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                localDb.CommandText(QUERY);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                localDb.CommandText(msQUERY);
            }
        }

        protected override ExecutionStatus InternalExecute()
        {
            try
            {
                if (WaitForEndOfRead() == 0)
                {

                    //localDb.SetNewCommand = commdb.GetCommand;

                    // MyDbCommand.Connection = ( MySqlConnection ) Connection;
                    localDb.CommandSqlConnection(localDb.SqlConnection);
                    //((MySqlConnection)Connection).Open();
                    //db.CommandConnectionOpen();
                    //count = Convert.ToInt32(MyDbCommand.ExecuteScalar());
                    count = Convert.ToInt32(localDb.CommandExecuteScalar());
                    commdb.CloseSqlCommand();
                    //localDb.SqlConnectionClose();
                }
                else
                {
                    commdb.CloseSqlCommand();
                    //localDb.SqlConnectionClose();
                    return ExecutionStatus.ERROR_DB;
                }
            }
            catch (AbandonedMutexException ex)
            {
                commdb.CloseSqlCommand();
                //localDb.SqlConnectionClose();
                return ExecutionStatus.ERROR_DATA;
            }

            return ExecutionStatus.OK;
        }
    }
}
