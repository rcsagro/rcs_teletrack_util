using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� �� �� �������� ���������� �������
    /// </summary>
    class A1SelectConfigPhone : SrvDbCommand
    {
        private const string QUERY =
            "SELECT COALESCE(num1.TelNumber, '') AS NumberSOS, " +
            " COALESCE(num2.TelNumber, '') AS NumberDspt, " +
            " COALESCE(num3.TelNumber, '') AS NumberAccept1, " +
            " COALESCE(num3.Name, '') AS Name1, " +
            " COALESCE(num4.TelNumber, '') AS NumberAccept2, " +
            " COALESCE(num4.Name, '') AS Name2, " +
            " COALESCE(num5.TelNumber, '') AS NumberAccept3, " +
            " COALESCE(num5.Name, '') AS Name3 " +
            "FROM ConfigTel cfg LEFT JOIN TelNumbers num1 ON num1.TelNumber_ID = cfg.TelSOS  " +
            " LEFT JOIN TelNumbers num2 ON num2.TelNumber_ID = cfg.TelDspt " +
            " LEFT JOIN TelNumbers num3 ON num3.TelNumber_ID = cfg.telAccept1 " +
            " LEFT JOIN TelNumbers num4 ON num4.TelNumber_ID = cfg.telAccept2 " +
            " LEFT JOIN TelNumbers num5 ON num5.TelNumber_ID = cfg.telAccept3 " +
            "WHERE cfg.Message_ID = ?MessageId " +
            "LIMIT 1";

        private const string msQUERY =
            "SELECT TOP 1 COALESCE(num1.TelNumber, '') AS NumberSOS, " +
            " COALESCE(num2.TelNumber, '') AS NumberDspt, " +
            " COALESCE(num3.TelNumber, '') AS NumberAccept1, " +
            " COALESCE(num3.Name, '') AS Name1, " +
            " COALESCE(num4.TelNumber, '') AS NumberAccept2, " +
            " COALESCE(num4.Name, '') AS Name2, " +
            " COALESCE(num5.TelNumber, '') AS NumberAccept3, " +
            " COALESCE(num5.Name, '') AS Name3 " +
            "FROM ConfigTel cfg LEFT JOIN TelNumbers num1 ON num1.TelNumber_ID = cfg.TelSOS  " +
            " LEFT JOIN TelNumbers num2 ON num2.TelNumber_ID = cfg.TelDspt " +
            " LEFT JOIN TelNumbers num3 ON num3.TelNumber_ID = cfg.telAccept1 " +
            " LEFT JOIN TelNumbers num4 ON num4.TelNumber_ID = cfg.telAccept2 " +
            " LEFT JOIN TelNumbers num5 ON num5.TelNumber_ID = cfg.telAccept3 " +
            "WHERE cfg.Message_ID = @MessageId ";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1SelectConfigPhone(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
                db.NewSqlParameter("?MessageId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
                db.NewSqlParameter("@MessageId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1SelectConfigPhone", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigPhone",
                    initObjects.Length, 1), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "MessageId", (int) initObjects[0]);
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            return db.CommandExecuteReader(); // MyDbCommand.ExecuteReader();
        }
    }
}
