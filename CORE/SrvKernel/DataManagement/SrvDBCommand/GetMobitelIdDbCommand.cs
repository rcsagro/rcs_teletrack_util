// Project: SrvKernel, File: GetMobitelIdDbCommand.cs
// Namespace: RCS.SrvKernel.DataManagement.SrvDBCommand, Class: GetMobitelIdDbCommand
// Path: D:\Development\TDataManager_Head\SrvKernel\DataManagement\SrvDBCommand, Author: guschin
// Code lines: 92, Size of file: 2,90 KB
// Creation date: 11.11.2008 14:55
// Last modified: 11.11.2008 15:10
// Generated with Commenter by abi.exDream.com

using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ����� Mobitel_ID �� �������� DevIdShort
    /// </summary>
    internal class GetMobitelIdDbCommand : SrvDbCommand
    {
        private const string QUERY =
            "SELECT m.Mobitel_ID AS MobitelID " +
            "FROM mobitels m JOIN internalmobitelconfig c  " +
            "  ON (c.ID = m.InternalMobitelConfig_ID) " +
            "WHERE (c.InternalMobitelConfig_ID = ( " +
            "  SELECT intConf.InternalMobitelConfig_ID " +
            "  FROM internalmobitelconfig intConf " +
            "  WHERE (intConf.ID = c.ID)  " +
            "  ORDER BY intConf.InternalMobitelConfig_ID DESC " +
            "  LIMIT 1)) AND (c.devIdShort = ?devIdShort) ";

        private const string msQUERY =
            "SELECT m.Mobitel_ID AS MobitelID " +
            "FROM mobitels m JOIN internalmobitelconfig c  " +
            "  ON (c.ID = m.InternalMobitelConfig_ID) " +
            "WHERE (c.InternalMobitelConfig_ID = ( " +
            "  SELECT TOP 1 intConf.InternalMobitelConfig_ID " +
            "  FROM internalmobitelconfig intConf " +
            "  WHERE (intConf.ID = c.ID)  " +
            "  ORDER BY intConf.InternalMobitelConfig_ID DESC " +
            "  )) AND (c.devIdShort = @devIdShort) ";

        private int mobitelId;

        /// <summary>
        /// MobitelId
        /// </summary>
        public int MobitelId
        {
            get { return mobitelId; }
        }

        private DriverDb db;
        private DriverDb commdb = new DriverDb();

        public GetMobitelIdDbCommand(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandText("");
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?devIdShort", MySqlDbType.String));
                db.NewSqlParameter("?devIdShort", db.GettingString());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?devIdShort", MySqlDbType.String));
                db.NewSqlParameter("@devIdShort", db.GettingVarChar());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. devIdShort
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "GetMobitelIdDbCommand", "object[] initObjects"));

            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "GetMobitelIdDbCommand",
                    initObjects.Length, 1), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;
            mobitelId = -1;
            //MyDbCommand.Parameters["?devIdShort"].Value = (string)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "devIdShort", (string) initObjects[0]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = (MySqlConnection)Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //(( MySqlConnection ) Connection).Open();
            //db.CommandConnectionOpen();
            object mobitelIdValue = db.CommandExecuteScalar(); //MyDbCommand.ExecuteScalar();

            if (mobitelIdValue == DBNull.Value)
            {
                return ExecutionStatus.ERROR_DATA;
            }
            else
            {
                mobitelId = Convert.ToInt32(mobitelIdValue);
                return ExecutionStatus.OK;
            }
        }
    }
}
