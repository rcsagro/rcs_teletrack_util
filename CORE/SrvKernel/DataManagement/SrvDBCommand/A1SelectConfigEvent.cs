using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� �� �� �������� �������
    /// </summary>
    internal class A1SelectConfigEvent : SrvDbCommand
    {
        private const string QUERY =
            "SELECT * FROM ConfigEvent " +
            "WHERE Message_ID = ?MessageId " +
            "LIMIT 1";

        private const string msQUERY =
            "SELECT TOP 1 * FROM ConfigEvent " +
            "WHERE Message_ID = @MessageId ";

        private DriverDb db;
        private DriverDb commdb = new DriverDb();

        public A1SelectConfigEvent(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
                db.NewSqlParameter("?MessageId", db.GettingInt32());
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
                db.CommandParametersAdd(db.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(msQUERY);
                db.NewSqlParameter("@MessageId", db.GettingInt32());
                //MyDbCommand.Parameters.Add( new MySqlParameter( "@MessageId", MySqlDbType.Int32 ) );
                db.CommandParametersAdd(db.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1SelectConfigEvent", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigEvent",
                    initObjects.Length, 1), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
            db.CommandParametersValue( db.ParamPrefics + "MessageId", ( int )initObjects[0] );
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = (MySqlConnection)Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            return db.CommandExecuteReader(); //MyDbCommand.ExecuteReader();
        }
    }
}
