using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.Protocol.A1.Entity;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ���������� ����������������� ����������
    /// </summary>
    class A1InsertConfigId : SrvDbCommand
    {
        private const string QUERY =
            "INSERT INTO InternalMobitelConfig (Name, Descr, Message_ID, " +
            " devIdShort, devIdLong, verProtocolShort, verProtocolLong, " +
            " moduleIdGps, moduleIdGsm, moduleIdRf, moduleIdSs, moduleIdMm, ID) " +
            "VALUES (?Name, ?Descr, ?Message_ID, " +
            " ?devIdShort, ?devIdLong, ?verProtocolShort, ?verProtocolLong, " +
            " ?moduleIdGps, ?moduleIdGsm, ?moduleIdRf, ?moduleIdSs, ?moduleIdMm, " +
            " COALESCE((SELECT InternalMobitelConfig_ID FROM Mobitels WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

        private const string msQUERY =
            "INSERT INTO InternalMobitelConfig (Name, Descr, Message_ID, " +
            " devIdShort, devIdLong, verProtocolShort, verProtocolLong, " +
            " moduleIdGps, moduleIdGsm, moduleIdRf, moduleIdSs, moduleIdMm, ID) " +
            "VALUES (@Name, @Descr, @Message_ID, " +
            " @devIdShort, @devIdLong, @verProtocolShort, @verProtocolLong, " +
            "@moduleIdGps, @moduleIdGsm, @moduleIdRf, @moduleIdSs, @moduleIdMm, " +
            " COALESCE((SELECT TOP 1 InternalMobitelConfig_ID FROM Mobitels WHERE Mobitel_ID = @MobitelId), -1))";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1InsertConfigId(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            //MyDbCommand.CommandText = QUERY;
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                db.CommandText(QUERY);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
            }

            //MyDbCommand.Parameters.Add( new MySqlParameter( "?Name", MySqlDbType.String ) );
            db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Descr", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Message_ID", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "MobitelId", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?devIdShort", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "devIdShort", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?devIdLong", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "devIdLong", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?verProtocolShort", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "verProtocolShort", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?verProtocolLong", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "verProtocolLong", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdGps", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "moduleIdGps", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdGsm", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "moduleIdGsm", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdRf", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "moduleIdRf", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //  MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdSs", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "moduleIdSs", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            // MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdMm", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "moduleIdMm", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigTel_ID)
        /// 3. IdConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1InsertConfigId", "object[] initObjects"));
            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigId",
                    initObjects.Length, 3), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;

            //MyDbCommand.Parameters["?Name"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Name", "");
            // MyDbCommand.Parameters["?Descr"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Descr", "");
            // MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "Message_ID", (int) initObjects[0]);
            //  MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];
            db.CommandParametersValue(db.ParamPrefics + "MobitelId", (int) initObjects[1]);

            IdConfig config = (IdConfig) initObjects[2];
            //  MyDbCommand.Parameters["?devIdShort"].Value = config.DevIdShort;
            db.CommandParametersValue(db.ParamPrefics + "devIdShort", config.DevIdShort);
            //  MyDbCommand.Parameters["?devIdLong"].Value = config.DevIdLong;
            db.CommandParametersValue(db.ParamPrefics + "devIdLong", config.DevIdLong);
            //  MyDbCommand.Parameters["?verProtocolShort"].Value = config.VerProtocolShort;
            db.CommandParametersValue(db.ParamPrefics + "verProtocolShor", config.VerProtocolShort);
            //   MyDbCommand.Parameters["?verProtocolLong"].Value = config.VerProtocolLong;
            db.CommandParametersValue(db.ParamPrefics + "verProtocolLong", config.VerProtocolLong);
            //  MyDbCommand.Parameters["?moduleIdGps"].Value = config.ModuleIdGps;
            db.CommandParametersValue(db.ParamPrefics + "moduleIdGps", config.ModuleIdGps);
            //   MyDbCommand.Parameters["?moduleIdGsm"].Value = config.ModuleIdGsm;
            db.CommandParametersValue(db.ParamPrefics + "moduleIdGsm", config.ModuleIdGsm);
            //  MyDbCommand.Parameters["?moduleIdRf"].Value = config.ModuleIdRf;
            db.CommandParametersValue(db.ParamPrefics + "moduleIdRf", config.ModuleIdRf);
            //  MyDbCommand.Parameters["?moduleIdSs"].Value = config.ModuleIdSs;
            db.CommandParametersValue(db.ParamPrefics + "moduleIdSs", config.ModuleIdSs);
            //  MyDbCommand.Parameters["?moduleIdMm"].Value = config.ModuleIdMm;
            db.CommandParametersValue(db.ParamPrefics + "moduleIdMm", config.ModuleIdMm);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //(( MySqlConnection ) Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
