// Project: SrvKernel, File: A1InsertSourceDbCommand.cs
// Namespace: RCS.SrvKernel.DataManagement.SrvDBCommand, Class: A1InsertSourceDbCommand
// Path: D:\Development\TDataManager_Head\SrvKernel\DataManagement\SrvDBCommand, Author: guschin
// Code lines: 80, Size of file: 2,61 KB
// Creation date: 10.11.2008 12:59
// Last modified: 10.11.2008 15:33
// Generated with Commenter by abi.exDream.com

using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� ������ � ������� Source
    /// </summary>
    internal class A1InsertSource : SrvDbCommand
    {
        private const string QUERY =
            "INSERT INTO Sources (SourceText, SourceError) VALUES (?SourceText, ?SourceError)";

        private const string msQUERY =
            "INSERT INTO Sources (SourceText, SourceError) VALUES (@SourceText, @SourceError)";

        /// <summary>
        /// �������� ���������� ����� Source_ID ����� ������,
        /// ������� ������ ��� ���������
        /// </summary>
        private int newSourceId;

        /// <summary>
        /// �������� ���������� ����� Source_ID ����� ������,
        /// ������� ������ ��� ���������
        /// </summary>
        public int NewSourceId
        {
            get { return newSourceId; }
        }

        private DriverDb db;
        private DriverDb commdb = new DriverDb();

        public A1InsertSource(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
                // MyDbCommand.Parameters.Add(new MySqlParameter("?SourceText", MySqlDbType.String));
                db.NewSqlParameter("?SourceText", db.GettingString());
                db.CommandParametersAdd(db.GetSqlParameter);
                // MyDbCommand.Parameters.Add(new MySqlParameter("?SourceError", MySqlDbType.String));
                db.NewSqlParameter("?SourceError", db.GettingString());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
                // MyDbCommand.Parameters.Add(new MySqlParameter("?SourceText", MySqlDbType.String));
                db.NewSqlParameter("@SourceText", db.GettingString());
                db.CommandParametersAdd(db.GetSqlParameter);
                // MyDbCommand.Parameters.Add(new MySqlParameter("?SourceError", MySqlDbType.String));
                db.NewSqlParameter("@SourceError", db.GettingString());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. SourceText
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1InsertSource", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertSource",
                    initObjects.Length, 1), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;
            newSourceId = 0;
            //MyDbCommand.Parameters["?SourceText"].Value = (string)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "SourceText", (string) initObjects[0]);
            //MyDbCommand.Parameters["?SourceError"].Value = "No error";
            db.CommandParametersValue(db.ParamPrefics + "SourceError", "No error");
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            //newSourceId = (int)MyDbCommand.LastInsertedId;
            newSourceId = (int) db.CommandLastInsertedId();

            return ExecutionStatus.OK;
        }
    }
}
