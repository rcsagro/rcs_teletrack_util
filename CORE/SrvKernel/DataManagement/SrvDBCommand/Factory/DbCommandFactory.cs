using System;
using RCS.SrvKernel.Logging;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand.Factory
{
    /// <summary>
    /// �������� �������� ������ �� ��������� ����.
    /// </summary>
    public class DbCommandFactory : IDbCommandFactory
    {
        /// <summary>
        /// ������ �����������.
        /// </summary>
        private readonly DriverDb _connection = null;

        private DbCommandFactory()
        {
        }

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="�onnectionString">������ �����������.</param>
        public DbCommandFactory(DriverDb �onnection)
        {
            _connection = �onnection;
        }

        #region IDbCommandFactory Members

        /// <summary>
        /// �������� ��������� �������.
        /// </summary>
        /// <typeparam name="T">��� �������.</typeparam>
        /// <returns>ISrvDbCommand.</returns>
        public ISrvDbCommand GetDbCommand<T>() where T : SrvDbCommand
        {
            try
            {
                return (SrvDbCommand) Activator.CreateInstance(
                    typeof (T), new object[1] {_connection});
            }
            catch (Exception ex)
            {
                Logger.Write("GetDbCommand<T>() where T : SrvDbCommand: " + ex.Message);
                return null;
            }
        }

        #endregion
    }
}
