using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.Protocol.A1.Entity;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� �������
    /// </summary>
    class A1InsertConfigEvent : SrvDbCommand
    {
        private const string QUERY =
            "INSERT INTO ConfigEvent (Name, Descr, Message_ID, " +
            " tmr1Log, tmr2Send, tmr3Zone, dist1Log, dist2Send, dist3Zone,  " +
            " gprsEmail, gprsFtp, gprsSocket,  " +
            " maskEvent1, maskEvent2, maskEvent3, maskEvent4, maskEvent5, maskEvent6, " +
            " maskEvent7, maskEvent8, maskEvent9, maskEvent10, maskEvent11, maskEvent12, " +
            " maskEvent13, maskEvent14, maskEvent15, maskEvent16, maskEvent17, maskEvent18, " +
            " maskEvent19, maskEvent20, maskEvent21, maskEvent22, maskEvent23, maskEvent24, " +
            " maskEvent25, maskEvent26, maskEvent27, maskEvent28, maskEvent29, maskEvent30, " +
            " maskEvent31, deltaTimeZone, maskSensor, ID) " +
            "VALUES (?Name, ?Descr, ?Message_ID, " +
            " ?tmr1Log, ?tmr2Send, ?tmr3Zone, ?dist1Log, ?dist2Send, ?dist3Zone, " +
            " ?gprsEmail, ?gprsFtp, ?gprsSocket, " +
            " ?maskEvent1, ?maskEvent2, ?maskEvent3, ?maskEvent4, ?maskEvent5, ?maskEvent6, " +
            " ?maskEvent7, ?maskEvent8, ?maskEvent9, ?maskEvent10, ?maskEvent11, ?maskEvent12, " +
            " ?maskEvent13, ?maskEvent14, ?maskEvent15, ?maskEvent16, ?maskEvent17, ?maskEvent18, " +
            " ?maskEvent19, ?maskEvent20, ?maskEvent21, ?maskEvent22, ?maskEvent23, ?maskEvent24, " +
            " ?maskEvent25, ?maskEvent26, ?maskEvent27, ?maskEvent28, ?maskEvent29, ?maskEvent30, " +
            " ?maskEvent31, ?deltaTimeZone, ?maskSensor,  " +
            " (COALESCE((SELECT ConfigEvent_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))) ";

        private const string msQUERY =
            "INSERT INTO ConfigEvent (Name, Descr, Message_ID, " +
            " tmr1Log, tmr2Send, tmr3Zone, dist1Log, dist2Send, dist3Zone,  " +
            " gprsEmail, gprsFtp, gprsSocket,  " +
            " maskEvent1, maskEvent2, maskEvent3, maskEvent4, maskEvent5, maskEvent6, " +
            " maskEvent7, maskEvent8, maskEvent9, maskEvent10, maskEvent11, maskEvent12, " +
            " maskEvent13, maskEvent14, maskEvent15, maskEvent16, maskEvent17, maskEvent18, " +
            " maskEvent19, maskEvent20, maskEvent21, maskEvent22, maskEvent23, maskEvent24, " +
            " maskEvent25, maskEvent26, maskEvent27, maskEvent28, maskEvent29, maskEvent30, " +
            " maskEvent31, deltaTimeZone, maskSensor, ID) " +
            "VALUES (@Name, @Descr, @Message_ID, " +
            " @tmr1Log, @tmr2Send, @tmr3Zone, @dist1Log, @dist2Send, @dist3Zone, " +
            " @gprsEmail, @gprsFtp, @gprsSocket, " +
            " @maskEvent1, @maskEvent2, @maskEvent3, @maskEvent4, @maskEvent5, @maskEvent6, " +
            " @maskEvent7, @maskEvent8, @maskEvent9, @maskEvent10, @maskEvent11, @maskEvent12, " +
            " @maskEvent13, @maskEvent14, @maskEvent15, @maskEvent16, @maskEvent17, @maskEvent18, " +
            " @maskEvent19, @maskEvent20, @maskEvent21, @maskEvent22, @maskEvent23, @maskEvent24, " +
            " @maskEvent25, @maskEvent26, @maskEvent27, @maskEvent28, @maskEvent29, @maskEvent30, " +
            " @maskEvent31, @deltaTimeZone, @maskSensor,  " +
            " (COALESCE((SELECT TOP 1 ConfigEvent_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1))) ";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1InsertConfigEvent(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetSqlCommand();
            
            //MyDbCommand.CommandType = CommandType.Text;
            
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
            }

            //MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Descr", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Message_ID", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "MobitelId", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?tmr1Log", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "tmr1Log", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?tmr2Send", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "tmr2Send", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?tmr3Zone", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "tmr3Zone", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?dist1Log", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "dist1Log", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?dist2Send", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "dist2Send", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?dist3Zone", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "dist3Zone", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?gprsEmail", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "gprsEmail", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?gprsFtp", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "gprsFtp", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?gprsSocket", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "gprsSocket", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);

            for (int i = 1; i < 32; i++)
            {
                //MyDbCommand.Parameters.Add(new MySqlParameter("?maskEvent" + Convert.ToString(i), MySqlDbType.Int32));
                db.NewSqlParameter(db.ParamPrefics + "maskEvent" + Convert.ToString(i), db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
            }

            //MyDbCommand.Parameters.Add(new MySqlParameter("?deltaTimeZone", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "deltaTimeZone", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?maskSensor", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "maskSensor", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigEvent_ID)
        /// 3. EventConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1InsertConfigEvent", "object[] initObjects"));
            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigEvent",
                    initObjects.Length, 3), "object[] initObjects");


            db.SetNewCommand = commdb.GetSqlCommand();

            // MyDbCommand.Parameters["?Name"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Name", "");
            // MyDbCommand.Parameters["?Descr"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Descr", "");
            // MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "Message_ID", (int) initObjects[0]);
            // MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];
            db.CommandParametersValue(db.ParamPrefics + "MobitelId", (int) initObjects[1]);

            EventConfig config = (EventConfig) initObjects[2];
            //  MyDbCommand.Parameters["?tmr1Log"].Value = config.MinSpeed;
            db.CommandParametersValue(db.ParamPrefics + "tmr1Log", config.MinSpeed);
            //  MyDbCommand.Parameters["?tmr2Send"].Value = config.Timer1;
            db.CommandParametersValue(db.ParamPrefics + "tmr2Send", config.Timer1);
            //  MyDbCommand.Parameters["?tmr3Zone"].Value = config.Timer2;
            db.CommandParametersValue(db.ParamPrefics + "tmr3Zone", config.Timer2);
            //  MyDbCommand.Parameters["?dist1Log"].Value = config.CourseBend;
            db.CommandParametersValue(db.ParamPrefics + "dist1Log", config.CourseBend);
            //  MyDbCommand.Parameters["?dist2Send"].Value = config.Distance1;
            db.CommandParametersValue(db.ParamPrefics + "dist2Send", config.Distance1);
            //   MyDbCommand.Parameters["?dist3Zone"].Value = config.Distance2;
            db.CommandParametersValue(db.ParamPrefics + "dist3Zone", config.Distance2);
            //  MyDbCommand.Parameters["?gprsEmail"].Value = config.GprsEmail;
            db.CommandParametersValue(db.ParamPrefics + "gprsEmail", config.GprsEmail);
            //    MyDbCommand.Parameters["?gprsFtp"].Value = 10;
            db.CommandParametersValue(db.ParamPrefics + "gprsFtp", 10);
            //    MyDbCommand.Parameters["?gprsSocket"].Value = 10;
            db.CommandParametersValue(db.ParamPrefics + "gprsSocket", 10);

            for (int i = 1; i < 32; i++)
            {
                //      MyDbCommand.Parameters["?maskEvent" + Convert.ToString(i)].Value = config.EventMask[i - 1];
                db.CommandParametersValue(db.ParamPrefics + "maskEvent" + Convert.ToString(i), config.EventMask[i - 1]);
            }

            //    MyDbCommand.Parameters["?deltaTimeZone"].Value = config.SpeedChange;
            //db.CommandParametersValue( db.ParamPrefics + "MessageCounter", config.SpeedChange );
            db.CommandParametersValue(db.ParamPrefics + "deltaTimeZone", config.SpeedChange);
            //   MyDbCommand.Parameters["?maskSensor"].Value = "----";
            //db.CommandParametersValue( db.ParamPrefics + "MessageCounter", "----");
            db.CommandParametersValue(db.ParamPrefics + "maskSensor", "----");
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
