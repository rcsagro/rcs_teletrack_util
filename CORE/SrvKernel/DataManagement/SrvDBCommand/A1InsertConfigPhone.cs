using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.Protocol.A1.Entity;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� ���������� �������
    /// </summary>
    class A1InsertConfigPhone : SrvDbCommand
    {
        private const string QUERY =
            "INSERT INTO ConfigTel (Name, Descr, Message_ID, " +
            " telSOS, telDspt, telAccept1, telAccept2, telAccept3, ID) " +
            "VALUES (?Name, ?Descr, ?Message_ID,  " +
            " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telSOS LIMIT 1), -1)), " +
            " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telDspt LIMIT 1), -1)), " +
            " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telAccept1 LIMIT 1), -1)), " +
            " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telAccept2 LIMIT 1), -1)), " +
            " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telAccept3 LIMIT 1), -1)), " +
            " (COALESCE((SELECT ConfigTel_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))) ";

        private const string msQUERY =
            "INSERT INTO ConfigTel (Name, Descr, Message_ID, " +
            " telSOS, telDspt, telAccept1, telAccept2, telAccept3, ID) " +
            "VALUES (@Name, @Descr, @Message_ID,  " +
            " (COALESCE((SELECT TOP 1 TelNumber_ID FROM Telnumbers WHERE TelNumber = @telSOS), -1)), " +
            " (COALESCE((SELECT TOP 1 TelNumber_ID FROM Telnumbers WHERE TelNumber = @telDspt), -1)), " +
            " (COALESCE((SELECT TOP 1 TelNumber_ID FROM Telnumbers WHERE TelNumber = @telAccept1), -1)), " +
            " (COALESCE((SELECT TOP 1 TelNumber_ID FROM Telnumbers WHERE TelNumber = @telAccept2), -1)), " +
            " (COALESCE((SELECT TOP 1 TelNumber_ID FROM Telnumbers WHERE TelNumber = @telAccept3), -1)), " +
            " (COALESCE((SELECT TOP 1 ConfigTel_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1))) ";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1InsertConfigPhone(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandParametersClear();
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(msQUERY);
            }

            //MyDbCommand.Parameters.Add( new MySqlParameter( "?Name", MySqlDbType.String ) );
            db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "Descr", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "Message_ID", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
            db.NewSqlParameter(db.ParamPrefics + "MobitelId", db.GettingInt32());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?telSOS", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "telSOS", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?telDspt", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "telDspt", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?telAccept1", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "telAccept1", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?telAccept2", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "telAccept2", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
            //MyDbCommand.Parameters.Add(new MySqlParameter("?telAccept3", MySqlDbType.String));
            db.NewSqlParameter(db.ParamPrefics + "telAccept3", db.GettingString());
            db.CommandParametersAdd(db.GetSqlParameter);
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigTel_ID)
        /// 3. PhoneNumberConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1InsertConfigPhoneDbCommand", "object[] initObjects"));
            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigPhoneDbCommand",
                    initObjects.Length, 3), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;

            //MyDbCommand.Parameters["?Name"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Name", "");
            //MyDbCommand.Parameters["?Descr"].Value = "";
            db.CommandParametersValue(db.ParamPrefics + "Descr", "");
            //MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "Message_ID", (int) initObjects[0]);
            //MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];
            db.CommandParametersValue(db.ParamPrefics + "MobitelId", (int) initObjects[1]);

            PhoneNumberConfig config = (PhoneNumberConfig) initObjects[2];
            //MyDbCommand.Parameters["?telSOS"].Value = config.NumberSOS;
            db.CommandParametersValue(db.ParamPrefics + "telSOS", config.NumberSOS);
            //MyDbCommand.Parameters["?telDspt"].Value = config.NumberDspt;
            db.CommandParametersValue(db.ParamPrefics + "telDspt", config.NumberDspt);
            //MyDbCommand.Parameters["?telAccept1"].Value = config.NumberAccept1;
            db.CommandParametersValue(db.ParamPrefics + "telAccept1", config.NumberAccept1);
            //MyDbCommand.Parameters["?telAccept2"].Value = config.NumberAccept2;
            db.CommandParametersValue(db.ParamPrefics + "telAccept2", config.NumberAccept2);
            //MyDbCommand.Parameters["?telAccept3"].Value = config.NumberAccept3;
            db.CommandParametersValue(db.ParamPrefics + "telAccept3", config.NumberAccept3);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
