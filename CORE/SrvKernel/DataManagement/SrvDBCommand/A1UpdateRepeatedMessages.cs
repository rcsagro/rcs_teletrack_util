using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ��������� �������� IsNew = 0 ��� ������������� ������ ���������,
    /// ��������������� ��� ��������. 
    /// �������������� ������ ����� ��������� ���������. ����������
    /// ������� ���� ������, ������� ������ ���������� ����������� �
    /// ���������� ��������� IsNew = 0 � ������ �� ��������������.
    /// </summary>
    class A1UpdateRepeatedMessages : SrvDbCommand
    {
        private const string QUERY =
            "UPDATE Messages " +
            "SET IsNew = 0, Source_ID = 0  " +
            "WHERE (Mobitel_ID = ?MobitelId) AND (Command_id = ?CommandId) AND " +
            "  (IsNew = 1) AND (Direction = 1) AND (IsSend = 0) AND (MessageCounter < ?MessageCounter)";

        private const string msQUERY =
            "UPDATE Messages " +
            "SET IsNew = 0, Source_ID = 0  " +
            "WHERE (Mobitel_ID = @MobitelId) AND (Command_id = @CommandId) AND " +
            "  (IsNew = 1) AND (Direction = 1) AND (IsSend = 0) AND (MessageCounter < @MessageCounter)";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public A1UpdateRepeatedMessages(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandText("");
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
                db.NewSqlParameter("?MobitelId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?CommandId", MySqlDbType.Int32));
                db.NewSqlParameter("?CommandId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageCounter", MySqlDbType.Int32));
                db.NewSqlParameter("?MessageCounter", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                db.CommandText(msQUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
                db.NewSqlParameter("@MobitelId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?CommandId", MySqlDbType.Int32));
                db.NewSqlParameter("@CommandId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageCounter", MySqlDbType.Int32));
                db.NewSqlParameter("@MessageCounter", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MobitelId
        /// 2. CommandId
        /// 3. MessageCounter
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1UpdateRepeatedMessages", "object[] initObjects"));
            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1UpdateRepeatedMessages",
                    initObjects.Length, 3), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[0];
            db.CommandParametersValue(db.ParamPrefics + "MobitelId", (int) initObjects[0]);
            //MyDbCommand.Parameters["?CommandId"].Value = (int)initObjects[1];
            db.CommandParametersValue(db.ParamPrefics + "CommandId", (int) initObjects[1]);
            //MyDbCommand.Parameters["?MessageCounter"].Value = (int)initObjects[2];
            db.CommandParametersValue(db.ParamPrefics + "MessageCounter", (int) initObjects[2]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            db.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
