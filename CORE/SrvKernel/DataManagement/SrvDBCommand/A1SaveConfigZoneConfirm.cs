using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ���������� ������ �� ��������� ���
    /// </summary>
    class A1SaveConfigZoneConfirm : SrvDbCommand
    {
        private const string QUERY =
            "UPDATE Messages SET AdvCounter = ?ZoneResult " +
            "WHERE Message_ID = ?ZoneMsgId";

        private const string msQUERY =
            "UPDATE Messages SET AdvCounter = @ZoneResult " +
            "WHERE Message_ID = @ZoneMsgId";

        DriverDb connect;
        DriverDb commdb = new DriverDb();

        public A1SaveConfigZoneConfirm(DriverDb connection)
            : base(connection)
        {
            connect = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            connect.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                connect.CommandText(QUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?ZoneResult", MySqlDbType.Int32));
                connect.NewSqlParameter("?ZoneResult", connect.GettingInt32());
                connect.CommandParametersAdd(connect.GetSqlParameter);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?ZoneMsgId", MySqlDbType.Int32));
                connect.NewSqlParameter("?ZoneMsgId", connect.GettingInt32());
                connect.CommandParametersAdd(connect.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                connect.CommandText(msQUERY);
                connect.NewSqlParameter("@ZoneResult", connect.GettingInt32());
                connect.CommandParametersAdd(connect.GetSqlParameter);
                connect.NewSqlParameter("@ZoneMsgId", connect.GettingInt32());
                connect.CommandParametersAdd(connect.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. ZoneMsgId
        /// 2. ZoneResult
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1SaveConfigZoneConfirm", "object[] initObjects"));
            if (initObjects.Length != 2)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1SaveConfigZoneConfirm",
                    initObjects.Length, 2), "object[] initObjects");

            connect.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Parameters["?ZoneMsgId"].Value = (int)initObjects[0];
            connect.CommandParametersValue(connect.ParamPrefics + "ZoneMsgId", (int) initObjects[0]);
            //MyDbCommand.Parameters["?ZoneResult"].Value = (int)initObjects[1];
            connect.CommandParametersValue(connect.ParamPrefics + "ZoneResult", (int) initObjects[1]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            connect.CommandSqlConnection(connect.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            //connect.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            connect.CommandExecuteReader();

            return ExecutionStatus.OK;
        }

    }
}
