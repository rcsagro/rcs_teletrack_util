using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RWLog.DriverDataBase;

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� ���������� ������� GPS ������
    /// </summary>
    class A1SelectDataGpsQuery : SrvDbCommand
    {
        private const string QUERY =
            "SELECT * FROM GpsMasks WHERE message_id = ?MessageId";

        private const string msQUERY =
            "SELECT * FROM GpsMasks WHERE message_id = @MessageId";

        DriverDb connect;
        DriverDb commdb = new DriverDb();

        public A1SelectDataGpsQuery(DriverDb connection)
            : base(connection)
        {
            connect = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            connect.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.CommandText = QUERY;

            //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                connect.CommandText(QUERY);
                connect.NewSqlParameter("?MessageId", connect.GettingInt32());
                connect.CommandParametersAdd(connection.GetSqlParameter);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                connect.CommandText(msQUERY);
                connect.NewSqlParameter("@MessageId", connect.GettingInt32());
                connect.CommandParametersAdd(connect.GetSqlParameter);
            }
            //db.CommandParametersAdd();
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1SelectDataGpsQuery", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectDataGpsQuery",
                    initObjects.Length, 1), "object[] initObjects");

            connect.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
            connect.CommandParametersValue(connect.ParamPrefics + "MessageId", (int) initObjects[0]);
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = (MySqlConnection)Connection;
            connect.CommandSqlConnection(connect.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            return connect.CommandExecuteReader(); // MyDbCommand.ExecuteReader();
        }
    }
}
