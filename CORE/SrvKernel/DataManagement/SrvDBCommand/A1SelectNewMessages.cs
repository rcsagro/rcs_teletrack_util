// Project: SrvKernel, File: A1SelectNewMessages.cs
// Namespace: RCS.SrvKernel.DataManagement.SrvDBCommand, Class: A1SelectNewMessages
// Path: D:\Development\TDataManager_Head\SrvKernel\DataManagement\SrvDBCommand, Author: guschin
// Code lines: 34, Size of file: 1,21 KB
// Creation date: 13.11.2008 11:53
// Last modified: 13.11.2008 12:26
// Generated with Commenter by abi.exDream.com

#region Using directives
using System;
using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.ErrorHandling;
using RWLog.DriverDataBase;

#endregion

namespace RCS.SrvKernel.DataManagement.SrvDBCommand
{
    /// <summary>
    /// ������� ����� ���������.
    /// �������� ������ ��������� ��������� ��� ������ �� ������ ������
    /// ����������, � ��� ���� ���������, ����� �������� ��� ��������� ������� 
    /// </summary>
    internal class A1SelectNewMessages : SrvDbCommand
    {
        private const string QUERY =
            "SELECT m1.Message_ID AS MessageID, m1.Command_ID AS CommandID, " +
            "  m1.MessageCounter AS MessageCounter " +
            "FROM Messages m1 JOIN ( " +
            "    SELECT Command_ID, MAX(Message_ID) AS MessageId " +
            "    FROM Messages " +
            "    WHERE (Mobitel_ID = ?MobitelId) AND (isNew = 1) AND (Direction = 1) " +
            "    GROUP BY Command_ID) m2 " +
            "  ON m1.Message_ID = m2.MessageId ";

        private const string msQUERY =
            "SELECT m1.Message_ID AS MessageID, m1.Command_ID AS CommandID, " +
            "  m1.MessageCounter AS MessageCounter " +
            "FROM Messages m1 JOIN ( " +
            "    SELECT Command_ID, MAX(Message_ID) AS MessageId " +
            "    FROM Messages " +
            "    WHERE (Mobitel_ID = @MobitelId) AND (isNew = 1) AND (Direction = 1) " +
            "    GROUP BY Command_ID) m2 " +
            "  ON m1.Message_ID = m2.MessageId ";

        private DriverDb db;
        private DriverDb commdb = new DriverDb();

        /// <summary>
        /// Create A1 select new messages
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public A1SelectNewMessages(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                //MyDbCommand.CommandText = QUERY;
                db.CommandText(QUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
                db.NewSqlParameter("?MobitelId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
            else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
            {
                db.CommandText(msQUERY);
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
                db.NewSqlParameter("@MobitelId", db.GettingInt32());
                db.CommandParametersAdd(db.GetSqlParameter);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MobitelId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "A1SelectNewMessages", "object[] initObjects"));

            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectNewMessages",
                    initObjects.Length, 1), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[0];
            //db.CommandParametersValue( db.ParamPrefics + "MessageId", ( int ) initObjects[0] );
            db.CommandParametersValue(db.ParamPrefics + "MobitelId", (int) initObjects[0]);
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            return db.CommandExecuteReader(); // MyDbCommand.ExecuteReader();
        }
    }
}
