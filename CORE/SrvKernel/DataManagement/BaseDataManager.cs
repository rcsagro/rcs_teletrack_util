#region Using directives
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Timers;
using MySql.Data.MySqlClient;
using RCS.Protocol.A1.Entity;
using RCS.SrvKernel.A1Handler;
using RCS.SrvKernel.DataManagement.Cache;
using RCS.SrvKernel.DataManagement.SrvDBCommand;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand.Sending;
using RCS.SrvKernel.Util;
using RCS.Protocol.A1;
using RCS.SrvKernel.DataManagement.SrvDBCommand.Factory;
using RWLog.DriverDataBase;

#endregion

namespace RCS.SrvKernel.DataManagement
{
    /// <summary>
    /// �������� ������ ������ � ����� ������
    /// </summary>
    public class BaseDataManager
    {
        /// <summary>
        /// ������������ ���-�� ����� �������� � ������ = 100
        /// </summary>
        private const int MAX_ROW_COUNT = 100;
        /// <summary>
        /// ������������� �������� ������������� 
        /// ������ ������ �� ������ � �� = 3 ������
        /// </summary>
        private const int FLUSH_BUFFER_INTERVAL = 3 * 1000;


        #region �������� � �����

        /// <summary>
        /// ������ ����������� ������������� ������ ������ �� ������ � ��
        /// </summary>
        private System.Timers.Timer timerFlushData;

        ///// <summary>
        ///// ������ ����������� ������������� ������ ������ �� ������64 � ��
        ///// </summary>
        //private System.Timers.Timer timerFlushData64;

        /// <summary>
        /// ����� GPS ������ 
        /// </summary>
        private CacheDataGps bufferDatagGPS;

        /// <summary>
        /// ����� GPS64 ������ 
        /// </summary>
        private CacheDataGps bufferDatagGPS64;

        /// <summary>
        /// ��� ������.
        /// </summary>
        protected readonly ICommandCache _commandCache;

        private static object syncNewDataGPSObject = new object();
        private static volatile object syncObject = new object();

        /// <summary>
        /// �� ������ �������� - ������ ����� ConnectionString
        /// </summary>
        private volatile string connectionString;
        /// <summary>
        /// �� ������ �������� - ������ ����� ConnectWithDataBase
        /// </summary>
        private volatile DriverDb _connectDataBase = null;
        /// <summary>
        /// Connection string
        /// </summary>
        /// <returns>String</returns>
        public string ConnectionStringBase
        {
            get { return GetConnectionString(); }
        }

        /// <summary>
        /// ���������� ������� ���������� � �����  ��������� � ������ �����������
        /// </summary>
        /// <returns>DriverDb</returns>
        public DriverDb ConnectWithDataBase
        {
            get { return _connectDataBase; }
        }

        /// <summary>
        /// ��������� �������� ���������� � ����� ������
        /// </summary>
        /// <returns>DriverDb</returns>
        private void GetConnectDataBase()
        {
            if (ConnectionStringBase != null && ConnectionStringBase.Length > 0)
            {
                _connectDataBase = new DriverDb(ConnectionStringBase);
                object connection = _connectDataBase.NewSqlConnection(ConnectionStringBase);
                _connectDataBase.NewSqlCommand();
                _connectDataBase.CommandSqlConnection(connection);
                _connectDataBase.CommandConnectionOpen();
            }
        }


        /// <summary>
        /// ��������� ������ �����������
        /// </summary>
        /// <returns>String</returns>
        protected virtual string GetConnectionString()
        {
            if (String.IsNullOrEmpty(connectionString))
            {
                DriverDb.AnalizeTypeDB(SrvSettings.TypeDataBase); // �����: ������ ���� ���� ������

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnectionStringBuilder connBuilder = new MySqlConnectionStringBuilder();
                    connBuilder.Server = SrvSettings.DataBaseHost;
                    connBuilder.Port = Convert.ToUInt32(SrvSettings.DataBasePort);
                    connBuilder.Database = SrvSettings.DataBaseName;
                    connBuilder.UserID = SrvSettings.DataBaseUser;
                    connBuilder.Password = SrvSettings.DataBasePassword;

                    connectionString = connBuilder.ToString();
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    string strConn = "Server=" + SrvSettings.DataBaseHost + ";" +
                                     "database=" + SrvSettings.DataBaseName + ";";

                    if (SrvSettings.TypeConnect.Contains("yes"))
                    {
                        strConn = strConn + "trusted_connection=yes;";
                    }
                    else
                    {
                        strConn = strConn + "user id=" + SrvSettings.DataBaseUser + ";" +
                            "password=" + SrvSettings.DataBasePassword + ";";
                    }

                    strConn = strConn + "asynchronous processing=yes;";
                    strConn = strConn + "MultipleActiveResultSets=true;";

                    connectionString = strConn;
                }
            }

            if (SrvSettings.DataBaseAdditionalOptions != "")
            {
                connectionString += ';' + SrvSettings.DataBaseAdditionalOptions;
            }

            return connectionString;
        }

        #endregion


        #region �����������

        /// <summary>
        /// Create data manager
        /// </summary>
        /// <param name="connectionSettings">Connection settings</param>
        /// <param name="debugMode">Debug mode</param>
        /// <param name="iErrorHandler">Error handler</param>
        public BaseDataManager()
        {
            if (PossibleTo�onnect())
                GetConnectDataBase();

            _commandCache = new CommandCache(new DbCommandFactory(ConnectWithDataBase));
            bufferDatagGPS = new CacheDataGps();
            bufferDatagGPS64 = new CacheDataGps();

            timerFlushData = new System.Timers.Timer();
            timerFlushData.AutoReset = false;
            timerFlushData.Elapsed += new ElapsedEventHandler(OnElapsedEventHandler);
        }


        #endregion

        private volatile bool connectAvailable;
        /// <summary>
        /// ���������� �������� �� ���������� � ��.
        /// �������� ��������� ��� ����� ���������� 
        /// ������ ������ PossibleTo�onnect
        /// </summary>
        public bool ConnectAvailable
        {
            get { return connectAvailable; }
        }

        /// <summary>
        /// �������� �������� �� ����������� � ��
        /// </summary>
        /// <returns>Bool</returns>
        public bool PossibleTo�onnect()
        {
            lock (syncObject)
            {
                connectAvailable = true;
                DriverDb.ConnectionString = ConnectionStringBase;
                try
                {
                    //MySqlConnection connection = new MySqlConnection(ConnectionString);
                    DriverDb db = new DriverDb(ConnectionStringBase);
                    //connection.Open();
                    db.NewSqlConnection();
                    //connection.Close();
                    db.SqlConnectionClose();
                }
                catch (Exception)
                {
                    connectAvailable = false;
                }
                return connectAvailable;
            }
        }

        public event SimpleEventDelegate eventDbNotAvailable;

        /// <summary>
        /// ���������������� �������� ����� ����������� �������.
        /// ����������� ����������� � ������������� ����
        /// </summary>
        /// <returns>Bool</returns>
        protected bool CheckConnect()
        {
            bool result = true;
            if (!PossibleTo�onnect())
            {
                result = false;
                ErrorHandle(new ApplicationException(String.Format(
                  AppError.DB_CONNECTION, ConnectionStringBase)));

                if (eventDbNotAvailable != null)
                {
                    eventDbNotAvailable();
                }
            }

            return result;
        }

        /// <summary>
        /// ���������� ���������� ����. ������������� � ����������
        /// �������� ��� ���������� ������������� ��������.
        /// </summary>
        public virtual void FillLocalCache()
        {
        }


        #region ������� ������ � DataGpsBuffer

        /// <summary>
        /// ���������� ������� ������ ������ �� ������ � ��
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">ElapsedEventArgs</param>
        private void OnElapsedEventHandler(object sender, ElapsedEventArgs e)
        {
            lock (syncNewDataGPSObject)
            {
                FlushDataGpsBuffer();
                FlushDataGps64Buffer();
            }
        }

        /// <summary>
        /// ���������� ������ � ���� � ������� � ���� ���� 
        /// ��� ����������. 
        /// </summary>
        /// <param name="dataGPS">Data GPS string[]</param>
        public void InsertDataGPS(params string[] dataGPS)
        {
            if (dataGPS == null)
            {
                throw new ArgumentNullException(
                  "string[] DataGPS", AppError.DB_NULL_DATAGPS_PARAM);
            }

            lock (syncNewDataGPSObject)
            {
                if (timerFlushData.Enabled)
                {
                    timerFlushData.Stop();
                }
                if (dataGPS.Length == 0)
                {
                    return;
                }
                // ��������� ������ � ������
                foreach (string row in dataGPS)
                {
                    if (row.Substring(0, 4) == ParserServices.MBIN_64_TT)
                    {
                        bufferDatagGPS64.Add(row.Substring(4));
                    }
                    else
                    {
                        bufferDatagGPS.Add(row);
                    }
                }

                if (bufferDatagGPS.GetCount() >= MAX_ROW_COUNT)
                {
                    FlushDataGpsBuffer();
                }
                else if (!timerFlushData.Enabled)
                {
                    timerFlushData.Start();
                }

                if (bufferDatagGPS64.GetCount() >= MAX_ROW_COUNT)
                {
                    FlushDataGps64Buffer();
                }
                else if (!timerFlushData.Enabled)
                {
                    timerFlushData.Start();
                }

            }
        }

        /// <summary>
        /// �������������� ����� ������ �� ������ � ���� 
        /// </summary>
        public void ManualFlushDataGpsBuffer()
        {
            lock (syncNewDataGPSObject)
            {
                FlushDataGpsBuffer();
            }
        }

        /// <summary>
        /// ����� GPS ������ �� ������ � ���� 
        /// � ������� ������
        /// </summary>
        private void FlushDataGpsBuffer()
        {
            // ����������� ������
            if (timerFlushData.Enabled)
            {
                timerFlushData.Stop();
            }
            try
            {
                if ((ConnectAvailable) && (bufferDatagGPS.GetCount() > 0))
                {
                    int insertedRowsCount = InsertData(bufferDatagGPS.GetArray());

                    if (insertedRowsCount > 0)
                    {
                        // ��� �������� - ������� ��������� ������ � ����� ���� ������.
                        // ������� ���������� ������
                        bufferDatagGPS.Clear();
                        // ����������� ������ �� ������ ���� ������ � ������� DataGps
                        if (TransferBuffer() != ExecutionStatus.OK)
                        {
                            CheckConnect();
                        }
                    }
                    else if (CheckConnect())
                    {
                        // ���� ����������� ������� ��� � ���� ������� - 
                        // ���-�� �������� ���������, ������ ��������� � ������, 
                        // ������������� ������� ��������� �����
                        bufferDatagGPS.Clear();
                    }
                }
            }
            catch (Exception Ex)
            {
                ErrorHandler.Handle(Ex);
                CheckConnect();
            }
            finally
            {
                // ����� �������
                timerFlushData.Start();
            }
        }

        /// <summary>
        /// ������� ����� ������.
        /// ����� ������ ���������� ������� � ����� ������������� �������� 
        /// ��� ������� ����������� �������. ����� ���������
        /// ������ � ������� DataGPS ��������, ������� ����� ���������� �
        /// ���������� ��������. ������� Transfer Buffer ����� �������� �����������
        /// ������� ����������� ������� (���� ����) ��� ����������, �������
        /// ���������� � ������ ������. 
        /// </summary>
        /// <param name="dataGpsBuffer">Data gps buffer string[]</param>
        /// <returns>���������� ������� ����������� � ������ ��</returns>
        private int InsertData(params string[] dataGpsBuffer)
        {
            //int result = 0;

            if (dataGpsBuffer.Length == 0)
            {
                return 0;
            }
            ISrvDbCommand command = GetInsertDataInBufferDBCommand();
            return InsertDataGeneral(command, dataGpsBuffer);
        }

        private int InsertDataGeneral(ISrvDbCommand command, string[] dataGpsBuffer, bool is64bit = false)
        {
            int result = 0;

            // ������� ��������� ������� ����� �������
            command.Init(new object[] { dataGpsBuffer });
            ExecutionStatus status = command.Execute();

            // ���� ��������� ������, �� �� ����� ������ �� �����������,
            // ������� ���� ���������� �������� ������ �� �����������
            if (status != ExecutionStatus.OK)
            {
                // �������� �������� �� ������������ � ��
                if (!CheckConnect())
                {
                    return 0;
                }
                // �������� ����� �� ��� ����� � �������� �������
                if (dataGpsBuffer.Length > 5)
                {
                    // ����� �������� �������, ��� �� ��� ���� ������
                    // ������� �������
                    int middle = (int)(dataGpsBuffer.Length / 2);
                    // ������ ������ ������ �������
                    int highArraySize = dataGpsBuffer.Length - middle;

                    // ������������ ����� ��������
                    string[] lowArray = new string[middle];
                    string[] highArray = new string[highArraySize];
                    Array.Copy(dataGpsBuffer, 0, lowArray, 0, middle);
                    Array.Copy(dataGpsBuffer, middle, highArray, 0, highArraySize);


                    // ��������� �� ������ 
                    if (is64bit)
                    {
                        result += InsertData64(lowArray);
                        result += InsertData64(highArray);
                    }
                    else
                    {
                        result += InsertData(lowArray);
                        result += InsertData(highArray);
                    }
                }
                else
                {
                    // ���� ������ ��-�� �������� ���� ������������
                    // ����� ��������� ����� ������.
                    // ������� � ����� ����� ������ ���������
                    // ����� ��������� �������� ������
                    int rowCount = dataGpsBuffer.Length - 1;
                    for (int index = rowCount; index >= 0; index--)
                    {
                        command.Init(new object[] { new string[1] { dataGpsBuffer[index] } });
                        status = command.Execute();
                        // ���� ������ ���������� - ���������
                        if (status == ExecutionStatus.OK)
                        {
                            ++result;
                        }
                    }
                }
            }
            else
            {
                result = dataGpsBuffer.Length;
            }

            return result;
        }

        private int InsertData64(params string[] dataGpsBuffer)
        {
            if (dataGpsBuffer.Length == 0)
            {
                return 0;
            }
            ISrvDbCommand command = GetInsertDataInBuffer64DBCommand();
            return InsertDataGeneral(command, dataGpsBuffer, true);
        }

        /// <summary>
        /// ����� GPS64 ������ �� ������ � ���� 
        /// � ������� ������
        /// </summary>
        private void FlushDataGps64Buffer()
        {
            // ����������� ������
            if (timerFlushData.Enabled)
            {
                timerFlushData.Stop();
            }
            try
            {
                if ((ConnectAvailable) && (bufferDatagGPS64.GetCount() > 0))
                {
                    int insertedRowsCount = InsertData64(bufferDatagGPS64.GetArray());

                    if (insertedRowsCount > 0)
                    {
                        // ��� �������� - ������� ��������� ������ � ����� ���� ������.
                        // ������� ���������� ������
                        bufferDatagGPS64.Clear();
                        // ����������� ������ �� ������ ���� ������ � ������� DataGps64
                        if (TransferBuffer64() != ExecutionStatus.OK)
                        {
                            CheckConnect();
                        }
                    }
                    else if (CheckConnect())
                    {
                        // ���� ����������� ������� ��� � ���� ������� - 
                        // ���-�� �������� ���������, ������ ��������� � ������, 
                        // ������������� ������� ��������� �����
                        bufferDatagGPS64.Clear();
                    }
                }
            }
            catch (Exception Ex)
            {
                ErrorHandler.Handle(Ex);
                CheckConnect();
            }
            finally
            {
                // ����� �������
                timerFlushData.Start();
            }
        }


        /// <summary>
        /// ������� ��� ������� � ����� ��
        /// </summary>
        /// <returns>ISrvDbCommand</returns>
        protected virtual ISrvDbCommand GetInsertDataInBufferDBCommand()
        {
            throw new NotImplementedException(String.Format(
              AppError.NOT_IMPLEMENTED, "GetInsertDataInBufferDBCommandType", "BaseDataManager"));
        }

        /// <summary>
        /// ������� ��� ������� � �����64 ��
        /// </summary>
        /// <returns>ISrvDbCommand</returns>
        protected virtual ISrvDbCommand GetInsertDataInBuffer64DBCommand()
        {
            throw new NotImplementedException(String.Format(
              AppError.NOT_IMPLEMENTED, "GetInsertDataInBuffer64DBCommandType", "BaseDataManager"));
        }

        #region ����� ������

        /// <summary>
        /// ������ ������� ����������� ������ �� ������ 
        /// datagpsbuffer_... � �������� ������� DataGPS
        /// </summary>
        /// <returns>FetchDataStatus</returns>
        public ExecutionStatus TransferBuffer()
        {
            if (SrvSettings.DebugMode)
            {
                WriteDebug(String.Format(
                  "����� ������ �� ������ � �������� �������. ������ �����������: {0}",
                  ConnectionStringBase));
            }
            return GetTransferBufferDBCommand().Execute();
        }

        public ExecutionStatus TransferBuffer64()
        {
            if (SrvSettings.DebugMode)
            {
                WriteDebug(String.Format(
                  "����� ������ �� ������ 64 � �������� �������. ������ �����������: {0}",
                  ConnectionStringBase));
            }
            return GetTransferBuffer64DBCommand().Execute();
        }

        /// <summary>
        /// ������� ��� ����������� ������ �� � ������� DataGPS
        /// </summary>
        /// <returns>ISrvDbCommand</returns>
        protected virtual ISrvDbCommand GetTransferBufferDBCommand()
        {
            throw new NotImplementedException(String.Format(
              AppError.NOT_IMPLEMENTED, "GetTransferBufferDBCommandType", "BaseDataManager"));
        }


        /// <summary>
        /// ������� ��� ����������� ������ �� � ������� DataGPS
        /// </summary>
        /// <returns>ISrvDbCommand</returns>
        protected virtual ISrvDbCommand GetTransferBuffer64DBCommand()
        {
            throw new NotImplementedException(String.Format(
              AppError.NOT_IMPLEMENTED, "GetTransferBuffer64DBCommandType", "BaseDataManager"));
        }

        #endregion

        #endregion


        #region ����������� ������

        /// <summary>
        /// ������ ���������� ������� ������� � PriorTimeValue
        /// �� �������� �������
        /// </summary>
        /// <param name="PriorTimeValue">���������� ������� �������</param>
        /// <returns>��������� ��������</returns>
        protected TimeSpan GetPassedTime(DateTime PriorTimeValue)
        {
            DateTime dtNow = DateTime.Now;
            // ��� ������
            DateTime dtNowM = new DateTime(
              dtNow.Year, dtNow.Month, dtNow.Day, dtNow.Hour, dtNow.Minute, 0);

            return dtNowM - PriorTimeValue;
        }

        /// <summary>
        /// Error handle
        /// </summary>
        protected void ErrorHandle(Exception Ex)
        {
            ErrorHandler.Handle(Ex);
        }

        /// <summary>
        /// Write to log
        /// </summary>
        protected void WriteDebug(string message)
        {
            Logger.WriteDebug("dm>", message);
        }

        #endregion

        #region �������� A1 �������� ��������

        /// <summary>
        /// �������� ���-�� �������������� ���������
        /// </summary>
        /// <returns>���-�� �������������� ���������</returns>
        public int GetNewMessageCount()
        {
            const string msgDebug = "A1. ������� ���-�� �������������� ���������";
            WriteDebug(msgDebug);

            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectNewMessageCount>();
            return (command.Execute() == ExecutionStatus.OK) ?
              ((A1SelectNewMessageCount)command).Count : 0;
        }

        /// <summary>
        /// ������� ��������������� ���������� � ��������� �����
        /// ���������-������ ��������������� ��� ��������� ����������
        /// </summary>
        /// <returns>������ ��������������� ����������</returns>
        public List<int> GetMobitelsInNewMessages()
        {
            WriteDebug("A1. �������� �� �� ������ ���������� ��� ������� ������������� ���������.");

            List<int> result = new List<int>();
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectMobitelsInNewMessages>();

            try
            {
                ExecutionStatus status;
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out status))
                    {
                        if ((status == ExecutionStatus.OK) && (reader != null))
                        {
                            while (reader.Read())
                            {
                                result.Add(reader.GetInt32("MobitelId"));
                            }
                        }
                    }
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out status))
                    {
                        if ((status == ExecutionStatus.OK) && (reader != null))
                        {
                            while (reader.Read())
                            {
                                int k = reader.GetOrdinal("MobitelId");
                                result.Add(reader.GetInt32(k));
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ErrorHandler.Handle(Ex);
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetMobitelsInNewMessages: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ��������� ���������� � ����� ���������� ��� ���������.
        /// �������� ������ ��������� ��������� ��� ������ ������ ������
        /// </summary>
        /// <param name="mobitelId">Mobitel_ID ������� mobitels</param>
        /// <param name="shortId">�������� ID ���������</param>
        /// <returns>������ NewMessage</returns>
        internal List<NewMessage> GetNewMessages(int mobitelId, string shortId)
        {
            WriteDebug(String.Format(
              "A1. �������� �� �� ���������� � �������������� ���������� ��������� {0}",
              mobitelId));

            List<NewMessage> result = new List<NewMessage>();
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectNewMessages>();
            command.Init(new object[] { mobitelId });

            try
            {
                ExecutionStatus status;
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out status))
                    {
                        if ((status == ExecutionStatus.OK) && (reader != null))
                        {
                            while (reader.Read())
                            {
                                NewMessage message = new NewMessage();
                                message.MessageId = reader.GetInt32("MessageId");
                                message.MobitelID = mobitelId;
                                message.DevIdShort = shortId;
                                message.CommandID = reader.GetInt32("CommandID");
                                message.MessageCounter = reader.GetInt32("MessageCounter");

                                result.Add(message);
                            }
                        }
                    }
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out status))
                    {
                        if ((status == ExecutionStatus.OK) && (reader != null))
                        {
                            while (reader.Read())
                            {
                                NewMessage message = new NewMessage();
                                int k = reader.GetOrdinal("MessageID");
                                if(!reader.IsDBNull(k))
                                    message.MessageId = reader.GetInt32(k);
                                message.MobitelID = mobitelId;
                                message.DevIdShort = shortId;
                                k = reader.GetOrdinal("CommandID");
                                if (!reader.IsDBNull(k))
                                	message.CommandID = (int)reader.GetInt16(k);
                                k = reader.GetOrdinal("MessageCounter");
                                if (!reader.IsDBNull(k))
                                    message.MessageCounter = reader.GetInt32(k);

                                result.Add(message);
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ErrorHandler.Handle(Ex);
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetNewMessages: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� �������� SMS �� ����
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">SmsAddrConfig c ������������ ����������������</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus GetSmsAddrConfig(int messageId, SmsAddrConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", String.Format(
                  AppError.ARGUMENT_NULL, "GetSmsAddrConfig", "BaseDataManager", "config"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� �������� SMS (MessageId = {0})", messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigSms>();
            command.Init(new object[] { messageId });

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                config.DsptEmailSMS = reader.GetString("DsptEmailSMS");
                                config.DsptEmailGprs = reader.GetString("DsptEmailGprs");
                                config.SmsCentre = reader.GetString("SmsCentre");
                                config.SmsDspt = reader.GetString("SmsDspt");
                                config.SmsEmailGate = reader.GetString("SmsEmailGate");
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                int k = reader.GetOrdinal("DsptEmailSMS");
                                config.DsptEmailSMS = reader.GetString(k);
                                k = reader.GetOrdinal("DsptEmailGprs");
                                config.DsptEmailGprs = reader.GetString(k);
                                k = reader.GetOrdinal("SmsCentre");
                                config.SmsCentre = reader.GetString(k);
                                k = reader.GetOrdinal("SmsDspt");
                                config.SmsDspt = reader.GetString(k);
                                k = reader.GetOrdinal("SmsEmailGate");
                                config.SmsEmailGate = reader.GetString(k);
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetSmsAddrConfig: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� �������� ���������� ������� �� ����
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">PhoneNumberConfig c ������������ ����������������</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus GetPhoneNumberConfig(int messageId, PhoneNumberConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", String.Format(
                  AppError.ARGUMENT_NULL, "GetPhoneNumberConfig", "BaseDataManager", "config"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� �������� ���������� ������� (MessageId = {0})", messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigPhone>();
            command.Init(new object[] { messageId });

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                config.NumberSOS = reader.GetString("NumberSOS");
                                config.NumberDspt = reader.GetString("NumberDspt");
                                config.NumberAccept1 = reader.GetString("NumberAccept1");
                                config.NumberAccept2 = reader.GetString("NumberAccept2");
                                config.NumberAccept3 = reader.GetString("NumberAccept3");
                                config.Name1 = reader.GetString("Name1");
                                config.Name2 = reader.GetString("Name2");
                                config.Name3 = reader.GetString("Name3");
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                int k = reader.GetOrdinal("NumberSOS");
                                config.NumberSOS = reader.GetString(k);
                                k = reader.GetOrdinal("NumberDspt");
                                config.NumberDspt = reader.GetString(k);
                                k = reader.GetOrdinal("NumberAccept1");
                                config.NumberAccept1 = reader.GetString(k);
                                k = reader.GetOrdinal("NumberAccept2");
                                config.NumberAccept2 = reader.GetString(k);
                                k = reader.GetOrdinal("NumberAccept3");
                                config.NumberAccept3 = reader.GetString(k);
                                k = reader.GetOrdinal("Name1");
                                config.Name1 = reader.GetString(k);
                                k = reader.GetOrdinal("Name2");
                                config.Name2 = reader.GetString(k);
                                k = reader.GetOrdinal("Name3");
                                config.Name3 = reader.GetString(k);
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetPhoneConfig: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� �������� ������� �� ����
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">EventConfig c ������������ ����������������</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus GetEventConfig(int messageId, EventConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", String.Format(
                  AppError.ARGUMENT_NULL, "GetEventConfig", "BaseDataManager", "config"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� �������� ������� (MessageId = {0})", messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigEvent>();
            command.Init(new object[] { messageId });

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("deltaTimeZone")))
                                {
                                    config.SpeedChange = (sbyte)reader.GetInt16("deltaTimeZone");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("dist1Log")))
                                {
                                    config.CourseBend = reader.GetUInt16("dist1Log");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("dist2Send")))
                                {
                                    config.Distance1 = reader.GetUInt16("dist2Send");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("dist3Zone")))
                                {
                                    config.Distance2 = reader.GetUInt16("dist3Zone");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("tmr1Log")))
                                {
                                    config.MinSpeed = reader.GetUInt16("tmr1Log");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("tmr2Send")))
                                {
                                    config.Timer1 = reader.GetUInt16("tmr2Send");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("tmr3Zone")))
                                {
                                    config.Timer2 = reader.GetUInt16("tmr3Zone");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("gprsEmail")))
                                {
                                    config.GprsEmail = reader.GetUInt16("gprsEmail");
                                }
                                for (int i = 1; i <= 31; i++)
                                {
                                    string field = String.Concat("maskEvent", i);

                                    if (!reader.IsDBNull(reader.GetOrdinal(field)))
                                    {
                                        config.EventMask[i - 1] = reader.GetUInt16(field);
                                    }
                                }
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                int k;
                                if (!reader.IsDBNull(reader.GetOrdinal("deltaTimeZone")))
                                {
                                    k = reader.GetOrdinal("deltaTimeZone");
                                    config.SpeedChange = (sbyte)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("dist1Log")))
                                {
                                    k = reader.GetOrdinal("dist1Log");
                                    config.CourseBend = (ushort)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("dist2Send")))
                                {
                                    k = reader.GetOrdinal("dist2Send");
                                    config.Distance1 = (ushort)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("dist3Zone")))
                                {
                                    k = reader.GetOrdinal("dist3Zone");
                                    config.Distance2 = (ushort)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("tmr1Log")))
                                {
                                    k = reader.GetOrdinal("tmr1Log");
                                    config.MinSpeed = (ushort)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("tmr2Send")))
                                {
                                    k = reader.GetOrdinal("tmr2Send");
                                    config.Timer1 = (ushort)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("tmr3Zone")))
                                {
                                    k = reader.GetOrdinal("tmr3Zone");
                                    config.Timer2 = (ushort)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("gprsEmail")))
                                {
                                    k = reader.GetOrdinal("gprsEmail");
                                    config.GprsEmail = (ushort)reader.GetInt32(k);
                                }
                                for (int i = 1; i <= 31; i++)
                                {
                                    string field = String.Concat("maskEvent", i);

                                    if (!reader.IsDBNull(reader.GetOrdinal(field)))
                                    {
                                        k = reader.GetOrdinal(field);
                                        config.EventMask[i - 1] = (ushort)reader.GetInt32(k);
                                    }
                                }
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    } // using
                } // else if
            }
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetEventConfig: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� �������� ���������� ���������� �� ����
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">UniqueConfig c ������������ ����������������</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus GetUniqueConfig(int messageId, UniqueConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", String.Format(
                  AppError.ARGUMENT_NULL, "GetUniqueConfig", "BaseDataManager", "config"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� �������� ���������� ���������� (MessageId = {0})", messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigUnique>();
            command.Init(new object[] { messageId });

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                config.DispatcherID = reader.GetString("DispatcherID");
                                config.Password = reader.GetString("Password");
                                config.TmpPassword = reader.GetString("TmpPassword");
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                int k = reader.GetOrdinal("DispatcherID");
                                config.DispatcherID = reader.GetString(k);
                                k = reader.GetOrdinal("Password");
                                config.Password = reader.GetString(k);
                                k = reader.GetOrdinal("TmpPassword");
                                config.TmpPassword = reader.GetString(k);
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetUniqueConfig: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� �������� ����������������� ���������� �� ����
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">IdConfig c ������������ ����������������</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus GetIdConfig(int messageId, IdConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", String.Format(
                  AppError.ARGUMENT_NULL, "GetIdConfig", "BaseDataManager", "config"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� �������� ����������������� ���������� (MessageId = {0})", messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigId>();
            command.Init(new object[] { messageId });

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("devIdShort")))
                                {
                                    config.DevIdShort = reader.GetString("devIdShort");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("devIdLong")))
                                {
                                    config.DevIdLong = reader.GetString("devIdLong");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("moduleIdGps")))
                                {
                                    config.ModuleIdGps = reader.GetString("moduleIdGps");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("moduleIdGsm")))
                                {
                                    config.ModuleIdGsm = reader.GetString("moduleIdGsm");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("moduleIdRf")))
                                {
                                    config.ModuleIdRf = reader.GetString("moduleIdRf");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("moduleIdSs")))
                                {
                                    config.ModuleIdSs = reader.GetString("moduleIdSs");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("verProtocolLong")))
                                {
                                    config.VerProtocolLong = reader.GetString("verProtocolLong");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("verProtocolShort")))
                                {
                                    config.VerProtocolShort = reader.GetString("verProtocolShort");
                                }
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                int k = 0;

                                if (!reader.IsDBNull(reader.GetOrdinal("devIdShort")))
                                {
                                    k = reader.GetOrdinal("devIdShort");
                                    config.DevIdShort = reader.GetString(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("devIdLong")))
                                {
                                    k = reader.GetOrdinal("devIdLong");
                                    config.DevIdLong = reader.GetString(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("moduleIdGps")))
                                {
                                    k = reader.GetOrdinal("moduleIdGps");
                                    config.ModuleIdGps = reader.GetString(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("moduleIdGsm")))
                                {
                                    k = reader.GetOrdinal("moduleIdGsm");
                                    config.ModuleIdGsm = reader.GetString(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("moduleIdRf")))
                                {
                                    k = reader.GetOrdinal("moduleIdRf");
                                    config.ModuleIdRf = reader.GetString(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("moduleIdSs")))
                                {
                                    k = reader.GetOrdinal("moduleIdSs");
                                    config.ModuleIdSs = reader.GetString(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("verProtocolLong")))
                                {
                                    k = reader.GetOrdinal("verProtocolLong");
                                    config.VerProtocolLong = reader.GetString(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("verProtocolShort")))
                                {
                                    k = reader.GetOrdinal("verProtocolShort");
                                    config.VerProtocolShort = reader.GetString(k);
                                }
                            } // if
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        } // if
                    } // using
                } // else if
            } // try
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetIdConfig: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� ��������� �������� �� ����
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">MessageToDriver c ������������ ����������������</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus GetMessageToDriver(int messageId, MessageToDriver config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", String.Format(
                  AppError.ARGUMENT_NULL, "GetMessageToDriver", "BaseDataManager", "config"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� ��������� �������� (MessageId = {0})", messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectMessageToDriver>();
            command.Init(new object[] { messageId });

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                config.Text = reader.GetString("Message");
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                int k = reader.GetOrdinal("Message");
                                config.Text = reader.GetString(k);
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetMessageToDriver: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� �������� ����������� ��� �� ����.
        /// ���� ����� ������� ��� ���������� ������, ����
        /// ��� ������� �������.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">UniqueConfig c ������������ ����������������</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus GetZoneConfig(int messageId, ZoneConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", String.Format(
                  AppError.ARGUMENT_NULL, "GetZoneConfig", "BaseDataManager", "config"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� �������� ����������� ��� (MessageId = {0})", messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectZones>();
            command.Init(new object[] { messageId });

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            while (reader.Read())
                            {
                                int zoneID = reader.GetInt32("Zone_ID");
                                List<ZonePoint> points = new List<ZonePoint>();

                                // ���� ����� ��� ������� ���� ���������� - ����� ����������
                                if ((GetZonePoints(zoneID, points) == ExecutionStatus.OK) &&
                                    (points.Count > 0))
                                {
                                    config.AddZone(new Zone(
                                                       new ZoneMask(reader.GetByte("In_flag"), reader.GetByte("Out_flag"),
                                                                    reader.GetByte("In_flag2"), reader.GetByte("Out_flag2")),
                                                       points.ToArray()));
                                }
                            }
                            if (config.ZoneCount == 0)
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            while (reader.Read())
                            {
                                int k0 = reader.GetOrdinal("Zone_ID");
                                int k1 = reader.GetOrdinal("In_flag");
                                int k2 = reader.GetOrdinal("Out_flag");
                                int k3 = reader.GetOrdinal("In_flag2");
                                int k4 = reader.GetOrdinal("Out_flag2");
                                int zoneID = reader.GetInt32(k0);
                                List<ZonePoint> points = new List<ZonePoint>();

                                // ���� ����� ��� ������� ���� ���������� - ����� ����������
                                if ((GetZonePoints(zoneID, points) == ExecutionStatus.OK) &&
                                    (points.Count > 0))
                                {
                                    config.AddZone(new Zone(
                                                       new ZoneMask(reader.GetByte(k1), reader.GetByte(k2),
                                                                    reader.GetByte(k3), reader.GetByte(k4)),
                                                       points.ToArray()));
                                }
                            }
                            if (config.ZoneCount == 0)
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetZoneConfig: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� ����� ����������� ��� �� ����.
        /// </summary>
        /// <param name="messageId">zoneId</param>
        /// <param name="config">points ������ ��� ���������� �������</param>
        /// <returns>Fetch data status</returns>
        private ExecutionStatus GetZonePoints(int zoneId, List<ZonePoint> points)
        {
            if (points == null)
            {
                throw new ArgumentNullException("points", String.Format(
                  AppError.ARGUMENT_NULL, "GetZonePoints", "BaseDataManager", "points"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� ����� ����������� ���� {0}", zoneId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectZonePoints>();
            command.Init(new object[] { zoneId });
            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            while (reader.Read())
                            {
                                points.Add(new ZonePoint(
                                               reader.GetInt32("Latitude"), reader.GetInt32("Longitude")));
                            }
                        }
                    }
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            while (reader.Read())
                            {
                                int k = reader.GetOrdinal("Latitude");
                                int m = reader.GetOrdinal("Longitude");
                                points.Add(new ZonePoint(reader.GetInt32(k), reader.GetInt32(m)));
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetZonePoints: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� �������� �������� GPRS
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">GprsBaseConfig c ������������ ����������������</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus GetGprsBaseConfig(int messageId, GprsBaseConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", String.Format(
                  AppError.ARGUMENT_NULL, "GetGprsBaseConfig", "BaseDataManager", "config"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� �������� �������� GPRS (MessageId = {0})", messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigGprsBase>();
            command.Init(new object[] { messageId });

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                config.Mode = reader.GetUInt16("Mode");
                                config.ApnServer = reader.GetString("Apnserv");
                                config.ApnLogin = reader.GetString("Apnun");
                                config.ApnPassword = reader.GetString("Apnpw");
                                config.DnsServer = reader.GetString("Dnsserv1");
                                config.DialNumber = reader.GetString("Dialn1");
                                config.GprsLogin = reader.GetString("Ispun");
                                config.GprsPassword = reader.GetString("Isppw");
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                int k = reader.GetOrdinal("Mode");
                                config.Mode = (ushort)reader.GetInt16(k);
                                k = reader.GetOrdinal("Apnserv");
                                config.ApnServer = reader.GetString(k);
                                k = reader.GetOrdinal("Apnun");
                                config.ApnLogin = reader.GetString(k);
                                k = reader.GetOrdinal("Apnpw");
                                config.ApnPassword = reader.GetString(k);
                                k = reader.GetOrdinal("Dnsserv1");
                                config.DnsServer = reader.GetString(k);
                                k = reader.GetOrdinal("Dialn1");
                                config.DialNumber = reader.GetString(k);
                                k = reader.GetOrdinal("Ispun");
                                config.GprsLogin = reader.GetString(k);
                                k = reader.GetOrdinal("Isppw");
                                config.GprsPassword = reader.GetString(k);
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetGprsBaseConfig: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� �������� GPRS Email
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">GprsEmailConfig c ������������ ����������������</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus GetGprsEmailConfig(int messageId, GprsEmailConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", String.Format(
                  AppError.ARGUMENT_NULL, "GetGprsEmailConfig", "BaseDataManager", "config"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� �������� GPRS Email (MessageId = {0})", messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigGprsEmail>();
            command.Init(new object[] { messageId });

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                config.SmtpServer = reader.GetString("Smtpserv");
                                config.SmtpLogin = reader.GetString("Smtpun");
                                config.SmtpPassword = reader.GetString("Smtppw");
                                config.Pop3Server = reader.GetString("Pop3serv");
                                config.Pop3Login = reader.GetString("Pop3un");
                                config.Pop3Password = reader.GetString("Pop3pw");
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                int k = reader.GetOrdinal("Smtpserv");
                                config.SmtpServer = reader.GetString(k);
                                k = reader.GetOrdinal("Smtpun");
                                config.SmtpLogin = reader.GetString(k);
                                k = reader.GetOrdinal("Smtppw");
                                config.SmtpPassword = reader.GetString(k);
                                k = reader.GetOrdinal("Pop3serv");
                                config.Pop3Server = reader.GetString(k);
                                k = reader.GetOrdinal("Pop3un");
                                config.Pop3Login = reader.GetString(k);
                                k = reader.GetOrdinal("Pop3pw");
                                config.Pop3Password = reader.GetString(k);
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetGprsEmailConfig: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� �������� GPRS Provider
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">GprsProviderConfig c ������������ ����������������</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus GetGprsProviderConfig(int messageId, GprsProviderConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config", String.Format(
                  AppError.ARGUMENT_NULL, "GetGprsProviderConfig", "BaseDataManager", "config"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� �������� GPRS Provider (MessageId = {0})", messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigGprsProvider>();
            command.Init(new object[] { messageId });

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                config.Domain = reader.GetString("Domain");
                                config.InitString = reader.GetString("InitString");
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                int k = reader.GetOrdinal("Domain");
                                config.Domain = reader.GetString(k);
                                k = reader.GetOrdinal("InitString");
                                config.InitString = reader.GetString(k);
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetGprsProviderConfig: Connection with server attempt clousing!");
            }

            return result;
        }

        /// <summary>
        /// ������� ���������� ������� GPS ������
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="query">DataGpsQuery c ������������ ����������������</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus GetDataGpsQuery(int messageId, DataGpsQuery query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query", String.Format(
                  AppError.ARGUMENT_NULL, "GetDataGpsQuery", "BaseDataManager", "query"));
            }
            WriteDebug(String.Format(
              "A1. �������� �� �� ���������� ������� GPS ������ (MessageId = {0})", messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectDataGpsQuery>();
            command.Init(new object[] { messageId });

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                // ��������! � ���� ��� �������� ������������� �������� 
                                // �������� ���� �� ������ � ���� �������������� MCA3
                                // ����� ������������� �������� - �����-�� ������

                                int tmp;
                                if (!reader.IsDBNull(reader.GetOrdinal("CheckMask")))
                                {
                                    tmp = reader.GetInt32("CheckMask");
                                    query.CheckMask = tmp < 0 ? 0 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Events")))
                                {
                                    tmp = reader.GetInt32("Events");
                                    query.Events = tmp < 0 ? 0 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LastRecords")))
                                {
                                    tmp = reader.GetInt32("LastRecords");
                                    query.LastRecords = tmp < 0 ? 0 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LastTimes")))
                                {
                                    tmp = reader.GetInt32("LastTimes");
                                    query.LastTimes = tmp < 0 ? 0 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("MaxSmsNum")))
                                {
                                    tmp = reader.GetInt32("MaxSmsNum");
                                    query.MaxSmsNumber = tmp < 0 ? 0 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("WhatSend")))
                                {
                                    tmp = reader.GetInt32("WhatSend");
                                    query.WhatSend = tmp < 0 ? 1023 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("UnixTimeStart")))
                                {
                                    query.StartTime = reader.GetInt32("UnixTimeStart");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("SpeedStart")))
                                {
                                    query.StartSpeed = (byte)reader.GetInt32("SpeedStart");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LatitudeStart")))
                                {
                                    query.StartLatitude = reader.GetInt32("LatitudeStart");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LongitudeStart")))
                                {
                                    query.StartLongitude = reader.GetInt32("LongitudeStart");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("AltitudeStart")))
                                {
                                    query.StartAltitude = (byte)reader.GetInt32("AltitudeStart");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("DirectionStart")))
                                {
                                    query.StartDirection = (byte)reader.GetInt32("DirectionStart");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("NumbersStart")))
                                {
                                    query.StartLogId = reader.GetInt32("NumbersStart");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("UnixTimeEnd")))
                                {
                                    query.EndTime = reader.GetInt32("UnixTimeEnd");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("SpeedEnd")))
                                {
                                    query.EndSpeed = (byte)reader.GetInt32("SpeedEnd");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LatitudeEnd")))
                                {
                                    query.EndLatitude = reader.GetInt32("LatitudeEnd");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LongitudeEnd")))
                                {
                                    query.EndLongitude = reader.GetInt32("LongitudeEnd");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("AltitudeEnd")))
                                {
                                    query.EndAltitude = (byte)reader.GetInt32("AltitudeEnd");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("DirectionEnd")))
                                {
                                    query.EndDirection = (byte)reader.GetInt32("DirectionEnd");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("NumbersEnd")))
                                {
                                    query.EndLogId = reader.GetInt32("NumbersEnd");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Number1")))
                                {
                                    query.LogId1 = reader.GetInt32("Number1");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Number2")))
                                {
                                    query.LogId2 = reader.GetInt32("Number2");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Number3")))
                                {
                                    query.LogId3 = reader.GetInt32("Number3");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Number4")))
                                {
                                    query.LogId4 = reader.GetInt32("Number4");
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Number5")))
                                {
                                    query.LogId5 = reader.GetInt32("Number5");
                                }
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        }
                    }
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                    {
                        if ((result == ExecutionStatus.OK) && (reader != null))
                        {
                            if (reader.Read())
                            {
                                // ��������! � ���� ��� �������� ������������� �������� 
                                // �������� ���� �� ������ � ���� �������������� MCA3
                                // ����� ������������� �������� - �����-�� ������

                                int tmp;
                                int k;
                                if (!reader.IsDBNull(reader.GetOrdinal("CheckMask")))
                                {
                                    k = reader.GetOrdinal("CheckMask");
                                    tmp = reader.GetInt32(k);
                                    query.CheckMask = tmp < 0 ? 0 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Events")))
                                {
                                    k = reader.GetOrdinal("Events");
                                    tmp = reader.GetInt32(k);
                                    query.Events = tmp < 0 ? 0 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LastRecords")))
                                {
                                    k = reader.GetOrdinal("LastRecords");
                                    tmp = reader.GetInt32(k);
                                    query.LastRecords = tmp < 0 ? 0 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LastTimes")))
                                {
                                    k = reader.GetOrdinal("LastTimes");
                                    tmp = reader.GetInt32(k);
                                    query.LastTimes = tmp < 0 ? 0 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("MaxSmsNum")))
                                {
                                    k = reader.GetOrdinal("MaxSmsNum");
                                    tmp = reader.GetInt32(k);
                                    query.MaxSmsNumber = tmp < 0 ? 0 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("WhatSend")))
                                {
                                    k = reader.GetOrdinal("WhatSend");
                                    tmp = reader.GetInt32(k);
                                    query.WhatSend = tmp < 0 ? 1023 : (uint)tmp;
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("UnixTimeStart")))
                                {
                                    k = reader.GetOrdinal("UnixTimeStart");
                                    query.StartTime = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("SpeedStart")))
                                {
                                    k = reader.GetOrdinal("SpeedStart");
                                    query.StartSpeed = (byte)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LatitudeStart")))
                                {
                                    k = reader.GetOrdinal("LatitudeStart");
                                    query.StartLatitude = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LongitudeStart")))
                                {
                                    k = reader.GetOrdinal("LongitudeStart");
                                    query.StartLongitude = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("AltitudeStart")))
                                {
                                    k = reader.GetOrdinal("AltitudeStart");
                                    query.StartAltitude = (byte)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("DirectionStart")))
                                {
                                    k = reader.GetOrdinal("DirectionStart");
                                    query.StartDirection = (byte)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("NumbersStart")))
                                {
                                    k = reader.GetOrdinal("NumbersStart");
                                    query.StartLogId = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("UnixTimeEnd")))
                                {
                                    k = reader.GetOrdinal("UnixTimeEnd");
                                    query.EndTime = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("SpeedEnd")))
                                {
                                    k = reader.GetOrdinal("SpeedEnd");
                                    query.EndSpeed = (byte)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LatitudeEnd")))
                                {
                                    k = reader.GetOrdinal("LatitudeEnd");
                                    query.EndLatitude = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("LongitudeEnd")))
                                {
                                    k = reader.GetOrdinal("LongitudeEnd");
                                    query.EndLongitude = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("AltitudeEnd")))
                                {
                                    k = reader.GetOrdinal("AltitudeEnd");
                                    query.EndAltitude = (byte)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("DirectionEnd")))
                                {
                                    k = reader.GetOrdinal("DirectionEnd");
                                    query.EndDirection = (byte)reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("NumbersEnd")))
                                {
                                    k = reader.GetOrdinal("NumbersEnd");
                                    query.EndLogId = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Number1")))
                                {
                                    k = reader.GetOrdinal("Number1");
                                    query.LogId1 = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Number2")))
                                {
                                    k = reader.GetOrdinal("Number2");
                                    query.LogId2 = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Number3")))
                                {
                                    k = reader.GetOrdinal("Number3");
                                    query.LogId3 = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Number4")))
                                {
                                    k = reader.GetOrdinal("Number4");
                                    query.LogId4 = reader.GetInt32(k);
                                }
                                if (!reader.IsDBNull(reader.GetOrdinal("Number5")))
                                {
                                    k = reader.GetOrdinal("Number5");
                                    query.LogId5 = reader.GetInt32(k);
                                }
                            }
                            else
                            {
                                result = ExecutionStatus.ERROR_DATA;
                            }
                        } // if
                    } // using
                } // else if
            } // try
            catch (Exception Ex)
            {
                result = ExecutionStatus.ERROR_DATA;
                throw Ex;
            }
            finally
            {
                //command.CloseConnection();
                Logger.Write("GetDataGpsQuery: Connection with server attempt clousing!");
            }

            return result;
        }

        #endregion

        #region �������� A1 ���������� ��������

        /// <summary>
        /// �������� ��������������� �� �������� 
        /// </summary>
        /// <param name="devShortId">�������� ������������� ���������</param>
        /// <param name="MobitelId">Mobitelid - � ������ ��������� ������</param>
        /// <returns>True - ���� �������� � �������� devShortId ���������������</returns>
        public bool IsMobitelExists(string devShortId, out int MobitelId)
        {
            if (String.IsNullOrEmpty(devShortId))
            {
                throw new ArgumentNullException("devShortId", String.Format(
                  AppError.ARGUMENT_NULL, "IsMobitelExists", "BaseDataManager", "devShortId"));
            }
            WriteDebug(String.Format(
              "A1. ����� ��������� � �� �� ��� ��������� ������������� (devShortId = {0})",
              devShortId));

            bool result = false;
            MobitelId = -1;

            ISrvDbCommand command = _commandCache.GetDbCommand<GetMobitelIdDbCommand>();
            // ������� �������� ����������:
            // 1. devIdShort
            command.Init(new object[] { devShortId });

            if (command.Execute() == ExecutionStatus.OK)
            {
                MobitelId = ((GetMobitelIdDbCommand)command).MobitelId;
                result = true;
            }

            return result;
        }

        /// <summary>
        /// ���������� ��������� ��������� �� ��������� � �������� source � messages
        /// </summary>
        /// <param name="source">�������� ����� ���������</param>
        /// <param name="commandId">������������� �������</param>
        /// <param name="mobitelId">Mobitel_ID</param>
        /// <param name="messageCounter">���������� ����� �������������� ������� ��� ������� ���������</param>
        /// <param name="messageId">MessageID ��� ���������� ������</param>
        /// <returns>FetchDataStatus</returns>
        internal ExecutionStatus SaveInMessages(string source, int commandId,
          int mobitelId, int messageCounter, out int messageId)
        {
            if (String.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException("source", String.Format(
                  AppError.ARGUMENT_NULL, "SaveInMessages", "BaseDataManager", "source"));
            }
            ExecutionStatus result = ExecutionStatus.OK;
            int sourceId = 0;
            messageId = -1;

            // 1. ������� �������� � ������ ����� 46 � 49
            if ((commandId != (int)CommandDescriptor.DataGpsAnswer) &&
              (commandId != (int)CommandDescriptor.DataGpsAuto))
            {
                WriteDebug(String.Format(
                  "A1. ���������� � �� ���������� ��������� ��������� � source � messages (CommandId = {0})",
                  commandId));

                ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertSource>();
                command.Init(new object[] { source });
                result = command.Execute();
                if (result == ExecutionStatus.OK)
                {
                    sourceId = ((A1InsertSource)command).NewSourceId;
                }
                // 2. ������ ����� � � messages �����������
                if (result == ExecutionStatus.OK)
                {
                    ISrvDbCommand command2 = _commandCache.GetDbCommand<A1InsertMessages>();
                    /// ������� �������� ����������:
                    /// 1. Time1 
                    /// 2. Command_ID
                    /// 3. Source_ID
                    /// 4. Address(email)
                    /// 5. Mobitel_ID
                    /// 6. MessageCounter
                    command2.Init(new object[] { RCS.Protocol.A1.Util.ToUnixTime(DateTime.Now), 
          commandId, sourceId, "", mobitelId, messageCounter });

                    result = command2.Execute();
                    if (result == ExecutionStatus.OK)
                    {
                        messageId = ((A1InsertMessages)command2).NewMessageId;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// ���������� ��������� ������������� ��������� � ������� source � 
        /// ��������� ����� ����� � messages �� ������� � source
        /// </summary>
        /// <param name="source">�������� ����� ���������</param>
        /// <param name="messageId">MessageID ��� ���������� ������</param>
        /// <returns>FetchDataStatus</returns>
        public ExecutionStatus SaveOutMessage(int messageId, string source)
        {
            if (String.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException(String.Format("source",
                  AppError.ARGUMENT_NULL, "SaveOutMessage", "BaseDataManager", "source"));
            }
            WriteDebug(String.Format(
              "A1. ���������� � �� ������������� ��������� � source � messages (messageId = {0})",
              messageId));

            ExecutionStatus result = ExecutionStatus.OK;
            int sourceId = 0;

            // 1. ������� �������� � ������ 
            ISrvDbCommand commandInsert = _commandCache.GetDbCommand<A1InsertSource>();
            commandInsert.Init(new object[] { source });
            result = commandInsert.Execute();
            if (result == ExecutionStatus.OK)
            {
                sourceId = ((A1InsertSource)commandInsert).NewSourceId;

                // 2. ������ ����� � � messages ��������
                ISrvDbCommand commandUpdate = _commandCache.GetDbCommand<A1UpdateSentMessage>();
                // ������� �������� ����������:
                // 1. Message_ID
                // 2. SourceId
                commandUpdate.Init(new object[] { messageId, sourceId });
                result = commandUpdate.Execute();
            }

            return result;
        }

        /// <summary>
        /// ���������� ������������ sms �������
        /// </summary>
        /// <param name="messageId">Message_ID �� ������� Messages</param>
        /// <param name="config">SmsAddrConfig</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus SaveConfigSms(int messageId, SmsAddrConfig config)
        {
            WriteDebug("A1. ���������� � �� ���������� ��������� � ����������� SMS �������");

            ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigSms>();
            // ������� �������� ����������:
            // 1. Message_ID
            // 2. MobitelId (����� � ConfigMain ���� ConfigSms_ID)
            // 3. SmsAddrConfig
            command.Init(new object[] { messageId, config.MobitelID, config });
            return command.Execute();
        }

        /// <summary>
        /// ���������� ������������ ���������� ������� 
        /// </summary>
        /// <param name="messageId">Messageid</param>
        /// <param name="config">PhoneNumberConfig</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus SaveConfigPhoneNumbers(int messageId, PhoneNumberConfig config)
        {
            WriteDebug("A1. ���������� � �� ���������� ��������� � ����������� ���������� �������");

            ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigPhone>();
            // ������� �������� ����������:
            // 1. Message_ID
            // 2. MobitelId (����� � ConfigMain ���� ConfigTel_ID)
            // 3. PhoneNumberConfig
            command.Init(new object[] { messageId, config.MobitelID, config });
            return command.Execute();
        }

        /// <summary>
        /// ���������� �������� �������
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">EventConfig</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus SaveConfigEvents(int messageId, EventConfig config)
        {
            WriteDebug("A1. ���������� � �� ���������� ��������� � ����������� �������");

            ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigEvent>();
            // ������� �������� ����������:
            // 1. Message_ID
            // 2. MobitelId (����� � ConfigMain ���� ConfigEvent_ID)
            // 3. EventConfig
            command.Init(new object[] { messageId, config.MobitelID, config });
            return command.Execute();
        }

        /// <summary>
        /// ���������� ���������� ��������
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="mobitelId">Mobitel id</param>
        /// <param name="password">Password</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus SaveConfigUnique(int messageId, int mobitelId, string password)
        {
            WriteDebug("A1. ���������� � �� ���������� ��������� � ����������� �����������");

            ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigUnique>();
            // ������� �������� ����������:
            // 1. Message_ID
            // 2. MobitelId (����� � ConfigMain ���� ConfigUnique_ID)
            // 3. ������
            command.Init(new object[] { messageId, mobitelId, password });
            return command.Execute();
        }

        /// <summary>
        /// ���������� ����������������� ����������
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">IdConfig</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus SaveConfigId(int messageId, IdConfig config)
        {
            WriteDebug("A1. ���������� � �� ���������� ��������� � ������������������ �����������");

            ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigId>();
            // ������� �������� ����������:
            // 1. Message_ID
            // 2. MobitelId (����� � ConfigMain ���� InternalMobitelConfig_ID)
            // 3. IdConfig
            command.Init(new object[] { messageId, config.MobitelID, config });
            return command.Execute();
        }

        /// <summary>
        /// ���������� ������ �� ��������� ���
        /// </summary>
        /// <param name="zoneMsgId">zoneMsgId</param>
        /// <param name="zoneResult">zoneResult</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus SaveZoneConfirm(int zoneMsgId, int zoneResult)
        {
            WriteDebug("A1. ���������� � �� ���������� ������ �� ��������� ���");

            ISrvDbCommand command = _commandCache.GetDbCommand<A1SaveConfigZoneConfirm>();
            // ������� �������� ����������:
            // 1. ZoneMsgId
            // 2. ZoneResult
            command.Init(new object[] { zoneMsgId, zoneResult });
            return command.Execute();
        }

        /// <summary>
        /// ���������� �������� �������� GPRS 
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">GprsBaseConfig</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus SaveConfigGprsBase(int messageId, GprsBaseConfig config)
        {
            WriteDebug("A1. ���������� � �� ���������� ��������� � ��������� ����������� GPRS ");

            ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigGprsBase>();
            // ������� �������� ����������:
            // 1. Message_ID
            // 2. MobitelId (����� � ConfigMain ���� ConfigGprsMain_ID)
            // 3. GprsBaseConfig
            command.Init(new object[] { messageId, config.MobitelID, config });
            return command.Execute();
        }

        /// <summary>
        /// ���������� �������� GPRS Email;
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">GprsEmailConfig</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus SaveConfigGprsEmail(int messageId, GprsEmailConfig config)
        {
            WriteDebug("A1. ���������� � �� ���������� ��������� � ����������� GPRS Email");

            ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigGprsEmail>();
            // ������� �������� ����������:
            // 1. Message_ID
            // 2. MobitelId (����� � ConfigMain ���� ConfigGprsEmail_ID)
            // 3. GprsEmailConfig
            command.Init(new object[] { messageId, config.MobitelID, config });
            return command.Execute();
        }

        /// <summary>
        /// ���������� �������� GPRS Provider;
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="config">GprsProviderConfig</param>
        /// <returns>Fetch data status</returns>
        internal ExecutionStatus SaveConfigGprsProvider(int messageId, GprsProviderConfig config)
        {
            WriteDebug("A1. ���������� � �� ���������� ��������� � ����������� GPRS Provider");

            ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigGprsProvider>();
            // ������� �������� ����������:
            // 1. Message_ID
            // 2. MobitelId (����� � ConfigMain ���� ConfigGprsInit_ID)
            // 3. GprsProviderConfig
            command.Init(new object[] { messageId, config.MobitelID, config });
            return command.Execute();
        }

        #endregion


        #region Update ������� Messages

        /// <summary>
        /// ��������� IsNew = 0 ��� ���������� ������ �1 ��������� ���������,
        /// messageCounter �������� ������ ��������� � ���������.
        /// </summary>
        /// <param name="mobitelId">Mobitel id</param>
        /// <param name="commandId">������������� ������� A1</param>
        /// <param name="messageCounter">�essage Counter</param>
        /// <returns>Fetch data status</returns>
        public ExecutionStatus FixRepeatedMessages(int mobitelId, int commandId, int messageCounter)
        {
            WriteDebug(String.Format(
              "A1. ����� �� ��������� ������������� ���������� ������ {0} ��������� {1} (messageCounter = {2})",
              commandId, mobitelId, messageCounter));

            ISrvDbCommand command = _commandCache.GetDbCommand<A1UpdateRepeatedMessages>();
            // ������� �������� ����������:
            // 1. MobitelId
            // 2. CommandId
            // 3. MessageCounter
            command.Init(new object[] { mobitelId, commandId, messageCounter });
            return command.Execute();
        }

        /// <summary>
        /// ��������� ����� �������� ��������� ��������� � ������� messages
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <returns>Fetch data status</returns>
        public ExecutionStatus SetMessageIsDelivered(int messageId)
        {
            WriteDebug(String.Format(
              "A1. ��������� ����� �������� ��������� ��������� � ������� messages (messageId = {0})",
              messageId));

            ISrvDbCommand command = _commandCache.GetDbCommand<A1UpdateDeliveredMessage>();
            // ������� �������� ����������:
            // 1. messageId
            command.Init(new object[] { messageId });
            return command.Execute();
        }

        /// <summary>
        /// ������� ��������� ��� ����������� ������������ ������.
        /// ������ ������������ �� ������ ��������.
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <returns>Fetch data status</returns>
        public ExecutionStatus SetMessageErrorCfg(int messageId)
        {
            WriteDebug(String.Format(
              "A1. ��������� ����� Error Settings ��������� � ������� messages (messageId = {0})",
              messageId));

            ISrvDbCommand command = _commandCache.GetDbCommand<A1UpdateErrorCfgMessage>();
            // ������� �������� ����������:
            // 1. messageId
            command.Init(new object[] { messageId });
            return command.Execute();
        }

        /// <summary>
        /// ������� ��������� ��� ������������� ���������� 
        /// � ����������� ������ (�� ������ ���������)
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <returns>Fetch data status</returns>
        public ExecutionStatus SetErrorMessageIsDelivered(int messageId)
        {
            WriteDebug(String.Format(
              "A1. ��������� ����� Teletrack reported about ERROR ��������� � ������� messages (messageId = {0})",
              messageId));

            ISrvDbCommand command = _commandCache.GetDbCommand<A1UpdateDeliveredErrorMessage>();
            // ������� �������� ����������:
            // 1. messageId
            command.Init(new object[] { messageId });
            return command.Execute();
        }

        #endregion
    }
}