using System;
using System.Collections.Generic;
using RCS.SrvKernel.DataManagement.SrvDBCommand;
using RCS.SrvKernel.DataManagement.SrvDBCommand.Factory;

namespace RCS.SrvKernel.DataManagement.Cache
{
    /// <summary>
    /// ��� ������, ���������� � ��
    /// DataManager �������� � ��������� ����� ���
    /// </summary>
    public class CommandCache : ICommandCache
    {
        /// <summary>
        /// ��� ������
        /// </summary>
        private readonly Dictionary<string, ISrvDbCommand> _cache;

        private readonly IDbCommandFactory _commandFactory;

        private readonly object _syncObject = new Object();

        private CommandCache()
        {
        }

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="commandFactory">������� �������� ������.</param>
        public CommandCache(IDbCommandFactory commandFactory)
        {
            _commandFactory = commandFactory;
            _cache = new Dictionary<string, ISrvDbCommand>();
        }

        /// <summary>
        /// �������� DB �������
        /// </summary>
        /// <typeparam name="T">��� �������</typeparam>
        /// <returns>DbCommand</returns>
        public ISrvDbCommand GetDbCommand<T>() where T : SrvDbCommand
        {
            ISrvDbCommand result;
            string commandTypeName = typeof (T).Name;

            lock (_syncObject)
            {
                if (_cache.ContainsKey(commandTypeName))
                {
                    result = (T) _cache[commandTypeName];
                }
                else
                {
                    result = _commandFactory.GetDbCommand<T>();
                    _cache.Add(commandTypeName, result);
                }
            }
            return result;
        }

        /// <summary>
        /// ������ ����.
        /// </summary>
        public int Size
        {
            get
            {
                lock (_syncObject)
                {
                    return _cache.Count;
                }
            }
        }
    }
}
