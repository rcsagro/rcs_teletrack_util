using RCS.SrvKernel.DataManagement.SrvDBCommand;

namespace RCS.SrvKernel.DataManagement.Cache
{
  /// <summary>
  /// ��� ������.
  /// </summary>
  public interface ICommandCache
  {
    /// <summary>
    /// �������� DB �������
    /// </summary>
    /// <typeparam name="T">��� �������</typeparam>
    /// <returns>DbCommand</returns>
    ISrvDbCommand GetDbCommand<T>() where T : SrvDbCommand;
    /// <summary>
    /// ������ ����.
    /// </summary>
    int Size { get; }
  }
}
