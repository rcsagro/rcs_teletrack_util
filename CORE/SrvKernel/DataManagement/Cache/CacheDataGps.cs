// Project: TDataMngrLib, File: DataGpsCache.cs
// Namespace: RCS.TDataMngrLib.DataManagement.Cache, Class: DataGpsCache
// Path: D:\Development\TDataManager_Head\TDataMngrLib\DataManagement\Cache, Author: guschin
// Code lines: 74, Size of file: 1.69 KB
// Creation date: 7/9/2008 6:23 PM
// Last modified: 7/9/2008 6:31 PM
// Generated with Commenter by abi.exDream.com

#region Using directives
using System.Collections.Generic;
#endregion

namespace RCS.SrvKernel.DataManagement.Cache
{
  /// <summary>
  /// ����� ������ DataGPS
  /// </summary>
  class CacheDataGps
  {
    /// <summary>
    /// ����� ��� ����� ������ ����������.
    /// ����������� ����� ��������
    /// </summary>
    private volatile List<string> bufferDatagGPS;

    private static volatile object syncObject = new object();

    internal CacheDataGps()
    {
      bufferDatagGPS = new List<string>();
    }

    /// <summary>
    /// ��������� ������ � �������
    /// </summary>
    /// <param name="dataGpsRow">Data gps row</param>
    internal void Add(string dataGpsRow)
    {
      lock (syncObject)
      {
        bufferDatagGPS.Add(dataGpsRow);
      }
    }

    /// <summary>
    /// ������ ����� ������
    /// </summary>
    /// <returns></returns>
    internal string[] GetArray()
    {
      lock (syncObject)
      {
        return bufferDatagGPS.ToArray();
      }
    }

    /// <summary>
    /// ������� ������
    /// </summary>
    internal void Clear()
    {
      lock (syncObject)
      {
        bufferDatagGPS.Clear();
      }
    }

    /// <summary>
    /// ���-�� ���������
    /// </summary>
    /// <returns>Int</returns>
    internal int GetCount()
    {
      lock (syncObject)
      {
        return bufferDatagGPS.Count;
      }
    }
  }
}
