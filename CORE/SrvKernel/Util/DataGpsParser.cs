// Project: SrvKernel, File: DataGpsParser.cs
// Namespace: RCS.SrvKernel.Util, Class: DataGpsInfo
// Path: D:\Development\Pop3Pump_Head\SrvKernel\Util, Author: guschin
// Code lines: 331, Size of file: 10,23 KB
// Creation date: 26.02.2009 10:18
// Last modified: 26.03.2009 10:59
// Generated with Commenter by abi.exDream.com

#region Using directives
using RCS.SrvKernel.ErrorHandling;
using System;
using System.Text;
using RCS.SrvKernel.Logging;

#endregion

namespace RCS.SrvKernel.Util
{
    /// <summary>
    /// ����� MainData32 ��������� �� ���������
    /// </summary>
    public class DataGpsInfo
    {
        private string mobitelID;
        /// <summary>
        /// ������������� ��������� MobitelID
        /// </summary>
        public string MobitelID
        {
            get { return mobitelID; }
            set { mobitelID = value; }
        }

        private string logID;
        /// <summary>
        /// ������������� ������
        /// </summary>
        public string LogID
        {
            get { return logID; }
            set { logID = value; }
        }

        private string unixTime;
        /// <summary>
        /// ����� � ������� UnixTime
        /// </summary>
        public string UnixTime
        {
            get { return unixTime; }
            set { unixTime = value; }
        }

        private string latitude;
        /// <summary>
        /// ������
        /// </summary>
        public string Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }

        private string longitude;
        /// <summary>
        /// �������
        /// </summary>
        public string Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }

        private string altitude;
        /// <summary>
        /// ������
        /// </summary>
        public string Altitude
        {
            get { return altitude; }
            set { altitude = value; }
        }

        private string speed;
        /// <summary>
        /// ��������
        /// </summary>
        public string Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        private string direction;
        /// <summary>
        /// ����������
        /// </summary>
        public string Direction
        {
            get { return direction; }
            set { direction = value; }
        }

        private string valid;
        /// <summary>
        /// ����������
        /// </summary>
        public string Valid
        {
            get { return valid; }
            set { valid = value; }
        }

        private string sensor1_8;
        /// <summary>
        /// ������� � 1 �� 8
        /// </summary>
        public string Sensor1_8
        {
            get { return sensor1_8; }
            set { sensor1_8 = value; }
        }

        private string events;
        /// <summary>
        /// �������
        /// </summary>
        public string Events
        {
            get { return events; }
            set { events = value; }
        }

        /// <summary>
        /// �������� ��� ���� WhatIs = 1023
        /// </summary>
        public const string DEFAULT_WHATIS_VALUE = "1023";

        public string WhatIs
        {
            get { return DEFAULT_WHATIS_VALUE; }
        }

        /// <summary>
        /// ����������� �������
        /// </summary>
        public const string DELIMITER = ",";

        /// <summary>
        /// �������� ������ ������� ��� ����������� � ������� 
        /// INSERT INTO DataGps
        /// </summary>
        /// <returns>������ ������� ��� ����������� � ������� INSERT INTO DataGps</returns>
        public string GetAssembledRow()
        {
            return String.Concat(
              MobitelID, DELIMITER,
              LogID, DELIMITER,
              UnixTime, DELIMITER,
              Latitude, DELIMITER,
              Longitude, DELIMITER,
              Speed, DELIMITER,
              Direction, DELIMITER,
              Altitude, DELIMITER,
              Valid, DELIMITER,
              Sensor1_8, DELIMITER,
              Events, DELIMITER,
              WhatIs);
        }

        /// <summary>
        /// �������� �������� ������ ��� ������ � ���
        /// </summary>
        /// <returns>�������� ������ ��� ������ � ���</returns>
        public string GetDebugRow()
        {
            return String.Concat(
              "MobitelID=", MobitelID, "; ",
              "LogID=", LogID, "; ",
              "Valid=", Valid, "; ",
              "Latitude=", Latitude, "; ",
              "Longitude=", Longitude, "; ",
              "Speed=", Speed, "; ",
              "UnixTime=", UnixTime, "; ",
              "Sensor1_8=", Sensor1_8, "; ",
              "Direction=", Direction, "; ",
              "Altitude=", Altitude, "; ",
              "Events=", Events, "; ",
              "WhatIs=", WhatIs);
        }
    } // struct DebugPacketInfo



    /// <summary>
    /// ������ ������ ��� ������� � DataGps
    /// </summary>
    public static class DataGpsParser
    {
        /// <summary>
        /// ���-�� ���� � DataGPS ������
        /// </summary>
        private const int DATA_GPS_BYTE_COUNT = 32;
        /// <summary>
        /// ������ �������� = NULL
        /// </summary>
        public const string NULL_VALUE = "NULL";


        /// <summary>
        /// ������������ ����� ��� INSERT� � ������� DataGPSBuffer.
        /// </summary>
        /// <param name="mobitelID">Mobitel ID</param>
        /// <param name="packet">Packet ������� ���� ������� (32 �����)</param>
        /// <returns>String</returns>
        public static DataGpsInfo GetDataGpsRow(int mobitelID, params byte[] packet)
        {
            if (packet.Length != DATA_GPS_BYTE_COUNT)
                throw new ArgumentOutOfRangeException("byte[] PacketToParse",
                 String.Format(AppError.PARSING_UNEXPECTED_LENGTH_ARRAY,
                 "GetDataGpsRow", "DataGpsParser", Convert.ToString(DATA_GPS_BYTE_COUNT),
                 Convert.ToString(packet.Length)));

            DataGpsInfo result = new DataGpsInfo();
            TimeSpan epochTicks = new TimeSpan(new DateTime(1970, 1, 1).Ticks);
            TimeSpan unixTicks = new TimeSpan(DateTime.UtcNow.Ticks) - epochTicks;
            long futureUnixTime = (long)unixTicks.TotalSeconds + 86400;
            bool valiDate = true;

            // MobitelID
            result.MobitelID = Convert.ToString(mobitelID);
            // LogID
            result.LogID = Convert.ToString(BitConverter.ToInt32(packet, 0));
            // UnixTime
            result.UnixTime = Convert.ToString(BitConverter.ToInt32(packet, 4));
            if (Convert.ToInt64(result.UnixTime) > futureUnixTime)
                valiDate = false;
            // Latitude
            int latitude = ParserServices.CheckForMinValue(BitConverter.ToInt32(packet, 8) * 10);

            result.Latitude = Convert.ToString(latitude);
            // Longitude
            int longitude = ParserServices.CheckForMinValue(BitConverter.ToInt32(packet, 12) * 10);
            result.Longitude = Convert.ToString(longitude);
            // Speed
            int speed = Convert.ToInt32(packet[16]);
            result.Speed = Convert.ToString(speed);
            // Direction
            result.Direction = Convert.ToString(packet[17]);
            // Altitude
            result.Altitude = Convert.ToString(packet[18]);

            if (ParserServices.TestValidity(latitude, longitude, speed))
            {
                result.Valid = Convert.ToString(Convert.ToByte((packet[19] & 0x08) != 0));
            }
            else
            {
                result.Valid = "0";
            }

            string oldValid = result.Valid;

            if (!valiDate)
            {
                result.Valid = "0";
                string Ex = "Error UnixTime of packet. ���� Valide=" + oldValid + " ����� Valide=" + result.Valid + ". LogId=" + result.LogID + ", MobitelId=" + result.MobitelID +
                            " UnixTime=" + result.UnixTime + " ������ ���������� UnixTime=" + futureUnixTime;
                Logger.Write(Ex);
            }

            // Sensor1 .. Sensor8
            StringBuilder sensors = new StringBuilder();
            for (int sensor = 20; sensor < 28; sensor++)
            {
                if (sensors.Length > 0)
                    sensors.Append(DataGpsInfo.DELIMITER);

                sensors.Append(Convert.ToString(packet[sensor]));
            } // for (sensor)
            result.Sensor1_8 = sensors.ToString();

            // Events
            result.Events = Convert.ToString(
              (int)(packet[30] << 16 | packet[29] << 8 | packet[28]));

            return result;
        } // GetDataGpsRows(mobitelID, packet, logID)
    }
}
