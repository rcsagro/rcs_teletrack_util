﻿using System.Collections.Generic;
using RCS.SrvKernel.ErrorHandling;
using System;
using RCS.SrvKernel.Logging;

namespace RCS.SrvKernel.Util
{
    public class DataGpsInfo64
    {

        public DataGpsInfo64()
        {
            DataGps32 = new DataGpsInfo();
        }
        
        public DataGpsInfo DataGps32 { get; set; }

        /// <summary>
        /// Изменение скорости по GPS за последнюю секунду
        /// </summary>
        public string Acceleration { get; set; }

        /// <summary>
        /// Количество спутников
        /// </summary>
        public string Satellites { get; set; }

        /// <summary>
        /// Уровень GSM сигнала
        /// </summary>
        public string RssiGsm { get; set; }

        /// <summary>
        /// Код набора датчиков
        /// </summary>
        public string SensorsSet { get; set; }

        /// <summary>
        /// Данные дифференциальной системы GPS - высота,широта,долгота
        /// </summary>
        public string DGPS { get; set; }

        /// <summary>
        /// Значения датчиков
        /// </summary>
        public string Sensors { get; set; }

        /// <summary>
        /// Напряжение питания текущего источника/1.5 от 0 до 38,2
        /// </summary>
        public string Voltage { get; set; }

        /// <summary>
        /// Собирает строку готовую для подстановки в команду 
        /// INSERT INTO DataGps
        /// </summary>
        /// <returns>Строка готовая для подстановки в команду INSERT INTO DataGps</returns>
        public string GetAssembledRow()
        {
            return String.Concat(ParserServices.MBIN_64_TT,
             DataGps32.MobitelID, DataGpsInfo.DELIMITER,
              DataGps32.LogID, DataGpsInfo.DELIMITER,
              DataGps32.Latitude, DataGpsInfo.DELIMITER,
              DataGps32.Longitude, DataGpsInfo.DELIMITER,
              DataGps32.Direction, DataGpsInfo.DELIMITER,
              Acceleration, DataGpsInfo.DELIMITER,
              DataGps32.UnixTime, DataGpsInfo.DELIMITER,
              DataGps32.Speed, DataGpsInfo.DELIMITER,
              DataGps32.Valid, DataGpsInfo.DELIMITER,
              Satellites, DataGpsInfo.DELIMITER,
              RssiGsm, DataGpsInfo.DELIMITER,
              DataGps32.Events, DataGpsInfo.DELIMITER,
              SensorsSet, DataGpsInfo.DELIMITER,
              Voltage, DataGpsInfo.DELIMITER,
              DGPS, DataGpsInfo.DELIMITER,
              Sensors);
        }

        /// <summary>
        /// Собирает дебажную строку для записи в лог
        /// </summary>
        /// <returns>Дебажная строка для записи в лог</returns>
        public string GetDebugRow()
        {
            return String.Concat(
              "MobitelID=", DataGps32.MobitelID, "; ",
              "LogID=", DataGps32.LogID, "; ",
              "Valid=", DataGps32.Valid, "; ",
              "Latitude=", DataGps32.Latitude, "; ",
              "Longitude=", DataGps32.Longitude, "; ",
              "Speed=", DataGps32.Speed, "; ",
              "UnixTime=", DataGps32.UnixTime, "; ",
              "Direction=", DataGps32.Direction, "; ",
              "Events=", DataGps32.Events, "; ",
              "Acceleration=", Acceleration, "; ",
              "Satellites=", Satellites, "; ",
              "RssiGsm=", RssiGsm, "; ",
              "SensorsSet=", SensorsSet, "; ",
              "Voltage=", Voltage, "; ",
              "Dgps=", DGPS, "; ",
              "Sensors=", Sensors);
        }
    } // DataGpsInfo64
    
    /// <summary>
    /// Персер данных для вставки в DataGps
    /// </summary>
    public static class DataGpsParser64
    {
        /// <summary>
        /// Формирование сроки для INSERTа в таблицу DataGPSBuffer.
        /// </summary>
        /// <param name="mobitelID">Mobitel ID</param>
        /// <param name="packet">Packet который надо парсить (64 байта)</param>
        /// <returns>String</returns>
        public static DataGpsInfo64 GetDataGpsRow(int mobitelID, params byte[] packet)
        {
            if (packet.Length != ParserServices.DATA_GPS_BYTE_COUNT_64)
                throw new ArgumentOutOfRangeException("byte[] PacketToParse",
                 String.Format(AppError.PARSING_UNEXPECTED_LENGTH_ARRAY,
                 "GetDataGpsRow", "DataGpsParser", Convert.ToString(ParserServices.DATA_GPS_BYTE_COUNT_64),
                 Convert.ToString(packet.Length)));

            DataGpsInfo64 result = new DataGpsInfo64();
            TimeSpan epochTicks = new TimeSpan(new DateTime(1970, 1, 1).Ticks);
            TimeSpan unixTicks = new TimeSpan(DateTime.UtcNow.Ticks) - epochTicks;
            long futureUnixTime = (long)unixTicks.TotalSeconds + 86400;
            bool valiDate = true;

            // MobitelID
            result.DataGps32.MobitelID = Convert.ToString(mobitelID);
            // LogID - 4b
            Int32 LogID = BitConverter.ToInt32(packet, 0);
            
            result.DataGps32.LogID = Convert.ToString(LogID); // вот сдесь есть LogId
            Logger.Write("result.DataGps32.LogID=" + result.DataGps32.LogID + ". result.DataGps32.MobitelID=" + result.DataGps32.MobitelID);
            
            // UnixTime - 4b
            result.DataGps32.UnixTime = Convert.ToString(BitConverter.ToInt32(packet, 4));
            if (Convert.ToInt64(result.DataGps32.UnixTime) > futureUnixTime)
                valiDate = false;

            // Latitude - 4b
            int latitude = ParserServices.CheckForMinMaxValueLati64(BitConverter.ToInt32(packet, 8));
            result.DataGps32.Latitude = Convert.ToString(latitude);
            // Longitude - 4b
            int longitude = ParserServices.CheckForMinMaxValueLongi64(BitConverter.ToInt32(packet, 12));
            result.DataGps32.Longitude = Convert.ToString(longitude);
            // Speed - 2b
            Int16 speed = BitConverter.ToInt16(packet, 16);
            result.DataGps32.Speed = Convert.ToString(speed);
            // Direction - 1b
            result.DataGps32.Direction = Convert.ToString(packet[18]);
            // Аcceleration - 1b
            result.Acceleration = Convert.ToString(packet[19]);

            // Valid Дополнительная проверка
            if (ParserServices.TestValidity64(latitude, longitude, speed))
            {
                result.DataGps32.Valid = Convert.ToString(Convert.ToByte((packet[20] & 0x80) != 0));
            }
            else
            {
                result.DataGps32.Valid = "0";
            }

            string oldValid = result.DataGps32.Valid;

            if (!valiDate)
            {
                result.DataGps32.Valid = "0";
                string Ex = "Error UnixTime of packet. Была Valide=" + oldValid + " стала Valide=" + 
                            result.DataGps32.Valid + ". LogId=" + result.DataGps32.LogID + 
                            ", MobitelId=" + result.DataGps32.MobitelID + " UnixTime=" + 
                            result.DataGps32.UnixTime + " больше граничного UnixTime=" + 
                            futureUnixTime;
                Logger.Write(Ex);
            }

            //satellites  - 1b - старшие 4 бита
            result.Satellites = Convert.ToString(packet[20] & 0x0F);
            //RssiGsm - 1b
            result.RssiGsm = Convert.ToString(packet[21]);
            //Events - 4b
            result.DataGps32.Events = Convert.ToString(BitConverter.ToInt32(packet, 22));
            //SensorsSet - 1b
            result.SensorsSet = Convert.ToString(packet[26]);
            //Voltage - 1b
            result.Voltage = Convert.ToString(packet[27]);
            //DGPS  4 + 4 + 2
            byte[] bufferPacket = new byte[10];

            //latitude = ParserServices.CheckForMinValue(BitConverter.ToInt32(packet, 28));
            //longitude = ParserServices.CheckForMinValue(BitConverter.ToInt32(packet, 32));
            //short altitude = BitConverter.ToInt16(packet, 36);
            for (int i = 0; i <= bufferPacket.GetUpperBound(0); i++)
            {
                bufferPacket[i] = packet[37 - i];
            }
            
            try {
            	short altitude = BitConverter.ToInt16(bufferPacket, 0);
            	altitude = ParserServices.CheckForMinMaxValueAlti64(altitude);
            	longitude = ParserServices.CheckForMinMaxValueLongi64(BitConverter.ToInt32(bufferPacket, 2));
            	latitude = ParserServices.CheckForMinMaxValueLati64(BitConverter.ToInt32(bufferPacket, 6));
            	if(latitude != 0 && longitude != 0 && altitude != 0)
            		result.DGPS = string.Format("'{0}/{1}/{2}'", latitude, longitude, altitude);
            	else
            		result.DGPS = string.Format("'0'");
            } catch {
				result.DGPS = string.Format("'0'");
			}
            
            //Sensors - 25b
            // первые нули не передаем для экономии места под DataGps 
            bool startAdd = false;
            //packet[38] = 1;
            var sensors = new List<byte>();
            for (int i = 0; i < 25; i++)
            {
                if (startAdd)
                {
                    sensors.Add(packet[38 + i]);
                }
                else if (packet[38 + i] != 0)
                {
                    sensors.Add(packet[38 + i]);
                    startAdd = true;
                }
            }
            
            result.Sensors = string.Format("'{0}'", Convert.ToBase64String(sensors.ToArray()));
            return result;
        } // GetDataGpsRows(mobitelID, packet, logID)
    }
}
