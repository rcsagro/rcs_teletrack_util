﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RCS.SrvKernel.Util
{
    public static class ParserServices
    {
        /// <summary>
        /// Кол-во байт в DataGPS64 пакете
        /// </summary>
        public const int DATA_GPS_BYTE_COUNT_64 = 64;

        public const string MBIN_64_TT = "%%NB";
        /// <summary>
        /// Максимальное абсолютное значение долготы = 108000000 (180)
        /// </summary>
        private const int MAX_LONGITUDE = 108000000;
        /// <summary>
        /// Минимальное абсолютное значение долготы = 108000000 (-180)
        /// </summary>
        private const int MIN_LONGITUDE = -108000000;
        /// <summary>
        /// Максимальное абсолютное значение широты = 54000000 (90)
        /// </summary>
        private const int MAX_LATITUDE = 54000000;
        /// <summary>
        /// Минимальное абсолютное значение широты = 54000000 (-90)
        /// </summary>
        private const int MIN_LATITUDE = -54000000;
        /// <summary>
        /// Максимальное абсолютное значение долготы = 1800000000 (180)
        /// </summary>
        private const int MAX_LONGITUDE64 = 1800000000;
        /// <summary>
        /// Минимальное абсолютное значение долготы = -1800000000 (-180)
        /// </summary>
        private const int MIN_LONGITUDE64 = -1800000000;
        /// <summary>
        /// Максимальное абсолютное значение широты = 900000000 (90)
        /// </summary>
        private const int MAX_LATITUDE64 = 900000000;
        /// <summary>
        /// Минимальное абсолютное значение широты = -900000000 (-90)
        /// </summary>
        private const int MIN_LATITUDE64 = -900000000;
         /// <summary>
        /// Максимальное абсолютное значение высоты = (3276.8м)
        /// </summary>
        private const int MAX_ALTITUDE64 = 32766;
        /// <summary>
        /// Минимальное абсолютное значение высоты = (-3276.8м)
        /// </summary>
        private const int MIN_ALTITUDE64 = -32767;
        /// <summary>
        /// Максимальное значение скорости = 110 миль в час (примерно 200 км/ч)
        /// </summary>
        private const int MAX_SPEED = 110;
        /// <summary>
        /// Максимальное значение скорости = 110 миль в час (примерно 200 км/ч)
        /// </summary>
        private const int MAX_SPEED_64 = 2000;
        /// <summary>
        /// Минимальное значение скорости = 0
        /// </summary>
        private const int MIN_SPEED = 0;

        /// <summary>
        /// Math.Abs(longitude) для Int32 вылетает на ошибку при 0x80000000
        /// </summary>
        /// <param name="latitude"></param>
        /// <returns></returns>
        public static int CheckForMinValue(int valInt)
        {
            if (valInt != (valInt & Int32.MaxValue) && (valInt & Int32.MaxValue) == 0)
                valInt = 0;
            return valInt;
        }
        
        public static int CheckForMinMaxValueLongi64(int valInt)
        {
        	if (valInt >= (ParserServices.MAX_LONGITUDE64))
        	{
        	    return 0;
        	}
        	
        	if (valInt <= (ParserServices.MIN_LONGITUDE64))
        	{
        	    return 0;
        	}
        	
        	return valInt;
        }
        
        public static int CheckForMinMaxValueLati64(int valInt)
        {
        	if (valInt >= (ParserServices.MAX_LATITUDE64))
        	{
        	    return 0;
        	}
        	
        	if (valInt <= (ParserServices.MIN_LATITUDE64))
        	{
        	    return 0;
        	}
        	
        	return valInt;
        }
        
        public static short CheckForMinMaxValueAlti64(short valInt)
        {
        	if (valInt > (ParserServices.MAX_ALTITUDE64))
        	{
        	    return 0;
        	}
        	
        	if (valInt < (ParserServices.MIN_ALTITUDE64))
        	{
        	    return 0;
        	}
        	
        	return valInt;
        }

        public static bool TestValidity(int latitude, int longitude, int speed)
        {
            if ((Math.Abs(longitude) > ParserServices.MAX_LONGITUDE) ||
                (Math.Abs(latitude) > ParserServices.MAX_LATITUDE) ||
                (speed > ParserServices.MAX_SPEED) || (speed < ParserServices.MIN_SPEED))
                return false;
            else
                return true;
        }

        public static bool TestValidity64(int latitude, int longitude, int speed)
        {
            if ((Math.Abs(longitude) > ParserServices.MAX_LONGITUDE64) ||
                (Math.Abs(latitude) > ParserServices.MAX_LATITUDE64) ||
                (speed > ParserServices.MAX_SPEED_64) || 
                (speed < ParserServices.MIN_SPEED))
                return false;
            else
                return true;
        }
    }
}
