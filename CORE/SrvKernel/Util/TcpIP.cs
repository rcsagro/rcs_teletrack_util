// Project: SrvKernel, File: TcpIP.cs
// Namespace: RCS.SrvKernel.Util, Class: TcpIP
// Path: D:\Development\TDataManager_Head\SrvKernel\Util, Author: guschin
// Code lines: 124, Size of file: 3,71 KB
// Creation date: 15.09.2008 16:24
// Last modified: 09.10.2008 18:08
// Generated with Commenter by abi.exDream.com

#region Using directives
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
#endregion

namespace RCS.SrvKernel.Util
{
  /// <summary>
  /// Dns info
  /// </summary>
  struct DnsInfo
  {
    /// <summary>
    /// IP �����
    /// </summary>
    public string IpAddress;
    /// <summary>
    /// ����� ����������
    /// </summary>
    public DateTime ResolvingTime;
  } // struct DnsInfo

  public static class TcpIP
  {
    /// <summary>
    /// ����� �������� �������� �� �������, � �������� (10 ���)
    /// </summary>
    private const int DNS_RESOLVING_TIME_SAVING = 10 * 60;

    /// <summary>
    /// ������� ��� ����� - ��� IP �����
    /// </summary>
    private static Dictionary<string, DnsInfo> dictDnsResolving =
      new Dictionary<string, DnsInfo>();


    /// <summary>
    /// �������� ����������� �������� �����
    /// </summary>
    /// <param name="port"></param>
    /// <returns>bool</returns>
    public static bool ValidateTcpPort(int port)
    {
      return ((port >= 0) && (port <= 0xffff));
    }

    /// <summary>
    /// ����������� �������� subject IP �������, ���� ���
    /// �� �������� �� �������� �� ����� IP �����. 
    /// ��� ������������� ����� ������ ������������ ��� ����������.
    /// ���� ��������� ���������� �������, ����� ������� �� ����
    /// ���� usingCache = True
    /// </summary>
    /// <param name="subject">������� �������</param>
    /// <param name="usingCache">������������ ��������� ��� ��� ��������� �� �������� �������� �� ���</param>
    /// <param name="ipAdress">Ip ����� ���� ������� ����� ��������</param>
    /// <returns>True - ���� ������� �������� IP �����</returns>
    public static bool Resolve(string subject, bool usingCache, out string ipAddress)
    {
      bool result = false;
      ipAddress = "";
      try
      {
        // �������� ���������� �� ��� �����
        if (usingCache && dictDnsResolving.ContainsKey(subject))
        {
          // �������� ����� ������� ������
          // ���� ����� ����� ���� ������ ���������
          if (DateTime.Now > dictDnsResolving[subject].ResolvingTime +
            new TimeSpan(0, 0, DNS_RESOLVING_TIME_SAVING))
          {
            result = InnerResolve(subject, out ipAddress);
          } // if (dtNow)
          else
          {
            ipAddress = dictDnsResolving[subject].IpAddress;
            result = true;
          } // else
        } // if (dictDnsResolving.ContainsKey)
        else
        {
          result = InnerResolve(subject, out ipAddress);
        } // else

      } // try
      catch { } // catch
      return result;
    } // Resolve(subject, usingCache, ipAddress)


    /// <summary>
    /// Inner resolve
    /// </summary>
    /// <param name="subject">Subject</param>
    /// <param name="ipAddress">Ip address</param>
    /// <returns>Bool</returns>
    private static bool InnerResolve(string subject, out string ipAddress)
    {
      bool result = false;
      ipAddress = "";
      IPAddress tmpIpAddress;
      if (IPAddress.TryParse(subject, out tmpIpAddress))
      {
        ipAddress = subject;
        result = true;
      } // if (IPAddress.TryParse)
      else
      {
        IPHostEntry host = Dns.GetHostEntry(subject);
        if (host.AddressList.Length > 0)
        {
          if (dictDnsResolving.ContainsKey(subject))
            dictDnsResolving.Remove(subject);

          ipAddress = host.AddressList[0].ToString();
          result = true;
        } // if (host.AddressList.Length)
      } // else

      if (result)
      {
        DnsInfo dnsInfo = new DnsInfo();
        dnsInfo.IpAddress = ipAddress;
        dnsInfo.ResolvingTime = DateTime.Now;
        if (!dictDnsResolving.ContainsKey(subject))
          dictDnsResolving.Add(subject, dnsInfo);
      } // if (result)

      return result;
    } // InnerResolve(subject, ipAddress)
  }
}
