// Project: SrvKernel, File: FileLogger.cs
// Namespace: RCS.SrvKernel.Logging, Class: FileLogger
// Path: D:\Development\Pop3Pump_Head\SrvKernel\Logging, Author: guschin
// Code lines: 159, Size of file: 3.83 KB
// Creation date: 8/18/2008 9:33 AM
// Last modified: 9/8/2008 4:32 PM
// Generated with Commenter by abi.exDream.com

#region Using directives
using System;
using System.IO;
using System.Text;
#endregion


// �����������
namespace RCS.SrvKernel.Logging
{
  /// <summary>
  /// ����������� ������� � �����
  /// </summary>
  /// <returns>Logger</returns>
  internal class FileLogger : BaseLogger
  {
    private const string LOG_FORMAT = "{0} {1}";

    /// <summary>
    /// ������ ������� ���� � ����
    /// 0 - ���������
    /// 1 - �������(��������)
    /// 2 - ����
    /// </summary>
    private const string FULL_PATH_FORMAT = @"{0}\{1}{2}.log";

    /// <summary>
    /// ���-���� StreamWriter
    /// </summary>
    private volatile StreamWriter logWriter;

    private object syncObject = new Object();


    private string path = "";
    private string fullPath
    {
      get
      {
        return String.Format(FULL_PATH_FORMAT,
          path,
          SrvSettings.LogPrefix,
          DateTime.Now.ToString(DATE_FORMAT));
      }
    }

    /// <summary>
    /// Create file logger
    /// </summary>
    public FileLogger()
    {
      RefreshPath();
    }

    /// <summary>
    /// Refresh path
    /// </summary>
    internal void RefreshPath()
    {
      path = SrvSettings.LogPath;

      if (String.IsNullOrEmpty(path))
      {
        path = AppDomain.CurrentDomain.BaseDirectory;
      }
      else
      {
        if (!Directory.Exists(path))
          Directory.CreateDirectory(path);
      }
    } // RefreshPath()

    /// <summary>
    /// ������ ��������� � ����
    /// </summary>
    /// <param name="message"></param>
    public override void Write(string message)
    {
      lock (syncObject)
      {
        try
        {
          logWriter = new StreamWriter(fullPath, true, Encoding.Default);
          logWriter.AutoFlush = true;
          try
          {
            logWriter.WriteLine(
              String.Format(LOG_FORMAT, DateTime.Now, message));
            logWriter.Flush();
          } // try
          finally
          {
            logWriter.Close();
          } // finally
        } // try
        catch (Exception ex) 
        {
          System.Windows.Forms.MessageBox.Show(
            ex.Message, System.Reflection.Assembly.GetExecutingAssembly().FullName,
            System.Windows.Forms.MessageBoxButtons.OK,
            System.Windows.Forms.MessageBoxIcon.Error,
            System.Windows.Forms.MessageBoxDefaultButton.Button1,
            System.Windows.Forms.MessageBoxOptions.ServiceNotification);
        } // catch
      } // lock
    } // Write(Message)

    /// <summary>
    /// ������ ���� � ���-����������� {0}, � ������������ � ����������� �������� �� ����� {1} ����.
    /// </summary>
    private const string DEL_LOG_MESSAGE =
      "������ ���� � ���-����������� {0}, � ������������ � �����������: ������� �� ����� {1} ����.";

    /// <summary>
    /// ������� ���������� ����������
    /// </summary>
    public override void CleanOldInformation()
    {
      DateTime dtNow = DateTime.Now;
      // ��� ������, �����, �����
      DateTime boundaryDate =
        new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 0, 0, 0) -
        new TimeSpan(SrvSettings.LogDataHoldDays, 0, 0, 0);

      // ���� ����� � ������
      const string search_template = "*.log";
      string[] logFiles = Directory.GetFiles(path, search_template);

      // ���� ���� ������� - ������
      foreach (string file in logFiles)
      {
        if ((File.GetCreationTime(file) < boundaryDate) ||
            (File.GetLastWriteTime(file) < boundaryDate))
        {
          try
          {
            File.Delete(file);
            Write(String.Format(DEL_LOG_MESSAGE, file, SrvSettings.LogDataHoldDays));
          }
          catch (Exception Ex)
          {
            Write(Ex.Message + "\r\n" + Ex.StackTrace);
          }
        }
      }

    }

    /// <summary>
    /// ���� � ����
    /// </summary>
    /// <returns>���� � ����</returns>
    public override string GetLogPath()
    {
      return fullPath;
    } // GetLogPath()

  }// END CLASS DEFINITION FileLogger
}
