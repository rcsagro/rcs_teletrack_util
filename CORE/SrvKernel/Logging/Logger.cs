// Project: SrvLib, File: Logger.cs
// Namespace: RCS.SrvLib.Logging, Class: Logger
// Path: D:\Development\SrvService\SrvLib\Logging, Author: guschin
// Code lines: 76, Size of file: 1.90 KB
// Creation date: 5/13/2008 12:07 PM
// Last modified: 5/21/2008 10:57 AM
// Generated with Commenter by abi.exDream.com

#region Using directives
using System;
#endregion

namespace RCS.SrvKernel.Logging
{
  /// <summary>
  /// ������������ ��� ������ � �����
  /// </summary>
  public static class Logger
  {
    private static object syncObject = new object();
    private static ILogger loggerInst;
    private static ILogger LoggerInst
    {
      get
      {
        if (loggerInst == null)
        {
          lock (syncObject)
          {
            if (loggerInst == null)
              loggerInst = CreateFileLogger();
          } // lock
        } // if (loggerInst)
        return loggerInst;
      }
    }

    static Logger() { }

    /// <summary>
    /// Create logger
    /// </summary>
    /// <returns>ILogger</returns>
    private static ILogger CreateFileLogger()
    {
      return new FileLogger();
    }

    /// <summary>
    /// ������ � ���
    /// </summary>
    /// <param name="message">Message</param>
    public static void Write(string message)
    {
      LoggerInst.Write(message);
    }

    /// <summary>
    /// ������ � ���
    /// </summary>
    /// <param name="symbol">���������� ������</param>
    /// <param name="message">Message</param>
    public static void Write(string symbol, string message)
    {
      const string formatedText = "{0} {1}";
      Write(String.Format(formatedText, symbol, message));
    }

    /// <summary>
    /// ������ � ��� ������ ��� ��������� ������ Debug
    /// </summary>
    /// <param name="symbol">���������� ������</param>
    /// <param name="message">Message</param>
    public static void WriteDebug(string symbol, string message)
    {
      if (SrvSettings.DebugMode)
      {
        const string formatedText = "{0} {1}";
        Write(String.Format(formatedText, symbol, message));
      } // if (SrvSettings.DebugMode)
    }

    /// <summary>
    /// ������ � ��� ������ ��� ��������� ������ Debug
    /// </summary>
    /// <param name="message">Message</param>
    public static void WriteDebug(string message)
    {
      if (SrvSettings.DebugMode)
        LoggerInst.Write(message);
    }

    /// <summary>
    /// ������� ���������� ����������
    /// </summary>
    public static void CleanOldInformation()
    {
      LoggerInst.CleanOldInformation();
    }

    /// <summary>
    /// ���� � ����
    /// </summary>
    /// <returns>���� � ����</returns>
    public static string GetLogPath()
    {
      return LoggerInst.GetLogPath();
    }
  }
}
