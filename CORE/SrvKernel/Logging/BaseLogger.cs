// Project: SrvKernel, File: BaseLogger.cs
// Namespace: RCS.SrvKernel.Logging, Class: BaseLogger
// Path: D:\Development\Pop3Pump_Head\SrvKernel\Logging, Author: guschin
// Code lines: 47, Size of file: 1.25 KB
// Creation date: 8/18/2008 9:33 AM
// Last modified: 9/4/2008 2:39 PM
// Generated with Commenter by abi.exDream.com

#region Using directives
using RCS.SrvKernel.ErrorHandling;
using System;
#endregion

// Логирование
namespace RCS.SrvKernel.Logging
{
  /// <summary>
  /// Абстрактный базовый клас регистраторов событий
  /// После завершения работы вызвать необходимо вызвать Dispose();
  /// </summary>
  abstract class BaseLogger : ILogger
  {
    protected const string DATE_FORMAT = "yyMMdd";

    /// <summary>
    /// Регистрация события
    /// </summary>
    /// <param name="Event">Строка для записи в лог</param>
    public virtual void Write(string message)
    {
      throw new NotImplementedException(AppError.LOGGER_ABSTRACT_USING);
    }

    /// <summary>
    /// Очистка устаревшей информации
    /// </summary>
    public virtual void CleanOldInformation()
    {
      throw new NotImplementedException(AppError.LOGGER_ABSTRACT_USING);
    }

    /// <summary>
    /// Путь к логу
    /// </summary>
    /// <returns>Путь к логу</returns>
    public virtual string GetLogPath()
    {
      throw new NotImplementedException(AppError.LOGGER_ABSTRACT_USING);
    }

  }
}
