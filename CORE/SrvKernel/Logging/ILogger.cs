
namespace RCS.SrvKernel.Logging
{
  /// <summary>
  /// ��������� ��� ������ � ������������� ������� 
  /// </summary>
  interface ILogger
  {
    /// <summary>
    /// ����������� �������
    /// </summary>
    /// <param name="Event">������ ��� ������ � ���</param>
    void Write(string message);
    /// <summary>
    /// ������� ���������� ����������
    /// </summary>
    void CleanOldInformation();
    /// <summary>
    /// ���� � ����
    /// </summary>
    /// <returns>���� � ����</returns>
    string GetLogPath();
  }
}
