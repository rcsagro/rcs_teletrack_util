#region Using directives
using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using RCS.SrvKernel.ErrorHandling;
#endregion

namespace RCS.SrvKernel
{
  /// <summary>
  /// ��������� ����������� � ��
  /// </summary>
  public struct DbConnectionSettings
  {
      public string TypeDataBase;
      public string Host;
      public string Trusted;
      public int Port;
      public string Database;
      public string User;
      public string Password;
      public string AdditionalOptions;
      public int CommandTimeout;
  }

  public struct Settings
  {
    public DbConnectionSettings dbConnection;
    /// <summary>
    /// ���� �������
    /// </summary>
    public int SrvPort;
    /// <summary>
    /// ��� ������
    /// </summary>
    public string ServiceName;
    /// <summary>
    /// ���-�� ���� �������� ������ ����
    /// </summary>
    public int LogDataHoldDays;
    /// <summary>
    /// ������� � ������������ ����
    /// </summary>
    public string LogPrefix;
    /// <summary>
    /// ���� � �����
    /// </summary>
    public string LogPath;
    /// <summary>
    /// ����� �����������
    /// </summary>
    public bool Debug;

    /// <summary>
    /// ���-�� ���� �������� ������ ���� �� ��������� = 10
    /// </summary>
    private const int DEFAULT_LOGDATA_HOLD_DAYS = 10;

    /// <summary>
    /// �������� ��������
    /// </summary>
    public void Validate()
    {
      if (LogDataHoldDays <= 0)
      {
        LogDataHoldDays = DEFAULT_LOGDATA_HOLD_DAYS;
      }
      if (ServiceName == null)
      {
        ServiceName = "";
      }
    }
  }

  /// <summary>
  /// ������ � ������ ��������.
  /// ���������������� ������� ������ LoadSettings() �����
  /// ������� �������� ��������, ������� ���������� � �����
  /// ������ ������ ��������. � ���������� �������� �����������
  /// �������� �����, ������� ��������� ������ �������������
  /// ��������� ����� ����������� �������.
  /// </summary>
  public static class SrvSettings
  {
    public const string SETTINGS_FILE = "settings.xml";

    /// <summary>
    /// ������ ���� � ����� � �����������
    /// </summary>
    public static readonly string FullPath = Path.Combine(
      Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
      SETTINGS_FILE);

    private static Settings settings;

    /// <summary>
    /// �������� ��������
    /// </summary>
    internal static void LoadSettings()
    {
      if (!File.Exists(FullPath))
      {
        throw new ApplicationException(String.Format(
          AppError.SETTINGS_FILE_NOT_FOUND, FullPath));
      }
      try
      {
        using (StreamReader xmlFile = new StreamReader(FullPath))
        {
          settings = (Settings)new XmlSerializer(typeof(Settings)).Deserialize(xmlFile);
        }
      }
      catch (Exception ex)
      {
        throw new ApplicationException(String.Format( AppError.SETTINGS_DESERIALIZE, FullPath, ex.Message), ex);
      }
      settings.Validate();
    }

    private static void SaveSettings()
    {
      XmlSerializer xmlSer = new XmlSerializer(settings.GetType());
      StreamWriter xmlFile = new StreamWriter(FullPath, false);
      xmlSer.Serialize(xmlFile, settings);
      xmlFile.Close();
    }

    /// <summary>
    /// ��� ������
    /// </summary>
    public static string ServiceName
    {
      get { return settings.ServiceName; }
    }

      // ���������� ��� ���� ������
      public static string TypeDataBase
      {
          get { return settings.dbConnection.TypeDataBase; }
      }

      // ���������� ��� ����������� � �������� SQL
      public static string TypeConnect
      {
          get { return settings.dbConnection.Trusted; }
      }

    /// <summary>
    /// ������ ��� ������
    /// </summary>
    public static string DataBaseHost
    {
      get { return settings.dbConnection.Host; }
    }

    /// <summary>
    /// ���� ������� ��� ������
    /// </summary>
    public static int DataBasePort
    {
      get { return settings.dbConnection.Port; }
    }

    /// <summary>
    /// ��� ���� ������
    /// </summary>
    public static string DataBaseName
    {
      get { return settings.dbConnection.Database; } // get
    }

    /// <summary>
    /// user ���� ������
    /// </summary>
    public static string DataBaseUser
    {
      get { return settings.dbConnection.User; }
    }

    /// <summary>
    /// password ��� user-� ���� ������
    /// </summary>
    public static string DataBasePassword
    {
      get { return settings.dbConnection.Password; }
    }

    /// <summary>
    /// �������������� ��������� ����������� � ��
    /// </summary>
    public static string DataBaseAdditionalOptions
    {
      get { return settings.dbConnection.AdditionalOptions; }
    }

    /// <summary>
    /// command timeout
    /// </summary>
    public static int DataBaseCommandTimeout
    {
      get { return settings.dbConnection.CommandTimeout; }
    }

    /// <summary>
    /// ���� �������
    /// </summary>
    public static int ServerPort
    {
      get { return settings.SrvPort; }
    }

    /// <summary>
    /// ������� �� ����� Debug
    /// </summary>
    public static bool DebugMode
    {
      get { return settings.Debug; }
    }

    /// <summary>
    /// ���-�� ���� �������� ������ ����
    /// </summary>
    public static int LogDataHoldDays
    {
      get { return settings.LogDataHoldDays; }
    }

    /// <summary>
    /// ������� ����
    /// </summary>
    public static string LogPrefix
    {
      get { return settings.LogPrefix; }
    }

    /// <summary>
    /// ������ ���� � ����(��� �������� �����)
    /// </summary>
    public static string LogPath
    {
      get { return settings.LogPath; }
    }

    /// <summary>
    /// ����������, ��� ������� ������� ���������.
    /// ����� ��������������� ��� �������������� ��������
    /// ������ ������������ ���������� � ������ ���� ��������
    /// ����� ����� ���������� (����� ����) � ������������� �� ����� 
    /// ���������� ��������� �� ��������� �� ���������� ���� ���������.
    /// </summary>
    public static volatile bool StopProcess = false;
  }
}
