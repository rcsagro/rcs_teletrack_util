using RCS.Protocol.A1.Entity;

namespace RCS.SrvKernel.A1Handler
{
  /// <summary>
  /// ��������� ���������� ���������
  /// </summary>
  public class OutgoingMsg
  {
    /// <summary>
    /// MessageId �� ������� Messages
    /// </summary>
    public int MessageId
    {
      get { return messageId; }
      set { messageId = value; }
    }
    private int messageId;

    /// <summary>
    /// ��������� ��� ��������
    /// </summary>
    public CommonDescription Message
    {
      get { return message; }
      set { message = value; }
    }
    private CommonDescription message;
  }
}
