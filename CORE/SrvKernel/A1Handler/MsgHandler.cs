#region Using directives
using System;
using System.Collections.Generic;
using RCS.Protocol.A1;
using RCS.Protocol.A1.Entity;
using RCS.SrvKernel.DataManagement;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand.Sending;
#endregion

namespace RCS.SrvKernel.A1Handler
{
  /// <summary>
  /// ���������� ��������� ��������� A1
  /// </summary>
  public class MsgHandler
  {
    protected BaseDataManager dataManager;

    /// <summary>
    /// �����������
    /// </summary>
    /// <param name="dataManager">BaseDataManager</param>
    public MsgHandler(BaseDataManager dataManager)
    {
      this.dataManager = dataManager;
    }

    /// <summary>
    /// ������� ��� ��������� ��������� {0}
    /// </summary>
    private const string SERVICE_MSG = "A1. ������� ��� ��������� ��������� {0}. {1}";

    /// <summary>
    /// ��������� ��������� ���������
    /// </summary>
    /// <param name="message">Message byte[]</param>
    /// <param name="answerCode">��� ������ �������</param>
    /// <param name="sentMessageId">messageId(������� messages) ������������� ���������</param>
    public void HandleIncomingMessage(byte[] message, int answerCode, int sentMessageId)
    {
      try
      {
        if (answerCode == 1)
        {
          // ������ ��������� ������� ����������
          dataManager.SetErrorMessageIsDelivered(sentMessageId);
          return;
        }

        // ������ ������� �� ��������� �������
        if (dataManager.SetMessageIsDelivered(sentMessageId) != ExecutionStatus.OK)
          return;

        // ���� ������ ������� ����� - ���������� ��������� ������� � �������� ���������
        if ((message == null) || (message.Length == 0))
          return;

        CommonDescription command = Decoder.DecodeMessage(message);

        //�������� ���������� �� ��������
        int mobitelID = -1;
        if (!dataManager.IsMobitelExists(command.ShortID, out mobitelID))
          throw new ApplicationException(String.Format(
            AppError.A1_TT_NOT_FOUND, command.ShortID));

        InnerHandling(command, mobitelID);
      }
      catch (Exception Ex)
      {
        ErrorHandler.Handle(Ex);
      }
    }

    /// <summary>
    /// ���������� ��������� ��������� ���������
    /// </summary>
    /// <param name="command">CommonDescription</param>
    /// <param name="mobitelID">mobitelID</param>
    protected void InnerHandling(CommonDescription command, int mobitelID)
    {
      Logger.Write(String.Format(SERVICE_MSG, command.CommandID, command.GetDebugInfo()));

      // Ping �� ������������
      if (command.CommandID == CommandDescriptor.Ping)
        return;

      command.MobitelID = mobitelID;

      // ��������� �������� ���������
      int messageId;
      if (dataManager.SaveInMessages(command.Message, (int)command.CommandID,
        mobitelID, command.MessageID, out messageId) != ExecutionStatus.OK)
      {
        return;
      }

      switch (command.CommandID)
      {
        case CommandDescriptor.SMSConfigConfirm:
        case CommandDescriptor.SMSConfigAnswer:
          SmsAddrConfig configSms = (SmsAddrConfig)command;
          dataManager.SaveConfigSms(messageId, configSms);
          break;

        case CommandDescriptor.PhoneConfigConfirm:
        case CommandDescriptor.PhoneConfigAnswer:
          PhoneNumberConfig configPhone = (PhoneNumberConfig)command;
          dataManager.SaveConfigPhoneNumbers(messageId, configPhone);
          break;

        case CommandDescriptor.EventConfigConfirm:
        case CommandDescriptor.EventConfigAnswer:
          EventConfig configEvent = (EventConfig)command;
          dataManager.SaveConfigEvents(messageId, configEvent);
          break;

        case CommandDescriptor.UniqueConfigConfirm:
        case CommandDescriptor.UniqueConfigAnswer:
          UniqueConfig configUnique = (UniqueConfig)command;
          dataManager.SaveConfigUnique(messageId, mobitelID, configUnique.Password);
          break;

        case CommandDescriptor.IdConfigConfirm:
        case CommandDescriptor.IdConfigAnswer:
          IdConfig configId = (IdConfig)command;
          dataManager.SaveConfigId(messageId, configId);
          break;

        case CommandDescriptor.ZoneConfigConfirm:
          ZoneConfigConfirm configZone = (ZoneConfigConfirm)command;
          dataManager.SaveZoneConfirm(
            configZone.ZoneMsgID, configZone.Result);
          break;

        case CommandDescriptor.GprsBaseConfigConfirm:
        case CommandDescriptor.GprsBaseConfigAnswer:
          GprsBaseConfig configGprsBase = (GprsBaseConfig)command;
          dataManager.SaveConfigGprsBase(messageId, configGprsBase);
          break;

        case CommandDescriptor.GprsEmailConfigConfirm:
        case CommandDescriptor.GprsEmailConfigAnswer:
          GprsEmailConfig configGprsEmail = (GprsEmailConfig)command;
          dataManager.SaveConfigGprsEmail(messageId, configGprsEmail);
          break;

        case CommandDescriptor.GprsProviderConfigConfirm:
        case CommandDescriptor.GprsProviderConfigAnswer:
          GprsProviderConfig configGprsProvider = (GprsProviderConfig)command;
          dataManager.SaveConfigGprsProvider(messageId, configGprsProvider);
          break;

        case CommandDescriptor.DataGpsAnswer:
        case CommandDescriptor.DataGpsAuto:
          DataGpsAnswer dataGpsAnswer = (DataGpsAnswer)command;
          SaveDataGps(dataGpsAnswer);
          break;

        case CommandDescriptor.GprsFtpConfigConfirm:
        case CommandDescriptor.GprsFtpConfigAnswer:
          throw new NotImplementedException(String.Format(
            AppError.A1_SAVE_NOT_IMPLEMENTED, "�������� GPRS FTP"));

        case CommandDescriptor.GprsSocketConfigConfirm:
        case CommandDescriptor.GprsSocketConfigAnswer:
          throw new NotImplementedException(String.Format(
            AppError.A1_SAVE_NOT_IMPLEMENTED, "�������� GPRS Socket"));

        default:
          throw new ApplicationException(String.Format(
            AppError.A1_DECODE_CMD_WRONG, command.CommandID, command.ShortID));
      } // switch
    }

    /// <summary>
    /// ���������� ������ �� ������ ������ DataGps ������ ���������� ��������
    /// </summary>
    /// <param name="messageId">Message id</param>
    /// <param name="mobitelID">Mobitel ID</param>
    /// <param name="dataGpsAnswer">DataGpsAnswer</param>
    protected virtual void SaveDataGps(DataGpsAnswer dataGpsAnswer)
    {
      throw new NotImplementedException(String.Format(
        AppError.A1_SAVE_NOT_IMPLEMENTED, "������ DataGps"));
    }


    /// <summary>
    /// ������������ ������ ��������� ��������� A1
    /// �������� ������ ��������� ��������� ��� ������ ������ ������ ���������
    /// </summary>
    /// <param name="mobitelId">Mobitel_ID ������� mobitels</param>
    /// <param name="shortId">�������� ID ���������</param> 
    /// <returns>List OutgoingMsg</returns>
    public List<OutgoingMsg> GetOutgoingA1Messages(int mobitelId, string shortId)
    {
      List<OutgoingMsg> result = new List<OutgoingMsg>();

      foreach (NewMessage message in dataManager.GetNewMessages(mobitelId, shortId))
      {
        try
        {
          switch (message.CommandID)
          {
            // SMS
            case (int)CommandDescriptor.SMSConfigSet:
              OutgoingMsg smsConfigSet = FillSmsConfigSet(message);
              if (smsConfigSet != null)
                result.Add(smsConfigSet);
              break;
            case (int)CommandDescriptor.SMSConfigQuery:
              result.Add(FillSmsConfigQuery(message));
              break;

            // ���������� ������
            case (int)CommandDescriptor.PhoneConfigSet:
              OutgoingMsg phoneConfigSet = FillPhoneConfigSet(message);
              if (phoneConfigSet != null)
                result.Add(phoneConfigSet);
              break;
            case (int)CommandDescriptor.PhoneConfigQuery:
              result.Add(FillPhoneConfigQuery(message));
              break;

            // �������
            case (int)CommandDescriptor.EventConfigSet:
              OutgoingMsg eventConfigSet = FillEventConfigSet(message);
              if (eventConfigSet != null)
                result.Add(eventConfigSet);
              break;
            case (int)CommandDescriptor.EventConfigQuery:
              result.Add(FillEventConfigQuery(message));
              break;

            // ���������� ���������
            case (int)CommandDescriptor.UniqueConfigSet:
              OutgoingMsg uniqueConfigSet = FillUniqueConfigSet(message);
              if (uniqueConfigSet != null)
                result.Add(uniqueConfigSet);
              break;
            case (int)CommandDescriptor.UniqueConfigQuery:
              result.Add(FillUniqueConfigQuery(message));
              break;

            // ����������������� ���������
            case (int)CommandDescriptor.IdConfigSet:
              OutgoingMsg idConfigSet = FillIdConfigSet(message);
              if (idConfigSet != null)
                result.Add(idConfigSet);
              break;
            case (int)CommandDescriptor.IdConfigQuery:
              result.Add(FillIdConfigQuery(message));
              break;

            // ��������� ��������
            case (int)CommandDescriptor.MessageToDriver:
              OutgoingMsg messageToDriver = FillMessageToDriver(message);
              if (messageToDriver != null)
                result.Add(messageToDriver);
              break;

            // ����������� ����
            case (int)CommandDescriptor.ZoneConfigSet:
              OutgoingMsg zoneConfigSet = FillZoneConfigSet(message);
              if (zoneConfigSet != null)
                result.Add(zoneConfigSet);
              break;

            // ������ GPS ������
            case (int)CommandDescriptor.DataGpsQuery:
              OutgoingMsg dataGpsQuery = FillDataGpsQuery(message);
              if (dataGpsQuery != null)
                result.Add(dataGpsQuery);
              break;

            // �������� ��������� GPRS
            case (int)CommandDescriptor.GprsBaseConfigSet:
              OutgoingMsg gprsBaseConfigSet = FillGprsBaseConfigSet(message);
              if (gprsBaseConfigSet != null)
                result.Add(gprsBaseConfigSet);
              break;
            case (int)CommandDescriptor.GprsBaseConfigQuery:
              result.Add(FillGprsBaseConfigQuery(message));
              break;

            // GPRS Email
            case (int)CommandDescriptor.GprsEmailConfigSet:
              OutgoingMsg gprsEmailConfigSet = FillGprsEmailConfigSet(message);
              if (gprsEmailConfigSet != null)
                result.Add(gprsEmailConfigSet);
              break;
            case (int)CommandDescriptor.GprsEmailConfigQuery:
              result.Add(FillGprsEmailConfigQuery(message));
              break;

            // GPRS Provider
            case (int)CommandDescriptor.GprsProviderConfigSet:
              OutgoingMsg gprsProviderConfigSet = FillGprsProviderConfigSet(message);
              if (gprsProviderConfigSet != null)
                result.Add(gprsProviderConfigSet);
              break;
            case (int)CommandDescriptor.GprsProviderConfigQuery:
              result.Add(FillGprsProviderConfigQuery(message));
              break;

            // GPRS FTP
            case (int)CommandDescriptor.GprsFtpConfigSet:
              throw new NotImplementedException(String.Format(
                AppError.A1_LOAD_NOT_IMPLEMENTED, "��������� ���������� GPRS FTP"));
            case (int)CommandDescriptor.GprsFtpConfigQuery:
              throw new NotImplementedException(String.Format(
                AppError.A1_LOAD_NOT_IMPLEMENTED, "������ ���������� GPRS FTP"));

            // GPRS Socket
            case (int)CommandDescriptor.GprsSocketConfigSet:
              throw new NotImplementedException(String.Format(
                AppError.A1_LOAD_NOT_IMPLEMENTED, "��������� ���������� GPRS Socket"));
            case (int)CommandDescriptor.GprsSocketConfigQuery:
              throw new NotImplementedException(String.Format(
                AppError.A1_LOAD_NOT_IMPLEMENTED, "������ ���������� GPRS Socket"));

            default:
              throw new ApplicationException(String.Format(
                AppError.A1_ENCODE_CMD_WRONG, message.CommandID, message.DevIdShort));
          }
        }
        catch (Exception Ex)
        {
          ErrorHandler.Handle(Ex);
          MarkMessageAsWrong(message.MessageId);
        }
      }
      return result;
    }

    private void MarkMessageAsWrong(int messageId)
    {
      dataManager.SetMessageErrorCfg(messageId);
    }

    /// <summary>
    /// �������� ��������� ���������� ���������
    /// </summary>
    /// <param name="MessageId">Message id</param>
    /// <param name="message">Message CommonDescription</param>
    /// <returns>Outgoing message</returns>
    private OutgoingMsg CreateOutgoingMsg(int �essageId, CommonDescription message)
    {
      OutgoingMsg result = new OutgoingMsg();
      result.MessageId = �essageId;
      result.Message = message;
      return result;
    }

    /// <summary>
    /// �������� ��������� ��� �������� ������
    /// </summary>
    /// <param name="mobitelID">mobitelID</param> 
    /// <param name="messageCounter">Message counter</param>
    /// <param name="devIdShort">Dev id short</param>
    /// <returns>Simple query</returns>
    private SimpleQuery CreateSimpleQuery(int mobitelID, int messageCounter, string devIdShort)
    {
      SimpleQuery result = new SimpleQuery();
      result.MessageID = (ushort)messageCounter;
      result.ShortID = devIdShort;
      result.MobitelID = mobitelID;
      return result;
    }

    /// <summary>
    /// ���������� ����� ���������� ��� ���� ������ ���������
    /// </summary>
    /// <param name="command">CommonDescription</param>
    /// <param name="message">NewMessage</param>
    private void FillBaseCommandInfo(CommonDescription command, NewMessage message)
    {
      command.MessageID = (ushort)message.MessageCounter;
      command.ShortID = message.DevIdShort;
      command.MobitelID = message.MobitelID;
    }

    /// <summary>
    /// ������������ ��������� ��������� �������� SMS
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillSmsConfigSet(NewMessage message)
    {
      OutgoingMsg result = null;
      SmsAddrConfig �onfig = new SmsAddrConfig();
      FillBaseCommandInfo(�onfig, message);

      if (dataManager.GetSmsAddrConfig(message.MessageId, �onfig) ==
        ExecutionStatus.OK)
      {
        Encoder.EncodeSmsAddrConfigSet(�onfig);
        result = CreateOutgoingMsg(message.MessageId, �onfig);
      }
      else
      {
        MarkMessageAsWrong(message.MessageId);
      }
      return result;
    }

    /// <summary>
    /// ������������ ��������� ������� �������� SMS
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillSmsConfigQuery(NewMessage message)
    {
      SimpleQuery query = CreateSimpleQuery(
        message.MobitelID, message.MessageCounter, message.DevIdShort);
      Encoder.EncodeSmsAddrConfigQuery(query);
      return CreateOutgoingMsg(message.MessageId, query);
    }

    /// <summary>
    /// ������������ ��������� ������� GPS ������
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillDataGpsQuery(NewMessage message)
    {
      OutgoingMsg result = null;
      DataGpsQuery query = new DataGpsQuery();
      FillBaseCommandInfo(query, message);

      if (dataManager.GetDataGpsQuery(message.MessageId, query) ==
        ExecutionStatus.OK)
      {
        Encoder.EncodeDataGpsQuery(query);
        result = CreateOutgoingMsg(message.MessageId, query);
      }
      else
      {
        MarkMessageAsWrong(message.MessageId);
      }
      return result;
    }

    /// <summary>
    /// ������������ ��������� ��������� �������� ���������� �������
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillPhoneConfigSet(NewMessage message)
    {
      OutgoingMsg result = null;
      PhoneNumberConfig �onfig = new PhoneNumberConfig();
      FillBaseCommandInfo(�onfig, message);

      if (dataManager.GetPhoneNumberConfig(message.MessageId, �onfig) ==
        ExecutionStatus.OK)
      {
        Encoder.EncodePhoneNumberConfigSet(�onfig);
        result = CreateOutgoingMsg(message.MessageId, �onfig);
      }
      else
      {
        MarkMessageAsWrong(message.MessageId);
      }
      return result;
    }

    /// <summary>
    /// ������������ ��������� ������� �������� ���������� �������
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillPhoneConfigQuery(NewMessage message)
    {
      SimpleQuery query = CreateSimpleQuery(
        message.MobitelID, message.MessageCounter, message.DevIdShort);
      Encoder.EncodePhoneNumberConfigQuery(query);
      return CreateOutgoingMsg(message.MessageId, query);
    }

    /// <summary>
    /// ������������ ��������� ��������� �������� �������
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillEventConfigSet(NewMessage message)
    {
      OutgoingMsg result = null;
      EventConfig �onfig = new EventConfig();
      FillBaseCommandInfo(�onfig, message);

      if (dataManager.GetEventConfig(message.MessageId, �onfig) ==
        ExecutionStatus.OK)
      {
        Encoder.EncodeEventConfigSet(�onfig);
        result = CreateOutgoingMsg(message.MessageId, �onfig);
      }
      else
      {
        MarkMessageAsWrong(message.MessageId);
      }
      return result;
    }

    /// <summary>
    /// ������������ ��������� ������� �������� �������
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillEventConfigQuery(NewMessage message)
    {
      SimpleQuery query = CreateSimpleQuery(
        message.MobitelID, message.MessageCounter, message.DevIdShort);
      Encoder.EncodeEventConfigQuery(query);
      return CreateOutgoingMsg(message.MessageId, query);
    }

    /// <summary>
    /// ������������ ��������� ��������� �������� ���������� ����������
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillUniqueConfigSet(NewMessage message)
    {
      OutgoingMsg result = null;
      UniqueConfig �onfig = new UniqueConfig();
      FillBaseCommandInfo(�onfig, message);

      if (dataManager.GetUniqueConfig(message.MessageId, �onfig) ==
        ExecutionStatus.OK)
      {
        Encoder.EncodeUniqueConfigSet(�onfig);
        result = CreateOutgoingMsg(message.MessageId, �onfig);
      }
      else
      {
        MarkMessageAsWrong(message.MessageId);
      }
      return result;
    }

    /// <summary>
    /// ������������ ��������� ������� �������� ���������� ����������
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillUniqueConfigQuery(NewMessage message)
    {
      SimpleQuery query = CreateSimpleQuery(
        message.MobitelID, message.MessageCounter, message.DevIdShort);
      Encoder.EncodeUniqueConfigQuery(query);
      return CreateOutgoingMsg(message.MessageId, query);
    }

    /// <summary>
    /// ������������ ��������� ��������� �������� ����������������� ����������
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillIdConfigSet(NewMessage message)
    {
      OutgoingMsg result = null;
      IdConfig �onfig = new IdConfig();
      FillBaseCommandInfo(�onfig, message);

      if (dataManager.GetIdConfig(message.MessageId, �onfig) ==
        ExecutionStatus.OK)
      {
        Encoder.EncodeIdConfigSet(�onfig);
        result = CreateOutgoingMsg(message.MessageId, �onfig);
      }
      else
      {
        MarkMessageAsWrong(message.MessageId);
      }
      return result;
    }

    /// <summary>
    /// ������������ ��������� ������� �������� ����������������� ����������
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillIdConfigQuery(NewMessage message)
    {
      SimpleQuery query = CreateSimpleQuery(
        message.MobitelID, message.MessageCounter, message.DevIdShort);
      Encoder.EncodeIdConfigQuery(query);
      return CreateOutgoingMsg(message.MessageId, query);
    }

    /// <summary>
    /// ������������ ��������� ��������
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillMessageToDriver(NewMessage message)
    {
      OutgoingMsg result = null;
      MessageToDriver �onfig = new MessageToDriver();
      FillBaseCommandInfo(�onfig, message);

      if (dataManager.GetMessageToDriver(message.MessageId, �onfig) ==
        ExecutionStatus.OK)
      {
        Encoder.EncodeMessageToDriver(�onfig);
        result = CreateOutgoingMsg(message.MessageId, �onfig);
      }
      else
      {
        MarkMessageAsWrong(message.MessageId);
      }
      return result;
    }

    /// <summary>
    /// ������������ ��������� ��������� �������� ����������� ���
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillZoneConfigSet(NewMessage message)
    {
      OutgoingMsg result = null;
      ZoneConfig �onfig = new ZoneConfig();
      FillBaseCommandInfo(�onfig, message);

      if (dataManager.GetZoneConfig(message.MessageId, �onfig) ==
        ExecutionStatus.OK)
      {
        Encoder.EncodeZoneConfigSet(�onfig);
        result = CreateOutgoingMsg(message.MessageId, �onfig);
      }
      else
      {
        MarkMessageAsWrong(message.MessageId);
      }
      return result;
    }

    /// <summary>
    /// ������������ ��������� ��������� �������� �������� GPRS
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillGprsBaseConfigSet(NewMessage message)
    {
      OutgoingMsg result = null;
      GprsBaseConfig �onfig = new GprsBaseConfig();
      FillBaseCommandInfo(�onfig, message);

      if (dataManager.GetGprsBaseConfig(message.MessageId, �onfig) ==
        ExecutionStatus.OK)
      {
        Encoder.EncodeGprsBaseConfigSet(�onfig);
        result = CreateOutgoingMsg(message.MessageId, �onfig);
      }
      else
      {
        MarkMessageAsWrong(message.MessageId);
      }
      return result;
    }

    /// <summary>
    /// ������������ ��������� ������� �������� �������� GPRS
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillGprsBaseConfigQuery(NewMessage message)
    {
      SimpleQuery query = CreateSimpleQuery(
        message.MobitelID, message.MessageCounter, message.DevIdShort);
      Encoder.EncodeGprsBaseConfigQuery(query);
      return CreateOutgoingMsg(message.MessageId, query);
    }

    /// <summary>
    /// ������������ ��������� ��������� �������� GPRS Email
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillGprsEmailConfigSet(NewMessage message)
    {
      OutgoingMsg result = null;
      GprsEmailConfig �onfig = new GprsEmailConfig();
      FillBaseCommandInfo(�onfig, message);

      if (dataManager.GetGprsEmailConfig(message.MessageId, �onfig) ==
        ExecutionStatus.OK)
      {
        Encoder.EncodeGprsEmailConfigSet(�onfig);
        result = CreateOutgoingMsg(message.MessageId, �onfig);
      }
      else
      {
        MarkMessageAsWrong(message.MessageId);
      }
      return result;
    }

    /// <summary>
    /// ������������ ��������� ������� �������� GPRS Email
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillGprsEmailConfigQuery(NewMessage message)
    {
      SimpleQuery query = CreateSimpleQuery(
        message.MobitelID, message.MessageCounter, message.DevIdShort);
      Encoder.EncodeGprsEmailConfigQuery(query);
      return CreateOutgoingMsg(message.MessageId, query);
    }

    /// <summary>
    /// ������������ ��������� ��������� �������� GPRS Provider
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillGprsProviderConfigSet(NewMessage message)
    {
      OutgoingMsg result = null;
      GprsProviderConfig �onfig = new GprsProviderConfig();
      FillBaseCommandInfo(�onfig, message);

      if (dataManager.GetGprsProviderConfig(message.MessageId, �onfig) ==
        ExecutionStatus.OK)
      {
        Encoder.EncodeGprsProviderConfigSet(�onfig);
        result = CreateOutgoingMsg(message.MessageId, �onfig);
      }
      else
      {
        MarkMessageAsWrong(message.MessageId);
      }
      return result;
    }

    /// <summary>
    /// ������������ ��������� ������� �������� GPRS Provider
    /// </summary>
    /// <param name="message">NewMessage</param>
    /// <returns>OutgoingMsg</returns>
    private OutgoingMsg FillGprsProviderConfigQuery(NewMessage message)
    {
      SimpleQuery query = CreateSimpleQuery(
        message.MobitelID, message.MessageCounter, message.DevIdShort);
      Encoder.EncodeGprsProviderConfigQuery(query);
      return CreateOutgoingMsg(message.MessageId, query);
    }
  }
}
