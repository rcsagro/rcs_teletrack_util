
namespace RCS.SrvKernel.A1Handler
{
  /// <summary>
  /// ���������� � ����� ��������� ��� ���������
  /// </summary>
  public class NewMessage
  {
    /// <summary>
    /// Message_ID
    /// </summary>
    private int messageId;
    /// <summary>
    /// Message_ID
    /// </summary>
    public int MessageId
    {
      get { return messageId; }
      set { messageId = value; }
    }

    /// <summary>
    /// ������������� ��������� � ��
    /// </summary>
    private int mobitelID;
    /// <summary>
    /// ������������� ��������� � ��
    /// </summary>
    public int MobitelID
    {
      get { return mobitelID; }
      set { mobitelID = value; }
    }

    /// <summary>
    /// ������������� ��������� ������������ ����� ����������
    /// </summary>
    private string devIdShort;
    /// <summary>
    /// ������������� ��������� ������������ ����� ����������
    /// </summary>
    public string DevIdShort
    {
      get { return devIdShort; }
      set { devIdShort = value; }
    }

    /// <summary>
    /// ������������� �������
    /// </summary>
    private int commandID;
    /// <summary>
    /// ������������� �������
    /// </summary>
    public int CommandID
    {
      get { return commandID; }
      set { commandID = value; }
    }

    /// <summary>
    /// ����� ��������� (�������� ��� ����� ���������)
    /// </summary>
    private int messageCounter;
    /// <summary>
    /// ����� ��������� (�������� ��� ����� ���������)
    /// </summary>
    public int MessageCounter
    {
      get { return messageCounter; }
      set { messageCounter = value; }
    }
  }
}
