#region Using directives
using System;
using System.Timers;
using RCS.Sockets.Connection;
using RCS.SrvKernel.A1Handler;
using RCS.SrvKernel.Communication;
using RCS.SrvKernel.DataManagement;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand;
using RCS.SrvKernel.SrvCommand.Sending;
using RCS.SrvKernel.SrvSystem;
using RCS.SrvKernel.SrvSystem.Messages;
#endregion

namespace RCS.SrvKernel
{
    /// <summary>
    /// ��������� ������
    /// </summary>
    public enum ServiceState
    {
        /// <summary>
        /// �������������� ��������� (����� �������)
        /// </summary>
        Undefined,

        /// <summary>
        /// ������ �����������
        /// </summary>
        Start,

        /// <summary>
        /// ������ ���������������
        /// </summary>
        Stop,

        /// <summary>
        /// ������� �����
        /// </summary>
        Operate,

        /// <summary>
        /// �������� ��������� ���������� ��
        /// ����� ��������� ������������ ��� ������
        /// </summary>
        ConnectionWaiting
    }

    /// <summary>
    ///  ������� ���������� ����������� ���� �������
    /// </summary>
    public class MainController : MsgObject, IMainController
    {
        #region Private Fields

        /// <summary>
        /// ������� ��� �������� ���������
        /// </summary>
        private SendDelegate sendMethod;

        /// <summary>
        /// ��������� ��� ������� c �����������.
        /// ������� � communicator-�� ���� � ����������� ������
        /// </summary>
        protected ICommunicator communicator;

        /// <summary>
        /// �������� ������
        /// </summary>
        protected volatile BaseDataManager baseDataManager;

        /// <summary>
        /// ������� ��������� �������
        /// </summary>
        protected volatile ServiceState state;

        /// <summary>
        /// ������� ��������� ������
        /// </summary>
        public ServiceState State
        {
            get { return state; }
        }

        /// <summary>
        /// ������ ��� ��������� ������� ���-����������
        /// </summary>
        private System.Timers.Timer timerDeleteOldLogData;

        /// <summary>
        /// �������� �������� ������ ����� 12 �����.
        /// </summary>
        private const int DELETE_OLD_LOG_DATA_INTERVAL = 12*60*60*1000;

        private static volatile object syncObject = new object();

        #endregion

        /// <summary>
        /// ���������� ��������� ��������� A1
        /// </summary>
        protected MsgHandler A1MsgHandler;

        /// <summary>
        /// �������� �����������, ���������� ��� ���������
        /// </summary>
        protected IMsgDispatcher owner;

        #region Constructor

        public MainController(IMsgDispatcher owner)
        {
            state = ServiceState.Undefined;

            if (owner == null)
            {
                Exception ex = new ArgumentNullException(String.Format(
                    AppError.ARGUMENT_NULL, "constructor", GetType().Name, "IMsgDispatcher owner"));
                ErrorHandler.Handle(ex);
                throw ex;
            }

            this.owner = owner;
            ErrorHandler.OverflowDBExceptionsEvent +=
                new CommonEventDelegate(OnOverflowDbExceptions);

            try
            {
                communicator = CreateCommunicator();
                baseDataManager = CreateDataManager();
                A1MsgHandler = CreateA1MsgHandler();
                sendMethod = new SendDelegate(communicator.Send);

                timerDeleteOldLogData =
                    new System.Timers.Timer(DELETE_OLD_LOG_DATA_INTERVAL);
                timerDeleteOldLogData.AutoReset = false;
                timerDeleteOldLogData.Elapsed +=
                    new ElapsedEventHandler(OnTimerDeleteOldLogDataElapsed);
            }
            catch (Exception Ex)
            {
                HandleException(Ex);
                throw Ex;
            }
        }

        /// <summary>
        /// �������� ����������� ������ �1
        /// </summary>
        /// <returns>MsgHandler</returns>
        protected virtual MsgHandler CreateA1MsgHandler()
        {
            return new MsgHandler(baseDataManager);
        }

        /// <summary>
        /// �������� ��������� ������.
        /// ������������� � ��������.
        /// </summary>
        /// <returns>BaseDataManager</returns>
        protected virtual BaseDataManager CreateDataManager()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "CreateDataManager", "MainController"));
        }

        /// <summary>
        /// �������� ���������������� ����.
        /// ������������� � ��������.
        /// </summary>
        /// <returns>BaseCommunicator</returns>
        protected virtual BaseCommunicator CreateCommunicator()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "CreateCommunicator", "MainController"));
        }

        #endregion

        #region ���������

        /// <summary>
        /// ��������� ���������
        /// </summary>
        /// <param name="message">MessageSpec</param>
        protected override void HandleMessage(MessageSpec message)
        {
            switch (message.msgType)
            {
                case MessageType.CMD_START:
                    Start();
                    break;
                case MessageType.CMD_STOP:
                    Stop();
                    break;
                default:
                    throw new ApplicationException(String.Format(
                        "��������� ��������� {0} ������� ������������ �� �������������.", message.msgType));
            }
        }

        /// <summary>
        /// �������� ��������� ������ � ������������ ������
        /// </summary>
        protected void SendStartedMessage()
        {
            MessageSpec message = new MessageSpec();
            message.msgType = MessageType.STARTED;
            owner.Post(message);
        }

        /// <summary>
        /// �������� ��������� ������ �� ���������
        /// </summary>
        protected void SendStoppedMessage()
        {
            MessageSpec message = new MessageSpec();
            message.msgType = MessageType.STOPPED;
            owner.Post(message);
        }

        #endregion

        #region �����/���� �������

        /// <summary>
        /// ���� ������� 
        /// </summary>
        protected virtual void Start()
        {
            state = ServiceState.Start;

            try
            {
                InnerStarting();
            }
            catch (Exception Ex)
            {
                state = ServiceState.Undefined;
                HandleException(Ex);
                throw Ex;
            }

            // ������ ������ ���-����������
            DeleteOldLogData();
            timerDeleteOldLogData.Start();
            state = ServiceState.Operate;
            // �������� ��������� �� �������� ������
            SendStartedMessage();
        }

        /// <summary>
        /// �������� ������������� � �������� ������
        /// </summary>
        protected virtual void InnerStarting()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "InnerStarting", "MainController"));
        }

        /// <summary>
        /// ��������� �������. 
        /// ����� ������ ����� ������ ���������� �����
        /// �������� ��������� ����� ������
        /// </summary>
        protected virtual void Stop()
        {
            state = ServiceState.Stop;

            try
            {
                if (communicator != null)
                {
                    communicator.Stop();
                }
                InnerStopping();
                StopMessageThread();

                // ����������� �������
                ((IDisposable) communicator).Dispose();
                communicator = null;
                baseDataManager = null;
            }
            catch (Exception Ex)
            {
                HandleException(Ex);
            }
            finally
            {
                SendStoppedMessage();
            }
        }

        /// <summary>
        /// �������� ��� ��������� ������(� �������� ���������)
        /// </summary>
        protected virtual void InnerStopping()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "InnerStopping", "MainController"));
        }

        #endregion

        #region IMainController Members

        /// <summary>
        /// ������ � ���
        /// </summary>
        /// <param name="message">string</param>
        public void WriteToLog(string message)
        {
            Logger.Write(message);
        }

        public void HandleException(Exception Ex)
        {
            lock (syncObject)
            {
                ErrorHandler.Handle(Ex);
            }
        }

        /// <summary>
        /// Is debug mode active
        /// </summary>
        /// <returns>Bool</returns>
        public bool IsDebugModeActive()
        {
            return SrvSettings.DebugMode;
        }

        #endregion

        /// <summary>
        /// �������� ������ ���������� �� ����/��
        /// </summary>
        private void DeleteOldLogData()
        {
            Logger.CleanOldInformation();
        }

        /// <summary>
        /// ���������� ������� ������� ����� �������
        /// </summary>
        /// <param name="parameter">���������� � ������ object</param>
        protected void OnHandleCommand(object parameter)
        {
            InnerHandleCommand(parameter);
        }

        /// <summary>
        /// ��������� �������
        /// </summary>
        /// <param name="parameter">Parameter object</param>
        protected virtual void InnerHandleCommand(object parameter)
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "InnerHandleCommand", "MainController"));
        }

        /// <summary>
        /// Get command
        /// </summary>
        /// <param name="commandID">Command ID</param>
        /// <param name="packet">Packet byte[]</param>
        /// <param name="connectionCtx">Connection context ConnectionInfo</param>
        /// <returns>ISrv command</returns>
        protected virtual ISrvCommand GetCommand(
            int commandID, byte[] packet, ConnectionInfo connectionCtx)
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "GetCommand", "MainController"));
        }

        /// <summary>
        /// ���������� ����� ������� ����� communicator 
        /// </summary>
        /// <param name="response">ResponseInfo</param>
        private void SendResponse(SendingInfo response)
        {
            sendMethod(response);
        }

        /// <summary>
        /// ��������� ������� ������� ������� ������ ���-����������
        /// </summary>
        /// <param name="sender">Object</param>
        /// <param name="e">ElapsedEventArgs</param>
        private void OnTimerDeleteOldLogDataElapsed(
            Object sender, ElapsedEventArgs e)
        {
            DeleteOldLogData();
            timerDeleteOldLogData.Start();
        }

        private void OnOverflowDbExceptions(object sender, EventArgsEx args)
        {
            HandleDbOverflowExceptions();
        }

        /// <summary>
        /// ���������� ������� "������������" ���������� DBException
        /// </summary>
        protected virtual void HandleDbOverflowExceptions()
        {
        }

        /// <summary>
        /// ������� ���������
        /// </summary>
        /// <param name="message">MessageSpec ���������</param>
        /// <returns>������ ���� ��������� ���������� � ������� �� ���������</returns>
        public override bool Post(MessageSpec message)
        {
            if (State != ServiceState.Stop)
            {
                return base.Post(message);
            }
            else
            {
                return false;
            }
        }

    }
}
