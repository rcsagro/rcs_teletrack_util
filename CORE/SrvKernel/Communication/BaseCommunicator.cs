#region Using directives
using System;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand.Sending;
using RCS.SrvKernel.Util;
#endregion

namespace RCS.SrvKernel.Communication
{
    /// <summary>
    /// ������� ����� �������������� �������������� � �����������
    /// </summary>
    public class BaseCommunicator : ICommunicator, IDisposable
    {
        /// <summary>
        /// ������ ������ ���������� � ����  -->
        /// </summary>
        protected const string RECEIVING_SYMBOL = "-->";

        /// <summary>
        /// ������ �������� ���������� � ����  <--
        /// </summary>
        protected const string SENDING_SYMBOL = "<--";

        /// <summary>
        /// ������� ����� �� {0}, ����� ������ {1}, ������������� ������� {2}
        /// </summary>
        protected const string RECEIVE_DEBUG_INFO =
            "������� ����� �� {0}, ����� ������ {1}, ������������� ������� {2}";

        /// <summary>
        /// ��������� ����� � {0} ������� {1}, ����� ������ {2}, ������������� ������ {3}
        /// </summary>
        protected const string SEND_DEBUG_INFO =
            "��������� ����� � {0} ������� {1}, ����� ������ {2}, ������������� ������ {3}";

        /// <summary>
        /// ������� �������� ���������� ��� �������� ��� ������� 
        /// �� ���������
        /// </summary>
        protected volatile HandleCommandDelegate CommandHandler;

        /// <summary>
        /// ���������� ������-��������
        /// </summary>
        protected volatile BasePacketStatistics statistics;

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="mCtrlCommandHandler">�����-���������� �������</param>
        public BaseCommunicator(HandleCommandDelegate mCtrlCommandHandler)
        {
            if (mCtrlCommandHandler == null)
            {
                throw new ArgumentNullException(
                    "HandleCommandDelegate MCtrlCommandHandler",
                    String.Format(AppError.TRANSPORT_NULL_PARAM_CTOR,
                        "HandleCommandDelegate MCtrlCommandHandler"));
            }
            if (!TcpIP.ValidateTcpPort(SrvSettings.ServerPort))
            {
                throw new ArgumentOutOfRangeException(String.Format(
                    AppError.TRANSPORT_PORT_PARAM_CTOR,
                    Convert.ToString(SrvSettings.ServerPort)));
            }
            CommandHandler = mCtrlCommandHandler;
            statistics = CreatePacketStatistics();
        }

        /// <summary>
        /// �������� �������� ����������
        /// </summary>
        /// <returns>Packet statistics</returns>
        protected virtual BasePacketStatistics CreatePacketStatistics()
        {
            return new BasePacketStatistics();
        }

        /// <summary>
        /// �������� ������� ������������� ������
        /// </summary>
        protected virtual void CreateTransportLevelObject()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "CreateTransportLevelObject", "BaseCommunicator"));
        }

        /// <summary>
        /// ������������������ ��������� ������� ������������� �������� 
        /// ������ �� ������ �������.
        /// </summary>
        /// <param name="packetID">Packet ID</param>
        protected virtual void PacketConfirmInnerHandler(int packetID)
        {
        }

        /// <summary>
        /// ��������� ���������
        /// </summary>
        /// <param name="response">������-����� � ����������� ��� ��������</param>
        public void Send(SendingInfo response)
        {
            InnerSend(response);
            statistics.IncrementSendPacketCount();
        }

        /// <summary>
        /// Inner send
        /// </summary>
        protected virtual void InnerSend(SendingInfo response)
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "InnerSend", "BaseCommunicator"));
        }

        /// <summary>
        /// �����.
        /// </summary>
        public void Start()
        {
            CreateTransportLevelObject();
            InnerStart();
        }

        /// <summary>
        /// ������ ������.
        /// ������������� ������������� ������:
        /// <para> - �������� �� �������</para> 
        /// </summary>
        protected virtual void InnerStart()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "InnerStart", "BaseCommunicator"));
        }

        /// <summary>
        /// ���������.
        /// </summary>
        public void Stop()
        {
            InnerStop();
        }

        #region IDisposable Members

        public void Dispose()
        {
            statistics.Dispose();
        }

        #endregion

        /// <summary>
        /// ����� ������:
        /// <para> - ������������ �� ������� � ������������ ������
        /// </para> 
        /// </summary>
        protected virtual void InnerStop()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "InnerStop", "BaseCommunicator"));
        }

        /// <summary>
        /// ������ �������� ���������� � ���
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="message">Message</param>
        protected void WriteInfo(string symbol, string message)
        {
            Logger.Write(symbol, message);
        }
    }
}
