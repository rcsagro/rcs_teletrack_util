using RCS.SrvKernel.SrvCommand.Sending;

namespace RCS.SrvKernel.Communication
{
  /// <summary>
  /// ��������� �������������� � ����������� ������������� ����
  /// </summary>
  public interface ICommunicator
  {
    /// <summary>
    /// ��������� ���������
    /// </summary>
    void Send(SendingInfo response);
    /// <summary>
    /// �������� ������
    /// </summary>
    void Start();
    /// <summary>
    /// ��������� ������
    /// </summary>
    void Stop();
  }
} 
