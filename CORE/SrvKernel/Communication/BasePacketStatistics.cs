#region Using directives
using System;
using System.Threading;
using RCS.SrvKernel.Logging;
#endregion

namespace RCS.SrvKernel.Communication
{
  /// <summary>
  /// ���������� ������/��������� ������� 
  /// ������ ����� ������ ������ ���� ��������� ����������
  /// </summary>
  public class BasePacketStatistics : IDisposable
  {
    /// <summary>
    /// ������������ ���������� �������� ������� 
    /// � �������, ������� ������� ������� ������������� 
    /// </summary>
    private const int PACKET_INFO_COUNT = 1000;

    /// <summary>
    /// ���������� ������������ �������
    /// </summary>
    public long SendPacketCount
    {
      get { return sendPacketCount; }
    }
    private long sendPacketCount;

    /// <summary>
    /// ���������� ���������� ������������� �� ��������
    /// � ������ ������������ ������� (�� �������)
    /// </summary>
    public long ConfirmationPacketCount
    {
      get { return confirmationPacketCount; }
    }
    private long confirmationPacketCount;

    /// <summary>
    /// ���������� �������� �������
    /// </summary>
    public long ReceivePacketCount
    {
      get { return receivePacketCount; }
    }
    private long receivePacketCount;

    /// <summary>
    /// ���������� �����, ������������ �������
    /// </summary>
    public long ReceiveErrorPacketCount
    {
      get { return receiveErrorPacketCount; }
    }
    private long receiveErrorPacketCount;

    public BasePacketStatistics()
    {
    }

    /// <summary>
    /// ��������� �� ������� ���-�� ������������ �������
    /// </summary>
    public void IncrementSendPacketCount()
    {
      if (sendPacketCount == long.MaxValue)
      {
        ResetStatistics();
      }
      Interlocked.Increment(ref sendPacketCount);
    }

    /// <summary>
    /// ��������� �� ������� ���-�� ������������� � ���������
    /// </summary>
    public void IncrementConfirmationPacketCount()
    {
      if (confirmationPacketCount == long.MaxValue)
      {
        ResetStatistics();
      }
      Interlocked.Increment(ref confirmationPacketCount);
    }

    /// <summary>
    /// ��������� �� ������� ���-�� �������� �������
    /// </summary>
    public void IncrementReceivePacketCount()
    {
      if (receivePacketCount == long.MaxValue)
      {
        ResetStatistics();
      }
      Interlocked.Increment(ref receivePacketCount);
    }

    /// <summary>
    /// ��������� �� ������� ���-�� ����� �������
    /// </summary>
    public void IncrementReceiveErrorPacketCount()
    {
      if (receiveErrorPacketCount == long.MaxValue)
      {
        ResetStatistics();
      }
      Interlocked.Increment(ref receiveErrorPacketCount);
    }

    private object syncObject = new object();

    /// <summary>
    /// ����� ����������. ������ � ��� �
    /// ��������� ���������
    /// </summary>
    private void ResetStatistics()
    {
      lock (syncObject)
      {
        WriteStatInfo();
        receiveErrorPacketCount = 0;
        receivePacketCount = 0;
        sendPacketCount = 0;
        confirmationPacketCount = 0;
      }
    }

    #region IDisposable Members

    public void Dispose()
    {
      WriteStatInfo();
    }

    #endregion

    /// <summary>
    /// ������ � ��� ����������
    /// </summary>
    protected virtual void WriteStatInfo()
    {

      Logger.Write(String.Format(
        "\r\n ����������.\r\n" + " �������� ������������ ������� {0}, ���������/����� - {1}.\r\n" + " ���������� ������� {2} �� ������� �������� ������������� � �������� �� ������ ������ {3}.\r\n",
        Convert.ToString(receivePacketCount),
        Convert.ToString(receiveErrorPacketCount),
        Convert.ToString(sendPacketCount),
        Convert.ToString(confirmationPacketCount)));
    }
  }
}
