// Project: SrvLib, File: IMainController.cs
// Namespace: RCS.SrvLib, Class: 
// Path: D:\Development\SrvService\SrvLib, Author: guschin
// Code lines: 52, Size of file: 1.33 KB
// Creation date: 2/15/2008 6:46 PM
// Last modified: 5/22/2008 2:25 PM
// Generated with Commenter by abi.exDream.com

using System;

namespace RCS.SrvKernel
{
  /// <summary>
  /// ��������� ���������� ������� ������������
  /// </summary>
  public interface IMainController
  {
    /// <summary>
    /// ��������� ������
    /// </summary>
    /// <param name="ex"></param>
    void HandleException(Exception ex);
    /// <summary>
    /// ������� ��������� ������
    /// </summary>
    ServiceState State { get; }
  }
} 
