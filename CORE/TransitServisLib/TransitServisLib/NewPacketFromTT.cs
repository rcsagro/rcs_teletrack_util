#region Using directives
using System;
#endregion

namespace TransitServisLib
{
  [Serializable]
  public class NewPacketFromTT
  {
    /// <summary>
    /// Create new packet from TT
    /// </summary>
    /// <param name="id_tltr">Id _tltr</param>
    /// <param name="pk">Pk</param>
    public NewPacketFromTT(string loginTT, byte[] pk)
    {
      log_teletrack = loginTT;
      packet = pk;
    }

    public NewPacketFromTT(long id_pk, string loginTT, byte[] pk)
    {
      id_packet = id_pk;
      log_teletrack = loginTT;
      packet = pk;
    }

    long id_packet;
    public long ID_packet
    {
      get { return id_packet; }
      set { id_packet = value; }
    }

    string log_teletrack;
    public string LoginTT
    {
      get { return log_teletrack; }
      set { log_teletrack = value; }
    }

    byte[] packet;
    public byte[] Packet
    {
      get { return packet; }
      set { packet = value; }
    }
  }
}
