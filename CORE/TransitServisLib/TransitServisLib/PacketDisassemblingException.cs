using System;
using System.Text;
using System.Runtime.Serialization;

namespace TransitServisLib
{
  /// <summary>
  /// ������ ����������������� ������.
  /// </summary>
  public class PacketDisassemblingException : Exception
  {
    public PacketDisassemblingException(string message)
      : base(String.Format("������ ����������������� ������: {0}", message))
    {
    }

    public PacketDisassemblingException(string message, Exception innerException)
      : base(message, innerException)
    {
    }

    private PacketDisassemblingException()
    {
    }

    protected PacketDisassemblingException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}
