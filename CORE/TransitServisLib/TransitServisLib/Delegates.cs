using System.Collections.Generic;

namespace TransitServisLib
{
  public delegate void IncomPacketFromSrv(IPacket pk);
  public delegate void IncomNewPacketFromTT(List<int> owners, NewPacketFromTT n_pk);
}
