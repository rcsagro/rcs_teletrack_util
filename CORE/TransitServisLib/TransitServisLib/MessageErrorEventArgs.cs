using System;

namespace TransitServisLib
{
  public class MessageErrorEventArgs : EventArgs
  {
    public MessageErrorEventArgs()
    {
    }

    public string Message
    {
      set { m_message = value; }
      get { return m_message; }
    }
    private string m_message;

    public int ErrorCode
    {
      set { errorCode = value; }
      get { return errorCode; }
    }
    private int errorCode;

    public DateTime DTime
    {
      set { d_time = value; }
      get { return d_time; }
    }
    private DateTime d_time;

    public string IPaddress
    {
      set { m_ip = value; }
      get { return m_ip; }
    }
    private string m_ip;

    public string Login
    {
      set { m_login = value; }
      get { return m_login; }
    }
    private string m_login;
  }

  public delegate void MessageErrorEventHandler(object sender, MessageErrorEventArgs e);
}
