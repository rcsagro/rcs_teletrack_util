using System;

namespace TransitServisLib
{
  public interface IPacket
  {
    byte[] AssemblePacket();
    int DisassemblePacket(byte[] incomPack, int header_pos);

    /// <summary>
    /// ���������� ������ ��������� ������
    /// </summary>
    int PacketSize { get; set; }
  }
}
