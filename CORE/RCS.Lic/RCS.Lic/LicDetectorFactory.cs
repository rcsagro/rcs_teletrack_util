using RCS.Lic.Exceptions;
using RCS.Lic.HASP;

namespace RCS.Lic
{
  /// <summary>
  /// ������� ������������ ����������.
  /// ��������� ������� � ��������� ����� ������ ����
  /// ���������� �������������� ������� (��. ����������� 
  /// �������� <see cref="RCS.Lic.LicDetectorFactory.Active"/>).
  /// ���������� ������������ �� ����� ���������� �������� 
  /// �������� ���������� PROTECT.
  /// </summary>
  /// <remarks>
  /// ����� �������������� ������� ��������� �������� Active.
  /// ���� ���������� ������� ������� ������� � ������ ��������. 
  /// </remarks>
  public class LicDetectorFactory
  {
    /// <summary>
    /// ������� �� ���������� ��������������.
    /// ������� ������ � Release �������.
    /// </summary>
    public static bool Active
    {
      get
      {
        return GetInitVector() != (VcZGIYO.Data[90] + VcZGIYO.Data[117]);
      }
    }

    private static int GetInitVector()
    {
#if (PROTECT)
      return VcZGIYO.Data[10];
#else
      return VcZGIYO.Data[11] + VcZGIYO.Data[54];
#endif
    }

    /// <summary>
    /// ������� �������� ������������ ����������.
    /// </summary>
    /// <exception cref="LicInactiveException">� ������, ����
    /// ���������� ��������� (��. ����������� �������� 
    /// <see cref="RCS.Lic.LicDetectorFactory.Active"/>).</exception>
    /// <returns>ILicDetector.</returns>
    public ILicDetector CreateDetector()
    {
      CheckActivity();
      return new LicDetector(new HaspWrapper(VcZGIYO.Data));
    }

    /// <summary>
    /// ������� �������� ������������ ���������� � ��������
    /// �������������� ����������.
    /// </summary>
    /// <exception cref="LicInactiveException">� ������, ����
    /// ���������� ��������� (��. ����������� �������� 
    /// <see cref="RCS.Lic.LicDetectorFactory.Active"/>).</exception>
    /// <returns>ILicRefreshable</returns>
    public ILicRefreshable CreateAutoDetector()
    {
      CheckActivity();
      return new LicAutoDetector(new HaspWrapper(VcZGIYO.Data));
    }

    private void CheckActivity()
    {
      if (!Active)
      {
        throw new LicInactiveException();
      }
    }
  }
}
