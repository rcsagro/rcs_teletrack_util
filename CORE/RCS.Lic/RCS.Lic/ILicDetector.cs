using RCS.Lic.Entities;

namespace RCS.Lic
{
  /// <summary>
  /// ��������� ��������� ������������ ����������.
  /// </summary>
  public interface ILicDetector
  {
    /// <summary>
    /// ������ ������������ ����������.
    /// </summary>
    /// <returns>������ LicenceInfo.</returns>
    Licence GetLicence();
  }
}
