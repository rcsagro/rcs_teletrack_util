using System;
using System.IO;
using System.Reflection;
using System.Xml;
using Aladdin.HASP;
using RCS.Lic.HASP;
using RCS.Lic.Entities;

namespace RCS.Lic
{
  /// <summary>
  /// ������� ����� ���������� ��������.
  /// ��������� �������� �������� <see cref="RCS.Lic.Entities.LicenceInfo"/>:
  /// ����������� ���� �������� � ��������� ������� ������
  /// �������� � HASP �����.
  /// </summary>
  /// <remarks>
  /// ���� RCS.lic ������������ ����� ������������ ������ xml �����, 
  /// � ������� �������� ��������� ������������ ����� �������� - max ���-�� ����������.
  /// ���� ����������� � ������� ������� HASP HL ToolBox.
  /// </remarks> 
  internal class BaseLicDetector : IStoppable
  {
    /// <summary>
    /// ��� ����� �������� RCS.lic.
    /// </summary>
    public const string RCS_LIC_FILE_NAME = "RCS.lic";
    /// <summary>
    /// ���� � ����� � ��������� RCS.lic.
    /// </summary>
    private readonly string _fileLicInfoPath;

    /// <summary>
    /// ����� ���������� ���������� ����������.
    /// </summary>
    private DateTime _lastUpdateTime;

    /// <summary>
    /// ������������ � �������� ���������
    /// </summary>
    protected volatile bool Stopping;

    private static object _syncObject = new object();

    private readonly IHasp _hasp;

    protected Licence LicenceInfo
    {
      get { return _licence; }
    }
    private Licence _licence;

    private BaseLicDetector() { }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">
    /// �� ������� ��������� IHasp.</exception>
    /// <param name="hasp">IHasp.</param>
    internal BaseLicDetector(IHasp hasp)
    {
      if (hasp == null)
      {
        throw new ArgumentNullException("hasp");
      }
      _hasp = hasp;
      _fileLicInfoPath = GetLicFilePath();
      _licence = new Licence();
    }

    /// <summary>
    /// ���������� ������.
    /// </summary>
    public virtual void Stop()
    {
      Stopping = true;
    }

    /// <summary>
    /// "������" ���������� ���������� � ��������.
    /// ����������� �������� ����� ���������� - 5 ������.
    /// </summary>
    protected void Refresh()
    {
      lock (_syncObject)
      {
        if (DateTime.Now.AddSeconds(-5) > _lastUpdateTime)
        {
          _lastUpdateTime = DateTime.Now;
          _licence = new Licence();

          FillLicenceFromlFile();
          CheckHaspLicNumber();
        }
      }
    }

    /// <summary>
    /// ��������� ������ ���� � ����� ��������.
    /// </summary>
    /// <returns>������ ���� � ����� ��������.</returns>
    private string GetLicFilePath()
    {
      UriBuilder uri = new UriBuilder(Assembly.GetExecutingAssembly().CodeBase);
      return Path.Combine(
        Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path)),
        RCS_LIC_FILE_NAME);
    }

    /// <summary>
    /// �������� ������������ ���������� �� ����� RCS.lic
    /// </summary>
    private void FillLicenceFromlFile()
    {
      byte[] buffer = null;
      try
      {
        _licence.DetailedHaspStatus =
          _hasp.DecodeFile(_fileLicInfoPath, out buffer);

        if (_licence.DetailedHaspStatus == HaspStatus.StatusOk)
        {
          FillFileLicInfo(buffer);
        }
      }
      catch (FileNotFoundException)
      {
        _licence.State = HState.LicFileNotFound;
      }
    }

    /// <summary>
    /// ���������� ���������� � ��������.
    /// </summary>
    /// <param name="buffer">�����, ���������� ���������������� xml-��������.</param>
    private void FillFileLicInfo(byte[] buffer)
    {
      using (Stream stream = new MemoryStream(buffer))
      {
        XmlDocument doc = new XmlDocument();
        try
        {
          doc.Load(stream);
          XmlNodeList nodeList = doc.SelectNodes("/cfg/lic");
          if (nodeList.Count == 1)
          {
            _licence.LicNumber = Convert.ToByte(
              nodeList[0].Attributes["feature"].Value);
            _licence.MaxPermittedTtCount = Convert.ToInt32(
              nodeList[0].Attributes["max"].Value);
          }
          else
          {
            _licence.State = HState.InvalidLicFileXmlFormat;
          }
        }
        catch (Exception)
        {
          _licence.State = HState.InvalidLicFileXmlFormat;
        }
      }
    }

    /// <summary>
    /// �������� ������� ������ �������� _licence.LicNumber � ����-�����.
    /// _licence.LicNumber ������ ���� ��� �������� �� ������������� �����.
    /// </summary>
    private void CheckHaspLicNumber()
    {
      if (_licence.State != HState.OK || Stopping)
      {
        return;
      }
      _licence.DetailedHaspStatus = _hasp.CheckLicenceNumber(_licence.LicNumber);
    }
  }
}
