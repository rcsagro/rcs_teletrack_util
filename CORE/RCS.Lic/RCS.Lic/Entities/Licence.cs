using System;
using Aladdin.HASP;

namespace RCS.Lic.Entities
{
  /// <summary>
  /// ������������ ����������.
  /// </summary>
  public class Licence
  {
    /// <summary>
    /// ��������� �������� HASP-����� � ��������.
    /// </summary>
    public HState State
    {
      get { return _state; }
      set { _state = value; }
    }
    private HState _state;

    /// <summary>
    /// ������ �������� HASP-�����.
    /// </summary>
    public HaspStatus DetailedHaspStatus
    {
      get { return _detailedHaspStatus; }
      set
      {
        _detailedHaspStatus = value;
        _state = HaspStatusToState(value);
      }
    }
    private HaspStatus _detailedHaspStatus;

    /// <summary>
    /// ����� ��������. 
    /// ��������� ������ ��� <code>State = HState.OK</code>
    /// </summary>
    public byte LicNumber
    {
      get
      {
        return _state == HState.OK ? _licNumber : (byte)0;
      }
      set { _licNumber = value; }
    }
    private byte _licNumber;

    /// <summary>
    /// ����������� ����������� ���-�� ����������.
    /// ��������� ������ ��� <code>State = HState.OK</code>
    /// </summary>
    public int MaxPermittedTtCount
    {
      get
      {
        return _state == HState.OK ? _maxPermittedTtCount : 0;
      }
      set { _maxPermittedTtCount = value; }
    }
    private int _maxPermittedTtCount;

    /// <summary>
    /// ��������� �������� ��������.
    /// </summary>
    public string Description
    {
      get { return GetStateDescription(); }
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="state">��������� �������� HASP-����� � ��������.</param>
    /// <param name="haspStatus">������ �������� HASP-�����.</param>
    /// <param name="licNumber">����� ��������.</param>
    /// <param name="maxPermittedTtCount">����������� ����������� ���-�� ����������.</param>
    public Licence(HState state, HaspStatus haspStatus, byte licNumber,
      int maxPermittedTtCount)
    {
      _state = state;
      _detailedHaspStatus = haspStatus;
      _licNumber = licNumber;
      _maxPermittedTtCount = maxPermittedTtCount;
    }

    /// <summary>
    /// �����������.
    /// </summary>
    public Licence()
      : this(HState .HaspNotFound , HaspStatus.ContainerNotFound, 0, 0)
    {
    }

    /// <summary>
    /// �������������� HaspStatus � HState.
    /// </summary>
    /// <param name="status">HaspStatus.</param>
    /// <returns>HState</returns>
    private static HState HaspStatusToState(HaspStatus status)
    {
      switch (status)
      {
        case HaspStatus.StatusOk:
          return HState.OK;
        case HaspStatus.FeatureNotFound:
          return HState.LicenseNotFound;
        default:
          return HState.HaspNotFound;
      }
    }

    /// <summary>
    /// ��������� �������� ��������.
    /// </summary>
    private string GetStateDescription()
    {
      switch (_state)
      {
        case HState.HaspNotFound:
          return String.Format("�� ������ Hasp-���� ({0}).", _detailedHaspStatus);
        case HState.LicenseNotFound:
          return String.Format("�� ������� ���������� �������� � Hasp-����� ({0}).", _detailedHaspStatus);
        case HState.LicFileNotFound:
          return "���� �������� " + BaseLicDetector.RCS_LIC_FILE_NAME + " �� ������.";
        case HState.InvalidLicFileXmlFormat:
          return "���� �������� " + BaseLicDetector.RCS_LIC_FILE_NAME + " ������������� �������.";
        default:
          return String.Format("{0} ����������.", _maxPermittedTtCount);
      }
    }
  }
}
