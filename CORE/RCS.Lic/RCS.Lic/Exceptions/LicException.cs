using System;

namespace RCS.Lic.Exceptions
{
  public class LicException : Exception
  {
    public LicException(string message)
      : base(GetInfo(message))
    {
    }

    public LicException(string message, Exception innerException)
      : base(GetInfo(message), innerException)
    {
    }

    private static string GetInfo(string message)
    {
      return String.Format("���������� ��������������: {0}", message);
    }
  }
}
