using System;

namespace RCS.Lic.Exceptions
{
  /// <summary>
  /// ���������� �������������� ���������.
  /// </summary>
  public class LicInactiveException : LicException
  {
    public LicInactiveException()
      : base("��������� ���������� � ��������� ���������")
    {
    }

    public LicInactiveException(string message)
      : base(message)
    {
    }

    public LicInactiveException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
