using System.Timers;
using RCS.Lic.Entities;
using RCS.Lic.HASP;

namespace RCS.Lic
{
  /// <summary>
  /// �������� ������������ ���������� � ��������
  /// �������������� ����������.
  /// </summary>
  internal class LicAutoDetector : BaseLicDetector, ILicRefreshable
  {
    private volatile Timer _timer;

    internal LicAutoDetector(IHasp hasp)
      : base(hasp)
    {
      CreateTimer();
    }

    #region ILicRefreshable Members

    /// <summary>
    /// �������, ����������� ����� ���������� ������������ ����������.
    /// </summary>
    public event LicRefreshedDelegate LicRefreshed;

    /// <summary>
    /// ������ �������� �������������� ���������� ������������ ����������.
    /// </summary>
    public void StartAutoRefresh()
    {
      Stopping = false;
      if (!_timer.Enabled)
      {
        _timer.Interval = 1000;
        _timer.Start();
      }
    }
        
    #endregion

    /// <summary>
    /// �������� �������.
    /// </summary>
    private void CreateTimer()
    {
      _timer = new Timer();
      _timer.AutoReset = false;
      _timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
    }

    /// <summary>
    /// ��������� ������� ������������ �������.
    /// </summary>
    /// <param name="source">object.</param>
    /// <param name="e">ElapsedEventArgs.</param>
    private void OnTimedEvent(object source, ElapsedEventArgs e)
    {
      try
      {
        Refresh();
      }
      finally
      {
        if (!Stopping)
        {
          if (LicRefreshed != null)
          {
            LicRefreshed(LicenceInfo);
          }
          // ���� �� ������ ���� ��� ������ ������������ ����, 
          // �� �������� ����� ��������� �������� ����������
          // ���� ������ ���� ��������.
          _timer.Interval =
            ((LicenceInfo.State == HState.OK) && (LicenceInfo.MaxPermittedTtCount > 0)) ?
            60 * 5 * 1000 : 15 * 1000;

          _timer.Start();
        }
      }
    }
  }
}
