using Aladdin.HASP;

namespace RCS.Lic.HASP
{
  /// <summary>
  /// ��������� ������ � HASP-������.
  /// </summary>
  public interface IHasp
  {
    /// <summary>
    /// �������� ������� ������ �������� � HASP-�����. 
    /// </summary>
    /// <param name="licenceNumber">����� ��������.</param>
    /// <returns>������ ��������.</returns>
    HaspStatus CheckLicenceNumber(int licenceNumber);
    /// <summary>
    /// ������������� �����.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ���� � �����.</exception>
    /// <exception cref="FileNotFoundException">���� �� ������.</exception>
    /// <param name="fileName">������ ���� � �����.</param>
    /// <param name="decodedFile">�������������� ����� � ������� ����.</param>
    /// <returns>������ ���������� ��������.</returns>
    HaspStatus DecodeFile(string fileName, out byte[] decodedFile);
  }
}
