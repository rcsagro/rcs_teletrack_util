using RCS.Lic.Entities;

namespace RCS.Lic
{
  public delegate void LicRefreshedDelegate(Licence licence);

  /// <summary>
  /// ��������� ��������� ������������ ���������� � ��������
  /// �������������� ����������.
  /// </summary>
  public interface ILicRefreshable
  {
    /// <summary>
    /// ������ �������� �������������� ���������� ������������ ����������.
    /// </summary>
    void StartAutoRefresh();
    /// <summary>
    /// �������, ����������� ����� ���������� ������������ ����������.
    /// </summary>
    event LicRefreshedDelegate LicRefreshed;
  }
}
