using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Security.Principal;
using RWLog;
using RW_Log;

namespace RemoteInteractionLib
{
  public class ChanelInteraction
  {
    static IpcServerChannel ipcServerChannel = null;
    static IpcClientChannel clientChannel = null;

    public static IGetState remoutService = null;

    /// <summary>
    /// ��������� ��������� ipc-����� 
    /// </summary>
    /// <param name="serviceName">��� ������</param>
    public static void OpenServerChanel(string serviceName)
    {
      try
      {
        CloseServerChanel(serviceName);

        SecurityIdentifier si = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
        IdentityReference ir = si.Translate(typeof(NTAccount));

        // ������������ ����� IPC �������.
        IDictionary poroperties = new Hashtable();
        poroperties["name"] = GetChannelName(serviceName);
        poroperties["portName"] = serviceName + "IPCPort";
        poroperties["exclusiveAddressUse"] = "false";
        poroperties.Add("authorizedGroup", ir.Value); //"Everyone");
        poroperties.Add("secure", "True");
        poroperties.Add("impersonationLevel", "Identification");
        ipcServerChannel = new IpcServerChannel(poroperties, null);
        ChannelServices.RegisterChannel(ipcServerChannel, true);
        RemotingConfiguration.RegisterWellKnownServiceType(
            typeof(AccessToServiceState),
            "AccessToServiceState",
            WellKnownObjectMode.Singleton);
      }
      catch (Exception e)
      {
        ExecuteLogging.Log("IpcChannel", String.Format(
          "ERROR OpenServerChanel: {0}\r\n{1}\r\n{2}",
          GetChannelName(serviceName), e.Message, e.StackTrace));
        throw e;
      }
    }
    /// <summary>
    /// ��������� ��������� ipc-����� 
    /// </summary>
    /// <param name="serviceName">��� �������</param>
    public static void CloseServerChanel(string serviceName)
    {
      IChannel ch = ChannelServices.GetChannel(GetChannelName(serviceName));
      if (ch != null)
      {
        ChannelServices.UnregisterChannel(ipcServerChannel);
      }
      ipcServerChannel = null;
    }

    private static string GetChannelName(string serviceName)
    {
      return String.Format("{0} IPC Server Channel", serviceName);
    }

    /// <summary>
    /// ��������� ���������� ipc-����� 
    /// </summary>
    /// <param name="serviceName">��� �������</param>
    /// <returns>�����-�����</returns>
    public static IGetState OpenClientChanel(string serviceName)
    {
      try
      {
        CloseClientChanel(serviceName);

        IDictionary properties = new Hashtable();
        properties.Add("name", GetChannelName(serviceName));
        properties.Add("impersonationLevel", "Identify");
        clientChannel = new IpcClientChannel(properties, new BinaryClientFormatterSinkProvider());
        ChannelServices.RegisterChannel(clientChannel, true);
        string url = String.Format("ipc://{0}IPCPort/AccessToServiceState", serviceName);
        remoutService = (IGetState)Activator.GetObject(typeof(IGetState), url);
        return remoutService;
      }
      catch (Exception e)
      {
        ExecuteLogging.Log("IpcChannel", String.Format("ERROR OpenClientChanel: {0}\r\n{1}\r\n{2}",
          GetChannelName(serviceName), e.Message, e.StackTrace));
        throw e;
      }
    }

    /// <summary>
    /// ��������� ���������� ipc-����� 
    /// </summary>
    /// <param name="serviceName">��� �������</param>
    public static void CloseClientChanel(string serviceName)
    {
      IChannel ch = ChannelServices.GetChannel(GetChannelName(serviceName));
      if (ch != null)
      {
        ChannelServices.UnregisterChannel(ch);
        remoutService = null;
      }
    }
  }
}
