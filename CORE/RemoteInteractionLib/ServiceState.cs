using System;
using System.Collections.Generic;

namespace RemoteInteractionLib
{
  public class ServiceState
  {
    public static MyServiceState beforeServiceState = MyServiceState.NotFound;
    public static MyServiceState curServiceState = MyServiceState.NotFound;

    public static ContainerDataFromService sd = null;

  }
  [Serializable]
  public class ContainerDataFromService
  {

    public List<ILetterInfo> lstLett;//������� � ����� ���������

    public List<string> lstErrors;

    public DateTime dtServiceStart;

    public long incLettCount;//���������� �������� �����(��� ��������� �������)
    public long incBinCount; //���������� �������� BIN-������� 
    public long incTrafic; //�������� ������
    public int connectionCount;//���������� ����������� � �������
    public string boxName; //���� �� �������� ���������� � ������ ������ ������(��� ��������� �������)
  }
}
