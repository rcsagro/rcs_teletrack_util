using System;
using System.Collections.Generic;

namespace RemoteInteractionLib
{
  public class AccessToServiceState : MarshalByRefObject, IGetState
  {
    public ContainerDataFromService GetDataFromService()
    {
      ContainerDataFromService ss = new ContainerDataFromService();
      ss.lstLett = RepositOfStat.GetLettList();
      ss.lstErrors = RepositOfStat.GetErrList();

      ss.incLettCount = RepositOfStat.IncomLettCount;
      ss.incBinCount = RepositOfStat.IncomBinCount;
      ss.incTrafic = RepositOfStat.SizeOfIncomTraffic;
      ss.dtServiceStart = RepositOfStat.DateTimeServiceStart;
      ss.connectionCount = RepositOfStat.ConnectionCount;
      ss.boxName = RepositOfStat.DispetchBoxName;

      return ss;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public List<ILetterInfo> GetLetterInfoList()
    {
      return RepositOfStat.GetLettList();
    }

    public List<string> GetErrorList()
    {
      return RepositOfStat.GetErrList();
    }
  }
}
