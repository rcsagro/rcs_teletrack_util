using System;
using System.Collections.Generic;

namespace RemoteInteractionLib
{
  public interface IGetState
  {
    /// <summary>
    /// ���������� �����-��������� � �������
    /// </summary>
    /// <returns></returns>
    ContainerDataFromService GetDataFromService();

    /// <summary>
    /// ������ �����
    /// (����� �� ������������)
    /// </summary>
    /// <returns></returns>
    List<ILetterInfo> GetLetterInfoList();

    /// <summary>
    /// ������ ������
    /// (����� �� ������������)
    /// </summary>
    /// <returns></returns>
    List<string> GetErrorList();
  }
}
