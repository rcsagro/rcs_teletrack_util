using System;
using System.ServiceProcess;
using System.Threading;
using RemoteInteractionLib;

namespace RemoteInteractionLib
{
  public enum StateIcon
  {
    /// <summary>
    /// ������ �������, � ������ �������� ������
    /// </summary>
    WorkAndWaitOfData,
    /// <summary>
    /// ������ �������, � ������ ������ ������
    /// </summary>
    WorkAndGetData,
    /// <summary>
    /// ����������� ������ � ������ �������
    /// </summary>
    Errors,
    /// <summary>
    /// ������ �� �������
    /// </summary>
    NotWork,
    /// <summary>
    /// ������
    /// </summary>
    Reserves
  }

  public enum MyServiceState
  {
    Running = 4,
    StartPending = 2,
    Stopped = 1,
    StopPending = 3,
    NotFound = 8
  }

  public class ClientIpc : IDisposable
  {
    /// <summary>
    /// ��������� ������, ��������������� 
    /// ������ ��� ������� � ������
    /// </summary>
    public IGetState remoutService;

    /// <summary>
    /// ������
    /// </summary>
    //ServiceController service = null;
    /// <summary>
    /// ��� ������� (���������� ������������� ��� ������)
    /// </summary>
    protected string serviceName;

    /// <summary>
    /// ��������
    /// </summary>
    protected int Number
    {
      get
      {
        if (numbLett > int.MaxValue - 1000)
        {
          numbLett = 0;
        }
        return Interlocked.Increment(ref numbLett);
      }
    }
    private static int numbLett;

    /// <summary>
    /// ��������� ������� �� ���������� ����
    /// </summary>
    protected MyServiceState oldServiceStatus;

    public ClientIpc(string srvName)
    {
      serviceName = srvName;
    }

    public ClientIpc(IRepresentation ui, string srvName)
    {
      userInterface = ui;
      serviceName = srvName;
    }

    #region IDisposable Members

    public void Dispose()
    {
      /*if (timerControllOfService != null)
          timerControllOfService.Dispose();
      ChanelInteraction.CloseClientChanel(serviceName);*/
    }

    #endregion

    protected static IRepresentation userInterface;

    protected ServiceController[] services;

    void OutMsgToUI(string msg, StateIcon state)
    {
      if (userInterface != null)
      {
        userInterface.WriteCurInfo(msg);
      }
    }

    /// <summary>
    /// ��������� ������
    /// </summary>
    public void MailServiceStart()
    {
      ServiceController sr = null;
      services = ServiceController.GetServices();
      for (int i = 0; i < services.Length; i++)
      {
        if (services[i].ServiceName == serviceName)
        {
          sr = services[i];
          switch (sr.Status)
          {
            case ServiceControllerStatus.Running:
              OutMsgToUI(String.Format("������ {0} ��� �������", serviceName),
                StateIcon.WorkAndWaitOfData);
              userInterface.SetEnvelopIcon();
              break;
            case ServiceControllerStatus.StartPending:
              OutMsgToUI(String.Format("������ {0} � �������� �������.", serviceName),
                StateIcon.WorkAndWaitOfData);
              break;
            case ServiceControllerStatus.Stopped:
              {
                StartService(sr);
              }
              break;
            case ServiceControllerStatus.StopPending:
              OutMsgToUI(String.Format("������ {0} � �������� ���������.", serviceName),
                StateIcon.NotWork);
              sr.WaitForStatus(ServiceControllerStatus.StopPending, TimeSpan.FromSeconds(30));
              StartService(sr);
              break;
          }
        }
        if (sr != null)
        {
          break;
        }
      }
      if (sr == null)
      {
        OutMsgToUI(String.Format("������ {0} �� ������.", serviceName), StateIcon.NotWork);
      }
    }

    /// <summary>
    /// ������������� ������
    /// </summary>
    public void MailServiceStop()
    {
      ServiceController sr = null;
      services = ServiceController.GetServices();
      for (int i = 0; i < services.Length; i++)
      {
        if (services[i].ServiceName == serviceName)
        {
          sr = services[i];
          switch (sr.Status)
          {
            case ServiceControllerStatus.Running:
              StopService(sr);
              break;
            case ServiceControllerStatus.StartPending:
              sr.WaitForStatus(ServiceControllerStatus.StartPending, TimeSpan.FromSeconds(30));
              StopService(sr);
              break;
            case ServiceControllerStatus.Stopped:
              userInterface.SetStopOrErrIcon();
              break;
            case ServiceControllerStatus.StopPending:
              //userInterface.WriteCurInfo("������ " + serviceName + " � �������� ���������.");
              break;
          }
        }
        if (sr != null)
        {
          break;
        }
      }
      if (sr == null)
      {
        userInterface.SetStopOrErrIcon();
      }
    }

    /// <summary>
    /// ����������� ��� ������� ��������� ��������
    /// </summary>
    public void StartProgramm()
    {
      ServiceController sr = null;
      try
      {
        services = ServiceController.GetServices();
        for (int i = 0; i < services.Length; i++)
        {
          if (services[i].ServiceName == serviceName)
          {
            sr = services[i];
            switch (sr.Status)
            {
              case ServiceControllerStatus.Running:
                remoutService = ChanelInteraction.OpenClientChanel(serviceName);
                break;
              case ServiceControllerStatus.StartPending:
                sr.WaitForStatus(ServiceControllerStatus.StartPending, TimeSpan.FromSeconds(30));
                remoutService = ChanelInteraction.OpenClientChanel(serviceName);
                break;
              case ServiceControllerStatus.Stopped:
                StartService(sr);
                break;
              case ServiceControllerStatus.StopPending:
                sr.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(30));
                StartService(sr);
                break;
            }
          }
          if (sr != null)
          {
            break;
          }
        }
      }
      catch (InvalidOperationException)
      {
        OutMsgToUI("�������� �������� �������(���������) �������", StateIcon.NotWork);
      }
    }

    void StartService(ServiceController sr)
    {
      sr.Start();
      sr.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(30));

      remoutService = ChanelInteraction.OpenClientChanel(serviceName);
    }

    void StopService(ServiceController sr)
    {
      ChanelInteraction.CloseClientChanel(serviceName);
      sr.Stop();
      sr.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(30));
    }

    /// <summary>
    /// ���������� ��������� �������
    /// </summary>
    /// <returns></returns>
    public MyServiceState GetServiceState(string srvName)
    {
      services = ServiceController.GetServices();
      for (int i = 0; i < services.Length; i++)
      {
        if (services[i].ServiceName == srvName)
        {
          return (MyServiceState)services[i].Status;
        }
      }
      return MyServiceState.NotFound;
    }
  }
}
