using System;

namespace RemoteInteractionLib
{
  public interface IRepresentation
  {
    void WriteCurInfo(string str);
    void WriteErros(string err);
    void LettersCount(int lettCount);
    void PacketsCount(int pkCount);
    void SetEnvelopIcon();

    void SetGetMailIcon();

    void SetSendLetIcon();

    void SetStopOrErrIcon();

    void SetErrorInLetter();
  }
}
