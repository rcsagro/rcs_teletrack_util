using System;

namespace RemoteInteractionLib
{
  public interface ILetterInfo
  {
    /// <summary>
    /// ����� ���������
    /// </summary>
    string Teletrack { get; set; }

    /// <summary>
    /// ������ ������/������
    /// </summary>
    long LettSize { get;  set; }

    /// <summary>
    /// ���-�� �������� MainData32 � ������/������
    /// </summary>
    int PackCountInLetter { get;  set; }

    /// <summary>
    /// ��� ������
    /// </summary>
    string PacketType { get;  set;  }

    /// <summary>
    /// ���� � ����� ��������� ������/������
    /// </summary>
    DateTime DateOfIncom { get; }
  }
}
