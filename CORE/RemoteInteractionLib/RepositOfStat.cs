#region Using directives
using System;
using System.Collections.Generic;
using System.Threading;
#endregion

namespace RemoteInteractionLib
{
  #region �����-�������� ��������� ����� (��� ������)
  /// <summary>
  /// �����-�������� ��������� ����� (��� ������
  /// ��������� �������������� ������/������)
  /// </summary>
  [Serializable]
  public class LetterInfo : ILetterInfo
  {
    public LetterInfo()
    {
      dateOfIncom = DateTime.Now;
    }

    /// <summary>
    /// ����/����� ��������� ������
    /// </summary>
    public DateTime DateOfIncom
    {
      get { return dateOfIncom; }
    }
    private DateTime dateOfIncom;

    /// <summary>
    /// �����������
    /// </summary>
    public string Teletrack
    {
      get { return teletrack; }
      set { teletrack = value; }
    }
    private string teletrack;

    /// <summary>
    /// ������ ������ 
    /// </summary>
    public long LettSize
    {
      get { return lettSize; }
      set { lettSize = value; }
    }
    private long lettSize;


    /// <summary>
    /// ���������� ������� � ������
    /// </summary>
    public int PackCountInLetter
    {
      get { return packCountInLetter; }
      set { packCountInLetter = value; }
    }
    private int packCountInLetter;

    /// <summary>
    /// ��� ��������� �����
    /// (� Pop3Pump-�������)
    /// </summary>
    public string Box
    {
      get { return box; }
      set { box = value; }
    }
    private string box = "";

    /// <summary>
    /// ��� ������  
    /// </summary>
    public string PacketType
    {
      get { return packetType; }
      set { packetType = value; }
    }
    private string packetType;
  }

  #endregion

  /// <summary>
  /// �����-��������� ������
  /// </summary>
  public static class RepositOfStat
  {
    /// <summary>
    /// ����� ������ ������ �������
    /// </summary>
    public static DateTime DateTimeServiceStart
    {
      get { return dtServiceStart; }
    }
    static readonly DateTime dtServiceStart = DateTime.Now;

    /// <summary>
    /// ���������� ���������� �������� ����� 
    /// � ������������������ BIN-�������� (� ������� ������ �������)
    /// </summary>
    public static long IncomLettCount
    {
      get { return Interlocked.Read(ref incomLettCount); }
    }
    private static long incomLettCount;

    /// <summary>
    /// ���������� ���������� �������� BIN-������� � ������� ������ �������
    /// </summary>
    public static long IncomBinCount
    {
      get { return Interlocked.Read(ref incomBinCount); }
    }
    private static long incomBinCount;

    /// <summary>
    /// ������ ��������� �������
    /// </summary>
    public static long SizeOfIncomTraffic
    {
      get { return Interlocked.Read(ref sizeOfIncomTraffic); }
    }
    private static long sizeOfIncomTraffic = 0;

    /// <summary>
    /// �������� ������ ��������� �������
    /// </summary>
    /// <param name="value">������ �������� �������</param>
    public static void AddSizeOfIncomTraffic(int value)
    {
      Interlocked.Add(ref sizeOfIncomTraffic, value);
    }

    /// <summary>
    /// ���������� ���������� ������ ����������� � ������ ������ �������
    /// </summary>
    public static int ErrorCount
    {
      get { return errorCount; }
      set { errorCount = value; }
    }
    private static volatile int errorCount;

    /// <summary>
    /// ���������� ����������� � �������
    /// </summary>
    public static int ConnectionCount
    {
      get { return connectionCount; }
      set { connectionCount = value; }
    }
    private static volatile int connectionCount = 0;

    /// <summary>
    /// ���������� ����� ��������� �� UI
    /// (������ �� ����)
    /// </summary>
    public static int RecordCount
    {
      get { return lstLenght; }
      set { lstLenght = value; }
    }
    private static volatile int lstLenght = 1000;

    /// <summary>
    /// ��� ��������� ����� � �������� ���� ���������
    /// </summary>
    public static string DispetchBoxName
    {
      get { return dispetcherBoxName; }
      set { dispetcherBoxName = value; }
    }
    private static volatile string dispetcherBoxName;

    private static object locker = new object();

    private static List<ILetterInfo> lstLett = new List<ILetterInfo>();

    private static void SaveLettInfo(LetterInfo letInfo)
    {
      lock (locker)
      {
        if (lstLett.Count >= lstLenght)
        {
          lstLett.RemoveAt(0);
        }
        lstLett.Add(letInfo);
        Interlocked.Add(ref incomLettCount, 1);
      }
    }

    /// <summary>
    /// ��������� �������� ���������� ������ �� ������� � ������ (TDataManager)
    /// </summary>
    /// <param name="teletr">����� ���������</param>
    /// <param name="type">��� ������</param>
    /// <param name="size">������ ������</param>
    /// <param name="pkCount">���-�������</param>
    public static void AddLettInfo(string teletr, string type, long size, int pkCount)
    {
      LetterInfo letInfo = new LetterInfo();
      letInfo.Teletrack = teletr;
      letInfo.PacketType = type;
      letInfo.LettSize = size;
      letInfo.PackCountInLetter = pkCount;
      Interlocked.Add(ref incomBinCount, pkCount);
      SaveLettInfo(letInfo);
    }

    /// <summary>
    /// ��������� �������� ������ ������ � ������ �����
    /// </summary>
    /// <param name="teletr">��������</param>
    /// <param name="size">������ ������</param>
    /// <param name="pkCount">���-�� �������</param>
    public static void AddLettInfo(string teletr, long size, int pkCount)
    {
      LetterInfo letInfo = new LetterInfo();
      letInfo.Teletrack = teletr;
      letInfo.LettSize = size;
      letInfo.PackCountInLetter = pkCount;
      Interlocked.Add(ref incomBinCount, pkCount);
      SaveLettInfo(letInfo);
    }

    /// <summary>
    /// ��������� �������� ������ ������ � ������ �����
    /// </summary>
    /// <param name="teletr">��������</param>
    /// <param name="size">������ ������</param>
    /// <param name="pkCount">���-�� �������</param>
    public static void AddLettInfo(string teletr, long size, int pkCount, string boxName)
    {
      LetterInfo letInfo = new LetterInfo();
      letInfo.Teletrack = teletr;
      letInfo.LettSize = size;
      letInfo.PackCountInLetter = pkCount;
      Interlocked.Add(ref incomBinCount, pkCount);
      letInfo.Box = boxName;
      SaveLettInfo(letInfo);
    }

    /// <summary>
    /// ���������� ����� ������ �������� ����� (��� �������
    /// ��������� �������������� ������/������)
    /// </summary>
    /// <returns></returns>
    public static List<ILetterInfo> GetLettList()
    {
      List<ILetterInfo> lst = new List<ILetterInfo>();
      lock (locker)
      {
        for (int i = 0; i < lstLett.Count; i++)
        {
          lst.Add(lstLett[i]);
        }
        lstLett.Clear();
      }
      return lst;
    }

    private static List<string> lstErrors = new List<string>();

    /// <summary>
    /// ��������� �������� ����� ������ � ������ ������
    /// </summary>
    /// <param name="e"></param>
    public static void AddInErrorLst(string e)
    {
      lock (locker)
      {
        if (lstErrors.Count >= lstLenght)
        {
          lstErrors.RemoveAt(lstErrors.Count - 1);
        }
        lstErrors.Add(e);
      }
    }

    /// <summary>
    /// ���������� ����� ������ ������
    /// </summary>
    /// <returns></returns>
    public static List<string> GetErrList()
    {
      List<string> lst = new List<string>();
      lock (locker)
      {
        for (int i = 0; i < lstErrors.Count; i++)
        {
          lst.Add(lstErrors[i]);
        }
        lstErrors.Clear();
      }
      return lst;
    }
  }
}
