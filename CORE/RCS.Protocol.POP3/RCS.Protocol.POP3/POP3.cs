using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace RCS.Protocol.POP3
{
  /// <summary>
  /// Summary description for POP3.
  /// </summary>
  public class POP3 : TcpClient
  {
    /// <summary>
    /// ��������� �� ����� � �������� ����� ���� ������� � ��������� �������
    /// (�������������� ��������)
    /// </summary>
    public static string LocalEndPoint
    {
      get { return localEndPoint; }
      set { localEndPoint = value; }
    }
    private static string localEndPoint;


    private string[] m_lineTypeString =
		{
			"From",
			"To",
			"Subject",
			"Content-Type",
      "Date"
		};

    public void ConnectPOP(string sServerName, int port, string sUserName, string sPassword)
    {
      //message and the server resulting response
      string sMessage;
      string sResult;

      //call the connect method of the TcpClient class
      //remember default port for server is 110
      if (localEndPoint != null && localEndPoint != "")
      {
        IPAddress adr = IPAddress.Parse(localEndPoint);
        IPEndPoint myEndpoint = new IPEndPoint(adr, 0);
        this.Client.Bind(myEndpoint);
      }

      Connect(sServerName, port);

      //get result back
      sResult = Response();

      if (sResult == "")
        throw new POPException("������ " + sServerName + " �� ������� �� ������ �����������.");

      //check response to make sure it�s +OK
      if (sResult.Substring(0, 3) != "+OK")
        throw new POPException(sResult);

      //got past connect, send username
      sMessage = "USER " + sUserName + "\r\n";
      //write sends data to the Tcp Connection
      Write(sMessage);
      sResult = Response();
      //check response
      if (sResult.Substring(0, 3) != "+OK")
      {
        throw new POPException(sResult);
      }

      //now follow up with sending password in same manner
      sMessage = "PASS " + sPassword + "\r\n";
      Write(sMessage);
      sResult = Response();
      if (sResult.Substring(0, 3) != "+OK")
      {
        throw new POPException(sResult);
      }
    }

    public void DisconnectPOP()
    {
      string sMessage;
      string sResult;

      sMessage = "QUIT\r\n";
      Write(sMessage);

      sResult = Response();
      Close();
      if (sResult.Substring(0, 3) != "+OK")
      {
        throw new POPException(sResult);
      }
    }

    int countOfLetter = 0;

    public List<POP3EmailMessage> StatMessages(int limitOfLetter)
    {
      //same sort of thing as in ConnectPOP and DisconnectPOP
      string sMessage;
      string sResult;

      List<POP3EmailMessage> returnValue = new List<POP3EmailMessage>();

      sMessage = "STAT\r\n";
      Write(sMessage);

      sResult = Response();
      if (sResult.Substring(0, 3) != "+OK")
      {
        throw new POPException(sResult);
      }
      //define a separator
      char[] sep = { ' ' };
      //use the split method to break out array of data
      string[] values = sResult.Split(sep);

      int msgCount = Int32.Parse(values[1]);
      if (msgCount > limitOfLetter) msgCount = limitOfLetter;
      for (int i = 1; i <= msgCount; i++)
      {
        POP3EmailMessage oMailMessage = new POP3EmailMessage();
        oMailMessage.msgNumber = i;
        oMailMessage.msgSize = 0;
        oMailMessage.msgReceived = false;
        returnValue.Add(oMailMessage);
      }
      return returnValue;
    }

    public List<POP3EmailMessage> ListMessages(int limitOfLetter)
    {
      //same sort of thing as in ConnectPOP and DisconnectPOP
      string sMessage;
      string sResult;

      List<POP3EmailMessage> returnValue = new List<POP3EmailMessage>();

      sMessage = "LIST\r\n";
      Write(sMessage);

      sResult = Response();
      if (sResult.Substring(0, 3) != "+OK")
      {
        throw new POPException(sResult);
      }

      while (true)
      {
        sResult = Response();
        if (sResult == ".\r\n" || countOfLetter >= limitOfLetter)
        {
          return returnValue;
        }
        else
        {
          countOfLetter++;
          POP3EmailMessage oMailMessage = new POP3EmailMessage();
          //define a separator
          char[] sep = { ' ' };
          //use the split method to break out array of data
          string[] values = sResult.Split(sep);

          //put data into oMailMessage object
          oMailMessage.msgNumber = Int32.Parse(values[0]);
          oMailMessage.msgSize = Int32.Parse(values[1]);
          oMailMessage.msgReceived = false;
          returnValue.Add(oMailMessage);
          //add to message box display

          continue;
        }
      }
    }

    public POP3EmailMessage RetrieveMessage(POP3EmailMessage msgRETR)
    {
      string sMessage;
      string sResult;

      //create new instance of object and set new values
      POP3EmailMessage oMailMessage = new POP3EmailMessage();
      oMailMessage.msgSize = msgRETR.msgSize;
      oMailMessage.msgNumber = msgRETR.msgNumber;

      //call the RETR command to get the appropriate message
      sMessage = "RETR " + msgRETR.msgNumber + "\r\n";
      Write(sMessage);
      sResult = Response();
      if (sResult.Substring(0, 3) != "+OK")
      {
        throw new POPException(sResult);
      }
      //������ ������ ������� ������
      char[] sep = { ' ' };
      //use the split method to break out array of data
      string[] values = sResult.Split(sep);
      oMailMessage.msgSize = Int32.Parse(values[1]);

      //set the received flag equal to true since we got the message
      oMailMessage.msgReceived = true;
      //now loop to get the message text until we hit the �.� end point
      bool flag = false;
      //string msg = "";
      while (true)
      {
        try
        {
          sResult = Response();
        }
        catch (POPException ex)
        {
          throw new POPException(ex.Message + " ��������� ������:\r\n oMailMessage.msgSize: " + oMailMessage.msgSize.ToString() +
              "\r\n oMailMessage.msgNumber: " + oMailMessage.msgNumber.ToString() + "\r\n oMailMessage.msgContent: " + oMailMessage.msgContent);
        }

        if (sResult == ".\r\n")
        {
          break;
        }
        else
        {
          if (!flag)
          {
            int pkType = GetHeaderLineType(sResult);
            switch (pkType)
            {
              case 0://"From":
                {
                  string str = Regex.Replace(sResult, @"^From:.*[ |<]([a-z|A-Z|0-9|\.|\-|_]+@[a-z|A-Z|0-9|\.|\-|_]+).*$", "$1");

                  oMailMessage.msgFrom = str.Remove(str.IndexOf('@'));
                }
                break;
              case 2://"Subject":
                {
                  if (sResult.IndexOf("Bin") != -1)
                    oMailMessage.msgType = (int)TeletrPKType.PACK_BIN;
                  else
                    oMailMessage.msgType = (int)TeletrPKType.PACK_A1;
                }
                break;
            }
          }
          else
            oMailMessage.msgContent += sResult;
        }
        if (sResult == "\r\n")
          flag = true;
      }
      return oMailMessage;
    }

    public void DeleteMessage(POP3EmailMessage msgDELE)
    {
      string sMessage;
      string sResult;

      sMessage = "DELE " + msgDELE.msgNumber + "\r\n";
      Write(sMessage);
      sResult = Response();
      if (sResult.Substring(0, 3) != "+OK")
      {
        throw new POPException(sResult);
      }
    }

    private void Write(string sMessage)
    {
      //used for Data Encoding
      System.Text.ASCIIEncoding oEncodedData = new System.Text.ASCIIEncoding();

      //now grab the message into a buffer for sending to the TCP network stream
      byte[] WriteBuffer = new byte[1024];
      WriteBuffer = oEncodedData.GetBytes(sMessage);

      //take the buffer and output it to the TCP stream
      NetworkStream NetStream = GetStream();
      NetStream.Write(WriteBuffer, 0, WriteBuffer.Length);
    }


    private string Response()
    {
      System.Text.ASCIIEncoding oEncodedData = new System.Text.ASCIIEncoding();
      byte[] ServerBuffer = new Byte[8192];
      NetworkStream NetStream = GetStream();
      int count = 0;

      //here we read from the server network stream and place data into
      //the buffer (to later decode and return)
      while (true)
      {
        byte[] buff = new Byte[2];
        int bytes = NetStream.Read(buff, 0, 1);
        if (bytes == 0)
          throw new POPException("������� 0 ����. ��������� ���� ������������� �������� ����������. \r\n");
        if (bytes == 1)
        {
          ServerBuffer[count] = buff[0];
          count++;

          if (buff[0] == '\n')
          {
            break;
          }
        }
        else
        {
          break;
        }
      }

      //return the decoded ASCII string value
      string ReturnValue = oEncodedData.GetString(ServerBuffer, 0, count);
      return ReturnValue;
    }

    /// <summary>
    /// ���������� ��� ������ �� ���������
    /// </summary>
    /// <param name="line"></param>
    /// <returns></returns>
    private int GetHeaderLineType(string line)
    {
      int lineType = -1;// m_notKnownState;

      for (int i = 0; i < m_lineTypeString.Length; i++)
      {
        string match = m_lineTypeString[i];

        if (Regex.Match(line, "^" + match + ":" + ".*$").Success)
        {
          lineType = i;
          break;
        }
        else
          if (line.Length == 0)
          {
            //lineType = m_endOfHeader;
            break;
          }
      }

      return lineType;
    }

  }
}
