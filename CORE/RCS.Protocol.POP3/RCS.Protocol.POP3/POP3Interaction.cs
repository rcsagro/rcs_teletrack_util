using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using RemoteInteractionLib;
using RW_Log;

namespace RCS.Protocol.POP3
{
  public class POP3Interaction
  {
    int limitOfLetter = 1000;
    /// <summary>
    /// ������������ ���������� �����, ���������� �� ��������� ����� �� ���� �����
    /// </summary>
    public int LimitOfLetter
    {
      get { return limitOfLetter; }
      set { limitOfLetter = value; }
    }

    /// <summary>
    /// ������������ �������� ������� � ������������� ������
    /// (������� �����)
    /// </summary>
    /// <param name="ip_address"></param>
    public static void BindServiceToAddr(string ip_address)
    {
      if (ip_address == null || ip_address == "")
      {
        throw new ArgumentNullException(
          "BindServiceToAddr: �� ����� ip-����� ��� �������� �������");
      }
      try
      {
        IPAddress.Parse(ip_address);
        POP3.LocalEndPoint = ip_address;
      }
      catch
      {
        throw new FormatException(
          "BindServiceToAddr: �������� ������ ip-������ ��� �������� �������");
      }
    }

    /// <summary>
    /// ������������� ������� �����������
    /// 1 - ���.; 0 - ����.
    /// </summary>
    public static int LogLevel
    {
      set { ExecuteLogging.loggingLevel = value; }
    }

    /// <summary>
    /// ��������� ����� �������� ������ ��� ���������
    /// �� �������� � ������ ����������
    /// </summary>
    /// <param name="serviceName">��� �������</param>
    public static void StatisticOn(string serviceName)
    {
      ChanelInteraction.OpenServerChanel(serviceName);
    }

    /// <summary>
    /// ��������� ����� �������� ������ ��� ���������
    /// �� �������� � ������ ����������
    /// </summary>
    /// <param name="serviceName">��� �������</param>
    public static void StatisticOff(string serviceName)
    {
      ChanelInteraction.CloseServerChanel(serviceName);
    }

    void Finalise()
    {
    }

    /// <summary>
    /// ����� ��� �������� ������� �1 �� �������� ������ 
    /// </summary>
    /// <param name="smtpServer">��� SMTP �������</param>
    /// <param name="smtpPort">���� SMTP �������</param>
    /// <param name="to">����</param>
    /// <param name="from">�� ����</param>
    /// <param name="body">�1</param>
    /// <param name="error">��������� ������</param>
    /// <returns></returns>
    public bool PostA1(string smtpServer, int smtpPort, string to,
      string from, string body, out string error)
    {
      bool rez = true;
      error = "";
      try
      {
        MailMessage email = new MailMessage();
        email.From = new System.Net.Mail.MailAddress(from);
        email.To.Add(new MailAddress(to));
        email.Body = body;// +"\r\n";
        //email.BodyEncoding = Encoding.GetEncoding("Windows-1254"); // Turkish Character Encoding// ��������� ���� �����

        SmtpClient smtp = new SmtpClient();
        smtp.Host = smtpServer;
        smtp.Port = smtpPort;

        smtp.Send(email);//�������� 
      }
      catch (SmtpException ex)
      {
        error = ex.Message;
        rez = false;
      }
      catch (Exception ex)
      {
        error = ex.Message;
        rez = false;
      }

      return rez;
    }

    /// <summary>
    /// ��������� ������� ����� ����� �� �������� �������
    /// </summary>
    /// <param name="mailSrv">POP3 ������</param>
    /// <param name="mailPort">POP3 ����</param> 
    /// <param name="login">login</param>
    /// <param name="pass">������</param>
    /// <param name="lstLett">���-�� �����</param>
    /// <param name="lstErrorLet">������ � �������</param>
    public void VerifyOfEMail(string mailSrv, int mailPort, string login, string pass,
      out List<POP3Packet> lstLett, out List<string> lstErrorLet)
    {
      if (String.IsNullOrEmpty(mailSrv))
      {
        new ArgumentNullException("�� ����� POP3 ������");
      }
      if (String.IsNullOrEmpty(login))
      {
        new ArgumentNullException("�� ����� POP3 �����");
      }

      lstLett = new List<POP3Packet>();
      lstErrorLet = new List<string>();

      POP3 oPOP = new POP3();
      RepositOfStat.DispetchBoxName = login;
      oPOP.ConnectPOP(mailSrv, mailPort, login, pass);

      List<POP3EmailMessage> messageList = oPOP.StatMessages(limitOfLetter);
      RepositOfStat.ConnectionCount++;

      try
      {
        foreach (POP3EmailMessage POPMsg in messageList)
        {
          POP3EmailMessage POPMsgContent = oPOP.RetrieveMessage(POPMsg);
          if (POPMsgContent.msgContent != null)
          {
            POP3Packet lett = new POP3Packet();
            lett.Teletrack = POPMsgContent.msgFrom;
            int oldPkCount = lett.lstPackets.Count;

            if (POPMsgContent.msgType == (int)TeletrPKType.PACK_BIN)
            {
              GetBinFromLetter(POPMsgContent.msgFrom, POPMsgContent.msgContent,
                ref lett.lstPackets, ref lstErrorLet);
            }
            else
            {
              GetA1FromLetter(POPMsgContent.msgFrom, POPMsgContent.msgContent,
                ref lett.lstPackets, ref lstErrorLet);
            }

            lstLett.Add(lett);
            oPOP.DeleteMessage(POPMsgContent);

            RepositOfStat.AddLettInfo(POPMsgContent.msgFrom,
              POPMsgContent.msgContent.Length, lett.lstPackets.Count - oldPkCount, login);
            RepositOfStat.AddSizeOfIncomTraffic(POPMsgContent.msgSize);
          }
        }
      }
      catch (Exception e)
      {
        lstErrorLet.Add(String.Format(
          "������ ��� ������� ���������� ���������� ������ ��� ��� ��� ��������.\r\n{0}", e));
      }
      oPOP.DisconnectPOP();
    }

    private void GetA1FromLetter(string teltrack, string msgBody,
      ref List<POP3Packet> lstPack, ref List<string> lstErrorLet)
    {
      try
      {
        string str = msgBody.Replace("\r\n", "");
        int verify;
        Math.DivRem(str.Length, 160, out verify);
        if (verify == 0)
        {
          for (int i = 0; i < str.Length; i += 160)
          {
            PacketA1 pk = new PacketA1();
            pk.packetA1 = str.Substring(i, 160);
            lstPack.Add(pk);
          }
        }
        else
        {
          lstErrorLet.Add(String.Format("��������: {0}\r\n���� ������ �������� 160. {1}",
            teltrack, msgBody));
        }
      }
      catch (Exception e)
      {
        lstErrorLet.Add(String.Format("��������: {0}\r\n������ ��������� ���� ������: {1}",
          teltrack, e.Message));
      }
    }

    private void GetBinFromLetter(string teltrack, string msgBody,
      ref List<POP3Packet> lst, ref List<string> lstErrorLet)
    {
      try
      {
        byte[] bytes = Convert.FromBase64String(msgBody);
        int verify;
        Math.DivRem(bytes.Length, 32, out verify);
        if (verify == 0)
        {
          for (int i = 0; i < bytes.Length; i += 32)
          {
            PacketBin pk = new PacketBin();
            Array.Copy(bytes, i, pk.packet, 0, pk.packet.Length);
            lst.Add(pk);
          }
        }
        else
        {
          lstErrorLet.Add(String.Format("��������: {0}\r\n���� ������ �������� 32  {1}",
            teltrack, msgBody));
        }
      }
      catch (Exception e)
      {
        lstErrorLet.Add(String.Format("��������: {0}\r\n������ ��������� ���� ������: {1}",
          teltrack, e.Message));
      }
    }
  }
}
