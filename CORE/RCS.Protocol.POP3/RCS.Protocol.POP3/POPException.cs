using System;

namespace RCS.Protocol.POP3
{
  /// <summary>
  /// Summary description for POPException.
  /// </summary>
  public class POPException : Exception
  {
    public POPException(string str) : base(str)
    {
    }
  }
}
