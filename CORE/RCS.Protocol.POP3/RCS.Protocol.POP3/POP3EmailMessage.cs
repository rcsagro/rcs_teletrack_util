using System;

namespace RCS.Protocol.POP3
{
  /// <summary>
  /// Summary description for POP3EmailMessage.
  /// </summary>
  public class POP3EmailMessage
  {
    //define public members
    public long msgNumber;
    public int msgSize;
    public bool msgReceived;
    public string msgContent;
    public string msgFrom;
    public int msgType;
    public DateTime Date;
  }

}
