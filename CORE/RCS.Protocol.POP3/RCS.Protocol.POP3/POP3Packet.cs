using System;
using System.Collections.Generic;

namespace RCS.Protocol.POP3
{
  public enum TeletrPKType
  {
    /// <summary>
    /// ����� �����������
    /// </summary>
    PACK_AUTH = 1,
    /// <summary>
    /// ����� MultiBin
    /// </summary>
    PACK_MBIN = 2,
    /// <summary>
    /// ����� PacketMultiBin
    /// </summary>
    PACK_PK_MBIN = 3,
    /// <summary>
    /// ����� CmdRequest
    /// </summary>
    PACK_CMD_REQUEST = 4,
    /// <summary>
    /// ����� Response
    /// </summary>
    PACK_RESPONSE = 5,
    /// <summary>
    /// ����� A1
    /// </summary>
    PACK_A1 = 6,
    /// <summary>
    /// ����� Bin
    /// </summary>
    PACK_BIN = 7,
    /// <summary>
    /// ����� Ack
    /// </summary>
    PACK_ACK = 8
  }

  public class POP3Packet
  {
    /// <summary>
    /// �����������
    /// </summary>
    public string Teletrack
    {
      get { return teletrack; }
      set { teletrack = value; }
    }
    string teletrack;
    public List<POP3Packet> lstPackets = new List<POP3Packet>();
  }
}
