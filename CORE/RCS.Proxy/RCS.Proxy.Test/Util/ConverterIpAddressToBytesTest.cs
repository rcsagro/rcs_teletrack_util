using System;
using NUnit.Framework;
using NUnit.Mocks;
using RCS.Proxy.Exceptions;
using RCS.Proxy.Util;

namespace RCS.Proxy.Test.Util
{
  [TestFixture, Description("�������������� ����� ����� ��� IP ������ � ������ ����.")]
  public class ConverterIpAddressToBytesTest
  {
    private readonly object[] _validAddressCases = {
      new object[] {"localhost", new byte[]{127, 0, 0, 1} },
      new object[] {"127.0.0.1", new byte[]{127, 0, 0, 1} }
    };

    [Test, TestCaseSource("_validAddressCases"),
      Description("�������������� �������� �������� ������.")]
    public void ConvertValidAddresses(string host, byte[] bytes)
    {
      byte[] result = Converter.IpAddressToBytes(host);
      Assert.AreEqual(bytes, result);
    }

    private readonly string[] _nullAddressCases = { null, String.Empty };

    [Test, TestCaseSource("_nullAddressCases"),
      Description("�������������� ������������� �������� ������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ConvertNullAddresses(string host)
    {
      Converter.IpAddressToBytes(host);
    }

    private readonly string[] _invalidAddressCases = { "1234.56.7.8", "qwerty.qwerty.com.asd" };

    [Test, TestCaseSource("_invalidAddressCases"),
      Description("�������������� ���������� �������� ������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void ConvertInvalidAddresses(string host)
    {
      DynamicMock converterMock = new DynamicMock(typeof(IHostToAddressConverter));
      converterMock.ExpectAndThrow("Convert", new ArgumentException(), null);

      Converter.IpAddressToBytes(host, (IHostToAddressConverter)converterMock.MockInstance);
    }
  }
}
