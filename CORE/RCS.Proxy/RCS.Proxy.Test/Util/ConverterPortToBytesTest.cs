using NUnit.Framework;
using RCS.Proxy.Exceptions;
using RCS.Proxy.Util;

namespace RCS.Proxy.Test.Util
{
  [TestFixture, Description("�������������� ������ ����� � ������ ����.")]
  public class ConverterPortToBytesTest
  {
    private readonly object[] _validPortValueCases = {
      new object[] {0, new byte[]{0,0} },
      new object[] {8080, new byte[]{31, 144} },
      new object[] {65535, new byte[]{255, 255} }
    };

    [Test, TestCaseSource("_validPortValueCases"),
      Description("�������� �������� �������� �����.")]
    public void ValidateValidPortValues(int port, byte[] bytes)
    {
      Assert.AreEqual(bytes, Converter.PortToBytes(port));
    }

    private readonly int[] _invalidPortValueCases = { -1, 65536 };

    [Test, TestCaseSource("_invalidPortValueCases"),
      Description("�������� ���������� �������� �����.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void ValidateInvalidPortValues(int port)
    {
      Converter.PortToBytes(port);
    }
  }
}
