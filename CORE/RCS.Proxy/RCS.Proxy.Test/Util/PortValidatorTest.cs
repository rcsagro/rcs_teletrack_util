using NUnit.Framework;
using RCS.Proxy.Exceptions;
using RCS.Proxy.Util;

namespace RCS.Proxy.Test.Util
{
  [TestFixture, Description("��������� �������� �����.")]
  public class PortValidatorTest
  {
    private readonly int[] _validPortValueCases = { 0, 8080, 65535 };

    [Test, TestCaseSource("_validPortValueCases"),
      Description("�������� �������� �������� �����.")]
    public void ValidateValidPortValues(int port)
    {
      PortValidator.Validate(port);
    }

    private readonly int[] _invalidPortValueCases = { -1, 65536 };

    [Test, TestCaseSource("_invalidPortValueCases"),
      Description("�������� ���������� �������� �����.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void ValidateInvalidPortValues(int port)
    {
      PortValidator.Validate(port);
    }
  }
}
