using System;
using NUnit.Framework;
using RCS.Proxy.Client.HttpProxy;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Test.Client.HttpProxy
{
  [TestFixture, Description("����� Http Proxy �������.")]
  public class HttpProxyResponseTest
  {
    private readonly string[] _nullResponses = { null };
    
    [Test, TestCaseSource("_nullResponses"), Description("�� ���������� �������� � �����������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CreateWithNullParam(string response)
    {
      new HttpProxyResponse(response);
    }

    private readonly object[] _goodResponses = 
    { 
     new object[] {
       "HTTP/1.0 200 Connection Established\r\nok ok ok ok ok\r\nbla, bla\r\n",
       new HttpResponseHeader(HttpResponseCode.OK, "Connection Established") },
     new object[] {
       "HTTP/1.0 502 BadGateway\r\nok ok ok ok ok\r\nbla, bla\r\n",
       new HttpResponseHeader(HttpResponseCode.BadGateway, "BadGateway") }
    };

    [Test, TestCaseSource("_goodResponses"), Description("������� ���������� �������.")]
    public void ParseResponse(string responseText, HttpResponseHeader testResult)
    {
      HttpProxyResponse response = new HttpProxyResponse(responseText);
      Assert.AreEqual(responseText, response.Response);

      HttpResponseHeader header = response.DecodeResponseHeader();
      Assert.AreEqual(testResult.Code, header.Code);
      Assert.AreEqual(testResult.Text, header.Text);
    }

    private readonly string[] _badResponses = 
    { 
      "",
      "\r\n",  
      "1.0 Connection Established",
      "1.0 200 Connection Established\r\nok ok ok ok ok\r\r\n",
      "HTTP/1.0 BadGateway\r\nok ok ok ok ok\r\nbla, bla\r\n",
      "HTTP/1.0 bla BadGateway\r\nok ok ok ok ok\r\nbla, bla\r\n"
    };

    [Test, TestCaseSource("_badResponses"), Description("������� ������������ �������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void ParseBadResponse(string response)
    {
      new HttpProxyResponse(response).DecodeResponseHeader();
    }
  }
}
