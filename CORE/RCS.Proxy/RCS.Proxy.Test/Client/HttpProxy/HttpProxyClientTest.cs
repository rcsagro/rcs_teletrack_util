using System;
using NUnit.Framework;
using RCS.Proxy.Client.HttpProxy;
using RCS.Proxy.Client;

namespace RCS.Proxy.Test.Client.HttpProxy
{
  [TestFixture, Description("HTTP Proxy ������.")]
  public class HttpProxyClientTest
  {
    [Test, Description("�� ���������� �������� ������������ � �����������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CreateWithNullCfg()
    {
      new HttpProxyClient(null);
    }

    [Test, Description("�������� HttpProxyClient.")]
    public void CreateHttpProxyClient()
    {
      ProxyCfg cfg = new ProxyCfg("localhost", 8080, "", "", 1);
      HttpProxyClient client = new HttpProxyClient(cfg);
      Assert.AreSame(cfg, client.ProxyConfig);
    }
  }
}
