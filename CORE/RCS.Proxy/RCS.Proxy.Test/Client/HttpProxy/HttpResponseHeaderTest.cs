using System;
using NUnit.Framework;
using RCS.Proxy.Client.HttpProxy;

namespace RCS.Proxy.Test.Client.HttpProxy
{
  [TestFixture, Description("��������� ������ HTTP Proxy �������.")]
  public class HttpResponseHeaderTest
  {
    private readonly object[] _constructorParams = 
    { 
     new object[] { HttpResponseCode.OK, "OK"},
     new object[] { HttpResponseCode.BadGateway, String.Empty}
    };

    [Test, TestCaseSource("_constructorParams"),
      Description("�������� ��������� ������ HTTP Proxy �������.")]
    public void CreateHttpResponseHeader(HttpResponseCode code, string text)
    {
      HttpResponseHeader header = new HttpResponseHeader(code, text);
      Assert.AreEqual(code, header.Code);
      Assert.AreEqual(text, header.Text);
    }

    [Test, Description("������� �������� null � �����������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CreateWithNullText()
    {
      new HttpResponseHeader(HttpResponseCode.BadGateway, null);
    }
  }
}
