using System;
using System.Text;
using NUnit.Framework;
using RCS.Proxy.Client.HttpProxy;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Test.Client.HttpProxy
{
  [TestFixture, Description("������ �� ����������� ����� Http Proxy ������.")]
  public class HttpProxyConnectionRequestTest
  {
    private readonly object[] _invalidHostParams = 
    { 
      new object[] { null, 80},
      new object[] { "", 80}
    };

    [Test, TestCaseSource("_invalidHostParams"),
      Description("�� ���������� �������� ���� � �����������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CreateWithNullHost(string host, int port)
    {
      new HttpProxyConnectionRequest(host, port);
    }

    private readonly object[] _invalidPortParams = 
    { 
      new object[] { "localhost", -1 },
      new object[] { "127.0.0.1", 100000 }
    };

    [Test, TestCaseSource("_invalidPortParams"),
      Description("���������� ������������ �������� ���� � �����������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void CreateWithBadPort(string host, int port)
    {
      new HttpProxyConnectionRequest(host, port);
    }

    private readonly object[] _constructorParams = 
    { 
     new object[] {"localhost", 8080},
     new object[] {"127.0.0.1", 1080}
    };

    [Test, TestCaseSource("_constructorParams"),
      Description("�������� ����������� HttpProxyConnectionCommand.")]
    public void CreateCommand(string host, int port)
    {
      HttpProxyConnectionRequest cmd = new HttpProxyConnectionRequest(host, port);
      string test = String.Format(HttpProxyConnectionRequest.CONNECTION_COMMAND, host, port);
      Assert.AreEqual(test, cmd.CommandText);
      Assert.AreEqual(Encoding.ASCII.GetBytes(test), cmd.Encode());
    }
  }
}
