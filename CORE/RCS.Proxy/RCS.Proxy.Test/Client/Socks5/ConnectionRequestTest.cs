using System;
using NUnit.Framework;
using RCS.Proxy.Client.Socks5;

namespace RCS.Proxy.Test.Client.Socks5
{
  [TestFixture, Description("�������-������ ���������� � �������� ������.")]
  public class ConnectionRequestTest
  {
    private readonly string[] _nullValue = { null, "" };

    [Test, TestCaseSource("_nullValue"), Description("�������� �������� null �����.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ValidateNullHost(string value)
    {
      new ConnectionRequest(value, 80);
    }

    [Test, Description("�����������.")]
    public void Encode()
    {
      ConnectionRequest r = new ConnectionRequest("127.0.0.1", 80);
      Assert.AreEqual("127.0.0.1", r.Host);
      Assert.AreEqual(80, r.Port);

      byte[] test = r.Encode();
      Assert.AreEqual(GetExpectedArray(), test);
    }

    private byte[] GetExpectedArray()
    {
      byte[] destAddr = new byte[4] { 127, 0, 0, 1 };
      byte[] destPort = new byte[2] { 0, 80 };
      byte[] expected = new byte[4 + destAddr.Length + 2];
      expected[0] = AbstractMessage.VERSION_NUMBER;
      expected[1] = (byte)Socks5ClientCommand.Connect;
      expected[2] = 0;
      expected[3] = (byte)AddressType.IPv4;
      destAddr.CopyTo(expected, 4);
      destPort.CopyTo(expected, 4 + destAddr.Length);
      return expected;
    }
  }
}
