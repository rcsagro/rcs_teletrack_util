using System;
using NUnit.Framework;
using RCS.Proxy.Client.Socks5;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Test.Client.Socks5
{
  [TestFixture, Description("����� proxy-������� Socks5 � ���������� ��������� ��������������.")]
  public class AuthenticationResponseTest
  {
    [Test, Description("�������� null �������� ��������� ������������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CreateWithNullParam()
    {
      new AuthenticationResponse(null);
    }

    private readonly object[] _wrongArraysLength = 
    { 
      new byte[] {}, new byte[] { 5 }, new byte[] { 5, 6, 7 } 
    };

    [Test, TestCaseSource("_wrongArraysLength"), Description("������� ������������� �������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void CreateWithWrongArrayLength(byte[] response)
    {
      new AuthenticationResponse(response);
    }

    [Test, Description("������ � ������ �� ����������� ��-�� ������� ��������� rfc1929.")]
    public void IgnoreCheckVersion()
    {
      new AuthenticationResponse(new byte[] { 6, 1 }).Decode();
    }

    private readonly object[] _goodResponses = 
    { 
      new object[] {
        new byte[] { AuthenticationRequest.LOGIN_PASSWORD_AUTH_VERSION_NUMBER, 0 },
        (byte)0 },
      new object[] { 
        new byte[] { AuthenticationRequest.LOGIN_PASSWORD_AUTH_VERSION_NUMBER, 1 }, 
        (byte)1 }
    };

    [Test, TestCaseSource("_goodResponses"), Description("������������� ���������� ���������.")]
    public void Decode(byte[] response, byte expectedStatus)
    {
      byte s = new AuthenticationResponse(response).Decode();
      Assert.AreEqual(expectedStatus, s);
    }
  }
}
