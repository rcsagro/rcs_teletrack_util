using System;
using NUnit.Framework;
using RCS.Proxy.Client.Socks5;

namespace RCS.Proxy.Test.Client.Socks5
{
  [TestFixture, Description("�������-����������� � ������ ��������������.")]
  public class AuthenticationMethodRequestTest
  {
    private readonly object[] _nullCases = { null, new AuthenticationMethod[] { } };

    [Test, TestCaseSource("_nullCases"),
      Description("�������� ������� �������� ��������� ������������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CreateWithNullParam(AuthenticationMethod[] methods)
    {
      new AuthenticationMethodRequest(methods);
    }

    [Test, Description("�������� �������� � ����������� �������� �������.")]
    [ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void CreateWithBigArrayParam()
    {
      new AuthenticationMethodRequest(new AuthenticationMethod[256]);
    }

    [Test, Description("����������� ����������� � ������ ��������������.")]
    public void AuthenticationMethodRequest()
    {
      AuthenticationMethod[] methods = new AuthenticationMethod[2] { 
          AuthenticationMethod.NoAuthenticationRequired,
          AuthenticationMethod.UsernamePassword };

      AuthenticationMethodRequest req = new AuthenticationMethodRequest(methods);

      Assert.AreEqual(2, req.MethodsNumber);
      Assert.AreEqual(methods, req.Methods);

      byte[] result = req.Encode();
      Assert.NotNull(result);
      Assert.AreEqual(4, result.Length);

      byte[] expected = new byte[4] { AbstractMessage.VERSION_NUMBER, 2, 0, 2 };
      Assert.AreEqual(expected, result);
    }
  }
}
