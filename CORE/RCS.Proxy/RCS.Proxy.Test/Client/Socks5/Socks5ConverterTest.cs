using System;
using NUnit.Framework;
using RCS.Proxy.Client.Socks5;

namespace RCS.Proxy.Test.Client.Socks5
{
  [TestFixture, Description("��������������.")]
  public class Socks5ConverterTest
  {
    private Socks5Converter _converter;

    [SetUp]
    public void Init()
    {
      _converter = new Socks5Converter();
    }

    private readonly string[] _nullHostCases = { null, "" };

    [Test, TestCaseSource("_nullHostCases"),
      Description("�������� ���������� �������� �����.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void AddressToBytesWithNullHost(string host)
    {
      AddressType addressType;
      _converter.AddressToBytes(host, out addressType);
    }

    private readonly object[] _goodHostCases = { 
      new object[] { "127.0.0.1",  AddressType.IPv4 },
      new object[] { "localhost",  AddressType.DomainName },
      new object[] { "::1",  AddressType.IPv6 }
    };

    [Test, TestCaseSource("_goodHostCases"),
      Description("�������� �������� �������� �����.")]
    public void AddressToBytes(string host, AddressType addrType)
    {
      AddressType test;
      byte[] addr = _converter.AddressToBytes(host, out test);
      Assert.AreEqual(addrType, test);
      Assert.IsNotNull(addr);
      Assert.Greater(addr.Length, 0);
    }
  }
}
