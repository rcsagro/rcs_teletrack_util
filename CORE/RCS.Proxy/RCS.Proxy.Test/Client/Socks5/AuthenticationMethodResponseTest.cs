using System;
using NUnit.Framework;
using RCS.Proxy.Client.Socks5;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Test.Client.Socks5
{
  [TestFixture, Description("����� proxy-������� � �������������� ������ ��������������.")]
  public class AuthenticationMethodResponseTest
  {
    [Test, Description("�������� null �������� ��������� ������������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CreateWithNullParam()
    {
      new AuthenticationMethodResponse(null);
    }

    private readonly object[] _wrongArraysLength = 
    { 
      new byte[] {}, new byte[] { 5 }, new byte[] { 5, 6, 7 } 
    };

    [Test, TestCaseSource("_wrongArraysLength"), Description("������� ������������� �������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void CreateWithWrongArrayLength(byte[] response)
    {
      new AuthenticationMethodResponse(response);
    }

    [Test, Description("������������ ������ � ������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void CheckWrongVersion()
    {
      new AuthenticationMethodResponse(new byte[] { 6, 1 }).Decode();
    }

    private readonly object[] _wrongMethods = 
    { 
      new byte[] { AbstractMessage.VERSION_NUMBER, 0x01 },
      new byte[] { AbstractMessage.VERSION_NUMBER, 0x03 }, 
      new byte[] { AbstractMessage.VERSION_NUMBER, 0x10 }, 
      new byte[] { AbstractMessage.VERSION_NUMBER, 0x7f }, 
      new byte[] { AbstractMessage.VERSION_NUMBER, 0x80 }, 
      new byte[] { AbstractMessage.VERSION_NUMBER, 0xbb }, 
      new byte[] { AbstractMessage.VERSION_NUMBER, 0xfe } 
    };

    [Test, TestCaseSource("_wrongMethods"), Description("���������������� ����� ��������������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void CheckWrongMethod(byte[] response)
    {
      new AuthenticationMethodResponse(response).Decode();
    }

    private readonly object[] _goodMethods = 
    { 
      new object[] {
        new byte[] { AbstractMessage.VERSION_NUMBER, 0x00 }, 
        AuthenticationMethod.NoAuthenticationRequired },
      new object[] { 
        new byte[] { AbstractMessage.VERSION_NUMBER, 0x02 }, 
        AuthenticationMethod.UsernamePassword },
      new object[] { 
        new byte[] { AbstractMessage.VERSION_NUMBER, 0xff }, 
        AuthenticationMethod.NoAcceptableMethods }
    };

    [Test, TestCaseSource("_goodMethods"), Description("������������� ���������� ���������.")]
    public void Decode(byte[] response, AuthenticationMethod expectedMethod)
    {
      AuthenticationMethod m = new AuthenticationMethodResponse(response).Decode();
      Assert.AreEqual(expectedMethod, m);
    }
  }
}
