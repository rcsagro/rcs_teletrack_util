using System;
using NUnit.Framework;
using RCS.Proxy.Client.Socks5;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Test.Client.Socks5
{
  [TestFixture, Description("����� Socks5 proxy-������� �� ������� ���������� � �������� ������.")]
  public class ConnectionResponseTest
  {
    [Test, Description("�������� null �������� ��������� ������������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CreateWithNullParam()
    {
      new ConnectionResponse(null);
    }

    private readonly object[] _wrongArraysLength = 
    { 
      new byte[] {}, new byte[] { 5 }
    };

    [Test, TestCaseSource("_wrongArraysLength"), Description("������� ������������� �������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void CreateWithWrongArrayLength(byte[] response)
    {
      new ConnectionResponse(response);
    }

    [Test, Description("������������ ������ � ������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void CheckWrongVersion()
    {
      new ConnectionResponse(new byte[] { 6, 1 }).Decode();
    }

    private readonly object[] _goodResponses = 
    { 
      new object[] {
        new byte[] { AbstractMessage.VERSION_NUMBER, 0, 0, (byte)AddressType.IPv4, 127, 0, 0, 1, 0, 80 },
        Socks5ServerResponse.Succeeded },
      new object[] { 
        new byte[] { AbstractMessage.VERSION_NUMBER, 5, 0 }, 
        Socks5ServerResponse.ConnectionRefused }
    };

    [Test, TestCaseSource("_goodResponses"), Description("������������� ���������� ���������.")]
    public void Decode(byte[] response, Socks5ServerResponse expectedCode)
    {
      Socks5ServerResponse test = new ConnectionResponse(response).Decode();
      Assert.AreEqual(expectedCode, test);
    }
  }
}
