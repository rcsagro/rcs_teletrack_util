using System;
using NUnit.Framework;
using RCS.Proxy.Client.Socks5;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Test.Client.Socks5
{
  [TestFixture, Description("������� ��������������.")]
  public class AuthenticationRequestTest
  {
    private readonly string[] _nullValue = { null, "" };

    [Test, TestCaseSource("_nullValue"), Description("�������� �������� null ������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ValidateNullLogin(string value)
    {
      new AuthenticationRequest(value, "e");
    }

    [Test, TestCaseSource("_nullValue"), Description("�������� �������� null ������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ValidateNullPassword(string value)
    {
      new AuthenticationRequest("user", value);
    }

    private readonly string _longLoginPassword =
      "11111111111111111111111111111111111111111111111111" +
      "11111111111111111111111111111111111111111111111111" +
      "11111111111111111111111111111111111111111111111111" +
      "11111111111111111111111111111111111111111111111111" +
      "11111111111111111111111111111111111111111111111111" +
      "111111111111111111111111111111111111111111111111111";

    [Test, Description("�������� �������� ������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void ValidateLongLogin()
    {
      new AuthenticationRequest(_longLoginPassword, "q");
    }

    [Test, Description("�������� �������� ������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void ValidateLongPassword()
    {
      new AuthenticationRequest("user", _longLoginPassword);
    }

    [Test, Description("����������� ������� ��������������.")]
    public void Encode()
    {
      string login = "u";
      string pass = "pwd";
      AuthenticationRequest request = new AuthenticationRequest(login, pass);
      Assert.AreEqual(login, request.Login);
      Assert.AreEqual(1, request.LoginLength);
      Assert.AreEqual(pass, request.Password);
      Assert.AreEqual(3, request.PasswordLength);

      byte[] expected = new byte[7] { AuthenticationRequest.LOGIN_PASSWORD_AUTH_VERSION_NUMBER, 
        1, 117, 3, 112, 119, 100 };
      Assert.AreEqual(expected, request.Encode());
    }
  }
}
