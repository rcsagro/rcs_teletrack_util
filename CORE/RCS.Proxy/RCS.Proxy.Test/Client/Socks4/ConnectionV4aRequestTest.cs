using System;
using NUnit.Framework;
using RCS.Proxy.Client.Socks4;

namespace RCS.Proxy.Test.Client.Socks4
{
  [TestFixture, Description("�������-������ ���������� � �������� ������.")]
  public class ConnectionV4aRequestTest
  {
    private readonly string[] _nullValue = { null, "" };

    [Test, TestCaseSource("_nullValue"), Description("�������� �������� null �����.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ValidateNullHost(string value)
    {
      new ConnectionV4aRequest(value, 80, "");
    }

    private readonly object[] _encodeValues = 
    {
      new object[] { "qw" , (ushort)80, null,  
        new byte[] { Socks4Client.VERSION_NUMBER, (byte)Socks4ClientCommand.Connect,
          0, 80, 0, 0, 0, 1, 0, 113, 119, 0 } },
      new object[] { "qw" , (ushort)80, "usr", 
        new byte[] { Socks4Client.VERSION_NUMBER, (byte)Socks4ClientCommand.Connect, 
          0, 80, 0, 0, 0, 1, 117, 115, 114, 0, 113, 119,  0 } }
    };

    [Test, TestCaseSource("_encodeValues"), Description("�����������.")]
    public void Encode(string host, ushort port, string login, byte[] expectedResult)
    {
      ConnectionV4aRequest r = new ConnectionV4aRequest(host, port, login);
      Assert.AreEqual(host, r.Host);
      Assert.AreEqual(port, r.Port);
      Assert.AreEqual(login, r.Login);

      byte[] test = r.Encode();
      Assert.AreEqual(expectedResult, test);
    }
  }
}
