using System;
using NUnit.Framework;
using RCS.Proxy.Client.Socks4;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Test.Client.Socks4
{
  [TestFixture, Description("����� Socks4(4a) proxy-������� �� ������� ����������.")]
  public class ConnectionResponseTest
  {
    [Test, Description("�������� null �������� ��������� ������������.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CreateWithNullParam()
    {
      new ConnectionResponse(null);
    }

    private readonly object[] _wrongArraysLength = 
    { 
      new byte[] {}, new byte[] { 5 }
    };

    [Test, TestCaseSource("_wrongArraysLength"), Description("������� ������������� �������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void CreateWithWrongArrayLength(byte[] response)
    {
      new ConnectionResponse(response);
    }

    [Test, Description("������������ ������ � ������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void CheckWrongVersion()
    {
      new ConnectionResponse(new byte[] { 6, 1 }).Decode();
    }

    private readonly object[] _goodResponses = 
    { 
      new object[] {
        new byte[] {0, (byte)Socks4ServerResponse.RequestGranted, 0, 80, 127, 0, 0, 1 },
        Socks4ServerResponse.RequestGranted },
      new object[] { 
        new byte[] { 0, (byte)Socks4ServerResponse.RequestRejectedOrFailed, 0, 80, 127, 0, 0, 1},
        Socks4ServerResponse.RequestRejectedOrFailed}
    };

    [Test, TestCaseSource("_goodResponses"), Description("������������� ���������� ���������.")]
    public void Decode(byte[] response, Socks4ServerResponse expectedCode)
    {
      Socks4ServerResponse test = new ConnectionResponse(response).Decode();
      Assert.AreEqual(expectedCode, test);
    }
  }
}
