using System;
using NUnit.Framework;
using RCS.Proxy.Client.Socks4;

namespace RCS.Proxy.Test.Client.Socks4
{
  [TestFixture, Description("�������-������ ���������� � �������� ������.")]
  public class ConnectionV4RequestTest
  {
    private readonly string[] _nullValue = { null, "" };

    [Test, TestCaseSource("_nullValue"), Description("�������� �������� null �����.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ValidateNullHost(string value)
    {
      new ConnectionV4Request(value, 80, "");
    }

    private readonly object[] _encodeValues = 
    {
      new object[] { "127.0.0.1" , (ushort)80, null,  
        new byte[] { Socks4Client.VERSION_NUMBER, (byte)Socks4ClientCommand.Connect,
          0, 80, 127, 0, 0, 1, 0 } },
      new object[] { "127.0.0.1" , (ushort)80, "usr", 
        new byte[] { Socks4Client.VERSION_NUMBER, (byte)Socks4ClientCommand.Connect, 
          0, 80, 127, 0, 0, 1, 117, 115, 114, 0 } }
    };

    [Test, TestCaseSource("_encodeValues"), Description("�����������.")]
    public void Encode(string host, ushort port, string login, byte[] expectedResult)
    {
      ConnectionV4Request r = new ConnectionV4Request(host, port, login);
      Assert.AreEqual(host, r.Host);
      Assert.AreEqual(port, r.Port);
      Assert.AreEqual(login, r.Login);

      byte[] test = r.Encode();
      Assert.AreEqual(expectedResult, test);
    }
  }
}
