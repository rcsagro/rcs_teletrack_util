using System;
using NUnit.Framework;
using RCS.Proxy.Client;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Test.Client
{
  [TestFixture, Description("��������� ����������� � Proxy �������.")]
  public class ProxyCfgTest
  {
    private readonly string[] _nullAddressCases = { null, String.Empty };

    [Test, TestCaseSource("_nullAddressCases"),
      Description("�������� �������� ���������� �������� ������.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void SetNullAddressParam(string host)
    {
      new ProxyCfg(host, 80, "", "", 0);
    }

    private readonly int[] _invalidPortValueCases = { -1, 65536 };

    [Test, TestCaseSource("_invalidPortValueCases"),
     Description("�������� �������� ���������� �������� �����.")]
    [ExpectedException(typeof(ProxyClientException))]
    public void SetInvalidPortValueParam(int port)
    {
      new ProxyCfg("localhost", port, "", "", 0);
    }

    private readonly object[] _invalidTimeOutCases = {
      new int[] { -1, 1 }, 
      new int[] { 0, 1 },
      new int[] { 40, 30 } };

    [Test, TestCaseSource("_invalidTimeOutCases"),
     Description("�������� �������� ���������� �������� ��������.")]
    public void SetInvalidTimeOutParam(int invalidTimeout, int correctTimeout)
    {
      ProxyCfg cfg = new ProxyCfg("localhost", 80, "", "", invalidTimeout);
      Assert.AreEqual(correctTimeout, cfg.AnswerTimeout);
    }

    [Test, Description("�������� �������� ����������.")]
    public void CreateProxyCfgInstance()
    {
      ProxyCfg cfg = new ProxyCfg("localhost", 80, "user", "pwd", 15);
      Assert.AreEqual("localhost", cfg.Host);
      Assert.AreEqual(80, cfg.Port);
      Assert.AreEqual("user", cfg.Login);
      Assert.AreEqual("pwd", cfg.Password);
      Assert.AreEqual(15, cfg.AnswerTimeout);
    }
  }
}
