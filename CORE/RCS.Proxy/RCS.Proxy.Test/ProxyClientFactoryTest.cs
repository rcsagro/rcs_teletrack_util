using System;
using NUnit.Framework;
using RCS.Proxy.Client;
using RCS.Proxy.Client.HttpProxy;
using RCS.Proxy.Client.Socks4;
using RCS.Proxy.Client.Socks5;

namespace RCS.Proxy.Test
{
  [TestFixture, Description("������� ������-��������.")]
  public class ProxyClientFactoryTest
  {
    private ProxyClientFactory _proxyClientFactory;
    [SetUp]
    public void Init()
    {
      _proxyClientFactory = new ProxyClientFactory();
    }

    [Test, Description("���� �� �������� ��������� ����������� � Proxy-������� � �����, �� �������� ArgumentNullException.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void FailureCreateProxyClient()
    {
      _proxyClientFactory.CreateProxyClient(null, ProxyType.Socks5);
    }

    private static object[] _createProxyClientCases = {
      new object[] { 
        new ProxyCfg("localhost", 80, "", "", 0), 
        ProxyType.HTTP,
        typeof(HttpProxyClient) },
      new object[] { 
        new ProxyCfg("localhost", 80, "", "", 0), 
        ProxyType.Socks4,
        typeof(Socks4Client) },
      new object[] { 
        new ProxyCfg("localhost", 80, "", "", 0), 
        ProxyType.Socks4a,
        typeof(Socks4aClient) },
      new object[] { 
        new ProxyCfg("localhost", 80, "", "", 0), 
        ProxyType.Socks5,
        typeof(Socks5Client) }
    };

    [Test, TestCaseSource("_createProxyClientCases"), Description(
      "�������� �������� �������������� �����.")]
    public void CreateProxyClient(ProxyCfg cfg, ProxyType proxyType, Type type)
    {
      IProxyClient clnt = _proxyClientFactory.CreateProxyClient(cfg, proxyType);
      Assert.IsNotNull(clnt);
      Assert.IsInstanceOf(type, clnt);
    }
  }
}
