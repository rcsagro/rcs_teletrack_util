using System.Net.Sockets;

namespace RCS.Proxy
{
  /// <summary>
  /// ��������� ����������� � ������ ����� ������ ������.
  /// </summary>
  public interface IProxyClient
  {
    /// <summary>
    /// ����������� � �����.
    /// </summary>
    /// <exception cref="RCS.Proxy.Exceptions.ProxyClientException"></exception>
    /// <exception cref="ArgumentNullException">�� ����� ���� ��� �����������.</exception>
    /// <exception cref="ArgumentOutOfRangeException">������������ �������� �����.</exception>
    /// <param name="destinationHost">�������� ����, � �������� ������ ������������.</param>
    /// <param name="destinationPort">���� ��������� �����, � �������� ������ ������������.</param>
    /// <returns>TcpClient.</returns>
    TcpClient ConnectToHost(string destinationHost, int destinationPort);
  }
}
