
namespace RCS.Proxy
{
  /// <summary>
  /// ���� ������-��������.
  /// </summary>
  public enum ProxyType
  {
    /// <summary>
    /// HTTP ������.
    /// </summary>
    HTTP,
    /// <summary>
    /// SOCKS ������ 4. 
    /// </summary>
    Socks4,
    /// <summary>
    /// SOCKS ������ 4a.
    /// </summary>
    Socks4a,
    /// <summary>
    /// SOCKS ������ 5.
    /// </summary>
    Socks5
  }
}
