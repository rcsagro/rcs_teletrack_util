using System;
using System.Runtime.Serialization;

namespace RCS.Proxy.Exceptions
{
  public class ProxyClientException : Exception
  {
    public ProxyClientException(string message)
      : base(String.Format("PROXY ERROR: {0}", message))
    {
    }

    public ProxyClientException(string message, Exception innerException)
      : base(message, innerException)
    {
    }

    private ProxyClientException()
    {
    }

    protected ProxyClientException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}
