using System;
using System.Net;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Util
{
  /// <summary>
  /// ���������������.
  /// </summary>
  public static class Converter
  {
    /// <summary>
    /// �������������� ������ ����� � ������ ����.
    /// </summary>
    /// <exception cref="ProxyClientException">�������� ����� ������ ���� �
    /// �������� ���������</exception>
    /// <param name="port">����� �����.</param>
    /// <returns>������ ����, �������������� ����� ����� �����.</returns>
    public static byte[] PortToBytes(int port)
    {
      PortValidator.Validate(port);

      return new byte[] {
        Convert.ToByte(port / 256), 
        Convert.ToByte(port % 256)};
    }

    /// <summary>
    /// �������������� ����� ����� ��� IP ������ � ������ ����.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ����.</exception>
    /// <exception cref="ProxyClientException">������ DNS-���������� ����� �����.</exception>
    /// <param name="host">��� ����� ��� IP �����.</param>
    /// <returns>������ ����, �������������� ����� IP �����.</returns>
    public static byte[] IpAddressToBytes(string host)
    {
      return IpAddressToBytes(host, new HostToAddressConverter());
    }

    /// <summary>
    /// �������������� ����� ����� ��� IP ������ � ������ ����.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ����.</exception>
    /// <exception cref="ProxyClientException">������ DNS-���������� ����� �����.</exception>
    /// <param name="host">��� ����� ��� IP �����.</param>
    /// <param name="converter">IHostToAddressConverter.</param>
    /// <returns>������ ����, �������������� ����� IP �����.</returns>
    public static byte[] IpAddressToBytes(string host, IHostToAddressConverter converter)
    {
      if (String.IsNullOrEmpty(host))
      {
        throw new ArgumentNullException("host");
      }

      IPAddress ipAddr;

      if (!IPAddress.TryParse(host, out ipAddr))
      {
        try
        {
          ipAddr = converter.Convert(host);
        }
        catch (Exception ex)
        {
          throw new ProxyClientException(String.Format(
            "������ DNS-���������� ����� ����� {0}.", host), ex);
        }
      }

      return ipAddr.GetAddressBytes();
    }
  }
}
