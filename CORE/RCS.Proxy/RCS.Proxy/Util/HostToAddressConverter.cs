using System.Net;

namespace RCS.Proxy.Util
{
  internal class HostToAddressConverter : IHostToAddressConverter
  {
    /// <summary>
    /// �������������� ����� ����� � IP �����.
    /// </summary>
    /// <param name="hostName">��� �����.</param>
    /// <returns>IP �����.</returns>
    public IPAddress Convert(string hostName)
    {
      return Dns.GetHostEntry(hostName).AddressList[0];
    }
  }
}
