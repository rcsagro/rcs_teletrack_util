using System.Net;

namespace RCS.Proxy.Util
{
  /// <summary>
  /// ��������� ��������������� ����� ����� � IP �����.
  /// </summary>
  public interface IHostToAddressConverter
  {
    /// <summary>
    /// �������������� ����� ����� � IP �����.
    /// </summary>
    /// <param name="hostName">��� �����.</param>
    /// <returns>IP �����.</returns>
    IPAddress Convert(string hostName);
  }
}
