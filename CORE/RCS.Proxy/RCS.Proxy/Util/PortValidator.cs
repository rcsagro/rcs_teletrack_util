using System.Net;
using System;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Util
{
  /// <summary>
  /// �������� �������� �����.
  /// </summary>
  public static class PortValidator
  {
    /// <summary>
    /// �������� ���������� �������� �����. ���� �������� ����������, �� 
    /// ���������� ���������� ProxyClientException.
    /// </summary>
    /// <exception cref="ProxyClientException">�������� ����� ������ ���� �
    /// �������� ���������.</exception>
    /// <param name="port">����������� ��������.</param>
    public static void Validate(int port)
    {
      if ((port < IPEndPoint.MinPort) || (port > IPEndPoint.MaxPort))
      {
        throw new ProxyClientException(String.Format(
          "�������� ����� ������ ���� � ��������� �� {0} �� {1}.",
          IPEndPoint.MinPort, IPEndPoint.MaxPort));
      }
    }
  }
}
