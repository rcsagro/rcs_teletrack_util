using System;
using RCS.Proxy.Client;
using RCS.Proxy.Client.HttpProxy;
using RCS.Proxy.Client.Socks4;
using RCS.Proxy.Client.Socks5;

namespace RCS.Proxy
{
  /// <summary>
  /// ������� ������-��������.
  /// </summary>
  public class ProxyClientFactory
  {
    /// <summary>
    /// �������� ������-������� � ����������� � �������� �������������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� �������� ��������� 
    /// ����������� � Proxy-�������.</exception>
    /// <param name="cfg">��������� ����������� � Proxy-�������.</param>
    /// <param name="type">���� ������-�������.</param>
    /// <returns>IProxyClient.</returns>
    public IProxyClient CreateProxyClient(ProxyCfg cfg, ProxyType type)
    {
      if (cfg == null)
      {
        throw new ArgumentNullException("cfg");
      }
      switch (type)
      {
        case ProxyType.HTTP:
          return new HttpProxyClient(cfg);
        case ProxyType.Socks4:
          return new Socks4Client(cfg);
        case ProxyType.Socks4a:
          return new Socks4aClient(cfg);
        default:
          return new Socks5Client(cfg);
      }
    }
  }
}
