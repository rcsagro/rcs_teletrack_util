using System;
using System.Net.Sockets;
using System.Text;
using RCS.Proxy.Client;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.Http
{
  /// <summary>
  /// HTTP Proxy ������.
  /// </summary>
  internal class HttpClient : ProxyClient
  {
    /// <summary>
    /// CONNECT {0}:{1} HTTP/1.0 \r\n\r\n
    /// </summary>
    private const string CONNECTION_COMMAND = "CONNECT {0}:{1} HTTP/1.0\r\n\r\n";

    /// <summary>
    /// ������ �� ����������� � �����.
    /// </summary>
    private readonly string _request;

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="cfg">��������� ����������� � Proxy-�������.</param>
    public HttpClient(ProxyCfg cfg)
      : base(cfg)
    {
      _request = String.Format(CONNECTION_COMMAND, DestinationHost, DestinationPort);
    }

    /// <summary>
    /// ������� ����������� � ��������� ����� ����� Proxy-������.
    /// </summary>
    /// <returns>TcpClient.</returns>
    protected override TcpClient CreateConnection()
    {
      TcpClient result = new TcpClient();
      result.Connect(Config.Host, Config.Port);

      NetworkStream networkStream = result.GetStream();
      SendConnectToHost(networkStream);
      WaitForResponse(networkStream);
      HandleResponse(networkStream, result.ReceiveBufferSize);
      return result;
    }

    /// <summary>
    /// �������� ������� Proxy-�������.
    /// </summary>
    /// <remarks>
    /// PROXY SERVER REQUEST
    /// <code>
    /// CONNECT host:port HTTP/1.0
    /// 
    /// </code>
    /// </remarks>
    /// <param name="proxy">Proxy server data stream.</param>
    protected void SendConnectToHost(NetworkStream proxy)
    {
      byte[] buffer = Encoding.ASCII.GetBytes(_request);
      proxy.Write(buffer, 0, buffer.Length);
    }

    /// <summary>
    /// ��������� ������ Proxy-�������.
    /// </summary>
    /// <remarks>
    /// PROXY SERVER RESPONSE
    /// <code>
    /// HTTP/1.0 200 Connection Established
    /// [.... other HTTP header lines
    /// ignore all of them]
    /// 
    /// </code>
    /// </remarks>
    /// <param name="proxy">Proxy server data stream.</param>
    /// <param name="receiveBufferSize">������ ������ ������.</param>
    private void HandleResponse(NetworkStream proxy, int receiveBufferSize)
    {
      byte[] buffer = new byte[receiveBufferSize];
      StringBuilder response = new StringBuilder();
      int bytes = 0;

      do
      {
        bytes = proxy.Read(buffer, 0, receiveBufferSize);
        response.Append(Encoding.UTF8.GetString(buffer, 0, bytes));
      } while (proxy.DataAvailable);

      HttpResponseHeader header = ParseHeader(GetResponseHeader(response.ToString()));

      if (header.Code != HttpResponseCode.OK)
      {
        HandleProxyResponseError(header);
      }
    }

    /// <summary>
    /// �������� ��������� ������ - ������ ������.
    /// </summary>
    /// <param name="response"></param>
    /// <returns></returns>
    private static string GetResponseHeader(string response)
    {
      StringBuilder header = new StringBuilder();
      int index = 0;

      while ((response[index] != '\n') && (response[index] != '\r'))
      {
        header.Append(response[index]);
        index++;
      }
      return header.ToString().Trim();
    }

    /// <summary>
    /// ������� ��������� HTTP-������ ������ �������.
    /// </summary>
    /// <exception cref="ProxyClientException"></exception>
    /// <param name="header">��������� HTTP-������ ������ �������.</param>
    /// <returns>HttpResponseHeader.</returns>
    private HttpResponseHeader ParseHeader(string header)
    {
      if (header.IndexOf("HTTP") == -1)
      {
        throw new ProxyClientException(String.Format(
          "�� ������ HTTP-����� �� ������ ������� {0}:{1}. ��������� ������: {2}.",
          Config.Host, Config.Port, header));
      }

      int begin = header.IndexOf(" ") + 1;
      int end = header.IndexOf(" ", begin);

      string val = header.Substring(begin, end - begin);
      Int32 code = 0;

      if (!Int32.TryParse(val, out code))
      {
        throw new ProxyClientException(String.Format(
          "������������ ��� ������ �� ������ ������� {0}:{1}. ��������� ������: {2}.",
          Config.Host, Config.Port, header));
      }

      return new HttpResponseHeader((HttpResponseCode)code,
        header.Substring(end + 1).Trim());
    }

    /// <summary>
    /// ��������� ������.
    /// </summary>
    /// <exception cref="ProxyClientException"></exception>
    /// <param name="responseHeader">HttpResponseHeader.</param>
    private void HandleProxyResponseError(HttpResponseHeader responseHeader)
    {
      string msg;

      switch (responseHeader.Code)
      {
        case HttpResponseCode.None:
          msg = String.Format(
            "������ ������ {0}:{1} �� ���� ������� �������������� ��� ������. ��������� ������: {2}.",
            Config.Host, Config.Port, responseHeader.Text);
          break;

        case HttpResponseCode.BadGateway:
          // HTTP/1.1 502 Proxy Error 
          // (The specified Secure Sockets Layer (SSL) port is not allowed. 
          // ISA Server is not configured to allow SSL requests from this port. 
          // Most Web browsers use port 443 for SSL requests.)
          msg = String.Format("��� ������ ������ ������� {0}:{1} Bad Gateway (502).\r\n" +
            "���� ������ �������� �������� Microsoft ISA, ���������� ����������� � �������� Q283284.\r\n" +
            "��������� ������: {2}.", Config.Host, Config.Port, responseHeader.Text);
          break;

        default:
          msg = String.Format("��� ������ ������ ������� {0}:{1} ������: {2}. ��������� ������: {3}.",
            Config.Host, Config.Port, responseHeader.Code, responseHeader.Text);
          break;
      }

      throw new ProxyClientException(msg);
    }
  }
}
