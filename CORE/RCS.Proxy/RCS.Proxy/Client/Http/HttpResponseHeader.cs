namespace RCS.Proxy.Client.Http
{
  /// <summary>
  /// ��������� ������ HTTP Proxy �������.
  /// </summary>
  internal class HttpResponseHeader
  {
    /// <summary>
    /// ��� ������.
    /// </summary>
    public HttpResponseCode Code
    {
      get { return _code; }
    }
    private HttpResponseCode _code;

    /// <summary>
    /// ����� ������.
    /// </summary>
    public string Text
    {
      get { return _text; }
    }
    private string _text;

    private HttpResponseHeader()
    {
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="code">��� ������.</param>
    /// <param name="text">����� ������.</param>
    public HttpResponseHeader(HttpResponseCode code, string text)
    {
      _code = code;
      _text = text;
    }
  }
}