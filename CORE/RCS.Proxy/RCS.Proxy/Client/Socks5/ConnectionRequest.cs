using System;
using RCS.Proxy.Util;

namespace RCS.Proxy.Client.Socks5
{
  /// <summary>
  /// �������-������ ���������� � �������� ������ ��� Socks5.
  /// </summary>
  /// <remarks>
  /// The connection request is made up of 6 bytes plus the
  /// length of the variable address byte array
  ///
  /// <para>  +----+-----+-------+------+----------+----------+  </para>
  /// <para>  |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |  </para>
  /// <para>  +----+-----+-------+------+----------+----------+  </para>
  /// <para>  | 1  |  1  | X'00' |  1   | Variable |    2     |  </para>
  /// <para>  +----+-----+-------+------+----------+----------+  </para>
  ///
  /// <para>VER protocol version: X'05'.</para>
  /// 
  /// <list type="table">
  ///   <listheader><description>CMD</description></listheader>
  ///   <item><term>X'01'</term><description>CONNECT</description></item>
  ///   <item><term>X'02'</term><description>BIND</description></item>
  ///   <item><term>X'03'</term><description>UDP ASSOCIATE</description></item>
  /// </list>
  /// 
  /// <para>RSV RESERVED.</para>
  /// 
  /// <list type="table">
  ///   <listheader><description>ATYP address itemType of following address</description></listheader>
  ///   <item><term>X'01'</term><description>IP V4 address</description></item>
  ///   <item><term>X'03'</term><description>DOMAINNAME</description></item>
  ///   <item><term>X'04'</term><description>IP V6 address</description></item>
  /// </list>
  /// 
  /// <para>DST.ADDR desired destination address.</para>
  /// <para>DST.PORT desired destination port in network octet order.</para>
  /// </remarks>
  public class ConnectionRequest : AbstractMessage
  {
    /// <summary>
    /// ���� �����������.
    /// </summary>
    public readonly string Host;
    /// <summary>
    /// ���� �����������.
    /// </summary>
    public readonly ushort Port;

    private ConnectionRequest() { }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� host.</exception>
    /// <param name="host">���� �����������.</param>
    /// <param name="port">���� �����������.</param>
    public ConnectionRequest(string host, ushort port)
    {
      if (String.IsNullOrEmpty(host))
      {
        throw new ArgumentNullException("host");
      }

      Host = host;
      Port = port;
    }

    /// <summary>
    /// ����������� �������-�������.
    /// </summary>
    /// <exception cref="ProxyClientException"></exception>
    /// <returns>������ ����.</returns>
    public byte[] Encode()
    {
      Socks5Converter addr�onverter = new Socks5Converter();
      AddressType addressType;
      byte[] destAddr = addr�onverter.AddressToBytes(Host, out addressType);
      byte[] destPort = Converter.PortToBytes(Port);
      byte[] request = new byte[4 + destAddr.Length + 2];

      request[0] = VERSION_NUMBER;
      request[1] = (byte)Socks5ClientCommand.Connect;
      request[2] = 0;
      request[3] = (byte)addressType;
      destAddr.CopyTo(request, 4);
      destPort.CopyTo(request, 4 + destAddr.Length);

      return request;
    }
  }
}
