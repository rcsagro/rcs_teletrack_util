
namespace RCS.Proxy.Client.Socks5
{
  /// <summary>
  /// ����� ������� SOCKS 5.
  /// </summary>
  public enum Socks5ServerResponse : byte
  {
    /// <summary>
    /// ������� ��������� �������: 0x00.
    /// </summary>
    Succeeded = 0x00,
    /// <summary>
    /// ������ SOCKS-�������: 0x01.
    /// </summary>
    GeneralSocksServerFailure = 0x01,
    /// <summary>
    /// ���������� ��������� ������� ������: 0x02.
    /// </summary>
    ConnectionNotAllowedByRuleset = 0x02,
    /// <summary>
    /// ���� ����������: 0x03.
    /// </summary>
    NetworkUnreachable = 0x03,
    /// <summary>
    /// ���� ����������: 0x04.
    /// </summary>
    HostUnreachable = 0x04,
    /// <summary>
    /// ����� � ����������: 0x05.
    /// </summary>
    ConnectionRefused = 0x05,
    /// <summary>
    /// ��������� TTL: 0x06.
    /// </summary>
    TtlExpired = 0x06,
    /// <summary>
    /// ������� �� ��������������: 0x07.
    /// </summary>
    CommandNotSupported = 0x07,
    /// <summary>
    /// ��� ������ �� ��������������: 0x08.
    /// </summary>
    AddressTypeNotSupported = 0x08
  }
}
