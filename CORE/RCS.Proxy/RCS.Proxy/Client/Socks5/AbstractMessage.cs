using System;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.Socks5
{
  /// <summary>
  /// ������� ����� ���������.
  /// </summary>
  public abstract class AbstractMessage
  {
    /// <summary>
    /// ����� ������ SOCKS5: 0x05.
    /// </summary>
    public const byte VERSION_NUMBER = 0x05;

    /// <summary>
    /// �������� ������ � ������ �������.
    /// </summary>
    /// <exception cref="ProxyClientException">� ������ proxy-������� �������
    /// ���������������� ������.</exception>
    /// <param name="version">������ � ������ �������.</param>
    protected void CheckResponseVersion(byte version)
    {
      if (version != VERSION_NUMBER)
      {
        throw new ProxyClientException(String.Format(
          "� ������ proxy-������� ������� ���������������� ������ ��������� : {0}. " +
          "��������� ������ {1}.", version, VERSION_NUMBER));
      }
    }
  }
}
