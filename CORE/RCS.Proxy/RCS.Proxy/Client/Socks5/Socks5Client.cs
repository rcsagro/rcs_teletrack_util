using System;
using System.Net.Sockets;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.Socks5
{
  /// <summary>
  /// Proxy SOCKS v5 ������.
  /// </summary>
  public class Socks5Client : ProxyClient
  {
    private AuthenticationMethod _proxyAuthMethod;

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="cfg">��������� ����������� � Proxy-�������.</param>
    public Socks5Client(ProxyCfg cfg)
      : base(cfg)
    {
    }

    /// <summary>
    /// ������� ����������� � ��������� ����� ����� Proxy-������.
    /// </summary>
    /// <returns>TcpClient.</returns>
    protected override TcpClient CreateConnection()
    {
      TcpClient result = new TcpClient();
      result.Connect(ProxyConfig.Host, ProxyConfig.Port);
      NetworkStream proxyStream = result.GetStream();

      DetermineClientAuthMethod();
      try
      {
        DetermineProxyServerAuthMethod(proxyStream);
        Authenticate(proxyStream);
        ConnectToHost(proxyStream);
      }
      catch
      {
        result.Close();
        throw;
      }

      return result;
    }

    /// <summary>
    /// ������������ ������ ��������������, ������� ��������������
    /// ������ ����������� ������� SOCKS 5.
    /// <para>������ �� ����� �������� ��� �������������� ��� �
    /// ��������������� �� ������ � ������.</para>
    /// </summary>
    private void DetermineClientAuthMethod()
    {
      _proxyAuthMethod = String.IsNullOrEmpty(ProxyConfig.Login) ?
        AuthenticationMethod.NoAuthenticationRequired :
        AuthenticationMethod.UsernamePassword;
    }

    /// <summary>
    /// ������������ ������ ��������������, ������� ��������������
    /// Proxy-��������.
    /// </summary>
    /// <param name="proxy">NetworkStream.</param>
    private void DetermineProxyServerAuthMethod(NetworkStream proxy)
    {
      SendAuthenticationMethod(proxy);
      WaitForResponse(proxy);
      HandleAuthenticationMethodResponse(proxy);
    }

    /// <summary>
    /// ��������������.
    /// </summary>
    /// <param name="proxy">NetworkStream.</param>
    private void Authenticate(NetworkStream proxy)
    {
      if (_proxyAuthMethod == AuthenticationMethod.UsernamePassword)
      {
        SendAuthentication(proxy);
        WaitForResponse(proxy);
        HandleAuthenticationResponse(proxy);
      }
    }

    /// <summary>
    /// ����������� � �������� ������.
    /// </summary>
    /// <param name="proxy">NetworkStream.</param>
    private void ConnectToHost(NetworkStream proxy)
    {
      SendConnectToHost(proxy);
      WaitForResponse(proxy);
      HandleConnectToHostResponse(proxy);
    }

    /// <summary>
    /// �������� �������-����������� � ������ ��������������, ������� ��� �������.
    /// </summary>
    /// <param name="proxy">NetworkStream.</param>
    private void SendAuthenticationMethod(NetworkStream proxy)
    {
      byte[] request;
      if (_proxyAuthMethod == AuthenticationMethod.NoAuthenticationRequired)
      {
        request = new AuthenticationMethodRequest(
          new AuthenticationMethod[1] { AuthenticationMethod.NoAuthenticationRequired }
          ).Encode();
      }
      else
      {
        request = new AuthenticationMethodRequest(
          new AuthenticationMethod[2] { 
            AuthenticationMethod.NoAuthenticationRequired,
            AuthenticationMethod.UsernamePassword }
          ).Encode();
      }
      proxy.Write(request, 0, request.Length);
    }

    /// <summary>
    /// ��������� ������ proxy-������� � �������������� ������
    /// ��������������.
    /// </summary>
    /// <param name="proxy"></param>
    private void HandleAuthenticationMethodResponse(NetworkStream proxy)
    {
      byte[] response = new byte[AuthenticationMethodResponse.RESPONSE_LENGTH];
      proxy.Read(response, 0, AuthenticationMethodResponse.RESPONSE_LENGTH);

      AuthenticationMethod acceptedAuthMethod =
        new AuthenticationMethodResponse(response).Decode();

      if (acceptedAuthMethod == AuthenticationMethod.NoAcceptableMethods)
      {
        throw new ProxyClientException(
          "Proxy-������ �� ������������ ������ �������������� �������.");
      }
      if ((acceptedAuthMethod == AuthenticationMethod.UsernamePassword) &&
          (_proxyAuthMethod == AuthenticationMethod.NoAuthenticationRequired))
      {
        throw new ProxyClientException(
          "��� ������� � Proxy-������� ��������� ������� ����� � ������.");
      }

      _proxyAuthMethod = acceptedAuthMethod;
    }

    /// <summary>
    /// �������� ������� ��������������.
    /// </summary>
    /// <param name="proxy">NetworkStream.</param>
    private void SendAuthentication(NetworkStream proxy)
    {
      byte[] request = new AuthenticationRequest(
        ProxyConfig.Login, ProxyConfig.Password).Encode();

      proxy.Write(request, 0, request.Length);
    }

    /// <summary>
    /// ��������� ������ proxy-������� � ���������� ��������� ��������������.
    /// </summary>
    /// <param name="proxy">NetworkStream.</param>
    private void HandleAuthenticationResponse(NetworkStream proxy)
    {
      byte[] response = new byte[2];
      proxy.Read(response, 0, response.Length);

      if (new AuthenticationResponse(response).Decode() != 0)
      {
        throw new ProxyClientException(String.Format(
          "�������������� �� �������� �� proxy-������� {0}:{1} (�����: {2}, ������ {3}). Status: {4}.",
          ProxyConfig.Host, ProxyConfig.Port, ProxyConfig.Login, ProxyConfig.Password, response[1]));
      }
    }

    /// <summary>
    /// �������� ������� ���������� � �������� ������.
    /// </summary>
    /// <param name="proxy">NetworkStream.</param>
    private void SendConnectToHost(NetworkStream proxy)
    {
      byte[] request = new ConnectionRequest(DestinationHost, DestinationPort).Encode();
      proxy.Write(request, 0, request.Length);
    }

    /// <summary>
    /// ��������� ������ proxy-������� �� ������� ���������� � �������� ������.
    /// </summary>
    /// <param name="proxy">NetworkStream.</param>
    private void HandleConnectToHostResponse(NetworkStream proxy)
    {
      byte[] response = new byte[255];
      proxy.Read(response, 0, response.Length);
      Socks5ServerResponse responseCode = new ConnectionResponse(response).Decode();
      if (responseCode != Socks5ServerResponse.Succeeded)
      {
        HandleConnectToHostResponseError(responseCode);
      }
    }

    /// <summary>
    /// ������ ����������� ������ Proxy-�������.
    /// </summary>
    /// <param name="responseCode">��� ������ �������.</param>
    private void HandleConnectToHostResponseError(Socks5ServerResponse responseCode)
    {
      string errorText;

      switch (responseCode)
      {
        case Socks5ServerResponse.GeneralSocksServerFailure:
          errorText = "������ �������";
          break;
        case Socks5ServerResponse.ConnectionNotAllowedByRuleset:
          errorText = "���������� ��������� ��������� ������";
          break;
        case Socks5ServerResponse.NetworkUnreachable:
          errorText = "���� ����������";
          break;
        case Socks5ServerResponse.HostUnreachable:
          errorText = "���� ����������";
          break;
        case Socks5ServerResponse.ConnectionRefused:
          errorText = "����� � ����������";
          break;
        case Socks5ServerResponse.TtlExpired:
          errorText = "����� ����� (TTL) �������";
          break;
        case Socks5ServerResponse.CommandNotSupported:
          errorText = "������� �� ��������������";
          break;
        case Socks5ServerResponse.AddressTypeNotSupported:
          errorText = "��� ������ �� ��������������";
          break;
        default:
          errorText = String.Format(
            "������� ����������� ��� ������ �� proxy-�������: {0}", responseCode);
          break;
      }

      throw new ProxyClientException(String.Format(
        "���������� ����������� � ����� {0}:{1} ����� proxy SOCKS5 ������ {2}:{3} �� �������.\r\n ����� �������: {4}.",
        DestinationHost, DestinationPort, ProxyConfig.Host, ProxyConfig.Port, errorText));
    }
  }
}
