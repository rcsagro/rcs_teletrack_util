
namespace RCS.Proxy.Client.Socks5
{
  /// <summary>
  /// ������� ������� SOCKS 5.
  /// </summary>
  public enum Socks5ClientCommand : byte
  {
    /// <summary>
    /// ��������� TCP-IP ����������: 0x01.
    /// </summary>
    Connect = 0x01,
    /// <summary>
    /// ���������� TCP-IP �����: 0x02.
    /// </summary>
    Bind = 0x02,
    /// <summary>
    /// �������������� UDP-�����: 0x03.
    /// </summary>
    UdpAssociate = 0x03
  }
}
