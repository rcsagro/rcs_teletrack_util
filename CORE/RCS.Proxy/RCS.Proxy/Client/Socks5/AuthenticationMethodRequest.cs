using System;

namespace RCS.Proxy.Client.Socks5
{
  /// <summary>
  /// Socks5 �������-����������� � ������ ��������������, ������� ��� �������.
  /// </summary>
  /// <remarks>
  /// SERVER AUTHENTICATION REQUEST
  /// <para>The client connects to the server, and sends a version
  /// identifier/method selection message:</para>
  ///
  /// <para>  +----+----------+----------+  </para>
  /// <para>  |VER | NMETHODS | METHODS  |  </para>
  /// <para>  +----+----------+----------+  </para>
  /// <para>  | 1  |    1     | 1 to 255 |  </para>
  /// <para>  +----+----------+----------+  </para> 
  /// </remarks>
  public class AuthenticationMethodRequest : AbstractMessage
  {
    /// <summary>
    /// ������ ��������������.
    /// </summary>
    public readonly AuthenticationMethod[] Methods;

    /// <summary>
    /// ���-�� ������� ��������������.
    /// </summary>
    public byte MethodsNumber
    {
      get { return (byte)Methods.Length; }
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������.</exception>
    /// <exception cref="ArgumentOutOfRangeException">������ ������� �� ������ 
    /// ��������� 255.</exception>
    /// <param name="methods">������ ������� ��������������.</param>
    public AuthenticationMethodRequest(AuthenticationMethod[] methods)
    {
      if ((methods == null) || (methods.Length == 0))
      {
        throw new ArgumentNullException("methods");
      }
      if (methods.Length > 255)
      {
        throw new ArgumentOutOfRangeException("methods",
          "������ ������� �� ������ ��������� 255 ���������.");
      }
      Methods = methods;
    }

    /// <summary>
    /// ����������� �������.
    /// </summary>
    /// <returns>������ ����.</returns>
    public byte[] Encode()
    {
      int length = 2 + MethodsNumber;
      byte[] request = new byte[length];
      request[0] = VERSION_NUMBER;
      request[1] = MethodsNumber;

      for (int i = 2; i < length; i++)
      {
        request[i] = (byte)Methods[i - 2];
      }

      return request;
    }
  }
}
