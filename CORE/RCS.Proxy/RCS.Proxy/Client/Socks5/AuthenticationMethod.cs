
namespace RCS.Proxy.Client.Socks5
{
  /// <summary>
  /// ����� ��������������.
  /// </summary>
  public enum AuthenticationMethod : byte
  {
    /// <summary>
    /// �������������� �� ���������: 0x00.
    /// </summary>
    NoAuthenticationRequired = 0x00,
    /// <summary>
    /// �������� �������������� GSS-API: 0x01.
    /// </summary>
    GssApi = 0x01,
    /// <summary>
    /// ��� ������������ � ������: 0x02.
    /// </summary>
    UsernamePassword = 0x02,
    /// <summary>
    /// ������ ���������, ������������� IANA: 0x03.
    /// </summary>
    IanaAssignedRangeBegin = 0x03,
    /// <summary>
    /// ����� ���������, ������������� IANA: 0x7f.
    /// </summary>
    IanaAssignedRangeEnd = 0x7f,
    /// <summary>
    /// ������ ������������������ ���������: 0x80.
    /// </summary>
    ReservedRangeBegin = 0x80,
    /// <summary>
    /// ����� ������������������ ���������: 0xfe.
    /// </summary>
    ReservedRangeEnd = 0xfe,
    /// <summary>
    /// ��� ����������� ������� ��� ������ �������(����� �������): 0xff.
    /// </summary>
    NoAcceptableMethods = 0xff
  }
}
