using System;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.Socks5
{
  /// <summary>
  /// ����� proxy-������� Socks5 � ���������� ��������� ��������������.
  /// </summary>
  /// <remarks>
  /// USERNAME / PASSWORD SERVER RESPONSE
  /// <para>The server verifies the supplied UNAME and PASSWD, and sends the
  /// following response:</para>
  ///
  /// <para>  +----+--------+  </para>
  /// <para>  |VER | STATUS |  </para>
  /// <para>  +----+--------+  </para>
  /// <para>  | 1  |   1    |  </para>
  /// <para>  +----+--------+  </para>
  ///
  /// A STATUS field of X'00' indicates success. If the server returns a
  /// `failure' (STATUS value other than X'00') status, it MUST close the
  /// connection.
  /// 
  /// <para>��������! � ��������� rfc 1929 ���������� ���� - ��� ��������
  /// ����� ��������� ��������� ���� VER. ������� ���� ������� � ������
  /// ��������� �01 (������ Username-Password Authentication), ������ - 
  /// �05 (������ ��������� Socks5), � ����������� �� ��������� ���������
  /// ������������. ������� �������� �������� ���� VER �� ��������������.</para>
  /// </remarks>
  public class AuthenticationResponse
  {
    /// <summary>
    /// ������ ������ = 2 �����. 
    /// </summary>
    public const byte RESPONSE_LENGTH = 2;

    /// <summary>
    /// ����� �������.
    /// </summary>
    public readonly byte[] Response;

    private AuthenticationResponse() { }

    /// <summary>
    /// ����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ����� ����.</exception>
    /// <exception cref="ProxyClientException">������������ ������ �������.</exception>
    /// <param name="response">����� ������� - ������ ���� ������ 2.</param>
    public AuthenticationResponse(byte[] response)
    {
      if (response == null)
      {
        throw new ArgumentNullException("response");
      }
      if (response.Length != RESPONSE_LENGTH)
      {
        throw new ProxyClientException(String.Format(
          "������ ������� ������ ��������� {0} �����", RESPONSE_LENGTH));
      }

      Response = response;
    }

    /// <summary>
    /// ������������� ������.
    /// </summary>
    /// <exception cref="ProxyClientException">������� ���������������� ������ ���������.</exception>
    /// <returns>������ ��������������.</returns>
    public byte Decode()
    {
      return Response[1];
    }
  }
}
