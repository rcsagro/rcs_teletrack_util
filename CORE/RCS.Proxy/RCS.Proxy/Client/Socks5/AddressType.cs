
namespace RCS.Proxy.Client.Socks5
{
  /// <summary>
  /// ��� ������.
  /// </summary>
  public enum AddressType : byte
  {
    /// <summary>
    /// IP version 4: 0x01.
    /// </summary>
    IPv4 = 0x01,
    /// <summary>
    /// �������� ���: 0x03.
    /// </summary>
    DomainName = 0x03,
    /// <summary>
    /// IP version 6: 0x04.
    /// </summary>
    IPv6 = 0x04
  }
}
