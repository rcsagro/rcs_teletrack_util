using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.Socks5
{
  internal class Socks5Converter
  {
    /// <summary>
    /// �������������� ������ ����� � ������ ����.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� host.</exception>
    /// <exception cref="ProxyClientException">��� ������ �� ��������������</exception>
    /// <param name="addressType">��� ������.</param>
    /// <param name="host">�����.</param>
    /// <returns>������ ����.</returns>
    internal byte[] AddressToBytes(string host, out AddressType addressType)
    {
      if (String.IsNullOrEmpty(host))
      {
        throw new ArgumentNullException("host");
      }
      addressType = GetHostAddressType(host);

      switch (addressType)
      {
        case AddressType.IPv4:
        case AddressType.IPv6:
          return IPAddress.Parse(host).GetAddressBytes();
        default: //AddressType.DomainName
          byte[] bytes = new byte[host.Length + 1];
          //  if the address field contains a fully-qualified domain name.  The first
          //  octet of the address field contains the number of octets of name that
          //  follow, there is no terminating NUL octet.
          bytes[0] = Convert.ToByte(host.Length);
          Encoding.ASCII.GetBytes(host).CopyTo(bytes, 1);
          return bytes;
      }
    }

    /// <summary>
    /// ��� ������ ��������� �����.
    /// </summary>
    /// <param name="host">����.</param>
    /// <returns>��� ������.</returns>
    private AddressType GetHostAddressType(string host)
    {
      IPAddress ipAddr;

      bool result = IPAddress.TryParse(host, out ipAddr);

      if (!result)
      {
        return AddressType.DomainName;
      }

      switch (ipAddr.AddressFamily)
      {
        case AddressFamily.InterNetwork:
          return AddressType.IPv4;
        case AddressFamily.InterNetworkV6:
          return AddressType.IPv6;
        default:
          throw new ProxyClientException(String.Format(
            "��� ������ {0} ����� {1} �� ��������������.\r\n" +
            "�������������� ������ InterNetwork (IPv4) � InterNetworkV6 (IPv6).",
            ipAddr.AddressFamily, host));
      }
    }
  }
}
