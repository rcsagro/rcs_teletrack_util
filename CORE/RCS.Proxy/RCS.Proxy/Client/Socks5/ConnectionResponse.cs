using System;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.Socks5
{/// <summary>
  /// ����� Socks5 proxy-������� �� ������� ���������� � �������� ������.
  /// </summary>
  /// <remarks>
  /// PROXY SERVER RESPONSE
  /// <para>  +----+-----+-------+------+----------+----------+  </para>
  /// <para>  |VER | REP |  RSV  | ATYP | BND.ADDR | BND.PORT |  </para>
  /// <para>  +----+-----+-------+------+----------+----------+  </para>
  /// <para>  | 1  |  1  | X'00' |  1   | Variable |    2     |  </para>
  /// <para>  +----+-----+-------+------+----------+----------+  </para>
  ///
  /// <para>VER protocol version: X'05'</para>
  /// 
  /// <list type="table">
  ///   <listheader><description>REP Reply field</description></listheader>
  ///   <item><term>X'00'</term><description>Succeeded</description></item>
  ///   <item><term>X'01'</term><description>General SOCKS server failure</description></item>
  ///   <item><term>X'02'</term><description>Connection not allowed by ruleset</description></item>
  ///   <item><term>X'03'</term><description>Network unreachable</description></item>
  ///   <item><term>X'04'</term><description>Host unreachable</description></item>
  ///   <item><term>X'05'</term><description>Connection refused</description></item>
  ///   <item><term>X'06'</term><description>TTL expired</description></item>
  ///   <item><term>X'07'</term><description>Command not supported</description></item>
  ///   <item><term>X'08'</term><description>Address itemType not supported</description></item>
  ///   <item><term>X'09' to X'FF'</term><description>Unassigned</description></item>
  /// </list>
  ///     
  /// <para>RSV RESERVED</para>
  /// <para>ATYP address itemType of following address</para>
  /// </remarks>
  public class ConnectionResponse : AbstractMessage
  {
    /// <summary>
    /// ����� �������.
    /// </summary>
    public readonly byte[] Response;

    private ConnectionResponse() { }

    /// <summary>
    /// ����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ����� ����.</exception>
    /// <exception cref="ProxyClientException">����� ����������� ������ ������� �� ������������� ���������.</exception>
    /// <param name="response">����� ������� - ������ ���� ������ 2.</param>
    public ConnectionResponse(byte[] response)
    {
      if (response == null)
      {
        throw new ArgumentNullException("response");
      }
      if (response.Length < 2)
      {
        throw new ProxyClientException(
          "����� ����������� ������ ������� �� ������������� ���������.");
      }
      Response = response;
    }

    /// <summary>
    /// ������������� ������.
    /// </summary>
    /// <exception cref="ProxyClientException">������� ���������������� ������ ���������.</exception>
    /// <returns>Socks5ServerResponse</returns>
    public Socks5ServerResponse Decode()
    {
      CheckResponseVersion(Response[0]);
      return (Socks5ServerResponse)Response[1];
    }

    /// <summary>
    /// �������� ������ � ������ �������.
    /// </summary>
    /// <param name="version">������ � ������ �������.</param>
    private void CheckResponseVersion(byte version)
    {
      if (version != VERSION_NUMBER)
      {
        throw new ProxyClientException(String.Format(
          "� ������ proxy-������� ������� ���������������� ������ ��������� : {0}. " +
          "��������� ������ {1}.", version, VERSION_NUMBER));
      }
    }
  }
}
