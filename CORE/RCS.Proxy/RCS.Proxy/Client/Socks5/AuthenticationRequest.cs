using System;
using System.Text;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.Socks5
{
  /// <summary>
  /// ������� �������������� Socks5.
  /// </summary>
  /// <remarks>
  /// USERNAME / PASSWORD SERVER REQUEST
  /// <para>Once the SOCKS V5 server has started, and the client has selected the
  /// Username/Password Authentication protocol, the Username/Password
  /// subnegotiation begins.  This begins with the client producing a
  /// Username/Password request:</para>
  ///
  /// <para>  +----+------+----------+------+----------+  </para>
  /// <para>  |VER | ULEN |  UNAME   | PLEN |  PASSWD  |  </para>
  /// <para>  +----+------+----------+------+----------+  </para>
  /// <para>  | 1  |  1   | 1 to 255 |  1   | 1 to 255 |  </para>
  /// <para>  +----+------+----------+------+----------+  </para>
  /// </remarks>
  public class AuthenticationRequest : AbstractMessage
  {
    /// <summary>
    /// ������������ ����� ������ � ������: 255.
    /// </summary>
    public const byte MAX_LOGIN_PASSWORD_LENGTH = 255;
    /// <summary>
    /// ������ ��������� �������������� �� ����� 
    /// ������������ � ������: 0x01.
    /// </summary>
    public const byte LOGIN_PASSWORD_AUTH_VERSION_NUMBER = 0x01;

    /// <summary>
    /// �����.
    /// </summary>
    public readonly string Login;
    /// <summary>
    /// ������.
    /// </summary>
    public readonly string Password;
    /// <summary>
    /// ����� ������.
    /// </summary>
    public byte LoginLength
    {
      get { return (byte)Login.Length; }
    }
    /// <summary>
    /// ����� ������.
    /// </summary>
    public byte PasswordLength
    {
      get { return (byte)Password.Length; }
    }

    private AuthenticationRequest() { }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� login ��� password.</exception>
    /// <exception cref="ProxyClientException">��������� ���������� ����� ������ ��� ������.</exception>
    /// <param name="login">�����.</param>
    /// <param name="password">������.</param>
    public AuthenticationRequest(string login, string password)
    {
      ValidateLogin(login);
      ValidatePassword(password);

      Login = login;
      Password = password;
    }

    /// <summary>
    /// �������� ������.
    /// </summary>
    /// <param name="login">�����.</param>
    private void ValidateLogin(string login)
    {
      if (String.IsNullOrEmpty(login))
      {
        throw new ArgumentNullException(
          "�� ������� ����� ��� �������������� �� Proxy �������", "login");
      }
      if (login.Length > MAX_LOGIN_PASSWORD_LENGTH)
      {
        throw new ProxyClientException(String.Format(
          "����� ������ �� ����� ���� ����� {0} ��������.",
          MAX_LOGIN_PASSWORD_LENGTH));
      }
    }

    /// <summary>
    /// �������� ������.
    /// </summary>
    /// <param name="password">������.</param>
    private void ValidatePassword(string password)
    {
      if (String.IsNullOrEmpty(password))
      {
        throw new ArgumentNullException(
          "�� ������� ������ ��� �������������� �� Proxy �������", "password");
      }
      if ((password != null) && (password.Length > MAX_LOGIN_PASSWORD_LENGTH))
      {
        throw new ProxyClientException(String.Format(
          "����� ������ �� ����� ���� ����� {0} ��������.",
          MAX_LOGIN_PASSWORD_LENGTH));
      }
    }

    /// <summary>
    /// ����������� �������.
    /// </summary>
    /// <returns>������ ����.</returns>
    public byte[] Encode()
    {
      byte[] request = new byte[LoginLength + PasswordLength + 3];
      request[0] = LOGIN_PASSWORD_AUTH_VERSION_NUMBER;
      request[1] = LoginLength;

      Array.Copy(Encoding.ASCII.GetBytes(Login), 0, request, 2, LoginLength);

      request[LoginLength + 2] = PasswordLength;

      Array.Copy(Encoding.ASCII.GetBytes(Password), 0,
        request, LoginLength + 3, PasswordLength);

      return request;
    }
  }
}
