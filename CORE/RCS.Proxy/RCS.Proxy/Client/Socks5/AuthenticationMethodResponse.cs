using System;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.Socks5
{
  /// <summary>
  /// ����� proxy-������� Socks5 � �������������� ������ ��������������.
  /// </summary>
  /// <remarks>
  /// SERVER AUTHENTICATION RESPONSE
  /// <para>The server selects from one of the methods given in METHODS, and
  /// sends a METHOD selection message:</para>
  ///
  /// <para>  +----+--------+  </para>
  /// <para>  |VER | METHOD |  </para>
  /// <para>  +----+--------+  </para>
  /// <para>  | 1  |   1    |  </para>
  /// <para>  +----+--------+  </para>
  ///
  ///  If the selected METHOD is X'FF', none of the methods listed by the
  ///  client are acceptable, and the client MUST close the connection.
  ///
  /// <list type="table">
  /// <listheader><description>The values currently defined for METHOD are:</description></listheader>
  /// <item><term>X'00'</term><description>NO AUTHENTICATION REQUIRED</description></item>
  /// <item><term>X'01'</term><description>GSSAPI</description></item>
  /// <item><term>X'02'</term><description>USERNAME/PASSWORD</description></item>
  /// <item><term>X'03' to X'7F'</term><description>IANA ASSIGNED</description></item>
  /// <item><term>X'80' to X'FE'</term><description>RESERVED FOR PRIVATE METHODS</description></item>
  /// <item><term>X'FF'</term><description>NO ACCEPTABLE METHODS</description></item>
  /// </list>
  /// </remarks>
  public class AuthenticationMethodResponse : AbstractMessage
  {
    /// <summary>
    /// ������ ������ = 2 �����. 
    /// </summary>
    public const byte RESPONSE_LENGTH = 2;
    /// <summary>
    /// ����� �������.
    /// </summary>
    public readonly byte[] Response;

    private AuthenticationMethodResponse() { }

    /// <summary>
    /// ����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ����� ����.</exception>
    /// <exception cref="ProxyClientException">������������ ������ �������.</exception>
    /// <param name="response">����� ������� - ������ ���� ������ 2.</param>
    public AuthenticationMethodResponse(byte[] response)
    {
      if (response == null)
      {
        throw new ArgumentNullException("response");
      }
      if (response.Length != RESPONSE_LENGTH)
      {
        throw new ProxyClientException(String.Format(
          "������ ������� ������ ��������� {0} �����", RESPONSE_LENGTH));
      }

      Response = response;
    }

    /// <summary>
    /// ������������� ������.
    /// </summary>
    /// <exception cref="ProxyClientException">������ ���������������� �����
    /// �������������� ��� ���������������� ������ ���������.</exception>
    /// <returns>����� ��������������.</returns>
    public AuthenticationMethod Decode()
    {
      CheckResponseVersion(Response[0]);
      
      byte method = Response[1];
      CheckMethod(method);
      return (AuthenticationMethod)method;
    }

    private static void CheckMethod(byte method)
    {
      if (method == (byte)AuthenticationMethod.GssApi)
      {
        throw new ProxyClientException("������ ���������������� ����� �������������� GssApi.");
      }
      if ((method >= (byte)AuthenticationMethod.IanaAssignedRangeBegin) &&
              (method <= (byte)AuthenticationMethod.IanaAssignedRangeEnd))
      {
        throw new ProxyClientException(String.Format(
          "������ ���������������� ����� �������������� � ������ proxy-�������: {0}. " +
          "���� ��� �� ���������, ������������� IANA.", method));
      }
      if ((method >= (byte)AuthenticationMethod.ReservedRangeBegin) &&
        (method <= (byte)AuthenticationMethod.ReservedRangeEnd))
      {
        throw new ProxyClientException(String.Format(
          "������ ���������������� ����� �������������� � ������ proxy-�������: {0}. " +
          "���� ��� �� ������������������ ���������.", method));
      }
    }
  }
}
