using System;
using RCS.Proxy.Exceptions;
using RCS.Proxy.Util;

namespace RCS.Proxy.Client
{
  /// <summary>
  /// ��������� ����������� � Proxy �������.
  /// </summary>
  public class ProxyCfg
  {
    /// <summary>
    /// ����� ������-�������.
    /// </summary>
    public readonly string Host;

    /// <summary>
    /// ���� ������-�������.
    /// </summary>
    public readonly int Port;

    /// <summary>
    /// �����, ���� ���������.
    /// </summary>
    public readonly string Login;

    /// <summary>
    /// ������, ���� ���������.
    /// </summary>
    public readonly string Password;

    /// <summary>
    /// ������� �������� ������ ������-�������, � ��������.
    /// </summary>
    public int AnswerTimeout
    {
      get { return _answerTimeout; }
    }
    private int _answerTimeout;

    /// <summary>
    /// �����������.
    /// </summary>
    private ProxyCfg()
    {
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="host">����� ������-�������.</param>
    /// <param name="port">���� ������-�������.</param>
    /// <param name="login">�����.</param>
    /// <param name="password">������.</param>
    /// <param name="answerTimeout">������� �������� ������ ������-�������, � �������� 1-30.</param>
    public ProxyCfg(string host, int port, string login, string password, int answerTimeout)
    {
      Host = host;
      Port = port;
      Login = login;
      Password = password;
      _answerTimeout = answerTimeout;
      Validate();
    }

    /// <summary>
    /// ��������� ����������� �����.
    /// </summary>
    /// <exception cref="RCS.Proxy.Exceptions.ProxyClientException">
    /// �� ������ ����� ������-������� ��� ����������� ������ ����.</exception>
    private void Validate()
    {
      ValidateHost();
      PortValidator.Validate(Port);
      ValidateAnswerTimeout();
    }

    /// <summary>
    /// ��������� Proxy ������.
    /// </summary>
    private void ValidateHost()
    {
      if (String.IsNullOrEmpty(Host))
      {
        throw new ProxyClientException("����� ������-������� ������ ���� ������.");
      }
    }

    /// <summary>
    /// ��������� Proxy ��������.
    /// </summary>
    private void ValidateAnswerTimeout()
    {
      const int min_timeout_value = 1;
      const int max_timeout_value = 30;

      if (_answerTimeout < min_timeout_value)
      {
        _answerTimeout = min_timeout_value;
      }
      else if (_answerTimeout > max_timeout_value)
      {
        _answerTimeout = max_timeout_value;
      }
    }
  }
}
