using System;
using System.Text;
using RCS.Proxy.Util;

namespace RCS.Proxy.Client.HttpProxy
{
  /// <summary>
  /// ������ �� ����������� ����� Http Proxy ������.
  /// </summary>
  public class HttpProxyConnectionRequest
  {
    /// <summary>
    /// CONNECT {0}:{1} HTTP/1.0 \r\n\r\n
    /// </summary>
    public const string CONNECTION_COMMAND = "CONNECT {0}:{1} HTTP/1.0\r\n\r\n";

    /// <summary>
    /// ������ �� ����������� � ����� � ��������� ����.
    /// </summary>
    public readonly string CommandText;

    private HttpProxyConnectionRequest() { }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <exception cref="RCS.Proxy.Exceptions.ProxyClientException"></exception>
    /// <exception cref="ArgumentNullException">�� ����� ���� ��� �����������.</exception>
    /// <param name="host">�������� ����, � �������� ������ ������������.</param>
    /// <param name="port">���� ��������� �����, � �������� ������ ������������.</param>
    public HttpProxyConnectionRequest(string host, int port)
    {
      if (String.IsNullOrEmpty(host))
      {
        throw new ArgumentNullException("host");
      }
      PortValidator.Validate(port);
      CommandText = String.Format(CONNECTION_COMMAND, host, port);
    }

    /// <summary>
    /// ������ �� ����������� � ����� � ���� ������� ����.
    /// </summary>
    public byte[] Encode()
    {
      return Encoding.ASCII.GetBytes(CommandText);
    }
  }
}
