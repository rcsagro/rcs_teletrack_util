using System;
using System.Text;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.HttpProxy
{
  /// <summary>
  /// ����� Http Proxy �������.
  /// </summary>
  public class HttpProxyResponse
  {
    /// <summary>
    /// ����� ������ Http Proxy �������.
    ///  <remarks>
    /// PROXY SERVER RESPONSE
    /// <code>
    /// HTTP/1.0 200 Connection Established
    /// [.... other HTTP header lines
    /// ignore all of them]
    /// 
    /// </code>
    /// </remarks>
    /// </summary>
    public readonly string Response;

    private HttpProxyResponse() { }

    /// <summary>
    /// ����������� ��� ������������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ����� �������.</exception>
    /// <param name="response">����� ������ Http Proxy �������.</param>
    public HttpProxyResponse(string response)
    {
      if (response == null)
      {
        throw new ArgumentNullException("response");
      }
      Response = response;
    }

    /// <summary>
    /// ��������� HTTP-������ ������ �������.
    /// </summary>
    /// <exception cref="ProxyClientException">�� ������ HTTP-����� �� ������ �������
    /// ��� ������������ ��� ������ �� ������ �������.</exception>
    /// <returns>HttpResponseHeader</returns>
    public HttpResponseHeader DecodeResponseHeader()
    {
      if (Response.Length == 0)
      {
        throw new ProxyClientException("�� ������ HTTP-����� �� ������ �������");
      }
      return ParseServerResponseHeader(GetServerResponseHeader());
    }

    /// <summary>
    /// �������� ��������� ������ ������� - ������ ������.
    /// </summary>
    /// <returns>����� ��������� ������ �������.</returns>
    private string GetServerResponseHeader()
    {
      StringBuilder header = new StringBuilder();
      int index = 0;

      while ((Response[index] != '\n') && (Response[index] != '\r') &&
        (index < Response.Length - 1))
      {
        header.Append(Response[index]);
        index++;
      }
      return header.ToString().Trim();
    }

    /// <summary>
    /// ������� ��������� HTTP-������ ������ �������.
    /// </summary>
    /// <param name="header">��������� HTTP-������ ������ �������.</param>
    /// <returns>HttpResponseHeader.</returns>
    private HttpResponseHeader ParseServerResponseHeader(string header)
    {
      if ((header.Length == 0) || (header.IndexOf("HTTP") == -1))
      {
        throw new ProxyClientException("� ������ ������ ������� �� ��������� HTTP-���������");
      }

      int begin = header.IndexOf(" ") + 1;
      int end = header.IndexOf(" ", begin);

      if ((begin == -1) || (end == -1))
      {
        RaiseExceptionBadResponce(header);
      }

      string val = header.Substring(begin, end - begin);
      Int32 code = 0;

      if (!Int32.TryParse(val, out code))
      {
        RaiseExceptionBadResponce(header);
      }

      return new HttpResponseHeader((HttpResponseCode)code,
        header.Substring(end + 1).Trim());
    }

    /// <summary>
    /// ��������� ���������� ������������ ��� ������.
    /// </summary>
    /// <param name="header">����� ���������</param>
    private static void RaiseExceptionBadResponce(string header)
    {
      throw new ProxyClientException(String.Format(
        "������������ ��� ������ HTTP ������ �������. ��������� ������: {0}.",
        header));
    }
  }
}
