using System;

namespace RCS.Proxy.Client.HttpProxy
{
  /// <summary>
  /// ��������� ������ HTTP Proxy �������.
  /// </summary>
  public class HttpResponseHeader
  {
    /// <summary>
    /// ��� ������.
    /// </summary>
    public HttpResponseCode Code
    {
      get { return _code; }
    }
    private HttpResponseCode _code;

    /// <summary>
    /// ����� ������.
    /// </summary>
    public string Text
    {
      get { return _text; }
    }
    private string _text;

    private HttpResponseHeader()
    {
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� �����.</exception>
    /// <param name="code">��� ������.</param>
    /// <param name="text">����� ������.</param>
    public HttpResponseHeader(HttpResponseCode code, string text)
    {
      if (text == null)
      {
        throw new ArgumentNullException("text");
      }
      _code = code;
      _text = text;
    }
  }
}