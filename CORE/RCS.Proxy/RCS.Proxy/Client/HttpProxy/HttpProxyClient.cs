using System;
using System.Net.Sockets;
using System.Text;
using RCS.Proxy.Client;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.HttpProxy
{
  /// <summary>
  /// HTTP Proxy ������.
  /// </summary>
  public class HttpProxyClient : ProxyClient
  {
    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="cfg">��������� ����������� � Proxy-�������.</param>
    public HttpProxyClient(ProxyCfg cfg)
      : base(cfg)
    {
    }

    /// <summary>
    /// ������� ����������� � ��������� ����� ����� Proxy-������.
    /// </summary>
    /// <returns>TcpClient.</returns>
    protected override TcpClient CreateConnection()
    {
      TcpClient result = new TcpClient();
      result.Connect(ProxyConfig.Host, ProxyConfig.Port);

      NetworkStream networkStream = result.GetStream();
      SendConnectToHost(networkStream);
      WaitForResponse(networkStream);
      HandleResponse(networkStream, result.ReceiveBufferSize);
      return result;
    }

    /// <summary>
    /// �������� ������� Proxy-�������.
    /// </summary>
    /// <param name="proxy">Proxy server data stream.</param>
    protected void SendConnectToHost(NetworkStream proxy)
    {
      byte[] buffer = new HttpProxyConnectionRequest(
        DestinationHost, DestinationPort).Encode();
      proxy.Write(buffer, 0, buffer.Length);
    }

    /// <summary>
    /// ��������� ������ Proxy-�������.
    /// </summary>
    /// <param name="proxy">Proxy server data stream.</param>
    /// <param name="receiveBufferSize">������ ������ ������.</param>
    private void HandleResponse(NetworkStream proxy, int receiveBufferSize)
    {
      byte[] buffer = new byte[receiveBufferSize];
      StringBuilder response = new StringBuilder();
      int bytes = 0;

      do
      {
        bytes = proxy.Read(buffer, 0, receiveBufferSize);
        response.Append(Encoding.UTF8.GetString(buffer, 0, bytes));
      } while (proxy.DataAvailable);

      HttpResponseHeader header;
      try
      {
        header = new HttpProxyResponse(response.ToString()).DecodeResponseHeader();
      }
      catch (ProxyClientException ex)
      {
        throw new ProxyClientException(String.Format(
          "������ ��������� ������ ������ ������� {0}:{1}.\r\n {2}",
          ProxyConfig.Host, ProxyConfig.Port, ex.Message), ex);
      }

      if (header.Code != HttpResponseCode.OK)
      {
        HandleProxyResponseError(header);
      }
    }

    /// <summary>
    /// ��������� ������.
    /// </summary>
    /// <exception cref="ProxyClientException"></exception>
    /// <param name="responseHeader">HttpResponseHeader.</param>
    private void HandleProxyResponseError(HttpResponseHeader responseHeader)
    {
      string msg;

      switch (responseHeader.Code)
      {
        case HttpResponseCode.None:
          msg = String.Format(
            "������ ������ {0}:{1} �� ���� ������� �������������� ��� ������.\r\n ��������� ������: {2}.",
            ProxyConfig.Host, ProxyConfig.Port, responseHeader.Text);
          break;

        case HttpResponseCode.BadGateway:
          // HTTP/1.1 502 Proxy Error 
          // (The specified Secure Sockets Layer (SSL) port is not allowed. 
          // ISA Server is not configured to allow SSL requests from this port. 
          // Most Web browsers use port 443 for SSL requests.)
          msg = String.Format("��� ������ ������ ������� {0}:{1} Bad Gateway (502).\r\n" +
            " ���� ������ �������� �������� Microsoft ISA, ���������� ����������� � �������� Q283284.\r\n" +
            " ��������� ������: {2}.", ProxyConfig.Host, ProxyConfig.Port, responseHeader.Text);
          break;

        default:
          msg = String.Format("��� ������ ������ ������� {0}:{1} ������: {2}.\r\n ��������� ������: {3}.",
            ProxyConfig.Host, ProxyConfig.Port, responseHeader.Code, responseHeader.Text);
          break;
      }

      throw new ProxyClientException(msg);
    }
  }
}
