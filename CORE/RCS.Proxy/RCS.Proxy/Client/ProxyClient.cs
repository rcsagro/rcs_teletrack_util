using System;
using System.Net.Sockets;
using System.Threading;
using RCS.Proxy.Exceptions;
using RCS.Proxy.Util;

namespace RCS.Proxy.Client
{
    /// <summary>
    /// ������� ����� Proxy-��������.
    /// </summary>
    public abstract class ProxyClient : IProxyClient
    {
        /// <summary>
        /// �������� �������� ������: 50 ��.
        /// </summary>
        private const int ANSWER_CHECK_INTERVAL = 50;

        /// <summary>
        /// ������� �������� ������.
        /// </summary>
        private readonly int _answer_timeout;

        /// <summary>
        /// ��������� ����������� � Proxy-�������.
        /// </summary>
        protected internal readonly ProxyCfg ProxyConfig;


        /// <summary>
        /// �������� ����, � �������� ������ ������������.
        /// </summary>
        protected internal string DestinationHost
        {
            get { return _destinationHost; }
        }

        private string _destinationHost;

        /// <summary>
        /// ���� ��������� �����, � �������� ������ ������������.
        /// </summary>
        protected internal ushort DestinationPort
        {
            get { return _destinationPort; }
        }

        private ushort _destinationPort;

        private ProxyClient()
        {
        }

        /// <summary>
        /// �����������.
        /// </summary>
        /// <exception cref="ArgumentNullException">�� �������� ��������� 
        /// ����������� � Proxy-�������.</exception>
        /// <param name="cfg">��������� ����������� � Proxy-�������.</param>
        public ProxyClient(ProxyCfg cfg)
        {
            if (cfg == null)
            {
                throw new ArgumentNullException("cfg");
            }
            ProxyConfig = cfg;
            _destinationHost = String.Empty;
            _destinationPort = 1080;
            _answer_timeout = cfg.AnswerTimeout*1000;
        }

        /// <summary>
        /// ����������� � �����.
        /// </summary>
        /// <exception cref="RCS.Proxy.Exceptions.ProxyClientException"></exception>
        /// <exception cref="ArgumentNullException">�� ����� ���� ��� �����������.</exception>
        /// <param name="destinationHost">�������� ����, � �������� ������ ������������.</param>
        /// <param name="destinationPort">���� ��������� �����, � �������� ������ ������������.</param>
        /// <returns>TcpClient.</returns>
        public TcpClient ConnectToHost(string destinationHost, int destinationPort)
        {
            if (String.IsNullOrEmpty(destinationHost))
            {
                throw new ArgumentNullException("destinationHost");
            }
            PortValidator.Validate(destinationPort);

            _destinationHost = destinationHost;
            _destinationPort = (ushort) destinationPort;
            return CreateConnection();
        }

        /// <summary>
        /// ������� ����������� � ��������� ����� ����� Proxy-������.
        /// </summary>
        /// <exception cref="RCS.Proxy.Exceptions.ProxyClientException"></exception>
        /// <exception cref="NotImplementedException"></exception>
        /// <returns>TcpClient.</returns>
        protected virtual TcpClient CreateConnection()
        {
            throw new NotImplementedException(
                "����� CreateConnection ������ ���� ���������� � �������-�����������.");
        }

        /// <summary>
        /// �������� ������ Proxy-�������. 
        /// </summary>
        /// <param name="stream">NetworkStream.</param>
        protected void WaitForResponse(NetworkStream stream)
        {
            int sleepTime = 0;
            while (!stream.DataAvailable)
            {
                Thread.Sleep(ANSWER_CHECK_INTERVAL);
                sleepTime += ANSWER_CHECK_INTERVAL;

                if (sleepTime > _answer_timeout)
                {
                    throw new ProxyClientException(String.Format(
                        "�������� ������� �������� ������ Proxy-������� {0}:{1}.",
                        ProxyConfig.Host, ProxyConfig.Port));
                }
            }
        }
    }
}
