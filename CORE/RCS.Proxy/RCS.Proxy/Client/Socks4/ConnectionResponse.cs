using System;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.Socks4
{
  /// <summary>
  /// ����� Socks4(4a) proxy-������� �� ������� ���������� � �������� ������.
  /// </summary>
  /// <remarks>
  /// PROXY SERVER RESPONSE
  /// <para>The SOCKS server checks to see whether such a request should be granted
  /// based on any combination of source IP address, destination IP address,
  /// destination port number, the userid, and information it may obtain by
  /// consulting IDENT, cf. RFC 1413.  If the request is granted, the SOCKS
  /// server makes a connection to the specified port of the destination host.
  /// A reply packet is sent to the client when this connection is established,
  /// or when the request is rejected or the operation fails.</para>
  ///
  /// <para>  +----+----+----+----+----+----+----+----+  </para>
  /// <para>  | VN | CD | DSTPORT |      DSTIP        |  </para>
  /// <para>  +----+----+----+----+----+----+----+----+  </para>
  /// <para>  |  1 |  1 |    2    |         4         |  </para>
  /// <para>  +----+----+----+----+----+----+----+----+  </para>
  ///
  /// VN is the version of the reply code and should be 0. CD is the result
  /// code with one of the following values:
  /// <para>  90 or 0x5a: request granted</para>
  /// <para>  91 or 0x5b: request rejected or failed</para>
  /// <para>  92 or 0x5c: request rejected becuase SOCKS server cannot connect to
  ///                     identd on the client</para>
  /// <para>  93 or 0x5d: request rejected because the client program and identd
  ///                     report different user-ids</para>
  ///
  /// The remaining fields (DSTPORT, DSTIP) are ignored.
  /// <para>The SOCKS server closes its connection immediately after notifying
  /// the client of a failed or rejected request. For a successful request,
  /// the SOCKS server gets ready to relay traffic on both directions. This
  /// enables the client to do I/O on its connection as if it were directly
  /// connected to the application server.</para>
  /// </remarks>
  public class ConnectionResponse
  {
    /// <summary>
    /// ����� �������.
    /// </summary>
    public readonly byte[] Response;

    private ConnectionResponse() { }

    /// <summary>
    /// ����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ����� ����.</exception>
    /// <exception cref="ProxyClientException">����� ����������� ������ ������� 
    /// �� ������������� ���������.</exception>
    /// <param name="response">����� ������� - ������ ���� ������ �� ����� 2.</param>
    public ConnectionResponse(byte[] response)
    {
      if (response == null)
      {
        throw new ArgumentNullException("response");
      }
      if (response.Length < 2)
      {
        throw new ProxyClientException(
          "����� ����������� ������ ������� �� ������������� ���������.");
      }
      Response = response;
    }

    /// <summary>
    /// ������������� ������.
    /// </summary>
    /// <exception cref="ProxyClientException">������� ���������������� ������ ������.</exception>
    /// <returns>Socks4ServerResponse</returns>
    public Socks4ServerResponse Decode()
    {
      CheckResponseVersion(Response[0]);
      return (Socks4ServerResponse)Response[1];
    }

    /// <summary>
    /// �������� ������ � ������ �������.
    /// </summary>
    /// <param name="version">������ � ������ �������.</param>
    private void CheckResponseVersion(byte version)
    {
      if (version != 0)
      {
        throw new ProxyClientException(String.Format(
          "� ������ proxy-������� ������� ���������������� ������ ������ : {0}. " +
          "��������� ������ {1}.", version, 0));
      }
    }
  }
}
