
namespace RCS.Proxy.Client.Socks4
{
  /// <summary>
  /// ������� ������� SOCKS 4(4a).
  /// </summary>
  public enum Socks4ClientCommand : byte
  {
    /// <summary>
    /// ������� TCP-IP ����������: 0x01.
    /// </summary>
    Connect = 0x01,
    /// <summary>
    /// ������� ���������� TCP-IP �����: 0x02.
    /// </summary>
    Bind = 0x02
  }
}
