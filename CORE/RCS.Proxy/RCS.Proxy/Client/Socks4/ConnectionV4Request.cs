using System;
using System.Text;
using RCS.Proxy.Util;

namespace RCS.Proxy.Client.Socks4
{
  /// <summary>
  /// ������� ���������� Proxy-������� ������ 4.
  /// </summary>
  /// <remarks>
  /// PROXY SERVER REQUEST
  /// <para>The client connects to the SOCKS server and sends a CONNECT request when
  /// it wants to establish a connection to an application server. The client
  /// includes in the request packet the IP address and the port number of the
  /// destination host, and userid, in the following format.</para>
  /// 
  /// <para>  +----+----+----+----+----+----+----+----+----+----+....+----+  </para>
  /// <para>  | VN | CD | DSTPORT |      DSTIP        | USERID       |NULL|  </para>
  /// <para>  +----+----+----+----+----+----+----+----+----+----+....+----+  </para>
  /// <para>  | 1  | 1  |    2    |         4         | variable     | 1  |  </para>
  /// <para>  +----+----+----+----+----+----+----+----+----+----+....+----+  </para>
  ///
  /// VN is the SOCKS protocol version number and should be 4. CD is the
  /// SOCKS command code and should be 1 for CONNECT request. NULL is a byte
  /// of all zero bits. 
  /// </remarks>
  public class ConnectionV4Request
  {
    /// <summary>
    /// ���� �����������.
    /// </summary>
    public readonly string Host;
    /// <summary>
    /// ���� �����������.
    /// </summary>
    public readonly ushort Port;
    /// <summary>
    /// �����.
    /// </summary>
    public readonly string Login;


    private ConnectionV4Request() { }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� host.</exception>
    /// <param name="host">���� �����������.</param>
    /// <param name="port">���� �����������.</param>
    /// <param name="login">�����.</param>
    public ConnectionV4Request(string host, ushort port, string login)
    {
      if (String.IsNullOrEmpty(host))
      {
        throw new ArgumentNullException("host");
      }

      Host = host;
      Port = port;
      Login = login;
    }

    /// <summary>
    /// ����������� �������.
    /// </summary>
    /// <returns>������ ����.</returns>
    public virtual byte[] Encode()
    {
      int userIdLength = String.IsNullOrEmpty(Login) ? 0 : Login.Length;
      byte[] request = new byte[9 + userIdLength];

      request[0] = Socks4Client.VERSION_NUMBER;
      request[1] = (byte)Socks4ClientCommand.Connect;
      Converter.PortToBytes(Port).CopyTo(request, 2);
      Converter.IpAddressToBytes(Host).CopyTo(request, 4);
      if (userIdLength > 0)
      {
        Encoding.ASCII.GetBytes(Login).CopyTo(request, 8);
      }
      request[8 + userIdLength] = 0x00;
      return request;
    }
  }
}
