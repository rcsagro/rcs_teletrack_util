using System;
using System.Net.Sockets;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.Socks4
{
  /// <summary>
  /// Proxy SOCKS v4 ������.
  /// </summary>
  public class Socks4Client : ProxyClient
  {
    /// <summary>
    /// ����� ������ SOCKS4: 0x04.
    /// </summary>
    public const byte VERSION_NUMBER = 0x04;

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="cfg">��������� ����������� � Proxy-�������.</param>
    public Socks4Client(ProxyCfg cfg)
      : base(cfg)
    {
    }

    /// <summary>
    /// ������� ����������� � ��������� ����� ����� Proxy-������.
    /// </summary>
    /// <returns>TcpClient.</returns>
    protected override TcpClient CreateConnection()
    {
      TcpClient result = new TcpClient();
      result.Connect(ProxyConfig.Host, ProxyConfig.Port);

      NetworkStream networkStream = result.GetStream();
      SendConnectToHost(networkStream);
      WaitForResponse(networkStream);
      HandleResponse(networkStream);
      return result;
    }

    /// <summary>
    /// �������� ������� Proxy-�������.
    /// </summary>
    /// <param name="proxy">Proxy server data stream.</param>
    protected virtual void SendConnectToHost(NetworkStream proxy)
    {
      byte[] request = new ConnectionV4Request(
        DestinationHost, DestinationPort, ProxyConfig.Login).Encode();
      proxy.Write(request, 0, request.Length);
    }

    /// <summary>
    /// ��������� ������ Proxy-�������.
    /// </summary>
    private void HandleResponse(NetworkStream proxy)
    {
      byte[] response = new byte[8];
      proxy.Read(response, 0, 8);

      Socks4ServerResponse responseCode = new ConnectionResponse(response).Decode();

      if (responseCode != Socks4ServerResponse.RequestGranted)
      {
        HandleProxyResponseError(responseCode);
      }
    }

    /// <summary>
    /// ������ ����������� ������ Proxy-�������.
    /// </summary>
    /// <param name="responseCode">��� ������ Proxy-�������.</param>
    private void HandleProxyResponseError(Socks4ServerResponse responseCode)
    {
      string errorText;
      switch (responseCode)
      {
        case Socks4ServerResponse.RequestRejectedOrFailed:
          errorText = "������ �������� ��� ��������";
          break;
        case Socks4ServerResponse.RequestFailedCannotConnectToIdentd:
          errorText = "������ �� ������ - �� ������� Identd ������";
          break;
        case Socks4ServerResponse.RequestRejectedDifferentUserId:
          errorText = "������ �������� - ��������� � ������� user-id �� ����������� Identd ��������";
          break;
        default:
          errorText = String.Format(
            "������� ����������� ��� ������ �� proxy-�������: {0}", responseCode);
          break;
      }

      throw new ProxyClientException(String.Format(
        "���������� ����������� � ����� {0}:{1} ����� proxy SOCKS4 ������ {2}:{3} �� �������.\r\n ����� �������: {4}.",
        DestinationHost, DestinationPort, ProxyConfig.Host, ProxyConfig.Port, errorText));
    }
  }
}
