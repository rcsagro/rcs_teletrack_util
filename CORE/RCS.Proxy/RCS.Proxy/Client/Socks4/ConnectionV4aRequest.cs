using System;
using System.Text;
using RCS.Proxy.Util;

namespace RCS.Proxy.Client.Socks4
{
  /// <summary>
  /// ������� ���������� Proxy-������� ������ 4a.
  /// </summary>
  /// <remarks>
  /// PROXY SERVER REQUEST
  /// <para>Please read SOCKS4.protocol first for an description of the version 4
  /// protocol. This extension is intended to allow the use of SOCKS on hosts
  /// which are not capable of resolving all domain names.</para>
  /// 
  /// <para>For version 4A, if the client cannot resolve the destination host's
  /// domain name to find its IP address, it should set the first three bytes
  /// of DSTIP to NULL and the last byte to a non-zero value. (This corresponds
  /// to IP address 0.0.0.x, with x nonzero. As decreed by IANA  -- The
  /// Internet Assigned Numbers Authority -- such an address is inadmissible
  /// as a destination IP address and thus should never occur if the client
  /// can resolve the domain name.) Following the NULL byte terminating
  /// USERID, the client must sends the destination domain name and termiantes
  /// it with another NULL byte. This is used for both CONNECT and BIND requests.</para> 
  /// 
  /// <para>A server using protocol 4A must check the DSTIP in the request packet.
  /// If it represent address 0.0.0.x with nonzero x, the server must read
  /// in the domain name that the client sends in the packet. The server
  /// should resolve the domain name and make connection to the destination
  /// host if it can. </para>
  /// </remarks>
  public class ConnectionV4aRequest : ConnectionV4Request
  {
    /// <summary>
    /// �����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� host.</exception>
    /// <param name="host">���� �����������.</param>
    /// <param name="port">���� �����������.</param>
    /// <param name="login">�����.</param>
    public ConnectionV4aRequest(string host, ushort port, string login)
      : base(host, port, login)
    {
    }

    /// <summary>
    /// ����������� �������.
    /// </summary>
    /// <returns>������ ����.</returns>
    public override byte[] Encode()
    {
      int userIdLength = String.IsNullOrEmpty(Login) ? 0 : Login.Length;
      // ���������� ip-�����, ��� ������� � ������������ ��������� ������ 4a
      byte[] destIp = { 0, 0, 0, 1 };
      byte[] hostBytes = Encoding.ASCII.GetBytes(Host);
      byte[] request = new byte[10 + userIdLength + hostBytes.Length];

      request[0] = Socks4Client.VERSION_NUMBER;
      request[1] = (byte)Socks4ClientCommand.Connect;
      Converter.PortToBytes(Port).CopyTo(request, 2);
      destIp.CopyTo(request, 4);

      if (userIdLength > 0)
      {
        Encoding.ASCII.GetBytes(Login).CopyTo(request, 8);
      }
      // null ��������� userId
      request[8 + userIdLength] = 0x00;

      // ��� �����
      hostBytes.CopyTo(request, 9 + userIdLength);
      // null ��������� ��� �����
      request[9 + userIdLength + hostBytes.Length] = 0x00;

      return request;
    }
  }
}
