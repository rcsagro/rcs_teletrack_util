
namespace RCS.Proxy.Client.Socks4
{
  /// <summary>
  /// ����� ������� SOCKS 4(4a).
  /// </summary>
  public enum Socks4ServerResponse : byte
  {
    /// <summary>
    /// ������ ������������: 0x5A.
    /// </summary>
    RequestGranted = 0x5A,
    /// <summary>
    /// ������ �������� ��� ��������: 0x5B.
    /// </summary>
    RequestRejectedOrFailed = 0x5B,
    /// <summary>
    /// ������ �� ������ - �� ������� Identd ������: 0x5C.
    /// </summary>
    RequestFailedCannotConnectToIdentd = 0x5C,
    /// <summary>
    /// ������ �������� - ��������� � ������� user-id 
    /// �� ����������� Identd ��������: 0x5D.
    /// </summary>
    RequestRejectedDifferentUserId = 0x5D
  }
}
