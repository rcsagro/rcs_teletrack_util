using System.Net.Sockets;
using RCS.Proxy.Exceptions;

namespace RCS.Proxy.Client.Socks4
{
  /// <summary>
  /// Proxy SOCKS v4a ������.
  /// </summary>
  public class Socks4aClient : Socks4Client
  {
    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="cfg">��������� ����������� � Proxy-�������.</param>
    public Socks4aClient(ProxyCfg cfg)
      : base(cfg)
    {
    }

    /// <summary>
    /// �������� ������� Proxy-�������.
    /// </summary>
    /// <param name="proxy">Proxy server data stream.</param>
    protected override void SendConnectToHost(NetworkStream proxy)
    {
      try
      {
        base.SendConnectToHost(proxy);
      }
      catch (ProxyClientException)
      {
        // ��� ������ ���������� ��� ������������� ��������� ���������
        // ��������� �����, IP-������. 
        SendConnectToHost4a(proxy);
      }
    }

    /// <summary>
    /// �������� ������� Proxy-������� ��������� ������ ��������� 4a.
    /// </summary>
    /// <param name="proxy">Proxy server data stream.</param>
    private void SendConnectToHost4a(NetworkStream proxy)
    {
      byte[] request = new ConnectionV4aRequest(DestinationHost,
        DestinationPort, ProxyConfig.Login).Encode();
      proxy.Write(request, 0, request.Length);
    }
  }
}
