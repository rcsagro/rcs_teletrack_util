using System;
using TransitServisLib;

namespace RCS.Trans
{
  public class ConteinerIncData
  {
    public ConteinerIncData(object obj, IPacket incData)
    {
      sender = obj;// as ConnectionInfo;
      packet = incData;
    }
    public IPacket packet;
    public object sender;
  }
}
