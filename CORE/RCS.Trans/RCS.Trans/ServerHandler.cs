using System;
using System.Collections.Generic;
using RCS.Sockets.Connection;
using RCS.Sockets;

namespace RCS.Trans
{
  public class ServerHandler
  {
    protected List<ConnectionInfo> taskList = new List<ConnectionInfo>();

    protected ServerSocket sockServerClass;

    /// <summary>
    /// ����� ��������� ������ �������
    /// </summary>
    public bool ServerStart(string address, int sock_port, Type type, bool isKeepAliveOn)
    {
      bool result = false;
      if (sockServerClass.SetupServerSocket(address, sock_port))
      {
        if (isKeepAliveOn)
        {
          sockServerClass.KeepAliveOn();
        }
        sockServerClass.AsynchronousAcceptConnections(type);
        result = true;
      }
      return result;
    }

    /// <summary>
    /// ����� ��������� ������ �������
    /// </summary>
    /// <param name="address"></param>
    /// <param name="sock_port"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    public bool ServerStart(string address, int sock_port, Type type)
    {
      bool result = false;
      if (sockServerClass.SetupServerSocket(address, sock_port))
      {
        sockServerClass.AsynchronousAcceptConnections(type);
        sockServerClass.bEndOfAcceptConn = false;//��������� ������ Accept ��������� �������� ����������
        result = true;
      }
      return result;
    }

    public void ServerStop()
    {
      sockServerClass.bEndOfAcceptConn = true;//��������� ������ Accept ��������� �������� ����������
      sockServerClass.CloseServer();
    }

    public void CloseSocket(ConnectionInfo connection)
    {
      sockServerClass.CloseSocket(connection);
    }

    /// <summary>
    /// �������� ������
    /// </summary>
    /// <param name="connect">���� ����������</param>
    /// <param name="data">������</param>
    /// <returns>����� ������������� ������</returns>
    public void SendData(ConnectionInfo connect, byte[] data)
    {
      sockServerClass.Send(connect, data);
    }
  }
}
