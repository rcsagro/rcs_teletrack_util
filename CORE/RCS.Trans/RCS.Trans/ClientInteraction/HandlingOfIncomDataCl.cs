using System;
using RCS.Protocol.Online;
using RCS.Sockets;
using RCS.Sockets.Connection;
using TransitServisLib;

namespace RCS.Trans.ClientInteraction
{
  public class HandlingOfIncomDataCl : ServerHandler
  {
    public event IncomNewPacket packFromClients;
    public event ConnectedStateEventHandler �onnectedState;
    /// <summary>
    /// �������: ������ �� ������������ ������
    /// </summary>
    public event MessageErrorEventHandler errorOfLevel;

    public HandlingOfIncomDataCl()
    {
      sockServerClass = new ServerSocket();
      sockServerClass.IncomMessage += new IncomingMessageEventHandler(EnqueueToParser);
      sockServerClass.�onnectedStateMsg += new ConnectedStateEventHandler(ConnStateMsg);
      sockServerClass.messageError += new MessageErrorEventHandler(ErrorFromTrLevel);
    }

    private void ErrorFromTrLevel(object sender, MessageErrorEventArgs e)
    {
      if (errorOfLevel != null)
      {
        errorOfLevel(sender, e);
      }
    }

    /// <summary>
    /// �������� �� ������� ���� ��������� ����������
    /// </summary>
    /// <param name="con_state"></param>
    private void ConnStateMsg(ConnectedStateEvArg con_state)
    {
      if (�onnectedState != null)
      {
        �onnectedState(con_state);
      }
    }

    private void OnPackFromClients(ConnectionInfo sender, IPacket pk)
    {
      if (packFromClients != null)
      {
        packFromClients(sender, pk);
      }
    }

    /// <summary>
    /// ���������� � ������� �� ���������
    /// (�������������� ��� �������)
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="connection"></param>
    private void EnqueueToParser(object obj, ConnectionInfo connection)
    {
      IncomToParser(connection);
    }

    /// <summary>
    /// ���������� �����, ���������� �� ������
    /// </summary>
    /// <param name="conn">����� ����������</param>
    private void IncomToParser(ConnectionInfo conn)
    {
      byte[] task;
      do
      {
        int header_pos = 0;
        task = conn.DeQueueIncomPack();

        if (task != null)
        {
          if (conn.incompl_pack.Len > 0)
          {
            byte[] bytes = conn.incompl_pack.AddHeadOfPacket(task);
            ExtractPacket(conn, bytes, header_pos);
          }
          else
          {
            ExtractPacket(conn, task, header_pos);
          }
        }
        else
        {
          taskList.Remove(conn);
        }
      }
      while (task != null);
    }

    /// <summary>
    /// ������ ���������
    /// </summary>
    /// <param name="conn">�� ���� �������� ������</param>
    /// <param name="data">������</param>
    /// <param name="pos">������� � ���. ����� ������ ������ ������</param>
    private void ExtractPacket(ConnectionInfo conn, byte[] data, int pos)
    {
      int header_pos = pos;
      IPacket iptr = Packet.PacketIdentifyNew(data, ref header_pos);
      if (iptr != null)
      {
        HandlingOfPack(iptr, conn, data, header_pos);
      }
    }

    protected virtual void HandlingOfPack(IPacket pk, ConnectionInfo conn, byte[] bytes, int header_pos)
    {
      int pack_size = pk.DisassemblePacket(bytes, header_pos);

      if (pack_size != (-1))
      {
        int cur_pos = pack_size + header_pos;
        if (cur_pos <= bytes.Length)//����� ����
        {
          pk.PacketSize = pack_size;//��� ����������
          OnPackFromClients(conn, pk);

          if ((cur_pos + 8) < bytes.Length)
          {
            ExtractPacket(conn, bytes, cur_pos);
          }
          else if (cur_pos != bytes.Length)
          {
            //������� ������ 8 ���� -> ���������� ���������� ������ ������
            conn.incompl_pack.PutBufIncompletePack(bytes, header_pos);
          }
        }
        else
        {
          conn.incompl_pack.PutBufIncompletePack(bytes, header_pos);
        }
      }
      else
      {
        throw (new Exception("������ ��������� � DisassemblePacket"));  // ��������� Exception �� ������ ������
      }
    }
  }
}
