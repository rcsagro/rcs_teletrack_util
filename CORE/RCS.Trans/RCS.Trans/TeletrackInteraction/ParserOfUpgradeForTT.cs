using System;
using System.Collections.Generic;
using System.Text;

namespace RCS.Trans.TeletrackInteraction
{
  public class ParserOfUpgradeForTT
  {
    public static List<byte[]> StrippingOfUpg(byte[] upg)
    {
      List<byte[]> lst = new List<byte[]>();
      string str = Encoding.ASCII.GetString(upg);
      string[] stringSeparators = new string[] { "\r\n" };
      string[] strBlock = str.Split(stringSeparators, StringSplitOptions.None);

      byte[] rez = null;
      for (int i = 0; i < strBlock.Length; i++)//����� �������
      {
        string sblock = strBlock[i];
        switch (sblock)
        {
          case "VER":
            {
              sblock += "\r\n" + strBlock[i + 1] + "\r\n";
              rez = Encoding.ASCII.GetBytes(sblock);
              //rezLength += rez.Length;
            }
            break;
          case "RESET":
            {
              sblock += "\r\n";
              rez = Encoding.ASCII.GetBytes(sblock);
            }
            break;
          case "DATA":
            {
              //CRCControl.CountCRC(data[j]);

              byte[] data = new byte[(strBlock[i + 1].Length) / 2];

              int pos = 0;
              for (int j = 0; j < data.Length; j++)
              {
                data[j] = Convert.ToByte(strBlock[i + 1].Substring(pos, 2), 16);

                pos += 2;
              }
              CRCControl.CountCRC(data, data.Length);
              sblock += "\r\n";
              byte[] tmp = Encoding.ASCII.GetBytes(sblock);
              byte[] endOfBlock = Encoding.ASCII.GetBytes("\r\n");
              rez = new byte[tmp.Length + data.Length + endOfBlock.Length];
              Array.Copy(tmp, 0, rez, 0, tmp.Length);
              Array.Copy(data, 0, rez, tmp.Length, data.Length);
              Array.Copy(endOfBlock, 0, rez, (tmp.Length + data.Length), endOfBlock.Length);
            }
            break;
        }
        lst.Add(rez);
        i++;
      }
      return lst;
    }

    /*
    public static List<byte[]> StrippingOfUpg(byte[] upg)
    {
        List<byte[]> lst = new List<byte[]>();
        string str = Encoding.ASCII.GetString(upg);
        string[] stringSeparators = new string[] { "\r\n" };
        string[] strBlock = str.Split(stringSeparators, StringSplitOptions.None);

        for (int i = 0; i < strBlock.Length; i++)//����� �������
        {
            string s = strBlock[i];
            if (s != "VER")
            {

                byte[] data = new byte[strBlock[i + 1].Length];
                if ((i != strBlock.Length - 1) || s != "RESET\r\n")
                {
                    int pos = 0;
                    for (int j = 0; j < strBlock[i + 1].Length - 1; j += 2)
                    {
                        data[j] = Convert.ToByte(strBlock[i + 1].Substring(pos, 2), 16);
                        pos += 2;
                    }
                }
                s += "\r\n";
                byte[] tmp = Encoding.ASCII.GetBytes(s);
                byte[] endOfBlock = Encoding.ASCII.GetBytes("\r\n");
                byte[] rez = new byte[tmp.Length + data.Length + endOfBlock.Length];
                Array.Copy(tmp, 0, rez, 0, tmp.Length);
                Array.Copy(data, 0, rez, tmp.Length, data.Length);
                Array.Copy(endOfBlock, 0, rez, (tmp.Length + data.Length), endOfBlock.Length);
            }
            else
                s += "\r\n" + strBlock[i + 1] + "\r\n";

            rez =
            lst.Add(Encoding.ASCII.GetBytes(s));
            i++;
        }
        return lst;
    }
    */
  }
}
