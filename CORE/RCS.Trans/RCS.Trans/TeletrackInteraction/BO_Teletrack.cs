using System.Collections.Generic;
using RCS.Sockets.Connection;

namespace RCS.Trans.TeletrackInteraction
{
  public class BO_Teletrack : ConnectionInfo
  {
    protected object locker = new object();

    #region �������� ���������

    /// <summary>
    /// ID_Teletrack � ���� �������
    /// </summary>
    public int ID_Teletrack
    {
      set { id_teletrack = value; }
      get { return id_teletrack; }
    }
    int id_teletrack = 0;

    public string LoginTT
    {
      set { loginTT = value; }
      get { return loginTT; }
    }
    string loginTT = "";

    public string PasswordTT
    {
      set { passwordTT = value; }
      get { return passwordTT; }
    }
    string passwordTT = "";

    /*public DateTime DTimeLastPack
    {
      get { return dTimeLastPack; }
      set { dTimeLastPack = value; }
    }
    DateTime dTimeLastPack = DateTime.Now;*/


    /// <summary>
    /// � ���� (����� ������������ ������������� � false) 
    /// </summary>
    public bool InNetwork
    {
      set { inNetwork = value; }
      get { return inNetwork; }
    }
    public bool inNetwork;

    /// <summary>
    /// ������ id ������������� ������
    /// </summary>
    public List<int> lstCmd = new List<int>();

    /// <summary>
    /// ������ id ���������� ���������
    /// </summary>
    public List<int> lst_owners = new List<int>();

    /// <summary>
    /// �������� ����� ������ ����� ������ �� ���������
    /// </summary>
    public List<byte[]> upgradeTT = new List<byte[]>();

    /// <summary>
    /// ���������� ������� �������� ����� ������
    /// ��� �������� ������ �� ���������
    /// </summary>
    public int AttemptToUpgradeTT
    {
      get { return attemptToUpgradeTT -= 1; }
      set { attemptToUpgradeTT = value; }
    }
    /// <summary>
    /// ���������� ������� �������� ����� ������
    /// ��� �������� ������ �� ���������
    /// </summary>
    private int attemptToUpgradeTT;

    public void AddNewCmdToList(int idCmd)
    {
      lock (locker)
      {
        lstCmd.Add(idCmd);
      }
    }

    #endregion

    #region ������ ��� �������� �� ���������� ������ �����

    public int ID_Session
    {
      get { return id_session; }
      set { id_session = value; }
    }
    int id_session = 0;


    /// <summary>
    /// ����� A1, ������������ �� ���������� ����
    /// </summary>
    public LastSendingPacket lastOutPacketA1 = null;

    #endregion
  }

  /// <summary>
  /// �����, ������������ �� ���������� ����
  /// </summary>
  public class LastSendingPacket
  {
    public LastSendingPacket() { }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="id">id ������� � �� ������� (��� SH - � �� �������)</param>
    /// <param name="str">��� ������� (�1 ��� U)</param>
    /// <param name="fromClient">id �������</param>
    public LastSendingPacket(int id, string str, int fromClient)
    {
      id_cmd = id;
      cmdType = str;
      id_client = fromClient;
    }
    public int id_cmd = 0;
    public string cmdType = "";
    public int id_client = 0;

  }
}
