using System;
using System.Text;
using TransitServisLib;

namespace RCS.Protocol.Online
{
  public class PacketSendQuery : Packet
  {
    /// <summary>
    /// ����� ���������
    /// </summary>
    public string LoginTT
    {
      get { return loginTT; }
      set { loginTT = value; }
    }
    string loginTT;

    /// <summary>
    /// �������� ��������� param1
    /// </summary>
    public long Param1
    {
      get { return param1; }
      set { param1 = value; }
    }
    /// <summary>
    /// ������ �������� ������� (id ������ ������� � ��������....)
    /// </summary>
    long param1;

    /// <summary>
    /// �������� ��������� param2
    /// </summary>
    public long Param2
    {
      get { return param2; }
      set { param2 = value; }
    }
    /// <summary>
    /// ������ �������� ������� (id ������ �� �������...)
    /// </summary>
    long param2;

    /// <summary>
    /// 28 ���� - 3 �������� �� 4 ����� � 2 �� 8.
    /// </summary>
    private static readonly int _packetLength = ATR_LENGTH * 7;

    public static byte[] AssemblePacket(string teletr, long param1, long param2, int message_id)
    {
      byte[] bytes = new byte[_packetLength * 7];
      //��������� �����
      PACK_SN_QU.CopyTo(bytes, 0);
      BitConverter.GetBytes(message_id).CopyTo(bytes, ATR_LENGTH);
      Encoding.ASCII.GetBytes(teletr).CopyTo(bytes, ATR_LENGTH * 2);
      BitConverter.GetBytes(param1).CopyTo(bytes, ATR_LENGTH * 3);
      BitConverter.GetBytes(param2).CopyTo(bytes, ATR_LENGTH * 5);

      return bytes;
    }

    public override byte[] AssemblePacket()
    {
      byte[] bytes = new byte[_packetLength * 7];
      //��������� �����
      PACK_SN_QU.CopyTo(bytes, 0);
      BitConverter.GetBytes(message_id).CopyTo(bytes, ATR_LENGTH);
      Encoding.ASCII.GetBytes(loginTT).CopyTo(bytes, ATR_LENGTH * 2);
      BitConverter.GetBytes(param1).CopyTo(bytes, ATR_LENGTH * 3);
      BitConverter.GetBytes(param2).CopyTo(bytes, ATR_LENGTH * 5);

      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      try
      {
        if (header_pos + _packetLength <= incomPack.Length)
        {
          MessageID = BitConverter.ToInt32(incomPack, ATR_LENGTH + header_pos);
          loginTT = Encoding.ASCII.GetString(incomPack, (ATR_LENGTH * 2) + header_pos, ATR_LENGTH);
          Param1 = BitConverter.ToInt64(incomPack, (ATR_LENGTH * 3) + header_pos);
          Param2 = BitConverter.ToInt64(incomPack, (ATR_LENGTH * 5) + header_pos);
        }

        return _packetLength;
      }
      catch (Exception ex)
      {
        throw new PacketDisassemblingException(String.Format(
          "PacketSendQuery.\r\n {0}.", ex.Message));
      }
    }
  }
}
