using System;
using TransitServisLib;

namespace RCS.Protocol.Online
{
  public class PacketAckExCl : Packet
  {
    /// <summary>
    /// ��� ������ (��� ������� AckEx)
    /// </summary>
    public int ConState
    {
      get { return con_state; }
      set { con_state = value; }
    }
    int con_state;

    /// <summary>
    /// id ���������� ������ ����������� �� ������� 
    /// (������������� �� ������� �������)
    /// </summary>
    public int MessageID_In
    {
      get { return message_id_in; }
      set { message_id_in = value; }
    }
    int message_id_in;

    /// <summary>
    /// 16 ����.
    /// </summary>
    private static readonly int _packetLength = ATR_LENGTH * 4;

    public byte[] AssemblePacket(int con_state, int message_id_in, int message_id)
    {
      byte[] bytes = new byte[_packetLength];
      //��������� �����
      ACK_EX_CL.CopyTo(bytes, 0);
      BitConverter.GetBytes(con_state).CopyTo(bytes, ATR_LENGTH);
      BitConverter.GetBytes(message_id_in).CopyTo(bytes, ATR_LENGTH * 2);
      BitConverter.GetBytes(message_id).CopyTo(bytes, ATR_LENGTH * 3);

      return bytes;
    }

    public override byte[] AssemblePacket()
    {
      byte[] bytes = new byte[_packetLength];
      //��������� �����
      ACK_EX_CL.CopyTo(bytes, 0);
      BitConverter.GetBytes(con_state).CopyTo(bytes, ATR_LENGTH);
      BitConverter.GetBytes(message_id_in).CopyTo(bytes, ATR_LENGTH * 2);
      BitConverter.GetBytes(message_id).CopyTo(bytes, ATR_LENGTH * 3);

      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      try
      {
        //������ ������ AckExtendCl = 16 ����
        if (header_pos + _packetLength <= incomPack.Length)
        {
          ConState = BitConverter.ToInt32(incomPack, ATR_LENGTH + header_pos);
          MessageID_In = BitConverter.ToInt32(incomPack, (ATR_LENGTH * 2) + header_pos);
          MessageID = BitConverter.ToInt32(incomPack, (ATR_LENGTH * 3) + header_pos);
        }

        return _packetLength;
      }
      catch (Exception ex)
      {
        throw new PacketDisassemblingException(String.Format(
          "PacketAckExCl.\r\n {0}.", ex.Message));
      }
    }
  }
}
