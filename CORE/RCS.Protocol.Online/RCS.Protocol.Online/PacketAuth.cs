using System;
using System.Text;
using TransitServisLib;

namespace RCS.Protocol.Online
{
  public class PacketAuth : Packet
  {
    public PacketAuth()
    {
    }

    public PacketAuth(string log, string pass)
    {
      login = log;
      password = pass;
    }

    /// <summary>
    /// �������� ��� (�����)
    /// </summary>
    public string Login
    {
      get { return login; }
      set { login = value; }
    }
    string login;

    /// <summary>
    /// ������ ID_Packet, ����������� ��� � �� �������
    /// </summary>
    public string Password
    {
      get { return password; }
      set { password = value; }
    }
    string password;

    /// <summary>
    /// �������� ����� Auth
    /// </summary>
    /// <param name="login"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public byte[] AssemblePacket(string login, string pass)
    {
      byte[] log = Encoding.ASCII.GetBytes(login);
      //������� NUL_SIMB
      Array.Resize(ref log, log.Length + NUL_SIMB.Length);
      byte[] pas = Encoding.ASCII.GetBytes(pass);
      Array.Resize(ref pas, pas.Length + NUL_SIMB.Length);
      byte[] bytes = new byte[ATR_LENGTH * 2 + log.Length + pas.Length];
      //��������� �����
      PACK_AUTH.CopyTo(bytes, 0);
      BitConverter.GetBytes(bytes.Length).CopyTo(bytes, ATR_LENGTH);
      log.CopyTo(bytes, ATR_LENGTH * 2);
      pas.CopyTo(bytes, (ATR_LENGTH * 2 + log.Length));

      return bytes;
    }

    public override byte[] AssemblePacket()
    {
      byte[] log = Encoding.ASCII.GetBytes(login);
      //������� NUL_SIMB
      Array.Resize(ref log, log.Length + NUL_SIMB.Length);
      byte[] pas = Encoding.ASCII.GetBytes(password);
      Array.Resize(ref pas, pas.Length + NUL_SIMB.Length);
      byte[] bytes = new byte[ATR_LENGTH * 2 + log.Length + pas.Length];
      //��������� �����
      PACK_AUTH.CopyTo(bytes, 0);
      BitConverter.GetBytes(bytes.Length).CopyTo(bytes, ATR_LENGTH);
      log.CopyTo(bytes, ATR_LENGTH * 2);
      pas.CopyTo(bytes, (ATR_LENGTH * 2 + log.Length));

      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      try
      {
        //header_pos - �������� �� ������ incomPack �� �������� ������� ������
        int pac_size = BitConverter.ToInt32(incomPack, ATR_LENGTH + header_pos);

        if (pac_size <= incomPack.Length)
        {
          //pos1 ������� NUL_SIMB ����� ������
          int pos1 = FindBytes(incomPack, NUL_SIMB, (ATR_LENGTH * 2) + header_pos);
          login = Encoding.ASCII.GetString(incomPack,
            (ATR_LENGTH * 2) + header_pos,
            (pos1 - (ATR_LENGTH * 2) + header_pos));
          //pos2 ������� NUL_SIMB ������� ������
          int pos2 = FindBytes(incomPack, NUL_SIMB, (pos1 + NUL_SIMB.Length) + header_pos);
          password = Encoding.ASCII.GetString(incomPack,
            (pos1 + NUL_SIMB.Length) + header_pos,
            (pos2 - (pos1 + NUL_SIMB.Length) + header_pos));
        }

        return pac_size;
      }
      catch (Exception ex)
      {
        throw new PacketDisassemblingException(String.Format(
          "PacketAuth.\r\n {0}.", ex.Message));
      }
    }
  }
}
