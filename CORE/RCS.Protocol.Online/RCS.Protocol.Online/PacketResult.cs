using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using TransitServisLib;

namespace RCS.Protocol.Online
{
  public class PacketResult : Packet
  {
    public PacketResult()
    {
    }

    public PacketResult(int message_id, string loginTT, long lastId, Dictionary<long, byte[]> result_query)
    {
      base.MessageID = message_id;
      //lastIDInResult = lastId;
      log_teletrack = loginTT;
      LastIDInResult = lastId;
      res_query = result_query;
    }

    /// <summary>
    /// ID ���������� ������ � �������
    /// ������������ ��� ��������� �������� ClientInfo.ID_Packet
    /// </summary>
    public long LastIDInResult
    {
      get { return lastIDInResult; }
      set { lastIDInResult = value; }
    }
    long lastIDInResult;

    public string LoginTT
    {
      get { return log_teletrack; }
      set { log_teletrack = value; }
    }
    string log_teletrack;

    /// <summary>
    /// ��������� �������
    /// </summary>
    public Dictionary<long, byte[]> res_query;

    /// <summary>
    /// ����� �������� �������� ������ � ����� (PacketResult) ���������
    /// </summary>
    /// <param name="message_id"></param>
    /// <param name="result_query">Result _query</param>
    /// <returns>Array of bytes.</returns>
    public byte[] AssemblePacket(int message_id, Dictionary<long, byte[]> result_query)
    {
      byte[] bytes = null;
      byte[] ms_buff = null;

      if (result_query.Count != 0)
      {
        lastIDInResult = result_query.GetEnumerator().Current.Key;

        using (MemoryStream ms = new MemoryStream())
        {
          new BinaryFormatter().Serialize(ms, result_query);
          ms_buff = ms.GetBuffer();
          //bytes = new byte[HEAD_LENGTH + ATR_LENGTH + ms.Length];
          bytes = new byte[ATR_LENGTH * 3 + ms_buff.Length];
        }
      }
      else
      {
        bytes = new byte[ATR_LENGTH * 3];
        ms_buff = new byte[0];
      }
      //��������� �����
      PACK_RES_QU.CopyTo(bytes, 0);
      BitConverter.GetBytes(bytes.Length).CopyTo(bytes, ATR_LENGTH);

      Encoding.ASCII.GetBytes(log_teletrack).CopyTo(bytes, ATR_LENGTH * 2);

      Array.Copy(ms_buff, 0, bytes, ATR_LENGTH * 3, ms_buff.Length);

      return bytes;
    }

    public override byte[] AssemblePacket()
    {
      byte[] bytes = null;
      byte[] ms_buff = null;

      if (res_query.Count != 0)
      {
        //lastIDInResult = res_query.GetEnumerator().Current.Key;
        using (MemoryStream ms = new MemoryStream())
        {
          new BinaryFormatter().Serialize(ms, res_query);
          ms_buff = ms.GetBuffer();
          //bytes = new byte[HEAD_LENGTH + ATR_LENGTH + ms.Length];
          bytes = new byte[ATR_LENGTH * 3 + ms_buff.Length];
        }
      }
      else
      {
        bytes = new byte[ATR_LENGTH * 3];
        ms_buff = new byte[0];
      }
      //��������� �����
      PACK_RES_QU.CopyTo(bytes, 0);
      BitConverter.GetBytes(bytes.Length).CopyTo(bytes, ATR_LENGTH);

      Encoding.ASCII.GetBytes(log_teletrack).CopyTo(bytes, ATR_LENGTH * 2);

      Array.Copy(ms_buff, 0, bytes, ATR_LENGTH * 3, ms_buff.Length);

      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      try
      {
        int pac_size = BitConverter.ToInt32(incomPack, ATR_LENGTH + header_pos);

        if (pac_size <= incomPack.Length)
        {
          MessageID = BitConverter.ToInt32(incomPack, (ATR_LENGTH * 2) + header_pos);

          //int buff_size = pac_size - ATR_LENGTH * 3;
          if (pac_size <= ATR_LENGTH * 3)
          {
            res_query = new Dictionary<long, byte[]>();
          }
          else
          {
            using (Stream ms = new MemoryStream())
            {
              ms.Write(incomPack, (ATR_LENGTH * 3) + header_pos, incomPack.Length - (ATR_LENGTH * 3) + header_pos);
              ms.Position = 0;
              res_query = (Dictionary<long, byte[]>)new BinaryFormatter().Deserialize(ms);
            }
          }
        }

        return pac_size;
      }
      catch (Exception ex)
      {
        throw new PacketDisassemblingException(String.Format(
          "PacketResult.\r\n {0}.", ex.Message));
      }
    }
  }
}
