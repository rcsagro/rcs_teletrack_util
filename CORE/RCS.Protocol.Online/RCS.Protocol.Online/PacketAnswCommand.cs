using System;
using TransitServisLib;

namespace RCS.Protocol.Online
{
  public class PacketAnswCommand : Packet
  {
    public PacketAnswCommand()
    {
    }

    public PacketAnswCommand(int message_id, /*int id_teletr,*/ int id_comCl, byte answ_code, byte[] com_text)
    {
      base.MessageID = message_id;
      //id_teletrack = id_teletr;
      id_commandCl = id_comCl;
      //id_cmdInSrDB = id_cmdInSrv;
      answer_code = answ_code;
      comtext = com_text;
    }

    /// <summary>
    /// ��� ������������ ������� Response
    /// </summary>
    public byte Answer_code
    {
      get { return answer_code; }
      set { answer_code = value; }
    }
    byte answer_code;

    /// <summary>
    /// ID ������� (� �� ������� ��� ����������� ������������)
    /// </summary>
    public int ID_CommandCl
    {
      get { return id_commandCl; }
      set { id_commandCl = value; }
    }
    private int id_commandCl;

    /// <summary>
    /// �������� ������ � comtext (��������)
    /// </summary>
    public byte[] ComText
    {
      get { return comtext; }
      set { comtext = value; }
    }

    /// <summary>
    /// ������� � �������� ���� (����� �1)
    /// </summary>
    private byte[] comtext;

    /// <summary>
    /// ����� �������� �������� ������ � ����� (PacketSendCommand) ���������
    /// </summary>
    /// <param name="message_id"></param>
    /// <param name="id_comCl">ID ������� � �� �������</param>
    /// <param name="com_text">���� �������</param>
    /// <returns></returns>
    internal byte[] AssemblePacket(int message_id, int id_comCl, byte answ_cod, byte[] com_text)
    {
      byte[] bytes = new byte[ATR_LENGTH * 4 + 1 + com_text.Length];
      //��������� �����
      PACK_ANSW_COM.CopyTo(bytes, 0);
      BitConverter.GetBytes(bytes.Length).CopyTo(bytes, ATR_LENGTH);
      BitConverter.GetBytes(message_id).CopyTo(bytes, ATR_LENGTH * 2);
      BitConverter.GetBytes(id_comCl).CopyTo(bytes, ATR_LENGTH * 3);
      bytes[ATR_LENGTH * 4] = answ_cod;
      com_text.CopyTo(bytes, ATR_LENGTH * 4 + 1);

      return bytes;
    }

    public override byte[] AssemblePacket()
    {
      byte[] bytes = new byte[ATR_LENGTH * 4 + 1 + comtext.Length];
      //��������� �����
      PACK_ANSW_COM.CopyTo(bytes, 0);
      BitConverter.GetBytes(bytes.Length).CopyTo(bytes, ATR_LENGTH);
      BitConverter.GetBytes(message_id).CopyTo(bytes, ATR_LENGTH * 2);
      BitConverter.GetBytes(id_commandCl).CopyTo(bytes, ATR_LENGTH * 3);
      bytes[ATR_LENGTH * 4] = answer_code;
      comtext.CopyTo(bytes, ATR_LENGTH * 4 + 1);

      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int pos)
    {
      try
      {
        int pac_size = BitConverter.ToInt32(incomPack, ATR_LENGTH + pos);

        if (pos + pac_size <= incomPack.Length)
        {
          MessageID = BitConverter.ToInt32(incomPack, (ATR_LENGTH * 2) + pos);
          ID_CommandCl = BitConverter.ToInt32(incomPack, (ATR_LENGTH * 3) + pos);
          answer_code = incomPack[(ATR_LENGTH * 4) + pos];

          int buff_size = pac_size - (ATR_LENGTH * 4 + 1);

          byte[] bytes = new byte[buff_size];
          Array.Copy(incomPack, (ATR_LENGTH * 4) + 1 + pos, bytes, 0, buff_size);
          ComText = bytes;
        }

        return pac_size;
      }
      catch (Exception ex)
      {
        throw new PacketDisassemblingException(String.Format(
          "PacketAnswCommand.\r\n {0}.", ex.Message));
      }
    }
  }
}
