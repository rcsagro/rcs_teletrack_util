using System;
using System.Text;
using TransitServisLib;

namespace RCS.Protocol.Online
{
  public class PacketSendCommand : Packet
  {
    public PacketSendCommand()
    {
    }

    public PacketSendCommand(int message_id, string logTT, int id_comCl, byte[] com_text)
    {
      base.MessageID = message_id;
      loginTT = logTT;
      idCmdInClDB = id_comCl;
      comtext = com_text;
    }

    /// <summary>
    /// LoginTT - �������� ��� (����� ���� ������������� �������)
    /// </summary>
    public string LoginTT
    {
      get { return loginTT; }
      set { loginTT = value; }
    }
    string loginTT;

    /// <summary>
    /// ID ������� (� �� ������� ��� ����������� ������������)
    /// </summary>
    public int IDCmdInClDB
    {
      get { return idCmdInClDB; }
      set { idCmdInClDB = value; }
    }
    private int idCmdInClDB;

    /// <summary>
    /// �������� ������ � comtext (��������)
    /// </summary>
    public byte[] ComText
    {
      get { return comtext; }
      set { comtext = value; }
    }
    /// <summary>
    /// ������� � �������� ���� (����� �1)
    /// </summary>
    private byte[] comtext;

    /// <summary>
    /// ����� �������� �������� ������ � ����� (PacketSendCommand) ���������
    /// </summary>
    /// <param name="message_id"></param>
    /// <param name="id_teletr">����</param>
    /// <param name="id_comCl">ID ������� � �� �������</param>
    /// <param name="com_text">���� �������</param>
    /// <returns></returns>
    public byte[] AssemblePacket(int message_id, string loginTT, int id_comCl, byte[] com_text)
    {
      byte[] bytes = new byte[ATR_LENGTH * 5 + com_text.Length];
      //��������� �����
      PACK_SN_COM.CopyTo(bytes, 0);
      BitConverter.GetBytes(bytes.Length).CopyTo(bytes, ATR_LENGTH);
      BitConverter.GetBytes(message_id).CopyTo(bytes, ATR_LENGTH * 2);

      (Encoding.ASCII.GetBytes(loginTT)).CopyTo(bytes, ATR_LENGTH * 3);

      //BitConverter.GetBytes(id_teletr).CopyTo(bytes, ATR_LENGTH * 3);
      BitConverter.GetBytes(id_comCl).CopyTo(bytes, ATR_LENGTH * 4);
      com_text.CopyTo(bytes, ATR_LENGTH * 5);

      return bytes;
    }

    public override byte[] AssemblePacket()
    {
      byte[] bytes = new byte[ATR_LENGTH * 5 + comtext.Length];
      //��������� �����
      PACK_SN_COM.CopyTo(bytes, 0);
      BitConverter.GetBytes(bytes.Length).CopyTo(bytes, ATR_LENGTH);
      BitConverter.GetBytes(message_id).CopyTo(bytes, ATR_LENGTH * 2);

      (Encoding.ASCII.GetBytes(loginTT)).CopyTo(bytes, ATR_LENGTH * 3);

      //BitConverter.GetBytes(id_teletr).CopyTo(bytes, ATR_LENGTH * 3);
      BitConverter.GetBytes(idCmdInClDB).CopyTo(bytes, ATR_LENGTH * 4);
      comtext.CopyTo(bytes, ATR_LENGTH * 5);

      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int pos)
    {
      try
      {
        int pac_size = BitConverter.ToInt32(incomPack, ATR_LENGTH + pos);

        if (pos + pac_size <= incomPack.Length)
        {
          MessageID = BitConverter.ToInt32(incomPack, (ATR_LENGTH * 2) + pos);
          loginTT = Encoding.ASCII.GetString(incomPack, (ATR_LENGTH * 3) + pos, ATR_LENGTH);
          idCmdInClDB = BitConverter.ToInt32(incomPack, (ATR_LENGTH * 4) + pos);

          int buff_size = pac_size - ATR_LENGTH * 5;

          byte[] bytes = new byte[buff_size];
          Array.Copy(incomPack, (ATR_LENGTH * 5) + pos, bytes, 0, buff_size);
          ComText = bytes;
        }

        return pac_size;
      }
      catch (Exception ex)
      {
        throw new PacketDisassemblingException(String.Format(
          "PacketSendCommand.\r\n {0}.", ex.Message));
      }
    }
  }
}
