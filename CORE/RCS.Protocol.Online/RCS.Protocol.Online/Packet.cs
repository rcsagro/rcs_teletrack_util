using System;
using System.Collections.Generic;
using System.Text;
using TransitServisLib;

namespace RCS.Protocol.Online
{
    public enum PKType
    {
        /// <summary>
        /// ����� �����������
        /// </summary>
        PACK_AUTH = 1,

        /// <summary>
        /// ����� AckExtendSr
        /// </summary>
        ACK_EX_SR = 2,

        /// <summary>
        /// ����� AckExtendCl
        /// </summary>
        ACK_EX_CL = 3,

        /// <summary>
        /// ����� PacketInfo
        /// </summary>
        PACK_INFO = 4,

        /// <summary>
        /// ����� SendCommand
        /// </summary>
        PACK_SN_COM = 5,

        /// <summary>
        /// ����� SendQuery
        /// </summary>
        PACK_SN_QU = 6,

        /// <summary>
        /// ����� Result
        /// </summary>
        PACK_RES_QU = 7,

        /// <summary>
        /// ����� AnswCommand
        /// </summary>
        PACK_ANSW_COM = 8
    }

    public abstract class Packet : IPacket
    {
        /// <summary>
        /// ���������� ������ ��������� ������
        /// (��� ���������� ����������)
        /// </summary>
        public int PacketSize
        {
            get { return packetSize; }
            set { packetSize = value; }
        }

        int packetSize = 0;

        /// <summary>
        /// ���������� id ������������� ������
        /// </summary>
        public int MessageID
        {
            get { return message_id; }
            set { message_id = value; }
        }

        protected int message_id;

        /// <summary>
        /// ������, ������� ����� - ������ � ����� 0�00
        /// </summary>
        public static readonly byte[] NUL_SIMB = new byte[1] {0x00};

        /// <summary>
        /// ������� ������ Auth
        /// </summary>
        public static readonly byte[] PACK_AUTH = Encoding.ASCII.GetBytes("%AuC");

        /// <summary>
        /// ������� ������ AckExtendSr
        /// </summary>
        public static readonly byte[] ACK_EX_SR = Encoding.ASCII.GetBytes("%AcS");

        /// <summary>
        /// ������� ������ AckExtendCl
        /// </summary>
        public static readonly byte[] ACK_EX_CL = Encoding.ASCII.GetBytes("%AcC");

        /// <summary>
        /// ������� ������ PacketInfo
        /// </summary>
        public static readonly byte[] PACK_INFO = Encoding.ASCII.GetBytes("%PkI");

        /// <summary>
        /// ������� ������ SendCommand
        /// </summary>
        public static readonly byte[] PACK_SN_COM = Encoding.ASCII.GetBytes("%SnC");

        /// <summary>
        /// ������� ������ SendQuery
        /// </summary>
        public static readonly byte[] PACK_SN_QU = Encoding.ASCII.GetBytes("%SeQ");

        /// <summary>
        /// ������� ������ Result
        /// </summary>
        public static readonly byte[] PACK_RES_QU = Encoding.ASCII.GetBytes("%ReS");

        /// <summary>
        /// ������� ������ AnswCommand
        /// </summary>
        public static readonly byte[] PACK_ANSW_COM = Encoding.ASCII.GetBytes("%AnC");

        /// <summary>
        /// ����� ��������� ��������� 4 �����
        /// </summary>
        public static readonly int ATR_LENGTH = 4;

        private static object locker = new object();

        protected static readonly List<byte[]> attributesPack = new List<byte[]>();

        static Packet()
        {
            attributesPack.Add(ACK_EX_SR);
            attributesPack.Add(ACK_EX_CL);
            attributesPack.Add(PACK_INFO);
            attributesPack.Add(PACK_RES_QU);
            attributesPack.Add(PACK_SN_COM);
            attributesPack.Add(PACK_SN_QU);
            attributesPack.Add(PACK_AUTH);
            attributesPack.Add(PACK_ANSW_COM);
        }

        public abstract byte[] AssemblePacket();
        public abstract int DisassemblePacket(byte[] incomPack, int header_pos);

        //******************************************************************************************
        /// <summary>
        /// �������������� �������� �����
        /// </summary>
        /// <param name="incom_pack">�����</param>
        /// <returns>������� ������</returns>
        public static IPacket PacketIdentifyNew(byte[] incom_pack, ref int pos)
        {
            //������ � ����� Packet
            byte[] attr = FindPacketHeader(incom_pack, ref pos);

            if (attr == PACK_AUTH) return new PacketAuth(); //����� Auth
            if (attr == ACK_EX_SR) return new PacketAckExSr(); //����� AckExtendSr
            if (attr == ACK_EX_CL) return new PacketAckExCl(); //����� AckExtendCl
            if (attr == PACK_INFO) return new PacketInfo(); //����� PacketInfo
            if (attr == PACK_SN_COM) return new PacketSendCommand(); //����� SendCommand
            if (attr == PACK_SN_QU) return new PacketSendQuery(); //����� SendQuery 
            if (attr == PACK_RES_QU) return new PacketResult(); //����� Result
            if (attr == PACK_ANSW_COM) return new PacketAnswCommand(); //����� AnswCommand 

            return null; //������ ���������� � Parser 
        }

        /// <summary>
        /// �������������� �������� �����
        /// </summary>
        /// <param name="incom_pack">�����</param>
        /// <returns>������� ������</returns>
        public static int PacketIdentify(byte[] incom_pack, ref int pos)
        {
            //������ � ����� Packet
            byte[] attr = FindPacketHeader(incom_pack, ref pos);
            if (attr != null)
            {
                if (attr == PACK_AUTH) return 1; //����� Auth
                if (attr == ACK_EX_SR) return 2; //����� AckExtendSr
                if (attr == ACK_EX_CL) return 3; //����� AckExtendCl
                if (attr == PACK_INFO) return 4; //����� PacketInfo
                if (attr == PACK_SN_COM) return 5; //����� SendCommand
                if (attr == PACK_SN_QU) return 6; //����� SendQuery 
                if (attr == PACK_RES_QU) return 7; //����� Result
                if (attr == PACK_ANSW_COM) return 8; //����� Result
            }

            //else 
            return 0; //������ ���������� � Parser
        }

        /// <summary>
        /// �������������� �������� �����
        /// </summary>
        /// <param name="bs">�����</param>
        /// <param name="res">�������</param>
        /// <returns>������� ������</returns>
        internal static byte[] FindPacketHeader(byte[] bs, ref int head_pos)
        {
            //������� �������� �� �������� ���������
            //����� ���������� ������������ ������ lock(object)
            lock (locker)
            {
                int count = attributesPack.Count;
                int tmp = count;
                //head_pos = -1;
                byte[] bd = new byte[ATR_LENGTH];
                bool flag = false;

                if (bs.Length < bd.Length)
                {
                    return null;
                }

                for (int i = head_pos; i < (bs.Length - (bd.Length - 1)); i++)
                {
                    for (int k = 0; k < attributesPack.Count; k++)
                    {
                        bd = attributesPack[k];
                        for (int j = 0; j < bd.Length; j++)
                        {
                            if (bs[i + j] == bd[j])
                            {
                                flag = true;
                            }
                            else
                            {
                                flag = false;
                                break;
                            }
                        }
                        if (flag)
                        {
                            head_pos = i;
                            return bd;
                        }
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// ��� ������ � PacketAuth
        /// </summary>
        /// <param name="bs">���</param>
        /// <param name="bd">����� ����� ����</param>
        /// <param name="start_pos">� ����� �������</param>
        /// <returns></returns>
        internal int FindBytes(byte[] bs, byte[] bd, int start_pos)
        {
            int res = -1;
            bool flag = false;

            if (bs.Length < bd.Length)
            {
                return -1;
            }

            for (int i = start_pos; i < (bs.Length - (bd.Length - 1)); i++)
            {
                for (int j = 0; j < bd.Length; j++)
                {
                    if (bs[i + j] == bd[j])
                    {
                        flag = true;
                    }
                    else
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    return i;
                }
            }
            return res;
        }

        /// <summary>
        /// ����������
        /// </summary>
        /// <param name="bs"></param>
        /// <param name="bd"></param>
        /// <returns></returns>
        internal int FindBytes(byte[] bs, byte[] bd)
        {
            int res = -1;
            bool flag = false;

            if (bs.Length < bd.Length)
            {
                return -1;
            }

            for (int i = 0; i < (bs.Length - (bd.Length - 1)); i++)
            {
                for (int j = 0; j < bd.Length; j++)
                {
                    if (bs[i + j] == bd[j])
                    {
                        flag = true;
                    }
                    else
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    return i;
                }
            }
            return res;
        }
    }
}
