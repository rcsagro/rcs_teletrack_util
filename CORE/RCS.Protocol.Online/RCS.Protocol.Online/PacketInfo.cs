using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using TransitServisLib;

namespace RCS.Protocol.Online
{
  /// <summary>
  /// ����� �������� ������ � ������ on-line
  /// (�� ����������� ��)
  /// </summary>
  public class PacketInfo : Packet
  {
    public PacketInfo()
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msg_id">����� ������(������������ ��� ������������� ��� �������� ����� ������)</param>
    /// <param name="loginTT">����� ���������</param>
    /// <param name="id_pack">id ������ � �� �������(������������ ��� �������� ����� ������)</param>
    /// <param name="inf_struct">Bin-�����</param>
    /// <param name="lst">������ Bin-������� (���� ������ MBin)</param>
    public PacketInfo(int msg_id, string loginTT, long id_pack, byte[] inf_struct, List<NewPacketFromTT> lst)
    {
      message_id = msg_id;
      log_teletrack = loginTT;
      id_packet = id_pack;
      packet = inf_struct;
      lstNewBin = lst;
    }

    /// <summary>
    /// ������ Bin-�������
    /// (������������ ����� � ������� �� �������� ������� ����� ����� ��� ���� Bin-�����)
    /// </summary>
    public List<NewPacketFromTT> lstNewBin;// = new List<NewPacketFromTT>();

    /// <summary>
    /// id_teletrack - �������� ��� (�����)
    /// </summary>
    public string LoginTT
    {
      get { return log_teletrack; }
      set { log_teletrack = value; }
    }
    string log_teletrack;

    /// <summary>
    /// ������ ID_Packet, ����������� ��� � �� �������
    /// </summary>
    public long ID_Packet
    {
      get { return id_packet; }
      set { id_packet = value; }
    }
    long id_packet;

    /// <summary>
    /// �������� ������� � ������ (A1, Bin, MultiBin � �.�.)
    /// </summary>
    public byte[] Packet
    {
      get { return packet; }
      set { packet = value; }
    }
    private byte[] packet;

    /// <summary>
    /// ����� �������� �������� ������ � ����� (PacketInfo) ���������
    /// </summary>
    public override byte[] AssemblePacket()
    {
      byte[] bytes = null;
      byte[] ms_buff = null;

      if (id_packet == -1 && packet == null)
      {
        if (lstNewBin == null)
        {
          throw new ArgumentOutOfRangeException("�� ����� ������ lstNewBin");
        }
        else
        {
          byte[] tmp;
          BinaryFormatter bf = new BinaryFormatter();
          using (MemoryStream ms = new MemoryStream())
          {
            bf.Serialize(ms, lstNewBin);
            tmp = ms.GetBuffer();
            ms_buff = new byte[ms.Length];
          }
          Array.Copy(tmp, 0, ms_buff, 0, ms_buff.Length);
          bytes = new byte[ATR_LENGTH * 6 + ms_buff.Length];
        }
      }
      else
      {
        bytes = new byte[ATR_LENGTH * 6 + packet.Length];
        ms_buff = packet;
      }

      //��������� �����
      PACK_INFO.CopyTo(bytes, 0);
      BitConverter.GetBytes(bytes.Length).CopyTo(bytes, ATR_LENGTH);
      BitConverter.GetBytes(message_id).CopyTo(bytes, ATR_LENGTH * 2);
      Encoding.ASCII.GetBytes(log_teletrack).CopyTo(bytes, ATR_LENGTH * 3);
      BitConverter.GetBytes(id_packet).CopyTo(bytes, ATR_LENGTH * 4);

      Array.Copy(ms_buff, 0, bytes, ATR_LENGTH * 6, ms_buff.Length);

      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      try
      {
        int pac_size = BitConverter.ToInt32(incomPack, ATR_LENGTH + header_pos);

        if (header_pos + pac_size <= incomPack.Length)
        {
          message_id = BitConverter.ToInt32(incomPack, (ATR_LENGTH * 2) + header_pos);
          log_teletrack = Encoding.ASCII.GetString(incomPack, (ATR_LENGTH * 3) + header_pos, ATR_LENGTH);
          id_packet = BitConverter.ToInt64(incomPack, (ATR_LENGTH * 4) + header_pos);

          if (id_packet == (-1))
          {
            BinaryFormatter bf = new BinaryFormatter();
            using (Stream ms = new MemoryStream())
            {
              ms.Write(incomPack, (ATR_LENGTH * 6) + header_pos, incomPack.Length - (ATR_LENGTH * 6) + header_pos);
              ms.Position = 0;
              lstNewBin = (List<NewPacketFromTT>)bf.Deserialize(ms);
            }
          }
          else
          {
            int buff_size = pac_size - ATR_LENGTH * 6;
            byte[] bytes = new byte[buff_size];
            Array.Copy(incomPack, (ATR_LENGTH * 6) + header_pos, bytes, 0, buff_size);
            packet = bytes;
          }
        }

        return pac_size;
      }
      catch (Exception ex)
      {
        throw new PacketDisassemblingException(String.Format(
          "PacketInfo.\r\n {0}.", ex.Message));
      }
    }
  }
}
