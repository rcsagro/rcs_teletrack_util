using System;
using TransitServisLib;

namespace RCS.Protocol.Online
{
  public class PacketAckExSr : Packet
  {
    public PacketAckExSr()
    {
    }

    public PacketAckExSr(int cn_state, long id_lstPack,  /*int id_comCl,int id_cmdSr,*/ int msg_id_cl, int msg_id)
    {
      con_state = cn_state;
      id_lastPack = id_lstPack;
      //id_commandCl = id_comCl;            
      //id_cmdInSrDB = id_cmdSr;
      message_id_in = msg_id_cl;
      base.MessageID = msg_id;
    }

    /// <summary>
    /// ��� ������ (��� ������� AckEx)
    /// </summary>
    public int ConState
    {
      get { return con_state; }
      set { con_state = value; }
    }
    protected int con_state;

    /// <summary>
    /// ID ���������� ������ ����������� �� �� ��� ������� ������� 
    /// </summary>
    public long IDLastPack
    {
      get { return id_lastPack; }
      set { id_lastPack = value; }
    }
    protected long id_lastPack;

    /// <summary>
    /// id ���������� ������ ����������� �� ������� 
    /// (������������� �� ������� �������)
    /// </summary>
    public int MessageID_In
    {
      get { return message_id_in; }
      set { message_id_in = value; }
    }
    protected int message_id_in;

    /// <summary>
    /// 24 �����.
    /// </summary>
    private static readonly int _packetLength = ATR_LENGTH * 6;

    /// <summary>
    /// ��������� ����� AckExtendSr
    /// </summary>
    /// <returns></returns>
    public override byte[] AssemblePacket()
    {
      byte[] bytes = new byte[_packetLength];
      //��������� �����
      ACK_EX_SR.CopyTo(bytes, 0);
      BitConverter.GetBytes(con_state).CopyTo(bytes, ATR_LENGTH);
      BitConverter.GetBytes(id_lastPack).CopyTo(bytes, ATR_LENGTH * 2);
      //BitConverter.GetBytes(id_commandCl).CopyTo(bytes, ATR_LENGTH * 3);
      BitConverter.GetBytes(message_id_in).CopyTo(bytes, ATR_LENGTH * 4);
      BitConverter.GetBytes(message_id).CopyTo(bytes, ATR_LENGTH * 5);

      return bytes;
    }

    /// <summary>
    /// ��������� ����� AckExtendSr
    /// </summary>
    /// <param name="con_state">��������� ����������</param>
    /// <param name="count_pack">���������� �������������� ������� �������</param>
    /// <param name="id_comCl">����� ����������� �������</param>
    /// <param name="message_id_in">����� ����������� �� ������� ������</param>
    /// <param name="message_id">����� ������� ������</param>
    /// <returns></returns>
    public byte[] AssemblePacket(int con_state, long id_lastPack, /*int id_comCl,*/ int message_id_in, int message_id)
    {
      byte[] bytes = new byte[_packetLength];
      //��������� �����
      ACK_EX_SR.CopyTo(bytes, 0);
      BitConverter.GetBytes(con_state).CopyTo(bytes, ATR_LENGTH);
      BitConverter.GetBytes(id_lastPack).CopyTo(bytes, ATR_LENGTH * 2);
      //BitConverter.GetBytes(id_comCl).CopyTo(bytes, ATR_LENGTH * 3);
      BitConverter.GetBytes(message_id_in).CopyTo(bytes, ATR_LENGTH * 4);
      BitConverter.GetBytes(message_id).CopyTo(bytes, ATR_LENGTH * 5);

      return bytes;
    }

    public override int DisassemblePacket(byte[] incomPack, int header_pos)
    {
      try
      {
        //������ ������ AckExtendSr = 24 ����
        if (header_pos + _packetLength <= incomPack.Length)
        {
          ConState = BitConverter.ToInt32(incomPack, ATR_LENGTH + header_pos);
          IDLastPack = BitConverter.ToInt64(incomPack, (ATR_LENGTH * 2) + header_pos);
          //ID_CommandCl = BitConverter.ToInt32(incomPack, (ATR_LENGTH * 3) + header_pos);
          MessageID_In = BitConverter.ToInt32(incomPack, (ATR_LENGTH * 4) + header_pos);
          MessageID = BitConverter.ToInt32(incomPack, (ATR_LENGTH * 5) + header_pos);
        }

        return _packetLength;
      }
      catch (Exception ex)
      {
        throw new PacketDisassemblingException(String.Format(
          "PacketAckExSr.\r\n {0}.", ex.Message));
      }
    }
  }
}
