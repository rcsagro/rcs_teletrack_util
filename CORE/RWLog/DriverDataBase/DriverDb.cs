﻿using System;
using System.Data;
using System.Data.Common;
using System.Reflection;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace RWLog.DriverDataBase
{
    /// <summary>
    /// Universal class for effective use Data Bases (MySql and MS SQL)
    /// </summary>
    public class DriverDb : IDisposable
    {

        private const string CheckTable = @"SELECT 1 FROM information_schema.tables WHERE TABLE_SCHEMA = '{0}' AND TABLE_NAME = '{1}';";
        private const string CheckColumn = @"SELECT 1 FROM information_schema.columns WHERE TABLE_SCHEMA = '{0}' AND TABLE_NAME = '{1}' AND COLUMN_NAME = '{2}';";

        private const string msCheckTable = @"SELECT 1 FROM information_schema.tables WHERE TABLE_SCHEMA = '{0}' AND TABLE_NAME = '{1}';";
        private const string msCheckColumn = @"SELECT 1 FROM information_schema.columns WHERE TABLE_SCHEMA = '{0}' AND TABLE_NAME = '{1}' AND COLUMN_NAME = '{2}';";

        public const int MySqlUse = 0;
        public const int MssqlUse = 1;

        protected static int TypeDataBaseForUsing = -1;

        // строки подключения к БД
        private static string _connectionString = String.Empty; // для глобального/внешнего использования из класса ConnectMySql
        //private string _cs = String.Empty; // для внутреннего использования из класса DbCommon

        #region MySQL Database data types

        // для работы приложения
        private MySqlDataReader data_reader;
        private MySqlParameter[] parSql;
        private MySqlParameter sqlParameter;
        private MySqlCommand command;
        private MySqlConnection sqlConnect;
        private MySqlTransaction transact;
        private IAsyncResult asyncResult;
        private MySqlConnection conn;

        // для работы дата адаптеров из LocalCache
        private MySqlDataAdapter _adapter;
        private MySqlCommand[] _commandCollection;
        private MySqlConnection _connection;

        // внутренние переменные
        private int numberParameter = 0;
        private int countParameter = 0;

        #endregion

        #region MS SQL Database data type

        // для работы приложения
        private SqlDataReader s_data_reader;
        private SqlParameter[] s_parSql;
        private SqlParameter s_sqlParameter;
        private SqlCommand s_command;
        private SqlConnection s_sqlConnect;
        private SqlTransaction s_transact;
        private IAsyncResult s_asyncResult;
        private SqlConnection s_conn;

        // для работы дата адаптеров из LocalCache
        private SqlDataAdapter s_adapter;
        private SqlCommand[] s_commandCollect;
        private SqlConnection s_connection;

        // внутренние переменные
        private int s_numberParameter = 0;
        private int s_countParameter = 0;
        private MySqlParameter newParameter = null;
        private SqlParameter s_newParameter = null;
        #endregion

        /// <summary>
        /// Constructor of class DriverDb
        /// </summary>
        public DriverDb()
        {
            data_reader = null;
            parSql = null;
            command = null;
            sqlConnect = null;
            transact = null;
            sqlParameter = null;
            conn = null;
            _adapter = null;
            _commandCollection = null;
            _connection = null;

            s_data_reader = null;
            s_parSql = null;
            s_command = null;
            s_sqlConnect = null;
            s_transact = null;
            s_sqlParameter = null;
            s_conn = null;
            s_adapter = null;
            s_commandCollect = null;
            s_connection = null;
        } // DriverDb

        /// <summary>
        /// Constructor of class DriverDb. Analog of class DbCommon
        /// </summary>
        /// <param name="connstr"></param>
        public DriverDb(string connstr)
        {
            if (String.IsNullOrEmpty(connstr))
            {
                throw new ArgumentException("Connection to database String can not be NULL or Empty.");
            }

            //_cs = connstr;
            _connectionString = connstr;

            AnalizeTypeDB();

            data_reader = null;
            parSql = null;
            command = null;
            sqlConnect = null;
            transact = null;
            sqlParameter = null;
            conn = null;
            _adapter = null;
            _commandCollection = null;
            _connection = null;

            s_data_reader = null;
            s_parSql = null;
            s_command = null;
            s_sqlConnect = null;
            s_transact = null;
            s_sqlParameter = null;
            s_conn = null;
            s_adapter = null;
            s_commandCollect = null;
            s_connection = null;
        } // DriverDb

        public static void AnalizeTypeDB(string str)
        {
            // сдесь анализ строки подключения
            try
            {
               // string nameDataBase = ""; //ConfigurationManager.AppSettings.GetKey(0);

                if( str.Equals( "MYSQL" ) )
                {
                    TypeDataBaseUsing = MySqlUse; // switch MySQL Data Base 
                }
                else if( str.Equals( "MSSQL" ) )
                {
                    TypeDataBaseUsing = MssqlUse; // switch MSSQL Data Base 
                }
            }
            catch( Exception ex )
            {
                throw new Exception( ex.Message + "!" );
            }
        }

        public void AnalizeTypeDB()
        {
            // сдесь анализ строки подключения
            try
            {
                string nameDataBase = ""; //ConfigurationManager.AppSettings.GetKey(0);

                if (nameDataBase.Equals("MYSQL"))
                {
                    TypeDataBaseUsing = MySqlUse; // switch MySQL Data Base 
                }
                else if (nameDataBase.Equals("MSSQL"))
                {
                    TypeDataBaseUsing = MssqlUse; // switch MSSQL Data Base 
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "!");
            }
        }

        /// <summary>
        /// Create new connect to data base. Analog ConnectMySql() in class ConnectMySql
        /// Using internal string to connect
        /// </summary>
        public void ConnectDb()
        {
            if (_connectionString.Length == 0)
            {
                //_connectionString =
                //    Crypto.GetDecryptConnectionString(ConfigurationManager.ConnectionStrings["CS"].ConnectionString);

                // сдесь анализ строки подключения
                AnalizeTypeDB();
            }

            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:

                    this.conn = new MySqlConnection(_connectionString);

                    try
                    {
                        conn.Open();
                    }
                    catch (MySqlException xException)
                    {
                        throw new Exception("Not connect to MY SQL data base!");
                    }
                    break;

                case MssqlUse:
                    this.s_conn = new SqlConnection(_connectionString);

                    try
                    {
                        s_conn.Open();
                    }
                    catch (SqlException xException)
                    {
                        throw new Exception("Not connect to MS SQL data base!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ConnectDb

        /// <summary>
        /// Create new connect to data base. Analog ConnectMySql() in class ConnectMySql
        /// Using outside connect string
        /// </summary>
        /// <param name="conString"></param>
        public void ConnectDb(string conString)
        {
            _connectionString = conString;

            // сдесь анализ строки подключения
            AnalizeTypeDB();

            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    this.conn = new MySqlConnection(_connectionString);

                    try
                    {
                        conn.Open();

                    }
                    catch (MySqlException xException)
                    {
                        throw new Exception("Not connect to MySql data base!");
                    }
                    break;

                case MssqlUse:
                    this.s_conn = new SqlConnection(_connectionString);

                    try
                    {
                        s_conn.Open();
                    }
                    catch (SqlException xException)
                    {
                        throw new Exception("Not connect to MS SQL data base!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //switch
        } // ConnectDb

        /// <summary>
        /// Type current data base for use
        /// </summary>
        public static int TypeDataBaseUsing
        {
            get { return TypeDataBaseForUsing; }
            set { TypeDataBaseForUsing = value; }
        }

        /// <summary>
        /// Close connect to database
        /// </summary>
        public void CloseDbConnection()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn != null)
                    {
                        try
                        {
                            conn.Dispose();
                        }
                        catch (Exception xException)
                        {
                            throw new Exception("Is not disposable connect to MY SQL database!");
                        }
                    }
                    else
                    {
                        throw new Exception("Conenct to database is not existing!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn != null)
                    {
                        try
                        {
                            s_conn.Dispose();
                        }
                        catch (Exception xException)
                        {
                            throw new Exception("Is not disposable connect to MS SQL database!");
                        }
                    }
                    else
                    {
                        throw new Exception("Conenct to database is not existing!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CloseDbConnection

        #region IDisposable Members

        /// <summary>
        /// Close connect to database using IDisposable memebes
        /// </summary>
        public void Dispose()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn != null)
                    {
                        try
                        {
                            conn.Dispose();

                        }
                        catch (Exception xException)
                        {

                            throw new Exception("Is not disposable connect to MY SQL database with IDisposable memebers!");
                        }
                    }
                    else
                    {
                        throw new Exception("Connect to database is not existing!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn != null)
                    {
                        try
                        {
                            s_conn.Dispose();
                        }
                        catch (Exception xException)
                        {

                            throw new Exception("Is not disposable connect to MS SQL database with IDisposable memebers!");
                        }
                    }
                    else
                    {
                        throw new Exception("Connect to database is not existing!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // Dispose

        #endregion

        /// <summary>
        /// Return connect class to database
        /// </summary>
        /// <returns></returns>
        public object Connect()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.conn != null)
                        return this.conn;
                    else
                        throw new Exception("Connect to MY SQL database is not existing!");
                    break;

                case MssqlUse:
                    if (this.s_conn != null)
                        return this.s_conn;
                    else
                        throw new Exception("Connect to MS SQL database is not existing!");
                    return this.conn;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //  switch
        } // Connect

        /// <summary>
        /// Init data reader using class MySqlCommand
        /// </summary>
        /// <param name="sSql"></param>
        public void GetDataReader(string sSql)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSql;
                        cmd.CommandTimeout = 0;
                        this.data_reader = cmd.ExecuteReader();
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Initialization MY SQL data_reader is have error!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSql;
                        s_cmd.CommandTimeout = 0;
                        this.s_data_reader = s_cmd.ExecuteReader();
                    }
                    catch (Exception xException)
                    {
                        throw new Exception(xException.Message + "!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetDataReader

        /// <summary>
        /// Getting data reader using class MySqlCommand
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="reader"></param>
        private void GetDataReader(string sSql, ref object reader)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSql;
                        cmd.CommandTimeout = 0;
                        reader = cmd.ExecuteReader();
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Initialization MY SQL data reader is have error!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSql;
                        s_cmd.CommandTimeout = 0;
                        reader = s_cmd.ExecuteReader();
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Initialization MS SQL data reader is have error!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetDataReader

        /// <summary>
        /// Initial data reader with array parameters
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="paramArray"></param>
        //               public void GetDataReader( string sSQL, MySqlParameter[] paramArray )
        public void GetDataReader(string sSql, object[] paramArray)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            MySqlParameter cmnd = paramArray[j] as MySqlParameter;

                            if (cmnd == null)
                                throw new Exception("Parameter is not valid!");

                            cmd.Parameters.Add(cmnd);
                        }

                        cmd.CommandText = sSql;
                        cmd.CommandTimeout = 0;
                        this.data_reader = cmd.ExecuteReader();
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Initialization My SQL data_reader with parameter is have error!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            SqlParameter s_cmnd = paramArray[j] as SqlParameter;

                            if (s_cmnd == null)
                                throw new Exception("Parameter is not valid!");

                            s_cmd.Parameters.Add(s_cmnd);
                        }

                        s_cmd.CommandText = sSql;
                        s_cmd.CommandTimeout = 0;
                        this.s_data_reader = s_cmd.ExecuteReader();
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Initialization MS SQL data_reader with parameter is have error!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetDataReader

        /// <summary>
        ///  Initial data reader with array parameters and proc name
        /// </summary>
        /// <param name="sProcName"></param>
        /// <param name="paramArray"></param>
        public void GetProcDataReader(string sProcName, object[] paramArray)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = sProcName;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            MySqlParameter cmnd = paramArray[j] as MySqlParameter;

                            if (cmnd == null)
                                throw new Exception("MY SQL parameter is not valid!");

                            cmd.Parameters.Add(cmnd);
                        }

                        cmd.CommandTimeout = 0;
                        this.data_reader = cmd.ExecuteReader();
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Initialization MY SQL data_reader with parameter and proc is have error!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        s_cmd.CommandText = sProcName;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            SqlParameter s_cmnd = paramArray[j] as SqlParameter;

                            if (s_cmnd == null)
                                throw new Exception("MS SQL parameter is not valid!");

                            s_cmd.Parameters.Add(s_cmnd);
                        }

                        s_cmd.CommandTimeout = 0;
                        this.s_data_reader = s_cmd.ExecuteReader();
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Initialization data_reader with parameter and proc is have error!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetProcDataReader

        /// <summary>
        /// Getting data reader count record
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public Int32 GetDataReaderRecordsCount(string sSQL)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = "select count(*) as CNT from (" + sSQL + ") as CC";
                        cmd.CommandTimeout = 0;
                        return Convert.ToInt32(cmd.ExecuteScalar());
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("GetDataReaderRecordsCount have MY SQL error!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = "select count(*) as CNT from (" + sSQL + ") as CC";
                        s_cmd.CommandTimeout = 0;
                        return Convert.ToInt32(s_cmd.ExecuteScalar());
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("GetDataReaderRecordsCount have MS SQL error!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetDataReaderRecordsCount

        /// <summary>
        /// Getting data reader count record with array parameters
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="paramArray"></param>
        /// <returns></returns>
        public Int32 GetDataReaderRecordsCount(string sSQL, object[] paramArray)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            MySqlParameter cmnd = paramArray[j] as MySqlParameter;

                            if (cmnd == null)
                                throw new Exception("MY SQL Parameter is not valid!");

                            cmd.Parameters.Add(cmnd);
                        }

                        cmd.CommandText = "select count(*) as CNT from (" + sSQL + ") as CC";
                        cmd.CommandTimeout = 0;
                        return Convert.ToInt32(cmd.ExecuteScalar());
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("GetDataReaderRecordsCount have MY SQL error!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            SqlParameter s_cmnd = paramArray[j] as SqlParameter;

                            if (s_cmnd == null)
                                throw new Exception("MS SQL Parameter is not valid!");

                            s_cmd.Parameters.Add(s_cmnd);
                        }

                        s_cmd.CommandText = "select count(*) as CNT from (" + sSQL + ") as CC";
                        s_cmd.CommandTimeout = 0;
                        return Convert.ToInt32(s_cmd.ExecuteScalar());
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("GetDataReaderRecordsCount have MS SQL error!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } //  GetDataReaderRecordsCount

        /// <summary>
        /// Executing non query command
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public int ExecuteNonQueryCommand(string sSQL)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSQL;
                        cmd.CommandTimeout = 0;
                        cmd.ExecuteNonQuery();
                        return 0;
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Is not execute non query MY SQL commands");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSQL;
                        s_cmd.CommandTimeout = 0;
                        s_cmd.ExecuteNonQuery();
                        return 0;
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Is not execute non query MS SQL commands");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ExecuteNonQueryCommand

        /// <summary>
        /// Execute command and return last inserting
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public int ExecuteReturnLastInsert(string sSQL)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSQL;
                        cmd.CommandTimeout = 0;
                        cmd.ExecuteNonQuery();
                        return (int)cmd.LastInsertedId;
                    }
                    catch (Exception xExcept)
                    {
                        throw new Exception("Not executing and return MY SQL last insert!");
                    }
                    break;

                case MssqlUse:
                    if (this.s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        // Этот код потестировать более внимательно
                        SqlCommand cmdSelectId = new SqlCommand("SELECT @@IDENTITY;", this.s_conn); // или
                        //SqlCommand cmdSelectId = new SqlCommand( "SELECT SCOPE_IDENTITY();", this.s_conn ); // или

                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = this.s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSQL;
                        s_cmd.CommandTimeout = 0;
                        s_cmd.ExecuteNonQuery(); // Добавляем запись
                        int id = (int)cmdSelectId.ExecuteScalar(); // Получает id вставленной записи
                        return id;
                    }
                    catch (Exception xExcept)
                    {
                        throw new Exception("Not executing and return MS SQL last insert!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ExecuteReturnLastInsert

        /// <summary>
        /// Execute command and return last inserting with array parameters
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="paramArray"></param>
        /// <returns></returns>
        public int ExecuteReturnLastInsert(string sSQL, object[] paramArray)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            MySqlParameter cmnd = paramArray[j] as MySqlParameter;

                            if (cmnd == null)
                                throw new Exception("MY SQL Parameter is not valid!");

                            cmd.Parameters.Add(cmnd);
                        }

                        cmd.CommandText = sSQL;
                        cmd.CommandTimeout = 0;
                        cmd.ExecuteNonQuery();
                        return (int)cmd.LastInsertedId;
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Not executing return MY SQL last insert!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            SqlParameter s_cmnd = paramArray[j] as SqlParameter;

                            if (s_cmnd == null)
                                throw new Exception("MS SQL Parameter is not valid!");

                            s_cmd.Parameters.Add(s_cmnd);
                        }

                        s_cmd.CommandText = sSQL;
                        s_cmd.CommandTimeout = 0;

                        // Этот код потестировать более внимательно
                        SqlCommand cmdSelectId = new SqlCommand("SELECT @@IDENTITY;", this.s_conn); // или
                        //SqlCommand cmdSelectId = new SqlCommand( "SELECT SCOPE_IDENTITY();", this.s_conn ); // или

                        s_cmd.ExecuteNonQuery();
                        int id = (int)cmdSelectId.ExecuteScalar(); // Получает id вставленной записи
                        return id;
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Not executing return MS SQL last insert!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ExecuteReturnLastInsert

        /// <summary>
        /// Executing non query command with array parameters
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="paramArray"></param>
        /// <returns></returns>
        public int ExecuteNonQueryCommand(string sSQL, object[] paramArray)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            MySqlParameter cmnd = paramArray[j] as MySqlParameter;

                            if (cmnd == null)
                                throw new Exception("Parameter is not valid!");

                            cmd.Parameters.Add(cmnd);
                        }

                        cmd.CommandText = sSQL;
                        cmd.CommandTimeout = 0;
                        cmd.ExecuteNonQuery();
                        return 0;
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Is not executing non query MY SQL command with array parameters!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            SqlParameter s_cmnd = paramArray[j] as SqlParameter;

                            if (s_cmnd == null)
                                throw new Exception("MS SQL Parameter is not valid!");

                            s_cmd.Parameters.Add(s_cmnd);
                        }

                        s_cmd.CommandText = sSQL;
                        s_cmd.CommandTimeout = 0;
                        s_cmd.ExecuteNonQuery();
                        return 0;
                    }
                    catch (Exception xException)
                    {
                        throw new Exception("Is not executing non query MS SQL command with array parameters!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //switch
        } // ExecuteNonQueryCommand

        /// <summary>
        /// Getting scalar value
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public object GetScalarValue(string sSQL)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSQL;
                        return cmd.ExecuteScalar();
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not getting MY SQL scalar value!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSQL;
                        return s_cmd.ExecuteScalar();
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not getting MS SQL scalar value!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetScalarValue

        /// <summary>
        /// Getting scalar value double null
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public double GetScalarValueDblNull(string sSQL)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSQL;
                        object obj = cmd.ExecuteScalar();
                        double ret;

                        if ((obj != null) && Double.TryParse(obj.ToString(), out ret))
                        {
                            return ret;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not executing MY SQL getting scalar value double null!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSQL;
                        object s_obj = s_cmd.ExecuteScalar();
                        double s_ret;

                        if ((s_obj != null) && Double.TryParse(s_obj.ToString(), out s_ret))
                        {
                            return s_ret;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not executing getting MS SQL scalar value double null!");
                    };
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetScalarValueDblNull

        /// <summary>
        /// Getting scalar value double null with array parameters
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="paramArray"></param>
        /// <returns></returns>
        public double GetScalarValueDblNull(string sSQL, object[] paramArray)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            MySqlParameter cmnd = paramArray[j] as MySqlParameter;

                            if (cmnd == null)
                                throw new Exception("MY SQL Parameter is not valid!");

                            cmd.Parameters.Add(cmd);
                        }

                        cmd.CommandText = sSQL;
                        object obj = cmd.ExecuteScalar();
                        double ret;

                        if ((obj != null) && Double.TryParse(obj.ToString(), out ret))
                        {
                            return ret;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not executing MY SQL GetScalarValueDblNull!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            SqlParameter s_cmnd = paramArray[j] as SqlParameter;

                            if (s_cmnd == null)
                                throw new Exception("MS SQL Parameter is not valid!");

                            s_cmd.Parameters.Add(s_cmd);
                        }

                        s_cmd.CommandText = sSQL;
                        object s_obj = s_cmd.ExecuteScalar();
                        double s_ret;

                        if ((s_obj != null) && Double.TryParse(s_obj.ToString(), out s_ret))
                        {
                            return s_ret;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not executing MS SQL GetScalarValueDblNull!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetScalarValueDblNull

        /// <summary>
        /// Getting scalar value time span null
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public TimeSpan GetScalarValueTimeSpanNull(string sSQL)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSQL;
                        object obj = cmd.ExecuteScalar();
                        TimeSpan ts;

                        if ((obj != null) && TimeSpan.TryParse(obj.ToString(), out ts))
                        {
                            return ts;
                        }
                        else
                        {
                            return TimeSpan.Zero;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not executing get MY SQL scalar value time span null!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSQL;
                        object s_obj = s_cmd.ExecuteScalar();
                        TimeSpan sts;

                        if ((s_obj != null) && TimeSpan.TryParse(s_obj.ToString(), out sts))
                        {
                            return sts;
                        }
                        else
                        {
                            return TimeSpan.Zero;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not executing get MS SQL scalar value time span null!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetScalarValueTimeSpanNull

        /// <summary>
        /// Getting scalar value date time null
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="DateReplace"></param>
        /// <returns></returns>
        public DateTime GetScalarValueDateTimeNull(string sSQL, DateTime DateReplace)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSQL;
                        object obj = cmd.ExecuteScalar();
                        DateTime dt;

                        if ((obj != null) && DateTime.TryParse(obj.ToString(), out dt))
                        {
                            return dt;
                        }
                        else
                        {
                            return DateReplace;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MY SQL Get Scalar Value Date Time Null!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSQL;
                        object s_obj = s_cmd.ExecuteScalar();
                        DateTime sdt;

                        if ((s_obj != null) && DateTime.TryParse(s_obj.ToString(), out sdt))
                        {
                            return sdt;
                        }
                        else
                        {
                            return DateReplace;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MS SQL Get Scalar Value Date Time Null!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetScalarValueDateTimeNull

        /// <summary>
        /// Getting scalar value integer null
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public int GetScalarValueIntNull(string sSQL)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    if (conn.State == ConnectionState.Closed)
                    {
                        return 0;
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSQL;
                        object obj = cmd.ExecuteScalar();
                        int ret;

                        if ((obj != null) && Int32.TryParse(obj.ToString(), out ret))
                        {
                            return ret;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MY SQL get scalar value int null!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    if (s_conn.State == ConnectionState.Closed)
                    {
                        return 0;
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSQL;
                        object s_obj = s_cmd.ExecuteScalar();
                        int s_ret;

                        if ((s_obj != null) && Int32.TryParse(s_obj.ToString(), out s_ret))
                        {
                            return s_ret;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MS SQL get scalar value int null!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetScalarValueIntNull

        /// <summary>
        /// Getting scalar value integer nullwith array parameters
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="paramArray"></param>
        /// <returns></returns>
        public int GetScalarValueIntNull(string sSQL, object[] paramArray)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            MySqlParameter cmnd = paramArray[j] as MySqlParameter;

                            if (cmnd == null)
                                throw new Exception("MY SQL Parameter is not valid!");

                            cmd.Parameters.Add(cmnd);
                        }

                        cmd.CommandText = sSQL;
                        object obj = cmd.ExecuteScalar();
                        int ret;

                        if ((obj != null) && Int32.TryParse(obj.ToString(), out ret))
                        {
                            return ret;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MY SQL Get Scalar Value Int Null!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            SqlParameter s_cmnd = paramArray[j] as SqlParameter;

                            if (s_cmnd == null)
                                throw new Exception("MS SQL Parameter is not valid!");

                            s_cmd.Parameters.Add(s_cmnd);
                        }

                        s_cmd.CommandText = sSQL;
                        object s_obj = s_cmd.ExecuteScalar();
                        int sret;

                        if ((s_obj != null) && Int32.TryParse(s_obj.ToString(), out sret))
                        {
                            return sret;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MS SQL Get Scalar Value Int Null!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetScalarValueIntNull

        /// <summary>
        /// Getting data table
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="paramArray"></param>
        /// <returns></returns>
        public DataTable GetDataTable(string sSQL, object[] paramArray)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            MySqlParameter cmnd = paramArray[j] as MySqlParameter;

                            if (cmnd == null)
                                throw new Exception("MY SQL Parameter is not valid!");

                            cmd.Parameters.Add(cmnd);
                        }

                        cmd.CommandText = sSQL;
                        MySqlDataAdapter da = new MySqlDataAdapter();
                        DataSet dsWork = new DataSet();
                        da.SelectCommand = cmd;
                        cmd.CommandTimeout = 0;
                        da.Fill(dsWork);
                        return dsWork.Tables[0];
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not MY SQL getting data table!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            SqlParameter s_cmnd = paramArray[j] as SqlParameter;

                            if (s_cmnd == null)
                                throw new Exception("MS SQL Parameter is not valid!");

                            s_cmd.Parameters.Add(s_cmnd);
                        }

                        s_cmd.CommandText = sSQL;
                        SqlDataAdapter sda = new SqlDataAdapter();
                        DataSet sdsWork = new DataSet();
                        sda.SelectCommand = s_cmd;
                        s_cmd.CommandTimeout = 0;
                        sda.Fill(sdsWork);
                        return sdsWork.Tables[0];
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not MS SQL getting data table!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetDataTable

        /// <summary>
        /// Getting data table
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public DataTable Data_Table(object commd)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    MySqlCommand cmd = commd as MySqlCommand;
                    if (cmd == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        DataTable table = new DataTable();
                        var da = new MySqlDataAdapter(cmd);
                        da.Fill(table);            
                        return table;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not MY SQL getting data table!");
                    }
                    break;

                case MssqlUse:
                    SqlCommand s_cmd = commd as SqlCommand;
                    if (s_cmd == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        DataTable table = new DataTable();
                        var da = new SqlDataAdapter(s_cmd);
                        da.Fill(table);
                        return table;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not MS SQL getting data table!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetDataTable

        /// <summary>
        /// Getting data table
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public DataTable GetDataTable(string sSQL)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSQL;
                        cmd.CommandTimeout = 0;
                        DataTable table = new DataTable();
                        MySqlDataReader dread = cmd.ExecuteReader();
                        table.Load(dread);
                        return table;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not MY SQL getting data table!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSQL;
                        s_cmd.CommandTimeout = 0;
                        DataTable stable = new DataTable();
                        SqlDataReader sdread = s_cmd.ExecuteReader();
                        stable.Load(sdread);
                        return stable;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not MS SQL getting data table!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetDataTable

        /// <summary>
        /// Getting data set
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="sTableName"></param>
        /// <returns></returns>
        public DataSet GetDataSet(string sSQL, string sTableName)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSQL;
                        MySqlDataAdapter da = new MySqlDataAdapter();
                        DataSet dsWork = new DataSet();
                        da.SelectCommand = cmd;
                        da.Fill(dsWork, sTableName);
                        return dsWork;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MY SQL Get Data Set!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSQL;
                        SqlDataAdapter sda = new SqlDataAdapter();
                        DataSet sdsWork = new DataSet();
                        sda.SelectCommand = s_cmd;
                        sda.Fill(sdsWork, sTableName);
                        return sdsWork;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing Get Data Set!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetDataSet

        /// <summary>
        /// Getting data set
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="paramArray"></param>
        /// <param name="sTableName"></param>
        /// <returns></returns>
        public DataSet GetDataSet(string sSQL, object[] paramArray, string sTableName)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            MySqlParameter cmnd = paramArray[j] as MySqlParameter;

                            if (cmnd == null)
                                throw new Exception("MY SQL Parameter is not valid!");

                            cmd.Parameters.Add(cmnd);
                        }

                        cmd.CommandText = sSQL;
                        MySqlDataAdapter da = new MySqlDataAdapter();
                        DataSet dsWork = new DataSet();
                        da.SelectCommand = cmd;
                        da.Fill(dsWork, sTableName);
                        return dsWork;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MY SQL Get Data Set!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            SqlParameter s_cmnd = paramArray[j] as SqlParameter;

                            if (s_cmnd == null)
                                throw new Exception("MS SQL Parameter is not valid!");

                            s_cmd.Parameters.Add(s_cmnd);
                        }

                        s_cmd.CommandText = sSQL;
                        SqlDataAdapter sda = new SqlDataAdapter();
                        DataSet sdsWork = new DataSet();
                        sda.SelectCommand = s_cmd;
                        sda.Fill(sdsWork, sTableName);
                        return sdsWork;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MS SQL Get Data Set!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetDataSet

        /// <summary>
        /// Filling data set
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="dsWork"></param>
        /// <param name="sTableName"></param>
        /// <returns></returns>
        public bool FillDataSet(string sSql, DataSet dsWork, string sTableName)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSql;
                        MySqlDataAdapter da = new MySqlDataAdapter();
                        da.SelectCommand = cmd;
                        da.Fill(dsWork, sTableName);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MY SQL Fill Data Set!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSql;
                        SqlDataAdapter sda = new SqlDataAdapter();
                        sda.SelectCommand = s_cmd;
                        sda.Fill(dsWork, sTableName);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MS SQL Fill Data Set!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // FillDataSet

        /// <summary>
        /// Filling data set
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="paramArray"></param>
        /// <param name="dsWork"></param>
        /// <param name="sTableName"></param>
        /// <returns></returns>
        public bool FillDataSet(string sSql, object[] paramArray, DataSet dsWork, string sTableName)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            MySqlParameter cmnd = paramArray[j] as MySqlParameter;

                            if (cmnd == null)
                                throw new Exception("MY SQL Parameter is not valid!");

                            cmd.Parameters.Add(cmnd);
                        }

                        cmd.CommandText = sSql;
                        MySqlDataAdapter da = new MySqlDataAdapter();
                        da.SelectCommand = cmd;
                        da.Fill(dsWork, sTableName);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MY SQL Fill Data Set!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            SqlParameter s_cmnd = paramArray[j] as SqlParameter;

                            if (s_cmnd == null)
                                throw new Exception("MS SQL Parameter is not valid!");

                            s_cmd.Parameters.Add(s_cmnd);
                        }

                        s_cmd.CommandText = sSql;
                        SqlDataAdapter sda = new SqlDataAdapter();
                        sda.SelectCommand = s_cmd;
                        sda.Fill(dsWork, sTableName);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MS SQL Fill Data Set!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // FillDataSet

        /// <summary>
        /// Data table presenting
        /// </summary>
        /// <param name="sTableName"></param>
        /// <returns></returns>
        public bool TablePresent(string sTableName)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;

                        cmd.CommandText = "SELECT TABLE_NAME FROM information_schema.`TABLES`"
                                          + " WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = '" + sTableName +
                                          "' LIMIT 1";

                        string sRes = (string)cmd.ExecuteScalar();

                        if (sRes == sTableName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MY SQL Table Prasent!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;

                        s_cmd.CommandText = "SELECT TABLE_NAME FROM information_schema.`TABLES`"
                                          + " WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = '" + sTableName +
                                          "' LIMIT 1";

                        string sRes = (string)s_cmd.ExecuteScalar();

                        if (sRes == sTableName)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MS SQL Table Prasent!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // TablePresent

        /// <summary>
        /// Table field present
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="FieldName"></param>
        /// <returns></returns>
        public bool TableFieldPresent(string TableName, string FieldName)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    try
                    {
                        if (TablePresent(TableName))
                        {
                            string sSQL = "SHOW COLUMNS FROM " + TableName + " LIKE '" + FieldName + "'";
                            object drr = null;
                            GetDataReader(sSQL, ref drr);

                            MySqlDataReader dr = drr as MySqlDataReader;

                            if (dr == null)
                                throw new Exception("Type MySqlDataReader is not peresent!");

                            if (dr.Read())
                            {
                                dr.Close();
                                return true;
                            }
                            else
                            {
                                dr.Close();
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not MY SQL Table Field Present executing!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    try
                    {
                        if (TablePresent(TableName))
                        {
                            string sSQL = "SHOW COLUMNS FROM " + TableName + " LIKE '" + FieldName + "'";
                            object sdrr = null;
                            GetDataReader(sSQL, ref sdrr);

                            SqlDataReader sdr = sdrr as SqlDataReader;

                            if (sdr == null)
                                throw new Exception("Type MS SqlDataReader is not peresent!");

                            if (sdr.Read())
                            {
                                sdr.Close();
                                return true;
                            }
                            else
                            {
                                sdr.Close();
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not MS SQL Table Field Present executing!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // TableFieldPresent

        /// <summary>
        /// Get scalar value null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sSql"></param>
        /// <param name="retInsteadOfNullValue"></param>
        /// <returns></returns>
        public T GetScalarValueNull<T>(string sSql, T retInsteadOfNullValue)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to MY SQL database is not existing!");
                    }

                    if (conn.State == ConnectionState.Closed)
                    {
                        return retInsteadOfNullValue;
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = sSql;
                        return GetCommandValue<T>(retInsteadOfNullValue, cmd);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MY SQL Get Scalar Value Null!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    if (s_conn.State == ConnectionState.Closed)
                    {
                        return retInsteadOfNullValue;
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;
                        s_cmd.CommandText = sSql;
                        return GetCommandValue<T>(retInsteadOfNullValue, s_cmd);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing MS SQL Get Scalar Value Null!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetScalarValueNull

        /// <summary>
        /// Getting scalar value null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sSql"></param>
        /// <param name="paramArray"></param>
        /// <param name="retInsteadOfNullValue"></param>
        /// <returns></returns>
        public T GetScalarValueNull<T>(string sSql, object[] paramArray, T retInsteadOfNullValue)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (conn == null)
                    {
                        throw new Exception("Connect to database is not existing!");
                    }

                    if (conn.State == ConnectionState.Closed)
                    {
                        return retInsteadOfNullValue;
                    }

                    try
                    {
                        MySqlCommand cmd = new MySqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            MySqlParameter cmnd = paramArray[j] as MySqlParameter;

                            if (cmnd == null)
                                throw new Exception("Sql parameters is not type valid!");

                            cmd.Parameters.Add((MySqlParameter)paramArray[j]);
                        }

                        cmd.CommandText = sSql;
                        return GetCommandValue<T>(retInsteadOfNullValue, cmd);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing Get Scalar Value Null!");
                    }
                    break;

                case MssqlUse:
                    if (s_conn == null)
                    {
                        throw new Exception("Connect to MS SQL database is not existing!");
                    }

                    if (s_conn.State == ConnectionState.Closed)
                    {
                        return retInsteadOfNullValue;
                    }

                    try
                    {
                        SqlCommand s_cmd = new SqlCommand();
                        s_cmd.Connection = s_conn;
                        s_cmd.CommandType = System.Data.CommandType.Text;

                        for (int j = 0; j < paramArray.Length; j++)
                        {
                            SqlParameter s_cmnd = paramArray[j] as SqlParameter;

                            if (s_cmnd == null)
                                throw new Exception("MS SQL parameters is not type valid!");

                            s_cmd.Parameters.Add((SqlParameter)paramArray[j]);
                        }

                        s_cmd.CommandText = sSql;
                        return GetCommandValue<T>(retInsteadOfNullValue, s_cmd);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing Get Scalar Value Null!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetScalarValueNull

        /// <summary>
        /// Getting command value MySql use
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="retInsteadOfNullValue"></param>
        /// <param name="cmd"></param>
        /// <returns></returns>
        private T GetCommandValue<T>(T retInsteadOfNullValue, MySqlCommand cmd)
        {
            if (cmd == null)
            {
                throw new Exception("Parameter cmd is null!");
            }

            try
            {
                object obj = cmd.ExecuteScalar();

                if (obj == null)
                    return retInsteadOfNullValue;

                Type t = typeof(T);

                if (t.Name == "String")
                    return (T)obj;

                MethodInfo TryParseT = t.GetMethod("TryParse", new Type[] { typeof(string), t.MakeByRefType() });

                if (TryParseT == null)
                    return retInsteadOfNullValue;

                object[] args = { obj.ToString(), null };

                if ((bool)TryParseT.Invoke(null, args))
                {
                    return (T)args[1];
                }
                else
                {
                    return retInsteadOfNullValue;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Is not executing Get Command Value!");
            }
        } // GetCommandValue

        /// <summary>
        /// Getting command value MySql use
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="retInsteadOfNullValue"></param>
        /// <param name="cmd"></param>
        /// <returns></returns>
        private T GetCommandValue<T>(T retInsteadOfNullValue, SqlCommand scmd)
        {
            if (scmd == null)
            {
                throw new Exception("Parameter MS SQL is null!");
            }

            try
            {
                object sobj = scmd.ExecuteScalar();

                if (sobj == null)
                    return retInsteadOfNullValue;

                Type t = typeof(T);

                if (t.Name == "String")
                    return (T)sobj;

                MethodInfo TryParseT = t.GetMethod("TryParse", new Type[] { typeof(string), t.MakeByRefType() });

                if (TryParseT == null)
                    return retInsteadOfNullValue;

                object[] sargs = { sobj.ToString(), null };

                if ((bool)TryParseT.Invoke(null, sargs))
                {
                    return (T)sargs[1];
                }
                else
                {
                    return retInsteadOfNullValue;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Is not executing MS SQL Get Command Value!");
            }
        } // GetCommandValue

        /// <summary>
        /// Return connecting string
        /// </summary>
        public string CS
        {
            get { return _connectionString; }
        } // CS

        /// <summary>
        /// Server Name to connecting
        /// </summary>
        public string ServerName
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        try
                        {
                            using (MySqlConnection conn = new MySqlConnection(_connectionString /*_cs*/))
                            {
                                return conn.DataSource;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Connecting MY SQL is not existing!");
                        }
                        break;

                    case MssqlUse:
                        try
                        {
                            using (SqlConnection s_conn = new SqlConnection(_connectionString /*_cs*/ ))
                            {
                                return s_conn.DataSource;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Connecting MS SQL is not existing!");
                        }
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
        } // ServerName

        /// <summary>
        /// Get new connection
        /// </summary>
        /// <returns></returns>
        //public MySqlConnection GetConnection()
        public object GetConnection()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        return new MySqlConnection(_connectionString /*_cs*/);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not connection to MY SQL data base!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        return new SqlConnection(_connectionString /*_cs*/ );
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not connection to MS SQL data base!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetConnection

        /// <summary>
        /// Get data base name
        /// </summary>
        public string DbName
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        try
                        {
                            using (MySqlConnection conn = new MySqlConnection(_connectionString /*_cs*/))
                            {
                                return conn.Database;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Connection to MY SQL data base is not existing!");
                        }
                        break;

                    case MssqlUse:
                        try
                        {
                            using (SqlConnection sconn = new SqlConnection(_connectionString /*_cs*/ ))
                            {
                                return sconn.Database;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Connection to MS SQL data base is not existing!");
                        }
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
        } // DbName

        /// <summary>
        /// Проверяет соединение с БД + наличие новых объектов
        /// </summary>
        public bool CheckConnection()
        {
            if (String.IsNullOrEmpty(_connectionString /*_cs*/ ))
                return false;

            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    MySqlConnection connection = null;

                    try
                    {
                        connection = new MySqlConnection(_connectionString /*_cs*/);
                        connection.Open();
                        TestNewDataBaseObjects testObj = new TestNewDataBaseObjects(this);

                        if (!testObj.TestIsExistNewObjects())
                        {
                            connection.Close();
                            return false;
                        }

                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        //XtraMessageBox.Show(ex.Message);
                        throw new Exception(ex.Message + "!");
                        return false;
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Dispose();
                    }
                    break;

                case MssqlUse:
                    SqlConnection sconnection = null;

                    try
                    {
                        sconnection = new SqlConnection(_connectionString /*_cs*/);
                        sconnection.Open();
                        TestNewDataBaseObjects testObj = new TestNewDataBaseObjects(this);

                        if (!testObj.TestIsExistNewObjects())
                        {
                            sconnection.Close();
                            return false;
                        }

                        sconnection.Close();
                    }
                    catch (Exception ex)
                    {
                        //XtraMessageBox.Show(ex.Message);
                        throw new Exception(ex.Message + "!");
                        return false;
                    }
                    finally
                    {
                        if (sconnection != null)
                            sconnection.Dispose();
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch

            return true;
        } // CheckConnection

        /// <summary>
        /// Проверяет существование таблицы БД.
        /// </summary>
        /// <param name="tableName">Название таблицы.</param>
        public bool IsTableExist(string tableName)
        {
            bool result = false;

            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand())
                        {
                            using (MySqlConnection connect = new MySqlConnection(_connectionString /*_cs*/))
                            {
                                command.CommandType = System.Data.CommandType.Text;
                                command.Connection = connect;
                                command.CommandText = String.Format(CheckTable, connect.Database, tableName);
                                connect.Open();
                                result = Convert.ToBoolean(command.ExecuteScalar());
                            } // using
                        } // using
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing is MY SQL Table Existing!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        using (SqlCommand s_command = new SqlCommand())
                        {
                            using (SqlConnection s_connect = new SqlConnection(_connectionString /*_cs */))
                            {
                                s_command.CommandType = System.Data.CommandType.Text;
                                s_command.Connection = s_connect;
                                s_command.CommandText = String.Format(msCheckTable, "dbo", tableName);
                                s_connect.Open();
                                result = Convert.ToBoolean(s_command.ExecuteScalar());
                            } // using
                        } // using
                    } // try
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing is MS SQL Table Existing!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //switch

            return result;
        } // IsTableExist

        /// <summary>
        /// Проверяет существование в БД таблицы и определенного столбца.
        /// </summary>
        /// <param name="tableName">Название таблицы.</param>
        /// <param name="columnName">Название столбца.</param>
        public bool IsColumnExist(string tableName, string columnName)
        {
            bool result = false;

            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand())
                        {
                            using (MySqlConnection connect = new MySqlConnection(_connectionString /*_cs*/))
                            {
                                command.CommandType = System.Data.CommandType.Text;
                                command.Connection = connect;
                                command.CommandText = String.Format(CheckColumn, connect.Database, tableName, columnName);
                                connect.Open();
                                result = Convert.ToBoolean(command.ExecuteScalar());
                            } // using
                        } // using
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing is MY SQL Column Exist!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        using (SqlCommand s_command = new SqlCommand())
                        {
                            using (SqlConnection s_connect = new SqlConnection(_connectionString /*_cs*/ ))
                            {
                                s_command.CommandType = System.Data.CommandType.Text;
                                s_command.Connection = s_connect;
                                s_command.CommandText = String.Format(msCheckColumn, "dbo", tableName, columnName);
                                s_connect.Open();
                                result = Convert.ToBoolean(s_command.ExecuteScalar());
                            } // using
                        } // using
                    } // try
                    catch (Exception ex)
                    {
                        throw new Exception("Is not executing is MS SQL Column Exist!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch

            return result;
        } // IsColumnExist

        /// <summary>
        /// Getiing Connection String
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                return _connectionString;
            } // get
            set
            {
                _connectionString = value;
                // сдесь анализ строки подключения
                string nameDataBase = ""; //ConfigurationManager.AppSettings.GetKey(0);

                if (nameDataBase.Equals("MYSQL"))
                {
                    TypeDataBaseUsing = MySqlUse; // switch MySQL Data Base 
                }
                else if (nameDataBase.Equals("MSSQL"))
                {
                    TypeDataBaseUsing = MssqlUse; // switch MSSQL Data Base 
                }
            } // set
        } // ConnectionString

        /// <summary>
        /// Close SQL commands
        /// </summary>
        public void CloseSqlCommand()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        if (command != null)
                        {
                            command.Dispose();
                            command = null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not command my sql dispose!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        if (s_command != null)
                        {
                            s_command.Dispose();
                            s_command = null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not command ms sql dispose!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CloseSqlCommand

        /// <summary>
        /// Close SQL connection
        /// </summary>
        public void SqlConnectionClose()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        if (sqlConnect != null)
                        {
                            if (sqlConnect.State != ConnectionState.Closed)
                            {
                                sqlConnect.Close();
                            }
                            sqlConnect = null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not MY SQL connection close!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        if (s_sqlConnect != null)
                        {
                            if (s_sqlConnect.State != ConnectionState.Closed)
                            {
                                s_sqlConnect.Close();
                            }
                            s_sqlConnect = null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not MS SQL connection close!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SqlConnectionClose

        /// <summary>
        /// Getting SQL parameter array
        /// </summary>
        //public MySqlParameter[] GetSqlParameterArray
        public object[] GetSqlParameterArray
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return parSql;
                        break;

                    case MssqlUse:
                        return s_parSql;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
        } // GetSqlParameterArray

        /// <summary>
        /// Getting SQL commands
        /// </summary>
        public object GetCommand
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return this.command;
                        break;

                    case MssqlUse:
                        return this.s_command;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
            set
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        this.command = (MySqlCommand)value;
                        break;

                    case MssqlUse:
                        this.s_command = (SqlCommand)value;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // set
        } // GetCommand

        /// <summary>
        /// Getting SQL connection
        /// </summary>
        //public MySqlConnection SqlConnection
        public object SqlConnection
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return sqlConnect;
                        break;

                    case MssqlUse:
                        return s_sqlConnect;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
            set
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        sqlConnect = (MySqlConnection)value;
                        break;

                    case MssqlUse:
                        s_sqlConnect = (SqlConnection)value;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // set
        } // SqlConnection

        /// <summary>
        /// Getting connection data base source
        /// </summary>
        public string ConnectionDataSource
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return sqlConnect.DataSource;
                        break;

                    case MssqlUse:
                        return s_sqlConnect.DataSource;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
        } // ConnectionDataSource

        /// <summary>
        /// Getting connection data base
        /// </summary>
        public string ConnectionDataBase
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return sqlConnect.Database;
                        break;

                    case MssqlUse:
                        return s_sqlConnect.Database;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
        } // ConnectionDataBase

        /// <summary>
        /// Getting SQL data reader
        /// </summary>
        //public MySqlDataReader SqlDataReader
        public object SqlDataReader
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return this.data_reader;
                        break;

                    case MssqlUse:
                        return this.s_data_reader;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            }
            set
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        this.data_reader = (MySqlDataReader)value;
                        break;

                    case MssqlUse:
                        this.s_data_reader = (SqlDataReader)value;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // set
        } // SqlDataReader

        /// <summary>
        /// Data reader visible field count
        /// </summary>
        public int DataReaderVisibleFieldCount
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return this.data_reader.VisibleFieldCount;
                        break;

                    case MssqlUse:
                        return this.s_data_reader.VisibleFieldCount;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } //get
        } // DataReaderVisibleFieldCount

        /// <summary>
        /// Getting IAsyncResult data
        /// </summary>
        public IAsyncResult AsyncResult
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return this.asyncResult;
                        break;

                    case MssqlUse:
                        return this.s_asyncResult;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
            set
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        this.asyncResult = value;
                        break;

                    case MssqlUse:
                        this.s_asyncResult = value;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // set
        } // AsyncResult

        /// <summary>
        /// Getting data adapter
        /// </summary>
        public object Adapter
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return this._adapter;
                        break;

                    case MssqlUse:
                        return this.s_adapter;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
            set
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        this._adapter = (MySqlDataAdapter)value;
                        break;

                    case MssqlUse:
                        this.s_adapter = (SqlDataAdapter)value;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } //set
        } // Adapter

        /// <summary>
        /// Getting connection in data adapter
        /// </summary>
        public object Connection
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return this._connection;
                        break;

                    case MssqlUse:
                        return this.s_connection;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
            set
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        this._connection = (MySqlConnection)value;
                        break;

                    case MssqlUse:
                        this.s_connection = (SqlConnection)value;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // set
        } // Connection

        /// <summary>
        /// Getting command collection
        /// </summary>
        public object[] CommandCollection
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return this._commandCollection;
                        break;

                    case MssqlUse:
                        return this.s_commandCollect;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
            set
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        this._commandCollection = (MySqlCommand[])value;
                        break;

                    case MssqlUse:
                        this.s_commandCollect = (SqlCommand[])value;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // set
        } // CommandCollection

        /// <summary>
        /// Getting visible field count
        /// </summary>
        public int VisibleFieldCount
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return this.data_reader.VisibleFieldCount;
                        break;

                    case MssqlUse:
                        return this.s_data_reader.VisibleFieldCount;
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
        } // VisibleFieldCount

        /// <summary>
        /// Getting type Binary
        /// </summary>
        /// <returns></returns>
        public object GettingBinary()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.Binary;
                    break;

                case MssqlUse:
                    return SqlDbType.Binary;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingBinary

        /// <summary>
        /// New SQL array parameters
        /// </summary>
        /// <param name="num"></param>
        public void NewSqlParameterArray(int num)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        numberParameter = num;
                        countParameter = 0;
                        parSql = new MySqlParameter[num];
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not get new MY SQL array parameters!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        numberParameter = num;
                        countParameter = 0;
                        this.s_parSql = new SqlParameter[num];
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not get new MS SQL array parameters!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlParameterArray

        /// <summary>
        /// New SQL parameter
        /// </summary>
        /// <param name="str"></param>
        /// <param name="typeName"></param>
        /// <param name="num"></param>
        public void NewSqlParameter(string str, string typeName, int num)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        parSql[num] = new MySqlParameter(str, typeName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not get new MY SQL parameter!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_parSql[num] = new SqlParameter(str, typeName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not get new MS SQL parameter!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlParameter

        /// <summary>
        /// New SQL parameter
        /// </summary>
        /// <param name="str"></param>
        /// <param name="typeName"></param>
        /// <param name="num"></param>
        public void NewSqlParameter(string str, DateTime typeName, int num)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        this.parSql[num] = new MySqlParameter(str, typeName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not get new MY SQL parameter!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_parSql[num] = new SqlParameter(str, typeName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not get new MS SQL parameter!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlParameter

        /// <summary>
        /// Getting string data reader
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string GetString(string str)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (data_reader != null)
                        return data_reader.GetString(str);
                    else
                        throw new Exception("MY SQL Data reader is null!");
                    break;

                case MssqlUse:
                    if (this.s_data_reader != null)
                    {
                        int k = this.s_data_reader.GetOrdinal(str);
                        return this.s_data_reader.GetString(k); // протестить
                    }
                    else
                    {
                        throw new Exception("MS SQL Data reader is null!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetString

        /// <summary>
        /// Getting Int32 data reader
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public int GetInt32(string str)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader != null)
                        return data_reader.GetInt32(str);
                    else
                        throw new Exception("MY SQL Data reader is null!");
                    break;

                case MssqlUse:
                    if (s_data_reader != null)
                    {
                        int k = this.s_data_reader.GetOrdinal(str); // протестить
                        return s_data_reader.GetInt32(k);
                    }
                    else
                    {
                        throw new Exception("MS SQL Data reader is null!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // return data_reader.GetInt32(str);

        /// <summary>
        /// Closing data reader
        /// </summary>
        public void CloseDataReader()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader != null)
                    {
                        try
                        {
                            this.data_reader.Close();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("MY SQL Data reader is not close!");
                        }
                    }
                    else
                    {
                        throw new Exception("MY SQL Datareader is null!");
                    }
                    break;

                case MssqlUse:
                    if (this.s_data_reader != null)
                    {
                        try
                        {
                            this.s_data_reader.Close();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("MS SQL Data reader is not close!");
                        }
                    }
                    else
                    {
                        throw new Exception("MS SQL Data_reader is null!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CloseDataReader

        public double GetDouble(string str)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader != null)
                        return this.data_reader.GetDouble(str);
                    else
                        throw new Exception("MY SQL Data_reader is null!");
                    break;

                case MssqlUse:
                    if (this.s_data_reader != null)
                    {
                        int k = this.s_data_reader.GetOrdinal(str);
                        return this.s_data_reader.GetDouble(k); // протестить
                    }
                    else
                        throw new Exception("MS SQL Data reader is null!");
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetDouble

        public double GetDouble(int indexInRecord)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (data_reader != null)
                        return this.data_reader.GetDouble(indexInRecord);
                    else
                        throw new Exception("MY SQL Data reader is null!");
                    break;

                case MssqlUse:
                    if (this.s_data_reader != null)
                        return this.s_data_reader.GetDouble(indexInRecord);
                    else
                        throw new Exception("MS SQL Data reader is null!");
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetDouble


        /// <summary>
        /// Getting SQL data reader
        /// </summary>
        /// <returns></returns>
        public object GetSqlDataReader()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return this.data_reader;
                    break;

                case MssqlUse:
                    return this.s_data_reader;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } //  GetSqlDataReader

        /// <summary>
        /// Data reader reading from data base
        /// </summary>
        /// <returns></returns>
        public bool Read()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (data_reader != null)
                    {
                        try
                        {
                            return this.data_reader.Read();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("MY SQL Data reader is not reading!");
                        }
                    }
                    else
                    {
                        throw new Exception("MY SQL Data reader is null!");
                    }
                    break;

                case MssqlUse:
                    if (this.s_data_reader != null)
                    {
                        try
                        {
                            return this.s_data_reader.Read();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("MS SQL Data reader is not reading!");
                        }
                    }
                    else
                    {
                        throw new Exception("MS SQL Data reader is null!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //switch
        } // Read

        /// <summary>
        /// Getting DateTime type
        /// </summary>
        /// <returns></returns>
        public object GettingDateTime()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.DateTime;
                    break;

                case MssqlUse:
                    return SqlDbType.DateTime;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingDateTime

        /// <summary>
        /// Getting String type
        /// </summary>
        /// <returns></returns>
        public object GettingString()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.String;
                    break;

                case MssqlUse:
                    return SqlDbType.Text; // протестить
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingString

        /// <summary>
        /// Setting value to SQL parameter
        /// </summary>
        /// <param name="tm"></param>
        /// <param name="num"></param>
        public void SetSqlParameterValue(DateTime tm, int num)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (parSql != null)
                        parSql[num].Value = tm;
                    else
                        throw new Exception("MY SQL parameters array is null!");
                    break;

                case MssqlUse:
                    if (s_parSql != null)
                        s_parSql[num].Value = tm;
                    else
                        throw new Exception("MS SQL parameters array is null!");
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SetSqlParameterValue

        /// <summary>
        /// Set SQL Parameter Value
        /// </summary>
        /// <param name="str"></param>
        /// <param name="num"></param>
        public void SetSqlParameterValue(string str, int num)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (parSql != null)
                        parSql[num].Value = str;
                    else
                        throw new Exception("MY SQL parameters array is null!");
                    break;

                case MssqlUse:
                    if (s_parSql != null)
                        s_parSql[num].Value = str;
                    else
                        throw new Exception("MS SQL parameters array is null!");
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SetSqlParameterValue

        /// <summary>
        /// Setting SQL parameter value
        /// </summary>
        /// <param name="p"></param>
        /// <param name="num"></param>
        public void SetSqlParameterValue(object p, int num)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (parSql != null)
                        parSql[num].Value = p;
                    else
                        throw new Exception("MY SQL parameters array is null!");
                    break;

                case MssqlUse:
                    if (s_parSql != null)
                        s_parSql[num].Value = p;
                    else
                        throw new Exception("MS SQL parameters array is null!");
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SetSqlParameterValue

        /// <summary>
        /// Getting Int32 type
        /// </summary>
        /// <returns></returns>
        public object GettingInt32()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.Int32;
                    break;

                case MssqlUse:
                    return SqlDbType.Int; // протестить
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingInt32

        /// <summary>
        /// Getting Double type
        /// </summary>
        /// <returns></returns>
        public object GettingDouble()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.Double;
                    break;

                case MssqlUse:
                    return SqlDbType.Real; // протестить
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingDouble

        /// <summary>
        /// Getting Bit type
        /// </summary>
        /// <returns></returns>
        public object GettingBit()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.Bit;
                    break;

                case MssqlUse:
                    return SqlDbType.Bit;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingBit

        /// <summary>
        /// Getting Time type
        /// </summary>
        /// <returns></returns>
        public object GettingTime()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.Time;
                    break;

                case MssqlUse:
                    return SqlDbType.Time;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingTime

        /// <summary>
        /// New SQL parameter
        /// </summary>
        /// <param name="p"></param>
        /// <param name="distance"></param>
        /// <param name="num"></param>
        public void NewSqlParameter(string p, double distance, int num)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        parSql[num] = new MySqlParameter(p, distance);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MY SQL parameter!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_parSql[num] = new SqlParameter(p, distance);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MS SQL parameter!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlParameter

        /// <summary>
        /// New SQL parameter
        /// </summary>
        /// <param name="p"></param>
        /// <param name="mySqlDbType"></param>
        /// <param name="length"></param>
        /// <param name="num"></param>
        public void NewSqlParameter(string p, object mySqlDbType, int length, int num)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        parSql[num] = new MySqlParameter(p, (MySqlDbType)mySqlDbType, length);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MY SQL parameter!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_parSql[num] = new SqlParameter(p, (SqlDbType)mySqlDbType, length);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MS SQL parameter!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlParameter

        /// <summary>
        /// Test data to DbNull
        /// </summary>
        /// <param name="getOrdinal"></param>
        /// <returns></returns>
        public bool IsDbNull(int getOrdinal)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                    {
                        throw new Exception("MY SQL Data Reader is not existing!");
                    }

                    return this.data_reader.IsDBNull(getOrdinal);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                    {
                        throw new Exception("MS SQL Data Reader is not existing!");
                    }

                    return this.s_data_reader.IsDBNull(getOrdinal);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // IsDBNull

        /// <summary>
        /// Getting ordinal data
        /// </summary>
        /// <param name="valid"></param>
        /// <returns></returns>
        public int GetOrdinal(string valid)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return this.data_reader.GetOrdinal(valid);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    return this.s_data_reader.GetOrdinal(valid);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetOrdinal

        /// <summary>
        /// Getting data Int16 type
        /// </summary>
        /// <param name="valid"></param>
        /// <returns></returns>
        public Int16 GetInt16(string valid)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                    {
                        throw new Exception("MY SQL Data Reader is not existing!");
                    }

                    return this.data_reader.GetInt16(valid);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                    {
                        throw new Exception("MS SQL Data Reader is not existing!");
                    }

                    int k = this.s_data_reader.GetOrdinal(valid);
                    return this.s_data_reader.GetInt16(k);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //Switch
        } // GetInt16

        /// <summary>
        /// Getting data Int16 type
        /// </summary>
        /// <param name="valid"></param>
        /// <returns></returns>
        public Int16 GetInt16(int indexInRecord)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                    {
                        throw new Exception("MY SQL Data Reader is not existing!");
                    }

                    return this.data_reader.GetInt16(indexInRecord);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                    {
                        throw new Exception("MS SQL Data Reader is not existing!");
                    }

                    return this.s_data_reader.GetInt16(indexInRecord);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //Switch
        } // GetInt16

        /// <summary>
        /// Getting data DateTime type
        /// </summary>
        /// <param name="datagps"></param>
        /// <returns></returns>
        public DateTime GetDateTime(string datagps)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                    {
                        throw new Exception("MY SQL Data Reader is not existing!");
                    }

                    return this.data_reader.GetDateTime(datagps);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                    {
                        throw new Exception("MS SQL Data Reader is not existing!");
                    }

                    int k = this.s_data_reader.GetOrdinal(datagps);
                    return this.s_data_reader.GetDateTime(k);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // Switch
        } // GetDateTime

        /// <summary>
        /// Getting data DateTime type form data reader
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public DateTime GetDateTime(int param)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                    {
                        throw new Exception("MY SQL Data Reader is not existing!");
                    }

                    return this.data_reader.GetDateTime(param);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                    {
                        throw new Exception("MS SQL Data Reader is not existing!");
                    }

                    return this.s_data_reader.GetDateTime(param);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // Switch
        } // GetDateTime

        /// <summary>
        /// Getting data Float type
        /// </summary>
        /// <param name="speed"></param>
        /// <returns></returns>
        public float GetFloat(string speed)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return this.data_reader.GetFloat(speed);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    int k = this.s_data_reader.GetOrdinal(speed);
                    return this.s_data_reader.GetFloat(k);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetFloat

        /// <summary>
        /// Getting data Boolean type
        /// </summary>
        /// <param name="valid"></param>
        /// <returns></returns>
        public bool GetBoolean(string valid)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return this.data_reader.GetBoolean(valid);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    int k = this.s_data_reader.GetOrdinal(valid);
                    return this.s_data_reader.GetBoolean(k);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetBoolean

        /// <summary>
        /// Getting data unsigned Int64 type
        /// </summary>
        /// <param name="sensor"></param>
        /// <returns></returns>
        public ulong GetUInt64(string sensor)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return this.data_reader.GetUInt64(sensor);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    int k = this.s_data_reader.GetOrdinal(sensor);
                    return (ushort)this.s_data_reader.GetInt64(k); // протестить
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetUInt54

        /// <summary>
        /// Getting data unsigned Int32 type
        /// </summary>
        /// <param name="events"></param>
        /// <returns></returns>
        public uint GetUInt32(string events)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return this.data_reader.GetUInt32(events);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    int k = this.s_data_reader.GetOrdinal(events);
                    return (ushort)this.s_data_reader.GetInt32(k); // протестить
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //switch
        } // GetUInt32

        /// <summary>
        /// Getting data String type
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public string GetString(int p)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return this.data_reader.GetString(p);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    return this.s_data_reader.GetString(p);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetString

        /// <summary>
        /// Getting Int16 type
        /// </summary>
        /// <returns></returns>
        public object GettingInt16()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.Int16;
                    break;

                case MssqlUse:
                    return SqlDbType.SmallInt; // протестить
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingInt16

        /// <summary>
        /// New SQL parameter
        /// </summary>
        /// <param name="parbool"></param>
        /// <param name="mySqlDbType"></param>
        /// <param name="num"></param>
        public void NewSqlParameter(string parbool, bool mySqlDbType, int num)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        this.parSql[num] = new MySqlParameter(parbool, mySqlDbType);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not getting new MY SQL parameter!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_parSql[num] = new SqlParameter(parbool, mySqlDbType);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not getting new MS SQL parameter!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //switch
        } // NewSqlParameter

        /// <summary>
        /// Getting data from data reader on name
        /// </summary>
        /// <param name="namefild"></param>
        /// <returns></returns>
        public object this[string namefild]
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        if (this.data_reader == null)
                            throw new Exception("MY SQL Data Reader is not existing!");

                        return data_reader[namefild];
                        break;

                    case MssqlUse:
                        if (this.s_data_reader == null)
                            throw new Exception("MS SQL Data Reader is not existing!");

                        return s_data_reader[namefild];
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
        }

        /// <summary>
        /// New SQL parameter
        /// </summary>
        /// <param name="iconsmall"></param>
        /// <param name="mySqlDbType"></param>
        /// <param name="num"></param>
        public void NewSqlParameter(string iconsmall, byte[] mySqlDbType, int num)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        this.parSql[num] = new MySqlParameter(iconsmall, mySqlDbType);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MY SQL parameter!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_parSql[num] = new SqlParameter(iconsmall, mySqlDbType);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MS SQL parameter!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlParameter 

        /// <summary>
        /// New SQL parameter and setting it value
        /// </summary>
        /// <param name="p"></param>
        /// <param name="param"></param>
        /// <param name="num"></param>
        public void SetNewSqlParameter(string p, object param)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (numberParameter <= countParameter)
                    {
                        throw new Exception("Number parameter is out of the array boundary!");
                    }

                    try
                    {
                        parSql[countParameter] = new MySqlParameter(p, param);
                        countParameter++;
                    } // try
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MY SQL parameter!");
                    }
                    break;

                case MssqlUse:
                    if (s_numberParameter <= s_countParameter)
                    {
                        throw new Exception("Number parameter is out of the array boundary!");
                    }

                    try
                    {
                        s_parSql[s_countParameter] = new SqlParameter(p, param);
                        s_countParameter++;
                    } // try
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MS SQL parameter!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlParameter

        /// <summary>
        /// New SQL parameter
        /// </summary>
        /// <param name="p"></param>
        /// <param name="types"></param>
        /// <param name="num"></param>
        public void NewSqlParameter(string p, object types, int num)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        parSql[num] = new MySqlParameter(p, (MySqlDbType)types);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MY SQL parameter!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        s_parSql[num] = new SqlParameter(p, (SqlDbType)types);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MS SQL parameter!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlParameter

        /// <summary>
        /// Getting data UInt16 type
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public ushort GetUInt16(string identifier)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return this.data_reader.GetUInt16(identifier);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    int k = this.s_data_reader.GetOrdinal(identifier);
                    return (ushort)this.s_data_reader.GetInt32(k); // протестить
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetUInt16

        /// <summary>
        /// Command the end execute reader
        /// </summary>
        /// <param name="curSqlResult"></param>
        public void CommandEndExecuteReader(IAsyncResult curSqlResult)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        this.data_reader = command.EndExecuteReader(curSqlResult);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("MY SQL is not getting data reader!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_data_reader = s_command.EndExecuteReader(curSqlResult);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandEndExecuteReader

        /// <summary>
        /// Getting value command parameters
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public object GetCommandParameters(int i)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL Command  parameters is not existing!");

                    return command.Parameters[i].Value;
                    break;

                case MssqlUse:
                    if (this.s_command == null)
                        throw new Exception("MS SQL Command  parameters is not existing!");

                    return this.s_command.Parameters[i].Value;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //switch
        } // GetCommandParameters

        /// <summary>
        /// Getting date time from data reader
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public DateTime ReaderGetDateTime(int p)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return this.data_reader.GetDateTime(p);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    return this.s_data_reader.GetDateTime(p);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //switch
        } // ReaderGetDateTime

        /// <summary>
        /// Reading visible field count
        /// </summary>
        /// <returns></returns>
        public int ReaderVisibleFieldCount()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return this.data_reader.VisibleFieldCount;
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    return this.s_data_reader.VisibleFieldCount;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ReaderVisibleFieldCount

        /// <summary>
        /// Get value from data reader
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public object GetValue(int param)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return this.data_reader.GetValue(param);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    return this.s_data_reader.GetValue(param);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetValue

        /// <summary>
        /// Getting values from data reader
        /// </summary>
        /// <param name="data"></param>
        public int ReaderGetValues(object[] data)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return this.data_reader.GetValues(data);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    return this.s_data_reader.GetValues(data);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ReaderGetValues

        /// <summary>
        /// Adding command parameters
        /// </summary>
        /// <param name="mySqlParameter"></param>
        public void CommandParametersAdd(object mySqlParameter)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL Command parameters is not existing!");

                    MySqlParameter cmnd = mySqlParameter as MySqlParameter;

                    if (cmnd == null)
                        throw new Exception("MY SQL parameters is not type valid!");

                    command.Parameters.Add(cmnd);
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL Command parameters is not existing!");

                    SqlParameter scmnd = mySqlParameter as SqlParameter;

                    if (scmnd == null)
                        throw new Exception("MS SQL parameters is not type valid!");

                    s_command.Parameters.Add(scmnd);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandParametersAdd

        /// <summary>
        /// New SQL Command
        /// </summary>
        public void NewSqlCommand()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        command = new MySqlCommand();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MY SQL commands!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        s_command = new SqlCommand();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MS SQL commands!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlCommand

        /// <summary>
        /// Getting SQL command
        /// </summary>
        /// <returns></returns>
        public object GetSqlCommand()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return this.command;
                    break;

                case MssqlUse:
                    return this.s_command;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetSqlCommand

        /// <summary>
        /// Setting command text
        /// </summary>
        /// <param name="cmdtext"></param>
        public void CommandText(string cmdtext)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    command.CommandText = cmdtext;
                    break;

                case MssqlUse:
                    if (this.s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    s_command.CommandText = cmdtext;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandText

        /// <summary>
        /// Setting SQL command connection
        /// </summary>
        public void CommandConnection()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.command == null)
                        throw new Exception("MY SQL command is not existing!");

                    if (this.conn == null)
                        throw new Exception("MY SQL connect is not existing!");

                    command.Connection = this.conn;
                    break;

                case MssqlUse:
                    if (this.s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    if (this.s_conn == null)
                        throw new Exception("MS SQL connect is not existing!");

                    this.s_command.Connection = this.s_conn;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandConnection

        /// <summary>
        /// Setting command connection
        /// </summary>
        /// <param name="connstr"></param>
        public void CommandConnection(string connstr)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    command.Connection = new MySqlConnection(connstr);
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    s_command.Connection = new SqlConnection(connstr);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandConnection

        public void CommandSqlConnection()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    command.Connection = sqlConnect;
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    s_command.Connection = s_sqlConnect;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandSqlConnection

        public void CommandSqlConnection( object connect )
        {
            switch( TypeDataBaseForUsing )
            {
                case MySqlUse:
                    if( command == null )
                        throw new Exception( "MY SQL command is not existing!" );

                    if( connect == null )
                        throw new Exception( "Connect is null!" );

                    command.Connection = connect as MySqlConnection;

                    if(command.Connection == null)
                        throw new Exception("Not MsSqlConnection object type!");
                    break;

                case MssqlUse:
                    if( s_command == null )
                        throw new Exception( "MS SQL command is not existing!" );

                    if( connect == null )
                        throw new Exception( "Connect is null!" );

                    s_command.Connection = connect as SqlConnection;
                    
                    if( s_command.Connection == null )
                        throw new Exception( "Not SqlConnection object type!" );
                    break;

                default:
                    throw new Exception( "Not a valid database type" );
            } // switch
        } // CommandSqlConnection

        /// <summary>
        /// Setting command timeout
        /// </summary>
        /// <param name="i"></param>
        public void CommandTimeout(int i)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    command.CommandTimeout = i;
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    s_command.CommandTimeout = i;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandTimeout

        /// <summary>
        /// Adding parameters into command
        /// </summary>
        /// <param name="mobitelid"></param>
        /// <param name="int32"></param>
        /// <param name="value"></param>
        public void CommandParametersAdd(string mobitelid, object int32, object value)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    command.Parameters.Add(mobitelid, (MySqlDbType)int32).Value = value;
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    s_command.Parameters.Add(mobitelid, (SqlDbType)int32).Value = value;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } //  CommandParametersAdd

        /// <summary>
        /// Command connection open
        /// </summary>
        public void CommandConnectionOpen()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    try
                    {
                        if (command.Connection.State == ConnectionState.Closed)
                            command.Connection.Open();
                        //command.Connection.Open();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    try
                    {
                        if(s_command.Connection.State == ConnectionState.Closed)
                            s_command.Connection.Open();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandConnectionOpen

        /// <summary>
        /// Command Exeute Reader
        /// </summary>
        /// <returns></returns>
        public object CommandExecuteReader()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");
                    if (command.Connection.State == ConnectionState.Open)
                        return command.ExecuteReader();
                    throw new Exception("CommandExecuteReader. Reader error. MS SQL conenction is closing!");
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    if (s_command.Connection.State == ConnectionState.Open)
                        return s_command.ExecuteReader();
                    throw new Exception("CommandExecuteReader. Reader error. MS SQL conenction is closing!");
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandExecuteReader

        /// <summary>
        /// Close command connection
        /// </summary>
        public void CommandConnectionClose()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    command.Connection.Close();
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    s_command.Connection.Close();
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandConnectionClose

        /// <summary>
        /// New sql command
        /// </summary>
        /// <param name="str"></param>
        public void NewSqlCommand(string str)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        command = new MySqlCommand(str);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MY SQL command!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        s_command = new SqlCommand(str);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not getting new MS SQL command!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlCommand

        /// <summary>
        /// New SQL command
        /// </summary>
        /// <param name="str"></param>
        /// <param name="connect"></param>
        public void NewSqlCommand(string str, object connect)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    MySqlConnection cn = connect as MySqlConnection;

                    if (cn == null)
                        throw new Exception("My SQL Connect type is not existing!");

                    command = new MySqlCommand(str, cn);
                    break;

                case MssqlUse:
                    SqlConnection scn = connect as SqlConnection;

                    if (scn == null)
                        throw new Exception("MS SQL Connect type is not existing!");

                    s_command = new SqlCommand(str, scn);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlCommand

        /// <summary>
        /// Getting Command Execute Scalar
        /// </summary>
        /// <returns></returns>
        public object CommandExecuteScalar()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    return command.ExecuteScalar();
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    return s_command.ExecuteScalar();
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandExecuteScalar

        /// <summary>
        /// Getting VarChar type
        /// </summary>
        /// <returns></returns>
        public object GettingVarChar()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.VarChar;
                    break;

                case MssqlUse:
                    return SqlDbType.VarChar;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingVarChar

        /// <summary>
        /// Setting command type
        /// </summary>
        /// <param name="text"></param>
        public void CommandType(CommandType text)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    command.CommandType = text;
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    s_command.CommandType = text;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandType

        /// <summary>
        /// Command Execute Non Query
        /// </summary>
        /// <returns></returns>
        public int CommandExecuteNonQuery()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    return command.ExecuteNonQuery();
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    return s_command.ExecuteNonQuery();
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandExecuteNonQuery

        /// <summary>
        /// Command Update Row Source
        /// </summary>
        /// <param name="firstReturnedRecord"></param>
        public void CommandUpdatedRowSource(UpdateRowSource firstReturnedRecord)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    command.UpdatedRowSource = firstReturnedRecord;
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    s_command.UpdatedRowSource = firstReturnedRecord;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandUpdatedRowSource

        /// <summary>
        /// Command Parameters Clear
        /// </summary>
        public void CommandParametersClear()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    command.Parameters.Clear();
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    s_command.Parameters.Clear();
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandParametersClear

        /// <summary>
        /// Setting command parameters value
        /// </summary>
        /// <param name="i"></param>
        /// <param name="id"></param>
        public void CommandParametersValue(int i, int id)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    command.Parameters[i].Value = id;
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    s_command.Parameters[i].Value = id;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandParametersValue

        public void CommandParametersValue( string name, int id )
        {
            switch( TypeDataBaseForUsing )
            {
                case MySqlUse:
                    if( command == null )
                        throw new Exception( "MY SQL command is not existing!" );

                    command.Parameters[name].Value = id;
                    break;

                case MssqlUse:
                    if( s_command == null )
                        throw new Exception( "MS SQL command is not existing!" );

                    s_command.Parameters[name].Value = id;
                    break;

                default:
                    throw new Exception( "Not a valid database type" );
            } // switch
        } // CommandParametersValue

        public void CommandParametersValue( string name, long id )
        {
            switch( TypeDataBaseForUsing )
            {
                case MySqlUse:
                    if( command == null )
                        throw new Exception( "MY SQL command is not existing!" );

                    command.Parameters[name].Value = id;
                    break;

                case MssqlUse:
                    if( s_command == null )
                        throw new Exception( "MS SQL command is not existing!" );

                    s_command.Parameters[name].Value = id;
                    break;

                default:
                    throw new Exception( "Not a valid database type" );
            } // switch
        } // CommandParametersValue

        public void CommandParametersValue( string name, string str )
        {
            switch( TypeDataBaseForUsing )
            {
                case MySqlUse:
                    if( command == null )
                        throw new Exception( "MY SQL command is not existing!" );

                    command.Parameters[name].Value = str;
                    break;

                case MssqlUse:
                    if( s_command == null )
                        throw new Exception( "MS SQL command is not existing!" );

                    s_command.Parameters[name].Value = str;
                    break;

                default:
                    throw new Exception( "Not a valid database type" );
            } // switch
        } // CommandParametersValue

        public void CommandParametersValue( string name, DateTime tm )
        {
            switch( TypeDataBaseForUsing )
            {
                case MySqlUse:
                    if( command == null )
                        throw new Exception( "MY SQL command is not existing!" );

                    command.Parameters[name].Value = tm;
                    break;

                case MssqlUse:
                    if( s_command == null )
                        throw new Exception( "MS SQL command is not existing!" );

                    s_command.Parameters[name].Value = tm;
                    break;

                default:
                    throw new Exception( "Not a valid database type" );
            } // switch
        } // CommandParametersValue

        /// <summary>
        /// Adding parameters to command
        /// </summary>
        /// <param name="mobitelid"></param>
        /// <param name="gettingInt32"></param>
        public void CommandParametersAdd(string mobitelid, object gettingInt32)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (command == null)
                        throw new Exception("MY SQL command is not existing!");

                    command.Parameters.Add(mobitelid, (MySqlDbType)gettingInt32);
                    break;

                case MssqlUse:
                    if (s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    s_command.Parameters.Add(mobitelid, (SqlDbType)gettingInt32);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandParametersAdd

        /// <summary>
        /// New SQL connection
        /// </summary>
        /// <param name="cs"></param>
        /// <returns></returns>
        public object NewSqlConnection(string cs)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        this.sqlConnect = new MySqlConnection(cs);
                        return this.sqlConnect;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not get new MY SQL connection!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_sqlConnect = new SqlConnection(cs);
                        return this.s_sqlConnect;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not get new MS SQL connection!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlConnection

        /// <summary>
        /// SQL clear all pools
        /// </summary>
        public static void SqlClearAllPools()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    MySqlConnection.ClearAllPools();
                    break;

                case MssqlUse:
                    global::System.Data.SqlClient.SqlConnection.ClearAllPools();
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SqlClearAllPools

        /// <summary>
        /// SQL connection open
        /// </summary>
        public void SqlConnectionOpen()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (sqlConnect == null)
                        throw new Exception("MY SQL connection is not existing!");

                    try
                    {
                        sqlConnect.Open();
                    }
                    catch (Exception exc)
                    {
                        throw new Exception("MS SQL connection is not open!");
                    }
                    break;

                case MssqlUse:
                    if (s_sqlConnect == null)
                        throw new Exception("MS SQL connection is not existing!");

                    try
                    {
                        s_sqlConnect.Open();
                    }
                    catch (Exception exc)
                    {
                        throw new Exception("MS SQL connection is not open!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SqlConnectionOpen

        /// <summary>
        /// SQL connection dispose
        /// </summary>
        public void SqlConnectionDispose()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (sqlConnect == null)
                        throw new Exception("MY SQL connect is not existing!");

                    try
                    {
                        sqlConnect.Dispose();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("MY SQL conection is not dispose!");
                    }
                    break;

                case MssqlUse:
                    if (s_sqlConnect == null)
                        throw new Exception("MS SQL connect is not existing!");

                    try
                    {
                        s_sqlConnect.Dispose();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("MS SQL conection is not dispose!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SqlConnectionDispose

        /// <summary>
        /// SQL connect begining transaction
        /// </summary>
        /// <param name="readCommitted"></param>
        /// <returns></returns>
        public object SqlConnectBeginTransaction(IsolationLevel readCommitted)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (sqlConnect == null)
                        throw new Exception("MY SQL connect is not existing!");

                    transact = sqlConnect.BeginTransaction(readCommitted);
                    return transact;
                    break;

                case MssqlUse:
                    if (s_sqlConnect == null)
                        throw new Exception("MS SQL connect is not existing!");

                    s_transact = s_sqlConnect.BeginTransaction(readCommitted);
                    return s_transact;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SqlConnectBeginTransaction

        /// <summary>
        /// Transaction commiting
        /// </summary>
        public void TransactCommit()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (transact == null)
                        throw new Exception("MY SQL transaction is not existing!");

                    try
                    {
                        transact.Commit();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("MY SQL transaction is not committing!");
                    }
                    break;

                case MssqlUse:
                    if (s_transact == null)
                        throw new Exception("MS SQL transaction is not existing!");

                    try
                    {
                        s_transact.Commit();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("MS SQL transaction is not committing!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // TransactCommit

        /// <summary>
        /// Transaction rolling back
        /// </summary>
        public void TransactRollback()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (transact == null)
                        throw new Exception("MY SQL transaction is not existing!");

                    try
                    {
                        transact.Rollback();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("MY SQL transaction is not rolling back!");
                    }
                    break;

                case MssqlUse:
                    if (s_transact == null)
                        throw new Exception("MS SQL transaction is not existing!");

                    try
                    {
                        s_transact.Rollback();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("MS SQL transaction is not rolling back!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // TransactRollback

        /// <summary>
        /// Data reader get values
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int DataReaderGetValues(object[] data)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL data reader is not existing!");

                    return this.data_reader.GetValues(data);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL data reader is not existing!");

                    return this.s_data_reader.GetValues(data);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // DataReaderGetValues

        /// <summary>
        /// Getting Time Span
        /// </summary>
        /// <param name="timebreak"></param>
        /// <returns></returns>
        public TimeSpan GetTimeSpan(string timebreak)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL data reader is not existing!");

                    return this.data_reader.GetTimeSpan(timebreak);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL data reader is not existing!");

                    int k = this.s_data_reader.GetOrdinal(timebreak);
                    return this.s_data_reader.GetTimeSpan(k); // протестить
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetTimeSpan

        /// <summary>
        /// Getting Time Span
        /// </summary>
        /// <param name="timebreak"></param>
        /// <returns></returns>
        public TimeSpan GetTimeSpan(int indexInRecord)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL data reader is not existing!");

                    return this.data_reader.GetTimeSpan(indexInRecord);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL data reader is not existing!");

                    return this.data_reader.GetTimeSpan(indexInRecord);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetTimeSpan

        /// <summary>
        /// Command creating parameter
        /// </summary>
        /// <returns></returns>
        public object CommandCreateParameter()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.command == null)
                        throw new Exception("MY SQL command is not existing!");

                    return command.CreateParameter();
                    break;

                case MssqlUse:
                    if (this.s_command == null)
                        throw new Exception("MS SQL command is not existing!");

                    return s_command.CreateParameter();
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // CommandCreateParameter

        /// <summary>
        /// Command begin execute reader
        /// </summary>
        /// <returns></returns>
        public IAsyncResult CommandBeginExecuteReader()
        {
            try
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        if (this.command == null)
                            throw new Exception("MY SQL command is not existing!");

                        return command.BeginExecuteReader();
                        break;

                    case MssqlUse:
                        if (this.s_command == null)
                            throw new Exception("MS SQL command is not existing!");

                        return s_command.BeginExecuteReader();
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // try
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        } // CommandBeginExecuteReader

        /// <summary>
        /// New data adatpter
        /// </summary>
        public void NewDataAdapter()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        this._adapter = new global::MySql.Data.MySqlClient.MySqlDataAdapter();
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not getting new MY SQL data adapter!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_adapter = new SqlDataAdapter();
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not getting new MS SQL data adapter!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewDataAdapter

        /// <summary>
        /// Data adapter table mapping adding
        /// </summary>
        /// <param name="tableMapping"></param>
        public void AdapterTableMappingsAdd(DataTableMapping tableMapping)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.TableMappings.Add(tableMapping);
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.TableMappings.Add(tableMapping);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //switch
        } // AdapterTableMappingsAdd

        /// <summary>
        /// New data adapter inserting command
        /// </summary>
        public void AdapterInsertCommand()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("SQL data adapter is not existing!");

                    this._adapter.InsertCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.InsertCommand = new SqlCommand();
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterInsertCommand

        /// <summary>
        /// Data adapter insert command connection
        /// </summary>
        /// <param name="connection"></param>
        public void AdapterInsertCommandConnection(object connection)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.InsertCommand.Connection = (MySqlConnection)connection;
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.InsertCommand.Connection = (SqlConnection)connection;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterInsertCommandConnection

        /// <summary>
        /// Data adapter insert command update row source
        /// </summary>
        /// <param name="firstReturnedRecord"></param>
        public void AdapterInsertCommandUpdatedRowSource(UpdateRowSource firstReturnedRecord)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.InsertCommand.UpdatedRowSource = firstReturnedRecord;
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.InsertCommand.UpdatedRowSource = firstReturnedRecord;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterInsertCommandUpdatedRowSource

        /// <summary>
        /// Data adapter Insert command text
        /// </summary>
        /// <param name="s"></param>
        public void AdapterInsertCommandText(string s)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.InsertCommand.CommandText = s;
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.InsertCommand.CommandText = s;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterInsertCommandText

        /// <summary>
        /// Data adapter insert command type
        /// </summary>
        /// <param name="text"></param>
        public void AdapterInsertCommandType(CommandType text)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.InsertCommand.CommandType = text;
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.InsertCommand.CommandType = text;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterInsertCommandType

        /// <summary>
        /// New SQL parameter
        /// </summary>
        public void NewSqlParameter()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        this.sqlParameter = new MySqlParameter();
                    }
                    catch (Exception e)
                    {
                        throw new Exception("MY SQL parameter is not creating!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_sqlParameter = new SqlParameter();
                    }
                    catch (Exception e)
                    {
                        throw new Exception("MS SQL parameter is not creating!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlParameter

        /// <summary>
        /// Data Adapter insert command parameters add
        /// </summary>
        /// <param name="param"></param>
        public void AdapterInsertCommandParametersAdd(object param)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("SQL data adapter is not existing!");

                    MySqlParameter cmn = param as MySqlParameter;

                    if (cmn == null)
                        throw new Exception("Sql parameter is not existing!");

                    this._adapter.InsertCommand.Parameters.Add(cmn);
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    SqlParameter scmn = param as SqlParameter;

                    if (scmn == null)
                        throw new Exception("MS SQL parameter is not existing!");

                    this.s_adapter.InsertCommand.Parameters.Add(scmn);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterInsertCommandParametersAdd

        /// <summary>
        /// Parameter naming
        /// </summary>
        /// <param name="str"></param>
        public void ParameterName(string str)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.sqlParameter == null)
                        throw new Exception("MY SQL parameter is not existing!");

                    this.sqlParameter.ParameterName = str;
                    break;

                case MssqlUse:
                    if (this.s_sqlParameter == null)
                        throw new Exception("MS SQL parameter is not existing!");

                    this.s_sqlParameter.ParameterName = str;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ParameterName

        /// <summary>
        /// Parameter data base type
        /// </summary>
        /// <param name="dbType"></param>
        public void ParameterDbType(DbType dbType)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.sqlParameter == null)
                        throw new Exception("MY SQL parameter is not existing!");

                    this.sqlParameter.DbType = dbType;
                    break;

                case MssqlUse:
                    if (this.s_sqlParameter == null)
                        throw new Exception("MS SQL parameter is not existing!");

                    this.s_sqlParameter.DbType = dbType;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ParameterDbType

        /// <summary>
        /// Parameter SQL data base type
        /// </summary>
        /// <param name="mySqlDbType"></param>
        public void ParameterSqlDbType(object mySqlDbType)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.sqlParameter == null)
                        throw new Exception("MY SQL parameter is not existing!");

                    this.sqlParameter.MySqlDbType = (MySqlDbType)mySqlDbType;
                    break;

                case MssqlUse:
                    if (this.s_sqlParameter == null)
                        throw new Exception("MS SQL parameter is not existing!");

                    this.s_sqlParameter.SqlDbType = (SqlDbType)mySqlDbType;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ParameterSqlDbType

        /// <summary>
        /// Parameter size
        /// </summary>
        /// <param name="i"></param>
        public void ParameterSize(int i)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.sqlParameter == null)
                        throw new Exception("MY SQL parameter is not existing!");

                    this.sqlParameter.Size = i;
                    break;

                case MssqlUse:
                    if (this.s_sqlParameter == null)
                        throw new Exception("MS SQL parameter is not existing!");

                    this.s_sqlParameter.Size = i;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ParameterSize

        /// <summary>
        /// Check Nullable parameter 
        /// </summary>
        /// <param name="b"></param>
        public void ParameterIsNullable(bool b)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.sqlParameter == null)
                        throw new Exception("SQL parameter is not existing!");

                    this.sqlParameter.IsNullable = b;
                    break;

                case MssqlUse:
                    if (this.s_sqlParameter == null)
                        throw new Exception("MS SQL parameter is not existing!");

                    this.s_sqlParameter.IsNullable = b;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ParameterIsNullable

        /// <summary>
        /// Parameter source column
        /// </summary>
        /// <param name="str"></param>
        public void ParameterSourceColumn(string str)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.sqlParameter == null)
                        throw new Exception("MY SQL parameter is not existing!");

                    this.sqlParameter.SourceColumn = str;
                    break;

                case MssqlUse:
                    if (this.s_sqlParameter == null)
                        throw new Exception("MS SQL parameter is not existing!");

                    this.s_sqlParameter.SourceColumn = str;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ParameterSourceColumn

        /// <summary>
        /// New data adapter update command
        /// </summary>
        public void AdapterUpdateCommand()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.UpdateCommand = new MySqlCommand();
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.UpdateCommand = new SqlCommand();
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterUpdateCommand

        /// <summary>
        /// Data adapter update command connection
        /// </summary>
        /// <param name="connection"></param>
        public void AdapterUpdateCommandConnection(object cnnect)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    MySqlConnection cnn = cnnect as MySqlConnection;

                    if (cnn == null)
                        throw new Exception("Connection is NULL!");

                    this._adapter.UpdateCommand.Connection = cnn;
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    SqlConnection scnn = cnnect as SqlConnection;

                    if (scnn == null)
                        throw new Exception("Connection is NULL!");

                    this.s_adapter.UpdateCommand.Connection = scnn;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterUpdateCommandConnection

        /// <summary>
        /// Data adapter update command text
        /// </summary>
        /// <param name="s"></param>
        public void AdapterUpdateCommandText(string s)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.UpdateCommand.CommandText = s;
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.UpdateCommand.CommandText = s;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterUpdateCommandText

        /// <summary>
        /// Data adapter update command type
        /// </summary>
        /// <param name="text"></param>
        public void AdapterUpdateCommandType(CommandType text)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.UpdateCommand.CommandType = text;
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.UpdateCommand.CommandType = text;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterUpdateCommandType

        /// <summary>
        /// Data adapter update command parameters adding
        /// </summary>
        /// <param name="param"></param>
        public void AdapterUpdateCommandParametersAdd(object param)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    MySqlCommand comm = param as MySqlCommand;

                    if (comm == null)
                        throw new Exception("Command is equal NULL!");

                    this._adapter.UpdateCommand.Parameters.Add(comm);
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    SqlCommand scomm = param as SqlCommand;

                    if (scomm == null)
                        throw new Exception("Command is equal NULL!");

                    this.s_adapter.UpdateCommand.Parameters.Add(scomm);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterUpdateCommandParametersAdd

        /// <summary>
        /// Data adapter update command parameters adding
        /// </summary>
        public void AdapterUpdateCommandParametersAdd()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.UpdateCommand.Parameters.Add(sqlParameter);
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.UpdateCommand.Parameters.Add(s_sqlParameter);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterUpdateCommandParametersAdd

        /// <summary>
        /// SQL parameters source version
        /// </summary>
        /// <param name="original"></param>
        public void ParameterSourceVersion(DataRowVersion original)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.sqlParameter == null)
                        throw new Exception("MY SQL parameter is not existing!");

                    this.sqlParameter.SourceVersion = original;
                    break;

                case MssqlUse:
                    if (this.s_sqlParameter == null)
                        throw new Exception("MS SQL parameter is not existing!");

                    this.s_sqlParameter.SourceVersion = original;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ParameterSourceVersion

        /// <summary>
        /// Data adapter delete command
        /// </summary>
        public void AdapterDeleteCommand()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.DeleteCommand = new MySqlCommand();
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.DeleteCommand = new SqlCommand();
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterDeleteCommand

        /// <summary>
        /// Data adapter delete command connection
        /// </summary>
        /// <param name="connect"></param>
        public void AdapterDeleteCommandConnection(object connect)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("SQL data adapter is not existing!");

                    MySqlConnection cnn = connect as MySqlConnection;

                    if (cnn == null)
                        throw new Exception("Connection is NULL!");

                    this._adapter.DeleteCommand.Connection = cnn;
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    SqlConnection scnn = connect as SqlConnection;

                    if (scnn == null)
                        throw new Exception("Connection is NULL!");

                    this.s_adapter.DeleteCommand.Connection = scnn;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterDeleteCommandConnection

        /// <summary>
        /// Data adapter delete command text
        /// </summary>
        /// <param name="text"></param>
        public void AdapterDeleteCommandText(string text)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.DeleteCommand.CommandText = text;
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.DeleteCommand.CommandText = text;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterDeleteCommandText

        /// <summary>
        /// Data adapter delete command type
        /// </summary>
        /// <param name="text"></param>
        public void AdapterDeleteCommandType(CommandType text)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.DeleteCommand.CommandType = global::System.Data.CommandType.Text;
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.DeleteCommand.CommandType = global::System.Data.CommandType.Text;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterDeleteCommandType

        /// <summary>
        /// Data adapter delete command parameter adding
        /// </summary>
        /// <param name="param"></param>
        public void AdapterDeleteCommandParametersAdd(object param)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception(" MY SQL data adapter is not existing!");

                    MySqlParameter prm = param as MySqlParameter;

                    if (prm == null)
                        throw new Exception("Connection is NULL!");

                    this._adapter.DeleteCommand.Parameters.Add(prm);
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    SqlParameter sprm = param as SqlParameter;

                    if (sprm == null)
                        throw new Exception("Connection is NULL!");

                    this.s_adapter.DeleteCommand.Parameters.Add(sprm);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterDeleteCommandParametersAdd

        /// <summary>
        /// Data adapter delete command parameter adding
        /// </summary>
        public void AdapterDeleteCommandParametersAdd()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("SQL data adapter is not existing!");

                    this._adapter.DeleteCommand.Parameters.Add(sqlParameter);
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.DeleteCommand.Parameters.Add(s_sqlParameter);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } //switch
        } // AdapterDeleteCommandParametersAdd

        /// <summary>
        /// New command collection
        /// </summary>
        /// <param name="i"></param>
        public void NewCommandCollection(int i)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        this._commandCollection = new MySqlCommand[i];
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not create new MY SQL command collection!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_commandCollect = new SqlCommand[i];
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not create new MS SQL command collection!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewCommandCollection

        /// <summary>
        /// Setting new command to command colelction
        /// </summary>
        /// <param name="i"></param>
        public void SetNewCommandCollection(int i)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._commandCollection == null)
                        throw new Exception("MY SQL command collection is not existing!");

                    try
                    {
                        this._commandCollection[i] = new MySqlCommand();
                    }
                    catch (Exception en)
                    {
                        throw new Exception("Is not initial MY SQL command collection!");
                    }
                    break;

                case MssqlUse:
                    if (this.s_commandCollect == null)
                        throw new Exception("MS SQL command collection is not existing!");

                    try
                    {
                        this.s_commandCollect[i] = new SqlCommand();
                    }
                    catch (Exception en)
                    {
                        throw new Exception("Is not initial MS SQL command collection!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SetNewCommandCollection

        /// <summary>
        /// Setting connection to command collection
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="k"></param>
        public void SetCommandCollectionConnection(object conn, int k)
        {
            switch (TypeDataBaseForUsing)
            {

                case MySqlUse:
                    if (this._commandCollection == null)
                        throw new Exception("SQL command collection is not existing!");

                    if (conn == null)
                        throw new Exception("Connection is null!");

                    MySqlConnection cnn = conn as MySqlConnection;
                    if (cnn == null)
                        throw new Exception("MySqlConnection type is not present!");

                    this._commandCollection[k].Connection = cnn;
                    break;

                case MssqlUse:
                    if (this.s_commandCollect == null)
                        throw new Exception("MS SQL command collection is not existing!");

                    if (conn == null)
                        throw new Exception("Connection is null!");

                    SqlConnection scnn = conn as SqlConnection;
                    if (scnn == null)
                        throw new Exception("MS SQL Connection type is not present!");

                    this.s_commandCollect[k].Connection = scnn;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SetCommandCollectionConnection

        /// <summary>
        /// Setting text to command collection
        /// </summary>
        /// <param name="text"></param>
        /// <param name="i"></param>
        public void SetCommandCollectionText(string text, int i)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._commandCollection == null)
                        throw new Exception("MY SQL command collection is not existing!");

                    this._commandCollection[i].CommandText = text;
                    break;

                case MssqlUse:
                    if (this.s_commandCollect == null)
                        throw new Exception("MS SQL command collection is not existing!");

                    this.s_commandCollect[i].CommandText = text;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SetCommandCollectionText

        /// <summary>
        /// Setting command collection type
        /// </summary>
        /// <param name="text"></param>
        /// <param name="i"></param>
        public void SetCommandCollectionType(CommandType text, int i)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._commandCollection == null)
                        throw new Exception("MY SQL command collection is not existing!");

                    this._commandCollection[i].CommandType = text;
                    break;

                case MssqlUse:
                    if (this.s_commandCollect == null)
                        throw new Exception("MS SQL command collection is not existing!");

                    this.s_commandCollect[i].CommandType = text;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // SetCommandCollectionType

        /// <summary>
        /// Data adapter new connection
        /// </summary>
        public void AdapNewConnection()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        this._connection = new global::MySql.Data.MySqlClient.MySqlConnection();
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not create MY SQL data adapter connection!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_connection = new SqlConnection();
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not create data MS SQL adapter connection!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapNewConnection

        /// <summary>
        /// Data adapter insert command parameters adding
        /// </summary>
        public void AdapterInsertCommandParametersAdd()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this._adapter == null)
                        throw new Exception("MY SQL data adapter is not existing!");

                    this._adapter.InsertCommand.Parameters.Add(sqlParameter);
                    break;

                case MssqlUse:
                    if (this.s_adapter == null)
                        throw new Exception("MS SQL data adapter is not existing!");

                    this.s_adapter.InsertCommand.Parameters.Add(s_sqlParameter);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapterInsertCommandParametersAdd

        /// <summary>
        /// Parameter diraction
        /// </summary>
        /// <param name="output"></param>
        public void ParameterDirection(ParameterDirection output)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.sqlParameter == null)
                        throw new Exception("MY SQL command collection is not existing!");

                    this.sqlParameter.Direction = output;
                    break;

                case MssqlUse:
                    if (this.s_sqlParameter == null)
                        throw new Exception("MS SQL command collection is not existing!");

                    this.s_sqlParameter.Direction = output;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // ParameterDirection

        /// <summary>
        /// Parameter source column null mapping
        /// </summary>
        /// <param name="p0"></param>
        public void ParameterSourceColumnNullMapping(bool p0)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.sqlParameter == null)
                        throw new Exception("MY SQL command collection is not existing!");

                    this.sqlParameter.SourceColumnNullMapping = p0;
                    break;

                case MssqlUse:
                    if (this.s_sqlParameter == null)
                        throw new Exception("MS SQL command collection is not existing!");

                    this.s_sqlParameter.SourceColumnNullMapping = p0;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch 
        } // ParameterSourceColumnNullMapping

        /// <summary>
        /// New connection for data adapter
        /// </summary>
        /// <param name="cs"></param>
        public void AdapNewConnection(string cs)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        this._connection = new MySqlConnection(cs);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not create MY SQL data adapter connection!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_connection = new SqlConnection(cs);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Is not create MS SQL data adapter connection!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // AdapNewConnection

        /// <summary>
        /// Getting data in type Int32
        /// </summary>
        /// <param name="ordinal"></param>
        /// <returns></returns>
        public int GetInt32(int ordinal)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data Reader is not existing!");

                    return data_reader.GetInt32(ordinal);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data Reader is not existing!");

                    return this.s_data_reader.GetInt32(ordinal);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GetInt32

        /// <summary>
        /// Load data table from data reader
        /// </summary>
        /// <param name="dt"></param>
        public void LoadDataTable(DataTable dt)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader == null)
                        throw new Exception("MY SQL Data reader is not existing!");

                    if (dt == null)
                        throw new Exception("Data table is not existing!");

                    dt.Load(this.data_reader);
                    break;

                case MssqlUse:
                    if (this.s_data_reader == null)
                        throw new Exception("MS SQL Data reader is not existing!");

                    if (dt == null)
                        throw new Exception("Data table is not existing!");

                    dt.Load(this.s_data_reader);
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // LoadDataTable

        public object GettingDate()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.Date;
                    break;

                case MssqlUse:
                    return SqlDbType.Date;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingDate

        public object GettingBlob()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.Blob;
                    break;

                case MssqlUse:
                    return SqlDbType.Binary;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingBlob

        public object GettingUInt16()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.UInt16;
                    break;

                case MssqlUse:
                    return SqlDbType.Int;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingUInt16

        public object GettingUInt32()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.UInt32;
                    break;

                case MssqlUse:
                    return SqlDbType.Int;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // GettingUInt32
        public object GettingText()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.Text;
                    break;

                case MssqlUse:
                    return SqlDbType.Text;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        }

        public object GettingByte()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    return MySqlDbType.Byte;
                    break;

                case MssqlUse:
                    return SqlDbType.TinyInt;
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        }

        public Int64 GetInt64(string id)
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    if (this.data_reader != null)
                        return data_reader.GetInt64(id);
                    else
                        throw new Exception("MY SQL Data reader is null!");
                    break;

                case MssqlUse:
                    if (s_data_reader != null)
                    {
                        int k = this.s_data_reader.GetOrdinal(id); // протестить
                        return s_data_reader.GetInt64(k);
                    }
                    else
                    {
                        throw new Exception("MS SQL Data reader is null!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        }

        public string ParamPrefics
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "?";
                        break;

                    case MssqlUse:
                        return "@";
                        break;

                    default:
                        throw new Exception("Not a valid database type");
                } // switch
            } // get
        }

        public object SetNewCommand
        {
            get { throw new NotImplementedException(); }

            set {
                switch( TypeDataBaseForUsing )
                {
                    case MySqlUse:
                        this.command = value as MySqlCommand;
                        break;

                    case MssqlUse:
                        this.s_command = value as SqlCommand;
                        break;

                    default:
                        throw new Exception( "Not a valid database type" );
                }
            }
        }

        public object GetSqlParameter
        {
            get
            {
                switch( TypeDataBaseForUsing )
                {
                    case MySqlUse:
                        return this.newParameter;
                        break;

                    case MssqlUse:
                        return this.s_newParameter;
                        break;

                    default:
                        throw new Exception( "Not a valid database type" );
                }
            }

            set { throw new NotImplementedException(); }
        }

        public string GetCommandText
        {
            get
            {
                switch( TypeDataBaseForUsing )
                {
                    case MySqlUse:
                        if( command == null )
                            throw new Exception( "MY SQL command is not existing!" );
                        return this.command.CommandText;
                        break;

                    case MssqlUse:
                        if( s_command == null )
                            throw new Exception( "MS SQL command is not existing!" );
                        return this.s_command.CommandText;
                        break;

                    default:
                        throw new Exception( "Not a valid database type" );
                } // switch
            } // get

            set { throw new NotImplementedException(); }
        }

        public CommandType GetCommandType
        {
            get
            {
                switch( TypeDataBaseForUsing )
                {
                    case MySqlUse:
                        if( command == null )
                            throw new Exception( "MY SQL command is not existing!" );
                        return this.command.CommandType;
                        break;

                    case MssqlUse:
                        if( s_command == null )
                            throw new Exception( "MS SQL command is not existing!" );
                        return this.s_command.CommandType;
                        break;

                    default:
                        throw new Exception( "Not a valid database type" );
                } // switch
            }
            set { throw new NotImplementedException(); }
        }

        public int GetParametersCount
        {
            get
            {
                switch( TypeDataBaseForUsing )
                {
                    case MySqlUse:
                        if( command == null )
                            throw new Exception( "MY SQL command is not existing!" );

                        return this.command.Parameters.Count;
                        break;

                    case MssqlUse:
                        if( s_command == null )
                            throw new Exception( "MS SQL command is not existing!" );
                        return this.s_command.Parameters.Count;
                        break;

                    default:
                        throw new Exception( "Not a valid database type" );
                } // switch
            } // get

            set { throw new NotImplementedException(); }
        }

        public object GetParameters(int i)
        {
            switch( TypeDataBaseForUsing )
            {
                case MySqlUse:
                    if( command == null )
                        throw new Exception( "MY SQL command is not existing!" );

                    return this.command.Parameters[i];
                    break;

                case MssqlUse:
                    if( s_command == null )
                        throw new Exception( "MS SQL command is not existing!" );

                    return this.s_command.Parameters[i];
                    break;

                default:
                    throw new Exception( "Not a valid database type" );
            } // switch
        } // GetParameters

// ParamPrefics
        public object NewSqlConnection()
        {
            switch (TypeDataBaseForUsing)
            {
                case MySqlUse:
                    try
                    {
                        this.sqlConnect = new MySqlConnection(_connectionString);
                        return this.sqlConnect;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not get new MY SQL connection!");
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_sqlConnect = new SqlConnection(_connectionString);
                        return this.s_sqlConnect;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Is not get new MS SQL connection!");
                    }
                    break;

                default:
                    throw new Exception("Not a valid database type");
            } // switch
        } // NewSqlConnection
        public void NewSqlParameter(string messageid, object mySqlDbType)
        {
            switch( TypeDataBaseForUsing )
            {
                case MySqlUse:
                    try
                    {
                        this.newParameter = new MySqlParameter( messageid, ( MySqlDbType ) mySqlDbType );
                    }
                    catch( Exception ex )
                    {
                        throw new Exception( ex.Message + "!" );
                    }
                    break;

                case MssqlUse:
                    try
                    {
                        this.s_newParameter = new SqlParameter( messageid, ( SqlDbType ) mySqlDbType );
                    }
                    catch( Exception ex )
                    {
                        throw new Exception( ex.Message + "!" );
                    }
                    break;

                default:
                    throw new Exception( "Not a valid database type" );
            } // switch
        }

        public long CommandLastInsertedId()
        {
            switch( TypeDataBaseForUsing )
            {
                case MySqlUse:
                    if( command == null )
                        throw new Exception( "MY SQL command is not existing!" );

                    return command.LastInsertedId;
                    break;

                case MssqlUse:
                    if( s_command == null )
                        throw new Exception( "MS SQL command is not existing!" );

                    SqlCommand cmdSelectId = new SqlCommand( "SELECT @@IDENTITY;", this.s_command.Connection );
                    s_command.CommandTimeout = 0;
                    s_command.ExecuteNonQuery(); // Добавляем запись
                    object ido = cmdSelectId.ExecuteScalar();
                    string ids = ido.ToString();
                    if (ids != "")
                    {
                        long id = Convert.ToInt64(ids); // Получает id вставленной записи
                        return id;
                    }

                    return 0;
                    
                    break;

                default:
                    throw new Exception( "Not a valid database type" );
            } // switch
        } // CommandLastInsertedId

        public object GettingInt64()
        {
            switch( TypeDataBaseForUsing )
            {
                case MySqlUse:
                    return MySqlDbType.Int64;
                    break;

                case MssqlUse:
                    return SqlDbType.BigInt;
                    break;

                default:
                    throw new Exception( "Not a valid database type" );
            } // switch
        }

        public void SetCommandTimeout(int time)
        {
            switch( TypeDataBaseForUsing )
            {
                case MySqlUse:
                    this.command.CommandTimeout = time;
                    break;

                case MssqlUse:
                    this.s_command.CommandTimeout = time;
                    break;

                default:
                    throw new Exception( "Not a valid database type" );
            } // switch
        }
    } // DriveDb

    /// <summary>
    /// Class for testing new objects in data base
    /// </summary>
    public class TestNewDataBaseObjects
    {
        DriverDb _dbCom;

        public TestNewDataBaseObjects(DriverDb dbCom)
        {
            _dbCom = dbCom;
        }

        public bool TestIsExistNewObjects()
        {
            string absenceMes;

            if (!_dbCom.IsTableExist("setting"))
            {
                absenceMes = "Отсутствует  таблица setting\n";
            }
            else
            {
                absenceMes = "";
            }

            TestIsExistColumn("setting", "FuelingMinMaxAlgorithm", ref absenceMes);
            TestIsExistTable("mobitels_rotate_bands", ref absenceMes);
            TestIsExistTable("ntf_events", ref absenceMes);
            TestIsExistTable("ntf_log", ref absenceMes);
            TestIsExistTable("ntf_main", ref absenceMes);
            TestIsExistTable("ntf_mobitels", ref absenceMes);
            TestIsExistTable("parameters", ref absenceMes);
            TestIsExistTable("users_list", ref absenceMes);
            TestIsExistTable("users_actions_log", ref absenceMes);
            TestIsExistColumn("zones", "OutLinkId", ref absenceMes);
            TestIsExistColumn("zonesgroup", "OutLinkId", ref absenceMes);
            TestIsExistColumn("driver", "OutLinkId", ref absenceMes);
            TestIsExistColumn("vehicle", "OutLinkId", ref absenceMes);
            TestIsExistColumn("team", "OutLinkId", ref absenceMes);
            TestIsExistColumn("setting", "TimeBreakMaxPermitted", ref absenceMes);
            TestIsExistColumn("users_list", "Id_role", ref absenceMes);
            TestIsExistTable("users_acs_roles", ref absenceMes);
            TestIsExistTable("users_acs_objects", ref absenceMes);
            TestIsExistTable("users_reports_list", ref absenceMes);
            TestIsExistColumn("ntf_log", "Value", ref absenceMes);
            TestIsExistTable("ntf_emails", ref absenceMes);
            TestIsExistColumn("ntf_main", "IsEMailActive", ref absenceMes);
            TestIsExistTable("driver_types", ref absenceMes);
            TestIsExistColumn("driver", "idType", ref absenceMes);
            absenceMes = TestAgroNewObjects(absenceMes);

            if (absenceMes.Length == 0)
            {
                return true;
            }
            else
            {
                //MessageBox.Show(absenceMes, "Запустите патч базы данных!");
                throw new Exception("Запустите патч базы данных!");
                return false;
            }
        } // TestIsExistNewObjects

        private string TestAgroNewObjects(string absenceMes)
        {
            if (_dbCom.IsTableExist("agro_order"))
            {
                TestIsExistTable("agro_agregat_vehicle", ref absenceMes);
                TestIsExistColumn("agro_order", "SquareWorkDescripOverlap", ref absenceMes);
                TestIsExistColumn("agro_order", "FactSquareCalcOverlap", ref absenceMes);
                TestIsExistColumn("agro_fieldgroupe", "Id_Zonesgroup", ref absenceMes);
                TestIsExistColumn("agro_order", "DateLastRecalc", ref absenceMes);
                TestIsExistColumn("agro_order", "UserLastRecalc", ref absenceMes);
                TestIsExistColumn("agro_order", "UserCreated", ref absenceMes);
                TestIsExistColumn("agro_order", "PointsValidity", ref absenceMes);
                TestIsExistColumn("agro_order", "PointsCalc", ref absenceMes);
                TestIsExistColumn("agro_order", "PointsFact", ref absenceMes);
                TestIsExistColumn("agro_order", "PointsIntervalMax", ref absenceMes);
                TestIsExistColumn("agro_ordert", "PointsValidity", ref absenceMes);
                TestIsExistColumn("agro_ordert", "LockRecord", ref absenceMes);
                TestIsExistColumn("agro_order", "StateOrder", ref absenceMes);
                TestIsExistColumn("agro_order", "BlockUserId", ref absenceMes);
                TestIsExistColumn("agro_order", "BlockDate", ref absenceMes);
                TestIsExistColumn("agro_agregat", "OutLinkId", ref absenceMes);
                TestIsExistTable("agro_fueling", ref absenceMes);
                TestIsExistColumn("agro_agregat", "HasSensor", ref absenceMes);
                TestIsExistTable("agro_work_types", ref absenceMes);
                TestIsExistColumn("agro_work", "TypeWork", ref absenceMes);
            } // if

            return absenceMes;
        } // TestAgroNewObjects

        void TestIsExistColumn(string table, string column, ref string mess)
        {
            if (!_dbCom.IsColumnExist(table, column))
            {
                mess = String.Format("{0} Отсутствует поле {2} в таблице {1}\n", mess, table, column);
            }
        }

        void TestIsExistTable(string Table, ref string Mess)
        {
            if (!_dbCom.IsTableExist(Table))
            {
                Mess = String.Format("{0} Отсутствует таблица {1}\n", Mess, Table);
            }
        }
    } // TestNewDataBaseObjects
} // TrackControl.General.DatabaseDriver