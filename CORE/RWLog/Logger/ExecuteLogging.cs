using System;
using System.IO;
using System.Text;

namespace RW_Log
{
  /// <summary>
  /// ������ � ��� ����
  /// </summary>
  public static class ExecuteLogging
  {
    private const string LOG_FORMAT = "{0} {1}";
    private const string LOG_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss:fff";
    private const string DATE_FORMAT = " dd_MM_yyyy";

    /// <summary>
    /// ������ ������� ���� � ����
    /// 0 - ���������
    /// 1 - �������(��������)
    /// 2 - ����
    /// </summary>
    private const string FULL_PATH_FORMAT = @"{0}\{1}{2}.log";

    /// <summary>
    /// ������� �����������. 0 - ��� �����������
    /// </summary>
    public static int loggingLevel = 0;

    /// <summary>
    /// ���-����
    /// </summary>
    private static StreamWriter logWriter;

    private static object syncObject = new Object();

    //private static string 

    /// <summary>
    /// ������ � ���
    /// </summary>
    /// <param name="AppLevel"></param>
    /// <param name="logMessage"></param>
    public static void Log(string AppLevel, string logMessage)
    {
      if (loggingLevel == 1)
      {
        lock (syncObject)
        {
          string fullPath = String.Format(FULL_PATH_FORMAT,
            AppDomain.CurrentDomain.BaseDirectory,
            AppLevel, DateTime.Now.ToString(DATE_FORMAT));

          using (logWriter = new StreamWriter(fullPath, true, Encoding.Default))
          {
            DateTime dt = DateTime.Now;
            logWriter.WriteLine(String.Format("{0} {1}", dt.ToString(LOG_TIME_FORMAT), logMessage));
            logWriter.Flush();
          }
        } // lock
      }
    }

    /// <summary>
    ///������� ���������� log-�����
    /// </summary>
    /// <param name="file"></param>
    public static string DumpLog(string file)
    {
      string strLog = "";
      lock (syncObject)
      {
        using (StreamReader r = File.OpenText(file))
        {
          string line;
          while ((line = r.ReadLine()) != null)
          {
            strLog += line + "\r\n";
            //Console.WriteLine(line);
          }
          r.Close();
        }
      }
      return strLog;
    }
    /*
     //������ ������������� ������� ������� �� ������ log-������
            /// <summary>
            /// ������������� � ������� ���������� ���-�����
            /// (�� ��������)
            /// </summary>
            int logDataHoldDays = 7;
            timerControlLogFile =
                    new Timer(new TimerCallback(ExecuteLogging.DeleteOldLogFile),
                    logDataHoldDays,
                    logDataHoldDays * 60,// * 60 * 1000
                    logDataHoldDays * 60 * 60 * 1000);*/

    /// <summary>
    /// ������� ���������� log-�����
    /// </summary>
    /// <param name="obj"></param>
    public static void DeleteOldLogFile(object obj)
    {
      int period = (int)obj;
      if (period < 0)
        period = 0;
      
      DateTime dtNow = DateTime.Now;
      // ��� ������, �����, �����
      DateTime boundaryDate =
        new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 0, 0, 0) -
        new TimeSpan(period, 0, 0, 0);

      // ���� ����� � ������
      const string search_template = "*.log";
      string[] logFiles = Directory.GetFiles(
        AppDomain.CurrentDomain.BaseDirectory, search_template);

      // ���� ���� ������� - ������
      foreach (string file in logFiles)
      {
        if ((File.GetCreationTime(file) < boundaryDate) ||
          (File.GetLastWriteTime(file) < boundaryDate))
        {
          try
          {
            File.Delete(file);
          }
          catch (Exception e)
          {
            Log("Error_", e.Message + " " + e.StackTrace);
          }
        }
      }
           
    }

  }
}
