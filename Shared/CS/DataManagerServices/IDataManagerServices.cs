﻿using System;
using System.ServiceModel;

namespace Teletrack.DataManagerServices
{
  /// <summary>
  /// Описание объединенного контракта службы TDataManager.
  /// </summary>
  [ServiceContract(Namespace = "Rcs.Teletrack.AppSrv")]
  public interface IDataManagerServices : IConnectionCfgService, IIdentificationCfgService
  {
  }

  /// <summary>
  /// Описание контракта службы настроек подключения.
  /// </summary>
  [ServiceContract(Namespace = "Rcs.Teletrack.AppSrv")]
  public interface IConnectionCfgService
  {
    /// <summary>
    /// Запрос строки подключения к БД.
    /// </summary>
    /// <exception cref="FaultException"></exception>
    /// <returns>Строка подключения к БД.</returns>
    [OperationContract(Action = "RequestCS")]
    string GetDbConnectionString();

    /// <summary>
    /// Запрос строки подключения к серверу лицензий.
    /// </summary>
    /// <exception cref="FaultException"></exception>
    /// <returns>Строка подключения к серверу лицензий.</returns>
    [OperationContract(Action = "RequestLSrvUrl")]
    string GetLicServerUrl();

    /// <summary>
    /// Запрос строки подключения к серверу Robosoft.
    /// </summary>
    /// <exception cref="FaultException"></exception>
    /// <returns>Строка подключения к серверу Robosoft.</returns>
    [OperationContract(Action = "RequestRSrvUrl")]
    string GetRoboMapServerUrl();
  }

  /// <summary>
  /// Описание контракта службы настроек идентификации.
  /// </summary>
  [ServiceContract(Namespace = "Rcs.Teletrack.AppSrv")]
  public interface IIdentificationCfgService
  {
    /// <summary>
    /// Запрос логина диспетчера.
    /// </summary>
    /// <exception cref="FaultException"></exception>
    /// <returns>Логин диспетчера.</returns>
    [OperationContract(Action = "RequestId")]
    string GetDispatcherLogin();
  }
}
