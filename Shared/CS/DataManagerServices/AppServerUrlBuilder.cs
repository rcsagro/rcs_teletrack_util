using System;

namespace Teletrack.DataManagerServices
{
  /// <summary>
  /// ����������� ������ ����������� � ������� ����������(TDataManager).
  /// </summary>
  internal class AppServerUrlBuilder
  {
    private const string APP_SERVER_VIRT_CATALOG = "TT";

    /// <summary>
    /// ������ ����������� ��� TrackControl'��.
    /// </summary>
    /// <param name="address">����� �����������.</param>
    /// <param name="port">���� �����������.</param>
    /// <returns>������ �����������.</returns>
    internal static Uri GetUri(string address, ushort port)
    {
      if (string.IsNullOrEmpty(address) || (address.Trim() == "*"))
      {
        address = System.Environment.MachineName;
      }
      return new UriBuilder(Uri.UriSchemeNetTcp, address, port, APP_SERVER_VIRT_CATALOG).Uri;
    }
  }
}