using System;
using System.Runtime.Serialization;

namespace Teletrack.Licensing.Exceptions
{
  /// <summary>
  /// ���������� ��������� � ������� �����.
  /// </summary>
  [Serializable]
  public class SenselockException : Exception
  {
    public SenselockException()
      : base()
    {
    }

    public SenselockException(string message)
      : base(message)
    {
    }

    public SenselockException(string message, Exception innerException)
      : base(message, innerException)
    {
    }

    public SenselockException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}