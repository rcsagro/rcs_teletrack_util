using System;
using System.Runtime.Serialization;

namespace Teletrack.Licensing.Exceptions
{
  /// <summary>
  /// �� ������� ������ � �����������.
  /// </summary>
  [Serializable]
  public class SessionNotFoundException : Exception
  {
    public SessionNotFoundException()
      : base()
    {
    }

    public SessionNotFoundException(string message)
      : base(message)
    {
    }

    public SessionNotFoundException(string message, Exception innerException)
      : base(message, innerException)
    {
    }

    public SessionNotFoundException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}