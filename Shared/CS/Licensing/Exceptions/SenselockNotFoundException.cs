using System;
using System.Runtime.Serialization;

namespace Teletrack.Licensing.Exceptions
{
  /// <summary>
  /// ���������� ��� ���������� �����
  /// </summary>
  [Serializable]
  public class SenselockNotFoundException : Exception
  {
    public SenselockNotFoundException()
      : base()
    {
    }

    public SenselockNotFoundException(string message)
      : base(message)
    {
    }

    public SenselockNotFoundException(string message, Exception innerException)
      : base(message, innerException)
    {
    }

    public SenselockNotFoundException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}