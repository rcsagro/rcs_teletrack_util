using System;
using System.Runtime.Serialization;

namespace Teletrack.Licensing.Exceptions
{
  /// <summary>
  /// Повторная регистрация логина.
  /// </summary>
  [Serializable]
  public class UniqueLoginException : Exception
  {
    public UniqueLoginException()
      : base()
    {
    }

    public UniqueLoginException(string message)
      : base(message)
    {
    }

    public UniqueLoginException(string message, Exception innerException)
      : base(message, innerException)
    {
    }

    public UniqueLoginException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}