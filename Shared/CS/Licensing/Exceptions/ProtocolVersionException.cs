using System;
using System.Runtime.Serialization;

namespace Teletrack.Licensing.Exceptions
{
  /// <summary>
  /// ���������� ��������� � ������� ���������.
  /// </summary>
  [Serializable]
  public class ProtocolVersionException : Exception
  {
    public ProtocolVersionException()
      : base()
    {
    }

    public ProtocolVersionException(string message)
      : base(message)
    {
    }

    public ProtocolVersionException(string message, Exception innerException)
      : base(message, innerException)
    {
    }

    public ProtocolVersionException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}