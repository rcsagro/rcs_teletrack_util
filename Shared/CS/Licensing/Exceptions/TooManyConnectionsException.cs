using System;
using System.Runtime.Serialization;

namespace Teletrack.Licensing.Exceptions
{
  /// <summary>
  /// ���������� ������������ ���-�� ���������.
  /// </summary>
  [Serializable]
  public class TooManyConnectionsException : Exception
  {
    public TooManyConnectionsException()
      : base()
    {
    }

    public TooManyConnectionsException(string message)
      : base(message)
    {
    }

    public TooManyConnectionsException(string message, Exception innerException)
      : base(message, innerException)
    {
    }

    public TooManyConnectionsException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}