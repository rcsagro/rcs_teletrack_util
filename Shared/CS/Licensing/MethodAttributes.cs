using System;

namespace Teletrack.Licensing
{
  /// <summary>
  /// ��������� �� ����� ������������ ������������� ������.
  /// </summary>
  public class InitSessionAttribute : Attribute
  {
  }

  /// <summary>
  /// ��������� �� ����� ������������ �������� ������.
  /// </summary>
  public class CloseSessionAttribute : Attribute
  {
  }
}