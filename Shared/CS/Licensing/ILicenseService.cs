using System;
using System.ServiceModel;
using Teletrack.Licensing.Exceptions;

namespace Teletrack.Licensing
{
  /// <summary>
  /// ��������� ������������ ������������� ���������� ��������������
  /// </summary>
  [ServiceContract(Namespace = "Rcs.Teletrack.LicSrv")]
  public interface ILicenseService : ISessionControl, ILicensing
  {
  }

  /// <summary>
  /// ��������� ������� � ��������
  /// </summary>
  [ServiceContract(Namespace = "Rcs.Teletrack.LicSrv")]
  public interface ILicensing
  {
    /// <summary>
    /// �������� ��������.
    /// </summary>
    /// <exception cref="FaultException&lt;ProtocolVersionException&gt;">��� ������������ ������ ���������.</exception>
    /// <exception cref="FaultException&lt;SenselockNotFoundException&gt;">�� ������ ���� Senselock.</exception>
    /// <exception cref="FaultException&lt;SenselockException&gt;">�������� ��������� �������� �� �����.</exception>
    /// <exception cref="FaultException&lt;SessionNotFoundException&gt;">�� ������� ������.</exception>
    /// <exception cref="FaultException&lt;MissingMethodException&gt;">�� ������ ����� � ���������.</exception>
    /// <exception cref="FaultException&lt;Exception&gt;">��� ��������� ������.</exception>
    /// <param name="licNumber">����� ��������.</param>
    /// <returns>�������� � ���� ������ ����.</returns>
    [OperationContract(Action = "Lic")]
    [FaultContract(typeof(ProtocolVersionException))]
    [FaultContract(typeof(SessionNotFoundException))]
    [FaultContract(typeof(SenselockNotFoundException))]
    [FaultContract(typeof(MissingMethodException))]
    [FaultContract(typeof(SenselockException))]
    [FaultContract(typeof(Exception))]
    byte[] GetLicense(byte[] licNumber);
  }

  /// <summary>
  /// ��������� ���������� ��������.
  /// </summary>
  [ServiceContract(Namespace = "Rcs.Teletrack.LicSrv")]
  public interface ISessionControl
  {
    /// <summary>
    /// �������� ������.
    /// </summary>
    /// <exception cref="FaultException&lt;ProtocolVersionException&gt;">��� ������������ ������ ���������.</exception>
    /// <exception cref="FaultException&lt;SessionNotFoundException&gt;">�� ������� ������.</exception>
    /// <exception cref="FaultException&lt;SenselockNotFoundException&gt;">�� ������ ���� Senselock.</exception>
    /// <exception cref="FaultException&lt;MissingMethodException&gt;">�� ������ ����� � ���������.</exception>
    /// <exception cref="FaultException&lt;UniqueLoginException&gt;">����� ��� TDataManager'�.
    /// ��������� ������� � ������������������ �������.</exception>
    /// <exception cref="FaultException&lt;TooManyConnectionsException&gt;">����� ��� TrackControl'�.
    /// ��������� ������ ����������� ������.</exception>
    /// <exception cref="FaultException&lt;SenselockException&gt;">����� ��� TrackControl'�.
    /// �������� � ���������: �� ������ ������, ��. ������.</exception>
    /// <exception cref="FaultException&lt;Exception&gt;">��� ��������� ������.</exception>
    /// <param name="login">����� ����������.</param>
    [OperationContract(Action = "Open")]
    [FaultContract(typeof(ProtocolVersionException))]
    [FaultContract(typeof(SessionNotFoundException))]
    [FaultContract(typeof(SenselockNotFoundException))]
    [FaultContract(typeof(MissingMethodException))]
    [FaultContract(typeof(UniqueLoginException))]
    [FaultContract(typeof(TooManyConnectionsException))]
    [FaultContract(typeof(SenselockException))]
    [FaultContract(typeof(Exception))]
    [InitSession]
    void OpenSession(string login);

    /// <summary>
    /// �������� ������.
    /// </summary>
    [OperationContract(Action = "Close", IsOneWay = true)]
    [CloseSession]
    void CloseSession();

    /// <summary>
    /// ����������� � ��� ��� ������ ��� ����� �������.
    /// </summary>
    /// <exception cref="FaultException&lt;ProtocolVersionException&gt;">��� ������������ ������ ���������.</exception>
    /// <exception cref="FaultException&lt;SessionNotFoundException&gt;">�� ������� ������.</exception>
    /// <exception cref="FaultException&lt;MissingMethodException&gt;">�� ������ ����� � ���������.</exception>
    /// <exception cref="FaultException&lt;Exception&gt;">��� ��������� ������.</exception>
    [OperationContract(Action = "Notify")]
    [FaultContract(typeof(SenselockException))]
    [FaultContract(typeof(SenselockNotFoundException))]
    [FaultContract(typeof(ProtocolVersionException))]
    [FaultContract(typeof(SessionNotFoundException))]
    [FaultContract(typeof(MissingMethodException))]
    [FaultContract(typeof(Exception))]
    void NotifySessionAlive();
  }
}