using System;
using System.ServiceModel;
using System.Timers;
using Teletrack.Licensing.Exceptions;

namespace Teletrack.Licensing
{
  internal class LicClientSeviceBase
  {
    internal event Action<Exception, string> ExceptionOccurred;

    /// <summary>
    /// ������ ������������ ����� �������� ����������� � ���, ��� ������ ����.
    /// </summary>
    private Timer _timer;

    private volatile bool shouldStop;

    /// <summary>
    /// ������������� ������.
    /// </summary>
    protected string SessionId
    {
      get;
      private set;
    }

    protected readonly object SyncObject = new object();

    internal bool IsSessionNotOpened
    {
      get
      {
        return string.IsNullOrEmpty(SessionId);
      }
    }

    public LicClientSeviceBase()
    {
      SetSessionIdEmpty();
      CreateTimer();
    }

    /// <summary>
    /// �������� �������: 1 ���.
    /// </summary>
    private const int TIMER_INTERVAL = 1 * 60 * 1000;

    private void CreateTimer()
    {
      _timer = new Timer(TIMER_INTERVAL);
      _timer.AutoReset = false;
      _timer.Elapsed += OnTimerElapsed;
    }

    private void OnTimerElapsed(object sender, ElapsedEventArgs e)
    {
      try
      {
        if (IsSessionNotOpened)
        {
          OpenSessionSafe();
        }
        else
        {
          NotifySessionAlive();
        }
      }
      finally
      {
        TimerStart();
      }
    }

    protected void TimerStart()
    {
      if (!shouldStop)
      {
        _timer.Start();
      }
    }


    protected void TimerStop()
    {
      _timer.Stop();
    }

    internal void Start()
    {
      shouldStop = false;
      OpenSession();
    }

    internal void Stop()
    {
      shouldStop = true;
      CloseSession();
    }

    /// <summary>
    /// �������� ������.
    /// </summary>
    protected virtual void OpenSession()
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// ���������� �������� ������ - ������ ��������� � �����������.
    /// </summary>
    protected virtual void OpenSessionSafe()
    {
      try
      {
        OpenSession();
      }
      catch (FaultException<SenselockNotFoundException> ex)
      {
        WriteExceptionToLog(ex.Message);
        NotifyExceptionOccurred(ex, ex.Message);
      }
      catch (FaultException<SenselockException> ex)
      {
        WriteExceptionToLog(ex.Message);
        NotifyExceptionOccurred(
          new FaultException<SenselockNotFoundException>(new SenselockNotFoundException()),
          ex.Message);
      }
      catch (Exception ex)
      {
        string message = string.Format("Error: open session failure: {0}", ex.Message);
        WriteExceptionToLog(message);
        NotifyExceptionOccurred(ex, message);
      }
    }

    /// <summary>
    /// �������� ������.
    /// </summary>
    protected virtual void CloseSession()
    {
      lock (SyncObject)
      {
        TimerStop();

        if (IsSessionNotOpened)
        {
          return;
        }

        try
        {
          using (LicClientProxy proxy = CreateProxy())
          {
            proxy.CloseSession();
          }
        }
        catch (Exception ex)
        {
          WriteExceptionToLog(string.Format("Warning: close session failure: {0}", ex.Message));
        }
        finally
        {
          SetSessionIdEmpty();
        }
      }
    }

    /// <summary>
    /// �������� ����������� � ���, ��� �� ��� ��������.
    /// </summary>
    protected virtual void NotifySessionAlive()
    {
      lock (SyncObject)
      {
        try
        {
          using (LicClientProxy proxy = CreateProxy())
          {
            proxy.NotifySessionAlive();
          }
        }
        catch (CommunicationObjectFaultedException ex)
        {
          SetSessionIdEmpty();
          string message = string.Format(
            "Cannot establish a connection to the license server: {0}", ex.Message);
          WriteExceptionToLog(message);
          NotifyExceptionOccurred(ex, message);
        }
        catch (FaultException<SenselockNotFoundException> ex)
        {
          CloseSession();
          string message = string.Format(
            "The license server cannot find the SenseLock key: {0}", ex.Message);
          WriteExceptionToLog(message);
          NotifyExceptionOccurred(
            new FaultException<SenselockException>(new SenselockException()),
            message);
        }
        catch (FaultException<SenselockException> ex)
        {
          string message = string.Format(
            "The license server cannot read the key: {0}", ex.Message);
          WriteExceptionToLog(message);
          NotifyExceptionOccurred(ex, message);
        }
        catch (FaultException<SessionNotFoundException> ex)
        {
          SetSessionIdEmpty();
          string message = string.Format(
            "Error: sending notification to the license server: {0}", ex.Message);
          WriteExceptionToLog(message);
        }
        catch (Exception ex)
        {
          string message = string.Format(
            "Error: sending notification to the license server: {0}", ex);
          WriteExceptionToLog(message);
          NotifyExceptionOccurred(ex, message);
        }
      }
    }

    /// <summary>
    /// �������� ��������.
    /// </summary>
    /// <exception cref="FaultException<SessionNotFoundException>"></exception>
    /// <param name="licNumber">����� ��������.</param>
    /// <returns>�������� � ���� ������� ����.</returns>
    internal virtual byte[] GetLicense(int licNumber)
    {
      lock (SyncObject)
      {
        try
        {
          TimerStop();
          if (IsSessionNotOpened)
          {
            OpenSession();
          }
          using (LicClientProxy proxy = CreateProxy())
          {
            byte[] reqLic = new byte[5];
            BitConverter.GetBytes(licNumber).CopyTo(reqLic, 0);
            return proxy.GetLicense(reqLic);
          }
        }
        catch (FaultException<SessionNotFoundException> ex)
        {
          SetSessionIdEmpty();
          throw ex;
        }
        finally
        {
          TimerStart();
        }
      }
    }

    /// <summary>
    /// ��������� ������ �������� �������������� ������ (SessionId).
    /// </summary>
    protected void GenerateSessionId()
    {
      SessionId = Guid.NewGuid().ToString();
    }

    /// <summary>
    /// ��������� �������� �������������� ������ (SessionId).
    /// </summary>
    protected void SetSessionIdEmpty()
    {
      SessionId = string.Empty;
    }

    protected LicClientProxy CreateProxy()
    {
      return new LicClientProxy(
        new NetTcpContextBinding(),
        new EndpointAddress(new Uri(GetLicServerUrl())),
        SessionId);
    }

    /// <summary>
    /// ������ ������ ����������� � ������� ��������. 
    /// </summary>
    /// <returns>������ ����������� � ������� ��������.</returns>
    protected virtual string GetLicServerUrl()
    {
      throw new NotImplementedException();
    }

    protected void NotifyExceptionOccurred(Exception ex, string message)
    {
      if (ExceptionOccurred != null)
      {
        ExceptionOccurred(ex, message);
      }
    }

    protected virtual void WriteExceptionToLog(string message)
    {
      throw new NotImplementedException();
    }
  }
}