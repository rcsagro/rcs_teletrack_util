namespace Teletrack.Licensing
{
  /// <summary>
  /// ������ ��������.
  /// </summary>
  public static class LicenseNumbers
  {
  /// <summary>
    /// �������� ��� ���-�� TDataMngr
  /// </summary>
    public const int MAX_DATA_MGR = 1;
    /// <summary>
    /// �������� ��� ���-�� ����-���������
    /// </summary>
    public const int MAX_TRACK_CTR = 2;
    /// <summary>
    /// �������� ������� ����
    /// </summary>
    public const int AGRO = 3;
    /// <summary>
    /// �������� ������� ���������
    /// </summary>
    public const int ROUTE = 4;
  }
}