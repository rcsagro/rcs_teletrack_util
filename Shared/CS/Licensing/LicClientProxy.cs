﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using Teletrack.Licensing;
using Teletrack.WcfServices;

namespace Teletrack.Licensing
{
  internal class LicClientProxy : AbstractContextClient<ILicenseService>, ILicenseService
  {
    public LicClientProxy(Binding binding, EndpointAddress remoteAddress, string sessionId)
      : base(binding, remoteAddress, sessionId)
    {
    }

    #region ISessionControl Members

    public void OpenSession(string login)
    {
      Channel.OpenSession(login);
    }

    public void CloseSession()
    {
      Channel.CloseSession();
    }

    public void NotifySessionAlive()
    {
      Channel.NotifySessionAlive();
    }

    #endregion

    #region ILicensing Members

    public byte[] GetLicense(byte[] licNumber)
    {
      return Channel.GetLicense(licNumber);
    }

    #endregion
  }
}
