using System;

namespace Teletrack.Licensing
{
  /// <summary>
  /// ����������� ������ ����������� � ������� ��������.
  /// </summary>
  internal class LicServerUrlBuilder
  {
    /// <summary>
    /// ����������� ������� ��� TrackControl-��: Lic/TC.
    /// </summary>
    internal const string TC_VIRT_CATALOG = "Lic/TC";

    /// <summary>
    /// ����������� ������� ������� ��������: Lic/RS.
    /// </summary>
    internal const string RS_VIRT_CATALOG = "Lic/RS";

    /// <summary>
    /// ����������� ������� ��� TDataManager-��: Lic/TDM.
    /// </summary>
    internal const string TDM_VIRT_CATALOG = "Lic/TDM";

    /// <summary>
    /// ������ ����������� � ������� �������� ��� TrackControl'��.
    /// </summary>
    /// <param name="address">����� �����������.</param>
    /// <param name="port">���� �����������.</param>
    /// <returns>������ �����������.</returns>
    internal static Uri GetSrvUriForTc(string address, ushort port)
    {
      ValidateAddress(ref address);
      return new UriBuilder(Uri.UriSchemeNetTcp, address, port, TC_VIRT_CATALOG).Uri;
    }

    /// <summary>
    /// ������ ����������� � ������� Robosoft ��� TrackControl'��.
    /// </summary>
    /// <param name="address">����� �����������.</param>
    /// <param name="port">���� �����������.</param>
    /// <returns>������ �����������.</returns>
    internal static Uri GetRoboUriForTc(string address, ushort port)
    {
      ValidateAddress(ref address);
      return new UriBuilder(Uri.UriSchemeHttp, address, port, RS_VIRT_CATALOG).Uri;
    }

    /// <summary>
    /// ������ ����������� � ������� �������� ��� TDataManager'��.
    /// </summary>
    /// <param name="address">����� �����������.</param>
    /// <param name="port">���� �����������.</param>
    /// <returns>������ �����������.</returns>
    internal static Uri GetUriForTdm(string address, ushort port)
    {
      ValidateAddress(ref address);
      return new UriBuilder(Uri.UriSchemeNetTcp, address, port, TDM_VIRT_CATALOG).Uri;
    }

    private static void ValidateAddress(ref string address)
    {
      if ((string.IsNullOrEmpty(address)) || (address.Trim() == "*"))
      {
        address = System.Environment.MachineName;
      }
    }
  }
}