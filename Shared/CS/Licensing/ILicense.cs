namespace Teletrack.Licensing
{
  /// <summary>
  /// ��������� ������� � ��������
  /// </summary>
  internal interface ILicense
  {
    /// <summary>
    /// �������� �������� � ���� ������� ����.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������ � ������� ��������.</exception>"
    /// <exception cref="SenselockNotFoundException">�� ������ ���� Senselock.</exception>
    /// <exception cref="SenselockException">�������� ��������� �������� �� �����.</exception>
    /// <param name="licNumber">����� ��������.</param>
    /// <returns>�������� � ���� ������ ����.</returns>
    byte[] GetLicense(byte[] licNumber);
    /// <summary>
    /// �������� �������� ���������� ������������� ��������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ������ � ������� ��������.</exception>"
    /// <exception cref="SenselockNotFoundException">�� ������ ���� Senselock.</exception>
    /// <param name="licNumber">����� ��������.</param>
    /// <returns>�������� � ���� ������ ����.</returns>
    int GetLicense(int licNum);
  }
}