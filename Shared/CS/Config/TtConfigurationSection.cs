﻿using System;
using System.Configuration;

namespace Teletrack.Config
{
  /// <summary>
  /// Базовый класс для секционных конфигураторов.
  /// </summary>
  public class TtConfigurationSection : ConfigurationSection
  {
    protected override void PostDeserialize()
    {
      base.PostDeserialize();
      Validate();
    }

    /// <summary>
    /// Валидация настроек.
    /// </summary>
    protected virtual void Validate()
    {
    }

    /// <summary>
    /// Создает экземпляр объекта-наследника ConfigurationSection.
    /// </summary>
    /// <exception cref="ConfigurationErrorsException"></exception>
    /// <param name="sectionName">Наименование секции в файле конфигурации.</param>
    /// <returns>ConfigurationSection as object.</returns>
    protected static object GetConfigSection(string sectionName)
    {
      try
      {
        return ConfigurationManager.GetSection(sectionName);
      }
      catch (Exception ex)
      {
        throw new ConfigurationErrorsException(string.Format(
          "Configuration section \"{0}\" has not been loaded.\r\n{1}",
          sectionName, ex.Message),
          ex);
      }
    }

    /// <summary>
    /// Сохранить значение элемента секции конфигурационного файла. 
    /// </summary>
    /// <param name="sectionName">Наименование секции.</param>
    /// <param name="propertyName">Наименование элемента.</param>
    /// <param name="value">Значение элемента.</param>
    protected static void SaveProperty(string sectionName, string propertyName, object value)
    {
      try
      {
        Configuration exeConfiguration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        ConfigurationSection diagnosticsSection = exeConfiguration.GetSection(sectionName);

        diagnosticsSection.ElementInformation.Properties[propertyName].Value = value;
        exeConfiguration.Save(ConfigurationSaveMode.Modified, false);
      }
      catch (Exception ex)
      {
        throw new ConfigurationErrorsException(string.Format(
          "Configuration property \"{0}\" of section \"{1}\" has not been saved, value: {2} \r\n{3}",
          propertyName, sectionName, value, ex.Message),
          ex);
      }
    }
  }
}
