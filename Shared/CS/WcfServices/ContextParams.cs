using System;

namespace Teletrack.WcfServices
{
  /// <summary>
  /// ����������� ����������. 
  /// </summary>
  internal static class ContextParams
  {
    /// <summary>
    /// ������������ ��������� � ��������������� �����: SessionId.
    /// </summary>
    internal const string SessionIdParamName = "SessionId";

    /// <summary>
    /// ������������ ��������� � ������� ���������: ProtVer.
    /// </summary>
    internal const string ProtocolVersionParamName = "ProtVer";
    /// <summary>
    /// ������ ���������: 1.0.0.0.
    /// </summary>
    internal static readonly Version ProtocolVersion = new Version(1, 0, 0, 0);
  }
}