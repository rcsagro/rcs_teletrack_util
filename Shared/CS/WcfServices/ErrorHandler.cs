using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using Teletrack.Licensing.Exceptions;
using Teletrack.Logging;

namespace Teletrack.WcfServices
{
  /// <summary>
  /// ���������� ������. ��������������� "�������" ���������� � FaultException
  /// ��� �������� �������.
  /// </summary>
  public class ErrorHandler : IErrorHandler
  {

    #region IErrorHandler Members

    public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
    {
      Type type = error.GetType();

      fault =
        type == typeof(SenselockNotFoundException) ? CreateMessage<SenselockNotFoundException>(error, version) :
        type == typeof(SenselockException) ? CreateMessage<SenselockException>(error, version) :
        type == typeof(SessionNotFoundException) ? CreateMessage<SessionNotFoundException>(error, version) :
        type == typeof(ProtocolVersionException) ? CreateMessage<ProtocolVersionException>(error, version) :
        type == typeof(MissingMethodException) ? CreateMessage<MissingMethodException>(error, version) :
        type == typeof(TooManyConnectionsException) ? CreateMessage<TooManyConnectionsException>(error, version) :
        type == typeof(UniqueLoginException) ? CreateMessage<UniqueLoginException>(error, version) :
          CreateMessage<Exception>(error, version);
    }

    public bool HandleError(Exception error)
    {
      LogHelper.Error(NeedPreciseLogging(error.GetType()) ? error.ToString() : error.Message);
      return true;
    }

    #endregion

    private bool NeedPreciseLogging(Type errorType)
    {
      return errorType != typeof(SenselockNotFoundException) &&
        errorType != typeof(SenselockException) &&
        errorType != typeof(SessionNotFoundException) &&
        errorType != typeof(ProtocolVersionException) &&
        errorType != typeof(MissingMethodException) &&
        errorType != typeof(TooManyConnectionsException) &&
        errorType != typeof(UniqueLoginException);
    }

    private static Message CreateMessage<T>(Exception error, MessageVersion version) where T : Exception
    {
      FaultException<T> ex = new FaultException<T>(
        (T)Activator.CreateInstance(typeof(T), new object[1] { error.Message }),
        error.Message);
      return Message.CreateMessage(version, ex.CreateMessageFault(), ex.Action);
    }
  }
}

