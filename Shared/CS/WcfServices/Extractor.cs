using System;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using Teletrack.Licensing.Exceptions;

namespace Teletrack.WcfServices
{
  /// <summary>
  /// ��������� �������� ����������: 
  /// ����� ������� �� ���������, ������������ ������ �� ��������� ��������� � �.�. 
  /// </summary>
  internal class Extractor
  {
    private const string UNKNOWN_PROTOCOL =
      "Unknown protocol. Check that your software is up to date and you have the latest version.";

    /// <summary>
    /// ���������� �������������� ������ �� ���������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� OperationContext.</exception>
    /// <exception cref="ProtocolVersionException">����������� �������� � ��������������� ������.</exception>
    /// <exception cref="SessionNotFoundException">�� �������� ������������� ������.</exception>
    /// <param name="context">OperationContext.</param>
    /// <returns>������������� ������.</returns>
    internal static string GetSessionIdFromContext(OperationContext context)
    {
      if (context == null)
      {
        throw new ArgumentNullException("context");
      }

      ContextMessageProperty contextProperty =
        context.IncomingMessageProperties[ContextMessageProperty.Name] as ContextMessageProperty;

      if (!contextProperty.Context.ContainsKey(ContextParams.SessionIdParamName))
      {
        throw new ProtocolVersionException(UNKNOWN_PROTOCOL);
      }

      string result = contextProperty.Context[ContextParams.SessionIdParamName];
      if (string.IsNullOrEmpty(result))
      {
        throw new SessionNotFoundException("Session identifier is empty.");
      }
      return result;
    }

    /// <summary>
    /// ���������� ������ ��������� �� ���������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� OperationContext.</exception>
    /// <exception cref="ProtocolVersionException">����������� �������� � ������� ���������
    /// ��� ������ � ������������ �������.</exception>
    /// <param name="context">OperationContext.</param>
    /// <returns>������ ���������.</returns>
    internal static Version GetProtocolVersionFromContext(OperationContext context)
    {
      if (context == null)
      {
        throw new ArgumentNullException("context");
      }

      ContextMessageProperty contextProperty =
        context.IncomingMessageProperties[ContextMessageProperty.Name] as ContextMessageProperty;

      if (!contextProperty.Context.ContainsKey(ContextParams.ProtocolVersionParamName))
      {
        throw new ProtocolVersionException(UNKNOWN_PROTOCOL);
      }

      Version result;
      try
      {
        result = new Version(contextProperty.Context[ContextParams.ProtocolVersionParamName]);
      }
      catch (Exception ex)
      {
        throw new ProtocolVersionException(string.Format(
          "Protocol version parsing failure: {0}", ex.Message));
      }
      return result;
    }

    /// <summary>
    /// ���������� ������ ������� �� ���������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� OperationContext.</exception>
    /// <param name="context">��������.</param>
    /// <returns>IPEndPoint.</returns>
    internal static IPEndPoint GetClientIpFromContext(OperationContext context)
    {
      if (context == null)
      {
        throw new ArgumentNullException("context");
      }
      RemoteEndpointMessageProperty clientEndpoint =
        context.IncomingMessageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
      return new IPEndPoint(IPAddress.Parse(clientEndpoint.Address), clientEndpoint.Port);
    }

    /// <summary>
    /// ���������� ������������ ������ �� ��������� ��������� Action.
    /// ����������� ������ ������������ ������ ��� ���������� � ����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� MessageHeaders.</exception>
    /// <exception cref="ProtocolVersionException">����������� ��������� Action.</exception>
    /// <param name="headers">��������� ���������.</param>
    /// <returns>������������ ������.</returns>
    internal static string GetMethodNameFromActionHeader(MessageHeaders headers)
    {
      if (headers == null)
      {
        throw new ArgumentNullException("headers");
      }

      string actionHeader = headers.Action;
      if (string.IsNullOrEmpty(actionHeader))
      {
        throw new ProtocolVersionException("Missed Action header.");
      }

      return actionHeader.Contains("/") ?
        actionHeader.Substring(actionHeader.LastIndexOf("/") + 1) : actionHeader;
    }

    /// <summary>
    /// ����� ��������� ����� ������� ������� ���������.
    /// </summary>
    /// <param name="context">OperationContext.</param>
    /// <returns>ServiceEndpoint.</returns>
    internal static ServiceEndpoint GetServiseEndpoint(OperationContext context)
    {
      if (context == null)
      {
        throw new ArgumentNullException("context");
      }
      ServiceDescription hostDesc = context.Host.Description;
      ServiceEndpoint result = hostDesc.Endpoints.Find(context.IncomingMessageHeaders.To);

      if (result == null)
      {
        // ��� ��������� ����� ����������, ��������, ����� � �������� ������ �������
        // ������������ ��� ����������, � ������� ����������� �� localhost.
        // � ���� ������ ���� �������� ����� �� ������������ ���� (��, ��� ������� ����� ������ �����). 
        foreach (ServiceEndpoint endPoint in hostDesc.Endpoints)
        {
          if (context.IncomingMessageHeaders.To.AbsolutePath == endPoint.ListenUri.AbsolutePath)
          {
            result = endPoint;
            break;
          }
        }
      }

      return result;
    }

    /// <summary>
    /// ����� ������ � ��������� �� ������������ ��������.
    /// ����� ������������� ��� �� ������ ������� ���������, ��� � ��
    /// ���������������� ������������� Action � ��������� ������� OperationContract.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� �������� contract ��� action.</exception>
    /// <exception cref="MissingMethodException">�� ������ ����� action � ���������.</exception>
    /// <param name="contract">�������� ContractDescription.</param>
    /// <param name="action">������������ ��������.</param>
    /// <returns>MethodInfo.</returns>
    internal static MethodInfo FindMethodByAction(ContractDescription contract, string action)
    {
      if (contract == null)
      {
        throw new ArgumentNullException("contract");
      }
      if (string.IsNullOrEmpty(action))
      {
        throw new ArgumentNullException("action");
      }

      MethodInfo result = contract.ContractType.GetMethod(action);

      if (result == null)
      {
        Type contractType = contract.ContractType;
        foreach (OperationDescription item in contract.Operations)
        {
          result = item.DeclaringContract.ContractType.GetMethod(item.Name);
          object[] attributes = result.GetCustomAttributes(typeof(OperationContractAttribute), false);
          if ((attributes.Length > 0) && (((OperationContractAttribute)attributes[0]).Action == action))
          {
            break;
          }
          else
          {
            result = null;
          }
        }

        if (result == null)
        {
          throw new MissingMethodException(string.Format("Method {0} not found.", action));
        }
      }
      return result;
    }
  }
}