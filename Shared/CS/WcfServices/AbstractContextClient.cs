﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Teletrack.WcfServices
{
  public abstract class AbstractContextClient<T> : ClientBase<T> where T : class
  {
    public AbstractContextClient(Binding binding, EndpointAddress remoteAddress, string sessionId)
      : base(binding, remoteAddress)
    {
      SetSessionContext((IChannel)InnerChannel, sessionId);
    }

    private static void SetSessionContext(IChannel channel, string sessionId)
    {
      IContextManager ctxMgr = channel.GetProperty<IContextManager>();
      IDictionary<string, string> ctx = new Dictionary<string, string>();
      ctx.Add(ContextParams.SessionIdParamName, sessionId);
      ctx.Add(ContextParams.ProtocolVersionParamName, ContextParams.ProtocolVersion.ToString());
      ctxMgr.SetContext(ctx);
    }
  }
}
