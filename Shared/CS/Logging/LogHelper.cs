using System;
using System.Reflection;
using NLog;
using NLog.Config;

namespace Teletrack.Logging
{
  /// <summary>
  /// ������� ������������ �������. 
  /// </summary>
  internal static class LogHelper
  {
    private static Logger _logger;

    static LogHelper()
    {
      try
      {
        _logger = LogManager.GetCurrentClassLogger();
      }
      catch (Exception ex)
      {
        throw new Exception("Can't create logger, check settings in Teletrack.LicServer.exe.config file", ex);
      }
    }

    internal static void Info(string message)
    {
      _logger.Info(message);
    }

    internal static void InfoFormat(string message, params object[] args)
    {
      _logger.Info(message, args);
    }

    internal static void Warning(string message)
    {
      _logger.Warn(message);
    }

    internal static void WarningFormat(string message, params object[] args)
    {
      _logger.Warn(message, args);
    }

    internal static void Debug(string message)
    {
      _logger.Debug(message);
    }

    internal static void DebugFormat(string message, params object[] args)
    {
      _logger.Debug(message, args);
    }

    internal static void Error(string message)
    {
      _logger.Error(message);
    }

    internal static void Error(string message, Exception ex)
    {
      _logger.Error(message, ex);
    }

    internal static void ErrorFormat(string message, params object[] args)
    {
      _logger.Error(message, args);
    }

    internal static void Fatal(string message, Exception ex)
    {
      _logger.Fatal(message, ex);
    }

    internal static void Fatal(string message)
    {
      _logger.Fatal(message);
    }

    internal static void FatalFormat(string message, params object[] args)
    {
      _logger.Fatal(message, args);
    }
  }
}