using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Teletrack.Logging
{
  /// <summary>
  /// ������ ������� � ���-����. ����� ��������� ��� � �����, �.�. ����
  /// ���-���� �������� ���������� �� ���� ������������ �����. 
  /// �� ����� �������� � � 0 ����� ���������� �������� ������ ���-������.
  /// </summary>
  [Target("RollingDayFile")]
  public class RollingDayFileTarget : FileTarget
  {
    /// <summary>
    /// ������������ ���-������: LicServer${date:format=yyyyMMdd}.log
    /// </summary>
    private const string FILE_NAME = "LicServer${date:format=yyyyMMdd}.log";
    /// <summary>
    /// ������ ������ � ���: ${longdate} ${level:uppercase=true} [${threadid}] ${message}
    /// </summary>
    private const string LAYOUT_FORMAT = "${longdate} ${level:uppercase=true} [${threadid}] ${message}";

    private static Timer _timerDeleteLogs;

    [RequiredParameter]
    public string LogDirectory { get; set; }

    public RollingDayFileTarget()
      : base()
    {
      FileName = "tmp.log";
      CreateTimer();
    }

    private void CreateTimer()
    {
      _timerDeleteLogs = new Timer();
      _timerDeleteLogs.AutoReset = false;
      _timerDeleteLogs.Elapsed += OnTimerElapsed;
      _timerDeleteLogs.Interval = GetTimerInterval();
    }

    protected override void InitializeTarget()
    {
      base.InitializeTarget();

      FileName = Path.Combine(LogDirectory, FILE_NAME);
      Layout = LAYOUT_FORMAT;

      if (MaxArchiveFiles < 1)
      {
        MaxArchiveFiles = 1;
      }

      CreateLogDirectoryIfNotExists();

      DeleteOldLogs();
      _timerDeleteLogs.Start();
    }

    private void CreateLogDirectoryIfNotExists()
    {
      string dir = Path.GetDirectoryName(FileName.Render(new LogEventInfo()));
      if (!Directory.Exists(dir))
      {
        Directory.CreateDirectory(dir);
      }
    }

    /// <summary>
    /// ������ ��������� �������� ������� �� 0 ����� 0 ����� 30 ������ ���������� ���.
    /// </summary>
    /// <returns>�������� �������� �������.</returns>
    private double GetTimerInterval()
    {
      DateTime now = DateTime.Now;
      DateTime nextTime = new DateTime(now.Year, now.Month, now.Day, 0, 0, 30).AddDays(1);

      long ticks = nextTime.Ticks - DateTime.Now.Ticks;
      return TimeSpan.FromTicks(ticks).TotalMilliseconds;
    }

    /// <summary>
    /// ���������� ������� ������������ �������.
    /// </summary>
    /// <param name="sender">object.</param>
    /// <param name="e">ElapsedEventArgs.</param>
    private void OnTimerElapsed(object sender, ElapsedEventArgs e)
    {
      try
      {
        DeleteOldLogs();
      }
      finally
      {
        _timerDeleteLogs.Interval = GetTimerInterval();
        _timerDeleteLogs.Start();
      }
    }

    /// <summary>
    /// ������ ���� � ���-����������� {0}, � ������������ � ����������� �������� �� ����� {1} ����.
    /// </summary>
    private const string DEL_LOG_MESSAGE =
      "������ ���� � ���-����������� {0}, � ������������ � �����������: ������� �� ����� {1} ������.";

    private void DeleteOldLogs()
    {
      foreach (string file in GetFilesPathsForDelete())
      {
        try
        {
          System.IO.File.Delete(file);
          WriteInfo(String.Format(DEL_LOG_MESSAGE, file, MaxArchiveFiles));
        }
        catch (Exception ex)
        {
          LogHelper.Error(String.Format("Can't delete old log-file {0}", file), ex);
        }
      }
    }

    /// <summary>
    /// ������� ������ ��������������� ��� ��������.
    /// </summary>
    /// <returns></returns>
    private IEnumerable<string> GetFilesPathsForDelete()
    {
      //DateTime boundaryDate = GetBoundarySavingDate();

      string[] files = GetAllLogFilesPaths();
      if (files.Length <= MaxArchiveFiles)
      {
        yield break;
      }

      int lastFileIndex = files.Length - MaxArchiveFiles - 1;

      for (int i = 0; i < lastFileIndex; i++)
      {
        yield return files[i];
      }
    }


    /// <summary>
    /// ������������ ������ ������ ����� � ���-������.
    /// </summary>
    /// <returns>������ ������ ����� �� ���� ���-������.</returns>
    private string[] GetAllLogFilesPaths()
    {
      SortedDictionary<DateTime, string> result = new SortedDictionary<DateTime, string>();
      // ���� ����� � ������
      const string search_template = "*.log";
      foreach (string item in Directory.GetFiles(
        Path.GetDirectoryName(FileName.Render(new LogEventInfo())),
        search_template))
      {
        DateTime creationTime = System.IO.File.GetCreationTime(item);
        DateTime lastWriteTime = System.IO.File.GetLastWriteTime(item);
        result.Add(
          creationTime < lastWriteTime ? creationTime : lastWriteTime,
          item);
      }
      return result.Values.ToArray();
    }

    private void WriteInfo(string message)
    {
      LogEventInfo data = new LogEventInfo();
      data.Message = message;
      data.Level = LogLevel.Info;
      data.TimeStamp = DateTime.Now;

      Write(data);
    }

    private void WriteError(string message, Exception ex)
    {
      LogEventInfo data = new LogEventInfo();
      data.Message = message;
      data.Exception = ex;
      data.Level = LogLevel.Error;
      data.TimeStamp = DateTime.Now;

      Write(data);
    }
  }
}