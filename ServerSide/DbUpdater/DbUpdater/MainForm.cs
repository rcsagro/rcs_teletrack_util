using System;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using DbUpdater.Entity;
using DbUpdater.Setting;

namespace DbUpdater
{
  /// <summary>
  /// ������� �����.
  /// </summary>
  public partial class MainForm : Form
  {
    /// <summary>
    /// ���������� �����.
    /// </summary>
    private MainFormCtrl formController;

    public MainForm()
    {
      InitializeComponent();
    }

    /// <summary>
    /// �������� �����.
    /// </summary>
    /// <param name="sender">object.</param>
    /// <param name="e">EventArgs.</param>
    private void MainForm_Load(object sender, EventArgs e)
    {
      try
      {
        Text = GetFormCaption();

        formController = new MainFormCtrl();
        formController.ProgressMessageEvent +=
          new ProgressMessageDelegate(OnProgressMessage);

        MapConnectionSettings();
        txtLogFile.Text = formController.LogPath;
        txtlblScriptDir.Text = UpdaterConfigMngr.Instatnce.Config.BaseScriptPath;
      }
      catch (Exception ex)
      {
        WriteToLog(String.Format("������: {0}.\r\n" +
          "�������� �������� UpdaterConfig.xml � ������� ������.", ex));
        DisableControls();
      }
    }

    /// <summary>
    /// ������������ ��������� �����.
    /// </summary>
    /// <returns>��������� �����.</returns>
    private string GetFormCaption()
    {
      StringBuilder result = new StringBuilder();

      object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(
        typeof(AssemblyCompanyAttribute), false);
      if (attributes.Length != 0)
      {
        result.Append(((AssemblyCompanyAttribute)attributes[0]).Company);
      }

      attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(
        typeof(AssemblyTitleAttribute), false);
      if (attributes.Length != 0)
      {
        result.Append(" ").Append(((AssemblyTitleAttribute)attributes[0]).Title);
      }

      Version ver = Assembly.GetExecutingAssembly().GetName().Version;
      result.Append(" v. ").Append(ver.Major).Append(".").Append(
        ver.Minor).Append(".").Append(ver.Build).Append(".");

      attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(
        typeof(AssemblyDescriptionAttribute), false);
      if (attributes.Length != 0)
      {
        result.Append(" ").Append(((AssemblyDescriptionAttribute)attributes[0]).Description);
      }

      return result.ToString();
    }

    /// <summary>
    /// ����, ����������� ��� ����������� UpdateConnectionSettings.
    /// </summary>
    private bool disableConnectionSettings;

    /// <summary>
    /// ���������� �������� �������� ����������� �������������� 
    /// ��������.
    /// </summary>
    private void MapConnectionSettings()
    {
      disableConnectionSettings = true;
      try
      {
        SqlDbConnection conn = ConnectionMngr.Instance.Connection;
        comboServer.Text = conn.DataSource;
        comboDB.Text = conn.InitialCatalog;

        if (!conn.IntegratedSecurity)
        {
          radioBtnAuthServer.Checked = true;
          txtLogin.Text = conn.UserID;
          txtPassword.Text = conn.Password;
        }
      }
      finally
      {
        disableConnectionSettings = false;
      }
    }

    /// <summary>
    /// ���������� �������� �����������.
    /// </summary>
    private void UpdateConnectionSettings()
    {
      if (!disableConnectionSettings)
      {
        formController.UpdateConnectionSettings(new SqlDbConnection(
         comboServer.Text, comboDB.Text,
         radioBtnAuthWindows.Checked,
         txtLogin.Text, txtPassword.Text));
      }
    }

    private void DisableControls()
    {
      comboServer.Text = "";
      comboServer.Enabled = false;
      btnServer.Enabled = false;
      SetControlsEnabled();
    }

    /// <summary>
    /// ����� ��������.
    /// </summary>
    private void FindServers()
    {
      this.Cursor = Cursors.WaitCursor;
      try
      {
        comboServer.DataSource = null;
        comboServer.Text = "";
        comboServer.DataSource = formController.FindServers();
        comboServer.DisplayMember = "FullServerName";
      }
      finally
      {
        this.Cursor = Cursors.Default;
      }
    }

    /// <summary>
    /// ����� ��� ������.
    /// </summary>
    private void FindDatabases()
    {
      this.Cursor = Cursors.WaitCursor;
      try
      {
        logBox.Clear();
        comboDB.DataSource = null;
        comboDB.Text = "";
        comboDB.DataSource = formController.FindDatabases();
      }
      finally
      {
        this.Cursor = Cursors.Default;
      }
    }

    /// <summary>
    /// ������ �������� ����������.
    /// </summary>
    private void RunUpdate()
    {
      this.Cursor = Cursors.WaitCursor;
      try
      {
        logBox.Clear();
        formController.RunUpdate();
      }
      finally
      {
        this.Cursor = Cursors.Default;
      }
    }

    /// <summary>
    /// ��������� ��������� ��� �������� �������
    /// </summary>
    /// <param name="message"></param>
    private void OnProgressMessage(string message)
    {
      WriteToLog(message);
    }

    /// <summary>
    /// ������������ ����������.
    /// </summary>
    private void TestConnection()
    {
      this.Cursor = Cursors.WaitCursor;
      try
      {
        logBox.Clear();
        if (!formController.ConnectionAvailable())
        {
          SetMainButtonEnabled(false);
        }
      }
      finally
      {
        this.Cursor = Cursors.Default;
      }
    }

    /// <summary>
    /// ����� ��������� � ���� ����������.
    /// </summary>
    /// <param name="text">�����.</param>
    private void WriteToLog(string text)
    {
      logBox.AppendText(String.Format("{0} {1}\r\n", DateTime.Now, text));
      logBox.ScrollToCaret();
    }

    /// <summary>
    /// ��������� Enabled ��� ��������� ��.
    /// </summary>
    /// <param name="enabled">True or false.</param>
    private void SetDbSelectorEnabled(bool enabled)
    {
      comboDB.Enabled = enabled;
      btnDB.Enabled = enabled;
    }

    /// <summary>
    /// ��������� Enabled ��� ��������� ��������������.
    /// </summary>
    /// <param name="enabled">True or false.</param>
    private void SetAuthSelectorEnabled(bool enabled)
    {
      radioBtnAuthServer.Enabled = enabled;
      radioBtnAuthWindows.Enabled = enabled;
      SetLoginPasswordEnabled();
    }

    /// <summary>
    /// ��������� Enabled ��� ������ � ������.
    /// </summary>
    private void SetLoginPasswordEnabled()
    {
      txtLogin.Enabled = txtPassword.Enabled = radioBtnAuthServer.Checked;
    }

    /// <summary>
    /// ��������� Enabled ������.
    /// </summary>
    /// <param name="enabled">True or false.</param>
    private void SetMainButtonEnabled(bool enabled)
    {
      btnTestConnection.Enabled = enabled;
      btnRun.Enabled = enabled;
    }

    /// <summary>
    /// ��������� Enabled ���������.
    /// </summary>
    private void SetControlsEnabled()
    {
      SetDbSelectorEnabled(ValidateDbSelectorEnabled());
      SetAuthSelectorEnabled(ValidateDbSelectorEnabled());
      SetMainButtonEnabled(ValidateMainButtonEnabled());
    }

    /// <summary>
    /// �������� Enabled ��� ��������� ������ ��.
    /// ������ ���� ������ ������.
    /// </summary>
    /// <returns>True or false.</returns>
    private bool ValidateDbSelectorEnabled()
    {
      return ((comboServer.Enabled) &&
        (!String.IsNullOrEmpty(comboServer.Text.Trim())));
    }

    /// <summary>
    /// �������� Enabled ��� ������.
    /// ������ ���� ������� ������, �� � ����� ���� ��������
    /// �������������� SQL �������.
    /// </summary>
    /// <returns>True or false.</returns>
    private bool ValidateMainButtonEnabled()
    {
      return
        ((!String.IsNullOrEmpty(comboServer.Text.Trim())) &&
        (comboDB.Enabled) && (!String.IsNullOrEmpty(comboDB.Text.Trim())) &&
        ((radioBtnAuthWindows.Checked) || (!String.IsNullOrEmpty(txtLogin.Text.Trim()))));
    }

    /// <summary>
    /// ��������� ������� ��������� �������.
    /// </summary>
    /// <param name="sender">object.</param>
    /// <param name="e">EventArgs.</param>
    private void comboServer_TextChanged(object sender, EventArgs e)
    {
      SetControlsEnabled();
      UpdateConnectionSettings();
    }

    /// <summary>
    /// ��������� ������� ��������� ��.
    /// </summary>
    /// <param name="sender">object.</param>
    /// <param name="e">EventArgs.</param>
    private void comboDB_TextChanged(object sender, EventArgs e)
    {
      SetControlsEnabled();
      UpdateConnectionSettings();
    }

    /// <summary>
    /// ��������� ������� ��������� ������� ��������������.
    /// </summary>
    /// <param name="sender">object.</param>
    /// <param name="e">EventArgs.</param>
    private void radioBtnAuthServer_CheckedChanged(object sender, EventArgs e)
    {
      SetControlsEnabled();
      SetLoginPasswordEnabled();
      UpdateConnectionSettings();
    }

    /// <summary>
    /// ��������� ������� ��������� ������.
    /// </summary>
    /// <param name="sender">sender.</param>
    /// <param name="e">EventArgs.</param>
    private void txtLogin_TextChanged(object sender, EventArgs e)
    {
      SetControlsEnabled();
      UpdateConnectionSettings();
    }

    /// <summary>
    /// ��������� ������� ��������� ������.
    /// </summary>
    /// <param name="sender">object.</param>
    /// <param name="e">EventArgs.</param>
    private void txtPassword_TextChanged(object sender, EventArgs e)
    {
      SetControlsEnabled();
      UpdateConnectionSettings();
    }

    /// <summary>
    /// ��������� ������� ������� �� ������ ������� ����������.
    /// </summary>
    /// <param name="sender">object.</param>
    /// <param name="e">EventArgs.</param>
    private void btnRun_Click(object sender, EventArgs e)
    {
      RunUpdate();
    }

    /// <summary>
    /// ��������� ������� ������� �� ������ ������������ �����������.
    /// </summary>
    /// <param name="sender">object.</param>
    /// <param name="e">EventArgs.</param>
    private void btnTestConnection_Click(object sender, EventArgs e)
    {
      TestConnection();
    }

    /// <summary>
    /// ��������� ������� ������� �� ������ ������ ��������.
    /// </summary>
    /// <param name="sender">object.</param>
    /// <param name="e">EventArgs.</param>
    private void btnServer_Click(object sender, EventArgs e)
    {
      FindServers();
    }

    /// <summary>
    /// ��������� ������� ������� �� ������ ������ ��.
    /// </summary>
    /// <param name="sender">object.</param>
    /// <param name="e">EventArgs.</param>
    private void btnDB_Click(object sender, EventArgs e)
    {
      FindDatabases();
    }
  }
}