using System;
using System.Collections.Generic;
using DbUpdater.DAL;
using DbUpdater.Entity;
using DbUpdater.Setting;
using DbUpdater.Utils;

namespace DbUpdater
{
  /// <summary>
  /// ���������� ������� �����
  /// </summary>
  class MainFormCtrl
  {
    /// <summary>
    /// ������: {0}
    /// </summary>
    private const string ERROR_MESSAGE = "������: {0}";

    /// <summary>
    /// ������� ���������� � ����������� ������-�� �����.
    /// </summary>
    public event ProgressMessageDelegate ProgressMessageEvent;

    /// <summary>
    /// �������� �����.
    /// </summary>
    private FileLogger fileLogger;

    /// <summary>
    /// ���� � ���-�����.
    /// </summary>
    public string LogPath
    {
      get { return fileLogger.LogPath; }
    }

    /// <summary>
    /// �������� �������� �����������.
    /// </summary>
    private ConnectionMngr connectionMngr;
    /// <summary>
    /// �������� �������� ����������.
    /// </summary>
    private UpdaterConfigMngr configMngr;

    /// <summary>
    /// �����������.
    /// </summary>
    public MainFormCtrl()
    {
      fileLogger = new FileLogger();
      try
      {
        connectionMngr = ConnectionMngr.Instance;
        configMngr = UpdaterConfigMngr.Instatnce;
      }
      catch (Exception ex)
      {
        Log(String.Format(ERROR_MESSAGE, ex));
      }
    }

    /// <summary>
    /// ������ � ���.
    /// </summary>
    /// <param name="text">�����.</param>
    private void Log(string text)
    {
      Log(text, true);
    }

    /// <summary>
    /// ������ � ���.
    /// </summary>
    /// <param name="text">�����.</param>
    /// <param name="withTimeMarker">���������� ��������������� 
    /// ���� � ����� ��� ���.</param>
    private void Log(string text, bool withTimeMarker)
    {
      try
      {
        fileLogger.Write(withTimeMarker ?
          String.Format("{0} {1}", DateTime.Now, text) : text);
        PostProgressMessage(text);
      }
      catch (Exception ex)
      {
        PostProgressMessage(String.Format(ERROR_MESSAGE, ex));
      }
    }

    /// <summary>
    /// ������ ������ � ��� ����.
    /// </summary>
    /// <param name="text">�����.</param>
    /// <param name="withTimeMarker">���������� ��������������� 
    /// ���� � ����� ��� ���.</param>
    private void WriteToLogFile(string text, bool withTimeMarker)
    {
      try
      {
        fileLogger.Write(withTimeMarker ?
          String.Format("{0} {1}", DateTime.Now, text) : text);
      }
      catch (Exception ex)
      {
        PostProgressMessage(String.Format(ERROR_MESSAGE, ex));
      }
    }

    /// <summary>
    /// �������� ���������� ��������� ����������.
    /// </summary>
    /// <param name="mMessage">���������</param>
    private void PostProgressMessage(string message)
    {
      if (ProgressMessageEvent != null)
      {
        ProgressMessageEvent(message);
      }
    }

    /// <summary>
    /// ������ �������� ����������.
    /// </summary>
    public void RunUpdate()
    {
      const string delimiter_string = "------------------------------------";
      try
      {
        WriteToLogFile("", false);
        Log(String.Format(
          "����� �������� ����������. ������ �����������: {0}",
          connectionMngr.Connection.ConnectionString));

        ScriptExecutor scriptExecutor = new ScriptExecutor();
        scriptExecutor.ProgressMessageEvent +=
          new ProgressMessageDelegate(Log);

        scriptExecutor.Run();

        Log("������� ���������� ������� ��������.");
        WriteToLogFile(delimiter_string, false);
      }
      catch (Exception ex)
      {
        Log(String.Format(ERROR_MESSAGE, ex));
        Log("������� ���������� �������.");
        WriteToLogFile(delimiter_string, false);
      }
    }

    /// <summary>
    /// ���������� �������� �����������.
    /// </summary>
    public void UpdateConnectionSettings(SqlDbConnection conn)
    {
      connectionMngr.Connection.DataSource = conn.DataSource;
      connectionMngr.Connection.InitialCatalog = conn.InitialCatalog;
      if (conn.IntegratedSecurity)
      {
        connectionMngr.Connection.IntegratedSecurity = true;
        connectionMngr.Connection.UserID = "";
        connectionMngr.Connection.Password = "";
      }
      else
      {
        connectionMngr.Connection.IntegratedSecurity = false;
        connectionMngr.Connection.UserID = conn.UserID;
        connectionMngr.Connection.Password = conn.Password;
      }
      connectionMngr.SaveSetting();
    }

    /// <summary>
    /// ����� ��������.
    /// </summary>
    /// <returns>������ SQLServerInfo.</returns>
    public List<SQLServerInfo> FindServers()
    {
      try
      {
        return new ServerEnumerator().GetServers();
      }
      catch (Exception ex)
      {
        Log(String.Format(ERROR_MESSAGE, ex));
        return null;
      }
    }

    /// <summary>
    /// ����� ��� ������.
    /// </summary>
    /// <returns>������ ������������ ��.</returns>
    public List<string> FindDatabases()
    {
      string error;
      if (new ConnectionTester().ConnectionAvailable(out error))
      {
        return new DbEnumerator().GetDbList();
      }
      else
      {
        Log(String.Format(ERROR_MESSAGE, error));
        return null;
      }
    }

    /// <summary>
    /// ������������ ����������.
    /// </summary>
    /// <returns>True - ���������� ��������.</returns>
    public bool ConnectionAvailable()
    {
      string error;
      bool result = false;
      try
      {
        if (new ConnectionTester().ConnectionAvailable(out error))
        {
          result = true;
          Log(String.Format("����������� �������� ({0}).",
            connectionMngr.Connection.ConnectionString));
        }
        else
        {
          Log(String.Format(ERROR_MESSAGE, error));
        }
      }
      catch (Exception ex)
      {
        Log(String.Format(ERROR_MESSAGE, ex));
      }
      return result;
    }
  }
}
