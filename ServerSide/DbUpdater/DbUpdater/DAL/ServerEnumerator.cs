using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using DbUpdater.Entity;

namespace DbUpdater.DAL
{
  /// <summary>
  /// ����� �������� MS SQL.
  /// </summary>
  public class ServerEnumerator
  {
    /// <summary>
    /// ������������ ������ ��������� �������� MS SQL.
    /// </summary>
    /// <returns>������ ��������� �������� MS SQL.</returns>
    public List<SQLServerInfo> GetServers()
    {
      List<SQLServerInfo> result = new List<SQLServerInfo>();
      foreach (DataRow row in SqlDataSourceEnumerator.Instance.GetDataSources().Rows)
      {
        result.Add(new SQLServerInfo(
          row["ServerName"].ToString(),
          row["InstanceName"].ToString()));
      }
      return result;
    }
  }
}
