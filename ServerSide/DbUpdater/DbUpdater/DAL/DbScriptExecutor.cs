using System;
using System.Data.SqlClient;
using DbUpdater.Setting;

namespace DbUpdater.DAL
{
  /// <summary>
  /// ������ ������ �� batch'�, ����������� ��������� ������� 
  /// � GO, � ��������� ��.
  /// </summary>
  public class DbScriptExecutor
  {
    /// <summary>
    /// ���������� �������. 
    /// </summary>
    /// <param name="script">����� �������.</param>
    public void Execute(string script)
    {
      using (SqlConnection conn = new SqlConnection(
        ConnectionMngr.Instance.Connection.ConnectionString))
      {
        conn.Open();
        SqlTransaction trn = conn.BeginTransaction("DbUpdater_ExecScript");
        try
        {
          ExecuteBatchNonQuery(script, conn, trn);
          trn.Commit();
        }
        catch (Exception ex)
        {
          trn.Rollback();
          throw ex;
        }
      }
    }

    private void ExecuteBatchNonQuery(string sql, SqlConnection conn, SqlTransaction trn)
    {
      string sqlBatch = string.Empty;
      sql += "\nGO";   // make sure last batch is executed.

      SqlCommand cmd = new SqlCommand(string.Empty, conn, trn);
      cmd.CommandTimeout = 3600;

      foreach (string line in sql.Split(new string[2] { "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries))
      {
        if (line.ToUpperInvariant().Trim() == "GO")
        {
          if (!String.IsNullOrEmpty(sqlBatch))
          {
            cmd.CommandText = sqlBatch;
            cmd.ExecuteNonQuery();
            sqlBatch = string.Empty;
          }
        }
        else
        {
          sqlBatch = String.Concat(sqlBatch, line, "\n");
        }
      }
    }
  }
}
