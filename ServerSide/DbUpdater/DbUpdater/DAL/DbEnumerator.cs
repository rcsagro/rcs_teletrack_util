using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using DbUpdater.Setting;

namespace DbUpdater.DAL
{
  /// <summary>
  /// ��������� ������ ��.
  /// </summary>
  public class DbEnumerator
  {
    /// <summary>
    /// ��� �� �� ���������� �������������.
    /// </summary>
    private static readonly string[] SYSTEM_DB =
      new string[] { "master", "model", "msdb", "tempdb" };

    /// <summary>
    /// ��������� ������ ��.
    /// </summary>
    /// <returns>������ ���� ��.</returns>
    public List<string> GetDbList()
    {
      List<string> result = new List<string>();

      using (SqlConnection conn = new SqlConnection(
        ConnectionMngr.Instance.Connection.ConnectionString))
      {
        SqlCommand cmd = conn.CreateCommand();
        cmd.CommandText = "SELECT name FROM sys.databases ORDER BY 1";
        conn.Open();
        using (SqlDataReader reader = cmd.ExecuteReader())
        {
          string dbName;
          while (reader.Read())
          {
            dbName = reader[0].ToString();
            if (Array.IndexOf(SYSTEM_DB, dbName) < 0)
            {
              result.Add(dbName);
            }
          }
        }
      }
      return result;
    }
  }
}
