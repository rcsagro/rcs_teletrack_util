using System;
using System.Data.SqlClient;
using DbUpdater.Setting;

namespace DbUpdater.DAL
{
  /// <summary>
  /// ������������ ����������.
  /// </summary>
  class ConnectionTester
  {
    /// <summary>
    /// �������� ����������� �����������.
    /// </summary>
    /// <param name="error">����� ������ ����������.</param>
    /// <returns>True - ���������� ��������.</returns>
    public bool ConnectionAvailable(out string error)
    {
      error = "";
      try
      {
        SqlConnection conn = new SqlConnection(
          ConnectionMngr.Instance.Connection.ConnectionString);
        conn.Open();
        conn.Close();
        return true;
      }
      catch (Exception ex)
      {
        error = String.Format("{0} {1}\r\n{2}",
          "���������� ������������ � ��, ��������� ������",
          ConnectionMngr.Instance.Connection.ConnectionString, ex.ToString());
        return false;
      }
    }
  }
}
