using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DbUpdater.Entity;
using DbUpdater.Setting;

namespace DbUpdater.DAL
{
  /// <summary>
  /// ���������� ����������.
  /// </summary>
  class UpdateSubsystem
  {
    /// <summary>
    /// �����������.
    /// </summary>
    public UpdateSubsystem()
    {

    }

    /// <summary>
    /// �������� ������������� �������� VersionDB. 
    /// </summary>
    /// <param name="version">������ ��, ���� �������� ����������.</param>
    /// <returns>True - ����������.</returns>
    public bool PropertyExists(out VersionDB version)
    {
      bool result = false;
      version = null;
      using (SqlConnection conn = new SqlConnection(
        ConnectionMngr.Instance.Connection.ConnectionString))
      {
        SqlCommand cmd = conn.CreateCommand();
        cmd.CommandText =
          "SELECT value " +
          "FROM sys.extended_properties " +
          "WHERE name = 'VersionDB'";

        conn.Open();
        object ver = cmd.ExecuteScalar();
        if ((ver != null) && (ver != DBNull.Value))
        {
          version = new VersionDB(Convert.ToString(ver));
          result = true;
        }
      }
      return result;
    }

    /// <summary>
    /// �������� � ������������� �������� �� VersionDB.
    /// </summary>
    /// <returns>������ �� �� ���������.</returns>
    public VersionDB CreateProperty()
    {
      const string default_version = "1.0.0";
      using (SqlConnection conn = new SqlConnection(
        ConnectionMngr.Instance.Connection.ConnectionString))
      {
        SqlCommand cmd = conn.CreateCommand();
        //cmd.CommandText =
        //  "EXEC sys.sp_addextendedproperty @name = 'VersionDB', @value = '1.0.0'";
        cmd.CommandText = "sys.sp_addextendedproperty";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@name", "VersionDB");
        cmd.Parameters.AddWithValue("@value", default_version);

        conn.Open();
        cmd.ExecuteNonQuery();
      }
      return new VersionDB(default_version);
    }

    /// <summary>
    /// ���������� �������� �� VersionDB.
    /// </summary>
    /// <param name="newVersion">����� �������� ������ ��.</param>
    public void UpdateProperty(string newVersion)
    {
      using (SqlConnection conn = new SqlConnection(
        ConnectionMngr.Instance.Connection.ConnectionString))
      {
        SqlCommand cmd = conn.CreateCommand();
        //cmd.CommandText = String.Format(
        //  "EXEC sys.sp_updateextendedproperty @name = 'VersionDB', @value = '{0}'",
        //  newVersion);
        cmd.CommandText = "sys.sp_updateextendedproperty";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@name", "VersionDB");
        cmd.Parameters.AddWithValue("@value", newVersion);

        conn.Open();
        cmd.ExecuteNonQuery();
      }
    }

    /// <summary>
    /// �������� ������������� ������� LogUpdate.
    /// </summary>
    /// <returns>True - ����������.</returns>
    public bool TableExists()
    {
      using (SqlConnection conn = new SqlConnection(
        ConnectionMngr.Instance.Connection.ConnectionString))
      {
        SqlCommand cmd = conn.CreateCommand();
        cmd.CommandText = "SELECT OBJECT_ID('dbo.LogUpdate')";
        conn.Open();
        object result = cmd.ExecuteScalar();
        return ((result == null) || (result == DBNull.Value)) ? false : true;
      }
    }

    /// <summary>
    /// �������� ������� LogUpdate.
    /// </summary>
    public void CreateTable()
    {
      using (SqlConnection conn = new SqlConnection(
        ConnectionMngr.Instance.Connection.ConnectionString))
      {
        SqlCommand cmd = conn.CreateCommand();
        cmd.CommandText =
          "CREATE TABLE dbo.LogUpdate (" +
          "  ID tinyint IDENTITY(1,1) NOT NULL," +
          "  VersionDB varchar(250) NOT NULL," +
          "  ScriptName varchar(250) NOT NULL," +
          "  Descr varchar(max) NULL," +
          "  UpdateTime smalldatetime NOT NULL CONSTRAINT DF_LogUpdateScript_UpdateTime DEFAULT (getdate())," +
          "  CONSTRAINT PK_UpdateScript PRIMARY KEY CLUSTERED ( ID ASC ) ON [PRIMARY]," +
          "  CONSTRAINT UQ_VersionDB_ScriptName UNIQUE NONCLUSTERED ( VersionDB, ScriptName ) ON [PRIMARY]" +
          ") ON [PRIMARY]";

        conn.Open();
        cmd.ExecuteNonQuery();
      }
    }

    /// <summary>
    /// ������� �� ������� LogUpdate.
    /// </summary>
    /// <returns>������ LogUpdate.</returns>
    public List<LogUpdate> Select()
    {
      List<LogUpdate> result = new List<LogUpdate>();

      using (SqlConnection conn = new SqlConnection(
        ConnectionMngr.Instance.Connection.ConnectionString))
      {
        SqlCommand cmd = conn.CreateCommand();
        cmd.CommandText =
          "SELECT ID, VersionDB, ScriptName, Descr " +
          "FROM dbo.LogUpdate " +
          "ORDER BY VersionDB, ScriptName";

        conn.Open();
        using (SqlDataReader reader = cmd.ExecuteReader())
        {
          while (reader.Read())
          {
            result.Add(new LogUpdate(
              Convert.ToInt32(reader[0]),
              reader[1].ToString(),
              reader[2].ToString(),
              reader.IsDBNull(3) ? "" : reader[3].ToString()));
          }
        }
      }
      return result;
    }

    /// <summary>
    /// ������� ������ � ������� LogUpdate.
    /// </summary>
    /// <param name="item">LogUpdate.</param>
    public void Insert(LogUpdate item)
    {
      using (SqlConnection conn = new SqlConnection(
        ConnectionMngr.Instance.Connection.ConnectionString))
      {
        SqlCommand cmd = conn.CreateCommand();
        cmd.CommandText =
          "INSERT INTO dbo.LogUpdate (VersionDB, ScriptName, Descr) " +
          "VALUES (@VersionDB, @ScriptName, @Descr); " +
          "SELECT SCOPE_IDENTITY();";

        cmd.Parameters.AddWithValue("@VersionDB", item.VersionDB);
        cmd.Parameters.AddWithValue("@ScriptName", item.ScriptName);
        cmd.Parameters.AddWithValue("@Descr", item.Descr);

        conn.Open();
        item.ID = Convert.ToInt32(cmd.ExecuteScalar());
      }
    }

    /// <summary>
    /// �������� ������ �� ������� LogUpdate.
    /// </summary>
    /// <param name="id">������������� ���������.</param>
    public void Delete(int id)
    {
      using (SqlConnection conn = new SqlConnection(
        ConnectionMngr.Instance.Connection.ConnectionString))
      {
        SqlCommand cmd = conn.CreateCommand();
        cmd.CommandText = "DELETE FROM dbo.LogUpdate WHERE ID = @ID";
        cmd.Parameters.AddWithValue("@ID", id);
        conn.Open();
        cmd.ExecuteNonQuery();
      }
    }
  }
}
