namespace DbUpdater
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnRun = new System.Windows.Forms.Button();
      this.groupConnection = new System.Windows.Forms.GroupBox();
      this.lblDB = new System.Windows.Forms.Label();
      this.btnDB = new System.Windows.Forms.Button();
      this.comboDB = new System.Windows.Forms.ComboBox();
      this.groupAuth = new System.Windows.Forms.GroupBox();
      this.lblPassword = new System.Windows.Forms.Label();
      this.lblLogin = new System.Windows.Forms.Label();
      this.radioBtnAuthWindows = new System.Windows.Forms.RadioButton();
      this.txtPassword = new System.Windows.Forms.TextBox();
      this.radioBtnAuthServer = new System.Windows.Forms.RadioButton();
      this.txtLogin = new System.Windows.Forms.TextBox();
      this.lblServer = new System.Windows.Forms.Label();
      this.comboServer = new System.Windows.Forms.ComboBox();
      this.btnServer = new System.Windows.Forms.Button();
      this.btnTestConnection = new System.Windows.Forms.Button();
      this.groupLog = new System.Windows.Forms.GroupBox();
      this.logBox = new System.Windows.Forms.TextBox();
      this.txtlblScriptDir = new System.Windows.Forms.TextBox();
      this.lblScriptDirCaption = new System.Windows.Forms.Label();
      this.txtLogFile = new System.Windows.Forms.TextBox();
      this.lblLogFileCaption = new System.Windows.Forms.Label();
      this.groupConnection.SuspendLayout();
      this.groupAuth.SuspendLayout();
      this.groupLog.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnRun
      // 
      this.btnRun.Enabled = false;
      this.btnRun.Location = new System.Drawing.Point(164, 216);
      this.btnRun.Name = "btnRun";
      this.btnRun.Size = new System.Drawing.Size(386, 41);
      this.btnRun.TabIndex = 9;
      this.btnRun.Text = "�������� ���� ������";
      this.btnRun.UseVisualStyleBackColor = true;
      this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
      // 
      // groupConnection
      // 
      this.groupConnection.Controls.Add(this.lblDB);
      this.groupConnection.Controls.Add(this.btnDB);
      this.groupConnection.Controls.Add(this.comboDB);
      this.groupConnection.Controls.Add(this.groupAuth);
      this.groupConnection.Controls.Add(this.lblServer);
      this.groupConnection.Controls.Add(this.comboServer);
      this.groupConnection.Controls.Add(this.btnServer);
      this.groupConnection.Location = new System.Drawing.Point(12, 4);
      this.groupConnection.Name = "groupConnection";
      this.groupConnection.Size = new System.Drawing.Size(538, 196);
      this.groupConnection.TabIndex = 15;
      this.groupConnection.TabStop = false;
      this.groupConnection.Text = "��������� �����������";
      // 
      // lblDB
      // 
      this.lblDB.AutoSize = true;
      this.lblDB.Location = new System.Drawing.Point(21, 168);
      this.lblDB.Name = "lblDB";
      this.lblDB.Size = new System.Drawing.Size(72, 13);
      this.lblDB.TabIndex = 20;
      this.lblDB.Text = "���� ������";
      // 
      // btnDB
      // 
      this.btnDB.Enabled = false;
      this.btnDB.Location = new System.Drawing.Point(379, 163);
      this.btnDB.Name = "btnDB";
      this.btnDB.Size = new System.Drawing.Size(136, 23);
      this.btnDB.TabIndex = 19;
      this.btnDB.Text = "����� ��";
      this.btnDB.UseVisualStyleBackColor = true;
      this.btnDB.Click += new System.EventHandler(this.btnDB_Click);
      // 
      // comboDB
      // 
      this.comboDB.Enabled = false;
      this.comboDB.FormattingEnabled = true;
      this.comboDB.Location = new System.Drawing.Point(114, 163);
      this.comboDB.Name = "comboDB";
      this.comboDB.Size = new System.Drawing.Size(252, 21);
      this.comboDB.TabIndex = 18;
      this.comboDB.TextChanged += new System.EventHandler(this.comboDB_TextChanged);
      // 
      // groupAuth
      // 
      this.groupAuth.Controls.Add(this.lblPassword);
      this.groupAuth.Controls.Add(this.lblLogin);
      this.groupAuth.Controls.Add(this.radioBtnAuthWindows);
      this.groupAuth.Controls.Add(this.txtPassword);
      this.groupAuth.Controls.Add(this.radioBtnAuthServer);
      this.groupAuth.Controls.Add(this.txtLogin);
      this.groupAuth.Location = new System.Drawing.Point(24, 51);
      this.groupAuth.Name = "groupAuth";
      this.groupAuth.Size = new System.Drawing.Size(491, 104);
      this.groupAuth.TabIndex = 17;
      this.groupAuth.TabStop = false;
      this.groupAuth.Text = "��������������";
      // 
      // lblPassword
      // 
      this.lblPassword.AutoSize = true;
      this.lblPassword.Location = new System.Drawing.Point(265, 75);
      this.lblPassword.Name = "lblPassword";
      this.lblPassword.Size = new System.Drawing.Size(45, 13);
      this.lblPassword.TabIndex = 9;
      this.lblPassword.Text = "������";
      // 
      // lblLogin
      // 
      this.lblLogin.AutoSize = true;
      this.lblLogin.Location = new System.Drawing.Point(46, 75);
      this.lblLogin.Name = "lblLogin";
      this.lblLogin.Size = new System.Drawing.Size(38, 13);
      this.lblLogin.TabIndex = 8;
      this.lblLogin.Text = "�����";
      // 
      // radioBtnAuthWindows
      // 
      this.radioBtnAuthWindows.AutoSize = true;
      this.radioBtnAuthWindows.Checked = true;
      this.radioBtnAuthWindows.Location = new System.Drawing.Point(22, 26);
      this.radioBtnAuthWindows.Name = "radioBtnAuthWindows";
      this.radioBtnAuthWindows.Size = new System.Drawing.Size(155, 17);
      this.radioBtnAuthWindows.TabIndex = 6;
      this.radioBtnAuthWindows.TabStop = true;
      this.radioBtnAuthWindows.Text = "Windows ��������������";
      this.radioBtnAuthWindows.UseVisualStyleBackColor = true;
      // 
      // txtPassword
      // 
      this.txtPassword.Enabled = false;
      this.txtPassword.Location = new System.Drawing.Point(316, 72);
      this.txtPassword.Name = "txtPassword";
      this.txtPassword.Size = new System.Drawing.Size(155, 20);
      this.txtPassword.TabIndex = 5;
      this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged);
      // 
      // radioBtnAuthServer
      // 
      this.radioBtnAuthServer.AutoSize = true;
      this.radioBtnAuthServer.Location = new System.Drawing.Point(22, 49);
      this.radioBtnAuthServer.Name = "radioBtnAuthServer";
      this.radioBtnAuthServer.Size = new System.Drawing.Size(166, 17);
      this.radioBtnAuthServer.TabIndex = 7;
      this.radioBtnAuthServer.Text = "SQL Server ��������������";
      this.radioBtnAuthServer.UseVisualStyleBackColor = true;
      this.radioBtnAuthServer.CheckedChanged += new System.EventHandler(this.radioBtnAuthServer_CheckedChanged);
      // 
      // txtLogin
      // 
      this.txtLogin.Enabled = false;
      this.txtLogin.Location = new System.Drawing.Point(90, 72);
      this.txtLogin.Name = "txtLogin";
      this.txtLogin.Size = new System.Drawing.Size(155, 20);
      this.txtLogin.TabIndex = 4;
      this.txtLogin.Text = "sa";
      this.txtLogin.TextChanged += new System.EventHandler(this.txtLogin_TextChanged);
      // 
      // lblServer
      // 
      this.lblServer.AutoSize = true;
      this.lblServer.Location = new System.Drawing.Point(21, 29);
      this.lblServer.Name = "lblServer";
      this.lblServer.Size = new System.Drawing.Size(63, 13);
      this.lblServer.TabIndex = 16;
      this.lblServer.Text = "������ ��";
      // 
      // comboServer
      // 
      this.comboServer.FormattingEnabled = true;
      this.comboServer.Location = new System.Drawing.Point(114, 24);
      this.comboServer.Name = "comboServer";
      this.comboServer.Size = new System.Drawing.Size(252, 21);
      this.comboServer.TabIndex = 15;
      this.comboServer.TextChanged += new System.EventHandler(this.comboServer_TextChanged);
      // 
      // btnServer
      // 
      this.btnServer.Location = new System.Drawing.Point(379, 24);
      this.btnServer.Name = "btnServer";
      this.btnServer.Size = new System.Drawing.Size(136, 23);
      this.btnServer.TabIndex = 14;
      this.btnServer.Text = "����� ��������";
      this.btnServer.UseVisualStyleBackColor = true;
      this.btnServer.Click += new System.EventHandler(this.btnServer_Click);
      // 
      // btnTestConnection
      // 
      this.btnTestConnection.Enabled = false;
      this.btnTestConnection.Location = new System.Drawing.Point(12, 216);
      this.btnTestConnection.Name = "btnTestConnection";
      this.btnTestConnection.Size = new System.Drawing.Size(131, 41);
      this.btnTestConnection.TabIndex = 21;
      this.btnTestConnection.Text = "���� �����������";
      this.btnTestConnection.UseVisualStyleBackColor = true;
      this.btnTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
      // 
      // groupLog
      // 
      this.groupLog.Controls.Add(this.logBox);
      this.groupLog.Controls.Add(this.txtlblScriptDir);
      this.groupLog.Controls.Add(this.lblScriptDirCaption);
      this.groupLog.Controls.Add(this.txtLogFile);
      this.groupLog.Controls.Add(this.lblLogFileCaption);
      this.groupLog.Location = new System.Drawing.Point(12, 273);
      this.groupLog.Name = "groupLog";
      this.groupLog.Size = new System.Drawing.Size(538, 191);
      this.groupLog.TabIndex = 22;
      this.groupLog.TabStop = false;
      this.groupLog.Text = "���������� � ���� ����������";
      // 
      // logBox
      // 
      this.logBox.Location = new System.Drawing.Point(6, 19);
      this.logBox.Multiline = true;
      this.logBox.Name = "logBox";
      this.logBox.ReadOnly = true;
      this.logBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.logBox.Size = new System.Drawing.Size(526, 116);
      this.logBox.TabIndex = 22;
      this.logBox.WordWrap = false;
      // 
      // txtlblScriptDir
      // 
      this.txtlblScriptDir.Location = new System.Drawing.Point(101, 164);
      this.txtlblScriptDir.Name = "txtlblScriptDir";
      this.txtlblScriptDir.ReadOnly = true;
      this.txtlblScriptDir.Size = new System.Drawing.Size(431, 20);
      this.txtlblScriptDir.TabIndex = 21;
      // 
      // lblScriptDirCaption
      // 
      this.lblScriptDirCaption.AutoSize = true;
      this.lblScriptDirCaption.Location = new System.Drawing.Point(6, 167);
      this.lblScriptDirCaption.Name = "lblScriptDirCaption";
      this.lblScriptDirCaption.Size = new System.Drawing.Size(95, 13);
      this.lblScriptDirCaption.TabIndex = 20;
      this.lblScriptDirCaption.Text = "���� � ��������:";
      // 
      // txtLogFile
      // 
      this.txtLogFile.Location = new System.Drawing.Point(101, 141);
      this.txtLogFile.Name = "txtLogFile";
      this.txtLogFile.ReadOnly = true;
      this.txtLogFile.Size = new System.Drawing.Size(431, 20);
      this.txtLogFile.TabIndex = 19;
      // 
      // lblLogFileCaption
      // 
      this.lblLogFileCaption.AutoSize = true;
      this.lblLogFileCaption.Location = new System.Drawing.Point(3, 144);
      this.lblLogFileCaption.Name = "lblLogFileCaption";
      this.lblLogFileCaption.Size = new System.Drawing.Size(97, 13);
      this.lblLogFileCaption.TabIndex = 18;
      this.lblLogFileCaption.Text = "���� � ��� �����:";
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(562, 476);
      this.Controls.Add(this.groupLog);
      this.Controls.Add(this.btnTestConnection);
      this.Controls.Add(this.groupConnection);
      this.Controls.Add(this.btnRun);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.Name = "MainForm";
      this.ShowIcon = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "���������� �� SQL Server";
      this.Load += new System.EventHandler(this.MainForm_Load);
      this.groupConnection.ResumeLayout(false);
      this.groupConnection.PerformLayout();
      this.groupAuth.ResumeLayout(false);
      this.groupAuth.PerformLayout();
      this.groupLog.ResumeLayout(false);
      this.groupLog.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnRun;
    private System.Windows.Forms.GroupBox groupConnection;
    private System.Windows.Forms.GroupBox groupAuth;
    private System.Windows.Forms.Label lblPassword;
    private System.Windows.Forms.Label lblLogin;
    private System.Windows.Forms.RadioButton radioBtnAuthWindows;
    private System.Windows.Forms.TextBox txtPassword;
    private System.Windows.Forms.RadioButton radioBtnAuthServer;
    private System.Windows.Forms.TextBox txtLogin;
    private System.Windows.Forms.Label lblServer;
    private System.Windows.Forms.ComboBox comboServer;
    private System.Windows.Forms.Button btnServer;
    private System.Windows.Forms.Label lblDB;
    private System.Windows.Forms.Button btnDB;
    private System.Windows.Forms.ComboBox comboDB;
    private System.Windows.Forms.Button btnTestConnection;
    private System.Windows.Forms.GroupBox groupLog;
    private System.Windows.Forms.Label lblLogFileCaption;
    private System.Windows.Forms.TextBox txtLogFile;
    private System.Windows.Forms.Label lblScriptDirCaption;
    private System.Windows.Forms.TextBox txtlblScriptDir;
    private System.Windows.Forms.TextBox logBox;
  }
}

