using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DbUpdater.DAL;
using DbUpdater.Entity;
using DbUpdater.Setting;

namespace DbUpdater
{
  delegate void ProgressMessageDelegate(string message);

  /// <summary>
  /// ���������� ������� �� ���������� � ����������� � �����������.
  /// </summary>
  class ScriptExecutor
  {
    /// <summary>
    /// ������� ������ ��.
    /// </summary>
    private VersionDB curVersionDB;
    /// <summary>
    /// ���������� ����������.
    /// </summary>
    private UpdateSubsystem updateSubsystem;
    /// <summary>
    /// ���������� ��������.
    /// </summary>
    private DbScriptExecutor executor;

    /// <summary>
    /// ������� ���������� � ����������� ������-�� �����.
    /// </summary>
    public event ProgressMessageDelegate ProgressMessageEvent;

    public ScriptExecutor()
    {
      updateSubsystem = new UpdateSubsystem();
      executor = new DbScriptExecutor();
    }

    /// <summary>
    /// �������� ���������� ��������� ����������.
    /// </summary>
    /// <param name="mMessage">���������</param>
    private void PostProgressMessage(string message)
    {
      if (ProgressMessageEvent != null)
      {
        ProgressMessageEvent(message);
      }
    }

    /// <summary>
    /// �������� ���������� ��������� � ������� ������.
    /// </summary>
    private void Post�urVersionDBMessage()
    {
      PostProgressMessage(String.Format("������� ������ �� {0}", curVersionDB));
    }

    /// <summary>
    /// ���������� ��������.
    /// <para>����� ����������� �������� ����������� ������� � �� ���������� 
    /// ����������: ������ �������������� �������������� �������� � �� -
    /// ������ VersionDb � ���-������� ����������� �������� LogUpdateScript.
    /// ���� ���������� ���������� ����������, �� ���������� �� ��������
    /// � ����������� ��������� ������ 1.0.0.</para>
    /// <para>����� ���������� �������� ���������� ���������� ������ - ������
    /// ������ ������ ���� ������ ������ ��. 
    /// ���� ����� �������� �� ���������� � �������, ������ �������� ��������
    /// ������ ������.</para>
    /// <para>� �� ��������� ���� �������������� ������ �� ������� 
    /// ����������� ������.</para>
    /// </summary>
    public void Run()
    {
      CheckUpdateSubsystem();
      Post�urVersionDBMessage();

      IDictionary<VersionDB, Packet> packets = GetNewPackets();

      if (packets.Count == 0)
      {
        PostProgressMessage(
          "���� ������ � ���������� ���������, ����� ���������� ���.");
      }

      foreach (KeyValuePair<VersionDB, Packet> kvp in packets)
      {
        PostProgressMessage(String.Format(
          "* ����� ��������� ������ {0}.", kvp.Value.Version));

        try
        {
          ExecutePacket(kvp.Value);

          // ������ ����� �������� ������ ��
          curVersionDB = new VersionDB(kvp.Value.Version);
          updateSubsystem.UpdateProperty(kvp.Value.Version);
        }
        catch (Exception ex)
        {
          throw new ApplicationException(String.Format(
            "! ������: ��������� ������ {0} ��������.\r\n  {1}",
            kvp.Value.Version, ex));
        }

        PostProgressMessage(String.Format(
          "  ��������� ������ {0} ���������.", kvp.Value.Version));

        Post�urVersionDBMessage();
      }
    }

    /// <summary>
    /// ���������� �� �������� ������ � ������ ��� ����������� ��������.
    /// </summary>
    /// <param name="list">������ ��� ����������� ��������.</param>
    /// <param name="scriptName">����������� ��� �������.</param>
    /// <param name="version">����������� ������.</param>
    /// <returns>True - ������ ��� ����������.</returns>
    private bool IsScriptExecuted(List<LogUpdate> list, string scriptName, string version)
    {
      foreach (LogUpdate item in list)
      {
        if ((item.ScriptName == scriptName) && (item.VersionDB == version))
        {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// ��������� ����� ����������.
    /// ������� ������ �� �����������.
    /// </summary>
    /// <param name="packet">����� ����������.</param>
    private void ExecutePacket(Packet packet)
    {
      if (packet.Scripts.Count == 0)
      {
        return;
      }

      List<LogUpdate> updateList = updateSubsystem.Select();

      foreach (Script script in packet.Scripts)
      {
        if (!IsScriptExecuted(updateList, script.Name, packet.Version))
        {
          ExecuteScript(script);
          // ������ � ��� ���������� � ���������� �������.
          updateSubsystem.Insert(new LogUpdate(
            0, packet.Version, script.Name, script.Description));
        }
      }
    }

    /// <summary>
    /// ��������� ������ ����������.
    /// </summary>
    /// <param name="script">Script.</param>
    private void ExecuteScript(Script script)
    {
      // ��� ��� ��������
      script.Validate();

      try
      {
        PostProgressMessage(String.Format("  > ����� ���������� ������� {0}.",
          script.UpdateScriptFile));

        executor.Execute(LoadScript(script.UpdateScriptFile));

        PostProgressMessage(String.Format("    ���������� ���������� ������� {0}.",
          script.UpdateScriptFile));
      }
      catch (Exception ex)
      {
        PostProgressMessage(String.Format(
          "  ! ������: ���������� ������� {0} ��������.\r\n {1}",
          script.UpdateScriptFile, ex));

        throw ex;
      }
    }

    /// <summary>
    /// �������� ����� �������.
    /// </summary>
    /// <param name="scriptFile">���� � ����� �������.</param>
    /// <returns>����� �������.</returns>
    private string LoadScript(string scriptFile)
    {
      using (StreamReader sr = new StreamReader(scriptFile, Encoding.Default))
      {
        return sr.ReadToEnd();
      }
    }

    /// <summary>
    /// �������� ������ � �� ���������� ����������, 
    /// � �������� �� � ������ ����������.
    /// </summary>
    private void CheckUpdateSubsystem()
    {
      if (!updateSubsystem.PropertyExists(out curVersionDB))
      {
        curVersionDB = updateSubsystem.CreateProperty();
      }

      if (!updateSubsystem.TableExists())
      {
        updateSubsystem.CreateTable();
      }
    }

    /// <summary>
    /// ������������ ������ �������, ������� ���� ����� �������.
    /// </summary>
    /// <returns>IDictionary VersionDB-Packet</returns>
    private IDictionary<VersionDB, Packet> GetNewPackets()
    {
      UpdaterConfig config = UpdaterConfigMngr.Instatnce.Config;
      IDictionary<VersionDB, Packet> result =
        new SortedDictionary<VersionDB, Packet>();

      foreach (Packet packet in config.Packets)
      {
        if (packet.VersionDB.CompareTo(curVersionDB) > 0)
        {
          result.Add(packet.VersionDB, packet);
        }
      }
      return result;
    }
  }
}
