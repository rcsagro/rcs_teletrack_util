using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace DbUpdater.Entity
{
  /// <summary>
  /// ����� ����������, ��������� �� ��������.
  /// </summary>
  [Serializable()]
  public class Packet
  {
    private string version;
    /// <summary>
    /// ������ �� ������� ����������� ��. 
    /// </summary>
    [XmlAttribute()]
    public string Version
    {
      get { return version; }
      set { version = value; }
    }

    private VersionDB versionDB;
    /// <summary>
    /// ������-������ �� ������� ����������� ��. 
    /// </summary>
    public VersionDB VersionDB
    {
      get { return versionDB; }
    }

    private string path;
    /// <summary>
    /// ������������� ���� � ���������� �� ��������� ������� ������.
    /// </summary>
    [XmlAttribute()]
    public string Path
    {
      get { return path; }
      set
      {
        path = value;
        fullDirPath = "";
        SetScriptDirPath();
      }
    }

    private string baseDir;
    /// <summary>
    /// ������� ���������� ��������.
    /// ����������� �� UpdaterConfig.
    /// </summary>
    public string BaseDir
    {
      get { return baseDir; }
      set
      {
        baseDir = value;
        fullDirPath = "";
        SetScriptDirPath();
      }
    }

    private string fullDirPath;
    /// <summary>
    /// ������ ���� � ���������� ������.
    /// </summary>
    public string FullDirPath
    {
      get
      {
        if (String.IsNullOrEmpty(fullDirPath))
        {
          fullDirPath = String.IsNullOrEmpty(path) ? baseDir :
            String.IsNullOrEmpty(baseDir) ? path :
            System.IO.Path.Combine(baseDir, path);
        }

        foreach (Script s in scripts)
        {
          s.DirPath = fullDirPath;
        }

        return fullDirPath;
      }
    }

    private List<Script> scripts;
    /// <summary>
    /// �������� ��������.
    /// </summary>
    public List<Script> Scripts
    {
      get { return scripts; }
      set
      {
        scripts = value;
        SetScriptDirPath();
      }
    }

    public Packet() { }

    private void SetScriptDirPath()
    {
      if ((scripts != null) && (!String.IsNullOrEmpty(FullDirPath)))
      {
        foreach (Script s in scripts)
        {
          s.DirPath = FullDirPath;
        }
      }
    }

    /// <summary>
    /// �������� �������. 
    /// </summary>
    /// <exception cref="ApplicationException">���� ���-�� �������.</exception>
    public void Validate()
    {
      versionDB = new VersionDB(version);

      if ((!String.IsNullOrEmpty(FullDirPath)) &&
        (!Directory.Exists(FullDirPath)))
      {
        throw new DirectoryNotFoundException(String.Format(
          "���������� {0} ������ ���������� ������ {1} �� �������.",
          FullDirPath, version));
      }
      foreach (Script s in scripts)
      {
        s.Validate();
      }
    }
  }
}
