
namespace DbUpdater.Entity
{
  /// <summary>
  /// ���������� � ����������� ���������� (�� ����������� �������).
  /// </summary>
  class LogUpdate
  {
    private int id;
    /// <summary>
    /// �������������.
    /// </summary>
    public int ID
    {
      get { return id; }
      set { id = value; }
    }

    private string versionDB;
    /// <summary>
    /// ������ ��, �� ������� ����������� ����������.
    /// </summary>
    public string VersionDB
    {
      get { return versionDB; }
      set { versionDB = value; }
    }

    private string scriptName;
    /// <summary>
    /// ��� �������.
    /// </summary>
    public string ScriptName
    {
      get { return scriptName; }
      set { scriptName = value; }
    }

    private string descr;
    /// <summary>
    /// �������� ������� �������.
    /// </summary>
    public string Descr
    {
      get { return descr; }
      set { descr = value; }
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="id">�������������.</param>
    /// <param name="versionDB">������ ��, �� ������� ����������� ����������.</param>
    /// <param name="scriptName">��� �������.</param>
    /// <param name="descr">�������� ������� �������.</param>
    public LogUpdate(int id, string versionDB, string scriptName, string descr)
    {
      this.id = id;
      this.scriptName = scriptName;
      this.descr = descr;
      this.versionDB = versionDB;
    }
  }
}
