using System;
using System.Data.SqlClient;
using System.Xml.Serialization;

namespace DbUpdater.Entity
{
  /// <summary>
  /// ��������� �����������.
  /// ������� ������ SqlConnectionStringBuilder.
  /// </summary>
  [Serializable()]
  public class SqlDbConnection
  {
    private SqlConnectionStringBuilder csb;

    /// <summary>
    /// ������.
    /// </summary>
    [XmlIgnore()]
    public string DataSource
    {
      get { return csb.DataSource; }
      set { csb.DataSource = value; }
    }

    /// <summary>
    /// ���� ������.
    /// </summary>
    [XmlIgnore()]
    public string InitialCatalog
    {
      get { return csb.InitialCatalog; }
      set { csb.InitialCatalog = value; }
    }

    /// <summary>
    /// ��� ��������������.
    /// </summary>
    [XmlIgnore()]
    public bool IntegratedSecurity
    {
      get { return csb.IntegratedSecurity; }
      set { csb.IntegratedSecurity = value; }
    }

    /// <summary>
    /// �����.
    /// </summary>
    [XmlIgnore()]
    public string UserID
    {
      get { return csb.UserID; }
      set { csb.UserID = value; }
    }

    /// <summary>
    /// ������.
    /// </summary>
    [XmlIgnore()]
    public string Password
    {
      get { return csb.Password; }
      set { csb.Password = value; }
    }

    /// <summary>
    /// ������ �����������.
    /// </summary>
    public string ConnectionString
    {
      get { return csb.ConnectionString; }
      set { csb.ConnectionString = value; }
    }

    public SqlDbConnection()
    {
      csb = new SqlConnectionStringBuilder();
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="connectionString">������ �����������.</param>
    public SqlDbConnection(string connectionString)
    {
      ConnectionString = connectionString;
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="dataSource">������.</param>
    /// <param name="initialCatalog">���� ������.</param>
    /// <param name="integratedSecurity">IntegratedSecurity ��� ���?</param>
    /// <param name="userID">�����.</param>
    /// <param name="password">������.</param>
    public SqlDbConnection(string dataSource, string initialCatalog,
      bool integratedSecurity, string userID, string password)
      : this()
    {
      DataSource = dataSource;
      InitialCatalog = initialCatalog;
      IntegratedSecurity = integratedSecurity;
      UserID = userID;
      Password = password;
    }
  }
}
