using System.Text;

namespace DbUpdater.Entity
{
  /// <summary>
  /// ���������� � MS SQL Server.
  /// </summary>
  public class SQLServerInfo
  {
    private string serverName;
    /// <summary>
    /// ��� �������.
    /// </summary>
    public string ServerName
    {
      get { return serverName; }
    }

    private string instanceName;
    /// <summary>
    /// ��� ����������.
    /// </summary>
    public string InstanceName
    {
      get { return instanceName; }
    }

    /// <summary>
    /// ������ ��� � ������ InstanceName.
    /// </summary>
    public string FullServerName
    {
      get
      {
        StringBuilder result = new StringBuilder(serverName);
        if (!string.IsNullOrEmpty(instanceName))
        {
          result.Append("\\").Append(instanceName);
        }
        return result.ToString();
      }
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="serverName">��� �������.</param>
    /// <param name="instanceName">��� ����������.</param>
    public SQLServerInfo(string serverName, string instanceName)
    {
      this.serverName = serverName;
      this.instanceName = instanceName;
    }
  }
}
