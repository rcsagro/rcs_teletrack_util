using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace DbUpdater.Entity
{
  /// <summary>
  /// ���������. 
  /// </summary>
  [Serializable(), XmlType("Updater")]
  public class UpdaterConfig
  {
    private string baseScriptPath;
    /// <summary>
    /// ���������� ��������.
    /// </summary>
    public string BaseScriptPath
    {
      get { return baseScriptPath; }
      set
      {
        baseScriptPath = value;
        SetPacketsBaseDir();
      }
    }

    private List<Packet> packets;
    /// <summary>
    /// �������� ��������.
    /// </summary>
    public List<Packet> Packets
    {
      get { return packets; }
      set
      {
        packets = value;
        SetPacketsBaseDir();
      }
    }

    public UpdaterConfig() { }

    /// <summary>
    /// ��������� ������� ���������� ��������.
    /// </summary>
    private void SetPacketsBaseDir()
    {
      if ((packets != null) && (!String.IsNullOrEmpty(baseScriptPath)))
      {
        foreach (Packet p in packets)
        {
          p.BaseDir = baseScriptPath;
        }
      }
    }

    /// <summary>
    /// �������������.
    /// </summary>
    public void Init()
    {
      SetPacketsBaseDir();
    }

    /// <summary>
    /// �������� �������. 
    /// </summary>
    /// <exception cref="ApplicationException">���� ���-�� �������.</exception>
    public void Validate()
    {
      if ((!String.IsNullOrEmpty(BaseScriptPath)) &&
        (!Directory.Exists(BaseScriptPath)))
      {
        throw new DirectoryNotFoundException(String.Format(
          "�� ������� ������� ���������� �������� {0}.", BaseScriptPath));
      }

      foreach (Packet p in packets)
      {
        p.Validate();
      }
    }
  }
}
