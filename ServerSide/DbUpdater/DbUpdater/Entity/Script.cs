using System;
using System.IO;
using System.Xml.Serialization;

namespace DbUpdater.Entity
{
  /// <summary>
  /// ���������� � �������.
  /// </summary>
  [Serializable()]
  public class Script
  {
    private string name;
    /// <summary>
    /// ��� ����� �������.
    /// </summary>
    [XmlAttribute()]
    public string Name
    {
      get { return name; }
      set
      {
        name = value;
        updateScriptFile = "";
      }
    }

    private string description;
    /// <summary>
    /// �������� �������.
    /// </summary>
    [XmlAttribute()]
    public string Description
    {
      get { return description; }
      set { description = value; }
    }

    private string dirPath;
    /// <summary>
    /// ���� � ���������� � �������.
    /// ����������� ������� Packet.
    /// </summary>
    public string DirPath
    {
      get { return dirPath; }
      set
      {
        dirPath = value;
        updateScriptFile = "";
      }
    }

    private string updateScriptFile;
    /// <summary>
    /// ������ ���� � ������������ �������.
    /// </summary>
    public string UpdateScriptFile
    {
      get
      {
        if (String.IsNullOrEmpty(updateScriptFile))
        {
          updateScriptFile = String.IsNullOrEmpty(dirPath) ?
            name : Path.Combine(dirPath, name);
        }
        return updateScriptFile;
      }
    }

    public Script()
    {
      name = "";
      description = "";
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="name">��� �������.</param>
    /// <param name="description">�������� �������.</param>
    public Script(string name, string description)
    {
      this.name = name;
      this.description = description;
    }

    /// <summary>
    /// �������� �������. 
    /// </summary>
    /// <exception cref="ApplicationException">���� ���-�� �������.</exception>
    public void Validate()
    {
      if (String.IsNullOrEmpty(name))
      {
        throw new ApplicationException(
          "� �������� ������� �� ������� ��� ����� ������� ���������� (������� Name).");
      }
      if (String.IsNullOrEmpty(description))
      {
        throw new ApplicationException(String.Format(
          "��� ������� {0} �� ������� �������� (������� Description).", name));
      }

      if (!File.Exists(UpdateScriptFile))
      {
        throw new FileNotFoundException(String.Format(
          "�� ������ ���� ������� ���������� �� {0}.", UpdateScriptFile));
      }
    }
  }
}
