using System;
using System.Collections.Generic;

namespace DbUpdater.Entity
{
  /// <summary>
  /// ������ ��.
  /// </summary>
  public class VersionDB : IComparer<VersionDB>, IComparable<VersionDB>
  {
    private byte majorVersion;
    /// <summary>
    /// Major ������.
    /// </summary>
    public byte MajorVersion
    {
      get { return majorVersion; }
      set { majorVersion = value; }
    }

    private byte minorVersion;
    /// <summary>
    /// Minor ������.
    /// </summary>
    public byte MinorVersion
    {
      get { return minorVersion; }
      set { minorVersion = value; }
    }

    private byte buildNumber;
    /// <summary>
    /// ����� �����.
    /// </summary>
    public byte BuildNumber
    {
      get { return buildNumber; }
      set { buildNumber = value; }
    }

    public VersionDB()
    {
      majorVersion = 0;
      minorVersion = 0;
      buildNumber = 0;
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="majorVersion">Major ������.</param>
    /// <param name="minorVersion">Minor ������.</param>
    /// <param name="buildNumber">����� �����.</param>
    public VersionDB(byte majorVersion, byte minorVersion, byte buildNumber)
    {
      this.majorVersion = majorVersion;
      this.minorVersion = minorVersion;
      this.buildNumber = buildNumber;
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="version">������ � ������� MajorVersion.MinorVersion.BuildNumber.</param>
    public VersionDB(string version)
    {
      Parse(version);
    }

    /// <summary>
    /// ������� ������ ������.
    /// </summary>
    /// <param name="version">������ ������ � ������� MajorVersion.MinorVersion.BuildNumber.</param>
    private void Parse(string version)
    {
      string[] splitVersion = version.Split(new char[] { '.' });

      if (splitVersion.Length != 3)
      {
        throw new ArgumentException(String.Format(
          "��������� ������ {0} �� ������������ ������� MajorVersion.MinorVersion.BuildNumber.",
          version));
      }

      majorVersion = Convert.ToByte(splitVersion[0]);
      minorVersion = Convert.ToByte(splitVersion[1]);
      buildNumber = Convert.ToByte(splitVersion[2]);
    }

    /// <summary>
    /// To String.
    /// </summary>
    /// <returns>string</returns>
    public override string ToString()
    {
      return String.Format("{0}.{1}.{2}",
        majorVersion, minorVersion, buildNumber);
    }

    #region IComparer<VersionDB> Members

    /// <summary>
    /// ��������� ���� ��������� VersionDB.
    /// ��� ��������� ��������� ������������ �������� ��� ����� �����.
    /// <para>Less than zero - x is less than y.</para>
    /// <para>Zero - x equals y.</para>
    /// <para>Greater than zero - x is greater than y.</para>
    /// </summary>
    /// <param name="x">VersionDB.</param>
    /// <param name="y">VersionDB.</param>
    /// <returns>
    /// Less than zero - x is less than y.
    /// Zero - x equals y.
    /// Greater than zero - x is greater than y.
    /// </returns>
    public int Compare(VersionDB x, VersionDB y)
    {
      uint intX = Convert.ToUInt32(
        String.Concat(x.MajorVersion, x.MinorVersion, x.BuildNumber));
      uint intY = Convert.ToUInt32(
        String.Concat(y.MajorVersion, y.MinorVersion, y.BuildNumber));

      return intX == intY ? 0 : intX < intY ? -1 : 1;
    }

    #endregion

    #region IComparable<VersionDB> Members

    /// <summary>
    /// ��������� ������ ������ � ������ VersionDB.
    /// ��� ��������� ��������� ������������ �������� ��� ����� �����.
    /// <para>Less than zero - This object is less than the other parameter.</para>
    /// <para>Zero - This object is equal to other.</para>
    /// <para>Greater than zero - This object is greater than other.</para>
    /// </summary>
    /// <param name="other">VersionDB</param>
    /// <returns>
    /// Less than zero - This object is less than the other parameter.
    /// Zero - This object is equal to other. 
    /// Greater than zero - This object is greater than other. 
    /// </returns>
    public int CompareTo(VersionDB other)
    {
      return Compare(this, other);
    }

    #endregion
  }
}
