using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace DbUpdater.Utils
{
  /// <summary>
  /// �������� �����
  /// </summary>
  class FileLogger
  {
    /// <summary>
    /// ���� � ��� ����� SqlUpdater_yyyyMMdd.log. 
    /// </summary>
    public string LogPath
    {
      get
      {
        return String.Format("{0}\\SqlUpdater_{1}.log",
          Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
          DateTime.Now.ToString("yyyyMMdd"));
      }
    }

    /// <summary>
    /// ������ � ����.
    /// </summary>
    /// <param name="text">�����.</param>
    public void Write(string text)
    {
      using (StreamWriter sr = new StreamWriter(LogPath, true, Encoding.Default))
      {
        sr.WriteLine(text);
      }
    }
  }
}
