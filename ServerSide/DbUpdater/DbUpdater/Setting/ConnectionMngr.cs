using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using DbUpdater.Entity;

namespace DbUpdater.Setting
{
  /// <summary>
  /// �������� �������� �����������.
  /// ��������� � ��������� ��������� ����������� �/�� ����(�).
  /// </summary>
  class ConnectionMngr
  {
    private readonly string settingFilePath = Path.Combine(
      Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
      "Connection.xml");

    private SqlDbConnection connection;
    /// <summary>
    /// ��������� �����������.
    /// </summary>
    public SqlDbConnection Connection
    {
      get
      {
        if (connection == null)
        {
          connection = LoadSetting();
        }
        return connection;
      }
    }

    private static ConnectionMngr instance;
    /// <summary>
    /// ��������� ��������� �������� �����������.
    /// </summary>
    public static ConnectionMngr Instance
    {
      get
      {
        if (instance == null)
        {
          instance = new ConnectionMngr();
        }
        return instance;
      }
    }

    private ConnectionMngr() { }

    /// <summary>
    /// �������� ��������� �� �����.
    /// </summary>
    /// <returns>��������� �����������.</returns>
    private SqlDbConnection LoadSetting()
    {
      if (File.Exists(settingFilePath))
      {
        try
        {
          using (StreamReader xmlFile = new StreamReader(settingFilePath, Encoding.Default))
          {
            return (SqlDbConnection)(new XmlSerializer(
              typeof(SqlDbConnection))).Deserialize(xmlFile);
          }
        }
        catch { }
      }

      return new SqlDbConnection();
    }

    /// <summary>
    /// ���������� ��������� � ����.
    /// </summary>
    /// <param name="connection">��������� �����������.</param>
    public void SaveSetting()
    {
      try
      {
        using (StreamWriter xmlFile = new StreamWriter(settingFilePath, false, Encoding.Default))
        {
          (new XmlSerializer(typeof(SqlDbConnection))).Serialize(xmlFile, Connection);
        }
      }
      catch (Exception ex)
      {
        throw new Exception(String.Format(
          "������ ���������� XML ��������� {0} � ����������� �����������.\r\n{1}",
          settingFilePath, ex));
      }
    }
  }
}
