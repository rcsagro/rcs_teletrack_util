using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using DbUpdater.Entity;

namespace DbUpdater.Setting
{
  /// <summary>
  /// �������� �������� ����������. 
  /// </summary>
  class UpdaterConfigMngr
  {
    private readonly string settingFilePath = Path.Combine(
      Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
      "UpdaterConfig.xml");

    private static UpdaterConfigMngr instatnce;
    /// <summary>
    /// ��������� ��������� ��������.
    /// </summary>
    public static UpdaterConfigMngr Instatnce
    {
      get
      {
        if (instatnce == null)
        {
          instatnce = new UpdaterConfigMngr();
        }
        return instatnce;
      }
    }

    private UpdaterConfig config;
    /// <summary>
    /// ���������.
    /// </summary>
    public UpdaterConfig Config
    {
      get { return config; }
    }

    private UpdaterConfigMngr()
    {
      config = LoadSetting();
    }

    /// <summary>
    /// �������� ��������� �� �����.
    /// </summary>
    /// <exception cref="FileNotFoundException">�� ������ ���� ��������</exception>
    /// <returns>��������� �����������.</returns>
    private UpdaterConfig LoadSetting()
    {
      if (File.Exists(settingFilePath))
      {
        UpdaterConfig cfg;
        try
        {
          using (StreamReader xmlFile = new StreamReader(settingFilePath, Encoding.Default))
          {
            cfg = (UpdaterConfig)(new XmlSerializer(
              typeof(UpdaterConfig))).Deserialize(xmlFile);
          }
        }
        catch (Exception ex)
        {
          throw new ApplicationException(String.Format(
            "������ �������� ����� �������� {0}\r\n{1}",
            settingFilePath, ex));
        }

        cfg.Init();
        cfg.Validate();
        return cfg;
      }
      else
      {
        throw new FileNotFoundException(String.Format(
          "�� ������ ���� �������� {0}", settingFilePath));
      }
    }
  }
}
