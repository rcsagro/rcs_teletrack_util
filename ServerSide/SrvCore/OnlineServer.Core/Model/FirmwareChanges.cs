﻿using System;
using System.Data.Linq.Mapping;

namespace RCS.OnlineServer.Core.Model
{
  /// <summary>
  /// Элемент журнала работы с объектами Firmware
  /// </summary>
  [Table(Name = "dbo.FirmwareChanges")]
  public class FirmwareChanges
  {
    /// <summary>
    /// Иденитификатор.
    /// </summary>
    [Column(IsPrimaryKey = true, Storage = "_id", DbType = "bigint NOT NULL IDENTITY", AutoSync = AutoSync.OnInsert,
      IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
    public long Id
    {
      get { return _id; }
    }
    private long _id;

    /// <summary>
    /// Ссылка на прошивку.
    /// </summary>
    [Column(Storage = "_firmwareId", DbType = "bigint NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public long FirmwareId
    {
      get { return _firmwareId; }
    }
    private long _firmwareId;

    /// <summary>
    /// Дата операции.
    /// </summary>
    [Column(Storage = "_date", DbType = "datetime NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public DateTime Date
    {
      get { return _date; }
    }
    private DateTime _date;

    /// <summary>
    /// Описание.
    /// </summary>
    [Column(Storage = "_description", DbType = "nvarchar(1000) NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string Description
    {
      get { return _description; }
    }
    private string _description;

    /// <summary>
    /// Имя пользователя.
    /// </summary>
    [Column(Storage = "_userName", DbType = "nvarchar(255) NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string UserName
    {
      get { return _userName; }
    }
    private string _userName;

    /// <summary>
    /// Ip адрес пользователя.
    /// </summary>
    [Column(Storage = "_userIp", DbType = "nvarchar(50) NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string UserIp
    {
      get { return _userIp; }
    }
    private string _userIp;

    public FirmwareChanges(long firmwareId, string description, string userName, string userIp)
    {
      _firmwareId = firmwareId;
      _date = DateTime.Now;
      _description = description;
      _userName = userName;
      _userIp = userIp;
    }

    public FirmwareChanges()
    {
    }

    public FirmwareChanges(long firmwareId, string description, DateTime date, string userName, string userIp)
    {
      _firmwareId = firmwareId;
      _date = date;
      _description = description;
      _userName = userName;
      _userIp = userIp;
    }
  }
}
