﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace RCS.OnlineServer.Core.Model
{
  /// <summary>
  /// Задача отправки изменений прошивки телетреку.
  /// </summary>
  [Table(Name = "dbo.FirmwareTask")]
  public class FirmwareTask
  {
    /// <summary>
    /// Иденитификатор.
    /// </summary>
    [Column(IsPrimaryKey = true, DbType = "bigint NOT NULL IDENTITY", AutoSync = AutoSync.OnInsert,
      IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
    public long Id { get; set; }

    /// <summary>
    /// Описание.
    /// </summary>
    [Column(DbType = "nvarchar(1000)", UpdateCheck = UpdateCheck.Never)]
    public string Description { get; set; }

    /// <summary>
    /// Логин телетрека.
    /// </summary>
    [Column(DbType = "nchar(4) NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string TeletrackLogin { get; set; }

    /// <summary>
    /// Ссылка на прошивку.
    /// </summary>
    [Column(DbType = "bigint NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public long FirmwareId { get; set; }

    /// <summary>
    /// Дата и время изменений.
    /// </summary>
    [Column(IsVersion = true, DbType = "rowversion NOT NULL", AutoSync = AutoSync.Always,
      CanBeNull = false, IsDbGenerated = true)]
    public Binary VersionStamp { get; set; }

    /// <summary>
    /// Текущая стадия обработки.
    /// </summary>
    [Column(Name = "Stage", Storage = "_stage", DbType = "nvarchar(50) NOT NULL", CanBeNull = false)]
    public string StageDb
    {
      get { return _stage; }
      set { _stage = value; }
    }
    private string _stage;

    public FirmwareTaskStage Stage
    {
      get { return (FirmwareTaskStage)Enum.Parse(typeof(FirmwareTaskStage), _stage); }
      set { _stage = value.ToString(); }
    }

    /// <summary>
    /// Дата создания задачи (первая запись в FirmwareTaskProcessing).
    /// </summary>
    [Column(DbType = "datetime NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public DateTime CreatedDate
    {
      get { return _createdDate; }
      set
      {
        _createdDate = value;
        CreatedDateStr = value.ToString();
      }
    }
    private DateTime _createdDate;

    public string CreatedDateStr
    {
      get { return _createdDateStr; }
      set
      {
        _createdDateStr = value;
        _createdDate = DateTime.Parse(value);
      }
    }
    private string _createdDateStr;

    /// <summary>
    /// Прошивка.
    /// </summary>
    [Association(Storage = "_firmware", ThisKey = "FirmwareId", OtherKey = "Id", IsForeignKey = true)]
    public FirmwareSource Firmware
    {
      get { return _firmware.Entity; }
      set { _firmware.Entity = value; }
    }
    private EntityRef<FirmwareSource> _firmware;

    public FirmwareTask()
    {
    }

    public FirmwareTask(string description, string teletrackLogin,
      long firmwareId, DateTime createdDate, FirmwareTaskStage stage,
      string userName, string userIp)
    {
      Description = description;
      TeletrackLogin = teletrackLogin;
      FirmwareId = firmwareId;
      CreatedDate = createdDate;
      Stage = stage;
    }
  }
}
