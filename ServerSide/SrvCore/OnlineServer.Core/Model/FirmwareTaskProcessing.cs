﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace RCS.OnlineServer.Core.Model
{
  /// <summary>
  /// Элемент журнала обработки задач отправки прошивок.
  /// </summary>
  [Table(Name = "dbo.FirmwareTaskProcessing")]
  public class FirmwareTaskProcessing
  {
    /// <summary>
    /// Иденитификатор.
    /// </summary>
    [Column(IsPrimaryKey = true, Storage = "_id", DbType = "bigint NOT NULL IDENTITY", AutoSync = AutoSync.OnInsert,
      IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
    public long Id
    {
      get { return _id; }
    }
    private long _id;

    /// <summary>
    /// Ссылка на задачу.
    /// </summary>
    [Column(Storage = "_firmwareTaskId", DbType = "bigint NOT NULL", CanBeNull = false,
      UpdateCheck = UpdateCheck.Never)]
    public long FirmwareTaskId
    {
      get { return _firmwareTaskId; }
    }
    private long _firmwareTaskId;

    /// <summary>
    /// Дата операции.
    /// </summary>
    [Column(Storage = "_date", DbType = "datetime NOT NULL", CanBeNull = false,
      UpdateCheck = UpdateCheck.Never)]
    public DateTime Date
    {
      get { return _date; }
    }
    private DateTime _date;

    /// <summary>
    /// Описание.
    /// </summary>
    [Column(DbType = "nvarchar(1000)")]
    public string Description { get; set; }


    /// <summary>
    /// Стадия обработки.
    /// </summary>
    [Column(Storage = "_stage", DbType = "nvarchar(50) NOT NULL", CanBeNull = false,
      UpdateCheck = UpdateCheck.Never)]
    public string Stage
    {
      get { return _stage; }
    }
    private string _stage;

    /// <summary>
    /// Имя пользователя.
    /// </summary>
    [Column(Storage = "_userName", DbType = "nvarchar(255) NOT NULL", CanBeNull = false,
      UpdateCheck = UpdateCheck.Never)]
    public string UserName
    {
      get { return _userName; }
    }
    private string _userName;

    /// <summary>
    /// Ip адрес пользователя.
    /// </summary>
    [Column(Storage = "_userIp", DbType = "nvarchar(50) NOT NULL", CanBeNull = false,
      UpdateCheck = UpdateCheck.Never)]
    public string UserIp
    {
      get { return _userIp; }
    }
    private string _userIp;

    /// <summary>
    /// Задача
    /// </summary>
    [Association(Storage = "_task", ThisKey = "FirmwareTaskId", OtherKey = "Id", IsForeignKey = true)]
    public FirmwareTask Task
    {
      get { return _task.Entity; }
      set { _task.Entity = value; }
    }
    private EntityRef<FirmwareTask> _task;

    public FirmwareTaskProcessing()
    {
    }

    public FirmwareTaskProcessing(long firmwareTaskId, string description,
      FirmwareTaskStage stage, string userName, string userIp)
    {
      _firmwareTaskId = firmwareTaskId;
      _date = DateTime.Now;
      Description = description;
      _stage = stage.ToString();
      _userName = userName;
      _userIp = userIp;
    }
  }
}
