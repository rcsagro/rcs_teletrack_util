﻿
namespace RCS.OnlineServer.Core.Model
{
  /// <summary>
  /// Статусы обработки задач отправки перепрошивки.
  /// </summary>
  public enum FirmwareTaskStage
  {
    /// <summary>
    /// Созданна новая задача.
    /// </summary>
    New,
    /// <summary>
    /// Принята в обработку онлайн-сервером.
    /// </summary>
    Waiting,
    /// <summary>
    /// Отправка телетреку (телетрек на связи) части прошивки.
    /// </summary>
    Sending,
    /// <summary>
    /// Последняя отправка принята телетреком.
    /// </summary>
    ReceivedOk,
    /// <summary>
    /// Последняя отправка не принята телетреком.
    /// </summary>
    ReceivedError,
    /// <summary>
    /// Задача отправки выполнена, телетрек принял все части прошивки.
    /// </summary>
    Completed,
    /// <summary>
    /// Задача не выполнена по причине ошибок или отмены задачи оператором.
    /// </summary>
    Cancelled
  }
}
