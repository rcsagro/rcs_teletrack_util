﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace RCS.OnlineServer.Core.Model
{
  /// <summary>
  /// Прошивка телетрека без текста прошивки.
  /// </summary>
  [Table(Name = "dbo.Firmware")]
  public class Firmware
  {
    /// <summary>
    /// Иденитификатор.
    /// </summary>
    [Column(IsPrimaryKey = true, DbType = "bigint NOT NULL IDENTITY", AutoSync = AutoSync.OnInsert,
      IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
    public long Id { get; set; }

    /// <summary>
    /// Описание.
    /// </summary>
    [Column(DbType = "nvarchar(1000)", UpdateCheck = UpdateCheck.WhenChanged)]
    public string Description { get; set; }

    /// <summary>
    /// Номер версии, для которой предназначены эти изменения. 
    /// </summary>
    [Column(DbType = "nvarchar(25) NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string VersionFrom { get; set; }

    /// <summary>
    /// Номер версии изменений.
    /// </summary>
    [Column(DbType = "nvarchar(25) NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.WhenChanged)]
    public string VersionTo { get; set; }

    /// <summary>
    /// Дата выпуска изменений.
    /// </summary>
    [Column(CanBeNull = false, DbType = "datetime NOT NULL", UpdateCheck = UpdateCheck.WhenChanged)]
    public DateTime IssueDate
    {
      get { return _issueDate; }
      set
      {
        _issueDate = value;
        IssueDateStr = value.ToShortDateString();
      }
    }
    private DateTime _issueDate;


    public string IssueDateStr
    {
      get { return _issueDateStr; }
      set
      {
        _issueDateStr = value;
        _issueDate = DateTime.Parse(value);
      }
    }
    private string _issueDateStr;

    /// <summary>
    /// Имя файла с изменениями прошивки.
    /// </summary>
    [Column(DbType = "nvarchar(255) NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string FileName { get; set; }

    /// <summary>
    /// Возможность использования.
    /// </summary>
    [Column(DbType = "bit NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public bool Enabled { get; set; }

    /// <summary>
    /// Дата и время изменений.
    /// </summary>
    [Column(IsVersion = true, DbType = "rowversion NOT NULL", AutoSync = AutoSync.Always,
      CanBeNull = false, IsDbGenerated = true)]
    public Binary VersionStamp { get; set; }

    /// <summary>
    /// Дата регистрации на сайте. Первая запись из лога.
    /// </summary>
    [Column(CanBeNull = false, DbType = "datetime NOT NULL", UpdateCheck = UpdateCheck.Never)]
    public DateTime RegistryDate { get; set; }

    public Firmware()
    {
    }

    public Firmware(string description, string versionFrom, string versionTo,
      DateTime issueDate, DateTime registryDate, string fileName, bool enabled)
    {
      Description = description;
      VersionFrom = versionFrom;
      VersionTo = versionTo;
      IssueDate = issueDate;
      RegistryDate = registryDate;
      FileName = fileName;
      Enabled = enabled;
    }
  }


  /// <summary>
  /// Прошивка телетрека.
  /// </summary>
  [Table(Name = "dbo.Firmware")]
  public class FirmwareSource
  {
    /// <summary>
    /// Иденитификатор.
    /// </summary>
    [Column(IsPrimaryKey = true, DbType = "bigint NOT NULL IDENTITY", AutoSync = AutoSync.OnInsert,
      IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
    public long Id { get; set; }

    /// <summary>
    /// Описание.
    /// </summary>
    [Column(DbType = "nvarchar(1000)", UpdateCheck = UpdateCheck.WhenChanged)]
    public string Description { get; set; }

    /// <summary>
    /// Номер версии, для которой предназначены эти изменения. 
    /// </summary>
    [Column(DbType = "nvarchar(25) NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string VersionFrom { get; set; }

    /// <summary>
    /// Номер версии изменений.
    /// </summary>
    [Column(DbType = "nvarchar(25) NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.WhenChanged)]
    public string VersionTo { get; set; }

    /// <summary>
    /// Дата выпуска изменений.
    /// </summary>
    [Column(CanBeNull = false, DbType = "datetime NOT NULL", UpdateCheck = UpdateCheck.WhenChanged)]
    public DateTime IssueDate
    {
      get { return _issueDate; }
      set
      {
        _issueDate = value;
        IssueDateStr = value.ToShortDateString();
      }
    }
    private DateTime _issueDate;

    /// <summary>
    /// Вспомогателное свойство для дата выпуска изменений
    /// </summary>
    public string IssueDateStr
    {
      get { return _issueDateStr; }
      set
      {
        _issueDateStr = value;
        _issueDate = DateTime.Parse(value);
      }
    }
    private string _issueDateStr;

    /// <summary>
    /// Имя файла с изменениями прошивки.
    /// </summary>
    [Column(DbType = "nvarchar(255) NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string FileName { get; set; }

    /// <summary>
    /// Возможность использования.
    /// </summary>
    [Column(DbType = "bit NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public bool Enabled { get; set; }

    /// <summary>
    /// Дата и время изменений.
    /// </summary>
    [Column(IsVersion = true, DbType = "rowversion NOT NULL", AutoSync = AutoSync.Always,
      CanBeNull = false, IsDbGenerated = true)]
    public Binary VersionStamp { get; set; }

    /// <summary>
    /// Дата регистрации на сайте. Первая запись из лога.
    /// </summary>
    [Column(CanBeNull = false, DbType = "datetime NOT NULL", UpdateCheck = UpdateCheck.Never)]
    public DateTime RegistryDate { get; set; }

    /// <summary>
    /// Текст команды прошивки.
    /// </summary>
    [Column(DbType = "text NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
    public string Source { get; set; }

    public FirmwareSource()
    {
    }

    public FirmwareSource(string description, string versionFrom, string versionTo,
      DateTime issueDate, DateTime registryDate, string fileName,
      bool enabled, string source)
    {
      Description = description;
      VersionFrom = versionFrom;
      VersionTo = versionTo;
      IssueDate = issueDate;
      RegistryDate = registryDate;
      FileName = fileName;
      Enabled = enabled;
      Source = source;
    }
  }
}
