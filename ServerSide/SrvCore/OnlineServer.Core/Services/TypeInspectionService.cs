﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;

namespace RCS.OnlineServer.Core.Services
{
  /// <summary>
  /// Сервис инспектирования типов.
  /// </summary>
  public class TypeInspectionService
  {
    /// <summary>
    /// Поиск имени свойства, отмеченного атрибутом Column и
    /// представляющего собой первичный ключ (IsPrimaryKey = true).
    /// </summary>
    /// <param name="t">Исследуемый тип.</param>
    /// <returns>Имя свойства или пустая строка, если первичный ключ не найден.</returns>
    public static string GetPKFieldName(Type t)
    {
      var property = t.GetProperties()
        .FirstOrDefault(prop => ((ColumnAttribute[])prop
          .GetCustomAttributes(typeof(ColumnAttribute), false))
          .Any(att => att.IsPrimaryKey));

      return property == null ? string.Empty : property.Name;
    }


    /// <summary>
    /// Список свойств, которые можно редактировать. Исключения: 
    /// свойства, которые нельзя изменить; 
    /// отмеченные атрибутом Column и представляющие собой первичный
    /// ключ (IsPrimaryKey = true) или версию (IsVersion = true).
    /// </summary>
    /// <param name="t">Исследуемый тип.</param>
    /// <returns>Имя свойства или пустая строка, если первичный ключ не найден.</returns>
    public static IEnumerable<PropertyInfo> GetEditableProperties(Type t)
    {
      return t.GetProperties()
        .Where(prop => (prop.CanWrite) && (
          ((ColumnAttribute[])prop.GetCustomAttributes(typeof(ColumnAttribute), false))
          .Any(att => (!att.IsPrimaryKey) && (!att.IsVersion))));
    }

    /// <summary>
    /// Отмечен ли тип атрибутом Table.
    /// </summary>
    /// <param name="t">Тип.</param>
    /// <returns></returns>
    public static bool IsTypeRepresentTable(Type t)
    {
      return t.GetCustomAttributes(typeof(TableAttribute), true).Length > 0;
    }
  }
}
