﻿using System;

namespace RCS.OnlineServer.Core.Services
{
  public static class FirmwareService
  {
    public static bool IsTextFirmwareChanges(string text)
    {
      if (string.IsNullOrEmpty(text))
      {
        throw new ArgumentException("text");
      }

      return text.StartsWith("VER")
        && text.Contains("DATA")
        && text.EndsWith("RESET\r\n");
    }

    public static string GetVersion(string text)
    {
      if (!IsTextFirmwareChanges(text))
      {
        throw new ArgumentException("The text is not a firmware", "text");
      }
      return text.Substring(5, text.IndexOf("\r\n", 5) - 5);
    }
  }
}
