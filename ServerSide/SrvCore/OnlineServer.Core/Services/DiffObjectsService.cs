﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace RCS.OnlineServer.Core.Services
{
  /// <summary>
  /// Описание различия содержимого свойства двух объектов
  /// </summary>
  public class DiffPropertyInfo
  {
    public readonly PropertyInfo PropInfo;
    public readonly object FirstValue;
    public readonly object SecondValue;

    public DiffPropertyInfo(PropertyInfo propInfo, object firstValue, object secondValue)
    {
      PropInfo = propInfo;
      FirstValue = firstValue;
      SecondValue = secondValue;
    }
  }

  /// <summary>
  /// Находит различия в содержимом объектов
  /// </summary>
  public class DiffObjectsService
  {
    /// <summary>
    /// Сравнение содержимого редактируемых свойств двух объектов одного типа,
    /// представляющих собой проекцию таблицы БД (отмеченные атрибутом Table).
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    /// <typeparam name="T">Тип сравнимаемых объектов.</typeparam>
    /// <param name="first">Первый объект.</param>
    /// <param name="second">Второй объект.</param>
    /// <returns>Список DiffPropertyInfo.</returns>
    public static IList<DiffPropertyInfo> GetEditablePropsDiff<T>(T first, T second) where T : class
    {
      if (first == null)
      {
        throw new ArgumentNullException("first");
      }
      if (second == null)
      {
        throw new ArgumentNullException("second");
      }

      IList<DiffPropertyInfo> result = new List<DiffPropertyInfo>();

      foreach (var p in TypeInspectionService.GetEditableProperties(typeof(T)))
      {
        object value1 = p.GetValue(first, null);
        object value2 = p.GetValue(second, null);
        if (value1 != null && value2 != null && !value1.Equals(value2))
        {
          result.Add(new DiffPropertyInfo(p, value1, value2));
        }
        else if ((value1 == null && value2 != null) ||
          ((value1 != null && value2 == null)))
        {
          result.Add(new DiffPropertyInfo(p, value1, value2));
        }
      }
      return result;
    }
  }
}
