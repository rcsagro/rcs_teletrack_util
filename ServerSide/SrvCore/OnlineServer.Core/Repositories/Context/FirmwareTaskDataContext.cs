﻿using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using RCS.OnlineServer.Core.Model;

namespace RCS.OnlineServer.Core.Repositories.Context
{
  public class FirmwareTaskDataContext : DataContext, IFirmwareTaskDataContext
  {
    #region IFirmwareTaskDataContext Members

    public Table<FirmwareTask> Tasks
    {
      get { return GetTable<FirmwareTask>(); }
    }

    public Table<FirmwareTaskProcessing> Processing
    {
      get { return GetTable<FirmwareTaskProcessing>(); }
    }

    public DataContext CurrentContext
    {
      get { return this; }
    }

    #endregion

    public FirmwareTaskDataContext(string connection)
      : base(connection) { }
    public FirmwareTaskDataContext(IDbConnection connection)
      : base(connection) { }
    public FirmwareTaskDataContext(IDbConnection connection, MappingSource mapping)
      : base(connection, mapping) { }
    public FirmwareTaskDataContext(string connection, MappingSource mapping)
      : base(connection, mapping) { }
  }
}
