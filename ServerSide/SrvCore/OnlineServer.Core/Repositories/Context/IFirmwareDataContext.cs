﻿using System.Data.Linq;
using RCS.OnlineServer.Core.Model;

namespace RCS.OnlineServer.Core.Repositories.Context
{
  public interface IFirmwareDataContext
  {
    Table<Firmware> Firmwares { get; }
    Table<FirmwareSource> FirmwareSources { get; }
    Table<FirmwareChanges> Changes { get; }
    DataContext CurrentContext { get; }
  }
}
