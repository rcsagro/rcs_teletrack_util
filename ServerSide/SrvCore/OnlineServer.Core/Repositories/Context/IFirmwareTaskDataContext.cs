﻿using System.Data.Linq;
using RCS.OnlineServer.Core.Model;

namespace RCS.OnlineServer.Core.Repositories.Context
{
  public interface IFirmwareTaskDataContext
  {
    Table<FirmwareTask> Tasks { get; }
    Table<FirmwareTaskProcessing> Processing { get; }
    DataContext CurrentContext { get; }
  }
}
