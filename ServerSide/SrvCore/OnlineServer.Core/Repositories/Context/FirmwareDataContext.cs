﻿using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using RCS.OnlineServer.Core.Model;

namespace RCS.OnlineServer.Core.Repositories.Context
{
  public class FirmwareDataContext : DataContext, IFirmwareDataContext
  {
    public Table<Firmware> Firmwares
    {
      get { return GetTable<Firmware>(); }
    }

    public Table<FirmwareSource> FirmwareSources
    {
      get { return GetTable<FirmwareSource>(); }
    }

    public Table<FirmwareChanges> Changes
    {
      get { return GetTable<FirmwareChanges>(); }
    }

    public DataContext CurrentContext
    {
      get { return this; }
    }

    public FirmwareDataContext(string connection)
      : base(connection) { }
    public FirmwareDataContext(IDbConnection connection)
      : base(connection) { }
    public FirmwareDataContext(IDbConnection connection, MappingSource mapping)
      : base(connection, mapping) { }
    public FirmwareDataContext(string connection, MappingSource mapping)
      : base(connection, mapping) { }
  }
}
