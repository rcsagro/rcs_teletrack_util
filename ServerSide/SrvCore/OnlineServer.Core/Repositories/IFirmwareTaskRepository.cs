﻿using System.Collections.Generic;
using System.Linq;
using RCS.OnlineServer.Core.Model;

namespace RCS.OnlineServer.Core.Repositories
{
  public interface IFirmwareTaskRepository
  {
    FirmwareTask GetById(long id);
    IQueryable<FirmwareTask> GetAllTasks();
    /// <summary>
    /// Выборка прошивок, которые необходимо отправить телетреку.
    /// </summary>
    /// <param name="loginTT">логин телетрека.</param>
    /// <returns></returns>
    FirmwareTask GetReadyTasksForSending(string loginTT);

    List<FirmwareTask> GetTasksPage(string sortExpr, int pageIndex, int pageSize);
    List<FirmwareTask> GetTasksPageById(long firmwareId, string sortExpr, int pageIndex, int pageSize);
    int GetTasksCount();
    int GetTasksCountById(long firmwareId);
    void Insert(FirmwareTask entity, string userName, string userIp);
    void Update(FirmwareTask entity, string userName, string userIp);
    void SetTaskCancelled(long taskId, string userName, string userIp);

    List<FirmwareTaskProcessing> GetProcessingPage(string sortExpr, int pageIndex, int pageSize);
    List<FirmwareTaskProcessing> GetProcessingPageById(long taskId, string sortExpr, int pageIndex, int pageSize);
    int GetProcCount();
    int GetProcCountById(long taskId);
  }
}
