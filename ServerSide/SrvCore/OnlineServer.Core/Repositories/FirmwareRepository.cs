﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Transactions;
using RCS.OnlineServer.Core.Model;
using RCS.OnlineServer.Core.Repositories.Context;
using RCS.OnlineServer.Core.Services;

namespace RCS.OnlineServer.Core.Repositories
{
  /// <summary>
  /// Репозиторий объектов Firmware
  /// </summary>
  public class FirmwareRepository : IFirmwareRepository
  {
    private readonly IFirmwareDataContext _context;

    public FirmwareRepository(IFirmwareDataContext dataContext)
    {
      _context = dataContext;
    }

    #region IRepository<T> Members

    private static readonly Func<FirmwareDataContext, long, Firmware> SelectById =
      CompiledQuery.Compile((FirmwareDataContext context, long id) =>
        context.Firmwares.Where(p => p.Id == id).SingleOrDefault());

    public Firmware GetById(long id)
    {
      return SelectById((FirmwareDataContext)_context, id);
    }

    private static readonly Func<FirmwareDataContext, IQueryable<Firmware>> SelectAll =
      CompiledQuery.Compile((FirmwareDataContext context) =>
        from f in context.Firmwares select f);

    public IQueryable<Firmware> GetAllFirmwares()
    {
      return _context.Firmwares.AsQueryable();
    }

    public List<Firmware> GetFirmwarePage(string sortExpr, int pageIndex, int pageSize)
    {
      return _context.Firmwares
        .OrderBy(string.IsNullOrEmpty(sortExpr) ? "Id" : sortExpr)
        .Skip(pageIndex)
        .Take(pageSize)
        .ToList();
    }

    public int GetFirmwareCount()
    {
      return GetAllFirmwares().Count();
    }

    public List<FirmwareChanges> GetChangesPage(string sortExpr, int pageIndex, int pageSize)
    {
      return _context.Changes
        .OrderBy(string.IsNullOrEmpty(sortExpr) ? "Id DESC" : sortExpr)
        .Skip(pageIndex)
        .Take(pageSize)
        .ToList();
    }

    public List<FirmwareChanges> GetChangesPageById(long firmwareId, string sortExpr, int pageIndex, int pageSize)
    {
      return _context.Changes
        .Where(p => p.FirmwareId == firmwareId)
        .OrderBy(string.IsNullOrEmpty(sortExpr) ? "Id DESC" : sortExpr)
        .Skip(pageIndex)
        .Take(pageSize)
        .ToList();
    }

    public IQueryable<FirmwareChanges> GetAllChanges()
    {
      return _context.Changes.AsQueryable();
    }

    public int GetChangesCount()
    {
      return GetAllChanges().Count();
    }

    public int GetChangesCountById(long firmwareId)
    {
      return GetAllChanges()
        .Where(p => p.FirmwareId == firmwareId)
        .Count();
    }

    public void Insert(FirmwareSource entity, string userName, string userIp)
    {
      using (var trans = new TransactionScope())
      {
        _context.FirmwareSources.InsertOnSubmit(entity);
        _context.CurrentContext.SubmitChanges();

        _context.Changes.InsertOnSubmit(new FirmwareChanges(entity.Id,
          "Firmware was added to repository", entity.RegistryDate, userName, userIp));
        _context.CurrentContext.SubmitChanges();

        trans.Complete();
      }
    }

    public void Update(Firmware entity, string userName, string userIp)
    {
      Firmware original = _context.Firmwares.Where(p => p.Id == entity.Id).SingleOrDefault();
      if (original == null)
      {
        new ChangeConflictException(string.Format(
          "Entity Firmware with id {0} was not been found in DB", entity.Id));
      }

      IList<DiffPropertyInfo> diff = DiffObjectsService.GetEditablePropsDiff<Firmware>(original, entity);
      if (diff.Count == 0)
      {
        return;
      }

      original.Description = entity.Description;
      original.Enabled = entity.Enabled;
      original.IssueDate = entity.IssueDate;
      original.VersionTo = entity.VersionTo;

      _context.Changes.InsertOnSubmit(new FirmwareChanges(entity.Id,
        GetDiffDescription(diff), userName, userIp));

      _context.CurrentContext.SubmitChanges();
    }

    private static readonly Func<FirmwareDataContext, long, string> SelectSourceById =
      CompiledQuery.Compile((FirmwareDataContext context, long id) =>
        context.FirmwareSources
          .Where(p => p.Id == id)
          .SingleOrDefault()
          .Source);

    public string GetSourceById(long id)
    {
      return SelectSourceById((FirmwareDataContext)_context, id);
    }

    #endregion

    private static string GetDiffDescription(IList<DiffPropertyInfo> diffList)
    {
      StringBuilder result = new StringBuilder();

      foreach (var d in diffList)
      {
        if (result.Length > 0)
        {
          result.Append("; ");
        }
        result.AppendFormat("Propery {0}: {1} -> {2}",
          d.PropInfo.Name,
          d.FirstValue ?? "null",
          d.SecondValue ?? "null");
      }
      if (result.Length > 0)
      {
        result.Append(".");
      }
      return result.ToString();
    }
  }
}
