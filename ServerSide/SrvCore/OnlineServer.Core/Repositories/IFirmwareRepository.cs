﻿using System.Collections.Generic;
using System.Linq;
using RCS.OnlineServer.Core.Model;

namespace RCS.OnlineServer.Core.Repositories
{
  /// <summary>
  /// Контракт репозитария Firmware.
  /// </summary>
  public interface IFirmwareRepository
  {
    /// <summary>
    /// Получить объект по первичному ключу.
    /// </summary>
    /// <param name="id">Значение первичного ключа.</param>
    /// <returns>Firmware.</returns>
    Firmware GetById(long id);
    List<Firmware> GetFirmwarePage(string sortExpr, int pageIndex, int pageSize);
    int GetFirmwareCount();
    void Insert(FirmwareSource entity, string userName, string userIp);
    void Update(Firmware entity, string userName, string userIp);
    string GetSourceById(long id);

    List<FirmwareChanges> GetChangesPage(string sortExpr, int pageIndex, int pageSize);
    List<FirmwareChanges> GetChangesPageById(long firmwareId, string sortExpr, int pageIndex, int pageSize);
    int GetChangesCount();
    int GetChangesCountById(long firmwareId);
  }
}
