﻿using System;
using System.Data.Linq;
using RCS.OnlineServer.Core.Repositories.Context;

namespace RCS.OnlineServer.Core.Repositories
{
  /// <summary>
  /// Фабрика репозиториев.
  /// </summary>
  public class RepositoryFactory
  {
    private readonly string _connectionString;
    private readonly int? _commandTimeout;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана
    /// строка подключения к БД.</exception>
    /// <param name="connectionString">Строка подключения к БД.</param>
    public RepositoryFactory(string connectionString)
    {
      if (string.IsNullOrEmpty(connectionString))
      {
        throw new ArgumentNullException("connectionString");
      }
      _connectionString = connectionString;
    }

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана
    /// строка подключения к БД.</exception>
    /// <param name="connectionString">Строка подключения к БД.</param>
    /// <param name="commandTimeout">Таймаут ожидания, в секундах 
    /// (по умолчанию 30 сек.)</param>
    public RepositoryFactory(string connectionString, int commandTimeout)
    {
      if (string.IsNullOrEmpty(connectionString))
      {
        throw new ArgumentNullException("connectionString");
      }
      if (commandTimeout < 30)
      {
        commandTimeout = 30;
      }
      _connectionString = connectionString;
      _commandTimeout = commandTimeout;
    }

    public IFirmwareRepository GetFirmwareRepository()
    {
      FirmwareDataContext ctx = new FirmwareDataContext(_connectionString);
      SetCommandTimeout(ctx);
      return new FirmwareRepository(ctx);
    }

    public IFirmwareTaskRepository GetFirmwareTaskRepository()
    {
      FirmwareTaskDataContext ctx = new FirmwareTaskDataContext(_connectionString);
      SetCommandTimeout(ctx);
      return new FirmwareTaskRepository(ctx);
    }

    private void SetCommandTimeout(DataContext ctx)
    {
      if (_commandTimeout.HasValue)
      {
        ctx.CommandTimeout = _commandTimeout.Value;
      }
    }
  }
}
