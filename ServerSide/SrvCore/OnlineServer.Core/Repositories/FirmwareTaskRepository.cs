﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using RCS.OnlineServer.Core.Model;
using RCS.OnlineServer.Core.Repositories.Context;

namespace RCS.OnlineServer.Core.Repositories
{
  public class FirmwareTaskRepository : IFirmwareTaskRepository
  {
    private readonly IFirmwareTaskDataContext _context;

    public FirmwareTaskRepository(IFirmwareTaskDataContext dataContext)
    {
      _context = dataContext;
    }

    #region IFirmwareTaskRepository Members

    private static readonly Func<FirmwareTaskDataContext, long, FirmwareTask> SelectById =
      CompiledQuery.Compile((FirmwareTaskDataContext context, long id) =>
        context.Tasks.Where(p => p.Id == id).SingleOrDefault());

    public FirmwareTask GetById(long id)
    {
      return SelectById((FirmwareTaskDataContext)_context, id);
    }

    private static readonly Func<FirmwareTaskDataContext, IQueryable<FirmwareTask>> SelectAllTasks =
      CompiledQuery.Compile((FirmwareTaskDataContext context) =>
        from f in context.Tasks select f);

    public IQueryable<FirmwareTask> GetAllTasks()
    {
      return SelectAllTasks((FirmwareTaskDataContext)_context);
    }

    private static readonly Func<FirmwareTaskDataContext, IQueryable<FirmwareTaskProcessing>> SelectAllProcRows =
      CompiledQuery.Compile((FirmwareTaskDataContext context) =>
        from f in context.Processing select f);

    public IQueryable<FirmwareTaskProcessing> GetAllProc()
    {
      return SelectAllProcRows((FirmwareTaskDataContext)_context);
    }

    public FirmwareTask GetReadyTasksForSending(string loginTT)
    {
      DataLoadOptions opt = new DataLoadOptions();
      opt.LoadWith<FirmwareTask>(t => t.Firmware);
      _context.CurrentContext.LoadOptions = opt;

      _context.CurrentContext.DeferredLoadingEnabled = false;

      return _context.Tasks
        .Where(p => p.Firmware.Enabled
          && p.TeletrackLogin == loginTT
          && p.StageDb != FirmwareTaskStage.Cancelled.ToString()
          && p.StageDb != FirmwareTaskStage.Completed.ToString())
        .Take(1)
        .SingleOrDefault();
    }

    public List<FirmwareTask> GetTasksPage(string sortExpr, int pageIndex, int pageSize)
    {
      return _context.Tasks
        .OrderBy(string.IsNullOrEmpty(sortExpr) ? "Id DESC" : sortExpr)
        .Skip(pageIndex)
        .Take(pageSize)
        .ToList();
    }

    public List<FirmwareTask> GetTasksPageById(long firmwareId, string sortExpr, int pageIndex, int pageSize)
    {
      return _context.Tasks
        .Where(p => p.FirmwareId == firmwareId)
        .OrderBy(string.IsNullOrEmpty(sortExpr) ? "Id DESC" : sortExpr)
        .Skip(pageIndex)
        .Take(pageSize)
        .ToList();
    }

    public int GetTasksCount()
    {
      return GetAllTasks().Count();
    }

    public int GetTasksCountById(long firmwareId)
    {
      return GetAllTasks()
        .Where(p => p.FirmwareId == firmwareId)
        .Count();
    }

    public void Insert(FirmwareTask entity, string userName, string userIp)
    {
      using (var trans = new TransactionScope())
      {
        _context.Tasks.InsertOnSubmit(entity);
        _context.CurrentContext.SubmitChanges();

        _context.Processing.InsertOnSubmit(new FirmwareTaskProcessing(
          entity.Id, FormatTaskDescription(entity, userName),
          FirmwareTaskStage.New, userName, userIp));
        _context.CurrentContext.SubmitChanges();

        trans.Complete();
      }
    }

    public void Update(FirmwareTask entity, string userName, string userIp)
    {
      FirmwareTask original = _context.Tasks.Where(p => p.Id == entity.Id).SingleOrDefault();
      if (original == null)
      {
        new ChangeConflictException(string.Format(
          "Entity FirmwareTask with id {0} was not been found in DB", entity.Id));
      }

      string originalStageDb = original.StageDb;

      original.Description = entity.Description;
      original.StageDb = entity.StageDb;

      if (originalStageDb != entity.StageDb)
      {
        _context.Processing.InsertOnSubmit(new FirmwareTaskProcessing(
          entity.Id,
          FormatStageChangedDescription(originalStageDb, entity.StageDb),
          entity.Stage, userName, userIp));
      }

      _context.CurrentContext.SubmitChanges();
    }

    public void SetTaskCancelled(long taskId, string userName, string userIp)
    {
      FirmwareTask original = _context.Tasks.Where(p => p.Id == taskId).SingleOrDefault();
      if (original == null)
      {
        new ChangeConflictException(string.Format(
          "Entity FirmwareTask with id {0} was not been found in DB", taskId));
      }

      original.Stage = FirmwareTaskStage.Cancelled;

      _context.Processing.InsertOnSubmit(new FirmwareTaskProcessing(
        taskId, "Task was cancelled by operator", FirmwareTaskStage.Cancelled, userName, userIp));

      _context.CurrentContext.SubmitChanges();
    }

    public List<FirmwareTaskProcessing> GetProcessingPage(string sortExpr, int pageIndex, int pageSize)
    {
      DataLoadOptions opt = new DataLoadOptions();
      opt.LoadWith<FirmwareTaskProcessing>(t => t.Task);
      _context.CurrentContext.LoadOptions = opt;

      _context.CurrentContext.DeferredLoadingEnabled = false;

      return _context.Processing
        .OrderBy(string.IsNullOrEmpty(sortExpr) ? "Id DESC" : sortExpr)
        .Skip(pageIndex)
        .Take(pageSize)
        .ToList();
    }

    public List<FirmwareTaskProcessing> GetProcessingPageById(long taskId,
      string sortExpr, int pageIndex, int pageSize)
    {
      return _context.Processing
        .Where(p => p.FirmwareTaskId == taskId)
        .OrderBy(string.IsNullOrEmpty(sortExpr) ? "Id DESC" : sortExpr)
        .Skip(pageIndex)
        .Take(pageSize)
        .ToList();
    }

    public int GetProcCount()
    {
      return GetAllProc().Count();
    }

    public int GetProcCountById(long taskId)
    {
      return GetAllProc()
       .Where(p => p.FirmwareTaskId == taskId)
       .Count();
    }

    #endregion

    private const string FIRMWARE_TASK_CREATED = "{0} created task {1} for teletrack {2}";

    private static string FormatTaskDescription(FirmwareTask entity, string userName)
    {
      return string.Format(FIRMWARE_TASK_CREATED, userName, entity.Description, entity.TeletrackLogin);
    }

    private const string STAGE_CHANGED = "Stage {0} changed on {1}";

    private static string FormatStageChangedDescription(string originalStage, string currentStage)
    {
      return string.Format(STAGE_CHANGED, originalStage, currentStage);
    }
  }
}
