﻿using System;
using NUnit.Framework;
using RCS.OnlineServer.Core.Model;

namespace RCS.OnlineServer.Core.Test.Model
{
  [TestFixture(Description = "Проверка создания объекта Firmware.")]
  class FirmwareInstance
  {
    private object[] _initData = { new object[] { 
      "Даунгрейд с версии 1.3 до 1.0.", "1.3", "1.0", new DateTime(2011, 7, 1), 
      new DateTime(2011, 7, 3), "tt_420_421.bineml", true} 
    };

    [Test(Description = "Свойства заполнены корректно при использовании конструктора с параметрами."),
      TestCaseSource("_initData")]
    public void CorrectFieldsSetupUsingCtor(string description, string versionFrom, string versionTo,
      DateTime issueDate, DateTime registryDate, string fileName, bool enabled)
    {
      Firmware firmware = new Firmware(description, versionFrom, versionTo,
        issueDate, registryDate, fileName, enabled);

      Assert.AreEqual(versionFrom, firmware.VersionFrom);
      Assert.AreEqual(versionTo, firmware.VersionTo);

      Assert.AreEqual(issueDate, firmware.IssueDate);
      Assert.AreEqual(registryDate, firmware.RegistryDate);

      Assert.AreEqual(fileName, firmware.FileName);
      Assert.AreEqual(enabled, firmware.Enabled);
    }
  }
}
