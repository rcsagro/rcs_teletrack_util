﻿using System;
using NUnit.Framework;
using RCS.OnlineServer.Core.Model;

namespace RCS.OnlineServer.Core.Test.Model
{
  [TestFixture(Description = "Проверка создания объекта FirmwareTaskProcessin.")]
  class FirmwareTaskProcessingInstance
  {
    private object[] _initData = { new object[] { 10, "Created by user", FirmwareTaskStage.Completed, 
      "Admin", "192.168.0.23"} };

    [Test(Description = "Свойства заполнены корректно при использовании конструктора с параметрами."),
      TestCaseSource("_initData")]
    public void CorrectFieldsSetupUsingCtor(long firmwateTaskId, string description, FirmwareTaskStage stage,
      string userName, string userIp)
    {
      DateTime timeBeforeCreation = DateTime.Now;
      FirmwareTaskProcessing ftp = new FirmwareTaskProcessing(firmwateTaskId, description, stage, userName, userIp);
      DateTime timeAfterCreation = DateTime.Now;

      Assert.GreaterOrEqual(ftp.Date, timeBeforeCreation);
      Assert.LessOrEqual(ftp.Date, timeAfterCreation);
      Assert.AreEqual(default(long), ftp.Id);
      Assert.AreEqual(firmwateTaskId, ftp.FirmwareTaskId);
      Assert.AreEqual(description, ftp.Description);
      Assert.AreEqual(stage, ftp.Stage);
      Assert.AreEqual(userName, ftp.UserName);
      Assert.AreEqual(userIp, ftp.UserIp);
    }
  }
}
