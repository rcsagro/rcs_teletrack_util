﻿using System;
using NUnit.Framework;
using RCS.OnlineServer.Core.Model;

namespace RCS.OnlineServer.Core.Test.Model
{
  [TestFixture(Description = "Проверка создания объекта FirmwareTask.")]
  class FirmwareTaskInstance
  {
    private object[] _initData = { new object[] { 
      "Created by user", "A001", 21, DateTime.Now, FirmwareTaskStage.New, "RCS", "192.168.0.25"} };

    [Test(Description = "Свойства заполнены корректно при использовании конструктора с параметрами."),
      TestCaseSource("_initData")]
    public void CorrectFieldsSetupUsingCtor(string description, string teletrackLogin, long firmware,
      DateTime createdDate, FirmwareTaskStage stage, string userName, string userIp)
    {
      FirmwareTask task = new FirmwareTask(description, teletrackLogin, firmware, createdDate, stage, userName, userIp);

      Assert.AreEqual(default(long), task.Id);
      Assert.AreEqual(firmware, task.FirmwareId);
      Assert.AreEqual(description, task.Description);
      Assert.AreEqual(teletrackLogin, task.TeletrackLogin);

      Assert.AreEqual(createdDate, task.CreatedDate);
      Assert.AreEqual(stage, task.StageDb);
    }
  }
}
