﻿using System;
using NUnit.Framework;
using RCS.OnlineServer.Core.Model;

namespace RCS.OnlineServer.Core.Test.Model
{
  [TestFixture(Description = "Проверка создания объекта FirmwareChange.")]
  class FirmwareChangesInstance
  {
    private object[] _initData = { new object[] { 0, "Insert", "Admin", "192.168.0.34" } };

    [Test(Description = "Свойства заполнены корректно при использовании конструктора с параметрами."),
      TestCaseSource("_initData")]
    public void CorrectFieldsSetupUsingCtor(long firmwareId, string description, string userName, string userIp)
    {
      DateTime timeBeforeCreation = DateTime.Now;
      FirmwareChanges firmwareChanges = new FirmwareChanges(firmwareId, description, userName, userIp);
      DateTime timeAfterCreation = DateTime.Now;

      Assert.GreaterOrEqual(firmwareChanges.Date, timeBeforeCreation);
      Assert.LessOrEqual(firmwareChanges.Date, timeAfterCreation);
      Assert.AreEqual(default(long), firmwareChanges.Id);
      Assert.AreEqual(firmwareId, firmwareChanges.FirmwareId);
      Assert.AreEqual(description, firmwareChanges.Description);
      Assert.AreEqual(userName, firmwareChanges.UserName);
      Assert.AreEqual(userIp, firmwareChanges.UserIp);
    }

    private object[] _initData2 = { new object[] { 0, "Insert", DateTime.Now, "Admin", "192.168.0.34" } };

    [Test(Description = "Свойства заполнены корректно при использовании конструктора с параметрами."),
      TestCaseSource("_initData2")]
    public void CorrectFieldsSetupUsingCtor(long firmwareId, string description, DateTime date, string userName, string userIp)
    {
      FirmwareChanges firmwareChanges = new FirmwareChanges(firmwareId, description, date, userName, userIp);

      Assert.AreEqual(default(long), firmwareChanges.Id);
      Assert.AreEqual(firmwareId, firmwareChanges.FirmwareId);
      Assert.AreEqual(description, firmwareChanges.Description);
      Assert.AreEqual(date, firmwareChanges.Date);
      Assert.AreEqual(userName, firmwareChanges.UserName);
      Assert.AreEqual(userIp, firmwareChanges.UserIp);
    }
  }
}
