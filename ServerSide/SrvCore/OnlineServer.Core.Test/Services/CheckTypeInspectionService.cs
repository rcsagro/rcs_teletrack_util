﻿using System.Linq;
using NUnit.Framework;
using RCS.OnlineServer.Core.Model;
using RCS.OnlineServer.Core.Services;

namespace RCS.OnlineServer.Core.Test.Services
{
  [TestFixture(Description = "Проверка работы сервиса-инспектора типов.")]
  class CheckTypeInspectionService
  {
    [Test(Description = "Метод GetPKFieldName.")]
    public void Call_GetPKFieldName()
    {
      Assert.AreEqual(string.Empty, TypeInspectionService.GetPKFieldName(typeof(string)));
      Assert.AreEqual("Id", TypeInspectionService.GetPKFieldName(typeof(Firmware)));
    }

    [Test(Description = "Метод GetEditableProperties.")]
    public void Call_GetEditableProperties()
    {
      Assert.AreEqual(0, TypeInspectionService.GetEditableProperties(typeof(string)).Count());
      Assert.AreEqual(4, TypeInspectionService.GetEditableProperties(typeof(Firmware)).Count());
    }

    [Test(Description = "Метод IsTypeRepresentTable.")]
    public void Call_IsTypeRepresentTable()
    {
      Assert.AreEqual(false, TypeInspectionService.IsTypeRepresentTable(typeof(string)));
      Assert.AreEqual(true, TypeInspectionService.IsTypeRepresentTable(typeof(Firmware)));
    }
  }
}
