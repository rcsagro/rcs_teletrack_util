﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using RCS.OnlineServer.Core.Model;
using RCS.OnlineServer.Core.Services;

namespace RCS.OnlineServer.Core.Test.Services
{
  [TestFixture(Description = "Проверка работы сервиса сравнения различий в объектах.")]
  class CheckDiffObjectsService
  {
    [Test(Description = "Метод GetEditablePropsDiff.")]
    public void Call_GetEditablePropsDiff()
    {
      DateTime issueDate = new DateTime(2011, 02, 20);
      Firmware first = new Firmware("first", "2.1", "3.2", issueDate, DateTime.Now, "file", true);
      Firmware second = new Firmware("second", "2.1", "3.1", issueDate, DateTime.Now, "file", true);
      
      IList<DiffPropertyInfo> diff = DiffObjectsService.GetEditablePropsDiff<Firmware>(first, second);
      Assert.AreEqual(2, diff.Count);
      Assert.AreEqual("Description", diff[0].PropInfo.Name);
      Assert.AreEqual("first", diff[0].FirstValue);
      Assert.AreEqual("second", diff[0].SecondValue);
      Assert.AreEqual("VersionTo", diff[1].PropInfo.Name);
      Assert.AreEqual("3.2", diff[1].FirstValue);
      Assert.AreEqual("3.1", diff[1].SecondValue);
    }

    [Test(Description = "Передача в метод GetEditablePropsDiff null параметров.")]
    public void Call_GetEditablePropsDiff_WithNullParams()
    {
      Firmware fw = new Firmware("first", "2.1", "3.2", DateTime.Now, DateTime.Now, "file", true);
      Assert.Throws<ArgumentNullException>(delegate
      {
        DiffObjectsService.GetEditablePropsDiff<Firmware>(null, fw);
      });
      Assert.Throws<ArgumentNullException>(delegate
      {
        DiffObjectsService.GetEditablePropsDiff<Firmware>(fw, null);
      });
    }
  }
}
