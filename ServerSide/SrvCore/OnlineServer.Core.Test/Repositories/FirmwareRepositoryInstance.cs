﻿using System;
using NUnit.Framework;
using RCS.OnlineServer.Core.Model;
using RCS.OnlineServer.Core.Repositories;
using RCS.OnlineServer.Core.Repositories.Context;

namespace RCS.OnlineServer.Core.Test.Repositories
{
  [TestFixture(Description = "Создание FirmwareRepository.")]
  class FirmwareRepositoryInstance
  {
    [Test(Description = "Создание с использованием фабрики.")]
    public void CreateUsingFactory()
    {
      IFirmwareRepository r = new RepositoryFactory("Data Source=srvavs")
          .GetFirmwareRepository();
      Assert.NotNull(r);
    }


    [Test(Description = "Создание с использованием фабрики.")]
    [Ignore("Это часть интеграционных тестов")]
    public void Insert()
    {
      FirmwareDataContext context = new FirmwareDataContext(
        "Data Source=srvavs;Initial Catalog=testInsertFirmware;Persist Security Info=True;User ID=sa;Password=Sa*srvavs*09");
      if (!context.DatabaseExists())
      {
        context.CreateDatabase();
      }
      Assert.IsTrue(context.DatabaseExists());
      try
      {
        IFirmwareRepository r = new FirmwareRepository(context);

        FirmwareSource f = new FirmwareSource("test", "0", "0.1", DateTime.Now, DateTime.Now, "test.txt", true, "");
        r.Insert(f, "user", "192.168.0.21");
        Assert.AreEqual(1, r.GetFirmwarePage("RegistryDate DESC", 0, 2).Count);

        string s = r.GetSourceById(f.Id);
      }
      finally
      {
        if (context.DatabaseExists())
        {
          context.DeleteDatabase();
        }
      }
      Assert.IsFalse(context.DatabaseExists());
    }
  }
}
