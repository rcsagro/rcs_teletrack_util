﻿using System;
using NUnit.Framework;
using RCS.OnlineServer.Core.Repositories;

namespace RCS.OnlineServer.Core.Test.Repositories
{
  [TestFixture(Description = "Создание RepositoryFactory.")]
  class RepositoryFactoryInstance
  {
    [Test(Description = "Создание фабрики.")]
    public void CreateFactory()
    {
      RepositoryFactory f = new RepositoryFactory("server=");
      Assert.NotNull(f);
    }

    [Test(Description = "Если не передать в конструктор строку подключения, то получим исключение.")]
    [ExpectedException(typeof(ArgumentNullException))]
    public void ConnectionStringCantBeNullOrEmpty()
    {
      RepositoryFactory f = new RepositoryFactory("");
    }
  }
}
