﻿using System;
using System.Web.Security;

namespace RCS.Web.UI.User
{
  public partial class Error : System.Web.UI.Page
  {
    protected void Button1_Click(object sender, EventArgs e)
    {
      FormsAuthentication.SignOut();
      FormsAuthentication.RedirectToLoginPage();
    }
  }
}