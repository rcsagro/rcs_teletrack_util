﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="RCS.Web.UI.User.Error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Ошибка</title>
</head>
<body style="text-align: center">
  <form id="form1" runat="server">
    Ошибка
    <br />
    <br />
    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="У Вас нет прав для просмотра этих данных"></asp:Label>
    <br />
    <br />
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Ок" Width="68px" />
  </form>
</body>
</html>
