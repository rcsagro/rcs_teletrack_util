﻿<%@ Page Language="C#" MasterPageFile="~/us/MasterPage.master" AutoEventWireup="true"
  CodeFile="MagOfDisp.aspx.cs" Inherits="RCS.Web.UI.User.MagOfDisp" Title="Журнал диспетчера" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <br />
  <div style="width: 100%; height: 100%; text-align: center;">
    <strong>Журнал подключений</strong>
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
      AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1" EmptyDataText="Нет данных для отображения."
      Font-Names="Verdana" Font-Size="Small" Font-Strikeout="False" ForeColor="#333333"
      HorizontalAlign="Center" Width="100%" PageSize="30">
      <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <RowStyle BackColor="#EFF3FB" />
      <Columns>
        <asp:TemplateField HeaderText="№п/п">
          <HeaderStyle Width="80px" />
          <ItemTemplate>
            <%# Convert.ToString( Container.DataItemIndex + 1 ) %>
          </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="LoginCl" HeaderText="Логин" SortExpression="LoginCl" />
        <asp:BoundField DataField="CapacityOut" HeaderText="Получено" SortExpression="CapacityOut" />
        <asp:BoundField DataField="CapacityIn" HeaderText="Отправлено" SortExpression="CapacityIn" />
        <asp:BoundField DataField="TimeCon" HeaderText="Время_соедин" SortExpression="TimeCon" />
        <asp:BoundField DataField="TimeDiscon" HeaderText="Время_разъедин" SortExpression="TimeDiscon" />
        <asp:BoundField DataField="Name" HeaderText="Пользователь" SortExpression="Name" />
      </Columns>
      <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
      <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
      <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <EditRowStyle BackColor="#2461BF" />
      <AlternatingRowStyle BackColor="White" />
      <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
        NextPageText="&gt;" PreviousPageText="&lt;" />
    </asp:GridView>
    <br />
    <table style="width: 100%">
      <tr>
        <td align="right" colspan="2" style="width: 50%; height: 18px; text-align: left">
        </td>
        <td align="right" colspan="2" rowspan="1" style="width: 50%; height: 18px; text-align: right">
          <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Verdana" Font-Size="Small"
            NavigateUrl="~/us/default.aspx" Width="172px">Пользователи >></asp:HyperLink>
        </td>
      </tr>
    </table>
    <strong></strong>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OnLineServiceDBConnectionString %>"
      SelectCommand="st_SelectStatInfoClForMonitForUs" SelectCommandType="StoredProcedure">
      <SelectParameters>
        <asp:SessionParameter Name="id_dealer" SessionField="WORKING_DEALER_ID" Type="Int32" />
        <asp:QueryStringParameter Name="id_client" QueryStringField="id_client" Type="Int32" />
      </SelectParameters>
    </asp:SqlDataSource>
  </div>
</asp:Content>
