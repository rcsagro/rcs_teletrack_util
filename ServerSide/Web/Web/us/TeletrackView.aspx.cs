﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;

namespace RCS.Web.UI.User
{
  public partial class TeletrackView : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        int intdex = e.Row.RowIndex;
        if (intdex >= 0)
        {
          DataRowView rowView = (DataRowView)e.Row.DataItem;
          if (rowView != null)
          {
            string str = (string)rowView.Row.ItemArray[5];
            if (str == "CRITICAL")
            {
              e.Row.BackColor = Color.MistyRose;
            }
          }
        }
      }
    }
  }
}