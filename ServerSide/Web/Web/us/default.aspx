<%@ Page Language="C#" MasterPageFile="~/us/MasterPage.master" AutoEventWireup="true"
  CodeFile="default.aspx.cs" Inherits="RCS.Web.UI.User.Default" Title="������������" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div style="width: 100%; height: 100%; text-align: center;">
    <strong>������������</strong>
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
      AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id_user" DataSourceID="ObjectDataSource1"
      EmptyDataText="��� ������ ��� �����������." Font-Names="Verdana" Font-Size="Small"
      ForeColor="#333333" HorizontalAlign="Center" Width="100%" PageSize="30">
      <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
        NextPageText="&lt;" PreviousPageText="&gt;" />
      <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <RowStyle BackColor="#EFF3FB" />
      <Columns>
        <asp:TemplateField HeaderText="��/�">
          <ItemTemplate>
            <%# Convert.ToString( Container.DataItemIndex + 1 ) %>
          </ItemTemplate>
          <HeaderStyle Width="80px" />
        </asp:TemplateField>
        <asp:BoundField DataField="Kod_b" HeaderText="�������" SortExpression="Kod_b" />
        <asp:BoundField DataField="Name" HeaderText="������������" SortExpression="Name">
          <ItemStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="���������" SortExpression="IsActiveState">
          <EditItemTemplate>
            <asp:Label ID="Label1" runat="server" Text='<%#  Eval("IsActiveState") %>'></asp:Label>
          </EditItemTemplate>
          <ItemTemplate>
            <asp:Label ID="Label1" runat="server" Text='<%#((bool)DataBinder.Eval(Container.DataItem, "IsActiveState") == true) ? "�������" : "������������" %>'></asp:Label>
          </ItemTemplate>
        </asp:TemplateField>
        <asp:HyperLinkField DataNavigateUrlFields="ID_User" DataNavigateUrlFormatString="dispetchView.aspx?ID_User={0}"
          HeaderText="����������" NavigateUrl="~/us/DispetchView.aspx" Target="_parent" Text="����������" />
        <asp:HyperLinkField DataNavigateUrlFields="ID_User" DataNavigateUrlFormatString="TeletrackView.aspx?ID_User={0}"
          HeaderText="���������" NavigateUrl="~/us/TeletrackView.aspx" Target="_parent" Text="���������" />
        <asp:BoundField DataField="Retention_Period" HeaderText="T_��������" SortExpression="Retention_Period" />
        <asp:BoundField DataField="Describe" HeaderText="��������" SortExpression="Describe" />
      </Columns>
      <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
      <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
      <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <EditRowStyle BackColor="#2461BF" />
      <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetUserInfoForUs"
      TypeName="RCS.Web.BLL.UserInfoForUsBL">
      <SelectParameters>
        <asp:SessionParameter Name="dealerID" SessionField="WORKING_DEALER_ID" Type="Int32" />
      </SelectParameters>
    </asp:ObjectDataSource>
  </div>
</asp:Content>
