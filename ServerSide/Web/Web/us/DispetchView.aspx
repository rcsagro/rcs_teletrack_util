﻿<%@ Page Language="C#" MasterPageFile="~/us/MasterPage.master" AutoEventWireup="true"
  CodeFile="DispetchView.aspx.cs" Inherits="RCS.Web.UI.User.DispetchView" Title="Мониторинг сервисов" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div style="width: 100%; height: 100%; text-align: center;">
    <strong>Диспетчеры<br />
      (сервисы)<br />
    </strong>
    <br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
      AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID_Client" DataSourceID="SqlDataSourceDispViewForUs"
      EmptyDataText="Нет данных для отображения." Font-Names="Verdana" Font-Size="Small"
      Font-Strikeout="False" ForeColor="#333333" HorizontalAlign="Center" Width="100%"
      PageSize="30">
      <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
        NextPageText="&gt;" PreviousPageText="&lt;" />
      <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <RowStyle BackColor="#EFF3FB" />
      <Columns>
        <asp:TemplateField HeaderText="№п/п">
          <ItemTemplate>
            <%# Convert.ToString( Container.DataItemIndex + 1 ) %>
          </ItemTemplate>
          <HeaderStyle Width="80px" />
        </asp:TemplateField>
        <asp:BoundField DataField="LoginCl" HeaderText="Логин" SortExpression="LoginCl" />
        <asp:BoundField DataField="Password" HeaderText="Пароль" SortExpression="Password" />
        <asp:BoundField DataField="NetState" HeaderText="Состояние" ReadOnly="True" SortExpression="NetState" />
        <asp:BoundField DataField="DTimeDeliveredPack" HeaderText="Последние_данные" SortExpression="DTimeDeliveredPack" />
        <asp:BoundField DataField="DeltaT" HeaderText="Дельта" ReadOnly="True" SortExpression="DeltaT" />
        <asp:BoundField DataField="NewDataDescr" HeaderText="Новые_данные" ReadOnly="True"
          SortExpression="NewDataDescr" />
        <asp:BoundField DataField="Name" HeaderText="Пользователь" SortExpression="Name" />
        <asp:BoundField DataField="Describe" HeaderText="Описание" SortExpression="Describe" />
        <asp:TemplateField HeaderText="Действия">
          <ItemTemplate>
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("ID_Client", "~/us/MagOfDisp.aspx?ID_Client={0}") %>'
              Target="_parent" Text="Журнал"></asp:HyperLink>
          </ItemTemplate>
        </asp:TemplateField>
      </Columns>
      <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
      <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" HorizontalAlign="Center" />
      <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <EditRowStyle BackColor="#2461BF" HorizontalAlign="Center" />
      <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <br />
    <table style="width: 100%">
      <tr>
        <td align="right" colspan="2" style="width: 50%; height: 18px; text-align: left">
        </td>
        <td align="right" colspan="2" rowspan="1" style="width: 50%; height: 18px; text-align: right">
          <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Verdana" Font-Size="Small"
            NavigateUrl="~/us/default.aspx" Width="172px">Пользователи >></asp:HyperLink>
        </td>
      </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSourceDispViewForUs" runat="server" ConnectionString="<%$ ConnectionStrings:OnLineServiceDBConnectionString %>"
      ProviderName="<%$ ConnectionStrings:OnLineServiceDBConnectionString.ProviderName %>"
      SelectCommand="st_SelectClientInfoForUs" SelectCommandType="StoredProcedure">
      <SelectParameters>
        <asp:SessionParameter Name="id_dealer" SessionField="WORKING_DEALER_ID" Type="Int32" />
        <asp:QueryStringParameter Name="id_user" QueryStringField="id_user" Type="Int32" />
      </SelectParameters>
    </asp:SqlDataSource>
  </div>
</asp:Content>
