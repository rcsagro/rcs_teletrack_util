﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace RCS.Web.UI.User
{
  public partial class MasterPage : System.Web.UI.MasterPage
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      SetNoCache();

      if (AppSettings.RestrictAccess)
      {
        SignOut();
      }
    }

    /// <summary>
    /// Установка директив запрета кэширования страниц
    /// на стороне клиента.
    /// </summary>
    private void SetNoCache()
    {
      Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1));
      Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
      Response.Cache.SetCacheability(HttpCacheability.NoCache);
      Response.Cache.SetNoStore();
      //Response.AppendHeader("Last-Modified", "-1");
      //Response.AppendHeader("Expires", "-1"); //Mon, 11 May 1998 00:00:00 GMT
      //Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate, post-check=0, pre-check=0");
      //Response.AddHeader("Pragma", "no-cache");
    }

    private void SignOut()
    {
      FormsAuthentication.SignOut();
      Session.Abandon();
      FormsAuthentication.RedirectToLoginPage();
    }

    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    {
      if (e.Item.Text == "Выход")
      {
        SignOut();
      }
    }
  }
}