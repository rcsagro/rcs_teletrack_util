﻿using System;
using System.Web.UI.WebControls;
using RCS.Web.BLL;

namespace RCS.Web.UI
{
  public partial class Login : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (AppSettings.RestrictAccess)
      {
        ctrlLogin.Visible = false;
        pnlAccessInfo.Visible = true;
      }
    }

    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {
      string log = ctrlLogin.UserName;  // Имя дилера
      string pass = ctrlLogin.Password; // Пароль дилера
      int id; // ID дилера

      string strRoles = AuthenticationHelper.GetRolesForUser(log, pass, out id);
      if (!String.IsNullOrEmpty(strRoles))
      {
        // redirect back to originally requested resource
        if (strRoles.Contains("Admin"))
        {
          CreateAuthenticationTicket(log, strRoles, id);
          Response.Redirect("~/tech/", false);
        }
        else if (strRoles.Contains("User"))
        {
          CreateAuthenticationTicket(log, strRoles, id);
          Response.Redirect("~/us/", false);
        }
        else
        {
          lblRoleError.Visible = true;
        }
      }
      else
      {
        lblLoginError.Visible = true;
      }
    }

    /// <summary>
    /// create authentication ticket
    /// </summary>
    /// <param name="log">Логин дилера.</param>
    /// <param name="strRoles">Роли.</param>
    /// <param name="dealerId">Идентификатор дилера.</param>
    private void CreateAuthenticationTicket(string log, string strRoles, int dealerId)
    {
      // issue authentication ticket
      FormsAuthHelper.SetAuthenticationCookie(
        FormsAuthHelper.CreateAuthenticationTicket(log, strRoles));
      // Сохраняем идентификатор дилера
      Session[SessionKeyNames.WORKING_DEALER_ID] = dealerId;
    }
  }
}