﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  CodeFile="AddTeletrack.aspx.cs" Inherits="RCS.Web.UI.Tech.AddTeletrack" Title="Добавление телетрека" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table width="100%" style="width: 100%">
    <tr>
      <td style="width: 50%; height: 30px;">
      </td>
      <td align="center" style="width: 400px; height: 30px">
        <strong>Добавление нового телетрека</strong>
      </td>
      <td style="width: 50%; height: 30px;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%;">
      </td>
      <td align="left" style="width: 400px; height: 158px">
        <asp:DetailsView ID="detailsView" runat="server" AutoGenerateRows="False" CellPadding="4"
          DataSourceID="objectDataSource" DefaultMode="Insert" Font-Names="Verdana" Font-Size="Small"
          ForeColor="#333333" GridLines="None" Height="50px" OnItemInserted="DetailsView_ItemInserted"
          OnModeChanging="DetailsView_ModeChanging" Width="400px" DataKeyNames="ID_User">
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
          <RowStyle BackColor="#EFF3FB" />
          <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <Fields>
            <asp:BoundField DataField="ID_Teletrack" HeaderText="ID_Teletrack" InsertVisible="False"
              SortExpression="ID_Teletrack" />
            <asp:TemplateField HeaderText="Логин" SortExpression="LoginTT">
              <InsertItemTemplate>
                <asp:TextBox ID="txtLogin" runat="server" MaxLength="4" Text='<%# Bind("LoginTT") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="fieldValidatorLogin" runat="server" ControlToValidate="txtLogin"
                  ErrorMessage='Поле"Логин" должно быть заполнено.'>*</asp:RequiredFieldValidator>
                <asp:CustomValidator ID="validatorUniqueLogin" runat="server" ControlToValidate="txtLogin"
                  ErrorMessage="Указанный Логин уже существует." OnServerValidate="validatorUniqueLogin_ServerValidate"
                  SetFocusOnError="True">*</asp:CustomValidator>
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("LoginTT") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Пароль" SortExpression="PasswordTT">
              <InsertItemTemplate>
                <asp:TextBox ID="txtPassword" runat="server" MaxLength="12" Text='<%# Bind("PasswordTT") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="fieldValidatorPassword" runat="server" ControlToValidate="txtPassword"
                  ErrorMessage='Поле "Пароль" должно быть заполнено.'>*</asp:RequiredFieldValidator>
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("PasswordTT") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Таймаут" SortExpression="Timeout">
              <InsertItemTemplate>
                <asp:TextBox ID="txtTimeout" runat="server" Text='<%# Bind("Timeout") %>' OnInit="txtTimeout_Init"></asp:TextBox>
                <asp:RangeValidator ID="rangeValidatorTimeout" runat="server" ControlToValidate="txtTimeout"
                  ErrorMessage="Неверный формат поля Таймаут" MaximumValue="14400" MinimumValue="1"
                  Type="Integer">*</asp:RangeValidator>
                <asp:RequiredFieldValidator ID="fieldValidatorTimeout" runat="server" ControlToValidate="txtTimeout"
                  ErrorMessage='Поле "Таймаут" должно быть заполнено.'>*</asp:RequiredFieldValidator>
                (минуты. 1-14400)
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Timeout") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="GSMIMEI" SortExpression="GSMIMEI">
              <InsertItemTemplate>
                <asp:TextBox ID="txtGSMIMEI" runat="server" Text='<%# Bind("GSMIMEI") %>'></asp:TextBox>
                <asp:Label ID="Label5" runat="server" Text="(для инфотреков)"></asp:Label>
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%# Bind("GSMIMEI") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ID_User" HeaderText="ID_User" InsertVisible="False" />
            <asp:CommandField CancelText="Отменить" InsertText="Добавить" ShowInsertButton="True" />
          </Fields>
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" />
          <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
      </td>
      <td style="width: 50%;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%;">
      </td>
      <td align="left" style="width: 400px;">
        <asp:ValidationSummary ID="validationSummary" runat="server" />
      </td>
      <td style="width: 50%;">
      </td>
    </tr>
  </table>
  <div style="vertical-align: middle; text-align: center">
    <asp:Label ID="lblLicWarning" runat="server" Font-Bold="False" Font-Size="Large"
      ForeColor="Red" Text="Label" Visible="False"></asp:Label>
  </div>
  <asp:ObjectDataSource ID="objectDataSource" runat="server" InsertMethod="Insert"
    TypeName="RCS.Web.BLL.TeletrackInfoBL" DataObjectTypeName="RCS.Web.Entities.TeletrackInfo"
    OnInserting="objectDataSource_Inserting">
    <SelectParameters>
      <asp:QueryStringParameter Name="id_user" QueryStringField="id_user" Type="Int32" />
    </SelectParameters>
  </asp:ObjectDataSource>
</asp:Content>
