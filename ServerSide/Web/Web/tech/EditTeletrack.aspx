﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  CodeFile="EditTeletrack.aspx.cs" Inherits="RCS.Web.UI.Tech.EditTeletrack" Title="Редактирование записи телетрека" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" style="width: 100%">
    <tr>
      <td style="height: 30px; text-align: center;" colspan="3">
        <strong style="text-align: center">Редактирование телетрека<br />
        </strong>
      </td>
    </tr>
    <tr>
      <td colspan="3">
        <asp:DetailsView ID="detailsView" runat="server" AutoGenerateRows="False" CellPadding="4"
          DataSourceID="objectDataSource" Font-Names="Verdana" Font-Size="Small" ForeColor="#333333"
          GridLines="None" Height="50px" Width="400px" DataKeyNames="ID_Teletrack,ID_User,DTimeLastPack"
          HorizontalAlign="Center">
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
          <RowStyle BackColor="#EFF3FB" />
          <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <Fields>
            <asp:TemplateField HeaderText="Логин" SortExpression="LoginTT">
              <EditItemTemplate>
                <asp:TextBox ID="txtLogin" runat="server" Text='<%# Bind("LoginTT") %>' ReadOnly="True"></asp:TextBox>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="lblLogin" runat="server" Text='<%# Bind("LoginTT") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Пароль" SortExpression="PasswordTT">
              <EditItemTemplate>
                <asp:TextBox ID="txtPassword" runat="server" Text='<%# Bind("PasswordTT") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="fieldValidatorPassword" runat="server" ControlToValidate="txtPassword"
                  ErrorMessage='Поле "Пароль" должно быть заполнено.'>*</asp:RequiredFieldValidator>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="lblPassword" runat="server" Text='<%# Bind("PasswordTT") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Таймаут" SortExpression="Timeout">
              <EditItemTemplate>
                <asp:TextBox ID="txtTimeout" runat="server" Text='<%# Bind("Timeout") %>'></asp:TextBox>
                <asp:RangeValidator ID="rangeValidatorTimeout" runat="server" ControlToValidate="txtTimeout"
                  ErrorMessage="Неверный формат поля Таймаут" MaximumValue="14400" MinimumValue="1"
                  Type="Integer">*</asp:RangeValidator>
                <asp:RequiredFieldValidator ID="fieldValidatorTimeout" runat="server" ControlToValidate="txtTimeout"
                  ErrorMessage='Поле "Таймаут" должно быть заполнено.'>*</asp:RequiredFieldValidator>
                (минуты. 1-14400)
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="lblTimeout" runat="server" Text='<%# Bind("Timeout") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="GSMIMEI" SortExpression="GSMIMEI">
              <EditItemTemplate>
                <asp:TextBox ID="txtGSMIMEI" runat="server" Text='<%# Bind("GSMIMEI") %>'></asp:TextBox>
                (для инфотреков)
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="lblGSMIMEI" runat="server" Text='<%# Bind("GSMIMEI") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ID_Teletrack" HeaderText="ID_Teletrack" SortExpression="ID_Teletrack"
              Visible="False" />
            <asp:BoundField DataField="ID_User" HeaderText="ID_User" SortExpression="ID_User"
              Visible="False" />
            <asp:BoundField DataField="DTimeLastPack" HeaderText="DTimeLastPack" SortExpression="DTimeLastPack"
              Visible="False" />
            <asp:TemplateField ShowHeader="False">
              <EditItemTemplate>
                <table style="width: 100%; height: 100%">
                  <tr>
                    <td style="width: 50%">
                      <asp:LinkButton ID="lnkBtnSave" runat="server" CausesValidation="True" CommandName="Update"
                        Text="Сохранить"></asp:LinkButton>
                    </td>
                    <td align="right" style="width: 50%">
                      <asp:LinkButton ID="lnkBtnCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                        Text="Отменить"></asp:LinkButton>
                    </td>
                  </tr>
                </table>
              </EditItemTemplate>
              <ItemTemplate>
                <table style="width: 100%; height: 100%">
                  <tr>
                    <td style="width: 50%">
                      <asp:LinkButton ID="lnkBtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                        Text="Изменить"></asp:LinkButton>
                    </td>
                    <td align="right" style="width: 50%">
                      <asp:LinkButton ID="lnkBtnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                        Text="Удалить" OnClientClick="return confirm ('Вы действительно хотите удалить эту запись?')"></asp:LinkButton>
                    </td>
                  </tr>
                </table>
              </ItemTemplate>
              <ControlStyle BackColor="#EFF3FB" />
              <ItemStyle BackColor="#EFF3FB" />
            </asp:TemplateField>
          </Fields>
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" />
          <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
      </td>
    </tr>
    <tr>
      <td style="width: 50%;" valign="bottom">
        <asp:HyperLink ID="linkTeletracks" runat="server" Font-Names="Verdana" Font-Size="Small">Вернуться к телетрекам</asp:HyperLink>
      </td>
      <td align="left">
          <table style="width: 100%">
              <tr>
                  <td>
          <asp:Button ID="btSetAll" runat="server" OnClick="btSetAll_Click" Text="Назначить всем" Width="112px" valign="bottom" />
                  </td>
                  <td>
          <asp:Button ID="btResetAll" runat="server" Text="Открепить все" Width="112px" OnClick="btResetAll_Click" valign="bottom" />
                  </td>
              </tr>
          </table>
      </td>
      <td style="width: 50%;">
          &nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" style="height: 1px; background-color: #cccccc" valign="bottom">
      </td>
    </tr>
    <tr>
      <td colspan="3" style="height: 142px; text-align: center" valign="top">
        <strong>Назначение диспетчеров(сервисов) телетреку</strong>
        <br />
        <asp:GridView ID="gridDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
          DataKeyNames="Linked,DetailID,CommunicID" DataSourceID="detailDataSource" Font-Names="Verdana"
          Font-Size="Small" HorizontalAlign="Center" Width="400px" EmptyDataText="У пользователя нет зарегистрированных диспетчеров."
          CellPadding="4">
          <PagerSettings Mode="NumericFirstLast" />
          <FooterStyle BorderStyle="Dotted" />
          <RowStyle BackColor="#EFF3FB" />
          <Columns>
            <asp:CommandField CancelText="Отмена" EditText="Изменить" ShowEditButton="True" UpdateText="ОК">
              <ControlStyle Font-Size="Small" />
              <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" Wrap="False" />
            </asp:CommandField>
            <asp:CheckBoxField DataField="Linked" HeaderText="Назначен" SortExpression="Linked">
              <ItemStyle HorizontalAlign="Center" />
            </asp:CheckBoxField>
            <asp:BoundField DataField="DetailLogin" HeaderText="Логин диспетчера(сервиса)" ReadOnly="True"
              SortExpression="DetailLogin">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="DetailID" HeaderText="DetailID" ReadOnly="True" SortExpression="DetailID"
              Visible="False" />
            <asp:BoundField DataField="CommunicID" HeaderText="CommunicID" ReadOnly="True" SortExpression="CommunicID"
              Visible="False" />
          </Columns>
          <PagerStyle BackColor="#507CD1" ForeColor="White" HorizontalAlign="Center" />
          <SelectedRowStyle BackColor="#D1DDF1" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" HorizontalAlign="Center"
            VerticalAlign="Middle" />
          <EditRowStyle BackColor="LightCoral" />
          <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
      </td>
    </tr>
    <tr>
      <td style="width: 50%" valign="bottom">
        <asp:HyperLink ID="linkUser" runat="server" Font-Names="Verdana" Font-Size="Small">Вернуться к пользователю</asp:HyperLink>
      </td>
      <td align="left">
      </td>
      <td style="width: 50%">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="objectDataSource" runat="server" DeleteMethod="Delete"
    OldValuesParameterFormatString="original_{0}" SelectMethod="SelectByID" TypeName="RCS.Web.BLL.TeletrackInfoBL"
    UpdateMethod="Update" OnDeleted="ObjectDataSource_Deleted" DataObjectTypeName="RCS.Web.Entities.TeletrackInfo">
    <SelectParameters>
      <asp:QueryStringParameter Name="id_teletrack" QueryStringField="id_teletrack" Type="Int32" />
    </SelectParameters>
  </asp:ObjectDataSource>
  <asp:ObjectDataSource ID="detailDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="SelectClientLinkedState" TypeName="RCS.Web.BLL.UserInfoBL" UpdateMethod="UpdateLinkedClients">
    <UpdateParameters>
      <asp:QueryStringParameter Name="idTeletrack" QueryStringField="id_teletrack" Type="Int32" />
      <asp:Parameter Name="linked" Type="Boolean" />
      <asp:Parameter Name="original_DetailID" Type="Int32" />
      <asp:Parameter Name="original_CommunicID" Type="Int32" />
      <asp:Parameter Name="original_Linked" Type="Boolean" />
    </UpdateParameters>
    <SelectParameters>
      <asp:QueryStringParameter Name="idUser" QueryStringField="id_user" Type="Int32" />
      <asp:QueryStringParameter Name="idTeletrack" QueryStringField="id_teletrack" Type="Int32" />
    </SelectParameters>
  </asp:ObjectDataSource>
</asp:Content>
