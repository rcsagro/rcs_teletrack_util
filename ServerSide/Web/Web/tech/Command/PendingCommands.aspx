<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  CodeFile="PendingCommands.aspx.cs" Inherits="RCS.Web.UI.Tech.Command.PendingCommands"
  Title="��������� �������" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table id="MainTable" style="width: 100%">
    <tr>
      <td style="width: 30%; height: 31px">
      </td>
      <td style="height: 31px; text-align: center">
        <asp:Label ID="lblTitle" runat="server" Font-Size="X-Large" ForeColor="RoyalBlue"
          Height="69%" Text="��������� �������" Width="100%"></asp:Label></td>
      <td style="width: 28%; height: 31px">
      </td>
    </tr>
    <tr>
      <td colspan="3" align="center">
        <table id="tblGridView" style="width: 90%">
          <tr>
            <td align="left">
              <asp:CheckBox ID="cbCheckAll" runat="server" Text="�����" OnCheckedChanged="cbCheckAll_CheckedChanged"
                AutoPostBack="True" />
              &nbsp;
              <asp:Button ID="btnSaveTop" runat="server" OnClick="btnSave_Click" Text="��������� ���������" /></td>
            <td style="width: 5%">
            </td>
            <td style="width: 30%">
            </td>
          </tr>
          <tr>
            <td colspan="3">
              <asp:GridView ID="gridCommands" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellPadding="2" DataSourceID="objDsCommand" EmptyDataText="��� ������ ��� �����������."
                Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center" DataKeyNames="Id,Approved"
                Width="100%" OnDataBound="gridCommands_DataBound">
                <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
                  NextPageText="&lt;" PreviousPageText="&gt;" />
                <RowStyle BackColor="#EFF3FB" />
                <Columns>
                  <asp:TemplateField HeaderText="���������" SortExpression="Approved">
                    <EditItemTemplate>
                      &nbsp;
                    </EditItemTemplate>
                    <ItemTemplate>
                      <asp:CheckBox ID="cbApproved" runat="server" Checked='<%# Bind("Approved") %>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                  </asp:TemplateField>
                  <asp:BoundField DataField="CommandType" HeaderText="���" ReadOnly="True">
                    <ItemStyle HorizontalAlign="Center" />
                  </asp:BoundField>
                  <asp:BoundField DataField="DispatcherLogin" HeaderText="����� ����������" ReadOnly="True"
                    SortExpression="LoginCl">
                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                  </asp:BoundField>
                  <asp:BoundField DataField="DispatcherDescription" HeaderText="�������� ����������"
                    ReadOnly="True" />
                  <asp:BoundField DataField="DispatcherIP" HeaderText="IP ����������" ReadOnly="True"
                    SortExpression="DispatcherIP">
                    <ItemStyle Wrap="False" />
                  </asp:BoundField>
                  <asp:BoundField DataField="TeletrackLogin" HeaderText="����� ���������" ReadOnly="True"
                    SortExpression="LoginTT">
                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                  </asp:BoundField>
                  <asp:BoundField DataField="DateEntry" HeaderText="���� �����������" ReadOnly="True"
                    SortExpression="DateQueueEntry">
                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                  </asp:BoundField>
                  <asp:BoundField DataField="DateExpire" HeaderText="������ ���� ��������� ��" ReadOnly="True"
                    SortExpression="DeltaTimeExCom">
                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                  </asp:BoundField>
                  <asp:HyperLinkField DataNavigateUrlFields="Id" DataNavigateUrlFormatString="CommandDetail.aspx?ID_Command={0}"
                    HeaderText="��������" NavigateUrl="~/tech/Command/CommandDetail.aspx" Target="_parent"
                    Text="...">
                    <ItemStyle HorizontalAlign="Center" />
                  </asp:HyperLinkField>
                  <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="ID_Command"
                    Visible="False" />
                </Columns>
                <FooterStyle BackColor="#507CD1" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" />
                <HeaderStyle BackColor="#507CD1" ForeColor="White" />
                <EditRowStyle BackColor="#2461BF" Wrap="False" />
                <AlternatingRowStyle BackColor="White" />
              </asp:GridView>
            </td>
          </tr>
          <tr>
            <td align="left" style="width: 228px">
              <asp:Button ID="btnSaveBottom" runat="server" OnClick="btnSave_Click" Text="��������� ���������" /></td>
            <td>
            </td>
            <td style="width: 30%">
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="width: 30%; height: 20px">
      </td>
      <td style="height: 20px">
      </td>
      <td style="width: 28%; height: 20px">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="objDsCommand" runat="server" EnablePaging="True" OldValuesParameterFormatString="original_{0}"
    SelectCountMethod="GetPendingCommandsCount" SelectMethod="GetPendingCommands" SortParameterName="sortColumns"
    TypeName="RCS.Web.BLL.CommandBL"></asp:ObjectDataSource>
</asp:Content>
