using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using RCS.Web.BLL;
using RCS.Web.BLL.Services;

namespace RCS.Web.UI.Tech.Command
{
  public partial class PendingCommands : Page
  {
    /// <summary>
    /// ������������ ����� ����������� ���� ��� �������� Approved:
    /// OriginalApprovedValues.
    /// </summary>
    private const string SESSION_APPROVED_KEY_NAME = "OriginalApprovedValues";
    /// <summary>
    /// ������������(ID) �������� � TemplateField ������ �����:
    /// cbApproved.
    /// </summary>
    private const string CHECK_BOX_APPROVED_NAME = "cbApproved";

    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gridCommands);

      if (!IsPostBack)
      {
        CacheApprovedValues();
      }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
      CommandBL.UpdateCommandsApproved(
        GetApprovedValuesFromCache(),
        GetKeyCheckboxValueDictionary());

      ColorGrid();
      CacheApprovedValues();
    }

    /// <summary>
    /// �������� � ��� ������� �������� Approved.
    /// </summary>
    private void CacheApprovedValues()
    {
      Session[SESSION_APPROVED_KEY_NAME] = GetKeyCheckboxValueDictionary();
    }

    /// <summary>
    /// ������� �� ���� ����������� �������� Approved.
    /// </summary>
    /// <returns></returns>
    private Dictionary<int, bool> GetApprovedValuesFromCache()
    {
      return (Dictionary<int, bool>)Session[SESSION_APPROVED_KEY_NAME];
    }

    private Dictionary<int, bool> GetKeyCheckboxValueDictionary()
    {
      return GridViewHelper.GetKeyValueDictionary(
        gridCommands, "Id", CHECK_BOX_APPROVED_NAME);
    }

    protected void cbCheckAll_CheckedChanged(object sender, EventArgs e)
    {
      GridViewHelper.SetCheckBoxField(
        gridCommands, CHECK_BOX_APPROVED_NAME, cbCheckAll.Checked);
    }

    protected void gridCommands_DataBound(object sender, EventArgs e)
    {
      SetControlsEnabled();
      ColorGrid();
    }

    /// <summary>
    /// ��������� ��������� �������� �������� Enabled.
    /// </summary>
    private void SetControlsEnabled()
    {
      btnSaveTop.Enabled = btnSaveBottom.Enabled = cbCheckAll.Enabled =
        gridCommands.Rows.Count == 0 ? false : true;
    }

    /// <summary>
    /// ������������ ������ �����.
    /// </summary>
    private void ColorGrid()
    {
      GridViewHelper.ColorRowsWithCheckBox(
        gridCommands, CHECK_BOX_APPROVED_NAME, Color.LightGreen, Color.MistyRose);
    }
  }
}