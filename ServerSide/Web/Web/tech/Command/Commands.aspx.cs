using System;

namespace RCS.Web.UI.Tech.Command
{
  public partial class Commands : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gridCommands);
    }
  }
}