<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" EnableSessionState="False" CodeFile="Commands.aspx.cs"
  Inherits="RCS.Web.UI.Tech.Command.Commands" Title="������ ���� ������" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table id="MainTable" style="width: 100%">
    <tr>
      <td style="width: 30%; height: 31px">
      </td>
      <td style="height: 31px; text-align: center">
        <asp:Label ID="lblTitle" runat="server" Font-Size="X-Large" ForeColor="RoyalBlue"
          Height="69%" Text="������� ���������� ����������" Width="100%"></asp:Label></td>
      <td style="width: 30%; height: 31px">
      </td>
    </tr>
    <tr>
      <td colspan="3">
        <asp:GridView ID="gridCommands" runat="server" AllowPaging="True" AllowSorting="True"
          AutoGenerateColumns="False" CellPadding="2" DataSourceID="objDsCommand" EmptyDataText="��� ������ ��� �����������."
          Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center" EnableViewState="False">
          <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
            NextPageText="&lt;" PreviousPageText="&gt;" />
          <RowStyle BackColor="#EFF3FB" />
          <Columns>
            <asp:BoundField DataField="CommandType" HeaderText="���" ReadOnly="True">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="���������" SortExpression="Approved">
              <ItemTemplate>
                <%# (bool)Eval("Approved") == true ? "��" : "���" %>
              </ItemTemplate>
              <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="DispatcherLogin" HeaderText="����� ����������" ReadOnly="True"
              SortExpression="LoginCl">
              <ItemStyle HorizontalAlign="Center" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DispatcherDescription" HeaderText="�������� ����������"
              ReadOnly="True" />
            <asp:BoundField DataField="DispatcherIP" HeaderText="IP ����������" ReadOnly="True"
              SortExpression="DispatcherIP">
              <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="TeletrackLogin" HeaderText="����� ���������" ReadOnly="True"
              SortExpression="LoginTT">
              <ItemStyle HorizontalAlign="Center" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DateEntry" HeaderText="���� �����������" ReadOnly="True"
              SortExpression="DateQueueEntry">
              <ItemStyle HorizontalAlign="Center" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DateExpire" HeaderText="������ ���� ��������� ��" ReadOnly="True"
              SortExpression="DeltaTimeExCom">
              <ItemStyle HorizontalAlign="Center" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="HandlingStatusDescr" HeaderText="������" ReadOnly="True"
              SortExpression="ExecuteState">
              <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="TeletrackAnswerCode" HeaderText="��� ������ ���������"
              ReadOnly="True" SortExpression="AnswCode">
              <ItemStyle HorizontalAlign="Center" Wrap="False" />
            </asp:BoundField>
            <asp:HyperLinkField DataNavigateUrlFields="Id" DataNavigateUrlFormatString="CommandDetail.aspx?ID_Command={0}"
              HeaderText="��������" NavigateUrl="~/tech/Command/CommandDetail.aspx" Text="..."
              Target="_parent">
              <ItemStyle HorizontalAlign="Center" />
            </asp:HyperLinkField>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="ID_Command"
              Visible="False" />
          </Columns>
          <FooterStyle BackColor="#507CD1" ForeColor="White" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" />
          <HeaderStyle BackColor="#507CD1" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" Wrap="False" />
          <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
      </td>
    </tr>
    <tr>
      <td style="width: 30%; height: 20px">
      </td>
      <td style="height: 20px">
      </td>
      <td style="width: 30%; height: 20px">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="objDsCommand" runat="server" SelectCountMethod="GetCommandsCount"
    SelectMethod="GetCommands" SortParameterName="sortColumns" TypeName="RCS.Web.BLL.CommandBL"
    EnablePaging="True"></asp:ObjectDataSource>
</asp:Content>
