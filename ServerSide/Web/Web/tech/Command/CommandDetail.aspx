<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" EnableSessionState="False" CodeFile="CommandDetail.aspx.cs"
  Inherits="RCS.Web.UI.Tech.Command.CommandDetail" Title="����������� �������" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table id="tblMain" width="100%">
    <tr>
      <td style="width: 30%; height: 29px;">
      </td>
      <td align="center" style="height: 29px">
        <asp:Label ID="lblTitle" runat="server" Font-Size="X-Large" ForeColor="RoyalBlue"
          Text="����������� �������" EnableViewState="False"></asp:Label></td>
      <td style="width: 28%; height: 29px;">
      </td>
    </tr>
    <tr>
      <td colspan="3" align="center">
        <asp:DetailsView ID="detailCommand" runat="server" AutoGenerateRows="False" DataSourceID="objDsCommand"
          Height="50px" HorizontalAlign="Center" CellPadding="2" Font-Names="Verdana" Font-Size="Small"
          ForeColor="#333333" EnableViewState="False">
          <Fields>
            <asp:BoundField DataField="Id" HeaderText="ID" SortExpression="Id" ReadOnly="True" />
            <asp:BoundField DataField="CommandType" HeaderText="���" SortExpression="CommandType"
              ReadOnly="True" />
            <asp:BoundField DataField="HandlingStatusDescr" HeaderText="������" ReadOnly="True"
              SortExpression="HandlingStatusDescr" />
            <asp:TemplateField HeaderText="���������" SortExpression="Approved">
              <ItemTemplate>
                <%# (bool)Eval("Approved") == true ? "��" : "���" %>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DateEntry" HeaderText="���� �����������" SortExpression="DateEntry"
              ReadOnly="True" />
            <asp:BoundField DataField="DateExpire" HeaderText="������ ���� ��������� ��" SortExpression="DateExpire"
              ReadOnly="True" />
            <asp:BoundField DataField="DispatcherIP" HeaderText="IP ����������" SortExpression="DispatcherIP"
              ReadOnly="True" />
            <asp:BoundField DataField="DispatcherLogin" HeaderText="����� ����������" SortExpression="DispatcherLogin"
              ReadOnly="True" />
            <asp:BoundField DataField="DispatcherDescription" HeaderText="�������� ����������"
              SortExpression="DispatcherDescription" ReadOnly="True" />
            <asp:BoundField DataField="TeletrackLogin" HeaderText="����� ���������" SortExpression="TeletrackLogin"
              ReadOnly="True" />
            <asp:BoundField DataField="TeletrackAnswerCode" HeaderText="��� ������ ���������"
              SortExpression="TeletrackAnswerCode" ReadOnly="True" />
            <asp:TemplateField HeaderText="������� ����������" SortExpression="DispatcherCmdText">
              <EditItemTemplate>
                &nbsp;
              </EditItemTemplate>
              <InsertItemTemplate>
                &nbsp;
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:TextBox ID="txtDispatcherCmd" runat="server" EnableViewState="False" Height="80px"
                  ReadOnly="True" Text='<%# Bind("DispatcherCmdText") %>' TextMode="MultiLine" Width="600px"></asp:TextBox>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="����� ���������" SortExpression="TeletrackAnswerText">
              <EditItemTemplate>
                &nbsp;
              </EditItemTemplate>
              <InsertItemTemplate>
                &nbsp;
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:TextBox ID="txtTeletrackAnswer" runat="server" EnableViewState="False" Height="80px"
                  ReadOnly="True" Text='<%# Bind("TeletrackAnswerText") %>' TextMode="MultiLine"
                  Width="600px"></asp:TextBox>
              </ItemTemplate>
            </asp:TemplateField>
          </Fields>
          <RowStyle BackColor="#EFF3FB" Width="100%" HorizontalAlign="Left" />
          <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" Wrap="False" HorizontalAlign="Left" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" Wrap="False" />
          <AlternatingRowStyle BackColor="White" Width="100%" />
        </asp:DetailsView>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td style="width: 30%">
      </td>
      <td>
      </td>
      <td style="width: 28%">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="objDsCommand" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="GetCommandById" TypeName="RCS.Web.BLL.CommandBL" EnableViewState="False">
    <SelectParameters>
      <asp:QueryStringParameter Name="ID_Command" QueryStringField="Id_Command" Type="Int32" />
    </SelectParameters>
  </asp:ObjectDataSource>
</asp:Content>
