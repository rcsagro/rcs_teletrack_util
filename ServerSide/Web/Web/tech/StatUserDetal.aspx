<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" EnableSessionState="False" CodeFile="StatUserDetal.aspx.cs"
  Inherits="RCS.Web.UI.Tech.StatUserDetal" Title="��������� ���������� �� �������" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td style="text-align: center;" colspan="3">
        <asp:Label ID="Label1" runat="server" Font-Size="X-Large" ForeColor="RoyalBlue" Text="��������� ���������� ��  �������"></asp:Label>
      </td>
    </tr>
    <tr>
      <td style="width: 30%; text-align: left; vertical-align: top;">
        <table style="width: 100%">
          <tr>
            <td>
              <asp:RadioButtonList ID="rbMonthSelect" runat="server" AutoPostBack="True" ForeColor="RoyalBlue"
                RepeatDirection="Horizontal" Width="100%">
                <asp:ListItem Selected="True" Value="0">������� �����</asp:ListItem>
                <asp:ListItem Value="1">������� �����</asp:ListItem>
              </asp:RadioButtonList>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label4" runat="server" ForeColor="RoyalBlue" Text="������ �� �������"></asp:Label>
            </td>
          </tr>
          <tr>
            <td>
              <asp:DropDownList ID="dlClient" runat="server" DataSourceID="sdsClient" DataTextField="Name"
                DataValueField="ID_User" AppendDataBoundItems="True" EnableTheming="False" AutoPostBack="True"
                CausesValidation="True" OnSelectedIndexChanged="dlClient_SelectedIndexChanged">
                <asp:ListItem Selected="True" Value="0">���</asp:ListItem>
              </asp:DropDownList>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="lbOwner" runat="server" ForeColor="RoyalBlue" Text="�����"></asp:Label>
              &nbsp;&nbsp;
              <asp:Label ID="lbDealer" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="Black"
                Width="28px"></asp:Label>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 40%; text-align: center">
        <table style="width: 100%">
          <tr>
            <td style="vertical-align: top; width: 40%; text-align: center">
              <table style="width: 100%; height: 100%">
                <tr>
                  <td style="width: 321px; text-align: left">
                    <asp:Label ID="lbCntTltr" runat="server" ForeColor="RoyalBlue" Text="����� ����������"></asp:Label>
                  </td>
                  <td style="height: 21px; text-align: left">
                    <asp:Label ID="lbTot" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="Black"
                      Text="0" Width="28px"></asp:Label>
                  </td>
                </tr>
                <tr>
                  <td style="width: 321px; text-align: left">
                    <asp:Label ID="Label7" runat="server" ForeColor="RoyalBlue" Text="��������"></asp:Label>
                  </td>
                  <td style="text-align: left">
                    <asp:Label ID="lbAct" runat="server" Font-Bold="True" ForeColor="ForestGreen" Text="0"
                      Width="28px"></asp:Label>
                  </td>
                </tr>
                <tr>
                  <td style="width: 321px; text-align: left;">
                    <asp:Label ID="Label8" runat="server" ForeColor="RoyalBlue" Text="����������"></asp:Label>
                  </td>
                  <td style="text-align: left">
                    <asp:Label ID="lbNAct" runat="server" Font-Bold="True" ForeColor="Red" Text="0" Width="28px"></asp:Label>
                  </td>
                </tr>
                <tr>
                  <td style="width: 321px; height: 21px; text-align: left">
                    <asp:Label ID="lbSumKb�" runat="server" Text="����� ���������� ������ �� �����(��)"
                      ForeColor="RoyalBlue" Width="318px"></asp:Label>
                  </td>
                  <td style="text-align: left">
                    <asp:Label ID="lbSumKb" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="Black"
                      Text="0" Width="28px"></asp:Label>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 30%; text-align: left; vertical-align: top;">
        <table style="width: 100%; height: 100%">
          <tr>
            <td style="width: 258px; text-align: left">
              <asp:Label ID="Label2" runat="server" ForeColor="RoyalBlue" Text="�������� �������� ����������"></asp:Label>
            </td>
            <td style="text-align: left">
              <asp:TextBox ID="txCritKb" runat="server" AutoPostBack="True" OnTextChanged="txCritKb_TextChanged"
                Style="text-align: center" Width="30px">10</asp:TextBox>
              &nbsp;
              <asp:Label ID="Label3" runat="server" ForeColor="RoyalBlue" Text="��"></asp:Label>
            </td>
          </tr>
          <tr>
            <td style="width: 258px; text-align: left">
              <asp:Label ID="lbCritDay" runat="server" ForeColor="RoyalBlue" Text="�������� �������� ����������"></asp:Label>
            </td>
            <td style="text-align: left">
              <asp:TextBox ID="txCritDay" runat="server" AutoPostBack="True" OnTextChanged="txCritDay_TextChanged"
                Style="text-align: center" Width="30px">20</asp:TextBox>
              &nbsp;
              <asp:Label ID="Label5" runat="server" ForeColor="RoyalBlue" Text="����"></asp:Label>
            </td>
          </tr>
        </table>
        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txCritDay"
          ErrorMessage="�������� ������ ������ 0" Font-Names="Arial" Font-Size="12px" Operator="GreaterThan"
          ValueToCompare="0">
        </asp:CompareValidator>
        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txCritDay"
          ErrorMessage="���������� ���� � ������ �� 1 �� 31" Font-Names="Arial" Font-Size="12px"
          MaximumValue="31" MinimumValue="1" Type="Integer">
        </asp:RangeValidator>
        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txCritKb"
          ErrorMessage="�������� ������ ���� ����� ��������!" Font-Names="Arial" Font-Size="12px"
          Operator="DataTypeCheck" Type="Integer">
        </asp:CompareValidator>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txCritKb"
          ErrorMessage="�������� ������ ���� ������ 0" Font-Names="Arial" Font-Size="12px"
          Operator="GreaterThan" ValueToCompare="0">
        </asp:CompareValidator>
      </td>
    </tr>
    <tr>
      <td style="text-align: left;">
      </td>
      <td style="text-align: left; vertical-align: top;">
      </td>
      <td style="text-align: left; vertical-align: top;">
      </td>
    </tr>
    <tr>
      <td colspan="3">
        <asp:GridView ID="gvWork" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
          CellPadding="4" DataKeyNames="ID_Teletrack" DataSourceID="odsTotalMonth" EmptyDataText="��� ������ ��� �����������."
          Font-Names="Verdana" Font-Size="Small" ForeColor="#333333" HorizontalAlign="Center"
          Width="100%" OnRowDataBound="gvWork_RowDataBound" OnDataBound="gvWork_DataBound"
          OnRowCommand="gvWork_RowCommand">
          <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
            NextPageText="&lt;" PreviousPageText="&gt;" />
          <RowStyle BackColor="PaleGreen" />
          <Columns>
            <asp:BoundField DataField="ID_Teletrack" HeaderText="ID_Teletrack" Visible="False" />
            <asp:TemplateField HeaderText="��������" SortExpression="LoginTT">
              <ItemTemplate>
                <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# Eval("ID_Teletrack") %>'
                  CommandName="Move" Text='<%# Eval("LoginTT") %>'></asp:LinkButton>
              </ItemTemplate>
              <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="Sum_Kb" HeaderText="����� �������� ������, ��" SortExpression="Sum_Kb">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Cnt_Session" HeaderText="����� ����������" SortExpression="Cnt_Session">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Act" HeaderText="�������� ����" SortExpression="Act">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="LastConn" HeaderText="����� ���������� ����������" SortExpression="LastConn">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Delta" HeaderText="������" SortExpression="Delta">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="State" HeaderText="���������" SortExpression="State">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
          </Columns>
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" />
          <AlternatingRowStyle BackColor="PaleGreen" />
        </asp:GridView>
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="odsTotalMonth" runat="server" SelectMethod="GetMonthDetal"
    TypeName="RCS.Web.BLL.StatisticsBL">
    <SelectParameters>
      <asp:ControlParameter ControlID="rbMonthSelect" DefaultValue="0" Name="iSelectMonth"
        PropertyName="SelectedValue" />
      <asp:ControlParameter ControlID="txCritKb" DefaultValue="10" Name="iThrKb" PropertyName="Text" />
      <asp:ControlParameter ControlID="txCritDay" DefaultValue="0" Name="iThrDay" PropertyName="Text" />
      <asp:ControlParameter ControlID="dlClient" DefaultValue="0" Name="iClient" PropertyName="SelectedValue" />
    </SelectParameters>
  </asp:ObjectDataSource>
  <asp:SqlDataSource ID="sdsClient" runat="server" ConnectionString="<%$ ConnectionStrings:OnLineServiceDBConnectionString %>"
    ProviderName="<%$ ConnectionStrings:OnLineServiceDBConnectionString.ProviderName %>"
    SelectCommand="Select_Clients" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
</asp:Content>
