﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" EnableSessionState="False" CodeFile="MagOfTT.aspx.cs" Inherits="RCS.Web.UI.Tech.MagOfTT"
  Title="Журнал телетрека" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div style="width: 100%; height: 100%; text-align: center">
    <strong>Журнал подключений<br />
    </strong>
    <br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
      AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1" EmptyDataText="Нет данных для отображения."
      Font-Names="Verdana" Font-Size="Small" Font-Strikeout="False" ForeColor="#333333"
      HorizontalAlign="Center" Width="100%">
      <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <RowStyle BackColor="#EFF3FB" />
      <Columns>
        <asp:TemplateField HeaderText="№п/п">
          <ItemTemplate>
            <%# Convert.ToString( Container.DataItemIndex + 1 ) %>
          </ItemTemplate>
          <HeaderStyle Width="80px" />
        </asp:TemplateField>
        <asp:BoundField DataField="Логин" HeaderText="Логин" SortExpression="Логин" />
        <asp:BoundField DataField="Получено" HeaderText="Получено" SortExpression="Получено" />
        <asp:BoundField DataField="Отправлено" HeaderText="Отправлено" SortExpression="Отправлено" />
        <asp:BoundField DataField="Время_соедин" HeaderText="Время соединения" SortExpression="Время_соедин" />
        <asp:BoundField DataField="Время_разъедин" HeaderText="Время разъединения" SortExpression="Время_разъедин" />
      </Columns>
      <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
      <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
      <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <EditRowStyle BackColor="#2461BF" />
      <AlternatingRowStyle BackColor="White" />
      <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
        NextPageText="&gt;" PreviousPageText="&lt;" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OnLineServiceDBConnectionString %>"
      SelectCommand="st_SelectStatInfoTTForMonit" SelectCommandType="StoredProcedure">
      <SelectParameters>
        <asp:QueryStringParameter Name="id_teletrack" QueryStringField="id_teletrack" />
      </SelectParameters>
    </asp:SqlDataSource>
  </div>
</asp:Content>
