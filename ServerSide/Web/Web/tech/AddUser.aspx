﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  CodeFile="AddUser.aspx.cs" Inherits="RCS.Web.UI.Tech.AddUser" Title="Добавление пользователя" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td style="width: 50%; height: 30px;">
      </td>
      <td style="height: 30px; text-align: center; width: 400px;">
        <strong>Добавление нового пользователя</strong>
      </td>
      <td style="width: 50%; height: 30px;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td style="width: 400px;">
        <table style="width: 400px;">
          <tr>
            <td style="width: 100px; height: 26px">
              КодБазы
            </td>
            <td style="width: 220px; height: 26px">
              <asp:TextBox ID="codB" runat="server" Width="178px"></asp:TextBox>
              <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="codB"
                ErrorMessage="Неверный формат для поля КодБазы" Operator="DataTypeCheck" SetFocusOnError="True"
                Type="Integer">*</asp:CompareValidator>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px">
            </td>
            <td style="width: 220px">
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px">
              Название
            </td>
            <td style="width: 220px">
              <asp:TextBox ID="nameTextBox" runat="server" Width="179px"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="nameTextBox"
                ErrorMessage="Поле Login не должно быть пустым" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px">
            </td>
            <td style="width: 220px">
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px; height: 24px">
              Описание
            </td>
            <td style="width: 220px; height: 24px">
              <asp:TextBox ID="descrTextBox" runat="server" Width="180px"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="descrTextBox"
                ErrorMessage="Необходимо задать описание">*</asp:RequiredFieldValidator>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px; height: 24px">
              Состояние
            </td>
            <td style="width: 220px">
              <br />
              <asp:RadioButtonList ID="isActiveRadioBtn" runat="server" Width="185px">
                <asp:ListItem Selected="True" Value="true">Активен</asp:ListItem>
                <asp:ListItem Value="false">Заблокирован</asp:ListItem>
              </asp:RadioButtonList>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px; height: 24px">
              Т_Хранения
            </td>
            <td style="width: 220px; height: 24px">
              <asp:DropDownList ID="retPeriodDropDownList" runat="server" Width="142px">
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem Selected="True">2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem>5</asp:ListItem>
                <asp:ListItem>6</asp:ListItem>
              </asp:DropDownList>
              <asp:Label ID="Label5" runat="server" Text="(мес.)"></asp:Label>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px; height: 24px">
            </td>
            <td style="width: 220px; height: 24px">
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px; height: 24px">
              Дилер
            </td>
            <td style="width: 220px; height: 24px">
              <asp:DropDownList ID="DealerDropDownList" runat="server" DataSourceID="ObjectDataSource1"
                Width="185px">
              </asp:DropDownList>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td style="width: 400px">
        <table width="400">
          <tr>
            <td style="width: 50%; height: 26px;" align="center">
              <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Добавить" />
            </td>
            <td style="width: 50%; height: 26px;" align="center">
              <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Отменить" CausesValidation="False" />
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td style="width: 400px">
        <asp:ValidationSummary ID="ValidationSummary2" runat="server" />
      </td>
      <td style="width: 50%">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td style="width: 400px">
        &nbsp;<asp:Label ID="Label4" runat="server" ForeColor="Red" Text="Пользователь  с таким именем(Логином) уже существует!"
          Visible="False" Width="100%"></asp:Label>
      </td>
      <td style="width: 50%">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetListOfDealers"
    TypeName="RCS.Web.BLL.DealerInfoBL"></asp:ObjectDataSource>
</asp:Content>
