using System;
using System.Web.UI.WebControls;

namespace RCS.Web.UI.Tech
{
  public partial class MagOfStatTT : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gvWork);
    }

    protected void gvWork_RowCommand(object sender, GridViewCommandEventArgs e)
    {
      string str;
      if (((str = e.CommandName) != null) && (str == "Move"))
      {
        base.Response.Redirect(String.Format(
          "~/tech/MagOfTT.aspx?ID_Teletrack={0}", e.CommandArgument));
      }
    }
  }
}