﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  CodeFile="AddDispetch.aspx.cs" Inherits="RCS.Web.UI.Tech.AddDispetch" Title="Добавление сервиса" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td style="width: 50%; height: 30px;">
      </td>
      <td style="height: 30px; text-align: center">
        <strong>Добавление нового диспетчера(сервиса)</strong>
      </td>
      <td style="width: 50%; height: 30px;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td>
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" CellPadding="4"
          DataSourceID="objectDataSource" DefaultMode="Insert" Font-Names="Verdana" Font-Size="Small"
          ForeColor="#333333" GridLines="None" Height="50px" OnItemInserted="DetailsView1_ItemInserted"
          OnModeChanging="DetailsView1_ModeChanging" Width="350px" DataKeyNames="ID_Client">
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
          <RowStyle BackColor="#EFF3FB" />
          <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <Fields>
            <asp:TemplateField HeaderText="Логин">
              <InsertItemTemplate>
                <asp:TextBox ID="txtLogin" runat="server" Text='<%# Bind("LoginCl") %>'>
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLogin"
                  ErrorMessage='Поле "Login" должно быть заполнено' SetFocusOnError="True">*</asp:RequiredFieldValidator>
                <asp:CustomValidator ID="validatorUniqueLogin" runat="server" ControlToValidate="txtLogin"
                  ErrorMessage="Указанный Логин уже существует." OnServerValidate="validatorUniqueLogin_ServerValidate"
                  SetFocusOnError="True">*</asp:CustomValidator>
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("LoginCl") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Пароль" SortExpression="Password">
              <InsertItemTemplate>
                <asp:TextBox ID="txtPassword" runat="server" Text='<%# Bind("Password") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
                  ErrorMessage='Поле "Пароль" должно быть заполнено' SetFocusOnError="True">*</asp:RequiredFieldValidator>
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Password") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Описание" SortExpression="Describe">
              <InsertItemTemplate>
                <asp:TextBox ID="txtDescr" runat="server" Text='<%# Bind("Describe") %>'></asp:TextBox>
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Describe") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Пользователь" SortExpression="ID_User">
              <InsertItemTemplate>
                <asp:DropDownList ID="listUser" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name"
                  DataValueField="Name" Width="118px">
                </asp:DropDownList>
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%# Bind("ID_User") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowInsertButton="True" CancelText="Отменить" InsertText="Добавить" />
          </Fields>
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" />
          <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
      </td>
      <td style="width: 50%">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
      </td>
      <td style="width: 50%">
      </td>
    </tr>
  </table>
  <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OnLineServiceDBConnectionString %>"
    SelectCommand="SELECT Name FROM UserInfo WHERE id_user = @id_user">
    <SelectParameters>
      <asp:QueryStringParameter Name="id_user" QueryStringField="id_user" />
    </SelectParameters>
  </asp:SqlDataSource>
  <asp:ObjectDataSource ID="objectDataSource" runat="server" InsertMethod="Insert"
    TypeName="RCS.Web.BLL.ClientInfoBL" OnInserting="objectDataSource_Inserting" DataObjectTypeName="RCS.Web.Entities.ClientInfo">
  </asp:ObjectDataSource>
</asp:Content>
