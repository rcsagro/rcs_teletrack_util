﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" EnableSessionState="False" CodeFile="default.aspx.cs" Inherits="RCS.Web.UI.Tech.Default"
  Title="Пользователи" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div style="width: 100%; height: 100%; text-align: center;">
    <table style="width: 100%">
      <tr>
        <td style="width: 33%" align="left">
        </td>
        <td style="width: 34%">
          <table style="width: 100%">
            <tr>
              <td colspan="2" style="height: 29px; text-align: center">
                <asp:Label ID="Label1" runat="server" Font-Size="X-Large" ForeColor="RoyalBlue" Text="Пользователи"></asp:Label>
              </td>
            </tr>
          </table>
        </td>
        <td style="width: 33%">
        </td>
      </tr>
      <tr>
        <td style="height: 21px" align="left">
          <asp:LinkButton ID="lnGoTelet" runat="server" OnClick="lnGoTelet_Click" ForeColor="RoyalBlue">Перейти на телетрек</asp:LinkButton>
          <asp:TextBox ID="txSeek" runat="server" Style="text-align: center" Width="60px" AutoCompleteType="Disabled"></asp:TextBox>
        </td>
        <td style="height: 21px" align="left">
        </td>
        <td style="height: 21px">
        </td>
      </tr>
      <tr>
        <td colspan="3" style="height: 21px">
          <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id_user" DataSourceID="objDS"
            EmptyDataText="Нет данных для отображения." Font-Names="Verdana" Font-Size="Small"
            ForeColor="#333333" HorizontalAlign="Center" Width="100%">
            <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
              NextPageText="&lt;" PreviousPageText="&gt;" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" Font-Bold="False" />
            <Columns>
              <asp:TemplateField HeaderText="№п/п">
                <ItemTemplate>
                  <%# Convert.ToString( Container.DataItemIndex + 1 ) %>
                </ItemTemplate>
                <HeaderStyle Width="80px" />
              </asp:TemplateField>
              <asp:BoundField DataField="Kod_b" HeaderText="КодБазы" SortExpression="Kod_b" />
              <asp:BoundField DataField="Name" HeaderText="Пользователь" SortExpression="Name">
                <ItemStyle HorizontalAlign="Left" />
              </asp:BoundField>
              <asp:BoundField DataField="DealerName" HeaderText="Дилер" NullDisplayText="RCS">
                <ItemStyle HorizontalAlign="Left" />
              </asp:BoundField>
              <asp:TemplateField HeaderText="Состояние" SortExpression="IsActiveState">
                <EditItemTemplate>
                  <asp:Label ID="Label1" runat="server" Text='<%#  Eval("IsActiveState") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                  <asp:Label ID="Label1" runat="server" Text='<%#((bool)DataBinder.Eval(Container.DataItem, "IsActiveState") == true) ? "Активен" : "Заблокирован" %>'></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:HyperLinkField DataNavigateUrlFields="ID_User" DataNavigateUrlFormatString="dispetchView.aspx?ID_User={0}"
                HeaderText="Диспетчеры" NavigateUrl="~/tech/DispetchView.aspx" Target="_parent"
                Text="Диспетчеры" />
              <asp:HyperLinkField DataNavigateUrlFields="ID_User" DataNavigateUrlFormatString="TeletrackView.aspx?ID_User={0}"
                HeaderText="Телетреки" NavigateUrl="~/tech/TeletrackView.aspx" Target="_parent"
                Text="Телетреки" />
              <asp:BoundField DataField="Describe" HeaderText="Описание" SortExpression="Describe">
                <ItemStyle HorizontalAlign="Left" />
              </asp:BoundField>
            </Columns>
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>
        </td>
      </tr>
    </table>
    <asp:ObjectDataSource ID="objDS" runat="server" DeleteMethod="DeleteUserInfo" InsertMethod="InsertUserInfo"
      SelectMethod="GetUserInfo" TypeName="RCS.Web.BLL.UserInfoBL" UpdateMethod="UpdateUserInfo">
      <DeleteParameters>
        <asp:Parameter Name="id_user" Type="Int32" />
      </DeleteParameters>
      <UpdateParameters>
        <asp:Parameter Name="name" Type="String" />
        <asp:Parameter Name="descr" Type="String" />
        <asp:Parameter Name="isActive" Type="Boolean" />
        <asp:Parameter Name="retentPeriod" Type="Int32" />
        <asp:Parameter Name="kodB" Type="Int32" />
        <asp:Parameter Name="id_user" Type="Int32" />
      </UpdateParameters>
      <SelectParameters>
        <asp:QueryStringParameter DefaultValue="0" Name="id_user" QueryStringField="id_user"
          Type="Int32" />
      </SelectParameters>
      <InsertParameters>
        <asp:Parameter Name="name" Type="String" />
        <asp:Parameter Name="descr" Type="String" />
        <asp:Parameter Name="isActive" Type="Boolean" />
        <asp:Parameter Name="retentPeriod" Type="Int32" />
        <asp:Parameter Name="kodB" Type="Int32" />
        <asp:Parameter Name="id_user" Type="Int32" />
      </InsertParameters>
    </asp:ObjectDataSource>
  </div>
</asp:Content>
