﻿using System;
using System.Web.UI.WebControls;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech
{
  public partial class AddDealer : System.Web.UI.Page
  {
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
      Label1.Visible = false;
      bool isExist = DealerInfoBL.IsDealerNameExist(loginTextBox.Text, 0);

      if (!isExist)
      {
        DealerInfoBL.InsertDealerInfo(loginTextBox.Text, passTextBox.Text, descrTextBox.Text, 2, isExist); //2 - Номер роли user
        Redirect();
      }
      else
      {
        Label1.Visible = true;
      }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
      Redirect();
    }

    private void Redirect()
    {
      Response.Redirect("~/tech/DealerView.aspx");
    }
  }
}