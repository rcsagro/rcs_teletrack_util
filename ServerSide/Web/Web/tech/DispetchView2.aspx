﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" EnableSessionState="False" CodeFile="DispetchView2.aspx.cs"
  Inherits="RCS.Web.UI.Tech.DispetchView2" Title="Диспетчеры" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div style="width: 100%; height: 100%; text-align: center;">
    <strong>Диспетчеры<br />
      (сервисы)<br />
    </strong>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
      AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID_Client" DataSourceID="SqlDataSourceDispView2"
      EmptyDataText="Нет данных для отображения." Font-Names="Verdana" Font-Size="Small"
      Font-Strikeout="False" ForeColor="#333333" HorizontalAlign="Center" Width="100%">
      <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <RowStyle BackColor="#EFF3FB" />
      <Columns>
        <asp:TemplateField HeaderText="№п/п">
          <HeaderStyle Width="80px" />
          <ItemTemplate>
            <%# Convert.ToString( Container.DataItemIndex + 1 ) %>
          </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Логин" HeaderText="Логин" SortExpression="Логин" />
        <asp:BoundField DataField="Пароль" HeaderText="Пароль" SortExpression="Пароль" />
        <asp:BoundField DataField="Состояние" HeaderText="Состояние" ReadOnly="True" SortExpression="Состояние" />
        <asp:BoundField DataField="Последние_данные" HeaderText="Последние данные" SortExpression="Последние_данные" />
        <asp:BoundField DataField="Дельта" HeaderText="Дельта" ReadOnly="True" SortExpression="Дельта" />
        <asp:BoundField DataField="Новые_данные" HeaderText="Новые данные" ReadOnly="True"
          SortExpression="Новые_данные" />
        <asp:BoundField DataField="Пользователь" HeaderText="Пользователь" SortExpression="Пользователь" />
        <asp:BoundField DataField="Описание" HeaderText="Описание" SortExpression="Описание" />
        <asp:TemplateField HeaderText="Действия">
          <ItemTemplate>
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Eval("ID_Client", "~/tech/EditDispetch.aspx?ID_Client={0}") + string.Format("&ID_User={0}",Request.QueryString["ID_User"]) %>'
              Target="_parent" Text="Редактировать"></asp:HyperLink>
          </ItemTemplate>
        </asp:TemplateField>
      </Columns>
      <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
      <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" HorizontalAlign="Center" />
      <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <EditRowStyle BackColor="#2461BF" HorizontalAlign="Center" />
      <AlternatingRowStyle BackColor="White" />
      <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
        NextPageText="&gt;" PreviousPageText="&lt;" />
    </asp:GridView>
    <br />
    <table style="width: 100%">
      <tr>
        <td align="right" colspan="2" style="width: 50%; height: 18px; text-align: left">
          <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/tech/AddDispetch.aspx"
            Font-Names="Verdana" Font-Size="Small">Добавить сервис</asp:HyperLink>
        </td>
        <td align="right" colspan="2" rowspan="1" style="width: 50%; height: 18px; text-align: right">
          <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Verdana" Font-Size="Small"
            NavigateUrl="~/tech/UserView.aspx" Width="172px">Пользователь >></asp:HyperLink>
        </td>
      </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSourceDispView2" runat="server" ConnectionString="<%$ ConnectionStrings:OnLineServiceDBConnectionString %>"
      ProviderName="<%$ ConnectionStrings:OnLineServiceDBConnectionString.ProviderName %>"
      SelectCommand="st_SelectClientInfo" SelectCommandType="StoredProcedure">
      <SelectParameters>
        <asp:QueryStringParameter Name="id_user" QueryStringField="id_user" />
      </SelectParameters>
    </asp:SqlDataSource>
  </div>
</asp:Content>
