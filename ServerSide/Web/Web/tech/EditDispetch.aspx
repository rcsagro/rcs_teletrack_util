﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  CodeFile="EditDispetch.aspx.cs" Inherits="RCS.Web.UI.Tech.EditDispetch" Title="Редактирование записи диспетчера(сервиса)" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
  Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 100%">
    <tr>
      <td colspan="3" style="height: 30px; text-align: center">
        <strong style="vertical-align: middle; text-align: center">Редактирование диспетчера<br />
          (сервиса)</strong>

      </td>
    </tr>
    <tr>
      <td style="height: 140px;" colspan="3">
        <asp:DetailsView ID="detailsView" runat="server" AutoGenerateRows="False" DataSourceID="objectDataSource"
          Height="50px" Width="400px" CellPadding="4" ForeColor="#333333" GridLines="None"
          Font-Names="Verdana" Font-Size="Small" DataKeyNames="ID_Client,ID_Packet,ID_LastPackInSrvDB,DTimeDeliveredPack,InNetwork"
          HorizontalAlign="Center">
          <Fields>
            <asp:TemplateField HeaderText="Логин" SortExpression="LoginCl">
              <EditItemTemplate>
                <asp:TextBox ID="txtLogin" runat="server" Text='<%# Bind("LoginCl") %>' ReadOnly="True"></asp:TextBox>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="lblLogin" runat="server" Text='<%# Bind("LoginCl") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Пароль" SortExpression="Password">
              <EditItemTemplate>
                <asp:TextBox ID="txtPassword" runat="server" Text='<%# Bind("Password") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="fieldValidatorPassword" runat="server" ControlToValidate="txtPassword"
                  ErrorMessage='Поле "Пароль" должно быть заполнено.'>*</asp:RequiredFieldValidator>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="lblPassword" runat="server" Text='<%# Bind("Password") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Пользователь" SortExpression="ID_User">
              <EditItemTemplate>
                <asp:TextBox ID="txtUserID" runat="server" Text='<%# Bind("ID_User") %>' ReadOnly="True"></asp:TextBox>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="lblUserID" runat="server" Text='<%# Bind("ID_User") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Описание" SortExpression="Describe">
              <EditItemTemplate>
                <asp:TextBox ID="txtDescr" runat="server" Text='<%# Bind("Describe") %>'></asp:TextBox>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="lblDescr" runat="server" Text='<%# Bind("Describe") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ID_Client" HeaderText="ID_Client" ReadOnly="True" SortExpression="ID_Client"
              Visible="False" />
            <asp:BoundField DataField="InNetwork" HeaderText="InNetwork" ReadOnly="True" SortExpression="InNetwork"
              Visible="False" />
            <asp:BoundField DataField="ID_Packet" HeaderText="ID_Packet" ReadOnly="True" SortExpression="ID_Packet"
              Visible="False" />
            <asp:BoundField DataField="DTimeDeliveredPack" HeaderText="DTimeDeliveredPack" ReadOnly="True"
              SortExpression="DTimeDeliveredPack" Visible="False" />
            <asp:BoundField DataField="ID_LastPackInSrvDB" HeaderText="ID_LastPackInSrvDB" ReadOnly="True"
              SortExpression="ID_LastPackInSrvDB" Visible="False" />
            <asp:TemplateField ShowHeader="False" ControlStyle-BackColor="#EFF3FB" ItemStyle-BackColor="#EFF3FB">
              <EditItemTemplate>
                <table style="width: 100%; height: 100%">
                  <tr>
                    <td style="width: 50%">
                      <asp:LinkButton ID="lnkBtnSave" runat="server" CausesValidation="True" CommandName="Update"
                        Text="Сохранить"></asp:LinkButton>
                    </td>
                    <td align="right" style="width: 50%">
                      <asp:LinkButton ID="lnkBtnCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                        Text="Отменить"></asp:LinkButton>
                    </td>
                  </tr>
                </table>
              </EditItemTemplate>
              <ItemTemplate>
                <table style="width: 100%; height: 100%">
                  <tr>
                    <td style="width: 50%">
                      <asp:LinkButton ID="lnkBtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                        Text="Изменить"></asp:LinkButton>
                    </td>
                    <td align="right" style="width: 50%">
                      <asp:LinkButton ID="lnkBtnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                        Text="Удалить" OnClientClick="return confirm ('Вы действительно хотите удалить эту запись?')"></asp:LinkButton>
                    </td>
                  </tr>
                </table>
              </ItemTemplate>
            </asp:TemplateField>
          </Fields>
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
          <RowStyle BackColor="#EFF3FB" />
          <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" />
          <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
      </td>
    </tr>
    <tr>
      <td style="width: 50%;" valign="bottom">
        <asp:HyperLink ID="linkDispatchers" runat="server" Font-Names="Verdana" Font-Size="Small">Вернуться к диспетчерам</asp:HyperLink>
      </td>
      <td align="left">
          <table style="width: 100%">
              <tr>
                  <td>
          <asp:Button ID="btSetAll" runat="server" OnClick="btSetAll_Click" Text="Назначить всем" Width="112px" valign="bottom" />
                  </td>
                  <td>
          <asp:Button ID="btResetAll" runat="server" Text="Открепить все" Width="112px" OnClick="btResetAll_Click" valign="bottom" />
                  </td>
              </tr>
          </table>
      </td>
      <td style="width: 50%;">
      </td>
    </tr>
    <tr>
      <td colspan="3" style="height: 1px; background-color: #cccccc" valign="bottom">
      </td>
    </tr>
    <tr>
      <td colspan="3" style="height: 65px; text-align: center;" valign="top">
        <strong>Назначение телетреков диспетчеруstrong>
        <asp:GridView ID="gridDetails" runat="server" AutoGenerateColumns="False" DataKeyNames="Linked,DetailID,CommunicID"
          DataSourceID="detailDataSource" Width="400px" AllowPaging="True" Font-Names="Verdana"
          Font-Size="Small" HorizontalAlign="Center" EmptyDataText="У пользователя нет зарегистрированных телетреков."
          CellPadding="4">
          <Columns>
            <asp:CommandField ShowEditButton="True" CancelText="Отмена" EditText="Изменить" UpdateText="ОК">
              <ControlStyle Font-Size="Small" />
              <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" Width="150px" />
            </asp:CommandField>
            <asp:CheckBoxField DataField="Linked" HeaderText="Назначен" SortExpression="Linked">
              <ItemStyle HorizontalAlign="Center" />
            </asp:CheckBoxField>
            <asp:BoundField DataField="DetailLogin" HeaderText="Логин телетрека" ReadOnly="True"
              SortExpression="DetailLogin">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="DetailID" HeaderText="DetailID" ReadOnly="True" SortExpression="DetailID"
              Visible="False" />
            <asp:BoundField DataField="CommunicID" HeaderText="CommunicID" ReadOnly="True" SortExpression="CommunicID"
              Visible="False" />
          </Columns>
          <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#507CD1"
            ForeColor="White" />
          <FooterStyle BorderStyle="Dotted" />
          <PagerStyle HorizontalAlign="Center" BackColor="#507CD1" ForeColor="White" />
          <EditRowStyle BackColor="LightCoral" />
          <SelectedRowStyle BackColor="#D1DDF1" />
          <PagerSettings Mode="NumericFirstLast" />
          <RowStyle BackColor="#EFF3FB" />
          <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
      </td>
    </tr>
    <tr>
      <td style="width: 50%; height: 4px" valign="bottom">
        <asp:HyperLink ID="linkUser" runat="server" Font-Names="Verdana" Font-Size="Small">Вернуться к пользователю</asp:HyperLink>
      </td>
      <td style="width: 83px; height: 4px">
      </td>
      <td style="width: 50%; height: 4px">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="objectDataSource" runat="server" DeleteMethod="Delete"
    OldValuesParameterFormatString="original_{0}" SelectMethod="SelectByID" TypeName="RCS.Web.BLL.ClientInfoBL"
    UpdateMethod="Update" DataObjectTypeName="RCS.Web.Entities.ClientInfo" OnDeleted="objectDataSource_Deleted">
    <SelectParameters>
      <asp:QueryStringParameter DefaultValue="0" Name="id_client" QueryStringField="id_client"
        Type="Int32" />
    </SelectParameters>
  </asp:ObjectDataSource>
  <asp:ObjectDataSource ID="detailDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    TypeName="RCS.Web.BLL.UserInfoBL" SelectMethod="SelectTeletrackLinkedState" SelectCountMethod="SelectTeletrackCount"
    UpdateMethod="UpdateLinkedTeletracks" EnablePaging="True">
    <UpdateParameters>
      <asp:QueryStringParameter Name="idClient" QueryStringField="id_client" Type="Int32" />
      <asp:Parameter Name="linked" Type="Boolean" />
      <asp:Parameter Name="original_DetailID" Type="Int32" />
      <asp:Parameter Name="original_CommunicID" Type="Int32" />
      <asp:Parameter Name="original_Linked" Type="Boolean" />
    </UpdateParameters>
    <SelectParameters>
      <asp:QueryStringParameter Name="idUser" QueryStringField="id_user" Type="Int32" />
      <asp:QueryStringParameter Name="idClient" QueryStringField="id_client" Type="Int32" />
    </SelectParameters>
  </asp:ObjectDataSource>
</asp:Content>
