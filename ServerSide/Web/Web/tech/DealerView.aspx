<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" CodeFile="DealerView.aspx.cs" Inherits="RCS.Web.UI.Tech.DealerView"
  Title="������" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div style="width: 100%; height: 100%; text-align: center;">
    <strong>������<br />
      <br />
    </strong>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
      AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" DataSourceID="SqlDataSource1"
      EmptyDataText="��� ������ ��� �����������." Font-Names="Verdana" Font-Size="Small"
      ForeColor="#333333" HorizontalAlign="Center" Width="100%">
      <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
        NextPageText="&lt;" PreviousPageText="&gt;" />
      <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <RowStyle BackColor="#EFF3FB" />
      <Columns>
        <asp:TemplateField HeaderText="��/�">
          <ItemTemplate>
            <%# Convert.ToString( Container.DataItemIndex + 1 ) %>
          </ItemTemplate>
          <HeaderStyle Width="80px" />
        </asp:TemplateField>
        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
          SortExpression="ID" Visible="False" />
        <asp:BoundField DataField="DealerName" HeaderText="�����" SortExpression="DealerName" />
        <asp:BoundField DataField="Password" HeaderText="������" SortExpression="Password" />
        <asp:BoundField DataField="Descr" HeaderText="��������" SortExpression="Descr" />
        <asp:TemplateField>
          <ItemTemplate>
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Eval("ID", "~/tech/DealerEdit.aspx?ID={0}") %>'
              Target="_parent" Text="�������������."></asp:HyperLink>
          </ItemTemplate>
          <HeaderTemplate>
            ��������
          </HeaderTemplate>
        </asp:TemplateField>
      </Columns>
      <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
      <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
      <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <EditRowStyle BackColor="#2461BF" />
      <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <table style="width: 100%">
      <tr>
        <td align="right" colspan="2" style="width: 50%; height: 18px; text-align: left">
        </td>
        <td align="right" colspan="2" rowspan="1" style="width: 50%; height: 18px; text-align: right">
          <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Verdana" Font-Size="Small"
            NavigateUrl="~/tech/AddDealer.aspx?ID_User=0" Width="172px">�������� ������</asp:HyperLink>
        </td>
      </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OnLineServiceDBConnectionString %>"
      SelectCommand="SELECT [ID], [DealerName], [Password], [Descr] FROM [DealerInfo]">
    </asp:SqlDataSource>
  </div>
</asp:Content>
