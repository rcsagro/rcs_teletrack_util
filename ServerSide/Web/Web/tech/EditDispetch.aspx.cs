﻿using System;
using System.Web.UI.WebControls;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech
{
  public partial class EditDispetch : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gridDetails);
      MaintainScrollPositionOnPostBack = true;
      linkDispatchers.NavigateUrl = GetDispetchViewUrl();
      linkUser.NavigateUrl = GetUserViewUrl();
    }

    private string GetDispetchViewUrl()
    {
      return string.Format(
        "~/tech/DispetchView2.aspx?ID_User={0}", Request["id_user"]);
    }

    private string GetUserViewUrl()
    {
      return string.Format(
        "~/tech/UserView.aspx?ID_User={0}", Request["id_user"]);
    }

    protected void objectDataSource_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
      if (e.Exception == null)
      {
        Response.Redirect(GetDispetchViewUrl());
      }
    }
    protected void btSetAll_Click(object sender, EventArgs e)
    {
        UserInfoBL.SetAllTeletracksLinkedTopDicpatch(Convert.ToInt32(Request["id_user"]), Convert.ToInt32(Request["id_client"]));
        gridDetails.DataBind();
    }
    protected void btResetAll_Click(object sender, EventArgs e)
    {
        UserInfoBL.ResetAllTeletracksLinkedToDicpatch(Convert.ToInt32(Request["id_user"]), Convert.ToInt32(Request["id_client"]));
        gridDetails.DataBind();
    }
}
}