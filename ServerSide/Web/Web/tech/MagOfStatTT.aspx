<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" EnableSessionState="False" CodeFile="MagOfStatTT.aspx.cs"
  Inherits="RCS.Web.UI.Tech.MagOfStatTT" Title="������ ������ ����������" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td style="text-align: center; height: 29px;" colspan="3">
        <asp:Label ID="Label1" runat="server" Font-Size="X-Large" ForeColor="RoyalBlue" Text="������ ������ ����������"></asp:Label>
      </td>
    </tr>
    <tr>
      <td colspan="3" style="height: 29px; text-align: left">
        <asp:LinkButton ID="LinkButton2" runat="server" Font-Names="Arial" Font-Size="12px"
          ForeColor="RoyalBlue" PostBackUrl="~/tech/MagOfWork.aspx">��������� �������� ������</asp:LinkButton>
      </td>
    </tr>
    <tr>
      <td colspan="3">
        <asp:GridView ID="gvWork" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
          CellPadding="4" DataKeyNames="ID_Teletrack" DataSourceID="odsWork" EmptyDataText="��� ������ ��� �����������."
          Font-Names="Verdana" Font-Size="Small" ForeColor="#333333" HorizontalAlign="Center"
          Width="100%" OnRowCommand="gvWork_RowCommand">
          <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
            NextPageText="&lt;" PreviousPageText="&gt;" />
          <RowStyle BackColor="#EFF3FB" />
          <Columns>
            <asp:BoundField DataField="ID_Teletrack" HeaderText="ID_Teletrack" SortExpression="ID_Teletrack"
              Visible="False" />
            <asp:BoundField DataField="ID_Session" HeaderText="������" SortExpression="ID_Session">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="��������" SortExpression="LoginTT">
              <ItemTemplate>
                <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# Eval("ID_Teletrack") %>'
                  CommandName="Move" Text='<%# Eval("LoginTT") %>'></asp:LinkButton>
              </ItemTemplate>
              <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="Name" HeaderText="������" SortExpression="Name">
              <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="CapacityIn" HeaderText="��������" SortExpression="CapacityIn">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="CapacityOut" HeaderText="����������" SortExpression="CapacityOut">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="TimeCon" HeaderText="������ ������" SortExpression="TimeCon">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="TimeDiscon" HeaderText="����� ������" SortExpression="TimeDiscon">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
          </Columns>
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" />
          <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="odsWork" runat="server" SelectMethod="GetLastStaticTT"
    TypeName="RCS.Web.BLL.StatisticsBL"></asp:ObjectDataSource>
</asp:Content>
