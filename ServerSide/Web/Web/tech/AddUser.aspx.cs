﻿using System;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech
{
  public partial class AddUser : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        DealerDropDownList.Text = "нет";
      }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
      int id_user;
      string dealName = DealerDropDownList.Text;
      if (dealName == "нет")
      {
        dealName = "";
      }

      int res = UserInfoBL.InsertUserInfo(
        int.Parse(codB.Text),
        nameTextBox.Text,
        descrTextBox.Text,
        bool.Parse(isActiveRadioBtn.SelectedItem.Value),
        int.Parse(retPeriodDropDownList.Text),
        dealName,
        out id_user);

      if (res > 0)
      {
        Response.Redirect(String.Format("~/tech/UserView.aspx?ID_User={0}", 0));
      }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
      Response.Redirect("~/tech/UserView.aspx");
    }
  }
}