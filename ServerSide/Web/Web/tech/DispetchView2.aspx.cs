﻿using System;

namespace RCS.Web.UI.Tech
{
  public partial class DispetchView2 : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        HyperLink1.NavigateUrl += string.Format("?ID_User={0}", Request["id_user"]);
        HyperLink2.NavigateUrl += string.Format("?ID_User={0}", Request["id_user"]);
      }
      ((MasterPage)Page.Master).SetGridViewPageSize(GridView1);
    }
  }
}