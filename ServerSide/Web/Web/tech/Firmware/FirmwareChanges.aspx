﻿<%@ Page Title="Журнал изменений описания прошивки" Language="C#" MasterPageFile="~/tech/MasterPage.master"
  AutoEventWireup="true" EnableViewState="false" EnableSessionState="False" CodeFile="FirmwareChanges.aspx.cs"
  Inherits="RCS.Web.UI.Tech.Firmware.FirmwareChanges" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td colspan="3" style="height: 30px; text-align: center">
        <strong style="vertical-align: middle; text-align: center; font-size: x-large; color: RoyalBlue;">
          Описание изменения прошивки</strong>
      </td>
    </tr>
    <tr>
      <td style="height: 140px;" colspan="3">
        <asp:DetailsView ID="detailsView" runat="server" AutoGenerateRows="False" DataSourceID="objectDataSource"
          Height="50px" Width="400px" CellPadding="4" ForeColor="#333333" GridLines="None"
          Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center" DataKeyNames="Id,VersionFrom,FileName,VersionStamp,RegistryDate">
          <Fields>
            <asp:TemplateField HeaderText="Версия прошивки" SortExpression="VersionTo">
              <ItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%# Bind("VersionTo") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Дата выпуска" SortExpression="IssueDateStr">
              <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("IssueDateStr") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Описание" SortExpression="Description">
              <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Доступность" SortExpression="Enabled">
              <ItemTemplate>
                <asp:CheckBox ID="CheckBox3" runat="server" Checked='<%# Bind("Enabled") %>' Enabled="False" />
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Версия телетрека" SortExpression="VersionFrom">
              <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%# Bind("VersionFrom") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Дата регистрации" SortExpression="RegistryDate">
              <ItemTemplate>
                <asp:Label ID="Label5" runat="server" Text='<%# Bind("RegistryDate") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Имя файла" SortExpression="FileName">
              <InsertItemTemplate>
                &nbsp;&nbsp;
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label6" runat="server" Text='<%# Bind("FileName") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
          </Fields>
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
          <RowStyle BackColor="#EFF3FB" />
          <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" />
          <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
      </td>
    </tr>
    <tr>
      <td style="width: 50%; height: 23px;" valign="bottom">
        &nbsp;
        <asp:HyperLink ID="lnkBack" runat="server" Font-Size="Small">Вернуться</asp:HyperLink>
      </td>
      <td align="left" style="height: 23px">
        &nbsp;
      </td>
      <td style="width: 50%; height: 23px;">
      </td>
    </tr>
    <tr>
      <td colspan="3" style="height: 1px; background-color: #cccccc" valign="bottom">
      </td>
    </tr>
    <tr>
      <td colspan="3" style="height: 23px; text-align: center;" valign="top">
        <strong style="font-size: large; color: RoyalBlue">Лог изменений описания прошивки</strong>
      </td>
    </tr>
    <tr>
      <td colspan="3" style="height: 65px; text-align: center;" valign="top">
        <asp:GridView ID="gridDetails" runat="server" AutoGenerateColumns="False" DataSourceID="detailDataSource"
          AllowPaging="True" Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center"
          EmptyDataText="Нет сформированных задач." CellPadding="4" AllowSorting="True">
          <Columns>
            <asp:BoundField DataField="Date" HeaderText="Дата" ReadOnly="True" SortExpression="Date" />
            <asp:BoundField DataField="Description" HeaderText="Описание" ReadOnly="True">
              <ItemStyle Width="250px" />
            </asp:BoundField>
            <asp:BoundField DataField="UserName" HeaderText="UserName" ReadOnly="True" SortExpression="UserName">
              <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:BoundField DataField="UserIp" HeaderText="UserIp" ReadOnly="True" SortExpression="UserIp" />
          </Columns>
          <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#507CD1"
            ForeColor="White" />
          <FooterStyle BorderStyle="Dotted" />
          <PagerStyle HorizontalAlign="Center" BackColor="#507CD1" ForeColor="White" />
          <EditRowStyle BackColor="LightCoral" />
          <SelectedRowStyle BackColor="#D1DDF1" />
          <PagerSettings Mode="NumericFirstLast" />
          <RowStyle BackColor="#EFF3FB" />
          <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
      </td>
    </tr>
    <tr>
      <td style="width: 50%; height: 4px" valign="bottom">
        &nbsp;
      </td>
      <td style="width: 83px; height: 4px">
      </td>
      <td style="width: 50%; height: 4px">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="objectDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="GetFirmwareById" TypeName="RCS.Web.BLL.FirmwareBL" DataObjectTypeName="RCS.OnlineServer.Core.Model.Firmware">
    <SelectParameters>
      <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" DbType="Int64" />
    </SelectParameters>
  </asp:ObjectDataSource>
  <asp:ObjectDataSource ID="detailDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    TypeName="RCS.Web.BLL.FirmwareBL" SelectMethod="GetChangesById" SelectCountMethod="GetChangesCountById"
    EnablePaging="True" DataObjectTypeName="RCS.OnlineServer.Core.Model.FirmwareChanges"
    SortParameterName="sortColumns">
    <SelectParameters>
      <asp:QueryStringParameter Name="firmwareId" QueryStringField="id" DbType="Int64" />
    </SelectParameters>
  </asp:ObjectDataSource>
</asp:Content>
