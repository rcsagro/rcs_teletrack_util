﻿<%@ Page Title="Список измений прошивок" Language="C#" MasterPageFile="~/tech/MasterPage.master"
  AutoEventWireup="true" EnableViewState="false" EnableSessionState="False" CodeFile="Firmwares.aspx.cs"
  Inherits="RCS.Web.UI.Tech.Firmware.Firmwares" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table id="MainTable" style="width: 100%">
    <tr>
      <td style="width: 30%; height: 31px" align="right">
        &nbsp;
      </td>
      <td style="height: 31px; text-align: center">
        <asp:Label ID="lblTitle" runat="server" Font-Size="X-Large" ForeColor="RoyalBlue"
          Height="69%" Text="Изменения прошивок" Width="100%"></asp:Label>
      </td>
      <td style="width: 30%; height: 31px">
      </td>
    </tr>
    <tr>
      <td style="width: 30%; height: 31px" align="right">
        <asp:Button ID="btnNew" runat="server" PostBackUrl="~/tech/Firmware/AddFirmware.aspx"
          Text="Создать" ToolTip="Создать описание изменения прошивки" Width="150px" />
      </td>
      <td style="height: 31px; text-align: center">
      </td>
      <td style="width: 30%; height: 31px">
      </td>
    </tr>
    <tr>
      <td colspan="3">
        <asp:GridView ID="gridFirmwares" runat="server" AllowPaging="True" AllowSorting="True"
          AutoGenerateColumns="False" CellPadding="2" DataSourceID="objDsFirmware" EmptyDataText="Нет данных для отображения."
          Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center" EnableViewState="False"
          DataKeyNames="Id">
          <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
            NextPageText="&lt;" PreviousPageText="&gt;" />
          <RowStyle BackColor="#EFF3FB" />
          <Columns>
            <asp:CheckBoxField DataField="Enabled" HeaderText="Разрешена" ReadOnly="True" SortExpression="Enabled" />
            <asp:BoundField DataField="VersionFrom" HeaderText="Версия телетрека" ReadOnly="True"
              SortExpression="VersionFrom">
              <HeaderStyle Width="10px" />
            </asp:BoundField>
            <asp:BoundField DataField="VersionTo" HeaderText="Версия прошивки" ReadOnly="True"
              SortExpression="VersionTo">
              <HeaderStyle Width="10px" />
            </asp:BoundField>
            <asp:BoundField DataField="IssueDate" HeaderText="Дата выпуска" ReadOnly="True" SortExpression="IssueDate"
              DataFormatString="{0:dd.MM.yyyy}"></asp:BoundField>
            <asp:BoundField DataField="RegistryDate" HeaderText="Дата регистрации" ReadOnly="True"
              SortExpression="RegistryDate" />
            <asp:BoundField DataField="Description" HeaderText="Описание" ReadOnly="True">
              <HeaderStyle Width="25px" />
              <ItemStyle Width="250px" />
            </asp:BoundField>
            <asp:BoundField DataField="FileName" HeaderText="Наименование файла" ReadOnly="True"
              SortExpression="FileName">
              <HeaderStyle Width="20px" />
              <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Детально">
              <ItemTemplate>
                <asp:HyperLink ID="DetailLink" runat="server" Text="..." NavigateUrl='<%# Eval("Id","~/tech/Firmware/EditFirmware.aspx?id={0}") %>'></asp:HyperLink>
              </ItemTemplate>
              <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id"
              Visible="False"></asp:BoundField>
          </Columns>
          <FooterStyle BackColor="#507CD1" ForeColor="White" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" />
          <HeaderStyle BackColor="#507CD1" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" Wrap="False" />
          <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
      </td>
    </tr>
    <tr>
      <td style="width: 30%; height: 20px">
      </td>
      <td style="height: 20px">
      </td>
      <td style="width: 30%; height: 20px">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="objDsFirmware" runat="server" SelectCountMethod="GetFirmwaresCount"
    SelectMethod="GetFirmwares" SortParameterName="sortColumns" TypeName="RCS.Web.BLL.FirmwareBL"
    EnablePaging="True"></asp:ObjectDataSource>
</asp:Content>
