﻿<%@ Page Title="Новая информация о прошивке" Language="C#" MasterPageFile="~/tech/MasterPage.master"
  AutoEventWireup="true" CodeFile="AddFirmware.aspx.cs" Inherits="RCS.Web.UI.Tech.Firmware.AddFirmware" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td colspan="3" style="height: 30px; text-align: center">
        <strong style="vertical-align: middle; text-align: center; font-size: x-large; color: RoyalBlue;">
          Новое описание изменения прошивки</strong>
      </td>
    </tr>
    <tr>
      <td style="height: 140px;" colspan="3">
        <asp:DetailsView ID="detailsView" runat="server" AutoGenerateRows="False" DataSourceID="objectDataSource"
          Height="50px" Width="400px" CellPadding="4" ForeColor="#333333" GridLines="None"
          Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center" OnItemInserting="detailsView_ItemInserting"
          OnModeChanging="detailsView_ModeChanging" DefaultMode="Insert" OnItemInserted="detailsView_ItemInserted">
          <Fields>
            <asp:TemplateField HeaderText="Версия обновления">
              <InsertItemTemplate>
                <asp:TextBox ID="txtVerTo" runat="server" Text='<%# Bind("VersionTo") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtVerTo"
                  ErrorMessage="Обязательное"></asp:RequiredFieldValidator>
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%# Bind("VersionTo") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Дата выпуска" SortExpression="IssueDateStr">
              <InsertItemTemplate>
                <asp:TextBox ID="txtIssueDate" runat="server" Text='<%# Bind("IssueDateStr") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtIssueDate"
                  ErrorMessage="Обязательное" SetFocusOnError="True">Обязательное</asp:RequiredFieldValidator>
                <asp:Label ID="lblDateError" runat="server" ForeColor="Red" Text="Неправильная дата"
                  Visible="False"></asp:Label>
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("IssueDateStr") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Имя файла" SortExpression="FileName">
              <ItemTemplate>
                <asp:Label ID="Label6" runat="server" Text='<%# Bind("FileName") %>'></asp:Label>
              </ItemTemplate>
              <InsertItemTemplate>
                <asp:FileUpload ID="fileUpload" runat="server" />
                <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Text="Загрузить" />
                &nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblUpload" runat="server" Font-Bold="True"></asp:Label>
                <asp:Label ID="lblFileError" runat="server" ForeColor="#FF3300" Text="Укажите  и загрузите файл"
                  Visible="False"></asp:Label>
              </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Описание" SortExpression="Description">
              <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
              </ItemTemplate>
              <InsertItemTemplate>
                <asp:TextBox ID="txtDescr0" runat="server" Height="50px" MaxLength="1000" Text='<%# Bind("Description") %>'
                  TextMode="MultiLine" Width="400px"></asp:TextBox>
              </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Доступность" SortExpression="Enabled">
              <InsertItemTemplate>
                <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("Enabled") %>' />
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:CheckBox ID="CheckBox3" runat="server" Checked='<%# Bind("Enabled") %>' />
              </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowInsertButton="True" />
          </Fields>
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
          <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <InsertRowStyle BackColor="#99CCFF" />
          <EditRowStyle BackColor="#2461BF" />
          <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
      </td>
    </tr>
    <tr>
      <td style="width: 50%;" valign="bottom">
        &nbsp;
      </td>
      <td align="left">
        &nbsp;
      </td>
      <td style="width: 50%;">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="objectDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    TypeName="RCS.Web.BLL.FirmwareBL" InsertMethod="Insert" DataObjectTypeName="RCS.OnlineServer.Core.Model.FirmwareSource"
    OnInserted="objectDataSource_Inserted" OnInserting="objectDataSource_Inserting">
  </asp:ObjectDataSource>
</asp:Content>
