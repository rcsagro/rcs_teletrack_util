﻿using System;
using System.Web.UI.WebControls;
using RCS.OnlineServer.Core.Model;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech.Firmware
{
  public partial class AddFirmwareTask : System.Web.UI.Page
  {
    static string prevPage = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        prevPage = Request.UrlReferrer.ToString();
      }
    }

    protected void detailsView_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
      TextBox txt = (TextBox)detailsView.FindControl("txtLogin");

      if (!TeletrackInfoBL.SelectTeletrackIDByLogin(txt.Text).HasValue)
      {
        e.Cancel = true;
        ((Label)detailsView.FindControl("lblWrongLogin")).Visible = true;
      }
    }

    protected void detailsView_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
      if (e.Exception != null)
      {
        throw e.Exception;
      }
      Response.Redirect(prevPage);
    }

    protected void detailsView_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
      if (e.CancelingEdit)
      {
        Response.Redirect(prevPage);
      }
    }

    protected void objectDataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
      ((FirmwareTask)e.InputParameters[0]).FirmwareId = Convert.ToInt64(Request["id"]);
    }
  }
}