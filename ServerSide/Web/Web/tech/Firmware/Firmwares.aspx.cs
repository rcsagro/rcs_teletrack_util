﻿using System;
using System.Web.UI;

namespace RCS.Web.UI.Tech.Firmware
{
  public partial class Firmwares : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gridFirmwares);
      MaintainScrollPositionOnPostBack = true;
    }
  }
}