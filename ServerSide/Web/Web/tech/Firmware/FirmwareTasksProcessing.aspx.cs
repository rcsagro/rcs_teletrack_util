﻿using System;
using System.Web.UI;

namespace RCS.Web.UI.Tech.Firmware
{
  public partial class FirmwareTasksProcessing : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gridProc);
    }
  }
}