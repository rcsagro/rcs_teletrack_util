﻿<%@ Page Title="Журнал обработки задач отправки прошивок" Language="C#" MasterPageFile="~/tech/MasterPage.master"
  AutoEventWireup="true" EnableViewState="false" EnableSessionState="False" CodeFile="FirmwareTasksProcessing.aspx.cs"
  Inherits="RCS.Web.UI.Tech.Firmware.FirmwareTasksProcessing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table id="MainTable" style="width: 100%">
    <tr>
      <td style="width: 25%; height: 31px" align="right">
        &nbsp;
      </td>
      <td style="height: 31px; text-align: center">
        <asp:Label ID="lblTitle" runat="server" Font-Size="X-Large" ForeColor="RoyalBlue"
          Text="Журнал обработки задач отправки прошивок" Font-Bold="False"></asp:Label>
      </td>
      <td style="width: 25%; height: 31px">
      </td>
    </tr>
    <tr>
      <td colspan="3">
        <asp:GridView ID="gridProc" runat="server" AllowPaging="True" AllowSorting="True"
          AutoGenerateColumns="False" CellPadding="2" DataSourceID="objDsFirmware" EmptyDataText="Нет данных для отображения."
          Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center" EnableViewState="False">
          <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
            NextPageText="&lt;" PreviousPageText="&gt;" />
          <RowStyle BackColor="#EFF3FB" />
          <Columns>
            <asp:BoundField DataField="Date" HeaderText="Дата" ReadOnly="True" SortExpression="Date">
            </asp:BoundField>
            <asp:BoundField DataField="Stage" HeaderText="Стадия" ReadOnly="True" SortExpression="Stage">
              <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Телетрек">
              <ItemTemplate>
                <asp:Label ID="lblTtLogin" runat="server" Text='<%# Eval("Task.TeletrackLogin") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Description" HeaderText="Описание" SortExpression="Description">
              <ItemStyle Width="250px" />
            </asp:BoundField>
            <asp:BoundField DataField="UserName" HeaderText="Логин оператора" ReadOnly="True"
              SortExpression="UserName">
              <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:BoundField DataField="UserIp" HeaderText="Ip адрес оператора" ReadOnly="True"
              SortExpression="UserIp"></asp:BoundField>
            <asp:TemplateField HeaderText="Задача">
              <ItemTemplate>
                <asp:HyperLink ID="HyperLink1" runat="server" Text="..." NavigateUrl='<%# string.Format(
                  "~/tech/Firmware/EditFirmwareTask.aspx?id={0}&firmware={1}",
                  Eval("FirmwareTaskId"),
                  Eval("Task.FirmwareId")) %>'>
                </asp:HyperLink>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="FirmwareTaskId" HeaderText="FirmwareTaskId" ReadOnly="True"
              SortExpression="FirmwareTaskId" Visible="False"></asp:BoundField>
          </Columns>
          <FooterStyle BackColor="#507CD1" ForeColor="White" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" />
          <HeaderStyle BackColor="#507CD1" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" Wrap="False" />
          <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
      </td>
    </tr>
    <tr>
      <td style="width: 25%; height: 20px">
      </td>
      <td style="height: 20px">
      </td>
      <td style="width: 25%; height: 20px">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="objDsFirmware" runat="server" SelectCountMethod="GetTaskProcCount"
    SelectMethod="GetProcPage" SortParameterName="sortColumns" TypeName="RCS.Web.BLL.FirmwareTaskBL"
    EnablePaging="True" DataObjectTypeName="RCS.OnlineServer.Core.Model.FirmwareTaskProcessing">
  </asp:ObjectDataSource>
</asp:Content>
