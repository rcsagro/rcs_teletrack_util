﻿<%@ Page Title="Редактирование информации о прошивке" Language="C#" MasterPageFile="~/tech/MasterPage.master"
  AutoEventWireup="true" CodeFile="EditFirmware.aspx.cs" Inherits="RCS.Web.UI.Tech.Firmware.EditFirmware" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td colspan="6" style="height: 30px; text-align: center">
        <strong style="vertical-align: middle; text-align: center; font-size: x-large; color: RoyalBlue;">
          Редактирование описания изменения прошивки</strong>
      </td>
    </tr>
    <tr>
      <td style="height: 140px;" colspan="6">
        <asp:DetailsView ID="detailsView" runat="server" AutoGenerateRows="False" DataSourceID="objectDataSource"
          Height="50px" Width="400px" CellPadding="4" ForeColor="#333333" GridLines="None"
          Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center" DataKeyNames="Id,VersionFrom,FileName,VersionStamp,RegistryDate">
          <Fields>
            <asp:TemplateField HeaderText="Версия прошивки" SortExpression="VersionTo">
              <EditItemTemplate>
                <asp:TextBox ID="txtVerTo" runat="server" Text='<%# Bind("VersionTo") %>' MaxLength="25"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtVerTo"
                  ErrorMessage="Обязательное" SetFocusOnError="True">Обязательное</asp:RequiredFieldValidator>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%# Bind("VersionTo") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Дата выпуска" SortExpression="IssueDateStr">
              <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("IssueDateStr") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
                  ErrorMessage="Обязательное" SetFocusOnError="True">Обязательное</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBox1"
                  ErrorMessage="Неправильная дата" Operator="DataTypeCheck" Type="Date">Неправильная дата</asp:CompareValidator>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("IssueDateStr") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Описание" SortExpression="Description">
              <EditItemTemplate>
                <asp:TextBox ID="txtDescr" runat="server" Text='<%# Bind("Description") %>' Height="50px"
                  MaxLength="1000" TextMode="MultiLine" Width="400px"></asp:TextBox>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Доступность" SortExpression="Enabled">
              <EditItemTemplate>
                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Enabled") %>' />
              </EditItemTemplate>
              <ItemTemplate>
                <asp:CheckBox ID="CheckBox3" runat="server" Checked='<%# Bind("Enabled") %>' Enabled="False" />
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Версия телетрека" SortExpression="VersionFrom">
              <EditItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Eval("VersionFrom") %>'></asp:Label>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%# Bind("VersionFrom") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Дата регистрации" SortExpression="RegistryDate">
              <EditItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# Eval("RegistryDate") %>'></asp:Label>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label5" runat="server" Text='<%# Bind("RegistryDate") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Имя файла" SortExpression="FileName">
              <EditItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
              </EditItemTemplate>
              <InsertItemTemplate>
                &nbsp;&nbsp;
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label6" runat="server" Text='<%# Bind("FileName") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" />
          </Fields>
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
          <RowStyle BackColor="#EFF3FB" />
          <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <EditRowStyle BackColor="#CCFFFF" />
          <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
      </td>
    </tr>
    <tr>
      <td style="width: 50%; height: 23px;" valign="bottom" colspan="2">
        &nbsp;
        <asp:HyperLink ID="lnkBackToList" runat="server" NavigateUrl="~/tech/Firmware/Firmwares.aspx"
          Font-Size="Small" ToolTip="Переход к списку изменений прошивок">Список прошивок</asp:HyperLink>
      </td>
      <td align="left" colspan="2" style="height: 23px">
        &nbsp;
      </td>
      <td style="width: 50%; height: 23px;" colspan="2" align="right">
        <asp:HyperLink ID="lnkLog" runat="server" Font-Size="Small" ToolTip="Журнал изменений описания прошивки">Лог изменений</asp:HyperLink>
      </td>
    </tr>
    <tr>
      <td colspan="6" style="height: 1px; background-color: #cccccc" valign="bottom">
      </td>
    </tr>
    <tr>
      <td colspan="6" style="height: 23px; text-align: center;" valign="top">
        <strong style="font-size: large; color: RoyalBlue">Задачи отправки изменения телетрекам</strong>
      </td>
    </tr>
    <tr>
      <td style="height: 25px;" valign="top" align="right">
        <asp:Button ID="btnNewTask" runat="server" Text="Новая задача" ToolTip="Создание новой задачи отправки прошиви телетреку возможно только если прошивка доступна"
          Width="150px" />
      </td>
      <td colspan="2" style="height: 25px; text-align: center; width: 11px;" valign="top">
      </td>
      <td colspan="2" style="height: 25px; text-align: center;" valign="top">
      </td>
      <td style="height: 25px; text-align: center;" valign="top">
      </td>
    </tr>
    <tr>
      <td colspan="6" style="height: 65px; text-align: center;" valign="top">
        <asp:GridView ID="gridDetails" runat="server" AutoGenerateColumns="False" DataSourceID="detailDataSource"
          AllowPaging="True" Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center"
          EmptyDataText="Нет сформированных задач." CellPadding="4" AllowSorting="True">
          <Columns>
            <asp:BoundField DataField="TeletrackLogin" HeaderText="Телетрек" ReadOnly="True"
              SortExpression="TeletrackLogin" />
            <asp:BoundField DataField="Stage" HeaderText="Стадия" SortExpression="Stage">
              <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:BoundField DataField="CreatedDate" HeaderText="Дата создания" ReadOnly="True"
              SortExpression="CreatedDate">
              <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:BoundField DataField="Description" HeaderText="Описание">
              <ItemStyle Width="250px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Детально">
              <ItemTemplate>
                <asp:HyperLink ID="detailLink" runat="server" Text="..." NavigateUrl='<%# string.Format(
                  "~/tech/Firmware/EditFirmwareTask.aspx?id={0}&firmware={1}", 
                  Eval("Id"), 
                  Eval("FirmwareId")) %>'>
                </asp:HyperLink>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id"
              Visible="False" />
            <asp:BoundField DataField="FirmwareId" HeaderText="FirmwareId" ReadOnly="True" SortExpression="FirmwareId"
              Visible="False" />
          </Columns>
          <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#507CD1"
            ForeColor="White" />
          <FooterStyle BorderStyle="Dotted" />
          <PagerStyle HorizontalAlign="Center" BackColor="#507CD1" ForeColor="White" />
          <EditRowStyle BackColor="LightCoral" />
          <SelectedRowStyle BackColor="#D1DDF1" />
          <PagerSettings Mode="NumericFirstLast" />
          <RowStyle BackColor="#EFF3FB" />
          <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
      </td>
    </tr>
    <tr>
      <td style="width: 50%; height: 4px" valign="bottom" colspan="2">
        &nbsp;
      </td>
      <td style="width: 83px; height: 4px" colspan="2">
      </td>
      <td style="width: 50%; height: 4px" colspan="2">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="objectDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="GetFirmwareById" TypeName="RCS.Web.BLL.FirmwareBL" UpdateMethod="Update"
    DataObjectTypeName="RCS.OnlineServer.Core.Model.Firmware" OnSelected="objectDataSource_Selected">
    <SelectParameters>
      <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" DbType="Int64" />
    </SelectParameters>
  </asp:ObjectDataSource>
  <asp:ObjectDataSource ID="detailDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    TypeName="RCS.Web.BLL.FirmwareTaskBL" SelectMethod="GetTasksPagesById" SelectCountMethod="GetFirmwareTasksCountById"
    EnablePaging="True" DataObjectTypeName="RCS.OnlineServer.Core.Model.FirmwareTask"
    SortParameterName="sortColumns">
    <SelectParameters>
      <asp:QueryStringParameter Name="firmwareId" QueryStringField="id" DbType="Int64" />
    </SelectParameters>
  </asp:ObjectDataSource>
</asp:Content>
