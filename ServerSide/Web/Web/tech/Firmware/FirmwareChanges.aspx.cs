﻿using System;
using System.Web.UI;

namespace RCS.Web.UI.Tech.Firmware
{
  public partial class FirmwareChanges : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gridDetails);

      if (!IsPostBack)
      {
        lnkBack.NavigateUrl = Request.UrlReferrer.ToString();
      }
    }
  }
}