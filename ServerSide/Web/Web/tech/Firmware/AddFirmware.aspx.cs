﻿using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using RCS.OnlineServer.Core.Model;
using RCS.OnlineServer.Core.Services;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech.Firmware
{
  public partial class AddFirmware : Page
  {
    static string prevPage = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        prevPage = Request.UrlReferrer.ToString();
        FirmwareBL.CancelUpload(Session);
      }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
      FileUpload fu = (FileUpload)detailsView.FindControl("fileUpload");
      Label label = (Label)detailsView.FindControl("lblUpload");
      string path = string.Empty;
      if (fu.HasFile)
      {
        try
        {
          label.Text = "Идет загрузка файла";
          path = Server.MapPath("~/App_Data/") + fu.FileName;
          fu.PostedFile.SaveAs(path);
          FirmwareBL.HandleUploadedFile(path, Session);
          label.Text = string.Format("Файл {0} загружен", fu.FileName);
          GetLblFileError().Visible = false;
        }
        catch (Exception ex)
        {
          label.Text = "ERROR: " + ex.Message.ToString();
        }
        finally
        {
          if (File.Exists(path))
          {
            File.Delete(path);
          }
        }
      }
    }

    protected void detailsView_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
      DateTime tmp;
      if (!DateTime.TryParse(Convert.ToString(e.Values["IssueDateStr"]), out tmp))
      {
        e.Cancel = true;
        GetLblDateError().Visible = true;
      }

      if (!FirmwareBL.IsFileUploaded(Session))
      {
        e.Cancel = true;
        GetLblFileError().Visible = true;
      }
    }

    protected void detailsView_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
      if (e.Exception != null)
      {
        throw e.Exception;
      }
      Redirect();
    }

    private Label GetLblFileError()
    {
      return (Label)detailsView.FindControl("lblFileError");
    }

    private Label GetLblDateError()
    {
      return (Label)detailsView.FindControl("lblDateError");
    }

    protected void detailsView_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
      if (e.CancelingEdit)
      {
        Redirect();
      }
    }

    private void Redirect()
    {
      FirmwareBL.CancelUpload(Session);
      Response.Redirect(prevPage);
    }

    protected void objectDataSource_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
    }

    protected void objectDataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
      FirmwareSource fw = (FirmwareSource)e.InputParameters[0];
      fw.RegistryDate = DateTime.Now;
      fw.FileName = Convert.ToString(FirmwareBL.GetFirmwareFileName(Session));

      string text = Convert.ToString(FirmwareBL.GetFirmwareText(Session));
      fw.Source = text;
      fw.VersionFrom = FirmwareService.GetVersion(text);
    }
  }
}