﻿using System;
using System.Web.UI;

namespace RCS.Web.UI.Tech.Firmware
{
  public partial class EditFirmware : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gridDetails);

      if (!IsPostBack)
      {
        btnNewTask.PostBackUrl = FormatAddTaskUrl();
        lnkLog.NavigateUrl = FormatLogUrl();
      }
    }

    private string FormatAddTaskUrl()
    {
      return string.Format("~/tech/Firmware/AddFirmwareTask.aspx?id={0}",
        Request["id"]);
    }

    private string FormatLogUrl()
    {
      return string.Format("~/tech/Firmware/FirmwareChanges.aspx?id={0}",
        Request["id"]);
    }

    protected void objectDataSource_Selected(object sender, System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs e)
    {
      btnNewTask.Enabled = ((RCS.OnlineServer.Core.Model.Firmware)e.ReturnValue).Enabled;
    }
  }
}