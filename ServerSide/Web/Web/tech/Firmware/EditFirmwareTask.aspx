<%@ Page Title="��������� ������� �������� ��������" Language="C#" MasterPageFile="~/tech/MasterPage.master"
  AutoEventWireup="true" CodeFile="EditFirmwareTask.aspx.cs" Inherits="RCS.Web.UI.Tech.Firmware.EditFirmwareTask" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td colspan="3" style="height: 30px; text-align: center">
        <strong style="vertical-align: middle; text-align: center; font-size: x-large; color: RoyalBlue;">
          �������������� ������� ������ �������� ��������</strong>
      </td>
    </tr>
    <tr>
      <td style="height: 140px;" colspan="3">
        <asp:DetailsView ID="detailsView" runat="server" AutoGenerateRows="False" DataSourceID="objectDataSource"
          Height="50px" Width="400px" CellPadding="4" ForeColor="#333333" GridLines="None"
          Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center" DataKeyNames="Id,FirmwareId,VersionStamp">
          <Fields>
            <asp:BoundField DataField="TeletrackLogin" HeaderText="����� ���������" ReadOnly="True" />
            <asp:BoundField DataField="StageDb" HeaderText="������ ����������" ReadOnly="True" />
            <asp:BoundField DataField="CreatedDateStr" HeaderText="���� ��������" ReadOnly="True" />
            <asp:TemplateField HeaderText="��������" SortExpression="Description">
              <EditItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Height="54px" Text='<%# Bind("Description") %>'
                  TextMode="MultiLine" Width="376px"></asp:TextBox>
              </EditItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" />
          </Fields>
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
          <RowStyle BackColor="#EFF3FB" />
          <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <EditRowStyle BackColor="#CCFFFF" />
          <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
        <br />
        <center>
          <asp:Button ID="btnCancel" runat="server" Text="������ ����������" ToolTip="�������� ���������� ������"
            Width="155px" Enabled="False" OnClick="btnCancel_Click" OnClientClick="return confirm ('�� ������������� ������ �������� ���������� ������?')"
            BorderColor="Red" />
        </center>
      </td>
    </tr>
    <tr>
      <td style="width: 50%;" valign="bottom">
        &nbsp;
        <asp:HyperLink ID="lnkBackToList" runat="server" Font-Size="Small" ToolTip="��������� �� ���������� ��������">���������</asp:HyperLink>
      </td>
      <td align="left">
        &nbsp;
      </td>
      <td style="width: 50%;" align="right">
        <asp:HyperLink ID="lnkFirmware" runat="server" Font-Size="Small" ToolTip="������� � �������� ��������� ��������">��������</asp:HyperLink>
      </td>
    </tr>
    <tr>
      <td colspan="3" style="height: 1px; background-color: #cccccc" valign="bottom">
      </td>
    </tr>
    <tr>
      <td colspan="3" style="height: 23px; text-align: center;" valign="top">
        <strong style="font-size: large; color: RoyalBlue">������ ����������� ��������� ������</strong>
      </td>
    </tr>
    <tr>
      <td colspan="3" style="height: 65px; text-align: center;" valign="top">
        <asp:GridView ID="gridDetails" runat="server" AutoGenerateColumns="False" DataSourceID="detailDataSource"
          AllowPaging="True" Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center"
          EmptyDataText="��� �������������� �����." CellPadding="4" AllowSorting="True">
          <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#507CD1"
            ForeColor="White" />
          <Columns>
            <asp:BoundField DataField="Date" HeaderText="����" ReadOnly="True" SortExpression="Date">
              <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:BoundField DataField="Description" HeaderText="��������">
              <ItemStyle Width="250px" />
            </asp:BoundField>
            <asp:BoundField DataField="Stage" HeaderText="������" ReadOnly="True" SortExpression="Stage">
              <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:BoundField DataField="UserName" HeaderText="����� ���������" ReadOnly="True"
              SortExpression="UserName">
              <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:BoundField DataField="UserIp" HeaderText="Ip ����� ���������" ReadOnly="True"
              SortExpression="UserIp" />
          </Columns>
          <FooterStyle BorderStyle="Dotted" />
          <PagerStyle HorizontalAlign="Center" BackColor="#507CD1" ForeColor="White" />
          <EditRowStyle BackColor="LightCoral" />
          <SelectedRowStyle BackColor="#D1DDF1" />
          <PagerSettings Mode="NumericFirstLast" />
          <RowStyle BackColor="#EFF3FB" />
          <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
      </td>
    </tr>
    <tr>
      <td style="width: 50%; height: 4px" valign="bottom">
        &nbsp;
      </td>
      <td style="width: 83px; height: 4px">
      </td>
      <td style="width: 50%; height: 4px">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="objectDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="GetTaskById" TypeName="RCS.Web.BLL.FirmwareTaskBL" UpdateMethod="Update"
    DataObjectTypeName="RCS.OnlineServer.Core.Model.FirmwareTask" OnSelected="objectDataSource_Selected">
    <SelectParameters>
      <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" DbType="Int64" />
    </SelectParameters>
  </asp:ObjectDataSource>
  <asp:ObjectDataSource ID="detailDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    TypeName="RCS.Web.BLL.FirmwareTaskBL" SelectMethod="GetProcPagesById" SelectCountMethod="GetTaskProcCountById"
    EnablePaging="True" DataObjectTypeName="RCS.OnlineServer.Core.Model.FirmwareTaskProcessing"
    SortParameterName="sortColumns">
    <SelectParameters>
      <asp:QueryStringParameter Name="taskId" QueryStringField="id" DbType="Int64" />
    </SelectParameters>
  </asp:ObjectDataSource>
</asp:Content>
