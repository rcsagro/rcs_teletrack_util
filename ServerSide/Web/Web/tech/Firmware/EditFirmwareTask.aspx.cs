﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using RCS.OnlineServer.Core.Model;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech.Firmware
{
  public partial class EditFirmwareTask : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gridDetails);

      if (!IsPostBack)
      {
        lnkBackToList.NavigateUrl = Request.UrlReferrer.ToString();
        lnkFirmware.NavigateUrl = GetFirmwareUrl();
      }
    }

    private string GetFirmwareUrl()
    {
      return string.Format("~/tech/Firmware/EditFirmware.aspx?id={0}",
        Request["Firmware"]);
    }

    protected void objectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
      FirmwareTask task = (FirmwareTask)e.ReturnValue;

      if (FirmwareTaskBL.CanCancelTask(task.Stage))
      {
        btnCancel.Enabled = true;
        btnCancel.BorderColor = Color.Red;
      }
      else
      {
        btnCancel.Enabled = false;
        btnCancel.BorderColor = Color.LightGray;
      }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
      FirmwareTaskBL.CancelTask(Convert.ToInt64(Request["id"]));
      Response.Redirect(Request.RawUrl);
    }
  }
}