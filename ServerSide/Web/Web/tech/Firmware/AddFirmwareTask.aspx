﻿<%@ Page Title="Новая задача отправки прошивки" Language="C#" MasterPageFile="~/tech/MasterPage.master"
  AutoEventWireup="true" CodeFile="AddFirmwareTask.aspx.cs" Inherits="RCS.Web.UI.Tech.Firmware.AddFirmwareTask" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table width="100%" style="width: 100%">
    <tr>
      <td style="width: 50%; height: 30px;">
      </td>
      <td align="center" style="width: 400px; height: 30px" valign="middle">
        <strong style="font-size: x-large; color: RoyalBlue; text-align: center; vertical-align: middle;">
          Добавление новой задачи отправки прошивки</strong>
      </td>
      <td style="width: 50%; height: 30px;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%;">
      </td>
      <td align="left" style="width: 400px; height: 158px">
        <asp:DetailsView ID="detailsView" runat="server" AutoGenerateRows="False" DataSourceID="objectDataSource"
          Height="50px" Width="400px" CellPadding="4" ForeColor="#333333" GridLines="None"
          Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center" OnItemInserting="detailsView_ItemInserting"
          OnModeChanging="detailsView_ModeChanging" DefaultMode="Insert" OnItemInserted="detailsView_ItemInserted">
          <Fields>
            <asp:TemplateField HeaderText="Логин телетрека" SortExpression="TeletrackLogin">
              <InsertItemTemplate>
                <asp:TextBox ID="txtLogin" runat="server" Text='<%# Bind("TeletrackLogin") %>'></asp:TextBox>
                <asp:Button ID="btnSelectLogin" runat="server" Text="Выбрать" Enabled="False" ToolTip="Выбрать телетрек" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtLogin"
                  ErrorMessage="Обязательное">Обязательное</asp:RequiredFieldValidator>
                <asp:Label ID="lblWrongLogin" runat="server" ForeColor="Red" Text="Введите правильный логин"
                  Visible="False"></asp:Label>
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Bind("TeletrackLogin") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Описание" SortExpression="Description">
              <InsertItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Height="49px" Text='<%# Bind("Description") %>'
                  TextMode="MultiLine" Width="370px"></asp:TextBox>
              </InsertItemTemplate>
              <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowInsertButton="True" />
          </Fields>
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
          <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <InsertRowStyle BackColor="#99CCFF" />
          <EditRowStyle BackColor="#2461BF" />
          <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
      </td>
      <td style="width: 50%;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%;">
      </td>
      <td align="left" style="width: 400px;">
        &nbsp;
      </td>
      <td style="width: 50%;">
      </td>
    </tr>
  </table>
  <div style="vertical-align: middle; text-align: center">
  </div>
  <asp:ObjectDataSource ID="objectDataSource" runat="server" InsertMethod="Insert"
    TypeName="RCS.Web.BLL.FirmwareTaskBL" DataObjectTypeName="RCS.OnlineServer.Core.Model.FirmwareTask"
    OldValuesParameterFormatString="original_{0}" OnInserting="objectDataSource_Inserting">
    <InsertParameters>
      <asp:QueryStringParameter DbType="Int64" Name="firmwareId" QueryStringField="id"
        Type="Int64" />
    </InsertParameters>
  </asp:ObjectDataSource>
</asp:Content>
