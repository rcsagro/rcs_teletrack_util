<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" EnableSessionState="False" CodeFile="StatUserTotal.aspx.cs"
  Inherits="RCS.Web.UI.Tech.StatUserTotal" Title="����� ���������� �� ������� / ��������" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td style="text-align: center; height: 29px;" colspan="3">
        <asp:Label ID="Label1" runat="server" Font-Size="X-Large" ForeColor="RoyalBlue" Text="����� ���������� �� ������� / ��������"></asp:Label>
      </td>
    </tr>
    <tr>
      <td style="width: 30%; text-align: left; vertical-align: top;">
        <table style="width: 100%">
          <tr>
            <td>
              <asp:RadioButtonList ID="rbMonthSelect" runat="server" RepeatDirection="Horizontal"
                Width="100%" AutoPostBack="True" ForeColor="RoyalBlue">
                <asp:ListItem Selected="True" Value="0">������� �����</asp:ListItem>
                <asp:ListItem Value="1">������� �����</asp:ListItem>
              </asp:RadioButtonList>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label6" runat="server" Text="������ �� ������" ForeColor="RoyalBlue"></asp:Label>
              &nbsp;&nbsp;
              <asp:DropDownList ID="dlDealer" runat="server" AutoPostBack="True" DataSourceID="sdsDealer"
                DataTextField="DealerName" DataValueField="Id" AppendDataBoundItems="True" EnableTheming="False">
                <asp:ListItem Selected="True" Value="0">���</asp:ListItem>
              </asp:DropDownList>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 40%; text-align: center; vertical-align: top;">
        <table style="width: 100%; height: 100%;">
          <tr>
            <td style="width: 80%; text-align: left;">
              <asp:Label ID="lbCntTltr" runat="server" Text="����� ����������" ForeColor="RoyalBlue"></asp:Label>
            </td>
            <td style="text-align: left; height: 21px;">
              <asp:Label ID="lbTot" runat="server" ForeColor="Black" Text="0" Width="28px" Font-Bold="True"
                Font-Names="Arial"></asp:Label>
            </td>
          </tr>
          <tr>
            <td style="width: 125px; text-align: left">
              <asp:Label ID="Label7" runat="server" ForeColor="RoyalBlue" Text="��������"></asp:Label>
            </td>
            <td style="text-align: left">
              <asp:Label ID="lbAct" runat="server" ForeColor="ForestGreen" Text="0" Width="28px"
                Font-Bold="True"></asp:Label>
            </td>
          </tr>
          <tr>
            <td style="width: 125px; text-align: left">
              <asp:Label ID="Label8" runat="server" ForeColor="RoyalBlue" Text="����������"></asp:Label>
            </td>
            <td style="text-align: left">
              <asp:Label ID="lbNAct" runat="server" ForeColor="Red" Text="0" Width="28px" Font-Bold="True"></asp:Label>
            </td>
          </tr>
          <tr>
            <td style="width: 125px; text-align: left">
              <asp:Label ID="lbSumKbl" runat="server" Text="����� ���������� ������ �� ����� (��)"
                ForeColor="RoyalBlue" Width="308px"></asp:Label>
            </td>
            <td style="text-align: left">
              <asp:Label ID="lbSumKb" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="Black"
                Text="0" Width="28px"></asp:Label>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 30%; text-align: right; vertical-align: top;">
        <table style="width: 100%; height: 100%;">
          <tr>
            <td style="width: 258px; text-align: left">
              <asp:Label ID="Label2" runat="server" Text="�������� �������� ����������" ForeColor="RoyalBlue"
                Width="222px"></asp:Label>
            </td>
            <td style="text-align: left">
              <asp:TextBox ID="txCritKb" runat="server" Width="30px" Style="text-align: center"
                OnTextChanged="txCritKb_TextChanged" AutoPostBack="True">10</asp:TextBox>
              &nbsp;&nbsp;
              <asp:Label ID="Label3" runat="server" Text="��" ForeColor="RoyalBlue"></asp:Label>
            </td>
          </tr>
          <tr>
            <td style="width: 258px; text-align: left;">
              <asp:Label ID="lbCritDay" runat="server" Text="�������� �������� ����������" ForeColor="RoyalBlue"></asp:Label>
            </td>
            <td style="text-align: left;">
              <asp:TextBox ID="txCritDay" runat="server" Style="text-align: center" Width="30px"
                AutoPostBack="True" OnTextChanged="txCritDay_TextChanged">20</asp:TextBox>
              &nbsp;&nbsp;
              <asp:Label ID="Label5" runat="server" Text="����" ForeColor="RoyalBlue"></asp:Label>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top; text-align: left;">
        &nbsp;
      </td>
      <td style="text-align: left; vertical-align: top;">
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txCritKb"
          ErrorMessage="�������� ������ ���������� 0" Font-Names="Arial" Font-Size="12px"
          Operator="GreaterThan" ValueToCompare="0">
        </asp:CompareValidator>
        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txCritKb"
          ErrorMessage="�������� ������ ���� ����� ��������!" Font-Names="Arial" Font-Size="12px"
          Operator="DataTypeCheck" Type="Integer">
        </asp:CompareValidator>
      </td>
      <td style="text-align: right; vertical-align: top; width: 30%;">
        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txCritDay"
          ErrorMessage="�������� ������ ������ 0" Font-Names="Arial" Font-Size="12px" Operator="GreaterThan"
          ValueToCompare="0">
        </asp:CompareValidator>
        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txCritDay"
          ErrorMessage="���������� ���� � ������ �� 1 �� 31" Font-Names="Arial" Font-Size="12px"
          MaximumValue="31" MinimumValue="1" Type="Integer">
        </asp:RangeValidator>
      </td>
    </tr>
    <tr>
      <td colspan="3">
        <asp:GridView ID="gvWork" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
          CellPadding="4" DataKeyNames="ID_User" DataSourceID="odsTotalMonth" EmptyDataText="��� ������ ��� �����������."
          Font-Names="Verdana" Font-Size="Small" ForeColor="#333333" HorizontalAlign="Center"
          Width="100%" OnDataBound="gvWork_DataBound" OnRowCommand="gvWork_RowCommand">
          <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
            NextPageText="&lt;" PreviousPageText="&gt;" />
          <RowStyle BackColor="#EFF3FB" />
          <Columns>
            <asp:BoundField DataField="ID_User" HeaderText="ID_User" Visible="False" />
            <asp:BoundField DataField="Id_Client_min" HeaderText="Id_Client_min" Visible="False" />
            <asp:BoundField DataField="Dealer" HeaderText="�����" NullDisplayText="RCS" SortExpression="Dealer" />
            <asp:TemplateField HeaderText="������" SortExpression="Name">
              <ItemTemplate>
                <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# Eval("Id_user") %>'
                  CommandName="Move" Text='<%# Eval("Name") %>'></asp:LinkButton>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Total_Trcs" HeaderText="����� ����������" SortExpression="Total_Trcs">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Act" HeaderText="��������" SortExpression="Act">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="ActNo" HeaderText="����������" SortExpression="ActNo">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Sum_kb" HeaderText="����� �������� ������ , ��" SortExpression="Sum_kb">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Cnt_Session" HeaderText="����� ���������� � �����" SortExpression="Cnt_Session">
              <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
          </Columns>
          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
          <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
          <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
          <EditRowStyle BackColor="#2461BF" />
          <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
      </td>
    </tr>
  </table>
  <asp:SqlDataSource ID="sdsDealer" runat="server" SelectCommand="Select_Dealers" SelectCommandType="StoredProcedure"
    ConnectionString="<%$ ConnectionStrings:OnLineServiceDBConnectionString %>" ProviderName="<%$ ConnectionStrings:OnLineServiceDBConnectionString.ProviderName %>">
  </asp:SqlDataSource>
  <asp:ObjectDataSource ID="odsTotalMonth" runat="server" SelectMethod="GetMonthTotal"
    TypeName="RCS.Web.BLL.StatisticsBL">
    <SelectParameters>
      <asp:ControlParameter ControlID="rbMonthSelect" DefaultValue="0" Name="iSelectMonth"
        PropertyName="SelectedValue" />
      <asp:ControlParameter ControlID="txCritKb" DefaultValue="10" Name="iThrKb" PropertyName="Text" />
      <asp:ControlParameter ControlID="txCritDay" DefaultValue="20" Name="iThrDay" PropertyName="Text" />
      <asp:ControlParameter ControlID="dlDealer" DefaultValue="0" Name="iDlr" PropertyName="SelectedValue" />
    </SelectParameters>
  </asp:ObjectDataSource>
</asp:Content>
