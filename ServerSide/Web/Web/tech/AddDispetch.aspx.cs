﻿using System;
using System.Web.UI.WebControls;
using RCS.Web.BLL;
using RCS.Web.Entities;

namespace RCS.Web.UI.Tech
{
  public partial class AddDispetch : System.Web.UI.Page
  {
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {          
      if (e.Exception == null)
      {
        object obj = Session[SessionKeyNames.LAST_INSERTED_CLIENT_ID];
        if (obj == null)
        {
          DispetchViewRedirect();
        }
        else
        {
          Session.Remove(SessionKeyNames.LAST_INSERTED_CLIENT_ID);
          Response.Redirect(String.Format(
            "~/tech/EditDispetch.aspx?ID_Client={0}&ID_User={1}",
            obj, Request["id_user"]));
        }
      }
    }

    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
      if (e.CancelingEdit)
      {
        DispetchViewRedirect();
      }
    }

    private void DispetchViewRedirect()
    {
      Response.Redirect(String.Format(
        "~/tech/DispetchView2.aspx?ID_User={0}", Request["id_user"]));
    }

    /// <summary>
    /// Установка значения ID_User.
    /// </summary>
    /// <param name="teletrackInfo">TeletrackInfo.</param>
    private void SetIdUserValue(ClientInfo client)
    {
      client.ID_User = Convert.ToInt32(Request["id_user"]);
    }

    protected void objectDataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
      // У ClientInfo не установлен ID_User.
      SetIdUserValue((ClientInfo)e.InputParameters[0]);
    }

    protected void validatorUniqueLogin_ServerValidate(object source, ServerValidateEventArgs args)
    {
      if (ClientInfoBL.ClientExists(args.Value))
      {
        args.IsValid = false;
      }
    }
  }
}