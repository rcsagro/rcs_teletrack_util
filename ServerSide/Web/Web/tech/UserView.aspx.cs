﻿using System;
using System.Web.UI.WebControls;

namespace RCS.Web.UI.Tech
{
  public partial class UserView2 : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack && Request["id_user"] != null)
      {
        int id_user = 0;
        int.TryParse(Request["id_user"].ToString(), out id_user);
        if (id_user == 0)
        {
          HyperLink2.Visible = false;
        }
        else
        {
          HyperLink2.Visible = true;
        }
      }
      ((MasterPage)Page.Master).SetGridViewPageSize(GridView1);
    }
  }
}