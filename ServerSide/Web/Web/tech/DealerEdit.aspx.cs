﻿using System;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech
{
  public partial class DealerEdit : System.Web.UI.Page
  {
    string oldLog = "";
    int id_dealer = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
      int id;
      if (int.TryParse(Request["id"].ToString(), out id))
      {
        id_dealer = id;
        if (!IsPostBack)
        {
          string login;
          string pass;
          string descr;
          if (DealerInfoBL.GetDealerInfoById(id, out login, out pass, out descr))
          {
            loginTextBox.Text = login;
            passTextBox.Text = pass;
            descrTextBox.Text = descr;
            oldLog = login;
          }
          else
          {
            Label1.Visible = true;
            SaveButton.Enabled = false;
          }
        }
      }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
      Label4.Visible = false; bool isExist = false;
      isExist = DealerInfoBL.IsDealerNameExist(loginTextBox.Text, id_dealer);

      if (!isExist)
      {
        DealerInfoBL.UpdateDealerInfo(loginTextBox.Text, passTextBox.Text, descrTextBox.Text, id_dealer);
        Redirect();
      }
      else
      {
        Label4.Visible = true;
      }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
      Redirect();
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
      SaveButton.Visible = true;
      Button2.Visible = true;
      Button1.Visible = false;
      Button3.Visible = false;
      ValidationSummary1.Enabled = true;
      RequiredFieldValidator1.Enabled = true;
      RequiredFieldValidator2.Enabled = true;
      RequiredFieldValidator3.Enabled = true;
      loginTextBox.ReadOnly = false;
      passTextBox.ReadOnly = false;
      descrTextBox.ReadOnly = false;
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
      DealerInfoBL.DeleteDealerInfo(id_dealer);
      Redirect();
    }

    private void Redirect()
    {
      Response.Redirect("~/tech/DealerView.aspx");
    }
  }
}