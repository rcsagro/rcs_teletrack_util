﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  CodeFile="DealerEdit.aspx.cs" Inherits="RCS.Web.UI.Tech.DealerEdit" Title="Редактирование записи дилера" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td style="width: 50%; height: 30px;">
      </td>
      <td style="text-align: center; width: 400px; height: 30px;">
        <strong>Редактирование записи дилера</strong>
      </td>
      <td style="width: 50%; height: 30px;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td style="width: 400px">
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Ошибка обращения к базе!"
          Visible="False" Width="400px"></asp:Label>
      </td>
      <td style="width: 50%">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td style="width: 400px">
        <table style="width: 400px">
          <tr>
            <td style="width: 100px">
              Логин
            </td>
            <td style="width: 242px">
              <asp:TextBox ID="loginTextBox" runat="server" Width="209px" ReadOnly="True"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="loginTextBox"
                ErrorMessage="Поле Login не должно быть пустым" SetFocusOnError="True" Enabled="False">*</asp:RequiredFieldValidator>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px">
            </td>
            <td style="width: 242px">
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px">
              Пароль
            </td>
            <td style="width: 242px">
              <asp:TextBox ID="passTextBox" runat="server" Width="210px" ReadOnly="True"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="passTextBox"
                ErrorMessage="Поле Пароль не должно быть пустым" SetFocusOnError="True" Enabled="False">*</asp:RequiredFieldValidator>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px">
            </td>
            <td style="width: 242px">
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px; height: 24px">
              Описание
            </td>
            <td style="width: 242px; height: 24px">
              <asp:TextBox ID="descrTextBox" runat="server" Width="212px" ReadOnly="True"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="descrTextBox"
                ErrorMessage="Необходимо задать описание" Enabled="False">*</asp:RequiredFieldValidator>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td style="width: 400px">
        <table width="400">
          <tr>
            <td style="width: 50%;" align="center">
              <asp:Button ID="Button1" runat="server" OnClick="Button1_Click1" Text="Изменить"
                Width="90px" />
            </td>
            <td style="width: 50%;" align="center">
              <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Удалить" OnClientClick="return confirm ('Вы действительно хотите удалить эту запись?')"
                Width="90px" />
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td style="width: 400px">
        <table width="400">
          <tr>
            <td style="width: 50%;" align="center">
              <asp:Button ID="SaveButton" runat="server" OnClick="Button1_Click" Text="Сохранить"
                Visible="False" Width="90px" />
            </td>
            <td style="width: 50%;" align="center">
              <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Отменить" Visible="False"
                CausesValidation="False" Width="90px" />
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td style="width: 400px;">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Enabled="False" />
      </td>
      <td style="width: 50%">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td style="width: 400px">
        <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="Дилер с таким именем(Логином) уже существует!"
          Visible="False" Width="400px"></asp:Label>
      </td>
      <td style="width: 50%">
      </td>
    </tr>
  </table>
</asp:Content>
