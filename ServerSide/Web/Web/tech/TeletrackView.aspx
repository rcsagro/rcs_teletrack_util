﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" EnableSessionState="False" CodeFile="TeletrackView.aspx.cs"
  Inherits="RCS.Web.UI.Tech.TeletrackView" Title="Мониторинг телетреков" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div style="width: 100%; height: 100%; text-align: center;">
    <strong>Телетреки<br />
    </strong>
    <br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
      AutoGenerateColumns="False" DataKeyNames="ID_Teletrack" DataSourceID="SqlDataSourceTTView"
      Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center" Width="100%" OnRowDataBound="GridView1_RowDataBound"
      CellPadding="4">
      <FooterStyle BackColor="#507CD1" />
      <RowStyle BackColor="PaleGreen" />
      <Columns>
        <asp:TemplateField HeaderText="№п/п">
          <HeaderStyle Width="80px" />
          <ItemTemplate>
            <%# Convert.ToString( Container.DataItemIndex + 1 ) %>
          </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="ID_Teletrack" HeaderText="Номер" InsertVisible="False"
          ReadOnly="True" SortExpression="ID_Teletrack" Visible="False" />
        <asp:BoundField DataField="LoginTT" HeaderText="Логин" SortExpression="LoginTT" />
        <asp:BoundField DataField="PasswordTT" HeaderText="Пароль" SortExpression="PasswordTT" />
        <asp:BoundField DataField="LastConn" HeaderText="Последнее соединение" ReadOnly="True"
          SortExpression="LastConn" />
        <asp:BoundField DataField="Delta" HeaderText="Дельта" ReadOnly="True" SortExpression="Delta" />
        <asp:BoundField DataField="State" HeaderText="Состояние" ReadOnly="True" SortExpression="State" />
        <asp:BoundField DataField="Timeout" HeaderText="Таймаут(мин.)" SortExpression="Timeout" />
        <asp:BoundField DataField="UserName" HeaderText="Пользователь" SortExpression="UserName" />
        <asp:HyperLinkField DataNavigateUrlFields="ID_Teletrack" DataNavigateUrlFormatString="~/tech/MagOfTT.aspx?ID_Teletrack={0}"
          HeaderText="Действия" NavigateUrl="~/tech/MagOfTT.aspx" Text="Журнал" />
      </Columns>
      <PagerStyle BackColor="#507CD1" ForeColor="White" HorizontalAlign="Center" />
      <HeaderStyle BackColor="#507CD1" ForeColor="White" />
    </asp:GridView>
    <table style="width: 100%">
      <tr>
        <td align="right" colspan="2" style="width: 50%; height: 18px; text-align: left">
        </td>
        <td align="right" colspan="2" rowspan="1" style="width: 50%; height: 18px; text-align: right">
          <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Verdana" Font-Size="Small"
            NavigateUrl="~/tech/default.aspx" Width="172px">Пользователь >></asp:HyperLink>
        </td>
      </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSourceTTView" runat="server" SelectCommand="st_SelectTeletrackOfUser"
      SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:OnLineServiceDBConnectionString %>">
      <SelectParameters>
        <asp:QueryStringParameter Name="id_user" QueryStringField="id_user" />
        <asp:QueryStringParameter Name="Id_tltr" QueryStringField="Id_Tltr" DefaultValue="0" />
      </SelectParameters>
    </asp:SqlDataSource>
  </div>
</asp:Content>
