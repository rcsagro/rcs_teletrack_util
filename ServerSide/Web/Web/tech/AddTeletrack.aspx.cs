﻿using System;
using System.Web.UI.WebControls;
using RCS.Web.BLL;
using RCS.Web.Entities;

namespace RCS.Web.UI.Tech
{
  public partial class AddTeletrack : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        LicInfo lic = TeletrackInfoBL.GetLicInfo();
        if (lic.LicenseViolationOccurred || lic.LicenseExhausted)
        {
          const string warning =
            "Новый телетрек будет сохранен в БД, но не будет обслуживаться, " +
            "т.к. ваша лицензия позволяет обслуживать максимум {0} телетреков " +
            "(количество телетреков в базе данных: {1}).";

          lblLicWarning.Text = String.Format(warning, lic.MaxPermittedTtCount, lic.TtCount);
          lblLicWarning.Visible = true;
        }
      }
    }

    protected void DetailsView_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
      if (e.CancelingEdit)
      {
        TeletrackViewRedirect();
      }
    }

    protected void DetailsView_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
      if (e.Exception == null)
      {
        object obj = Session[SessionKeyNames.LAST_INSERTED_TT_ID];
        if (obj == null)
        {
          TeletrackViewRedirect();
        }
        else
        {
          Session.Remove(SessionKeyNames.LAST_INSERTED_TT_ID);
          Response.Redirect(String.Format(
            "~/tech/EditTeletrack.aspx?ID_Teletrack={0}&ID_User={1}",
            obj, Request["id_user"]));
        }
      }
    }

    private void TeletrackViewRedirect()
    {
      Response.Redirect(String.Format(
        "~/tech/TeletrackView2.aspx?ID_User={0}", Request["id_user"]));
    }

    protected void objectDataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
      // У TeletrackInfo не установлен ID_User.
      SetIdUserValue((TeletrackInfo)e.InputParameters[0]);
    }

    protected void txtTimeout_Init(object sender, EventArgs e)
    {
      SetTimeoutDefaultValue((TextBox)sender);
    }

    /// <summary>
    /// Установка дефолтного значения полю.
    /// </summary>
    /// <param name="textBox">Поле редактирования.</param>
    private void SetTimeoutDefaultValue(TextBox textBox)
    {
      textBox.Text = "1440";
    }

    /// <summary>
    /// Установка значения ID_User.
    /// </summary>
    /// <param name="teletrackInfo">TeletrackInfo.</param>
    private void SetIdUserValue(TeletrackInfo teletrackInfo)
    {
      teletrackInfo.ID_User = Convert.ToInt32(Request["id_user"]);
    }

    protected void validatorUniqueLogin_ServerValidate(object source, ServerValidateEventArgs args)
    {
      if (TeletrackInfoBL.TeletrackExists(args.Value))
      {
        args.IsValid = false;
      }
    }
  }
}