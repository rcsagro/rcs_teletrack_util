﻿using System;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech
{
  public partial class MasterPage : System.Web.UI.MasterPage
  {
    /// <summary>
    /// Имя Cookie - RCSTeleSrvPageInfo.
    /// </summary>
    private const string COOKIE_NAME = "RCSTeleSrvPageInfo";
    /// <summary>
    /// Наименование параметра для хранения кол-ва 
    /// отображаемых строк - RowCount. 
    /// </summary>
    private const string ROW_COUNT_KEY = "RowCount";

    protected void Page_Load(object sender, EventArgs e)
    {
      SetNoCache();

      if (AppSettings.RestrictAccess)
      {
        SignOut();
      }

      FillLastDataInfo();
    }

    private void FillLastDataInfo()
    {
      DateTime dtLast = StatisticsBL.GetLastTeletrackConnection();

      if (dtLast == DateTime.MinValue)
      {
        lbLastConnect.Visible = false;
        lbDelta.Text = "Нет данных";
      }
      else
      {
        TimeSpan dTime = DateTime.Now.Subtract(dtLast);
        TimeSpan tsLast = DateTime.Now.AddDays(-dTime.Days).Subtract(dtLast);

        lbLastConnect.Text = String.Format("Последние данные получены {0}", dtLast);
        lbDelta.Text = String.Format("Дельта {0} дн. {1}",
          Convert.ToString(dTime.Days), Convert.ToString(tsLast).Substring(0, 8));

        lbDelta.ForeColor = dTime.Minutes >= 5 || dTime.Days > 0 || dTime.Hours > 0 ?
          Color.Red : Color.Green;
      }
    }

    /// <summary>
    /// Установка директив запрета кэширования страниц
    /// на стороне клиента.
    /// </summary>
    private void SetNoCache()
    {
      Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1));
      Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
      Response.Cache.SetCacheability(HttpCacheability.NoCache);
      Response.Cache.SetNoStore();
      //Response.AppendHeader("Last-Modified", "-1");
      //Response.AppendHeader("Expires", "-1"); //Mon, 11 May 1998 00:00:00 GMT
      //Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate, post-check=0, pre-check=0");
      //Response.AddHeader("Pragma", "no-cache");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
      InitRowCount();
    }

    /// <summary>
    /// Инициализация контрола, отображающего кол-во строк.
    /// </summary>
    private void InitRowCount()
    {
      // Вычитываем из Cookie кол-во отображаемых строк
      if (Request.Cookies[COOKIE_NAME] != null)
      {
        int tmp;
        if (!Int32.TryParse(
          Server.HtmlEncode(Request.Cookies[COOKIE_NAME][ROW_COUNT_KEY]),
          out tmp))
        {
          tmp = 50;
          SaveRowCountCookie(tmp);
        }

        txPage.Text = tmp.ToString();
      }
    }

    private void SignOut()
    {
      FormsAuthentication.SignOut();
      FormsAuthentication.RedirectToLoginPage();
    }

    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    {
      if (e.Item.Text == "Выход")
      {
        SignOut();
      }
    }

    protected void txPage_TextChanged(object sender, EventArgs e)
    {
      if (ValidatetxPage())
      {
        SaveRowCountCookie(Int32.Parse(txPage.Text));
      }
      else
      {
        txPage.Focus();
      }
    }

    private bool ValidatetxPage()
    {
      validatorGtZerro.Validate();
      if (!validatorGtZerro.IsValid)
      {
        return false;
      }
      validatorDigits.Validate();
      if (!validatorDigits.IsValid)
      {
        return false;
      }
      validatorRequired.Validate();
      if (!validatorRequired.IsValid)
      {
        return false;
      }
      return true;
    }

    /// <summary>
    /// Сохраняем в Cookie кол-во отображаемых строк.
    /// </summary>
    /// <param name="value">Кол-во строк.</param>
    private void SaveRowCountCookie(int value)
    {
      Response.Cookies[COOKIE_NAME][ROW_COUNT_KEY] = value.ToString();
      Response.Cookies[COOKIE_NAME].Expires = DateTime.Now.AddYears(1);
    }

    /// <summary>
    /// Установка свойства PageSize для GridView.
    /// </summary>
    /// <param name="gridView">GridView.</param>
    public void SetGridViewPageSize(GridView gridView)
    {
      if (ValidatetxPage())
      {
        gridView.PageSize = Convert.ToInt32(txPage.Text);
      }
      else
      {
        txPage.Focus();
      }
    }
  }
}