﻿using System;
using System.Collections.Generic;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using RCS.Web.BLL;
using RCS.Web.Entities;

namespace RCS.Web.UI.Tech
{
  public partial class EditTeletrack : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gridDetails);
      MaintainScrollPositionOnPostBack = true;
      linkTeletracks.NavigateUrl = GetTeletrackViewUrl();
      linkUser.NavigateUrl = GetUserViewUrl();
    }

    private string GetTeletrackViewUrl()
    {
      return string.Format(
        "~/tech/TeletrackView2.aspx?ID_User={0}", Request["id_user"]);
    }

    private string GetUserViewUrl()
    {
      return string.Format(
        "~/tech/UserView.aspx?ID_User={0}", Request["id_user"]);
    }

    protected void ObjectDataSource_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
      if (e.Exception == null)
      {
        Response.Redirect(GetTeletrackViewUrl());
      }
    }

    protected void btSetAll_Click(object sender, EventArgs e)
    {
        UserInfoBL.SetAllDicpatchLinkedToTeletrack(Convert.ToInt32(Request["id_user"]), Convert.ToInt32(Request["id_teletrack"]));
        gridDetails.DataBind();
    }

    protected void btResetAll_Click(object sender, EventArgs e)
    {
        UserInfoBL.ResetAllDicpatchLinkedToTeletrack(Convert.ToInt32(Request["id_user"]), Convert.ToInt32(Request["id_teletrack"]));
        gridDetails.DataBind();
    }
}
}