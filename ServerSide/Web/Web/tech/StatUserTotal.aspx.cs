using System;
using System.Web.UI.WebControls;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech
{
  public partial class StatUserTotal : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gvWork);
      if (!IsPostBack)
      {
        txCritKb.Text = AuthenticationHelper.ParamRead(1, "10");
        txCritDay.Text = AuthenticationHelper.ParamRead(2, "20");
      }
    }

    protected void gvWork_DataBound(object sender, EventArgs e)
    {
      lbTot.Text = Convert.ToString(StatisticsBL.Statistics.TtCount);
      lbAct.Text = Convert.ToString(StatisticsBL.Statistics.ActiveTtCount);
      lbNAct.Text = Convert.ToString(StatisticsBL.Statistics.TtCount -
        StatisticsBL.Statistics.ActiveTtCount);
      lbSumKb.Text = Convert.ToString(StatisticsBL.Statistics.TtTrafficKb);
    }

    protected void gvWork_RowCommand(object sender, GridViewCommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "Move":
          Response.Redirect(String.Format(
            "~/tech/StatUserDetal.aspx?UserId={0}&SelectMonth={1}",
            e.CommandArgument, rbMonthSelect.SelectedValue));
          break;
      }
    }

    protected void txCritKb_TextChanged(object sender, EventArgs e)
    {
      AuthenticationHelper.ParamWrite(1, txCritKb.Text);
    }

    protected void txCritDay_TextChanged(object sender, EventArgs e)
    {
      AuthenticationHelper.ParamWrite(2, txCritDay.Text);
    }
  }
}