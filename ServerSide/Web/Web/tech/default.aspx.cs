﻿using System;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech
{
  public partial class Default : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(GridView1);
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
      int i = 0;
      i++;
    }

    protected void AddUserButton_Click(object sender, EventArgs e)
    {
      GridView1.ShowFooter = true;
    }

    protected void lnGoTelet_Click(object sender, EventArgs e)
    {
      txSeek.Text = txSeek.Text.Trim();

      if (txSeek.Text.Length == 0)
      {
        Response.Write("<script>alert('Укажите номер телетрека!')</script>");
      }
      else
      {
        int? ID_tltr = TeletrackInfoBL.SelectTeletrackIDByLogin(txSeek.Text);
        if (ID_tltr.HasValue)
        {
          Response.Redirect(String.Format(
            "~/tech/TeletrackView.aspx?ID_User=0&Id_Tltr={0}", ID_tltr));
        }
        else
        {
          Response.Write(String.Format(
            "<script>alert('Телетрек с логином {0} не найден в базе')</script>", txSeek.Text));
        }
      }
    }
  }
}