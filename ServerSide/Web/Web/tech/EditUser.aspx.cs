﻿using System;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech
{
  public partial class EditUser : System.Web.UI.Page
  {
    int id_user = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
      int id;
      if (int.TryParse(Request["id_user"].ToString(), out id))
      {
        id_user = id;
        if (!IsPostBack)
        {
          UserInfoBL usBL = new UserInfoBL();
          int kodB;
          string usName;
          string descr;
          bool isActState;
          int retPeriod;
          string dealer;

          UserInfoBL.GetUserInfoByID(id_user,
            out kodB, out usName, out descr,
            out isActState, out retPeriod, out dealer);

          codB.Text = kodB.ToString();
          nameTextBox.Text = usName;
          descrTextBox.Text = descr;
          int sel = 0;
          if (!isActState)
          {
            sel = 1;
          }
          isActiveRadioBtn.SelectedIndex = sel;
          retPeriodDropDownList.Text = retPeriod.ToString();

          DealerDropDownList.Text = dealer;
        }
      }
    }

    protected void CanselButton_Click(object sender, EventArgs e)
    {
      Redirect();
    }

    protected void SaveButton1_Click(object sender, EventArgs e)
    {
      int idB;
      if (int.TryParse(codB.Text, out idB))
      {
        bool isActState = false;
        if (isActiveRadioBtn.SelectedIndex == 0)
        {
          isActState = true;
        }
        string dealerName = DealerDropDownList.Text;
        if (dealerName == "нет")
        {
          dealerName = "";
        }

        UserInfoBL.UpdateUserInfo(
          idB, nameTextBox.Text,
          descrTextBox.Text, isActState,
          int.Parse(retPeriodDropDownList.Text),
          dealerName, id_user);

        Redirect();
      }
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
      SaveButton1.Visible = true; CanselButton.Visible = true;
      Button3.Visible = false; Button4.Visible = false;
      ValidationSummary2.Enabled = true;
      CompareValidator1.Enabled = true;
      RequiredFieldValidator1.Enabled = true;
      RequiredFieldValidator3.Enabled = true;

      isActiveRadioBtn.Enabled = true;
      retPeriodDropDownList.Enabled = true;
      DealerDropDownList.Enabled = true;
      codB.ReadOnly = false;
      nameTextBox.ReadOnly = false;
      descrTextBox.ReadOnly = false;
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
      UserInfoBL.DeleteUserInfo(id_user);
      Redirect();
    }

    private void Redirect()
    {
      Response.Redirect("~/tech/UserView.aspx");
    }
  }
}