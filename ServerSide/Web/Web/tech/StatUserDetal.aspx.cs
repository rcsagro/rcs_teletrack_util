using System;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech
{
  public partial class StatUserDetal : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gvWork);
      if (!IsPostBack)
      {
        txCritKb.Text = AuthenticationHelper.ParamRead(1, "10");
        txCritDay.Text = AuthenticationHelper.ParamRead(2, "20");
        if (Request.QueryString.Get("UserId") != null)
        {
          dlClient.DataBind();
          dlClient.SelectedValue = Request.QueryString.Get("UserId");
          dlClient_SelectedIndexChanged(sender, e);
          gvWork.DataBind();
          if (Request.QueryString.Get("SelectMonth") != null)
          {
            rbMonthSelect.SelectedValue = Request.QueryString.Get("SelectMonth");
          }
        }
      }
    }

    protected void gvWork_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        int intdex = e.Row.RowIndex;
        if (intdex >= 0)
        {
          DataRowView rowView = (DataRowView)e.Row.DataItem;
          if (rowView != null)
          {
            int iActDays = (int)rowView.Row.ItemArray[5];
            if (iActDays < Convert.ToInt32(txCritDay.Text))
            {
              e.Row.BackColor = Color.MistyRose;
            }
          }
        }
      }
    }

    protected void gvWork_DataBound(object sender, EventArgs e)
    {
      lbTot.Text = Convert.ToString(StatisticsBL.Statistics.TtCount);
      lbAct.Text = Convert.ToString(StatisticsBL.Statistics.ActiveTtCount);
      lbNAct.Text = Convert.ToString(StatisticsBL.Statistics.TtCount - StatisticsBL.Statistics.ActiveTtCount);
      lbSumKb.Text = Convert.ToString(StatisticsBL.Statistics.TtTrafficKb);
    }

    protected void gvWork_RowCommand(object sender, GridViewCommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "Move":
          Response.Redirect(String.Format(
            "~/tech/StatTT.aspx?TltrId={0}&SelectMonth={1}",
            e.CommandArgument, rbMonthSelect.SelectedValue));
          break;
      }
    }

    protected void txCritKb_TextChanged(object sender, EventArgs e)
    {
      AuthenticationHelper.ParamWrite(1, txCritKb.Text);
    }

    protected void txCritDay_TextChanged(object sender, EventArgs e)
    {
      AuthenticationHelper.ParamWrite(2, txCritDay.Text);
    }

    protected void dlClient_SelectedIndexChanged(object sender, EventArgs e)
    {
      lbDealer.Text = DealerInfoBL.GetNameByUserID(
        Convert.ToInt32(dlClient.SelectedValue));
    }
  }
}