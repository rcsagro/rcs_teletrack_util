﻿using System;

namespace RCS.Web.UI.Tech
{
  public partial class DispetchView : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        HyperLink2.NavigateUrl += String.Format("?ID_User={0}", Request["id_user"]);
        HyperLink3.NavigateUrl += String.Format("?ID_User={0}", Request["id_user"]);
      }

      ((MasterPage)Page.Master).SetGridViewPageSize(GridView1);
    }
  }
}