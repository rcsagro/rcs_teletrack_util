﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" EnableSessionState="False" CodeFile="TeletrackView2.aspx.cs"
  Inherits="RCS.Web.UI.Tech.TeletrackView2" Title="Телетреки" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div style="width: 100%; height: 100%; text-align: center;">
    <strong>Телетреки&nbsp;<br />
    </strong>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
      AutoGenerateColumns="False" DataKeyNames="ID_Teletrack" DataSourceID="SqlDataSourceTTView2"
      Font-Names="Verdana" Font-Size="Small" HorizontalAlign="Center" Width="100%" OnRowDataBound="GridView1_RowDataBound"
      EmptyDataText="На данного пользователя незарегистрировано ни одного телетрека."
      CellPadding="4">
      <FooterStyle BackColor="#507CD1" />
      <RowStyle BackColor="PaleGreen" />
      <Columns>
        <asp:TemplateField HeaderText="№п/п">
          <HeaderStyle Width="80px" />
          <ItemTemplate>
            <%# Convert.ToString( Container.DataItemIndex + 1 ) %>
          </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="LoginTT" HeaderText="Логин" SortExpression="LoginTT" />
        <asp:BoundField DataField="PasswordTT" HeaderText="Пароль" SortExpression="PasswordTT" />
        <asp:BoundField DataField="LastConn" HeaderText="Последнее соединение" ReadOnly="True"
          SortExpression="LastConn" />
        <asp:BoundField DataField="Delta" HeaderText="Дельта" ReadOnly="True" SortExpression="Delta" />
        <asp:BoundField DataField="State" HeaderText="Состояние" ReadOnly="True" SortExpression="State" />
        <asp:BoundField DataField="Timeout" HeaderText="Таймаут(мин.)" SortExpression="Timeout" />
        <asp:BoundField DataField="UserName" HeaderText="Пользователь" SortExpression="UserName" />
        <asp:TemplateField HeaderText="Действия">
          <ItemTemplate>
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Eval("ID_Teletrack", "~/tech/EditTeletrack.aspx?ID_Teletrack={0}") + string.Format("&ID_User={0}",Request.QueryString["ID_User"]) %>'
              Target="_parent" Text="Редактировать">
            </asp:HyperLink>
          </ItemTemplate>
        </asp:TemplateField>
      </Columns>
      <PagerStyle BackColor="#507CD1" ForeColor="White" HorizontalAlign="Center" />
      <HeaderStyle BackColor="#507CD1" ForeColor="White" />
      <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
        NextPageText="&gt;" PreviousPageText="&lt;" />
    </asp:GridView>
    <table style="width: 100%">
      <tr>
        <td align="right" colspan="2" style="width: 50%; text-align: left; height: 20px;">
          <asp:HyperLink ID="HyperLink1" runat="server" Font-Names="Verdana" Font-Size="Small"
            NavigateUrl="~/tech/AddTeletrack.aspx">Добавить телетрек</asp:HyperLink>
        </td>
        <td align="right" colspan="2" style="width: 50%; text-align: right; height: 20px;"
          rowspan="">
          <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Verdana" Font-Size="Small"
            NavigateUrl="~/tech/UserView.aspx">Пользователь >></asp:HyperLink>
        </td>
      </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSourceTTView2" runat="server" ConnectionString="<%$ ConnectionStrings:OnLineServiceDBConnectionString %>"
      SelectCommand="st_SelectTeletrackOfUser" SelectCommandType="StoredProcedure">
      <SelectParameters>
        <asp:QueryStringParameter Name="id_user" QueryStringField="id_user" />
      </SelectParameters>
    </asp:SqlDataSource>
  </div>
</asp:Content>
