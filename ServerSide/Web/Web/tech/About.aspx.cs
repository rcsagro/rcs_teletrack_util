using System;
using System.Drawing;
using RCS.Web.BLL;
using RCS.Web.Entities;

namespace RCS.Web.UI.Tech
{
  public partial class About : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      lblVerSite.Text = VersionInfoBL.SiteVersion;
      lblVerDb.Text = VersionInfoBL.DbVersion;

      LicInfo lic = VersionInfoBL.Lic;
      lblbTtCount.Text = lic.TtCount.ToString();

      if (!String.IsNullOrEmpty(lic.Description))
      {
        LblInfo.Text = lic.Description;
      }

      if (lic.LicenseViolationOccurred)
      {
        lblbTtCount.ForeColor = Color.Red;
        LblInfo.ForeColor = Color.Red;
      }
    }
  }
}