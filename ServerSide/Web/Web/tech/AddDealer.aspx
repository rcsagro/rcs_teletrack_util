﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  CodeFile="AddDealer.aspx.cs" Inherits="RCS.Web.UI.Tech.AddDealer" Title="Добавление дилера" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td style="width: 50%; height: 30px;">
      </td>
      <td style="text-align: center; height: 30px;">
        <strong>Добавление нового дилера</strong>
      </td>
      <td style="width: 50%; height: 30px;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%; height: 129px">
      </td>
      <td style="height: 129px">
        <table style="width: 400px">
          <tr>
            <td style="width: 100px">
              Логин
            </td>
            <td style="width: 187px">
              <asp:TextBox ID="loginTextBox" runat="server"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="loginTextBox"
                ErrorMessage="Поле Login не должно быть пустым" SetFocusOnError="True">*</asp:RequiredFieldValidator></td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px">
            </td>
            <td style="width: 187px">
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px">
              Пароль
            </td>
            <td style="width: 187px">
              <asp:TextBox ID="passTextBox" runat="server"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="passTextBox"
                ErrorMessage="Поле Пароль не должно быть пустым" SetFocusOnError="True">*</asp:RequiredFieldValidator></td>
          </tr>
          <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 187px">
            </td>
          </tr>
          <tr>
            <td style="width: 100px; height: 24px">
              Описание
            </td>
            <td style="width: 187px; height: 24px">
              <asp:TextBox ID="descrTextBox" runat="server"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="descrTextBox"
                ErrorMessage="Необходимо задать описание">*</asp:RequiredFieldValidator>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%; height: 129px">
      </td>
    </tr>
    <tr>
      <td style="width: 50%;">
      </td>
      <td>
        <table width="400">
          <tr>
            <td style="width: 50%; height: 23px" align="center">
              <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Добавить" />
            </td>
            <td style="width: 50%; height: 23px;" align="center">
              <asp:Button ID="Button2" runat="server" Text="Отменить" OnClick="Button2_Click" CausesValidation="False" />
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%;">
      </td>
      <td>
        <asp:ValidationSummary ID="ValidationSummary2" runat="server" />
      </td>
      <td style="width: 50%;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%;">
      </td>
      <td>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Дилер с таким именем(Логином) уже существует!"
          Visible="False"></asp:Label>
      </td>
      <td style="width: 50%;">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" InsertMethod="InsertDealerInfo"
    TypeName="RCS.Web.BLL.DealerInfoBL">
    <DeleteParameters>
      <asp:Parameter Name="id_dealer" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
      <asp:Parameter Name="login" Type="String" />
      <asp:Parameter Name="pass" Type="String" />
      <asp:Parameter Name="descr" Type="String" />
      <asp:Parameter Name="id" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
      <asp:Parameter Name="login" Type="String" />
      <asp:Parameter Name="pass" Type="String" />
      <asp:Parameter Name="descr" Type="String" />
      <asp:Parameter Name="id_role" Type="Int32" />
      <asp:Parameter Name="isDealerNameExist" Type="Boolean" />
    </InsertParameters>
  </asp:ObjectDataSource>
</asp:Content>
