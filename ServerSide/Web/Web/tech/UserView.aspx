<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  EnableViewState="false" EnableSessionState="False" CodeFile="UserView.aspx.cs"
  Inherits="RCS.Web.UI.Tech.UserView2" Title="������������������" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div id="DIV1" style="width: 100%; height: 100%; text-align: center">
    <strong>������������<br />
      <br />
    </strong>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
      AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id_user" DataSourceID="ObjectDataSource"
      EmptyDataText="��� ������ ��� �����������." Font-Names="Verdana" Font-Size="Small"
      ForeColor="#333333" HorizontalAlign="Center" Width="100%">
      <PagerSettings FirstPageText="&lt;&lt;" LastPageText="&gt;&gt;" Mode="NumericFirstLast"
        NextPageText="&lt;" PreviousPageText="&gt;" />
      <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <RowStyle BackColor="#EFF3FB" />
      <Columns>
        <asp:TemplateField HeaderText="��/�">
          <ItemTemplate>
            <%# Convert.ToString( Container.DataItemIndex + 1 ) %>
          </ItemTemplate>
          <HeaderStyle Width="80px" />
        </asp:TemplateField>
        <asp:BoundField DataField="Kod_b" HeaderText="�������" SortExpression="Kod_b" />
        <asp:BoundField DataField="Name" HeaderText="������������" SortExpression="Name" />
        <asp:TemplateField HeaderText="���������" SortExpression="IsActiveState">
          <EditItemTemplate>
            <asp:Label ID="Label1" runat="server" Text='<%#  Eval("IsActiveState") %>'></asp:Label>
          </EditItemTemplate>
          <ItemTemplate>
            <asp:Label ID="Label1" runat="server" Text='<%#((bool)DataBinder.Eval(Container.DataItem, "IsActiveState") == true) ? "�������" : "������������" %>'></asp:Label>
          </ItemTemplate>
        </asp:TemplateField>
        <asp:HyperLinkField DataNavigateUrlFields="ID_User" DataNavigateUrlFormatString="dispetchView2.aspx?ID_User={0}"
          HeaderText="����������" NavigateUrl="~/tech/DispetchView2.aspx" Target="_parent"
          Text="����������" />
        <asp:HyperLinkField DataNavigateUrlFields="ID_User" DataNavigateUrlFormatString="TeletrackView2.aspx?ID_User={0}"
          HeaderText="���������" NavigateUrl="~/tech/TeletrackView2.aspx" Target="_parent"
          Text="���������" />
        <asp:BoundField DataField="Retention_Period" HeaderText="T_��������" SortExpression="Retention_Period" />
        <asp:BoundField DataField="Describe" HeaderText="��������" SortExpression="Describe" />
        <asp:TemplateField>
          <ItemTemplate>
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Eval("ID_User", "~/tech/EditUser.aspx?ID_User={0}") %>'
              Target="_parent" Text="���."></asp:HyperLink>
          </ItemTemplate>
        </asp:TemplateField>
      </Columns>
      <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
      <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
      <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
      <EditRowStyle BackColor="#2461BF" />
      <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <table style="width: 100%">
      <tr>
        <td align="right" colspan="2" style="width: 50%; text-align: left">
          <asp:HyperLink ID="HyperLink1" runat="server" Font-Names="Verdana" Font-Size="Small"
            NavigateUrl="~/tech/AddUser.aspx">�������� ������������</asp:HyperLink>
        </td>
        <td align="right" colspan="2" rowspan="1" style="width: 50%; text-align: right">
          <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Verdana" Font-Size="Small"
            NavigateUrl="~/tech/UserView.aspx?ID_User=0">��� ������������ >></asp:HyperLink>
        </td>
      </tr>
    </table>
    <asp:ObjectDataSource ID="ObjectDataSource" runat="server" SelectMethod="GetUserInfo"
      TypeName="RCS.Web.BLL.UserInfoBL" OldValuesParameterFormatString="original_{0}">
      <SelectParameters>
        <asp:QueryStringParameter DefaultValue="0" Name="id_user" QueryStringField="id_user"
          Type="Int32" />
      </SelectParameters>
    </asp:ObjectDataSource>
  </div>
</asp:Content>
