﻿<%@ Page Language="C#" MasterPageFile="~/tech/MasterPage.master" AutoEventWireup="true"
  CodeFile="EditUser.aspx.cs" Inherits="RCS.Web.UI.Tech.EditUser" Title="Редактирование записи пользователя" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <table style="width: 100%">
    <tr>
      <td style="width: 50%; height: 30px;">
      </td>
      <td style="height: 30px; text-align: center">
        <strong>Редактирование пользователя</strong>
      </td>
      <td style="width: 50%; height: 30px;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%; height: 326px;">
      </td>
      <td style="height: 326px">
        <table width="400">
          <tr>
            <td style="width: 100px; height: 26px">
              КодБазы
            </td>
            <td style="width: 207px; height: 26px">
              <asp:TextBox ID="codB" runat="server" ReadOnly="True" Width="178px"></asp:TextBox>
              <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="codB"
                ErrorMessage="Неверный формат для поля КодБазы" Operator="DataTypeCheck" SetFocusOnError="True"
                Type="Integer">*</asp:CompareValidator>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px">
            </td>
            <td style="width: 207px">
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px">
              Название
            </td>
            <td style="width: 207px">
              <asp:TextBox ID="nameTextBox" runat="server" ReadOnly="True" Width="179px"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="nameTextBox"
                ErrorMessage="Поле Login не должно быть пустым" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px">
            </td>
            <td style="width: 207px">
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px; height: 24px">
              Описание
            </td>
            <td style="width: 207px; height: 24px">
              <asp:TextBox ID="descrTextBox" runat="server" ReadOnly="True" Width="180px"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="descrTextBox"
                ErrorMessage="Необходимо задать описание">*</asp:RequiredFieldValidator>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px; height: 72px">
              Состояние
            </td>
            <td style="width: 207px; height: 72px;">
              <br />
              <asp:RadioButtonList ID="isActiveRadioBtn" runat="server" Enabled="False" Width="185px">
                <asp:ListItem Selected="True" Value="true">Активен </asp:ListItem>
                <asp:ListItem Value="false">Заблокирован</asp:ListItem>
              </asp:RadioButtonList>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px; height: 24px">
              Т_Хранения
            </td>
            <td style="width: 207px; height: 24px">
              <asp:DropDownList ID="retPeriodDropDownList" runat="server" Enabled="False" Width="142px">
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem Selected="True">2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem>5</asp:ListItem>
                <asp:ListItem>6</asp:ListItem>
              </asp:DropDownList>
              <asp:Label ID="Label5" runat="server" Text="(мес.)"></asp:Label>
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px; height: 24px">
            </td>
            <td style="width: 207px; height: 24px">
            </td>
          </tr>
          <tr style="color: #000000">
            <td style="width: 100px; height: 24px">
              Дилер
            </td>
            <td style="width: 207px; height: 24px">
              <asp:DropDownList ID="DealerDropDownList" runat="server" DataSourceID="ObjectDataSource2"
                Enabled="False" Width="185px">
              </asp:DropDownList>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%; height: 326px;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%; height: 1px;">
      </td>
      <td>
        <table width="400" id="tableBtnEdit">
          <tr>
            <td style="width: 50%; height: 26px;" align="center">
              <asp:Button ID="Button3" runat="server" Text="Изменить" OnClick="Button3_Click" Width="90px" />
            </td>
            <td style="width: 50%; height: 26px;" align="center">
              <asp:Button ID="Button4" runat="server" Text="Удалить" OnClick="Button4_Click" OnClientClick="return confirm ('Вы действительно хотите удалить эту запись?')"
                Width="90px" />
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%; height: 1px;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%; height: 50px;">
      </td>
      <td colspan="1" rowspan="1">
        <table id="tableBtnSave" width="400">
          <tr>
            <td style="width: 50%; height: 26px;" align="center">
              <asp:Button ID="SaveButton1" runat="server" OnClick="SaveButton1_Click" Text="Сохранить"
                Visible="False" Width="90px" />
            </td>
            <td style="width: 50%; height: 26px;" align="center">
              <asp:Button ID="CanselButton" runat="server" OnClick="CanselButton_Click" Text="Отменить"
                Visible="False" CausesValidation="False" Width="90px" />
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%; height: 50px;">
      </td>
    </tr>
    <tr>
      <td style="width: 50%">
      </td>
      <td>
        <asp:ValidationSummary ID="ValidationSummary2" runat="server" />
      </td>
      <td style="width: 50%">
      </td>
    </tr>
  </table>
  <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetListOfDealers"
    TypeName="RCS.Web.BLL.DealerInfoBL"></asp:ObjectDataSource>
</asp:Content>
