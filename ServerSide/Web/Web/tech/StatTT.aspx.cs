using System;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;
using RCS.Web.BLL;

namespace RCS.Web.UI.Tech
{
  public partial class StatTT : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ((MasterPage)Page.Master).SetGridViewPageSize(gvWork);
      if (!IsPostBack)
      {
        txCritKb.Text = AuthenticationHelper.ParamRead(1, "10");
        txCritDay.Text = AuthenticationHelper.ParamRead(2, "20");
        if (Request.QueryString.Get("TltrId") != null)
        {
          dlTltr.DataBind();
          dlTltr.SelectedValue = Request.QueryString.Get("TltrId");
          dlTltr_SelectedIndexChanged(sender, e);
          gvWork.DataBind();
        }
        if (Request.QueryString.Get("SelectMonth") != null)
        {
          rbMonthSelect.SelectedValue = Request.QueryString.Get("SelectMonth");
        }
      }
    }

    protected void gvWork_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        int intdex = e.Row.RowIndex;
        if (intdex >= 0)
        {
          DataRowView rowView = (DataRowView)e.Row.DataItem;
          if (rowView != null)
          {
            string str = (string)rowView.Row.ItemArray[5];
            if (str == "���")
            {
              e.Row.BackColor = Color.MistyRose;
            }
          }
        }
      }
    }

    protected void gvWork_DataBound(object sender, EventArgs e)
    {
      DateTime dtWork = DateTime.Now;
      if (rbMonthSelect.SelectedValue != "0")
      {
        dtWork = DateTime.Now.AddMonths(-1);
      }
      int iMonthDays = DateTime.DaysInMonth(dtWork.Year, dtWork.Month);
      lbTot.Text = Convert.ToString(iMonthDays);
      lbAct.Text = Convert.ToString(StatisticsBL.Statistics.ActiveTtCount);
      lbNAct.Text = Convert.ToString(iMonthDays - StatisticsBL.Statistics.ActiveTtCount);

      lbSumKb.Text = Convert.ToString(StatisticsBL.Statistics.TtTrafficKb);
      if (StatisticsBL.Statistics.BActive)
      {
        lbActiv.Text = "�������";
        lbActiv.ForeColor = Color.Green;
      }
      else
      {
        lbActiv.Text = "�� �������";
        lbActiv.ForeColor = Color.Red;
      }
    }

    protected void dlTltr_DataBound(object sender, EventArgs e)
    {
      dlTltr_SelectedIndexChanged(sender, e);
    }

    protected void dlTltr_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (dlTltr.SelectedValue.Length > 0)
      {
        DataRow DR = TeletrackInfoBL.SelectTeletrackInfoByID_proc(
          Convert.ToInt32(dlTltr.SelectedValue));
        if ((string)DR["LastConn"] != "��� ������")
        {
          lbLastCon.Text = String.Format("��������� ���������� {0}",
            Convert.ToString(DR["LastConn"]).Substring(0, 19));
          lbDelta.Text = String.Format("������ {0}", DR["Delta"]);

          if ((string)DR["State"] == "CRITICAL")
          {
            lbDelta.ForeColor = Color.Red;
          }
          else
          {
            lbDelta.ForeColor = Color.Green;
          }
        }
        else
        {
          lbLastCon.Text = "��� ������ � ��������� ���������� ";
          lbDelta.Visible = false;
        }
        lbClient.Text = (string)DR["Name"];
      }
    }

    protected void txCritKb_TextChanged(object sender, EventArgs e)
    {
      AuthenticationHelper.ParamWrite(1, txCritKb.Text);
    }

    protected void txCritDay_TextChanged(object sender, EventArgs e)
    {
      AuthenticationHelper.ParamWrite(2, txCritDay.Text);
    }
  }
}