﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;

namespace RCS.Web.UI.Tech
{
  public partial class TeletrackView : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        HyperLink2.NavigateUrl += string.Format(
          "?ID_User={0}", Request["id_user"]);
      }
      ((MasterPage)Page.Master).SetGridViewPageSize(GridView1);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        int intdex = e.Row.RowIndex;
        if (intdex >= 0)
        {
          DataRowView rowView = (DataRowView)e.Row.DataItem;
          if (rowView != null)
          {
            string str = (string)rowView.Row.ItemArray[5];
            if (str == "CRITICAL")
            {
              e.Row.BackColor = Color.MistyRose;
            }
          }
        }
      }

    }
  }
}