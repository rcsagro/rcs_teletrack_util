using System.Drawing;
using System.Web.UI.WebControls;
using System.Collections.Generic;

namespace RCS.Web.BLL.Services
{
  /// <summary>
  /// ������ � GridView.
  /// </summary>
  public static class GridViewHelper
  {
    /// <summary>
    /// ��������� ��������� �������� ���������� �������� 
    /// �� ���� ������� �����.
    /// ������� ������� ������ ��������� � TemplateField.
    /// </summary>
    /// <param name="grid">GridView.</param>
    /// <param name="checkBoxName">������������(ID) �������� � TemplateField ������ �����.</param>
    /// <param name="value">True or False.</param>
    public static void SetCheckBoxField(GridView grid, string checkBoxName, bool value)
    {
      foreach (GridViewRow row in grid.Rows)
      {
        CheckBox chBox = row.FindControl(checkBoxName) as CheckBox;

        if ((chBox != null) && (chBox.Checked != value))
        {
          chBox.Checked = value;
        }
      }
    }

    /// <summary>
    /// ��������� ����� ���� ����� ����� � ����������� �� ��������� 
    /// ���������� ��������.
    /// ������� ������� ������ ��������� � TemplateField.
    /// </summary>
    /// <param name="grid">GridView.</param>
    /// <param name="checkBoxName">������������(ID) �������� � TemplateField ������ �����.</param>
    /// <param name="checkedColor">���� ���� ������ ��� ������������� ��������.</param>
    /// <param name="uncheckedColor">���� ���� ������ ��� ���������� ��������.</param>
    public static void ColorRowsWithCheckBox(GridView grid, string checkBoxName,
      Color checkedColor, Color uncheckedColor)
    {
      foreach (GridViewRow row in grid.Rows)
      {
        CheckBox chBox = row.FindControl(checkBoxName) as CheckBox;

        if (chBox != null)
        {
          row.BackColor = chBox.Checked ? checkedColor : uncheckedColor;
        }
      }
    }

    /// <summary>
    /// ������������ �������: ������������� �������� ����� - �������� ��������.
    /// </summary>
    /// <param name="grid">GridView.</param>
    /// <param name="keyFieldName">������������ �������������� ��������� ����.</param>
    /// <param name="checkBoxName">������������(ID) �������� � TemplateField ������ �����.</param>
    /// <returns>Dictionary &lt;int, bool&gt;></returns>
    public static Dictionary<int, bool> GetKeyValueDictionary(GridView grid,
      string keyFieldName, string checkBoxName)
    {
      Dictionary<int, bool> result = new Dictionary<int, bool>();

      foreach (GridViewRow row in grid.Rows)
      {
        CheckBox chBox = row.FindControl(checkBoxName) as CheckBox;

        if (chBox != null)
        {
          result.Add(
            (int)grid.DataKeys[row.RowIndex][keyFieldName],
            chBox.Checked);
        }
      }
      return result;
    }
  }
}