using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace RCS.Web.DAL
{
  /// <summary>
  /// AuthenticationHelperProvider
  /// </summary>
  internal class AuthenticationHelperProvider : BaseDAO
  {
    internal string GetRolesForUser(string log, string pass, out int res)
    {
      res = 0;
      object ob;
      StringBuilder strRoles = new StringBuilder("");
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand("dbo.FindDealer", cn))
        {
          cm.CommandTimeout = AppSettings.CommandTimeout;
          cm.CommandType = CommandType.StoredProcedure;
          SqlParameter prmName = new SqlParameter("@DealerName", SqlDbType.NVarChar, 50);
          prmName.Value = log;
          cm.Parameters.Add(prmName);
          SqlParameter prmPass = new SqlParameter("@Pass", SqlDbType.NVarChar, 50);
          prmPass.Value = pass;
          cm.Parameters.Add(prmPass);

          cn.Open();
          ob = cm.ExecuteScalar();
        }
        if (ob != null)
        {
          // ���� ����� � ����� ������ � ������� ����������, �� ���� ��� ����
          res = (int)ob;
          using (SqlCommand cm = new SqlCommand("dbo.FindRoles", cn))
          {
            cm.CommandTimeout = AppSettings.CommandTimeout;
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.AddWithValue("@DealerName", log);
            using (SqlDataReader dr = cm.ExecuteReader())
            {
              // ���������� ������ �����
              while (dr.Read())
              {
                if (strRoles.Length == 0)
                {
                  strRoles.Append(dr.GetSqlString(0).Value);
                }
                else
                {
                  strRoles.Append("|").Append(dr.GetSqlString(0).Value);
                }
              }
            }
          }
        }

      }
      return strRoles.ToString();
    }
  }
}