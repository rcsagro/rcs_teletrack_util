using RCS.Web;
using System.Data.SqlClient;

namespace LocalCashTableAdapters
{
  /// <summary>
  /// ��������� �������� Timeout ��� SqlCommand.
  /// </summary>
  internal static class SqlCmdTimeoutSetter
  {
    internal static void SetTimeout(SqlCommand command)
    {
      if (command != null)
      {
        command.CommandTimeout = AppSettings.CommandTimeout;
      }
    }

    internal static void SetTimeout(SqlCommand[] commands)
    {
      if ((commands == null) || (commands.Length == 0))
      {
        return;
      }

      int commandTimeout = AppSettings.CommandTimeout;
      foreach (SqlCommand command in commands)
      {
        if (command != null)
        {
          command.CommandTimeout = commandTimeout;
        }
      }
    }
  }

  public partial class ClientInfoTableAdapter
  {
    public static ClientInfoTableAdapter Instance
    {
      get
      {
        ClientInfoTableAdapter instance = new ClientInfoTableAdapter();

        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.SelectCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.InsertCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.UpdateCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.DeleteCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.CommandCollection);

        return instance;
      }
    }
  }

  public partial class CommunicTableTableAdapter
  {
    public static CommunicTableTableAdapter Instance
    {
      get
      {
        CommunicTableTableAdapter instance = new CommunicTableTableAdapter();

        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.SelectCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.InsertCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.UpdateCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.DeleteCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.CommandCollection);

        return instance;
      }
    }
  }

  public partial class TeletrackInfoTableAdapter
  {
    public static TeletrackInfoTableAdapter Instance
    {
      get
      {
        TeletrackInfoTableAdapter instance = new TeletrackInfoTableAdapter();

        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.SelectCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.InsertCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.UpdateCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.DeleteCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.CommandCollection);

        return instance;
      }
    }
  }

  public partial class UserInfoTableAdapter
  {
    public static UserInfoTableAdapter Instance
    {
      get
      {
        UserInfoTableAdapter instance = new UserInfoTableAdapter();

        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.SelectCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.InsertCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.UpdateCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.DeleteCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.CommandCollection);

        return instance;
      }
    }
  }

  public partial class DealerInfoTableAdapter
  {
    public static DealerInfoTableAdapter Instance
    {
      get
      {
        DealerInfoTableAdapter instance = new DealerInfoTableAdapter();

        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.SelectCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.InsertCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.UpdateCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.DeleteCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.CommandCollection);

        return instance;
      }
    }
  }

  public partial class DealerRoleTableAdapter
  {
    public static DealerRoleTableAdapter Instance
    {
      get
      {
        DealerRoleTableAdapter instance = new DealerRoleTableAdapter();

        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.SelectCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.InsertCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.UpdateCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.DeleteCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.CommandCollection);

        return instance;
      }
    }
  }

  public partial class RolesTableAdapter
  {
    public static RolesTableAdapter Instance
    {
      get
      {
        RolesTableAdapter instance = new RolesTableAdapter();

        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.SelectCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.InsertCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.UpdateCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.Adapter.DeleteCommand);
        SqlCmdTimeoutSetter.SetTimeout(instance.CommandCollection);

        return instance;
      }
    }
  }
}