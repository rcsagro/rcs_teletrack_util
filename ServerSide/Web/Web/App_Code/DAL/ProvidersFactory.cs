using System.Collections.Generic;

namespace RCS.Web.DAL
{
  /// <summary>
  /// ��� ����������� ������� � ������.
  /// </summary>
  internal static class ProvidersFactory
  {
    private static readonly Dictionary<string, object> _providers =
      new Dictionary<string, object>();

    private static readonly object _syncObject = new object();

    /// <summary>
    /// ������ ���������� ��������� ����.
    /// </summary>
    /// <typeparam name="T">��� ����������, ��������������� �� BaseDAO.</typeparam>
    /// <returns>���������.</returns>
    internal static T GetProvider<T>() where T : BaseDAO, new()
    {
      T result;
      string key = typeof(T).Name;
      lock (_syncObject)
      {
        if (_providers.ContainsKey(key))
        {
          result = (T)_providers[key];
        }
        else
        {
          result = new T();
          _providers.Add(key, result);
        }
      }
      return result;
    }
  }
}