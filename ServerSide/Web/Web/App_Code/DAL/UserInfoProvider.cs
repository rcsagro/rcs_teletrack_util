using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using RCS.Web.DAL;
using RCS.Web.Entities;

namespace RCS.Web.DAL
{
  /// <summary>
  /// ��������� ������� � ������ UserInfo. 
  /// </summary>
  internal class UserInfoProvider : BaseDAO
  {
    /// <summary>
    /// ������������ ������ ��������� �������� ���������� � ���������
    /// ����������-�������.
    /// </summary>
    /// <param name="idUser">������������� ������������.</param>
    /// <param name="idClient">������������� ����������-�������.</param>
    /// <returns>������ ��������� �������� ���������� � ����������.</returns>
    internal List<UserDetailsLinkedState> GetTeletrackLinkedState(
      int idUser, int idClient, int startRowIndex, int maximumRows)
    {
      List<UserDetailsLinkedState> result = new List<UserDetailsLinkedState>();
      using (SqlConnection con = new SqlConnection(ConnectionString))
      {
        using (SqlCommand command = new SqlCommand("dbo.adm_SelectTeletrackLinkedState", con))
        {
          command.CommandType = CommandType.StoredProcedure;
          command.CommandTimeout = AppSettings.CommandTimeout;
          command.Parameters.AddWithValue("@idUser", idUser);
          command.Parameters.AddWithValue("@idClient", idClient);
          command.Parameters.AddWithValue("@startIndex", startRowIndex + 1);
          command.Parameters.AddWithValue("@finishIndex", startRowIndex + maximumRows);
          con.Open();
          using (SqlDataReader reader = command.ExecuteReader())
          {
            FillList(reader, result);
          }
        }
      }
      return result;
    }

    /// <summary>
    /// ������������ ������ ��������� �������� ����������� � ��������� ���������.
    /// </summary>
    /// <param name="idUser">������������� ������������.</param>
    /// <param name="idTeletrack">������������� ���������.</param>
    /// <returns>������ ��������� �������� ������������ � ���������.</returns>
    internal List<UserDetailsLinkedState> GetClientLinkedState(int idUser, int idTeletrack)
    {
      List<UserDetailsLinkedState> result = new List<UserDetailsLinkedState>();
      using (SqlConnection con = new SqlConnection(ConnectionString))
      {
        using (SqlCommand command = new SqlCommand("dbo.adm_SelectClientLinkedState", con))
        {
          command.CommandType = CommandType.StoredProcedure;
          command.CommandTimeout = AppSettings.CommandTimeout;
          command.Parameters.AddWithValue("@idUser", idUser);
          command.Parameters.AddWithValue("@idTeletrack", idTeletrack);
          con.Open();
          using (SqlDataReader reader = command.ExecuteReader())
          {
            FillList(reader, result);
          }
        }
      }
      return result;
    }

    /// <summary>
    /// ������������ ������ �������� DetailLinkedState, ��������� ������ ������.
    /// </summary>
    /// <param name="reader">SqlDataReader.</param>
    /// <param name="list">List ��� ����������.</param>
    /// <returns>������ �������� DetailLinkedState.</returns>
    private static void FillList(SqlDataReader reader, List<UserDetailsLinkedState> list)
    {
      UserDetailsLinkedState newItem;
      while (reader.Read())
      {
        newItem = new UserDetailsLinkedState();
        newItem.DetailID = (int)reader["DetailID"];
        newItem.DetailLogin = (string)reader["DetailLogin"];
        if (reader["CommunicID"] != DBNull.Value)
        {
          newItem.CommunicID = (int)reader["CommunicID"];
        }
        newItem.Linked = (bool)reader["Linked"];

        list.Add(newItem);
      }
    }

    /// <summary>
    /// ������ ������������� ��� ���� USER 
    /// </summary>
    /// <param name="dealer">������������� ������</param>
    /// <returns>UserInfoDataTable</returns>
    public LocalCash.UserInfoDataTable GetUserInfoForUs(int dealerID)
    {
      string sqlCmd =
        "SELECT id_user, Kod_b, Name, IsActiveState, Retention_Period, Describe " +
        "FROM dbo.UserInfo " +
        "WHERE DealerId = @DealerId " +
        "ORDER BY id_user DESC";

      LocalCash.UserInfoDataTable usDt = new LocalCash.UserInfoDataTable();

      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        SqlDataAdapter da = new SqlDataAdapter(sqlCmd, cn);
        da.SelectCommand.Parameters.AddWithValue("@DealerId", dealerID);
        da.Fill(usDt);
      }
      return usDt;
    }
  }
}