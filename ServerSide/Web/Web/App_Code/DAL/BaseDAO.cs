using System.Web.Configuration;

namespace RCS.Web.DAL
{
  /// <summary>
  /// ������� ����� ��� ��������, ���������� � ��.
  /// </summary>
  internal class BaseDAO
  {
    /// <summary>
    /// ������ ����������� � ��.
    /// </summary>
    protected static string ConnectionString
    {
      get
      {
        return AppSettings.ConnectionString;
      }
    }
  }
}