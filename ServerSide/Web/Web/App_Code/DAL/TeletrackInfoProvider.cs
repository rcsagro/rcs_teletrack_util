using System;
using System.Data;
using System.Data.SqlClient;
using RCS.Web.Entities;

namespace RCS.Web.DAL
{
    /// <summary>
    /// ��������� ������� � ������ TeletrackInfo.
    /// </summary>
    internal class TeletrackInfoProvider : BaseDAO
    {
        /// <summary>
        /// ���������� �� �� �� ������.
        /// </summary>
        /// <param name="login">�����.</param>
        /// <returns>TeletrackInfo.</returns>
        internal TeletrackInfo GetByLogin(string login)
        {
            TeletrackInfo result = null;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand("dbo.adm_SelectTeletrackInfoByLogin", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = AppSettings.CommandTimeout;
                    command.Parameters.AddWithValue("@login", login);
                    con.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        result = FillTeletrackInfo(reader);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// ���������� �� �� �� ��������������.
        /// </summary>
        /// <param name="id">������������� ���������.</param>
        /// <returns>TeletrackInfo.</returns>
        internal TeletrackInfo GetByID(int id)
        {
            TeletrackInfo result = null;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand("dbo.adm_SelectTeletrackInfoByID", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = AppSettings.CommandTimeout;
                    command.Parameters.AddWithValue("@id", id);
                    con.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        result = FillTeletrackInfo(reader);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// ������������ ������� TeletrackInfo, ��������� ������ ������.
        /// </summary>
        /// <param name="reader">SqlDataReader.</param>
        /// <returns>TeletrackInfo.</returns>
        private static TeletrackInfo FillTeletrackInfo(SqlDataReader reader)
        {
            TeletrackInfo result = null;
            if (reader.Read())
            {
                result = new TeletrackInfo();
                result.ID_Teletrack = (int) reader["ID_Teletrack"];
                result.ID_User = (int) reader["ID_User"];
                result.LoginTT = (string) reader["LoginTT"];
                result.PasswordTT = (string) reader["PasswordTT"];
                if (reader["Timeout"] != DBNull.Value)
                {
                    result.Timeout = (int) reader["Timeout"];
                }
                if (reader["GSMIMEI"] != DBNull.Value)
                {
                    result.GSMIMEI = (string) reader["GSMIMEI"];
                }
                if (reader["DTimeLastPack"] != DBNull.Value)
                {
                    result.DTimeLastPack = (DateTime) reader["DTimeLastPack"];
                }
            }
            return result;
        }

        /// <summary>
        /// ���-�� ���������� � ������� ������������.
        /// </summary>
        /// <param name="idUser">������������� ������������.</param>
        /// <returns>���-�� ����������.</returns>
        public int GetRowCount(int idUser)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(
                    "SELECT COUNT(1) FROM dbo.TeletrackInfo WHERE ID_User = @id", con))
                {
                    command.CommandTimeout = AppSettings.CommandTimeout;
                    command.Parameters.AddWithValue("@id", idUser);
                    con.Open();
                    return (int) command.ExecuteScalar();
                }
            }
        }

        /// <summary>
        /// ���-�� ���������� � ����.
        /// </summary>
        /// <returns>���-�� ���������� � ����.</returns>
        public int GetTtCount()
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(
                    "SELECT COUNT(1) FROM dbo.TeletrackInfo", con))
                {
                    command.CommandTimeout = AppSettings.CommandTimeout;
                    con.Open();
                    return (int) command.ExecuteScalar();
                }
            }
        }
    }
}