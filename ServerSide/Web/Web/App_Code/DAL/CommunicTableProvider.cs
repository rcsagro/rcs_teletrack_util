using System.Data;
using System.Data.SqlClient;
using RCS.Web.DAL;

namespace RCS.Web.DAL
{
  /// <summary>
  /// ��������� ������� � ������ ������� CommunicTable.
  /// </summary>
  internal class CommunicTableProvider : BaseDAO
  {
    /// <summary>
    /// �������� ������.
    /// </summary>
    /// <param name="id">�������� ���������� �����.</param>
    internal void Delete(int id)
    {
      using (SqlConnection con = new SqlConnection(ConnectionString))
      {
        using (SqlCommand command = new SqlCommand("dbo.adm_DeleteCommunicTable", con))
        {
          command.CommandType = CommandType.StoredProcedure;
          command.CommandTimeout = AppSettings.CommandTimeout;
          command.Parameters.AddWithValue("@id", id);
          con.Open();
          command.ExecuteNonQuery();
        }
      }
    }

    /// <summary>
    /// ������� ����� ������.
    /// </summary>
    /// <param name="teletrackID">������������� ���������.</param>
    /// <param name="clientID">������������� ����������.</param>
    internal void Insert(int teletrackID, int clientID)
    {
      using (SqlConnection con = new SqlConnection(ConnectionString))
      {
        using (SqlCommand command = new SqlCommand("dbo.adm_InsertCommunicTable", con))
        {
          command.CommandType = CommandType.StoredProcedure;
          command.CommandTimeout = AppSettings.CommandTimeout;
          command.Parameters.AddWithValue("@ID_Client", clientID);
          command.Parameters.AddWithValue("@ID_Teletrack", teletrackID);
          con.Open();
          command.ExecuteNonQuery();
        }
      }
    }
  }
}