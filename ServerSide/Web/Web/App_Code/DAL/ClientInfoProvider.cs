using System;
using System.Data;
using System.Data.SqlClient;
using RCS.Web.Entities;

namespace RCS.Web.DAL
{
  /// <summary>
  /// ��������� ������� � ������ ClientInfo.
  /// </summary>
  internal class ClientInfoProvider : BaseDAO
  {
    /// <summary>
    /// ���������� �� �� �� ������.
    /// </summary>
    /// <param name="login">�����.</param>
    /// <returns>�������� ���������� ClientInfo.</returns>
    internal ClientInfo GetByLogin(string login)
    {
      ClientInfo result = null;
      using (SqlConnection con = new SqlConnection(ConnectionString))
      {
        using (SqlCommand command = new SqlCommand(
          "dbo.adm_SelectClientInfoByLogin", con))
        {
          command.CommandType = CommandType.StoredProcedure;
          command.CommandTimeout = AppSettings.CommandTimeout;
          command.Parameters.AddWithValue("@loginCl", login);
          con.Open();
          using (SqlDataReader reader = command.ExecuteReader())
          {
            result = FillTeletrackInfo(reader);
          }
        }
      }
      return result;
    }

    /// <summary>
    /// ���������� �� �� �� ��������������.
    /// </summary>
    /// <param name="id">������������� ����������.</param>
    /// <returns>�������� ���������� ClientInfo.</returns>
    internal ClientInfo GetByID(int id)
    {
      ClientInfo result = null;
      using (SqlConnection con = new SqlConnection(ConnectionString))
      {
        using (SqlCommand command = new SqlCommand(
          "dbo.adm_SelectClientInfoByID", con))
        {
          command.CommandType = CommandType.StoredProcedure;
          command.CommandTimeout = AppSettings.CommandTimeout;
          command.Parameters.AddWithValue("@id", id);
          con.Open();
          using (SqlDataReader reader = command.ExecuteReader())
          {
            result = FillTeletrackInfo(reader);
          }
        }
      }
      return result;
    }

    /// <summary>
    /// ������������ ������� TeletrackInfo, ��������� ������ ������.
    /// </summary>
    /// <param name="reader">SqlDataReader.</param>
    /// <returns>�������� ���������� ClientInfo.</returns>
    private static ClientInfo FillTeletrackInfo(SqlDataReader reader)
    {
      ClientInfo result = null;
      if (reader.Read())
      {
        result = new ClientInfo();
        result.ID_Client = (int)reader["ID_Client"];
        result.LoginCl = (string)reader["LoginCl"];
        result.Password = (string)reader["Password"];
        result.InNetwork = (bool)reader["InNetwork"];
        result.ID_Packet = (long)reader["ID_Packet"];
        result.DTimeDeliveredPack = (DateTime)reader["DTimeDeliveredPack"];
        result.ID_LastPackInSrvDB = (long)reader["ID_LastPackInSrvDB"];
        result.ID_User = (int)reader["ID_User"];
        if (reader["Describe"] != DBNull.Value)
        {
          result.Describe = (string)reader["Describe"];
        }
      }
      return result;
    }
  }
}
