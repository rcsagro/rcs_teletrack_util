using System;
using System.Data.SqlClient;

namespace RCS.Web.DAL
{
  /// <summary>
  /// ���������������� ������� ������ � ��.
  /// </summary>
  internal class AdminProvider : BaseDAO
  {
    private const string VERSION_DB =
      "SELECT value FROM sys.extended_properties WHERE name = 'VersionDB'";

    internal string GetDbVersion()
    {
      using (SqlConnection con = new SqlConnection(ConnectionString))
      {
        using (SqlCommand command = new SqlCommand(VERSION_DB, con))
        {
          command.CommandTimeout = AppSettings.CommandTimeout;
          con.Open();
          object obj = command.ExecuteScalar();
          return obj != null ? (string)obj : String.Empty;
        }
      }
    }
  }
}