using System.Data;
using System.Data.SqlClient;

namespace RCS.Web.DAL
{
  /// <summary>
  /// ��������� ������� � ������ VersionControl.
  /// </summary>
  internal class VersionControlProvider : BaseDAO
  {
    internal void IncrementVersion(string tableName)
    {
      using (SqlConnection con = new SqlConnection(ConnectionString))
      {
        using (SqlCommand command = new SqlCommand("dbo.incrementOfVersion", con))
        {
          command.CommandType = CommandType.StoredProcedure;
          command.CommandTimeout = AppSettings.CommandTimeout;
          command.Parameters.AddWithValue("@tableName", tableName);
          con.Open();
          command.ExecuteNonQuery();
        }
      }
    }
  }
}