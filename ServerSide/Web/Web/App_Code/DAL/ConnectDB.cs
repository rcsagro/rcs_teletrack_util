using System;
using System.Data;
using System.Data.SqlClient;

namespace RCS.Web.DAL
{
  /// <summary>
  /// Summary description for ConnectDB
  /// </summary>
  internal class ConnectDB : BaseDAO
  {
    internal int? GetInt32ValueSP(string spName, string sParName, string sParValue)
    {
      object obj;
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(spName, cn))
        {
          cm.CommandType = CommandType.StoredProcedure;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          SqlParameter prmName = new SqlParameter(sParName, SqlDbType.NVarChar, 20);
          prmName.Value = sParValue;
          cm.Parameters.Add(prmName);
          cn.Open();
          // Выбираем единственное значение
          obj = cm.ExecuteScalar();
        }
      }
      return (obj == null) ? (int?)obj : (int)obj;
    }

    internal int GetInt32ValueSP(string sComName, SqlParameter Par)
    {
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(sComName, cn))
        {
          cm.CommandType = CommandType.StoredProcedure;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          cm.Parameters.Add(Par);
          cn.Open();
          return Convert.ToInt32(cm.ExecuteScalar());
        }
      }
    }

    internal int? GetInt32ValueSP(string spName, SqlParameter[] paramArray)
    {
      object obj;
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(spName, cn))
        {
          cm.CommandType = CommandType.StoredProcedure;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          cm.Parameters.AddRange(paramArray);
          cn.Open();
          obj = cm.ExecuteScalar();
        }
      }
      return (obj == null) ? (int?)obj : (int)obj;
    }

    internal string GetStringValueSP(string spName, SqlParameter[] paramArray)
    {
      object obj;
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(spName, cn))
        {
          cm.CommandType = CommandType.StoredProcedure;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          cm.Parameters.AddRange(paramArray);
          cn.Open();
          obj = cm.ExecuteScalar();
        }
      }
      return (obj == null) ? null : (string)obj;
    }

    internal string GetStringValueSP(string spName, string sParName, int iParValue)
    {
      object obj;
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(spName, cn))
        {
          cm.CommandType = CommandType.StoredProcedure;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          SqlParameter prmName = new SqlParameter(sParName, SqlDbType.Int);
          prmName.Value = iParValue;
          cm.Parameters.Add(prmName);
          cn.Open();
          // Выбираем единственное значение
          obj = cm.ExecuteScalar();
        }
      }
      return obj == null ? "0" : (string)obj;
    }

    internal DateTime GetDateTimeValueSP(string spName)
    {
      object obj;
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(spName, cn))
        {
          cm.CommandType = CommandType.StoredProcedure;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          cn.Open();
          obj = cm.ExecuteScalar();
        }
      }
      return ((obj == null) || (obj == DBNull.Value)) ?
        DateTime.MinValue : (DateTime)obj;
    }

    internal int GetInt32Value(string sSQLString)
    {
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(sSQLString, cn))
        {
          cm.CommandType = CommandType.Text;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          cn.Open();
          return Convert.ToInt32(cm.ExecuteScalar());
        }
      }
    }

    internal DataSet GetDataSP(string spName, SqlParameter[] paramArray)
    {
      DataSet dsWork = new DataSet();
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(spName, cn))
        {
          cm.CommandType = CommandType.StoredProcedure;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          for (int j = 0; j < paramArray.Length; j++)
          {
            cm.Parameters.Add(paramArray[j]);
          }
          using (SqlDataAdapter da = new SqlDataAdapter(cm))
          {
            da.Fill(dsWork);
          }
        }
      }
      return dsWork;
    }

    internal DataSet GetDataSP(string spName)
    {
      DataSet dsWork = new DataSet();
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(spName, cn))
        {
          cm.CommandType = CommandType.StoredProcedure;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          using (SqlDataAdapter da = new SqlDataAdapter(cm))
          {
            da.Fill(dsWork);
          }
        }
      }
      return dsWork;
    }

    internal DataSet GetDateSQL(string sSQL)
    {
      DataSet dsWork = new DataSet();
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(sSQL, cn))
        {
          cm.CommandType = CommandType.Text;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          using (SqlDataAdapter da = new SqlDataAdapter(cm))
          {
            da.Fill(dsWork);
          }
        }
      }
      return dsWork;
    }

    internal int ExecuteNonQuery(string sSQL)
    {
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(sSQL, cn))
        {
          cm.CommandType = CommandType.Text;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          cn.Open();
          return cm.ExecuteNonQuery();
        }
      }
    }

    internal int ExecuteNonQuerySP(string sComName, SqlParameter[] paramArray)
    {
      using (SqlConnection cn = new SqlConnection(ConnectionString))
      {
        using (SqlCommand cm = new SqlCommand(sComName, cn))
        {
          cm.CommandType = CommandType.StoredProcedure;
          cm.CommandTimeout = AppSettings.CommandTimeout;
          for (int j = 0; j < paramArray.Length; j++)
          {
            cm.Parameters.Add(paramArray[j]);
          }
          cn.Open();
          return cm.ExecuteNonQuery();
        }
      }
    }
  }
}