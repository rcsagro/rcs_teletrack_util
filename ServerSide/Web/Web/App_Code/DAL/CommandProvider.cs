using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using RCS.Web.Entities;
using System.Data;

namespace RCS.Web.DAL
{
    /// <summary>
    /// ��������� ������� � ������ Command.
    /// </summary>
    internal class CommandProvider : BaseDAO
    {
        #region ������� ���� ������

        private const string SELECT_ALL =
            @"SELECT ID_Command, CommandType, ExecuteState, Approved, DateQueueEntry, DeltaTimeExCom,
        DispatcherIP, LoginCl, Describe, LoginTT, AnswCode
      FROM (
        SELECT ROW_NUMBER() OVER(ORDER BY {0}) AS RowNumber,
        cmd.ID_Command AS ID_Command, cmd.CommandType AS CommandType, 
        cmd.ExecuteState AS ExecuteState, cmd.Approved AS Approved,
        cmd.DateQueueEntry AS DateQueueEntry, cmd.DeltaTimeExCom AS DeltaTimeExCom, 
        COALESCE(cmd.DispatcherIP, '��� ��������') AS DispatcherIP, 
        ci.LoginCl AS LoginCl, COALESCE(ci.Describe, '') AS Describe, 
        ti.LoginTT AS LoginTT, cmd.AnswCode AS AnswCode
      FROM dbo.Command cmd 
        JOIN dbo.ClientInfo ci ON cmd.ID_Client = ci.ID_Client
        JOIN dbo.TeletrackInfo ti ON cmd.ID_Teletrack = ti.ID_Teletrack) AS t
      WHERE (RowNumber >= @startIndex) AND (RowNumber < @finishIndex)";

        private enum SelectAllSortColumns
        {
            ID_Command,
            CommandType,
            ExecuteState,
            Approved,
            DateQueueEntry,
            DeltaTimeExCom,
            DispatcherIP,
            LoginCl,
            Describe,
            LoginTT,
            AnswCode
        }

        /// <summary>
        /// ������� ���� ������ �� ������� Command.
        /// </summary>
        /// <param name="sortColumns">������ �������� ����������� � ����������.</param>
        /// <param name="startRowIndex">��������� ������ �������.</param>
        /// <param name="maximumRows">������������ ���-�� ����� �������.</param>
        /// <returns>������ ������.</returns>
        internal List<Command> GetCommands(string sortColumns, int startRowIndex, int maximumRows)
        {
            if (String.IsNullOrEmpty(sortColumns.Trim()))
            {
                sortColumns = "DateQueueEntry DESC";
            }
            else
            {
                CheckSortColumns(sortColumns);
            }
            List<Command> result = new List<Command>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(
                    String.Format(SELECT_ALL, sortColumns), con))
                {
                    command.CommandTimeout = AppSettings.CommandTimeout;
                    command.Parameters.AddWithValue("@startIndex", ++startRowIndex);
                    command.Parameters.AddWithValue("@finishIndex", startRowIndex + maximumRows);
                    con.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        FillCommandList(reader, result);
                    }
                }
            }
            return result;
        }

        private void FillCommandList(SqlDataReader reader, List<Command> list)
        {
            while (reader.Read())
            {
                Command newItem = new Command();
                newItem.Id = (int) reader["ID_Command"];
                newItem.CommandType = (string) reader["CommandType"];
                newItem.HandlingStatus = (A1CmdHandlingStatus) ((byte) reader["ExecuteState"]);
                newItem.Approved = (bool) reader["Approved"];
                newItem.DateEntry = (DateTime) reader["DateQueueEntry"];
                newItem.DateExpire = (DateTime) reader["DeltaTimeExCom"];
                newItem.DispatcherIP = (string) reader["DispatcherIP"];
                newItem.DispatcherLogin = (string) reader["LoginCl"];
                newItem.DispatcherDescription = (string) reader["Describe"];
                newItem.TeletrackLogin = (string) reader["LoginTT"];
                if (reader["AnswCode"] != DBNull.Value)
                {
                    newItem.TeletrackAnswerCode = (byte) reader["AnswCode"];
                }
                list.Add(newItem);
            }
        }

        /// <summary>
        /// ���-�� ������ � ��.
        /// </summary>
        /// <returns>���-�� ������.</returns>
        internal int GetCommandsCount()
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(
                    "SELECT COUNT(1) FROM dbo.Command", con))
                {
                    command.CommandTimeout = AppSettings.CommandTimeout;
                    con.Open();
                    return (int) command.ExecuteScalar();
                }
            }
        }

        private const string ERROR_SORT_COLUMN =
            "������ ������������ ������� ���������� {0} ��� ������� {1}";

        private static void CheckSortColumns(string checkColumns)
        {
            foreach (string column in checkColumns.Split(new Char[] {','}))
            {
                string[] tmp = column.Split(new Char[] {' '});
                // �������� 2 ������� - ������� � ����������� ����������
                if (((tmp.Length < 1) && (tmp.Length > 2)) ||
                    (!Enum.IsDefined(typeof (SelectAllSortColumns), tmp[0])))
                {
                    throw new ApplicationException(String.Format(
                        ERROR_SORT_COLUMN, column, SELECT_ALL));
                }
            }
        }

        #endregion

        #region ������ � ������ ���������, ���������� ������������

        private const string SELECT_PENDING =
            @"SELECT ID_Command, CommandType, Approved, DateQueueEntry, 
        DeltaTimeExCom, DispatcherIP, LoginCl, Describe, LoginTT
      FROM (
        SELECT ROW_NUMBER() OVER(ORDER BY {0}) AS RowNumber,
          cmd.ID_Command AS ID_Command, cmd.CommandType AS CommandType, 
          cmd.Approved AS Approved,
          cmd.DateQueueEntry AS DateQueueEntry, cmd.DeltaTimeExCom AS DeltaTimeExCom, 
          COALESCE(cmd.DispatcherIP, '��� ��������') AS DispatcherIP, 
          ci.LoginCl AS LoginCl, COALESCE(ci.Describe, '') AS Describe, 
          ti.LoginTT AS LoginTT
        FROM dbo.Command cmd 
          JOIN dbo.ClientInfo ci ON cmd.ID_Client = ci.ID_Client
          JOIN dbo.TeletrackInfo ti ON cmd.ID_Teletrack = ti.ID_Teletrack
        WHERE (cmd.DeltaTimeExCom > getdate()) AND (cmd.ExecuteState = 0)) AS t
      WHERE (RowNumber >= @startIndex) AND (RowNumber < @finishIndex)";

        /// <summary>
        /// ������� �����(�� ������������ ����������, ������ ������������) 
        /// ������ �� ������� Command.
        /// </summary>
        /// <param name="sortColumns">������ �������� ����������� � ����������.</param>
        /// <param name="startRowIndex">��������� ������ �������.</param>
        /// <param name="maximumRows">������������ ���-�� ����� �������.</param>
        /// <returns>������ ����� ������.</returns>
        internal List<Command> GetPendingCommands(string sortColumns, int startRowIndex, int maximumRows)
        {
            if (String.IsNullOrEmpty(sortColumns.Trim()))
            {
                sortColumns = "ID_Command";
            }
            else
            {
                CheckSortColumns(sortColumns);
            }
            List<Command> result = new List<Command>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(
                    String.Format(SELECT_PENDING, sortColumns), con))
                {
                    command.CommandTimeout = AppSettings.CommandTimeout;
                    command.Parameters.AddWithValue("@startIndex", ++startRowIndex);
                    command.Parameters.AddWithValue("@finishIndex", startRowIndex + maximumRows);
                    con.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        FillPendingCommandList(reader, result);
                    }
                }
            }
            return result;
        }

        private void FillPendingCommandList(SqlDataReader reader, List<Command> list)
        {
            while (reader.Read())
            {
                Command newItem = new Command();
                newItem.Id = (int) reader["ID_Command"];
                newItem.CommandType = (string) reader["CommandType"];
                newItem.HandlingStatus = A1CmdHandlingStatus.Pending;
                newItem.Approved = (bool) reader["Approved"];
                newItem.DateEntry = (DateTime) reader["DateQueueEntry"];
                newItem.DateExpire = (DateTime) reader["DeltaTimeExCom"];
                newItem.DispatcherIP = (string) reader["DispatcherIP"];
                newItem.DispatcherLogin = (string) reader["LoginCl"];
                newItem.DispatcherDescription = (string) reader["Describe"];
                newItem.TeletrackLogin = (string) reader["LoginTT"];
                list.Add(newItem);
            }
        }

        private const string COUNT_PENDING_COMMANDS =
            "SELECT COUNT(1) FROM dbo.Command WHERE (DeltaTimeExCom > getdate()) AND (ExecuteState = 0)";

        /// <summary>
        /// ���-�� �����(�� ������������ ����������, ������ ������������) ������ � ��.
        /// </summary>
        /// <returns>���-�� ����� ������.</returns>
        internal int GetPendingCommandsCount()
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(COUNT_PENDING_COMMANDS, con))
                {
                    command.CommandTimeout = AppSettings.CommandTimeout;
                    con.Open();
                    return (int) command.ExecuteScalar();
                }
            }
        }

        /// <summary>
        /// ���������/����� ����� "����������" ��� ���������(�����) �������.
        /// </summary>
        /// <param name="id">������������� �������.</param>
        /// <param name="approved">�������� ����� "����������".</param>
        internal void SetCommandApproved(int id, bool approved)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand("dbo.adm_SetCommandApproved", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = AppSettings.CommandTimeout;
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@approved", approved);
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// �������� �������� ����� "����������" ��� ���������(�����) ������. 
        /// </summary>
        /// <param name="updateSet">������� � ������� ����������: 
        /// id ������� - �������� Approved.</param>
        internal void UpdateCommandsApproved(Dictionary<int, bool> updateSet)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand("dbo.adm_SetCommandApproved", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = AppSettings.CommandTimeout;
                    command.Parameters.Add("@id", SqlDbType.Int);
                    command.Parameters.Add("@approved", SqlDbType.Bit);
                    con.Open();
                    foreach (KeyValuePair<int, bool> item in updateSet)
                    {
                        command.Parameters["@id"].Value = item.Key;
                        command.Parameters["@approved"].Value = item.Value;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        #endregion

        #region ������� ������� �� ��������������

        private const string SELECT_COMMAND =
            @"SELECT cmd.ID_Command AS ID_Command, cmd.CommandType AS CommandType, 
        cmd.ExecuteState AS ExecuteState, cmd.Approved AS Approved,
        cmd.DateQueueEntry AS DateQueueEntry, cmd.DeltaTimeExCom AS DeltaTimeExCom, 
        COALESCE(cmd.DispatcherIP, '��� ��������') AS DispatcherIP, 
        ci.LoginCl AS LoginCl, COALESCE(ci.Describe, '') AS Describe, 
        ti.LoginTT AS LoginTT, cmd.AnswCode AS AnswCode,
        CAST(cmd.CommandText AS varchar(max)) AS DispCmdText,
        COALESCE(CAST(cmd.AnswData AS varchar(max)), '��� ��������') AS TtAnswerText
      FROM dbo.Command cmd 
        JOIN dbo.ClientInfo ci ON cmd.ID_Client = ci.ID_Client
        JOIN dbo.TeletrackInfo ti ON cmd.ID_Teletrack = ti.ID_Teletrack
      WHERE cmd.ID_Command = @id";

        /// <summary>
        /// ������� ������� �� ��������������.
        /// </summary>
        /// <param name="ID_Command">������������� �������.</param>
        /// <returns>Command</returns>
        internal Command GetCommandById(int ID_Command)
        {
            Command result = new Command();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(SELECT_COMMAND, con))
                {
                    command.CommandTimeout = AppSettings.CommandTimeout;
                    command.Parameters.AddWithValue("@id", ID_Command);
                    con.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        FillCommand(reader, result);
                    }
                }
            }
            return result;
        }

        private void FillCommand(SqlDataReader reader, Command cmd)
        {
            if (reader.Read())
            {
                cmd.Id = (int) reader["ID_Command"];
                cmd.CommandType = (string) reader["CommandType"];
                cmd.HandlingStatus = (A1CmdHandlingStatus) ((byte) reader["ExecuteState"]);
                cmd.Approved = (bool) reader["Approved"];
                cmd.DateEntry = (DateTime) reader["DateQueueEntry"];
                cmd.DateExpire = (DateTime) reader["DeltaTimeExCom"];
                cmd.DispatcherIP = (string) reader["DispatcherIP"];
                cmd.DispatcherLogin = (string) reader["LoginCl"];
                cmd.DispatcherDescription = (string) reader["Describe"];
                cmd.TeletrackLogin = (string) reader["LoginTT"];
                if (!reader.IsDBNull(reader.GetOrdinal("AnswCode")))
                {
                    cmd.TeletrackAnswerCode = (byte) reader["AnswCode"];
                }
                cmd.DispatcherCmdText = (string) reader["DispCmdText"];
                cmd.TeletrackAnswerText = (string) reader["TtAnswerText"];
            }
        }

        #endregion
    }
}