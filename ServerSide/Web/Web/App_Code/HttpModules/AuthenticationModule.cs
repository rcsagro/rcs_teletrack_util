using System;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Security;

namespace RCS.Web.HttpModules
{
  public class AuthenticationModule : IHttpModule
  {
    public void Init(HttpApplication context)
    {
      // register for the PostAuthenticateRequest event
      context.PostAuthenticateRequest +=
        new EventHandler(OnPostAuthenticate);
    }

    void OnPostAuthenticate(object sender, EventArgs e)
    {
      HttpContext context = HttpContext.Current;

      if (context.Request.IsAuthenticated)
      {
        // extracting the roles from the ticket
        FormsIdentity id = (FormsIdentity)context.User.Identity;
        string[] roles = id.Ticket.UserData.Split('|');

        context.User = Thread.CurrentPrincipal =
          new GenericPrincipal(context.User.Identity, roles);
      }
    }

    public void Dispose()
    {
    }
  }
}