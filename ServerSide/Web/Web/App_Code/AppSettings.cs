using System;
using System.Web.Configuration;

namespace RCS.Web
{
  /// <summary>
  /// ��������� ������ appSettings ����� ������������ Web.Config.
  /// </summary>
  public static class AppSettings
  {
    /// <summary>
    /// ����������� ������� � �����.
    /// </summary>
    public static bool RestrictAccess
    {
      get
      {
        string value = WebConfigurationManager.AppSettings["RestrictAccess"];
        return ((!String.IsNullOrEmpty(value)) &&
          (value.Equals("true", StringComparison.OrdinalIgnoreCase))) ?
          true : false;
      }
    }

    /// <summary>
    /// ������� ���������� sql ������.
    /// </summary>
    public static int CommandTimeout
    {
      get
      {
        int result = 0;
        string value = WebConfigurationManager.AppSettings["CommandTimeout"];
        if (!String.IsNullOrEmpty(value))
        {
          Int32.TryParse(value, out result);
        }
        if (result < 30)
        {
          result = 180;
        }
        return result;
      }
    }

    /// <summary>
    /// ������ ����������� � ��.
    /// </summary>
    public static string ConnectionString
    {
      get
      {
        return WebConfigurationManager.ConnectionStrings[
          "OnLineServiceDBConnectionString"].ConnectionString;
      }
    }
  }
}