﻿using System.ComponentModel;
using RCS.Web.DAL;

namespace RCS.Web.BLL
{
  /// <summary>
  /// UserInfoForUsBL
  /// </summary>
  public class UserInfoForUsBL
  {
    /// <summary>
    /// Возвращает объект(таблицу) UserInfoDataTable
    /// </summary>
    /// <param name="dealerId">Идентификатор дилера.</param>
    /// <returns>LocalCash.UserInfoDataTable</returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static LocalCash.UserInfoDataTable GetUserInfoForUs(int dealerId)
    {
      return ProvidersFactory.GetProvider<UserInfoProvider>().
        GetUserInfoForUs(dealerId);
    }
  }
}