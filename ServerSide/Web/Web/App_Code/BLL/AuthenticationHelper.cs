using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using RCS.Web.DAL;

namespace RCS.Web.BLL
{
  /// <summary>
  /// Summary description for AuthenticationHelper
  /// </summary>
  public class AuthenticationHelper
  {
    // TODO: ����� � FormsAuthHelper

    public static string GetRolesForUser(string log, string pass, out int res)
    {
      return ProvidersFactory.GetProvider<AuthenticationHelperProvider>().
        GetRolesForUser(log, pass, out res);
    }

    // TODO: ��� ��� �� ������ ���� �� ��������� ���������� � ��� ����� ������� �� ������ ???
    // TODO: � ��� ����� :-(
    public static string ParamRead(int iNum, string sDefault)
    {
      SqlParameter[] parArr = new SqlParameter[2];
      parArr[0] = new SqlParameter("@DL", SqlDbType.Int);
      parArr[1] = new SqlParameter("@NUM", SqlDbType.Int);
      SqlParameter prmName = new SqlParameter("@Name", SqlDbType.NVarChar, 30);
      prmName.Value = HttpContext.Current.User.Identity.Name;

      ConnectDB cDB = ProvidersFactory.GetProvider<ConnectDB>();

      int iDealer = cDB.GetInt32ValueSP("dbo.Select_Dealer_By_Name", prmName);
      parArr[0].SqlValue = iDealer;
      parArr[1].SqlValue = iNum;

      string result = cDB.GetStringValueSP("ParamGet", parArr);
      if (!String.IsNullOrEmpty(result))
      {
        return result;
      }
      else
      {
        ParamInsert(iDealer, iNum, sDefault);
        return sDefault;
      }
    }

    public static bool ParamWrite(int iNum, string sValue)
    {
      ConnectDB cDB = ProvidersFactory.GetProvider<ConnectDB>();

      SqlParameter prmName = new SqlParameter("@Name", SqlDbType.NVarChar, 30);
      prmName.Value = HttpContext.Current.User.Identity.Name;
      int iDealer = cDB.GetInt32ValueSP("dbo.Select_Dealer_By_Name", prmName);

      SqlParameter[] parArr = new SqlParameter[2];
      parArr[0] = new SqlParameter("@DL", SqlDbType.Int);
      parArr[1] = new SqlParameter("@NUM", SqlDbType.Int);
      parArr[0].SqlValue = iDealer;
      parArr[1].SqlValue = iNum;

      int? res = cDB.GetInt32ValueSP("ParamGetId", parArr);
      if (res.HasValue)
      {
        ParamUpdate(res.Value, sValue);
      }
      else
      {
        ParamInsert(iDealer, iNum, sValue);
      }
      return true;
    }

    private static bool ParamInsert(int iDealer, int iNum, string sValue)
    {
      SqlParameter[] parArr = new SqlParameter[3];
      parArr[0] = new SqlParameter("@DL", SqlDbType.Int);
      parArr[1] = new SqlParameter("@NUM", SqlDbType.Int);
      parArr[2] = new SqlParameter("@VAL", SqlDbType.NVarChar, 255);
      parArr[0].SqlValue = iDealer;
      parArr[1].SqlValue = iNum;
      parArr[2].SqlValue = sValue;
      ProvidersFactory.GetProvider<ConnectDB>().ExecuteNonQuerySP("dbo.ParamInSet", parArr);
      return true;
    }

    private static bool ParamUpdate(int iID, string sValue)
    {
      SqlParameter[] parArr = new SqlParameter[2];
      parArr[0] = new SqlParameter("@ID", SqlDbType.Int);
      parArr[1] = new SqlParameter("@VAL", SqlDbType.NVarChar, 255);
      parArr[0].SqlValue = iID;
      parArr[1].SqlValue = sValue;
      ProvidersFactory.GetProvider<ConnectDB>().ExecuteNonQuerySP("dbo.ParamSet", parArr);
      return true;
    }
  }
}