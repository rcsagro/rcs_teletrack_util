using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;

namespace RCS.Web.BLL
{
  /// <summary>
  /// Collection of useful helper functions for forms authentication 
  /// dominick baier - dbaier@leastprivilege.com
  /// </summary>
  public class FormsAuthHelper
  {
    // TODO: ����� � AuthenticationHelper

    /// <summary>
    /// Ticket creation.
    /// Core ticket creation method - supply a username and userdat.
    /// </summary>
    /// <param name="username"></param>
    /// <param name="userData"></param>
    /// <returns>FormsAuthenticationTicket</returns>
    public static FormsAuthenticationTicket CreateAuthenticationTicket(string username, string userData)
    {
      // grab the current request context
      HttpContext context = HttpContext.Current;

      // get the ticket timeout from forms configuration
      AuthenticationSection config =
        (AuthenticationSection)context.GetSection("system.web/authentication");

      int timeout = (int)config.Forms.Timeout.TotalMinutes;

      if (string.IsNullOrEmpty(userData))
      {
        userData = String.Empty;
      }
      // create the auth ticket manually and set values
      FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
        1,                                    // version
        username,                             // user name
        DateTime.Now,                         // creation time
        DateTime.Now.AddMinutes(timeout),     // expiration time
        false,                                // persistent
        userData,                             // optional user data
        FormsAuthentication.FormsCookiePath); // path

      return ticket;
    }

    /// <summary>
    /// Create a ticket for a user and empty userData.
    /// </summary>
    /// <param name="username"></param>
    /// <returns>FormsAuthenticationTicket</returns>
    public static FormsAuthenticationTicket CreateAuthenticationTicket(string username)
    {
      return CreateAuthenticationTicket(username, string.Empty);
    }

    /// <summary>
    /// Create a ticket for a user with embedded role information.
    /// </summary>
    /// <param name="username"></param>
    /// <param name="roles"></param>
    /// <returns>FormsAuthenticationTicket</returns>
    public static FormsAuthenticationTicket CreateAuthenticationTicket(string username, string[] roles)
    {
      return CreateAuthenticationTicket(username, String.Join("|", roles));
    }

    /// <summary>
    /// Create a ticket for a user with embedded roles and a timeout.
    /// </summary>
    /// <param name="username"></param>
    /// <param name="roles"></param>
    /// <param name="timeout"></param>
    /// <returns>FormsAuthenticationTicket</returns>
    public static FormsAuthenticationTicket CreateAuthenticationTicket(string username, string[] roles, int timeout)
    {
      HttpContext context = HttpContext.Current;
      // set the role caching timeout
      DateTime rolesExpiration = DateTime.Now.AddMinutes(timeout);
      // encode roles and timeout in the ticket
      string userData = String.Format("{0:yyyy.MM.dd.HH.mm.ss}|{1}",
        rolesExpiration, string.Join("|", roles));

      return CreateAuthenticationTicket(username, userData);
    }

    /// <summary>
    /// Ticket issuing.
    /// Issue the authentication ticket as a cookie.
    /// </summary>
    /// <param name="ticket">FormsAuthenticationTicket</param>
    public static void SetAuthenticationCookie(FormsAuthenticationTicket ticket)
    {
      // grab the current request context
      HttpContext context = HttpContext.Current;
      // encrypt the ticket
      string authcookie = FormsAuthentication.Encrypt(ticket);

      // create new cookie and set contents
      HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName);
      cookie.Value = authcookie;
      // respect requireSSL and domain settings
      cookie.Secure = FormsAuthentication.RequireSSL;
      cookie.Domain = FormsAuthentication.CookieDomain;
      // set HttpOnly - cookie will not be available to client script 
      cookie.HttpOnly = true;

      // check if SSL is required
      if (!context.Request.IsSecureConnection && FormsAuthentication.RequireSSL)
      {
        throw new HttpException("Ticket requires SSL");
      }
      // set the cookie
      context.Response.Cookies.Add(cookie);
    }

    /// <summary>
    /// Issue the authentication ticket as a query string variable.
    /// </summary>
    /// <param name="ticket"></param>
    /// <param name="url"></param>
    public static void SetQueryStringRedirect(FormsAuthenticationTicket ticket, string url)
    {
      // grab the current request context
      HttpContext context = HttpContext.Current;

      if (!context.Request.IsSecureConnection && FormsAuthentication.RequireSSL)
      {
        throw new HttpException("Ticket requires SSL");
      }
      string encTicket = FormsAuthentication.Encrypt(ticket);

      context.Response.Redirect(String.Format("{0}?{1}={2}",
        url,
        FormsAuthentication.FormsCookieName,
        encTicket));
    }

    /// <summary>
    /// Role extraction from the ticket.
    /// Extracts roles from Context.User.Identity.
    /// </summary>
    /// <returns></returns>
    public static string[] ExtractRoles()
    {
      // grab the current request context
      HttpContext context = HttpContext.Current;

      if (context.User.Identity is FormsIdentity)
      {
        return ExtractRoles((FormsIdentity)context.User.Identity);
      }
      else
      {
        throw new ArgumentException("Not of type FormsIdentity", "Context.User.Identity");
      }
    }

    public static string GetUserName()
    {
      return HttpContext.Current.User.Identity.Name;
    }

    public static string GetUserAddress()
    {
      return HttpContext.Current.Request.UserHostAddress;
    }

    /// <summary>
    /// Extracts roles from a ticket.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static string[] ExtractRoles(FormsIdentity id)
    {
      return ExtractRoles(id.Ticket.UserData);
    }

    /// <summary>
    /// Extracts roles from a userData string.
    /// </summary>
    /// <param name="userdata"></param>
    /// <returns></returns>
    public static string[] ExtractRoles(string userdata)
    {
      // extract roles
      string[] roles = userdata.Split('|');
      return roles;
    }

    ///// <summary>
    ///// Extracts roles from the current Context.User, 
    ///// taking checking and updating of the timeout into account.
    ///// </summary>
    ///// <returns></returns>
    //public static string[] ExtractRolesWithTimeout()
    //{
    //  // grab the current request context
    //  HttpContext context = HttpContext.Current;

    //  if (context.User.Identity is FormsIdentity)
    //  {
    //    return ExtractRolesWithTimeout((FormsIdentity)context.User.Identity);
    //  }
    //  else
    //  {
    //    throw new ArgumentException("Not of type FormsIdentity", "Context.User.Identity");
    //  }
    //}

    ///// <summary>
    ///// Extracts roles from a ticket, taking checking and updating of the timeout into account.
    ///// </summary>
    ///// <param name="id"></param>
    ///// <returns></returns>
    //public static string[] ExtractRolesWithTimeout(FormsIdentity id)
    //{
    //  return FormsAuthHelper.ExtractRolesWithTimeout(
    //    id.Ticket.UserData,
    //    id.Name);
    //}

    ///// <summary>
    ///// Extracts roles from a userData string, 
    ///// taking checking and updating of the timeout into account.
    ///// </summary>
    ///// <param name="userdata"></param>
    ///// <param name="username"></param>
    ///// <returns></returns>
    //public static string[] ExtractRolesWithTimeout(string userdata, string username)
    //{
    //  // extract expiration date
    //  string[] data = userdata.Split('|');
    //  string[] expiration = data[0].Split('.');

    //  DateTime expirationDate = DateTime.Parse(String.Format("{0}.{1}.{2} {3}:{4}:{5}",
    //    expiration[0],
    //    expiration[1],
    //    expiration[2],
    //    expiration[3],
    //    expiration[4],
    //    expiration[5]));

    //  // check if roles information has expired
    //  if (DateTime.Now > expirationDate)
    //  {
    //    // check if user is still valid, if yes issue a new ticket
    //    // if no, throw an exception
    //    string[] roles = AuthenticationHelper.GetRolesForUser(username);

    //    FormsAuthenticationTicket ticket =
    //      FormsAuthHelper.CreateAuthenticationTicket(username, roles, 30);
    //    FormsAuthHelper.SetAuthenticationCookie(ticket);
    //    return roles;
    //  }
    //  else
    //  {
    //    // extract roles from userData string and return them as an array
    //    string[] roles = new string[data.Length - 1];
    //    for (int i = 0; i < roles.Length; i++)
    //    {
    //      roles[i] = data[i + 1];
    //    }
    //    return roles;
    //  }
    //}

    /// <summary>
    /// Convert a token into a GenericPrincipal.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="roles"></param>
    /// <returns></returns>
    public static GenericPrincipal ConvertWindowsToGeneric(WindowsIdentity id, out string[] roles)
    {
      roles = getWindowsGroups(id);

      return new GenericPrincipal(
        new GenericIdentity(id.Name, "GenericIdentityFromWindowsIdentity"),
        roles);
    }

    /// <summary>
    /// Extract roles from a WindowsIdentity.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private static string[] getWindowsGroups(WindowsIdentity id)
    {
      List<string> groups = new List<string>();
      IdentityReferenceCollection irc =
        id.Groups.Translate(typeof(NTAccount));

      foreach (NTAccount acc in irc)
      {
        groups.Add(acc.Value);
      }
      return groups.ToArray();
    }
  }
}

