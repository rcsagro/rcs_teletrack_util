﻿using System;
using System.ComponentModel;
using LocalCashTableAdapters;
using RCS.Web.DAL;

namespace RCS.Web.BLL
{
  /// <summary>
  /// Пользователи.
  /// </summary>
  public partial class UserInfoBL
  {
    /// <summary>
    /// Возвращает объект(таблицу) UserInfoDataTable
    /// </summary>
    /// <returns> LocalCash.UserInfoDataTable.</returns>
    public static LocalCash.UserInfoDataTable GetUserInfo(int id_user)
    {
      return UserInfoTableAdapter.Instance.GetData(id_user);
    }

    public static bool GetUserInfoByID(int id_user, out int kodB, out string usName, out string descr, out bool isActState, out int retPeriod, out string dealer)
    {
      LocalCash cash_ds = new LocalCash();
      kodB = 0;
      usName = "";
      descr = "";
      isActState = false;
      retPeriod = 2;
      dealer = "";

      UserInfoTableAdapter.Instance.Fill(cash_ds.UserInfo, id_user);

      LocalCash.UserInfoRow usRw = cash_ds.UserInfo.FindByid_user(id_user);
      if (!usRw.IsKod_bNull())
      {
        kodB = usRw.Kod_b;
      }
      usName = usRw.Name;
      if (!usRw.IsDescribeNull())
      {
        descr = usRw.Describe;
      }
      isActState = usRw.IsActiveState;
      retPeriod = usRw.Retention_Period;
      if (usRw.IsDealerNull())
      {
        dealer = "нет";
      }
      else
      {
        dealer = usRw.Dealer;
      }
      return true;
    }

    /// <summary>
    /// Добавляет в БД нового пользователя
    /// </summary>
    /// <param name="name"></param>
    /// <param name="descr"></param>
    /// <param name="isActive"></param>
    /// <param name="retentPeriod"></param>
    /// <param name="kodB"></param>
    /// <param name="id_user"></param>
    /// <returns></returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
    public static int InsertUserInfo(int kodB, string name, string descr, bool isActive, int retentPeriod, string dealerName, out int id_user)
    {
      id_user = 0;
      LocalCash cash_ds = new LocalCash();

      UserInfoTableAdapter ta = UserInfoTableAdapter.Instance;
      ta.Fill(cash_ds.UserInfo, id_user);//Здесь выборка всего, как есть adm_SelectUserInfo

      LocalCash.UserInfoRow usRw = cash_ds.UserInfo.NewUserInfoRow();
      usRw.Kod_b = kodB;
      usRw.Name = name;
      usRw.Describe = descr;
      usRw.IsActiveState = isActive;
      usRw.Retention_Period = retentPeriod;

      if (dealerName == "")
      {
        usRw.SetDealerNull();
      }
      else
      {
        usRw.Dealer = dealerName;
      }

      cash_ds.UserInfo.AddUserInfoRow(usRw);

      return ta.Update(usRw);
    }

    /// <summary>
    /// Вносит изменения в запись
    /// </summary>
    /// <param name="name"></param>
    /// <param name="descr"></param>
    /// <param name="isActive"></param>
    /// <param name="retentPeriod"></param>
    /// <param name="kodB"></param>
    /// <param name="id_user"></param>
    /// <returns></returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
    public static int UpdateUserInfo(int kodB, string name, string descr, bool isActive, int retentPeriod, string dealerName, int id_user)
    {
      LocalCash cash_ds = new LocalCash();

      UserInfoTableAdapter ta = UserInfoTableAdapter.Instance;
      ta.Fill(cash_ds.UserInfo, id_user);

      LocalCash.UserInfoRow usRw = cash_ds.UserInfo.FindByid_user(id_user);
      if (usRw != null)
      {
        usRw.Kod_b = kodB;
        usRw.Name = name;
        usRw.Describe = descr;
        usRw.IsActiveState = isActive;
        usRw.Retention_Period = retentPeriod;
        usRw.Dealer = dealerName;

        ConnectDB cDB = ProvidersFactory.GetProvider<ConnectDB>();
        string sSQL = String.Format(
          "SELECT ID FROM dbo.DealerInfo WHERE (DealerName = N'{0}')", dealerName);
        int dealerCode = cDB.GetInt32Value(sSQL);
        sSQL = String.Format(
          "UPDATE dbo.UserInfo SET DealerId = {0} WHERE (ID_User = {1})", dealerCode, id_user);
        cDB.ExecuteNonQuery(sSQL);

        if (ta.Update(usRw) > 0)
        {
          return 1;
        }
      }
      return 0;
    }

    /// <summary>
    /// Удаляет пользователя
    /// </summary>
    /// <param name="id_client"></param>
    [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
    public static void DeleteUserInfo(int id_user)
    {
      LocalCash cash_ds = new LocalCash();

      UserInfoTableAdapter ta = UserInfoTableAdapter.Instance;
      ta.Fill(cash_ds.UserInfo, id_user);

      ClientInfoTableAdapter taClientInfo = ClientInfoTableAdapter.Instance;
      taClientInfo.Fill(cash_ds.ClientInfo);

      TeletrackInfoTableAdapter taTeletrackInfo = TeletrackInfoTableAdapter.Instance;
      taTeletrackInfo.Fill(cash_ds.TeletrackInfo);

      LocalCash.UserInfoRow delUserInfoRow =
        cash_ds.UserInfo.FindByid_user(id_user);

      foreach (LocalCash.ClientInfoRow row in delUserInfoRow.GetClientInfoRows())
      {
        row.Delete();
      }
      foreach (LocalCash.TeletrackInfoRow row in delUserInfoRow.GetTeletrackInfoRows())
      {
        row.Delete();
      }
      delUserInfoRow.Delete();

      VersionControlProvider vcp = ProvidersFactory.
        GetProvider<VersionControlProvider>();
      bool needIncVer = false;

      if (taTeletrackInfo.Update(cash_ds.TeletrackInfo) > 0)
      {
        vcp.IncrementVersion("TeletrackInfo");
        needIncVer = true;
      }
      if (taClientInfo.Update(cash_ds.ClientInfo) > 0)
      {
        needIncVer = true;
      }
      if (ta.Update(cash_ds.UserInfo) > 0)
      {
        vcp.IncrementVersion("UserInfo");
      }
      if (needIncVer)
      {
        vcp.IncrementVersion("CommunicTable");
      }
    }

    public static int UpdateUserInfo(LocalCash.UserInfoRow[] usRw)
    {
      return UserInfoTableAdapter.Instance.Update(usRw);
    }
  }
}