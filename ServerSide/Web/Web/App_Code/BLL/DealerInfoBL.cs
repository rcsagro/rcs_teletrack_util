﻿using System.Collections.Generic;
using System.ComponentModel;
using LocalCashTableAdapters;
using RCS.Web.DAL;

namespace RCS.Web.BLL
{
  /// <summary>
  /// Summary description for DealerInfoBL
  /// </summary>
  public class DealerInfoBL
  {
    /// <summary>
    /// Возвращает объект(таблицу) ClientInfoDataTable
    /// </summary>
    /// <returns></returns>
    public static LocalCash.DealerInfoDataTable GetDealerInfo()
    {
      return DealerInfoTableAdapter.Instance.GetData();
    }

    public static bool IsDealerNameExist(string login, int id)
    {
      LocalCash cash_ds = new LocalCash();

      DealerInfoTableAdapter.Instance.Fill(cash_ds.DealerInfo);
      LocalCash.DealerInfoRow[] dd = (LocalCash.DealerInfoRow[])
        cash_ds.DealerInfo.Select(string.Format("DealerName = '{0}'", login));
      if (dd.Length > 0 && dd[0].ID != id)
      {
        return true;
      }
      return false;
    }

    public static bool GetDealerInfoById(int id, out string login, out string pass, out string descr)
    {
      login = "";
      pass = "";
      descr = "";
      LocalCash cash_ds = new LocalCash();
      DealerInfoTableAdapter.Instance.Fill(cash_ds.DealerInfo);
      LocalCash.DealerInfoRow dRw = cash_ds.DealerInfo.FindByID(id);
      if (dRw == null)
      {
        return false;
      }
      if (!dRw.IsDealerNameNull())
      {
        login = dRw.DealerName;
      }
      if (!dRw.IsPasswordNull())
      {
        pass = dRw.Password;
      }
      if (!dRw.IsDescrNull())
      {
        descr = dRw.Descr;
      }
      return true;
    }

    /// <summary>
    /// Список логинов дилеров
    /// </summary>
    /// <returns></returns>
    public static List<string> GetListOfDealers()
    {
      List<string> ls = new List<string>();

      LocalCash cash_ds = new LocalCash();
      DealerInfoTableAdapter.Instance.Fill(cash_ds.DealerInfo);

      foreach (LocalCash.DealerInfoRow rw in cash_ds.DealerInfo.Rows)
      {
        if (!rw.IsDealerNameNull())
        {
          ls.Add(rw.DealerName);
        }
      }
      ls.Add("нет");
      return ls;
    }

    /// <summary>
    /// Добавляет в БД нового dealer
    /// </summary>
    /// <param name="login">Логин</param>
    /// <param name="pass">Пароль</param>
    /// <param name="descr">Описание</param>
    /// <param name="id_user">id пользователя</param>
    /// <returns></returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
    public static int InsertDealerInfo(string login, string pass, string descr, int id_role, bool isDealerNameExist)
    {
      int result = 0;
      if (!isDealerNameExist)
      {
        LocalCash cash_ds = new LocalCash();
        DealerInfoTableAdapter ta = DealerInfoTableAdapter.Instance;
        ta.Fill(cash_ds.DealerInfo);

        LocalCash.DealerInfoRow dRw = cash_ds.DealerInfo.NewDealerInfoRow();
        dRw.DealerName = login;
        dRw.Password = pass;
        dRw.Descr = descr;
        cash_ds.DealerInfo.AddDealerInfoRow(dRw);
        ta.Update(dRw);

        DealerRoleTableAdapter daDR = DealerRoleTableAdapter.Instance;
        daDR.Fill(cash_ds.DealerRole);

        LocalCash.DealerRoleRow dealRole = cash_ds.DealerRole.NewDealerRoleRow();
        dealRole.DealerID = dRw.ID;
        dealRole.RoleID = 2; //Всегда User
        cash_ds.DealerRole.AddDealerRoleRow(dealRole);
        result = daDR.Update(dealRole);
      }
      return result;
    }

    /// <summary>
    /// Вносит изменения в запись
    /// </summary>
    /// <param name="login"></param>
    /// <param name="pass"></param>
    /// <param name="descr"></param>
    /// <param name="id_user"></param>
    /// <returns></returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
    public static int UpdateDealerInfo(string login, string pass, string descr, int id)
    {
      LocalCash cash_ds = new LocalCash();
      DealerInfoTableAdapter ta = DealerInfoTableAdapter.Instance;
      ta.Fill(cash_ds.DealerInfo);

      LocalCash.DealerInfoRow dlRw = cash_ds.DealerInfo.FindByID(id);
      if (dlRw != null)
      {
        dlRw.DealerName = login;
        dlRw.Password = pass;
        dlRw.Descr = descr;
        if (ta.Update(dlRw) > 0)
        {
          return 1;
        }
      }
      return 0;
    }

    /// <summary>
    /// Удаляет дилера
    /// </summary>
    /// <param name="id_client"></param>
    /// <returns></returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
    public static int DeleteDealerInfo(int id_dealer)
    {
      LocalCash cash_ds = new LocalCash();
      DealerInfoTableAdapter ta = DealerInfoTableAdapter.Instance;
      ta.Fill(cash_ds.DealerInfo);
      LocalCash.DealerInfoRow dlRw = cash_ds.DealerInfo.FindByID(id_dealer);
      if (dlRw != null)
      {
        dlRw.Delete();
        if (ta.Update(dlRw) > 0)
        {
          DealerRoleTableAdapter daDR = DealerRoleTableAdapter.Instance;
          daDR.Fill(cash_ds.DealerRole);
          LocalCash.DealerRoleRow[] dealerRoleRw = (LocalCash.DealerRoleRow[])cash_ds.DealerRole.Select(string.Format("DealerID = {0}", id_dealer));
          foreach (LocalCash.DealerRoleRow rw in dealerRoleRw)
          {
            rw.Delete();
          }
          daDR.Update(dealerRoleRw);
        }
        return 1;
      }
      return 0;
    }

    /// <summary>
    /// Имя дилера по заданному идентификатору пользователя.
    /// </summary>
    /// <param name="userID">Идентификатор пользователя.</param>
    /// <returns>Имя дилера.</returns>
    public static string GetNameByUserID(int userID)
    {
      return ProvidersFactory.GetProvider<ConnectDB>().GetStringValueSP(
        "dbo.Select_Clients_Dealer_Name", "@UserID", userID);
    }
  }
}