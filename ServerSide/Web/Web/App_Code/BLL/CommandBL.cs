using System;
using System.Collections.Generic;
using System.ComponentModel;
using RCS.Web.DAL;
using RCS.Web.Entities;

namespace RCS.Web.BLL
{
  /// <summary>
  /// ���������-������.
  /// </summary>
  [DataObject]
  public class CommandBL
  {
    /// <summary>
    /// ������� ���� ������ �� ������� Command.
    /// </summary>
    /// <param name="sortColumns">������ �������� ����������� � ����������.</param>
    /// <param name="startRowIndex">��������� ������ �������.</param>
    /// <param name="maximumRows">������������ ���-�� ����� �������.</param>
    /// <returns>������ ������.</returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static List<Command> GetCommands(string sortColumns, int startRowIndex, int maximumRows)
    {
      return ProvidersFactory.GetProvider<CommandProvider>().GetCommands(
        sortColumns, startRowIndex, maximumRows);
    }

    /// <summary>
    /// ���-�� ������ � ��.
    /// </summary>
    /// <returns>���-�� ������.</returns>
    public static int GetCommandsCount()
    {
      return ProvidersFactory.GetProvider<CommandProvider>().GetCommandsCount();
    }

    /// <summary>
    /// ������� ������� �� ��������������.
    /// </summary>
    /// <param name="ID_Command">������������� �������.</param>
    /// <returns>Command</returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select)]
    public static Command GetCommandById(int ID_Command)
    {
      return ProvidersFactory.GetProvider<CommandProvider>().GetCommandById(ID_Command);
    }

    /// <summary>
    /// ������� �����(�� ������������ ����������, ������ ������������)
    /// ������ �� ������� Command.
    /// </summary>
    /// <param name="sortColumns">������ �������� ����������� � ����������.</param>
    /// <param name="startRowIndex">��������� ������ �������.</param>
    /// <param name="maximumRows">������������ ���-�� ����� �������.</param>
    /// <returns>������ ����� ������.</returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select)]
    public static List<Command> GetPendingCommands(string sortColumns, int startRowIndex, int maximumRows)
    {
      return ProvidersFactory.GetProvider<CommandProvider>().GetPendingCommands(
       sortColumns, startRowIndex, maximumRows);
    }

    /// <summary>
    /// ���-�� �����(�� ������������ ����������) ������ � ��, 
    /// ������ ������������.
    /// </summary>
    /// <returns>���-�� ����� ������.</returns>
    public static int GetPendingCommandsCount()
    {
      return ProvidersFactory.GetProvider<CommandProvider>().GetPendingCommandsCount();
    }

    /// <summary>
    /// ���������/����� ����� "����������" ��� ���������(�����) �������.
    /// </summary>
    /// <param name="original_Id">������������� �������.</param>
    /// <param name="original_Approved">��������� �������� ����� "����������".</param>
    /// <param name="approved">����� �������� ����� "����������".</param>
    [DataObjectMethodAttribute(DataObjectMethodType.Update)]
    public static void SetCommandApproved(int original_Id, bool original_Approved, bool approved)
    {
      if (original_Approved != approved)
      {
        ProvidersFactory.GetProvider<CommandProvider>().SetCommandApproved(original_Id, approved);
      }
    }

    /// <summary>
    /// �������� �������� ����� "����������" ��� ���������(�����) ������. 
    /// </summary>
    /// <exception cref="ArgumentNullException">���� originalSet ��� currentSet = null.</exception>
    /// <param name="originalSet">������� ������� �� ���������: id ������� - �������� Approved.</param>
    /// <param name="currentSet">������� � �������� �������: id ������� - �������� Approved.</param>
    public static void UpdateCommandsApproved(Dictionary<int, bool> originalSet,
      Dictionary<int, bool> currentSet)
    {
      if (originalSet == null)
      {
        throw new ArgumentNullException("originalSet");
      }
      if (currentSet == null)
      {
        throw new ArgumentNullException("currentSet");
      }

      Dictionary<int, bool> updateSet = new Dictionary<int, bool>();

      foreach (KeyValuePair<int, bool> kvp in currentSet)
      {
        if ((originalSet.ContainsKey(kvp.Key)) && (originalSet[kvp.Key] != kvp.Value))
        {
          updateSet.Add(kvp.Key, kvp.Value);
        }
      }

      if (updateSet.Count > 0)
      {
        ProvidersFactory.GetProvider<CommandProvider>().UpdateCommandsApproved(updateSet);
      }
    }
  }
}