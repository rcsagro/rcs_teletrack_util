﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using RCS.OnlineServer.Core.Model;
using RCS.OnlineServer.Core.Repositories;

namespace RCS.Web.BLL
{
  /// <summary>
  /// Summary description for FirmwareTaskBL
  /// </summary>
  [DataObject]
  public class FirmwareTaskBL
  {
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static List<FirmwareTask> GetTasksPagesById(long firmwareId, string sortColumns, int maximumRows, int startRowIndex)
    {
      return GetRepository().GetTasksPageById(
        firmwareId,
        sortColumns,
        startRowIndex,
        maximumRows);
    }

    public static int GetFirmwareTasksCountById(long firmwareId)
    {
      return GetRepository().GetTasksCountById(firmwareId);
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static List<FirmwareTaskProcessing> GetProcPagesById(long taskId, string sortColumns, int maximumRows, int startRowIndex)
    {
      return GetRepository().GetProcessingPageById(
        taskId,
        sortColumns,
        startRowIndex,
        maximumRows);
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static List<FirmwareTaskProcessing> GetProcPage(string sortColumns, int maximumRows, int startRowIndex)
    {
      return GetRepository().GetProcessingPage(
        sortColumns,
        startRowIndex,
        maximumRows);
    }

    public static int GetTaskProcCountById(long taskId)
    {
      return GetRepository().GetProcCountById(taskId);
    }

    public static int GetTaskProcCount()
    {
      return GetRepository().GetProcCount();
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
    public static void Insert(long firmwareId, FirmwareTask task)
    {
      task.FirmwareId = firmwareId;
      task.CreatedDate = DateTime.Now;
      task.Stage = FirmwareTaskStage.New;
      GetRepository().Insert(
        task,
        FormsAuthHelper.GetUserName(),
        FormsAuthHelper.GetUserAddress());
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
    public static void Insert(FirmwareTask task)
    {
      //task.FirmwareId = 
      task.CreatedDate = DateTime.Now;
      task.Stage = FirmwareTaskStage.New;
      GetRepository().Insert(
        task,
        FormsAuthHelper.GetUserName(),
        FormsAuthHelper.GetUserAddress());
    }

    public static FirmwareTask GetTaskById(long id)
    {
      return GetRepository().GetById(id);
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
    public static void Update(FirmwareTask task)
    {
      GetRepository().Update(
        task,
        FormsAuthHelper.GetUserName(),
        FormsAuthHelper.GetUserAddress());
    }

    public static void CancelTask(long id)
    {
      GetRepository().SetTaskCancelled(
        id,
        FormsAuthHelper.GetUserName(),
        FormsAuthHelper.GetUserAddress());
    }

    private static IFirmwareTaskRepository GetRepository()
    {
      return new RepositoryFactory(AppSettings.ConnectionString, AppSettings.CommandTimeout)
        .GetFirmwareTaskRepository();
    }

    /// <summary>
    /// Возможно ли отменить выполнение задачи.
    /// </summary>
    /// <param name="currentStage">Текущая стадия обработки команды.</param>
    /// <returns>True - можно прервать.</returns>
    public static bool CanCancelTask(FirmwareTaskStage currentStage)
    {
      return currentStage != FirmwareTaskStage.Completed
        && currentStage != FirmwareTaskStage.Cancelled;
    }
  }
}
