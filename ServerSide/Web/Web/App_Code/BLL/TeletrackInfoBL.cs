﻿using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using LocalCashTableAdapters;
using RCS.Web.DAL;
using RCS.Web.Entities;

namespace RCS.Web.BLL
{
  /// <summary>
  /// Summary description for TeletrackInfoBL
  /// </summary>
  [DataObject()]
  public class TeletrackInfoBL
  {
    /// <summary>
    /// Добавляет в БД новый телетрек.
    /// </summary>
    /// <param name="ttInfo">TeletrackInfo.</param>
    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public static void Insert(TeletrackInfo ttInfo)
    {
      LocalCash cash_ds = new LocalCash();

      ClientInfoTableAdapter.Instance.Fill(cash_ds.ClientInfo);
      TeletrackInfoTableAdapter taTeletrackInfo = TeletrackInfoTableAdapter.Instance;
      taTeletrackInfo.Fill(cash_ds.TeletrackInfo);
      CommunicTableTableAdapter taCommunicTable = CommunicTableTableAdapter.Instance;
      taCommunicTable.Fill(cash_ds.CommunicTable);

      LocalCash.TeletrackInfoRow newTiRow = cash_ds.TeletrackInfo.NewTeletrackInfoRow();
      newTiRow.LoginTT = ttInfo.LoginTT;
      newTiRow.PasswordTT = ttInfo.PasswordTT;
      newTiRow.Timeout = ttInfo.Timeout;
      if (ttInfo.GSMIMEI == "")
      {
        newTiRow.SetGSMIMEINull();
      }
      else
      {
        newTiRow.GSMIMEI = ttInfo.GSMIMEI;
      }
      newTiRow.ID_User = ttInfo.ID_User;
      cash_ds.TeletrackInfo.AddTeletrackInfoRow(newTiRow);

      taTeletrackInfo.Update(cash_ds.TeletrackInfo);

      //Проверяем есть ли у пользователя еще сервисы(для привязки сервисов к новому телетреку)
      LocalCash.ClientInfoRow[] servicesOfThisUser = (LocalCash.ClientInfoRow[])
        cash_ds.ClientInfo.Select(string.Format("ID_User = {0}", ttInfo.ID_User));
      if (servicesOfThisUser.Length != 0)
      {
        LocalCash.CommunicTableRow newCtRow;
        foreach (LocalCash.ClientInfoRow ciRow in servicesOfThisUser)
        {
          newCtRow = cash_ds.CommunicTable.NewCommunicTableRow();
          newCtRow.ID_Client = ciRow.ID_Client;
          newCtRow.ID_Teletrack = newTiRow.ID_Teletrack;
          cash_ds.CommunicTable.AddCommunicTableRow(newCtRow);
        }

        taCommunicTable.Update(cash_ds.CommunicTable);
        ProvidersFactory.GetProvider<VersionControlProvider>().
          IncrementVersion("CommunicTable");
      }
      //Сохраняем идентификатор для редиректа.
      System.Web.HttpContext.Current.Session[
        SessionKeyNames.LAST_INSERTED_TT_ID] = newTiRow.ID_Teletrack;
    }

    /// <summary>
    /// Вносит изменения в запись.
    /// </summary>
    /// <param name="ttInfo">TeletrackInfo.</param>
    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public static void Update(TeletrackInfo ttInfo)
    {
      LocalCash cash_ds = new LocalCash();

      TeletrackInfoTableAdapter ta = TeletrackInfoTableAdapter.Instance;
      ta.Fill(cash_ds.TeletrackInfo);

      LocalCash.TeletrackInfoRow ttRw =
        cash_ds.TeletrackInfo.FindByID_Teletrack(ttInfo.ID_Teletrack);
      if (ttRw != null)
      {
        ttRw.LoginTT = ttInfo.LoginTT;
        ttRw.PasswordTT = ttInfo.PasswordTT;
        ttRw.Timeout = ttInfo.Timeout;
        ttRw.GSMIMEI = ttInfo.GSMIMEI;

        ta.Update(ttRw);
      }
    }

    /// <summary>
    /// Удаляет телетрек.
    /// </summary>
    /// <param name="TeletrackInfo">ttInfo.</param>
    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public static void Delete(TeletrackInfo ttInfo)
    {
      LocalCash cash_ds = new LocalCash();

      TeletrackInfoTableAdapter ta = TeletrackInfoTableAdapter.Instance;
      ta.Fill(cash_ds.TeletrackInfo);

      LocalCash.TeletrackInfoRow ttRw =
        cash_ds.TeletrackInfo.FindByID_Teletrack(ttInfo.ID_Teletrack);
      if (ttRw != null)
      {
        ttRw.Delete();
        if (ta.Update(ttRw) > 0)
        {
          VersionControlProvider vcp = ProvidersFactory.
            GetProvider<VersionControlProvider>();
          vcp.IncrementVersion("TeletrackInfo");
          vcp.IncrementVersion("CommunicTable");
        }
      }
    }

    /// <summary>
    /// Поиск TeletrackInfo по идентификатору.
    /// </summary>
    /// <param name="id_teletrack">Идентификатор телетрека.</param>
    /// <returns>TeletrackInfo.</returns>
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static TeletrackInfo SelectByID(int id_teletrack)
    {
      return ProvidersFactory.GetProvider<TeletrackInfoProvider>().
        GetByID(id_teletrack);
    }

    /// <summary>
    ///  Поиск TeletrackInfo по логину.
    /// </summary>
    /// <param name="login">Логин телетрека.</param>
    /// <returns>TeletrackInfo</returns>
    public static TeletrackInfo GetByLogin(string login)
    {
      return ProvidersFactory.GetProvider<TeletrackInfoProvider>().
        GetByLogin(login);
    }

    /// <summary>
    /// Проверка существования телетрека с заданным логином.
    /// </summary>
    /// <param name="sLogin">Логин телетрека.</param>
    /// <returns>true - телетрек существует.</returns>
    public static bool TeletrackExists(string sLogin)
    {
      return GetByLogin(sLogin) != null;
    }

    /// <summary>
    /// Поиск идентификатора телетрека по его логину.
    /// </summary>
    /// <param name="sLogin">Логин телетрека.</param>
    /// <returns>Идентификатор телетрека, если телетрек найден,
    /// иначе - null.</returns>
    public static int? SelectTeletrackIDByLogin(string sLogin)
    {
      return ProvidersFactory.GetProvider<ConnectDB>().GetInt32ValueSP(
        "dbo.st_SelectTeletrackIdByLogin", "@Login", sLogin);
    }

    public static DataRow SelectTeletrackInfoByID_proc(int id_teletrack)
    {
      SqlParameter[] parArr = new SqlParameter[1];
      parArr[0] = new SqlParameter("@Id_Teletr", id_teletrack);
      DataSet dsWork = ProvidersFactory.GetProvider<ConnectDB>().GetDataSP(
        "dbo.st_SelectTeletrackOfId", parArr);
      return dsWork.Tables[0].Rows[0];
    }

    /// <summary>
    /// Возвращает объект(таблицу) TeletrackInfoDataTable.
    /// </summary>
    /// <returns>TeletrackInfoDataTable.</returns>
    public static LocalCash.TeletrackInfoDataTable GetTeletrackInfo()
    {
      return TeletrackInfoTableAdapter.Instance.GetData();
    }

    public static int UpdateTeletrackInfo(LocalCash.TeletrackInfoRow[] ttRw)
    {
      return TeletrackInfoTableAdapter.Instance.Update(ttRw);
    }

    /// <summary>
    /// Кол-во телетреков в БД.
    /// </summary>
    /// <returns></returns>
    public static int GetTtCount()
    {
      return ProvidersFactory.GetProvider<TeletrackInfoProvider>().
        GetTtCount();
    }

    /// <summary>
    /// Информация о лицензии.
    /// </summary>
    /// <returns>Информация о лицензии.</returns>
    public static LicInfo GetLicInfo()
    {
      return VersionInfoBL.Lic;
    }
  }
}