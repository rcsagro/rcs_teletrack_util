﻿using LocalCashTableAdapters;

namespace RCS.Web.BLL
{
  /// <summary>
  /// Summary description for CommunicTableBL
  /// </summary>
  public class CommunicTableBL
  {
    /// <summary>
    /// Возвращает объект(таблицу) ClientInfoDataTable
    /// </summary>
    /// <returns></returns>
    public static LocalCash.CommunicTableDataTable GetCommunicTable()
    {
      return CommunicTableTableAdapter.Instance.GetData();
    }

    public static int UpdateCommunicTable(LocalCash.CommunicTableRow[] communicRw)
    {
      return CommunicTableTableAdapter.Instance.Update(communicRw);
    }
  }
}