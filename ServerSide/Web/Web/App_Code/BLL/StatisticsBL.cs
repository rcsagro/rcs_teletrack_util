using System;
using System.Data;
using System.Data.SqlClient;
using RCS.Web.DAL;
using RCS.Web.Entities;

namespace RCS.Web.BLL
{
  /// <summary>
  /// ����������.
  /// </summary>
  public class StatisticsBL
  {
    private static CommonStatistics statistics;
    /// <summary>
    /// ����� ����������.
    /// </summary>
    public static CommonStatistics Statistics
    {
      get
      {
        if (statistics == null)
        {
          statistics = new CommonStatistics();
        }
        return statistics;
      }
    }

    private static ConnectDB provider
    {
      get
      {
        return ProvidersFactory.GetProvider<ConnectDB>();
      }
    }

    /// <summary>
    /// ��������� ����� �������� ����������.
    /// </summary>
    /// <returns>DateTime ��������� ����� �������� ����������.</returns>
    public static DateTime GetLastTeletrackConnection()
    {
      return provider.GetDateTimeValueSP("dbo.st_SelectDateLast_StatisticDataTT");
    }

    public static DataSet GetLastTranferedData()
    {
      return provider.GetDataSP("dbo.st_SelectTransfDB");
    }

    public static DataSet GetLastStaticTT()
    {
      return provider.GetDataSP("dbo.st_SelectStaticTT");
    }

    public static DataSet GetMonthDetalTT(int iSelectMonth, int iThrKb, int iThrDay, int iTltr)
    {
      Statistics.TtCount = 0;
      Statistics.ActiveTtCount = 0;
      Statistics.TtTrafficKb = 0;
      SqlParameter[] parArr = new SqlParameter[3];
      parArr[0] = new SqlParameter("@Y", SqlDbType.Int);
      parArr[1] = new SqlParameter("@M", SqlDbType.Int);
      parArr[2] = new SqlParameter("@TLTR", SqlDbType.Int);
      parArr[2].SqlValue = iTltr;
      if (iSelectMonth == 0)
      {
        parArr[0].SqlValue = DateTime.Now.Year;
        parArr[1].SqlValue = DateTime.Now.Month;
      }
      else
      {
        DateTime dtWork = DateTime.Now.AddMonths(-1);
        parArr[0].SqlValue = dtWork.Year;
        parArr[1].SqlValue = dtWork.Month;
      }
      DataSet dsWork = provider.GetDataSP("dbo.st_SelectStatTotalTT", parArr);
      DataTable tWork = dsWork.Tables[0];
      foreach (DataRow wRow in tWork.Rows)
      {
        if (wRow["Sum_kb"] != DBNull.Value)
        {
          Statistics.TtTrafficKb += Convert.ToInt32(wRow["Sum_kb"]);
        }
        if (Convert.ToInt32(wRow["Sum_kb"]) >= iThrKb)
        {
          wRow["Act"] = "��";
          Statistics.ActiveTtCount++;
        }
        Statistics.TtCount++;
      }
      Statistics.BActive = Statistics.ActiveTtCount >= iThrDay ? true : false;
      return dsWork;
    }

    public static DataSet GetMonthDetal(int iSelectMonth, int iThrKb, int iThrDay, int iClient)
    {
      Statistics.TtCount = 0;
      Statistics.ActiveTtCount = 0;
      Statistics.TtTrafficKb = 0;
      SqlParameter[] parArr = new SqlParameter[4];
      parArr[0] = new SqlParameter("@Y", SqlDbType.Int);
      parArr[1] = new SqlParameter("@M", SqlDbType.Int);
      parArr[2] = new SqlParameter("@threshold", SqlDbType.Int);
      parArr[3] = new SqlParameter("@CL", SqlDbType.Int);
      if (iSelectMonth == 0)
      {
        parArr[0].SqlValue = DateTime.Now.Year;
        parArr[1].SqlValue = DateTime.Now.Month;
      }
      else
      {
        DateTime dtWork = DateTime.Now.AddMonths(-1);
        parArr[0].SqlValue = dtWork.Year;
        parArr[1].SqlValue = dtWork.Month;
      }
      parArr[2].SqlValue = iThrKb;
      parArr[3].SqlValue = iClient;
      DataSet dsWork = provider.GetDataSP("dbo.st_SelectStatTotalMonthTT", parArr);
      DataTable tWork = dsWork.Tables[0];
      foreach (DataRow wRow in tWork.Rows)
      {
        if (wRow["Sum_kb"] != DBNull.Value)
        {
          Statistics.TtTrafficKb += Convert.ToInt32(wRow["Sum_kb"]);
        }
        if (Convert.ToInt32(wRow["Act"]) >= iThrDay)
        {
          Statistics.ActiveTtCount++;
        }
        Statistics.TtCount++;
      }
      return dsWork;
    }

    public static DataSet GetMonthTotal(int iSelectMonth, int iThrKb, int iThrDay, int iDlr)
    {
      Statistics.TtCount = 0;
      Statistics.ActiveTtCount = 0;
      Statistics.TtTrafficKb = 0;
      SqlParameter[] parArr = new SqlParameter[3];
      SqlParameter[] parArrM = new SqlParameter[4];
      parArr[0] = new SqlParameter("@Y", SqlDbType.Int);
      parArr[1] = new SqlParameter("@M", SqlDbType.Int);
      parArr[2] = new SqlParameter("@DLR", SqlDbType.Int);
      parArrM[0] = new SqlParameter("@Y", SqlDbType.Int);
      parArrM[1] = new SqlParameter("@M", SqlDbType.Int);
      parArrM[2] = new SqlParameter("@DLR", SqlDbType.Int);
      if (iSelectMonth == 0)
      {
        parArr[0].SqlValue = DateTime.Now.Year;
        parArr[1].SqlValue = DateTime.Now.Month;
      }
      else
      {
        DateTime dtWork = DateTime.Now.AddMonths(-1);
        parArr[0].SqlValue = dtWork.Year;
        parArr[1].SqlValue = dtWork.Month;
      }
      parArr[2].SqlValue = iDlr;
      DataSet dsWork = provider.GetDataSP("dbo.st_SelectStatTotalMonth", parArr);
      DataTable tWork = dsWork.Tables[0];
      for (int i = 0; i < 3; i++)
      {
        parArrM[i].SqlValue = parArr[i].SqlValue;
      }
      parArrM[3] = new SqlParameter("@threshold", SqlDbType.Int);
      parArrM[3].Value = iThrKb;
      DataSet dsAct = provider.GetDataSP("dbo.st_SelectStatActiveDayMonth", parArrM);
      DataTable tAct = dsAct.Tables[0];

      int cntAct;
      foreach (DataRow wRow in tWork.Rows)
      {
        DataRow[] aRows = tAct.Select(String.Format(
          "ID_User = {0}", wRow["Id_User"]));
        cntAct = 0;
        foreach (DataRow aRow in aRows)
        {
          if (Convert.ToInt32(aRow["CNT"]) >= iThrDay)
          {
            cntAct++;
          }
        }
        wRow["Act"] = cntAct;
        wRow["ActNo"] = Convert.ToInt32(wRow["Total_Trcs"]) - cntAct;
        Statistics.TtCount += Convert.ToInt32(wRow["Total_Trcs"]);
        Statistics.ActiveTtCount += cntAct;

        if (wRow["Sum_kb"] != DBNull.Value)
        {
          Statistics.TtTrafficKb += Convert.ToInt32(wRow["Sum_kb"]);
        }
      }
      return dsWork;
    }
  }
}
