using System;
using System.Collections.Generic;
using System.ComponentModel;
using RCS.Web.DAL;
using RCS.Web.Entities;

namespace RCS.Web.BLL
{
  /// <summary>
  /// ��������� �������� �������� ������������ ���� � ����� - 
  /// ���������� � ��������� ���������� ��� ����������� � ��������� ���������.
  /// </summary>
  [DataObject]
  public partial class UserInfoBL
  {
    /// <summary>
    /// ������������ ������ ��������� �������� ���������� � ���������
    /// ����������-�������. 
    /// </summary>
    /// <param name="idUser">������������� ������������.</param>
    /// <param name="idClient">������������� ����������-�������.</param>
    /// <param name="startRowIndex">��������� ������ ������ �������.</param>
    /// <param name="maximumRows">���-�� ����� �������.</param>
    /// <returns>������ ��������� �������� ���������� � ����������.</returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static List<UserDetailsLinkedState> SelectTeletrackLinkedState(
      int idUser, int idClient, int startRowIndex, int maximumRows)
    {
      return ProvidersFactory.GetProvider<UserInfoProvider>().
        GetTeletrackLinkedState(idUser, idClient, startRowIndex, maximumRows);
    }

    /// <summary>
    /// ���-�� ���������� � ������� ������������.
    /// </summary>
    /// <param name="idUser">������������� ������������.</param>
    /// <param name="idClient">�� ������������, ��������� ASP ��� ������ ������.</param>
    /// <returns>���-�� ����������.</returns>
    public static int SelectTeletrackCount(int idUser, int idClient)
    {
      return ProvidersFactory.GetProvider<TeletrackInfoProvider>().
        GetRowCount(idUser);
    }

    /// <summary>
    /// ������������ ������ ��������� �������� ����������� � ��������� ���������.
    /// </summary>
    /// <param name="idUser">������������� ������������.</param>
    /// <param name="idTeletrack">������������� ���������.</param>
    /// <returns>������ ��������� �������� ������������ � ���������.</returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static List<UserDetailsLinkedState> SelectClientLinkedState(int idUser, int idTeletrack)
    {
      return ProvidersFactory.GetProvider<UserInfoProvider>().
        GetClientLinkedState(idUser, idTeletrack);
    }

    /// <summary>
    /// ���������� �������� ��������� � ����������.
    /// </summary>
    /// <param name="idClient">������������� ����������-�������.</param>
    /// <param name="linked">������� �������� ��������.</param>
    /// <param name="original_DetailID">������������� ���������.</param>
    /// <param name="original_CommunicID">������������� � ������� CommunicTable.</param>
    /// <param name="original_Linked">��������� �������� ��������.</param>
    [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
    public static void UpdateLinkedTeletracks(int idClient, bool linked,
      int original_DetailID, int original_CommunicID, bool original_Linked)
    {
      if (original_Linked != linked)
      {
        CommunicTableProvider provider = ProvidersFactory.GetProvider<CommunicTableProvider>();
        if (linked)
        {
          provider.Insert(original_DetailID, idClient);
        }
        else
        {
          provider.Delete(original_CommunicID);
        }
      }
    }

    /// <summary>
    /// ���������� �������� ���������� � ���������.
    /// </summary>
    /// <param name="idTeletrack">������������� ���������.</param>
    /// <param name="linked">������� �������� ��������.</param>
    /// <param name="original_DetailID">������������� ����������-�������.</param>
    /// <param name="original_CommunicID">������������� � ������� CommunicTable.</param>
    /// <param name="original_Linked">��������� �������� ��������.</param>
    [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
    public static void UpdateLinkedClients(int idTeletrack, bool linked,
      int original_DetailID, int original_CommunicID, bool original_Linked)
    {
      if (original_Linked != linked)
      {
        CommunicTableProvider provider = ProvidersFactory.GetProvider<CommunicTableProvider>();
        if (linked)
        {
          provider.Insert(idTeletrack, original_DetailID);
        }
        else
        {
          provider.Delete(original_CommunicID);
        }
      }
    }
      /// <summary>
    /// �������� ���� ����������� ���������
      /// </summary>
    /// <param name="idUser">������������� ������������.</param>
    /// <param name="idTeletrack">������������� ���������.</param>
    public static void SetAllDicpatchLinkedToTeletrack(int idUser,int idTeletrack)
      {
          List<UserDetailsLinkedState> linkStates = UserInfoBL.SelectClientLinkedState(idUser, idTeletrack);
          if (linkStates != null)
          {
              CommunicTableProvider provider = ProvidersFactory.GetProvider<CommunicTableProvider>();
              foreach (UserDetailsLinkedState linkState in linkStates)
              {
                  if (!linkState.Linked)
                  {
                      provider.Insert(idTeletrack, linkState.DetailID);
                  }
              }
          }
      }


      /// <summary>
    /// ������� ���� ���������� �� ����������
    /// </summary>
    /// <param name="idUser">������������� ������������.</param>
    /// <param name="idTeletrack">������������� ���������.</param>
    public static void ResetAllDicpatchLinkedToTeletrack(int idUser, int idTeletrack)
      {
          List<UserDetailsLinkedState> linkStates = UserInfoBL.SelectClientLinkedState(idUser, idTeletrack);
          ResetLinks(linkStates);
      }

      private static void ResetLinks(List<UserDetailsLinkedState> linkStates)
      {
          if (linkStates != null)
          {
              CommunicTableProvider provider = ProvidersFactory.GetProvider<CommunicTableProvider>();
              foreach (UserDetailsLinkedState linkState in linkStates)
              {
                  if (linkState.Linked && linkState.CommunicID != null)
                  {
                      provider.Delete(linkState.CommunicID ?? 0);
                  }
              }
          }
      }

      /// <summary>
    /// �������� ���� ���������� � ����������
    /// </summary>
    /// <param name="idUser">������������� ������������.</param>
    /// <param name="idTeletrack">������������� ���������.</param> 
    public static void SetAllTeletracksLinkedTopDicpatch(int idUser, int idClient)
    {
        List<UserDetailsLinkedState> linkStates = UserInfoBL.SelectTeletrackLinkedState(idUser, idClient, 0, SelectTeletrackCount(idUser, 0));
        if (linkStates != null)
        {
            CommunicTableProvider provider = ProvidersFactory.GetProvider<CommunicTableProvider>();
            foreach (UserDetailsLinkedState linkState in linkStates)
            {
                if (!linkState.Linked)
                {
                    provider.Insert(linkState.DetailID, idClient);
                }
            }
        }
    }


    /// <summary>
    /// ������� ���� ���������� �� ����������
    /// </summary>
    /// <param name="idUser">������������� ������������.</param>
    /// <param name="idTeletrack">������������� ���������.</param>
    public static void ResetAllTeletracksLinkedToDicpatch(int idUser, int idTeletrack)
    {
        List<UserDetailsLinkedState> linkStates = UserInfoBL.SelectTeletrackLinkedState(idUser, idTeletrack, 0, SelectTeletrackCount(idUser, 0));
        ResetLinks(linkStates);
    }

  }
}