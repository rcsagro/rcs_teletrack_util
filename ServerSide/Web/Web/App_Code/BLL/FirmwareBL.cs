﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Web.SessionState;
using RCS.OnlineServer.Core.Model;
using RCS.OnlineServer.Core.Repositories;
using RCS.OnlineServer.Core.Services;

namespace RCS.Web.BLL
{
  /// <summary>
  /// Изменение прошивки
  /// </summary>
  [DataObject]
  public class FirmwareBL
  {
    /// <summary>
    /// Выборка страницы данных прошивок.
    /// </summary>
    /// <param name="sortColumns">Список столбцов участвующих в сортировке.</param>
    /// <param name="startRowIndex">Стартовый индекс выборки.</param>
    /// <param name="maximumRows">Максимальное кол-во строк выборки.</param>
    /// <returns>Список прошивок.</returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static List<Firmware> GetFirmwares(string sortColumns, int startRowIndex, int maximumRows)
    {
      return GetFirmwareRepository().GetFirmwarePage(
        sortColumns,
        startRowIndex,
        maximumRows);
    }

    /// <summary>
    /// Кол-во прошивок в БД.
    /// </summary>
    /// <returns>Кол-во прошивок.</returns>
    public static int GetFirmwaresCount()
    {
      return GetFirmwareRepository().GetFirmwareCount();
    }

    /// <summary>
    /// Выборка страницы лога изменений репозитария прошивок.
    /// </summary>
    /// <param name="sortColumns">Список столбцов участвующих в сортировке.</param>
    /// <param name="startRowIndex">Стартовый индекс выборки.</param>
    /// <param name="maximumRows">Максимальное кол-во строк выборки.</param>
    /// <returns>Лог изменений.</returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static List<FirmwareChanges> GetChanges(string sortColumns, int startRowIndex, int maximumRows)
    {
      return GetFirmwareRepository().GetChangesPage(
        sortColumns,
        startRowIndex,
        maximumRows);
    }

    /// <summary>
    /// Выборка страницы лога изменений репозитария прошивок.
    /// </summary>
    /// <param name="sortColumns">Список столбцов участвующих в сортировке.</param>
    /// <param name="startRowIndex">Стартовый индекс выборки.</param>
    /// <param name="maximumRows">Максимальное кол-во строк выборки.</param>
    /// <returns>Лог изменений.</returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static List<FirmwareChanges> GetChangesById(string sortColumns, int startRowIndex, int maximumRows,
      long firmwareId)
    {
      return GetFirmwareRepository().GetChangesPageById(
        firmwareId,
        sortColumns,
        startRowIndex,
        maximumRows);
    }


    /// <summary>
    /// Кол-во строк лога изменений репозитария прошивок.
    /// </summary>
    /// <returns>Кол-во строк лога.</returns>
    public static int GetChangesCount()
    {
      return GetFirmwareRepository().GetChangesCount();
    }

    /// <summary>
    /// Кол-во строк лога изменений репозитария прошивок.
    /// </summary>
    /// <returns>Кол-во строк лога.</returns>
    public static int GetChangesCountById(long firmwareId)
    {
      return GetFirmwareRepository().GetChangesCountById(firmwareId);
    }

    /// <summary>
    /// Выборка команды по идентификатору.
    /// </summary>
    /// <param name="ID_Command">Идентификатор команды.</param>
    /// <returns>Command</returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static Firmware GetFirmwareById(long id)
    {
      return GetFirmwareRepository().GetById(id);
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
    public static void Update(Firmware firmware)
    {
      GetFirmwareRepository().Update(
        firmware,
        FormsAuthHelper.GetUserName(),
        FormsAuthHelper.GetUserAddress());
    }

    [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
    public static void Insert(FirmwareSource firmware)
    {
      GetFirmwareRepository().Insert(
         firmware,
         FormsAuthHelper.GetUserName(),
         FormsAuthHelper.GetUserAddress());
    }

    private static IFirmwareRepository GetFirmwareRepository()
    {
      return new RepositoryFactory(AppSettings.ConnectionString, AppSettings.CommandTimeout)
        .GetFirmwareRepository();
    }

    private const string firmware_text = "FirmwareText";
    private const string firmware_file_name = "FirmwareFileName";

    public static void HandleUploadedFile(string file, HttpSessionState session)
    {
      string tmpFirmware;
      using (StreamReader sr = new StreamReader(file, Encoding.Default))
      {
        tmpFirmware = sr.ReadToEnd();
      }

      if (!FirmwareService.IsTextFirmwareChanges(tmpFirmware))
      {
        throw new ArgumentException("Загруженный файл не является изменением прошивки");
      }

      session.Add(firmware_text, tmpFirmware);
      session.Add(firmware_file_name, Path.GetFileName(file));
    }

    public static void CancelUpload(HttpSessionState session)
    {
      session.Remove(firmware_text);
      session.Remove(firmware_file_name);
    }

    public static bool IsFileUploaded(HttpSessionState session)
    {
      return session[firmware_file_name] != null;
    }

    public static object GetFirmwareFileName(HttpSessionState session)
    {
      return GetCachedValue(session, firmware_file_name);
    }

    public static object GetFirmwareText(HttpSessionState session)
    {
      return GetCachedValue(session, firmware_text);
    }

    private static object GetCachedValue(HttpSessionState session, string key)
    {
      object tmp = session[key];
      if (tmp == null)
      {
        throw new Exception("В сессионном кэше отсутсвует элемент " + key);
      }
      return tmp;
    }
  }
}