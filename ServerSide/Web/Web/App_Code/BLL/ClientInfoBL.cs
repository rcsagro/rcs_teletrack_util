﻿using System;
using System.ComponentModel;
using LocalCashTableAdapters;
using RCS.Web.DAL;
using RCS.Web.Entities;

namespace RCS.Web.BLL
{
  /// <summary>
  /// Диспетчер-клиент.
  /// </summary>
  [DataObject]
  public class ClientInfoBL
  {
    /// <summary>
    /// Возвращает объект(таблицу) ClientInfoDataTable.
    /// </summary>
    /// <returns>LocalCash.ClientInfoDataTable.</returns>
    public static LocalCash.ClientInfoDataTable GetClientInfo()
    {
      return ClientInfoTableAdapter.Instance.GetData();
    }

    /// <summary>
    /// Добавляет в БД нового диспетчера-клиента.
    /// </summary>
    /// <param name="client">Описание диспетчера-клиента.</param>
    [DataObjectMethodAttribute(DataObjectMethodType.Insert, true)]
    public void Insert(ClientInfo client)
    {
      LocalCash cash_ds = new LocalCash();

      ClientInfoTableAdapter.Instance.Fill(cash_ds.ClientInfo);
      TeletrackInfoTableAdapter.Instance.Fill(cash_ds.TeletrackInfo);
      CommunicTableTableAdapter taCommunicTable = CommunicTableTableAdapter.Instance;
      taCommunicTable.Fill(cash_ds.CommunicTable);

      LocalCash.ClientInfoRow newCiRow = cash_ds.ClientInfo.NewClientInfoRow();
      newCiRow.LoginCl = client.LoginCl;
      newCiRow.Password = client.Password;
      newCiRow.InNetwork = false;
      newCiRow.ID_Packet = 0;
      newCiRow.DTimeDeliveredPack = DateTime.Now;
      newCiRow.ID_LastPackInSrvDB = 0;
      newCiRow.ID_User = client.ID_User;
      newCiRow.Describe = client.Describe;

      cash_ds.ClientInfo.AddClientInfoRow(newCiRow);
      ClientInfoTableAdapter.Instance.Update(cash_ds.ClientInfo);

      //Проверяем есть ли у пользователя телетреки.
      LocalCash.TeletrackInfoRow[] teletrackInfoRows =
        (LocalCash.TeletrackInfoRow[])cash_ds.TeletrackInfo.Select(
          string.Format("ID_User = {0}", client.ID_User));

      if (teletrackInfoRows.Length > 0)
      {
        LocalCash.CommunicTableRow newCtRow;
        // Заполняем CommunicTable
        foreach (LocalCash.TeletrackInfoRow tiRow in teletrackInfoRows)
        {
          newCtRow = cash_ds.CommunicTable.NewCommunicTableRow();
          newCtRow.ID_Client = newCiRow.ID_Client;
          newCtRow.ID_Teletrack = tiRow.ID_Teletrack;
          cash_ds.CommunicTable.AddCommunicTableRow(newCtRow);
        }

        taCommunicTable.Update(cash_ds.CommunicTable);
        ProvidersFactory.GetProvider<VersionControlProvider>().
          IncrementVersion("CommunicTable");
      }
      //Сохраняем идентификатор для редиректа.
      System.Web.HttpContext.Current.Session[
        SessionKeyNames.LAST_INSERTED_CLIENT_ID] = newCiRow.ID_Client;
    }

    /// <summary>
    /// Вносит изменения в запись диспетчера.
    /// </summary>
    /// <param name="client">Описание диспетчера-клиента.</param>
    [DataObjectMethodAttribute(DataObjectMethodType.Update, true)]
    public static void Update(ClientInfo client)
    {
      LocalCash cash_ds = new LocalCash();

      ClientInfoTableAdapter ta = ClientInfoTableAdapter.Instance;
      ta.Fill(cash_ds.ClientInfo);

      LocalCash.ClientInfoRow clRw =
        cash_ds.ClientInfo.FindByID_Client(client.ID_Client);
      if (clRw != null)
      {
        clRw.LoginCl = client.LoginCl;
        clRw.Password = client.Password;
        clRw.Describe = client.Describe;
        ta.Update(clRw);
      }
    }

    /// <summary>
    /// Удаляет диспетчера (сервис).
    /// </summary>
    /// <param name="client">Описание диспетчера-клиента.</param>
    [DataObjectMethodAttribute(DataObjectMethodType.Delete, true)]
    public static void Delete(ClientInfo client)
    {
      LocalCash cash_ds = new LocalCash();

      ClientInfoTableAdapter ta = ClientInfoTableAdapter.Instance;
      ta.Fill(cash_ds.ClientInfo);

      LocalCash.ClientInfoRow clRw = cash_ds.ClientInfo.FindByID_Client(client.ID_Client);
      if (clRw != null)
      {
        clRw.Delete();
        ta.Update(clRw);
        ProvidersFactory.GetProvider<VersionControlProvider>().
          IncrementVersion("CommunicTable");
      }
    }

    /// <summary>
    /// Поиск диспетчера по идентификатору.
    /// </summary>
    /// <param name="id_client">Идентификатор диспетчера.</param>
    /// <returns>Описание диспетчера ClientInfo.</returns>
    [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
    public static ClientInfo SelectByID(int id_client)
    {
      return ProvidersFactory.GetProvider<ClientInfoProvider>().GetByID(id_client);
    }

    /// <summary>
    /// Поиск диспетчера по логину.
    /// </summary>
    /// <param name="login">Логин диспетчера.</param>
    /// <returns>Описание диспетчера ClientInfo.</returns>
    public static ClientInfo GetByLogin(string login)
    {
      return ProvidersFactory.GetProvider<ClientInfoProvider>().GetByLogin(login);
    }

    /// <summary>
    /// Проверка существования диспетчера с заданным логином.
    /// </summary>
    /// <param name="sLogin">Логин диспетчера.</param>
    /// <returns>true - диспетчер существует.</returns>
    public static bool ClientExists(string sLogin)
    {
      return GetByLogin(sLogin) != null;
    }
  }
}