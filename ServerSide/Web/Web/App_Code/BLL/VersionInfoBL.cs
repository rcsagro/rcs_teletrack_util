using System;
using System.Reflection;
using RCS.Lic;
using RCS.Lic.Entities;
using RCS.Web.DAL;
using RCS.Web.Entities;

namespace RCS.Web.BLL
{
  /// <summary>
  /// ���������� � ������ ����� � ��.
  /// </summary>
  public class VersionInfoBL
  {
    private static ILicDetector _licDetector;

    static VersionInfoBL()
    {
      if (LicDetectorFactory.Active)
      {
        _licDetector = new LicDetectorFactory().CreateDetector();
      }
    }

    /// <summary>
    /// ���������� � ��������, ����-�����. 
    /// </summary>
    public static LicInfo Lic
    {
      get
      {
        LicInfo result = new LicInfo();
        result.TtCount = TeletrackInfoBL.GetTtCount();
        result.IsSubsystemActive = LicDetectorFactory.Active;
        if (LicDetectorFactory.Active)
        {
          Licence lic = _licDetector.GetLicence();
          result.LicNumber = lic.LicNumber;
          result.MaxPermittedTtCount = lic.MaxPermittedTtCount;
          result.Description = lic.Description;
        }
        else
        {
          result.Description = "��� �����������.";
        }
        return result;
      }
    }

    /// <summary>
    /// ������ �����.
    /// </summary>
    public static string SiteVersion
    {
      get
      {
        return Assembly.GetExecutingAssembly().GetName().Version.ToString();
      }
    }

    /// <summary>
    /// ������ ���� ������.
    /// </summary>
    public static string DbVersion
    {
      get
      {
        string version = ProvidersFactory.GetProvider<AdminProvider>().GetDbVersion();
        return String.IsNullOrEmpty(version) ? "��� ������" : version;
      }
    }
  }
}