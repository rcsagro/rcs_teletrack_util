
namespace RCS.Web
{
  /// <summary>
  /// ��������� ���� ������ ���������� �������� � 
  /// ������� Session.
  /// </summary>
  public class SessionKeyNames
  {
    /// <summary>
    /// ������������ ����� ��� �������������� ������, ��� ������� ��� �������� ���� �� ����.
    /// </summary>
    public const string WORKING_DEALER_ID = "WORKING_DEALER_ID";
    /// <summary>
    /// ������������ ����� ��� �������������� ������ ��� ���������� �������-����������.
    /// </summary>
    public const string LAST_INSERTED_CLIENT_ID = "LAST_INSERTED_CLIENT_ID";
    /// <summary>
    /// ������������ ����� ��� �������������� ������ ��� ���������� ���������.
    /// </summary>
    public const string LAST_INSERTED_TT_ID = "LAST_INSERTED_TT_ID";
  }
}