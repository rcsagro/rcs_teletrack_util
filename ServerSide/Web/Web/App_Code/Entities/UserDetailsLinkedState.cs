using System.ComponentModel;

namespace RCS.Web.Entities
{
  /// <summary>
  /// ���������� � ��������� �������� �������� ������������
  /// (TeletrackInfo, ClientInfo) �� ��������� ���� � �����.
  /// ��������, ��������� ��� �������� ������ TeletrackInfo
  /// � ClientInfo.
  /// </summary>
  public class UserDetailsLinkedState
  {
    private int detailID;
    /// <summary>
    /// ������������� �������.
    /// </summary>
    [DataObjectField(true)]
    public int DetailID
    {
      get { return detailID; }
      set { detailID = value; }
    }

    private string detailLogin;
    /// <summary>
    /// ����� �������.
    /// </summary>
    [DataObjectField(true)]
    public string DetailLogin
    {
      get { return detailLogin; }
      set { detailLogin = value; }
    }

    private bool linked;
    /// <summary>
    /// ���� �������� � �������.
    /// </summary>
    [DataObjectField(true)]
    public bool Linked
    {
      get { return linked; }
      set { linked = value; }
    }

    private int? communicID;
    /// <summary>
    /// ������������� ������ �������� � ������� CommunicTable.
    /// </summary>
    [DataObjectField(true)]
    public int? CommunicID
    {
      get { return communicID; }
      set { communicID = value; }
    }

    /// <summary>
    /// �����������.
    /// </summary>
    public UserDetailsLinkedState()
    {
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="detailID">������������� �������.</param>
    /// <param name="detailLogin">����� �������.</param>
    /// <param name="linked">���� �������� � �������.</param>
    /// <param name="communicID">������������� ������ �������� � ������� CommunicTable.</param>
    public UserDetailsLinkedState(int detailID, string detailLogin, bool linked, int? communicID)
    {
      this.detailID = detailID;
      this.detailLogin = detailLogin;
      Linked = linked;
      this.communicID = communicID;
    }
  }
}