using System;

namespace RCS.Web.Entities
{
  /// <summary>
  /// �������� ������� ���������.
  /// </summary>
  public class Command
  {
    /// <summary>
    /// ������������� � ������� Command.
    /// </summary>
    public int Id
    {
      get { return _id; }
      set { _id = value; }
    }
    private int _id;

    /// <summary>
    /// ��� �������.
    /// </summary>
    public string CommandType
    {
      get { return _commandType; }
      set { _commandType = value; }
    }
    private string _commandType;

    /// <summary>
    /// ��������� ���������� �������. 
    /// </summary>
    public A1CmdHandlingStatus HandlingStatus
    {
      get { return _handlingStatus; }
      set { _handlingStatus = value; }
    }
    private A1CmdHandlingStatus _handlingStatus;

    public string HandlingStatusDescr
    {
      get
      {
        switch (_handlingStatus)
        {
          case A1CmdHandlingStatus.Done:
            return "���������";
          case A1CmdHandlingStatus.TeletrackAnswered:
            return "���������� ����������";
          case A1CmdHandlingStatus.Pending:
            return "��������";
          default:
            throw new ApplicationException(
              "�� ����������� ������ ��������� ��������� A1CmdHandlingStatus");
        }
      }
    }

    /// <summary>
    /// ���� ����������� ������� � ���������� ����������.
    /// </summary>
    public bool Approved
    {
      get { return _approved; }
      set { _approved = value; }
    }
    private bool _approved;

    /// <summary>
    /// ���� ������� ������� �� ����������.
    /// </summary>
    public DateTime DateEntry
    {
      get { return _dateEntry; }
      set { _dateEntry = value; }
    }
    private DateTime _dateEntry;

    /// <summary>
    /// �������� ����-�����, �� ������� ����� ��������� ������� ���������.
    /// </summary>
    public DateTime DateExpire
    {
      get { return _dateExpire; }
      set { _dateExpire = value; }
    }
    private DateTime _dateExpire;

    /// <summary>
    /// IP ����������.
    /// </summary>
    public string DispatcherIP
    {
      get { return _dispatcherIP; }
      set { _dispatcherIP = value; }
    }
    private string _dispatcherIP;

    /// <summary>
    /// ����� ����������.
    /// </summary>
    public string DispatcherLogin
    {
      get { return _dispatcherLogin; }
      set { _dispatcherLogin = value; }
    }
    private string _dispatcherLogin;

    /// <summary>
    /// �������� ����������.
    /// </summary>
    public string DispatcherDescription
    {
      get { return _dispatcherDescription; }
      set { _dispatcherDescription = value; }
    }
    private string _dispatcherDescription;

    /// <summary>
    /// ����� ���������.
    /// </summary>
    public string TeletrackLogin
    {
      get { return _teletrackLogin; }
      set { _teletrackLogin = value; }
    }
    private string _teletrackLogin;

    /// <summary>
    /// ��� ������ ���������.
    /// </summary>
    public byte? TeletrackAnswerCode
    {
      get { return _teletrackAnswerCode; }
      set { _teletrackAnswerCode = value; }
    }
    private byte? _teletrackAnswerCode;

    /// <summary>
    /// ����� ������� ����������.
    /// </summary>
    public string DispatcherCmdText
    {
      get { return _dispatcherCmdText; }
      set { _dispatcherCmdText = value; }
    }
    private string _dispatcherCmdText;

    /// <summary>
    /// ����� ������ ���������.
    /// </summary>
    public string TeletrackAnswerText
    {
      get { return _teletrackAnswerText; }
      set { _teletrackAnswerText = value; }
    }
    private string _teletrackAnswerText;

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="id">������������� � ������� Command.</param>
    /// <param name="commandType">��� �������.</param>
    /// <param name="handlingStatus">��������� ���������� �������.</param>
    /// <param name="approved">���� ����������� ������� � ���������� ����������.</param>
    /// <param name="dateEntry">���� ������� ������� �� ����������.</param>
    /// <param name="dateExpire">�������� ����-�����, �� ������� ����� ��������� ������� ���������.</param>
    /// <param name="dispatcherIP">IP ����������.</param>
    /// <param name="dispatcherLogin">����� ����������.</param>
    /// <param name="dispatcherDescription">�������� ����������.</param>
    /// <param name="teletrackLogin"����� ���������.>����� ���������.</param>
    /// <param name="teletrackAnswerCode">��� ������ ���������.</param>
    /// <param name="dispatcherCmdText">����� ������� ����������.</param>
    /// <param name="teletrackAnswerText">����� ������ ���������.</param>
    public Command(int id, string commandType, A1CmdHandlingStatus handlingStatus, bool approved,
      DateTime dateEntry, DateTime dateExpire, string dispatcherIP, string dispatcherLogin,
      string dispatcherDescription, string teletrackLogin, byte? teletrackAnswerCode,
      string dispatcherCmdText, string teletrackAnswerText)
    {
      _id = id;
      _commandType = commandType;
      _handlingStatus = handlingStatus;
      _approved = approved;
      _dateEntry = dateEntry;
      _dateExpire = dateExpire;
      _dispatcherIP = dispatcherIP;
      _dispatcherLogin = dispatcherLogin;
      _dispatcherDescription = dispatcherDescription;
      _teletrackLogin = teletrackLogin;
      _teletrackAnswerCode = teletrackAnswerCode;
      _dispatcherCmdText = dispatcherCmdText;
      _teletrackAnswerText = teletrackAnswerText;
    }

    public Command()
    {
    }
  }
}