
namespace RCS.Web.Entities
{
  /// <summary>
  /// ���������� � ��������.
  /// </summary>
  public class LicInfo
  {
    private byte licNumber;
    /// <summary>
    /// ����� ��������.
    /// </summary>
    public byte LicNumber
    {
      get { return licNumber; }
      set { licNumber = value; }
    }

    private int maxPermittedTtCount;
    /// <summary>
    /// ����������� ����������� ���-�� ���������� ��� ������ ��������.
    /// </summary>
    public int MaxPermittedTtCount
    {
      get { return maxPermittedTtCount; }
      set { maxPermittedTtCount = value; }
    }

    private int ttCount;
    /// <summary>
    /// ���-�� ���������� � ����.
    /// </summary>
    public int TtCount
    {
      get { return ttCount; }
      set { ttCount = value; }
    }

    private bool isSubsystemActive;
    /// <summary>
    /// ������� �� ���������� ��������������.
    /// </summary>
    public bool IsSubsystemActive
    {
      get { return isSubsystemActive; }
      set { isSubsystemActive = value; }
    }

    /// <summary>
    /// �������� �� ������������ ����������.
    /// </summary>
    public bool LicenseViolationOccurred
    {
      get
      {
        return ((isSubsystemActive) && (maxPermittedTtCount < ttCount));
      }
    }

    /// <summary>
    /// ��������� �� ��������(���-�� ����������).
    /// </summary>
    public bool LicenseExhausted
    {
      get
      {
        return ((isSubsystemActive) && (maxPermittedTtCount == ttCount));
      }
    }

    /// <summary>
    /// ��������.
    /// </summary>
    public string Description
    {
      get { return _description; }
      set { _description = value; }
    }
    private string _description;
  }
}