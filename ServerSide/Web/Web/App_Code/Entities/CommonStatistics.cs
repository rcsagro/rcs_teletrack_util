
namespace RCS.Web.Entities
{
  /// <summary>
  /// ����� ����������.
  /// </summary>
  public class CommonStatistics
  {
    private int ttCount;
    /// <summary>
    /// ���-�� ����������.
    /// </summary>
    public int TtCount
    {
      get { return ttCount; }
      set { ttCount = value; }
    }

    private int activeTtCount;
    /// <summary>
    /// ���-�� �������� ����������.
    /// </summary>
    public int ActiveTtCount
    {
      get { return activeTtCount; }
      set { activeTtCount = value; }
    }

    private int ttTrafficKb;
    /// <summary>
    /// ������. 
    /// </summary>
    public int TtTrafficKb
    {
      get { return ttTrafficKb; }
      set { ttTrafficKb = value; }
    }

    private bool bActive = false;
    public bool BActive
    {
      get { return bActive; }
      set { bActive = value; }
    }
  }
}