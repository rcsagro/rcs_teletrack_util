using System;
using System.ComponentModel;

namespace RCS.Web.Entities
{
  /// <summary>
  /// �������� ��������.
  /// </summary>
  public class TeletrackInfo
  {
    int id_teletrack;
    /// <summary>
    /// ������������� ���������.
    /// </summary>
    [DataObjectField(true)]
    public int ID_Teletrack
    {
      get { return id_teletrack; }
      set { id_teletrack = value; }
    }

    int id_user;
    /// <summary>
    /// ������������� ������������.
    /// </summary>
    [DataObjectField(false)]
    public int ID_User
    {
      get { return id_user; }
      set { id_user = value; }
    }

    string loginTT;
    /// <summary>
    /// ����� ���������.
    /// </summary>
    [DataObjectField(false)]
    public string LoginTT
    {
      get { return loginTT; }
      set { loginTT = value; }
    }

    string passwordTT;
    /// <summary>
    /// ������ ���������.
    /// </summary>
    [DataObjectField(false)]
    public string PasswordTT
    {
      get { return passwordTT; }
      set { passwordTT = value; }
    }

    int timeout;
    /// <summary>
    /// �������.
    /// </summary>
    [DataObjectField(false)]
    public int Timeout
    {
      get { return timeout; }
      set { timeout = value; }
    }

    string gsmimei;
    /// <summary>
    /// GSMIMEI.
    /// </summary>
    [DataObjectField(false)]
    public string GSMIMEI
    {
      get { return gsmimei; }
      set { gsmimei = value; }
    }

    private DateTime? dTimeLastPack;
    /// <summary>
    /// ����� ��������� ���������� ������.
    /// </summary>
    [DataObjectField(false)]
    public DateTime? DTimeLastPack
    {
      get { return dTimeLastPack; }
      set { dTimeLastPack = value; }
    }

    /// <summary>
    /// �����������.
    /// </summary>
    public TeletrackInfo()
    {
      timeout = 1440;
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="id_teletrack"> ������������� ���������.</param>
    /// <param name="id_user">������������� ������������.</param>
    /// <param name="loginTT">����� ���������.</param>
    /// <param name="passwordTT">������ ���������.</param>
    /// <param name="timeout">�������.</param>
    /// <param name="gsmimei">GSMIMEI.</param>
    /// <param name="dTimeLastPack">����� ��������� ���������� ������.</param>
    public TeletrackInfo(int id_teletrack, int id_user, string loginTT,
      string passwordTT, int timeout, string gsmimei, DateTime? dTimeLastPack)
    {
      this.id_teletrack = id_teletrack;
      this.id_user = id_user;
      this.loginTT = loginTT;
      this.passwordTT = passwordTT;
      this.timeout = timeout;
      this.gsmimei = gsmimei;
      this.dTimeLastPack = dTimeLastPack;
    }
  }
}
