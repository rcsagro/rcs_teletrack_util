using System;
using System.ComponentModel;

namespace RCS.Web.Entities
{
  /// <summary>
  /// �������� ���������.
  /// </summary>
  public class ClientInfo
  {
    private int id_Client;
    /// <summary>
    /// ������������� ����������.
    /// </summary>
    [DataObjectField(true)]
    public int ID_Client
    {
      get { return id_Client; }
      set { id_Client = value; }
    }

    private string loginCl;
    /// <summary>
    /// �����.
    /// </summary>
    [DataObjectField(true)]
    public string LoginCl
    {
      get { return loginCl; }
      set { loginCl = value; }
    }

    private string password;
    /// <summary>
    /// ������.
    /// </summary>
    [DataObjectField(true)]
    public string Password
    {
      get { return password; }
      set { password = value; }
    }

    private bool inNetwork;
    /// <summary>
    /// ��������� �� ������.
    /// </summary>
    [DataObjectField(true)]
    public bool InNetwork
    {
      get { return inNetwork; }
      set { inNetwork = value; }
    }

    private long id_Packet;
    /// <summary>
    /// ������������� ���������� ����������� �������� ������ ������.
    /// </summary>
    [DataObjectField(true)]
    public long ID_Packet
    {
      get { return id_Packet; }
      set { id_Packet = value; }
    }

    private DateTime dTimeDeliveredPack;
    /// <summary>
    /// ����-����� ��������� ���������� ������.
    /// </summary>
    [DataObjectField(true)]
    public DateTime DTimeDeliveredPack
    {
      get { return dTimeDeliveredPack; }
      set { dTimeDeliveredPack = value; }
    }

    private long id_LastPackInSrvDB;
    /// <summary>
    /// ������������� ���������� ������ ��� ������� �������.
    /// </summary>
    [DataObjectField(true)]
    public long ID_LastPackInSrvDB
    {
      get { return id_LastPackInSrvDB; }
      set { id_LastPackInSrvDB = value; }
    }

    private int id_User;
    /// <summary>
    /// ������������� ������������.
    /// </summary>
    [DataObjectField(true)]
    public int ID_User
    {
      get { return id_User; }
      set { id_User = value; }
    }

    private string describe;
    /// <summary>
    /// ��������.
    /// </summary>
    [DataObjectField(true)]
    public string Describe
    {
      get { return describe; }
      set { describe = value; }
    }

    /// <summary>
    /// �����������.
    /// </summary>
    public ClientInfo()
    {
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="id_Client">������������� ����������.</param>
    /// <param name="loginCl">�����.</param>
    /// <param name="password">������.</param>
    /// <param name="inNetwork">��������� �� ������.</param>
    /// <param name="id_Packet">������������� ���������� ����������� �������� ������ ������.</param>
    /// <param name="dTimeDeliveredPack">����-����� ��������� ���������� ������.</param>
    /// <param name="id_LastPackInSrvDB">������������� ���������� ������ ��� ������� �������.</param>
    /// <param name="id_User">������������� ������������.</param>
    /// <param name="describe">��������.</param>
    public ClientInfo(int id_Client, string loginCl, string password, bool inNetwork,
      long id_Packet, DateTime dTimeDeliveredPack, long id_LastPackInSrvDB, int id_User,
      string describe)
    {
      this.id_Client = id_Client;
      this.loginCl = loginCl;
      this.password = password;
      this.inNetwork = inNetwork;
      this.id_Packet = id_Packet;
      this.dTimeDeliveredPack = dTimeDeliveredPack;
      this.id_LastPackInSrvDB = id_LastPackInSrvDB;
      this.id_User = id_User;
      this.describe = describe;
    }
  }
}
