﻿<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="false" CodeFile="login.aspx.cs"
  Inherits="RCS.Web.UI.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Аутентификация</title>
</head>
<body style="background-color: #99ccff">
  <form id="form1" runat="server">
    <div style="text-align: center">
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <center>
        <asp:Login ID="ctrlLogin" runat="server" BackColor="#EFF3FB" BorderColor="#B5C7DE"
          BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" FailureText="Неверный логин или пароль. Попробуйте еще раз"
          Font-Names="Verdana" Font-Size="Medium" ForeColor="#333333" LoginButtonText="Войти"
          OnAuthenticate="Login1_Authenticate" PasswordLabelText="Пароль:" PasswordRequiredErrorMessage="Пароль не задан"
          RememberMeText="Запомнить для следующего входа." TitleText="Аутентификация" UserNameLabelText="Логин:"
          UserNameRequiredErrorMessage="Логин не задан" Width="319px" Height="143px">
          <TextBoxStyle Font-Size="Medium" />
          <LoginButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px"
            Font-Names="Verdana" Font-Size="Medium" ForeColor="#284E98" />
          <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
          <TitleTextStyle BackColor="#507CD1" Font-Bold="True" Font-Size="0.9em" ForeColor="White"
            HorizontalAlign="Center" />
          <CheckBoxStyle HorizontalAlign="Left" Font-Size="Small" />
          <LabelStyle HorizontalAlign="Left" />
        </asp:Login>
        <br />
        <asp:Label ID="lblLoginError" runat="server" Font-Bold="True" Font-Strikeout="False"
          ForeColor="Red" Text="Неверный логин или пароль!" Visible="False" Font-Size="Medium"></asp:Label>
        <br />
        <asp:Label ID="lblRoleError" runat="server" Font-Bold="True" Font-Size="Medium" Font-Strikeout="False"
          ForeColor="Red" Text="Для роли данного пользователя вход запрещен" Visible="False"></asp:Label>
        <br />
        <asp:Panel ID="pnlAccessInfo" runat="server" BorderStyle="Solid" Font-Bold="True"
          Font-Size="Medium" ForeColor="Red" Height="142px" Visible="False" Width="481px">
          <br />
          Внимание!
          <br />
          <br />
          В данный момент доступ к сайту ограничен,
          <br />
          в связи с проведением технологических работ.
          <br />
          Попробуйте обновить текущую страницу через 10 минут.
        </asp:Panel>
      </center>
    </div>
  </form>
</body>
</html>
