using System;
using System.IO;
using System.Xml.Serialization;

namespace RCS.OnlineServerLib
{
    /// <summary>
    /// ��������� �������.
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// ������ ����������� � MSSQL.
        /// </summary>
        [XmlElement(ElementName = "StringConnToDB")] public string ConnectionString;

        /// <summary>
        /// ������� ���������� ������� MSSQL, � ��������.
        /// </summary>
        [XmlElement(ElementName = "MsSqlCmdTimeout")] public int MsSqlCmdTimeout = 300;

        /// <summary>
        /// ����� ������� ��� �������������� � �����������.
        /// </summary>
        [XmlElement(ElementName = "IP_addressTT")] public string IpAddressTT;

        /// <summary>
        /// ���� ������� ��� �������������� � �����������.
        /// </summary>
        [XmlElement(ElementName = "PortTT")] public int PortTT;

        /// <summary>
        /// ����� ������� ��� �������������� � ���������.
        /// </summary>
        [XmlElement(ElementName = "IP_addressCl")] public string IpAddressCl;

        /// <summary>
        /// ���� ������� ��� �������������� � ���������.
        /// </summary>
        [XmlElement(ElementName = "PortCl")] public int PortCl;

        /// <summary>
        /// ������� ����������� ����� �������:
        /// 0-���; 
        /// 1(��-���������)-����.
        /// </summary>
        [XmlElement(ElementName = "ServiceLogLevel")] public int ServiceLogLevel;

        /// <summary>
        /// ���� �������� ���-������, � ����.
        /// </summary>
        [XmlElement(ElementName = "LogDataHoldDays")] public int LogDataHoldDays;

        /// <summary>
        /// �������������(�������) ���������� ���������� ���� � ��, 
        /// � ������������.
        /// </summary>
        [XmlElement(ElementName = "PeriodOfSaveCash")] public int PeriodOfSaveCash;

        /// <summary>
        /// ���������� ������� ����� ������� �������� � ��������
        /// ��������� ���������� ������. ���������� �����, � ������� �������� 
        /// ���������� ��������� ��������. ���������� � ������������.
        /// </summary>
        [XmlElement(ElementName = "DeltaIncomLastPack")] public int DeltaIncomLastPack;

        /// <summary>
        /// �����, � ������� �������� ���������� � �������� ��������� ��������.
        /// ���������� � ������������.
        /// </summary>
        [XmlElement(ElementName = "DeltaIncomLastPackForClient")] public int DeltaIncomLastPackForClient;

        /// <summary>
        /// ���������� ������� ������� �� ��������� �������� ������� 
        /// ��� ����� �������������� � �����������.
        /// </summary>
        [XmlElement(ElementName = "CountOfWorkerThreadTT")] public int CountOfWorkerThreadTT;

        /// <summary>
        /// ���������� ������� ������� �� ��������� �������� �������
        /// ��� ����� �������������� � ���������.
        /// </summary>
        [XmlElement(ElementName = "CountOfWorkerThreadCl")] public int CountOfWorkerThreadCl;

        /// <summary>
        /// ����������� ����������� ��������� ���-�� ����������.
        /// </summary>
        [XmlIgnore()]
        public int MaxPermittedTtCount
        {
            get { return _maxPermittedTtCount; }
            set
            {
                ValidateMaxPermittedTtCount(value);
                _maxPermittedTtCount = value;
            }
        }

        private int _maxPermittedTtCount;

        private void ValidateMaxPermittedTtCount(int value)
        {
            if (value < 0)
            {
                throw new ApplicationException(
                    "�������� ����������� ������������ ���-�� ���������� �� ����� ���� ������ ����.");
            }
        }

        /// <summary>
        /// �������� �������� ��������.
        /// </summary>
        public void Validate()
        {
            if (String.IsNullOrEmpty(ConnectionString))
            {
                throw new Exception("� ����� �������� �� ������ ������ ����������� � ��.");
            }
            if (MsSqlCmdTimeout < 30)
            {
                MsSqlCmdTimeout = 300;
            }

            if (String.IsNullOrEmpty(IpAddressTT))
            {
                throw new Exception("� ����� �������� �� ����� Ip ����� ��� �������������� � �����������.");
            }
            if (String.IsNullOrEmpty(IpAddressCl))
            {
                throw new Exception("� ����� �������� �� ����� Ip ����� ��� �������������� � ���������.");
            }

            if (!PortValid(PortTT))
            {
                PortTT = 9009;
            }
            if (!PortValid(PortCl))
            {
                PortTT = 9010;
            }

            if ((ServiceLogLevel < 0) || (ServiceLogLevel > 1))
            {
                ServiceLogLevel = 1;
            }

            if (LogDataHoldDays < 1)
            {
                LogDataHoldDays = 7;
            }

            if (PeriodOfSaveCash < 3000)
            {
                PeriodOfSaveCash = 60000;
            }

            if (DeltaIncomLastPack < 10000)
            {
                DeltaIncomLastPack = 50000;
            }

            if (DeltaIncomLastPackForClient < 100000)
            {
                DeltaIncomLastPackForClient = 600000;
            }

            if (CountOfWorkerThreadTT < 1)
            {
                CountOfWorkerThreadTT = 3;
            }

            if (CountOfWorkerThreadCl < 1)
            {
                CountOfWorkerThreadCl = 1;
            }
        }

        private bool PortValid(int port)
        {
            return (port > 0) && (port < 0xffff) ? true : false;
        }
    }

    public class AppSettings
    {
        public static Settings LoadSettings()
        {
            Settings settings;
            XmlSerializer xmlFormat = new XmlSerializer(typeof (Settings));
            using (FileStream fStream = new FileStream(
                Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "AppSettings.xml"), FileMode.Open))
            {
                settings = (Settings) xmlFormat.Deserialize(fStream);
            }
            settings.Validate();
            return settings;
        }

        public static void SaveSettings(Settings st)
        {
            XmlSerializer xmlFormat = new XmlSerializer(typeof (Settings));

            using (TextWriter fs = new StreamWriter(
                Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "AppSettings.xml")))
            {
                xmlFormat.Serialize(fs, st);
            }
        }
    }
}
