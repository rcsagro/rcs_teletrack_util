using System;
using System.Collections.Generic;
using TransitServisLib;

namespace RCS.OnlineServerLib.ClientInteraction
{
  public class LstPackNewToClient : List<NewPacketFromTT>, ICloneable
  {
    object obj = new object();
    /// <summary>
    /// ��������� �������� ������
    /// </summary>
    /// <param name="newPacket">NewPacketFromTT</param>
    public void AddPackToLst(NewPacketFromTT newPacket)
    {
      lock (obj)
      {
        this.Add(newPacket);
      }
    }

    /// <summary>
    /// ���������� �� ������ ����� ��� �������� �������
    /// </summary>
    /// <returns>NewPacketFromTT</returns>
    public NewPacketFromTT GetPack()
    {
      NewPacketFromTT pk = null;
      lock (obj)
      {
        if (this.Count != 0)
        {
          pk = this[0];
          this.RemoveAt(0);
        }
      }
      return pk;
    }

    /// <summary>
    /// ������� ������ (������ �������)
    /// </summary>
    /// <param name="id_pk">Id _pk</param>
    public void RemovePackFromLst(long id_pk)
    {
      lock (obj)
      {
        if (this.Count != 0)
        {
          for (int i = 0; i > this.Count; i++)
          {
            if (this[i].ID_packet == id_pk)
            {
              this.Remove(this[i]);
              break;
            }
          }
        }
      }
    }

    /// <summary>
    /// ������� ����� ������ ����� ������� � ������� �������� ������
    /// </summary>
    /// <returns>object</returns>
    public object Clone()
    {
      List<NewPacketFromTT> newList = new List<NewPacketFromTT>();
      lock (obj)
      {
        for (int i = 0; i < this.Count; i++)
        {
          newList.Add(this[i]);
        }
        this.Clear();
      }
      return newList;
    }
  }
}
