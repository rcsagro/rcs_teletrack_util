#region Using directives
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using RCS.OnlineServerLib.DACL;
using RCS.Protocol.Online;
using RCS.Sockets;
using RCS.Sockets.Connection;
using RCS.Trans;
using RCS.Trans.ClientInteraction;
using RW_Log;
using TransitServisLib;
#endregion

namespace RCS.OnlineServerLib.ClientInteraction
{
    /// <summary>
    /// �����, ����������� ������� ������ ����������
    /// </summary>
    /// <returns>IDisposable</returns>
    public class BPControllerCl : IDisposable
    {
        /// <summary>
        /// ����� ���������� ���������
        /// </summary>
        public int NumOutPack
        {
            get
            {
                if (numb_packet > int.MaxValue - MainControlDALC.TT_INSERT_PACKET_SIZE)
                {
                    numb_packet = 0;
                }
                return Interlocked.Increment(ref numb_packet);
            }
        }

        private static int numb_packet;

        /// <summary>
        /// ������� ��������� � ��� �������
        /// </summary>
        public static int �ounter
        {
            get
            {
                if (counter > MainControlDALC.TT_INSERT_PACKET_SIZE)
                {
                    counter = 0;
                }
                return Interlocked.Increment(ref counter);
            }
        }

        static int counter;

        //public event CmdTransmissionDelegate incomNewCmdFromClient;

        /// <summary>
        /// �������������� ����������/�������� � ������ 
        /// </summary>
        private static volatile object activeConnobj = new object();

        public HandlingOfIncomDataCl transpLevelCl = new HandlingOfIncomDataCl();

        /// <summary>
        /// ������ �������� ����������
        /// </summary>
        SortedList<int, BO_Client> lstActiveConn = new SortedList<int, BO_Client>();

        /// <summary>
        /// ������������� �� ��������� ������ �������
        /// </summary>
        private static volatile bool bEndOfWork;

        /// <summary>
        /// ������
        /// </summary>
        public event MessageErrorEventHandler bp_error;

        /// <summary>
        /// ����� � ������� �������� ���������� ��������� �����, �����������
        /// </summary>
        private int deltaLastPackForClient = 600000;

        /// <summary>
        /// ������ ������� � ������� �������� ���������� ��������� �����
        /// </summary>
        /// <returns>Int</returns>
        public int DeltaIncomLastPackForClient
        {
            get { return deltaLastPackForClient; }
            set { deltaLastPackForClient = value; }
        }

        //********************************************************
        private static volatile object locker = new object();
        private Thread[] workers;
        private Queue<ConteinerIncData> taskQueue = new Queue<ConteinerIncData>();
        //********************************************************

        #region ������������

        MainController mainCtrl;

        public BPControllerCl(MainController mCtrl, int countOfWorkerThread, int periodOfSaveCash)
        {
            ExecuteLogging.Log("BPLevelTT", "BPControllerCl. �����������");
            mainCtrl = mCtrl;

            mainCtrl.newPackFromTT += new IncomNewPacketFromTT(NewBinPackFromTT);
            mainCtrl.newA1FromTT += new CmdTransmissionDelegate(NewA1FromTT);

            transpLevelCl.packFromClients += new IncomNewPacket(IncomDataFromClient);
            transpLevelCl.�onnectedState += new ConnectedStateEventHandler(DisconectOfClient);
            transpLevelCl.errorOfLevel += new MessageErrorEventHandler(TransportErrorHandler);


            const string thread_name = "ClientPacketsHandler_{0}";

            workers = new Thread[countOfWorkerThread];
            for (int i = 0; i < countOfWorkerThread; i++)
            {
                workers[i] = new Thread(Consume);
                workers[i].Name = String.Format(thread_name, i);
                workers[i].Start();
            }
        }

        private BPControllerCl()
        {
        }

        #endregion

        private void TransportErrorHandler(object sender, MessageErrorEventArgs e)
        {
            ExecuteLogging.Log("Error_", String.Format(
                "������ �� ������ ��������� ���������� �������:\r\n {0} \r\n {1}",
                e.Message, e.Login));
        }

        /// <summary>
        /// ��������� ������ �� ���� � ��
        /// </summary>
        /// <param name="state">object</param>
        public void SaveData(object state)
        {
            ExecuteLogging.Log("BPLevelTT", "SaveData. ��������� ������ �� ���� � ��");

            ControlEndOfSession();

            lock (activeConnobj)
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();
                MainControlDALC.SaveTransfDataBulkCopy();
                watch.Stop();
                ExecuteLogging.Log("BPLevelTT", String.Format("SaveData. ����� ����������: {0} c. {1} ��.",
                    watch.Elapsed.Seconds, watch.Elapsed.Milliseconds));

                MainControlDALC.SaveCommandTableChanges();
                MainControlDALC.SaveClientInfoChanges();
                MainControlDALC.SaveStatisticDataTTChanges();
                MainControlDALC.SaveStatisticDataClChanges();
                MainControlDALC.SaveTeletrackInfoChanges();
            }

            ExecuteLogging.Log("BPLevelTT", "SaveData. ������ ��������� �� ���� � ��");
        }

        /// <summary>
        /// ������������ ��������� ����������
        /// ������� ���������� 
        /// </summary>
        void ControlEndOfSession()
        {
            ExecuteLogging.Log("BPLevelTT", "ControlEndOfSession. ������������ ��������� ����������, ������� ����������");
            if (lstActiveConn.Count != 0)
            {
                List<BO_Client> lst_bo = new List<BO_Client>();
                lock (activeConnobj)
                {
                    foreach (BO_Client bo in lstActiveConn.Values)
                    {
                        if (bo.TimeIncomLastPack.AddMilliseconds(deltaLastPackForClient) < DateTime.Now)
                        {
                            lst_bo.Add(bo);

                            ExecuteLogging.Log("BPLevelTT", String.Format(
                                "ControlEndOfSession. ��������� ����� �������� ������ �� ������� : {0}", GetClientName(bo)));
                        }
                    }
                }
                if (lst_bo.Count != 0)
                {
                    ExecuteLogging.Log("BPLevelTT", "ControlEndOfSession. �������� ���������� ����������");

                    for (int i = 0; i < lst_bo.Count; i++)
                    {
                        CloseConnection(lst_bo[i]);
                    }
                }
            }
        }

        /// <summary>
        /// ��������� �������� ������� ��� ��� id � �� �������
        /// </summary>
        /// <param name="sender">������ ����������(������) BO_Client</param>
        /// <returns>���</returns>
        static string GetClientName(BO_Client sender)
        {
            return String.Format("{0}({1}) IP:{2} id:{3} ",
                sender.Login,
                String.IsNullOrEmpty(sender.DescribeClient) ? "��� ��������" : sender.DescribeClient,
                sender.RemoteAddress == null ? "��� ��������" : sender.RemoteAddress.ToString(),
                Convert.ToString(sender.ID_Client));
        }

        void DisconectOfClient(ConnectedStateEvArg con_state)
        {
            BO_Client bo = (BO_Client) con_state.conn;

            if (!con_state.B_State && bo.InNetwork) //������ ����������
            {
                lock (activeConnobj)
                {
                    StatisticDACL.DeleteSessiontCient(bo.ID_SessionCl);

                    if (lstActiveConn.ContainsKey(bo.ID_Client))
                    {
                        ClientDACL.SetInNetworkClient(bo.ID_Client, false);
                    }
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "<!> (DisconectOfClient)������ ���������� � {0} ����� {1}",
                        GetClientName(bo), Thread.CurrentThread.GetHashCode()));

                    lstActiveConn.Remove(bo.ID_Client);
                }
            }
        }

        protected void CloseConnection(BO_Client sender)
        {
            lock (activeConnobj)
            {
                if (lstActiveConn.ContainsKey(sender.ID_Client))
                {
                    //sender.InNetwork = false;
                    ClientDACL.SetInNetworkClient(sender.ID_Client, false);

                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "<!> CloseConnection: {0} ������� �� ������ �������� ����������.",
                        GetClientName(sender)));

                    StatisticDACL.DeleteSessiontCient(sender.ID_SessionCl);

                    lstActiveConn.Remove(sender.ID_Client);
                }
            }
            transpLevelCl.CloseSocket(sender); //������ ����������
        }

        /// <summary>
        /// ��������� �������� �������� �����(� ������ ������ ����������� �� ���������) ������
        /// </summary>
        /// <param name="owners">������ ���������� ���������</param>
        /// <param name="n_pk">����� NewPacketFromTT</param>
        void NewBinPackFromTT(List<int> owners, NewPacketFromTT n_pk)
        {
            lock (activeConnobj)
            {
                ExecuteLogging.Log("BPLevelTT",
                    "NewBinPackFromTT. ��������� �������� �������� �����(� ������ ������ ����������� �� ���������) ������");
                //��������� ������ �� �������� ���������� ����
                if (�ounter >= MainControlDALC.TT_INSERT_PACKET_SIZE)
                {
                    IncomDataFromClient(this, null); //�� ������ ������
                }

                for (int i = 0; i < owners.Count; i++)
                {
                    if (lstActiveConn.ContainsKey(owners[i])) //������ �� �����
                    {
                        BO_Client bo = (BO_Client) lstActiveConn[owners[i]];
                        bo.lstOfNewPack.Add(n_pk); //�������� �� ��������

                        ExecuteLogging.Log("BPLevelTT",
                            String.Format(
                                "NewBinPackFromTT. �������� ����� (ID_Packet = {0}) � ������ �� �������� �������: {1}",
                                n_pk.ID_packet, GetClientName(bo)));
                    }
                    //else //�������� ��������� � ID_LastPackInSrvDB (� BPControllerCl)
                    ClientDACL.SetLastPkForClient(owners[i], n_pk.ID_packet);
                }
            }
        }

        void NewA1FromTT(int id_client, int id_cmd)
        {
            lock (activeConnobj)
            {
                if (lstActiveConn.ContainsKey(id_client)) //������ �� �����
                {
                    BO_Client bo = (BO_Client) lstActiveConn[id_client];
                    bo.AddNewCmdToList(id_cmd); //�������� �� ��������
                }
            }
        }

        #region IDisposable Members

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            ExecuteLogging.Log("BPLevelTT", "Dispose. ��������� ����������....");
            bEndOfWork = true;
            EndOfWork();

            ExecuteLogging.Log("BPLevelTT", "Dispose. ��������� ������....");
            SaveData(this);

            // ��������� null-������ ����� ������������ ���� ������������
            IncomDataFromClient(null, null);

            foreach (Thread worker in workers)
            {
                worker.Join();
            }
        }

        /// <summary>
        /// ��������� ��� ����������, ��������� ������
        /// </summary>
        void EndOfWork()
        {
            lock (activeConnobj)
            {
                foreach (BO_Client bo in lstActiveConn.Values)
                {
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "EndOfWork: �������� ���������� � {0}", GetClientName(bo)));

                    StatisticDACL.DeleteSessiontCient(bo.ID_SessionCl);

                    //lstActiveConn.Remove(bo.ID_Client);
                    bo.InNetwork = false;
                    transpLevelCl.CloseSocket(bo); //������ ����������
                }
            }
        }

        #endregion

        #region ���������� ������� ��� UI

        /// <summary>
        /// Err DA level
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">MessageErrorEventArgs</param>
        void ErrDALevel(object sender, MessageErrorEventArgs e)
        {
            ExecuteLogging.Log("Error_", e.Message);
            if (bp_error != null)
            {
                bp_error(sender, e);
            }
        }

        #endregion

        void Consume()
        {
            while (!bEndOfWork)
            {
                try
                {
                    ConteinerIncData dataContainer;
                    lock (locker)
                    {
                        while (taskQueue.Count == 0)
                        {
                            Monitor.Wait(locker);

                            if (bEndOfWork) // ������ �� �����
                            {
                                return;
                            }
                        }

                        dataContainer = taskQueue.Dequeue();
                    }

                    if (dataContainer.sender == null)
                    {
                        return; // ������ �� �����
                    }

                    if (dataContainer.packet != null)
                    {
                        ParserIncomData(dataContainer);
                    }
                }
                catch (Exception ex)
                {
                    ExecuteLogging.Log("Error_", String.Format(
                        "������ � ������-����������� ������� ��������� ���������� ����� \r\n {0} \r\n {1}",
                        ex.Message, ex.StackTrace));
                }
            }
        }

        /// <summary>
        /// ������ ����� �� Client
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="data">IPacket</param>
        public void IncomDataFromClient(object sender, IPacket data)
        {
            lock (locker)
            {
                ConteinerIncData ct = new ConteinerIncData(sender, data);
                //ParserIncomData(ct);
                taskQueue.Enqueue(ct);
                Monitor.PulseAll(locker);
            }
        }

        /// <summary>
        /// Switch �������� �������
        /// </summary>
        /// <param name="obj">object</param>
        protected virtual void ParserIncomData(object obj)
        {
            ConteinerIncData dataContainer = obj as ConteinerIncData;
            IPacket packet = dataContainer.packet;
            BO_Client sender = (BO_Client) dataContainer.sender;
            sender.TimeIncomLastPack = DateTime.Now; //��������� ����� ����������� ������

            if (packet.GetType().Name != "PacketAuth")
            {
                StatisticDACL.CapacityInCl(sender.ID_SessionCl, packet.PacketSize);
            }

            switch (packet.GetType().Name)
            {
                case "PacketAuth": //����� Auth
                    //�������� ������
                    PacketAuth pk_au = packet as PacketAuth;
                    try
                    {
                        if (!bEndOfWork && NewConnection(sender, pk_au.Login, pk_au.Password))
                        {
                            SendPacketToClient(
                                sender,
                                new PacketAckExSr(0, sender.ID_LastPacketInSrDB, 0, 0));
                        }
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log("Error_", String.Format(
                            "{0} \r\n {1}", e.Message, e.StackTrace));
                    }
                    break;

                case "PacketAckExSr": //����� AckExtendSr
                    //�������� ������
                    PacketAckExSr pk_ack_sr = packet as PacketAckExSr;
                    break;

                case "PacketAckExCl": //����� AckExtendCl
                    //�������� ������
                    PacketAckExCl pk_ack_cl = packet as PacketAckExCl;
                    try
                    {
                        ExecuteLogging.Log("BPLevelTT", String.Format(
                            "--> �������� PacketAckExCl �� ������� {0}", GetClientName(sender)));

                        AckFromClient(sender, pk_ack_cl); //������ ���������

                        if (pk_ack_cl.ConState == 1)
                        {
                            SendFictiveAckExSr(sender); //������ ��������� �������(�������)
                        }
                        else
                        {
                            AnswerOnAckExCl(sender, NumOutPack); //���������� ������� ������
                        }
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log("Error_", String.Format(
                            "{0} \r\n {1}", e.Message, e.StackTrace));
                    }
                    break;

                case "PacketInfo": //����� PacketInfo
                    //�������� ������                       
                    PacketInfo pk_inf = packet as PacketInfo;
                    break;

                case "PacketResult": //����� Result
                    PacketResult pk_rez = packet as PacketResult;
                    break;

                case "PacketSendCommand": //����� SendCommand
                    //�������� ������
                    PacketSendCommand pk_sn_com = packet as PacketSendCommand;
                    try
                    {
                        NewCommandFromClient(sender, pk_sn_com);
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log("Error_", String.Format(
                            "{0} \r\n {1}", e.Message, e.StackTrace));
                    }
                    break;

                case "PacketSendQuery": //����� SendQuery
                    //�������� ������
                    PacketSendQuery pk_sn_qe = packet as PacketSendQuery;

                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "--> �������� PacketSendQuery �� {0}. LoginTT = {1} Param1 = {2} Param2 = {3}",
                        GetClientName(sender), pk_sn_qe.LoginTT, pk_sn_qe.Param1, pk_sn_qe.Param2));

                    try
                    {
                        AnswerOnSendQuery(sender, pk_sn_qe);
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log("Error_", String.Format(
                            "{0} \r\n {1}", e.Message, e.StackTrace));
                    }
                    break;

                case "PacketAnswCommand": //����� AnswCommand
                    PacketAnswCommand pk_answ_cmd = new PacketAnswCommand();
                    break;
            }
        }

        /// <summary>
        /// ��������� ����� ����������
        /// </summary>
        /// <param name="sender">BO_Client</param>
        /// <param name="login"></param>
        /// <param name="password"></param>
        public bool NewConnection(BO_Client sender, string login, string password)
        {
            bool rez = false;
            int id_client;
            bool isActive;
            bool inNetwork;
            long id_packet;
            DateTime dTimeDeliveredPack;
            long id_LastPacketInSrDB;
            int id_Dealer;
            string descr;

            if (ClientDACL.AuthenticationClient(login, password, out id_client, out isActive,
                out inNetwork, out id_packet, out dTimeDeliveredPack, out id_LastPacketInSrDB,
                out id_Dealer, out descr) && isActive)
            {
                //������������� ������� �������
                sender.InitializationOfClientObj(login, id_client, isActive, inNetwork, id_packet,
                    dTimeDeliveredPack, id_LastPacketInSrDB, id_Dealer, descr);

                lock (activeConnobj)
                {
                    if (lstActiveConn.ContainsKey(sender.ID_Client))
                    {
                        BO_Client oldSender = lstActiveConn[sender.ID_Client];
                        ExecuteLogging.Log("BPLevelTT", String.Format(
                            "!!! C��������� c {0} ��� ����. CloseConnection(sender)",
                            GetClientName(oldSender)));
                        CloseConnection(oldSender);
                    }
                    ClientDACL.SetInNetworkClient(sender.ID_Client, true);
                    sender.InNetwork = true;
                    lstActiveConn.Add(sender.ID_Client, sender);
                }
                //����� ������ � ������� ����������
                sender.ID_SessionCl = StatisticDACL.AddSessionCl(sender.ID_Client);

                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "+++ �������� ����� ���������� c {0} {1}",
                    GetClientName(sender), sender.socket.RemoteEndPoint));
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "    ID_LastPacketInSrDB: {0}", sender.ID_LastPacketInSrDB));

                sender.lstExecCmd = ClientDACL.GetLstExecCommands(sender.ID_Client);
                rez = true;
            }
            else
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "!!! �������� � �������. �����: {0} ������: {1} IsActiveState: {2}",
                    login, password, sender.IsActive));

                CloseConnection(sender); //������ ����������
            }
            return rez;
        }

        /// <summary>
        /// ��������� ����� �� ����� ������� SendQuery
        /// </summary>
        /// <param name="sender">�� ����</param>
        /// <param name="pk">����� ������� SendQuery</param>
        private void AnswerOnSendQuery(BO_Client sender, PacketSendQuery pk)
        {
            if (pk.LoginTT == "" || pk.LoginTT.Length != 4)
            {
                throw new ArgumentOutOfRangeException("����� ����� ��������� ������� 4-� ��������");
            }

            ExecuteLogging.Log("BPLevelTT", String.Format(
                "    ������ �� ����������� ������: ������ {0} �������� {1} ���������: {2} {3}",
                GetClientName(sender), pk.LoginTT, pk.Param1, pk.Param2));

            long lastId; // id ���������� ������ � ���������� �������
            bool errorOccurred = false;

            //�������� �������� �� �������������� ��������� ������� �������
            Dictionary<long, byte[]> dic = MainControlDALC.GetDataForSendQuery(
                sender.ID_Client, sender.DescribeClient, pk.LoginTT, pk.Param1, pk.Param2,
                out lastId, out errorOccurred);

            if (errorOccurred)
            {
                // ������ ����������, ������ ��� ���� ������� ������������� ������ TDataMngr
                // ������� � ������� ������� �����, �� ������������� ������ ����� ����������
                // ��� ��������, ��� ��������� ������ �� ������� �� �������������� ���������.
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    " !!! ������ ��� ������������ ������ ���������� {0} �� ����������� ������� ��������� {1}",
                    GetClientName(sender), pk.LoginTT));

                CloseConnection(sender);
            }
            else
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "<-- ��������� ���������� {0} Dictionary �� {1} ������� ��������� {2}",
                    GetClientName(sender), dic.Count, pk.LoginTT));

                SendPacketToClient(sender, new PacketResult(NumOutPack, pk.LoginTT, lastId, dic));
            }
        }

        /// <summary>
        /// ��������� ����� ������� �� �������
        /// </summary>
        /// <param name="sender">BO_Client</param>
        /// <param name="pk_sn_com">PacketSendCommand</param>
        public bool NewCommandFromClient(BO_Client sender, PacketSendCommand pk_sn_com)
        {
            ExecuteLogging.Log("BPLevelTT", String.Format(
                "--> �������� ����� �������. IDCmdInClDB: {0} ��� ���������: {1} �� ������� {2}\r\n" +
                "    ����� A1: {3} ����� : � {4}",
                pk_sn_com.IDCmdInClDB, pk_sn_com.LoginTT, GetClientName(sender),
                Encoding.ASCII.GetString(pk_sn_com.ComText, 0, pk_sn_com.ComText.Length),
                Thread.CurrentThread.GetHashCode()));

            bool rez = false;
            if (ClientDACL.VerificatIsOwner(sender.ID_Client, pk_sn_com.LoginTT))
            {
                int id_cmd = 0;
                //���������� ������� � �� � � ���
                ClientDACL.InsertNewCmd(
                    pk_sn_com.LoginTT, //����� ��
                    sender.ID_Client, //id ������� � �� �������
                    pk_sn_com.IDCmdInClDB, //id ������� � �� �������
                    pk_sn_com.ComText, //������� (����� �1)
                    sender.RemoteAddress.IpAddress, //IP ����������. 
                    out id_cmd); //out id ������� � �� �������  
            }
            else
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "�������� {0} ����������� � �� �������, ���� ������ {1} �� �������� ���������� ������� ���������.",
                    pk_sn_com.LoginTT, GetClientName(sender)));
            }

            //����� �������
            SendPacketToClient(sender, new PacketAckExSr(
                0, sender.ID_LastPacketInSrDB, pk_sn_com.MessageID, 0));

            ExecuteLogging.Log("BPLevelTT", String.Format(
                "<-- ������������� ��������� ������� �1 � {0}(localID: {1}) ������� {2} ����� : � {3}.",
                pk_sn_com.IDCmdInClDB, pk_sn_com.MessageID, GetClientName(sender),
                Thread.CurrentThread.GetHashCode()));

            return rez;
        }

        /// <summary>
        /// ��������� �������: ��������� ����� ������� �� �������
        /// </summary>
        /// <param name="id_teletrack">id_teletrack � �� �������</param>
        /// <param name="newRow">������ � ����</param>
        //private void OnNewCommandFromClient(int id_teletrack, int id_cmd)
        //{
        //  if (incomNewCmdFromClient != null)
        //  {
        //    incomNewCmdFromClient(id_teletrack, id_cmd);
        //  }
        //}

        /// <summary>
        /// �������� ������������� �� ���������� ������������ �����
        /// </summary>
        /// <param name="sender">BO_Client</param>
        /// <param name="pk_ack_cl">PacketAckExCl</param>
        private void AckFromClient(BO_Client sender, PacketAckExCl pk_ack_cl)
        {
            switch (sender.lastOutPacket.GetType().Name)
            {
                case "PacketInfo":
                    ConfirmOnPacketInfo(sender, pk_ack_cl);
                    break;

                case "PacketResult":
                    ConfirmOnResult(sender, pk_ack_cl);
                    break;

                case "PacketAnswCommand":
                    ConfirmOnAnswCommand(sender, pk_ack_cl);
                    break;
            }
        }

        private void ConfirmOnAnswCommand(BO_Client sender, PacketAckExCl pk_ack_cl)
        {
            PacketAnswCommand lastPack = sender.lastOutPacket as PacketAnswCommand;
            if (lastPack != null && sender.lstExecCmd.Count != 0)
            {
                int id = sender.lstExecCmd[0];
                MainControlDALC.DeleteQvitCmd(id);
                sender.RemoveCmdFromList(id);
            }
        }

        /// <summary>
        /// �������������� �� Result
        /// </summary>
        /// <param name="sender">BO_Client</param>
        /// <param name="pk_ack_cl">PacketAckExCl</param>
        private void ConfirmOnResult(BO_Client sender, PacketAckExCl pk_ack_cl)
        {
            PacketResult lastPack = sender.lastOutPacket as PacketResult;

            if ((lastPack != null) && (sender.ID_Packet < lastPack.LastIDInResult))
            {
                sender.ID_Packet = lastPack.LastIDInResult;
                ClientDACL.LastDeliveredPackToClient(sender.ID_Client, sender.ID_Packet);
            }

            ExecuteLogging.Log("BPLevelTT", String.Format(
                "    ������������� �� �������� PacketResult. ����� {0} ��������� ������� {1}",
                sender.ID_Packet, GetClientName(sender)));
        }

        /// <summary>
        /// �������������� �� PacketInfo
        /// </summary>
        /// <param name="sender">BO_Client</param>
        /// <param name="pk_ack_cl">PacketAckExCl</param>
        private void ConfirmOnPacketInfo(BO_Client sender, PacketAckExCl pk_ack_cl)
        {
            PacketInfo lastPack = sender.lastOutPacket as PacketInfo;
            if (lastPack != null)
            {
                long id_packInSrDB = sender.ID_LastPacketInSrDB;

                id_packInSrDB = lastPack.lstNewBin != null
                    ? lastPack.lstNewBin[lastPack.lstNewBin.Count - 1].ID_packet
                    : lastPack.ID_Packet;

                sender.ID_LastPacketInSrDB = id_packInSrDB;
                ClientDACL.SetLastPkForClient(sender.ID_Client, sender.ID_LastPacketInSrDB);
                sender.ID_Packet = id_packInSrDB;
                ClientDACL.LastDeliveredPackToClient(sender.ID_Client, sender.ID_Packet);

                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "    ������������� �� �������� PacketInfo. ����� {0} ��������� ������� {1}",
                    sender.ID_Packet, GetClientName(sender)));
            }
        }

        /// <summary>
        /// ��������� � ���������� ��������� AckExSr
        /// </summary>
        /// <param name="sender">BO_Client</param>
        private void SendFictiveAckExSr(BO_Client sender)
        {
            PacketAckExSr answ = new PacketAckExSr(0, 0, 0, 0);
            SendPacketToClient(sender, answ);
        }

        /// <summary>
        /// � ������� ��� ��������.
        /// ������ ���������� ������
        /// </summary>
        /// <param name="sender">BO_Client</param>
        /// <param name="numb_incom_pack"></param>
        private void AnswerOnAckExCl(BO_Client sender, int numb_incom_pack)
        {
            ExecuteLogging.Log("BPLevelTT", String.Format(
                "<-- ��������� ����� �� PacketAckExCl ������� {0} ����� : � {1}",
                GetClientName(sender), Thread.CurrentThread.GetHashCode()));

            IPacket answ = null;
            int cmdCount = sender.lstExecCmd.Count;
            //����������� ������ � ����� ������� ���  
            if (cmdCount == 0 && sender.lstOfNewPack.Count == 0)
            {
                answ = new PacketAckExSr(0, //��������� ����������
                    sender.ID_LastPacketInSrDB, // ����� ���������� ������ ��� ������� �������
                    numb_incom_pack, // ����� ����������� ������ �� ������� 
                    NumOutPack); // ����� ����� ������
            }

            //���� ������� ��� ������������
            if (cmdCount != 0)
            {
                int idInClDb; //
                int id_teletr;
                byte cod;
                byte[] cmdText;
                if (ClientDACL.GetExecCmdForQvit(sender.GetCmdFromList(), out id_teletr,
                    out idInClDb, out cod, out cmdText))
                {
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "    ���������� ����� PacketAnswCommand ������� {0}. �������� �������� {1} ���: {2} �����: {3}",
                        GetClientName(sender), id_teletr, (int) cod,
                        Encoding.ASCII.GetString(cmdText, 0, cmdText.Length)));

                    answ = new PacketAnswCommand(NumOutPack, idInClDb, cod, cmdText);
                }
            }

            //���� ����� ������
            if (cmdCount == 0 && sender.lstOfNewPack.Count > 0)
            {
                if (sender.lstOfNewPack.Count == 1)
                {
                    NewPacketFromTT new_pk = sender.lstOfNewPack.GetPack();
                    answ = new PacketInfo(NumOutPack, new_pk.LoginTT, new_pk.ID_packet, new_pk.Packet, null);

                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "    ���������� ����� ID_packet = {0} ������� {1} ����� : � {2}",
                        new_pk.ID_packet, GetClientName(sender), Thread.CurrentThread.GetHashCode()));
                }
                else //��������� PacketInfo �� ������� ����� �������
                {
                    List<NewPacketFromTT> lst = (List<NewPacketFromTT>) sender.lstOfNewPack.Clone();
                    answ = new PacketInfo(NumOutPack, "", -1, null, lst);

                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "    ���������� ������� {0} ������ �� {1} �������",
                        GetClientName(sender), lst.Count));
                }
            }

            SendPacketToClient(sender, answ);
        }

        /// <summary>
        /// ���������� ������ � ��������� � ������� ����������
        /// ��������� ������������ �����
        /// </summary>
        /// <param name="sender">BO_Client</param>
        /// <param name="answ">IPacket</param>
        private void SendPacketToClient(BO_Client sender, IPacket answ)
        {
            byte[] pk = answ.AssemblePacket();

            transpLevelCl.SendData(sender, pk);

            StatisticDACL.CapacityOutCl(sender.ID_SessionCl, pk.Length);
            sender.lastOutPacket = answ;
        }
    }
}
