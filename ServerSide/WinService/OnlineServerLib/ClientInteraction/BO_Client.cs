using System;
using System.Collections.Generic;
using RCS.Sockets.Connection;
using TransitServisLib;

namespace RCS.OnlineServerLib.ClientInteraction
{
  public class BO_Client : ConnectionInfo
  {
    #region �������� �������
    /// <summary>
    /// ������������� ������� �������
    /// </summary>
    /// <param name="loginClient">����� ������� (����������)</param>
    /// <param name="idClient"></param>
    /// <param name="isAct"></param>
    /// <param name="inNet"></param>
    /// <param name="idPk"></param>
    /// <param name="dTDelivPk"></param>
    /// <param name="idLastPkInSrDB"></param>
    /// <param name="idDealer"></param>
    /// <param name="desc"></param>
    public void InitializationOfClientObj(string loginClient, int idClient,
      bool isAct, bool inNet, long idPk, DateTime dTDelivPk, long idLastPkInSrDB,
      int idDealer, string desc)
    {
      login = loginClient;
      id_client = idClient;
      isActive = isAct;
      inNetwork = inNet;
      id_Packet = idPk;
      dTimeDeliveredPack = dTDelivPk;
      id_lastPacketInSrDB = idLastPkInSrDB;
      id_dealer = idDealer;
      describe = desc;
    }

    /// <summary>
    /// ID_Client � ���� �������
    /// </summary>
    public int ID_Client
    {
      set { id_client = value; }
      get { return id_client; }
    }
    private int id_client;

    /// <summary>
    /// ��������� �� ������������
    /// </summary>
    public bool IsActive
    {
      set { isActive = value; }
      get { return isActive; }
    }
    private bool isActive;

    /// <summary>
    /// � ���� (����� ������������ ������������� � false) 
    /// </summary>
    public bool InNetwork
    {
      set { inNetwork = value; }
      get { return inNetwork; }
    }
    private bool inNetwork;

    /// <summary>
    /// ID ���������� ����������� ������
    /// </summary>
    public long ID_Packet
    {
      set { id_Packet = value; }
      get { return id_Packet; }
    }
    private long id_Packet;

    /// <summary>
    /// ����� �������� ������� ���������� ������
    /// </summary>
    public DateTime DTimeDeliveredPack
    {
      set { dTimeDeliveredPack = value; }
      get { return dTimeDeliveredPack; }
    }
    private DateTime dTimeDeliveredPack;

    /// <summary>
    /// ����� ���������� (� �� �������) ������ ��� ������� 
    /// </summary>
    public long ID_LastPacketInSrDB
    {
      set { id_lastPacketInSrDB = value; }
      get { return id_lastPacketInSrDB; }
    }
    private long id_lastPacketInSrDB;

    /// <summary>
    /// ID ������
    /// </summary>
    public int ID_Dealer
    {
      set { id_dealer = value; }
      get { return id_dealer; }
    }
    private int id_dealer;

    /// <summary>
    /// �������� �������
    /// </summary>
    public string DescribeClient
    {
      get { return describe; }
      set { describe = value; }
    }
    private string describe = "";

    /// <summary>
    /// ����� �������(����������) 
    /// </summary>
    public string Login
    {
      get { return login; }
      set { login = value; }
    }
    private string login;

    /// <summary>
    /// ������ id ����������� ������
    /// </summary>
    public List<int> lstExecCmd = new List<int>();

    #endregion

    #region ������ ��� ������ �� ������� ����������� ������

    object locker = new object();

    internal int GetCmdFromList()
    {
      int id_cmd = 0;
      lock (locker)
      {
        if (lstExecCmd.Count != 0)
        {
          id_cmd = lstExecCmd[0];
        }
      }
      return id_cmd;
    }

    internal void AddNewCmdToList(int id_cmd)
    {
      lock (locker)
      {
        lstExecCmd.Add(id_cmd);
      }
    }

    internal void RemoveCmdFromList(int id_cmd)
    {
      lock (locker)
      {
        lstExecCmd.Remove(id_cmd);
      }
    }
    #endregion

    /// <summary>
    /// ������ ����������� �� ��, � on-line ������, ������� ��� ������� �������
    /// </summary>
    public LstPackNewToClient lstOfNewPack = new LstPackNewToClient();

    #region ������ ��� �������� �� ���������� ������ �����

    /// <summary>
    /// id ������ � ������� StatisticDataCl
    /// </summary>
    public int ID_SessionCl
    {
      get { return ID_sessionCl; }
      set { ID_sessionCl = value; }
    }
    int ID_sessionCl;

    /// <summary>
    /// �����, ������������ �� ���������� ����
    /// </summary>
    public IPacket lastOutPacket;

    #endregion
  }
}
