
namespace RCS.OnlineServerLib.Lic
{
  /// <summary>
  /// ��������� ����������� ���������� ��������������.
  /// </summary>
  public interface ILicController
  {
    /// <summary>
    /// ������ �������� ������ �������� � ����-�����.
    /// </summary>
    void Start();
    /// <summary>
    /// ���������� ������.
    /// </summary>
    void Break();
    /// <summary>
    /// �������, ����������� ����� ���������� �������� � 
    /// ������������� ���-�� ����������.
    /// </summary>
    event LicFoundEventHandler LicFound;
  }
}
