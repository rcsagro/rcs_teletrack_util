using System;
using RCS.Lic;
using RCS.Lic.Entities;
using RW_Log;

namespace RCS.OnlineServerLib.Lic
{
  /// <summary>
  /// �������. 
  /// </summary>
  /// <param name="maxPermittedTtCount">����������� ����������� ���������
  /// ���-�� ����������.</param>
  public delegate void LicFoundEventHandler(int maxPermittedTtCount);

  public class LicController : ILicController
  {
    /// <summary>
    /// ���������� ��������������.
    /// </summary>
    private ILicRefreshable _licDetector;

    /// <summary>
    /// ������������ � �������� ���������
    /// </summary>
    private volatile bool _stopping;

    private volatile LicRefreshedDelegate _licRefreshedDelegate;


    #region ILicController Members

    /// <summary>
    /// �������, ����������� ����� ���������� �������� � 
    /// ������������� ���-�� ����������.
    /// </summary>
    public event LicFoundEventHandler LicFound;

    /// <summary>
    /// ������ �������� ������ �������� � ����-�����.
    /// </summary>
    public void Start()
    {
      _stopping = false;

      if (!LicDetectorFactory.Active)
      {
        FinishLicOperations(0, "��� �����������.");
        return;
      }

      _licDetector = new LicDetectorFactory().CreateAutoDetector();
      _licRefreshedDelegate = new LicRefreshedDelegate(OnLicRefreshed);
      _licDetector.LicRefreshed += _licRefreshedDelegate;
      _licDetector.StartAutoRefresh();
    }

    /// <summary>
    /// ���������� ������.
    /// </summary>
    public void Break()
    {
      _stopping = true;
      StopLicRefresh();
    }

    #endregion

    /// <summary>
    /// ���������� ������� ���������� ������������ ����������.
    /// </summary>
    private void OnLicRefreshed(Licence licence)
    {
      if ((licence.State == HState.OK) && (licence.MaxPermittedTtCount > 0))
      {
        StopLicRefresh();
        FinishLicOperations(licence.MaxPermittedTtCount, licence.Description);
      }
      else
      {
        WriteLicDescription(licence.Description);
      }
    }

    /// <summary>
    /// ��������� �������� ������ ����-�����\��������.
    /// </summary>
    private void StopLicRefresh()
    {
      if (_licDetector != null)
      {
        _licDetector.LicRefreshed -= _licRefreshedDelegate;
        ((IStoppable)_licDetector).Stop();
        _licRefreshedDelegate = null;
      }
    }

    /// <summary>
    /// ����������� �������� � �����������
    /// ��������������. ������ ��� ��� �� �����.
    /// </summary>
    /// <param name="maxPermittedTtCount">��� ���-�� ����������.</param>
    /// <param name="logMessage">��������� ��� ��� �����.</param>
    private void FinishLicOperations(int maxPermittedTtCount, string logMessage)
    {
      _licDetector = null;
      WriteLicDescription(logMessage);
      if ((!_stopping) && (LicFound != null))
      {
        LicFound(maxPermittedTtCount);
      }
    }

    /// <summary>
    /// ������ � ��� ������������ ����������.
    /// </summary>
    /// <param name="descr">��������.</param>
    private void WriteLicDescription(string descr)
    {
      ExecuteLogging.Log("BPLevelTT", String.Format("��������: {0}", descr));
    }
  }
}
