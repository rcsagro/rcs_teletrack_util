using RCS.OnlineServerLib.DACL;
using RCS.Trans;
using RCS.Trans.TeletrackInteraction;

namespace RCS.OnlineServerLib.TeletrackInteraction
{
  /// <summary>
  /// ������� ������ BO_Teletrack
  /// </summary>
  class BO_TeletrackSrv
  {
    private static object locker = new object();
    private BO_Teletrack boTeletrack;

    public BO_TeletrackSrv(BO_Teletrack boTeletrack)
    {
      this.boTeletrack = boTeletrack;
    }

    /// <summary>
    /// ���������� �������(A1) ��� ���� ������ ���������� ��,
    /// ��� �������� ���������
    /// </summary>
    public byte[] GetDataForTT()
    {
      byte[] cmd = null;
      lock (locker)
      {
        //if (boTeletrack.upgradeTT.Count != 0)
        //{
        //  return boTeletrack.upgradeTT[0];
        //}

        if (boTeletrack.lstCmd.Count != 0)
        {
          string cmdType = "";
          int id_client = 0;
          cmd = TeletreckDALC.GetCmdForTT(boTeletrack.lstCmd[0], ref cmdType, ref id_client);
          //if (cmdType == "U")
          //{
          //  boTeletrack.upgradeTT = ParserOfUpgradeForTT.StrippingOfUpg(cmd);
          //  if (boTeletrack.upgradeTT.Count != 0)
          //  {
          //    cmd = boTeletrack.upgradeTT[0];
          //  }
          //  boTeletrack.AttemptToUpgradeTT = 3;
          //}

          boTeletrack.lastOutPacketA1 = new LastSendingPacket(boTeletrack.lstCmd[0], cmdType, id_client);
          boTeletrack.lstCmd.RemoveAt(0);
        }
      }
      return cmd;
    }
  }
}
