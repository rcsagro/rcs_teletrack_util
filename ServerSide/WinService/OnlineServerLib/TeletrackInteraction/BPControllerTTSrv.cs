using System;
using System.Text;
using System.Threading;
using RCS.OnlineServer.Core.Model;
using RCS.OnlineServer.Core.Repositories;
using RCS.OnlineServerLib.DACL;
using RCS.Protocol.TT;
using RCS.Sockets;
using RCS.Sockets.Connection;
using RCS.Trans;
using RCS.Trans.TeletrackInteraction;
using RW_Log;
using TransitServisLib;

namespace RCS.OnlineServerLib.TeletrackInteraction
{
    class BPControllerTTSrv : BPControllerTT
    {
        /// <summary>
        /// ���������� ������� �������� ����� ������
        /// ��� �������� ������ �� ���������
        /// </summary>
        const int COUNT_AttemptToUpgrade = 3;

        public BPControllerTTSrv(MainController mainCtrl, int countOfWorkerThread)
        {
            //mainCtrl.newA1FromClient += new CmdTransmissionDelegate(NewCommandFromClient);

            ExecuteLogging.Log("BPLevelTT", "BPControllerTTSrv. �������� ����������� ����������");

            handler = new HandlingOfIncomDataTT();
            handler.packFromTeletrack += new IncomNewPacket(IncomDataFromTT);

            handler.�onnectedState += new ConnectedStateEventHandler(DisconectOfTeletrack);
            handler.errorOfLevel += new MessageErrorEventHandler(ErrorFromTrLevel);

            const string thread_name = "TeletrackPacketsHandler_{0}";
            //������ ������-�����������
            workers = new Thread[countOfWorkerThread];

            // 
            ExecuteLogging.Log("BPLevelTT", "BPControllerTTSrv. ������� � ��������� ��������� ����� �� ������� �����������: " + countOfWorkerThread);
            for (int i = 0; i < countOfWorkerThread; i++)
            {
                workers[i] = new Thread(Consume);
                workers[i].Name = String.Format(thread_name, i);
                workers[i].IsBackground = true;
                workers[i].Start();
            }
        }

        //private void NewCommandFromClient(int id_teletrack, int id_cmd)
        //{
        //  lock (activeConnobj)
        //  {
        //    if (lstActiveConnTT.ContainsKey(id_teletrack))//�������� �� �����
        //    {
        //      BO_Teletrack bo_tt = (BO_Teletrack)lstActiveConnTT[id_teletrack];
        //      bo_tt.AddNewCmdToList(id_cmd);//�������� � ������ �� ��������
        //    }
        //  }
        //  //else  //������
        //}

        protected override void CloseConnection(BO_Teletrack sender)
        {
            lock (activeConnobj)
            {
                if (lstActiveConnTT.ContainsKey(sender.ID_Teletrack))
                {
                    StatisticDACL.DeleteSessiontTT(sender.ID_Session);

                    lstActiveConnTT.Remove(sender.ID_Teletrack);

                    sender.InNetwork = false;
                }
            }
            ExecuteLogging.Log("BPLevelTT", String.Format(
                "[!]  (CloseConnection)������ ���������� � {0} ����� {1}",
                sender.LoginTT, Thread.CurrentThread.GetHashCode()));

            handler.CloseSocket(sender); //������ ����������
        }

        /// <summary>
        /// ������������ ������� �� ����� CmdRequst - ������ 
        /// ��������� ������ ��� ���������� ��
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        protected override void IncomCmdRequst(BO_Teletrack sender)
        {
            ExecuteLogging.Log("BPLevelTT",
                String.Format("[]-> ����� CmdRequst �� {0}", sender.LoginTT));

            if (sender.upgradeTT.Count != 0)
            {
                if (IsFirmwareTaskCancelled(sender))
                {
                    ClearSendingFirmware(sender);
                    SendFakeA1(sender);
                }
                else
                {
                    SendFirmware(sender);
                }
            }
            else
            {
                byte[] dataForTT = new BO_TeletrackSrv(sender).GetDataForTT();
                if (dataForTT == null)
                {
                    SendFakeA1(sender);
                }
                else
                {
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "[]<- ���������� ����� A1 ��������� {0}: {1}",
                        sender.LoginTT, Encoding.ASCII.GetString(dataForTT, 0, dataForTT.Length)));

                    StatisticDACL.CapacityInTT(sender.ID_Session, dataForTT.Length);
                    handler.SendData(sender, dataForTT);
                }
            }
        }

        /// <summary>
        /// ���������� ��������� ����� �1
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        private void SendFakeA1(BO_Teletrack sender)
        {
            handler.SendData(sender, new PacketA1TT().AssemblePacket("", new byte[0]));
            ExecuteLogging.Log("BPLevelTT", String.Format(
                "[]<- ���������� ��������� ����� A1 ��������� {0}", sender.LoginTT));
        }

        /// <summary>
        /// �������� ��������� ��������.
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        private void SendFirmware(BO_Teletrack sender)
        {
            byte[] dataForTT = sender.upgradeTT[0];
            ExecuteLogging.Log("BPLevelTT", String.Format(
                "[]<- ���������� ����� ��������� �������� ��������� {0}: {1}",
                sender.LoginTT, Encoding.ASCII.GetString(dataForTT, 0, dataForTT.Length)));

            StatisticDACL.CapacityInTT(sender.ID_Session, dataForTT.Length);

            UpdateFirmwareTaskStage(sender, FirmwareTaskStage.Sending);
            sender.SendingFirmware = true;
            handler.SendData(sender, dataForTT);
        }

        /// <summary>
        /// �������� ������ ��������� ������ �������� ��������.
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        /// <param name="stage">������� ������ ���������</param>
        private void UpdateFirmwareTaskStage(BO_Teletrack sender, FirmwareTaskStage stage)
        {
            sender.FirmwareTask.Stage = stage;
            new RepositoryFactory(MainControlDALC.ConnectionString, MainControlDALC.CommandTimeout)
                .GetFirmwareTaskRepository()
                .Update(sender.FirmwareTask, "Online Server", sender.socket.LocalEndPoint.ToString());
        }

        /// <summary>
        /// ���� �� �������� ������ �������� ��������
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        /// <returns></returns>
        private bool IsFirmwareTaskCancelled(BO_Teletrack sender)
        {
            FirmwareTask task = new RepositoryFactory(MainControlDALC.ConnectionString, MainControlDALC.CommandTimeout)
                .GetFirmwareTaskRepository()
                .GetById(sender.FirmwareTask.Id);

            if (task.Stage == FirmwareTaskStage.Cancelled)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "������ ��������� �������� ��� ��������� {0} ���� �������� ����������.",
                    sender.LoginTT));
                return true;
            }
            return false;
        }

        /// <summary>
        /// ������� ���������� � ������ �������� ��������
        /// </summary>
        /// <param name="sender"></param>
        private void ClearSendingFirmware(BO_Teletrack sender)
        {
            sender.upgradeTT.Clear();
            sender.FirmwareTask = null;
            sender.SendingFirmware = false;
        }

        protected override void DisconectOfTeletrack(ConnectedStateEvArg con_state)
        {
            BO_Teletrack bo = (BO_Teletrack) con_state.conn;

            if (!con_state.B_State && bo.InNetwork) //������ ����������
            {
                lock (activeConnobj)
                {
                    StatisticDACL.DeleteSessiontTT(bo.ID_Session);
                    if (lstActiveConnTT.ContainsKey(bo.ID_Teletrack))
                    {
                        ExecuteLogging.Log("BPLevelTT", String.Format(
                            "[!]  (DisconectOfTeletrack)������ ���������� � {0} ����� {1}",
                            bo.LoginTT, Thread.CurrentThread.GetHashCode()));

                        lstActiveConnTT.Remove(bo.ID_Teletrack);
                    }
                }
            }
        }

        /// <summary>
        /// ��������� ��� ����������, ��������� ������
        /// </summary>
        protected override void EndOfWork()
        {
            lock (activeConnobj)
            {
                foreach (BO_Teletrack bo in lstActiveConnTT.Values)
                {
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "EndOfWork: �������� ���������� � {0}", bo.LoginTT));

                    StatisticDACL.DeleteSessiontTT(bo.ID_Session);

                    //lstActiveConnTT.Remove(bo.ID_Teletrack);
                    bo.InNetwork = false;
                    handler.CloseSocket(bo); //������ ����������
                }
            }
        }

        /// <summary>
        /// ����� ����������
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        /// <param name="login"></param>
        /// <param name="password"></param>
        protected override void NewConnection(BO_Teletrack sender, string login, string password)
        {
            ExecuteLogging.Log("BPLevelTT", String.Format(
                "[+] NewConnection. ����� ���������� c ���������� Login: {0} Password: {1}, {2}",
                login, password,
                sender.socket.Connected ? sender.socket.RemoteEndPoint.ToString() : "��� ��������"));
            int id_teletrack;

            if (TeletreckDALC.AuthenticationTeletr(login, password, out id_teletrack))
            {
                sender.ID_Teletrack = id_teletrack;
                sender.LoginTT = login;
                sender.PasswordTT = password;
                lock (activeConnobj)
                {
                    if (lstActiveConnTT.ContainsKey(sender.ID_Teletrack))
                    {
                        ExecuteLogging.Log("BPLevelTT", String.Format(
                            "[!] C��������� c ���������� Login: {0} Password: {1} ��� ����. CloseConnection(sender)",
                            login, password));

                        BO_Teletrack bo_tt = (BO_Teletrack) lstActiveConnTT[sender.ID_Teletrack];
                        CloseConnection(bo_tt);
                    }

                    lstActiveConnTT.Add(sender.ID_Teletrack, sender);

                    sender.InNetwork = true;
                }
                //����� ������ � ������� ����������
                sender.ID_Session = StatisticDACL.AddSessionTT(sender.ID_Teletrack);
                sender.lstCmd = TeletreckDALC.GetLstOfCmdForTT(sender.ID_Teletrack);
                sender.lst_owners = TeletreckDALC.GetLstOwnersOfTT(sender.ID_Teletrack);

                sender.FirmwareTask = new RepositoryFactory(MainControlDALC.ConnectionString,
                    MainControlDALC.CommandTimeout)
                    .GetFirmwareTaskRepository().GetReadyTasksForSending(sender.LoginTT);

                if (sender.FirmwareTask == null || string.IsNullOrEmpty(sender.FirmwareTask.Firmware.Source))
                {
                    sender.upgradeTT.Clear();
                    ExecuteLogging.Log("BPLevelTT",
                        String.Format("[!] NewConnection. �������� Login: {0} Password: {1} �� ����� FirmwareTask",
                            login, password));
                }
                else
                {

                    sender.upgradeTT = ParserOfUpgradeForTT.StrippingOfUpg(sender.FirmwareTask.Firmware.Source);
                    sender.AttemptToUpgradeTT = COUNT_AttemptToUpgrade;
                    UpdateFirmwareTaskStage(sender, FirmwareTaskStage.Waiting);
                }

                SendAck(sender, false);
            }
            else
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "[!] NewConnection. �������� Login: {0} Password: {1} �� ������ ��������������",
                    login, password));
                handler.CloseSocket(sender); //������ ����������
            }
        }

        protected override bool IncomNewBin(BO_Teletrack sender, byte[] pk_bin, bool testCRC)
        {
            ExecuteLogging.Log("BPLevelTT", String.Format("[]-> IncomNewBin. ����� Bin: � {0} ����� : � {1}",
                BitConverter.ToInt32(pk_bin, 0), Thread.CurrentThread.GetHashCode()));

            //01.03.2012
            if (testCRC && !CRCControl.CountCRC_TT(pk_bin, pk_bin.Length))
            {
                ExecuteLogging.Log("BPLevelTT", "IncomNewBin. �� ������� ����������� ����� ");
                return false;
            }
            if (pk_bin.Length == 32)
            {

                long id_packet = MainControlDALC.InsertPackToLockCashe(sender.ID_Teletrack, pk_bin);

                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "    IncomNewBin. �������� �������� � ��������� ���. ID_Teletrack = {0} ID_Packet = {1}",
                    sender.LoginTT, id_packet));

                //MainControlDALC.SaveTransferedDataInDB();
                NewPacketFromTT newPk = new NewPacketFromTT(id_packet, sender.LoginTT, pk_bin);

                OnNewPacketFromTT(sender.lst_owners, newPk);
                return true;
                //�������� ��������� � ID_LastPackInSrvDB (� BPControllerCl)
                /*for (int i = 0; i < sender.lst_row_owners.Count; i++)
        {
            if (!sender.lst_row_owners[i].InNetwork)
                sender.lst_row_owners[i].ID_LastPackInSrvDB = id_packet;
        }*/
            }
            return false;
        }

        protected override void IncomNew64Bin(BO_Teletrack sender, byte[] pk_bin)
        {
            ExecuteLogging.Log("BPLevelTT", String.Format("[]-> IncomNew64Bin. ����� Bin64: � {0} ����� : � {1}",
                BitConverter.ToInt32(pk_bin, 0), Thread.CurrentThread.GetHashCode()));

            if (pk_bin.Length == 64)
            {
                long id_packet = MainControlDALC.InsertPackToLockCashe(sender.ID_Teletrack, pk_bin);

                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "  IncomNew64Bin.   �������� ����� � ��������� ���. ID_Teletrack = {0} ID_Packet = {1}",
                    sender.LoginTT, id_packet));

                //MainControlDALC.SaveTransferedDataInDB();
                NewPacketFromTT newPk = new NewPacketFromTT(id_packet, sender.LoginTT, pk_bin);

                OnNewPacketFromTT(sender.lst_owners, newPk);

                //�������� ��������� � ID_LastPackInSrvDB (� BPControllerCl)
                /*for (int i = 0; i < sender.lst_row_owners.Count; i++)
            {
                if (!sender.lst_row_owners[i].InNetwork)
                    sender.lst_row_owners[i].ID_LastPackInSrvDB = id_packet;
            }*/
            }
        }

        /// <summary>
        /// ��������� ����� ������� MultiBin � PacketMultiBin
        /// </summary>
        /// <param name="sender">��������</param>
        /// <param name="pk_m_bin">������</param>
        protected override bool IncomNewMBin(BO_Teletrack sender, PacketMultiBinTT pk_m_bin)
        {
            if (pk_m_bin is PacketPKMultiBin)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "[]-> ����� PacketPKMultiBin �������� {0} Bin ������� {1}",
                    pk_m_bin.Bin_Count, Thread.CurrentThread.GetHashCode()));

                int len = pk_m_bin.Packet.Length - 5; //5 ���� - ��� ������� � ���������� ����������������� Bin
                byte[] tmp = new byte[len];
                Array.Copy(pk_m_bin.Packet, 5, tmp, 0, tmp.Length);
                if (!CRCControl.CountCRC(tmp, tmp.Length))
                {
                    ExecuteLogging.Log("BPLevelTT", "�� ������� ����������� ����� ");
                    SendAck(sender, true); //
                    return false;
                }
                else
                {
                    ExecuteLogging.Log("BPLevelTT", "����������� ����� �������");
                }
            }
            else
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "[]-> ����� MultiBin �������� {0} Bin ������� ", pk_m_bin.Bin_Count));
                //================= 01.03.2012==============================
                if (CheckCRC(sender, pk_m_bin))
                    ExecuteLogging.Log("BPLevelTT", "����������� ����� �������");
                else
                    return false;
                //========================================================
            }

            if (pk_m_bin.Bin_Count == 0)
            {
                IncomCmdRequst(sender);
            }
            else
            {
                foreach (byte[] bytes in pk_m_bin.lst_newBin)
                {
                    IncomNewBin(sender, bytes, false);
                }
            }
            return true;
        }

        private bool CheckCRC(BO_Teletrack sender, PacketMultiBinTT pk_m_bin)
        {
            int cnt = 0;
            foreach (byte[] bytes in pk_m_bin.lst_newBin)
            {
                if (!CRCControl.CountCRC_TT(bytes, bytes.Length))
                {
                    ExecuteLogging.Log("BPLevelTT", string.Format("�� ������� ����������� �����.����� {0}", cnt));
                    SendAck(sender, true); //
                    return false;
                }
                cnt++;
            }
            return true;
        }

        /// <summary>
        /// ��������� ����� ������� 64MultiBin
        /// </summary>
        /// <param name="sender">��������</param>
        /// <param name="pk_m_bin">������</param>
        protected override bool IncomNew64MBin(BO_Teletrack sender, Packet64MultiBinTT pk_64m_bin)
        {
            ExecuteLogging.Log("BPLevelTT",
                string.Format("[]-> ����� 64MultiBin �������� {0} 64Bin ������� ", pk_64m_bin.Bin_Count));
            if (this.CheckCRC64(sender, pk_64m_bin))
            {
                ExecuteLogging.Log("BPLevelTT", "����������� ����� �������");
            }
            else
            {
                return false;
            }
            if (pk_64m_bin.Bin_Count == 0)
            {
                this.IncomCmdRequst(sender);
            }
            else
            {
                foreach (byte[] buffer in pk_64m_bin.lst_newBin)
                {
                    this.IncomNew64Bin(sender, buffer);
                }
            }
            return true;

        }

        /// <summary>
        /// ������������� ������� ��� �����������
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        /// <param name="pk_response">PacketResponseTT</param>
        protected override void IncomResponse(BO_Teletrack sender, PacketResponseTT pk_response)
        {
            ExecuteLogging.Log("BPLevelTT",
                String.Format("[]-> ����� Response: {0} �� ��������� {1}", pk_response.Cod_answ, sender.LoginTT));

            
            if (sender.SendingFirmware)
            {
                HandleResponseOnSendingFirmware(sender, pk_response);
            }
            else if (sender.lastOutPacketA1 != null)
            {
                MainControlDALC.SetCmdHowEx(sender.lastOutPacketA1.id_cmd, pk_response.Cod_answ, null);

                OnNewA1FromTT(sender.lastOutPacketA1.id_client, sender.lastOutPacketA1.id_cmd);
                sender.lastOutPacketA1 = null;
            }
            SendAck(sender, false);
        }

        /// <summary>
        /// ��������� ������ �� �������� ��������� ��������.
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        /// <param name="pk_response">PacketResponseTT</param>
        private void HandleResponseOnSendingFirmware(BO_Teletrack sender, PacketResponseTT pk_response)
        {
            if (pk_response.Cod_answ == 1) // �������� ������� �� ������
            {
                sender.AttemptToUpgradeTT--;

                if (sender.AttemptToUpgradeTT < 0) //3-� ��������� �������
                {
                    sender.upgradeTT.Clear();
                    if (!IsFirmwareTaskCancelled(sender))
                    {
                        UpdateFirmwareTaskStage(sender, FirmwareTaskStage.Cancelled);
                    }
                    sender.FirmwareTask = null;
                    sender.SendingFirmware = false;
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "��� ������� �������� ���������� �������� ��������� {0} ���������, ������ ��������.",
                        sender.LoginTT));
                }
                else
                {
                    if (IsFirmwareTaskCancelled(sender))
                    {
                        ClearSendingFirmware(sender);
                    }
                    else
                    {
                        ExecuteLogging.Log("BPLevelTT", String.Format(
                            "�������� {0} ������� �������� ���������� �������� ��������� {1}.",
                            sender.AttemptToUpgradeTT + 1, sender.LoginTT));
                        UpdateFirmwareTaskStage(sender, FirmwareTaskStage.ReceivedError);
                    }
                }
            }
            else
            {
                if (IsFirmwareTaskCancelled(sender))
                {
                    if (sender.upgradeTT.Count == 1)
                    {
                        // �������� ������, ������������ �� ���������� ���� - 
                        // �������� ���������
                        SetFirmwareTaskCompleted(sender);
                    }
                    ClearSendingFirmware(sender);
                }
                else
                {
                    UpdateFirmwareTaskStage(sender, FirmwareTaskStage.ReceivedOk);
                    sender.upgradeTT.RemoveAt(0); //������� ������� ���������� ����
                    sender.AttemptToUpgradeTT = COUNT_AttemptToUpgrade;
                        //��������������� ������� ������ ����� �������� ��������
                    if (sender.upgradeTT.Count == 0)
                    {
                        SetFirmwareTaskCompleted(sender);
                        ClearSendingFirmware(sender);
                    }
                }
            }
        }

        /// <summary>
        /// ��������� ������ �������� �������� ������� ���������
        /// </summary>
        /// <param name="sender"></param>
        private void SetFirmwareTaskCompleted(BO_Teletrack sender)
        {
            UpdateFirmwareTaskStage(sender, FirmwareTaskStage.Completed);
            ExecuteLogging.Log("BPLevelTT", String.Format(
                "���������� �������� ������� �������� ��������� {0}.", sender.LoginTT));
        }

        /// <summary>
        /// ����� �� ����� �1
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        /// <param name="sender">PacketA1TT</param>
        protected override void AnswOnA1(BO_Teletrack sender, PacketA1TT pk_A1)
        {
            if (sender.lastOutPacketA1 != null)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "[]-> ����� ��������� {0} �� ������� A1: {1} ����� : � {2}",
                    sender.LoginTT,
                    Encoding.ASCII.GetString(pk_A1.ComText, 0, pk_A1.ComText.Length),
                    Thread.CurrentThread.GetHashCode()));

                MainControlDALC.SetCmdHowEx(sender.lastOutPacketA1.id_cmd, 0, pk_A1.ComText);

                OnNewA1FromTT(sender.lastOutPacketA1.id_client, sender.lastOutPacketA1.id_cmd);
                sender.lastOutPacketA1 = null;
            }
            SendAck(sender, false);
        }

        /// <summary>
        /// ���������� ����� ACK
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        /// <param name="isError"></param>
        protected override void SendAck(BO_Teletrack sender, bool isError)
        {
            if (!isError)
            {
                if (sender.lstCmd.Count != 0 || sender.upgradeTT.Count != 0)
                {
                    handler.SendData(sender, PacketAckTT.ACK_OK_Y); //���� �������
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "[]<- SendAck ACK_OK_Y ���������: {0}; �����: {1}",
                        sender.LoginTT,
                        Thread.CurrentThread.GetHashCode()));
                }
                else
                {
                    handler.SendData(sender, PacketAckTT.ACK_OK_N); //��� ��, ������ ���
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "[]<- SendAck ACK_OK_N ���������: {0}; �����: {1}",
                        sender.LoginTT,
                        Thread.CurrentThread.GetHashCode()));
                }
            }
            else
            {
                if (sender.lstCmd.Count != 0 || sender.upgradeTT.Count != 0)
                {
                    handler.SendData(sender, PacketAckTT.ACK_ERR_Y); //������, ���� �������
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "[]<- SendAck ACK_ERR_Y ��������� {0}; �����: {1}",
                        sender.LoginTT,
                        Thread.CurrentThread.GetHashCode()));
                }
                else
                {
                    handler.SendData(sender, PacketAckTT.ACK_ERR_N); //������, ������ ���
                    ExecuteLogging.Log("BPLevelTT", String.Format(
                        "[]<- SendAck ACK_ERR_N ���������: {0}; �����: {1}",
                        sender.LoginTT,
                        Thread.CurrentThread.GetHashCode()));
                }
            }
            StatisticDACL.CapacityOutTT(sender.ID_Session, 1); //1 ���� (��� ����������))
        }

        /// <summary>
        /// ���������� ��������� ������� ���������
        /// </summary>
        /// <param name="packet">IPacket</param>
        /// <param name="sender">BO_Teletrack</param>
        protected override void SetStatCapacity(IPacket packet, BO_Teletrack sender)
        {
            ExecuteLogging.Log("BPLevelTT", "SetStatCapacity. ���������� ��������� ������� ���������");
            if (sender.ID_Session != 0)
            {
                StatisticDACL.CapacityInTT(sender.ID_Session, packet.PacketSize);
            }
        }

        /// <summary>
        /// ����������: ����� ��������� ���������� ������ �� ���������
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        protected override void SetStatLastPacketTime(BO_Teletrack sender)
        {
            TeletreckDALC.DTimeLastPackFromTT(sender.ID_Teletrack, DateTime.Now);
        }

        private bool CheckCRC64(BO_Teletrack sender, Packet64MultiBinTT pk_64m_bin)
        {
            int num = 0;
            foreach (byte[] buffer in pk_64m_bin.lst_newBin)
            {
                if (!CRCControl.CountCRC_TT(buffer, buffer.Length))
                {
                    ExecuteLogging.Log("BPLevelTT", string.Format("�� ������� ����������� �����.����� 64Bin {0}", num));
                    this.SendAck(sender, true);
                    return false;
                }
                num++;
            }
            return true;
        }
    }
}
