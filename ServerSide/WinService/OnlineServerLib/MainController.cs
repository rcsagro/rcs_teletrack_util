using System;
using System.Collections.Generic;
using System.Threading;
using RCS.OnlineServerLib.ClientInteraction;
using RCS.OnlineServerLib.DACL;
using RCS.OnlineServerLib.InfoTrackInteraction;
using RCS.OnlineServerLib.TeletrackInteraction;
using RCS.Trans;
using RCS.Trans.TeletrackInteraction;
using RW_Log;
using TransitServisLib;

namespace RCS.OnlineServerLib
{

    public class MainController : IDisposable
    {
        private BPControllerTTSrv ctrl_tt;
        private BPControllerCl ctrl_cl;
        private InfoTrackController ctrl_it;

        internal event IncomNewPacketFromTT newPackFromTT;
        internal event CmdTransmissionDelegate newA1FromTT;
        internal event CmdTransmissionDelegate newA1FromClient;

        //������������� ������ ���� � ��
        public volatile int savingPeriod = 60 * 1000;
        public Timer timerSavingControl;
        private Timer timerControlLogFile;
        /// <summary>
        /// ������������� � �������� ���������.
        /// </summary>
        private volatile bool stopping;

        /// <summary>
        /// ������ ���� {0}:{1} ��� ������������ {2}.
        /// </summary>
        private const string SOCKET_OPENED =
          "������ ���� {0}:{1} ��� ������������ {2}.";

        private volatile Settings settings;

        private MainController()
        {
        }

        public MainController(Settings st)
        {
            settings = st;
        }

        /// <summary>
        /// ����� �����������.
        /// </summary>
        public void Start()
        {
            ExecuteLogging.Log("BPLevelTT", "Start. ����� �����������");

            MainControlDALC.ConnectionString = settings.ConnectionString;
            MainControlDALC.CommandTimeout = settings.MsSqlCmdTimeout;
            MainControlDALC.MaxPermittedTtCount = settings.MaxPermittedTtCount;

            WaitForDbAvailability();

            if (stopping)
            {
                return;
            }

            MainControlDALC.FillLocalCacheDataSet();
            //TODO: �������� �������� ���������� ����
            if (stopping)
            {
                return;
            }

            // �������� ������������ � �������� ������
            CreateTeletrackController(settings);
            if (stopping)
            {
                return;
            }

            CreateDispatcherController(settings);
            if (stopping)
            {
                return;
            }

            CreateInfotrackController(settings);
            if (stopping)
            {
                return;
            }

            //������ ������������� �������� log-������
            timerControlLogFile = new Timer(
              new TimerCallback(TimerControlLogFile),
              settings.LogDataHoldDays,
              settings.LogDataHoldDays * 60 * 60 * 1000,
              settings.LogDataHoldDays * 60 * 60 * 1000);

            if (stopping)
            {
                return;
            }
            //������ ������������� ������ ���� � ��
            timerSavingControl = new Timer(new TimerCallback(TimerControl), null,
              settings.PeriodOfSaveCash, settings.PeriodOfSaveCash);

            savingPeriod = settings.PeriodOfSaveCash;
        }

        /// <summary>
        /// �������� ����������� ���� ������. �������� 5 �������
        /// ����������� � �� � ���������� 15 ������.
        /// <para>� ������ ������� ����������� ����������.</para>
        /// </summary>
        /// <exception cref="Exception">�� ������� ������������ 
        /// � ���� ������ �� ���������� �����.</exception>
        private void WaitForDbAvailability()
        {
            ExecuteLogging.Log("BPLevelTT", String.Format(
              "WaitForDbAvailability. �������� ����������� ��: {0}.", MainControlDALC.ConnectionString));

            for (int i = 0; i < 5; i++)
            {
                if (stopping || MainControlDALC.DatabaseAvailable())
                {
                    return;
                }
                Thread.Sleep(15000);
            }

            throw new Exception(String.Format(
              "�� ������� ������������ � ���� ������ �� ���������� �����. \r\n ������ �����������: {0}",
              MainControlDALC.ConnectionString));
        }

        /// <summary>
        /// �������� �������� �����. �������� 5 �������
        /// ����������� � �� � ���������� 15 ������.
        /// <para>� ������ ������� ����������� ����������.</para>
        /// </summary>
        /// <exception cref="Exception">"�� ������� ������� ����.</exception>
        /// <param name="handler">��������� ����������</param>
        /// <param name="ipAddress">IP �����</param>
        /// <param name="port">����</param>
        /// <param name="boType">��� ������ ������ - ��������� ����� ��� �����???</param>
        /// <param name="isKeepAliveOn">������� �� ���������� �������� ������ KeepAlive</param>
        private void WaitForOpenPort(ServerHandler handler, string ipAddress, int port,
          Type boType, bool isKeepAliveOn)
        {
            ExecuteLogging.Log("BPLevelTT", "WaitForOpenPort. �������� �������� �����. �������� 5 ������� �� 15 ���:" + ipAddress + ":" + port);

            for (int i = 0; i < 5; i++)
            {
                if (stopping || handler.ServerStart(ipAddress, port, boType, isKeepAliveOn))
                {
                    return;
                }
                ExecuteLogging.Log("BPLevelTT", String.Format("WaitForOpenPort. ������� �������� ����� {0}:{1} �{2} �� �������.", ipAddress, port, i));
                
                Thread.Sleep(15000);
            }

            throw new Exception(String.Format(
              "�� ������� ������� ���� {0}:{1}.", ipAddress, port));
        }

        /// <summary>
        /// �������� ����������� ���������� � �������� �����.
        /// </summary>
        /// <param name="st">Settings</param>
        private void CreateTeletrackController(Settings st)
        {

            ExecuteLogging.Log("BPLevelTT", "CreateTeletrackController. �������� ����������� ���������� � �������� �����.");

            ctrl_tt = new BPControllerTTSrv(this, st.CountOfWorkerThreadTT);
            ctrl_tt.DeltaIncomLastPack = st.DeltaIncomLastPack;
            ctrl_tt.newPacketFromTT += new IncomNewPacketFromTT(OnNewPacketFromTT);
            ctrl_tt.newA1FromTT += new CmdTransmissionDelegate(OnNewA1FromTT);

            WaitForOpenPort(ctrl_tt.handler, st.IpAddressTT, st.PortTT,
              typeof(BO_Teletrack), true);

            if (!stopping)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                  "CreateTeletrackController. " + SOCKET_OPENED, st.IpAddressTT, st.PortTT, "����������"));
            }
        }

        /// <summary>
        /// �������� ����������� ������������� ����� � �������� �����.
        /// </summary>
        /// <param name="st">Settings</param>
        private void CreateDispatcherController(Settings st)
        {
            ExecuteLogging.Log("BPLevelTT", "CreateDispatcherController. �������� ����������� ������������� ����� � �������� �����.");
            ctrl_cl = new BPControllerCl(this, st.CountOfWorkerThreadCl, st.PeriodOfSaveCash);
            ctrl_cl.DeltaIncomLastPackForClient = st.DeltaIncomLastPackForClient;
            //ctrl_cl.incomNewCmdFromClient += new CmdTransmissionDelegate(OnNewCmdFromClient);

            WaitForOpenPort(ctrl_cl.transpLevelCl, st.IpAddressCl, st.PortCl,
              typeof(BO_Client), true);

            if (!stopping)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                  "CreateDispatcherController. " + SOCKET_OPENED, st.IpAddressCl, st.PortCl, "������������� �����"));
            }
        }

        /// <summary>
        /// �������� ����������� ���������� � �������� �����.
        /// </summary>
        /// <param name="st">Settings</param>
        private void CreateInfotrackController(Settings st)
        {
            ExecuteLogging.Log("BPLevelTT", "CreateInfotrackController. �������� ����������� ���������� � �������� �����.");

            ctrl_it = new InfoTrackController();
            ctrl_it.newPacketFromIT += new IncomNewPacketFromTT(OnNewPacketFromTT);

            WaitForOpenPort(ctrl_it, st.IpAddressTT, 9011,
              typeof(BO_InfoTrack), true);

            if (!stopping)
            {
                
                ExecuteLogging.Log("BPLevelTT", String.Format(
                  "CreateInfotrackController. " + SOCKET_OPENED, st.IpAddressTT, 9011, "����������"));
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            stopping = true;

            if (timerSavingControl != null)
            {
                timerSavingControl.Dispose();
                timerSavingControl = null;
            }

            if (ctrl_tt != null)
            {
                ctrl_tt.Dispose();
            }

            if (ctrl_cl != null)
            {
                ctrl_cl.Dispose();
            }

            if (timerControlLogFile != null)
            {
                timerControlLogFile.Dispose();
            }
        }

        #endregion

        void OnNewPacketFromTT(List<int> owners, NewPacketFromTT n_pk)
        {
            ExecuteLogging.Log("BPLevelTT", "OnNewPacketFromTT");

            if (newPackFromTT != null)
            {
                newPackFromTT(owners, n_pk);
            }
        }

        void OnNewA1FromTT(int target, int id_cmd)
        {
            ExecuteLogging.Log("BPLevelTT", "OnNewA1FromTT");

            if (newA1FromTT != null)
            {
                newA1FromTT(target, id_cmd);
            }
        }

        //void OnNewCmdFromClient(int id_teletrack, int id_cmd)
        //{
        //  if (newA1FromClient != null)
        //  {
        //    newA1FromClient(id_teletrack, id_cmd);
        //  }
        //}

        // ������ ��������� ���������� �� ���
        // ����� ������� � ���������� � ��������
        void TimerControl(object state)
        {
            ExecuteLogging.Log("BPLevelTT", "TimerControl. ������ ��������� ���������� �� ��� ����� ������� � ����������, ���� �� ���������");
            timerSavingControl.Change(Timeout.Infinite, Timeout.Infinite);
            try
            {
                ctrl_tt.ControlEndOfSession();
                ctrl_cl.SaveData(state);
            }
            catch (Exception e)
            {
                ExecuteLogging.Log("Error_", String.Format(
                  "TimerControl" + "{0} \r\n {1}", e.Message, e.StackTrace));
            }
            finally
            {
                timerSavingControl.Change(settings.PeriodOfSaveCash, settings.PeriodOfSaveCash);
            }
        }

        public void TimerControlLogFile(object obj)
        {
            try
            {
                ExecuteLogging.DeleteOldLogFile(obj);
            }
            catch (Exception e)
            {
                ExecuteLogging.Log("Error_", String.Format(
                  "{0} \r\n {1}", e.Message, e.StackTrace));
            }
        }
    }
}
