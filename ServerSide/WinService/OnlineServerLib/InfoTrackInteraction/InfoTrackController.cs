using System;
using System.Collections.Generic;
using System.Text;
using RCS.OnlineServerLib.DACL;
using RCS.Sockets.Connection;
using RCS.Trans.TeletrackInteraction;
using RW_Log;
using TransitServisLib;

namespace RCS.OnlineServerLib.InfoTrackInteraction
{
  public class InfoTrackController : HandlingOfIncomDataTT
  {
    /// <summary>
    /// ��������� ������ �������
    /// </summary>
    /// <param name="ip_addressTT">�����</param>
    /// <param name="portTT">����</param>
    /// <returns></returns>
    public bool ServerStart(string ip_addressTT, int portTT)
    {
      if (base.ServerStart(ip_addressTT, portTT, typeof(BO_InfoTrack)))
      {
        ExecuteLogging.Log("BPLevelTT", "||****************************************************************||");
        ExecuteLogging.Log("BPLevelTT", "||            ����� ������ ������ �� ���������� ������            ||");
        ExecuteLogging.Log("BPLevelTT", "||****************************************************************||");

        return true;
      }
      else
      {
        ExecuteLogging.Log("BPLevelTT", String.Format(
          "�� ������� ������� ����� ������� {0}:{1}", ip_addressTT, portTT));
      }
      return false;
    }

    protected override void DetectPacket(ConnectionInfo conn, byte[] data, int pos)
    {
      BO_InfoTrack infoTr = (BO_InfoTrack)conn;
      string message = Encoding.ASCII.GetString(data);
      do
      {
        string msg = message;
        int tmpPos = 0;
        int curPos = 0;
        int endPack = 0;

        tmpPos = msg.IndexOf("imei=", curPos);
        curPos = msg.IndexOf("&", tmpPos);
        endPack = msg.IndexOf("imei=", curPos + 1);

        if (endPack == -1)
        {
          endPack = msg.Length;
        }

        if (curPos > tmpPos && endPack > 0)
        {
          infoTr.IMEI = msg.Substring(tmpPos + 5, curPos - (tmpPos + 5));

          byte[] pk_bin = new PacketBin().HandlerOfIncomMessage(
            msg.Substring(curPos + 5, endPack - (curPos + 5)));
          message = msg.Remove(0, endPack);
          if (pk_bin != null)
          {
            NewBinFromInfoTrack(infoTr, pk_bin);
          }
        }
      }
      while (message.Length > 0);
    }

    protected void NewBinFromInfoTrack(BO_InfoTrack sender, byte[] pk_bin)
    {
      if (sender.ID_Teletrack == 0 || sender.LoginTT == "")
      {
        int id; string login;
        if (TeletreckDALC.AuthenticationTeletr(sender.IMEI, out login, out id))
        {
          sender.ID_Teletrack = id;
          sender.LoginTT = login;
          sender.lst_owners = TeletreckDALC.GetLstOwnersOfTT(sender.ID_Teletrack);

          if (pk_bin.Length == 32)
          {
            long id_packet = MainControlDALC.InsertPackToLockCashe(
              sender.ID_Teletrack, pk_bin);

            ExecuteLogging.Log("BPLevelTT", String.Format(
              "     �������� ����� � ��������� ���. ��������: {0}; IMEI: {1}; ID_Packet: {2}.",
              sender.LoginTT, sender.IMEI, id_packet));

            //MainControlDALC.SaveTransferedDataInDB();
            NewPacketFromTT newPk = new NewPacketFromTT(id_packet, sender.LoginTT, pk_bin);

            OnNewPacketFromInfoTrack(sender.lst_owners, newPk);
          }
        }
        else
        {
          ExecuteLogging.Log("Error_", String.Format(
            "�������� � IMEI {0} �� ��������������� � ��", sender.IMEI));
          CloseSocket(sender);
        }
      }
    }

    public event IncomNewPacketFromTT newPacketFromIT;

    /// <summary>
    /// �����. ������� - ������� ����� ����� 
    /// </summary>
    private void OnNewPacketFromInfoTrack(List<int> owners, NewPacketFromTT newPk)
    {
      if (newPacketFromIT != null)
      {
        newPacketFromIT(owners, newPk);
      }
    }
  }
}
