using System;
using RW_Log;

namespace RCS.OnlineServerLib.InfoTrackInteraction
{

    public class PacketBin
    {
        int iLogID = 0;
        int unixTime = 0;
        int latitude = 0;
        int longititude = 0;
        byte speed = 0;
        byte course = 0;
        byte altitude = 0;
        byte status = 0;
        byte sensor1 = 0;
        byte sensor2 = 0;
        byte sensor3 = 0;
        byte sensor4 = 0;
        byte sensor5 = 0;
        byte sensor6 = 0;
        byte sensor7 = 0;
        byte sensor8 = 0;
        byte[] events = new byte[3];
        byte checkSum = 0;

        public byte[] HandlerOfIncomMessage(string msg)
        {
            ExecuteLogging.Log("BPLevelTT", String.Format("�����: {0}", msg));
            byte[] pkBin = null;
            try
            {
                string[] msgData = msg.Split(',');
                if (msgData.Length >= 12)
                {

                    DateTime dt = new DateTime(
                        int.Parse("20" + msgData[9].Substring(4, 2)),
                        int.Parse(msgData[9].Substring(2, 2)),
                        int.Parse(msgData[9].Substring(0, 2)),
                        int.Parse(msgData[1].Substring(0, 2)),
                        int.Parse(msgData[1].Substring(2, 2)),
                        int.Parse(msgData[1].Substring(4, 2)));

                    unixTime = ToUnixtime(dt);

                    int tmpLatitude = int.Parse(msgData[3].Remove(msgData[3].IndexOf('.'), 1)); // *6 / 100;

                    latitude = ((tmpLatitude/1000000)*60000) + (tmpLatitude%1000000)/10;


                    int tmpLongititude = int.Parse(msgData[5].Remove(msgData[5].IndexOf('.'), 1)); // *6 / 100;

                    longititude = ((tmpLongititude/1000000)*60000) + (tmpLongititude%1000000)/10;
                    if (msgData[7] != "")
                    {
                        string s = msgData[7].Replace(".", ",");
                        float sp = float.Parse(s); // (float)1.852;
                        if (sp <= 255)
                            speed = (byte) sp;
                    }

                    if (msgData[8] != "")
                    {
                        int cur = int.Parse(msgData[8].Remove(msgData[8].IndexOf('.'), 1))/100;
                        course = (byte) (cur/1.5);
                    }

                    status = msgData[2] == "A" ? (byte) 0x08 : (byte) 0;

                    if (msgData.Length > 11)
                    {
                        switch (msgData[12])
                        {
                            case "AUTO":
                                sensor1 = 1;
                                break;
                            case "OPEN":
                                sensor1 = 2;
                                break;
                            case "CLOSE":
                                sensor1 = 3;
                                break;
                            case "Vibration":
                                sensor1 = 4;
                                break;
                            case "POLL":
                                sensor1 = 5;
                                break;
                            case "SOS":
                                sensor1 = 6;
                                break;
                            case "LP":
                                sensor1 = 7;
                                break;
                        }
                    }
                    pkBin = new byte[32];
                    BitConverter.GetBytes(iLogID).CopyTo(pkBin, 0);
                    BitConverter.GetBytes(unixTime).CopyTo(pkBin, 4);
                    BitConverter.GetBytes(latitude).CopyTo(pkBin, 8);
                    BitConverter.GetBytes(longititude).CopyTo(pkBin, 12);
                    BitConverter.GetBytes(speed).CopyTo(pkBin, 16);
                    BitConverter.GetBytes(course).CopyTo(pkBin, 17);
                    BitConverter.GetBytes(altitude).CopyTo(pkBin, 18);
                    pkBin[19] = status;
                    pkBin[20] = sensor1;
                    pkBin[21] = sensor2;
                    pkBin[22] = sensor3;
                    pkBin[23] = sensor4;
                    pkBin[24] = sensor5;
                    pkBin[25] = sensor6;
                    pkBin[26] = sensor7;
                    pkBin[27] = sensor8;

                    Array.Copy(events, 0, pkBin, 28, events.Length);

                    checkSum = CountCRC(pkBin);
                    pkBin[31] = checkSum;
                }
            }
            catch (Exception e)
            {
                ExecuteLogging.Log("Error_", "������ ��� ������������ Bin ������" + e.StackTrace.ToString());
            }

            return pkBin;
        }

        public int ToUnixtime(DateTime date)
        {

            DateTime unixStartTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);

            TimeSpan timeSpan = date - unixStartTime;

            return (int) Math.Floor(timeSpan.TotalSeconds);

        }

        public byte CountCRC(byte[] data)
        {
            byte ret = 0xee;
            byte[] CRC8_DATA = new byte[]
            {
                0x00, 0x5e, 0xbc, 0xe2, 0x61, 0x3f, 0xdd, 0x83,
                0xc2, 0x9c, 0x7e, 0x20, 0xa3, 0xfd, 0x1f, 0x41,
                0x9d, 0xc3, 0x21, 0x7f, 0xfc, 0xa2, 0x40, 0x1e,
                0x5f, 0x01, 0xe3, 0xbd, 0x3e, 0x60, 0x82, 0xdc,
                0x23, 0x7d, 0x9f, 0xc1, 0x42, 0x1c, 0xfe, 0xa0,
                0xe1, 0xbf, 0x5d, 0x03, 0x80, 0xde, 0x3c, 0x62,
                0xbe, 0xe0, 0x02, 0x5c, 0xdf, 0x81, 0x63, 0x3d,
                0x7c, 0x22, 0xc0, 0x9e, 0x1d, 0x43, 0xa1, 0xff,
                0x46, 0x18, 0xfa, 0xa4, 0x27, 0x79, 0x9b, 0xc5,
                0x84, 0xda, 0x38, 0x66, 0xe5, 0xbb, 0x59, 0x07,
                0xdb, 0x85, 0x67, 0x39, 0xba, 0xe4, 0x06, 0x58,
                0x19, 0x47, 0xa5, 0xfb, 0x78, 0x26, 0xc4, 0x9a,
                0x65, 0x3b, 0xd9, 0x87, 0x04, 0x5a, 0xb8, 0xe6,
                0xa7, 0xf9, 0x1b, 0x45, 0xc6, 0x98, 0x7a, 0x24,
                0xf8, 0xa6, 0x44, 0x1a, 0x99, 0xc7, 0x25, 0x7b,
                0x3a, 0x64, 0x86, 0xd8, 0x5b, 0x05, 0xe7, 0xb9,
                0x8c, 0xd2, 0x30, 0x6e, 0xed, 0xb3, 0x51, 0x0f,
                0x4e, 0x10, 0xf2, 0xac, 0x2f, 0x71, 0x93, 0xcd,
                0x11, 0x4f, 0xad, 0xf3, 0x70, 0x2e, 0xcc, 0x92,
                0xd3, 0x8d, 0x6f, 0x31, 0xb2, 0xec, 0x0e, 0x50,
                0xaf, 0xf1, 0x13, 0x4d, 0xce, 0x90, 0x72, 0x2c,
                0x6d, 0x33, 0xd1, 0x8f, 0x0c, 0x52, 0xb0, 0xee,
                0x32, 0x6c, 0x8e, 0xd0, 0x53, 0x0d, 0xef, 0xb1,
                0xf0, 0xae, 0x4c, 0x12, 0x91, 0xcf, 0x2d, 0x73,
                0xca, 0x94, 0x76, 0x28, 0xab, 0xf5, 0x17, 0x49,
                0x08, 0x56, 0xb4, 0xea, 0x69, 0x37, 0xd5, 0x8b,
                0x57, 0x09, 0xeb, 0xb5, 0x36, 0x68, 0x8a, 0xd4,
                0x95, 0xcb, 0x29, 0x77, 0xf4, 0xaa, 0x48, 0x16,
                0xe9, 0xb7, 0x55, 0x0b, 0x88, 0xd6, 0x34, 0x6a,
                0x2b, 0x75, 0x97, 0xc9, 0x4a, 0x14, 0xf6, 0xa8,
                0x74, 0x2a, 0xc8, 0x96, 0x15, 0x4b, 0xa9, 0xf7,
                0xb6, 0xe8, 0x0a, 0x54, 0xd7, 0x89, 0x6b, 0x35
            };

            data[31] = 0;
            //ret = CheckSummArr(s_buffer+1, position);
            //BYTE CheckSummArr(BYTE *pDat,BYTE Size)
            {
                byte i = 0;
                byte CRC = 0;

                for (i = 0; i < 32; i++)
                {
                    CRC = CRC8_DATA[CRC ^ data[i]];
                }
                ret = CRC;
            }

            return ret;
        }
    }
}
