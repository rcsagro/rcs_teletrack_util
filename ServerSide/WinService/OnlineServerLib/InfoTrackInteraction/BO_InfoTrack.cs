using RCS.Trans.TeletrackInteraction;

namespace RCS.OnlineServerLib.InfoTrackInteraction
{
  public class BO_InfoTrack : BO_Teletrack
  {
    /// <summary>
    /// imei инфотрека
    /// </summary>
    public string IMEI
    {
      get { return imei; }
      set { imei = value; }
    }
    private string imei = "";
  }
}
