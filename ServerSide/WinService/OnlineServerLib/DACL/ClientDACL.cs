using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using RCS.OnlineServerLib.LocalCacheSrv;
using RW_Log;

namespace RCS.OnlineServerLib.DACL
{
  public class ClientDACL : MainControlDALC
  {
    /// <summary>
    /// Error_
    /// </summary>
    private const string ERROR_FILE_PREFIX = "Error_";
    /// <summary>
    /// {0} \r\n {1}
    /// </summary>
    private const string ERROR_TXT_FORMAT = "{0} \r\n {1}";

    /// <summary>
    /// �������� �������������� ������ ���������� � ��������
    /// </summary>
    /// <param name="log"></param>
    /// <param name="pass"></param>
    /// <param name="id_client"></param>
    /// <param name="isActive"></param>
    /// <param name="inNetwork"></param>
    /// <param name="id_packet"></param>
    /// <param name="dTimeDeliveredPack"></param>
    /// <param name="id_LastPacketInSrDB"></param>
    /// <param name="id_User"></param>
    /// <returns></returns>
    public static bool AuthenticationClient(string log, string pass, out int id_client,
      out bool isActive, out bool inNetwork, out long id_packet,
      out DateTime dTimeDeliveredPack, out long id_LastPacketInSrDB, out int id_User)
    {
      id_client = 0;
      isActive = false;
      inNetwork = false;
      id_packet = 0;
      dTimeDeliveredPack = DateTime.Now;
      id_LastPacketInSrDB = 0;
      id_User = 0;

      lock (locker)
      {
        LocalCache.ClientInfoRow[] cl_dr =
          (LocalCache.ClientInfoRow[])cache_ds.ClientInfo.Select(
            string.Format(@"LoginCl = '{0}' AND Password = '{1}'", log, pass));

        if (cl_dr.Length != 0)
        {
          id_client = cl_dr[0].ID_Client;
          //isActive = cl_dr[0].IsActiveState;
          inNetwork = cl_dr[0].InNetwork;
          id_packet = cl_dr[0].ID_Packet;
          dTimeDeliveredPack = cl_dr[0].DTimeDeliveredPack;
          id_LastPacketInSrDB = cl_dr[0].ID_LastPackInSrvDB;
          id_User = cl_dr[0].ID_User;
          LocalCache.UserInfoRow rw = cache_ds.UserInfo.FindByID_User(id_User);
          if (rw != null)
          {
            isActive = rw.IsActiveState;
          }
          return true;
        }

      }
      return false;
    }

    /// <summary>
    /// �������� �������������� ������ ���������� � ��������
    /// </summary>
    /// <param name="log">�����</param>
    /// <param name="pass">������</param>
    /// <param name="id_client"></param>
    /// <param name="isActive"></param>
    /// <param name="inNetwork"></param>
    /// <param name="id_packet"></param>
    /// <param name="dTimeDeliveredPack"></param>
    /// <param name="id_LastPacketInSrDB"></param>
    /// <param name="id_User"></param>
    /// <param name="descr"></param>
    /// <returns></returns>
    public static bool AuthenticationClient(string log, string pass,
      out int id_client, out bool isActive, out bool inNetwork,
      out long id_packet, out DateTime dTimeDeliveredPack, out long id_LastPacketInSrDB,
      out int id_User, out string descr)
    {
      id_client = 0;
      isActive = false;
      inNetwork = false;
      id_packet = 0;
      dTimeDeliveredPack = DateTime.Now;
      id_LastPacketInSrDB = 0;
      id_User = 0;
      descr = "";

      lock (locker)
      {
        LocalCache.ClientInfoRow[] cl_dr =
          (LocalCache.ClientInfoRow[])cache_ds.ClientInfo.Select(
            string.Format(@"LoginCl = '{0}' AND Password = '{1}'", log, pass));

        if (cl_dr.Length != 0)
        {
          id_client = cl_dr[0].ID_Client;
          //isActive = cl_dr[0].IsActiveState;
          inNetwork = cl_dr[0].InNetwork;
          id_packet = cl_dr[0].ID_Packet;
          dTimeDeliveredPack = cl_dr[0].DTimeDeliveredPack;
          id_LastPacketInSrDB = cl_dr[0].ID_LastPackInSrvDB;
          id_User = cl_dr[0].ID_User;
          LocalCache.UserInfoRow rw = cache_ds.UserInfo.FindByID_User(id_User);
          if (rw != null)
          {
            isActive = rw.IsActiveState;
          }
          if (!cl_dr[0].IsDescribeNull())
          {
            descr = cl_dr[0].Describe;
          }
          cl_dr[0].InNetwork = true;//� on-line ������ 
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="flag"></param>
    public static void SetInNetworkClient(int id, bool flag)
    {
      lock (locker)
      {
        LocalCache.ClientInfoRow rw = cache_ds.ClientInfo.FindByID_Client(id);
        if (rw != null)
        {
          rw.BeginEdit();
          rw.InNetwork = flag;
          rw.EndEdit();
        }
      }
    }

    /// <summary>
    /// ���������� ������ �����������, �� ���������������� ������
    /// </summary>
    /// <param name="id_client">id ������� � �� �������</param>
    /// <returns> List </returns>
    public static List<int> GetLstExecCommands(int id_client)
    {
      List<int> lst = new List<int>();
      lock (locker)
      {
        LocalCache.CommandRow[] cmd_rw =
          (LocalCache.CommandRow[])cache_ds.Command.Select(
            string.Format(@"ID_Client = '{0}' AND ExecuteState = '{1}'",
            id_client, (byte)A1CmdHandlingStatus.TeletrackAnswered));

        for (int i = 0; i < cmd_rw.Length; i++)
        {
          lst.Add(cmd_rw[i].ID_Command);
        }
      }
      return lst;
    }

    /// <summary>
    /// ���������� ��������� ����������� �������
    /// </summary>
    /// <param name="idInClDb">id � �� �������</param>
    /// <param name="idInSrDb">id � �� �������</param>
    /// <param name="cod">��� ����������</param>
    /// <param name="cmdText">����� � ���� �1</param>
    /// <returns></returns>
    public static bool GetExecCmdForQvit(int id_cmd, out int id_teletrack,
      out int idInClDb, out byte cod, out byte[] cmdText)
    {
      bool rez = false;
      idInClDb = 0;
      id_teletrack = 0;
      cod = new byte();
      cmdText = new byte[0];
      lock (locker)
      {
        LocalCache.CommandRow cmd_rw = cache_ds.Command.FindByID_Command(id_cmd);

        if (cmd_rw != null)
        {
          id_teletrack = cmd_rw.ID_Teletrack;
          idInClDb = cmd_rw.ID_ComInClDB;
          if (!cmd_rw.IsAnswCodeNull())
          {
            cod = cmd_rw.AnswCode;
          }
          if (!cmd_rw.IsAnswDataNull())
          {
            cmdText = cmd_rw.AnswData;
          }
          rez = true;
        }
      }
      return rez;
    }

    /// <summary>
    /// ��������� �������� �� ������ ������ ���������� ���������
    /// </summary>
    /// <param name="id_client"></param>
    /// <returns></returns>
    public static bool VerificatIsOwner(int id_client, string loginTT)
    {
      lock (locker)
      {
        LocalCache.TeletrackInfoRow[] tt_dr =
          (LocalCache.TeletrackInfoRow[])cache_ds.TeletrackInfo.Select(
            string.Format(@"LoginTT = '{0}'", loginTT));

        if (tt_dr.Length == 0)
        {
          return false;
        }

        LocalCache.CommunicTableRow[] communic_dr =
          (LocalCache.CommunicTableRow[])cache_ds.CommunicTable.Select(
            string.Format(@"ID_Client = {0} AND ID_Teletrack = {1}",
            id_client, tt_dr[0].ID_Teletrack));

        return communic_dr.Length != 0;
      }
    }

    /// <summary>
    /// ������� ����������� ������� � �� � ��������� ���
    /// </summary>
    /// <param name="LoginTT"></param>
    /// <param name="id_client"></param>
    /// <param name="id_comInClDB"></param>
    /// <param name="newCmd"></param>
    /// <param name="dispatcherIP">IP ����������.</param>
    /// <param name="idCmdInSrv"></param>
    /// <returns></returns>
    public static int InsertNewCmd(string LoginTT, int id_client, int id_comInClDB,
      byte[] newCmd, string dispatcherIP, out int idCmdInSrv)
    {
      const int deltaDays = 2; //(��������� ��������. ������� ��������� 2 ���)
      int id_teletr = 0;
      idCmdInSrv = 0;

      DateTime checkInTime = DateTime.Now;
      DateTime expirationTime = DateTime.Now.AddDays(deltaDays);

      try
      {
        using (SqlConnection cn = new SqlConnection(ConnectionString))
        {
          using (SqlCommand cmd = new SqlCommand("insert_command", cn))
          {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = CommandTimeout;
            cmd.Parameters.Add(new SqlParameter("@id_client", id_client));
            cmd.Parameters.Add(new SqlParameter("@loginTT", LoginTT));
            cmd.Parameters.Add(new SqlParameter("@id_cmdClDB", id_comInClDB));
            cmd.Parameters.Add(new SqlParameter("@cmdtext", newCmd));
            cmd.Parameters.Add(new SqlParameter("@checkInTime", checkInTime));
            cmd.Parameters.Add(new SqlParameter("@expirationTime", expirationTime));
            cmd.Parameters.Add(new SqlParameter("@dispatcher_ip", dispatcherIP));
            SqlParameter param = new SqlParameter("@id_teletreck", id_teletr);
            param.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(param);
            SqlParameter param1 = new SqlParameter("@id_cmd", idCmdInSrv);
            param1.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(param1);

            cmd.Connection.Open();
            //���������� id ������� � �� �������.
            cmd.ExecuteScalar();
            if (param.Value != DBNull.Value)
            {
              id_teletr = (int)(param.Value);
            }
            if (param1.Value != DBNull.Value)
            {
              idCmdInSrv = (int)(param1.Value);
            }
          }
        }
      }
      catch (Exception e)
      {
        ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
          ERROR_TXT_FORMAT, e.Message, e.StackTrace));
      }
      return id_teletr;
    }

    /// <summary>
    /// ������������� ����� ���������� ������ � �� ��� ������� �������
    /// </summary>
    /// <param name="id_client"></param>
    /// <param name="lastPack"></param>
    public static void SetLastPkForClient(int id_client, long lastPack)
    {
      lock (locker)
      {
        LocalCache.ClientInfoRow cl_rw =
          cache_ds.ClientInfo.FindByID_Client(id_client);

        if (cl_rw != null)
        {
          cl_rw.BeginEdit();
          cl_rw.ID_LastPackInSrvDB = lastPack;
          cl_rw.EndEdit();
        }
      }
    }

    /// <summary>
    /// ������������� ����� ���������� ������������� ������� ������
    /// </summary>
    /// <param name="id_client"></param>
    /// <param name="lastPack"></param>
    public static void LastDeliveredPackToClient(int id_client, long lastPack)
    {
      lock (locker)
      {
        LocalCache.ClientInfoRow cl_rw = cache_ds.ClientInfo.FindByID_Client(id_client);
        if (cl_rw != null)
        {
          cl_rw.BeginEdit();
          cl_rw.ID_Packet = lastPack;
          cl_rw.DTimeDeliveredPack = DateTime.Now;
          cl_rw.EndEdit();
        }
      }
    }
  }
}
