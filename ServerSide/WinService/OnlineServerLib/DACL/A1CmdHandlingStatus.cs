namespace RCS.OnlineServerLib.DACL
{
  /// <summary>
  /// ������� ��������� ������� A1.
  /// </summary>
  enum A1CmdHandlingStatus
  {
    /// <summary>
    /// 0 - ����� �������, ��������� ������������, ��� ��� 
    /// ������������ �������, ��������� ��������� ����������..
    /// </summary>
    Pending = 0,
    /// <summary>
    /// 1 - ������� ���������� ���������� (�� ��������� ������� �����).
    /// </summary>
    TeletrackAnswered = 1,
    /// <summary>
    /// 2 - ��������� ���������. ����� ��������� ����������.
    /// </summary>
    Done = 2
  }
}
