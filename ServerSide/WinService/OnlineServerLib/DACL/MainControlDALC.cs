using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using RCS.Lic;
using RCS.OnlineServerLib.LocalCacheSrv;
using RW_Log;

namespace RCS.OnlineServerLib.DACL
{
    /// <summary>
    /// ����� ������ � �������
    /// </summary>
    public abstract class MainControlDALC
    {
        /// <summary>
        /// {0} \r\n {1}
        /// </summary>
        private const string ERROR_TXT_FORMAT = "{0} \r\n {1}";

        /// <summary>
        /// ������������ ���-�� ������� ������ ����������, 
        /// ������� ����� ������� � ����.
        /// </summary>
        public const int TT_INSERT_PACKET_SIZE = 1000;

        /// <summary>
        /// Error_
        /// </summary>
        private const string ERROR_FILE_PREFIX = "Error_";

        private static string _connectionString = "";

        public static string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        public static int CommandTimeout = 300;
        public static int MaxPermittedTtCount;

        public static LocalCache cache_ds = new LocalCache();

        static SqlDataAdapter sqladap;

        protected static object locker = new object();

        /// <summary>
        /// ��������� TransferedData �� ���� � ��
        /// �������� ������� ����� BulkCopy
        /// </summary>
        public static void SaveTransfDataBulkCopy()
        {
            try
            {
                ExecuteLogging.Log("BPLevelTT", "SaveTransfDataBulkCopy. ��������� TransferedData �� ���� � ��");

                lock (locker)
                {
                    using (SqlConnection cnDestination = new SqlConnection(ConnectionString))
                    {

                        cnDestination.Open();
                        ExecuteLogging.Log("BPLevelTT", String.Format(
                            "SaveTransfDataBulkCopy. ������ ������ � ��. cash_ds.TransferedData.Rows.Count == {0}",
                            cache_ds.TransferedData.Rows.Count));

                        using (SqlTransaction txn = cnDestination.BeginTransaction())
                        {
                            using (SqlBulkCopy bulkcopy =
                                new SqlBulkCopy(cnDestination, SqlBulkCopyOptions.KeepIdentity, txn))
                            {
                                bulkcopy.DestinationTableName = "TransferedData";

                                bulkcopy.BatchSize = TT_INSERT_PACKET_SIZE;

                                //bulkcopy.ColumnMappings.Add(0, 0);
                                bulkcopy.ColumnMappings.Add(1, 1);
                                bulkcopy.ColumnMappings.Add(2, 2);
                                bulkcopy.ColumnMappings.Add(3, 3);

                                bulkcopy.WriteToServer(cache_ds.TransferedData);
                                bulkcopy.Close();
                            }
                            txn.Commit();
                            //������� ������� � ����
                            cache_ds.TransferedData.Clear();
                            ExecuteLogging.Log("BPLevelTT", String.Format("SaveTransfDataBulkCopy. ����� ������ � ��. cash_ds.TransferedData.Rows.Count == {0}",
                            cache_ds.TransferedData.Rows.Count));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
                    "SaveTransfDataBulkCopy. " + ERROR_TXT_FORMAT, e.Message, e.StackTrace));
            }
        }

        /// <summary>
        /// ������� ������������ ������ � ��������� ���  
        /// </summary>
        /// <param name="id_tltr">��������</param>
        /// <param name="pack">�����</param>
        /// <returns></returns>
        public static long InsertPackToLockCashe(int id_tltr, byte[] pack)
        {
            long id_pack = 0;
            lock (locker)
            {
                LocalCache.TransferedDataRow new_pack = cache_ds.TransferedData.NewTransferedDataRow();

                new_pack.ID_Teletrack = id_tltr;
                new_pack.IncomData = pack;
                new_pack.DTimeIncomData = DateTime.Now;
                id_pack = (long) new_pack.ID_Packet;

                cache_ds.TransferedData.Rows.Add(new_pack);
            }

            if (cache_ds.TransferedData.Rows.Count >= TT_INSERT_PACKET_SIZE)
            {
                SaveTransfDataBulkCopy();
            }

            return id_pack;
        }

        /// <summary>
        /// ��������� ��������� ��� ��� ������
        /// </summary>
        public static void FillLocalCacheDataSet()
        {
            ExecuteLogging.Log("BPLevelTT", "FillLocalCacheDataSet. �������� ���������� ����.");
            ExecuteLogging.Log("BPLevelTT", String.Format("FillLocalCacheDataSet. ������ ��: {0}.", GetDbVersion()));

            FillTransferedData();
            FillClientInfo();
            FillUserInfo();
            FillTeletrackInfo();
            FillCommunicTable();
            FillCommand();
            FillVersionControl();

            SqlDependency.Start(ConnectionString);
            IsTableChange();

            ExecuteLogging.Log("BPLevelTT", "FillLocalCacheDataSet. ��������� ��� ��������.");
        }

        private const string VERSION_DB =
            "SELECT value FROM sys.extended_properties WHERE name = 'VersionDB'";

        /// <summary>
        /// ���������� ������ ��, ���������� � ����������� �������� VersionDB.
        /// </summary>
        /// <returns>������ ��.</returns>
        private static string GetDbVersion()
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(VERSION_DB, con))
                {
                    command.CommandTimeout = CommandTimeout;
                    con.Open();
                    object obj = command.ExecuteScalar();
                    return obj != null ? (string) obj : "�� ������";
                }
            }
        }

        /// <summary>
        /// ��������� UserInfo � ����
        /// </summary>
        private static void FillUserInfo()
        {
            ExecuteLogging.Log("BPLevelTT", "FillUserInfo. ��������� UserInfo � ����");

            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("selectUserInfo", cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = CommandTimeout;
                    sqladap = new SqlDataAdapter(cmd);
                    sqladap.Fill(cache_ds.UserInfo);
                }
            }
            ExecuteLogging.Log("BPLevelTT", String.Format("FillUserInfo. ��������� {0} �������������.", cache_ds.UserInfo.Count));
        }

        /// <summary>
        /// ��������� VersionControl � ����
        /// </summary>
        private static void FillVersionControl()
        {
            ExecuteLogging.Log("BPLevelTT", "FillVersionControl. ��������� VersionControl � ����");

            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("selectVersionControl", cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = CommandTimeout;
                    sqladap = new SqlDataAdapter(cmd);
                    sqladap.Fill(cache_ds.VersionControl);
                }
            }
          
            ExecuteLogging.Log("BPLevelTT", String.Format("FillVersionControl. ��������� {0} ������� ������� ������.", cache_ds.VersionControl.Count));
        }

        /// <summary>
        /// ��������� ClientInfo � ����
        /// </summary>
        private static void FillClientInfo()
        {
            ExecuteLogging.Log("BPLevelTT", "FillClientInfo. ��������� ClientInfo � ����");

            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("selectClientInfo", cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = CommandTimeout;
                    sqladap = new SqlDataAdapter(cmd);
                    sqladap.Fill(cache_ds.ClientInfo);
                }
            }

           
            ExecuteLogging.Log("BPLevelTT", String.Format(
                "FillClientInfo. ��������� {0} �����������.", cache_ds.ClientInfo.Count));

            foreach (LocalCacheSrv.LocalCache.ClientInfoRow row in cache_ds.ClientInfo.Rows)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "  ��������� {0} {1}(ID: {2}); ��������� ����� ��������: {3}; ID ���������� ������ � ��: {4}",
                    String.IsNullOrEmpty(row.LoginCl) ? "������ null" : row.LoginCl,
                    row.IsDescribeNull() ? "" : row.Describe,
                    row.ID_Client, row.DTimeDeliveredPack, row.ID_LastPackInSrvDB));
            }
        }

        /// <summary>
        /// SELECT TOP {0} * FROM dbo.TeletrackInfo
        /// </summary>
        private const string SELECT_TOP_TT_SQL =
            "SELECT TOP {0} * " +
            "FROM dbo.TeletrackInfo " +
            "ORDER BY ID_Teletrack";

        /// <summary>
        /// SELECT * FROM dbo.TeletrackInfo
        /// </summary>
        private const string SELECT_ALL_TT_SQL =
            "SELECT * FROM dbo.TeletrackInfo";

        /// <summary>
        /// ��������� TeletrackInfo � ����
        /// </summary>
        private static void FillTeletrackInfo()
        {
            ExecuteLogging.Log("BPLevelTT", "FillTeletrackInfo. ��������� TeletrackInfo � ����");

            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(GetSelectTtSql(), cn))
                {
                    cmd.CommandTimeout = CommandTimeout;
                    sqladap = new SqlDataAdapter(cmd);
                    sqladap.Fill(cache_ds.TeletrackInfo);
                }
            }

           
            ExecuteLogging.Log("BPLevelTT", String.Format(
                "FillTeletrackInfo. ��������� {0} ���������� (��������: {1} ����������).",
                cache_ds.TeletrackInfo.Count,
                LicDetectorFactory.Active ? MaxPermittedTtCount.ToString() : "��� ����������� ���-��"));

            foreach (LocalCacheSrv.LocalCache.TeletrackInfoRow row in cache_ds.TeletrackInfo.Rows)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "  �������� {0}(ID: {1}); Timeout: {2}; ��������� ����� ������: {3}",
                    String.IsNullOrEmpty(row.LoginTT) ? "������ null" : row.LoginTT,
                    row.ID_Teletrack, row.Timeout,
                    row.IsDTimeLastPackNull() ? "��� ��������" : row.DTimeLastPack.ToString()));
            }
        }

        /// <summary>
        /// ������������ ������ ������� ����������.
        /// </summary>
        /// <returns>����� ������� ����������.</returns>
        private static string GetSelectTtSql()
        {
            return LicDetectorFactory.Active
                ? String.Format(SELECT_TOP_TT_SQL, MaxPermittedTtCount)
                : SELECT_ALL_TT_SQL;
        }

        /// <summary>
        /// ������ �� ������� CommunicTable � ����������� � ��������
        /// SELECT TOP {0} * FROM dbo.TeletrackInfo.
        /// </summary>
        private const string SELECT_TOP_COMMUNIC_SQL =
            "WITH TtIdList(ID) AS ( " +
            "  SELECT TOP {0} ID_Teletrack " +
            "  FROM dbo.TeletrackInfo " +
            "  ORDER BY 1 ) " +
            "SELECT ct.* " +
            "FROM TtIdList AS ti JOIN dbo.CommunicTable AS ct " +
            "  ON ti.ID = ct.ID_Teletrack";

        /// <summary>
        /// SELECT * FROM dbo.CommunicTable.
        /// </summary>
        private const string SELECT_ALL_COMMUNIC_SQL =
            "SELECT * FROM dbo.CommunicTable";

        /// <summary>
        /// ������������ ������ ������� �� CommunicTable.
        /// </summary>
        /// <returns>����� ������� ����������.</returns>
        private static string GetSelectCommunicSql()
        {
            return LicDetectorFactory.Active
                ? String.Format(SELECT_TOP_COMMUNIC_SQL, MaxPermittedTtCount)
                : SELECT_ALL_COMMUNIC_SQL;
        }

        /// <summary>
        /// ��������� CommunicTable � ����. 
        /// �������� ������ ����� FillTeletrackInfo().
        /// </summary>
        private static void FillCommunicTable()
        {
            ExecuteLogging.Log("BPLevelTT", "FillCommunicTable. ��������� CommunicTable � ����");

            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(GetSelectCommunicSql(), cn))
                {
                    cmd.CommandTimeout = CommandTimeout;
                    sqladap = new SqlDataAdapter(cmd);
                    sqladap.Fill(cache_ds.CommunicTable);
                }
            }

           
            ExecuteLogging.Log("BPLevelTT", String.Format(
                "FillCommunicTable. ��������� {0} ������� ������� ������.", cache_ds.CommunicTable.Count));
        }

        /// <summary>
        /// ��������� Command � ����
        /// </summary>
        private static void FillCommand()
        {
            ExecuteLogging.Log("BPLevelTT", "FillCommand. ��������� Command � ����");

            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("selectCommand", cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = CommandTimeout;
                    sqladap = new SqlDataAdapter(cmd);
                    sqladap.Fill(cache_ds.Command);
                }
            }
           
            ExecuteLogging.Log("BPLevelTT", String.Format(
                "FillCommand. ��������� {0} ������� ������� ������.", cache_ds.Command.Count));
        }

        /// <summary>
        /// �������� id_packet � �������� ���������� ��������� � ����
        /// </summary>
        public static void FillTransferedData()
        {
            try
            {
                ExecuteLogging.Log("BPLevelTT", "FillTransferedData. �������� id_packet � �������� ���������� ��������� � ����");
   
                using (SqlConnection cn = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("selectTransferedData", cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = CommandTimeout;
                        cmd.Connection.Open();
                        long res = 0;
                        object ob = cmd.ExecuteScalar();
                        if (ob != null)
                        {
                            res = (long) ob;
                        }
                        cache_ds.TransferedData.ID_PacketColumn.AutoIncrement = true;
                        cache_ds.TransferedData.ID_PacketColumn.AutoIncrementSeed = res + 1;
                    }
                    cache_ds.TransferedData.ID_PacketColumn.AutoIncrementStep = 1;
                }
            }
            catch (Exception e)
            {
                ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
                    ERROR_TXT_FORMAT, e.Message, e.StackTrace));
            }
        }

        /// <summary>
        /// ������� �������������� �������
        /// </summary>
        /// <param name="id_cmd">id_cmd � �� �������</param>
        public static void DeleteQvitCmd(int id_cmd)
        {
            lock (locker)
            {
                LocalCache.CommandRow cmd_row = cache_ds.Command.FindByID_Command(id_cmd);

                if (cmd_row != null)
                {
                    cmd_row.ExecuteState = (byte) A1CmdHandlingStatus.Done;
                }
            }
        }

        /// <summary>
        /// ������� ����������� ��� ����� ������� �� ���������
        /// </summary>
        /// <param name="id_cmd"></param>
        public static void DeleteUpgrade(int id_cmd)
        {
            lock (locker)
            {
                LocalCache.CommandRow cmd_row = cache_ds.Command.FindByID_Command(id_cmd);

                if (cmd_row != null)
                {
                    cmd_row.Delete();
                }
            }
        }

        /// <summary>
        /// ������������� ������� � ��������� ���������
        /// </summary>
        /// <param name="id_cmd"></param>
        /// <param name="answ_cod"></param>
        /// <param name="a1"></param>
        public static void SetCmdHowEx(int id_cmd, byte answ_cod, byte[] a1)
        {
            lock (locker)
            {
                LocalCache.CommandRow cmd_row = cache_ds.Command.FindByID_Command(id_cmd);

                if (cmd_row != null)
                {
                    cmd_row.ExecuteState = (byte) A1CmdHandlingStatus.TeletrackAnswered;
                    cmd_row.AnswCode = answ_cod;
                    if (a1 != null)
                    {
                        cmd_row.AnswData = a1;
                    }
                }

                ExecuteLogging.Log("BPLevelTT", "����� ��������� �� ���� � ������� Command");
                SaveCommandTableChanges();
            }
        }

        /// <summary>
        /// ��������� ����� �� ����� ������
        /// �������� ������ � ���������
        /// </summary>
        /// <param name="id_client">������</param>
        /// <param name="id_teletrack">��������</param>
        /// <returns>bool</returns>
        static bool IsOwner(int id_client, int id_teletrack)
        {
            lock (locker)
            {
                return cache_ds.CommunicTable.Select(String.Format(
                    @"ID_Client = {0} AND ID_Teletrack = {1}", id_client, id_teletrack)).Length != 0;
            }
        }

        /// <summary>
        /// ���������� ������� � ����������� ������� ����������� ������
        /// </summary>
        /// <param name="id_client">Id ����������</param>
        /// <param name="clientDescr">�������� ����������</param>
        /// <param name="logTT">�� ������ ���������</param>
        /// <param name="param1">� ������ ������</param>
        /// <param name="param2">�� �����</param>
        /// <param name="lastId">Last id</param>
        /// <param name="errorOccurred">True - ��������� ������ �� ����� ����������.</param>
        /// <returns>�������</returns>
        public static Dictionary<long, byte[]> GetDataForSendQuery(int id_client, string clientDescr,
            string logTT, long param1, long param2, out long lastId, out bool errorOccurred)
        {
            lastId = 0;
            errorOccurred = false;

            //�������� �������� �� �������������� ��������� ������� �������
            Dictionary<long, byte[]> dict = new Dictionary<long, byte[]>();

            LocalCache.TeletrackInfoRow[] dr;
            lock (locker)
            {
                //������� ������� �� ���������� ����
                dr = (LocalCache.TeletrackInfoRow[]) cache_ds.TeletrackInfo.Select(
                    string.Format(@"LoginTT = '{0}'", logTT));
            }

            if (dr.Length == 0 || !IsOwner(id_client, dr[0].ID_Teletrack))
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "�������� {0} ����������� � �� ��� �� ����������� ������� {1} (id: {2}).",
                    logTT, clientDescr, id_client));
            }
            else
            {
                lock (locker)
                {
                    LocalCache.TransferedDataRow[] drPk =
                        (LocalCache.TransferedDataRow[]) cache_ds.TransferedData.Select(
                            string.Format(@"ID_Teletrack = '{0}' AND ID_Packet > '{1}' AND ID_Packet <= '{2}'",
                                dr[0].ID_Teletrack, param1, param2));

                    for (int i = 0; i < drPk.Length; i++)
                    {
                        dict.Add(drPk[i].ID_Packet, drPk[i].IncomData);
                    }
                }

                try
                {
                    using (SqlConnection cn = new SqlConnection(ConnectionString))
                    {
                        //���������� ���������� ��� ��_���������
                        using (SqlCommand cmd = new SqlCommand("selectForSendQuery", cn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = CommandTimeout;
                            cmd.Parameters.Add(new SqlParameter("@id_client", id_client));
                            cmd.Parameters.Add(new SqlParameter("@loginTT", logTT));
                            cmd.Parameters.Add(new SqlParameter("@param1", param1));
                            cmd.Parameters.Add(new SqlParameter("@param2", param2));
                            cmd.Connection.Open();

                            using (SqlDataReader rdr = cmd.ExecuteReader())
                            {
                                bool flag = true;
                                //���������� ������� ������������ �������
                                while (rdr.Read())
                                {
                                    if (flag)
                                    {
                                        lastId = rdr.GetInt64(0);
                                        flag = false;
                                    }
                                    dict.Add(rdr.GetInt64(0), rdr.GetSqlBinary(1).Value);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    errorOccurred = true;
                    ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
                        ERROR_TXT_FORMAT, e.Message, e.StackTrace));
                }
            }
            return dict;
        }

        /// <summary>
        /// ��������� ���������� ��������� � ������� TeletrackInfo
        /// </summary>
        /// <returns>bool</returns>
        public static bool SaveTeletrackInfoChanges()
        {
            bool rez = false;
            //int id_client = 0;
            lock (locker)
            {
                LocalCache.TeletrackInfoDataTable tmpdt = cache_ds.TeletrackInfo;

                if (tmpdt != null)
                {
                    try
                    {
                        using (SqlConnection cn = new SqlConnection(ConnectionString))
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Connection = cn;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.CommandTimeout = CommandTimeout;
                                cn.Open();

                                for (int i = 0; i < tmpdt.Rows.Count; i++)
                                {
                                    switch (tmpdt[i].RowState)
                                    {
                                        case DataRowState.Modified:
                                            cmd.CommandText = "updateCurDataInTeletrackInfo";
                                            cmd.Parameters.Add(new SqlParameter("@ID_Teletrack", tmpdt[i].ID_Teletrack));
                                            //cmd.Parameters.Add(new SqlParameter("@InNetwork", tmpdt[i].InNetwork));
                                            cmd.Parameters.Add(new SqlParameter("@DTimeLastPack", tmpdt[i].DTimeLastPack));
                                            int row = cmd.ExecuteNonQuery();
                                            cmd.Parameters.Clear();
                                            if (row == 0)
                                            {
                                                tmpdt[i].AcceptChanges();
                                            }
                                            break;

                                            #region ���������������

                                            /*
                                case DataRowState.Deleted:
                                    {
                                        cmd.CommandText = "delRecInClientInfo";

                                        cmd.Parameters.Add(new SqlParameter("@ID_Client", tmpdt[i].ID_Client));
                                        int row = cmd.ExecuteNonQuery();
                                        cmd.Parameters.Clear();
                                        if (row == 1)
                                            tmpdt[i].AcceptChanges();
                                    }
                                    break;

                                
                                case DataRowState.Added:
                                    {
                                        cmd.CommandText = "insRecInClientInfo";

                                        cmd.Parameters.Add(new SqlParameter("@LoginCl", tmpdt[i].LoginCl));
                                        cmd.Parameters.Add(new SqlParameter("@Password", tmpdt[i].Password));

                                        //if (tmpdt[i].IsIsActiveStateNull()) tmpdt[i].IsActiveState = true;
                                        cmd.Parameters.Add(new SqlParameter("@IsActiveState", tmpdt[i].IsActiveState));
                                        //if (tmpdt[i].IsInNetworkNull()) tmpdt[i].InNetwork = false;
                                        cmd.Parameters.Add(new SqlParameter("@InNetwork", tmpdt[i].InNetwork));
                                        //if (tmpdt[i].IsID_PacketNull()) tmpdt[i].ID_Packet = 0;
                                        cmd.Parameters.Add(new SqlParameter("@ID_Packet", tmpdt[i].ID_Packet));
                                        //if (tmpdt[i].IsDTimeDeliveredPackNull()) tmpdt[i].DTimeDeliveredPack = DateTime.Now;
                                        cmd.Parameters.Add(new SqlParameter("@DTimeDeliveredPack", tmpdt[i].DTimeDeliveredPack));
                                        //if (tmpdt[i].IsID_LastPackInSrvDBNull()) tmpdt[i].ID_LastPackInSrvDB = 0;
                                        cmd.Parameters.Add(new SqlParameter("@ID_LastPackInSrvDB", tmpdt[i].ID_LastPackInSrvDB));
                                        //if (tmpdt[i].IsID_DealerNull()) tmpdt[i].ID_Dealer = 0;
                                        cmd.Parameters.Add(new SqlParameter("@ID_Dealer", tmpdt[i].ID_Dealer));
                                        //if (tmpdt[i].IsRetention_PeriodNull()) tmpdt[i].Retention_Period = DateTime.Now;
                                        cmd.Parameters.Add(new SqlParameter("@Retention_Period", tmpdt[i].Retention_Period));

                                        SqlParameter param = new SqlParameter("@ID_Client", id_client);
                                        param.Direction = ParameterDirection.Output;
                                        cmd.Parameters.Add(param);

                                        //���������� id ������� � �� �������.
                                        cmd.ExecuteScalar();
                                        if (param.Value != DBNull.Value)
                                        {
                                            id_client = (int)(param.Value);
                                            tmpdt[i].AcceptChanges();
                                        }
                                        cmd.Parameters.Clear();
                                    }
                                    break;
                                     * */

                                            #endregion
                                    }
                                }
                                rez = true;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
                            ERROR_TXT_FORMAT, e.Message, e.StackTrace));
                    }
                }
            }
            return rez;
        }

        /// <summary>
        /// ��������� ���������� ��������� � ������� ClientInfo
        /// </summary>
        /// <returns>bool</returns>
        public static bool SaveClientInfoChanges()
        {
            bool rez = false;
            //int id_client = 0;
            lock (locker)
            {
                LocalCache.ClientInfoDataTable tmpdt = cache_ds.ClientInfo;

                if (tmpdt != null)
                {
                    try
                    {
                        using (SqlConnection cn = new SqlConnection(ConnectionString))
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Connection = cn;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.CommandTimeout = CommandTimeout;
                                cn.Open();

                                for (int i = 0; i < tmpdt.Rows.Count; i++)
                                {
                                    switch (tmpdt[i].RowState)
                                    {
                                        case DataRowState.Modified:
                                            cmd.CommandText = "updateCurDataInClientInfo";
                                            cmd.Parameters.Add(new SqlParameter("@ID_Client", tmpdt[i].ID_Client));
                                            cmd.Parameters.Add(new SqlParameter("@InNetwork", tmpdt[i].InNetwork));
                                            cmd.Parameters.Add(new SqlParameter("@ID_Packet", tmpdt[i].ID_Packet));
                                            cmd.Parameters.Add(new SqlParameter("@DTimeDeliveredPack",
                                                tmpdt[i].DTimeDeliveredPack));
                                            cmd.Parameters.Add(new SqlParameter("@ID_LastPackInSrvDB",
                                                tmpdt[i].ID_LastPackInSrvDB));

                                            int row = cmd.ExecuteNonQuery();
                                            cmd.Parameters.Clear();
                                            if (row == 1)
                                                tmpdt[i].AcceptChanges();
                                            break;

                                            #region ���������������

                                            /*
                                case DataRowState.Deleted:
                                    {
                                        cmd.CommandText = "delRecInClientInfo";

                                        cmd.Parameters.Add(new SqlParameter("@ID_Client", tmpdt[i].ID_Client));
                                        int row = cmd.ExecuteNonQuery();
                                        cmd.Parameters.Clear();
                                        if (row == 1)
                                            tmpdt[i].AcceptChanges();
                                    }
                                    break;

                                
                                case DataRowState.Added:
                                    {
                                        cmd.CommandText = "insRecInClientInfo";

                                        cmd.Parameters.Add(new SqlParameter("@LoginCl", tmpdt[i].LoginCl));
                                        cmd.Parameters.Add(new SqlParameter("@Password", tmpdt[i].Password));

                                        //if (tmpdt[i].IsIsActiveStateNull()) tmpdt[i].IsActiveState = true;
                                        cmd.Parameters.Add(new SqlParameter("@IsActiveState", tmpdt[i].IsActiveState));
                                        //if (tmpdt[i].IsInNetworkNull()) tmpdt[i].InNetwork = false;
                                        cmd.Parameters.Add(new SqlParameter("@InNetwork", tmpdt[i].InNetwork));
                                        //if (tmpdt[i].IsID_PacketNull()) tmpdt[i].ID_Packet = 0;
                                        cmd.Parameters.Add(new SqlParameter("@ID_Packet", tmpdt[i].ID_Packet));
                                        //if (tmpdt[i].IsDTimeDeliveredPackNull()) tmpdt[i].DTimeDeliveredPack = DateTime.Now;
                                        cmd.Parameters.Add(new SqlParameter("@DTimeDeliveredPack", tmpdt[i].DTimeDeliveredPack));
                                        //if (tmpdt[i].IsID_LastPackInSrvDBNull()) tmpdt[i].ID_LastPackInSrvDB = 0;
                                        cmd.Parameters.Add(new SqlParameter("@ID_LastPackInSrvDB", tmpdt[i].ID_LastPackInSrvDB));
                                        //if (tmpdt[i].IsID_DealerNull()) tmpdt[i].ID_Dealer = 0;
                                        cmd.Parameters.Add(new SqlParameter("@ID_Dealer", tmpdt[i].ID_Dealer));
                                        //if (tmpdt[i].IsRetention_PeriodNull()) tmpdt[i].Retention_Period = DateTime.Now;
                                        cmd.Parameters.Add(new SqlParameter("@Retention_Period", tmpdt[i].Retention_Period));

                                        SqlParameter param = new SqlParameter("@ID_Client", id_client);
                                        param.Direction = ParameterDirection.Output;
                                        cmd.Parameters.Add(param);

                                        //���������� id ������� � �� �������.
                                        cmd.ExecuteScalar();
                                        if (param.Value != DBNull.Value)
                                        {
                                            id_client = (int)(param.Value);
                                            tmpdt[i].AcceptChanges();
                                        }
                                        cmd.Parameters.Clear();
                                    }
                                    break;
                                     * */

                                            #endregion
                                    }
                                }
                                rez = true;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
                            ERROR_TXT_FORMAT, e.Message, e.StackTrace));
                    }
                }
            }
            return rez;
        }

        /// <summary>
        /// ��������� ��������� � ������� Command (UPDATE, DELETE)
        /// </summary>
        /// <returns>bool</returns>
        public static bool SaveCommandTableChanges()
        {
            bool rez = false;
            lock (locker)
            {
                LocalCache.CommandDataTable tmpdt = cache_ds.Command;

                try
                {
                    using (SqlConnection cn = new SqlConnection(ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = CommandTimeout;
                            cn.Open();

                            int rowCount;

                            for (int i = 0; i < tmpdt.Rows.Count; i++)
                            {
                                rowCount = 0;
                                switch (tmpdt[i].RowState)
                                {
                                    case DataRowState.Modified:
                                        cmd.CommandText = "updateCommandTabl";
                                        cmd.Parameters.Add(new SqlParameter("@ExecuteState", tmpdt[i].ExecuteState));
                                        if (tmpdt[i].IsAnswCodeNull())
                                            tmpdt[i].AnswCode = 0;
                                        cmd.Parameters.Add(new SqlParameter("@AnswCode", tmpdt[i].AnswCode));
                                        if (tmpdt[i].IsAnswDataNull())
                                            tmpdt[i].AnswData = new byte[0];
                                        cmd.Parameters.Add(new SqlParameter("@AnswData", tmpdt[i].AnswData));
                                        cmd.Parameters.Add(new SqlParameter("@Original_ID_Command", tmpdt[i].ID_Command));
                                        rowCount = cmd.ExecuteNonQuery();
                                        cmd.Parameters.Clear();
                                        if (rowCount == 1)
                                            tmpdt[i].AcceptChanges();
                                        break;

                                    case DataRowState.Deleted:
                                        cmd.CommandText = "delExCmdFromCommandTabl";
                                        cmd.Parameters.Add(new SqlParameter("@Original_ID_Command", tmpdt[i].ID_Command));
                                        rowCount = cmd.ExecuteNonQuery();
                                        cmd.Parameters.Clear();
                                        if (rowCount == 1)
                                            tmpdt[i].AcceptChanges();
                                        break;
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
                        ERROR_TXT_FORMAT, e.Message, e.StackTrace));
                }
            }
            return rez;
        }

        /// </summary>
        /// ��������� ��������� � ������� StatisticDataTT
        /// </summary>
        /// <returns>bool</returns>
        public static bool SaveStatisticDataTTChanges()
        {
            bool rez = false;

            lock (locker)
            {
                LocalCache.StatisticDataTTDataTable tmpdt = cache_ds.StatisticDataTT;

                if (tmpdt != null)
                {
                    try
                    {
                        using (SqlConnection cn = new SqlConnection(ConnectionString))
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Connection = cn;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.CommandTimeout = CommandTimeout;
                                cn.Open();

                                int rowCount;

                                for (int i = 0; i < tmpdt.Rows.Count; i++)
                                {
                                    rowCount = 0;
                                    switch (tmpdt[i].RowState)
                                    {
                                        /*
                case DataRowState.Added:
                    {

                        cmd.CommandText = "insRecInStatisticDataTT";

                        cmd.Parameters.Add(new SqlParameter("@id_teletrack", tmpdt[i].ID_Teletrack));
                                            
                        if (tmpdt[i].IsCapacityInNull()) tmpdt[i].CapacityIn = 0;
                        cmd.Parameters.Add(new SqlParameter("@capasityIn", tmpdt[i].CapacityIn));

                        if (tmpdt[i].IsCapacityOutNull()) tmpdt[i].CapacityOut = 0;
                        cmd.Parameters.Add(new SqlParameter("@capasityOut", tmpdt[i].CapacityOut));

                        if (tmpdt[i].IsTimeConNull()) tmpdt[i].TimeCon = DateTime.Now;
                        cmd.Parameters.Add(new SqlParameter("@timeConn", tmpdt[i].TimeCon));

                        //if (tmpdt[i].IsTimeDisconNull()) tmpdt[i].TimeDiscon = null;
                        //cmd.Parameters.Add(new SqlParameter("@timeDisconn", tmpdt[i].TimeDiscon));

                        SqlParameter param = new SqlParameter("@id_session", id_session);
                        param.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(param);

                        //���������� id ������� � �� �������.
                        cmd.ExecuteScalar();
                        if (param.Value != DBNull.Value)
                        {
                            id_session = (int)(param.Value);
                            if (id_session != tmpdt[i].ID_Session)
                                ExecuteLogging.Log("Error_", "�� ������� ID_Session � ���� � � ��");

                            ExecuteLogging.Log("DACL_", "�������� ������TT: id: " + id_session.ToString());
                        }
                        tmpdt[i].AcceptChanges();

                        cmd.Parameters.Clear();
                    }
                    break;
                     * */
                                        case DataRowState.Modified:
                                            cmd.CommandText = "updateStatisticDataTT";
                                            cmd.Parameters.Add(new SqlParameter("@id_session", tmpdt[i].ID_Session));
                                            cmd.Parameters.Add(new SqlParameter("@capasityIn", tmpdt[i].CapacityIn));
                                            cmd.Parameters.Add(new SqlParameter("@capasityOut", tmpdt[i].CapacityOut));
                                            cmd.Parameters.Add(new SqlParameter("@timeDisconn", tmpdt[i].TimeDiscon));

                                            rowCount = cmd.ExecuteNonQuery();
                                            cmd.Parameters.Clear();
                                            if (rowCount != 1)
                                            {
                                                ExecuteLogging.Log(ERROR_FILE_PREFIX,
                                                    "�� ������ DataRowState.Modified � SaveStatisticDataTTChanges");
                                            }
                                            tmpdt[i].AcceptChanges();
                                            break;

                                        case DataRowState.Deleted:
                                            cmd.CommandText = "endOfSessionTT";
                                            cmd.Parameters.Add(new SqlParameter("@id_session",
                                                tmpdt[i]["ID_Session", DataRowVersion.Original]));
                                            cmd.Parameters.Add(new SqlParameter("@capasityIn",
                                                tmpdt[i]["CapacityIn", DataRowVersion.Original]));
                                            cmd.Parameters.Add(new SqlParameter("@capasityOut",
                                                tmpdt[i]["CapacityOut", DataRowVersion.Original]));
                                            cmd.Parameters.Add(new SqlParameter("@timeDisconnect",
                                                tmpdt[i]["TimeDiscon", DataRowVersion.Original]));

                                            rowCount = cmd.ExecuteNonQuery();
                                            cmd.Parameters.Clear();
                                            if (rowCount != 1)
                                            {
                                                ExecuteLogging.Log(ERROR_FILE_PREFIX,
                                                    "�� ������ DataRowState.Deleted � SaveStatisticDataTTChanges " +
                                                    tmpdt[i]["ID_Session", DataRowVersion.Original].ToString());
                                            }
                                            tmpdt[i].AcceptChanges();
                                            break;
                                    }
                                }
                                rez = true;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
                            ERROR_TXT_FORMAT, e.Message, e.StackTrace));
                    }
                }
            }
            return rez;
        }

        /// <summary>
        /// ��������� ��������� � ������� StatisticDataCl
        /// </summary>
        /// <returns></returns>
        public static bool SaveStatisticDataClChanges()
        {
            bool rez = false;
            int id_session = 0;

            lock (locker)
            {
                LocalCache.StatisticDataClDataTable tmpdt = cache_ds.StatisticDataCl;

                if (tmpdt != null)
                {
                    try
                    {
                        using (SqlConnection cn = new SqlConnection(ConnectionString))
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Connection = cn;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.CommandTimeout = CommandTimeout;
                                cn.Open();

                                int rowCount;

                                for (int i = 0; i < tmpdt.Rows.Count; i++)
                                {
                                    rowCount = 0;
                                    switch (tmpdt[i].RowState)
                                    {
                                        case DataRowState.Added:
                                            cmd.CommandText = "insRecInStatisticDataCl";
                                            cmd.Parameters.Add(new SqlParameter("@id_client", tmpdt[i].ID_Client));

                                            if (tmpdt[i].IsCapacityInNull())
                                                tmpdt[i].CapacityIn = 0;
                                            cmd.Parameters.Add(new SqlParameter("@capasityIn", tmpdt[i].CapacityIn));

                                            if (tmpdt[i].IsCapacityOutNull())
                                                tmpdt[i].CapacityOut = 0;
                                            cmd.Parameters.Add(new SqlParameter("@capasityOut", tmpdt[i].CapacityOut));

                                            if (tmpdt[i].IsTimeConNull())
                                                tmpdt[i].TimeCon = DateTime.Now;
                                            cmd.Parameters.Add(new SqlParameter("@timeConn", tmpdt[i].TimeCon));

                                            if (tmpdt[i].IsTimeDisconNull())
                                                tmpdt[i].TimeDiscon = DateTime.Now;
                                            cmd.Parameters.Add(new SqlParameter("@timeDisconn", tmpdt[i].TimeDiscon));

                                            SqlParameter param = new SqlParameter("@id_session", DbType.Int32);
                                            param.Direction = ParameterDirection.Output;
                                            cmd.Parameters.Add(param);

                                            //���������� id ������� � �� �������.
                                            cmd.ExecuteScalar();
                                            if (param.Value != DBNull.Value)
                                            {
                                                id_session = Convert.ToInt32(param.Value);
                                                if (id_session != tmpdt[i].ID_SessionCl)
                                                {
                                                    ExecuteLogging.Log(ERROR_FILE_PREFIX,
                                                        "�� ������� ID_SessionCl � ���� � � ��");
                                                }
                                            }
                                            tmpdt[i].AcceptChanges();
                                            cmd.Parameters.Clear();
                                            break;

                                        case DataRowState.Modified:
                                            cmd.CommandText = "updateStatisticDataCl";
                                            cmd.Parameters.Add(new SqlParameter("@id_session", tmpdt[i].ID_SessionCl));
                                            cmd.Parameters.Add(new SqlParameter("@capasityIn", tmpdt[i].CapacityIn));
                                            cmd.Parameters.Add(new SqlParameter("@capasityOut", tmpdt[i].CapacityOut));
                                            cmd.Parameters.Add(new SqlParameter("@timeDisconn", tmpdt[i].TimeDiscon));

                                            rowCount = cmd.ExecuteNonQuery();
                                            cmd.Parameters.Clear();
                                            if (rowCount != 1)
                                            {
                                                ExecuteLogging.Log(ERROR_FILE_PREFIX,
                                                    "�� ������ DataRowState.Modified � SaveStatisticDataClChanges");
                                            }
                                            tmpdt[i].AcceptChanges();
                                            break;

                                        case DataRowState.Deleted:
                                            cmd.CommandText = "endOfSessionCl";
                                            cmd.Parameters.Add(new SqlParameter("@id_session",
                                                tmpdt[i]["ID_SessionCl", DataRowVersion.Original]));
                                            cmd.Parameters.Add(new SqlParameter("@capasityIn",
                                                tmpdt[i]["CapacityIn", DataRowVersion.Original]));
                                            cmd.Parameters.Add(new SqlParameter("@capasityOut",
                                                tmpdt[i]["CapacityOut", DataRowVersion.Original]));
                                            cmd.Parameters.Add(new SqlParameter("@timeDisconnect",
                                                tmpdt[i]["TimeDiscon", DataRowVersion.Original]));

                                            rowCount = cmd.ExecuteNonQuery();
                                            cmd.Parameters.Clear();
                                            if (rowCount != 1)
                                            {
                                                ExecuteLogging.Log(ERROR_FILE_PREFIX,
                                                    "�� ������ DataRowState.Deleted � SaveStatisticDataClChanges");
                                            }

                                            tmpdt[i].AcceptChanges();
                                            break;
                                    }
                                }
                                rez = true;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
                            ERROR_TXT_FORMAT, e.Message, e.StackTrace));
                    }
                }
            }
            return rez;
        }

        /// <summary>
        /// ���������� ������ ������ � ������� ��������� ���������
        /// </summary>
        static void IsTableChange()
        {
            try
            {
                ExecuteLogging.Log("BPLevelTT", "IsTableChange. ���������� ������ ������ � ������� ��������� ���������");

                using (SqlConnection cn = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("selectVersionControl", cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlDependency depend = new SqlDependency(cmd);
                        // ����������� ���������� ������� �� ���������� �� ���������� � 
                        // ����������� �������, ������������ ����� SqlCommand
                        depend.OnChange += new OnChangeEventHandler(OnDataChange);

                        cn.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                byte id = (byte) reader[0];
                                string str = (string) reader[1];
                                int ver = (int) reader[2];
                                LocalCache.VersionControlRow rw = null;
                                lock (locker)
                                {
                                    rw = (LocalCache.VersionControlRow) cache_ds.VersionControl.FindByID(id);
                                }

                                if ((rw != null) && (rw.Version != ver))
                                {
                                    rw.Version = ver;
                                    LoadTableAfterChange(str);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
                    ERROR_TXT_FORMAT, e.Message, e.StackTrace));
            }
        }

        /// <summary>
        /// ��������� ������ �� ����������� �� ������� �������
        /// </summary>
        /// <param name="tblName"></param>
        static void LoadTableAfterChange(string tblName)
        {
           
            ExecuteLogging.Log("BPLevelTT", String.Format("LoadTableAfterChange. ��������� ��������� �  ������� {0}", tblName));

            lock (locker) //��������� ��������
            {
                switch (tblName)
                {
                    case "UserInfo":
                        cache_ds.UserInfo.Clear();
                        FillUserInfo();
                        break;

                    case "ClientInfo":
                        cache_ds.ClientInfo.Clear();
                        FillClientInfo();
                        break;

                    case "TeletrackInfo":
                        cache_ds.TeletrackInfo.Clear();
                        FillTeletrackInfo();
                        break;

                    case "CommunicTable":
                        cache_ds.CommunicTable.Clear();
                        FillCommunicTable();
                        break;

                    case "Command":
                        cache_ds.Command.Clear();
                        FillCommand();
                        break;
                }
            }
        }

        /// <summary>
        /// ���������� ������� ��������� ������ �� ������� � ����������� ������.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void OnDataChange(object sender, SqlNotificationEventArgs e)
        {
            ExecuteLogging.Log("BPLevelTT", "� �� ��������� ���������");

            // ���� �� ��������� ������, �� ���������� ���� ���������������� ������
            // � �������� ����� ����� ������.
            if (e.Info != SqlNotificationInfo.Invalid)
            {
                IsTableChange();
            }
            else
            {
                throw new Exception("SqlNotificationEvent: SqlNotificationInfo.Invalid");
            }
        }

        /// <summary>
        /// �������� �� MS SQL Server � ��. �������� �������������� �����
        /// ��������� ����������.
        /// </summary>
        /// <exception cref="ApplicationException">�� ������ ������ ����������� � ���� ������
        /// ��� C����� ����������� � ���� ������ ������������ �����������</exception>
        /// <returns>True - ���� ������ ��������</returns>
        public static bool DatabaseAvailable()
        {
            ExecuteLogging.Log("BPLevelTT",
                "�������� �� MS SQL Server � ��. �������� �������������� ����� ��������� ����������");

            if (String.IsNullOrEmpty(ConnectionString))
            {
                throw new ApplicationException("�� ������ ������ ����������� � ���� ������.");
            }

            try
            {
                new SqlConnectionStringBuilder(ConnectionString);
            }
            catch
            {
                throw new ApplicationException("C����� ����������� � ���� ������ ������������ �����������.");
            }

            try
            {
                using (SqlConnection c = new SqlConnection(ConnectionString))
                {
                    c.Open();
                }
            }
            catch
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "���� ����������� � ���� ������: �� �������� (c����� �����������:{0})", ConnectionString));
                return false;
            }

            ExecuteLogging.Log("BPLevelTT", "DatabaseAvailable. ���� ����������� � ���� ������: �����.");
            return true;
        }
    }
}

