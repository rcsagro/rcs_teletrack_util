using System;
using System.Data;
using System.Data.SqlClient;
using RCS.OnlineServerLib.LocalCacheSrv;
using RW_Log;

namespace RCS.OnlineServerLib.DACL
{
  public class StatisticDACL : MainControlDALC
  {
    /// <summary>
    /// Error_
    /// </summary>
    private const string ERROR_FILE_PREFIX = "Error_";
    /// <summary>
    /// {0} \r\n {1}
    /// </summary>
    private const string ERROR_TXT_FORMAT = "{0} \r\n {1}";

    #region ������ ����� ���������� �� ������ ����� ���������

    /// <summary>
    /// ��������� ����� ������ � ������� ����� ���������� �� ����������
    /// </summary>
    /// <param name="id_teletrack"></param>
    /// <returns></returns>
    public static int AddSessionTT(int id_teletrack)
    {
      int id_session = 0;
      int capasityIn = 0;
      int capasityOut = 0;
      DateTime dt = DateTime.Now;

      try
      {
        lock (locker)
        {
          using (SqlConnection cn = new SqlConnection(ConnectionString))
          {
            using (SqlCommand cmd = new SqlCommand())
            {
              cmd.Connection = cn;
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.CommandTimeout = CommandTimeout;
              cn.Open();

              cmd.CommandText = "insRecInStatisticDataTT";
              cmd.Parameters.Add(new SqlParameter("@id_teletrack", id_teletrack));
              cmd.Parameters.Add(new SqlParameter("@capasityIn", capasityIn));
              cmd.Parameters.Add(new SqlParameter("@capasityOut", capasityOut));
              cmd.Parameters.Add(new SqlParameter("@timeConn", dt));
              cmd.Parameters.Add(new SqlParameter("@timeDisconn", dt));

              SqlParameter param = new SqlParameter("@id_session", DbType.Int32);
              param.Direction = ParameterDirection.Output;
              cmd.Parameters.Add(param);

              //���������� id ������ � �� �������.
              cmd.ExecuteScalar();
              if (param.Value != DBNull.Value)
              {
                id_session = Convert.ToInt32(param.Value);
              }
              else
              {
                throw new Exception("��������� ������� �������� ������ � �� �������");
              }

              LocalCache.StatisticDataTTRow new_rw = cache_ds.StatisticDataTT.NewStatisticDataTTRow();
              new_rw.ID_Session = id_session;
              new_rw.ID_Teletrack = id_teletrack;
              new_rw.CapacityIn = 0;
              new_rw.CapacityOut = 0;
              new_rw.TimeCon = dt;
              new_rw.TimeDiscon = dt;
              cache_ds.StatisticDataTT.AddStatisticDataTTRow(new_rw);
              new_rw.AcceptChanges();

              cmd.Parameters.Clear();
            }
          }
        }
      }
      catch (Exception e)
      {
        ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
          ERROR_TXT_FORMAT, e.Message, e.StackTrace));
      }
      return id_session;
    }

    /// <summary>
    /// �� ���������� ������� ������ �� ���������� ����
    /// </summary>
    /// <param name="id_session"></param>
    public static void DeleteSessiontTT(int id_session)
    {
      lock (locker)
      {
        LocalCache.StatisticDataTTRow rw =
          cache_ds.StatisticDataTT.FindByID_Session(id_session);

        if (rw != null)
        {
          rw.TimeDiscon = DateTime.Now;
          rw.AcceptChanges();
          rw.Delete();
        }
      }
    }

    /// <summary>
    /// �������� ������ ���������
    /// </summary>
    /// <param name="id_session"></param>
    /// <param name="byteCount"></param>
    public static void CapacityInTT(int id_session, int byteCount)
    {
        ExecuteLogging.Log("BPLevelTT", "CapacityInTT. �������� ������ ���������");
      lock (locker)
      {
        LocalCache.StatisticDataTTRow rw =
          cache_ds.StatisticDataTT.FindByID_Session(id_session);

        if (rw != null)
        {
          rw.BeginEdit();
          if (rw.IsCapacityInNull())
          {
            rw.CapacityIn = 0;
          }
          rw.CapacityIn += byteCount;
          rw.TimeDiscon = DateTime.Now;
          rw.EndEdit();
        }
      }
    }

    /// <summary>
    /// ��������� ������ ���������
    /// </summary>
    /// <param name="id_session"></param>
    /// <param name="byteCount"></param>
    public static void CapacityOutTT(int id_session, int byteCount)
    {
      lock (locker)
      {
        LocalCache.StatisticDataTTRow rw =
          cache_ds.StatisticDataTT.FindByID_Session(id_session);

        if (rw != null)
        {
          rw.BeginEdit();
          if (rw.IsCapacityOutNull())
          {
            rw.CapacityOut = 0;
          }
          rw.CapacityOut += byteCount;
          rw.TimeDiscon = DateTime.Now;
          rw.EndEdit();
        }
      }
    }

    /// <summary>
    /// ����� ������ ���������� � ����������
    /// </summary>
    /// <param name="id_session"></param>
    /// <param name="dt"></param>
    public static void TimeConnTT(int id_session, DateTime dt)
    {
      lock (locker)
      {
        LocalCache.StatisticDataTTRow rw =
          cache_ds.StatisticDataTT.FindByID_Session(id_session);

        if (rw != null)
        {
          rw.BeginEdit();
          rw.TimeCon = dt;
          rw.EndEdit();
        }
      }
    }

    /// <summary>
    /// ����� ������� ����������
    /// </summary>
    /// <param name="id_session"></param>
    /// <param name="dt"></param>
    public static void TimeDisconnTT(int id_session, DateTime dt)
    {
      lock (locker)
      {
        LocalCache.StatisticDataTTRow rw =
          cache_ds.StatisticDataTT.FindByID_Session(id_session);

        if (rw != null)
        {
          rw.BeginEdit();
          rw.TimeDiscon = dt;
          rw.EndEdit();
        }
      }
    }

    #endregion

    #region ������ ����� ���������� �� ������ ����� �������

    /// <summary>
    /// ��������� ������ � StatisticDataCl ���� ��� ������ ����������
    /// </summary>
    /// <param name="id_client"></param>
    public static LocalCache.StatisticDataClRow AddNewRowToStatisticCl(int id_client)
    {
      // ��������� ����� ������ � ���-������� StatisticDataTT
      LocalCache.StatisticDataClRow new_rw = null;
      lock (locker)
      {
        new_rw = cache_ds.StatisticDataCl.NewStatisticDataClRow();
        new_rw.ID_Client = id_client;
        new_rw.TimeCon = DateTime.Now;
        cache_ds.StatisticDataCl.AddStatisticDataClRow(new_rw);
      }
      return new_rw;
    }

    /// <summary>
    /// ��������� ������ � StatisticDataCl �� ��� ������ ����������
    /// </summary>
    /// <param name="id_client"></param>
    /// <returns>int</returns>
    public static int AddSessionCl(int id_client)
    {
      /*����� ����������. � ���� id_session= -1. ����� ������� � �� � ���������� ��������� ������  */
      //LocalCache.StatisticDataClRow tmpdt = AddNewRowToStatisticCl(id_client);

      int id_session = 0;
      int capasityIn = 0;
      int capasityOut = 0;
      DateTime dt = DateTime.Now;

      try
      {
        lock (locker)
        {
          using (SqlConnection cn = new SqlConnection(ConnectionString))
          {
            using (SqlCommand cmd = new SqlCommand())
            {
              cmd.Connection = cn;
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.CommandTimeout = CommandTimeout;
              cn.Open();

              cmd.CommandText = "insRecInStatisticDataCl";
              cmd.Parameters.Add(new SqlParameter("@id_client", id_client));
              cmd.Parameters.Add(new SqlParameter("@capasityIn", capasityIn));
              cmd.Parameters.Add(new SqlParameter("@capasityOut", capasityOut));
              cmd.Parameters.Add(new SqlParameter("@timeConn", dt));
              cmd.Parameters.Add(new SqlParameter("@timeDisconn", dt));

              SqlParameter param = new SqlParameter("@id_session", DbType.Int32);
              param.Direction = ParameterDirection.Output;
              cmd.Parameters.Add(param);

              //���������� id ������ ������� � �� �������.
              cmd.ExecuteScalar();
              if (param.Value != DBNull.Value)
              {
                id_session = Convert.ToInt32(param.Value);
              }
              else
              {
                throw new Exception(
                  "��������� ������� �������� ������ � ������� StatisticDataCl �� �������");
              }

              LocalCache.StatisticDataClRow new_rw =
                cache_ds.StatisticDataCl.NewStatisticDataClRow();
              new_rw.ID_SessionCl = id_session;
              new_rw.ID_Client = id_client;
              new_rw.CapacityIn = capasityIn;
              new_rw.CapacityOut = capasityOut;
              new_rw.TimeCon = dt;
              new_rw.TimeDiscon = dt;
              cache_ds.StatisticDataCl.AddStatisticDataClRow(new_rw);
              new_rw.AcceptChanges();

              cmd.Parameters.Clear();
            }
          }
        }
      }
      catch (Exception e)
      {
        ExecuteLogging.Log(ERROR_FILE_PREFIX, String.Format(
          ERROR_TXT_FORMAT, e.Message, e.StackTrace));
      }
      return id_session;
    }

    /// <summary>
    /// �� ���������� ������� ������ �� ���������� ����
    /// </summary>
    /// <param name="id_session"></param>
    public static void DeleteSessiontCient(int id_session)
    {
      lock (locker)
      {
        LocalCache.StatisticDataClRow rw =
          cache_ds.StatisticDataCl.FindByID_SessionCl(id_session);

        if (rw != null)
        {
          rw.TimeDiscon = DateTime.Now;
          rw.AcceptChanges();
          rw.Delete();
        }
      }
    }

    /// <summary>
    /// �������� ������ ���������
    /// </summary>
    /// <param name="id_session"></param>
    /// <param name="byteCount"></param>
    public static void CapacityInCl(int id_session, int byteCount)
    {
      lock (locker)
      {
        LocalCache.StatisticDataClRow rw =
          cache_ds.StatisticDataCl.FindByID_SessionCl(id_session);

        if (rw != null)
        {
          rw.BeginEdit();
          if (rw.IsCapacityInNull())
            rw.CapacityIn = 0;
          rw.CapacityIn += byteCount;
          rw.TimeDiscon = DateTime.Now;
          rw.EndEdit();
        }
      }
    }

    /// <summary>
    /// ��������� ������ ���������
    /// </summary>
    /// <param name="id_session"></param>
    /// <param name="byteCount"></param>
    public static void CapacityOutCl(int id_session, int byteCount)
    {
      lock (locker)
      {
        LocalCache.StatisticDataClRow rw =
          cache_ds.StatisticDataCl.FindByID_SessionCl(id_session);

        if (rw != null)
        {
          rw.BeginEdit();
          if (rw.IsCapacityOutNull())
          {
            rw.CapacityOut = 0;
          }
          rw.CapacityOut += byteCount;
          rw.TimeDiscon = DateTime.Now;
          rw.EndEdit();
        }
      }
    }

    /// <summary>
    /// ����� ������ ���������� � ����������
    /// </summary>
    /// <param name="id_session"></param>
    /// <param name="dt"></param>
    public static void TimeConnCl(int id_session, DateTime dt)
    {
      lock (locker)
      {
        LocalCache.StatisticDataClRow rw =
          cache_ds.StatisticDataCl.FindByID_SessionCl(id_session);

        if (rw != null)
        {
          rw.BeginEdit();
          rw.TimeCon = dt;
          rw.EndEdit();
        }
      }
    }

    /// <summary>
    /// ����� ������� ����������
    /// </summary>
    /// <param name="id_session"></param>
    /// <param name="dt"></param>
    public static void TimeDisconnCl(int id_session, DateTime dt)
    {
      lock (locker)
      {
        LocalCache.StatisticDataClRow rw =
          cache_ds.StatisticDataCl.FindByID_SessionCl(id_session);

        if (rw != null)
        {
          rw.BeginEdit();
          rw.TimeDiscon = dt;
          rw.EndEdit();
        }
      }
    }

    #endregion
  }
}
