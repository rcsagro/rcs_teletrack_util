using System;
using System.Collections.Generic;
using RCS.OnlineServerLib.LocalCacheSrv;
using RW_Log;

namespace RCS.OnlineServerLib.DACL
{
  public class TeletreckDALC : MainControlDALC
  {
    /// <summary>
    /// �������� �������������� ���������
    /// </summary>
    /// <param name="log"></param>
    /// <param name="pass"></param>
    /// <param name="id_teletrack"></param>
    /// <returns></returns>
    public static bool AuthenticationTeletr(string log, string pass, out int id_teletrack)
    {
      id_teletrack = 0;
      ExecuteLogging.Log("BPLevelTT", "AuthenticationTeletr. �������� �������������� ���������: " + log);
      lock (locker)
      {
        LocalCache.TeletrackInfoRow[] tt_dr =
          (LocalCache.TeletrackInfoRow[])cache_ds.TeletrackInfo.Select(
            string.Format(@"LoginTT = '{0}' AND PasswordTT = '{1}'", log, pass));

        if (tt_dr.Length != 0)
        {
          id_teletrack = tt_dr[0].ID_Teletrack;
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// �������� �������������� ���������
    /// </summary>
    /// <param name="imei"></param>
    /// <param name="login"></param>
    /// <param name="id_teletrack"></param>
    /// <returns></returns>
    public static bool AuthenticationTeletr(string imei, out string login, out int id_teletrack)
    {
      id_teletrack = 0;
      login = "";
      lock (locker)
      {
        LocalCache.TeletrackInfoRow[] tt_dr =
          (LocalCache.TeletrackInfoRow[])cache_ds.TeletrackInfo.Select(
            string.Format(@"GSMIMEI = '{0}'", imei));

        if (tt_dr.Length != 0)
        {
          id_teletrack = tt_dr[0].ID_Teletrack;
          login = tt_dr[0].LoginTT;
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// ������������� ����� ��������� ���������� ������ �� ������� ���������
    /// </summary>
    /// <param name="id_teletrack"></param>
    /// <param name="dt"></param>
    public static void DTimeLastPackFromTT(int id_teletrack, DateTime dt)
    {
      lock (locker)
      {
        LocalCache.TeletrackInfoRow tt_rw =
          cache_ds.TeletrackInfo.FindByID_Teletrack(id_teletrack);

        if (tt_rw != null)
        {
          tt_rw.BeginEdit();
          tt_rw.DTimeLastPack = dt;
          tt_rw.EndEdit();
        }
      }
    }

    /// <summary>
    /// ���������� ������, id ������ ��� ������� ���������
    /// </summary>
    /// <param name="id_teletrack"></param>
    /// <returns>List</returns>
    public static List<int> GetLstOfCmdForTT(int id_teletrack)
    {
      lock (locker)
      {
        LocalCache.CommandRow[] cmd_rw =
          (LocalCache.CommandRow[])cache_ds.Command.Select(string.Format(
            @"ID_Teletrack = '{0}' AND ExecuteState = '{1}' AND DeltaTimeExCom > '{2}'",
            id_teletrack, (byte)A1CmdHandlingStatus.Pending, DateTime.Now));

        List<int> idCmdLst = new List<int>();
        for (int i = 0; i < cmd_rw.Length; i++)
        {
          idCmdLst.Add(cmd_rw[i].ID_Command);
        }

        return idCmdLst;
      }
    }

    /// <summary>
    /// ���������� (������������� ������� ��� ��������) ������� Command
    /// </summary>
    /// <param name="id_cmd"></param>
    /// <param name="cmdType"></param>
    /// <param name="id_client"></param>
    /// <returns>byte[]</returns>
    public static byte[] GetCmdForTT(int id_cmd, ref string cmdType, ref int id_client)
    {
      lock (locker)
      {
        LocalCache.CommandRow rw = cache_ds.Command.FindByID_Command(id_cmd);
        if (rw != null)
        {
          if (!rw.IsCommandTypeNull())
            cmdType = rw.CommandType;

          id_client = rw.ID_Client;
          return rw.CommandText;
        }
      }
      return null;
    }

    /// <summary>
    /// ���������� ������ ���������� ���������
    /// </summary>
    /// <param name="id_teletrack"></param>
    /// <returns>List</returns>
    public static List<int> GetLstOwnersOfTT(int id_teletrack)
    {
      lock (locker)
      {
        //������ ���������� ���������. (��������� ��� ������������� ������)
        LocalCache.CommunicTableRow[] owners =
          (LocalCache.CommunicTableRow[])cache_ds.CommunicTable.Select(
            string.Format("ID_Teletrack = {0}", id_teletrack));

        List<int> lst_owners = new List<int>();
        if (owners.Length != 0)
        {
          for (int i = 0; i < owners.Length; i++)
          {
            lst_owners.Add(owners[i].ID_Client);
          }
        }
        return lst_owners;
      }
    }
  }
}
