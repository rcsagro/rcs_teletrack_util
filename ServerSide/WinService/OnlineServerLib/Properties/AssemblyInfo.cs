﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("OnlineServerLib")]
[assembly: AssemblyDescription("Online Server Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("RCS")]
[assembly: AssemblyProduct("OnlineServer")]
[assembly: AssemblyCopyright("Copyright © 2008-2011 RCS")]
[assembly: AssemblyTrademark("Teletrack")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3fccbe71-7fac-45f6-a9a7-da2a30af5d58")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("4.0.0.0")]
[assembly: AssemblyFileVersion("4.0.0.0")]
