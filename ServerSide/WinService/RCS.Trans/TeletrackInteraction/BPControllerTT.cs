#region Using directives
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using RCS.Protocol.TT;
using RCS.Sockets.Connection;
using RW_Log;
using TransitServisLib;
#endregion

namespace RCS.Trans.TeletrackInteraction
{
    public class BPControllerTT : IDisposable
    {
        public event IncomNewPacketFromTT newPacketFromTT;

        public event CmdTransmissionDelegate newA1FromTT;

        protected static volatile object activeConnobj = new object();

        public HandlingOfIncomDataTT handler;

        /// <summary>
        /// 5000
        /// </summary>
        public const int QUEUE_LENGTH_LIMIT = 5000;

        protected SortedList<int, BO_Teletrack> lstActiveConnTT = new SortedList<int, BO_Teletrack>();

        /// <summary>
        /// ����� � ������� �������� ���������� ��������� �����
        /// </summary>
        protected int deltaLastPack = 60000; //(�����������)

        /// <summary>
        /// ������ ������� � ������� �������� ���������� ��������� �����
        /// </summary>
        /// <returns>Int</returns>
        public int DeltaIncomLastPack
        {
            get { return deltaLastPack; }
            set { deltaLastPack = value; }
        }

        //********************************************************
        /// <summary>
        /// �������������
        /// </summary>
        protected static volatile object locker = new object();

        protected Thread[] workers;
        protected Queue<ConteinerIncData> taskQueue = new Queue<ConteinerIncData>();
        //int workerCount = 0;
        //********************************************************

        protected IOnLineClient iClient = null;

        #region ������������, �����������

        public BPControllerTT()
        {
        }

        public BPControllerTT(int countOfWorkerThread, bool bForTest)
        {
            //������ ������-�����������
            //------------------------------------------------------
            workers = new Thread[countOfWorkerThread];

            // ������� � ��������� ��������� ����� �� ������� �����������
            for (int i = 0; i < countOfWorkerThread; i++)
            {
                workers[i] = new Thread(Consume);
                workers[i].IsBackground = true;
                workers[i].Start();
            }
        }

        /*
    public BPControllerTT(IOnLineClient icl, int countOfWorkerThread)
    {
        iClient = icl;
        handler = new HandlingOfIncomDataTT();
        handler.packFromTeletrack += new IncomNewPacket(IncomDataFromTT);

        handler.�onnectedState += new ConnectedStateEventHandler(DisconectOfTeletrack);
        handler.errorOfLevel += new MessageErrorEventHandler(ErrorFromTrLevel);
        //������ ������-�����������
        //------------------------------------------------------
        workers = new Thread[countOfWorkerThread];

        // ������� � ��������� ��������� ����� �� ������� �����������
        for (int i = 0; i < countOfWorkerThread; i++)
        {
            workers[i] = new Thread(Consume);
            workers[i].IsBackground = true;
            workers[i].Start();
        }
        //------------------------------------------------------ 
    }
    */

        #region IDisposable Members

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            bEndOfWork = true;
            EndOfWork();
            // ��������� null-������ ����� ������������ ���� ������������
            IncomDataFromTT(null, null);

            foreach (Thread worker in workers)
                worker.Join();
        }

        #endregion

        #endregion

        protected void ErrorFromTrLevel(object sender, MessageErrorEventArgs e)
        {
            ExecuteLogging.Log("ErrorFromTrLevel: ", "" + e.Message + " ����� " + Thread.CurrentThread.GetHashCode().ToString());
        }

        /// <summary>
        /// ���� ������ �� ����������
        /// (�� ���� ��������� ����� ���������� ����
        ///  ������� ������� �� ������� � �����)
        /// </summary>
        protected static volatile bool bOverloadTaskQueue = false;

        /// <summary>
        /// ������������� �� ��������� ������ �������
        /// ��� �������� ������ �������
        /// </summary>
        protected static volatile bool bEndOfWork = false;

        /// <summary>
        /// �����������
        /// </summary>
        protected void Consume()
        {
            ExecuteLogging.Log("BPLevelTT", "Consume. ����� �����������.");

            while (!bEndOfWork)
            {
                try
                {
                    ConteinerIncData dataContainer;
                    lock (locker)
                    {
                        while (taskQueue.Count == 0)
                        {
                            Monitor.Wait(locker);

                            if (bEndOfWork) // ������ �� �����
                                return;
                        }

                        dataContainer = taskQueue.Dequeue();
                    }

                    if (dataContainer.sender == null)
                        return; // ������ �� �����

                    if (dataContainer.packet == null)
                        ControlEndOfSession(); //��������� ���������� ����������
                    else
                        ParserIncomData(dataContainer);
                }
                catch (Exception ex)
                {
                    ExecuteLogging.Log("Error_", String.Format(
                        "Consume. ������ � ������-����������� ������� ��������� ���������� \r\n {0} \r\n {1}",
                        ex.Message, ex.StackTrace));
                }
            }
        }

        /// <summary>
        /// ������ ����� �� ���������
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="data">IPacket</param>
        public virtual void IncomDataFromTT(object sender, IPacket data)
        {
            lock (locker)
            {
                if (taskQueue.Count >= QUEUE_LENGTH_LIMIT)
                {
                    ExecuteLogging.Log("IncomDataFromTT", String.Format("Queue: ����� ������� �����: {0} ����� {1}", taskQueue.Count, Thread.CurrentThread.GetHashCode()));

                    bOverloadTaskQueue = true;
                }
                else
                {
                    bOverloadTaskQueue = false;
                }

                taskQueue.Enqueue(new ConteinerIncData(sender, data));

                Monitor.PulseAll(locker);
            }
        }

        /// <summary>
        /// �����. ������� - ������� ����� ����� 
        /// </summary>
        /// <param name="owners">List of int</param>
        /// <param name="newPk">NewPacketFromTT</param>
        protected void OnNewPacketFromTT(List<int> owners, NewPacketFromTT newPk)
        {
            ExecuteLogging.Log("BPLevelTT", "OnNewPacketFromTT. �����. ������� - ������� ����� �����");
            if (newPacketFromTT != null)
                newPacketFromTT(owners, newPk);
        }

        /// <summary>
        /// �����. ������� - ������� ����� ����� �1 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="idCmd"></param>
        protected void OnNewA1FromTT(int target, int idCmd)
            //(string sender, int target, int idCmdinClDB, byte[] answOnA1)
        {
            if (newA1FromTT != null)
                newA1FromTT(target, idCmd);
        }

        /// <summary>
        /// ������� �� ������ �������� �����������
        /// </summary>
        /// <param name="con_state">ConnectedStateEvArg</param>
        protected virtual void DisconectOfTeletrack(ConnectedStateEvArg con_state)
        {
            throw new NotImplementedException(
                "����� DisconectOfTeletrack ������ BPControllerTT ������ ���� �������� ������������.");
        }

        /// <summary>
        /// ������������ ��������� ����������
        /// ������� ���������� 
        /// </summary>
        public virtual void ControlEndOfSession()
        {
            try
            {
                ExecuteLogging.Log("BPLevelTT",
                    "ControlEndOfSession. ������������ ��������� ����������. ������� ����������.");

                if (lstActiveConnTT.Count != 0)
                {
                    #region Test block

                    /*Mobitel tmp_bo = lstActiveConn[0];
                if (tmp_bo.TimeIncomLastPack.AddMilliseconds(deltaLastPack) < DateTime.Now)
                {
                    ExecuteLogging.Log(LevelName, "TimerControl: ���� ��������� �� ��������");
                    //outOnUserInterface("TimerControl: ���� ��������� �� ��������");
                    EndOfSession(tmp_bo);
                }*/
                    //������� �������� � ������� ����� �����

                    #endregion

                    List<BO_Teletrack> lst_bo = new List<BO_Teletrack>();
                    lock (activeConnobj)
                    {
                        foreach (BO_Teletrack bo in lstActiveConnTT.Values)
                        {
                            if (bo.TimeIncomLastPack.AddMilliseconds(deltaLastPack) < DateTime.Now)
                                lst_bo.Add(bo);
                        }
                    }
                    if (lst_bo.Count != 0)
                    {
                        ExecuteLogging.Log("BPLevelTT", "ControlEndOfSession. TimerControl: ���� ��������� �� ��������");
                        for (int i = 0; i < lst_bo.Count; i++)
                            CloseConnection(lst_bo[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                ExecuteLogging.Log("BPLevelTT", String.Format(
                    "ControlEndOfSession. {0} \r\n {1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// ��������� ��� ����������, ��������� ������
        /// </summary>
        protected virtual void EndOfWork()
        {
            throw new NotImplementedException(
                "����� EndOfWork ������ BPControllerTT ������ ���� �������� ������������.");
        }

        /// <summary>
        /// ��������� ����������
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        protected virtual void CloseConnection(BO_Teletrack sender)
        {
            throw new NotImplementedException(
                "����� CloseConnection ������ BPControllerTT ������ ���� �������� ������������.");
        }

        /// <summary>
        /// Switch �������� �������
        /// </summary>
        /// <param name="obj">object</param>
        protected void ParserIncomData(object obj)
        {
            ExecuteLogging.Log("BPLevelTT", "ParserIncomData. Switch �������� �������");
            ConteinerIncData dataContainer = obj as ConteinerIncData;
            IPacket packet = dataContainer.packet;
            BO_Teletrack sender = (BO_Teletrack) dataContainer.sender;

            sender.TimeIncomLastPack = DateTime.Now; //��������� ����� ����������� ������
            SetStatCapacity(packet, sender);

            switch (packet.GetType().Name)
            {
                case "PacketAuthExtendTT":
                case "PacketAuthTT": //����� Auth
                {
                    //�������� ������

                    #region Auth

                    PacketAuthTT pk_au;
                    try
                    {
                        if (packet is PacketAuthExtendTT)
                            pk_au = (PacketAuthExtendTT) packet;
                        else
                            pk_au = (PacketAuthTT) packet;

                        ExecuteLogging.Log("BPLevelTT", String.Format("ParserIncomData. PacketAuthTT. bEndOfWork = {0}", bEndOfWork));

                        if (!bOverloadTaskQueue && !bEndOfWork)
                            NewConnection(sender, pk_au.Login, pk_au.Password); //IncomPackInfo(sender, data);
                        else CloseConnection(sender);
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log("Error_", String.Format("ParserIncomData. PacketAuthTT. {0} \r\n {1}", e.Message, e.StackTrace));
                    }

                    #endregion
                }
                    break;
                case "PacketPKMultiBin": //����� MultiBin
                case "PacketMultiBinTT": //����� PacketMultiBin
                {
                    //�������� ������

                    #region PacketMultiBin

                    ExecuteLogging.Log("BPLevelTT", String.Format("ParserIncomData. PacketMultiBinTT. bEndOfWork = {0}", bEndOfWork));

                    PacketMultiBinTT pk_m_bin = (PacketMultiBinTT) packet;
                    try
                    {
                        if (!IncomNewMBin(sender, pk_m_bin)) return; // 01.03.2012

                        SetStatLastPacketTime(sender);

                        SendAck(sender, false);
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log("Error_", String.Format("ParserIncomData. PacketMultiBinTT {0} \r\n {1}", e.Message, e.StackTrace));
                    }

                    #endregion
                }
                    break;
                case "Packet64MultiBinTT": //����� PacketNewMultiBin
                {
                    //�������� ������

                    #region PacketNewMultiBin

                    ExecuteLogging.Log("BPLevelTT", String.Format("ParserIncomData. Packet64MultiBinTT. bEndOfWork = {0}", bEndOfWork));

                    Packet64MultiBinTT pk_64m_bin = (Packet64MultiBinTT) packet;
                    try
                    {
                        IncomNew64MBin(sender, pk_64m_bin);

                        SetStatLastPacketTime(sender);

                        SendAck(sender, false);
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log("Error_", String.Format("ParserIncomData. Packet64MultiBinTT. {0} \r\n {1}", e.Message, e.StackTrace));
                    }

                    #endregion
                }
                    break;
                case "PacketCmdRequestTT": //����� CmdRequest
                {
                    #region CmdRequest
                    ExecuteLogging.Log("BPLevelTT", String.Format("ParserIncomData. PacketCmdRequestTT. bEndOfWork = {0}", bEndOfWork));
                    try
                    {
                        IncomCmdRequst(sender);
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log("Error_", String.Format("ParserIncomData. PacketCmdRequestTT {0} \r\n {1}", e.Message, e.StackTrace));
                    }

                    #endregion
                }
                    break;
                case "PacketResponseTT": //����� Response
                {
                    #region Response
                    ExecuteLogging.Log("BPLevelTT", String.Format("ParserIncomData. PacketResponseTT. bEndOfWork = {0}", bEndOfWork));
                    try
                    {
                        PacketResponseTT pk_resp = (PacketResponseTT) packet;
                        IncomResponse(sender, pk_resp);
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log("Error_", String.Format("ParserIncomData. PacketResponseTT {0} \r\n {1}", e.Message, e.StackTrace));
                    }

                    #endregion
                }
                    break;
                case "PacketA1TT": //����� A1
                {
                    #region ����� A1
                    ExecuteLogging.Log("BPLevelTT", String.Format("ParserIncomData. PacketA1TT. bEndOfWork = {0}", bEndOfWork));
                    try
                    {
                        PacketA1TT pk_a1 = (PacketA1TT) packet;
                        AnswOnA1(sender, pk_a1);
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log("Error_", String.Format("ParserIncomData. PacketA1TT {0} \r\n {1}", e.Message, e.StackTrace));
                    }

                    #endregion
                }
                    break;
                case "PacketBinTT": //����� Bin
                {
                    #region ����� Bin
                    ExecuteLogging.Log("BPLevelTT", String.Format("ParserIncomData. PacketBinTT. bEndOfWork = {0}", bEndOfWork));
                    try
                    {
                        PacketBinTT pk_bin = (PacketBinTT) packet;
                        if (!IncomNewBin(sender, pk_bin.Packet, true)) return;

                        SetStatLastPacketTime(sender);

                        SendAck(sender, false);
                    }
                    catch (Exception e)
                    {
                        ExecuteLogging.Log("Error_", String.Format("ParserIncomData. PacketBinTT {0} \r\n {1}", e.Message, e.StackTrace));
                    }

                    #endregion
                }
                    break;
                //case (int)TeletrPKType.PACK_ACK: //����� ACK
                default:
                    throw (new Exception("ParserIncomData. ����� �� ���������")); //������ ��������� �������� ������
            }
        }

        /// <summary>
        /// ����������: ����� ��������� ���������� ������ �� ���������
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        protected virtual void SetStatLastPacketTime(BO_Teletrack sender)
        {
        }

        /// <summary>
        /// ���������� ��������� ������� ���������
        /// </summary>
        /// <param name="packet">IPacket</param>
        /// <param name="sender">BO_Teletrack</param>
        protected virtual void SetStatCapacity(IPacket packet, BO_Teletrack sender)
        {
        }

        /// <summary>
        /// ����� ����������
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        /// <param name="login"></param>
        /// <param name="password"></param>
        protected virtual void NewConnection(BO_Teletrack sender, string login, string password)
        {
            throw new NotImplementedException(
                "����� NewConnection ������ BPControllerTT ������ ���� �������� ������������.");
        }

        protected virtual bool IncomNewBin(BO_Teletrack sender, byte[] pk_bin, bool testCRC)
        {
            throw new NotImplementedException(
                "����� IncomNewBin ������ BPControllerTT ������ ���� �������� ������������.");
        }

        /// <summary>
        /// ��������� ����� ������� MultiBin � PacketMultiBin
        /// </summary>
        /// <param name="sender">��������</param>
        /// <param name="pk_m_bin">������</param>
        protected virtual bool IncomNewMBin(BO_Teletrack sender, PacketMultiBinTT pk_m_bin)
        {
            throw new NotImplementedException(
                "����� IncomNewMBin ������ BPControllerTT ������ ���� �������� ������������.");
        }

        protected virtual void IncomNew64Bin(BO_Teletrack sender, byte[] pk_bin)
        {
            throw new NotImplementedException(
                "����� IncomNew64Bin ������ BPControllerTT ������ ���� �������� ������������.");
        }

        /// <summary>
        /// ��������� ����� ������� 64MultiBin
        /// </summary>
        /// <param name="sender">��������</param>
        /// <param name="pk_m_bin">������</param>
        protected virtual bool IncomNew64MBin(BO_Teletrack sender, Packet64MultiBinTT pk_64m_bin)
        {
            throw new NotImplementedException(
                "����� IncomNew64MBin ������ BPControllerTT ������ ���� �������� ������������.");
        }


        string BinToStr(byte[] bytes)
        {
            byte[] zag = new byte[6];
            Array.Copy(bytes, 0, zag, 0, 6);
            string res = Encoding.ASCII.GetString(zag);

            for (int k = 6; k < bytes.Length; k++)
            {
                int i = (int) bytes[k];
                res += string.Format("{0} ", i);
            }
            return res;
        }

        /// <summary>
        /// ������������ ������� �� ����� CmdRequst - ������ 
        /// ��������� ������ ��� ���������� ��
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        protected virtual void IncomCmdRequst(BO_Teletrack sender)
        {
            throw new NotImplementedException(
                "����� IncomCmdRequst ������ BPControllerTT ������ ���� �������� ������������.");
        }

        /// <summary>
        /// ������������� ������� ��� �����������
        /// </summary>
        /// <param name="sender"></param>
        protected virtual void IncomResponse(BO_Teletrack sender, PacketResponseTT pk_response)
        {
            throw new NotImplementedException(
                "����� IncomResponse ������ BPControllerTT ������ ���� �������� ������������.");
        }

        /// <summary>
        /// ����� �� ����� �1
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        /// <param name="pk_A1">PacketA1TT</param>
        protected virtual void AnswOnA1(BO_Teletrack sender, PacketA1TT pk_A1)
        {
            throw new NotImplementedException(
                "����� AnswOnA1 ������ BPControllerTT ������ ���� �������� ������������.");
        }

        /// <summary>
        /// ���������� ����� ACK
        /// </summary>
        /// <param name="sender">BO_Teletrack</param>
        /// <param name="isError"></param>
        protected virtual void SendAck(BO_Teletrack sender, bool isError)
        {
            throw new NotImplementedException(
                "����� SendAck ������ BPControllerTT ������ ���� �������� ������������.");
        }

        #region Old

        //������ ������ � TransferedData � ����� ���������
        /*int InsPackInTransferedDataDS(ConnectionInfoTT cn, byte[] bytes)
    {
        //������ ������ Bin � TransferedData
        int id_packet = TeletreckDALC.InsertPackInDS(tr_dbDS, cn.ID_Teletrack, bytes);
        //���������� ����� Ack
        if (cn.lst_cmd.Count != 0)
            sockTT.Send(cn, PacketAckTT.ACK_OK_Y);//���� �������
        else
            sockTT.Send(cn, PacketAckTT.ACK_OK_N);//��� ��, ������ ���
        return id_packet;
    }*/

        /*
    //������� - ��������� ����� �������
    void Command_TableNewRow(object sender, System.Data.DataTableNewRowEventArgs e)
    {
        int id_tltr = TeletreckDALC.GetTargetNewCmd(e);
        if (active_conn.ContainsKey(id_tltr))//�������� �� �����
            active_conn[id_tltr].lst_cmd.Add(e.Row);//�������� �������
    }

    //������� ���������� ����������
    internal event CmdExecutedEventArgs confirmExCmd;

    /// <summary>
    /// ������� ������������ ����������� ������� 
    /// </summary>
    /// <param name="e">ID_Client + ID_ComInClDB</param>
    void OnCmdExecuted(int id_cmd)
    {
        if (confirmExCmd != null)
            confirmExCmd(id_cmd);
    }
    /// <summary>
    /// ������� - ������ ������ sock
    /// </summary>
    /// <param name="pk">�����</param>
    void MessageError(object sender, MessageErrorEventArgs e)
    {

    }

    /// <summary>
    /// Enqueue to parser
    /// </summary>
    public void EnqueueToParser(object obj, ConnectionInfo conn)
    {
        ConnectionInfoTT conn_tt = (ConnectionInfoTT)conn;
        ThreadPool.QueueUserWorkItem(Parser, conn_tt);
    }

    /// <summary>
    /// ������ ���������
    /// </summary>
    /// <param name="conn">�� ���� �������� ������</param>
    /// <param name="data">������</param>
    /// <param name="pos">������� � ���. ����� ������ ������ ������</param>
    protected void Parser(object obj)
    {
        ConnectionInfoTT conn = (ConnectionInfoTT)obj;

        byte[] bytes = conn.QueueIncomPack;
        int header_pos = 0;
        try
        {
            switch (PacketTT.PacketIdentify(bytes, ref header_pos))
            {
                case 1://����� Auth
                    {
                        //�������� ������
                        #region Auth
                        PacketAuthTT pk_au = new PacketAuthTT();
                        int result = pk_au.DisassemblePacket(bytes, header_pos);

                        if (result != (-1))
                        {
                            //int id_teletrack;
                            conn.Teletrack_info = TeletreckDALC.AuthenticationTT(tr_dbDS, pk_au.Login, pk_au.Password, conn.IP_address, out conn.dr_ownersOfTT);

                            if (conn.ID_Teletrack != 0)//���� ����� ������ � ��
                            {
                                conn.lst_cmd = TeletreckDALC.GetCommands(tr_dbDS, conn.ID_Teletrack);

                                //��������� ���� �� ����� ����������
                                if (active_conn.IndexOfKey(conn.ID_Teletrack) != -1)
                                {
                                    sockTT.CloseSocket(conn);//�������� � �������. ������ � ������
                                    throw (new Exception("����� ������ ���������� � ����������� �����������. ID_Teletrack: " + conn.ID_Teletrack.ToString()));
                                }
                                else
                                {
                                    //�������� � ������ �������� ����������
                                    active_conn.Add(conn.ID_Teletrack, conn);
                                    if (conn.lst_cmd.Count != 0)
                                        sockTT.Send(conn, PacketAckTT.ACK_OK_Y);//���� �������
                                    else
                                        sockTT.Send(conn, PacketAckTT.ACK_OK_N);//��� ��, ������ ���
                                }
                            }
                            else sockTT.CloseSocket(conn); //sockTT.Send(conn, PacketAckTT.ACK_ERR_N);
                        }
                        else sockTT.CloseSocket(conn); //sockTT.Send(conn, PacketAckTT.ACK_ERR_N);

                        //StatisticCalc(conn, pack_size, PacketAckTT.ACK_OK_Y.Length);
                        #endregion
                    }
                    break;
                case 2://����� MultiBin ��� PacketMultiBin
                    {
                        #region MultiBin ��� PacketMultiBin
                        PacketMultiBinTT m_bin = new PacketMultiBinTT();
                        int pack_size = m_bin.DisassemblePacket(bytes, header_pos);
                        if (pack_size != (-1))
                        {
                            if (m_bin.Bin_Count != 0)
                                //������ ������ MultiBin � TransferedData
                                InsPackInTransferedDataDS(conn, bytes);
                            else //������ ����� MultiBin ������ ��� �������� ������� ������
                            {
                                if (conn.lst_cmd.Count != 0)
                                    sockTT.Send(conn, PacketAckTT.ACK_OK_Y);//���� �������
                                else
                                    sockTT.Send(conn, PacketAckTT.ACK_OK_N);//��� ��, ������ ���
                            }
                            //StatisticCalc()
                        }
                        #endregion
                    }
                    break;
                case 3://����� CmdRequest
                    {
                        #region CmdRequest

                        byte[] command = conn.GetCommand();
                        if (command == null)
                        {
                            //��������� ��������� ����� A1
                            PacketA1TT a1 = new PacketA1TT();
                            byte[] sn_pack = a1.AssemblePacket("", new byte[0]);
                            sockTT.Send(conn, sn_pack);
                        }
                        else //�������� ��������� ������� �� ��
                            sockTT.Send(conn, command);
                        #endregion
                    }
                    break;
                case 4://����� Response
                    {
                        #region Response
                        //�������� ������� � DataSet ��� �����������
                        //��������� �� �������� �������
                        if (conn.ID_out_cmd != 0)
                        {
                            TeletreckDALC.SetCmdExecState(tr_dbDS, conn.ID_out_cmd);
                            conn.RemoteCmdFromList();
                            //����������� ������ ������ � ���������
                            OnCmdExecuted(conn.ID_out_cmd);
                            conn.ID_out_cmd = 0;
                        }
                        //���������(����������) ����� Ack
                        if (conn.lst_cmd.Count != 0)
                            sockTT.Send(conn, PacketAckTT.ACK_OK_Y);//���� �������
                        else
                            sockTT.Send(conn, PacketAckTT.ACK_OK_N);//��� ��, ������ ���
                        #endregion
                    }
                    break;
                case 5://����� �1
                    {
                        #region �1
                        if (conn.ID_out_cmd != 0)
                        {
                            //������� ����������
                            TeletreckDALC.SetCmdExecState(tr_dbDS, conn.ID_out_cmd);
                            conn.RemoteCmdFromList();
                            //����������� ������ ������ � ���������
                            //OnCmdExecuted(conn.ID_out_cmd);
                            //conn.ID_out_cmd = 0;
                        }
                        //������ ������ A1 � TransferedData
                        InsPackInTransferedDataDS(conn, bytes);
                        #endregion
                    }
                    break;
                case 6://����� Bin
                    {
                        #region Bin
                        //������ ������ � TransferedData
                        InsPackInTransferedDataDS(conn, bytes);
                        #endregion
                    }
                    break;
                default:
                    throw (new Exception("����� �� ���������"));  //������ ��������� �������� ������
            }
        }
        catch (Exception e)
        {
            MessageErrorEventArgs msg_except = new MessageErrorEventArgs();
            msg_except.Message = e.Message;
            msg_except.DTime = DateTime.Now;
            MessageError(this, msg_except);
        }
    }


    /// <summary>
    /// ������� ����������/������������ ����
    /// </summary>
    /// <param name="incom">��. ����</param>
    /// <param name="outgoing">���. ����</param>
    void StatisticCalc(ConnectionInfoTT cn, int incom, int outgoing)
    {
        //�� �������!
        cn.CapasityIn = incom;
        cn.CapasityOut = outgoing;
    }
    * */

        #endregion

    }
}
