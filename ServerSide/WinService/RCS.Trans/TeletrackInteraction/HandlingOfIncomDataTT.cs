using System;
using System.Text;
using System.Threading;
using RCS.Protocol.TT;
using RCS.Sockets;
using RCS.Sockets.Connection;
using RW_Log;
using TransitServisLib;

namespace RCS.Trans.TeletrackInteraction
{
  public class HandlingOfIncomDataTT : ServerHandler
  {
    public event IncomNewPacket packFromTeletrack;
    public event ConnectedStateEventHandler �onnectedState;
    /// <summary>
    /// �������: ������ �� ������������ ������
    /// </summary>
    public event MessageErrorEventHandler errorOfLevel;

    public HandlingOfIncomDataTT(bool bForTest)
    {
    }

    public HandlingOfIncomDataTT()
    {
      sockServerClass = new ServerSocket();
      sockServerClass.IncomMessage += new IncomingMessageEventHandler(EnqueueToParser);
      sockServerClass.�onnectedStateMsg += new ConnectedStateEventHandler(ConnStateMsg);
      sockServerClass.messageError += new MessageErrorEventHandler(ErrorFromTrLevel);
    }

    private void ErrorFromTrLevel(object sender, MessageErrorEventArgs e)
    {
      if (errorOfLevel != null)
      {
        errorOfLevel(sender, e);
      }
    }

    /// <summary>
    /// �������� �� ������� ���� ��������� ����������
    /// </summary>
    /// <param name="con_state"></param>
    private void ConnStateMsg(ConnectedStateEvArg con_state)
    {
      if (�onnectedState != null)
      {
        �onnectedState(con_state);
      }
    }

    private void OnPackFromTeletrack(ConnectionInfo sender, IPacket pk)
    {
      if (packFromTeletrack != null)
      {
        packFromTeletrack(sender, pk);
      }
    }

    /// <summary>
    /// ���������� � ������� �� ���������
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="connection"></param>
    private void EnqueueToParser(object obj, ConnectionInfo connection)
    {
      IncomToParser(connection);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="conn">����� ����������</param>
    private void IncomToParser(ConnectionInfo conn)
    {
      byte[] task;
      do
      {
        int header_pos = 0;
        task = conn.DeQueueIncomPack();

        if (task != null)
        {
          if (conn.incompl_pack.Len > 0)
          {
            byte[] bytes = conn.incompl_pack.AddHeadOfPacket(task);
            DetectPacket(conn, bytes, header_pos);
          }
          else
          {
            DetectPacket(conn, task, header_pos);
          }
        }
      }
      while (task != null);
    }

    /// <summary>
    /// ���������� ��� ������
    /// </summary>
    /// <param name="conn"></param>
    /// <param name="data"></param>
    /// <param name="pos"></param>
    protected virtual void DetectPacket(ConnectionInfo conn, byte[] data, int pos)
    {
      int header_pos = pos;
      try
      {
        IPacket iptr = PacketTT.PacketIdentify(data, ref header_pos);
        if (iptr != null)
        {
          HandlingOfPack(iptr, conn, data, header_pos);
        }
        else if (data.Length >= 4)//�� ����� ��������)
        {
          new Exception(String.Format(
            "������ ����� ����, �� ���������� ���������: {0}",
            Encoding.ASCII.GetString(data)));
        }
        else
        {
          conn.incompl_pack.PutBufIncompletePack(data, header_pos);
        }
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    private void HandlingOfPack(IPacket pk, ConnectionInfo conn, byte[] bytes, int header_pos)
    {
      int pack_size = pk.DisassemblePacket(bytes, header_pos);

      if (pack_size != (-1))
      {
        if (pack_size + header_pos <= bytes.Length)//����� ����
        {
          pk.PacketSize = pack_size;//��� ����������
          OnPackFromTeletrack(conn, pk);
        }
        else
        {
          ExecuteLogging.Log("BPLevelTT", String.Format(
            "HandlingOfPack: �������� �����. ������ {0} ����.  ����� : {1}",
            bytes.Length, Thread.CurrentThread.GetHashCode()));

          conn.incompl_pack.PutBufIncompletePack(bytes, header_pos);
        }
      }
    }
  }
}
