using System;
using System.ServiceProcess;
using System.Threading;
using RCS.OnlineServerLib;
using RCS.OnlineServerLib.Lic;
using RW_Log;

namespace RCS.OnlineServer
{
    public partial class OnlineServer : ServiceBase
    {
        /// <summary>
        /// �������������� ����� ����������� ���
        /// ������ � ���������� ������ - 5 �����.
        /// </summary>
        private const int ADDITIONAL_TIME = 5*60*1000;

        /// <summary>
        /// ������� ����������.
        /// </summary>
        private volatile MainController _ctrl;

        /// <summary>
        /// ������������ � �������� ���������
        /// </summary>
        private volatile bool _stopping;

        private volatile EventWaitHandle _waitEvent;

        /// <summary>
        /// ������� �����.
        /// </summary>
        private volatile Thread _ctrlThread;

        /// <summary>
        /// ���������� ��������������.
        /// </summary>
        private ILicController _licController;

        /// <summary>
        /// ���������� ������� ��������� ���������� ����� � ����������.
        /// </summary>
        private LicFoundEventHandler _licFoundEventHandler;

        /// <summary>
        /// ���������. 
        /// </summary>
        private Settings _settings;

        public OnlineServer()
        {
            InitializeComponent();
        }

#if DEBUG
        /// <summary>
        /// ��� ������� � ������.
        /// </summary>
        internal void RunDebug()
        {
            _settings = AppSettings.LoadSettings();
            ExecuteLogging.loggingLevel = 1;
            ExecuteLogging.Log("BPLevelTT", "RunDebug. ��� ������� � ������.");

            InnerStart();
        }
#endif

        protected override void OnStart(string[] args)
        {
            this.RequestAdditionalTime(ADDITIONAL_TIME);

            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(UnhandledExceptionHandler);

            base.OnStart(args);

            _settings = AppSettings.LoadSettings();
            if (_settings != null)
            {
                ExecuteLogging.loggingLevel = _settings.ServiceLogLevel;
                ExecuteLogging.Log("BPLevelTT", "");
                ExecuteLogging.Log("BPLevelTT", " ********* ������ ������ ********* ");
                ExecuteLogging.Log("BPLevelTT", System.Reflection.Assembly.GetExecutingAssembly().FullName);

                InnerStart();

                ExecuteLogging.Log("BPLevelTT", "||********************************************||");
                ExecuteLogging.Log("BPLevelTT", "||                ������ ��������             ||");
                ExecuteLogging.Log("BPLevelTT", "||********************************************||");
            }
            else
            {
                ExecuteLogging.Log("BPLevelTT", "�������� ������ ����� ��������. ������ �� �������");
            }
        }

        protected override void OnStop()
        {
            this.RequestAdditionalTime(ADDITIONAL_TIME);
            InnerStop();
            ExecuteLogging.Log("BPLevelTT", "||********************************************||");
            ExecuteLogging.Log("BPLevelTT", "||             ������ �����������             ||");
            ExecuteLogging.Log("BPLevelTT", "||********************************************||");
            base.OnStop();
        }

        protected override void OnShutdown()
        {
            InnerStop();
            ExecuteLogging.Log("BPLevelTT", "||********************************************||");
            ExecuteLogging.Log("BPLevelTT", "||       ������ ����������� OnShutdown()      ||");
            ExecuteLogging.Log("BPLevelTT", "||********************************************||");
            base.OnShutdown();
        }

        private void InnerStop()
        {
            _stopping = true;

            StopLicRefresh();

            if (_ctrl != null)
            {
                _ctrl.Dispose();
                _ctrl = null;
            }
            if (_ctrlThread != null)
            {
                if (_waitEvent != null)
                {
                    _waitEvent.Set();
                }
                _ctrlThread.Join(ADDITIONAL_TIME);
                _ctrlThread = null;
            }
        }

        private void InnerStart()
        {
            try
            {
                _waitEvent = new AutoResetEvent(false);

                ExecuteLogging.Log("BPLevelTT", "InnerStart. ���������� � ������� �������� ������");

                CreateMainControllerThread();
                StartLicSearch();
            }
            catch (Exception e)
            {
                ExecuteLogging.Log("Error_", String.Format(
                    "{0} \r\n {1}", e.Message, e.StackTrace));
                OnStop();
                throw e;
            }
        }

        private void CreateMainControllerThread()
        {
            ExecuteLogging.Log("BPLevelTT", "CreateMainControllerThread. ������ ��������� �������: StartMainCtrlThread");

            _ctrl = new MainController(_settings);
            _ctrlThread = new Thread(this.StartMainCtrlThread);
            _ctrlThread.Name = "MainControllerThread";
            _ctrlThread.Start();
        }

        /// <summary>
        /// ������ ������ �������� �����������.
        /// </summary>
        private void StartMainCtrlThread()
        {
            ExecuteLogging.Log("BPLevelTT", "StartMainCtrlThread. ������ ������ �������� �����������.");

            while (!_stopping)
            {
                _waitEvent.WaitOne();

                if (!_stopping)
                {
                    _ctrl.Start();
                }
            }

            _waitEvent.Close();
            _waitEvent = null;
        }

        /// <summary>
        /// ������ ������ ������ HASP-�����/��������.
        /// </summary>
        private void StartLicSearch()
        {
            _licController = new LicController();
            _licFoundEventHandler = new LicFoundEventHandler(OnLicFound);
            _licController.LicFound += _licFoundEventHandler;
            _licController.Start();
        }

        /// <summary>
        /// ���������� ������� ��������� ������ ��������.
        /// </summary>
        /// <param name="maxPermittedTtCount">
        /// ����������� ����������� ��������� ���-�� ����������.</param>
        private void OnLicFound(int maxPermittedTtCount)
        {
            _settings.MaxPermittedTtCount = maxPermittedTtCount;
            StopLicRefresh();
            if (!_stopping)
            {
                _waitEvent.Set();
            }
        }

        /// <summary>
        /// ��������� �������� ������ ����-�����\��������.
        /// </summary>
        private void StopLicRefresh()
        {
            if (_licController != null)
            {
                _licController.LicFound -= _licFoundEventHandler;
                _licController.Break();
                _licFoundEventHandler = null;
                _licController = null;
            }
        }

        /// <summary>
        /// Unhandled exception handler
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">E</param>
        private void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            ExecuteLogging.Log("Error_", String.Format(
                "UnhandledExceptionHandler {0} \r\n {1}",
                ((Exception) e.ExceptionObject).Message,
                ((Exception) e.ExceptionObject).StackTrace));
            ExecuteLogging.Log("BPLevelTT", "");
            ExecuteLogging.Log("BPLevelTT", "***** ������ ����������� ��-�� ��������� ������. *****");
        }
    }
}
