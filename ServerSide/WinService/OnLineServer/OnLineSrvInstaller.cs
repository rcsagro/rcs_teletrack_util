using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace RCS.OnlineServer
{
  [RunInstaller(true)]
  public partial class OnLineSrvInstaller : Installer
  {
    private System.ServiceProcess.ServiceInstaller serviceInstaller;
    private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;

    public OnLineSrvInstaller()
    {
      InitializeComponent();

      serviceProcessInstaller = new ServiceProcessInstaller();
      serviceInstaller = new ServiceInstaller();

      //# Service Account Information
      serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
      serviceProcessInstaller.Username = null;
      serviceProcessInstaller.Password = null;

      //# Service Information
      serviceInstaller.DisplayName = "OnlineServer";
      serviceInstaller.Description = "������������ ���� ������ � ���������� ����� GPRS � �� �������� �������� ����� Internet";
      serviceInstaller.StartType = ServiceStartMode.Automatic;

      //# This must be identical to the WindowsService.ServiceBase name
      //# set in the constructor of WindowsService.cs
      serviceInstaller.ServiceName = "OnlineServer";

      this.Installers.Add(serviceProcessInstaller);
      this.Installers.Add(serviceInstaller);
    }
  }
}