using System;
using System.ServiceProcess;
using RW_Log;

namespace RCS.OnlineServer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG
            if (Environment.UserInteractive)
            {
                // ������ � ������ ��� �������.
                Console.WriteLine("Main. ������ OnlineServer � ������ �������");
                ExecuteLogging.loggingLevel = 1;
                ExecuteLogging.Log("BPLevelTT", "Main. ������ ������� � ������ �������");
                new OnlineServer().RunDebug();
                Console.WriteLine("Press any key to stop Service1 and exit...");
                Console.ReadLine();
            }
            else
            {
                RunAsService();
            }
#else
            RunAsService();
#endif
        }

        private static void RunAsService()
        {
            ServiceBase[] ServicesToRun;

            // More than one user Service may run within the same process. To add
            // another service to this process, change the following line to
            // create a second service object. For example,
            //
            //   ServicesToRun = new ServiceBase[] {new Service1(), new MySecondUserService()};
            //
            ServicesToRun = new ServiceBase[] {new OnlineServer()};

            ServiceBase.Run(ServicesToRun);
        }
    }
}