SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Firmware](
  [Id] [bigint] IDENTITY(1,1) NOT NULL,
  [Description] [nvarchar](1000) NULL,
  [VersionFrom] [nvarchar](25) NOT NULL,
  [VersionTo] [nvarchar](25) NOT NULL,
  [IssueDate] [datetime] NOT NULL,
  [RegistryDate] [datetime] NOT NULL,
  [FileName] [nvarchar](255) NOT NULL,
  [Source] [text] NOT NULL,
  [Enabled] [bit] NOT NULL CONSTRAINT [DF_Firmware_Enabled]  DEFAULT ((0)),
  [VersionStamp] [timestamp] NOT NULL,
  CONSTRAINT [PK_Firmware] PRIMARY KEY CLUSTERED (
    [Id] ASC
  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_VersionFrom] ON [dbo].[Firmware] (
  [VersionFrom] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_VersionTo] ON [dbo].[Firmware] (
  [VersionTo] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������, ��� ������� ������������� ��� ���������. ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'VersionFrom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������ ���������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'VersionTo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������� ���������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'IssueDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� � ����������� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'FileName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� �������������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'Enabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ��������� ������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'VersionStamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �������� ���������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware'
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FirmwareChanges](
  [Id] [bigint] IDENTITY(1,1) NOT NULL,
  [FirmwareId] [bigint] NOT NULL,
  [Date] [datetime] NOT NULL,
  [Description] [nvarchar](1000) NOT NULL,
  [UserName] [nvarchar](255) NOT NULL,
  [UserIp] [nvarchar](50) NOT NULL,
  CONSTRAINT [PK_FirmwareChanges] PRIMARY KEY CLUSTERED (
    [Id] ASC
  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_FirmwareId] ON [dbo].[FirmwareChanges] (
  [FirmwareId] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Date] ON [dbo].[FirmwareChanges] (
  [Date] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges', @level2type=N'COLUMN',@level2name=N'FirmwareId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges', @level2type=N'COLUMN',@level2name=N'Date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' ��� ������������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ip ����� ������������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges', @level2type=N'COLUMN',@level2name=N'UserIp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������ � ��������� Firmware' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges'
GO

ALTER TABLE [dbo].[FirmwareChanges] WITH CHECK ADD CONSTRAINT [FK_FirmwareChanges_Firmware]
FOREIGN KEY([FirmwareId]) REFERENCES [dbo].[Firmware] ([Id])
ON DELETE CASCADE
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FirmwareTask](
  [Id] [bigint] IDENTITY(1,1) NOT NULL,
  [Description] [nvarchar](1000) NULL,
  [TeletrackLogin] [nchar](4) NOT NULL,
  [FirmwareId] [bigint] NOT NULL,
  [CreatedDate] [datetime] NOT NULL,
  [Stage] [nvarchar](50) NOT NULL,
  [VersionStamp] [timestamp] NOT NULL,
  CONSTRAINT [PK_FirmwareTask] PRIMARY KEY CLUSTERED (
    [Id] ASC
  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, 
    ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
 
CREATE NONCLUSTERED INDEX [IX_FirmwareId] ON [dbo].[FirmwareTask] (
  [FirmwareId] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TeletrackLogin] ON [dbo].[FirmwareTask] (
  [TeletrackLogin] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_CreatedDate] ON [dbo].[FirmwareTask] (
  [CreatedDate] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ���������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask', @level2type=N'COLUMN',@level2name=N'TeletrackLogin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask', @level2type=N'COLUMN',@level2name=N'FirmwareId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� �������� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ������ ��������� ������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask', @level2type=N'COLUMN',@level2name=N'Stage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �������� ��������� �������� ����������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask'
GO

ALTER TABLE [dbo].[FirmwareTask] WITH CHECK ADD CONSTRAINT [FK_FirmwareTask_Firmware]
FOREIGN KEY([FirmwareId]) REFERENCES [dbo].[Firmware] ([Id])
ON DELETE CASCADE
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FirmwareTaskProcessing](
  [Id] [bigint] IDENTITY(1,1) NOT NULL,
  [FirmwareTaskId] [bigint] NOT NULL,
  [Date] [datetime] NOT NULL,
  [Description] [nvarchar](1000) NULL,
  [Stage] [nvarchar](50) NOT NULL,
  [UserName] [nvarchar](255) NOT NULL,
  [UserIp] [nvarchar](50) NOT NULL,
  CONSTRAINT [PK_FirmwareTaskProcessing] PRIMARY KEY CLUSTERED (
    [Id] ASC
  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, 
    ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_FirmwareTaskId] ON [dbo].[FirmwareTaskProcessing] (
  [FirmwareTaskId] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Date] ON [dbo].[FirmwareTaskProcessing] (
  [Date] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Stage] ON [dbo].[FirmwareTaskProcessing] (
  [Stage] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'FirmwareTaskId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'Date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ���������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'Stage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ip ����� ������������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'UserIp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ��������� ����� �������� ��������� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing'
GO

ALTER TABLE [dbo].[FirmwareTaskProcessing] WITH CHECK ADD CONSTRAINT [FK_FirmwareTaskProcessing_FirmwareTask]
FOREIGN KEY([FirmwareTaskId]) REFERENCES [dbo].[FirmwareTask] ([Id])
ON DELETE CASCADE
GO