ALTER PROCEDURE [dbo].[st_SelectClientInfoForUs]
  @id_dealer int,
  @id_user int
AS
SET NOCOUNT ON

DECLARE @dd datetime
SET @dd = getdate()
DECLARE @str char(5)
SET @str = ' ��. '

SELECT cl.ID_Client, cl.LoginCl, cl.Password, 
  CASE (cl.InNetwork)
    WHEN -1 THEN '�� � ����'
    WHEN 0 THEN '�� � ����'
    WHEN 1 THEN '� ����'
  END AS NetState,

  cl.DTimeDeliveredPack, 

  CONVERT(varchar, Datediff(dd, cl.DTimeDeliveredPack, GETDATE()), 2) + @str +
  CONVERT(varchar(8), (GETDATE() - cl.DTimeDeliveredPack), 114) AS DeltaT,

  CASE (SIGN ( cl.ID_Packet - cl.ID_LastPackInSrvDB))
    WHEN -1 THEN '����'
    WHEN 0 THEN '���'
    WHEN 1 THEN '���'
  END AS NewDataDescr,

    b.Name, cl.Describe
FROM UserInfo AS b INNER JOIN ClientInfo AS cl ON cl.ID_User = b.ID_User
WHERE (b.DealerId = @id_dealer) AND (cl.ID_User = @id_user)
ORDER BY b.Name

GO


ALTER PROCEDURE [dbo].[st_SelectTeletrackOfUserForUs]
  @id_dealer int,
  @id_user int
AS
SET NOCOUNT ON

DECLARE @str char(5)
SET @str = ' ��. '

SELECT  tt.ID_Teletrack, tt.LoginTT, tt.PasswordTT, 
  COALESCE(CONVERT(varchar, tt.DTimeLastPack, 121), '��� ������') as LastConn, 

  CASE (COALESCE(CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������'))
    WHEN '��� ������' THEN '��� ������'
    ELSE COALESCE(CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������') + 
      @str + CONVERT(varchar(8), (GETDATE() - tt.DTimeLastPack), 114)
  END AS Delta,

  CASE (COALESCE(CONVERT(varchar, (SIGN(tt.Timeout - CONVERT(int, Datediff(mi, tt.DTimeLastPack, GETDATE()), 2)))), '��� ������'))
    WHEN '��� ������' THEN '��� ������'
    WHEN -1 THEN 'CRITICAL'
    WHEN 0 THEN 'OK'
    WHEN 1 THEN 'OK'
  END AS State, 

  tt.Timeout, u.Name
FROM UserInfo u INNER JOIN TeletrackInfo tt ON u.ID_User = tt.ID_User
WHERE (u.DealerId = @id_dealer) AND (u.ID_User = @id_user)

GO


ALTER PROCEDURE [dbo].[st_SelectStatInfoClForMonitForUs]
  @id_dealer int,
  @id_client int
AS
SET NOCOUNT ON

SELECT cl.LoginCl, st.CapacityOut, st.CapacityIn, 
  st.TimeCon, st.TimeDiscon, us.Name
FROM UserInfo AS us
  INNER JOIN dbo.ClientInfo AS cl ON cl.ID_User = us.ID_User
  INNER JOIN dbo.StatisticDataCl AS st ON st.ID_Client = cl.ID_Client 
WHERE (us.DealerId = @id_dealer) AND (st.id_client = @id_client)
ORDER BY st.TimeCon DESC

GO


ALTER PROCEDURE [dbo].[st_SelectStatInfoClForMonit]
  @id_client int
AS
SELECT cl.LoginCl AS �����, st.CapacityOut AS ��������, st.CapacityIn AS ����������, 
  st.TimeCon AS �����_������, st.TimeDiscon AS �����_��������, us.Name AS ������������
FROM dbo.StatisticDataCl AS st 
  INNER JOIN dbo.ClientInfo AS cl ON st.ID_Client = cl.ID_Client 
  INNER JOIN UserInfo AS us ON cl.ID_User = us.ID_User
WHERE @id_client IS NULL OR (st.id_client = @id_client)
ORDER BY st.TimeCon DESC

GO


ALTER PROCEDURE [dbo].[st_SelectStatInfoTTForMonit] 
  @id_teletrack int
AS
SET NOCOUNT ON

SELECT tt.LoginTT AS �����, st.CapacityOut AS ��������, st.CapacityIn AS ����������, 
  st.TimeCon AS �����_������, st.TimeDiscon AS �����_��������
FROM dbo.StatisticDataTT AS st INNER JOIN dbo.TeletrackInfo AS tt ON st.ID_Teletrack = tt.ID_Teletrack 
WHERE (st.id_teletrack = @id_teletrack)
ORDER BY st.TimeCon DESC

GO

