ALTER PROCEDURE dbo.adm_DeleteCommunicTable
  @id int
AS
SET NOCOUNT OFF

DELETE FROM dbo.CommunicTable 
WHERE ID_Communic = @id

exec incrementOfVersion 'CommunicTable'

GO
