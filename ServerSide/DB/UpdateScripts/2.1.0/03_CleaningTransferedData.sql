DELETE FROM dbo.TransferedData 
WHERE ID_Teletrack IS NULL
GO

DELETE FROM dbo.TransferedData 
WHERE ID_Teletrack IN (
  SELECT DISTINCT d.ID_Teletrack 
  FROM dbo.TransferedData d LEFT JOIN dbo.TeletrackInfo i 
    ON d.ID_Teletrack = i.ID_Teletrack
  WHERE i.ID_Teletrack IS NULL)
GO
