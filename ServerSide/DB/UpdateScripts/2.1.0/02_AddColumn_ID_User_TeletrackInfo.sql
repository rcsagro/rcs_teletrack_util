ALTER TABLE dbo.TeletrackInfo ADD ID_User int 
GO

WITH TtUser(TtID, UserId)
AS (
  SELECT DISTINCT ti.ID_Teletrack, ci.ID_User
  FROM dbo.ClientInfo ci 
    INNER JOIN dbo.CommunicTable ct ON ci.ID_Client = ct.ID_Client
    INNER JOIN dbo.TeletrackInfo ti ON ct.ID_Teletrack = ti.ID_Teletrack
)
UPDATE dbo.TeletrackInfo  
SET ID_User = tu.UserId
FROM TtUser tu
WHERE ID_Teletrack = tu.TtID
GO

ALTER TABLE dbo.TeletrackInfo ALTER COLUMN ID_User int NOT NULL
GO

CREATE NONCLUSTERED INDEX IX_IdUser ON dbo.TeletrackInfo (
  ID_User ASC
) WITH (
  PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO