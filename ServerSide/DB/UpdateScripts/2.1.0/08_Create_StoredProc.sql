CREATE PROCEDURE dbo.adm_SelectTeletrackInfoByLogin
  @login char(4)
AS
SET NOCOUNT ON

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout, GSMIMEI, DTimeLastPack, ID_User
FROM dbo.TeletrackInfo
WHERE LoginTT = @login

GO


CREATE PROCEDURE dbo.adm_SelectTeletrackInfoByID
  @id int
AS
SET NOCOUNT ON

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout, GSMIMEI, DTimeLastPack, ID_User
FROM dbo.TeletrackInfo
WHERE ID_Teletrack = @id

GO


CREATE PROCEDURE dbo.adm_SelectClientInfoByLogin
  @loginCl nvarchar(50) 
AS
SET NOCOUNT ON

SELECT ID_Client, LoginCl, Password, InNetwork, ID_Packet,
  DTimeDeliveredPack, ID_LastPackInSrvDB, ID_User, Describe
FROM dbo.ClientInfo
WHERE LoginCl = @loginCl

GO


CREATE PROCEDURE dbo.adm_SelectClientInfoByID
  @id int 
AS
SET NOCOUNT ON

SELECT ID_Client, LoginCl, Password, InNetwork, ID_Packet,
  DTimeDeliveredPack, ID_LastPackInSrvDB, ID_User, Describe
FROM dbo.ClientInfo
WHERE ID_Client = @id

GO

CREATE PROCEDURE dbo.adm_SelectTeletrackLinkedState
  @idUser int,
  @idClient int,
  @startIndex int,
  @finishIndex int
AS
SET NOCOUNT ON

SELECT DetailID, DetailLogin, CommunicID, Linked 
FROM (
  SELECT tt.ID_Teletrack AS DetailID, tt.LoginTT AS DetailLogin, ct.ID_Communic AS CommunicID, 
    CAST(
      CASE 
        WHEN ct.ID_Client IS NULL THEN 0
        ELSE 1
      END 
    AS bit) AS Linked,
    ROW_NUMBER() OVER (ORDER BY tt.LoginTT) AS RowNum
  FROM dbo.TeletrackInfo tt 
    LEFT JOIN dbo.ClientInfo ci ON (tt.ID_User = ci.ID_User)
    LEFT JOIN dbo.CommunicTable ct ON (ci.ID_Client = ct.ID_Client)
      AND (tt.ID_Teletrack = ct.ID_Teletrack)
    WHERE (tt.ID_User = @idUser) AND (ci.ID_Client = @idClient)) tt
WHERE tt.RowNum BETWEEN @startIndex AND @finishIndex

GO


CREATE PROCEDURE dbo.adm_SelectClientLinkedState
  @idUser int,
  @idTeletrack int
AS
SET NOCOUNT ON

SELECT ci.ID_Client AS DetailID, ci.LoginCl AS DetailLogin, ct.ID_Communic AS CommunicID, 
  CAST(
    CASE 
      WHEN ct.ID_Client IS NULL THEN 0
      ELSE 1
    END 
  AS bit) AS Linked
FROM dbo.ClientInfo ci 
  LEFT JOIN dbo.TeletrackInfo tt ON (ci.ID_User = tt.ID_User)
  LEFT JOIN dbo.CommunicTable ct ON (ci.ID_Client = ct.ID_Client)
   AND (tt.ID_Teletrack = ct.ID_Teletrack)
WHERE (ci.ID_User = @idUser) AND (tt.ID_Teletrack = @idTeletrack)
ORDER BY ci.LoginCl

GO


CREATE PROCEDURE dbo.st_SelectStatInfoTTForMonitForUs 
  @id_dealer int,
  @id_teletrack int
AS
SET NOCOUNT ON

SELECT tt.LoginTT, st.CapacityOut, st.CapacityIn, st.TimeCon, st.TimeDiscon
FROM dbo.UserInfo AS u 
  INNER JOIN dbo.TeletrackInfo AS tt ON u.ID_User = tt.ID_User
  INNER JOIN dbo.StatisticDataTT AS st ON st.ID_Teletrack = tt.ID_Teletrack 
WHERE (u.DealerId = @id_dealer) AND (st.id_teletrack = @id_teletrack)
ORDER BY st.TimeCon DESC

GO
