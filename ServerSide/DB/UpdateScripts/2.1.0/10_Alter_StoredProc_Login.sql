ALTER PROCEDURE [dbo].[FindDealer]
  @DealerName nvarchar(50),
  @Pass nvarchar(50)
AS
SET NOCOUNT ON

-- case sensitive search
SELECT TOP(1) ID
FROM dbo.DealerInfo
WHERE (CAST(DealerName AS varbinary(max)) = CAST(@DealerName AS varbinary(max))) AND 
  (CAST([Password] AS varbinary(max)) = CAST(@Pass AS varbinary(max)))

GO


ALTER PROCEDURE [dbo].[FindRoles]
  @DealerName nvarchar (50)
AS
SET NOCOUNT ON

SELECT R.Role 
FROM dbo.DealerInfo AS U, dbo.Roles AS R, dbo.DealerRole AS UR
WHERE (U.DealerName = @DealerName) AND 
  (UR.DealerID = U.ID) AND (UR.RoleID = R.ID)

GO
