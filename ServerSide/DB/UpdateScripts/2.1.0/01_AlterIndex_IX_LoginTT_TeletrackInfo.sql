if exists (
  SELECT 1 
  FROM sys.indexes 
  WHERE (object_id = OBJECT_ID(N'[dbo].[TeletrackInfo]')) AND (name = N'IX_LoginTT'))
begin
  DROP INDEX IX_LoginTT ON dbo.TeletrackInfo
end
GO

CREATE UNIQUE NONCLUSTERED INDEX IX_LoginTT ON dbo.TeletrackInfo (
  LoginTT
) WITH (
  PAD_INDEX = OFF, FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
) ON [PRIMARY]
GO
