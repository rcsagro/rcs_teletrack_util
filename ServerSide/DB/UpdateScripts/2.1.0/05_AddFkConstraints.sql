ALTER TABLE dbo.TeletrackInfo WITH CHECK ADD CONSTRAINT FK_TeletrackInfo_UserInfo 
  FOREIGN KEY (ID_User) REFERENCES dbo.UserInfo (ID_User)
GO

ALTER TABLE dbo.ClientInfo WITH CHECK ADD CONSTRAINT FK_ClientInfo_UserInfo 
  FOREIGN KEY (ID_User) REFERENCES dbo.UserInfo (ID_User)
GO

ALTER TABLE dbo.TransferedData WITH CHECK ADD CONSTRAINT FK_TransferedData_TeletrackInfo 
  FOREIGN KEY (ID_Teletrack) REFERENCES dbo.TeletrackInfo (ID_Teletrack) 
  ON DELETE CASCADE
GO
