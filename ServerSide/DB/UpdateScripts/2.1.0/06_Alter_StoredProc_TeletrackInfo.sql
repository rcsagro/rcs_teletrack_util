/* --------------------------------   adm_SelectTeletrackInfo  -----------------------------------  */
ALTER PROCEDURE dbo.adm_SelectTeletrackInfo
AS
SET NOCOUNT ON

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout, GSMIMEI, DTimeLastPack, ID_User
FROM TeletrackInfo

GO


/* --------------------------------   adm_UpdateTeletrackInfo  -----------------------------------  */
ALTER PROCEDURE dbo.adm_UpdateTeletrackInfo
  @LoginTT char(4),
  @PasswordTT varchar(10),
  @Timeout int,
  @ID_Teletrack int,
  @GSMIMEI varchar(20),
  @ID_User int
AS
SET NOCOUNT OFF

UPDATE TeletrackInfo 
SET LoginTT = @LoginTT, PasswordTT = @PasswordTT, Timeout = @Timeout, 
  GSMIMEI = @GSMIMEI, ID_User = @ID_User
WHERE ID_Teletrack = @ID_Teletrack

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout, GSMIMEI, ID_User 
FROM TeletrackInfo 
WHERE ID_Teletrack = @ID_Teletrack

exec incrementOfVersion 'TeletrackInfo'

GO


/* --------------------------------   adm_InsertTeletrackInfo  -----------------------------------  */
ALTER PROCEDURE dbo.adm_InsertTeletrackInfo
  @LoginTT char(4),
  @PasswordTT varchar(10),
  @Timeout int,
  @GSMIMEI varchar(20) = NULL,
  @ID_User int
AS
SET NOCOUNT OFF

INSERT INTO TeletrackInfo (LoginTT, PasswordTT, Timeout, GSMIMEI, ID_User) 
VALUES (@LoginTT, @PasswordTT, @Timeout, @GSMIMEI, @ID_User)

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout, GSMIMEI, ID_User 
FROM TeletrackInfo 
WHERE ID_Teletrack = SCOPE_IDENTITY()

exec incrementOfVersion 'TeletrackInfo'

GO


/* --------------------------------   DeleteTeletrackOldData  -----------------------------------  */
ALTER PROCEDURE dbo.DeleteTeletrackOldData
AS
SET NOCOUNT ON

DECLARE @date DateTime
SET @date = GETDATE()

-- �� ��������� ������� ��������� ���������� � ������ �������� ������ ����������
SELECT ti.ID_Teletrack, usr.Retention_Period INTO #TempTable
FROM dbo.TeletrackInfo ti JOIN dbo.UserInfo usr 
  ON usr.ID_User = ti.ID_User
ORDER BY 1

-- �������� ������ ����������
DELETE FROM dbo.TransferedData
FROM #TempTable tmp JOIN dbo.TransferedData data 
  ON tmp.ID_Teletrack = data.ID_Teletrack
WHERE data.DTimeIncomData < dateadd("Month", tmp.Retention_Period * -1, @date)

-- �������� ���������� ������ ����������
DELETE FROM dbo.StatisticDataTT
FROM #TempTable tmp JOIN dbo.StatisticDataTT stat 
  ON tmp.ID_Teletrack = stat.ID_Teletrack
WHERE stat.TimeDiscon < dateadd("Month", tmp.Retention_Period * -1, @date)

DROP TABLE #TempTable  

GO


/* --------------------------------   st_SelectAllTeletrackForMonit  -----------------------------------  */
ALTER PROCEDURE dbo.st_SelectAllTeletrackForMonit
AS
DECLARE @str varchar(5)
SET @str = ' ��. '

SELECT DISTINCT tt.ID_Teletrack, tt.LoginTT, tt.PasswordTT, 
  COALESCE(CONVERT(varchar, st.TimeCon, 121),'��� ������') AS LastConn, 

  CASE (COALESCE(CONVERT(varchar, Datediff(dd, st.TimeCon, GETDATE()), 2), '��� ������'))
    WHEN '��� ������' THEN '��� ������'
    ELSE COALESCE(CONVERT(varchar, Datediff(dd, st.TimeCon, GETDATE()), 2), '��� ������') + 
      @str + CONVERT(varchar(8), (GETDATE() - st.TimeCon), 114)
  END AS Delta,

  CASE (COALESCE(CONVERT(varchar, (SIGN(tt.Timeout - CONVERT(int, Datediff(mi, st.TimeCon, GETDATE()), 2)))), '��� ������'))
    WHEN '��� ������' THEN '��� ������'
    WHEN -1 THEN 'CRITICAL'
    WHEN 0 THEN 'OK'
    WHEN 1 THEN 'OK'
  END AS State,

 tt.Timeout, u.Name

FROM TeletrackInfo AS tt 
  INNER JOIN UserInfo AS u ON tt.ID_User = u.ID_User 
  LEFT JOIN StatisticDataTT AS st ON tt.ID_Teletrack = st.ID_Teletrack 
    AND (st.TimeCon IS NULL) OR (st.Id_Session = (
      SELECT MAX(ins.Id_Session) AS MaxSesId 
      FROM StatisticDataTT ins 
      WHERE ins.ID_Teletrack = tt.ID_Teletrack))
      ORDER BY u.Name

GO


/* --------------------------------   st_SelectAllTeletrackForMonit  -----------------------------------  */
ALTER PROCEDURE dbo.st_SelectTeletrack
  @id_user int
AS
SET NOCOUNT ON

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout
FROM TeletrackInfo 
WHERE ID_User = @id_user

GO


/* --------------------------------   st_SelectTeletrackOfUser  -----------------------------------  */
ALTER PROCEDURE dbo.st_SelectTeletrackOfUser
  @id_user int,
  @Id_tltr int = 0
AS
BEGIN

if @Id_tltr = 0 
begin
  if(@id_user <= 0) 
  begin
     SET @id_user = null;
  end
  
  DECLARE @str varchar(5);
  SET @str = ' ��. ';
  
  SELECT  tt.ID_Teletrack, tt.LoginTT, tt.PasswordTT, 
    COALESCE(CONVERT(varchar, tt.DTimeLastPack, 121), '��� ������') AS LastConn, 

    CASE (COALESCE(CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������'))
      WHEN '��� ������' THEN '��� ������'
      ELSE COALESCE(CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2),'��� ������') + 
        @str + CONVERT(varchar(8), (GETDATE() - tt.DTimeLastPack), 114)
      END AS Delta,

      CASE (COALESCE(CONVERT(varchar, (SIGN(tt.Timeout - CONVERT(int, Datediff(mi, tt.DTimeLastPack, GETDATE()), 2)))), '��� ������'))
        WHEN '��� ������' THEN '��� ������'
        WHEN -1 THEN 'CRITICAL'
        WHEN 0 THEN 'OK'
        WHEN 1 THEN 'OK'
      END AS State, 

      tt.Timeout, u.Name AS UserName
  FROM TeletrackInfo tt LEFT JOIN UserInfo u ON tt.ID_User = u.ID_User
  WHERE  (@id_user is null) or (u.ID_User = @ID_User)
end
else begin
  exec st_SelectTeletrackOfId @Id_tltr
end
END

GO


/* --------------------------------   st_SelectTeletrackOfId  -----------------------------------  */
ALTER PROCEDURE dbo.st_SelectTeletrackOfId
  @Id_Teletr int
AS

SET NOCOUNT ON;
DECLARE @str varchar(5);
SET @str = ' ��. ';

SELECT tt.ID_Teletrack, tt.LoginTT, tt.PasswordTT, COALESCE (CONVERT(varchar, tt.DTimeLastPack, 121), '��� ������') AS LastConn, 

  CASE (COALESCE (CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������')) 
    WHEN '��� ������' THEN '��� ������' 
    ELSE COALESCE (CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������') 
      + @str + CONVERT(varchar(8), (GETDATE() - tt.DTimeLastPack), 114) 
  END AS Delta, 

  CASE (COALESCE (CONVERT(
    varchar, (SIGN(tt.Timeout - CONVERT(int, Datediff(mi, tt.DTimeLastPack, GETDATE()), 2)))), '��� ������')) 
    WHEN '��� ������' THEN '��� ������' 
    WHEN - 1 THEN 'CRITICAL' 
    WHEN 0 THEN 'OK' 
    WHEN 1 THEN 'OK' 
  END AS State, 

  tt.Timeout, '' AS Name,  UserInfo.Name AS UserName
FROM UserInfo 
  INNER JOIN adm_User_TT ON UserInfo.ID_User = adm_User_TT.ID_User 
  INNER JOIN TeletrackInfo AS tt ON adm_User_TT.ID_Teletrack = tt.ID_Teletrack
WHERE (tt.ID_Teletrack = @Id_Teletr)

GO
