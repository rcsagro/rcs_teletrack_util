ALTER PROCEDURE dbo.selectCommand
AS
SELECT * 
FROM dbo.Command
WHERE (DeltaTimeExCom > getdate()) AND (ExecuteState < 2) AND (Approved = 1) 
ORDER BY DateQueueEntry
GO


-- ==========================================================
-- ��������� ������ ����� ������� � �� �������
-- ==========================================================
ALTER PROCEDURE dbo.insert_command
  @loginTT char(4), -- ���(�����) ���������
  @id_teletreck int output, -- ID ��������� � �� �������
  @id_client int, -- ID ������� � �� �������
  @id_cmdClDB int, -- ID ������� � �� �������
  @cmdtext binary(160), -- �������
  @id_cmd int output, -- ID ����������� �������
  @checkInTime datetime, -- ����� ������� ������� �� ����������
  @expirationTime datetime, -- ����� ��������� �������� ������� 
  @dispatcher_ip varchar(40) -- IP-����� ����������
AS
SET NOCOUNT ON
--
SELECT @id_teletreck = ID_Teletrack 
FROM dbo.TeletrackInfo 
WHERE LoginTT = @loginTT
--
IF @id_teletreck > 0
BEGIN
  INSERT INTO dbo.Command (CommandText, DateQueueEntry, DeltaTimeExCom, ID_Client, 
    ID_Teletrack, ID_ComInClDB, DispatcherIP, Approved) 
  VALUES (CONVERT(varbinary(max), @cmdtext), @checkInTime, @expirationTime, @id_client, 
    @id_teletreck, @id_cmdClDB, @dispatcher_ip, 0)
  --���������� id_comand
  SELECT @id_cmd = SCOPE_IDENTITY();
END ELSE
BEGIN 
  INSERT INTO dbo.ExeptError (ErrorMsg) 
  VALUES ('������ ���������� ������� A1. ����������� ' + @loginTT + ' � �� ��� ������� � id=' + @id_client )
END
GO


-- =============================================
-- ���������� ��������� 1000 ����������� ������� �� ������� �������
-- =============================================
ALTER PROCEDURE dbo.selectForSendQuery
  @id_client int,
  @loginTT char(4),
  @param1 bigint,
  @param2 bigint
AS
DECLARE @id_teletreck int
--
SELECT  @id_teletreck = COALESCE(ct.ID_Teletrack, -1) 
FROM dbo.TeletrackInfo info INNER JOIN dbo.CommunicTable ct
  ON info.ID_Teletrack = ct.ID_Teletrack
WHERE (ct.ID_Client = @id_client) AND (info.LoginTT = @loginTT)
--
IF @id_teletreck >= 0
  SELECT TOP(1000) ID_Packet, IncomData 
  FROM dbo.TransferedData WITH(INDEX(NCI_Packet_Teletrack))
  WHERE (ID_Teletrack = @id_teletreck) AND
    (ID_Packet > @param1) AND (ID_Packet <= @param2)
  ORDER BY ID_Packet DESC
ELSE
  INSERT INTO dbo.ExeptError (ErrorMsg) 
  VALUES ('������ ������� ����������� ������. ����������� �������� ' + @loginTT + ' � �� �������.' )
GO


-- =============================================
-- �������� ���������� ������ ����������.
-- � �������� ������ �������� 10 ��������� ������� ������������� ���������.
-- =============================================
ALTER PROCEDURE dbo.DeleteTeletrackOldData
AS
SET NOCOUNT ON;

-- �� ��������� ������� ��������� ���������� � ������ �������� ������ ����������
SELECT ti.ID_Teletrack, 
  dateadd("Month", usr.Retention_Period * -1, GETDATE()) as Retention_Period 
  INTO #TempTable
FROM dbo.TeletrackInfo ti JOIN dbo.UserInfo usr 
  ON usr.ID_User = ti.ID_User
ORDER BY 1

-- �������� ������ ����������
DELETE FROM data
FROM #TempTable tmp JOIN (
  SELECT ID_Packet, ID_Teletrack, DTimeIncomData,
    ROW_NUMBER() OVER(PARTITION BY ID_Teletrack ORDER BY DTimeIncomData DESC) AS RowNumber
  FROM dbo.TransferedData) data ON data.ID_Teletrack = tmp.ID_Teletrack
WHERE (data.RowNumber > 10) AND (data.DTimeIncomData < tmp.Retention_Period)

-- �������� ���������� ������ ����������
DELETE FROM stat
FROM #TempTable tmp JOIN (
  SELECT ID_Session, ID_Teletrack, TimeDiscon,
    ROW_NUMBER() OVER(PARTITION BY ID_Teletrack ORDER BY TimeDiscon DESC) AS RowNumber
  FROM dbo.StatisticDataTT) stat ON stat.ID_Teletrack = tmp.ID_Teletrack
WHERE (stat.RowNumber > 10) AND (stat.TimeDiscon < tmp.Retention_Period)

-- �������� ������ A1. 
DELETE FROM cmd 
FROM #TempTable tmp JOIN (
  SELECT ID_Command, ID_Teletrack, DeltaTimeExCom,
    ROW_NUMBER() OVER(PARTITION BY ID_Teletrack ORDER BY DeltaTimeExCom DESC) AS RowNumber
  FROM dbo.Command) cmd ON cmd.ID_Teletrack = tmp.ID_Teletrack
WHERE (cmd.RowNumber > 10) AND (cmd.DeltaTimeExCom < tmp.Retention_Period)
--
DROP TABLE #TempTable  
GO


-- =============================================
-- �������� ���������� ������ ���������� ������ ��������-�����������.
-- =============================================
ALTER PROCEDURE dbo.DeleteClientOldData
AS
SET NOCOUNT ON;

-- �� ��������� ������� ��������� ���������� � ������ �������� ���������� �� ��������
SELECT clnt.ID_Client, 
  dateadd("Month", usr.Retention_Period * -1, GETDATE()) as Retention_Period 
  INTO #TempTable
FROM dbo.ClientInfo clnt JOIN UserInfo usr ON usr.ID_User = clnt.ID_User 

-- �������� ���������� �� ��������. ������ �������� 10 ��������� ������� ������������� �������.
DELETE FROM stat
FROM #TempTable tmp JOIN (
  SELECT ID_SessionCl, ID_Client, TimeDiscon,
    ROW_NUMBER() OVER(PARTITION BY ID_Client ORDER BY TimeDiscon DESC) AS RowNumber
  FROM dbo.StatisticDataCl) stat ON stat.ID_Client = tmp.ID_Client
WHERE (stat.RowNumber > 10) AND (stat.TimeDiscon < tmp.Retention_Period)

-- �������� ���������� � �������� ��������
DELETE FROM dbo.ClientQuery
FROM #TempTable tmp JOIN dbo.ClientQuery clQuery ON tmp.ID_Client = clQuery.ID_Client
WHERE clQuery.DateTimeExQuery < tmp.Retention_Period
--
DROP TABLE #TempTable
GO


-- =============================================
-- ����� ���������� ������� ����������
-- =============================================
ALTER PROCEDURE dbo.st_SelectDateLast_StatisticDataTT
AS
SET NOCOUNT ON;
--
SELECT TOP(1) TimeCon AS D
FROM dbo.StatisticDataTT
ORDER BY ID_Session DESC
GO