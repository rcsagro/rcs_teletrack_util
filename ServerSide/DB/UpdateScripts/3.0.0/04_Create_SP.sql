-- =============================================
-- Description: 
-- ��������� ��� ������ ����� "����������" ��� ������� �1.
-- ����������� ������ �������, ������� ��� �� ���� ����������
-- ���������� � ���� �������� ������� ��� �� �����.
-- � ������ ��������� ���������� �������������� ������ ������
-- ������� Command.
-- =============================================
CREATE PROCEDURE dbo.adm_SetCommandApproved(
  @id int,
  @approved bit)
AS
SET NOCOUNT ON
--
UPDATE dbo.Command 
SET Approved = @approved
WHERE (ID_Command = @id) AND (DeltaTimeExCom > getdate()) AND (ExecuteState = 0)
--
IF @@ROWCOUNT > 0 
  EXEC dbo.incrementOfVersion 'Command'
GO
