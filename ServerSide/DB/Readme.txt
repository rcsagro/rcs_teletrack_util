���������� ��������� �� ������� �� ���������� ������:
 - �������� �������������� �������� � SQL Server Management Studio
   � ����������� �� � ����� ����������;
 - ������ ����������������� ����� UpdaterConfig.xml ��������� ��;
 - ���������� ����� ������� �������� �� CreateDB.sql � ����������� ����,
   � ������������ �� ��������� �����������.

������� ���������� �� ������� �� ��������� ������ �������� ����� ����������.
����� ���������� ������������ ����� ���������� � UpdateScripts, ����������
������� ����������.

������ �� ��������� � ����������� �������� VersionDB. �������� ������
������ ���� ����������, ������� ��� �������� ��������� �� �������� ���������
@DB_VERSION � ������� CreateDB.sql (������ 1.1. DATABASE VERSION).

���������� � ����������� ��������.      
  - ������������ batch'�� �������� ������� GO. ������� GO ����������� 
    ������ ���������� �� ��������� ������ (�� ���� ������ �� ������ ����
    ������� �������������� ������, SQL-����������, ������������).
  - � ������ ������� ������������� ���������� TCL (COMMIT, ROLLBACK  
    � �.�.) �� ���������, �.�. ��� batches, ������������ � ������� 
    ����������� � ����� ����������.


���������.

14.04.15 ������ 5.0.0

 ��������� ���� IncomData � ������� TransferedData �� varbinary(64) NOT NULL

.09.11 ������ 4.0.0
  - ��������� ���������� ��������� ������������ ����������. ����� �������:
    * Firmware - ��������� �������� ���������;
    * FirmwareChanges - ������ ������ � ��������� Firmware;
    * FirmwareTask - ������ �������� ��������� �������� ����������;
    * FirmwareTaskProcessing - ������ ��������� ����� �������� ��������� 
      ��������.

25.11.10 ������ 3.0.0
  - � ������� Command ����� ����: 
    * DispatcherIP - IP-����� ����������, �� �������� ��������� �������;
    * Approved - ���� ����������� � ����������. 
  - � ������� Command ����� �������: 
    * IX_DeltaTime_State_Approved(DeltaTimeExCom, ExecuteState, Approved);
    * IX_IdClient(ID_Client).
  - �������� ���������: selectCommand, insert_command, selectForSendQuery, 
    DeleteTeletrackOldData, DeleteClientOldData, st_SelectDateLast_StatisticDataTT.
  - ��������� ��������� adm_SetCommandApproved.

18.10.10 ������ 2.2.0
  - ������ ����� ������ NCI_Packet_Teletrack � ������� TransferedData �� �����
    ID_Teletrack � ID_Packet, � ������ ������� IX_DTimeIncomData � 
    IX_IdTeletrack �������.

10.09.10 ������ 2.1.0
  - ��������� ����������� �������� �� "������" VersionDB.
  - ������ ���������� ������ �� ���� LoginCl ������� ClientInfo.
  - ������ ���������� ������ �� ���� LoginTT ������� TeletrackInfo.
  - ��������� ���� ID_User � ������� TeletrackInfo ��� ����� � 
    �������� UserInfo.
  - ������� ������ �� TransferedData, ������� ��������� ����� � TeletrackInfo.
  - ���������� TransferedData: ID_Teletrack ���� NOT NULL.
  - ��������� ������� ����� ����� ��������� TeletrackInfo-UserInfo, 
    ClientInfo-UserInfo, TransferedData-TeletrackInfo.
  - �������� ���������, ���������� � �������� TeletrackInfo.
  - �������� ���������, ���������� � �������� CommunicTable.
  - ����� ���������: adm_SelectTeletrackInfoByLogin, 
    adm_SelectTeletrackInfoByID, adm_SelectClientInfoByLogin, 
    adm_SelectClientInfoByID, adm_SelectTeletrackLinkedState, 
    adm_SelectClientLinkedState.
  - �������� ���������, ������������ ���������� ��� ���� User. 
  - �������� ��������� �������������� ������� �� Web �����: 
    FindDealer, FindDoles. ������ case sensitive �����.
