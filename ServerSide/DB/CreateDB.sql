/************************************************************/
/*                                                          */
/*    ������ �������� �� ��� ��������������� �������        */
/*                      4.2.0                               */
/*                                                          */
/*  ������                                                  */
/*    1. DATABASE                                           */
/*      1.1. DATABASE VERSION                               */
/*    2. TABLES                                             */
/*    3. PROCEDURES                                         */
/*    4. VIEWS                                              */
/*    5. REFERENCES                                         */
/*    6. SERVICE BROKER                                     */
/*    7. JOBS                                               */
/*                                                          */
/*  ��� �������� ��������� ��������������� ������������     */
/*  ���� ���������.                                         */
/*                                                          */
/*  ��������!!!                                             */
/*    ����� �������� ��������� �������� ������ ��           */
/*    � ������� 1.1. DATABASE VERSION.                      */
/*                                                          */
/*                                                          */
/************************************************************/


/************************************************************/
/*                                                          */
/*                     1. DATABASE                          */
/*                                                          */
/************************************************************/

USE [master]
GO

/****** Object:  Database [OnLineServiceDB]    Script Date: 08/11/2009 11:35:01 ******/
CREATE DATABASE [OnLineServiceDB] ON  PRIMARY 
( NAME = N'OnLineServiceDB', FILENAME = N'D:\MSSQL_DB\OnLineServiceDB.mdf' , SIZE = 41536KB , MAXSIZE = UNLIMITED, FILEGROWTH = 102400KB )
 LOG ON 
( NAME = N'OnLineServiceDB_log', FILENAME = N'D:\MSSQL_DB\OnLineServiceDB_log.ldf' , SIZE = 4080KB , MAXSIZE = 2048GB , FILEGROWTH = 102400KB )
GO

EXEC dbo.sp_dbcmptlevel @dbname=N'OnLineServiceDB', @new_cmptlevel=90
GO

USE [OnLineServiceDB]
GO

DECLARE @DB_VERSION sql_variant

/************************************************************/
/*                 1.1. DATABASE VERSION                    */
/************************************************************/
/***********/                                 /**************/
/***********/   SET @DB_VERSION = N'5.0.0'    /**************/
/***********/                                 /**************/
/************************************************************/

EXEC [sys].[sp_addextendedproperty] @name=N'VersionDB', @value=@DB_VERSION  

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [OnLineServiceDB].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [OnLineServiceDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [OnLineServiceDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET AUTO_UPDATE_STATISTICS OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [OnLineServiceDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [OnLineServiceDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [OnLineServiceDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [OnLineServiceDB] SET  READ_WRITE 
GO
ALTER DATABASE [OnLineServiceDB] SET RECOVERY FULL 
GO
ALTER DATABASE [OnLineServiceDB] SET  MULTI_USER 
GO
ALTER DATABASE [OnLineServiceDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [OnLineServiceDB] SET DB_CHAINING OFF 
GO

USE [OnLineServiceDB]
GO


/************************************************************/
/*                                                          */
/*                      2. TABLES                           */
/*                                                          */
/************************************************************/

/****** Object:  Table [dbo].[VersionControl]    Script Date: 08/11/2009 11:36:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VersionControl](
  [ID] [tinyint] IDENTITY(1,1) NOT NULL,
  [TableName] [varchar](50) NOT NULL,
  [Version] [int] NOT NULL,
 CONSTRAINT [PK_VersionControl] PRIMARY KEY CLUSTERED 
(
  [ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[UserInfo]    Script Date: 08/11/2009 11:36:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserInfo](
  [ID_User] [int] IDENTITY(1,1) NOT NULL,
  [Name] [varchar](max) NOT NULL,
  [Describe] [varchar](max) NULL,
  [IsActiveState] [bit] NOT NULL,
  [Retention_Period] [int] NOT NULL,
  [Kod_b] [int] NULL,
  [Dealer] [nvarchar](50) NULL,
  [DealerId] [int] NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
  [ID_User] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[UserInfo] ADD  CONSTRAINT [DF_Client_IsActiveState]  DEFAULT ((1)) FOR [IsActiveState]
GO
ALTER TABLE [dbo].[UserInfo] ADD  CONSTRAINT [DF_Client_Retention_Period]  DEFAULT ((2)) FOR [Retention_Period]
GO
ALTER TABLE [dbo].[UserInfo] ADD  CONSTRAINT [DF_UserInfo_Dealer]  DEFAULT (NULL) FOR [Dealer]
GO
ALTER TABLE [dbo].[UserInfo] ADD  DEFAULT ((0)) FOR [DealerId]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ���� � AVS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserInfo', @level2type=N'COLUMN',@level2name=N'Kod_b'
GO


/****** Object:  Table [dbo].[Roles]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
  [ID] [int] IDENTITY(1,1) NOT NULL,
  [Role] [nvarchar](50) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
  [ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[ExeptError]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExeptError](
  [ID_Error] [int] IDENTITY(1,1) NOT NULL,
  [IP_adress] [varchar](25) NULL,
  [ErrorMsg] [varchar](max) NULL,
  [Date] [smalldatetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ExeptError] ADD  CONSTRAINT [DF_ExeptError_Date]  DEFAULT (getdate()) FOR [Date]
GO


/****** Object:  Table [dbo].[TransferedData]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransferedData](
  [ID_Packet] [bigint] IDENTITY(1,1) NOT NULL,
  [IncomData] [varbinary](64) NOT NULL,
  [DTimeIncomData] [smalldatetime] NULL CONSTRAINT [DF_TransferedData_DTimeIncomData] DEFAULT (getdate()),
  CONSTRAINT [PK_TransferedData] PRIMARY KEY CLUSTERED ([ID_Packet] ASC) 
    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80)
    ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [NCI_Packet_Teletrack] ON [dbo].[TransferedData]([ID_Teletrack] ASC, [ID_Packet] ASC) 
  WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
    DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80)
  ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ���� (��).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransferedData', @level2type=N'COLUMN',@level2name=N'ID_Packet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ���������� �� �� (Bin, MultiBin, A1).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransferedData', @level2type=N'COLUMN',@level2name=N'IncomData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�� ������ ��������� ���������� ������� �����.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransferedData', @level2type=N'COLUMN',@level2name=N'ID_Teletrack'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������� ������ �� ��������� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransferedData', @level2type=N'COLUMN',@level2name=N'DTimeIncomData'
GO


/****** Object:  Table [dbo].[TeletrackInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TeletrackInfo](
  [ID_Teletrack] [int] IDENTITY(1,1) NOT NULL,
  [LoginTT] [char](4) NOT NULL,
  [PasswordTT] [varchar](10) NOT NULL,
  [Timeout] [int] NULL,
  [GSMIMEI] [varchar](20) NULL,
  [DTimeLastPack] [datetime] NULL,
  [ID_User] int NOT NULL,
 CONSTRAINT [PK_TeletrackInfo] PRIMARY KEY CLUSTERED 
(
  [ID_Teletrack] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LoginTT] ON [dbo].[TeletrackInfo](
  [LoginTT] ASC
)WITH (
  PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_IdUser] ON [dbo].[TeletrackInfo] (
  [ID_User] ASC
) WITH (
  PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TeletrackInfo] WITH CHECK ADD CONSTRAINT [FK_TeletrackInfo_UserInfo] 
  FOREIGN KEY ([ID_User]) REFERENCES [dbo].[UserInfo] ([ID_User])
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ���� (��).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeletrackInfo', @level2type=N'COLUMN',@level2name=N'ID_Teletrack'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������������ ����������(���). ������������ ����� ���������� ���� � �������� ������� 4 �����. ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeletrackInfo', @level2type=N'COLUMN',@level2name=N'LoginTT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ������������������ ����������(������) ������������ ������� 10 ����.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeletrackInfo', @level2type=N'COLUMN',@level2name=N'PasswordTT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������� ����� �������� ��������� �� �����.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeletrackInfo', @level2type=N'COLUMN',@level2name=N'Timeout'
GO


/****** Object:  Table [dbo].[ClientQuery]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientQuery](
  [ID_Query] [int] IDENTITY(1,1) NOT NULL,
  [ID_Client] [int] NOT NULL,
  [ID_Teletrack] [int] NOT NULL,
  [StartID_Packet] [bigint] NULL,
  [EndID_Packet] [bigint] NULL,
  [DateTimeExQuery] [smalldatetime] NULL,
 CONSTRAINT [PK_ClientQuery] PRIMARY KEY CLUSTERED 
(
  [ID_Query] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_IdTeletrack] ON [dbo].[ClientQuery] 
(
  [ID_Teletrack] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� 1. ID_Packet, ������� � �������� ������ ����������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClientQuery', @level2type=N'COLUMN',@level2name=N'StartID_Packet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� 2. ID_Packet, ���������� ������� ������ ����������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClientQuery', @level2type=N'COLUMN',@level2name=N'EndID_Packet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����, ����� ���������� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClientQuery', @level2type=N'COLUMN',@level2name=N'DateTimeExQuery'
GO


/****** Object:  Table [dbo].[ClientInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientInfo](
  [ID_Client] [int] IDENTITY(1,1) NOT NULL,
  [LoginCl] [nvarchar](50) NOT NULL,
  [Password] [nvarchar](50) NOT NULL,
  [InNetwork] [bit] NOT NULL,
  [ID_Packet] [bigint] NOT NULL,
  [DTimeDeliveredPack] [datetime] NOT NULL,
  [ID_LastPackInSrvDB] [bigint] NOT NULL,
  [ID_User] [int] NOT NULL,
  [Describe] [nvarchar](max) NULL,
 CONSTRAINT [PK_ClientInfo] PRIMARY KEY CLUSTERED 
(
  [ID_Client] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_IdUser] ON [dbo].[ClientInfo] 
(
  [ID_User] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LoginCl] ON [dbo].[ClientInfo] (
  [LoginCl]
) WITH (
  PAD_INDEX = OFF, FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClientInfo] ADD  CONSTRAINT [DF_ClientInfo_InNetwork]  DEFAULT ((1)) FOR [InNetwork]
GO
ALTER TABLE [dbo].[ClientInfo] ADD  CONSTRAINT [DF_ClientInfo_ID_Packet]  DEFAULT ((0)) FOR [ID_Packet]
GO
ALTER TABLE [dbo].[ClientInfo] ADD  CONSTRAINT [DF_ClientInfo_DTimeDeliveredPack]  DEFAULT (getdate()) FOR [DTimeDeliveredPack]
GO
ALTER TABLE [dbo].[ClientInfo] ADD  CONSTRAINT [DF_ClientInfo_UnDeliverData]  DEFAULT ((0)) FOR [ID_LastPackInSrvDB]
GO
ALTER TABLE [dbo].[ClientInfo] ADD  CONSTRAINT [DF_ClientInfo_ID_Dealer]  DEFAULT ((0)) FOR [ID_User]
GO
ALTER TABLE [dbo].[ClientInfo] ADD CONSTRAINT [FK_ClientInfo_UserInfo] 
  FOREIGN KEY ([ID_User]) REFERENCES [dbo].[UserInfo] ([ID_User])
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClientInfo', @level2type=N'COLUMN',@level2name=N'ID_Client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ��������� �� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClientInfo', @level2type=N'COLUMN',@level2name=N'InNetwork'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ���������� ����������� �������� ������ ������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClientInfo', @level2type=N'COLUMN',@level2name=N'ID_Packet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������� ���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClientInfo', @level2type=N'COLUMN',@level2name=N'DTimeDeliveredPack'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����(���-�� �������) �� ���������� �������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClientInfo', @level2type=N'COLUMN',@level2name=N'ID_LastPackInSrvDB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ������(ID_Client) � ������� ������ ������. � ������� �� = null' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClientInfo', @level2type=N'COLUMN',@level2name=N'ID_User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClientInfo', @level2type=N'COLUMN',@level2name=N'Describe'
GO


/****** Object:  Table [dbo].[DealerRole]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealerRole](
  [ID] [int] IDENTITY(1,1) NOT NULL,
  [DealerID] [int] NOT NULL,
  [RoleID] [int] NOT NULL,
 CONSTRAINT [PK_DealerRole] PRIMARY KEY CLUSTERED 
(
  [ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[DealerParams]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealerParams](
  [Id] [int] IDENTITY(1,1) NOT NULL,
  [ID_Dealer] [int] NOT NULL,
  [Number] [int] NOT NULL,
  [Value] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_DealerParams] PRIMARY KEY CLUSTERED 
(
  [Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[DealerInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealerInfo](
  [ID] [int] IDENTITY(1,1) NOT NULL,
  [DealerName] [nvarchar](50) NULL,
  [Password] [nvarchar](50) NULL,
  [Descr] [nvarchar](max) NULL,
 CONSTRAINT [PK_Dealer] PRIMARY KEY CLUSTERED 
(
  [ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[CommunicTable]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommunicTable](
  [ID_Communic] [int] IDENTITY(1,1) NOT NULL,
  [ID_Client] [int] NOT NULL,
  [ID_Teletrack] [int] NOT NULL,
 CONSTRAINT [PK_CommunicTable] PRIMARY KEY CLUSTERED 
(
  [ID_Communic] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_IdClient] ON [dbo].[CommunicTable] 
(
  [ID_Client] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_IdTeletrack] ON [dbo].[CommunicTable] 
(
  [ID_Teletrack] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[Command]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Command](
  [ID_Command] [int] IDENTITY(1,1) NOT NULL,
  [CommandText] [varbinary](max) NOT NULL,
  [CommandType] [varchar](5) NULL,
  [ExecuteState] [tinyint] NOT NULL,
  [DateQueueEntry] [datetime] NOT NULL,
  [DeltaTimeExCom] [datetime] NOT NULL,
  [ID_Client] [int] NOT NULL,
  [ID_Teletrack] [int] NOT NULL,
  [ID_ComInClDB] [int] NOT NULL,
  [AnswCode] [tinyint] NULL,
  [AnswData] [varbinary](160) NULL,
  [DispatcherIP] [varchar](40) NULL,
  [Approved] [bit] NOT NULL,
  CONSTRAINT [PK_Command_1] PRIMARY KEY CLUSTERED (
    [ID_Command] ASC) 
  WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_IdTeletrack] ON [dbo].[Command] (
  [ID_Teletrack] ASC)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80)
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DeltaTime_State_Approved] ON [dbo].[Command] (
  [DeltaTimeExCom], [ExecuteState], [Approved])
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80)
ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_IdClient] ON [dbo].[Command] (
  [ID_Client])
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80)
ON [PRIMARY]
GO
ALTER TABLE [dbo].[Command] ADD  CONSTRAINT [DF_Command_CommandType]  DEFAULT ('A1') FOR [CommandType]
GO
ALTER TABLE [dbo].[Command] ADD  CONSTRAINT [DF_Command_ExecuteState]  DEFAULT ((0)) FOR [ExecuteState]
GO
ALTER TABLE [dbo].[Command] ADD  CONSTRAINT [DF_Command_DateQueueEntry]  DEFAULT (getdate()) FOR [DateQueueEntry]
GO
ALTER TABLE [dbo].[Command] ADD  CONSTRAINT [DF_Command_DeltaTimeExCom]  DEFAULT (dateadd(day,(2),getdate())) FOR [DeltaTimeExCom]
GO
ALTER TABLE [dbo].[Command] ADD  CONSTRAINT [DF_Command_Approved]  DEFAULT ((0)) FOR [Approved]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ���� (��)', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Command', @level2type=N'COLUMN', @level2name=N'ID_Command'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� � ���� ������ �������� ������������ ������� 60', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Command', @level2type=N'COLUMN', @level2name=N'CommandText'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� (���������������).', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Command', @level2type=N'COLUMN', @level2name=N'CommandType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ��������� ������������ �������. ��������� ��������: 0 - ������� ���������, �� �������� ��; 1 - ������� �������� �� ��; 2 - ������� �� ���������. ', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Command', @level2type=N'COLUMN', @level2name=N'ExecuteState'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����/����� ����������� ������� �� �������.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Command', @level2type=N'COLUMN', @level2name=N'DateQueueEntry'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������� (�� ������� ����������� �������) � ������� �������� ������� �������� ����������.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Command', @level2type=N'COLUMN', @level2name=N'DeltaTimeExCom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ������� � ���� ������ �������.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Command', @level2type=N'COLUMN', @level2name=N'ID_ComInClDB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������ ��������� �� �������', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Command', @level2type=N'COLUMN', @level2name=N'AnswCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������� �� �������', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Command', @level2type=N'COLUMN', @level2name=N'AnswData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IP-����� ����������, ������������ �������.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Command', @level2type=N'COLUMN', @level2name=N'DispatcherIP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ���������� ���������� �������.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Command', @level2type=N'COLUMN', @level2name=N'Approved'
GO


/****** Object:  Table [dbo].[StatisticDataTT]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StatisticDataTT](
  [ID_Session] [int] IDENTITY(1,1) NOT NULL,
  [ID_Teletrack] [int] NOT NULL,
  [CapacityIn] [int] NULL,
  [CapacityOut] [int] NULL,
  [TimeCon] [datetime] NULL,
  [TimeDiscon] [datetime] NULL,
 CONSTRAINT [PK_StatisticDataTT] PRIMARY KEY CLUSTERED 
(
  [ID_Session] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_IdTeletrack] ON [dbo].[StatisticDataTT] 
(
  [ID_Teletrack] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TimeDiscon] ON [dbo].[StatisticDataTT] 
(
  [TimeDiscon] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StatisticDataTT] ADD  DEFAULT (getdate()) FOR [TimeCon]
GO
ALTER TABLE [dbo].[StatisticDataTT] ADD  DEFAULT (getdate()) FOR [TimeDiscon]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ������ �� ���������  � �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StatisticDataTT', @level2type=N'COLUMN',@level2name=N'CapacityIn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ������ �� ������� � ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StatisticDataTT', @level2type=N'COLUMN',@level2name=N'CapacityOut'
GO


/****** Object:  Table [dbo].[StatisticDataCl]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StatisticDataCl](
  [ID_SessionCl] [int] IDENTITY(1,1) NOT NULL,
  [ID_Client] [int] NULL,
  [CapacityIn] [int] NULL,
  [CapacityOut] [int] NULL,
  [TimeCon] [datetime] NULL,
  [TimeDiscon] [datetime] NULL,
 CONSTRAINT [PK_StatisticDataCl] PRIMARY KEY CLUSTERED 
(
  [ID_SessionCl] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_IdClient] ON [dbo].[StatisticDataCl] 
(
  [ID_Client] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TimeDiscon] ON [dbo].[StatisticDataCl] 
(
  [TimeDiscon] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StatisticDataCl] ADD  DEFAULT (getdate()) FOR [TimeCon]
GO
ALTER TABLE [dbo].[StatisticDataCl] ADD  DEFAULT (getdate()) FOR [TimeDiscon]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ������ �� ������� � �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StatisticDataCl', @level2type=N'COLUMN',@level2name=N'CapacityIn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ������ �� ������� � �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StatisticDataCl', @level2type=N'COLUMN',@level2name=N'CapacityOut'
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogUpdate] (
  ID tinyint IDENTITY(1,1) NOT NULL,
  VersionDB varchar(250) NOT NULL,
  ScriptName varchar(250) NOT NULL,
  Descr varchar(max) NULL,
  UpdateTime smalldatetime NOT NULL CONSTRAINT DF_LogUpdateScript_UpdateTime DEFAULT (getdate()),
  CONSTRAINT PK_UpdateScript PRIMARY KEY CLUSTERED ( ID ASC ) ON [PRIMARY],
  CONSTRAINT UQ_VersionDB_ScriptName UNIQUE NONCLUSTERED ( VersionDB, ScriptName) ON [PRIMARY]
) ON [PRIMARY]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Firmware](
  [Id] [bigint] IDENTITY(1,1) NOT NULL,
  [Description] [nvarchar](1000) NULL,
  [VersionFrom] [nvarchar](25) NOT NULL,
  [VersionTo] [nvarchar](25) NOT NULL,
  [IssueDate] [datetime] NOT NULL,
  [RegistryDate] [datetime] NOT NULL,
  [FileName] [nvarchar](255) NOT NULL,
  [Source] [text] NOT NULL,
  [Enabled] [bit] NOT NULL CONSTRAINT [DF_Firmware_Enabled]  DEFAULT ((0)),
  [VersionStamp] [timestamp] NOT NULL,
  CONSTRAINT [PK_Firmware] PRIMARY KEY CLUSTERED (
    [Id] ASC
  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_VersionFrom] ON [dbo].[Firmware] (
  [VersionFrom] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_VersionTo] ON [dbo].[Firmware] (
  [VersionTo] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������, ��� ������� ������������� ��� ���������. ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'VersionFrom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������ ���������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'VersionTo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������� ���������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'IssueDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� � ����������� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'FileName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� �������������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'Enabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ��������� ������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware', @level2type=N'COLUMN',@level2name=N'VersionStamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �������� ���������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Firmware'
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FirmwareChanges](
  [Id] [bigint] IDENTITY(1,1) NOT NULL,
  [FirmwareId] [bigint] NOT NULL,
  [Date] [datetime] NOT NULL,
  [Description] [nvarchar](1000) NOT NULL,
  [UserName] [nvarchar](255) NOT NULL,
  [UserIp] [nvarchar](50) NOT NULL,
  CONSTRAINT [PK_FirmwareChanges] PRIMARY KEY CLUSTERED (
    [Id] ASC
  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, 
    ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_FirmwareId] ON [dbo].[FirmwareChanges] (
  [FirmwareId] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Date] ON [dbo].[FirmwareChanges] (
  [Date] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges', @level2type=N'COLUMN',@level2name=N'FirmwareId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges', @level2type=N'COLUMN',@level2name=N'Date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' ��� ������������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ip ����� ������������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges', @level2type=N'COLUMN',@level2name=N'UserIp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������ � ��������� Firmware' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareChanges'
GO

ALTER TABLE [dbo].[FirmwareChanges] WITH CHECK ADD CONSTRAINT [FK_FirmwareChanges_Firmware]
FOREIGN KEY([FirmwareId]) REFERENCES [dbo].[Firmware] ([Id])
ON DELETE CASCADE
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FirmwareTask](
  [Id] [bigint] IDENTITY(1,1) NOT NULL,
  [Description] [nvarchar](1000) NULL,
  [TeletrackLogin] [nchar](4) NOT NULL,
  [FirmwareId] [bigint] NOT NULL,
  [CreatedDate] [datetime] NOT NULL,
  [Stage] [nvarchar](50) NOT NULL,
  [VersionStamp] [timestamp] NOT NULL,
  CONSTRAINT [PK_FirmwareTask] PRIMARY KEY CLUSTERED (
    [Id] ASC
  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, 
    ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
 
CREATE NONCLUSTERED INDEX [IX_FirmwareId] ON [dbo].[FirmwareTask] (
  [FirmwareId] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TeletrackLogin] ON [dbo].[FirmwareTask] (
  [TeletrackLogin] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_CreatedDate] ON [dbo].[FirmwareTask] (
  [CreatedDate] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ���������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask', @level2type=N'COLUMN',@level2name=N'TeletrackLogin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask', @level2type=N'COLUMN',@level2name=N'FirmwareId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� �������� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ������ ��������� ������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask', @level2type=N'COLUMN',@level2name=N'Stage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �������� ��������� �������� ����������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTask'
GO

ALTER TABLE [dbo].[FirmwareTask] WITH CHECK ADD CONSTRAINT [FK_FirmwareTask_Firmware]
FOREIGN KEY([FirmwareId]) REFERENCES [dbo].[Firmware] ([Id])
ON DELETE CASCADE
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FirmwareTaskProcessing](
  [Id] [bigint] IDENTITY(1,1) NOT NULL,
  [FirmwareTaskId] [bigint] NOT NULL,
  [Date] [datetime] NOT NULL,
  [Description] [nvarchar](1000) NULL,
  [Stage] [nvarchar](50) NOT NULL,
  [UserName] [nvarchar](255) NOT NULL,
  [UserIp] [nvarchar](50) NOT NULL,
  CONSTRAINT [PK_FirmwareTaskProcessing] PRIMARY KEY CLUSTERED (
    [Id] ASC
  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, 
    ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_FirmwareTaskId] ON [dbo].[FirmwareTaskProcessing] (
  [FirmwareTaskId] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Date] ON [dbo].[FirmwareTaskProcessing] (
  [Date] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Stage] ON [dbo].[FirmwareTaskProcessing] (
  [Stage] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
  DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'FirmwareTaskId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'Date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ���������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'Stage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ip ����� ������������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing', @level2type=N'COLUMN',@level2name=N'UserIp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ��������� ����� �������� ��������� ��������.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FirmwareTaskProcessing'
GO

ALTER TABLE [dbo].[FirmwareTaskProcessing] WITH CHECK ADD CONSTRAINT [FK_FirmwareTaskProcessing_FirmwareTask]
FOREIGN KEY([FirmwareTaskId]) REFERENCES [dbo].[FirmwareTask] ([Id])
ON DELETE CASCADE
GO


ALTER TABLE [dbo].[TransferedData] WITH CHECK ADD CONSTRAINT [FK_TransferedData_TeletrackInfo] 
  FOREIGN KEY ([ID_Teletrack]) REFERENCES [dbo].[TeletrackInfo] ([ID_Teletrack]) 
  ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_CommunicTable_Service]    Script Date: 08/11/2009 11:36:24 ******/
ALTER TABLE [dbo].[CommunicTable]  WITH CHECK ADD  CONSTRAINT [FK_CommunicTable_Service] FOREIGN KEY([ID_Client])
REFERENCES [dbo].[ClientInfo] ([ID_Client])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CommunicTable] CHECK CONSTRAINT [FK_CommunicTable_Service]
GO
/****** Object:  ForeignKey [FK_CommunicTable_TeletrackInfo1]    Script Date: 08/11/2009 11:36:24 ******/
ALTER TABLE [dbo].[CommunicTable]  WITH CHECK ADD  CONSTRAINT [FK_CommunicTable_TeletrackInfo1] FOREIGN KEY([ID_Teletrack])
REFERENCES [dbo].[TeletrackInfo] ([ID_Teletrack])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CommunicTable] CHECK CONSTRAINT [FK_CommunicTable_TeletrackInfo1]
GO
/****** Object:  ForeignKey [FK_Command_Service]    Script Date: 08/11/2009 11:36:24 ******/
ALTER TABLE [dbo].[Command]  WITH CHECK ADD  CONSTRAINT [FK_Command_Service] FOREIGN KEY([ID_Client])
REFERENCES [dbo].[ClientInfo] ([ID_Client])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Command] CHECK CONSTRAINT [FK_Command_Service]
GO
/****** Object:  ForeignKey [FK_Command_TeletrackInfo1]    Script Date: 08/11/2009 11:36:24 ******/
ALTER TABLE [dbo].[Command]  WITH CHECK ADD  CONSTRAINT [FK_Command_TeletrackInfo1] FOREIGN KEY([ID_Teletrack])
REFERENCES [dbo].[TeletrackInfo] ([ID_Teletrack])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Command] CHECK CONSTRAINT [FK_Command_TeletrackInfo1]
GO
/****** Object:  ForeignKey [FK_StatisticDataTT_TeletrackInfo]    Script Date: 08/11/2009 11:36:24 ******/
ALTER TABLE [dbo].[StatisticDataTT]  WITH CHECK ADD  CONSTRAINT [FK_StatisticDataTT_TeletrackInfo] FOREIGN KEY([ID_Teletrack])
REFERENCES [dbo].[TeletrackInfo] ([ID_Teletrack])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StatisticDataTT] CHECK CONSTRAINT [FK_StatisticDataTT_TeletrackInfo]
GO
/****** Object:  ForeignKey [FK_StatisticDataCl_Service]    Script Date: 08/11/2009 11:36:24 ******/
ALTER TABLE [dbo].[StatisticDataCl]  WITH CHECK ADD  CONSTRAINT [FK_StatisticDataCl_Service] FOREIGN KEY([ID_Client])
REFERENCES [dbo].[ClientInfo] ([ID_Client])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StatisticDataCl] CHECK CONSTRAINT [FK_StatisticDataCl_Service]
GO


/************************************************************/
/*                                                          */
/*                     3. PROCEDURES                        */
/*                                                          */
/************************************************************/

/****** Object:  StoredProcedure [dbo].[SqlQueryNotificationStoredProcedure-9d4bfaf7-ffb8-4bee-b089-b4dfe38e5ad1]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-9d4bfaf7-ffb8-4bee-b089-b4dfe38e5ad1] 
AS 
BEGIN 

BEGIN TRANSACTION; 

RECEIVE TOP(0) conversation_handle 
FROM [SqlQueryNotificationService-9d4bfaf7-ffb8-4bee-b089-b4dfe38e5ad1]; 

IF (SELECT COUNT(*) 
    FROM [SqlQueryNotificationService-9d4bfaf7-ffb8-4bee-b089-b4dfe38e5ad1] 
    WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 
BEGIN 
  DROP SERVICE [SqlQueryNotificationService-9d4bfaf7-ffb8-4bee-b089-b4dfe38e5ad1]; 
  DROP QUEUE [SqlQueryNotificationService-9d4bfaf7-ffb8-4bee-b089-b4dfe38e5ad1]; 
  DROP PROCEDURE [SqlQueryNotificationStoredProcedure-9d4bfaf7-ffb8-4bee-b089-b4dfe38e5ad1]; 
END 

COMMIT TRANSACTION; 

END
GO


/****** Object:  StoredProcedure [dbo].[MaintenanceUpdateStatistics]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	���������� ����������
-- =============================================
CREATE PROCEDURE [dbo].[MaintenanceUpdateStatistics]
AS
BEGIN
  UPDATE STATISTICS [dbo].[ClientInfo] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[ClientQuery] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[Command] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[CommunicTable] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[DealerInfo] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[DealerRole] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[ExeptError] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[Roles] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[StatisticDataCl] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[StatisticDataTT] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[TeletrackInfo] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[TransferedData] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[UserInfo] WITH FULLSCAN, NORECOMPUTE;
  UPDATE STATISTICS [dbo].[VersionControl] WITH FULLSCAN, NORECOMPUTE;
END
GO


/****** Object:  StoredProcedure [dbo].[MaintenanceRebuildIndex]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	<����������� ��������>
-- =============================================
CREATE PROCEDURE [dbo].[MaintenanceRebuildIndex]
AS
BEGIN

-- ������������� ������� sys.dm_db_index_physical_stats � �������� 
-- ��� ����������� ��� ������������� ��������
SET NOCOUNT ON;
DECLARE @objectid int;
DECLARE @indexid int;
DECLARE @partitioncount bigint;
DECLARE @schemaname nvarchar(130); 
DECLARE @objectname nvarchar(130); 
DECLARE @indexname nvarchar(130); 
DECLARE @partitionnum bigint;
DECLARE @partitions bigint;
DECLARE @frag float;
DECLARE @command nvarchar(4000);
DECLARE @indexType nvarchar(130);
DECLARE @engineEdition int; -- �������� ������ �������

-- Conditionally select tables and indexes from the sys.dm_db_index_physical_stats function 
-- and convert object and index IDs to names.
SELECT
  object_id AS objectid,
  index_id AS indexid,
  index_type_desc AS indexType,
  partition_number AS partitionnum,
  avg_fragmentation_in_percent AS frag
INTO #work_to_do
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, 'LIMITED')
WHERE avg_fragmentation_in_percent > 10.0 AND index_id > 0
ORDER BY object_id;

-- ���������� �������� ������ �������
SELECT @engineEdition = CAST(SERVERPROPERTY('EngineEdition') AS INT); 

-- Declare the cursor for the list of partitions to be processed.
DECLARE partitions CURSOR FOR SELECT * FROM #work_to_do;

-- Open the cursor.
OPEN partitions;

-- Loop through the partitions.
WHILE (1=1)
BEGIN
  FETCH NEXT FROM partitions
     INTO @objectid, @indexid, @indexType, @partitionnum, @frag;

  IF @@FETCH_STATUS < 0 BREAK;

  SELECT @objectname = QUOTENAME(o.name), @schemaname = QUOTENAME(s.name)
  FROM sys.objects AS o
    JOIN sys.schemas as s ON s.schema_id = o.schema_id
  WHERE o.object_id = @objectid;

  -- ��� ������� ��� �� ���������
  IF @objectname = N'[sysdiagrams]'
    CONTINUE;

  SELECT @indexname = QUOTENAME(name)
  FROM sys.indexes
  WHERE (object_id = @objectid) AND (index_id = @indexid);

  SELECT @partitioncount = count(*)
  FROM sys.partitions
  WHERE (object_id = @objectid) AND (index_id = @indexid);

  -- 30 is an arbitrary decision point at which to switch between reorganizing and rebuilding.
  IF (@frag < 30.0)
  BEGIN
    SET @command = N'ALTER INDEX ' + @indexname + N' ON ' +  @schemaname + N'.' + @objectname + N' REORGANIZE';
  END ELSE
  BEGIN
    -- ONLINE = ON �������� ������ � �������� ������ ������� = 3 (Enterprise, Enterprise Evaluation, Developer).
    -- ���������� ������� ������ ������������� � ������ ONLINE = ON, ���� � ������� ����������� 
    -- ���� ��������� �����: text, ntext, image, varchar(max), nvarchar(max), varbinary(max), xml. 
    IF (@engineEdition != 3) OR ((@indexType = N'CLUSTERED INDEX') AND 
       ((@objectname = N'[ClientInfo]') OR (@objectname = N'[Command]') OR (@objectname = N'[DealerInfo]') OR 
       (@objectname = N'[ExeptError]') OR (@objectname = N'[UserInfo]')))
    BEGIN   
      SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + 
        N' REBUILD WITH(FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = ON, ONLINE = OFF)';
    END ELSE
    BEGIN
      SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + 
        N' REBUILD WITH(FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = ON, ONLINE = ON)'; 
    END
  END

  IF (@partitioncount > 1)
    SET @command = @command + N' PARTITION=' + CAST(@partitionnum AS nvarchar(10));
   
  -- PRINT N'Executed: ' + @command;
  EXEC (@command);
END

-- Close and deallocate the cursor.
CLOSE partitions;
DEALLOCATE partitions;

-- Drop the temporary table.
DROP TABLE #work_to_do; 

END
GO

/****** Object:  StoredProcedure [dbo].[adm_DeleteUserInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_DeleteUserInfo]
  @Original_ID_User int,
  @Original_IsActiveState bit,
  @Original_Retention_Period int
AS
SET NOCOUNT OFF;
DELETE FROM [UserInfo]
WHERE (([ID_User] = @Original_ID_User) AND ([IsActiveState] = @Original_IsActiveState) AND ([Retention_Period] = @Original_Retention_Period))
--exec incrementOfVersion 'UserInfo'
GO


/****** Object:  StoredProcedure [dbo].[adm_DeleteTeletrackInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_DeleteTeletrackInfo]
  @Original_ID_Teletrack int
AS
SET NOCOUNT OFF;

DELETE FROM [TeletrackInfo] 
WHERE (([ID_Teletrack] = @Original_ID_Teletrack))
  --exec incrementOfVersion 'TeletrackInfo'
GO


/****** Object:  StoredProcedure [dbo].[adm_SelectClientInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_SelectClientInfo]
AS
SET NOCOUNT ON;

SELECT ID_Client, LoginCl, Password, InNetwork, ID_Packet, DTimeDeliveredPack, ID_LastPackInSrvDB, ID_User, Describe
FROM ClientInfo
GO


/****** Object:  StoredProcedure [dbo].[incrementOfVersion]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[incrementOfVersion]
  @tableName nchar(30)
AS
DECLARE @ver int
SET @ver = 0

SELECT @ver = Version 
FROM VersionControl 
WHERE TableName = @tableName

UPDATE VersionControl 
SET Version = @ver + 1 
WHERE TableName = @tableName

GO

/****** Object:  StoredProcedure [dbo].[FindRoles]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--������ ��������� 'FindRoles'
CREATE PROCEDURE [dbo].[FindRoles]
  @DealerName nvarchar (50)
AS
SET NOCOUNT ON

SELECT R.Role 
FROM dbo.DealerInfo AS U, dbo.Roles AS R, dbo.DealerRole AS UR
WHERE (U.DealerName = @DealerName) AND 
  (UR.DealerID = U.ID) AND (UR.RoleID = R.ID)

GO


/****** Object:  StoredProcedure [dbo].[FindDealer]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--������ ��������� 'FindDealer'
CREATE PROCEDURE [dbo].[FindDealer]
  @DealerName nvarchar(50),
  @Pass nvarchar(50)
AS
SET NOCOUNT ON

-- case sensitive search
SELECT TOP(1) ID
FROM dbo.DealerInfo
WHERE (CAST(DealerName AS varbinary(max)) = CAST(@DealerName AS varbinary(max))) AND 
  (CAST([Password] AS varbinary(max)) = CAST(@Pass AS varbinary(max)))

GO


/****** Object:  StoredProcedure [dbo].[selectClientInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	�������� ��� ������ �� ������� Client
-- =============================================
CREATE PROCEDURE [dbo].[selectClientInfo] 
AS
SELECT * FROM dbo.ClientInfo

GO


/****** Object:  StoredProcedure [dbo].[Select_TT]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		�.�����
-- Create date: 07.08.2009
-- Description:	����� ������ ����������
-- =============================================
CREATE PROCEDURE [dbo].[Select_TT] 
AS
SET NOCOUNT ON

SELECT ID_Teletrack, LoginTT
FROM TeletrackInfo
ORDER BY LoginTT

GO


/****** Object:  StoredProcedure [dbo].[Select_Dealers_Clients]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		�.�����
-- Create date: 06.08.2009
-- Description:	������� ������ 
-- =============================================
CREATE PROCEDURE [dbo].[Select_Dealers_Clients]
  @DL int=0
AS
SET NOCOUNT ON

SELECT ID_User, Name
FROM UserInfo
WHERE (DealerId = @DL) OR (@DL = 0)
ORDER BY Name 

GO


/****** Object:  StoredProcedure [dbo].[Select_Dealers]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		����� �.�
-- Create date: 31.07.2009
-- Description:	������ ������� ��� �������
-- =============================================
CREATE PROCEDURE [dbo].[Select_Dealers] 
AS
SET NOCOUNT ON

SELECT ID, DealerName
FROM  DealerInfo
ORDER BY DealerName

GO


/****** Object:  StoredProcedure [dbo].[Select_Dealer_By_Name]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		�.�����
-- Create date: 10.08.2009
-- Description:	����� �� �����  - ��� ������ � AuthenticationModule
-- =============================================
CREATE PROCEDURE [dbo].[Select_Dealer_By_Name]
  @Name nvarchar(30)
AS
SET NOCOUNT ON

SELECT ID
FROM DealerInfo
WHERE (DealerName = @Name)

GO


/****** Object:  StoredProcedure [dbo].[Select_Clients_Dealer_Name]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		�.�����
-- Create date: 11.08.2009
-- Description:	��� ������ �� ���� ������� (����������)
-- =============================================
CREATE PROCEDURE [dbo].[Select_Clients_Dealer_Name]
  @UserID int
AS
SET NOCOUNT ON

SELECT DealerInfo.DealerName
FROM UserInfo INNER JOIN DealerInfo ON UserInfo.DealerId = DealerInfo.ID
WHERE (UserInfo.ID_User = @UserID)

GO


/****** Object:  StoredProcedure [dbo].[Select_Clients]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		�.�����
-- Create date: 06.08.2009
-- Description:	������ �������� 
-- =============================================
CREATE PROCEDURE [dbo].[Select_Clients] 
AS
SET NOCOUNT ON

SELECT ID_User, Name
FROM  UserInfo
ORDER BY Name 

GO


/****** Object:  StoredProcedure [dbo].[ParamSet]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		�.�����
-- Create date: 07.08.2009
-- Description:	���������� ��������� ������
-- =============================================
CREATE PROCEDURE [dbo].[ParamSet] 
  @ID int,
  @VAL nvarchar(255)
AS
SET NOCOUNT ON

UPDATE DealerParams
SET Value = @VAL
WHERE (Id  = @ID)

GO


/****** Object:  StoredProcedure [dbo].[ParamInSet]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		�.�����
-- Create date: 07.08.2009
-- Description:	���������� ��������� ������
-- =============================================
CREATE PROCEDURE [dbo].[ParamInSet] 
  @DL int,
  @NUM int,
  @VAL nvarchar(255)
AS
SET NOCOUNT ON

INSERT INTO DealerParams (ID_Dealer,Value, Number)
VALUES (@DL,@VAL,@NUM)

GO


/****** Object:  StoredProcedure [dbo].[ParamGetId]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		�.�����
-- Create date: 10.08.2009
-- Description:	�� ���������
-- =============================================
CREATE PROCEDURE [dbo].[ParamGetId] 
  @DL int,
  @NUM int
AS
SET NOCOUNT ON

SELECT ID
FROM DealerParams
WHERE (Number = @NUM) AND (ID_Dealer=@DL)

GO


/****** Object:  StoredProcedure [dbo].[ParamGet]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		�.�����
-- Create date: 07.08.2009
-- Description:	������ �� ���� ��������� ������
-- =============================================
CREATE PROCEDURE [dbo].[ParamGet] 
  @DL int,
  @NUM int
AS
SET NOCOUNT ON

SELECT Value
FROM  DealerParams
WHERE (Number = @NUM) AND (ID_Dealer=@DL)

GO


/****** Object:  StoredProcedure [dbo].[adm_SelectUserInfoNew]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	���������� ���� ������������� ��� �����������
-- =============================================
CREATE PROCEDURE [dbo].[adm_SelectUserInfoNew]
  @id_user int
AS
SET NOCOUNT ON

if (@id_user <= 0) 
begin
  SET @id_user = null
end

SELECT u.id_user, u.Kod_b as �������, u.Name as ������������,  
  case (u.IsActiveState)
    when -1 then '������������'
    when 0 then '������������'
    when 1 then '�������'
  end as ���������,
  u.Retention_Period as T_��������,
  u.Describe as ��������
FROM UserInfo AS u
WHERE  @id_user is null or (u.ID_User = @id_user)
ORDER BY id_user

GO


/****** Object:  StoredProcedure [dbo].[adm_SelectUserInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_SelectUserInfo]
  @id_user int
AS
SET NOCOUNT ON

if (@id_user <= 0) 
begin 
  SET @id_user = null
end

SELECT u.id_user, u.Kod_b, u.Name, u.IsActiveState,  
/*case (u.IsActiveState)
    when -1 then '������������'
    when 0 then '������������'
    when 1 then '�������'
    end as IsActiveState,*/

  u.Retention_Period,
  u.Describe,
  d.DealerName,d.DealerName as Dealer
FROM UserInfo AS u LEFT OUTER JOIN DealerInfo as d ON u.DealerId = d.ID
WHERE  @id_user is null or (u.ID_User = @id_user)
ORDER BY id_user DESC

SELECT DealerInfo.DealerName
FROM UserInfo LEFT OUTER JOIN DealerInfo ON UserInfo.DealerId = DealerInfo.ID

GO


/****** Object:  StoredProcedure [dbo].[adm_SelectTeletrackInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_SelectTeletrackInfo]
AS
SET NOCOUNT ON

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout, GSMIMEI, DTimeLastPack, ID_User
FROM TeletrackInfo

GO


/****** Object:  StoredProcedure [dbo].[st_SelectClientInfoForUs]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		edubr
-- Create date: 19.03.2009
-- Description:	������� ���� �����������(�������) ������� ������������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectClientInfoForUs]
  @id_dealer int,
  @id_user int
AS
SET NOCOUNT ON

DECLARE @dd datetime
SET @dd = getdate()
DECLARE @str char(5)
SET @str = ' ��. '

SELECT cl.ID_Client, cl.LoginCl, cl.Password, 
  CASE (cl.InNetwork)
    WHEN -1 THEN '�� � ����'
    WHEN 0 THEN '�� � ����'
    WHEN 1 THEN '� ����'
  END AS NetState,

  cl.DTimeDeliveredPack, 

  CONVERT(varchar, Datediff(dd, cl.DTimeDeliveredPack, GETDATE()), 2) + @str +
  CONVERT(varchar(8), (GETDATE() - cl.DTimeDeliveredPack), 114) AS DeltaT,

  CASE (SIGN ( cl.ID_Packet - cl.ID_LastPackInSrvDB))
    WHEN -1 THEN '����'
    WHEN 0 THEN '���'
    WHEN 1 THEN '���'
  END AS NewDataDescr,

    b.Name, cl.Describe
FROM UserInfo AS b INNER JOIN ClientInfo AS cl ON cl.ID_User = b.ID_User
WHERE (b.DealerId = @id_dealer) AND (cl.ID_User = @id_user)
ORDER BY b.Name

GO


/****** Object:  StoredProcedure [dbo].[st_SelectClientInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		edubr
-- Create date: 19.03.2009
-- Description:	������� ���� �����������(�������) ������� ������������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectClientInfo]
  @id_user int
AS
SET NOCOUNT ON

if (@id_user <= 0) 
begin 
  SET @id_user = null
end

DECLARE @dd datetime
SET @dd = getdate()
DECLARE @str varchar(5)
SET @str = ' ��. '

SELECT  cl.ID_Client, cl.LoginCl as �����, cl.Password as ������, 
  case (cl.InNetwork)
    when -1 then '�� � ����'
    when 0 then '�� � ����'
    when 1 then '� ����'
  end as ���������,

  cl.DTimeDeliveredPack as ���������_������, 

  CONVERT(varchar, Datediff(dd, cl.DTimeDeliveredPack, GETDATE()),2)+ @str  +
    CONVERT(varchar(8),	(GETDATE() - cl.DTimeDeliveredPack), 114) as ������,

  case (SIGN ( cl.ID_Packet - cl.ID_LastPackInSrvDB))
    when -1 then '����'
    when 0 then '���'
    when 1 then '���'
  end as �����_������,

  b.Name as ������������, cl.Describe as ��������
FROM ClientInfo AS cl INNER JOIN UserInfo AS b ON cl.ID_User = b.ID_User
WHERE  @id_user is null or (cl.ID_User = @id_user)
ORDER BY b.Name

GO


/****** Object:  StoredProcedure [dbo].[st_SelectAllUserInfoForMonit]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	���������� ���� ������������� ��� �����������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectAllUserInfoForMonit]

AS
SELECT u.id_user, u.Kod_b as �������, u.Name as ������������,  
  case (u.IsActiveState)
      when -1 then '������������'
      when 0 then '������������'
      when 1 then '�������'
  end as ���������,

  u.Retention_Period as T_��������,
  u.Describe as ��������
FROM UserInfo AS u
ORDER BY id_user

GO


/****** Object:  StoredProcedure [dbo].[selectVersionControl]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selectVersionControl]
AS
--SET NOCOUNT ON;
SELECT ID, TableName, Version FROM dbo.VersionControl
--ORDER BY ID ASC
GO


/****** Object:  StoredProcedure [dbo].[selectUserInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	�������� ��� ������ �� ������� Service
-- =============================================
create PROCEDURE [dbo].[selectUserInfo] 
AS
SELECT * FROM dbo.UserInfo

GO


/****** Object:  StoredProcedure [dbo].[selectTransferedData]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectTransferedData]
AS
SELECT TOP(1) ID_Packet
FROM [dbo].[TransferedData]
ORDER BY [ID_Packet] DESC

GO


/****** Object:  StoredProcedure [dbo].[selectTeletrackInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectTeletrackInfo] 

AS
SET NOCOUNT ON

SELECT * FROM TeletrackInfo

GO


/****** Object:  StoredProcedure [dbo].[st_SelectAllClientInfoForMonit]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	���������� ��� ������� ��� �����������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectAllClientInfoForMonit]

AS
DECLARE @dd datetime
SET @dd = getdate()
DECLARE @str char(5)
SET @str = ' ��. '

SELECT  cl.ID_Client  AS �����, cl.LoginCl  AS �����, cl.Password  AS ������, 
  case (cl.InNetwork)
    when -1 then '�� � ����'
    when 0 then '�� � ����'
    when 1 then '� ����'
  end as ���������,

  cl.DTimeDeliveredPack as ���������_������, 

  CONVERT(varchar, Datediff(dd, cl.DTimeDeliveredPack, GETDATE()), 2)+ @str  +
  CONVERT(varchar(8), (GETDATE() - cl.DTimeDeliveredPack), 114)  AS ������,

  case (SIGN ( cl.ID_Packet - cl.ID_LastPackInSrvDB))
    when -1 then '����'
    when 0 then '���'
    when 1 then '���'
  end  AS �����_������,

  b.Name  AS ������������, cl.Describe  AS ��������
FROM ClientInfo AS cl INNER JOIN UserInfo AS b ON cl.ID_User = b.ID_User
ORDER BY b.Name

GO


/****** Object:  StoredProcedure [dbo].[SqlQueryNotificationStoredProcedure-c8e58c14-89c0-41ba-8650-0e60c44ad4cb]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-c8e58c14-89c0-41ba-8650-0e60c44ad4cb] 
AS 
BEGIN 

BEGIN TRANSACTION; 
RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-c8e58c14-89c0-41ba-8650-0e60c44ad4cb]; 

IF (SELECT COUNT(*) 
FROM [SqlQueryNotificationService-c8e58c14-89c0-41ba-8650-0e60c44ad4cb] 
WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 
BEGIN 
  DROP SERVICE [SqlQueryNotificationService-c8e58c14-89c0-41ba-8650-0e60c44ad4cb]; 
  DROP QUEUE [SqlQueryNotificationService-c8e58c14-89c0-41ba-8650-0e60c44ad4cb]; 
  DROP PROCEDURE [SqlQueryNotificationStoredProcedure-c8e58c14-89c0-41ba-8650-0e60c44ad4cb]; 
END 
COMMIT TRANSACTION; 

END
GO


/****** Object:  StoredProcedure [dbo].[st_SelectTeletrackIdByLogin]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		����� �.�.
-- Create date: 24,07,2009
-- Description:	������� �� ��������� �� ������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectTeletrackIdByLogin] 
  @Login nvarchar(20)
AS
SET NOCOUNT ON

SELECT ID_Teletrack
FROM dbo.TeletrackInfo
WHERE (LoginTT = @Login)

GO


/****** Object:  StoredProcedure [dbo].[updateCurDataInTeletrackInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateCurDataInTeletrackInfo] 
  @ID_Teletrack int,
  -- @InNetwork bit,
  @DTimeLastPack datetime
AS
BEGIN

UPDATE dbo.TeletrackInfo 
SET DTimeLastPack = @DTimeLastPack
WHERE ID_Teletrack = @ID_Teletrack

SELECT * FROM TeletrackInfo

END
GO


/****** Object:  StoredProcedure [dbo].[updateCurDataInClientInfo]    Script Date: 08/11/2009 11:36:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateCurDataInClientInfo] 
  @ID_Client int,
  @InNetwork bit,
  @ID_Packet bigint,
  @DTimeDeliveredPack smalldatetime,
  @ID_LastPackInSrvDB bigint
AS
UPDATE dbo.ClientInfo 
SET InNetwork = @InNetwork, ID_Packet = @ID_Packet, DTimeDeliveredPack = @DTimeDeliveredPack, 
  ID_LastPackInSrvDB = @ID_LastPackInSrvDB
WHERE ID_Client = @ID_Client

GO

/****** Object:  StoredProcedure [dbo].[updateStatisticDataTT]    Script Date: 08/11/2009 11:36:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateStatisticDataTT]
  @id_session int, 
  @capasityIn int,
  @capasityOut int,
  @timeDisconn datetime
AS  
-- ���������� ������ ������ � StatisticDataTT 
UPDATE dbo.StatisticDataTT 
SET CapacityIn = @capasityIn, CapacityOut = @capasityOut, TimeDiscon = @timeDisconn 
WHERE ID_Session = @id_session

GO


/****** Object:  StoredProcedure [dbo].[updateStatisticDataCl]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateStatisticDataCl]
  @id_session int, 
  @capasityIn int,
  @capasityOut int,
  @timeDisconn datetime
AS  
-- ���������� ������ ������ � StatisticDataTT 
UPDATE dbo.StatisticDataCl 
SET CapacityIn = @capasityIn, CapacityOut = @capasityOut, TimeDiscon = @timeDisconn 
WHERE ID_SessionCl = @id_session

GO


/****** Object:  StoredProcedure [dbo].[updateCommandTabl]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[updateCommandTabl] 
  @ExecuteState tinyint,
  @AnswCode tinyint,
  @AnswData varbinary(160),
  @Original_ID_Command int
AS
UPDATE dbo.Command
SET ExecuteState = @ExecuteState, AnswCode = @AnswCode, AnswData = @AnswData
WHERE ID_Command = @Original_ID_Command

GO


/****** Object:  StoredProcedure [dbo].[st_SelectTeletrack]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		edubr
-- Create date: 19.03.2009
-- Description:	���������� ��� ���������, ������������� ������� ������������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectTeletrack]
  -- Add the parameters for the stored procedure here
  @id_user int
AS
SET NOCOUNT ON

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout
FROM dbo.TeletrackInfo 
WHERE ID_User = @id_user

GO


/****** Object:  StoredProcedure [dbo].[st_SelectStatTotalTT]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		����� �.�.
-- Create date: 07.08.2009
-- Description:	����� ���������� �� ���������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectStatTotalTT] 
  @Y int, 
  @M int,
  @TLTR int = 0
AS
SET NOCOUNT ON

SELECT  ID_Teletrack, DAY(TimeCon) AS D, SUM(CapacityIn)/1024 AS Sum_kb, 
  SUM(CapacityOut) AS Sum_kb_out, COUNT(ID_Session) AS Cnt_Session,'���' AS Act
FROM dbo.StatisticDataTT
WHERE (MONTH(TimeCon) = @M) AND (YEAR(TimeCon) = @Y)
GROUP BY DAY(TimeCon), ID_Teletrack
HAVING (ID_Teletrack = @TLTR)
ORDER BY D

GO


/****** Object:  StoredProcedure [dbo].[st_SelectDateLast_StatisticDataTT]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		����� �.�.
-- Create date: 27.07.2009
-- Description:	����� ���������� ���������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectDateLast_StatisticDataTT]
AS
SET NOCOUNT ON

SELECT TOP(1) TimeCon AS D
FROM dbo.StatisticDataTT
ORDER BY ID_Session DESC

GO


/****** Object:  StoredProcedure [dbo].[selectIDFromStatisticDataTT]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	�������� ����� ��������� ������ � ������� StatisticDataTT
-- =============================================
CREATE PROCEDURE [dbo].[selectIDFromStatisticDataTT]
AS
SELECT TOP(1) ID_Session
FROM dbo.StatisticDataTT
ORDER BY ID_Session DESC

GO


/****** Object:  StoredProcedure [dbo].[selectIDFromStatisticDataCl]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	�������� ����� ��������� ������ � ������� StatisticDataCl
-- =============================================
CREATE PROCEDURE [dbo].[selectIDFromStatisticDataCl]
AS
SELECT TOP(1) ID_SessionCl
FROM dbo.StatisticDataCl
ORDER BY ID_SessionCl DESC

GO


/****** Object:  StoredProcedure [dbo].[selectForSendQuery]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- ���������� ��������� 1000 ����������� ������� �� ������� �������
-- =============================================
CREATE PROCEDURE [dbo].[selectForSendQuery]
  @id_client int,
  @loginTT char(4),
  @param1 bigint,
  @param2 bigint

AS
DECLARE @id_teletreck int

SELECT  @id_teletreck = COALESCE(ct.ID_Teletrack, -1) 
FROM dbo.TeletrackInfo info INNER JOIN dbo.CommunicTable ct
  ON info.ID_Teletrack = ct.ID_Teletrack
WHERE (ct.ID_Client = @id_client) AND (info.LoginTT = @loginTT)

IF @id_teletreck >= 0
  SELECT TOP(1000) ID_Packet, IncomData 
  FROM dbo.TransferedData WITH(INDEX(NCI_Packet_Teletrack))
  WHERE (ID_Teletrack = @id_teletreck) AND
    (ID_Packet > @param1) AND (ID_Packet <= @param2)
  ORDER BY ID_Packet DESC
ELSE
  INSERT INTO dbo.ExeptError (ErrorMsg) 
  VALUES ('������ ������� ����������� ������. ����������� �������� ' + @loginTT + ' � �� �������.' )

GO


/****** Object:  StoredProcedure [dbo].[selectCommunicTable]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectCommunicTable] 
AS
SELECT * FROM dbo.CommunicTable

GO


/****** Object:  StoredProcedure [dbo].[selectCommand]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selectCommand]
AS
SELECT * 
FROM dbo.Command
WHERE (DeltaTimeExCom > getdate()) AND (ExecuteState < 2) AND (Approved = 1) 
ORDER BY DateQueueEntry

GO


/****** Object:  StoredProcedure [dbo].[st_SelectAllTeletrackForMonit]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		edubr
-- Create date: 19.03.2009
-- Description:	���������� ��� ���������, ������������� ������� ������������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectAllTeletrackForMonit]
  -- Add the parameters for the stored procedure here
AS
DECLARE @str varchar(5)
SET @str = ' ��. '

SELECT DISTINCT tt.ID_Teletrack, tt.LoginTT, tt.PasswordTT, 
  COALESCE(CONVERT(varchar, st.TimeCon, 121), '��� ������') AS LastConn, 

  CASE (COALESCE(CONVERT(varchar, Datediff(dd, st.TimeCon, GETDATE()), 2), '��� ������'))
    WHEN '��� ������' THEN '��� ������'
    ELSE COALESCE(CONVERT(varchar, Datediff(dd, st.TimeCon, GETDATE()), 2), '��� ������') + 
      @str + CONVERT(varchar(8), (GETDATE() - st.TimeCon), 114)
  END AS Delta,

  CASE (COALESCE(CONVERT(varchar, (SIGN(tt.Timeout - CONVERT(int, Datediff(mi, st.TimeCon, GETDATE()), 2)))), '��� ������'))
    WHEN '��� ������' THEN '��� ������'
    WHEN -1 THEN 'CRITICAL'
    WHEN 0 THEN 'OK'
    WHEN 1 THEN 'OK'
  END AS State,

 tt.Timeout, u.Name

FROM TeletrackInfo AS tt 
  INNER JOIN UserInfo AS u ON tt.ID_User = u.ID_User 
  LEFT JOIN StatisticDataTT AS st ON tt.ID_Teletrack = st.ID_Teletrack 
    AND (st.TimeCon IS NULL) OR (st.Id_Session = (
      SELECT MAX(ins.Id_Session) AS MaxSesId 
      FROM StatisticDataTT ins 
      WHERE ins.ID_Teletrack = tt.ID_Teletrack))
      ORDER BY u.Name
GO


/****** Object:  StoredProcedure [dbo].[st_SelectStatInfoTTForMonit]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectStatInfoTTForMonit] 
  @id_teletrack int
AS
SET NOCOUNT ON

SELECT tt.LoginTT AS �����, st.CapacityOut AS ��������, st.CapacityIn AS ����������, 
  st.TimeCon AS �����_������, st.TimeDiscon AS �����_��������
FROM dbo.StatisticDataTT AS st INNER JOIN dbo.TeletrackInfo AS tt ON st.ID_Teletrack = tt.ID_Teletrack 
WHERE (st.id_teletrack = @id_teletrack)
ORDER BY st.TimeCon DESC

GO


CREATE PROCEDURE [dbo].[st_SelectStatInfoTTForMonitForUs] 
  @id_dealer int,
  @id_teletrack int
AS
SET NOCOUNT ON

SELECT tt.LoginTT, st.CapacityOut, st.CapacityIn, st.TimeCon, st.TimeDiscon
FROM dbo.UserInfo AS u 
  INNER JOIN dbo.TeletrackInfo AS tt ON u.ID_User = tt.ID_User
  INNER JOIN dbo.StatisticDataTT AS st ON st.ID_Teletrack = tt.ID_Teletrack 
WHERE (u.DealerId = @id_dealer) AND (st.id_teletrack = @id_teletrack)
ORDER BY st.TimeCon DESC

GO


/****** Object:  StoredProcedure [dbo].[st_SelectStatInfoClForMonitForUs]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	���������� ���������� ����������� ��������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectStatInfoClForMonitForUs]
  @id_dealer int,
  @id_client int
AS
SET NOCOUNT ON

SELECT cl.LoginCl, st.CapacityOut, st.CapacityIn, 
  st.TimeCon, st.TimeDiscon, us.Name
FROM UserInfo AS us
  INNER JOIN dbo.ClientInfo AS cl ON cl.ID_User = us.ID_User
  INNER JOIN dbo.StatisticDataCl AS st ON st.ID_Client = cl.ID_Client 
WHERE (us.DealerId = @id_dealer) AND (st.id_client = @id_client)
ORDER BY st.TimeCon DESC

GO


/****** Object:  StoredProcedure [dbo].[st_SelectStatInfoClForMonit]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectStatInfoClForMonit]
  @id_client int
AS
SELECT cl.LoginCl AS �����, st.CapacityOut AS ��������, st.CapacityIn AS ����������, 
  st.TimeCon AS �����_������, st.TimeDiscon AS �����_��������, us.Name AS ������������
FROM dbo.StatisticDataCl AS st 
  INNER JOIN dbo.ClientInfo AS cl ON st.ID_Client = cl.ID_Client 
  INNER JOIN UserInfo AS us ON cl.ID_User = us.ID_User
WHERE @id_client IS NULL OR (st.id_client = @id_client)
ORDER BY st.TimeCon DESC

GO


/****** Object:  StoredProcedure [dbo].[insRecInStatisticDataTT]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insRecInStatisticDataTT] 
  @id_teletrack int,
  @capasityIn int,
  @capasityOut int,
  @timeConn datetime,
  @timeDisconn datetime,
  @id_session int output

AS
INSERT INTO StatisticDataTT (ID_Teletrack, CapacityIn, CapacityOut, TimeCon, TimeDiscon)
VALUES (@id_teletrack, @capasityIn, @capasityOut, @timeConn, @timeDisconn);

SELECT @id_session = SCOPE_IDENTITY()

GO


/****** Object:  StoredProcedure [dbo].[insRecInStatisticDataCl]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[insRecInStatisticDataCl] 
  @id_client int,
  @capasityIn int,
  @capasityOut int,
  @timeConn datetime,
  @timeDisconn datetime,
  @id_session int output

AS
INSERT INTO StatisticDataCl (ID_Client, CapacityIn, CapacityOut, TimeCon,  TimeDiscon)
VALUES (@id_client, @capasityIn, @capasityOut, @timeConn, @timeDisconn);

SELECT @id_session = SCOPE_IDENTITY()

GO


/****** Object:  StoredProcedure [dbo].[insert_command]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================
-- ��������� ������ ����� ������� � �� �������
-- ==========================================================
CREATE PROCEDURE [dbo].[insert_command]
  @loginTT char(4), -- ���(�����) ���������
  @id_teletreck int output, -- ID ��������� � �� �������
  @id_client int, -- ID ������� � �� �������
  @id_cmdClDB int, -- ID ������� � �� �������
  @cmdtext binary(160), -- �������
  @id_cmd int output, -- ID ����������� �������
  @checkInTime datetime, -- ����� ������� ������� �� ����������
  @expirationTime datetime, -- ����� ��������� �������� ������� 
  @dispatcher_ip varchar(40) -- IP-����� ����������
AS
SET NOCOUNT ON

SELECT @id_teletreck = ID_Teletrack 
FROM dbo.TeletrackInfo 
WHERE LoginTT = @loginTT

IF @id_teletreck > 0
BEGIN
  INSERT INTO dbo.Command (CommandText, DateQueueEntry, DeltaTimeExCom, ID_Client, 
    ID_Teletrack, ID_ComInClDB, DispatcherIP, Approved) 
  VALUES (CONVERT(varbinary(max), @cmdtext), @checkInTime, @expirationTime, @id_client, 
    @id_teletreck, @id_cmdClDB, @dispatcher_ip, 0)
  --���������� id_comand
  SELECT @id_cmd = SCOPE_IDENTITY();
END ELSE
BEGIN 
  INSERT INTO dbo.ExeptError (ErrorMsg) 
  VALUES ('������ ���������� ������� A1. ����������� ' + @loginTT + ' � �� ��� ������� � id=' + @id_client )
END

GO


/****** Object:  StoredProcedure [dbo].[adm_SelectCommunicTable]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_SelectCommunicTable]
AS
SET NOCOUNT ON

SELECT * FROM CommunicTable

GO


/****** Object:  StoredProcedure [dbo].[endOfSessionTT]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[endOfSessionTT]
  @id_session int, 
  @capasityIn int,
  @capasityOut int,
  @timeDisconnect datetime
AS  
-- ���������� ������ ������ � StatisticDataTT 
UPDATE StatisticDataTT 
SET CapacityIn = @capasityIn, CapacityOut = @capasityOut, TimeDiscon =  @timeDisconnect
WHERE ID_Session = @id_session

GO


/****** Object:  StoredProcedure [dbo].[endOfSessionCl]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[endOfSessionCl]
  @id_session int, 
  @capasityIn int,
  @capasityOut int,
  @timeDisconnect datetime
AS  
-- ���������� ������ ������ � StatisticDataTT 
UPDATE StatisticDataCl 
SET CapacityIn = @capasityIn, CapacityOut = @capasityOut, TimeDiscon =  @timeDisconnect
WHERE ID_SessionCl = @id_session

GO


/****** Object:  StoredProcedure [dbo].[delExCmdFromCommandTabl]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[delExCmdFromCommandTabl]
  @Original_ID_Command int
AS
DELETE FROM dbo.Command
WHERE ID_Command = @Original_ID_Command

GO


/****** Object:  StoredProcedure [dbo].[DeleteTeletrackOldData]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- �������� ���������� ������ ����������.
-- � �������� ������ �������� 10 ��������� ������� ������������� ���������.
-- =============================================
CREATE PROCEDURE [dbo].[DeleteTeletrackOldData]
AS
SET NOCOUNT ON

-- �� ��������� ������� ��������� ���������� � ������ �������� ������ ����������
SELECT ti.ID_Teletrack, 
  dateadd("Month", usr.Retention_Period * -1, GETDATE()) as Retention_Period 
  INTO #TempTable
FROM dbo.TeletrackInfo ti JOIN dbo.UserInfo usr 
  ON usr.ID_User = ti.ID_User
ORDER BY 1

-- �������� ������ ����������
DELETE FROM data
FROM #TempTable tmp JOIN (
  SELECT ID_Packet, ID_Teletrack, DTimeIncomData,
    ROW_NUMBER() OVER(PARTITION BY ID_Teletrack ORDER BY DTimeIncomData DESC) AS RowNumber
  FROM dbo.TransferedData) data ON data.ID_Teletrack = tmp.ID_Teletrack
WHERE (data.RowNumber > 10) AND (data.DTimeIncomData < tmp.Retention_Period)

-- �������� ���������� ������ ����������
DELETE FROM stat
FROM #TempTable tmp JOIN (
  SELECT ID_Session, ID_Teletrack, TimeDiscon,
    ROW_NUMBER() OVER(PARTITION BY ID_Teletrack ORDER BY TimeDiscon DESC) AS RowNumber
  FROM dbo.StatisticDataTT) stat ON stat.ID_Teletrack = tmp.ID_Teletrack
WHERE (stat.RowNumber > 10) AND (stat.TimeDiscon < tmp.Retention_Period)

-- �������� ������ A1. 
DELETE FROM cmd 
FROM #TempTable tmp JOIN (
  SELECT ID_Command, ID_Teletrack, DeltaTimeExCom,
    ROW_NUMBER() OVER(PARTITION BY ID_Teletrack ORDER BY DeltaTimeExCom DESC) AS RowNumber
  FROM dbo.Command) cmd ON cmd.ID_Teletrack = tmp.ID_Teletrack
WHERE (cmd.RowNumber > 10) AND (cmd.DeltaTimeExCom < tmp.Retention_Period)

DROP TABLE #TempTable  
 
GO


/****** Object:  StoredProcedure [dbo].[DeleteClientOldData]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- �������� ���������� ������ ���������� ������ ��������-�����������.
-- =============================================
CREATE PROCEDURE [dbo].[DeleteClientOldData]
AS
SET NOCOUNT ON

-- �� ��������� ������� ��������� ���������� � ������ �������� ���������� �� ��������
SELECT clnt.ID_Client, 
  dateadd("Month", usr.Retention_Period * -1, GETDATE()) as Retention_Period 
  INTO #TempTable
FROM dbo.ClientInfo clnt JOIN UserInfo usr ON usr.ID_User = clnt.ID_User 

-- �������� ���������� �� ��������. ������ �������� 10 ��������� ������� ������������� �������.
DELETE FROM stat
FROM #TempTable tmp JOIN (
  SELECT ID_SessionCl, ID_Client, TimeDiscon,
    ROW_NUMBER() OVER(PARTITION BY ID_Client ORDER BY TimeDiscon DESC) AS RowNumber
  FROM dbo.StatisticDataCl) stat ON stat.ID_Client = tmp.ID_Client
WHERE (stat.RowNumber > 10) AND (stat.TimeDiscon < tmp.Retention_Period)

-- �������� ���������� � �������� ��������
DELETE FROM dbo.ClientQuery
FROM #TempTable tmp JOIN dbo.ClientQuery clQuery ON tmp.ID_Client = clQuery.ID_Client
WHERE clQuery.DateTimeExQuery < tmp.Retention_Period

DROP TABLE #TempTable

GO


/****** Object:  StoredProcedure [dbo].[st_SelectTeletrackOfUserForUs]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		edubr
-- Create date: 19.03.2009
-- Description:	���������� ��� ��������� ��� ������ ������������� ������� ������������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectTeletrackOfUserForUs]
  @id_dealer int,
  @id_user int
AS
SET NOCOUNT ON

DECLARE @str char(5)
SET @str = ' ��. '

SELECT  tt.ID_Teletrack, tt.LoginTT, tt.PasswordTT, 
  COALESCE(CONVERT(varchar, tt.DTimeLastPack, 121), '��� ������') as LastConn, 

  CASE (COALESCE(CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������'))
    WHEN '��� ������' THEN '��� ������'
    ELSE COALESCE(CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������') + 
      @str + CONVERT(varchar(8), (GETDATE() - tt.DTimeLastPack), 114)
  END AS Delta,

  CASE (COALESCE(CONVERT(varchar, (SIGN(tt.Timeout - CONVERT(int, Datediff(mi, tt.DTimeLastPack, GETDATE()), 2)))), '��� ������'))
    WHEN '��� ������' THEN '��� ������'
    WHEN -1 THEN 'CRITICAL'
    WHEN 0 THEN 'OK'
    WHEN 1 THEN 'OK'
  END AS State, 

  tt.Timeout, u.Name
FROM UserInfo u INNER JOIN TeletrackInfo tt ON u.ID_User = tt.ID_User
WHERE (u.DealerId = @id_dealer) AND (u.ID_User = @id_user)

GO


/****** Object:  StoredProcedure [dbo].[adm_UpdateUserInfo]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_UpdateUserInfo]
  @Name varchar(MAX),
  @Describe varchar(MAX),
  @IsActiveState bit,
  @Retention_Period int,
  @Kod_b int,
  @ID_User int,
  @Dealer nvarchar(50)
AS
SET NOCOUNT OFF

UPDATE [UserInfo] SET [Name] = @Name, [Describe] = @Describe, 
  [IsActiveState] = @IsActiveState, [Retention_Period] = @Retention_Period, 
  [Kod_b] = @Kod_b, [Dealer] = @Dealer 
WHERE [ID_User] = @ID_User
  
SELECT ID_User, Name, Describe, IsActiveState, Retention_Period, Kod_b, Dealer 
FROM UserInfo 
WHERE (ID_User = @ID_User)

exec incrementOfVersion 'UserInfo'

GO


/****** Object:  StoredProcedure [dbo].[adm_UpdateTeletrackInfo]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_UpdateTeletrackInfo]
  @LoginTT char(4),
  @PasswordTT varchar(10),
  @Timeout int,
  @ID_Teletrack int,
  @GSMIMEI varchar(20),
  @ID_User int
AS
SET NOCOUNT OFF

UPDATE TeletrackInfo 
SET LoginTT = @LoginTT, PasswordTT = @PasswordTT, Timeout = @Timeout, 
  GSMIMEI = @GSMIMEI, ID_User = @ID_User
WHERE ID_Teletrack = @ID_Teletrack

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout, GSMIMEI, ID_User 
FROM TeletrackInfo 
WHERE ID_Teletrack = @ID_Teletrack

exec incrementOfVersion 'TeletrackInfo'

GO


/****** Object:  StoredProcedure [dbo].[adm_UpdateCommunicTable]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_UpdateCommunicTable]
  @ID_Client int,
  @ID_Teletrack int,
  @Original_ID_Communic int,
  @Original_ID_Client int,
  @Original_ID_Teletrack int,
  @ID_Communic int
AS
SET NOCOUNT OFF;

UPDATE [CommunicTable] SET [ID_Client] = @ID_Client, [ID_Teletrack] = @ID_Teletrack 
WHERE (([ID_Communic] = @Original_ID_Communic) AND 
  ([ID_Client] = @Original_ID_Client) AND ([ID_Teletrack] = @Original_ID_Teletrack));
  
SELECT ID_Communic, ID_Client, ID_Teletrack 
FROM CommunicTable 
WHERE (ID_Communic = @ID_Communic)

exec incrementOfVersion 'CommunicTable'

GO


/****** Object:  StoredProcedure [dbo].[adm_UpdateClientInfo]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_UpdateClientInfo]
  @LoginCl nvarchar(50),
  @Password nvarchar(50),
  @ID_User int,
  @Describe nvarchar(MAX),
  @Original_ID_Client int,
  @Original_LoginCl nvarchar(50),
  @Original_Password nvarchar(50),
  @Original_ID_User int,
  @ID_Client int
AS
SET NOCOUNT OFF

UPDATE [ClientInfo] 
SET [LoginCl] = @LoginCl, [Password] = @Password, [ID_User] = @ID_User, [Describe] = @Describe 
WHERE (([ID_Client] = @Original_ID_Client) AND ([LoginCl] = @Original_LoginCl) AND 
  ([Password] = @Original_Password) AND ([ID_User] = @Original_ID_User));
  
SELECT ID_Client, LoginCl, Password, InNetwork, ID_Packet, 
  DTimeDeliveredPack, ID_LastPackInSrvDB, ID_User, Describe 
FROM ClientInfo 
WHERE (ID_Client = @ID_Client)

exec incrementOfVersion 'ClientInfo'

GO


/****** Object:  StoredProcedure [dbo].[adm_InsertUserInfo]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_InsertUserInfo]
  @Name varchar(MAX),
  @Describe varchar(MAX),
  @IsActiveState bit,
  @Retention_Period int,
  @Kod_b int,
  @Dealer nvarchar(50)
AS
SET NOCOUNT OFF

INSERT INTO [UserInfo] ([Name], [Describe], [IsActiveState], 
  [Retention_Period], [Kod_b], [Dealer]) 
VALUES (@Name, @Describe, @IsActiveState, @Retention_Period, @Kod_b, @Dealer);
  
SELECT ID_User, Name, Describe, IsActiveState, Retention_Period, Kod_b 
FROM UserInfo 
WHERE (ID_User = SCOPE_IDENTITY())

exec incrementOfVersion 'UserInfo'

GO


/****** Object:  StoredProcedure [dbo].[adm_InsertTeletrackInfo]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_InsertTeletrackInfo]
  @LoginTT char(4),
  @PasswordTT varchar(10),
  @Timeout int,
  @GSMIMEI varchar(20) = NULL,
  @ID_User int
AS
SET NOCOUNT OFF

INSERT INTO TeletrackInfo (LoginTT, PasswordTT, Timeout, GSMIMEI, ID_User) 
VALUES (@LoginTT, @PasswordTT, @Timeout, @GSMIMEI, @ID_User)

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout, GSMIMEI, ID_User 
FROM TeletrackInfo 
WHERE ID_Teletrack = SCOPE_IDENTITY()

exec incrementOfVersion 'TeletrackInfo'

GO


/****** Object:  StoredProcedure [dbo].[adm_InsertCommunicTable]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_InsertCommunicTable]
  @ID_Client int,
  @ID_Teletrack int
AS
SET NOCOUNT OFF

INSERT INTO [CommunicTable] ([ID_Client], [ID_Teletrack]) 
VALUES (@ID_Client, @ID_Teletrack);
  
SELECT ID_Communic, ID_Client, ID_Teletrack 
FROM CommunicTable 
WHERE (ID_Communic = SCOPE_IDENTITY())

exec incrementOfVersion 'CommunicTable'

GO


/****** Object:  StoredProcedure [dbo].[adm_InsertClientInfo]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_InsertClientInfo]
  @LoginCl nvarchar(50),
  @Password nvarchar(50),
  @InNetwork bit,
  @ID_Packet bigint,
  @DTimeDeliveredPack datetime,
  @ID_LastPackInSrvDB bigint,
  @ID_User int,
  @Describe nvarchar(MAX)
AS
SET NOCOUNT OFF;

INSERT INTO [ClientInfo] ([LoginCl], [Password], [InNetwork], [ID_Packet],
  [DTimeDeliveredPack], [ID_LastPackInSrvDB], [ID_User], [Describe]) 
VALUES (@LoginCl, @Password, @InNetwork, @ID_Packet, @DTimeDeliveredPack, 
  @ID_LastPackInSrvDB, @ID_User, @Describe);
  
SELECT ID_Client, LoginCl, Password, InNetwork, ID_Packet, 
  DTimeDeliveredPack, ID_LastPackInSrvDB, ID_User, Describe 
FROM ClientInfo 
WHERE (ID_Client = SCOPE_IDENTITY())

exec incrementOfVersion 'ClientInfo'

GO


/****** Object:  StoredProcedure [dbo].[adm_DeleteCommunicTable]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_DeleteCommunicTable]
  @id int
AS
SET NOCOUNT OFF

DELETE FROM dbo.CommunicTable 
WHERE ID_Communic = @id

exec incrementOfVersion 'CommunicTable'

GO



/****** Object:  StoredProcedure [dbo].[adm_DeleteClientInfo]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_DeleteClientInfo]
  @Original_ID_Client int
AS
SET NOCOUNT OFF

DELETE FROM [ClientInfo] 
WHERE ([ID_Client] = @Original_ID_Client) 

exec incrementOfVersion 'ClientInfo'

GO


/****** Object:  StoredProcedure [dbo].[st_SelectStatActiveDayMonth]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		����� �.�.
-- Create date: 30.07.2009
-- Description:	���������� �� ������� ��������� ���� � �����, ����� �������� ������ ������ @threshold
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectStatActiveDayMonth] 
  @Y int,
  @M int,
  @DLR int = 0, 
  @threshold int
AS
SET NOCOUNT ON

SELECT adm_User_Client.ID_User, CommunicTable.ID_Teletrack,
  (SELECT COUNT(*) AS CNT
   FROM (SELECT DAY(TimeCon) AS D
         FROM StatisticDataTT
         WHERE (MONTH(TimeCon) = @M) AND (YEAR(TimeCon) = @Y)
         GROUP BY DAY(TimeCon), ID_Teletrack
         HAVING (SUM(CapacityIn)/1024 > @threshold) AND (ID_Teletrack = CommunicTable.ID_Teletrack)) AS THR) AS CNT
FROM CommunicTable INNER JOIN adm_User_Client ON CommunicTable.ID_Client = adm_User_Client.Id_Client_min
WHERE (adm_User_Client.DealerId = @DLR OR @DLR=0)

GO


/****** Object:  StoredProcedure [dbo].[st_SelectStatTotalMonth]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		����� �.�.
-- Create date: 28.07.2009
-- Description:	����� ���������� �� �������/��������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectStatTotalMonth] 
  @Y int, 
  @M int,
  @DLR int = 0
AS
SET NOCOUNT ON

SELECT  adm_User_Client.ID_User , adm_User_Client.Id_Client_min, adm_User_Client.DealerId, 
  DealerInfo.DealerName AS Dealer, UserInfo.Name, 
  COUNT(CommunicTable_1.ID_Teletrack) AS Total_Trcs, 0 AS Act, 0 AS ActNo,

  (SELECT COUNT(StatisticDataTT.ID_Session) AS Cnt_Session
   FROM StatisticDataTT INNER JOIN CommunicTable ON StatisticDataTT.ID_Teletrack = CommunicTable.ID_Teletrack
   WHERE (MONTH(StatisticDataTT.TimeCon) = @M) AND (YEAR(StatisticDataTT.TimeCon) = @Y)
   GROUP BY CommunicTable.ID_Client
   HAVING (CommunicTable.ID_Client = adm_User_Client.Id_Client_min)) AS Cnt_Session,

  (SELECT SUM(StatisticDataTT_1.CapacityIn)/1024 AS Skb
   FROM StatisticDataTT AS StatisticDataTT_1 
     INNER JOIN CommunicTable AS CommunicTable_2 ON StatisticDataTT_1.ID_Teletrack = CommunicTable_2.ID_Teletrack
   WHERE (MONTH(StatisticDataTT_1.TimeCon) = @M) AND (YEAR(StatisticDataTT_1.TimeCon) = @Y)
   GROUP BY CommunicTable_2.ID_Client
   HAVING (CommunicTable_2.ID_Client = adm_User_Client.Id_Client_min)) AS Sum_kb
FROM UserInfo 
  INNER JOIN adm_User_Client ON UserInfo.ID_User = adm_User_Client.ID_User 
  INNER JOIN CommunicTable AS CommunicTable_1 ON adm_User_Client.Id_Client_min = CommunicTable_1.ID_Client 
  LEFT OUTER JOIN DealerInfo ON UserInfo.DealerId = DealerInfo.ID
WHERE (UserInfo.DealerId = @DLR) OR (@DLR = 0)
GROUP BY UserInfo.Name, adm_User_Client.Id_Client_min, adm_User_Client.ID_User, 
  DealerInfo.DealerName, adm_User_Client.DealerId
ORDER BY UserInfo.Name

GO


/****** Object:  StoredProcedure [dbo].[st_SelectTeletrackOfId]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		����� �.�.
-- Create date: 24,07,2009
-- Description:	������ ��������� ��� ��� ����������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectTeletrackOfId]
  -- Add the parameters for the stored procedure here
  @Id_Teletr int
AS
BEGIN

SET NOCOUNT ON;
declare @str varchar(5);
set @str = ' ��. ';

SELECT tt.ID_Teletrack, tt.LoginTT, tt.PasswordTT, COALESCE (CONVERT(varchar, tt.DTimeLastPack, 121), '��� ������') AS LastConn, 

  CASE (COALESCE (CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������')) 
    WHEN '��� ������' THEN '��� ������' 
    ELSE COALESCE (CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������') 
      + @str + CONVERT(varchar(8), (GETDATE() - tt.DTimeLastPack), 114) 
  END AS Delta, 

  CASE (COALESCE (CONVERT(
    varchar, (SIGN(tt.Timeout - CONVERT(int, Datediff(mi, tt.DTimeLastPack, GETDATE()), 2)))), '��� ������')) 
    WHEN '��� ������' THEN '��� ������' 
    WHEN - 1 THEN 'CRITICAL' 
    WHEN 0 THEN 'OK' 
    WHEN 1 THEN 'OK' 
  END AS State, 

  tt.Timeout, '' AS Name,  UserInfo.Name AS UserName
FROM UserInfo 
  INNER JOIN adm_User_TT ON UserInfo.ID_User = adm_User_TT.ID_User 
  INNER JOIN TeletrackInfo AS tt ON adm_User_TT.ID_Teletrack = tt.ID_Teletrack
WHERE (tt.ID_Teletrack = @Id_Teletr)

END
GO


/****** Object:  StoredProcedure [dbo].[st_SelectStatTotalMonthTT]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		����� �.�.
-- Create date: 05.08.2009
-- Description:	��������� ���������� �� �������, ��������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectStatTotalMonthTT]
  @Y int, 
  @M int,
  @threshold int, 
  @CL int = 0
AS
SET NOCOUNT ON

DECLARE @str varchar(5);
SET @str = ' ��. ';

SELECT  ut.ID_User, tt.ID_Teletrack, tt.LoginTT,   
  (SELECT COUNT(ID_Session) AS Cnt_Session
   FROM StatisticDataTT
   WHERE (MONTH(TimeCon) = @M) AND (YEAR(TimeCon) = @Y) AND (ID_Teletrack = tt.ID_Teletrack)) AS Cnt_Session,

  (SELECT SUM(StatisticDataTT.CapacityIn)/1024 as Sum_Kb
   FROM StatisticDataTT
   WHERE (MONTH(TimeCon) = @M) AND (YEAR(TimeCon) = @Y) AND (ID_Teletrack = tt.ID_Teletrack) ) AS Sum_Kb,
 
  ((SELECT COUNT(*) AS CNT
    FROM (SELECT DAY(TimeCon) AS D
          FROM StatisticDataTT
          WHERE (MONTH(TimeCon) = @M) AND (YEAR(TimeCon) = @Y)
          GROUP BY DAY(TimeCon), ID_Teletrack
          HAVING (SUM(CapacityIn)/1024 >= @threshold) AND (ID_Teletrack = tt.ID_Teletrack)) AS THR) ) AS Act,

  COALESCE (CONVERT(varchar, tt.DTimeLastPack, 121), '��� ������') AS LastConn, 

  CASE (COALESCE (CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������')) 
     WHEN '��� ������' THEN '��� ������' 
     ELSE COALESCE (CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������') 
       + @str + CONVERT(varchar(8), (GETDATE() - tt.DTimeLastPack), 114) 
  END AS Delta, 
  
  CASE (COALESCE (CONVERT(varchar, (SIGN(tt.Timeout - CONVERT(int, Datediff(mi, tt.DTimeLastPack, GETDATE()), 2)))), '��� ������')) 
    WHEN '��� ������' THEN '��� ������' 
    WHEN - 1 THEN 'CRITICAL' 
    WHEN 0 THEN 'OK' 
    WHEN 1 THEN 'OK' 
  END AS State, 

  tt.Timeout
FROM TeletrackInfo AS tt INNER JOIN adm_User_TT ut ON tt.ID_Teletrack = ut.ID_Teletrack
WHERE ut.ID_User = @CL or @CL=0

GO


/****** Object:  StoredProcedure [dbo].[st_SelectTeletrackOfUser]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		edubr
-- Create date: 19.03.2009
-- Description:	���������� ��� ��������� ��� ������ ������������� ������� ������������
-- =============================================
CREATE PROCEDURE [dbo].[st_SelectTeletrackOfUser]
  @id_user int,
  @Id_tltr int = 0
AS
if @Id_tltr = 0 
begin
  if(@id_user <= 0) 
  begin
     SET @id_user = null;
  end
  
  DECLARE @str varchar(5);
  SET @str = ' ��. ';
  
  SELECT  tt.ID_Teletrack, tt.LoginTT, tt.PasswordTT, 
    COALESCE(CONVERT(varchar, tt.DTimeLastPack, 121), '��� ������') AS LastConn, 

    CASE (COALESCE(CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������'))
      WHEN '��� ������' THEN '��� ������'
      ELSE COALESCE(CONVERT(varchar, Datediff(dd, tt.DTimeLastPack, GETDATE()), 2), '��� ������') + 
        @str + CONVERT(varchar(8), (GETDATE() - tt.DTimeLastPack), 114)
      END AS Delta,

      CASE (COALESCE(CONVERT(varchar, (SIGN(tt.Timeout - CONVERT(int, Datediff(mi, tt.DTimeLastPack, GETDATE()), 2)))), '��� ������'))
        WHEN '��� ������' THEN '��� ������'
        WHEN -1 THEN 'CRITICAL'
        WHEN 0 THEN 'OK'
        WHEN 1 THEN 'OK'
      END AS State, 

      tt.Timeout, u.Name AS UserName
  FROM TeletrackInfo tt LEFT JOIN UserInfo u ON tt.ID_User = u.ID_User
  WHERE  (@id_user is null) or (u.ID_User = @ID_User)
end
else begin
  exec st_SelectTeletrackOfId @Id_tltr
end

GO


CREATE PROCEDURE [dbo].[adm_SelectTeletrackInfoByLogin]
  @login char(4)
AS
SET NOCOUNT ON

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout, GSMIMEI, DTimeLastPack, ID_User
FROM dbo.TeletrackInfo
WHERE LoginTT = @login

GO


CREATE PROCEDURE [dbo].[adm_SelectTeletrackInfoByID]
  @id int
AS
SET NOCOUNT ON

SELECT ID_Teletrack, LoginTT, PasswordTT, Timeout, GSMIMEI, DTimeLastPack, ID_User
FROM dbo.TeletrackInfo
WHERE ID_Teletrack = @id

GO


CREATE PROCEDURE [dbo].[adm_SelectClientInfoByLogin]
  @loginCl nvarchar(50) 
AS
SET NOCOUNT ON;

SELECT ID_Client, LoginCl, Password, InNetwork, ID_Packet, 
  DTimeDeliveredPack, ID_LastPackInSrvDB, ID_User, Describe
FROM dbo.ClientInfo
WHERE LoginCl = @loginCl

GO


CREATE PROCEDURE [dbo].[adm_SelectClientInfoByID]
  @id int 
AS
SET NOCOUNT ON

SELECT ID_Client, LoginCl, Password, InNetwork, ID_Packet, 
  DTimeDeliveredPack, ID_LastPackInSrvDB, ID_User, Describe
FROM dbo.ClientInfo
WHERE ID_Client = @id

GO

CREATE PROCEDURE [dbo].[adm_SelectTeletrackLinkedState]
  @idUser int,
  @idClient int,
  @startIndex int,
  @finishIndex int
AS
SET NOCOUNT ON

SELECT DetailID, DetailLogin, CommunicID, Linked 
FROM (
  SELECT tt.ID_Teletrack AS DetailID, tt.LoginTT AS DetailLogin, ct.ID_Communic AS CommunicID, 
    CAST(
      CASE 
        WHEN ct.ID_Client IS NULL THEN 0
        ELSE 1
      END 
    AS bit) AS Linked,
    ROW_NUMBER() OVER (ORDER BY tt.LoginTT) AS RowNum
  FROM dbo.TeletrackInfo tt 
    LEFT JOIN dbo.ClientInfo ci ON (tt.ID_User = ci.ID_User)
    LEFT JOIN dbo.CommunicTable ct ON (ci.ID_Client = ct.ID_Client)
      AND (tt.ID_Teletrack = ct.ID_Teletrack)
    WHERE (tt.ID_User = @idUser) AND (ci.ID_Client = @idClient)) tt
WHERE tt.RowNum BETWEEN @startIndex AND @finishIndex

GO


CREATE PROCEDURE [dbo].[adm_SelectClientLinkedState]
  @idUser int,
  @idTeletrack int
AS
SET NOCOUNT ON

SELECT ci.ID_Client AS DetailID, ci.LoginCl AS DetailLogin, ct.ID_Communic AS CommunicID, 
  CAST(
    CASE 
      WHEN ct.ID_Client IS NULL THEN 0
      ELSE 1
    END 
  AS bit) AS Linked
FROM dbo.ClientInfo ci 
  LEFT JOIN dbo.TeletrackInfo tt ON (ci.ID_User = tt.ID_User)
  LEFT JOIN dbo.CommunicTable ct ON (ci.ID_Client = ct.ID_Client)
   AND (tt.ID_Teletrack = ct.ID_Teletrack)
WHERE (ci.ID_User = @idUser) AND (tt.ID_Teletrack = @idTeletrack)
ORDER BY ci.LoginCl

GO


-- =============================================
-- Description: ��������� ��� ������ ����� "����������" ��� ������� �1.
-- =============================================
CREATE PROCEDURE [dbo].[adm_SetCommandApproved]
  @id int,
  @approved bit
AS
SET NOCOUNT ON

UPDATE dbo.Command 
SET Approved = @approved
WHERE (ID_Command = @id) AND (DeltaTimeExCom > getdate()) AND (ExecuteState = 0)

IF @@ROWCOUNT > 0 
  EXEC dbo.incrementOfVersion 'Command'

GO


/************************************************************/
/*                                                          */
/*                       4. VIEWS                           */
/*                                                          */
/************************************************************/

/****** Object:  View [dbo].[User_Client]    Script Date: 08/11/2009 11:36:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[User_Client]
AS
SELECT dbo.ClientInfo.ID_User, MIN(dbo.CommunicTable.ID_Client) AS Id_Client, dbo.ClientInfo.Describe
FROM dbo.ClientInfo INNER JOIN dbo.CommunicTable 
  ON dbo.ClientInfo.ID_Client = dbo.CommunicTable.ID_Client
GROUP BY dbo.ClientInfo.ID_User, dbo.ClientInfo.Describe
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CommunicTable"
            Begin Extent = 
               Top = 13
               Left = 94
               Bottom = 106
               Right = 245
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ClientInfo"
            Begin Extent = 
               Top = 6
               Left = 428
               Bottom = 289
               Right = 606
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'User_Client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'User_Client'
GO


/****** Object:  View [dbo].[adm_User_Client]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[adm_User_Client]
AS
SELECT dbo.ClientInfo.ID_User, MIN(dbo.CommunicTable.ID_Client) AS Id_Client_min, dbo.UserInfo.DealerId
FROM dbo.CommunicTable 
  INNER JOIN dbo.ClientInfo ON dbo.CommunicTable.ID_Client = dbo.ClientInfo.ID_Client 
  INNER JOIN dbo.UserInfo ON dbo.ClientInfo.ID_User = dbo.UserInfo.ID_User
GROUP BY dbo.ClientInfo.ID_User, dbo.UserInfo.DealerId
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CommunicTable"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 99
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ClientInfo"
            Begin Extent = 
               Top = 6
               Left = 227
               Bottom = 231
               Right = 405
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "UserInfo"
            Begin Extent = 
               Top = 6
               Left = 443
               Bottom = 209
               Right = 622
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'adm_User_Client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'adm_User_Client'
GO


/****** Object:  View [dbo].[adm_Trafic_Select]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[adm_Trafic_Select]
AS
SELECT TOP (100) PERCENT 
  SUM(dbo.StatisticDataTT.CapacityOut) AS Out, SUM(dbo.StatisticDataTT.CapacityIn) AS [In], dbo.TeletrackInfo.LoginTT, 
  COUNT(dbo.StatisticDataTT.ID_Session) AS DayContacts, CONVERT(char(11), dbo.StatisticDataTT.TimeCon) AS D,
  
  (SELECT dbo.UserInfo.Name + ' ' + dbo.UserInfo.Describe AS Expr1
   FROM dbo.CommunicTable 
     INNER JOIN dbo.ClientInfo ON dbo.CommunicTable.ID_Client = dbo.ClientInfo.ID_Client 
     INNER JOIN dbo.UserInfo ON dbo.ClientInfo.ID_User = dbo.UserInfo.ID_User
   GROUP BY dbo.CommunicTable.ID_Teletrack, dbo.UserInfo.Describe, dbo.UserInfo.Name
   HAVING (dbo.CommunicTable.ID_Teletrack = dbo.TeletrackInfo.ID_Teletrack)) AS Client, 

  DAY(dbo.StatisticDataTT.TimeCon) AS Day, MONTH(dbo.StatisticDataTT.TimeCon) AS Month, 
  YEAR(dbo.StatisticDataTT.TimeCon) AS Year
FROM dbo.StatisticDataTT 
  INNER JOIN dbo.TeletrackInfo ON dbo.StatisticDataTT.ID_Teletrack = dbo.TeletrackInfo.ID_Teletrack
GROUP BY dbo.TeletrackInfo.LoginTT, YEAR(dbo.StatisticDataTT.TimeCon), 
  MONTH(dbo.StatisticDataTT.TimeCon), DAY(dbo.StatisticDataTT.TimeCon), 
  CONVERT(char(11), dbo.StatisticDataTT.TimeCon), dbo.TeletrackInfo.ID_Teletrack, 
  DATEDIFF(DAY, dbo.StatisticDataTT.TimeCon, GETDATE())
HAVING (DATEDIFF(DAY, dbo.StatisticDataTT.TimeCon, GETDATE()) <= 60)
ORDER BY Year, Month, Day, Client
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[11] 2[29] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "StatisticDataTT"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 205
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TeletrackInfo"
            Begin Extent = 
               Top = 6
               Left = 243
               Bottom = 114
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'adm_Trafic_Select'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'adm_Trafic_Select'
GO


/****** Object:  View [dbo].[adm_Trafic_Percent]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[adm_Trafic_Percent]
AS
SELECT TOP (100) PERCENT
  (SELECT dbo.UserInfo.Name + ' ' + dbo.UserInfo.Describe AS Expr1
   FROM dbo.CommunicTable 
     INNER JOIN dbo.ClientInfo ON dbo.CommunicTable.ID_Client = dbo.ClientInfo.ID_Client 
     INNER JOIN dbo.UserInfo ON dbo.ClientInfo.ID_User = dbo.UserInfo.ID_User
   GROUP BY dbo.CommunicTable.ID_Teletrack, dbo.UserInfo.Describe, dbo.UserInfo.Name
   HAVING (dbo.CommunicTable.ID_Teletrack = dbo.TeletrackInfo.ID_Teletrack)) AS Client, 

   SUM(StatisticDataTT_1.CapacityIn) AS [In], dbo.TeletrackInfo.LoginTT, 
   MONTH(StatisticDataTT_1.TimeCon) AS M, YEAR(StatisticDataTT_1.TimeCon) AS Y,

   (SELECT COUNT(*) AS CD
    FROM (SELECT SUM(CapacityIn) AS S
          FROM dbo.StatisticDataTT
          WHERE (ID_Teletrack = dbo.TeletrackInfo.ID_Teletrack) AND (MONTH(TimeCon) = MONTH(StatisticDataTT_1.TimeCon))
          GROUP BY DAY(TimeCon)
          HAVING (SUM(CapacityIn) > 10)) AS S) AS CNT, 

  ROUND(100 * (
    SELECT COUNT(*) AS CD
    FROM (SELECT SUM(CapacityIn) AS S
          FROM dbo.StatisticDataTT AS StatisticDataTT_2
          WHERE (ID_Teletrack = dbo.TeletrackInfo.ID_Teletrack) AND (MONTH(TimeCon) = MONTH(StatisticDataTT_1.TimeCon))
          GROUP BY DAY(TimeCon)
          HAVING (SUM(CapacityIn) > 10)) AS S_1) / 
            CASE MONTH(StatisticDataTT_1.TimeCon) 
							WHEN 1 THEN 31 
							WHEN 2 THEN 28 
							WHEN 3 THEN 31 
							WHEN 4 THEN 30 
							WHEN 5 THEN 31 
							WHEN 6 THEN 30 
							WHEN 7 THEN 31 
							WHEN 8 THEN 31 
							WHEN 9 THEN 30 
							WHEN 10 THEN 31 
							WHEN 11 THEN 30 
							WHEN 12 THEN 31 
            END, 2) AS PERS
FROM dbo.StatisticDataTT AS StatisticDataTT_1 
  INNER JOIN dbo.TeletrackInfo ON StatisticDataTT_1.ID_Teletrack = dbo.TeletrackInfo.ID_Teletrack
GROUP BY dbo.TeletrackInfo.LoginTT, YEAR(StatisticDataTT_1.TimeCon), 
  MONTH(StatisticDataTT_1.TimeCon), dbo.TeletrackInfo.ID_Teletrack
HAVING (MONTH(StatisticDataTT_1.TimeCon) = MONTH(GETDATE())) AND (YEAR(StatisticDataTT_1.TimeCon) = YEAR(GETDATE())) OR
  (MONTH(StatisticDataTT_1.TimeCon) =  
    CASE MONTH(StatisticDataTT_1.TimeCon) 
      WHEN 12 THEN 1 
      ELSE MONTH(GETDATE()) - 1 
    END) AND 
  (YEAR(StatisticDataTT_1.TimeCon) = 
    CASE MONTH(StatisticDataTT_1.TimeCon) 
      WHEN 12 THEN YEAR(GETDATE()) - 1 
      ELSE YEAR(GETDATE()) 
    END)
ORDER BY Y, M, Client
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "StatisticDataTT_1"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 205
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TeletrackInfo"
            Begin Extent = 
               Top = 6
               Left = 243
               Bottom = 114
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'adm_Trafic_Percent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'adm_Trafic_Percent'
GO


/****** Object:  View [dbo].[adm_User_TT]    Script Date: 08/11/2009 11:36:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[adm_User_TT]
AS
SELECT     dbo.CommunicTable.ID_Teletrack, dbo.adm_User_Client.ID_User, dbo.adm_User_Client.DealerId
FROM         dbo.CommunicTable INNER JOIN
                      dbo.adm_User_Client ON dbo.CommunicTable.ID_Client = dbo.adm_User_Client.Id_Client_min
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CommunicTable"
            Begin Extent = 
               Top = 22
               Left = 142
               Bottom = 115
               Right = 293
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "adm_User_Client"
            Begin Extent = 
               Top = 7
               Left = 364
               Bottom = 100
               Right = 515
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 2190
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'adm_User_TT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'adm_User_TT'
GO


/************************************************************/
/*                                                          */
/*                      5. REFERENCES                       */
/*                                                          */
/************************************************************/

USE [OnLineServiceDB]
GO
INSERT INTO [dbo].[VersionControl]([TableName],[Version]) VALUES ('ClientInfo',1);
INSERT INTO [dbo].[VersionControl]([TableName],[Version]) VALUES ('TeletrackInfo',1);
INSERT INTO [dbo].[VersionControl]([TableName],[Version]) VALUES ('CommunicTable',1);
INSERT INTO [dbo].[VersionControl]([TableName],[Version]) VALUES ('Command',1);
INSERT INTO [dbo].[VersionControl]([TableName],[Version]) VALUES ('UserInfo',1);
GO
INSERT INTO [dbo].[Roles]([Role]) VALUES('Admin');
INSERT INTO [dbo].[Roles]([Role]) VALUES('User');
GO
INSERT INTO [dbo].[DealerInfo]([DealerName], [Password], [Descr]) 
Values ('Admin', 'admin', 'Account of administrator')
GO
INSERT INTO [dbo].[DealerRole]([DealerID], [RoleID]) Values (1,1)
GO


/************************************************************/
/*                                                          */
/*                    6. SERVICE BROKER                     */
/*                                                          */
/************************************************************/

-- �������� �������� ����� ���� ������ (��������� ������ �������)

CREATE MASTER KEY ENCRYPTION BY PASSWORD = '(-)RCS(*)2009(+)'
GO

USE master;
GO

-- ��������� ������ �������
ALTER DATABASE OnLineServiceDB SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
GO
ALTER DATABASE OnLineServiceDB SET NEW_BROKER
GO
ALTER DATABASE OnLineServiceDB SET MULTI_USER
GO


/************************************************************/
/*                                                          */
/*                        7. JOBS                           */
/*                                                          */
/************************************************************/

-- Job �������� ������ ������ 

USE [msdb]
GO
/****** Object:  Job [OnlineServiceDB_FullMaintenance.Daily05_00]    Script Date: 07/17/2009 19:34:50 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 07/17/2009 19:34:50 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'OnlineServiceDB_FullMaintenance.Daily05_00', 
    @enabled=1, 
    @notify_level_eventlog=0, 
    @notify_level_email=0, 
    @notify_level_netsend=0, 
    @notify_level_page=0, 
    @delete_level=0, 
    @description=N'1. ������� ���������� ������ (����������, ������) ����������� � ����������
2.������� ���������� ������ (����������, �������) ����������� � ��������
3. ����������� ��������
4. ���������� ����������', 
    @category_name=N'[Uncategorized (Local)]', 
    @owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Delete Teletrack Old Data]    Script Date: 07/17/2009 19:34:51 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Delete Teletrack Old Data', 
    @step_id=1, 
    @cmdexec_success_code=0, 
    @on_success_action=3, 
    @on_success_step_id=0, 
    @on_fail_action=2, 
    @on_fail_step_id=0, 
    @retry_attempts=0, 
    @retry_interval=0, 
    @os_run_priority=0, @subsystem=N'TSQL', 
    @command=N'EXECUTE [dbo].[DeleteTeletrackOldData]', 
    @database_name=N'OnLineServiceDB', 
    @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Delete Client Old Data]    Script Date: 07/17/2009 19:34:51 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Delete Client Old Data', 
    @step_id=2, 
    @cmdexec_success_code=0, 
    @on_success_action=3, 
    @on_success_step_id=0, 
    @on_fail_action=2, 
    @on_fail_step_id=0, 
    @retry_attempts=0, 
    @retry_interval=0, 
    @os_run_priority=0, @subsystem=N'TSQL', 
    @command=N'EXECUTE [dbo].[DeleteClientOldData] ', 
    @database_name=N'OnLineServiceDB', 
    @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Rebuild Indexes]    Script Date: 07/17/2009 19:34:51 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Rebuild Indexes', 
    @step_id=3, 
    @cmdexec_success_code=0, 
    @on_success_action=3, 
    @on_success_step_id=0, 
    @on_fail_action=2, 
    @on_fail_step_id=0, 
    @retry_attempts=0, 
    @retry_interval=0, 
    @os_run_priority=0, @subsystem=N'TSQL', 
    @command=N'EXECUTE [dbo].[MaintenanceRebuildIndex]', 
    @database_name=N'OnLineServiceDB', 
    @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Update Statistics]    Script Date: 07/17/2009 19:34:51 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Update Statistics', 
    @step_id=4, 
    @cmdexec_success_code=0, 
    @on_success_action=1, 
    @on_success_step_id=0, 
    @on_fail_action=2, 
    @on_fail_step_id=0, 
    @retry_attempts=0, 
    @retry_interval=0, 
    @os_run_priority=0, @subsystem=N'TSQL', 
    @command=N'EXECUTE [dbo].[MaintenanceUpdateStatistics]', 
    @database_name=N'OnLineServiceDB', 
    @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily_5_00', 
    @enabled=1, 
    @freq_type=4, 
    @freq_interval=1, 
    @freq_subday_type=1, 
    @freq_subday_interval=0, 
    @freq_relative_interval=0, 
    @freq_recurrence_factor=0, 
    @active_start_date=20090717, 
    @active_end_date=99991231, 
    @active_start_time=50000, 
    @active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:



