using System;
using System.Collections.Generic;
using System.Text;

namespace RCS.Lic
{
  /// <summary>
  /// ��������� ���������� ������.
  /// </summary>
  public interface IStoppable
  {
    /// <summary>
    /// ���������� ������.
    /// </summary>
    void Stop();
  }
}
