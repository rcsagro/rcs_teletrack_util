
namespace RCS.Lic
{
  /// <summary>
  /// ��������� �������� ����-����� � ��������.
  /// </summary>
  public enum HState
  {
    /// <summary>
    /// ���� � �������� �������.
    /// </summary>
    OK,
    /// <summary>
    /// �� ������ ����-����.
    /// </summary>
    HaspNotFound,
    /// <summary>
    /// �� ������� �������� � ���� �����.
    /// </summary>
    LicenseNotFound,
    /// <summary>
    /// �� ������ xml ���� � ���������.
    /// </summary>
    LicFileNotFound,
    /// <summary>
    /// ������������ ������ xml ����� ��������.
    /// </summary>
    InvalidLicFileXmlFormat
  }
}
