using RCS.Lic.Entities;
using RCS.Lic.HASP;

namespace RCS.Lic
{
  /// <summary>
  /// �������� ������������ ����������.
  /// </summary>
  internal class LicDetector : BaseLicDetector, ILicDetector
  {
    internal LicDetector(IHasp hasp)
      : base(hasp)
    {
    }

    #region ILicDetector Members

    /// <summary>
    /// ������ ������������ ����������.
    /// </summary>
    /// <returns>������ LicenceInfo.</returns>
    public Licence GetLicence()
    {
      Refresh();
      return LicenceInfo;
    }

    #endregion
  }
}
