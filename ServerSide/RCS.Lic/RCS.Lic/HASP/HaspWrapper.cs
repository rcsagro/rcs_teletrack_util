using System;
using System.IO;
using Aladdin.HASP;

namespace RCS.Lic.HASP
{
  public class HaspWrapper : IHasp
  {
    private readonly byte[] _vendorCode;

    private HaspWrapper() { }

    /// <summary>
    /// �����������.
    /// </summary>                                    
    /// <exception cref="ArgumentNullException">
    /// �� ������� ������-���.</exception>
    /// <param name="vendorCode">Vendor Code.</param>
    public HaspWrapper(byte[] vendorCode)
    {
      if ((vendorCode == null) || (vendorCode.Length == 0))
      {
        throw new ArgumentNullException("vendorCode");
      }
      _vendorCode = vendorCode;
    }

    #region IHasp Members

    /// <summary>
    /// �������� ������� ������ �������� � HASP-�����. 
    /// </summary>
    /// <param name="licenceNumber">����� ��������.</param>
    /// <returns>������ ��������.</returns>
    public HaspStatus CheckLicenceNumber(int licenceNumber)
    {
      HaspStatus result;
      using (Hasp hasp = new Hasp(HaspFeature.FromProgNum(licenceNumber)))
      {
        result = hasp.Login(_vendorCode);
        if (hasp.IsLoggedIn())
        {
          hasp.Logout();
        }
      }
      return result;
    }

    /// <summary>
    /// ������������� �����.
    /// </summary>
    /// <exception cref="ArgumentNullException">�� ������� ���� � �����.</exception>
    /// <exception cref="FileNotFoundException">���� �� ������.</exception>
    /// <param name="fileName">������ ���� � �����.</param>
    /// <param name="decodedFile">�������������� ����� � ������� ����.</param>
    /// <returns>������ ���������� ��������.</returns>
    public HaspStatus DecodeFile(string fileName, out byte[] decodedFile)
    {
      if (String.IsNullOrEmpty(fileName.Trim()))
      {
        throw new ArgumentNullException("fileName");
      }
      if (!File.Exists(fileName))
      {
        throw new FileNotFoundException(String.Format(
          "�� ������ ����: {0}", fileName));
      }

      HaspStatus result;
      decodedFile = null;
      using (Hasp hasp = new Hasp(HaspFeature.ProgNumDefault))
      {
        result = hasp.Login(_vendorCode);

        if (hasp.IsLoggedIn())
        {
          // �������� � ����� ������������ ����.
          decodedFile = File.ReadAllBytes(fileName);
          // ��������������.
          result = hasp.Decrypt(decodedFile);
          hasp.Logout();
        }
      }
      return result;
    }

    #endregion
  }
}
