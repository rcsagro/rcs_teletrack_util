/****** Object:  StoredProcedure [dbo].[CheckDB]    Script Date: 14.12.2016 13:08:49 ******/
IF OBJECT_ID('CheckDB','P') IS NOT NULL
DROP PROCEDURE [dbo].[CheckDB];
GO

/****** Object:  StoredProcedure [dbo].[CheckDB]    Script Date: 14.12.2016 13:08:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CheckDB]
--SQL SECURITY INVOKER
--COMMENT '�������� �� ��� �����������.'
AS
BEGIN
  SELECT 'OK';
END

GO

/****** Object:  StoredProcedure [dbo].[CheckOdo]    Script Date: 14.12.2016 13:10:15 ******/
IF OBJECT_ID('CheckOdo','P') IS NOT NULL
DROP PROCEDURE [dbo].[CheckOdo];
GO

/****** Object:  StoredProcedure [dbo].[CheckOdo]    Script Date: 14.12.2016 13:10:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CheckOdo]
(
  @m_id     INTEGER,
  @end_time DATETIME
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT dbo.FROM_UNIXTIME(dbo.datagps.UnixTime) AS [time]
       , (dbo.datagps.Latitude / 600000.00000) AS Lat
       , (dbo.datagps.Longitude / 600000.00000) AS Lon
  FROM
    dbo.datagps
  WHERE
    dbo.datagps.Mobitel_ID = @m_id
    AND dbo.datagps.UnixTime BETWEEN (SELECT dbo.UNIX_TIMESTAMPS(odoTime)
                                  FROM
                                    dbo.vehicle
                                  WHERE
                                    Mobitel_id = @m_id) AND dbo.UNIX_TIMESTAMPS(@end_time)
    AND Valid = 1;
END

GO

/****** Object:  StoredProcedure [dbo].[dataview]    Script Date: 14.12.2016 13:14:39 ******/
IF OBJECT_ID('dataview','P') IS NOT NULL
DROP PROCEDURE [dbo].[dataview]
GO

/****** Object:  StoredProcedure [dbo].[dataview]    Script Date: 14.12.2016 13:14:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[dataview]
(
  @m_id       INTEGER,
  @val        SMALLINT,
  @begin_time DATETIME,
  @end_time   DATETIME
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS [time]
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
       , [Events] AS [Events]
       , LogID AS LogID
  FROM
    datagps
  WHERE
    Mobitel_ID = @m_id
    AND (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@begin_time) AND dbo.UNIX_TIMESTAMPS(@end_time))
    AND (Valid = @val)
    AND NOT (Latitude = 0
    AND Longitude = 0)
  ORDER BY
    UnixTime;
END

GO

IF OBJECT_ID('delDupSrvPacket','P') IS NOT NULL
DROP PROCEDURE [dbo].[delDupSrvPacket]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE[dbo].[delDupSrvPacket]
AS
BEGIN
    -- Insert statements for procedure here
	if exists (
        select * from tempdb.dbo.sysobjects o
        where o.xtype in ('U') 
        and o.id = object_id(N'tempdb..#tmp64')
)
BEGIN
  DROP TABLE #tmp64; 
END;

if object_id('tempdb..#tmp64') is not null drop table #tmp64  -- �������� �� ������������� �������

create table #tmp64 -- �������� ��������� �������
    (mobi int, lat int, lng int, dir int, acc int, unixt int, spd int, vald int, stl int, rsg int, evn int, snsset int, sns varbinary(50), vlt int, dgp varchar(32), lid int, spid bigint)

insert into #tmp64 SELECT 
  Mobitel_ID,
  Latitude,
  Longitude,
  Direction,
  Acceleration,
  UnixTime,
  Speed,
  Valid,
  Satellites,
  RssiGsm,
  [Events],
  SensorsSet,
  Sensors,
  Voltage,
  DGPS,
  LogID,
  SrvPacketID
FROM datagpsbuffer_on64
GROUP BY SrvPacketID, Mobitel_ID,
  Latitude,
  Longitude,
  Direction,
  Acceleration,
  UnixTime,
  Speed,
  Valid,
  Satellites,
  RssiGsm,
  [Events],
  SensorsSet,
  Sensors,
  Voltage,
  DGPS,
  LogID;

TRUNCATE TABLE datagpsbuffer_on64;

INSERT INTO dbo.datagpsbuffer_on64 SELECT d.mobi,
  d.lat,
  d.lng,
  d.dir,
  d.acc,
  d.unixt,
  d.spd,
  d.vald,
  d.stl,
  d.rsg,
  d.evn,
  d.snsset,
  d.sns,
  d.vlt,
  d.dgp,
  d.lid,
  d.spid
FROM #tmp64 d;

-- Insert statements for procedure here
	if exists (
        select * from tempdb.dbo.sysobjects o
        where o.xtype in ('U') 
        and o.id = object_id(N'tempdb..#tmp64')
)
BEGIN
  DROP TABLE #tmp64; 
END;

if object_id('tempdb..#tmp64') is not null drop table #tmp64  -- �������� �� ������������� �������

END
GO

/****** Object:  StoredProcedure [dbo].[DrListMobitelConfig]    Script Date: 14.12.2016 13:52:21 ******/
IF OBJECT_ID('DrListMobitelConfig','P') IS NOT NULL
DROP PROCEDURE [dbo].[DrListMobitelConfig]
GO

/****** Object:  StoredProcedure [dbo].[DrListMobitelConfig]    Script Date: 14.12.2016 13:52:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DrListMobitelConfig]
--SQL SECURITY INVOKER
--COMMENT '������� �������� ���������� � ������ DirectOnline'
AS
BEGIN
  SELECT m.Mobitel_id AS MobitelID
       , imc.DevIdShort AS DevIdShort
       , cge.Pop3un AS Login
       , cge.Pop3pw AS Password
  FROM
    Mobitels m
    JOIN ConfigMain cm
      ON m.Mobitel_id = cm.Mobitel_ID
    JOIN ConfigGprsEmail cge
      ON cm.ConfigGprsEmail_ID = cge.ID
    JOIN internalmobitelconfig imc
      ON m.InternalMobitelConfig_ID = imc.ID
  WHERE
    (cge.ConfigGprsEmail_ID = (SELECT max(intCge.ConfigGprsEmail_ID)
                               FROM
                                 ConfigGprsEmail intCge
                               WHERE
                                 intCge.ID = cge.ID))
    AND (imc.InternalMobitelConfig_ID = (SELECT max(intConf.InternalMobitelConfig_ID)
                                         FROM
                                           internalmobitelconfig intConf
                                         WHERE
                                           intConf.ID = imc.ID))
    AND (imc.devIdShort IS NOT NULL)
    AND (imc.devIdShort <> '')
    AND (cge.Pop3un IS NOT NULL)
    AND (cge.Pop3un <> '')
  ORDER BY
    m.Mobitel_ID;
END

GO

/****** Object:  StoredProcedure [dbo].[DrTransferBuffer]    Script Date: 14.12.2016 13:56:44 ******/
IF OBJECT_ID('DrTransferBuffer','P') IS NOT NULL
DROP PROCEDURE [dbo].[DrTransferBuffer]
GO

/****** Object:  StoredProcedure [dbo].[DrTransferBuffer]    Script Date: 14.12.2016 13:56:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[DrTransferBuffer]
--SQL SECURITY INVOKER
--COMMENT '��������� GPS ������ DirectOnline �� �������� ������� � datagps'
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  --DECLARE @DataExists TINYINT;
  --SET @DataExists = 1; 

  DECLARE @TmpMaxUnixTime INT;

  DECLARE CursorMobitels CURSOR FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_dr;

  DELETE o
  FROM
    [online] o, datagpsbuffer_dr dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             [online] WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    INSERT INTO [online](Mobitel_ID
                     , LogID
                     , UnixTime
                     , Latitude
                     , Longitude
                     , Altitude
                     , Direction
                     , Speed
                     , Valid
                     , [Events]
                     , Sensor1
                     , Sensor2
                     , Sensor3
                     , Sensor4
                     , Sensor5
                     , Sensor6
                     , Sensor7
                     , Sensor8
                     , Counter1
                     , Counter2
                     , Counter3
                     , Counter4
                     , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , [Events]
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_dr
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;


    DELETE
    FROM
      [online]
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        [online]
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END; --END WHILE;
  CLOSE CursorMobitels;

  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, 
	Valid = b.Valid, InMobitelID = b.LogID, [Events] = b.[Events], Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, 
	Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, 
	Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, 
	Counter4 = b.Counter4
  FROM
    dbo.datagps d, dbo.datagpsbuffer_dr b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  DELETE b
  FROM
    datagps d, datagpsbuffer_dr b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);


  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , [Events]
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , [Events]
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
  FROM
    datagpsbuffer_dr GROUP BY Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , [Events]
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs;

  TRUNCATE TABLE datagpsbuffer_dr;
END


GO

/****** Object:  StoredProcedure [dbo].[ExecuteQuery]    Script Date: 14.12.2016 14:13:23 ******/
IF OBJECT_ID('ExecuteQuery','P') IS NOT NULL
DROP PROCEDURE [dbo].[ExecuteQuery]
GO

/****** Object:  StoredProcedure [dbo].[ExecuteQuery]    Script Date: 14.12.2016 14:13:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ExecuteQuery]
(
  @sqlQuery VARCHAR
)
AS
BEGIN
  DECLARE @s VARCHAR;
  SET @s = @sqlQuery;
  EXECUTE (@s);
END

GO

/****** Object:  StoredProcedure [dbo].[ExecuteQuery]    Script Date: 14.12.2016 14:13:23 ******/
IF OBJECT_ID('FillLostDataGPS','P') IS NOT NULL
DROP PROCEDURE [dbo].[FillLostDataGPS]
GO

/****** Object:  StoredProcedure [dbo].[ExecuteQuery]    Script Date: 14.12.2016 14:13:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FillLostDataGPS]
AS
BEGIN
  DECLARE @LAST_RECORDS_COUNT INT;
  SET @LAST_RECORDS_COUNT = 32000;
  DECLARE @LastMobitelRecord INT; 

  DECLARE @MobitelIDInCursor INT;		
  DECLARE @ConfirmedIDInCursor INT;	
  DECLARE @NewConfirmedID INT;			
  DECLARE @NewStartIDSelect INT; 
  SET  @NewStartIDSelect = 0;
  DECLARE @MaxMobitelLogID INT;		

  DECLARE @BeginLogIDInCursor INT; 
  DECLARE @EndLogIDInCursor INT;		
  DECLARE @CountInLostRange INT;	
  --DECLARE @DataExists TINYINT;	
  --SET @DataExists = 1;
  DECLARE @DataChanged TINYINT;	
  DECLARE @NeedOptimize TINYINT; 
  SET @NeedOptimize = 0;
  DECLARE @TableName VARCHAR(255);

  DECLARE CursorMobitels CURSOR FOR
    SELECT Mobitel_ID, ConfirmedID FROM mobitels ORDER BY Mobitel_ID;
  DECLARE CursorLostData CURSOR FOR
    SELECT Begin_LogID, End_LogID FROM datagpslosted
    WHERE Mobitel_ID = @MobitelIDInCursor ORDER BY Begin_LogID;
  DECLARE CursorTable CURSOR FOR SELECT
    '#MUTEX_FILL_LOST_DATA_GPS'
  FROM information_schema.tables
  WHERE table_schema = DB_NAME();

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0;
  
 -- DECLARE EXIT HANDLER FOR SQLEXCEPTION
  --BEGIN
    
    --DROP TABLE IF EXISTS [#MUTEX_FILL_LOST_DATA_GPS];
  --END;

  
  OPEN CursorTable;
  FETCH CursorTable INTO @TableName;
  CLOSE CursorTable;
  
  IF not (@@fetch_status = 0) BEGIN
    
    CREATE TABLE #MUTEX_FILL_LOST_DATA_GPS (dummy char(1));
    --SET @DataExists = 1;

    --CREATE TABLE IF NOT EXISTS #tmp_datagpslosted LIKE dbo.datagps;
	if object_id('tempdb..#tmp_datagpslosted') is not null drop table #tmp_datagpslosted;
	SELECT * INTO #tmp_datagpslosted FROM dbo.datagps WHERE 1 = 2

    OPEN CursorMobitels;
    FETCH CursorMobitels INTO @MobitelIDInCursor, @ConfirmedIDInCursor;
    WHILE @@fetch_status = 0
      SET @NewConfirmedID = 0;
      SET @NewStartIDSelect = 0;

      
      TRUNCATE TABLE #tmp_datagpslosted;

      
      SET @MaxMobitelLogID = (SELECT COALESCE(MAX(LogID), 0) 
      FROM dbo.datagps
      WHERE (Mobitel_ID = @MobitelIDInCursor));

      IF @ConfirmedIDInCursor <> @MaxMobitelLogID BEGIN

        SET @DataChanged = 0;
        
        SET @LastMobitelRecord = @MaxMobitelLogID - @LAST_RECORDS_COUNT;

        OPEN CursorLostData;
        FETCH CursorLostData INTO @BeginLogIDInCursor, @EndLogIDInCursor;

        IF  not(@@fetch_status = 0) BEGIN
          SET @DataChanged = 1;
          
          --SET @NewStartIDSelect = GREATEST(@ConfirmedIDInCursor, @LastMobitelRecord);

		   IF @ConfirmedIDInCursor > @LastMobitelRecord
      SET @NewStartIDSelect = @ConfirmedIDInCursor
    ELSE
      SET @NewStartIDSelect = @LastMobitelRecord
        END;

        
        WHILE @@fetch_status = 0 BEGIN
          
          SET  @CountInLostRange = (SELECT COUNT(1)
          FROM dbo.datagps
          WHERE (Mobitel_ID = @MobitelIDInCursor) AND
            (LogID > @BeginLogIDInCursor) AND (LogID < @EndLogIDInCursor));

          
          IF (@CountInLostRange > 0) OR ((@CountInLostRange = 0) AND
            (@BeginLogIDInCursor < @LastMobitelRecord)) BEGIN
            
            DELETE FROM datagpslosted
            WHERE (Mobitel_ID = @MobitelIDInCursor) AND
              (Begin_LogID >= @BeginLogIDInCursor);

            SET @DataChanged = 1;
            SET @NewStartIDSelect = @BeginLogIDInCursor;
          END;

          FETCH CursorLostData INTO @BeginLogIDInCursor, @EndLogIDInCursor;
        END;
        CLOSE CursorLostData;
        
        --SET @DataExists = 1;

        
        IF (@DataChanged = 0) AND (@EndLogIDInCursor < @MaxMobitelLogID) BEGIN
          SET @DataChanged = 1;
          SET @NewStartIDSelect = @EndLogIDInCursor;
        END;

        IF @DataChanged = 1 BEGIN
          
          INSERT INTO #tmp_datagpslosted
          SELECT *
          FROM datagps
          WHERE (Mobitel_ID = @MobitelIDInCursor) AND
            (LogID >= @NewStartIDSelect);

          
          INSERT INTO datagpslosted
          SELECT
            @MobitelIDInCursor AS Mobitel_ID,
            
            dg1.LogID AS Begin_LogID,
            
            dg3.LogID AS End_LogID
          FROM
            
            #tmp_datagpslosted dg1 LEFT JOIN #tmp_datagpslosted dg2 ON (
              (dg2.Mobitel_ID = dg1.Mobitel_ID) AND (dg2.LogID = (dg1.LogID + 1)))
            
            LEFT JOIN #tmp_datagpslosted dg3 ON (
              (dg3.Mobitel_ID = dg1.Mobitel_ID) AND
              
              (dg3.LogID = (
                SELECT MIN(dg.LogID)
                FROM #tmp_datagpslosted dg
                WHERE dg.LogID > dg1.LogID )))
          WHERE
            
            (dg2.LogID IS NULL) AND
            
            (dg3.LogID IS NOT NULL)
          ORDER BY Begin_LogID;

          SET @NeedOptimize = 1;

          
          SET  @NewConfirmedID = (SELECT COALESCE(MIN(Begin_LogID), -1)
          FROM datagpslosted
          WHERE Mobitel_ID = @MobitelIDInCursor);

          
          IF @NewConfirmedID = -1 BEGIN
            SET @NewConfirmedID = @MaxMobitelLogID;
          END;

          
          IF @NewConfirmedID > @ConfirmedIDInCursor BEGIN
            UPDATE mobitels
            SET ConfirmedID = @NewConfirmedID
            WHERE (Mobitel_ID = @MobitelIDInCursor) AND
              (ConfirmedID < @NewConfirmedID);
          END;
         END;
      END;

      FETCH CursorMobitels INTO @MobitelIDInCursor, @ConfirmedIDInCursor;
    END;
    CLOSE CursorMobitels;

	if object_id('tempdb..#tmp_datagpslosted') is not null drop table #tmp_datagpslosted
	  IF @NeedOptimize = 1 begin
    ALTER INDEX ALL ON datagpslosted REORGANIZE
    END;
    
	if object_id('tempdb..#tmp_datagpslosted') is not null drop table #MUTEX_FILL_LOST_DATA_GPS
  BEGIN
    SELECT '1';
  END;
END

GO

/****** Object:  StoredProcedure [dbo].[hystoryOnline]    Script Date: 14.12.2016 15:41:28 ******/
IF OBJECT_ID('hystoryOnline','P') IS NOT NULL
DROP PROCEDURE [dbo].[hystoryOnline]
GO

/****** Object:  StoredProcedure [dbo].[hystoryOnline]    Script Date: 14.12.2016 15:41:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[hystoryOnline]
(
  @id INTEGER
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT LogID
       , Mobitel_Id
       , dbo.FROM_UNIXTIME(unixtime) AS [time]
       , round((Latitude / 600000.00000), 6) AS Lat
       , round((Longitude / 600000.00000), 6) AS Lon
       , Altitude AS Altitude
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + 
	   (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + 
	   (Sensor1 * 72057594037927936)) AS sensor
       , [Events] AS [Events]
       , DataGPS_ID AS 'DataGPS_ID'
  FROM
    [online]
  WHERE
    datagps_id > @id
  ORDER BY
    datagps_id DESC;
END

GO

/****** Object:  StoredProcedure [dbo].[IsTeletrackDatabase]    Script Date: 14.12.2016 15:43:54 ******/
IF OBJECT_ID('IsTeletrackDatabase','P') IS NOT NULL
DROP PROCEDURE [dbo].[IsTeletrackDatabase]
GO

/****** Object:  StoredProcedure [dbo].[IsTeletrackDatabase]    Script Date: 14.12.2016 15:43:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[IsTeletrackDatabase]
--SQL SECURITY INVOKER
--COMMENT '�������� ���� ������ �� ������������ ��������� ������� Teletrack'
AS
BEGIN
  DECLARE @i TINYINT;
END

GO

/****** Object:  StoredProcedure [dbo].[new_proc]    Script Date: 14.12.2016 15:44:53 ******/
IF OBJECT_ID('new_proc','P') IS NOT NULL
DROP PROCEDURE [dbo].[new_proc]
GO

/****** Object:  StoredProcedure [dbo].[new_proc]    Script Date: 14.12.2016 15:44:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[new_proc]
(
  @sensor     INTEGER,
  @m_id       INTEGER,
  @s_id       INTEGER,
  @begin_time DATETIME,
  @end_time   DATETIME
)
--SQL SECURITY INVOKER
--READS SQL DATA
AS
BEGIN
  SELECT round((datagps.Longitude / 600000), 6) AS lon
       , round((datagps.Latitude / 600000), 6) AS lat
       , datagps.Mobitel_ID AS Mobitel_ID
       , sensordata.value AS Value
       , (datagps.Speed * 1.852) AS speed
       , dbo.from_unixtime(datagps.UnixTime) AS [time]
       , sensordata.sensor_id AS sensor_id
       , datagps.DataGps_ID AS datagps_id
       , datagps.Sensor1 & @sensor AS sensor
  FROM
    (sensordata
    JOIN datagps
      ON ((sensordata.datagps_id = datagps.DataGps_ID)))
  WHERE
    (datagps.Valid = 1)
    AND (datagps.UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@begin_time) AND dbo.UNIX_TIMESTAMPS(@end_time))
    AND datagps.Mobitel_ID = @m_id
    AND sensordata.sensor_id = @s_id
  ORDER BY
    datagps.UnixTime
  , datagps.Mobitel_ID
  , sensordata.sensor_id;
END

GO

/****** Object:  StoredProcedure [dbo].[OnCorrectInfotrackLogId64]    Script Date: 14.12.2016 15:47:11 ******/
IF OBJECT_ID('OnCorrectInfotrackLogId64','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnCorrectInfotrackLogId64]
GO

/****** Object:  StoredProcedure [dbo].[OnCorrectInfotrackLogId64]    Script Date: 14.12.2016 15:47:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OnCorrectInfotrackLogId64]
AS
--SQL SECURITY INVOKER
--COMMENT '������������� �������� LogId � InMobitelID � ������ ����������'
BEGIN
  DECLARE @MobitelIDInCursor INT; /* ������� �������� MobitelID  */
  DECLARE @DataExists TINYINT; /* ���� ������� ������ ����� ����� */
  SET @DataExists = 1;
  DECLARE @MaxLogId INT; /* ������������ LogId ��� ������� �������*/
  SET @MaxLogId = 0;
  DECLARE @SrvPacketIDInCursor BIGINT; /* ������� �������� ���������� ������  */
  SET @SrvPacketIDInCursor = 0;

  /* ������ �� ���������� � ��������� ����� ������. 
     ��� ��������� ���������� � ����� I  */
  DECLARE CursorInfoTrack CURSOR LOCAL FOR
  SELECT DISTINCT buf.Mobitel_ID
  FROM
    datagpsbuffer_on64 buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort LIKE 'I%')
    AND (buf.LogId = 0)
  ORDER BY
    1;

  DECLARE CursorInfoTrackUpdateSet CURSOR LOCAL FOR
  SELECT SrvPacketID
  FROM
    datagpsbuffer_on64
  WHERE
    (Mobitel_ID = @MobitelIDInCursor)
    AND (LogId = 0)
  ORDER BY
    UnixTime;

  /*  ������ �� ������ ��������� */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* ������� ������������ LogId ��� ������� ��������� */
    SET @MaxLogId = (SELECT coalesce(max(LogID), 0)
                     FROM
                       DataGps
                     WHERE
                       Mobitel_ID = @MobitelIDInCursor);

    /* ��������� LogId � InMobitelID � ������ ����� ������ �������� ��������� */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      SET @MaxLogId = @MaxLogId + 1;

      UPDATE datagpsbuffer_on64
      SET
        LogId = @MaxLogId, Mobitel_ID = @MaxLogId
      WHERE
        SrvPacketID = @SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    END;
    CLOSE CursorInfoTrackUpdateSet;
    --DEALLOCATE CursorInfoTrackUpdateSet;

    --SET DataExists = 1;
    FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  END;
  CLOSE CursorInfoTrack;
--DEALLOCATE CursorInfoTrack;
END

GO

/****** Object:  StoredProcedure [dbo].[OnCorrectInfotrackLogId]    Script Date: 14.12.2016 15:48:26 ******/
IF OBJECT_ID('OnCorrectInfotrackLogId','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnCorrectInfotrackLogId]
GO

/****** Object:  StoredProcedure [dbo].[OnCorrectInfotrackLogId]    Script Date: 14.12.2016 15:48:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[OnCorrectInfotrackLogId]
AS
--SQL SECURITY INVOKER
--COMMENT '������������� �������� LogId � InMobitelID � ������ ����������'
BEGIN
  DECLARE @MobitelIDInCursor INT; 
  DECLARE @DataExists TINYINT; 
  SET @DataExists = 1;
  DECLARE @MaxLogId INT; 
  SET @MaxLogId = 0;
  DECLARE @SrvPacketIDInCursor BIGINT; 
  SET @SrvPacketIDInCursor = 0;

  DECLARE CursorInfoTrack CURSOR LOCAL FOR
  SELECT DISTINCT buf.Mobitel_ID
  FROM
    datagpsbuffer_on buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort LIKE 'I%')
    AND (buf.LogId = 0)
  ORDER BY
    1;

  DECLARE CursorInfoTrackUpdateSet CURSOR LOCAL FOR
  SELECT SrvPacketID
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelIDInCursor)
    AND (LogId = 0)
  ORDER BY
    UnixTime;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SET @MaxLogId = (SELECT coalesce(max(LogID), 0)
                     FROM
                       DataGps
                     WHERE
                       Mobitel_ID = @MobitelIDInCursor);

    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      SET @MaxLogId = @MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET
        LogId = @MaxLogId, InMobitelID = @MaxLogId
      WHERE
        SrvPacketID = @SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    END;
    CLOSE CursorInfoTrackUpdateSet;

    SET @DataExists = 1;
    FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  END;
  CLOSE CursorInfoTrack;
END

GO

/****** Object:  StoredProcedure [dbo].[OnDeleteDuplicates]    Script Date: 14.12.2016 16:08:03 ******/
IF OBJECT_ID('OnDeleteDuplicates','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnDeleteDuplicates]
GO

/****** Object:  StoredProcedure [dbo].[OnDeleteDuplicates]    Script Date: 14.12.2016 16:08:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OnDeleteDuplicates]
AS
BEGIN
  DECLARE @MobitelIdInCursor INT;
  SET @MobitelIdInCursor = 0;
  DECLARE @LogIdInCursor INT;
  SET @LogIdInCursor = 0;
  DECLARE @MaxSrvPacketId BIGINT;
  SET @MaxSrvPacketId = 0;
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  DECLARE CursorDuplicateGroups CURSOR LOCAL FOR
  SELECT Mobitel_Id
       , LogId
  FROM
    datagpsbuffer_on
  GROUP BY
    Mobitel_Id
  , LogId
  HAVING
    count(LogId) > 1;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET @DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SELECT @MaxSrvPacketId = max(SrvPacketID)
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor)
      AND (LogId = @LogIdInCursor);
    DELETE
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor) AND
      (LogId = @LogIdInCursor) AND
      (SrvPacketID < @MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  END;
  CLOSE CursorDuplicateGroups;
--DEALLOCATE CursorDuplicateGroups;
END

GO

/****** Object:  StoredProcedure [dbo].[OnDeleteDuplicates64]    Script Date: 14.12.2016 16:12:28 ******/
IF OBJECT_ID('OnDeleteDuplicates64','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnDeleteDuplicates64]
GO

/****** Object:  StoredProcedure [dbo].[OnDeleteDuplicates64]    Script Date: 14.12.2016 16:12:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[OnDeleteDuplicates64]
AS
BEGIN
  /* �������� ������������� ����� �� datagpsbuffer_on64' */

  /* ������� MobitelId � ������� CursorDuplicateGroups*/
  DECLARE @MobitelIdInCursor INT;
  SET @MobitelIdInCursor = 0;
  /* ������� LogID � ������� CursorDuplicateGroups  */
  DECLARE @LogIdInCursor INT;
  SET @LogIdInCursor = 0;
  /* Max SrvPacketId � ��������� ����� ������� ������ ���������� */
  DECLARE @MaxSrvPacketId BIGINT;
  SET @MaxSrvPacketId = 0;
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /* ������ ��� ������� �� ���� ������� ���������� */
  DECLARE CursorDuplicateGroups CURSOR LOCAL FOR
  SELECT Mobitel_Id
       , LogId
  FROM
    datagpsbuffer_on64
  GROUP BY
    Mobitel_Id
  , LogId
  HAVING
    count(LogId) > 1;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET @DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SELECT @MaxSrvPacketId = max(SrvPacketID)
    FROM
      datagpsbuffer_on64
    WHERE
      (Mobitel_ID = @MobitelIdInCursor)
      AND (LogId = @LogIdInCursor);
    DELETE
    FROM
      datagpsbuffer_on64
    WHERE
      (Mobitel_ID = @MobitelIdInCursor) AND
      (LogId = @LogIdInCursor) AND
      (SrvPacketID < @MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  END;
  CLOSE CursorDuplicateGroups;
--DEALLOCATE CursorDuplicateGroups;
END

GO

/****** Object:  StoredProcedure [dbo].[OnDeleteExistingRecords64]    Script Date: 14.12.2016 16:12:28 ******/
IF OBJECT_ID('OnDeleteExistingRecords64','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnDeleteExistingRecords64]
GO

/****** Object:  StoredProcedure [dbo].[OnDeleteExistingRecords64]    Script Date: 14.12.2016 16:12:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OnDeleteExistingRecords64]
  --COMMENT '�������� �� datagpsbuffer_on64 �������, ��� ������������� � data'
  as
BEGIN

  /* ������� MobitelId � ������� CursorExistingRecords */
  DECLARE @MobitelIdInCursor int;
  SET @MobitelIdInCursor = 0;
  /* ������� SrvPacketId � ������� CursorExistingRecords */
  DECLARE @SrvPacketIdInCursor int;
  SET @SrvPacketIdInCursor = 0;

  DECLARE @DataExists tinyint;
  SET @DataExists = 1;

  /* ������ ��� ������� �� ���� ������� */
  DECLARE CursorExistingRecords CURSOR FOR
  SELECT
    datagpsbuffer_on64.Mobitel_ID,
    datagpsbuffer_on64.SrvPacketID
  FROM datagpsbuffer_on64
    INNER JOIN datagps64
      ON datagpsbuffer_on64.Mobitel_ID = datagps64.Mobitel_ID
      AND datagpsbuffer_on64.SrvPacketID = datagps64.SrvPacketID
  GROUP BY datagpsbuffer_on64.Mobitel_ID,
           datagpsbuffer_on64.SrvPacketID;

 -- DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET @DataExists = 1;
  OPEN CursorExistingRecords;
  FETCH CursorExistingRecords INTO @MobitelIdInCursor, @SrvPacketIdInCursor;
  WHILE @@fetch_status = 0 BEGIN
    DELETE
      FROM datagpsbuffer_on64
    WHERE (Mobitel_ID = @MobitelIdInCursor)
      AND (SrvPacketID = @SrvPacketIdInCursor);
    FETCH CursorExistingRecords INTO @MobitelIdInCursor, @SrvPacketIdInCursor;
  END;
  CLOSE CursorExistingRecords;
END

GO

/****** Object:  StoredProcedure [dbo].[OnDeleteLostRange]    Script Date: 14.12.2016 16:22:49 ******/
IF OBJECT_ID('OnDeleteLostRange','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnDeleteLostRange];
GO

/****** Object:  StoredProcedure [dbo].[OnDeleteLostRange]    Script Date: 14.12.2016 16:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OnDeleteLostRange](  @MobitelID int, @BeginSrvPacketID BIGINT)
--COMMENT '�������� ���������� ��������� ��������� �� datagpslost_on.'
AS
BEGIN

  DECLARE @MinBeginSrvPacketID BIGINT; -- 
  DECLARE @NewConfirmedID INT; -- 

  SELECT @MinBeginSrvPacketID = min(Begin_SrvPacketID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @MinBeginSrvPacketID IS NOT NULL
  BEGIN
    IF @MinBeginSrvPacketID = @BeginSrvPacketID
    BEGIN
      SELECT @NewConfirmedID = End_LogID
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (Begin_SrvPacketID = @BeginSrvPacketID);
    END; -- IF

    DELETE
    FROM
      datagpslost_on
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (Begin_SrvPacketID = @BeginSrvPacketID);

    IF @MinBeginSrvPacketID = @BeginSrvPacketID
    BEGIN
      UPDATE mobitels
      SET
        ConfirmedID = @NewConfirmedID
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (@NewConfirmedID > ConfirmedID);
    END; -- IF
  END; -- IF  
END

GO

/****** Object:  StoredProcedure [dbo].[OnFullRefreshLostRanges]    Script Date: 14.12.2016 16:28:39 ******/
IF OBJECT_ID('OnFullRefreshLostRanges','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnFullRefreshLostRanges]
GO

/****** Object:  StoredProcedure [dbo].[OnFullRefreshLostRanges]    Script Date: 14.12.2016 16:28:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[OnFullRefreshLostRanges]
AS
--NOT DETERMINISTIC
--CONTAINS SQL
--SQL SECURITY INVOKER
--COMMENT '����� ���� ��������� ��� TDataManager'
BEGIN
  DECLARE @MobitelIDInCursor INT; /* ������� �������� MobitelID */
  --DECLARE @DataExists TINYINT; /* ���� ������� ������ ����� ����� */
  --SET @DataExists = 1;

  DECLARE @LogIdInCursor INT; /* ������� �������� LogId ��������� */
  DECLARE @SrvPacketIdInCursor BIGINT; /* ������� �������� SrvPacketID ��������� */
  DECLARE @PreviousLogId INT; /* �������� LogId ��������� �� ���������� ����*/
  DECLARE @PreviousSrvPacketId BIGINT; /* �������� SrvPacketID ��������� �� ���������� ���� */

  DECLARE @FirstIteration TINYINT; /* ���� ������ �������� �� ������ */
  SET @FirstIteration = 1;
  DECLARE @NewRowCount INT; /* ���������� ����� ������� � ��������� */
  SET @NewRowCount = 0;
  DECLARE @NewConfirmedID INT; /* ����� �������� ConfirmedID */

  DECLARE @MaxNegativeDataGpsId INT; /* ������������ DataGpsId � ��������� ������������� LogId */
  DECLARE @MaxPositiveDataGpsId INT; /* ������������ DataGpsId � ��������� ������������� LogId */
  DECLARE @InsertLostDataStatement NVARCHAR;

  /* ������ �� ���� ���������� */
  DECLARE CursorMobitels CURSOR FOR
  SELECT DISTINCT (Mobitel_ID) AS Mobitel_ID
  FROM
    mobitels
  ORDER BY
    Mobitel_ID;

  /* ������ �� ������ ��������� */
  DECLARE CursorDataGps CURSOR FOR
  SELECT LogId
       , SrvPacketID
  FROM
    DataGps
  WHERE
    Mobitel_ID = @MobitelIDInCursor
  ORDER BY
    LogId;

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* ������� ������, ��������� ������ ��������� ��� TDataManager  */
  TRUNCATE TABLE datagpsbuffer_on;
  TRUNCATE TABLE datagpslost_on;
  TRUNCATE TABLE datagpslost_ontmp;

  /* ------------------- FETCH Mobitels ------------------------ */
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* ��������� ��� ������� �������  */
    SET @InsertLostDataStatement = 'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';

    /* ----------------- FETCH DataGPS --------------------- */
    SET @FirstIteration = 1;
    SET @NewRowCount = 0;

    OPEN CursorDataGps;
    FETCH CursorDataGps INTO @LogIdInCursor, @SrvPacketIdInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstIteration = 1
      BEGIN
        /* ������ �������� � ������ */
        SET @PreviousLogId = @LogIdInCursor;
        SET @PreviousSrvPacketId = @SrvPacketIdInCursor;
        SET @FirstIteration = 0;
      END
      ELSE /* �������� ����� �������*/
      BEGIN
        IF (((@LogIdInCursor - @PreviousLogId) > 1) AND (@PreviousSrvPacketId < @SrvPacketIdInCursor))
        BEGIN
          /* ������ ������ */
          IF @NewRowCount > 0
          BEGIN
            SET @InsertLostDataStatement = @InsertLostDataStatement + ',';
          END; --END IF;

          SET @InsertLostDataStatement = @InsertLostDataStatement + ' (' + @MobitelIDInCursor + ', ' + @PreviousLogId + ', ' + @LogIdInCursor + ', ' + @PreviousSrvPacketId + ', ' + @SrvPacketIdInCursor + ')';

          SET @NewRowCount = @NewRowCount + 1;
        END; --END IF;

        /* ���������� ������� �������� � ���������� */
        SET @PreviousLogId = @LogIdInCursor;
        SET @PreviousSrvPacketId = @SrvPacketIdInCursor;
      END; --END IF; -- IF @FirstIteration = 1 

      FETCH CursorDataGps INTO @LogIdInCursor, @SrvPacketIdInCursor;
    END; --END WHILE; --  WHILE DataExistsInner = 1 
    CLOSE CursorDataGps;

    /* ���� � ������� ��������� ���� ����������� ������ - 
       ����� ConfirmedID ����� ����� ������������ Begin_LogID 
       �� ������� ���������. 
       ����� - ������������� LogID ������� ��������� �� 
       ������� datagps � ����� �� ���������� ������ ���� ��� 
       ������ ����, � ����������� �� ������� LogId. */
    IF @NewRowCount > 0
    BEGIN
      /* ���������� ������� ����������� ������ */
      EXEC (@InsertLostDataStatement);

      SET @NewConfirmedID = (SELECT min(Begin_LogID)
                             FROM
                               datagpslost_on
                             WHERE
                               Mobitel_ID = @MobitelIDInCursor);
    END
    ELSE
    BEGIN
      /* � ���� ������ �� ��� ��� �������.
         ���� ���� ������������� �������� LogID, �� ��� �����������
         ������ �������� ConfirmedID ��������� �������������� ������:
         ��������� ����� �������� LogID ������������� ��� �������������
         ����� ����� (�� DataGpsId). */
      IF EXISTS (SELECT TOP 1 1
                 FROM
                   dbo.datagps
                 WHERE
                   (Mobitel_ID = @MobitelIDInCursor)
                   AND (LogId < 0))
      BEGIN
        SET @MaxNegativeDataGpsId = (SELECT max(DataGps_ID)
                                     FROM
                                       DataGps
                                     WHERE
                                       (Mobitel_ID = @MobitelIDInCursor)
                                       AND (LogId < 0));

        SET @MaxPositiveDataGpsId = (SELECT coalesce(max(DataGps_ID), 0)
                                     FROM
                                       DataGps
                                     WHERE
                                       (Mobitel_ID = @MobitelIDInCursor)
                                       AND (LogId >= 0));

        IF (@MaxNegativeDataGpsId > @MaxPositiveDataGpsId)
        BEGIN
          SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                                 FROM
                                   datagps
                                 WHERE
                                   (Mobitel_ID = @MobitelIDInCursor)
                                   AND (LogId < 0));
        END
        ELSE
        BEGIN
          SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                                 FROM
                                   datagps
                                 WHERE
                                   (Mobitel_ID = @MobitelIDInCursor)
                                   AND (LogId >= 0));
        END --END IF; -- IF (@MaxNegativeDataGpsId > @MaxPositiveDataGpsId)
      END
      ELSE
      BEGIN
        /* ���������� ������ ������������� LogId */
        SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                               FROM
                                 datagps
                               WHERE
                                 Mobitel_ID = @MobitelIDInCursor);
      END; --END IF; -- IF EXISTS 
    END; --END IF; -- IF @NewRowCount > 0

    /* ������� ConfirmedID */
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    --SET DataExists = 1;
    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END; --END WHILE;
  CLOSE CursorMobitels;
END;

GO

/****** Object:  StoredProcedure [dbo].[OnInsertDatagpsLost]    Script Date: 14.12.2016 16:30:02 ******/
IF OBJECT_ID('OnInsertDatagpsLost','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnInsertDatagpsLost]
GO

/****** Object:  StoredProcedure [dbo].[OnInsertDatagpsLost]    Script Date: 14.12.2016 16:30:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[OnInsertDatagpsLost]
(
  @MobitelID        INT,
  @BeginLogID       INT,
  @EndLogID         INT,
  @BeginSrvPacketID BIGINT,
  @EndSrvPacketID   BIGINT
)
AS
--SQL SECURITY INVOKER
--COMMENT '������� ������ � ������� datagpslost_on.'
BEGIN
  DECLARE @MobitelId_BeginLogID_Exists TINYINT;
  SET @MobitelId_BeginLogID_Exists = 0;
  DECLARE @MobitelId_BeginSrvPacketID_Exists TINYINT;
  SET @MobitelId_BeginSrvPacketID_Exists = 0;

  DECLARE @RowBeginSrvPacketID BIGINT;

  IF EXISTS (SELECT NULL
             FROM
               datagpslost_on
             WHERE
               (Mobitel_ID = @MobitelID)
               AND (Begin_LogID = @BeginLogID))
  BEGIN
    SET @MobitelId_BeginLogID_Exists = 1;
  END; -- IF

  IF EXISTS (SELECT NULL
             FROM
               datagpslost_on
             WHERE
               (Mobitel_ID = @MobitelID)
               AND (Begin_SrvPacketID = @BeginSrvPacketID))
  BEGIN
    SET @MobitelId_BeginSrvPacketID_Exists = 1;
  END; -- IF

  IF (@MobitelId_BeginLogID_Exists = 0) AND (@MobitelId_BeginSrvPacketID_Exists = 0)
  BEGIN
    /* ??????? ????? ?????? */
    INSERT INTO datagpslost_on(Mobitel_ID
                             , Begin_LogID
                             , End_LogID
                             , Begin_SrvPacketID
                             , End_SrvPacketID)
    VALUES
      (@MobitelID, @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID);
  END
  ELSE
  IF ((@MobitelId_BeginLogID_Exists = 1) AND (@MobitelId_BeginSrvPacketID_Exists = 1))
  BEGIN
    UPDATE datagpslost_on
    SET
      End_LogID = @EndLogID, End_SrvPacketID = @EndSrvPacketID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (Begin_LogID = @BeginLogID);
  END
  ELSE
  BEGIN
    SET @RowBeginSrvPacketID = (SELECT TOP 1 Begin_SrvPacketID
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (Begin_LogID = @BeginLogID));

    IF ((@MobitelId_BeginLogID_Exists = 1) AND (@MobitelId_BeginSrvPacketID_Exists = 0) AND (@BeginSrvPacketID < @RowBeginSrvPacketID))
    BEGIN
      UPDATE datagpslost_on
      SET
        End_LogID = @EndLogID, Begin_SrvPacketID = @BeginSrvPacketID, End_SrvPacketID = @EndSrvPacketID
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);
    END; -- IF
  END; -- ELSE
END
--END


GO

/****** Object:  StoredProcedure [dbo].[online_table]    Script Date: 14.12.2016 16:36:41 ******/
IF OBJECT_ID('online_table','P') IS NOT NULL
DROP PROCEDURE [dbo].[online_table]
GO

/****** Object:  StoredProcedure [dbo].[online_table]    Script Date: 14.12.2016 16:36:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[online_table]
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT Log_ID
       , MobitelId
       , (SELECT TOP 1 DataGPS_ID
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS 'DataGPS_ID'
       , (SELECT TOP 1 dbo.from_unixtime(unixtime)
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS 'time'
       , (SELECT TOP 1 round((Latitude / 600000.00000), 6)
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lat
       , (SELECT TOP 1 round((Longitude / 600000.00000), 6)
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lon
       , (SELECT TOP 1 Altitude
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Altitude
       , (SELECT TOP 1 (Speed * 1.852)
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS speed
       , (SELECT TOP 1 ((Direction * 360) / 255)
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS direction
       , (SELECT TOP 1 Valid
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Valid
       , (SELECT TOP 1 (Sensor8 + (Sensor7 * 256) + 
	   (Sensor6 * 65536) + 
	   (cast(Sensor5 AS BIGINT) * 16777216) + 
	   (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + 
	   (Sensor2 * 281474976710656) + 
	   (Sensor1 * 72057594037927936))
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS sensor
       , (SELECT TOP 1 [Events]
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS [Events]

  FROM
    (SELECT max(LogID) AS Log_ID
          , mobitel_id AS MobitelId
     FROM
       [online]
     GROUP BY
       Mobitel_ID) T1;
END

GO

/****** Object:  StoredProcedure [dbo].[OnListMobitelConfig]    Script Date: 14.12.2016 16:42:55 ******/
IF OBJECT_ID('OnListMobitelConfig','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnListMobitelConfig]
GO

/****** Object:  StoredProcedure [dbo].[OnListMobitelConfig]    Script Date: 14.12.2016 16:42:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OnListMobitelConfig]
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  DECLARE @DevIdShortInCursor CHAR(4);
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /*  */
  DECLARE CursorMobitels CURSOR FOR
  SELECT m.Mobitel_ID AS MobitelID
       , c.devIdShort AS DevIdShort
  FROM
    mobitels m
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort IS NOT NULL)
    AND (c.devIdShort <> '')
  ORDER BY
    m.Mobitel_ID;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /*  */
  if object_id(N'tempdb..#tmpMobitelsConfig') is not null drop table #tmpMobitelsConfig;

CREATE TABLE #tmpMobitelsConfig(
  MobitelID INT NOT NULL,
  DevIdShort CHAR(4) NOT NULL,
  LastPacketID BIGINT NOT NULL
);

OPEN CursorMobitels;
FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
WHILE @@fetch_status = 0
BEGIN
  INSERT INTO #tmpMobitelsConfig
  SELECT @MobitelIDInCursor
       , @DevIdShortInCursor
       , (SELECT coalesce(max(SrvPacketID), 0)
          FROM
            datagps
          WHERE
            Mobitel_ID = @MobitelIDInCursor) AS LastPacketID;

  FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
END;
CLOSE CursorMobitels;
--DEALLOCATE CursorMobitels;

SELECT MobitelID
     , DevIdShort
     , LastPacketID
FROM
  #tmpMobitelsConfig;

if object_id(N'tempdb..#tmpMobitelsConfig') is not null drop table #tmpMobitelsConfig;
END


GO

/****** Object:  StoredProcedure [dbo].[OnListMobitelConfig64]    Script Date: 14.12.2016 16:42:55 ******/
IF OBJECT_ID('OnListMobitelConfig64','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnListMobitelConfig64]
GO

/****** Object:  StoredProcedure [dbo].[OnListMobitelConfig64]    Script Date: 14.12.2016 16:42:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[OnListMobitelConfig64]
  -- COMMENT '������� �������� ���������� ������� ����� �������� � online ������ ��� datagps � datagps64'
  AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  DECLARE @Is64Cursor tinyint;
  DECLARE @DevIdShortInCursor CHAR(4);

  /* ������ � ��������� ����������� ���������� */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS MobitelID, m.Is64bitPackets AS Is64, c.devIdShort AS DevIdShort
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT TOP 1 intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID)
      ORDER BY intConf.InternalMobitelConfig_ID DESC)) AND
      (c.devIdShort IS NOT NULL) AND (c.devIdShort <> '')
    ORDER BY m.Mobitel_ID;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

if object_id(N'tempdb..#tmpMobitelsConfig64') is not null drop table #tmpMobitelsConfig64  -- �������� �� ������������� �������


  /* ��������� ������� �������� ��������� */
  create table #tmpMobitelsConfig64 -- �������� ��������� �������
    (
    MobitelID INT NOT NULL,
    Is64 tinyint NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL,
    LastPacketID64 BIGINT NOT NULL);

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor, @Is64Cursor, @DevIdShortInCursor;
  WHILE  @@fetch_status = 0 begin
    INSERT INTO #tmpMobitelsConfig64 SELECT @MobitelIDInCursor, @Is64Cursor, @DevIdShortInCursor,
      (SELECT COALESCE(MAX(d.SrvPacketID), 0)
       FROM datagps d
       WHERE d.Mobitel_ID = @MobitelIDInCursor) AS LastPacketID,
      (SELECT COALESCE(MAX(t.SrvPacketID), 0)
       FROM datagps64 t
       WHERE t.Mobitel_ID = @MobitelIDInCursor) AS LastPacketID64;

    FETCH CursorMobitels INTO @MobitelIDInCursor, @Is64Cursor, @DevIdShortInCursor;
  END;
  CLOSE CursorMobitels;

  SELECT MobitelID, Is64, DevIdShort, LastPacketID, LastPacketID64
  FROM #tmpMobitelsConfig64;

  if object_id(N'tempdb..#tmpMobitelsConfig64') is not null drop table #tmpMobitelsConfig64  -- �������� �� ������������� �������;
END

GO

/****** Object:  StoredProcedure [dbo].[OnLostDataGPS]    Script Date: 14.12.2016 16:56:06 ******/
IF OBJECT_ID('OnLostDataGPS','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnLostDataGPS]
GO

/****** Object:  StoredProcedure [dbo].[OnLostDataGPS]    Script Date: 14.12.2016 16:56:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[OnLostDataGPS]
(
  @MobitelID    INT,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT '����� ��������� � online ������ ��������� ��������� � ����������'
BEGIN

  /********** DECLARE **********/

  DECLARE @CurrentConfirmedID INT; 
  SET @CurrentConfirmedID = 0;
  DECLARE @NewConfirmedID INT; 
  SET @NewConfirmedID = 0;
  DECLARE @StartLogIDSelect INT;
  SET @StartLogIDSelect = 0;
  DECLARE @FinishLogIDSelect INT;
  SET @FinishLogIDSelect = 0;
  DECLARE @MaxEndLogID INT; 
  SET @MaxEndLogID = 0;

  DECLARE @MinLogID_n INT; 
  SET @MinLogID_n = 0;
  DECLARE @MaxLogID_n INT; 
  SET @MaxLogID_n = 0;

  DECLARE @DataExists TINYINT; 
  SET @DataExists = 1;

  DECLARE @NewLogID INT;
  DECLARE @NewSrvPacketID BIGINT; 
  DECLARE @FirstLogID INT;
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; 
  DECLARE @FirstSrvPacketID BIGINT; 
  DECLARE @SecondSrvPacketID BIGINT; 

  DECLARE @tmpSrvPacketID BIGINT;
  SET @tmpSrvPacketID = 0;

  DECLARE CursorNewLogID CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  SET @CurrentConfirmedID = (SELECT coalesce(ConfirmedID, 0)
                             FROM
                               mobitels
                             WHERE
                               Mobitel_ID = @MobitelID);


  SELECT @MinLogID_n = min(LogID)
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelID)
    AND (LogID > @CurrentConfirmedID);


  SET @MaxLogID_n = (SELECT max(LogID)
                     FROM
                       datagpsbuffer_on
                     WHERE
                       Mobitel_ID = @MobitelID);


  SELECT @MaxEndLogID = coalesce(max(End_LogID), 0)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;


  IF @MinLogID_n IS NOT NULL
  BEGIN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF @MinLogID_n < @MaxEndLogID
    BEGIN
     
      SET @StartLogIDSelect = (SELECT coalesce(Begin_LogID, @MinLogID_n)
                               FROM
                                 datagpslost_on
                               WHERE
                                 (Mobitel_ID = @MobitelID)
                                 AND (@MinLogID_n > Begin_LogID)
                                 AND (@MinLogID_n < End_LogID));


      SET @FinishLogIDSelect = (SELECT coalesce(End_LogID, @MaxLogID_n)
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (@MaxLogID_n > Begin_LogID)
                                  AND (@MaxLogID_n < End_LogID));

   
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID >= @StartLogIDSelect) AND
        (Begin_LogID <= @FinishLogIDSelect);

    
      IF (@StartLogIDSelect = 0) AND (NOT EXISTS (SELECT 1
                                                  FROM
                                                    datagps
                                                  WHERE
                                                    (Mobitel_ID = @MobitelID)
                                                    AND (LogID = @StartLogIDSelect)))
      BEGIN
        INSERT INTO datagpslost_ontmp(LogID
                                    , SrvPacketID)
        VALUES
          (0, 0);
      END;
    END;
    ELSE
    BEGIN

   
      SELECT @StartLogIDSelect = coalesce(max(End_LogID), @CurrentConfirmedID)
      FROM
        datagpslost_on
      WHERE
        Mobitel_ID = @MobitelID;

    
      SELECT @FinishLogIDSelect = max(LogID)
      FROM
        datagpsbuffer_on
      WHERE
        Mobitel_ID = @MobitelID;

    
      SELECT @tmpSrvPacketID = coalesce(SrvPacketID, 0)
      FROM
        datagps
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (LogID = @StartLogIDSelect);

     
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@StartLogIDSelect, @tmpSrvPacketID);
    END; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

  
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT LogID
         , SrvPacketID
    FROM
      datagps
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (LogID >= @StartLogIDSelect)
      AND (LogID <= @FinishLogIDSelect)
    ORDER BY
      LogID;

    
    --SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstLogID = -9999999
      BEGIN
      
        SET @FirstLogID = @NewLogID;
        SET @FirstSrvPacketID = @NewSrvPacketID;
      END
      ELSE
      BEGIN
      
        SET @SecondLogID = @NewLogID;
        SET @SecondSrvPacketID = @NewSrvPacketID;

        IF (@SecondLogID - @FirstLogID) > 1
        BEGIN
         
          EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

          IF @NeedOptimize = 0
          BEGIN
            SET @NeedOptimize = 1;
          END; -- IF

        END; -- IF
       
        SET @FirstLogID = @SecondLogID;
        SET @FirstSrvPacketID = @SecondSrvPacketID;
      END; -- IF
      FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    END; -- WHILE
    CLOSE CursorNewLogID;
    --DEALLOCATE CursorNewLogID; 

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

   
    SET @NewConfirmedID = (SELECT min(Begin_LogID)
                           FROM
                             datagpslost_on
                           WHERE
                             Mobitel_ID = @MobitelID);

    IF @NewConfirmedID IS NULL
    BEGIN
      SET @NewConfirmedID = (SELECT max(LogID)
                             FROM
                               datagps
                             WHERE
                               Mobitel_ID = @MobitelID);
    END;

   
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (ConfirmedID < @NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END; -- IF MaxEndLogID IS NOT NULL  
END



GO

/****** Object:  StoredProcedure [dbo].[OnLostDataGPS2]    Script Date: 14.12.2016 17:19:20 ******/
IF OBJECT_ID('OnLostDataGPS2','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnLostDataGPS2]
GO

/****** Object:  StoredProcedure [dbo].[OnLostDataGPS2]    Script Date: 14.12.2016 17:19:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[OnLostDataGPS2]
(
  @MobitelID    INTEGER,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT '����������� ���������� ���������. ������ ����������� ������ ����'
BEGIN
  /********** DECLARE **********/

  DECLARE @NewConfirmedID INT;
  SET @NewConfirmedID = 0; /* ????? ???????? ConfirmedID */
  DECLARE @BeginLogID INT; /* Begin_LogID ? ??????? CursorLostRanges */
  DECLARE @EndLogID INT; /* End_LogID ? ??????? CursorLostRanges */
  DECLARE @BeginSrvPacketID BIGINT; /* Begin_SrvPacketID ? ??????? CursorLostRanges */
  DECLARE @EndSrvPacketID BIGINT; /* End_SrvPacketID ? ??????? CursorLostRanges */
  DECLARE @RowCountForAnalyze INT; /* ?????????? ??????? ??? ??????? */
  SET @RowCountForAnalyze = 0;
  DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  SET @DataExists = 1;
  DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
  DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
  DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
  DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
  DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

  /* ?????? ??? ??????? ?? ?????????? ????????? ????????? */
  DECLARE CursorLostRanges CURSOR LOCAL FOR
  SELECT Begin_LogID
       , End_LogID
       , Begin_SrvPacketID
       , End_SrvPacketID
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* ?????? ??? ??????? ?? ????? LogID */
  DECLARE CursorNewLogID2 CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp
  ORDER BY
    SrvPacketID;

  /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* ?????????? ????????? ? ????????? ??????? ???? ??? ???????? */
  SET @DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;

  WHILE @@fetch_status = 0
  BEGIN
  
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT DISTINCT LogID
                  , SrvPacketID
    FROM
      datagpsbuffer_ontmp
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (SrvPacketID > @BeginSrvPacketID)
      AND (SrvPacketID < @EndSrvPacketID)
    ORDER BY
      SrvPacketID;

    /* ???? ???? ?????? ???????????? ???????? */
    SET @RowCountForAnalyze = (SELECT count(1)
                               FROM
                                 datagpslost_ontmp);

    IF @RowCountForAnalyze > 0
    BEGIN
      /* ????????? ?????? ? ????????? ?????? ??? ??????? */
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@BeginLogID, @BeginSrvPacketID)
      , (@EndLogID, @EndSrvPacketID);

      /* ??????? ?????? ???????? */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        --DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
        --DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
        --DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --SET @FirstLogID = -9999999;
        --DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
        --DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

        /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
        --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* ???????????? ???????-??????? ? ??????? ??????????? ?????????? */
        --SET DataExists = 1;
        OPEN CursorNewLogID2;
        FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;

        WHILE @@fetch_status = 0
        BEGIN
          IF @FirstLogID = -9999999
          BEGIN
            /* ?????? ???????? ? ?????? */
            SET @FirstLogID = @NewLogID;
            SET @FirstSrvPacketID = @NewSrvPacketID;
          END
          ELSE
          BEGIN
            /* ???????? ????? ???????*/
            SET @SecondLogID = @NewLogID;
            SET @SecondSrvPacketID = @NewSrvPacketID;

            IF (@SecondSrvPacketID - @FirstSrvPacketID) > 1
            BEGIN
              /* ?????? ?????? */
              EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

              IF @NeedOptimize = 0
              BEGIN
                SET @NeedOptimize = 1;
              END; -- IF

            END; -- IF
            /* ?????????? ??????? ?????? ???????? ?? ?????? ???????
               ??? ??????? ? ????????? ???????? ? ???????? ?????????? ???????? */
            SET @FirstLogID = @SecondLogID;
            SET @FirstSrvPacketID = @SecondSrvPacketID;
          END; -- ELSE  
          FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;
        END; -- WHILE;
        CLOSE CursorNewLogID2;
      --DEALLOCATE CursorNewLogID2;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END;

    FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;
  END;
  CLOSE CursorLostRanges;
  --DEALLOCATE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* ???? ??????????? ??????? ? ??????? ????????? ??? - 
     ????? ConfirmedID ????? ????? ????????????? LogID 
     ??????? ?????????. ????? - ??????????? Begin_LogID
     ?? ??????? ????????? */
  SELECT @NewConfirmedID = min(Begin_LogID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @NewConfirmedID IS NULL
  BEGIN
    SET @NewConfirmedID = (SELECT max(LogID)
                           FROM
                             datagps
                           WHERE
                             Mobitel_ID = @MobitelID);
  END;

  /* ??????? ConfirmedID ???? ???? */
  UPDATE mobitels
  SET
    ConfirmedID = @NewConfirmedID
  WHERE
    (Mobitel_ID = @MobitelID) AND
    (ConfirmedID < @NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END


GO

/****** Object:  StoredProcedure [dbo].[OnLostDataGPS264]    Script Date: 15.12.2016 11:43:49 ******/
IF OBJECT_ID('OnLostDataGPS264','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnLostDataGPS264]
GO

/****** Object:  StoredProcedure [dbo].[OnLostDataGPS264]    Script Date: 15.12.2016 11:43:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[OnLostDataGPS264]
(
  @MobitelID    INTEGER,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT '����������� ���������� ���������. ������ ����������� ������ ��������� ���������.'
BEGIN
  /********** DECLARE **********/

  DECLARE @NewConfirmedID INT;
  SET @NewConfirmedID = 0; /* ����� �������� ConfirmedID  */
  DECLARE @BeginLogID INT; /* Begin_LogID � ������� CursorLostRanges */
  DECLARE @EndLogID INT; /* End_LogID � ������� CursorLostRanges */
  DECLARE @BeginSrvPacketID BIGINT; /*  Begin_SrvPacketID � ������� CursorLostRanges */
  DECLARE @EndSrvPacketID BIGINT; /* End_SrvPacketID � ������� CursorLostRanges  */
  DECLARE @RowCountForAnalyze INT; /* ���������� ������� ��� �������  */
  SET @RowCountForAnalyze = 0;
  DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  SET @DataExists = 1;
  DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
  DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
  DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
  DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
  DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

  /* ������ ��� ������� �� ���������� ��������� ��������� */
  DECLARE CursorLostRanges CURSOR LOCAL FOR
  SELECT Begin_LogID
       , End_LogID
       , Begin_SrvPacketID
       , End_SrvPacketID
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* ������ ��� ������� �� ����� LogID */
  DECLARE CursorNewLogID2 CURSOR FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp
  ORDER BY
    SrvPacketID;

  /* ���������� ������������� ������ ��� FETCH ��������� ������*/
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* ���������� ��������� � ��������� ������� ���� ��� ��������  */
  SET @DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;

  WHILE @@fetch_status = 0
  BEGIN
    /* ���������� ��������� ������� ������������ ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT DISTINCT LogID
                  , SrvPacketID
    FROM
      datagpsbuffer_ontmp64
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (SrvPacketID > @BeginSrvPacketID)
      AND (SrvPacketID < @EndSrvPacketID)
    ORDER BY
      SrvPacketID;

     /* ���� ���� ������ ������������ �������� */  
    SET @RowCountForAnalyze = (SELECT count(1)
                               FROM
                                 datagpslost_ontmp);

    IF @RowCountForAnalyze > 0
    BEGIN
      /* ��������� ������ � ��������� ������ ��� ������� */ 
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@BeginLogID, @BeginSrvPacketID)
      , (@EndLogID, @EndSrvPacketID);

       /* ������� ������ �������� */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        --DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
        --DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
        --DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --SET @FirstLogID = -9999999;
        --DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
        --DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

        /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
        --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* ������������ �������-������� � ������� ����������� ���������� */
        --SET DataExists = 1;
        OPEN CursorNewLogID2;
        FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;

        WHILE @@fetch_status = 0
        BEGIN
          IF @FirstLogID = -9999999
          BEGIN
            /* ������ �������� � ������ */
            SET @FirstLogID = @NewLogID;
            SET @FirstSrvPacketID = @NewSrvPacketID;
          END
          ELSE
          BEGIN
            /* �������� ����� �������*/
            SET @SecondLogID = @NewLogID;
            SET @SecondSrvPacketID = @NewSrvPacketID;

            IF (@SecondSrvPacketID - @FirstSrvPacketID) > 1
            BEGIN
              /* ������ ������ */
              EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

              IF @NeedOptimize = 0
              BEGIN
                SET @NeedOptimize = 1;
              END; -- IF

            END; -- IF
            /* ���������� ������� ������ �������� �� ������ �������
               ��� ������� � ��������� �������� � �������� ���������� �������� */
            SET @FirstLogID = @SecondLogID;
            SET @FirstSrvPacketID = @SecondSrvPacketID;
          END; -- ELSE  
          FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;
        END; -- WHILE;
        CLOSE CursorNewLogID2;
      --DEALLOCATE CursorNewLogID2;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END;

    FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;
  END;
  CLOSE CursorLostRanges;
  --DEALLOCATE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* ���� ����������� ������� � ������� ��������� ��� - 
     ����� ConfirmedID ����� ����� ������������� LogID 
     ������� ���������. ����� - ����������� Begin_LogID
     �� ������� ��������� */
  SELECT @NewConfirmedID = min(Begin_LogID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @NewConfirmedID IS NULL
  BEGIN
    SET @NewConfirmedID = (SELECT max(LogID)
                           FROM
                             datagps64
                           WHERE
                             Mobitel_ID = @MobitelID);
  END;

  /* ������� ConfirmedID ���� ���� */
  UPDATE mobitels
  SET
    ConfirmedID = @NewConfirmedID
  WHERE
    (Mobitel_ID = @MobitelID) AND
    (ConfirmedID < @NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END

GO

/****** Object:  StoredProcedure [dbo].[OnLostDataGPS64]    Script Date: 15.12.2016 11:54:50 ******/
IF OBJECT_ID('OnLostDataGPS64','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnLostDataGPS64]
GO

/****** Object:  StoredProcedure [dbo].[OnLostDataGPS64]    Script Date: 15.12.2016 11:54:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[OnLostDataGPS64]
(
  @MobitelID    INT,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT '����� ��������� � online ������ ��������� ��������� � ����������'
BEGIN

  /********** DECLARE **********/

  DECLARE @CurrentConfirmedID INT; /* ������� �������� ConfirmedID  */
  SET @CurrentConfirmedID = 0;
  DECLARE @NewConfirmedID INT; /* ����� �������� ConfirmedID */
  SET @NewConfirmedID = 0;
  DECLARE @StartLogIDSelect INT; /* min LogID ��� ������� */
  SET @StartLogIDSelect = 0;
  DECLARE @FinishLogIDSelect INT; /* max LogID ��� ������� */
  SET @FinishLogIDSelect = 0;
  DECLARE @MaxEndLogID INT; /* max End_LogID � ����������� ���������� ������� ��������� */
  SET @MaxEndLogID = 0;

  DECLARE @MinLogID_n INT; /* ����������� LogID ����� ConfirmedID � ��������� ����� ������ */
  SET @MinLogID_n = 0;
  DECLARE @MaxLogID_n INT; /* ������������ LogID � ��������� ����� ������ */
  SET @MaxLogID_n = 0;

  DECLARE @DataExists TINYINT; /*  ���� ������� ������ ����� ����� */
  SET @DataExists = 1;

  DECLARE @NewLogID INT; /* �������� LogID � ������� CursorNewLogID */
  DECLARE @NewSrvPacketID BIGINT; /*  �������� SrvPacketID � ������� CursorNewLogID */
  DECLARE @FirstLogID INT; /* ������ �������� LogID ��� �������*/
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ������ �������� LogID ��� �������*/
  DECLARE @FirstSrvPacketID BIGINT; /* ������ �������� SrvPacketID*/
  DECLARE @SecondSrvPacketID BIGINT; /* ������ �������� SrvPacketID*/

  DECLARE @tmpSrvPacketID BIGINT; /* �������� SrvPacketID ��� ������ ������������� ������ � ������� ����� ������, �� �������������� � �������������*/
  SET @tmpSrvPacketID = 0;

  /* ������ ��� ������� �� ����� LogID  */
  DECLARE CursorNewLogID CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp;

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  SET @CurrentConfirmedID = (SELECT coalesce(ConfirmedID, 0)
                             FROM
                               mobitels
                             WHERE
                               Mobitel_ID = @MobitelID);

  /*  ����������� LogID � ����� ������ ������ c ������� ����� �������� */
  SELECT @MinLogID_n = min(LogID)
  FROM
    datagpsbuffer_on64
  WHERE
    (Mobitel_ID = @MobitelID)
    AND (LogID > @CurrentConfirmedID);

  /* ������������ End_LogID � ����������� ���������� ������� ���������*/
  SET @MaxLogID_n = (SELECT max(LogID)
                     FROM
                       datagpsbuffer_on64
                     WHERE
                       Mobitel_ID = @MobitelID);

  /* ������������ End_LogID � ����������� ���������� ������� ���������  */
  SELECT @MaxEndLogID = coalesce(max(End_LogID), 0)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* �������� ���� �� ������ ��� ������� */
  IF @MinLogID_n IS NOT NULL
  BEGIN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF @MinLogID_n < @MaxEndLogID
    BEGIN
      /* ����  MinLogID_n < MaxEndLogID ������ ������ ������, 
       ������� �������� ������� ����� ���������. */
       
      /* ������� ��������� LogID ��� ���������. */
      SET @StartLogIDSelect = (SELECT coalesce(Begin_LogID, @MinLogID_n)
                               FROM
                                 datagpslost_on
                               WHERE
                                 (Mobitel_ID = @MobitelID)
                                 AND (@MinLogID_n > Begin_LogID)
                                 AND (@MinLogID_n < End_LogID));

      /* ������� ��������� LogID ��� ���������. */
      SET @FinishLogIDSelect = (SELECT coalesce(End_LogID, @MaxLogID_n)
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (@MaxLogID_n > Begin_LogID)
                                  AND (@MaxLogID_n < End_LogID));

      /* ������ ������ ������ �� ������� ����������� ������� 
         (����� �������������, ���� �����������) */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID >= @StartLogIDSelect) AND
        (Begin_LogID <= @FinishLogIDSelect);

      /* ���� ������ ������ LogID = 0 � ���� ������ ��� � DataGps, �����
         ���� �������� ������ ��������� ������ ��� �������. */
      IF (@StartLogIDSelect = 0) AND (NOT EXISTS (SELECT 1
                                                  FROM
                                                    datagps64
                                                  WHERE
                                                    (Mobitel_ID = @MobitelID)
                                                    AND (LogID = @StartLogIDSelect)))
      BEGIN
        INSERT INTO datagpslost_ontmp(LogID
                                    , SrvPacketID)
        VALUES
          (0, 0);
      END;
    END;
    ELSE
    BEGIN

      /* ����  MinLogID_n < MaxEndLogID ������ ������ ������, 
       ������� �������� ������� ����� ���������. */
       
      /* ������� ��������� LogID ��� ���������. */
      SELECT @StartLogIDSelect = coalesce(max(End_LogID), @CurrentConfirmedID)
      FROM
        datagpslost_on
      WHERE
        Mobitel_ID = @MobitelID;

      /* ������� ��������� LogID ��� ���������. */  
      SELECT @FinishLogIDSelect = max(LogID)
      FROM
        datagpsbuffer_on64
      WHERE
        Mobitel_ID = @MobitelID;

      /* ������ ������ ������ �� ������� ����������� ������� 
         (����� �������������, ���� �����������) */
      SELECT @tmpSrvPacketID = coalesce(SrvPacketID, 0)
      FROM
        datagps64
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (LogID = @StartLogIDSelect);

      /* �������� � ������� datagpslost_ontmp ��� ������� ��������� 
         �������������� ������ ������ */
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@StartLogIDSelect, @tmpSrvPacketID);
    END; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

    /* ���������� ��������� ������� ���������� ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT LogID
         , SrvPacketID
    FROM
      datagps64
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (LogID >= @StartLogIDSelect)
      AND (LogID <= @FinishLogIDSelect)
    ORDER BY
      LogID;

     /* ������������ �������-������� � ������� ����������� ���������� */
    --SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstLogID = -9999999
      BEGIN
        /* ������ �������� � ������ */
        SET @FirstLogID = @NewLogID;
        SET @FirstSrvPacketID = @NewSrvPacketID;
      END
      ELSE
      BEGIN
        /* �������� ����� �������*/
        SET @SecondLogID = @NewLogID;
        SET @SecondSrvPacketID = @NewSrvPacketID;

        IF (@SecondLogID - @FirstLogID) > 1
        BEGIN
          /* ������ ������ */
          EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

          IF @NeedOptimize = 0
          BEGIN
            SET @NeedOptimize = 1;
          END; -- IF

        END; -- IF
        /* ���������� ������� ������ �������� �� ������ �������
           ��� ������� � ��������� �������� � �������� ���������� �������� */
        SET @FirstLogID = @SecondLogID;
        SET @FirstSrvPacketID = @SecondSrvPacketID;
      END; -- IF
      FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    END; -- WHILE
    CLOSE CursorNewLogID;
    --DEALLOCATE CursorNewLogID; 

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

    /* ���� ����������� ������� � ������� ��������� ��� - 
       ����� ConfirmedID ����� ����� ������������� LogID 
       ������� ���������. ����� - ����������� Begin_LogID
       �� ������� ��������� */
    SET @NewConfirmedID = (SELECT min(Begin_LogID)
                           FROM
                             datagpslost_on
                           WHERE
                             Mobitel_ID = @MobitelID);

    IF @NewConfirmedID IS NULL
    BEGIN
      SET @NewConfirmedID = (SELECT max(LogID)
                             FROM
                               datagps64
                             WHERE
                               Mobitel_ID = @MobitelID);
    END;

    /* ������� ConfirmedID ���� ���� */
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (ConfirmedID < @NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END; -- IF MaxEndLogID IS NOT NULL  
END

GO

/****** Object:  StoredProcedure [dbo].[OnTransferBuffer]    Script Date: 15.12.2016 11:59:09 ******/
IF OBJECT_ID('OnTransferBuffer','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnTransferBuffer]
GO

/****** Object:  StoredProcedure [dbo].[OnTransferBuffer]    Script Date: 15.12.2016 11:59:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OnTransferBuffer]
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC OnDeleteDuplicates
  EXEC OnCorrectInfotrackLogId

  /* OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp(Mobitel_ID
                                , LogID
                                , SrvPacketID)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.SrvPacketID
  FROM
    datagpsbuffer_on b
    LEFT JOIN datagps d
      ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  WHERE
    d.LogID IS NOT NULL;

  /* --------------------  Online ---------------------- */
  /*  online  datagpsbuffer_on */
  /*  Mobitel_ID LogID */
  DELETE o
  FROM
    [online] o, datagpsbuffer_on dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT TOP 1 coalesce(max(UnixTime), 0)
                           FROM
                             [online] WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
    INSERT INTO [dbo].[online](Mobitel_ID
                         , LogID
                         , UnixTime
                         , Latitude
                         , Longitude
                         , Altitude
                         , Direction
                         , Speed
                         , Valid
                         , [Events]
                         , Sensor1
                         , Sensor2
                         , Sensor3
                         , Sensor4
                         , Sensor5
                         , Sensor6
                         , Sensor7
                         , Sensor8
                         , Counter1
                         , Counter2
                         , Counter3
                         , Counter4
                         , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , [Events]
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_on
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;

    /*  100 */
    DELETE
    FROM
      [online]
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT TOP 1 coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        [online]
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  /* .. */
  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = b.LogID, [Events] = [b].[Events], Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4, SrvPacketID = b.SrvPacketID
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID) AND
    (d.SrvPacketID < b.SrvPacketID);

  /*  */
  DELETE b
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  /* */
  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , [Events]
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs
                    , SrvPacketID)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , [Events]
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
       , SrvPacketID
  FROM
    datagpsbuffer_on GROUP BY Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , [Events]
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
       , SrvPacketID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS @MobitelIDInCursor, @NeedOptimizeParam OUT
    
    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS2 @MobitelIDInCursor, @NeedOptimizeParam OUT

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  IF @NeedOptimize = 1 begin
    ALTER INDEX ALL ON datagpslost_on REORGANIZE
  END;
END

GO

/****** Object:  StoredProcedure [dbo].[OnTransferBuffer64]    Script Date: 15.12.2016 12:12:14 ******/
IF OBJECT_ID('OnTransferBuffer64','P') IS NOT NULL
DROP PROCEDURE [dbo].[OnTransferBuffer64]
GO

/****** Object:  StoredProcedure [dbo].[OnTransferBuffer64]    Script Date: 15.12.2016 12:12:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OnTransferBuffer64]
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  DECLARE @DataExists TINYINT; /*  */
  SET @DataExists = 1;
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on64;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp64;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC delDupSrvPacket
  EXEC OnDeleteDuplicates64
  --EXEC OnCorrectInfotrackLogId64
  --EXEC OnCorrectInfotrackLogId64

   /* ��������� ������, ������� ��� ���� � ��, ��� ������� ����� ������� ��������� 
     ������������ ���������� OnLostDataGPS264. */
  /*INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID)
    SELECT b.Mobitel_ID, b.LogID, b.SrvPacketID
    FROM datagpsbuffer_on64 b LEFT JOIN datagps64 d ON
      (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL  GROUP BY d.SrvPacketID;*/
    
  IF OBJECT_ID(N'tempdb..#temp1', N'U') IS NOT NULL   
	DROP TABLE #temp1;

  CREATE TABLE #temp1 (Mobitel_ID int, LogID int, SrvPacketID bigint);
  insert into #temp1  SELECT
      b.Mobitel_ID,
      b.LogID,
      b.SrvPacketID
    FROM datagpsbuffer_on64 b
      LEFT JOIN datagps64 d
        ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL GROUP BY b.SrvPacketID, b.Mobitel_ID,
      b.LogID;

	INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID) SELECT * FROM #temp1 GROUP BY SrvPacketID,Mobitel_ID, LogID;

	IF OBJECT_ID(N'tempdb..#temp1', N'U') IS NOT NULL   
		DROP TABLE #temp1;

  /* -------------------- ���������� ������� Online ---------------------- */
  /* �������� �� ������� online ����������� ����� ������� datagpsbuffer_on64 */
  /* �� ����� Mobitel_ID � LogID */
   DELETE o
  FROM online64 o, datagpsbuffer_on64 dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online64 WITH (INDEX (IDX_Mobitelid64))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
	      INSERT INTO online64 (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude,Acceleration, 
      Direction, Speed, Valid, [Events],
      Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude,Acceleration, 
      Direction, Speed, Valid, [Events],
      Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS
    FROM (
      SELECT TOP 100 *
      FROM datagpsbuffer_on64
      WHERE (Mobitel_ID = @MobitelIDInCursor)  AND 
        (UnixTime > @TmpMaxUnixTime) AND (Valid = 1)
      ORDER BY UnixTime DESC
      ) AS T
    ORDER BY UnixTime ASC;

     /* �������� ����� �� ������� ����������� ������ �� ���������������
       �������� �������� - �� �������� ���������� 100 �������� */
    DELETE
    FROM
      online64
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        [online64]
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));


    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  UPDATE datagps64 SET
    Latitude = b.Latitude,
    Longitude = b.Longitude,
    Acceleration = b.Acceleration,
    UnixTime = b.UnixTime,
    Speed = b.Speed,
    Direction = b.Direction,
    Valid = b.Valid,
    LogID = b.LogID,
    [Events] = [b].[Events],
    Satellites = b.Satellites,
    RssiGsm = b.RssiGsm,
    SensorsSet = b.SensorsSet,
    Sensors = b.Sensors,
    Voltage = b.Voltage,
    DGPS = b.DGPS,
    SrvPacketID = b.SrvPacketID
  FROM
    datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    AND (d.SrvPacketID < b.SrvPacketID);
  /*  */
  DELETE b
  FROM datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);
  /* */
    INSERT INTO datagps64 (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, [Events], 
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, [Events], 
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID
  FROM datagpsbuffer_on64 GROUP BY Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, [Events], 
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID;
  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS64 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);*/

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on64;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET @DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS264 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = CREATEST(@NeedOptimize, @NeedOptimizeParam); */

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsUpdateSet;
  --DEALLOCATE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp64;

  /*  */
  IF @NeedOptimize = 1
    ALTER INDEX ALL ON datagpslost_on REORGANIZE
END

GO

/****** Object:  StoredProcedure [dbo].[PopListMobitelConfig]    Script Date: 15.12.2016 12:12:14 ******/
IF OBJECT_ID('PopListMobitelConfig','P') IS NOT NULL
DROP PROCEDURE [dbo].[PopListMobitelConfig]
GO

/****** Object:  StoredProcedure [dbo].[PopListMobitelConfig]    Script Date: 15.12.2016 12:12:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PopListMobitelConfig]
  -- COMMENT '������� �������� ���������� ��� ������ � ������ POP3'
  AS
BEGIN
  SELECT m.Mobitel_ID AS MobitelID, c.devIdShort AS DevIdShort,
    COALESCE(
      (SELECT TOP 1 s.EmailMobitel 
       FROM Mobitels m2 JOIN ServiceSend s ON s.ID = m2.ServiceSend_ID 
       WHERE m2.Mobitel_ID = m.Mobitel_ID
       ORDER BY s.ServiceSend_ID DESC), '') AS Email
  FROM mobitels m JOIN internalmobitelconfig c ON c.ID = m.InternalMobitelConfig_ID
  WHERE (c.InternalMobitelConfig_ID = (
    SELECT TOP 1 intConf.InternalMobitelConfig_ID
    FROM internalmobitelconfig intConf
    WHERE (intConf.ID = c.ID)
    ORDER BY intConf.InternalMobitelConfig_ID DESC)) AND (c.devIdShort IS NOT NULL) AND (c.devIdShort <> '')
  ORDER BY m.Mobitel_ID;
END

GO

/****** Object:  StoredProcedure [dbo].[PopTransferBuffer]    Script Date: 15.12.2016 12:12:14 ******/
IF OBJECT_ID('PopTransferBuffer','P') IS NOT NULL
DROP PROCEDURE [dbo].[PopTransferBuffer]
GO

/****** Object:  StoredProcedure [dbo].[PopTransferBuffer]    Script Date: 15.12.2016 12:12:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PopTransferBuffer]
  --COMMENT '��������� GPS ������ �� �������� ������� � datagps'
  AS
BEGIN
  DECLARE @MobitelIDInCursor INT; 
  --DECLARE @DataExists TINYINT;
  --SET @DataExists = 1;
  DECLARE @TmpMaxUnixTime INT; 

  DECLARE CursorMobitels CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_pop;

  
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  DELETE o
  FROM [online] o, datagpsbuffer_pop dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);
  
  --SET DataExists = 1;
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@FETCH_STATUS = 0 begin
    
    SET @TmpMaxUnixTime = (SELECT COALESCE(MAX(UnixTime), 0)
    FROM [online] WITH( INDEX (IDX_Mobitelid))
    WHERE Mobitel_ID = @MobitelIDInCursor);

    
    INSERT INTO [online] (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, [Events],
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, [Events],
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs
    FROM (
      SELECT TOP 100 *
      FROM datagpsbuffer_pop
      WHERE (Mobitel_ID = @MobitelIDInCursor)  AND 
        (UnixTime > @TmpMaxUnixTime) AND (Valid = 1)
      GROUP BY  Mobitel_ID, LogID, Message_ID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, [Events],
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs, InMobitelID, isShow
      ORDER BY UnixTime DESC) AS T
    ORDER BY UnixTime ASC;

    DELETE FROM [online]
    WHERE (Mobitel_ID = @MobitelIDInCursor) AND (UnixTime < 
      (SELECT COALESCE(MIN(UnixTime), 0) 
       FROM (
         SELECT TOP 100 UnixTime
         FROM [online]
         WHERE Mobitel_ID = @MobitelIDInCursor 
         ORDER BY UnixTime DESC) T1)); 
  
    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END;
  CLOSE CursorMobitels;
    
  UPDATE datagps SET
    Latitude = b.Latitude,
    Longitude = b.Longitude,
    Altitude = b.Altitude,
    UnixTime = b.UnixTime,
    Speed = b.Speed,
    Direction = b.Direction,
    Valid = b.Valid,
    InMobitelID = b.LogID,
    [Events] = b.[Events],
    Sensor1 = b.Sensor1,
    Sensor2 = b.Sensor2,
    Sensor3 = b.Sensor3,
    Sensor4 = b.Sensor4,
    Sensor5 = b.Sensor5,
    Sensor6 = b.Sensor6,
    Sensor7 = b.Sensor7,
    Sensor8 = b.Sensor8,
    whatIs = b.whatIs,
    Counter1 = b.Counter1,
    Counter2 = b.Counter2,
    Counter3 = b.Counter3,
    Counter4 = b.Counter4
	FROM datagps d, datagpsbuffer_pop b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  
  DELETE b
  FROM datagps d, datagpsbuffer_pop b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  
  INSERT INTO datagps (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, [Events], InMobitelID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs)
  SELECT
    p.Mobitel_ID, p.LogID, p.UnixTime, p.Latitude, p.Longitude, p.Altitude,
    p.Direction, p.Speed, p.Valid, [p].[Events], p.LogID,
    p.Sensor1, p.Sensor2, p.Sensor3, p.Sensor4, p.Sensor5, p.Sensor6, p.Sensor7, p.Sensor8,
    p.Counter1, p.Counter2, p.Counter3, p.Counter4, p.whatIs
  FROM datagps d, datagpsbuffer_pop p GROUP BY p.Mobitel_ID, p.LogID, p.UnixTime, p.Latitude, p.Longitude, p.Altitude,
    p.Direction, p.Speed, p.Valid, [p].[Events], p.LogID,
    p.Sensor1, p.Sensor2, p.Sensor3, p.Sensor4, p.Sensor5, p.Sensor6, p.Sensor7, p.Sensor8,
    p.Counter1, p.Counter2, p.Counter3, p.Counter4, p.whatIs;

  
  TRUNCATE TABLE datagpsbuffer_pop;
END

GO

/****** Object:  StoredProcedure [dbo].[SelectDataGpsPeriodAll]    Script Date: 15.12.2016 12:55:26 ******/
IF OBJECT_ID('SelectDataGpsPeriodAll','P') IS NOT NULL
DROP PROCEDURE [dbo].[SelectDataGpsPeriodAll]
GO

/****** Object:  StoredProcedure [dbo].[SelectDataGpsPeriodAll]    Script Date: 15.12.2016 12:55:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SelectDataGpsPeriodAll]
(
  @mobile      INT,
  @beginperiod DATETIME,
  @endperiod   DATETIME,
  @timebegin   VARCHAR(9),
  @timeend     VARCHAR(9),
  @weekdays    VARCHAR(15),
  @daymonth    VARCHAR(90)
)
AS
BEGIN
  -- DECLARE @begin DATETIME;
  -- SET @begin = convert(DATETIME, '24/03/2013 00:00:00');
  -- DECLARE @end DATETIME;
  -- SET @end = convert(DATETIME, '1/04/2013 23:59:59');
  DECLARE @saTail VARCHAR(90);
  DECLARE @saHead VARCHAR(2);
  DECLARE @indx INT;
  DECLARE @time0 TIME;
  SET @time0 = '23:59:59';
  DECLARE @time1 TIME;
  SET @time1 = '00:00:00';

  IF object_id('tempdb..#tempperiod4') IS NOT NULL
    DROP TABLE #tempperiod4;

  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS time
       , (Speed * 1.852) AS speed
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
       , [Events] AS [Events]
       , LogID AS LogID
  INTO
    #tempperiod4
  FROM
    datagps
  WHERE
    Mobitel_ID = @mobile
    AND (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@beginperiod) AND dbo.UNIX_TIMESTAMPS(@endperiod))
    AND (Valid = 1)
    AND NOT (Latitude = 0
    AND Longitude = 0)
  ORDER BY
    UnixTime;

  IF object_id('tempdb..#tempweeks') IS NOT NULL
    DROP TABLE #tempweeks;

  IF @weekdays != ''
  BEGIN
    CREATE TABLE #tempweeks(
      shead VARCHAR(2)
    );

    SET @saTail = @weekdays;

    WHILE @saTail != ''
    BEGIN
      SET @indx = charindex(';', @saTail);
      SET @saHead = substring(@saTail, 1, @indx - 1);
      SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

      INSERT INTO #tempweeks(shead)
      VALUES
        (@saHead);
    END;
  END -- IF

  IF object_id('tempdb..#tempday0') IS NOT NULL
    DROP TABLE #tempday0;

  IF @daymonth != ''
  BEGIN
    CREATE TABLE #tempday0(
      shead VARCHAR(2)
    );

    SET @saTail = @daymonth;

    WHILE @saTail != ''
    BEGIN
      SET @indx = charindex(';', @saTail);
      SET @saHead = substring(@saTail, 1, @indx - 1);
      SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

      INSERT INTO #tempday0(shead)
      VALUES
        (@saHead);
    END; -- while
  END; -- IF

  -- getting select data
  IF (@daymonth != '') AND (@weekdays != '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- ������ �� ���� ������
        (datepart (dw, t.time) IN (SELECT *
                                   FROM
                                     #tempweeks))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- ������ ���
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- ������ �� ���� ������
        (datepart (dw, t.time) IN (SELECT *
                                   FROM
                                     #tempweeks))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- ������ ���
    END;
  END;

  IF (@daymonth = '') AND (@weekdays != '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((datepart (dw, t.time) IN (SELECT *
                                    FROM
                                      #tempweeks))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- ������ ���
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((datepart (dw, t.time) IN (SELECT *
                                    FROM
                                      #tempweeks))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- ������ ���
    END;
  END;

  IF (@daymonth != '') AND (@weekdays = '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- ������ ���
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- ������ ���
    END;
  END;

  IF (@daymonth = '') AND (@weekdays = '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend))
      ORDER BY
        t.time; -- ������ ���
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend)))
      ORDER BY
        t.time; -- ������ ���
    END;
  END;
END

GO

/****** Object:  StoredProcedure [dbo].[SelectDataGpsPeriodMonth]    Script Date: 15.12.2016 12:55:26 ******/
IF OBJECT_ID('SelectDataGpsPeriodMonth','P') IS NOT NULL
DROP PROCEDURE [dbo].[SelectDataGpsPeriodMonth]
GO

/****** Object:  StoredProcedure [dbo].[SelectDataGpsPeriodMonth]    Script Date: 15.12.2016 12:55:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SelectDataGpsPeriodMonth] @mobile int, @beginperiod datetime, @endperiod datetime,
@daymonth varchar(82)
AS
BEGIN
IF object_id('tempdb..#tempperiod2') IS NOT NULL
    DROP TABLE #tempperiod2;

  CREATE TABLE #tempperiod2 (DataGps_ID int, Mobitel_ID int, Lat int, Lon int, Altitude int, [time] int, speed int, Valid int, 
  sensor bigint, [Events] bigint, LogID int);
  
  INSERT INTO #tempperiod2 SELECT
    DataGps_ID AS DataGps_ID,
    Mobitel_ID AS Mobitel_ID,
    ((Latitude * 1.0000) / 600000) AS Lat,
    ((Longitude * 1.0000) / 600000) AS Lon,
    Altitude AS Altitude,
    dbo.From_UnixTime(UnixTime) AS [time],
    (Speed * 1.852) AS speed,
    Valid AS Valid,
	(Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor,
    [Events] AS [Events],
    LogID AS LogID
  FROM datagps
  WHERE Mobitel_ID = @mobile AND
  (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@beginperiod) AND dbo.UNIX_TIMESTAMPS(@endperiod)) AND
  (Valid = 1) AND NOT (Latitude = 0 AND Longitude = 0)
  ORDER BY UnixTime;

  IF @daymonth = '' begin
    return;
  END;

  IF object_id('tempdb..#tempdays') IS NOT NULL
    DROP TABLE #tempdays;

  CREATE TABLE #tempdays (
    shead varchar(2)
  );

  DECLARE @saTail varchar(82);
  DECLARE @saHead varchar(82);
  DECLARE @indx int;
  SET @saTail = @daymonth;
  WHILE @saTail != '' begin
	 SET @indx = charindex(';', @saTail);
     SET @saHead = substring(@saTail, 1, @indx - 1);
     SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

    INSERT INTO #tempdays (shead)
      VALUES (@saHead);
  END;

  SELECT
    *
  FROM #tempperiod2 d
  WHERE DAY([d].[time]) IN (SELECT
    *
  FROM #tempdays);
  END

  GO

/****** Object:  StoredProcedure [dbo].[tmpAddIndex]    Script Date: 15.12.2016 13:30:05 ******/
IF OBJECT_ID('tmpAddIndex','P') IS NOT NULL
DROP PROCEDURE [dbo].[tmpAddIndex]
GO

/****** Object:  StoredProcedure [dbo].[tmpAddIndex]    Script Date: 15.12.2016 13:30:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tmpAddIndex]
AS
--COMMENT '��������� ��������� ���������� ������� � DataGpsBuffer_on'
BEGIN
  DECLARE @DbName VARCHAR(64); /* ������������ ������� �� */
  DECLARE @IndexExist TINYINT;
  SET @IndexExist = 0;

  SET @DbName = (SELECT db_name(@@procid));

  SET @IndexExist = (SELECT count(1)
                     FROM
                       sys.indexes i
                     WHERE
                       i.name = 'IDX_SrvPacketID');

  IF @IndexExist = 0
  BEGIN
    CREATE INDEX IDX_SrvPacketID ON DataGpsBuffer_on (SrvPacketID);
    ALTER TABLE DataGpsBuffer_on ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON);
  END;
END

--EXEC tmpAddIndex
--DROP PROCEDURE IF EXISTS tmpAddIndex$$

GO

/****** Object:  StoredProcedure [dbo].[TransferBuffer]    Script Date: 15.12.2016 13:31:02 ******/
IF OBJECT_ID('TransferBuffer','P') IS NOT NULL
DROP PROCEDURE [dbo].[TransferBuffer]
GO

/****** Object:  StoredProcedure [dbo].[TransferBuffer]    Script Date: 15.12.2016 13:31:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TransferBuffer]
--SQL SECURITY INVOKER
--COMMENT '��������� GPS ������ �� �������� ������� � datagps.'
AS
BEGIN

  --CREATE TEMPORARY TABLE IF NOT EXISTS temp1 ENGINE = HEAP

  IF object_id('tempdb..#temp1') IS NOT NULL
    DROP TABLE #temp1;

  CREATE TABLE #temp1(
    DataGps_ID INT
  );
  INSERT INTO #temp1
  SELECT DataGPS_ID
  FROM
    datagpsbuffer;

  UPDATE datagps
  SET
    Message_ID = 0, Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = NULL, [Events] = b.[Events], Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, isShow = 0, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagps d, datagpsbuffer b, #temp1 t
  WHERE
    (b.DataGps_ID = t.DataGps_ID) AND
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  DELETE b
  FROM
    datagps d, datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.LogID = b.LogID;

  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , [Events]
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.[Events]
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID
  GROUP BY
   b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.[Events]
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs;


  DELETE b
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID;

  DROP TABLE #temp1
END

GO

/****** Object:  StoredProcedure [dbo].[TransferLiteBuffer]    Script Date: 15.12.2016 13:35:43 ******/
IF OBJECT_ID('TransferLiteBuffer','P') IS NOT NULL
DROP PROCEDURE [dbo].[TransferLiteBuffer]
GO

/****** Object:  StoredProcedure [dbo].[TransferLiteBuffer]    Script Date: 15.12.2016 13:35:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TransferLiteBuffer]
--COMMENT '��������� GPS ������ �� �������� ������� � datagps.'
AS
BEGIN
  IF object_id('tempdb..#temp2') IS NOT NULL
    DROP TABLE #temp2;

  CREATE TABLE #temp2(
    DataGps_ID INT
  );
  INSERT INTO #temp2
  SELECT DataGPS_ID
  FROM
    datagpsbufferlite;

  UPDATE datagps
  SET
    Message_ID = b.Message_ID, Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = NULL, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, isShow = b.IsShow, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagps d, datagpsbuffer b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.Latitude = b.Latitude AND
    d.Longitude = b.Longitude AND
    d.Altitude = b.Altitude AND
    d.UnixTime = b.UnixTime;

  DELETE b
  FROM
    datagps d, datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.Latitude = b.Latitude AND
    d.Longitude = b.Longitude AND
    d.Altitude = b.Altitude AND
    d.UnixTime = b.UnixTime;

  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.[Events]
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs
  FROM
    datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID
  GROUP BY
   b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.[Events]
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs;

  DELETE b
  FROM
    datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID;

  DROP TABLE #temp2
END

GO















