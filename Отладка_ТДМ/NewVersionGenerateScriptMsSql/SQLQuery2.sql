create table #tmp64 -- �������� ��������� �������
    (mobi int, lat int, lng int, dir int, acc int, unixt int, spd int, vald int, stl int, rsg int, evn int, snsset int, sns varbinary(50), vlt int, dgp varchar(32), lid int, spid bigint)

insert into #tmp64 SELECT 
  d.Mobitel_ID,
  d.Latitude,
  d.Longitude,
  d.Direction,
  d.Acceleration,
  d.UnixTime,
  d.Speed,
  d.Valid,
  d.Satellites,
  d.RssiGsm,
  d.[Events],
  d.SensorsSet,
  d.Sensors,
  d.Voltage,
  d.DGPS,
  d.LogID,
  d.SrvPacketID
FROM datagpsbuffer_on64 d;
--GROUP BY d.SrvPacketID;