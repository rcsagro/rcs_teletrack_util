ALTER TABLE [dbo].[datagps]
ALTER COLUMN DataGps_ID bigint NOT NULL

ALTER TABLE [dbo].[datagps64]
ALTER COLUMN Sensors  varbinary(50) NOT NULL

ALTER TABLE [dbo].[datagps64]
ADD DEFAULT '0' FOR Sensors;

ALTER TABLE [dbo].[datagps64]
ALTER COLUMN DGPS varchar(32)

ALTER TABLE [dbo].[datagps64]
ADD DEFAULT NULL FOR DGPS;

ALTER TABLE [dbo].[datagpsbuffer]
ALTER COLUMN DataGps_ID bigint NOT NULL

ALTER TABLE [dbo].[datagpsbufferlite]
ALTER COLUMN DataGps_ID bigint NOT NULL

ALTER TABLE [dbo].[datagpsbuffer_on64]
ALTER COLUMN Sensors varbinary(50) NOT NULL

ALTER TABLE [dbo].[datagpsbuffer_on64]
ADD DEFAULT '0' FOR Sensors;

ALTER TABLE [dbo].[datagpsbuffer_on64]
ALTER COLUMN DGPS varchar(32) NOT NULL

ALTER TABLE [dbo].[datagpsbuffer_on64]
ADD DEFAULT '0' FOR DGPS;








