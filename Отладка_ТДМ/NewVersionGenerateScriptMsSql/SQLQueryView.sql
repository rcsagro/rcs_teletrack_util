/****** Object:  View [dbo].[datavalue1]    Script Date: 14.12.2016 12:08:42 ******/
if exists(select 1 from sys.views where name='datavalue1' and type='v')
drop view [dbo].[datavalue1];
GO
/****** Object:  View [dbo].[datavalue1]    Script Date: 14.12.2016 12:08:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[datavalue1]
AS SELECT round((Longitude / 600000), 6) AS lon
        , round((dbo.datagps.Latitude / 600000), 6) AS lat
        , dbo.datagps.Mobitel_ID AS Mobitel_ID
        , dbo.sensordata.value AS Value
        , (dbo.datagps.Speed * 1.852) AS speed
        , dbo.from_unixtime(dbo.datagps.UnixTime) AS [time]
        , dbo.sensordata.sensor_id AS sensor_id
        , dbo.datagps.DataGps_ID AS datavalue_id
   FROM
     (dbo.sensordata
     JOIN dbo.datagps
       ON ((dbo.sensordata.datagps_id = dbo.datagps.DataGps_ID)))
   WHERE
     (dbo.datagps.Valid = 1);
	 --ORDER BY datagps.UnixTime, datagps.Mobitel_ID, sensordata.sensor_id DESC;

GO

/****** Object:  View [dbo].[dataview1]    Script Date: 14.12.2016 12:18:26 ******/
if exists(select 1 from sys.views where name='dataview1' and type='v')
drop view [dbo].[dataview1];
GO

/****** Object:  View [dbo].[dataview1]    Script Date: 14.12.2016 12:18:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[dataview1] 
AS SELECT dbo.datagps.DataGps_ID AS DataGps_ID
        , dbo.datagps.Mobitel_ID AS Mobitel_ID
        , (dbo.datagps.Latitude / 600000.00000) AS Lat
        , (dbo.datagps.Longitude / 600000.00000) AS Lon
        , dbo.datagps.Altitude AS Altitude
        , dbo.FROM_UNIXTIME(datagps.UnixTime) AS [time]
        , (dbo.datagps.Speed * 1.852) AS speed
        , ((dbo.datagps.Direction * 360) / 255) AS direction
        , dbo.datagps.Valid AS Valid
        , (dbo.datagps.Sensor8 + (dbo.datagps.Sensor7 * 256) + (dbo.datagps.Sensor6 * 65536) + (cast(dbo.datagps.Sensor5 AS BIGINT) * 16777216) + (dbo.datagps.Sensor4 * 4294967296) + (dbo.datagps.Sensor3 * 1099511627776) + (dbo.datagps.Sensor2 * 281474976710656) + (dbo.datagps.Sensor1 * 72057594037927936)) AS sensor
        , dbo.datagps.[Events] AS [Events]
        , dbo.datagps.LogID AS LogID
   FROM
     dbo.datagps;

GO

/****** Object:  View [dbo].[deviceview]    Script Date: 14.12.2016 12:25:55 ******/
if exists(select 1 from sys.views where name='deviceview' and type='v')
DROP VIEW [dbo].[deviceview]
GO

/****** Object:  View [dbo].[deviceview]    Script Date: 14.12.2016 12:25:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[deviceview]
AS (SELECT m.Mobitel_ID AS Mobitel_ID
         , m.Name AS Name
         , m.Descr AS Descr
         , c.devIdShort AS devIdShort
         , m.DeviceType AS DeviceType
         , m.ConfirmedID AS ConfirmedID
         , c.moduleIdRf AS moduleIdRf
   FROM
     ((mobitels m
     JOIN internalmobitelconfigview v
       ON v.ID = m.InternalMobitelConfig_ID)
     JOIN internalmobitelconfig c
       ON v.LastRecord = c.InternalMobitelConfig_ID)); --ORDER BY [m].[Name] DESC);

GO

/****** Object:  View [dbo].[internalmobitelconfigview]    Script Date: 14.12.2016 12:43:57 ******/
if exists(select 1 from sys.views where name='internalmobitelconfigview' and type='v')
DROP VIEW [dbo].[internalmobitelconfigview]
GO

/****** Object:  View [dbo].[internalmobitelconfigview]    Script Date: 14.12.2016 12:43:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[internalmobitelconfigview]
AS (SELECT c.ID AS ID
         , max(c.InternalMobitelConfig_ID) AS LastRecord
   FROM
     internalmobitelconfig c
   WHERE
     (c.devIdShort <> '')
   GROUP BY
     c.ID);

GO




