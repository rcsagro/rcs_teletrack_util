﻿--
-- Скрипт сгенерирован Devart dbForge Studio for SQL Server, Версия 5.1.178.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/sql/studio
-- Дата скрипта: 16.12.2016 11:29:59
-- Версия сервера: 10.50.2500
-- Версия клиента: 
--



USE mca_agro
GO

IF DB_NAME() <> N'mca_agro' SET NOEXEC ON
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

--
-- Создать процедуру "dbo.OnInsertDatagpsLost"
--
GO
PRINT (N'Создать процедуру "dbo.OnInsertDatagpsLost"')
GO


CREATE PROCEDURE dbo.OnInsertDatagpsLost
(
  @MobitelID        INT,
  @BeginLogID       INT,
  @EndLogID         INT,
  @BeginSrvPacketID BIGINT,
  @EndSrvPacketID   BIGINT
)
AS
--SQL SECURITY INVOKER
--COMMENT 'Вставка данных в таблицу datagpslost_on.'
BEGIN
  DECLARE @MobitelId_BeginLogID_Exists TINYINT;
  SET @MobitelId_BeginLogID_Exists = 0;
  DECLARE @MobitelId_BeginSrvPacketID_Exists TINYINT;
  SET @MobitelId_BeginSrvPacketID_Exists = 0;

  DECLARE @RowBeginSrvPacketID BIGINT;

  IF EXISTS (SELECT NULL
             FROM
               datagpslost_on
             WHERE
               (Mobitel_ID = @MobitelID)
               AND (Begin_LogID = @BeginLogID))
  BEGIN
    SET @MobitelId_BeginLogID_Exists = 1;
  END; -- IF

  IF EXISTS (SELECT NULL
             FROM
               datagpslost_on
             WHERE
               (Mobitel_ID = @MobitelID)
               AND (Begin_SrvPacketID = @BeginSrvPacketID))
  BEGIN
    SET @MobitelId_BeginSrvPacketID_Exists = 1;
  END; -- IF

  IF (@MobitelId_BeginLogID_Exists = 0) AND (@MobitelId_BeginSrvPacketID_Exists = 0)
  BEGIN
    /* ??????? ????? ?????? */
    INSERT INTO datagpslost_on(Mobitel_ID
                             , Begin_LogID
                             , End_LogID
                             , Begin_SrvPacketID
                             , End_SrvPacketID)
    VALUES
      (@MobitelID, @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID);
  END
  ELSE
  IF ((@MobitelId_BeginLogID_Exists = 1) AND (@MobitelId_BeginSrvPacketID_Exists = 1))
  BEGIN
    UPDATE datagpslost_on
    SET
      End_LogID = @EndLogID, End_SrvPacketID = @EndSrvPacketID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (Begin_LogID = @BeginLogID);
  END
  ELSE
  BEGIN
    SET @RowBeginSrvPacketID = (SELECT TOP 1 Begin_SrvPacketID
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (Begin_LogID = @BeginLogID));

    IF ((@MobitelId_BeginLogID_Exists = 1) AND (@MobitelId_BeginSrvPacketID_Exists = 0) AND (@BeginSrvPacketID < @RowBeginSrvPacketID))
    BEGIN
      UPDATE datagpslost_on
      SET
        End_LogID = @EndLogID, Begin_SrvPacketID = @BeginSrvPacketID, End_SrvPacketID = @EndSrvPacketID
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);
    END; -- IF
  END; -- ELSE
END
--END


GO

--
-- Создать процедуру "dbo.OnLostDataGPS64"
--
GO
PRINT (N'Создать процедуру "dbo.OnLostDataGPS64"')
GO


CREATE PROCEDURE dbo.OnLostDataGPS64
(
  @MobitelID    INT,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT 'Поиск пропусков в online данных заданного телетрека и заполнение'
BEGIN

  /********** DECLARE **********/

  DECLARE @CurrentConfirmedID INT; /* Текущее значение ConfirmedID  */
  SET @CurrentConfirmedID = 0;
  DECLARE @NewConfirmedID INT; /* Новое значение ConfirmedID */
  SET @NewConfirmedID = 0;
  DECLARE @StartLogIDSelect INT; /* min LogID для выборки */
  SET @StartLogIDSelect = 0;
  DECLARE @FinishLogIDSelect INT; /* max LogID для выборки */
  SET @FinishLogIDSelect = 0;
  DECLARE @MaxEndLogID INT; /* max End_LogID в пропущенных диапазонах данного телетрека */
  SET @MaxEndLogID = 0;

  DECLARE @MinLogID_n INT; /* Минимальный LogID после ConfirmedID в множестве новых данных */
  SET @MinLogID_n = 0;
  DECLARE @MaxLogID_n INT; /* Максимальный LogID в множестве новых данных */
  SET @MaxLogID_n = 0;

  DECLARE @DataExists TINYINT; /*  Флаг наличия данных после фетча */
  SET @DataExists = 1;

  DECLARE @NewLogID INT; /* Значение LogID в курсоре CursorNewLogID */
  DECLARE @NewSrvPacketID BIGINT; /*  Значение SrvPacketID в курсоре CursorNewLogID */
  DECLARE @FirstLogID INT; /* Первое значение LogID для анализа*/
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* Второе значение LogID для анализа*/
  DECLARE @FirstSrvPacketID BIGINT; /* Первое значение SrvPacketID*/
  DECLARE @SecondSrvPacketID BIGINT; /* Второе значение SrvPacketID*/

  DECLARE @tmpSrvPacketID BIGINT; /* Значение SrvPacketID для первой искусственной записи в анализе новых данных, не пересекающихся с существующими*/
  SET @tmpSrvPacketID = 0;

  /* Курсор для прохода по новым LogID  */
  DECLARE CursorNewLogID CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  SET @CurrentConfirmedID = (SELECT coalesce(ConfirmedID, 0)
                             FROM
                               mobitels
                             WHERE
                               Mobitel_ID = @MobitelID);

  /*  Минимальный LogID в новом наборе данных c которым будем работать */
  SELECT @MinLogID_n = min(LogID)
  FROM
    datagpsbuffer_on64
  WHERE
    (Mobitel_ID = @MobitelID)
    AND (LogID > @CurrentConfirmedID);

  /* Максимальный End_LogID в пропущенных диапазонах данного телетрека*/
  SET @MaxLogID_n = (SELECT max(LogID)
                     FROM
                       datagpsbuffer_on64
                     WHERE
                       Mobitel_ID = @MobitelID);

  /* Максимальный End_LogID в пропущенных диапазонах данного телетрека  */
  SELECT @MaxEndLogID = coalesce(max(End_LogID), 0)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* Проверим есть ли данные для анализа */
  IF @MinLogID_n IS NOT NULL
  BEGIN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF @MinLogID_n < @MaxEndLogID
    BEGIN
      /* Если  MinLogID_n < MaxEndLogID значит пришли данные, 
       которые возможно закроют часть пропусков. */
       
      /* Находим стартовый LogID для пересчета. */
      SET @StartLogIDSelect = (SELECT coalesce(Begin_LogID, @MinLogID_n)
                               FROM
                                 datagpslost_on
                               WHERE
                                 (Mobitel_ID = @MobitelID)
                                 AND (@MinLogID_n > Begin_LogID)
                                 AND (@MinLogID_n < End_LogID));

      /* Находим последний LogID для пересчета. */
      SET @FinishLogIDSelect = (SELECT coalesce(End_LogID, @MaxLogID_n)
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (@MaxLogID_n > Begin_LogID)
                                  AND (@MaxLogID_n < End_LogID));

      /* Удалим старые записи из реестра пропущенных записей 
         (стали неактуальными, надо пересчитать) */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID >= @StartLogIDSelect) AND
        (Begin_LogID <= @FinishLogIDSelect);

      /* Если первая запись LogID = 0 и этой записи нет в DataGps, тогда
         надо добавить первую фиктивную запись для анализа. */
      IF (@StartLogIDSelect = 0) AND (NOT EXISTS (SELECT 1
                                                  FROM
                                                    datagps64
                                                  WHERE
                                                    (Mobitel_ID = @MobitelID)
                                                    AND (LogID = @StartLogIDSelect)))
      BEGIN
        INSERT INTO datagpslost_ontmp(LogID
                                    , SrvPacketID)
        VALUES
          (0, 0);
      END;
    END;
    ELSE
    BEGIN

      /* Если  MinLogID_n < MaxEndLogID значит пришли данные, 
       которые возможно закроют часть пропусков. */
       
      /* Находим стартовый LogID для пересчета. */
      SELECT @StartLogIDSelect = coalesce(max(End_LogID), @CurrentConfirmedID)
      FROM
        datagpslost_on
      WHERE
        Mobitel_ID = @MobitelID;

      /* Находим последний LogID для пересчета. */  
      SELECT @FinishLogIDSelect = max(LogID)
      FROM
        datagpsbuffer_on64
      WHERE
        Mobitel_ID = @MobitelID;

      /* Удалим старые записи из реестра пропущенных записей 
         (стали неактуальными, надо пересчитать) */
      SELECT @tmpSrvPacketID = coalesce(SrvPacketID, 0)
      FROM
        datagps64
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (LogID = @StartLogIDSelect);

      /* Помещаем в таблицу datagpslost_ontmp для анализа пропусков 
         дополнительную первую запись */
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@StartLogIDSelect, @tmpSrvPacketID);
    END; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

    /* Заполнение временной таблицы последними данными телетрека
       для анализа разрывов */
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT LogID
         , SrvPacketID
    FROM
      datagps64
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (LogID >= @StartLogIDSelect)
      AND (LogID <= @FinishLogIDSelect)
    ORDER BY
      LogID;

     /* Формирование команды-вставки в таблицу пропущенных диапазонов */
    --SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstLogID = -9999999
      BEGIN
        /* Первое значение в списке */
        SET @FirstLogID = @NewLogID;
        SET @FirstSrvPacketID = @NewSrvPacketID;
      END
      ELSE
      BEGIN
        /* Основная ветка расчета*/
        SET @SecondLogID = @NewLogID;
        SET @SecondSrvPacketID = @NewSrvPacketID;

        IF (@SecondLogID - @FirstLogID) > 1
        BEGIN
          /* Найден разрыв */
          EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

          IF @NeedOptimize = 0
          BEGIN
            SET @NeedOptimize = 1;
          END; -- IF

        END; -- IF
        /* Переместим текущее второе значение на первую позицию
           для участия в следующей итерации в качестве начального значения */
        SET @FirstLogID = @SecondLogID;
        SET @FirstSrvPacketID = @SecondSrvPacketID;
      END; -- IF
      FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    END; -- WHILE
    CLOSE CursorNewLogID;
    --DEALLOCATE CursorNewLogID; 

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

    /* Если пропущенных записей у данного телетрека нет - 
       тогда ConfirmedID будет равен максимальному LogID 
       данного телетрека. Иначе - минимальный Begin_LogID
       из таблицы пропусков */
    SET @NewConfirmedID = (SELECT min(Begin_LogID)
                           FROM
                             datagpslost_on
                           WHERE
                             Mobitel_ID = @MobitelID);

    IF @NewConfirmedID IS NULL
    BEGIN
      SET @NewConfirmedID = (SELECT max(LogID)
                             FROM
                               datagps64
                             WHERE
                               Mobitel_ID = @MobitelID);
    END;

    /* Обновим ConfirmedID если надо */
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (ConfirmedID < @NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END; -- IF MaxEndLogID IS NOT NULL  
END

GO

--
-- Создать процедуру "dbo.OnLostDataGPS264"
--
GO
PRINT (N'Создать процедуру "dbo.OnLostDataGPS264"')
GO


CREATE PROCEDURE dbo.OnLostDataGPS264
(
  @MobitelID    INTEGER,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT 'Модификация диапазонов пропусков. Анализ дублирующих данных заданного телетрека.'
BEGIN
  /********** DECLARE **********/

  DECLARE @NewConfirmedID INT;
  SET @NewConfirmedID = 0; /* Новое значение ConfirmedID  */
  DECLARE @BeginLogID INT; /* Begin_LogID в курсоре CursorLostRanges */
  DECLARE @EndLogID INT; /* End_LogID в курсоре CursorLostRanges */
  DECLARE @BeginSrvPacketID BIGINT; /*  Begin_SrvPacketID в курсоре CursorLostRanges */
  DECLARE @EndSrvPacketID BIGINT; /* End_SrvPacketID в курсоре CursorLostRanges  */
  DECLARE @RowCountForAnalyze INT; /* Количество записей для анализа  */
  SET @RowCountForAnalyze = 0;
  DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  SET @DataExists = 1;
  DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
  DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
  DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
  DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
  DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

  /* Курсор для прохода по диапазонам пропусков телетрека */
  DECLARE CursorLostRanges CURSOR FOR
  SELECT Begin_LogID
       , End_LogID
       , Begin_SrvPacketID
       , End_SrvPacketID
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* Курсор для прохода по новым LogID */
  DECLARE CursorNewLogID2 CURSOR FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp
  ORDER BY
    SrvPacketID;

  /* Обработчик отсутствующей записи при FETCH следующей записи*/
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* Перебираем диапазоны и сокращаем разрывы если это возможно  */
  SET @DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;

  WHILE @@fetch_status = 0
  BEGIN
    /* Заполнение временной таблицы дублирующими данными телетрека
       для анализа разрывов */
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT DISTINCT LogID
                  , SrvPacketID
    FROM
      datagpsbuffer_ontmp64
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (SrvPacketID > @BeginSrvPacketID)
      AND (SrvPacketID < @EndSrvPacketID)
    ORDER BY
      SrvPacketID;

     /* Если есть данные модифицируем диапазон */  
    SET @RowCountForAnalyze = (SELECT count(1)
                               FROM
                                 datagpslost_ontmp);

    IF @RowCountForAnalyze > 0
    BEGIN
      /* Добавляем первую и последнюю записи для анализа */ 
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@BeginLogID, @BeginSrvPacketID)
      , (@EndLogID, @EndSrvPacketID);

       /* Удаляем старый диапазон */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        --DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
        --DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
        --DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --SET @FirstLogID = -9999999;
        --DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
        --DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

        /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
        --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* Формирование команды-вставки в таблицу пропущенных диапазонов */
        --SET DataExists = 1;
        OPEN CursorNewLogID2;
        FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;

        WHILE @@fetch_status = 0
        BEGIN
          IF @FirstLogID = -9999999
          BEGIN
            /* Первое значение в списке */
            SET @FirstLogID = @NewLogID;
            SET @FirstSrvPacketID = @NewSrvPacketID;
          END
          ELSE
          BEGIN
            /* Основная ветка расчета*/
            SET @SecondLogID = @NewLogID;
            SET @SecondSrvPacketID = @NewSrvPacketID;

            IF (@SecondSrvPacketID - @FirstSrvPacketID) > 1
            BEGIN
              /* Найден разрыв */
              EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

              IF @NeedOptimize = 0
              BEGIN
                SET @NeedOptimize = 1;
              END; -- IF

            END; -- IF
            /* Переместим текущее второе значение на первую позицию
               для участия в следующей итерации в качестве начального значения */
            SET @FirstLogID = @SecondLogID;
            SET @FirstSrvPacketID = @SecondSrvPacketID;
          END; -- ELSE  
          FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;
        END; -- WHILE;
        CLOSE CursorNewLogID2;
      --DEALLOCATE CursorNewLogID2;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END;

    FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;
  END;
  CLOSE CursorLostRanges;
  --DEALLOCATE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* Если пропущенных записей у данного телетрека нет - 
     тогда ConfirmedID будет равен максимальному LogID 
     данного телетрека. Иначе - минимальный Begin_LogID
     из таблицы пропусков */
  SELECT @NewConfirmedID = min(Begin_LogID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @NewConfirmedID IS NULL
  BEGIN
    SET @NewConfirmedID = (SELECT max(LogID)
                           FROM
                             datagps64
                           WHERE
                             Mobitel_ID = @MobitelID);
  END;

  /* Обновим ConfirmedID если надо */
  UPDATE mobitels
  SET
    ConfirmedID = @NewConfirmedID
  WHERE
    (Mobitel_ID = @MobitelID) AND
    (ConfirmedID < @NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END

GO

--
-- Создать процедуру "dbo.OnLostDataGPS2"
--
GO
PRINT (N'Создать процедуру "dbo.OnLostDataGPS2"')
GO
CREATE PROCEDURE dbo.OnLostDataGPS2
(
  @MobitelID    INTEGER,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT 'Модификация диапазонов пропусков. Анализ дублирующих данных зада'
BEGIN
  /********** DECLARE **********/

  DECLARE @NewConfirmedID INT;
  SET @NewConfirmedID = 0; /* ????? ???????? ConfirmedID */
  DECLARE @BeginLogID INT; /* Begin_LogID ? ??????? CursorLostRanges */
  DECLARE @EndLogID INT; /* End_LogID ? ??????? CursorLostRanges */
  DECLARE @BeginSrvPacketID BIGINT; /* Begin_SrvPacketID ? ??????? CursorLostRanges */
  DECLARE @EndSrvPacketID BIGINT; /* End_SrvPacketID ? ??????? CursorLostRanges */
  DECLARE @RowCountForAnalyze INT; /* ?????????? ??????? ??? ??????? */
  SET @RowCountForAnalyze = 0;
  DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  SET @DataExists = 1;
  DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
  DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
  DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
  DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
  DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

  /* ?????? ??? ??????? ?? ?????????? ????????? ????????? */
  DECLARE CursorLostRanges CURSOR LOCAL FOR
  SELECT Begin_LogID
       , End_LogID
       , Begin_SrvPacketID
       , End_SrvPacketID
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* ?????? ??? ??????? ?? ????? LogID */
  DECLARE CursorNewLogID2 CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp
  ORDER BY
    SrvPacketID;

  /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* ?????????? ????????? ? ????????? ??????? ???? ??? ???????? */
  SET @DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;

  WHILE @@fetch_status = 0
  BEGIN
  
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT DISTINCT LogID
                  , SrvPacketID
    FROM
      datagpsbuffer_ontmp
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (SrvPacketID > @BeginSrvPacketID)
      AND (SrvPacketID < @EndSrvPacketID)
    ORDER BY
      SrvPacketID;

    /* ???? ???? ?????? ???????????? ???????? */
    SET @RowCountForAnalyze = (SELECT count(1)
                               FROM
                                 datagpslost_ontmp);

    IF @RowCountForAnalyze > 0
    BEGIN
      /* ????????? ?????? ? ????????? ?????? ??? ??????? */
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@BeginLogID, @BeginSrvPacketID)
      , (@EndLogID, @EndSrvPacketID);

      /* ??????? ?????? ???????? */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        --DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
        --DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
        --DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --SET @FirstLogID = -9999999;
        --DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
        --DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

        /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
        --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* ???????????? ???????-??????? ? ??????? ??????????? ?????????? */
        --SET DataExists = 1;
        OPEN CursorNewLogID2;
        FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;

        WHILE @@fetch_status = 0
        BEGIN
          IF @FirstLogID = -9999999
          BEGIN
            /* ?????? ???????? ? ?????? */
            SET @FirstLogID = @NewLogID;
            SET @FirstSrvPacketID = @NewSrvPacketID;
          END
          ELSE
          BEGIN
            /* ???????? ????? ???????*/
            SET @SecondLogID = @NewLogID;
            SET @SecondSrvPacketID = @NewSrvPacketID;

            IF (@SecondSrvPacketID - @FirstSrvPacketID) > 1
            BEGIN
              /* ?????? ?????? */
              EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

              IF @NeedOptimize = 0
              BEGIN
                SET @NeedOptimize = 1;
              END; -- IF

            END; -- IF
            /* ?????????? ??????? ?????? ???????? ?? ?????? ???????
               ??? ??????? ? ????????? ???????? ? ???????? ?????????? ???????? */
            SET @FirstLogID = @SecondLogID;
            SET @FirstSrvPacketID = @SecondSrvPacketID;
          END; -- ELSE  
          FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;
        END; -- WHILE;
        CLOSE CursorNewLogID2;
      --DEALLOCATE CursorNewLogID2;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END;

    FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;
  END;
  CLOSE CursorLostRanges;
  --DEALLOCATE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* ???? ??????????? ??????? ? ??????? ????????? ??? - 
     ????? ConfirmedID ????? ????? ????????????? LogID 
     ??????? ?????????. ????? - ??????????? Begin_LogID
     ?? ??????? ????????? */
  SELECT @NewConfirmedID = min(Begin_LogID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @NewConfirmedID IS NULL
  BEGIN
    SET @NewConfirmedID = (SELECT max(LogID)
                           FROM
                             datagps
                           WHERE
                             Mobitel_ID = @MobitelID);
  END;

  /* ??????? ConfirmedID ???? ???? */
  UPDATE mobitels
  SET
    ConfirmedID = @NewConfirmedID
  WHERE
    (Mobitel_ID = @MobitelID) AND
    (ConfirmedID < @NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END


GO

--
-- Создать процедуру "dbo.OnLostDataGPS"
--
GO
PRINT (N'Создать процедуру "dbo.OnLostDataGPS"')
GO


CREATE PROCEDURE dbo.OnLostDataGPS
(
  @MobitelID    INT,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT 'Поиск пропусков в online данных заданного телетрека и заполнение'
BEGIN

  /********** DECLARE **********/

  DECLARE @CurrentConfirmedID INT; 
  SET @CurrentConfirmedID = 0;
  DECLARE @NewConfirmedID INT; 
  SET @NewConfirmedID = 0;
  DECLARE @StartLogIDSelect INT;
  SET @StartLogIDSelect = 0;
  DECLARE @FinishLogIDSelect INT;
  SET @FinishLogIDSelect = 0;
  DECLARE @MaxEndLogID INT; 
  SET @MaxEndLogID = 0;

  DECLARE @MinLogID_n INT; 
  SET @MinLogID_n = 0;
  DECLARE @MaxLogID_n INT; 
  SET @MaxLogID_n = 0;

  DECLARE @DataExists TINYINT; 
  SET @DataExists = 1;

  DECLARE @NewLogID INT;
  DECLARE @NewSrvPacketID BIGINT; 
  DECLARE @FirstLogID INT;
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; 
  DECLARE @FirstSrvPacketID BIGINT; 
  DECLARE @SecondSrvPacketID BIGINT; 

  DECLARE @tmpSrvPacketID BIGINT;
  SET @tmpSrvPacketID = 0;

  DECLARE CursorNewLogID CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  SET @CurrentConfirmedID = (SELECT coalesce(ConfirmedID, 0)
                             FROM
                               mobitels
                             WHERE
                               Mobitel_ID = @MobitelID);


  SELECT @MinLogID_n = min(LogID)
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelID)
    AND (LogID > @CurrentConfirmedID);


  SET @MaxLogID_n = (SELECT max(LogID)
                     FROM
                       datagpsbuffer_on
                     WHERE
                       Mobitel_ID = @MobitelID);


  SELECT @MaxEndLogID = coalesce(max(End_LogID), 0)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;


  IF @MinLogID_n IS NOT NULL
  BEGIN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF @MinLogID_n < @MaxEndLogID
    BEGIN
     
      SET @StartLogIDSelect = (SELECT coalesce(Begin_LogID, @MinLogID_n)
                               FROM
                                 datagpslost_on
                               WHERE
                                 (Mobitel_ID = @MobitelID)
                                 AND (@MinLogID_n > Begin_LogID)
                                 AND (@MinLogID_n < End_LogID));


      SET @FinishLogIDSelect = (SELECT coalesce(End_LogID, @MaxLogID_n)
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (@MaxLogID_n > Begin_LogID)
                                  AND (@MaxLogID_n < End_LogID));

   
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID >= @StartLogIDSelect) AND
        (Begin_LogID <= @FinishLogIDSelect);

    
      IF (@StartLogIDSelect = 0) AND (NOT EXISTS (SELECT 1
                                                  FROM
                                                    datagps
                                                  WHERE
                                                    (Mobitel_ID = @MobitelID)
                                                    AND (LogID = @StartLogIDSelect)))
      BEGIN
        INSERT INTO datagpslost_ontmp(LogID
                                    , SrvPacketID)
        VALUES
          (0, 0);
      END;
    END;
    ELSE
    BEGIN

   
      SELECT @StartLogIDSelect = coalesce(max(End_LogID), @CurrentConfirmedID)
      FROM
        datagpslost_on
      WHERE
        Mobitel_ID = @MobitelID;

    
      SELECT @FinishLogIDSelect = max(LogID)
      FROM
        datagpsbuffer_on
      WHERE
        Mobitel_ID = @MobitelID;

    
      SELECT @tmpSrvPacketID = coalesce(SrvPacketID, 0)
      FROM
        datagps
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (LogID = @StartLogIDSelect);

     
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@StartLogIDSelect, @tmpSrvPacketID);
    END; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

  
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT LogID
         , SrvPacketID
    FROM
      datagps
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (LogID >= @StartLogIDSelect)
      AND (LogID <= @FinishLogIDSelect)
    ORDER BY
      LogID;

    
    --SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstLogID = -9999999
      BEGIN
      
        SET @FirstLogID = @NewLogID;
        SET @FirstSrvPacketID = @NewSrvPacketID;
      END
      ELSE
      BEGIN
      
        SET @SecondLogID = @NewLogID;
        SET @SecondSrvPacketID = @NewSrvPacketID;

        IF (@SecondLogID - @FirstLogID) > 1
        BEGIN
         
          EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

          IF @NeedOptimize = 0
          BEGIN
            SET @NeedOptimize = 1;
          END; -- IF

        END; -- IF
       
        SET @FirstLogID = @SecondLogID;
        SET @FirstSrvPacketID = @SecondSrvPacketID;
      END; -- IF
      FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    END; -- WHILE
    CLOSE CursorNewLogID;
    --DEALLOCATE CursorNewLogID; 

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

   
    SET @NewConfirmedID = (SELECT min(Begin_LogID)
                           FROM
                             datagpslost_on
                           WHERE
                             Mobitel_ID = @MobitelID);

    IF @NewConfirmedID IS NULL
    BEGIN
      SET @NewConfirmedID = (SELECT max(LogID)
                             FROM
                               datagps
                             WHERE
                               Mobitel_ID = @MobitelID);
    END;

   
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (ConfirmedID < @NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END; -- IF MaxEndLogID IS NOT NULL  
END



GO

--
-- Создать представление "dbo.id_gps_id_mobitel"
--
GO
PRINT (N'Создать представление "dbo.id_gps_id_mobitel"')
GO
CREATE VIEW dbo.id_gps_id_mobitel 
AS



SELECT max(online.DataGps_ID) AS expr1
     , online.Mobitel_ID
FROM
  dbo.online
GROUP BY
  online.Mobitel_ID
GO

--
-- Создать представление "dbo.internalmobitelconfigview"
--
GO
PRINT (N'Создать представление "dbo.internalmobitelconfigview"')
GO

CREATE VIEW dbo.internalmobitelconfigview
AS (SELECT c.ID AS ID
         , max(c.InternalMobitelConfig_ID) AS LastRecord
   FROM
     internalmobitelconfig c
   WHERE
     (c.devIdShort <> '')
   GROUP BY
     c.ID);

GO

--
-- Создать представление "dbo.deviceview"
--
GO
PRINT (N'Создать представление "dbo.deviceview"')
GO

CREATE VIEW dbo.deviceview
AS (SELECT m.Mobitel_ID AS Mobitel_ID
         , m.Name AS Name
         , m.Descr AS Descr
         , c.devIdShort AS devIdShort
         , m.DeviceType AS DeviceType
         , m.ConfirmedID AS ConfirmedID
         , c.moduleIdRf AS moduleIdRf
   FROM
     ((mobitels m
     JOIN internalmobitelconfigview v
       ON v.ID = m.InternalMobitelConfig_ID)
     JOIN internalmobitelconfig c
       ON v.LastRecord = c.InternalMobitelConfig_ID));

GO

--
-- Создать процедуру "dbo.OnDeleteDuplicates64"
--
GO
PRINT (N'Создать процедуру "dbo.OnDeleteDuplicates64"')
GO


CREATE PROCEDURE dbo.OnDeleteDuplicates64
AS
BEGIN
  /* Удаление повторяющихся строк из datagpsbuffer_on64' */

  /* Текущий MobitelId в курсоре CursorDuplicateGroups*/
  DECLARE @MobitelIdInCursor INT;
  SET @MobitelIdInCursor = 0;
  /* Текущий LogID в курсоре CursorDuplicateGroups  */
  DECLARE @LogIdInCursor INT;
  SET @LogIdInCursor = 0;
  /* Max SrvPacketId в множестве строк текущей группы дубликатов */
  DECLARE @MaxSrvPacketId BIGINT;
  SET @MaxSrvPacketId = 0;
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /* Курсор для прохода по всем группам дубликатов */
  DECLARE CursorDuplicateGroups CURSOR LOCAL FOR
  SELECT Mobitel_Id
       , LogId
  FROM
    datagpsbuffer_on64
  GROUP BY
    Mobitel_Id
  , LogId
  HAVING
    count(LogId) > 1;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET @DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SELECT @MaxSrvPacketId = max(SrvPacketID)
    FROM
      datagpsbuffer_on64
    WHERE
      (Mobitel_ID = @MobitelIdInCursor)
      AND (LogId = @LogIdInCursor);
    DELETE
    FROM
      datagpsbuffer_on64
    WHERE
      (Mobitel_ID = @MobitelIdInCursor) AND
      (LogId = @LogIdInCursor) AND
      (SrvPacketID < @MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  END;
  CLOSE CursorDuplicateGroups;
--DEALLOCATE CursorDuplicateGroups;
END

GO

--
-- Создать процедуру "dbo.delDupSrvPacket"
--
GO
PRINT (N'Создать процедуру "dbo.delDupSrvPacket"')
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDUREdbo.delDupSrvPacket
AS
BEGIN
    -- Insert statements for procedure here
	if exists (
        select * from tempdb.dbo.sysobjects o
        where o.xtype in ('U') 
        and o.id = object_id(N'tempdb..#tmp64')
)
BEGIN
  DROP TABLE #tmp64; 
END;

if object_id('tempdb..#tmp64') is not null drop table #tmp64  -- проверка на существование таблицы

create table #tmp64 -- создание временной таблицы
    (mobi int, lat int, lng int, dir int, acc int, unixt int, spd int, vald int, stl int, rsg int, evn int, snsset int, sns varbinary(50), vlt int, dgp varchar(32), lid int, spid bigint)

insert into #tmp64 SELECT 
  d.Mobitel_ID,
  d.Latitude,
  d.Longitude,
  d.Direction,
  d.Acceleration,
  d.UnixTime,
  d.Speed,
  d.Valid,
  d.Satellites,
  d.RssiGsm,
  d.[Events],
  d.SensorsSet,
  d.Sensors,
  d.Voltage,
  d.DGPS,
  d.LogID,
  d.SrvPacketID
FROM datagpsbuffer_on64 d;
--GROUP BY d.SrvPacketID;

TRUNCATE TABLE datagpsbuffer_on64;

INSERT INTO dbo.datagpsbuffer_on64 SELECT d.mobi,
  d.lat,
  d.lng,
  d.dir,
  d.acc,
  d.unixt,
  d.spd,
  d.vald,
  d.stl,
  d.rsg,
  d.evn,
  d.snsset,
  d.sns,
  d.vlt,
  d.dgp,
  d.lid,
  d.spid
FROM #tmp64 d;

-- Insert statements for procedure here
	if exists (
        select * from tempdb.dbo.sysobjects o
        where o.xtype in ('U') 
        and o.id = object_id(N'tempdb..#tmp64')
)
BEGIN
  DROP TABLE #tmp64; 
END;

if object_id('tempdb..#tmp64') is not null drop table #tmp64  -- проверка на существование таблицы

END
GO

--
-- Создать процедуру "dbo.OnDeleteDuplicates"
--
GO
PRINT (N'Создать процедуру "dbo.OnDeleteDuplicates"')
GO

CREATE PROCEDURE dbo.OnDeleteDuplicates
AS
BEGIN
  DECLARE @MobitelIdInCursor INT;
  SET @MobitelIdInCursor = 0;
  DECLARE @LogIdInCursor INT;
  SET @LogIdInCursor = 0;
  DECLARE @MaxSrvPacketId BIGINT;
  SET @MaxSrvPacketId = 0;
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  DECLARE CursorDuplicateGroups CURSOR LOCAL FOR
  SELECT Mobitel_Id
       , LogId
  FROM
    datagpsbuffer_on
  GROUP BY
    Mobitel_Id
  , LogId
  HAVING
    count(LogId) > 1;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET @DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SELECT @MaxSrvPacketId = max(SrvPacketID)
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor)
      AND (LogId = @LogIdInCursor);
    DELETE
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor) AND
      (LogId = @LogIdInCursor) AND
      (SrvPacketID < @MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  END;
  CLOSE CursorDuplicateGroups;
--DEALLOCATE CursorDuplicateGroups;
END

GO

--
-- Создать процедуру "dbo.OnCorrectInfotrackLogId"
--
GO
PRINT (N'Создать процедуру "dbo.OnCorrectInfotrackLogId"')
GO


CREATE PROCEDURE dbo.OnCorrectInfotrackLogId
AS
--SQL SECURITY INVOKER
--COMMENT 'Корректировка значений LogId и InMobitelID в данных инфотреков'
BEGIN
  DECLARE @MobitelIDInCursor INT; 
  DECLARE @DataExists TINYINT; 
  SET @DataExists = 1;
  DECLARE @MaxLogId INT; 
  SET @MaxLogId = 0;
  DECLARE @SrvPacketIDInCursor BIGINT; 
  SET @SrvPacketIDInCursor = 0;

  DECLARE CursorInfoTrack CURSOR LOCAL FOR
  SELECT DISTINCT buf.Mobitel_ID
  FROM
    datagpsbuffer_on buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort LIKE 'I%')
    AND (buf.LogId = 0)
  ORDER BY
    1;

  DECLARE CursorInfoTrackUpdateSet CURSOR LOCAL FOR
  SELECT SrvPacketID
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelIDInCursor)
    AND (LogId = 0)
  ORDER BY
    UnixTime;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SET @MaxLogId = (SELECT coalesce(max(LogID), 0)
                     FROM
                       DataGps
                     WHERE
                       Mobitel_ID = @MobitelIDInCursor);

    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      SET @MaxLogId = @MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET
        LogId = @MaxLogId, InMobitelID = @MaxLogId
      WHERE
        SrvPacketID = @SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    END;
    CLOSE CursorInfoTrackUpdateSet;

    SET @DataExists = 1;
    FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  END;
  CLOSE CursorInfoTrack;
END

GO

--
-- Создать функцию "dbo.UNIX_TIMESTAMPS"
--
GO
PRINT (N'Создать функцию "dbo.UNIX_TIMESTAMPS"')
GO

CREATE FUNCTION dbo.UNIX_TIMESTAMPS
(
  @ctimestamp DATETIME
)
RETURNS INTEGER
AS
BEGIN
  /* Function body */
  DECLARE @return INTEGER;
  DECLARE @GMT DATETIME;

  SET @GMT = getdate() - getutcdate();
  SET @ctimestamp = @ctimestamp - @GMT;
  SELECT @return = datediff(SECOND, { D '1970-01-01' }, @ctimestamp)
  RETURN @return
END

GO

--
-- Создать процедуру "dbo.OnTransferBuffer64"
--
GO
PRINT (N'Создать процедуру "dbo.OnTransferBuffer64"')
GO
CREATE PROCEDURE dbo.OnTransferBuffer64
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  DECLARE @DataExists TINYINT; /*  */
  SET @DataExists = 1;
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on64;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp64;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC delDupSrvPacket
  EXEC OnDeleteDuplicates64
  --EXEC OnCorrectInfotrackLogId64
  --EXEC OnCorrectInfotrackLogId64

   /* Сохраняем записи, которые уже есть в БД, для второго этапа анализа пропусков 
     Используется процедурой OnLostDataGPS264. */
  /*INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID)
    SELECT b.Mobitel_ID, b.LogID, b.SrvPacketID
    FROM datagpsbuffer_on64 b LEFT JOIN datagps64 d ON
      (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL  GROUP BY d.SrvPacketID;*/
    
  IF OBJECT_ID(N'tempdb..#temp1', N'U') IS NOT NULL   
	DROP TABLE #temp1;

  CREATE TABLE #temp1 (Mobitel_ID int, LogID int, SrvPacketID bigint);
  insert into #temp1  SELECT
      b.Mobitel_ID,
      b.LogID,
      b.SrvPacketID
    FROM datagpsbuffer_on64 b
      LEFT JOIN datagps64 d
        ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL; --GROUP BY d.SrvPacketID;

	INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID) SELECT * FROM #temp1 GROUP BY SrvPacketID;

	IF OBJECT_ID(N'tempdb..#temp1', N'U') IS NOT NULL   
		DROP TABLE #temp1;

  /* -------------------- Заполнение таблицы Online ---------------------- */
  /* Удаление из таблицы online дублирующих строк таблицы datagpsbuffer_on64 */
  /* по полям Mobitel_ID и LogID */
   DELETE o
  FROM online64 o, datagpsbuffer_on64 dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online64 WITH (INDEX (IDX_Mobitelid64))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
	      INSERT INTO online64 (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude,Acceleration, 
      Direction, Speed, Valid, [Events],
      Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude,Acceleration, 
      Direction, Speed, Valid, [Events],
      Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS
    FROM (
      SELECT TOP 100 *
      FROM datagpsbuffer_on64
      WHERE (Mobitel_ID = @MobitelIDInCursor)  AND 
        (UnixTime > @TmpMaxUnixTime) AND (Valid = 1)
      ORDER BY UnixTime DESC
      ) AS T
    ORDER BY UnixTime ASC;

     /* Удаление строк из таблицы оперативных данных не удовлетворяющие
       условиям хранения - не являются последними 100 строками */
    DELETE
    FROM
      online64
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        [online64]
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));


    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  UPDATE datagps64 SET
    Latitude = b.Latitude,
    Longitude = b.Longitude,
    Acceleration = b.Acceleration,
    UnixTime = b.UnixTime,
    Speed = b.Speed,
    Direction = b.Direction,
    Valid = b.Valid,
    LogID = b.LogID,
    [Events] = [b].[Events],
    Satellites = b.Satellites,
    RssiGsm = b.RssiGsm,
    SensorsSet = b.SensorsSet,
    Sensors = b.Sensors,
    Voltage = b.Voltage,
    DGPS = b.DGPS,
    SrvPacketID = b.SrvPacketID
  FROM
    datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    AND (d.SrvPacketID < b.SrvPacketID);
  /*  */
  DELETE b
  FROM datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);
  /* */
    INSERT INTO datagps64 (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, [Events], 
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, [Events], 
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID
  FROM datagpsbuffer_on64;
  --GROUP BY Mobitel_ID,LogID;
  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS64 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);*/

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on64;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET @DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS264 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = CREATEST(@NeedOptimize, @NeedOptimizeParam); */

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsUpdateSet;
  --DEALLOCATE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp64;

  /*  */
  IF @NeedOptimize = 1
    ALTER INDEX ALL ON datagpslost_on REORGANIZE
END

GO

--
-- Создать процедуру "dbo.OnTransferBuffer_orig"
--
GO
PRINT (N'Создать процедуру "dbo.OnTransferBuffer_orig"')
GO
CREATE PROCEDURE dbo.OnTransferBuffer_orig
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  --DECLARE @DataExists TINYINT; /*  */
  --SET @DataExists = 1;
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC OnDeleteDuplicates
  EXEC OnCorrectInfotrackLogId

  /* OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp(Mobitel_ID
                                , LogID
                                , SrvPacketID)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.SrvPacketID
  FROM
    datagpsbuffer_on b
    LEFT JOIN datagps d
      ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  WHERE
    d.LogID IS NOT NULL;

  /* --------------------  Online ---------------------- */
  /*  online  datagpsbuffer_on */
  /*  Mobitel_ID LogID */
  DELETE o
  FROM
    online o, datagpsbuffer_on dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
    INSERT INTO dbo.online(Mobitel_ID
                         , LogID
                         , UnixTime
                         , Latitude
                         , Longitude
                         , Altitude
                         , Direction
                         , Speed
                         , Valid
                         , Events
                         , Sensor1
                         , Sensor2
                         , Sensor3
                         , Sensor4
                         , Sensor5
                         , Sensor6
                         , Sensor7
                         , Sensor8
                         , Counter1
                         , Counter2
                         , Counter3
                         , Counter4
                         , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , Events
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_on
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;

    /*  100 */
    DELETE
    FROM
      online
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        online
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  /* .. */
  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = b.LogID, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4, SrvPacketID = b.SrvPacketID
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID) AND
    (d.SrvPacketID < b.SrvPacketID);

  /*  */
  DELETE b
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  /* */
  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs
                    , SrvPacketID)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , Events
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
       , SrvPacketID
  FROM
    datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);*/

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS2 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = CREATEST(@NeedOptimize, @NeedOptimizeParam); */

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsUpdateSet;
  --DEALLOCATE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  --/*  */
  --IF @NeedOptimize = 1
  --  ALTER INDEX ALL ON datagpslost_on REORGANIZE
END
GO

--
-- Создать процедуру "dbo.OnTransferBuffer"
--
GO
PRINT (N'Создать процедуру "dbo.OnTransferBuffer"')
GO


CREATE PROCEDURE dbo.OnTransferBuffer
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  DECLARE @DataExists TINYINT; /*  */
  SET @DataExists = 1;
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC OnDeleteDuplicates
  EXEC OnCorrectInfotrackLogId

  /* OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp(Mobitel_ID
                                , LogID
                                , SrvPacketID)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.SrvPacketID
  FROM
    datagpsbuffer_on b
    LEFT JOIN datagps d
      ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  WHERE
    d.LogID IS NOT NULL;

  /* --------------------  Online ---------------------- */
  /*  online  datagpsbuffer_on */
  /*  Mobitel_ID LogID */
  DELETE o
  FROM
    [online] o, datagpsbuffer_on dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             [online] WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
    INSERT INTO dbo.online(Mobitel_ID
                         , LogID
                         , UnixTime
                         , Latitude
                         , Longitude
                         , Altitude
                         , Direction
                         , Speed
                         , Valid
                         , Events
                         , Sensor1
                         , Sensor2
                         , Sensor3
                         , Sensor4
                         , Sensor5
                         , Sensor6
                         , Sensor7
                         , Sensor8
                         , Counter1
                         , Counter2
                         , Counter3
                         , Counter4
                         , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , Events
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_on
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;

    /*  100 */
    DELETE
    FROM
      [online]
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        [online]
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  /* .. */
  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = b.LogID, [Events] = [b].[Events], Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4, SrvPacketID = b.SrvPacketID
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID) AND
    (d.SrvPacketID < b.SrvPacketID);

  /*  */
  DELETE b
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  /* */
  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , [Events]
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs
                    , SrvPacketID)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , [Events]
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
       , SrvPacketID
  FROM
    datagpsbuffer_on;
	--GROUP BY Mobitel_ID, LogID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS @MobitelIDInCursor, @NeedOptimizeParam OUT
    
    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS2 @MobitelIDInCursor, @NeedOptimizeParam OUT

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  IF @NeedOptimize = 1 begin
    ALTER INDEX ALL ON datagpslost_on REORGANIZE
  END;

  --/*  */
  --IF @NeedOptimize = 1
  --  ALTER INDEX ALL ON datagpslost_on REORGANIZE
END

GO

--
-- Создать процедуру "dbo.DeleteDataGPS_1000"
--
GO
PRINT (N'Создать процедуру "dbo.DeleteDataGPS_1000"')
GO
CREATE PROCEDURE dbo.DeleteDataGPS_1000
 @BeginDate datetime = null
,@EndDate	datetime = null
AS
BEGIN
set nocount on
-- exec DeleteDataGPS '20130601', '20131231'
if	(	(@BeginDate is null )
	and (@EndDate is null )
	)
	begin
	 RAISERROR ('@BeginDate and @EndDate is null - I dont delete all data!!!!', -- Message text.
				   13, -- Severity.
				   1 -- State.
				   );
	return 0
	end 

  declare @BeginDateUnix int = dbo.UNIX_TIMESTAMPS(@BeginDate)
  declare @EndDateUnix int = dbo.UNIX_TIMESTAMPS(@EndDate)
  declare @rcount int = 0
			,@rtotal int = 0
			,@StartScript datetime = getdate()
  
	
	CREATE TABLE #Mobitels (
		[Mobitel_ID] [int] NULL
	)
	
	insert into #Mobitels
				(Mobitel_ID)
		select distinct 
			Mobitel_ID
		from dbo.datagps (nolock)
	
	Declare @Count int = 0,
			@CurNumber int = 0
	select	 @Count  = count(1) 
			,@CurNumber = 0
	from #Mobitels
	print 'Count Mobitels: ' + cast(@Count as varchar(255))
	
	DECLARE cur1 CURSOR
	READ_ONLY
	FOR   select Mobitel_ID
			from #Mobitels 
			order by 1

	DECLARE @Mobitel_ID int
	DECLARE @error int, @rc int
	OPEN cur1

	FETCH NEXT FROM cur1 INTO @Mobitel_ID 
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
			select @CurNumber = @CurNumber + 1	
			SELECT  @rc = 1
            
            WHILE @rc <> 0
			BEGIN
				BEGIN TRAN

				delete top (10000)
			    from datagps  
			    where	Mobitel_ID = @Mobitel_ID
				     and UnixTime >= coalesce(@BeginDateUnix, 0)
				     and (	@EndDateUnix is null
					 or (	@EndDateUnix is not null
						and UnixTime < @EndDateUnix 
						)
					)	
								    
			    select @rcount = @@ROWCOUNT,@rc = @@ROWCOUNT
			    select @rtotal = @rtotal + @rcount
			    print 'Current number: ' + cast(@CurNumber as varchar(255))+ 
		    		', MobitelID = ' + cast(@Mobitel_ID as varchar(255)) + 
			    	', Rows deleted: ' + cast(@rcount as varchar(255))

				SELECT @error = @@ERROR

				IF @error <> 0
				   BEGIN
				    ROLLBACK TRAN
					GOTO EXIT_PROC
				   END

				COMMIT TRAN
			END			
		END
		FETCH NEXT FROM cur1 INTO @Mobitel_ID 
	END
	
	EXIT_PROC:
    
	CLOSE cur1
	DEALLOCATE cur1
	
	drop table #Mobitels
	print '-------------------------------'
	print 'Total deleted rows: ' + cast(@rtotal as varchar(255))
	print 'Script work seconds: ' + cast(datediff(second, @StartScript, getdate()) as varchar(255))
END
GO

--
-- Создать процедуру "dbo.DeleteDataGPS"
--
GO
PRINT (N'Создать процедуру "dbo.DeleteDataGPS"')
GO
CREATE PROCEDURE dbo.DeleteDataGPS
 @BeginDate datetime = null
,@EndDate	datetime = null
AS
BEGIN
set nocount on
-- exec DeleteDataGPS '20130601', '20131231'
if	(	(@BeginDate is null )
	and (@EndDate is null )
	)
	begin
	 RAISERROR ('@BeginDate and @EndDate is null - I dont delete all data!!!!', -- Message text.
				   13, -- Severity.
				   1 -- State.
				   );
	return 0
	end 

  declare @BeginDateUnix int = dbo.UNIX_TIMESTAMPS(@BeginDate)
  declare @EndDateUnix int = dbo.UNIX_TIMESTAMPS(@EndDate)
  declare @rcount int = 0
			,@rtotal int = 0
			,@StartScript datetime = getdate()
  
	
	CREATE TABLE #Mobitels (
		[Mobitel_ID] [int] NULL
	)
	
	insert into #Mobitels
				(Mobitel_ID)
		select distinct 
			Mobitel_ID
		from dbo.datagps (nolock)
	
	Declare @Count int = 0,
			@CurNumber int = 0
	select	 @Count  = count(1) 
			,@CurNumber = 0
	from #Mobitels
	print 'Count Mobitels: ' + cast(@Count as varchar(255))
	
	DECLARE cur1 CURSOR
	READ_ONLY
	FOR   select Mobitel_ID
			from #Mobitels 
			order by 1

	DECLARE @Mobitel_ID int
	OPEN cur1

	FETCH NEXT FROM cur1 INTO @Mobitel_ID 
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
			select @CurNumber = @CurNumber + 1
			
			
			delete 
			from datagps  
			where	Mobitel_ID = @Mobitel_ID
				and UnixTime >= coalesce(@BeginDateUnix, 0)
				and (	@EndDateUnix is null
					or (	@EndDateUnix is not null
						and UnixTime < @EndDateUnix 
						)
					)
			
			select @rcount = @@ROWCOUNT
			select @rtotal = @rtotal + @rcount
			print 'Current number: ' + cast(@CurNumber as varchar(255))+ 
				', MobitelID = ' + cast(@Mobitel_ID as varchar(255)) + 
				', Rows deleted: ' + cast(@rcount as varchar(255))
		END
		FETCH NEXT FROM cur1 INTO @Mobitel_ID 
	END

	CLOSE cur1
	DEALLOCATE cur1
	
	drop table #Mobitels
	print '-------------------------------'
	print 'Total deleted rows: ' + cast(@rtotal as varchar(255))
	print 'Script work seconds: ' + cast(datediff(second, @StartScript, getdate()) as varchar(255))
END
GO

--
-- Создать функцию "dbo.From_UnixTime"
--
GO
PRINT (N'Создать функцию "dbo.From_UnixTime"')
GO
CREATE FUNCTION dbo.From_UnixTime
(
  @timestamp INTEGER
)
RETURNS DATETIME
AS
BEGIN
  /* Function body */
  DECLARE @return DATETIME
  DECLARE @GMT DATETIME;
  DECLARE @tm DATETIME;

  SET @GMT = getdate() - getutcdate();
  SET @tm = dateadd(SECOND, @timestamp, { D '1970-01-01' });
  SET @return = @tm + @GMT;
RETURN @return
END
GO

--
-- Создать процедуру "dbo.ua_dedez"
--
GO
PRINT (N'Создать процедуру "dbo.ua_dedez"')
GO
CREATE PROCEDURE dbo.ua_dedez AS
DECLARE @iidd int
DECLARE @cursor CURSOR

SET @cursor = CURSOR SCROLL
FOR 
  SELECT vehicle.Mobitel_id FROM vehicle WHERE NumberPlate LIKE 'ТГ%'
OPEN @cursor

FETCH NEXT FROM @cursor INTO @iidd
WHILE @@fetch_status=0
BEGIN
  IF NOt EXISTS(SELECT vehicle.Mobitel_id FROM vehicle WHERE Mobitel_id=@iidd)
BEGIN
INSERT INTO ua_gsp_on(taim, tz) SELECT DISTINCT dbo.From_UnixTime(round(datagps.UnixTime / 600, 0) * 600) AS expr1, vehicle.NumberPlate
FROM  dbo.vehicle
  INNER JOIN dbo.datagps WITH (INDEX (Index_3))
    ON vehicle.Mobitel_id = datagps.Mobitel_ID
WHERE
  datagps.Mobitel_ID = @iidd  AND dbo.from_unixtime(datagps.UnixTime) BETWEEN '08.08.2016 08:00' AND '08.09.2016 07:59'
  END
FETCH NEXT FROM @cursor INTO @iidd
  END
  CLOSE @cursor
GO

--
-- Создать процедуру "dbo.SelectDataGpsPeriodMonth"
--
GO
PRINT (N'Создать процедуру "dbo.SelectDataGpsPeriodMonth"')
GO

CREATE PROCEDURE dbo.SelectDataGpsPeriodMonth @mobile int, @beginperiod datetime, @endperiod datetime,
@daymonth varchar(82)
AS
BEGIN
IF object_id('tempdb..#tempperiod2') IS NOT NULL
    DROP TABLE #tempperiod2;

  CREATE TABLE #tempperiod2 (DataGps_ID int, Mobitel_ID int, Lat int, Lon int, Altitude int, [time] int, speed int, Valid int, 
  sensor bigint, [Events] bigint, LogID int);
  
  INSERT INTO #tempperiod2 SELECT
    DataGps_ID AS DataGps_ID,
    Mobitel_ID AS Mobitel_ID,
    ((Latitude * 1.0000) / 600000) AS Lat,
    ((Longitude * 1.0000) / 600000) AS Lon,
    Altitude AS Altitude,
    dbo.From_UnixTime(UnixTime) AS [time],
    (Speed * 1.852) AS speed,
    Valid AS Valid,
	(Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor,
    [Events] AS [Events],
    LogID AS LogID
  FROM datagps
  WHERE Mobitel_ID = @mobile AND
  (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@beginperiod) AND dbo.UNIX_TIMESTAMPS(@endperiod)) AND
  (Valid = 1) AND NOT (Latitude = 0 AND Longitude = 0)
  ORDER BY UnixTime;

  IF @daymonth = '' begin
    return;
  END;

  IF object_id('tempdb..#tempdays') IS NOT NULL
    DROP TABLE #tempdays;

  CREATE TABLE #tempdays (
    shead varchar(2)
  );

  DECLARE @saTail varchar(82);
  DECLARE @saHead varchar(82);
  DECLARE @indx int;
  SET @saTail = @daymonth;
  WHILE @saTail != '' begin
	 SET @indx = charindex(';', @saTail);
     SET @saHead = substring(@saTail, 1, @indx - 1);
     SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

    INSERT INTO #tempdays (shead)
      VALUES (@saHead);
  END;

  SELECT
    *
  FROM #tempperiod2 d
  WHERE DAY([d].[time]) IN (SELECT
    *
  FROM #tempdays);
  END

  
GO

--
-- Создать процедуру "dbo.SelectDataGpsPeriodAll"
--
GO
PRINT (N'Создать процедуру "dbo.SelectDataGpsPeriodAll"')
GO

CREATE PROCEDURE dbo.SelectDataGpsPeriodAll
(
  @mobile      INT,
  @beginperiod DATETIME,
  @endperiod   DATETIME,
  @timebegin   VARCHAR(9),
  @timeend     VARCHAR(9),
  @weekdays    VARCHAR(15),
  @daymonth    VARCHAR(90)
)
AS
BEGIN
  -- DECLARE @begin DATETIME;
  -- SET @begin = convert(DATETIME, '24/03/2013 00:00:00');
  -- DECLARE @end DATETIME;
  -- SET @end = convert(DATETIME, '1/04/2013 23:59:59');
  DECLARE @saTail VARCHAR(90);
  DECLARE @saHead VARCHAR(2);
  DECLARE @indx INT;
  DECLARE @time0 TIME;
  SET @time0 = '23:59:59';
  DECLARE @time1 TIME;
  SET @time1 = '00:00:00';

  IF object_id('tempdb..#tempperiod4') IS NOT NULL
    DROP TABLE #tempperiod4;

  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS time
       , (Speed * 1.852) AS speed
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
       , [Events] AS [Events]
       , LogID AS LogID
  INTO
    #tempperiod4
  FROM
    datagps
  WHERE
    Mobitel_ID = @mobile
    AND (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@beginperiod) AND dbo.UNIX_TIMESTAMPS(@endperiod))
    AND (Valid = 1)
    AND NOT (Latitude = 0
    AND Longitude = 0)
  ORDER BY
    UnixTime;

  IF object_id('tempdb..#tempweeks') IS NOT NULL
    DROP TABLE #tempweeks;

  IF @weekdays != ''
  BEGIN
    CREATE TABLE #tempweeks(
      shead VARCHAR(2)
    );

    SET @saTail = @weekdays;

    WHILE @saTail != ''
    BEGIN
      SET @indx = charindex(';', @saTail);
      SET @saHead = substring(@saTail, 1, @indx - 1);
      SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

      INSERT INTO #tempweeks(shead)
      VALUES
        (@saHead);
    END;
  END -- IF

  IF object_id('tempdb..#tempday0') IS NOT NULL
    DROP TABLE #tempday0;

  IF @daymonth != ''
  BEGIN
    CREATE TABLE #tempday0(
      shead VARCHAR(2)
    );

    SET @saTail = @daymonth;

    WHILE @saTail != ''
    BEGIN
      SET @indx = charindex(';', @saTail);
      SET @saHead = substring(@saTail, 1, @indx - 1);
      SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

      INSERT INTO #tempday0(shead)
      VALUES
        (@saHead);
    END; -- while
  END; -- IF

  -- getting select data
  IF (@daymonth != '') AND (@weekdays != '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (datepart (dw, t.time) IN (SELECT *
                                   FROM
                                     #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (datepart (dw, t.time) IN (SELECT *
                                   FROM
                                     #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;

  IF (@daymonth = '') AND (@weekdays != '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((datepart (dw, t.time) IN (SELECT *
                                    FROM
                                      #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((datepart (dw, t.time) IN (SELECT *
                                    FROM
                                      #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;

  IF (@daymonth != '') AND (@weekdays = '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;

  IF (@daymonth = '') AND (@weekdays = '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;
END

GO

--
-- Создать процедуру "dbo.online_table"
--
GO
PRINT (N'Создать процедуру "dbo.online_table"')
GO


CREATE PROCEDURE dbo.online_table
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT Log_ID
       , MobitelId
       , (SELECT TOP 1 DataGPS_ID
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS 'DataGPS_ID'
       , (SELECT TOP 1 dbo.from_unixtime(unixtime)
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS 'time'
       , (SELECT TOP 1 round((Latitude / 600000.00000), 6)
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lat
       , (SELECT TOP 1 round((Longitude / 600000.00000), 6)
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lon
       , (SELECT TOP 1 Altitude
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Altitude
       , (SELECT TOP 1 (Speed * 1.852)
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS speed
       , (SELECT TOP 1 ((Direction * 360) / 255)
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS direction
       , (SELECT TOP 1 Valid
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Valid
       , (SELECT TOP 1 (Sensor8 + (Sensor7 * 256) + 
	   (Sensor6 * 65536) + 
	   (cast(Sensor5 AS BIGINT) * 16777216) + 
	   (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + 
	   (Sensor2 * 281474976710656) + 
	   (Sensor1 * 72057594037927936))
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS sensor
       , (SELECT TOP 1 [Events]
          FROM
            [online]
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS [Events]

  FROM
    (SELECT max(LogID) AS Log_ID
          , mobitel_id AS MobitelId
     FROM
       [online]
     GROUP BY
       Mobitel_ID) T1;
END

GO

--
-- Создать процедуру "dbo.new_proc"
--
GO
PRINT (N'Создать процедуру "dbo.new_proc"')
GO


CREATE PROCEDURE dbo.new_proc
(
  @sensor     INTEGER,
  @m_id       INTEGER,
  @s_id       INTEGER,
  @begin_time DATETIME,
  @end_time   DATETIME
)
--SQL SECURITY INVOKER
--READS SQL DATA
AS
BEGIN
  SELECT round((datagps.Longitude / 600000), 6) AS lon
       , round((datagps.Latitude / 600000), 6) AS lat
       , datagps.Mobitel_ID AS Mobitel_ID
       , sensordata.value AS Value
       , (datagps.Speed * 1.852) AS speed
       , dbo.from_unixtime(datagps.UnixTime) AS [time]
       , sensordata.sensor_id AS sensor_id
       , datagps.DataGps_ID AS datagps_id
       , datagps.Sensor1 & @sensor AS sensor
  FROM
    (sensordata
    JOIN datagps
      ON ((sensordata.datagps_id = datagps.DataGps_ID)))
  WHERE
    (datagps.Valid = 1)
    AND (datagps.UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@begin_time) AND dbo.UNIX_TIMESTAMPS(@end_time))
    AND datagps.Mobitel_ID = @m_id
    AND sensordata.sensor_id = @s_id
  ORDER BY
    datagps.UnixTime
  , datagps.Mobitel_ID
  , sensordata.sensor_id;
END

GO

--
-- Создать процедуру "dbo.hystoryOnline"
--
GO
PRINT (N'Создать процедуру "dbo.hystoryOnline"')
GO


CREATE PROCEDURE dbo.hystoryOnline
(
  @id INTEGER
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT LogID
       , Mobitel_Id
       , dbo.FROM_UNIXTIME(unixtime) AS [time]
       , round((Latitude / 600000.00000), 6) AS Lat
       , round((Longitude / 600000.00000), 6) AS Lon
       , Altitude AS Altitude
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + 
	   (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + 
	   (Sensor1 * 72057594037927936)) AS sensor
       , [Events] AS [Events]
       , DataGPS_ID AS 'DataGPS_ID'
  FROM
    [online]
  WHERE
    datagps_id > @id
  ORDER BY
    datagps_id DESC;
END

GO

--
-- Создать процедуру "dbo.dataview"
--
GO
PRINT (N'Создать процедуру "dbo.dataview"')
GO


CREATE PROCEDURE dbo.dataview
(
  @m_id       INTEGER,
  @val        SMALLINT,
  @begin_time DATETIME,
  @end_time   DATETIME
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS [time]
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
       , [Events] AS [Events]
       , LogID AS LogID
  FROM
    datagps
  WHERE
    Mobitel_ID = @m_id
    AND (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@begin_time) AND dbo.UNIX_TIMESTAMPS(@end_time))
    AND (Valid = @val)
    AND NOT (Latitude = 0
    AND Longitude = 0)
  ORDER BY
    UnixTime;
END

GO

--
-- Создать процедуру "dbo.CheckOdo"
--
GO
PRINT (N'Создать процедуру "dbo.CheckOdo"')
GO


CREATE PROCEDURE dbo.CheckOdo
(
  @m_id     INTEGER,
  @end_time DATETIME
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT dbo.FROM_UNIXTIME(dbo.datagps.UnixTime) AS [time]
       , (dbo.datagps.Latitude / 600000.00000) AS Lat
       , (dbo.datagps.Longitude / 600000.00000) AS Lon
  FROM
    dbo.datagps
  WHERE
    dbo.datagps.Mobitel_ID = @m_id
    AND dbo.datagps.UnixTime BETWEEN (SELECT dbo.UNIX_TIMESTAMPS(odoTime)
                                  FROM
                                    dbo.vehicle
                                  WHERE
                                    Mobitel_id = @m_id) AND dbo.UNIX_TIMESTAMPS(@end_time)
    AND Valid = 1;
END

GO

--
-- Создать представление "dbo.tz_position"
--
GO
PRINT (N'Создать представление "dbo.tz_position"')
GO
CREATE VIEW dbo.tz_position
AS
SELECT
  vehicle.id
 ,team.Name AS [group]
 ,vehicle.NumberPlate AS tz
 ,online.Latitude * 1.000000 / 600000 AS lat
 ,online.Longitude * 1.000000 / 600000 AS lon
 ,dbo.From_UnixTime(online.UnixTime) AS point_time
 ,vehicle_category.Name
FROM dbo.id_gps_id_mobitel
INNER JOIN dbo.online
  ON id_gps_id_mobitel.expr1 = online.DataGps_ID
  AND id_gps_id_mobitel.Mobitel_ID = online.Mobitel_ID
INNER JOIN dbo.vehicle
  ON vehicle.Mobitel_id = id_gps_id_mobitel.Mobitel_ID
INNER JOIN dbo.team
  ON vehicle.Team_id = team.id
LEFT OUTER JOIN dbo.vehicle_category
  ON vehicle.Category_id = vehicle_category.Id
WHERE (vehicle.Team_id = 2
OR vehicle.Team_id = 4
OR vehicle.Team_id = 6
OR vehicle.Team_id = 8
OR vehicle.Team_id = 12
OR vehicle.Team_id = 13
OR vehicle.Team_id = 14
OR vehicle.Team_id = 17
OR vehicle.Team_id = 18
  OR vehicle.Team_id = 10
  OR vehicle.Team_id = 24)
AND online.Latitude > 0
OR (vehicle.Team_id = 2
OR vehicle.Team_id = 4
OR vehicle.Team_id = 6
OR vehicle.Team_id = 8
OR vehicle.Team_id = 12
OR vehicle.Team_id = 13
OR vehicle.Team_id = 14
OR vehicle.Team_id = 17
OR vehicle.Team_id = 18
  OR vehicle.Team_id = 10
  OR vehicle.Team_id = 24)
AND online.Longitude > 0
GO

--
-- Добавить расширенное свойство "MS_DiagramPane1" для "dbo.tz_position" (представление)
--
PRINT (N'Добавить расширенное свойство "MS_DiagramPane1" для "dbo.tz_position" (представление)')
GO
EXEC sys.sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "id_gps_id_mobitel"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 84
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "online"
            Begin Extent = 
               Top = 16
               Left = 451
               Bottom = 240
               Right = 602
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vehicle"
            Begin Extent = 
               Top = 116
               Left = 28
               Bottom = 293
               Right = 179
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "team"
            Begin Extent = 
               Top = 114
               Left = 227
               Bottom = 222
               Right = 380
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      E', 'SCHEMA', N'dbo', 'VIEW', N'tz_position'
GO

--
-- Добавить расширенное свойство "MS_DiagramPane2" для "dbo.tz_position" (представление)
--
PRINT (N'Добавить расширенное свойство "MS_DiagramPane2" для "dbo.tz_position" (представление)')
GO
EXEC sys.sp_addextendedproperty N'MS_DiagramPane2', N'nd
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'tz_position'
GO

--
-- Добавить расширенное свойство "MS_DiagramPaneCount" для "dbo.tz_position" (представление)
--
PRINT (N'Добавить расширенное свойство "MS_DiagramPaneCount" для "dbo.tz_position" (представление)')
GO
EXEC sys.sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'tz_position'
GO

--
-- Создать представление "dbo.gps_check"
--
GO
PRINT (N'Создать представление "dbo.gps_check"')
GO
CREATE VIEW dbo.gps_check 
AS SELECT vehicle.NumberPlate AS tz
        , dbo.from_unixtime(max(online.UnixTime)) AS date_time
   FROM
     dbo.vehicle
     INNER JOIN dbo.online
       ON vehicle.Mobitel_id = online.Mobitel_ID
   WHERE
     Team_id = 13
   GROUP BY
     vehicle.NumberPlate
   , vehicle.Mobitel_id
   , online.Mobitel_ID
GO

--
-- Создать представление "dbo.dataview1"
--
GO
PRINT (N'Создать представление "dbo.dataview1"')
GO

CREATE VIEW dbo.dataview1 
AS SELECT dbo.datagps.DataGps_ID AS DataGps_ID
        , dbo.datagps.Mobitel_ID AS Mobitel_ID
        , (dbo.datagps.Latitude / 600000.00000) AS Lat
        , (dbo.datagps.Longitude / 600000.00000) AS Lon
        , dbo.datagps.Altitude AS Altitude
        , dbo.FROM_UNIXTIME(datagps.UnixTime) AS [time]
        , (dbo.datagps.Speed * 1.852) AS speed
        , ((dbo.datagps.Direction * 360) / 255) AS direction
        , dbo.datagps.Valid AS Valid
        , (dbo.datagps.Sensor8 + (dbo.datagps.Sensor7 * 256) + (dbo.datagps.Sensor6 * 65536) + (cast(dbo.datagps.Sensor5 AS BIGINT) * 16777216) + (dbo.datagps.Sensor4 * 4294967296) + (dbo.datagps.Sensor3 * 1099511627776) + (dbo.datagps.Sensor2 * 281474976710656) + (dbo.datagps.Sensor1 * 72057594037927936)) AS sensor
        , dbo.datagps.[Events] AS [Events]
        , dbo.datagps.LogID AS LogID
   FROM
     dbo.datagps;

GO

--
-- Создать представление "dbo.datavalue1"
--
GO
PRINT (N'Создать представление "dbo.datavalue1"')
GO

CREATE VIEW dbo.datavalue1
AS SELECT round((dbo.datagps.Longitude / 600000), 6) AS lon
        , round((dbo.datagps.Latitude / 600000), 6) AS lat
        , dbo.datagps.Mobitel_ID AS Mobitel_ID
        , dbo.sensordata.value AS Value
        , (dbo.datagps.Speed * 1.852) AS speed
        , dbo.from_unixtime(dbo.datagps.UnixTime) AS [time]
        , dbo.sensordata.sensor_id AS sensor_id
        , dbo.datagps.DataGps_ID AS datavalue_id
   FROM
     (dbo.sensordata
     JOIN dbo.datagps
       ON ((dbo.sensordata.datagps_id = dbo.datagps.DataGps_ID)))
   WHERE
     (dbo.datagps.Valid = 1);

GO

--
-- Создать представление "dbo.ua_misceznah"
--
GO
PRINT (N'Создать представление "dbo.ua_misceznah"')
GO
CREATE VIEW dbo.ua_misceznah 
AS SELECT team.Name AS team
        , vehicle_category.Name AS blok
        , vehicle.NumberPlate AS tz
        , left(mobitels.Name, 7) AS mobitel_id
       
   FROM
 vehicle
     INNER JOIN mobitels
       ON mobitels.Mobitel_ID = vehicle.Mobitel_id
     INNER JOIN team
       ON team.id = vehicle.Team_id
     INNER JOIN vehicle_category
       ON vehicle_category.Id = vehicle.Category_id
   WHERE
      (vehicle.Team_id = 5
     OR vehicle.Team_id = 2
     OR vehicle.Team_id = 3
     OR vehicle.Team_id = 4
     OR vehicle.Team_id = 6
     OR vehicle.Team_id = 8
     OR vehicle.Team_id = 9
     OR vehicle.Team_id = 12
     OR vehicle.Team_id = 13
     OR vehicle.Team_id = 14
  OR vehicle.Team_id = 15
  OR vehicle.Team_id = 16
  OR vehicle.Team_id = 17
  OR vehicle.Team_id = 18)
   GROUP BY
     team.Name
   , vehicle_category.Name
   , vehicle.NumberPlate
   , mobitels.Name
GO

--
-- Создать представление "dbo.ua_agregati"
--
GO
PRINT (N'Создать представление "dbo.ua_agregati"')
GO
CREATE VIEW dbo.ua_agregati 
AS SELECT agro_agregat.Comment AS Blok
        , agro_agregat1.Name AS Vid_agregat
        , agro_agregat.Name AS agregat
        , agro_work.Name AS work
   FROM
     agro_agregat
     INNER JOIN agro_work
       ON agro_work.Id = agro_agregat.Id_work
     INNER JOIN agro_agregat agro_agregat1
       ON agro_agregat1.Id = agro_agregat.Id_main
   WHERE
     agro_agregat.Comment <> ''
GO

--
-- Создать представление "dbo.square_for_gis_2017"
--
GO
PRINT (N'Создать представление "dbo.square_for_gis_2017"')
GO
CREATE VIEW dbo.square_for_gis_2017 
AS SELECT zones.Zone_ID AS id
        , left(zonesgroup.Title, len(zonesgroup.Title) - 5) AS klaster
        , left(zones.Name, len(zones.Name) - 5) AS name
        , zone_category2.Name AS kult
        , cast(zones.Square * 100 AS DEC(12, 1)) AS square
        , zones.Descr
   FROM
     dbo.zones
     INNER JOIN dbo.zonesgroup
       ON zones.ZonesGroupId = zonesgroup.Id
     INNER JOIN dbo.zone_category2
       ON zone_category2.Id = zones.Category_id2
   WHERE
     (zones.ZonesGroupId = 176
     OR zones.ZonesGroupId = 183
     OR zones.ZonesGroupId = 184
     OR zones.ZonesGroupId = 185
     OR zones.ZonesGroupId = 186
     OR zones.ZonesGroupId = 187
     OR zones.ZonesGroupId = 188)
     AND (zones.Name LIKE '%_2017'
     AND zones.Name NOT LIKE '%_SOC%')
GO

--
-- Создать представление "dbo.square_for_gis_2016"
--
GO
PRINT (N'Создать представление "dbo.square_for_gis_2016"')
GO
CREATE VIEW dbo.square_for_gis_2016 
AS SELECT zones.Zone_ID AS id
        ,left(zonesgroup.Title, len(zonesgroup.Title)-5) AS klaster
        , left(zones.Name, len(zones.Name) - 5) AS name
        , zone_category2.Name AS kult
        , cast(zones.Square * 100 AS DEC(12, 1)) AS square
        , zones.Descr
   FROM
     dbo.zones
     INNER JOIN dbo.zonesgroup
       ON zones.ZonesGroupId = zonesgroup.Id
     INNER JOIN dbo.zone_category2
       ON zone_category2.Id = zones.Category_id2
   WHERE
     (zones.ZonesGroupId = 73
     OR zones.ZonesGroupId = 23
     OR zones.ZonesGroupId = 65
     OR zones.ZonesGroupId = 141
     OR zones.ZonesGroupId = 14
     OR zones.ZonesGroupId = 130
     OR zones.ZonesGroupId = 43)
     AND zones.Name LIKE '%_2016'
     AND zones.Name NOT LIKE '%_SOC%'
GO

--
-- Создать представление "dbo.points_for_gis_2017"
--
GO
PRINT (N'Создать представление "dbo.points_for_gis_2017"')
GO
CREATE VIEW dbo.points_for_gis_2017 
AS SELECT zones.Zone_ID AS id
        , left(zones.Name, len(zones.Name) - 5) AS Name
        , points.Point_ID
        , (points.Latitude * 1.000000) / 600000 AS Lat
        , (points.Longitude * 1.000000) / 600000 AS Lon
   FROM
     dbo.zone_category
     INNER JOIN dbo.zones
       ON zone_category.Id = zones.Category_id
     INNER JOIN dbo.points
       ON points.Zone_ID = zones.Zone_ID
   WHERE
     (zones.ZonesGroupId = 176
     OR zones.ZonesGroupId = 183
     OR zones.ZonesGroupId = 184
     OR zones.ZonesGroupId = 185
     OR zones.ZonesGroupId = 186
     OR zones.ZonesGroupId = 187
     OR zones.ZonesGroupId = 188)
     AND (zones.Name LIKE '%_2017'
     AND zones.Name NOT LIKE '%_SOC%')

GO

--
-- Создать представление "dbo.points_for_gis_2016"
--
GO
PRINT (N'Создать представление "dbo.points_for_gis_2016"')
GO
CREATE VIEW dbo.points_for_gis_2016 
AS SELECT zones.Zone_ID AS id
        , left(zones.Name, len(zones.Name) - 5) AS Name
        , points.Point_ID
        , (points.Latitude * 1.000000) / 600000 AS Lat
        , (points.Longitude * 1.000000) / 600000 AS Lon
   FROM
     dbo.zone_category
     INNER JOIN dbo.zones
       ON zone_category.Id = zones.Category_id
     INNER JOIN dbo.points
       ON points.Zone_ID = zones.Zone_ID
   WHERE
     (zones.ZonesGroupId = 73
     OR zones.ZonesGroupId = 23
     OR zones.ZonesGroupId = 65
     OR zones.ZonesGroupId = 141
     OR zones.ZonesGroupId = 14
     OR zones.ZonesGroupId = 130
     OR zones.ZonesGroupId = 43)
     AND zones.Name LIKE '%_2016'
     AND zones.Name NOT LIKE '%_SOC%'

GO

--
-- Создать представление "dbo.for_1C"
--
GO
PRINT (N'Создать представление "dbo.for_1C"')
GO
CREATE VIEW dbo.for_1C
AS SELECT zones.Zone_ID AS id
        , left(zonesgroup.Title, len(zonesgroup.Title) - 5) AS klaster
        , left(zones.Name, len(zones.Name) - 5) AS posivna
        , left(zones.Name, 10) AS pole
        , RIGHT(zones.Name, 4) AS rik
        , zone_category2.Name AS kult
        , cast(zones.Square * 100 AS DEC(12, 1)) AS square
   FROM
     dbo.zones
     INNER JOIN dbo.zonesgroup
       ON zones.ZonesGroupId = zonesgroup.Id
     INNER JOIN dbo.zone_category2
       ON zone_category2.Id = zones.Category_id2
   WHERE
     (zones.ZonesGroupId = 176
     OR zones.ZonesGroupId = 183
     OR zones.ZonesGroupId = 184
     OR zones.ZonesGroupId = 185
     OR zones.ZonesGroupId = 186
     OR zones.ZonesGroupId = 187
     OR zones.ZonesGroupId = 188
     OR zones.ZonesGroupId = 73
     OR zones.ZonesGroupId = 23
     OR zones.ZonesGroupId = 65
     OR zones.ZonesGroupId = 141
     OR zones.ZonesGroupId = 14
     OR zones.ZonesGroupId = 130
     OR zones.ZonesGroupId = 43)
     AND zones.Name NOT LIKE '%_SOC%'
GO

--
-- Создать процедуру "dbo.zvit"
--
GO
PRINT (N'Создать процедуру "dbo.zvit"')
GO
CREATE PROCEDURE dbo.zvit (@z datetime, @po datetime, @blok char(50)) 
AS
	SELECT CAST(Date AS Date), SUBSTRING(Title, 1, CHARINDEX('_', Title, 1)-1), work, NumberPlate, agreg, zony, LockRecord, plos
FROM (SELECT TOP 10000 a_ord.Date, zgr.Title, a_wk.Name work, veh.NumberPlate, a_agr.Name agreg, zones.Name zony, a_ordt.LockRecord, Round(Sum(a_ordt.FactSquareCalc), 2) plos 
FROM agro_ordert a_ordt 
LEFT OUTER JOIN agro_order a_ord ON a_ordt.Id_main = a_ord.Id  
LEFT OUTER JOIN agro_agregat a_agr ON a_ordt.Id_agregat=a_agr.Id  
LEFT OUTER JOIN agro_work a_wk ON a_ordt.Id_work=a_wk.Id  
LEFT OUTER JOIN vehicle veh ON a_ord.Id_mobitel=veh.Mobitel_id  
LEFT OUTER JOIN team_new team ON veh.Team_id = team.id  
LEFT OUTER JOIN zones ON a_ordt.Id_zone=zones.Zone_ID  
LEFT OUTER JOIN zonesgroup zgr ON zones.ZonesGroupId=zgr.Id  
WHERE a_ord.Date between @z And @po  AND zgr.Title LIKE '%[_]%' AND (team.groupTeam=1) AND (a_ordt.Confirm=1)  
GROUP BY zgr.Title, a_wk.Name, a_ord.Date, veh.NumberPlate, a_agr.Name, zones.Name, a_agr.Width, a_ordt.LockRecord  
ORDER BY zgr.Title, a_wk.Name, veh.NumberPlate, a_agr.Name) t1  
WHERE SUBSTRING(Title, 1, CHARINDEX('_', Title, 1)-1) = @blok
GO

--
-- Создать процедуру "dbo.TransferLiteBuffer"
--
GO
PRINT (N'Создать процедуру "dbo.TransferLiteBuffer"')
GO

CREATE PROCEDURE dbo.TransferLiteBuffer
--COMMENT 'Переносит GPS данные из буферной таблицы в datagps.'
AS
BEGIN
  IF object_id('tempdb..#temp2') IS NOT NULL
    DROP TABLE #temp2;

  CREATE TABLE #temp2(
    DataGps_ID INT
  );
  INSERT INTO #temp2
  SELECT DataGPS_ID
  FROM
    datagpsbufferlite;

  UPDATE datagps
  SET
    Message_ID = b.Message_ID, Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = NULL, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, isShow = b.IsShow, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagps d, datagpsbuffer b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.Latitude = b.Latitude AND
    d.Longitude = b.Longitude AND
    d.Altitude = b.Altitude AND
    d.UnixTime = b.UnixTime;

  DELETE b
  FROM
    datagps d, datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.Latitude = b.Latitude AND
    d.Longitude = b.Longitude AND
    d.Altitude = b.Altitude AND
    d.UnixTime = b.UnixTime;

  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.[Events]
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs
  FROM
    datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID
  GROUP BY
    b.Mobitel_ID
  , b.LogID
  , b.Latitude
  , b.Longitude
  , b.Altitude
  , b.UnixTime;

  DELETE b
  FROM
    datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID;

  DROP TABLE #temp2
END

GO

--
-- Создать процедуру "dbo.TransferBuffer"
--
GO
PRINT (N'Создать процедуру "dbo.TransferBuffer"')
GO


CREATE PROCEDURE dbo.TransferBuffer
--SQL SECURITY INVOKER
--COMMENT 'Переносит GPS данные из буферной таблицы в datagps.'
AS
BEGIN

  --CREATE TEMPORARY TABLE IF NOT EXISTS temp1 ENGINE = HEAP

  IF object_id('tempdb..#temp1') IS NOT NULL
    DROP TABLE #temp1;

  CREATE TABLE #temp1(
    DataGps_ID INT
  );
  INSERT INTO #temp1
  SELECT DataGPS_ID
  FROM
    datagpsbuffer;

  UPDATE datagps
  SET
    Message_ID = 0, Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = NULL, [Events] = b.[Events], Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, isShow = 0, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagps d, datagpsbuffer b, #temp1 t
  WHERE
    (b.DataGps_ID = t.DataGps_ID) AND
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  DELETE b
  FROM
    datagps d, datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.LogID = b.LogID;

  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , [Events]
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.[Events]
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID
  GROUP BY
    b.Mobitel_ID
  , b.LogID;


  DELETE b
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID;

  DROP TABLE #temp1
END

GO

--
-- Создать процедуру "dbo.tmpAddIndex"
--
GO
PRINT (N'Создать процедуру "dbo.tmpAddIndex"')
GO


CREATE PROCEDURE dbo.tmpAddIndex
AS
--COMMENT 'Временная процедура добавления индекса в DataGpsBuffer_on'
BEGIN
  DECLARE @DbName VARCHAR(64); /* Наименование текущей БД */
  DECLARE @IndexExist TINYINT;
  SET @IndexExist = 0;

  SET @DbName = (SELECT db_name(@@procid));

  SET @IndexExist = (SELECT count(1)
                     FROM
                       sys.indexes i
                     WHERE
                       i.name = 'IDX_SrvPacketID');

  IF @IndexExist = 0
  BEGIN
    CREATE INDEX IDX_SrvPacketID ON DataGpsBuffer_on (SrvPacketID);
    ALTER TABLE DataGpsBuffer_on ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON);
  END;
END

--EXEC tmpAddIndex
--DROP PROCEDURE IF EXISTS tmpAddIndex$$

GO

--
-- Создать процедуру "dbo.sp_sela"
--
GO
PRINT (N'Создать процедуру "dbo.sp_sela"')
GO
CREATE PROCEDURE dbo.sp_sela 
@selo varchar(50)
as 
begin
select tab1.id, tab1.name, tab2.name, tab3.name from (select id, name, left(id,2)+'00000000' as obl, left(id,5)+'00000' as rn 
from docs_agro.dbo.d_koatuu  where name like @selo  AND left(id,2) in ('05', '07', '18', '26', '46', '56', '61', '68', '73')) tab1
left join d_koatuu tab2 on tab1.obl = tab2.id
left join d_koatuu tab3 on tab1.rn = tab3.id
end
GO

--
-- Создать процедуру "dbo.PopTransferBuffer"
--
GO
PRINT (N'Создать процедуру "dbo.PopTransferBuffer"')
GO

CREATE PROCEDURE dbo.PopTransferBuffer
  --COMMENT 'Переносит GPS данные из буферной таблицы в datagps'
  AS
BEGIN
  DECLARE @MobitelIDInCursor INT; 
  --DECLARE @DataExists TINYINT;
  --SET @DataExists = 1;
  DECLARE @TmpMaxUnixTime INT; 

  DECLARE CursorMobitels CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_pop;

  
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  DELETE o
  FROM online o, datagpsbuffer_pop dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);
  
  --SET DataExists = 1;
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@FETCH_STATUS = 0 begin
    
    SET @TmpMaxUnixTime = (SELECT COALESCE(MAX(UnixTime), 0)
    FROM [online] WITH( INDEX (IDX_Mobitelid))
    WHERE Mobitel_ID = @MobitelIDInCursor);

    
    INSERT INTO [online] (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, [Events],
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, [Events],
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs
    FROM (
      SELECT TOP 100 *
      FROM datagpsbuffer_pop
      WHERE (Mobitel_ID = @MobitelIDInCursor)  AND 
        (UnixTime > @TmpMaxUnixTime) AND (Valid = 1)
      --GROUP BY Mobitel_ID, LogID
      ORDER BY UnixTime DESC) AS T
    ORDER BY UnixTime ASC;

    DELETE FROM [online]
    WHERE (Mobitel_ID = @MobitelIDInCursor) AND (UnixTime < 
      (SELECT COALESCE(MIN(UnixTime), 0) 
       FROM (
         SELECT TOP 100 UnixTime
         FROM online
         WHERE Mobitel_ID = @MobitelIDInCursor 
         ORDER BY UnixTime DESC) T1)); 
  
    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END;
  CLOSE CursorMobitels;
    
  UPDATE datagps SET
    Latitude = b.Latitude,
    Longitude = b.Longitude,
    Altitude = b.Altitude,
    UnixTime = b.UnixTime,
    Speed = b.Speed,
    Direction = b.Direction,
    Valid = b.Valid,
    InMobitelID = b.LogID,
    [Events] = b.[Events],
    Sensor1 = b.Sensor1,
    Sensor2 = b.Sensor2,
    Sensor3 = b.Sensor3,
    Sensor4 = b.Sensor4,
    Sensor5 = b.Sensor5,
    Sensor6 = b.Sensor6,
    Sensor7 = b.Sensor7,
    Sensor8 = b.Sensor8,
    whatIs = b.whatIs,
    Counter1 = b.Counter1,
    Counter2 = b.Counter2,
    Counter3 = b.Counter3,
    Counter4 = b.Counter4
	FROM datagps d, datagpsbuffer_pop b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  
  DELETE b
  FROM datagps d, datagpsbuffer_pop b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  
  INSERT INTO datagps (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, [Events], InMobitelID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs)
  SELECT
    p.Mobitel_ID, p.LogID, p.UnixTime, p.Latitude, p.Longitude, p.Altitude,
    p.Direction, p.Speed, p.Valid, [p].[Events], p.LogID,
    p.Sensor1, p.Sensor2, p.Sensor3, p.Sensor4, p.Sensor5, p.Sensor6, p.Sensor7, p.Sensor8,
    p.Counter1, p.Counter2, p.Counter3, p.Counter4, p.whatIs
  FROM datagps d, datagpsbuffer_pop p;
  --GROUP BY d.Mobitel_ID, d.LogID;

  
  TRUNCATE TABLE datagpsbuffer_pop;
END

GO

--
-- Создать процедуру "dbo.PopListMobitelConfig"
--
GO
PRINT (N'Создать процедуру "dbo.PopListMobitelConfig"')
GO

CREATE PROCEDURE dbo.PopListMobitelConfig
  -- COMMENT 'Выборка настроек телетреков для работы в режиме POP3'
  AS
BEGIN
  SELECT m.Mobitel_ID AS MobitelID, c.devIdShort AS DevIdShort,
    COALESCE(
      (SELECT TOP 1 s.EmailMobitel 
       FROM Mobitels m2 JOIN ServiceSend s ON s.ID = m2.ServiceSend_ID 
       WHERE m2.Mobitel_ID = m.Mobitel_ID
       ORDER BY s.ServiceSend_ID DESC), '') AS Email
  FROM mobitels m JOIN internalmobitelconfig c ON c.ID = m.InternalMobitelConfig_ID
  WHERE (c.InternalMobitelConfig_ID = (
    SELECT TOP 1 intConf.InternalMobitelConfig_ID
    FROM internalmobitelconfig intConf
    WHERE (intConf.ID = c.ID)
    ORDER BY intConf.InternalMobitelConfig_ID DESC)) AND (c.devIdShort IS NOT NULL) AND (c.devIdShort <> '')
  ORDER BY m.Mobitel_ID;
END

GO

--
-- Создать процедуру "dbo.OnListMobitelConfig64"
--
GO
PRINT (N'Создать процедуру "dbo.OnListMobitelConfig64"')
GO


CREATE PROCEDURE dbo.OnListMobitelConfig64
  -- COMMENT 'Выборка настроек телетреков которые могут работать в online режиме для datagps и datagps64'
  AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  DECLARE @Is64Cursor tinyint;
  DECLARE @DevIdShortInCursor CHAR(4);
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /* Курсор в множестве настроенных телетреков */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS MobitelID, m.Is64bitPackets AS Is64, c.devIdShort AS DevIdShort
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT TOP 1 intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID)
      ORDER BY intConf.InternalMobitelConfig_ID DESC)) AND
      (c.devIdShort IS NOT NULL) AND (c.devIdShort <> '')
    ORDER BY m.Mobitel_ID;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* Временная таблица настроек телетрека */
  CREATE TABLE dbo.tmpMobitelsConfig64 (
    MobitelID INT NOT NULL,
    Is64 tinyint NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL,
    LastPacketID64 BIGINT NOT NULL);

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor, @Is64Cursor, @DevIdShortInCursor;
  WHILE  @@fetch_status = 0 begin
    INSERT INTO dbo.tmpMobitelsConfig64 SELECT @MobitelIDInCursor, @Is64Cursor, @DevIdShortInCursor,
      (SELECT COALESCE(MAX(d.SrvPacketID), 0)
       FROM datagps d
       WHERE d.Mobitel_ID = @MobitelIDInCursor) AS LastPacketID,
      (SELECT COALESCE(MAX(t.SrvPacketID), 0)
       FROM datagps64 t
       WHERE t.Mobitel_ID = @MobitelIDInCursor) AS LastPacketID64;

    FETCH CursorMobitels INTO @MobitelIDInCursor, @Is64Cursor, @DevIdShortInCursor;
  END;
  CLOSE CursorMobitels;

  SELECT MobitelID, Is64, DevIdShort, LastPacketID, LastPacketID64
  FROM dbo.tmpMobitelsConfig64;

  DROP TABLE dbo.tmpMobitelsConfig64;
END

GO

--
-- Создать процедуру "dbo.OnListMobitelConfig"
--
GO
PRINT (N'Создать процедуру "dbo.OnListMobitelConfig"')
GO

CREATE PROCEDURE dbo.OnListMobitelConfig
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  DECLARE @DevIdShortInCursor CHAR(4);
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /*  */
  DECLARE CursorMobitels CURSOR FOR
  SELECT m.Mobitel_ID AS MobitelID
       , c.devIdShort AS DevIdShort
  FROM
    mobitels m
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort IS NOT NULL)
    AND (c.devIdShort <> '')
  ORDER BY
    m.Mobitel_ID;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /*  */
  IF EXISTS(SELECT * FROM sys.tables WHERE name = 'tmpMobitelsConfig')
    DROP TABLE dbo.tmpMobitelsConfig;

CREATE TABLE dbo.tmpMobitelsConfig(
  MobitelID INT NOT NULL,
  DevIdShort CHAR(4) NOT NULL,
  LastPacketID BIGINT NOT NULL
);

OPEN CursorMobitels;
FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
WHILE @@fetch_status = 0
BEGIN
  INSERT INTO tmpMobitelsConfig
  SELECT @MobitelIDInCursor
       , @DevIdShortInCursor
       , (SELECT coalesce(max(SrvPacketID), 0)
          FROM
            datagps
          WHERE
            Mobitel_ID = @MobitelIDInCursor) AS LastPacketID;

  FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
END;
CLOSE CursorMobitels;
--DEALLOCATE CursorMobitels;

SELECT MobitelID
     , DevIdShort
     , LastPacketID
FROM
  tmpMobitelsConfig;

DROP TABLE dbo.tmpMobitelsConfig;
END


GO

--
-- Создать процедуру "dbo.OnFullRefreshLostRanges"
--
GO
PRINT (N'Создать процедуру "dbo.OnFullRefreshLostRanges"')
GO


CREATE PROCEDURE dbo.OnFullRefreshLostRanges
AS
--NOT DETERMINISTIC
--CONTAINS SQL
--SQL SECURITY INVOKER
--COMMENT 'Поиск всех пропусков для TDataManager'
BEGIN
  DECLARE @MobitelIDInCursor INT; /* Текущее значение MobitelID */
  --DECLARE @DataExists TINYINT; /* Флаг наличия данных после фетча */
  --SET @DataExists = 1;

  DECLARE @LogIdInCursor INT; /* Текущее значение LogId телетрека */
  DECLARE @SrvPacketIdInCursor BIGINT; /* Текущее значение SrvPacketID телетрека */
  DECLARE @PreviousLogId INT; /* Значение LogId телетрека на предыдущем шаге*/
  DECLARE @PreviousSrvPacketId BIGINT; /* Значение SrvPacketID телетрека на предыдущем шаге */

  DECLARE @FirstIteration TINYINT; /* Флаг первой итерации по данным */
  SET @FirstIteration = 1;
  DECLARE @NewRowCount INT; /* Количество новых записей с разрывами */
  SET @NewRowCount = 0;
  DECLARE @NewConfirmedID INT; /* Новое значение ConfirmedID */

  DECLARE @MaxNegativeDataGpsId INT; /* максимальный DataGpsId в множестве отрицательных LogId */
  DECLARE @MaxPositiveDataGpsId INT; /* максимальный DataGpsId в множестве положительных LogId */
  DECLARE @InsertLostDataStatement NVARCHAR;

  /* Курсор по всем телетрекам */
  DECLARE CursorMobitels CURSOR FOR
  SELECT DISTINCT (Mobitel_ID) AS Mobitel_ID
  FROM
    mobitels
  ORDER BY
    Mobitel_ID;

  /* Курсор по данным телетрека */
  DECLARE CursorDataGps CURSOR FOR
  SELECT LogId
       , SrvPacketID
  FROM
    DataGps
  WHERE
    Mobitel_ID = @MobitelIDInCursor
  ORDER BY
    LogId;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* Очистка таблиц, связанных учетом пропусков для TDataManager  */
  TRUNCATE TABLE datagpsbuffer_on;
  TRUNCATE TABLE datagpslost_on;
  TRUNCATE TABLE datagpslost_ontmp;

  /* ------------------- FETCH Mobitels ------------------------ */
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* Заголовок для запроса вставки  */
    SET @InsertLostDataStatement = 'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';

    /* ----------------- FETCH DataGPS --------------------- */
    SET @FirstIteration = 1;
    SET @NewRowCount = 0;

    OPEN CursorDataGps;
    FETCH CursorDataGps INTO @LogIdInCursor, @SrvPacketIdInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstIteration = 1
      BEGIN
        /* Первое значение в списке */
        SET @PreviousLogId = @LogIdInCursor;
        SET @PreviousSrvPacketId = @SrvPacketIdInCursor;
        SET @FirstIteration = 0;
      END
      ELSE /* Основная ветка расчета*/
      BEGIN
        IF (((@LogIdInCursor - @PreviousLogId) > 1) AND (@PreviousSrvPacketId < @SrvPacketIdInCursor))
        BEGIN
          /* Найден разрыв */
          IF @NewRowCount > 0
          BEGIN
            SET @InsertLostDataStatement = @InsertLostDataStatement + ',';
          END; --END IF;

          SET @InsertLostDataStatement = @InsertLostDataStatement + ' (' + @MobitelIDInCursor + ', ' + @PreviousLogId + ', ' + @LogIdInCursor + ', ' + @PreviousSrvPacketId + ', ' + @SrvPacketIdInCursor + ')';

          SET @NewRowCount = @NewRowCount + 1;
        END; --END IF;

        /* Переместим текущие значения в предыдущие */
        SET @PreviousLogId = @LogIdInCursor;
        SET @PreviousSrvPacketId = @SrvPacketIdInCursor;
      END; --END IF; -- IF @FirstIteration = 1 

      FETCH CursorDataGps INTO @LogIdInCursor, @SrvPacketIdInCursor;
    END; --END WHILE; --  WHILE DataExistsInner = 1 
    CLOSE CursorDataGps;

    /* Если у данного телетрека есть пропущенные записи - 
       тогда ConfirmedID будет равен минимальному Begin_LogID 
       из таблицы пропусков. 
       Иначе - максимальному LogID данного телетрека из 
       таблицы datagps в одном из диапазонов больше нуля или 
       меньше нуля, в зависимости от текущих LogId. */
    IF @NewRowCount > 0
    BEGIN
      /* Заполнение таблицы пропущенных данных */
      EXEC (@InsertLostDataStatement);

      SET @NewConfirmedID = (SELECT min(Begin_LogID)
                             FROM
                               datagpslost_on
                             WHERE
                               Mobitel_ID = @MobitelIDInCursor);
    END
    ELSE
    BEGIN
      /* В этом случае не все так радужно.
         Если есть отрицательные значения LogID, то для определения
         нового значения ConfirmedID необходим дополнительный анализ:
         оцениваем какие значения LogID положительные или отрицательные
         более новые (по DataGpsId). */
      IF EXISTS (SELECT TOP 1 1
                 FROM
                   dbo.datagps
                 WHERE
                   (Mobitel_ID = @MobitelIDInCursor)
                   AND (LogId < 0))
      BEGIN
        SET @MaxNegativeDataGpsId = (SELECT max(DataGps_ID)
                                     FROM
                                       DataGps
                                     WHERE
                                       (Mobitel_ID = @MobitelIDInCursor)
                                       AND (LogId < 0));

        SET @MaxPositiveDataGpsId = (SELECT coalesce(max(DataGps_ID), 0)
                                     FROM
                                       DataGps
                                     WHERE
                                       (Mobitel_ID = @MobitelIDInCursor)
                                       AND (LogId >= 0));

        IF (@MaxNegativeDataGpsId > @MaxPositiveDataGpsId)
        BEGIN
          SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                                 FROM
                                   datagps
                                 WHERE
                                   (Mobitel_ID = @MobitelIDInCursor)
                                   AND (LogId < 0));
        END
        ELSE
        BEGIN
          SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                                 FROM
                                   datagps
                                 WHERE
                                   (Mobitel_ID = @MobitelIDInCursor)
                                   AND (LogId >= 0));
        END --END IF; -- IF (@MaxNegativeDataGpsId > @MaxPositiveDataGpsId)
      END
      ELSE
      BEGIN
        /* существуют только положительные LogId */
        SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                               FROM
                                 datagps
                               WHERE
                                 Mobitel_ID = @MobitelIDInCursor);
      END; --END IF; -- IF EXISTS 
    END; --END IF; -- IF @NewRowCount > 0

    /* Обновим ConfirmedID */
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    --SET DataExists = 1;
    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END; --END WHILE;
  CLOSE CursorMobitels;
END;

GO

--
-- Создать процедуру "dbo.OnDeleteLostRange"
--
GO
PRINT (N'Создать процедуру "dbo.OnDeleteLostRange"')
GO

CREATE PROCEDURE dbo.OnDeleteLostRange(  @MobitelID int, @BeginSrvPacketID BIGINT)
--COMMENT 'Удаление указанного диапазона пропусков из datagpslost_on.'
AS
BEGIN

  DECLARE @MinBeginSrvPacketID BIGINT; -- 
  DECLARE @NewConfirmedID INT; -- 

  SELECT @MinBeginSrvPacketID = min(Begin_SrvPacketID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @MinBeginSrvPacketID IS NOT NULL
  BEGIN
    IF @MinBeginSrvPacketID = @BeginSrvPacketID
    BEGIN
      SELECT @NewConfirmedID = End_LogID
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (Begin_SrvPacketID = @BeginSrvPacketID);
    END; -- IF

    DELETE
    FROM
      datagpslost_on
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (Begin_SrvPacketID = @BeginSrvPacketID);

    IF @MinBeginSrvPacketID = @BeginSrvPacketID
    BEGIN
      UPDATE mobitels
      SET
        ConfirmedID = @NewConfirmedID
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (@NewConfirmedID > ConfirmedID);
    END; -- IF
  END; -- IF  
END

GO

--
-- Создать процедуру "dbo.OnDeleteExistingRecords64"
--
GO
PRINT (N'Создать процедуру "dbo.OnDeleteExistingRecords64"')
GO

CREATE PROCEDURE dbo.OnDeleteExistingRecords64
  --COMMENT 'Удаление из datagpsbuffer_on64 записей, уже присутсвующих в data'
  as
BEGIN

  /* Текущий MobitelId в курсоре CursorExistingRecords */
  DECLARE @MobitelIdInCursor int;
  SET @MobitelIdInCursor = 0;
  /* Текущий SrvPacketId в курсоре CursorExistingRecords */
  DECLARE @SrvPacketIdInCursor int;
  SET @SrvPacketIdInCursor = 0;

  DECLARE @DataExists tinyint;
  SET @DataExists = 1;

  /* Курсор для прохода по всем записям */
  DECLARE CursorExistingRecords CURSOR FOR
  SELECT
    datagpsbuffer_on64.Mobitel_ID,
    datagpsbuffer_on64.SrvPacketID
  FROM datagpsbuffer_on64
    INNER JOIN datagps64
      ON datagpsbuffer_on64.Mobitel_ID = datagps64.Mobitel_ID
      AND datagpsbuffer_on64.SrvPacketID = datagps64.SrvPacketID
  GROUP BY datagpsbuffer_on64.Mobitel_ID,
           datagpsbuffer_on64.SrvPacketID;

 -- DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET @DataExists = 1;
  OPEN CursorExistingRecords;
  FETCH CursorExistingRecords INTO @MobitelIdInCursor, @SrvPacketIdInCursor;
  WHILE @@fetch_status = 0 BEGIN
    DELETE
      FROM datagpsbuffer_on64
    WHERE (Mobitel_ID = @MobitelIdInCursor)
      AND (SrvPacketID = @SrvPacketIdInCursor);
    FETCH CursorExistingRecords INTO @MobitelIdInCursor, @SrvPacketIdInCursor;
  END;
  CLOSE CursorExistingRecords;
END

GO

--
-- Создать процедуру "dbo.OnCorrectInfotrackLogId64"
--
GO
PRINT (N'Создать процедуру "dbo.OnCorrectInfotrackLogId64"')
GO

CREATE PROCEDURE dbo.OnCorrectInfotrackLogId64
AS
--SQL SECURITY INVOKER
--COMMENT 'Корректировка значений LogId и InMobitelID в данных инфотреков'
BEGIN
  DECLARE @MobitelIDInCursor INT; /* Текущее значение MobitelID  */
  DECLARE @DataExists TINYINT; /* Флаг наличия данных после фетча */
  SET @DataExists = 1;
  DECLARE @MaxLogId INT; /* Максимальный LogId для данного дивайса*/
  SET @MaxLogId = 0;
  DECLARE @SrvPacketIDInCursor BIGINT; /* Текущее значение серверного пакета  */
  SET @SrvPacketIDInCursor = 0;

  /* Курсор по инфотрекам в множестве новых данных. 
     Все инфотреки начинаются с буквы I  */
  DECLARE CursorInfoTrack CURSOR LOCAL FOR
  SELECT DISTINCT buf.Mobitel_ID
  FROM
    datagpsbuffer_on64 buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort LIKE 'I%')
    AND (buf.LogId = 0)
  ORDER BY
    1;

  DECLARE CursorInfoTrackUpdateSet CURSOR LOCAL FOR
  SELECT SrvPacketID
  FROM
    datagpsbuffer_on64
  WHERE
    (Mobitel_ID = @MobitelIDInCursor)
    AND (LogId = 0)
  ORDER BY
    UnixTime;

  /*  Курсор по данным телетрека */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* Находим максимальный LogId для данного инфотрека */
    SET @MaxLogId = (SELECT coalesce(max(LogID), 0)
                     FROM
                       DataGps
                     WHERE
                       Mobitel_ID = @MobitelIDInCursor);

    /* Обновляем LogId и InMobitelID в наборе новых данных текущего инфотрека */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      SET @MaxLogId = @MaxLogId + 1;

      UPDATE datagpsbuffer_on64
      SET
        LogId = @MaxLogId, Mobitel_ID = @MaxLogId
      WHERE
        SrvPacketID = @SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    END;
    CLOSE CursorInfoTrackUpdateSet;
    --DEALLOCATE CursorInfoTrackUpdateSet;

    --SET DataExists = 1;
    FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  END;
  CLOSE CursorInfoTrack;
--DEALLOCATE CursorInfoTrack;
END

GO

--
-- Создать процедуру "dbo.MaintenanceRebuildIndex"
--
GO
PRINT (N'Создать процедуру "dbo.MaintenanceRebuildIndex"')
GO
-- =============================================
-- Description:	<Перестройка индексов>
-- =============================================
CREATE PROCEDURE dbo.MaintenanceRebuildIndex
AS
BEGIN

-- Использование функции sys.dm_db_index_physical_stats в сценарии 
-- для перестройки или реорганизации индексов
SET NOCOUNT ON;
DECLARE @objectid int;
DECLARE @indexid int;
DECLARE @partitioncount bigint;
DECLARE @schemaname nvarchar(130); 
DECLARE @objectname nvarchar(130); 
DECLARE @indexname nvarchar(130); 
DECLARE @partitionnum bigint;
DECLARE @partitions bigint;
DECLARE @frag float;
DECLARE @command nvarchar(4000);
DECLARE @indexType nvarchar(130);
DECLARE @engineEdition int; -- редакция движка сервера

-- Conditionally select tables and indexes from the sys.dm_db_index_physical_stats function 
-- and convert object and index IDs to names.
SELECT
  object_id AS objectid,
  index_id AS indexid,
  index_type_desc AS indexType,
  partition_number AS partitionnum,
  avg_fragmentation_in_percent AS frag
INTO #work_to_do
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, 'LIMITED')
WHERE avg_fragmentation_in_percent > 5.0 AND index_id > 0
ORDER BY object_id;

-- Определяем редакцию движка сервера
SELECT @engineEdition = CAST(SERVERPROPERTY('EngineEdition') AS INT); 

-- Declare the cursor for the list of partitions to be processed.
DECLARE partitions CURSOR FOR SELECT * FROM #work_to_do;

-- Open the cursor.
OPEN partitions;

-- Loop through the partitions.
WHILE (1=1)
BEGIN
  FETCH NEXT FROM partitions
     INTO @objectid, @indexid, @indexType, @partitionnum, @frag;

  IF @@FETCH_STATUS < 0 BREAK;

  SELECT @objectname = QUOTENAME(o.name), @schemaname = QUOTENAME(s.name)
  FROM sys.objects AS o
    JOIN sys.schemas as s ON s.schema_id = o.schema_id
  WHERE o.object_id = @objectid;

  -- Эта таблица нас не итересует
  IF @objectname = N'[sysdiagrams]'
    CONTINUE;

  SELECT @indexname = QUOTENAME(name)
  FROM sys.indexes
  WHERE (object_id = @objectid) AND (index_id = @indexid);

  SELECT @partitioncount = count(*)
  FROM sys.partitions
  WHERE (object_id = @objectid) AND (index_id = @indexid);

  -- 30 is an arbitrary decision point at which to switch between reorganizing and rebuilding.
  IF (@frag < 30.0)
  BEGIN
    SET @command = N'ALTER INDEX ' + @indexname + N' ON ' +  @schemaname + N'.' + @objectname + N' REORGANIZE';
  END ELSE
  BEGIN
    -- ONLINE = ON работает только в редакции движка сервера = 3 (Enterprise, Enterprise Evaluation, Developer).
    -- Кластерные индексы нельзя перестраивать с опцией ONLINE = ON, если в таблице присутсвуют 
    -- поля следующих типов: text, ntext, image, varchar(max), nvarchar(max), varbinary(max), xml. 
    IF (@engineEdition != 3) OR ((@indexType = N'CLUSTERED INDEX') AND 
       ((@objectname = N'[ClientInfo]') OR (@objectname = N'[Command]') OR (@objectname = N'[DealerInfo]') OR 
       (@objectname = N'[ExeptError]') OR (@objectname = N'[UserInfo]')))
    BEGIN   
      SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + 
        N' REBUILD WITH(FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = ON, ONLINE = OFF)';
    END ELSE
    BEGIN
      SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + 
        N' REBUILD WITH(FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = ON, ONLINE = ON)'; 
    END
  END

  IF (@partitioncount > 1)
    SET @command = @command + N' PARTITION=' + CAST(@partitionnum AS nvarchar(10));
   
  -- PRINT N'Executed: ' + @command;
  EXEC (@command);
END

-- Close and deallocate the cursor.
CLOSE partitions;
DEALLOCATE partitions;

-- Drop the temporary table.
DROP TABLE #work_to_do; 

END
GO

--
-- Создать процедуру "dbo.IsTeletrackDatabase"
--
GO
PRINT (N'Создать процедуру "dbo.IsTeletrackDatabase"')
GO


CREATE PROCEDURE dbo.IsTeletrackDatabase
--SQL SECURITY INVOKER
--COMMENT 'Проверка базы данных на соответствие структуре системы Teletrack'
AS
BEGIN
  DECLARE @i TINYINT;
END

GO

--
-- Создать процедуру "dbo.FillLostDataGPS"
--
GO
PRINT (N'Создать процедуру "dbo.FillLostDataGPS"')
GO

CREATE PROCEDURE dbo.FillLostDataGPS
AS
BEGIN
  DECLARE @LAST_RECORDS_COUNT INT;
  SET @LAST_RECORDS_COUNT = 32000;
  DECLARE @LastMobitelRecord INT; 

  DECLARE @MobitelIDInCursor INT;		
  DECLARE @ConfirmedIDInCursor INT;	
  DECLARE @NewConfirmedID INT;			
  DECLARE @NewStartIDSelect INT; 
  SET  @NewStartIDSelect = 0;
  DECLARE @MaxMobitelLogID INT;		

  DECLARE @BeginLogIDInCursor INT; 
  DECLARE @EndLogIDInCursor INT;		
  DECLARE @CountInLostRange INT;	
  --DECLARE @DataExists TINYINT;	
  --SET @DataExists = 1;
  DECLARE @DataChanged TINYINT;	
  DECLARE @NeedOptimize TINYINT; 
  SET @NeedOptimize = 0;
  DECLARE @TableName VARCHAR(255);

  DECLARE CursorMobitels CURSOR FOR
    SELECT Mobitel_ID, ConfirmedID FROM mobitels ORDER BY Mobitel_ID;
  DECLARE CursorLostData CURSOR FOR
    SELECT Begin_LogID, End_LogID FROM datagpslosted
    WHERE Mobitel_ID = @MobitelIDInCursor ORDER BY Begin_LogID;
  DECLARE CursorTable CURSOR FOR SELECT
    '#MUTEX_FILL_LOST_DATA_GPS'
  FROM information_schema.tables
  WHERE table_schema = DB_NAME();

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0;
  
 -- DECLARE EXIT HANDLER FOR SQLEXCEPTION
  --BEGIN
    
    --DROP TABLE IF EXISTS [#MUTEX_FILL_LOST_DATA_GPS];
  --END;

  
  OPEN CursorTable;
  FETCH CursorTable INTO @TableName;
  CLOSE CursorTable;
  
  IF not (@@fetch_status = 0) BEGIN
    
    CREATE TABLE #MUTEX_FILL_LOST_DATA_GPS (dummy char(1));
    --SET @DataExists = 1;

    --CREATE TABLE IF NOT EXISTS #tmp_datagpslosted LIKE dbo.datagps;
	if object_id('tempdb..#tmp_datagpslosted') is not null drop table #tmp_datagpslosted
	SELECT * INTO #tmp_datagpslosted FROM dbo.datagps WHERE 1 = 2

    OPEN CursorMobitels;
    FETCH CursorMobitels INTO @MobitelIDInCursor, @ConfirmedIDInCursor;
    WHILE @@fetch_status = 0
      SET @NewConfirmedID = 0;
      SET @NewStartIDSelect = 0;

      
      TRUNCATE TABLE #tmp_datagpslosted;

      
      SET @MaxMobitelLogID = (SELECT COALESCE(MAX(LogID), 0) 
      FROM dbo.datagps
      WHERE (Mobitel_ID = @MobitelIDInCursor));

      IF @ConfirmedIDInCursor <> @MaxMobitelLogID BEGIN

        SET @DataChanged = 0;
        
        SET @LastMobitelRecord = @MaxMobitelLogID - @LAST_RECORDS_COUNT;

        OPEN CursorLostData;
        FETCH CursorLostData INTO @BeginLogIDInCursor, @EndLogIDInCursor;

        IF  not(@@fetch_status = 0) BEGIN
          SET @DataChanged = 1;
          
          --SET @NewStartIDSelect = GREATEST(@ConfirmedIDInCursor, @LastMobitelRecord);

		   IF @ConfirmedIDInCursor > @LastMobitelRecord
      SET @NewStartIDSelect = @ConfirmedIDInCursor
    ELSE
      SET @NewStartIDSelect = @LastMobitelRecord
        END;

        
        WHILE @@fetch_status = 0 BEGIN
          
          SET  @CountInLostRange = (SELECT COUNT(1)
          FROM dbo.datagps
          WHERE (Mobitel_ID = @MobitelIDInCursor) AND
            (LogID > @BeginLogIDInCursor) AND (LogID < @EndLogIDInCursor));

          
          IF (@CountInLostRange > 0) OR ((@CountInLostRange = 0) AND
            (@BeginLogIDInCursor < @LastMobitelRecord)) BEGIN
            
            DELETE FROM datagpslosted
            WHERE (Mobitel_ID = @MobitelIDInCursor) AND
              (Begin_LogID >= @BeginLogIDInCursor);

            SET @DataChanged = 1;
            SET @NewStartIDSelect = @BeginLogIDInCursor;
          END;

          FETCH CursorLostData INTO @BeginLogIDInCursor, @EndLogIDInCursor;
        END;
        CLOSE CursorLostData;
        
        --SET @DataExists = 1;

        
        IF (@DataChanged = 0) AND (@EndLogIDInCursor < @MaxMobitelLogID) BEGIN
          SET @DataChanged = 1;
          SET @NewStartIDSelect = @EndLogIDInCursor;
        END;

        IF @DataChanged = 1 BEGIN
          
          INSERT INTO #tmp_datagpslosted
          SELECT *
          FROM datagps
          WHERE (Mobitel_ID = @MobitelIDInCursor) AND
            (LogID >= @NewStartIDSelect);

          
          INSERT INTO datagpslosted
          SELECT
            @MobitelIDInCursor AS Mobitel_ID,
            
            dg1.LogID AS Begin_LogID,
            
            dg3.LogID AS End_LogID
          FROM
            
            #tmp_datagpslosted dg1 LEFT JOIN #tmp_datagpslosted dg2 ON (
              (dg2.Mobitel_ID = dg1.Mobitel_ID) AND (dg2.LogID = (dg1.LogID + 1)))
            
            LEFT JOIN #tmp_datagpslosted dg3 ON (
              (dg3.Mobitel_ID = dg1.Mobitel_ID) AND
              
              (dg3.LogID = (
                SELECT MIN(dg.LogID)
                FROM #tmp_datagpslosted dg
                WHERE dg.LogID > dg1.LogID )))
          WHERE
            
            (dg2.LogID IS NULL) AND
            
            (dg3.LogID IS NOT NULL)
          ORDER BY Begin_LogID;

          SET @NeedOptimize = 1;

          
          SET  @NewConfirmedID = (SELECT COALESCE(MIN(Begin_LogID), -1)
          FROM datagpslosted
          WHERE Mobitel_ID = @MobitelIDInCursor);

          
          IF @NewConfirmedID = -1 BEGIN
            SET @NewConfirmedID = @MaxMobitelLogID;
          END;

          
          IF @NewConfirmedID > @ConfirmedIDInCursor BEGIN
            UPDATE mobitels
            SET ConfirmedID = @NewConfirmedID
            WHERE (Mobitel_ID = @MobitelIDInCursor) AND
              (ConfirmedID < @NewConfirmedID);
          END;
         END;
      END;

      FETCH CursorMobitels INTO @MobitelIDInCursor, @ConfirmedIDInCursor;
    END;
    CLOSE CursorMobitels;

	if object_id('tempdb..#tmp_datagpslosted') is not null drop table #tmp_datagpslosted
	  IF @NeedOptimize = 1 begin
    ALTER INDEX ALL ON datagpslosted REORGANIZE
    END;
    
	if object_id('tempdb..#tmp_datagpslosted') is not null drop table #MUTEX_FILL_LOST_DATA_GPS
  BEGIN
    SELECT '1';
  END;
END

GO

--
-- Создать процедуру "dbo.ExecuteQuery"
--
GO
PRINT (N'Создать процедуру "dbo.ExecuteQuery"')
GO

CREATE PROCEDURE dbo.ExecuteQuery
(
  @sqlQuery VARCHAR
)
AS
BEGIN
  DECLARE @s VARCHAR;
  SET @s = @sqlQuery;
  EXECUTE (@s);
END

GO

--
-- Создать процедуру "dbo.DrTransferBuffer"
--
GO
PRINT (N'Создать процедуру "dbo.DrTransferBuffer"')
GO


CREATE PROCEDURE dbo.DrTransferBuffer
--SQL SECURITY INVOKER
--COMMENT 'Переносит GPS данные DirectOnline из буферной таблицы в datagps'
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  --DECLARE @DataExists TINYINT;
  --SET @DataExists = 1; 

  DECLARE @TmpMaxUnixTime INT;

  DECLARE CursorMobitels CURSOR FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_dr;

  DELETE o
  FROM
    online o, datagpsbuffer_dr dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    INSERT INTO online(Mobitel_ID
                     , LogID
                     , UnixTime
                     , Latitude
                     , Longitude
                     , Altitude
                     , Direction
                     , Speed
                     , Valid
                     , [Events]
                     , Sensor1
                     , Sensor2
                     , Sensor3
                     , Sensor4
                     , Sensor5
                     , Sensor6
                     , Sensor7
                     , Sensor8
                     , Counter1
                     , Counter2
                     , Counter3
                     , Counter4
                     , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , [Events]
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_dr
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;


    DELETE
    FROM
      online
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        [online]
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END; --END WHILE;
  CLOSE CursorMobitels;

  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, 
	Valid = b.Valid, InMobitelID = b.LogID, [Events] = b.[Events], Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, 
	Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, 
	Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, 
	Counter4 = b.Counter4
  FROM
    dbo.datagps d, dbo.datagpsbuffer_dr b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  DELETE b
  FROM
    datagps d, datagpsbuffer_dr b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);


  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , [Events]
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , [Events]
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
  FROM
    datagpsbuffer_dr;
	--GROUP BY  Mobitel_ID, LogID;

  TRUNCATE TABLE datagpsbuffer_dr;
END


GO

--
-- Создать процедуру "dbo.DrListMobitelConfig"
--
GO
PRINT (N'Создать процедуру "dbo.DrListMobitelConfig"')
GO

CREATE PROCEDURE dbo.DrListMobitelConfig
--SQL SECURITY INVOKER
--COMMENT 'Выборка настроек телетреков в режиме DirectOnline'
AS
BEGIN
  SELECT m.Mobitel_id AS MobitelID
       , imc.DevIdShort AS DevIdShort
       , cge.Pop3un AS Login
       , cge.Pop3pw AS Password
  FROM
    Mobitels m
    JOIN ConfigMain cm
      ON m.Mobitel_id = cm.Mobitel_ID
    JOIN ConfigGprsEmail cge
      ON cm.ConfigGprsEmail_ID = cge.ID
    JOIN internalmobitelconfig imc
      ON m.InternalMobitelConfig_ID = imc.ID
  WHERE
    (cge.ConfigGprsEmail_ID = (SELECT max(intCge.ConfigGprsEmail_ID)
                               FROM
                                 ConfigGprsEmail intCge
                               WHERE
                                 intCge.ID = cge.ID))
    AND (imc.InternalMobitelConfig_ID = (SELECT max(intConf.InternalMobitelConfig_ID)
                                         FROM
                                           internalmobitelconfig intConf
                                         WHERE
                                           intConf.ID = imc.ID))
    AND (imc.devIdShort IS NOT NULL)
    AND (imc.devIdShort <> '')
    AND (cge.Pop3un IS NOT NULL)
    AND (cge.Pop3un <> '')
  ORDER BY
    m.Mobitel_ID;
END

GO

--
-- Создать процедуру "dbo.CheckDB"
--
GO
PRINT (N'Создать процедуру "dbo.CheckDB"')
GO

CREATE PROCEDURE dbo.CheckDB
--SQL SECURITY INVOKER
--COMMENT 'Проверка БД при подключении.'
AS
BEGIN
  SELECT 'OK';
END

GO

--
-- Создать функцию "dbo.get_hh_min"
--
GO
PRINT (N'Создать функцию "dbo.get_hh_min"')
GO
CREATE FUNCTION dbo.get_hh_min
(
  @minute FLOAT
)
RETURNS TIME
AS
BEGIN
	DECLARE @tm VARCHAR(5);
	DECLARE @times TIME;
  if (@minute<0)
  BEGIN
	SET @tm = '00:00';
  END
  ELSE
	BEGIN			
	  DECLARE @hours INT;
	  SET @hours = @minute / 60.0;
	  DECLARE @mn INT;
	  SET @mn = @minute - @hours * 60;
	  SET @tm = convert(VARCHAR(2), @hours) + ':' + convert(VARCHAR(2), @mn);
	END
	SET @times = convert(TIME, @tm);
	RETURN @times;
END;
GO
SET NOEXEC OFF
GO