﻿--
-- Скрипт сгенерирован Devart dbForge Studio for SQL Server, Версия 5.1.178.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/sql/studio
-- Дата скрипта: 21.12.2016 17:49:24
-- Версия сервера: 10.50.2500
-- Версия клиента: 
--



USE mca_agro
GO

IF DB_NAME() <> N'mca_agro' SET NOEXEC ON
GO

--
-- Создать таблицу "dbo.zonesgroup"
--
PRINT (N'Создать таблицу "dbo.zonesgroup"')
GO
CREATE TABLE dbo.zonesgroup (
  Id bigint IDENTITY,
  Title varchar(50) NOT NULL DEFAULT (''),
  Description varchar(200) NOT NULL DEFAULT (''),
  OutLinkId varchar(20) NULL,
  CONSTRAINT PK_zonesgroup PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zonesgroup.OutLinkId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zonesgroup.OutLinkId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код внешней базы', 'SCHEMA', N'dbo', 'TABLE', N'zonesgroup', 'COLUMN', N'OutLinkId'
GO

--
-- Создать таблицу "dbo.zones"
--
PRINT (N'Создать таблицу "dbo.zones"')
GO
CREATE TABLE dbo.zones (
  Zone_ID int IDENTITY,
  Name varchar(50) NULL,
  Descr varchar(max) NULL,
  ID int NULL CONSTRAINT DF__zones__ID__00CA12DE DEFAULT (0),
  Status smallint NULL CONSTRAINT DF__zones__Status__01BE3717 DEFAULT (0),
  Square float NULL CONSTRAINT DF__zones__Square__02B25B50 DEFAULT (0.00000),
  ZoneColor int NULL CONSTRAINT DF__zones__ZoneColor__03A67F89 DEFAULT (-16776961),
  TextColor int NULL CONSTRAINT DF__zones__TextColor__049AA3C2 DEFAULT (255),
  TextFontName varchar(50) NULL CONSTRAINT DF__zones__TextFontN__058EC7FB DEFAULT ('Microsoft Sans Serif'),
  TextFontSize float NULL CONSTRAINT DF__zones__TextFontS__0682EC34 DEFAULT (8.00),
  TextFontParams char(8) NULL CONSTRAINT DF__zones__TextFontP__0777106D DEFAULT ('0;0;0;0;'),
  Icon varbinary(max) NULL,
  HatchStyle int NULL CONSTRAINT DF__zones__HatchStyl__086B34A6 DEFAULT (0),
  DateCreate datetime NULL,
  Id_main int NULL CONSTRAINT DF__zones__Id_main__095F58DF DEFAULT (0),
  Level smallint NULL CONSTRAINT DF__zones__Level__0A537D18 DEFAULT (1),
  PassengersNoCount smallint NOT NULL CONSTRAINT DF__zones__Passenger__0B47A151 DEFAULT (0),
  StyleId bigint NOT NULL CONSTRAINT DF__zones__StyleId__0C3BC58A DEFAULT (0),
  ZonesGroupId bigint NOT NULL CONSTRAINT DF__zones__ZonesGrou__0D2FE9C3 DEFAULT (1),
  OutLinkId varchar(20) NULL,
  Category_id int NULL DEFAULT (NULL),
  Category_id2 int NULL DEFAULT (NULL),
  CONSTRAINT PK_zones PRIMARY KEY CLUSTERED (Zone_ID),
  CONSTRAINT fgn_key_FK_zones_zonesgroup FOREIGN KEY (ZonesGroupId) REFERENCES dbo.zonesgroup (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "FK_zones_zonesgroup" для объекта типа таблица "dbo.zones"
--
PRINT (N'Создать индекс "FK_zones_zonesgroup" для объекта типа таблица "dbo.zones"')
GO
CREATE INDEX FK_zones_zonesgroup
  ON dbo.zones (ZonesGroupId)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.Square" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.Square" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Площадь контрольной зоны', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'Square'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.ZoneColor" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.ZoneColor" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Цвет контрольной зоны', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'ZoneColor'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.TextColor" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.TextColor" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Цвет подписи контрольной зоны', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'TextColor'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.TextFontName" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.TextFontName" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Шрирфт подписи контрольной зоны', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'TextFontName'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.TextFontSize" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.TextFontSize" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Размер шрирфта подписи контрольной зоны', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'TextFontSize'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.TextFontParams" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.TextFontParams" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры шрирфта подписи контрольной зоны', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'TextFontParams'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.Icon" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.Icon" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Иконка контрольной зоны', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'Icon'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.HatchStyle" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.HatchStyle" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Текстура контрольной зоны', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'HatchStyle'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.DateCreate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.DateCreate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата создания', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'DateCreate'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.Id_main" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.Id_main" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на элемент верхнего уровня в иерархии', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'Id_main'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.Level" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.Level" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Уровень в иерархии', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'Level'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.PassengersNoCount" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.PassengersNoCount" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Следует ли исключить подсчет пассажиров в этой зоне', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'PassengersNoCount'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.StyleId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.StyleId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ID стиля зоны', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'StyleId'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.ZonesGroupId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.ZonesGroupId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ID Группы зон', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'ZonesGroupId'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.zones.OutLinkId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.zones.OutLinkId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код внешней базы', 'SCHEMA', N'dbo', 'TABLE', N'zones', 'COLUMN', N'OutLinkId'
GO

--
-- Создать таблицу "dbo.report_pass_zones"
--
PRINT (N'Создать таблицу "dbo.report_pass_zones"')
GO
CREATE TABLE dbo.report_pass_zones (
  ID int IDENTITY,
  ZoneID int NOT NULL,
  CONSTRAINT FK_RPZ_Zones FOREIGN KEY (ZoneID) REFERENCES dbo.zones (Zone_ID) ON DELETE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_ZoneID" для объекта типа таблица "dbo.report_pass_zones"
--
PRINT (N'Создать индекс "IDX_ZoneID" для объекта типа таблица "dbo.report_pass_zones"')
GO
CREATE INDEX IDX_ZoneID
  ON dbo.report_pass_zones (ZoneID)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.report_pass_zones.ZoneID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.report_pass_zones.ZoneID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор КЗ', 'SCHEMA', N'dbo', 'TABLE', N'report_pass_zones', 'COLUMN', N'ZoneID'
GO

--
-- Создать таблицу "dbo.points"
--
PRINT (N'Создать таблицу "dbo.points"')
GO
CREATE TABLE dbo.points (
  Point_ID int IDENTITY,
  Name varchar(50) NULL,
  Descr varchar(200) NULL,
  ID int NULL CONSTRAINT DF__points__ID__153B1FDF DEFAULT (0),
  Latitude int NULL CONSTRAINT DF__points__Latitude__162F4418 DEFAULT (0),
  Longitude int NULL CONSTRAINT DF__points__Longitud__17236851 DEFAULT (0),
  Zone_ID int NULL CONSTRAINT DF__points__Zone_ID__18178C8A DEFAULT (0),
  CONSTRAINT fgn_key_FK_points_zones FOREIGN KEY (Zone_ID) REFERENCES dbo.zones (Zone_ID) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "FK_points_zones" для объекта типа таблица "dbo.points"
--
PRINT (N'Создать индекс "FK_points_zones" для объекта типа таблица "dbo.points"')
GO
CREATE INDEX FK_points_zones
  ON dbo.points (Zone_ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.zone_category2"
--
PRINT (N'Создать таблицу "dbo.zone_category2"')
GO
CREATE TABLE dbo.zone_category2 (
  Id int IDENTITY,
  Name varchar(255) NOT NULL,
  CONSTRAINT PK_zone_category2 PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

--
-- Создать представление "dbo.square_for_gis_2017"
--
GO
PRINT (N'Создать представление "dbo.square_for_gis_2017"')
GO
CREATE VIEW dbo.square_for_gis_2017 
AS SELECT zones.Zone_ID AS id
        , left(zonesgroup.Title, len(zonesgroup.Title) - 5) AS klaster
        , left(zones.Name, len(zones.Name) - 5) AS name
        , zone_category2.Name AS kult
        , cast(zones.Square * 100 AS DEC(12, 1)) AS square
        , zones.Descr
   FROM
     dbo.zones
     INNER JOIN dbo.zonesgroup
       ON zones.ZonesGroupId = zonesgroup.Id
     INNER JOIN dbo.zone_category2
       ON zone_category2.Id = zones.Category_id2
   WHERE
     (zones.ZonesGroupId = 176
     OR zones.ZonesGroupId = 183
     OR zones.ZonesGroupId = 184
     OR zones.ZonesGroupId = 185
     OR zones.ZonesGroupId = 186
     OR zones.ZonesGroupId = 187
     OR zones.ZonesGroupId = 188)
     AND (zones.Name LIKE '%_2017'
     AND zones.Name NOT LIKE '%_SOC%')
GO

--
-- Создать представление "dbo.square_for_gis_2016"
--
GO
PRINT (N'Создать представление "dbo.square_for_gis_2016"')
GO
CREATE VIEW dbo.square_for_gis_2016 
AS SELECT zones.Zone_ID AS id
        ,left(zonesgroup.Title, len(zonesgroup.Title)-5) AS klaster
        , left(zones.Name, len(zones.Name) - 5) AS name
        , zone_category2.Name AS kult
        , cast(zones.Square * 100 AS DEC(12, 1)) AS square
        , zones.Descr
   FROM
     dbo.zones
     INNER JOIN dbo.zonesgroup
       ON zones.ZonesGroupId = zonesgroup.Id
     INNER JOIN dbo.zone_category2
       ON zone_category2.Id = zones.Category_id2
   WHERE
     (zones.ZonesGroupId = 73
     OR zones.ZonesGroupId = 23
     OR zones.ZonesGroupId = 65
     OR zones.ZonesGroupId = 141
     OR zones.ZonesGroupId = 14
     OR zones.ZonesGroupId = 130
     OR zones.ZonesGroupId = 43)
     AND zones.Name LIKE '%_2016'
     AND zones.Name NOT LIKE '%_SOC%'
GO

--
-- Создать представление "dbo.for_1C"
--
GO
PRINT (N'Создать представление "dbo.for_1C"')
GO
CREATE VIEW dbo.for_1C
AS SELECT zones.Zone_ID AS id
        , left(zonesgroup.Title, len(zonesgroup.Title) - 5) AS klaster
        , left(zones.Name, len(zones.Name) - 5) AS posivna
        , left(zones.Name, 10) AS pole
        , RIGHT(zones.Name, 4) AS rik
        , zone_category2.Name AS kult
        , cast(zones.Square * 100 AS DEC(12, 1)) AS square
   FROM
     dbo.zones
     INNER JOIN dbo.zonesgroup
       ON zones.ZonesGroupId = zonesgroup.Id
     INNER JOIN dbo.zone_category2
       ON zone_category2.Id = zones.Category_id2
   WHERE
     (zones.ZonesGroupId = 176
     OR zones.ZonesGroupId = 183
     OR zones.ZonesGroupId = 184
     OR zones.ZonesGroupId = 185
     OR zones.ZonesGroupId = 186
     OR zones.ZonesGroupId = 187
     OR zones.ZonesGroupId = 188
     OR zones.ZonesGroupId = 73
     OR zones.ZonesGroupId = 23
     OR zones.ZonesGroupId = 65
     OR zones.ZonesGroupId = 141
     OR zones.ZonesGroupId = 14
     OR zones.ZonesGroupId = 130
     OR zones.ZonesGroupId = 43)
     AND zones.Name NOT LIKE '%_SOC%'
GO

--
-- Создать таблицу "dbo.zone_category"
--
PRINT (N'Создать таблицу "dbo.zone_category"')
GO
CREATE TABLE dbo.zone_category (
  Id int IDENTITY,
  Name varchar(255) NOT NULL,
  CONSTRAINT PK_zone_category PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать представление "dbo.points_for_gis_2017"
--
GO
PRINT (N'Создать представление "dbo.points_for_gis_2017"')
GO
CREATE VIEW dbo.points_for_gis_2017 
AS SELECT zones.Zone_ID AS id
        , left(zones.Name, len(zones.Name) - 5) AS Name
        , points.Point_ID
        , (points.Latitude * 1.000000) / 600000 AS Lat
        , (points.Longitude * 1.000000) / 600000 AS Lon
   FROM
     dbo.zone_category
     INNER JOIN dbo.zones
       ON zone_category.Id = zones.Category_id
     INNER JOIN dbo.points
       ON points.Zone_ID = zones.Zone_ID
   WHERE
     (zones.ZonesGroupId = 176
     OR zones.ZonesGroupId = 183
     OR zones.ZonesGroupId = 184
     OR zones.ZonesGroupId = 185
     OR zones.ZonesGroupId = 186
     OR zones.ZonesGroupId = 187
     OR zones.ZonesGroupId = 188)
     AND (zones.Name LIKE '%_2017'
     AND zones.Name NOT LIKE '%_SOC%')

GO

--
-- Создать представление "dbo.points_for_gis_2016"
--
GO
PRINT (N'Создать представление "dbo.points_for_gis_2016"')
GO
CREATE VIEW dbo.points_for_gis_2016 
AS SELECT zones.Zone_ID AS id
        , left(zones.Name, len(zones.Name) - 5) AS Name
        , points.Point_ID
        , (points.Latitude * 1.000000) / 600000 AS Lat
        , (points.Longitude * 1.000000) / 600000 AS Lon
   FROM
     dbo.zone_category
     INNER JOIN dbo.zones
       ON zone_category.Id = zones.Category_id
     INNER JOIN dbo.points
       ON points.Zone_ID = zones.Zone_ID
   WHERE
     (zones.ZonesGroupId = 73
     OR zones.ZonesGroupId = 23
     OR zones.ZonesGroupId = 65
     OR zones.ZonesGroupId = 141
     OR zones.ZonesGroupId = 14
     OR zones.ZonesGroupId = 130
     OR zones.ZonesGroupId = 43)
     AND zones.Name LIKE '%_2016'
     AND zones.Name NOT LIKE '%_SOC%'

GO

--
-- Создать таблицу "dbo.ver"
--
PRINT (N'Создать таблицу "dbo.ver"')
GO
CREATE TABLE dbo.ver (
  id int IDENTITY,
  Num bigint NOT NULL DEFAULT (0),
  [Desc] varchar(max) NOT NULL,
  time datetime NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.ver.Num" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.ver.Num" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер текущей сборки БД', 'SCHEMA', N'dbo', 'TABLE', N'ver', 'COLUMN', N'Num'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.ver.[Desc]" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.ver.[Desc]" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Описание текущей сборки БД', 'SCHEMA', N'dbo', 'TABLE', N'ver', 'COLUMN', N'Desc'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.ver.time" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.ver.time" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Время обновления', 'SCHEMA', N'dbo', 'TABLE', N'ver', 'COLUMN', N'time'
GO

--
-- Создать таблицу "dbo.vehicle_state"
--
PRINT (N'Создать таблицу "dbo.vehicle_state"')
GO
CREATE TABLE dbo.vehicle_state (
  Id int IDENTITY,
  Name varchar(50) NOT NULL,
  MaxTime decimal(10, 2) NOT NULL,
  Remark varchar(255) NULL,
  Tariffication bit NOT NULL,
  CONSTRAINT PK_vehicle_state PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.vehicle_old"
--
PRINT (N'Создать таблицу "dbo.vehicle_old"')
GO
CREATE TABLE dbo.vehicle_old (
  id int NOT NULL,
  NumberPlate varchar(max) NULL,
  Login char(20) NULL,
  Mobitel_id int NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle_old.NumberPlate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle_old.NumberPlate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Номерной знак', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_old', 'COLUMN', N'NumberPlate'
GO

--
-- Создать таблицу "dbo.vehicle_new"
--
PRINT (N'Создать таблицу "dbo.vehicle_new"')
GO
CREATE TABLE dbo.vehicle_new (
  id int NOT NULL,
  MakeCar varchar(max) NULL,
  NumberPlate varchar(max) NULL,
  Team_id int NULL,
  Mobitel_id int NULL,
  Odometr int NULL DEFAULT (0),
  CarModel varchar(max) NULL,
  setting_id int NULL DEFAULT (1),
  odoTime datetime NULL,
  driver_id int NULL DEFAULT (-1),
  OutLinkId varchar(20) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.MakeCar" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.MakeCar" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Марка автомобиля', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_new', 'COLUMN', N'MakeCar'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.NumberPlate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.NumberPlate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Номерной знак', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_new', 'COLUMN', N'NumberPlate'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.Odometr" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.Odometr" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Показания одометра', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_new', 'COLUMN', N'Odometr'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.CarModel" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.CarModel" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Модель', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_new', 'COLUMN', N'CarModel'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.odoTime" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.odoTime" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата и время сверки одометра', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_new', 'COLUMN', N'odoTime'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.OutLinkId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle_new.OutLinkId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код внешней базы', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_new', 'COLUMN', N'OutLinkId'
GO

--
-- Создать таблицу "dbo.vehicle_commentary"
--
PRINT (N'Создать таблицу "dbo.vehicle_commentary"')
GO
CREATE TABLE dbo.vehicle_commentary (
  Id int IDENTITY,
  Comment varchar(255) NOT NULL,
  Mobitel_id int NOT NULL,
  CONSTRAINT PK_vehicle_commentary PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.vehicle_category4"
--
PRINT (N'Создать таблицу "dbo.vehicle_category4"')
GO
CREATE TABLE dbo.vehicle_category4 (
  Id int IDENTITY,
  Name varchar(255) NOT NULL,
  CONSTRAINT PK_vehicle_category4 PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.vehicle_category3"
--
PRINT (N'Создать таблицу "dbo.vehicle_category3"')
GO
CREATE TABLE dbo.vehicle_category3 (
  Id int IDENTITY,
  Name varchar(255) NOT NULL,
  CONSTRAINT PK_vehicle_category3 PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.vehicle_category2"
--
PRINT (N'Создать таблицу "dbo.vehicle_category2"')
GO
CREATE TABLE dbo.vehicle_category2 (
  Id int IDENTITY,
  Name varchar(255) NOT NULL DEFAULT ('')
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle_category2.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle_category2.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Категория2 транспортного средства', 'SCHEMA', N'dbo', 'TABLE', N'vehicle_category2', 'COLUMN', N'Name'
GO

--
-- Создать таблицу "dbo.vehicle_category"
--
PRINT (N'Создать таблицу "dbo.vehicle_category"')
GO
CREATE TABLE dbo.vehicle_category (
  Id int IDENTITY,
  Name varchar(50) NOT NULL DEFAULT (''),
  Tonnage decimal(10, 3) NOT NULL DEFAULT (0),
  FuelNormMoving float NOT NULL DEFAULT (0),
  FuelNormParking float NOT NULL DEFAULT (0),
  CONSTRAINT PK_vehicle_category PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.users_reports_list"
--
PRINT (N'Создать таблицу "dbo.users_reports_list"')
GO
CREATE TABLE dbo.users_reports_list (
  Id int IDENTITY,
  ReportType varchar(127) NOT NULL DEFAULT (''),
  ReportCaption varchar(127) NOT NULL DEFAULT ('')
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.users_list"
--
PRINT (N'Создать таблицу "dbo.users_list"')
GO
CREATE TABLE dbo.users_list (
  Id int IDENTITY,
  Name varchar(255) NOT NULL DEFAULT (''),
  WinLogin varchar(50) NULL,
  Password varbinary(max) NOT NULL,
  Admin smallint NOT NULL DEFAULT (0),
  Id_role int NULL DEFAULT (0)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.users_list.Id_role" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.users_list.Id_role" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код роли пользователя', 'SCHEMA', N'dbo', 'TABLE', N'users_list', 'COLUMN', N'Id_role'
GO

--
-- Создать таблицу "dbo.users_actions_log"
--
PRINT (N'Создать таблицу "dbo.users_actions_log"')
GO
CREATE TABLE dbo.users_actions_log (
  Id int IDENTITY,
  TypeLog int NOT NULL DEFAULT (0),
  DateAction datetime NOT NULL,
  Id_document int NOT NULL DEFAULT (0),
  TextLog varchar(255) NOT NULL DEFAULT (''),
  [User] varchar(127) NOT NULL DEFAULT (''),
  Computer varchar(63) NOT NULL DEFAULT ('')
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.users_acs_roles"
--
PRINT (N'Создать таблицу "dbo.users_acs_roles"')
GO
CREATE TABLE dbo.users_acs_roles (
  Id int IDENTITY,
  Name varchar(255) NOT NULL DEFAULT (''),
  CONSTRAINT PK_users_acs_roles PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.users_acs_objects"
--
PRINT (N'Создать таблицу "dbo.users_acs_objects"')
GO
CREATE TABLE dbo.users_acs_objects (
  Id int IDENTITY,
  Id_role int NOT NULL,
  Id_type int NOT NULL,
  Id_object int NOT NULL,
  CONSTRAINT fgn_key_FK_users_acs_objects_users_acs_roles_Id FOREIGN KEY (Id_role) REFERENCES dbo.users_acs_roles (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "FK_users_acs_objects_users_acs_roles_Id" для объекта типа таблица "dbo.users_acs_objects"
--
PRINT (N'Создать индекс "FK_users_acs_objects_users_acs_roles_Id" для объекта типа таблица "dbo.users_acs_objects"')
GO
CREATE INDEX FK_users_acs_objects_users_acs_roles_Id
  ON dbo.users_acs_objects (Id_role)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.users"
--
PRINT (N'Создать таблицу "dbo.users"')
GO
CREATE TABLE dbo.users (
  UserID int IDENTITY (21, 1),
  Login nvarchar(15) NOT NULL,
  FirstName nvarchar(30) NOT NULL,
  LastName nvarchar(30) NOT NULL,
  Password nvarchar(120) NOT NULL,
  RightType smallint NOT NULL,
  TrackControlUserID int NULL DEFAULT (0),
  CONSTRAINT PK_users_UserID PRIMARY KEY CLUSTERED (UserID)
)
ON [PRIMARY]
GO

--
-- Создать индекс "Login" для объекта типа таблица "dbo.users"
--
PRINT (N'Создать индекс "Login" для объекта типа таблица "dbo.users"')
GO
CREATE INDEX Login
  ON dbo.users (Login)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.users" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.users" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.users', 'SCHEMA', N'dbo', 'TABLE', N'users'
GO

--
-- Создать таблицу "dbo.ua_zvirka"
--
PRINT (N'Создать таблицу "dbo.ua_zvirka"')
GO
CREATE TABLE dbo.ua_zvirka (
  klaster varchar(50) NULL,
  kategory varchar(50) NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.ua_gsp_on"
--
PRINT (N'Создать таблицу "dbo.ua_gsp_on"')
GO
CREATE TABLE dbo.ua_gsp_on (
  taim datetime NULL,
  tz varchar(max) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.transition"
--
PRINT (N'Создать таблицу "dbo.transition"')
GO
CREATE TABLE dbo.transition (
  Id int IDENTITY,
  InitialStateId int NULL,
  FinalStateId int NULL,
  Title varchar(50) NOT NULL CONSTRAINT DF__transitio__Title__511AFFBC DEFAULT (''),
  SensorId int NULL,
  IconName varchar(50) NOT NULL CONSTRAINT DF__transitio__IconN__520F23F5 DEFAULT (''),
  SoundName varchar(50) NULL CONSTRAINT DF__transitio__Sound__5303482E DEFAULT (''),
  Enabled smallint NOT NULL,
  ViewInReport smallint NOT NULL CONSTRAINT DF__transitio__ViewI__53F76C67 DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "FinalState_FK" для объекта типа таблица "dbo.transition"
--
PRINT (N'Создать индекс "FinalState_FK" для объекта типа таблица "dbo.transition"')
GO
CREATE INDEX FinalState_FK
  ON dbo.transition (FinalStateId)
  ON [PRIMARY]
GO

--
-- Создать индекс "InitialState_FK" для объекта типа таблица "dbo.transition"
--
PRINT (N'Создать индекс "InitialState_FK" для объекта типа таблица "dbo.transition"')
GO
CREATE INDEX InitialState_FK
  ON dbo.transition (InitialStateId)
  ON [PRIMARY]
GO

--
-- Создать индекс "Sensors_FK" для объекта типа таблица "dbo.transition"
--
PRINT (N'Создать индекс "Sensors_FK" для объекта типа таблица "dbo.transition"')
GO
CREATE INDEX Sensors_FK
  ON dbo.transition (SensorId)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.telnumbers"
--
PRINT (N'Создать таблицу "dbo.telnumbers"')
GO
CREATE TABLE dbo.telnumbers (
  TelNumber_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  TelNumber char(50) NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.team"
--
PRINT (N'Создать таблицу "dbo.team"')
GO
CREATE TABLE dbo.team (
  id int IDENTITY,
  Name varchar(50) NULL,
  Descr varchar(max) NULL,
  Setting_id int NULL,
  OutLinkId varchar(20) NULL,
  FuelWayKmGrp float NULL DEFAULT (0),
  FuelMotoHrGrp float NULL DEFAULT (0)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "id" для объекта типа таблица "dbo.team"
--
PRINT (N'Создать индекс "id" для объекта типа таблица "dbo.team"')
GO
CREATE UNIQUE INDEX id
  ON dbo.team (id)
  ON [PRIMARY]
GO

--
-- Создать индекс "Setting_id" для объекта типа таблица "dbo.team"
--
PRINT (N'Создать индекс "Setting_id" для объекта типа таблица "dbo.team"')
GO
CREATE INDEX Setting_id
  ON dbo.team (Setting_id)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.team.Descr" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.team.Descr" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Описание', 'SCHEMA', N'dbo', 'TABLE', N'team', 'COLUMN', N'Descr'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.team.OutLinkId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.team.OutLinkId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код внешней базы', 'SCHEMA', N'dbo', 'TABLE', N'team', 'COLUMN', N'OutLinkId'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.team.FuelWayKmGrp" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.team.FuelWayKmGrp" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Норма расхода топлива л/100 км', 'SCHEMA', N'dbo', 'TABLE', N'team', 'COLUMN', N'FuelWayKmGrp'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.team.FuelMotoHrGrp" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.team.FuelMotoHrGrp" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Норма расхода топлива на холостом ходу л/моточас', 'SCHEMA', N'dbo', 'TABLE', N'team', 'COLUMN', N'FuelMotoHrGrp'
GO

--
-- Создать таблицу "dbo.sysschemas"
--
PRINT (N'Создать таблицу "dbo.sysschemas"')
GO
CREATE TABLE dbo.sysschemas (
  SysSchemaID int IDENTITY (7, 1),
  SysSchemaName nvarchar(50) NOT NULL,
  SysModuleID int NOT NULL,
  [Create] datetime2(0) NULL DEFAULT (NULL),
  Created int NULL DEFAULT (NULL),
  Modify datetime2(0) NULL DEFAULT (NULL),
  Modified int NULL DEFAULT (NULL),
  SchemaMetaData nvarchar(max) NULL,
  IsActive smallint NOT NULL,
  CONSTRAINT PK_sysschemas_SysSchemaID PRIMARY KEY CLUSTERED (SysSchemaID)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.sysschemas" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.sysschemas" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.sysschemas', 'SCHEMA', N'dbo', 'TABLE', N'sysschemas'
GO

--
-- Создать таблицу "dbo.sysmodules"
--
PRINT (N'Создать таблицу "dbo.sysmodules"')
GO
CREATE TABLE dbo.sysmodules (
  SysModuleID int IDENTITY (3, 1),
  ModuleName nvarchar(50) NOT NULL,
  [Create] datetime2(0) NULL DEFAULT (NULL),
  Created int NULL DEFAULT (NULL),
  Modify datetime2(0) NULL DEFAULT (NULL),
  Modified int NULL DEFAULT (NULL),
  ModuleMetaData nvarchar(max) NULL,
  IsActive smallint NOT NULL,
  CONSTRAINT PK_sysmodules_SysModuleID PRIMARY KEY CLUSTERED (SysModuleID)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.sysmodules" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.sysmodules" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.sysmodules', 'SCHEMA', N'dbo', 'TABLE', N'sysmodules'
GO

--
-- Создать таблицу "dbo.swupdate"
--
PRINT (N'Создать таблицу "dbo.swupdate"')
GO
CREATE TABLE dbo.swupdate (
  ID int IDENTITY,
  ShortID char(4) NOT NULL DEFAULT (''),
  Soft varchar(max) NOT NULL,
  InsertTime datetime NOT NULL DEFAULT ('GETDATE()')
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.swupdate.ShortID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.swupdate.ShortID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор телетрека(логин)', 'SCHEMA', N'dbo', 'TABLE', N'swupdate', 'COLUMN', N'ShortID'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.swupdate.Soft" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.swupdate.Soft" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Прошивка', 'SCHEMA', N'dbo', 'TABLE', N'swupdate', 'COLUMN', N'Soft'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.swupdate.InsertTime" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.swupdate.InsertTime" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата и время вставки в таблицу', 'SCHEMA', N'dbo', 'TABLE', N'swupdate', 'COLUMN', N'InsertTime'
GO

--
-- Создать таблицу "dbo.specialmessages"
--
PRINT (N'Создать таблицу "dbo.specialmessages"')
GO
CREATE TABLE dbo.specialmessages (
  SpecMessage_ID int IDENTITY,
  DataString char(50) NULL,
  DataInteger int NULL DEFAULT (0),
  unixtime char(50) NULL,
  isNew int NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.sources"
--
PRINT (N'Создать таблицу "dbo.sources"')
GO
CREATE TABLE dbo.sources (
  Source_ID int IDENTITY,
  SourceText varchar(max) NULL,
  SourceError varchar(max) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.smsnumbers"
--
PRINT (N'Создать таблицу "dbo.smsnumbers"')
GO
CREATE TABLE dbo.smsnumbers (
  SmsNumber_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  smsNumber char(14) NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.setting"
--
PRINT (N'Создать таблицу "dbo.setting"')
GO
CREATE TABLE dbo.setting (
  id int IDENTITY,
  TimeBreak time NULL DEFAULT ('00:01:00'),
  RotationMain int NULL DEFAULT (100),
  RotationAdd int NULL DEFAULT (100),
  FuelingEdge int NULL DEFAULT (0),
  band int NULL DEFAULT (25),
  FuelingDischarge int NULL DEFAULT (0),
  accelMax float NULL DEFAULT (1),
  breakMax float NULL DEFAULT (1),
  speedMax float NULL DEFAULT (70),
  idleRunningMain int NULL DEFAULT (0),
  idleRunningAdd int NULL DEFAULT (0),
  Name char(20) NULL DEFAULT ('Установки'),
  [Desc] char(80) NULL DEFAULT ('Описание установки'),
  AvgFuelRatePerHour float NOT NULL DEFAULT (30.00000),
  FuelApproximationTime time NOT NULL DEFAULT ('00:10:00'),
  TimeGetFuelAfterStop time NOT NULL DEFAULT ('00:00:00'),
  TimeGetFuelBeforeMotion time NOT NULL DEFAULT ('00:00:00'),
  CzMinCrossingPairTime time NOT NULL DEFAULT ('00:01:00'),
  FuelerRadiusFinds int NOT NULL DEFAULT (10),
  FuelerMaxTimeStop time NOT NULL DEFAULT ('00:02:00'),
  FuelerMinFuelrate int NOT NULL DEFAULT (1),
  FuelerCountInMotion int NOT NULL DEFAULT (0),
  FuelrateWithDischarge int NOT NULL DEFAULT (0),
  FuelingMinMaxAlgorithm int NOT NULL DEFAULT (0),
  TimeBreakMaxPermitted time NOT NULL DEFAULT ('03:00:00'),
  InclinometerMaxAngleAxisX float NOT NULL DEFAULT (5),
  InclinometerMaxAngleAxisY float NOT NULL DEFAULT (5),
  speedMin float NOT NULL DEFAULT (0),
  TimeMinimMovingSize time NOT NULL DEFAULT ('00:00:00'),
  MinTimeSwitch int NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "id" для объекта типа таблица "dbo.setting"
--
PRINT (N'Создать индекс "id" для объекта типа таблица "dbo.setting"')
GO
CREATE UNIQUE INDEX id
  ON dbo.setting (id)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.TimeBreak" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.TimeBreak" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Минимальное время остановки', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'TimeBreak'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.RotationMain" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.RotationMain" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Обороты основного двигателя', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'RotationMain'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.RotationAdd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.RotationAdd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Обороты доп. двигателя', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'RotationAdd'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelingEdge" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelingEdge" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Минимальная заправка', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'FuelingEdge'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.band" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.band" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Полоса осреднения', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'band'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelingDischarge" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelingDischarge" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Минимальный слив топлива', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'FuelingDischarge'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.accelMax" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.accelMax" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Максимальное ускорение', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'accelMax'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.breakMax" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.breakMax" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Максимальное торможение', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'breakMax'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.speedMax" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.speedMax" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Максимальная скорость', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'speedMax'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.idleRunningMain" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.idleRunningMain" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Холостой ход основного двигателя', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'idleRunningMain'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.idleRunningAdd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.idleRunningAdd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Холосой ход доп. двигателя', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'idleRunningAdd'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Название', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'Name'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.[Desc]" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.[Desc]" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Описание ', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'Desc'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.AvgFuelRatePerHour" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.AvgFuelRatePerHour" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Средний расход топлива в час', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'AvgFuelRatePerHour'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelApproximationTime" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelApproximationTime" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Временной интервал перед стоянкой, по точка которого уточняется уровень топлива в начале стоянки', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'FuelApproximationTime'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.TimeGetFuelAfterStop" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.TimeGetFuelAfterStop" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Временной интервал между началом стоянки и замером уровня топлива', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'TimeGetFuelAfterStop'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.TimeGetFuelBeforeMotion" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.TimeGetFuelBeforeMotion" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Временной интервал между замером уровня топлива и началом движения', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'TimeGetFuelBeforeMotion'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.CzMinCrossingPairTime" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.CzMinCrossingPairTime" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Min временной интервал между двумя пересечениями КЗ', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'CzMinCrossingPairTime'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelerRadiusFinds" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelerRadiusFinds" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Радиус поиска заправляемых ТС вокруг топливозаправщика', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'FuelerRadiusFinds'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelerMaxTimeStop" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelerMaxTimeStop" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Минимальное время до заправки следующего ТС', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'FuelerMaxTimeStop'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelerMinFuelrate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelerMinFuelrate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Минимальная величина заправки, что будет попадать в отчет', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'FuelerMinFuelrate'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelerCountInMotion" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelerCountInMotion" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Считать ли заправки, при не нулевой скорости', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'FuelerCountInMotion'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelrateWithDischarge" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelrateWithDischarge" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Считать ли в расходе сливы', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'FuelrateWithDischarge'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelingMinMaxAlgorithm" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.FuelingMinMaxAlgorithm" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Считать заправки по MAX-MIN', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'FuelingMinMaxAlgorithm'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.TimeBreakMaxPermitted" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.TimeBreakMaxPermitted" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Макс. время разрешенной стоянки', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'TimeBreakMaxPermitted'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.InclinometerMaxAngleAxisX" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.InclinometerMaxAngleAxisX" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Предельный угол наклона относительно продольной оси Х ', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'InclinometerMaxAngleAxisX'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.setting.InclinometerMaxAngleAxisY" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.setting.InclinometerMaxAngleAxisY" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Предельный угол наклона относительно продольной оси Y ', 'SCHEMA', N'dbo', 'TABLE', N'setting', 'COLUMN', N'InclinometerMaxAngleAxisY'
GO

--
-- Создать таблицу "dbo.servicetypes"
--
PRINT (N'Создать таблицу "dbo.servicetypes"')
GO
CREATE TABLE dbo.servicetypes (
  ServiceType_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  Flags int NULL DEFAULT (0),
  D1 char(200) NULL,
  D2 char(200) NULL,
  D3 char(200) NULL,
  D4 char(200) NULL,
  D5 char(200) NULL,
  D6 char(200) NULL,
  D7 char(200) NULL,
  D8 char(200) NULL,
  D9 char(200) NULL,
  D10 char(200) NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.servicesend"
--
PRINT (N'Создать таблицу "dbo.servicesend"')
GO
CREATE TABLE dbo.servicesend (
  ServiceSend_ID int IDENTITY,
  Name varchar(50) NULL,
  Descr varchar(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  telMobitel varchar(64) NULL,
  smsMobitel varchar(64) NULL,
  emailMobitel varchar(64) NULL,
  IPMobitel varchar(64) NULL,
  URLMobitel varchar(64) NULL,
  PortMobitel int NULL DEFAULT (0),
  RfMobitel varchar(50) NULL,
  Message_ID int NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.servicesend"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.servicesend"')
GO
CREATE INDEX Index_2
  ON dbo.servicesend (ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.serviceinit"
--
PRINT (N'Создать таблицу "dbo.serviceinit"')
GO
CREATE TABLE dbo.serviceinit (
  ServiceInit_ID int IDENTITY,
  Name varchar(max) NULL,
  Descr varchar(max) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  ServiceType_ID int NULL DEFAULT (0),
  A1 varchar(max) NULL,
  A2 varchar(max) NULL,
  A3 varchar(max) NULL,
  A4 varchar(max) NULL,
  A5 varchar(max) NULL,
  A6 varchar(max) NULL,
  A7 varchar(max) NULL,
  A8 varchar(max) NULL,
  A9 varchar(max) NULL,
  A10 varchar(max) NULL,
  CAtoSAint int NULL DEFAULT (0),
  CAtoSAtime int NULL DEFAULT (0),
  SAtoCAint int NULL DEFAULT (0),
  SAtoCAtime int NULL DEFAULT (0),
  SAtoCAtext char(50) NULL DEFAULT ('STOPPED'),
  CAtoSAtext char(50) NULL DEFAULT ('STOPPED'),
  MobitelActive_ID int NOT NULL DEFAULT (0),
  Active tinyint NOT NULL DEFAULT (0)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.sensors"
--
PRINT (N'Создать таблицу "dbo.sensors"')
GO
CREATE TABLE dbo.sensors (
  id int IDENTITY,
  mobitel_id int NOT NULL,
  Name varchar(45) NOT NULL CONSTRAINT DF__sensors__Name__6FD49106 DEFAULT (''),
  Description varchar(200) NOT NULL CONSTRAINT DF__sensors__Descrip__70C8B53F DEFAULT (''),
  StartBit int NOT NULL,
  Length int NOT NULL,
  NameUnit varchar(45) NOT NULL CONSTRAINT DF__sensors__NameUni__71BCD978 DEFAULT (''),
  K float NOT NULL CONSTRAINT DF__sensors__K__72B0FDB1 DEFAULT (0.00000),
  CONSTRAINT PK_sensors PRIMARY KEY CLUSTERED (id)
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.sensors"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.sensors"')
GO
CREATE INDEX Index_2
  ON dbo.sensors (mobitel_id)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.state"
--
PRINT (N'Создать таблицу "dbo.state"')
GO
CREATE TABLE dbo.state (
  Id int IDENTITY,
  MinValue float NOT NULL,
  MaxValue float NOT NULL,
  Title varchar(50) NOT NULL DEFAULT (''),
  SensorId int NULL,
  CONSTRAINT fgn_key_State_FK FOREIGN KEY (SensorId) REFERENCES dbo.sensors (id)
)
ON [PRIMARY]
GO

--
-- Создать индекс "State_FK" для объекта типа таблица "dbo.state"
--
PRINT (N'Создать индекс "State_FK" для объекта типа таблица "dbo.state"')
GO
CREATE INDEX State_FK
  ON dbo.state (SensorId)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.sensoralgorithms"
--
PRINT (N'Создать таблицу "dbo.sensoralgorithms"')
GO
CREATE TABLE dbo.sensoralgorithms (
  ID int IDENTITY,
  Name char(45) NOT NULL DEFAULT (''),
  Description char(45) NULL,
  AlgorithmID int NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "ID" для объекта типа таблица "dbo.sensoralgorithms"
--
PRINT (N'Создать индекс "ID" для объекта типа таблица "dbo.sensoralgorithms"')
GO
CREATE UNIQUE INDEX ID
  ON dbo.sensoralgorithms (ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.sensor_list"
--
PRINT (N'Создать таблицу "dbo.sensor_list"')
GO
CREATE TABLE dbo.sensor_list (
  ID int IDENTITY,
  Title varchar(70) NULL,
  Kind varchar(20) NULL,
  BitsCount int NULL,
  FirstBit int NULL,
  Data varchar(max) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.rules"
--
PRINT (N'Создать таблицу "dbo.rules"')
GO
CREATE TABLE dbo.rules (
  Rule_ID int IDENTITY,
  Name char(50) NULL,
  Password char(50) NULL,
  Descr char(200) NULL,
  ID int NOT NULL DEFAULT (0),
  Rule1 smallint NOT NULL DEFAULT (0),
  Rule2 smallint NOT NULL DEFAULT (0),
  Rule3 smallint NOT NULL DEFAULT (0),
  Rule4 smallint NOT NULL DEFAULT (0),
  Rule5 smallint NOT NULL DEFAULT (0),
  Rule6 smallint NOT NULL DEFAULT (0),
  Rule7 smallint NOT NULL DEFAULT (0),
  Rule8 smallint NOT NULL DEFAULT (0),
  Rule9 smallint NOT NULL DEFAULT (0),
  Rule10 smallint NOT NULL DEFAULT (0),
  Rule11 smallint NOT NULL DEFAULT (0),
  Rule12 smallint NOT NULL DEFAULT (0),
  Rule13 smallint NOT NULL DEFAULT (0),
  Rule14 smallint NOT NULL DEFAULT (0),
  Rule15 smallint NOT NULL DEFAULT (0),
  Rule16 smallint NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.rt_shedule_dc"
--
PRINT (N'Создать таблицу "dbo.rt_shedule_dc"')
GO
CREATE TABLE dbo.rt_shedule_dc (
  Id int IDENTITY,
  Id_zone_main int NOT NULL,
  Remark nvarchar(127) NULL,
  Name nvarchar(127) NOT NULL,
  CONSTRAINT PK_rt_shedule_dc PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.rt_shedulet_dc"
--
PRINT (N'Создать таблицу "dbo.rt_shedulet_dc"')
GO
CREATE TABLE dbo.rt_shedulet_dc (
  Id int IDENTITY,
  Id_main int NOT NULL,
  Id_zone_to int NOT NULL,
  Distance float NOT NULL CONSTRAINT DF_rt_shedulet_dc_Distance DEFAULT (0),
  TimePlanStart char(5) NOT NULL,
  TimePlanEnd char(5) NULL,
  CONSTRAINT PK_rt_shedulet_dc PRIMARY KEY CLUSTERED (Id),
  CONSTRAINT FK_rt_shedulet_dc_rt_shedule_dc FOREIGN KEY (Id_main) REFERENCES dbo.rt_shedule_dc (Id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_rt_shedulet_dc_zones FOREIGN KEY (Id_zone_to) REFERENCES dbo.zones (Zone_ID)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.rt_shedule"
--
PRINT (N'Создать таблицу "dbo.rt_shedule"')
GO
CREATE TABLE dbo.rt_shedule (
  Id int IDENTITY,
  Id_shedule_dc int NULL,
  Id_zone_main int NULL,
  DateShedule smalldatetime NOT NULL,
  Remark nvarchar(127) NULL,
  OutLinkId varchar(20) NULL,
  CONSTRAINT PK_rt_shedule PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.rt_shedulet"
--
PRINT (N'Создать таблицу "dbo.rt_shedulet"')
GO
CREATE TABLE dbo.rt_shedulet (
  Id int IDENTITY,
  Id_main int NOT NULL,
  Id_zone_to int NOT NULL,
  Id_vehicle int NULL,
  TimePlanStart smalldatetime NOT NULL,
  TimePlanEnd smalldatetime NULL,
  TimeFactStart smalldatetime NULL,
  TimeFactEnd smalldatetime NULL,
  State nvarchar(127) NULL,
  IsClose bit NOT NULL CONSTRAINT DF_rt_shedulet_IsClose DEFAULT (0),
  Distance float NOT NULL CONSTRAINT DF_rt_shedulet_Distance DEFAULT (0),
  CONSTRAINT PK_rt_shedulet PRIMARY KEY CLUSTERED (Id),
  CONSTRAINT FK_rt_shedulet_rt_shedule FOREIGN KEY (Id_main) REFERENCES dbo.rt_shedule (Id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_rt_shedulet_zones FOREIGN KEY (Id_zone_to) REFERENCES dbo.zones (Zone_ID)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.rt_samplet"
--
PRINT (N'Создать таблицу "dbo.rt_samplet"')
GO
CREATE TABLE dbo.rt_samplet (
  Id int IDENTITY,
  Id_main int NOT NULL,
  Position smallint NOT NULL,
  Id_event int NOT NULL,
  Id_zone int NOT NULL,
  TimeRel char(5) NULL CONSTRAINT DF__rt_sample__TimeR__515009E6 DEFAULT ('00:00'),
  TimeRelP char(5) NULL CONSTRAINT DF__rt_sample__TimeR__52442E1F DEFAULT ('00:00'),
  TimeRelM char(5) NULL CONSTRAINT DF__rt_sample__TimeR__53385258 DEFAULT ('00:00'),
  TimeAbs char(5) NULL CONSTRAINT DF__rt_sample__TimeA__542C7691 DEFAULT ('00:00'),
  CONSTRAINT PK_rt_samplet PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.Position" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.Position" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Порядковый номер записи шаблона маршрута', 'SCHEMA', N'dbo', 'TABLE', N'rt_samplet', 'COLUMN', N'Position'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.Id_event" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.Id_event" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Событие - въезд / выезд ', 'SCHEMA', N'dbo', 'TABLE', N'rt_samplet', 'COLUMN', N'Id_event'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.Id_zone" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.Id_zone" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Контрольная зона', 'SCHEMA', N'dbo', 'TABLE', N'rt_samplet', 'COLUMN', N'Id_zone'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.TimeRel" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.TimeRel" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Относительное время от начала маршрута', 'SCHEMA', N'dbo', 'TABLE', N'rt_samplet', 'COLUMN', N'TimeRel'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.TimeRelP" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.TimeRelP" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Допустимое отклонение (+)', 'SCHEMA', N'dbo', 'TABLE', N'rt_samplet', 'COLUMN', N'TimeRelP'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.TimeRelM" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.TimeRelM" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Допустимое отклонение (-)', 'SCHEMA', N'dbo', 'TABLE', N'rt_samplet', 'COLUMN', N'TimeRelM'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.TimeAbs" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_samplet.TimeAbs" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Абсолютное время  маршрута', 'SCHEMA', N'dbo', 'TABLE', N'rt_samplet', 'COLUMN', N'TimeAbs'
GO

--
-- Создать таблицу "dbo.rt_sample"
--
PRINT (N'Создать таблицу "dbo.rt_sample"')
GO
CREATE TABLE dbo.rt_sample (
  Id int IDENTITY,
  Name varchar(100) NOT NULL DEFAULT (''),
  Id_mobitel int NULL DEFAULT (0),
  TimeStart time NULL,
  Distance float NULL DEFAULT (0),
  Remark varchar(max) NULL,
  Id_main int NOT NULL DEFAULT (0),
  IsGroupe smallint NOT NULL DEFAULT (0),
  AutoCreate smallint NOT NULL DEFAULT (0),
  CONSTRAINT PK_rt_sample PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.Id_mobitel" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.Id_mobitel" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Транспортное средство (опц)', 'SCHEMA', N'dbo', 'TABLE', N'rt_sample', 'COLUMN', N'Id_mobitel'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.TimeStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.TimeStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Время начала маршрута (опц)', 'SCHEMA', N'dbo', 'TABLE', N'rt_sample', 'COLUMN', N'TimeStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.Distance" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.Distance" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Общий путь, км', 'SCHEMA', N'dbo', 'TABLE', N'rt_sample', 'COLUMN', N'Distance'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.Remark" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.Remark" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Примечание', 'SCHEMA', N'dbo', 'TABLE', N'rt_sample', 'COLUMN', N'Remark'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.Id_main" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.Id_main" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на элемент верхнего уровня в иерархии', 'SCHEMA', N'dbo', 'TABLE', N'rt_sample', 'COLUMN', N'Id_main'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.IsGroupe" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.IsGroupe" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Запись - группа', 'SCHEMA', N'dbo', 'TABLE', N'rt_sample', 'COLUMN', N'IsGroupe'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.AutoCreate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_sample.AutoCreate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Маршрут может автосоздаваться ', 'SCHEMA', N'dbo', 'TABLE', N'rt_sample', 'COLUMN', N'AutoCreate'
GO

--
-- Создать таблицу "dbo.rt_routet"
--
PRINT (N'Создать таблицу "dbo.rt_routet"')
GO
CREATE TABLE dbo.rt_routet (
  Id int IDENTITY,
  Id_main int NOT NULL,
  Id_zone int NOT NULL DEFAULT (0),
  Id_event int NOT NULL DEFAULT (1),
  DatePlan datetime NULL,
  DateFact datetime NULL,
  Deviation varchar(10) NULL,
  Remark varchar(max) NULL,
  Distance float NULL DEFAULT (0),
  Location varchar(255) NULL,
  CONSTRAINT PK_rt_routet PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Id_zone" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Id_zone" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Контрольная зона', 'SCHEMA', N'dbo', 'TABLE', N'rt_routet', 'COLUMN', N'Id_zone'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Id_event" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Id_event" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Событие - въезд / выезд ', 'SCHEMA', N'dbo', 'TABLE', N'rt_routet', 'COLUMN', N'Id_event'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.DatePlan" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.DatePlan" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'План время события ', 'SCHEMA', N'dbo', 'TABLE', N'rt_routet', 'COLUMN', N'DatePlan'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.DateFact" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.DateFact" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Факт время события ', 'SCHEMA', N'dbo', 'TABLE', N'rt_routet', 'COLUMN', N'DateFact'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Deviation" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Deviation" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Отклонение от графика', 'SCHEMA', N'dbo', 'TABLE', N'rt_routet', 'COLUMN', N'Deviation'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Remark" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Remark" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Примечание', 'SCHEMA', N'dbo', 'TABLE', N'rt_routet', 'COLUMN', N'Remark'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Distance" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Distance" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Путь между точками маршрута', 'SCHEMA', N'dbo', 'TABLE', N'rt_routet', 'COLUMN', N'Distance'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Location" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_routet.Location" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Населенный пункт', 'SCHEMA', N'dbo', 'TABLE', N'rt_routet', 'COLUMN', N'Location'
GO

--
-- Создать таблицу "dbo.rt_route_type"
--
PRINT (N'Создать таблицу "dbo.rt_route_type"')
GO
CREATE TABLE dbo.rt_route_type (
  Id int IDENTITY,
  Name varchar(50) NOT NULL DEFAULT (''),
  CONSTRAINT PK_rt_route_type PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route_type.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route_type.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип маршрута', 'SCHEMA', N'dbo', 'TABLE', N'rt_route_type', 'COLUMN', N'Name'
GO

--
-- Создать таблицу "dbo.rt_route_stops"
--
PRINT (N'Создать таблицу "dbo.rt_route_stops"')
GO
CREATE TABLE dbo.rt_route_stops (
  Id int IDENTITY,
  Id_main int NOT NULL,
  Location varchar(255) NULL,
  InitialTime datetime NULL,
  FinalTime datetime NULL,
  Interval time NULL,
  CheckZone smallint NOT NULL DEFAULT (0),
  CONSTRAINT PK_rt_route_stops PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route_stops.CheckZone" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route_stops.CheckZone" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Остановка в контрольной зоне', 'SCHEMA', N'dbo', 'TABLE', N'rt_route_stops', 'COLUMN', N'CheckZone'
GO

--
-- Создать таблицу "dbo.rt_route_sensors"
--
PRINT (N'Создать таблицу "dbo.rt_route_sensors"')
GO
CREATE TABLE dbo.rt_route_sensors (
  Id int IDENTITY,
  Id_main int NOT NULL,
  SensorName varchar(127) NOT NULL DEFAULT (''),
  Location varchar(255) NOT NULL DEFAULT (''),
  EventTime datetime NOT NULL,
  Duration time NOT NULL,
  Description varchar(255) NULL,
  Speed float NULL DEFAULT (0),
  Distance float NULL DEFAULT (0.00),
  CONSTRAINT PK_rt_route_sensors PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route_sensors.Distance" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route_sensors.Distance" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Путь, км', 'SCHEMA', N'dbo', 'TABLE', N'rt_route_sensors', 'COLUMN', N'Distance'
GO

--
-- Создать таблицу "dbo.rt_route_fuel"
--
PRINT (N'Создать таблицу "dbo.rt_route_fuel"')
GO
CREATE TABLE dbo.rt_route_fuel (
  Id int IDENTITY,
  Id_main int NOT NULL,
  Location varchar(255) NOT NULL DEFAULT (''),
  time_ datetime NOT NULL,
  dValueCalc float NOT NULL DEFAULT (0),
  dValueHandle float NULL DEFAULT (0),
  CONSTRAINT PK_rt_route_fuel PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route_fuel.dValueCalc" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route_fuel.dValueCalc" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Расчетная величина', 'SCHEMA', N'dbo', 'TABLE', N'rt_route_fuel', 'COLUMN', N'dValueCalc'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route_fuel.dValueHandle" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route_fuel.dValueHandle" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ручная правка', 'SCHEMA', N'dbo', 'TABLE', N'rt_route_fuel', 'COLUMN', N'dValueHandle'
GO

--
-- Создать таблицу "dbo.rt_route"
--
PRINT (N'Создать таблицу "dbo.rt_route"')
GO
CREATE TABLE dbo.rt_route (
  Id int IDENTITY,
  Id_sample int NOT NULL,
  Id_mobitel int NOT NULL DEFAULT (0),
  Id_driver int NOT NULL DEFAULT (0),
  TimeStartPlan datetime NULL,
  Distance float NOT NULL DEFAULT (0),
  TimePlanTotal varchar(20) NULL,
  TimeFactTotal varchar(20) NULL,
  Deviation varchar(20) NULL,
  DeviationAr varchar(20) NULL,
  Remark varchar(max) NULL,
  FuelStart float NOT NULL DEFAULT (0.00),
  FuelAdd float NOT NULL DEFAULT (0.00),
  FuelSub float NOT NULL DEFAULT (0.00),
  FuelEnd float NOT NULL DEFAULT (0.00),
  FuelExpens float NOT NULL DEFAULT (0.00),
  FuelExpensAvg float NOT NULL DEFAULT (0.00),
  Fuel_ExpensMove float NOT NULL DEFAULT (0.00),
  Fuel_ExpensStop float NOT NULL DEFAULT (0.00),
  Fuel_ExpensTotal float NOT NULL DEFAULT (0.00),
  Fuel_ExpensAvg float NOT NULL DEFAULT (0.00),
  TimeMove varchar(20) NULL,
  TimeStop varchar(20) NULL,
  SpeedAvg float NULL DEFAULT (0.00),
  FuelAddQty int NULL DEFAULT (0),
  FuelSubQty int NULL DEFAULT (0),
  PointsValidity int NULL DEFAULT (0),
  PointsCalc int NULL DEFAULT (0),
  PointsFact int NULL DEFAULT (0),
  PointsIntervalMax varchar(20) NULL,
  Type smallint NOT NULL DEFAULT (1),
  TimeEndFact datetime NULL,
  DistLogicSensor float NULL DEFAULT (0.00),
  IsClose bit NOT NULL CONSTRAINT DF_rt_route_IsClose DEFAULT (0),
  CONSTRAINT PK_rt_route PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Id_sample" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Id_sample" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на шаблон маршрута ', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'Id_sample'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Id_mobitel" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Id_mobitel" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Транспортное средство ', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'Id_mobitel'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Id_driver" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Id_driver" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Водитель', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'Id_driver'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimeStartPlan" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimeStartPlan" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'План время начала маршрута', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'TimeStartPlan'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Distance" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Distance" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Общий путь, км', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'Distance'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimePlanTotal" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimePlanTotal" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Общее время на маршруте (план)', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'TimePlanTotal'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimeFactTotal" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimeFactTotal" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Общее время на маршруте (факт)', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'TimeFactTotal'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Deviation" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Deviation" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Отклонение от графика', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'Deviation'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.DeviationAr" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.DeviationAr" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Отклонение по времени прибытия', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'DeviationAr'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Remark" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Remark" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Примечание', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'Remark'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Топливо в начале', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'FuelStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelAdd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelAdd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Заправлено', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'FuelAdd'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelSub" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelSub" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Слито', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'FuelSub'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelEnd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelEnd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Топливо в конце', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'FuelEnd'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelExpens" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelExpens" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Общий расход топлива', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'FuelExpens'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelExpensAvg" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelExpensAvg" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Средний расход,  л/100 км', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'FuelExpensAvg'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Fuel_ExpensMove" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Fuel_ExpensMove" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДРТ/CAN Расход топлива в движении, л', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'Fuel_ExpensMove'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Fuel_ExpensStop" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Fuel_ExpensStop" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДРТ/CAN Расход топлива на стоянках, л', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'Fuel_ExpensStop'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Fuel_ExpensTotal" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Fuel_ExpensTotal" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДРТ/CAN Общий расход, л', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'Fuel_ExpensTotal'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Fuel_ExpensAvg" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Fuel_ExpensAvg" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДРТ/CAN Средний расход, л/100 км', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'Fuel_ExpensAvg'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimeMove" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimeMove" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Время движения', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'TimeMove'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimeStop" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimeStop" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Время остановок', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'TimeStop'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.SpeedAvg" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.SpeedAvg" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Средняя скорость', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'SpeedAvg'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelAddQty" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelAddQty" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Заправлено - кол-во', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'FuelAddQty'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelSubQty" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.FuelSubQty" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Слито - кол-во', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'FuelSubQty'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.PointsValidity" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.PointsValidity" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Валидность данных', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'PointsValidity'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.PointsCalc" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.PointsCalc" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Количество точек (по Log_ID) расчетное', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'PointsCalc'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.PointsFact" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.PointsFact" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Количество точек (по Log_ID) фактическое', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'PointsFact'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.PointsIntervalMax" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.PointsIntervalMax" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Максимальный интервал между точками', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'PointsIntervalMax'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Type" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.Type" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип маршрутного листа - КЗ или населенные пункты', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'Type'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimeEndFact" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.TimeEndFact" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Фактическое время окончания маршрута', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'TimeEndFact'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.rt_route.DistLogicSensor" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.rt_route.DistLogicSensor" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Путь с включенным логическим датчиком, км', 'SCHEMA', N'dbo', 'TABLE', N'rt_route', 'COLUMN', N'DistLogicSensor'
GO

--
-- Создать таблицу "dbo.rt_events"
--
PRINT (N'Создать таблицу "dbo.rt_events"')
GO
CREATE TABLE dbo.rt_events (
  Id int IDENTITY,
  Name varchar(50) NOT NULL DEFAULT (''),
  CONSTRAINT PK_rt_events PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.route_mobitel_plans"
--
PRINT (N'Создать таблицу "dbo.route_mobitel_plans"')
GO
CREATE TABLE dbo.route_mobitel_plans (
  ID int IDENTITY,
  Title varchar(70) NULL,
  Time1 int NULL,
  Time2 int NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.route_lists"
--
PRINT (N'Создать таблицу "dbo.route_lists"')
GO
CREATE TABLE dbo.route_lists (
  ID int IDENTITY,
  Parent int NULL,
  Title varchar(70) NULL,
  Data varchar(max) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.route_list"
--
PRINT (N'Создать таблицу "dbo.route_list"')
GO
CREATE TABLE dbo.route_list (
  Route_ID int IDENTITY,
  GIS varbinary(max) NULL,
  Name varchar(50) NULL,
  Descr varchar(200) NULL,
  Modified int NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.roles"
--
PRINT (N'Создать таблицу "dbo.roles"')
GO
CREATE TABLE dbo.roles (
  RoleID tinyint IDENTITY (13, 1),
  RoleName nvarchar(25) NOT NULL,
  RoleDescription nvarchar(250) NOT NULL,
  CONSTRAINT PK_roles_RoleID PRIMARY KEY CLUSTERED (RoleID)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.roles" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.roles" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.roles', 'SCHEMA', N'dbo', 'TABLE', N'roles'
GO

--
-- Создать таблицу "dbo.righttype"
--
PRINT (N'Создать таблицу "dbo.righttype"')
GO
CREATE TABLE dbo.righttype (
  ID int IDENTITY (3, 1),
  RightName nvarchar(30) NOT NULL,
  RightDescription nvarchar(250) NOT NULL,
  CONSTRAINT PK_righttype_ID PRIMARY KEY CLUSTERED (ID)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.righttype" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.righttype" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.righttype', 'SCHEMA', N'dbo', 'TABLE', N'righttype'
GO

--
-- Создать таблицу "dbo.rightschemainusers"
--
PRINT (N'Создать таблицу "dbo.rightschemainusers"')
GO
CREATE TABLE dbo.rightschemainusers (
  SysSchemaID int NOT NULL,
  SysModuleID int NOT NULL,
  UserID int NOT NULL,
  IsVisible nvarchar(3) NOT NULL DEFAULT (N'yes')
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.rightschemainusers" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.rightschemainusers" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.rightschemainusers', 'SCHEMA', N'dbo', 'TABLE', N'rightschemainusers'
GO

--
-- Создать таблицу "dbo.rightschemainroles"
--
PRINT (N'Создать таблицу "dbo.rightschemainroles"')
GO
CREATE TABLE dbo.rightschemainroles (
  SysSchemaID int NOT NULL,
  SysModuleID int NOT NULL,
  RoleID int NOT NULL,
  IsVisible nvarchar(3) NOT NULL DEFAULT (N'yes')
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.rightschemainroles" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.rightschemainroles" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.rightschemainroles', 'SCHEMA', N'dbo', 'TABLE', N'rightschemainroles'
GO

--
-- Создать таблицу "dbo.rightmodulesinusers"
--
PRINT (N'Создать таблицу "dbo.rightmodulesinusers"')
GO
CREATE TABLE dbo.rightmodulesinusers (
  UserID int NOT NULL,
  SysModuleID int NOT NULL,
  IsVisible nvarchar(3) NOT NULL DEFAULT (N'yes')
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.rightmodulesinusers" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.rightmodulesinusers" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.rightmodulesinusers', 'SCHEMA', N'dbo', 'TABLE', N'rightmodulesinusers'
GO

--
-- Создать таблицу "dbo.rightmodulesinroles"
--
PRINT (N'Создать таблицу "dbo.rightmodulesinroles"')
GO
CREATE TABLE dbo.rightmodulesinroles (
  SysModuleID int NOT NULL,
  RoleID int NOT NULL,
  IsVisible nvarchar(3) NOT NULL DEFAULT (N'yes')
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.rightmodulesinroles" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.rightmodulesinroles" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.rightmodulesinroles', 'SCHEMA', N'dbo', 'TABLE', N'rightmodulesinroles'
GO

--
-- Создать таблицу "dbo.pcbversion"
--
PRINT (N'Создать таблицу "dbo.pcbversion"')
GO
CREATE TABLE dbo.pcbversion (
  Id bigint IDENTITY,
  Name varchar(50) NOT NULL DEFAULT ('*'),
  Comment varchar(255) NULL DEFAULT (' '),
  MobitelId int NULL DEFAULT (-1)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.pcbversion.Id" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.pcbversion.Id" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Индекс', 'SCHEMA', N'dbo', 'TABLE', N'pcbversion', 'COLUMN', N'Id'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.pcbversion.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.pcbversion.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя платы датчика', 'SCHEMA', N'dbo', 'TABLE', N'pcbversion', 'COLUMN', N'Name'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.pcbversion.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.pcbversion.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'pcbversion', 'COLUMN', N'Comment'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.pcbversion.MobitelId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.pcbversion.MobitelId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ID машины на которую назначена эта плата датчиков', 'SCHEMA', N'dbo', 'TABLE', N'pcbversion', 'COLUMN', N'MobitelId'
GO

--
-- Создать таблицу "dbo.pcbdata"
--
PRINT (N'Создать таблицу "dbo.pcbdata"')
GO
CREATE TABLE dbo.pcbdata (
  Id bigint IDENTITY,
  Typedata varchar(255) NOT NULL DEFAULT (' '),
  koeffK float NOT NULL DEFAULT (0),
  Startbit int NOT NULL DEFAULT (0),
  Lengthbit int NOT NULL DEFAULT (0),
  Comment varchar(255) NULL DEFAULT (' '),
  Idpcb int NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Id" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Id" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Индекс', 'SCHEMA', N'dbo', 'TABLE', N'pcbdata', 'COLUMN', N'Id'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Typedata" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Typedata" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип данных', 'SCHEMA', N'dbo', 'TABLE', N'pcbdata', 'COLUMN', N'Typedata'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.koeffK" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.koeffK" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Коеффициент К', 'SCHEMA', N'dbo', 'TABLE', N'pcbdata', 'COLUMN', N'koeffK'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Startbit" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Startbit" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Стартовый бит поля', 'SCHEMA', N'dbo', 'TABLE', N'pcbdata', 'COLUMN', N'Startbit'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Lengthbit" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Lengthbit" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Длина поля', 'SCHEMA', N'dbo', 'TABLE', N'pcbdata', 'COLUMN', N'Lengthbit'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Коментарий', 'SCHEMA', N'dbo', 'TABLE', N'pcbdata', 'COLUMN', N'Comment'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Idpcb" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.pcbdata.Idpcb" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Индекс платы датчика', 'SCHEMA', N'dbo', 'TABLE', N'pcbdata', 'COLUMN', N'Idpcb'
GO

--
-- Создать таблицу "dbo.parameters"
--
PRINT (N'Создать таблицу "dbo.parameters"')
GO
CREATE TABLE dbo.parameters (
  Id int IDENTITY,
  Number int NOT NULL,
  Value varchar(255) NULL
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.parameters.Number" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.parameters.Number" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер параметра', 'SCHEMA', N'dbo', 'TABLE', N'parameters', 'COLUMN', N'Number'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.parameters.Value" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.parameters.Value" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение', 'SCHEMA', N'dbo', 'TABLE', N'parameters', 'COLUMN', N'Value'
GO

--
-- Создать таблицу "dbo.online64"
--
PRINT (N'Создать таблицу "dbo.online64"')
GO
CREATE TABLE dbo.online64 (
  DataGps_ID bigint IDENTITY,
  Mobitel_ID int NOT NULL,
  Latitude int NOT NULL,
  Longitude int NOT NULL,
  Direction tinyint NULL,
  Acceleration tinyint NULL,
  UnixTime int NOT NULL,
  Speed smallint NOT NULL,
  Valid bit NOT NULL,
  Satellites smallint NULL,
  RssiGsm smallint NULL,
  Events int NULL,
  SensorsSet smallint NULL,
  Sensors varchar(50) NULL,
  Voltage smallint NULL,
  DGPS varchar(24) NULL,
  LogID int NULL,
  SrvPacketID bigint NULL,
  CONSTRAINT PK_online64 PRIMARY KEY CLUSTERED (DataGps_ID)
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_Mobitelid64" для объекта типа таблица "dbo.online64"
--
PRINT (N'Создать индекс "IDX_Mobitelid64" для объекта типа таблица "dbo.online64"')
GO
CREATE INDEX IDX_Mobitelid64
  ON dbo.online64 (Mobitel_ID)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidLogid64" для объекта типа таблица "dbo.online64"
--
PRINT (N'Создать индекс "IDX_MobitelidLogid64" для объекта типа таблица "dbo.online64"')
GO
CREATE INDEX IDX_MobitelidLogid64
  ON dbo.online64 (Mobitel_ID, LogID)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidUnixtime64" для объекта типа таблица "dbo.online64"
--
PRINT (N'Создать индекс "IDX_MobitelidUnixtime64" для объекта типа таблица "dbo.online64"')
GO
CREATE INDEX IDX_MobitelidUnixtime64
  ON dbo.online64 (Mobitel_ID, UnixTime)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidUnixtimeValid64" для объекта типа таблица "dbo.online64"
--
PRINT (N'Создать индекс "IDX_MobitelidUnixtimeValid64" для объекта типа таблица "dbo.online64"')
GO
CREATE INDEX IDX_MobitelidUnixtimeValid64
  ON dbo.online64 (Mobitel_ID, UnixTime, Valid)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.ntf_main"
--
PRINT (N'Создать таблицу "dbo.ntf_main"')
GO
CREATE TABLE dbo.ntf_main (
  Id int IDENTITY,
  IsActive smallint NOT NULL DEFAULT (0),
  IsPopup smallint NOT NULL DEFAULT (0),
  Title varchar(255) NOT NULL DEFAULT (''),
  IconSmall varbinary(max) NULL,
  IconLarge varbinary(max) NULL,
  SoundName varchar(50) NULL,
  TimeForControl int NULL DEFAULT (0),
  IsEmailActive smallint NOT NULL DEFAULT (0),
  Period int NOT NULL DEFAULT (0),
  CONSTRAINT PK_ntf_main PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.ntf_main.IsEmailActive" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.ntf_main.IsEmailActive" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Активна E-mail рассылка уведомления', 'SCHEMA', N'dbo', 'TABLE', N'ntf_main', 'COLUMN', N'IsEmailActive'
GO

--
-- Создать таблицу "dbo.ntf_popupusers"
--
PRINT (N'Создать таблицу "dbo.ntf_popupusers"')
GO
CREATE TABLE dbo.ntf_popupusers (
  Id int IDENTITY,
  Id_main int NOT NULL,
  Id_user int NOT NULL,
  CONSTRAINT fgn_key_FK_ntf_popupUsers_ntf_main_Id FOREIGN KEY (Id_main) REFERENCES dbo.ntf_main (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "FK_ntf_popupUsers_ntf_main_Id" для объекта типа таблица "dbo.ntf_popupusers"
--
PRINT (N'Создать индекс "FK_ntf_popupUsers_ntf_main_Id" для объекта типа таблица "dbo.ntf_popupusers"')
GO
CREATE INDEX FK_ntf_popupUsers_ntf_main_Id
  ON dbo.ntf_popupusers (Id_main)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.ntf_mobitels"
--
PRINT (N'Создать таблицу "dbo.ntf_mobitels"')
GO
CREATE TABLE dbo.ntf_mobitels (
  Id int IDENTITY,
  Id_main int NOT NULL,
  MobitelId int NOT NULL,
  SensorLogicId int NULL DEFAULT (0),
  SensorId int NULL DEFAULT (0),
  CONSTRAINT fgn_key_FK_ntf_mobitels_ntf_main_Id FOREIGN KEY (Id_main) REFERENCES dbo.ntf_main (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "FK_ntf_mobitels_ntf_main_Id" для объекта типа таблица "dbo.ntf_mobitels"
--
PRINT (N'Создать индекс "FK_ntf_mobitels_ntf_main_Id" для объекта типа таблица "dbo.ntf_mobitels"')
GO
CREATE INDEX FK_ntf_mobitels_ntf_main_Id
  ON dbo.ntf_mobitels (Id_main)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.ntf_log"
--
PRINT (N'Создать таблицу "dbo.ntf_log"')
GO
CREATE TABLE dbo.ntf_log (
  Id int IDENTITY,
  Id_main int NOT NULL,
  DateEvent datetime NOT NULL,
  DateWrite datetime NOT NULL,
  TypeEvent smallint NOT NULL,
  Mobitel_id int NOT NULL DEFAULT (0),
  Zone_id int NOT NULL DEFAULT (0),
  Location varchar(255) NULL,
  Lat float NULL DEFAULT (0),
  Lng float NULL DEFAULT (0),
  Infor varchar(255) NULL,
  IsRead bit NULL DEFAULT (0),
  DataGps_ID int NULL DEFAULT (0),
  Speed float NULL DEFAULT (0),
  Value float NULL DEFAULT (0),
  CONSTRAINT fgn_key_FK_ntf_log_ntf_main_Id FOREIGN KEY (Id_main) REFERENCES dbo.ntf_main (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "FK_ntf_log_ntf_main_Id" для объекта типа таблица "dbo.ntf_log"
--
PRINT (N'Создать индекс "FK_ntf_log_ntf_main_Id" для объекта типа таблица "dbo.ntf_log"')
GO
CREATE INDEX FK_ntf_log_ntf_main_Id
  ON dbo.ntf_log (Id_main)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.ntf_log.IsRead" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.ntf_log.IsRead" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Уведомление прочитано ', 'SCHEMA', N'dbo', 'TABLE', N'ntf_log', 'COLUMN', N'IsRead'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.ntf_log.Value" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.ntf_log.Value" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Цифровое значение события (датчики)', 'SCHEMA', N'dbo', 'TABLE', N'ntf_log', 'COLUMN', N'Value'
GO

--
-- Создать таблицу "dbo.ntf_events"
--
PRINT (N'Создать таблицу "dbo.ntf_events"')
GO
CREATE TABLE dbo.ntf_events (
  Id int IDENTITY,
  Id_main int NOT NULL,
  TypeEvent smallint NOT NULL,
  EventId int NOT NULL DEFAULT (0),
  ParInt1 int NOT NULL DEFAULT (0),
  ParDbl1 float NOT NULL DEFAULT (0),
  ParDbl2 float NOT NULL DEFAULT (0),
  ParBool bit NOT NULL DEFAULT (0),
  ParInt2 int NOT NULL DEFAULT (0),
  CONSTRAINT fgn_key_FK_ntf_events_ntf_main_Id FOREIGN KEY (Id_main) REFERENCES dbo.ntf_main (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "FK_ntf_events_ntf_main_Id" для объекта типа таблица "dbo.ntf_events"
--
PRINT (N'Создать индекс "FK_ntf_events_ntf_main_Id" для объекта типа таблица "dbo.ntf_events"')
GO
CREATE INDEX FK_ntf_events_ntf_main_Id
  ON dbo.ntf_events (Id_main)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.ntf_events.ParInt1" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.ntf_events.ParInt1" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'INT параметр любого события', 'SCHEMA', N'dbo', 'TABLE', N'ntf_events', 'COLUMN', N'ParInt1'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.ntf_events.ParDbl1" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.ntf_events.ParDbl1" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'DOUBLE параметр №1 любого события', 'SCHEMA', N'dbo', 'TABLE', N'ntf_events', 'COLUMN', N'ParDbl1'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.ntf_events.ParDbl2" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.ntf_events.ParDbl2" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'DOUBLE параметр №2 любого события', 'SCHEMA', N'dbo', 'TABLE', N'ntf_events', 'COLUMN', N'ParDbl2'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.ntf_events.ParBool" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.ntf_events.ParBool" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'BOOL параметр любого события', 'SCHEMA', N'dbo', 'TABLE', N'ntf_events', 'COLUMN', N'ParBool'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.ntf_events.ParInt2" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.ntf_events.ParInt2" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'INT параметр №2 любого события', 'SCHEMA', N'dbo', 'TABLE', N'ntf_events', 'COLUMN', N'ParInt2'
GO

--
-- Создать таблицу "dbo.ntf_emails"
--
PRINT (N'Создать таблицу "dbo.ntf_emails"')
GO
CREATE TABLE dbo.ntf_emails (
  Id int IDENTITY,
  Id_main int NOT NULL,
  IsActive smallint NOT NULL DEFAULT (0),
  Email varchar(127) NOT NULL DEFAULT (''),
  CONSTRAINT fgn_key_FK_ntf_email_ntf_main_Id FOREIGN KEY (Id_main) REFERENCES dbo.ntf_main (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "FK_ntf_email_ntf_main_Id" для объекта типа таблица "dbo.ntf_emails"
--
PRINT (N'Создать индекс "FK_ntf_email_ntf_main_Id" для объекта типа таблица "dbo.ntf_emails"')
GO
CREATE INDEX FK_ntf_email_ntf_main_Id
  ON dbo.ntf_emails (Id_main)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.mobitels_info"
--
PRINT (N'Создать таблицу "dbo.mobitels_info"')
GO
CREATE TABLE dbo.mobitels_info (
  Mobitel_ID bigint NOT NULL,
  Map_Style_Car varchar(max) NULL,
  Track_Color bigint NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.mobitels"
--
PRINT (N'Создать таблицу "dbo.mobitels"')
GO
CREATE TABLE dbo.mobitels (
  Mobitel_ID int IDENTITY,
  Name varchar(max) NULL,
  Descr varchar(max) NULL,
  ID bigint NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  ServiceInit_ID int NULL DEFAULT (0),
  ServiceSend_ID int NULL DEFAULT (0),
  InternalMobitelConfig_ID int NULL DEFAULT (0),
  MapStyleLine_ID int NULL DEFAULT (0),
  MapStyleLastPoint_ID int NULL DEFAULT (0),
  MapStylePoint_ID int NULL DEFAULT (0),
  Route_ID int NULL DEFAULT (0),
  DeviceType int NULL DEFAULT (0),
  DeviceEngine int NULL DEFAULT (0),
  ConfirmedID int NOT NULL DEFAULT (0),
  LastInsertTime int NOT NULL DEFAULT (0),
  Is64bitPackets bit NULL DEFAULT (0),
  IsNotDrawDgps bit NULL DEFAULT (0),
  CONSTRAINT PK_mobitels PRIMARY KEY CLUSTERED (Mobitel_ID)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "IDX_InternalmobitelconfigID" для объекта типа таблица "dbo.mobitels"
--
PRINT (N'Создать индекс "IDX_InternalmobitelconfigID" для объекта типа таблица "dbo.mobitels"')
GO
CREATE INDEX IDX_InternalmobitelconfigID
  ON dbo.mobitels (InternalMobitelConfig_ID)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.mobitels.ConfirmedID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.mobitels.ConfirmedID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'MIN значение LogID менее которого считается нет пропусков или уже невозможно загрузить', 'SCHEMA', N'dbo', 'TABLE', N'mobitels', 'COLUMN', N'ConfirmedID'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.mobitels.LastInsertTime" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.mobitels.LastInsertTime" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'UNIX_TIMESTAMP Время последней вставки данных в DataGPS', 'SCHEMA', N'dbo', 'TABLE', N'mobitels', 'COLUMN', N'LastInsertTime'
GO

--
-- Создать таблицу "dbo.vehicle"
--
PRINT (N'Создать таблицу "dbo.vehicle"')
GO
CREATE TABLE dbo.vehicle (
  id int IDENTITY,
  MakeCar varchar(max) NULL,
  NumberPlate varchar(max) NULL,
  Team_id int NULL,
  Mobitel_id int NULL,
  Odometr int NULL DEFAULT (0),
  CarModel varchar(max) NULL,
  setting_id int NULL DEFAULT (1),
  odoTime datetime NULL,
  driver_id int NULL DEFAULT (-1),
  OutLinkId varchar(20) NULL,
  Category_id int NULL,
  FuelWayLiter float NULL DEFAULT (0),
  FuelMotorLiter float NULL DEFAULT (0),
  Identifier int NULL DEFAULT (0),
  Category_id2 int NULL,
  PcbVersionId int NULL DEFAULT (-1),
  Category_id3 int NULL DEFAULT (NULL),
  Category_id4 int NULL DEFAULT (NULL),
  CONSTRAINT fgn_key_FK_vehicle_mobitels_Mobitel_ID FOREIGN KEY (Mobitel_id) REFERENCES dbo.mobitels (Mobitel_ID),
  CONSTRAINT fgn_key_FK_vehicle_team_id FOREIGN KEY (Team_id) REFERENCES dbo.team (id),
  CONSTRAINT fgn_key_FK_vehicle_vehicle_category_Id FOREIGN KEY (Category_id) REFERENCES dbo.vehicle_category (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "FK_vehicle_mobitels_Mobitel_ID" для объекта типа таблица "dbo.vehicle"
--
PRINT (N'Создать индекс "FK_vehicle_mobitels_Mobitel_ID" для объекта типа таблица "dbo.vehicle"')
GO
CREATE INDEX FK_vehicle_mobitels_Mobitel_ID
  ON dbo.vehicle (Mobitel_id)
  ON [PRIMARY]
GO

--
-- Создать индекс "FK_vehicle_team_id" для объекта типа таблица "dbo.vehicle"
--
PRINT (N'Создать индекс "FK_vehicle_team_id" для объекта типа таблица "dbo.vehicle"')
GO
CREATE INDEX FK_vehicle_team_id
  ON dbo.vehicle (Team_id)
  ON [PRIMARY]
GO

--
-- Создать индекс "FK_vehicle_vehicle_category_Id" для объекта типа таблица "dbo.vehicle"
--
PRINT (N'Создать индекс "FK_vehicle_vehicle_category_Id" для объекта типа таблица "dbo.vehicle"')
GO
CREATE INDEX FK_vehicle_vehicle_category_Id
  ON dbo.vehicle (Category_id)
  ON [PRIMARY]
GO

--
-- Создать индекс "id" для объекта типа таблица "dbo.vehicle"
--
PRINT (N'Создать индекс "id" для объекта типа таблица "dbo.vehicle"')
GO
CREATE UNIQUE INDEX id
  ON dbo.vehicle (id)
  ON [PRIMARY]
GO

--
-- Создать индекс "setting_id" для объекта типа таблица "dbo.vehicle"
--
PRINT (N'Создать индекс "setting_id" для объекта типа таблица "dbo.vehicle"')
GO
CREATE INDEX setting_id
  ON dbo.vehicle (setting_id)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.MakeCar" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.MakeCar" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Марка автомобиля', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'MakeCar'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.NumberPlate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.NumberPlate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Номерной знак', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'NumberPlate'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.Odometr" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.Odometr" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Показания одометра', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'Odometr'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.CarModel" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.CarModel" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Модель', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'CarModel'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.odoTime" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.odoTime" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата и время сверки одометра', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'odoTime'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.OutLinkId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.OutLinkId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код внешней базы', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'OutLinkId'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.Category_id" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.Category_id" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Категория транспортая', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'Category_id'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.FuelWayLiter" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.FuelWayLiter" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Расход топлива л/100 км', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'FuelWayLiter'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.FuelMotorLiter" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.FuelMotorLiter" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Расход топлива л/моточас', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'FuelMotorLiter'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.Identifier" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.Identifier" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор машины', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'Identifier'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.Category_id2" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.Category_id2" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Категория2 транспорта', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'Category_id2'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.vehicle.PcbVersionId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.vehicle.PcbVersionId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Версия плат датчика назначенная на машину', 'SCHEMA', N'dbo', 'TABLE', N'vehicle', 'COLUMN', N'PcbVersionId'
GO

--
-- Создать представление "dbo.ua_misceznah"
--
GO
PRINT (N'Создать представление "dbo.ua_misceznah"')
GO
CREATE VIEW dbo.ua_misceznah 
AS SELECT team.Name AS team
        , vehicle_category.Name AS blok
        , vehicle.NumberPlate AS tz
        , left(mobitels.Name, 7) AS mobitel_id
       
   FROM
 vehicle
     INNER JOIN mobitels
       ON mobitels.Mobitel_ID = vehicle.Mobitel_id
     INNER JOIN team
       ON team.id = vehicle.Team_id
     INNER JOIN vehicle_category
       ON vehicle_category.Id = vehicle.Category_id
   WHERE
      (vehicle.Team_id = 5
     OR vehicle.Team_id = 2
     OR vehicle.Team_id = 3
     OR vehicle.Team_id = 4
     OR vehicle.Team_id = 6
     OR vehicle.Team_id = 8
     OR vehicle.Team_id = 9
     OR vehicle.Team_id = 12
     OR vehicle.Team_id = 13
     OR vehicle.Team_id = 14
  OR vehicle.Team_id = 15
  OR vehicle.Team_id = 16
  OR vehicle.Team_id = 17
  OR vehicle.Team_id = 18)
   GROUP BY
     team.Name
   , vehicle_category.Name
   , vehicle.NumberPlate
   , mobitels.Name
GO

--
-- Создать таблицу "dbo.mobitels_rotate_bands"
--
PRINT (N'Создать таблицу "dbo.mobitels_rotate_bands"')
GO
CREATE TABLE dbo.mobitels_rotate_bands (
  Id int IDENTITY,
  Mobitel_ID int NOT NULL DEFAULT (0),
  Bound int NOT NULL DEFAULT (0),
  CONSTRAINT fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID FOREIGN KEY (Mobitel_ID) REFERENCES dbo.mobitels (Mobitel_ID) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "FK_mobitels_rotate_bands_mobitels_Mobitel_ID" для объекта типа таблица "dbo.mobitels_rotate_bands"
--
PRINT (N'Создать индекс "FK_mobitels_rotate_bands_mobitels_Mobitel_ID" для объекта типа таблица "dbo.mobitels_rotate_bands"')
GO
CREATE INDEX FK_mobitels_rotate_bands_mobitels_Mobitel_ID
  ON dbo.mobitels_rotate_bands (Mobitel_ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.lines"
--
PRINT (N'Создать таблицу "dbo.lines"')
GO
CREATE TABLE dbo.lines (
  id int IDENTITY,
  Color int NULL DEFAULT (0),
  Width int NULL DEFAULT (3),
  Mobitel_ID int NULL,
  Icon varbinary(max) NULL,
  CONSTRAINT fgn_key_lines_FK1 FOREIGN KEY (Mobitel_ID) REFERENCES dbo.mobitels (Mobitel_ID)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "lines_FK1" для объекта типа таблица "dbo.lines"
--
PRINT (N'Создать индекс "lines_FK1" для объекта типа таблица "dbo.lines"')
GO
CREATE INDEX lines_FK1
  ON dbo.lines (Mobitel_ID)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.lines.Color" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.lines.Color" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Цвет', 'SCHEMA', N'dbo', 'TABLE', N'lines', 'COLUMN', N'Color'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.lines.Width" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.lines.Width" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Толщина линии', 'SCHEMA', N'dbo', 'TABLE', N'lines', 'COLUMN', N'Width'
GO

--
-- Создать таблицу "dbo.datagpsbuffer_ontmp64"
--
PRINT (N'Создать таблицу "dbo.datagpsbuffer_ontmp64"')
GO
CREATE TABLE dbo.datagpsbuffer_ontmp64 (
  Mobitel_ID int NOT NULL,
  LogID int NOT NULL,
  SrvPacketID bigint NOT NULL,
  CONSTRAINT FK_datagps64_mobitels_Mobitel_ID_2 FOREIGN KEY (Mobitel_ID) REFERENCES dbo.mobitels (Mobitel_ID) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.agro_order"
--
PRINT (N'Создать таблицу "dbo.agro_order"')
GO
CREATE TABLE dbo.agro_order (
  Id int IDENTITY,
  Date datetime NULL,
  Id_mobitel int NOT NULL DEFAULT (0),
  Id_agregat int NOT NULL DEFAULT (0),
  LocationStart varchar(255) NULL,
  LocationEnd varchar(255) NULL,
  TimeStart datetime NULL,
  TimeEnd datetime NULL,
  Regime smallint NULL DEFAULT (0),
  TimeWork char(10) NULL,
  TimeMove char(10) NULL,
  Distance float NULL DEFAULT (0),
  SpeedAvg float NULL,
  Comment varchar(max) NULL,
  PathWithoutWork float NULL DEFAULT (0),
  SquareWorkDescript varchar(max) NULL,
  FuelDUTExpensSquare float NULL DEFAULT (0),
  FuelDRTExpensSquare float NULL DEFAULT (0),
  FuelDUTExpensAvgSquare float NULL DEFAULT (0),
  FuelDRTExpensAvgSquare float NULL DEFAULT (0),
  FuelDUTExpensWithoutWork float NULL DEFAULT (0),
  FuelDRTExpensWithoutWork float NULL DEFAULT (0),
  FuelDUTExpensAvgWithoutWork float NULL DEFAULT (0),
  FuelDRTExpensAvgWithoutWork float NULL DEFAULT (0),
  SquareWorkDescripOverlap varchar(max) NULL,
  FactSquareCalcOverlap float NULL DEFAULT (0),
  DateLastRecalc datetime NULL,
  UserLastRecalc varchar(127) NULL,
  UserCreated varchar(127) NULL,
  PointsValidity int NULL,
  PointsCalc int NULL DEFAULT (0),
  PointsFact int NULL DEFAULT (0),
  PointsIntervalMax varchar(20) NULL,
  StateOrder int NOT NULL DEFAULT (0),
  BlockUserId int NOT NULL DEFAULT (0),
  BlockDate datetime NULL,
  CONSTRAINT PK_agro_order PRIMARY KEY CLUSTERED (Id),
  CONSTRAINT fgn_key_Id_mobitel_Order_FK1 FOREIGN KEY (Id_mobitel) REFERENCES dbo.mobitels (Mobitel_ID)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "Id_mobitel_Order_FK1" для объекта типа таблица "dbo.agro_order"
--
PRINT (N'Создать индекс "Id_mobitel_Order_FK1" для объекта типа таблица "dbo.agro_order"')
GO
CREATE INDEX Id_mobitel_Order_FK1
  ON dbo.agro_order (Id_mobitel)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Date" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Date" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата наряда на работы(new type - DATE->DATETIME)', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'Date'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Id_mobitel" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Id_mobitel" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Транспортное средство', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'Id_mobitel'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Id_agregat" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Id_agregat" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Навесное оборудование', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'Id_agregat'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.LocationStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.LocationStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Место начала движения', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'LocationStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.LocationEnd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.LocationEnd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Место окончания движения', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'LocationEnd'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.TimeStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.TimeStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Время начала движения', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'TimeStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.TimeEnd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.TimeEnd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Время окончания движения', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'TimeEnd'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Regime" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Regime" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Режим наряд -ручной,автоматический, закрыт', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'Regime'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.TimeWork" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.TimeWork" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Продолжительность смены, ч', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'TimeWork'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.TimeMove" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.TimeMove" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Общее время движения, ч', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'TimeMove'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Distance" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Distance" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Пройденный путь, км', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'Distance'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.SpeedAvg" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.SpeedAvg" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Средняя скорость, км/ч', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'SpeedAvg'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'Comment'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.PathWithoutWork" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.PathWithoutWork" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Переезды, км', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'PathWithoutWork'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.SquareWorkDescript" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.SquareWorkDescript" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Обработанная площадь', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'SquareWorkDescript'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDUTExpensSquare" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDUTExpensSquare" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Расход в полях,  всего', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'FuelDUTExpensSquare'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDRTExpensSquare" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDRTExpensSquare" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Расход в полях,  всего', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'FuelDRTExpensSquare'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDUTExpensAvgSquare" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDUTExpensAvgSquare" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Расход в полях, л/га:', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'FuelDUTExpensAvgSquare'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDRTExpensAvgSquare" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDRTExpensAvgSquare" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Расход в полях, л/га:', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'FuelDRTExpensAvgSquare'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDUTExpensWithoutWork" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDUTExpensWithoutWork" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Расход на переездах, всего', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'FuelDUTExpensWithoutWork'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDRTExpensWithoutWork" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDRTExpensWithoutWork" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Расход на переездах, всего', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'FuelDRTExpensWithoutWork'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDUTExpensAvgWithoutWork" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDUTExpensAvgWithoutWork" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Переезды, км', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'FuelDUTExpensAvgWithoutWork'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDRTExpensAvgWithoutWork" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FuelDRTExpensAvgWithoutWork" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Переезды, км', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'FuelDRTExpensAvgWithoutWork'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.SquareWorkDescripOverlap" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.SquareWorkDescripOverlap" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Обработанная площадь с учетом перекрытий всех записей', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'SquareWorkDescripOverlap'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FactSquareCalcOverlap" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.FactSquareCalcOverlap" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Факт обработанная площадь полученная расчетом с учетом перекрытий всех записей', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'FactSquareCalcOverlap'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.DateLastRecalc" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.DateLastRecalc" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата последнего пересчета наряда', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'DateLastRecalc'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.UserLastRecalc" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.UserLastRecalc" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Пользователь, последней пересчитавший наряд', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'UserLastRecalc'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.UserCreated" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.UserCreated" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Пользователь, создавший наряд', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'UserCreated'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.PointsValidity" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.PointsValidity" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Валидность данных', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'PointsValidity'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.PointsCalc" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.PointsCalc" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Количество точек (по Log_ID) расчетное', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'PointsCalc'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.PointsFact" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.PointsFact" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Количество точек (по Log_ID) фактическое', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'PointsFact'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.PointsIntervalMax" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.PointsIntervalMax" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Максимальный интервал между точками', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'PointsIntervalMax'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.StateOrder" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.StateOrder" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Статус наряда', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'StateOrder'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.BlockUserId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.BlockUserId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код пользователя, работающего с документом', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'BlockUserId'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_order.BlockDate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_order.BlockDate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала работы с документом', 'SCHEMA', N'dbo', 'TABLE', N'agro_order', 'COLUMN', N'BlockDate'
GO

--
-- Создать таблицу "dbo.agro_ordert_control"
--
PRINT (N'Создать таблицу "dbo.agro_ordert_control"')
GO
CREATE TABLE dbo.agro_ordert_control (
  Id int IDENTITY,
  Id_main int NOT NULL DEFAULT (0),
  Id_object int NOT NULL DEFAULT (0),
  Type_object smallint NOT NULL DEFAULT (0),
  TimeStart datetime NOT NULL,
  TimeEnd datetime NOT NULL,
  CONSTRAINT fgn_key_FK_agro_ordert_control_agro_order_Id FOREIGN KEY (Id_main) REFERENCES dbo.agro_order (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "FK_agro_ordert_control_agro_order_Id" для объекта типа таблица "dbo.agro_ordert_control"
--
PRINT (N'Создать индекс "FK_agro_ordert_control_agro_order_Id" для объекта типа таблица "dbo.agro_ordert_control"')
GO
CREATE INDEX FK_agro_ordert_control_agro_order_Id
  ON dbo.agro_ordert_control (Id_main)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert_control.Id_main" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert_control.Id_main" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'?????? ?? ?????', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert_control', 'COLUMN', N'Id_main'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert_control.Id_object" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert_control.Id_object" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'??? ??????? ??????????', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert_control', 'COLUMN', N'Id_object'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert_control.Type_object" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert_control.Type_object" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'??? ??????? ??????????', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert_control', 'COLUMN', N'Type_object'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert_control.TimeStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert_control.TimeStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'???? ????? ?????? ??????', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert_control', 'COLUMN', N'TimeStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert_control.TimeEnd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert_control.TimeEnd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'???? ????? ????????? ?????', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert_control', 'COLUMN', N'TimeEnd'
GO

--
-- Создать таблицу "dbo.messages"
--
PRINT (N'Создать таблицу "dbo.messages"')
GO
CREATE TABLE dbo.messages (
  Message_ID int IDENTITY,
  Time1 int NULL DEFAULT (0),
  Time2 int NULL DEFAULT (0),
  Time3 int NULL DEFAULT (0),
  Time4 int NULL DEFAULT (0),
  isSend smallint NULL,
  isDelivered smallint NULL,
  isNew smallint NULL,
  Direction smallint NULL,
  Mobitel_ID int NULL DEFAULT (0),
  ServiceInit_ID int NULL DEFAULT (0),
  Command_ID smallint NULL DEFAULT (0),
  CommandMask_ID int NULL DEFAULT (0),
  MessageCounter int NULL DEFAULT (0),
  DataFromService char(32) NULL,
  Address char(20) NULL,
  Source_ID int NULL DEFAULT (0),
  Crc smallint NULL DEFAULT (1),
  SmsId int NULL DEFAULT (0),
  AdvCounter int NOT NULL DEFAULT (0),
  isNewFromMob smallint NOT NULL DEFAULT (1)
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.messages"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.messages"')
GO
CREATE INDEX Index_2
  ON dbo.messages (isNewFromMob, Message_ID, Time1)
  ON [PRIMARY]
GO

--
-- Создать индекс "Index_4" для объекта типа таблица "dbo.messages"
--
PRINT (N'Создать индекс "Index_4" для объекта типа таблица "dbo.messages"')
GO
CREATE INDEX Index_4
  ON dbo.messages (Mobitel_ID, Command_ID)
  ON [PRIMARY]
GO

--
-- Создать индекс "Index_5" для объекта типа таблица "dbo.messages"
--
PRINT (N'Создать индекс "Index_5" для объекта типа таблица "dbo.messages"')
GO
CREATE INDEX Index_5
  ON dbo.messages (Mobitel_ID, MessageCounter)
  ON [PRIMARY]
GO

--
-- Создать индекс "Index_6" для объекта типа таблица "dbo.messages"
--
PRINT (N'Создать индекс "Index_6" для объекта типа таблица "dbo.messages"')
GO
CREATE INDEX Index_6
  ON dbo.messages (isNew, Direction, ServiceInit_ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.mapstylepoints"
--
PRINT (N'Создать таблицу "dbo.mapstylepoints"')
GO
CREATE TABLE dbo.mapstylepoints (
  MapStylePoint_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  isBitmap smallint NULL,
  SymbolCode smallint NULL DEFAULT (0),
  Color int NULL DEFAULT (0),
  Font char(200) NULL,
  Size int NULL DEFAULT (0),
  Rotation int NULL DEFAULT (0),
  BitmapName char(200) NULL,
  BitmapStyle smallint NULL DEFAULT (0),
  Style smallint NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.mapstylelines"
--
PRINT (N'Создать таблицу "dbo.mapstylelines"')
GO
CREATE TABLE dbo.mapstylelines (
  MapStyleLine_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Color int NULL DEFAULT (0),
  Width smallint NULL DEFAULT (0),
  Style smallint NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.mapstyleglobal"
--
PRINT (N'Создать таблицу "dbo.mapstyleglobal"')
GO
CREATE TABLE dbo.mapstyleglobal (
  MapStyleGlobal_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  DefaultLineStyle_ID int NULL DEFAULT (0),
  DefaulPointStyle_ID int NULL DEFAULT (0),
  EventPointStyle1_ID int NULL DEFAULT (0),
  EventPointStyle2_ID int NULL DEFAULT (0),
  EventPointStyle3_ID int NULL DEFAULT (0),
  EventPointStyle4_ID int NULL DEFAULT (0),
  EventPointStyle5_ID int NULL DEFAULT (0),
  EventPointStyle6_ID int NULL DEFAULT (0),
  EventPointStyle7_ID int NULL DEFAULT (0),
  EventPointStyle8_ID int NULL DEFAULT (0),
  EventPointStyle9_ID int NULL DEFAULT (0),
  EventPointStyle10_ID int NULL DEFAULT (0),
  EventPointStyle11_ID int NULL DEFAULT (0),
  EventPointStyle12_ID int NULL DEFAULT (0),
  EventPointStyle13_ID int NULL DEFAULT (0),
  EventPointStyle14_ID int NULL DEFAULT (0),
  EventPointStyle15_ID int NULL DEFAULT (0),
  EventPointStyle16_ID int NULL DEFAULT (0),
  EventPointStyle17_ID int NULL DEFAULT (0),
  EventPointStyle18_ID int NULL DEFAULT (0),
  EventPointStyle19_ID int NULL DEFAULT (0),
  EventPointStyle20_ID int NULL DEFAULT (0),
  EventPointStyle21_ID int NULL DEFAULT (0),
  EventPointStyle22_ID int NULL DEFAULT (0),
  EventPointStyle23_ID int NULL DEFAULT (0),
  EventPointStyle24_ID int NULL DEFAULT (0),
  EventPointStyle25_ID int NULL DEFAULT (0),
  EventPointStyle26_ID int NULL DEFAULT (0),
  EventPointStyle27_ID int NULL DEFAULT (0),
  EventPointStyle28_ID int NULL DEFAULT (0),
  EventPointStyle29_ID int NULL DEFAULT (0),
  EventPointStyle30_ID int NULL DEFAULT (0),
  EventPointStyle31_ID int NULL DEFAULT (0),
  EventPointStyle32_ID int NULL DEFAULT (0),
  SensorPointStyle1_ID int NULL DEFAULT (0),
  SensorPointStyle2_ID int NULL DEFAULT (0),
  SensorPointStyle3_ID int NULL DEFAULT (0),
  SensorPointStyle4_ID int NULL DEFAULT (0),
  SensorPointStyle5_ID int NULL DEFAULT (0),
  SensorPointStyle6_ID int NULL DEFAULT (0),
  SensorPointStyle7_ID int NULL DEFAULT (0),
  SensorPointStyle8_ID int NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.localizationconstvalue"
--
PRINT (N'Создать таблицу "dbo.localizationconstvalue"')
GO
CREATE TABLE dbo.localizationconstvalue (
  ConstID int NOT NULL,
  LocalizationID int NOT NULL,
  Value nvarchar(100) NOT NULL
)
ON [PRIMARY]
GO

--
-- Создать индекс "LocalizationID" для объекта типа таблица "dbo.localizationconstvalue"
--
PRINT (N'Создать индекс "LocalizationID" для объекта типа таблица "dbo.localizationconstvalue"')
GO
CREATE INDEX LocalizationID
  ON dbo.localizationconstvalue (LocalizationID)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.localizationconstvalue" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.localizationconstvalue" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.localizationconstvalue', 'SCHEMA', N'dbo', 'TABLE', N'localizationconstvalue'
GO

--
-- Создать таблицу "dbo.localization"
--
PRINT (N'Создать таблицу "dbo.localization"')
GO
CREATE TABLE dbo.localization (
  LocalizationID int IDENTITY (9, 1),
  ShortTitle nvarchar(5) NOT NULL,
  LongTitle nvarchar(30) NOT NULL,
  CONSTRAINT PK_localization_LocalizationID PRIMARY KEY CLUSTERED (LocalizationID)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.localization" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.localization" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.localization', 'SCHEMA', N'dbo', 'TABLE', N'localization'
GO

--
-- Создать таблицу "dbo.lasttimes"
--
PRINT (N'Создать таблицу "dbo.lasttimes"')
GO
CREATE TABLE dbo.lasttimes (
  LastTime_ID int NULL DEFAULT (0),
  ID int NULL DEFAULT (0),
  Name char(200) NULL,
  Descr char(200) NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.lastrecords"
--
PRINT (N'Создать таблицу "dbo.lastrecords"')
GO
CREATE TABLE dbo.lastrecords (
  LastRecord_ID int NULL DEFAULT (0),
  ID int NULL DEFAULT (0),
  Name char(200) NULL,
  Descr char(200) NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.lastmeters"
--
PRINT (N'Создать таблицу "dbo.lastmeters"')
GO
CREATE TABLE dbo.lastmeters (
  LastMeter_ID int NULL DEFAULT (0),
  ID int NULL DEFAULT (0),
  Name char(200) NULL,
  Descr char(200) NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.internalmobitelconfig"
--
PRINT (N'Создать таблицу "dbo.internalmobitelconfig"')
GO
CREATE TABLE dbo.internalmobitelconfig (
  InternalMobitelConfig_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  devIdLong char(16) NULL,
  devIdShort char(4) NULL,
  verProtocolLong char(16) NULL,
  verProtocolShort char(2) NULL,
  moduleIdGps char(50) NULL,
  moduleIdGsm char(50) NULL,
  moduleIdRf char(50) NULL,
  RfType smallint NOT NULL DEFAULT (0),
  ActiveRf smallint NOT NULL DEFAULT (0),
  moduleIdSs char(50) NULL,
  moduleIdMm char(50) NOT NULL DEFAULT ('moduleIdMm')
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_InternalmobitelconfigID" для объекта типа таблица "dbo.internalmobitelconfig"
--
PRINT (N'Создать индекс "IDX_InternalmobitelconfigID" для объекта типа таблица "dbo.internalmobitelconfig"')
GO
CREATE INDEX IDX_InternalmobitelconfigID
  ON dbo.internalmobitelconfig (ID)
  ON [PRIMARY]
GO

--
-- Создать индекс "Index_3" для объекта типа таблица "dbo.internalmobitelconfig"
--
PRINT (N'Создать индекс "Index_3" для объекта типа таблица "dbo.internalmobitelconfig"')
GO
CREATE INDEX Index_3
  ON dbo.internalmobitelconfig (Message_ID)
  ON [PRIMARY]
GO

--
-- Создать индекс "Index_4" для объекта типа таблица "dbo.internalmobitelconfig"
--
PRINT (N'Создать индекс "Index_4" для объекта типа таблица "dbo.internalmobitelconfig"')
GO
CREATE INDEX Index_4
  ON dbo.internalmobitelconfig (devIdShort)
  ON [PRIMARY]
GO

--
-- Создать процедуру "dbo.PopListMobitelConfig"
--
GO
PRINT (N'Создать процедуру "dbo.PopListMobitelConfig"')
GO

CREATE PROCEDURE dbo.PopListMobitelConfig
  -- COMMENT 'Выборка настроек телетреков для работы в режиме POP3'
  AS
BEGIN
  SELECT m.Mobitel_ID AS MobitelID, c.devIdShort AS DevIdShort,
    COALESCE(
      (SELECT TOP 1 s.EmailMobitel 
       FROM Mobitels m2 JOIN ServiceSend s ON s.ID = m2.ServiceSend_ID 
       WHERE m2.Mobitel_ID = m.Mobitel_ID
       ORDER BY s.ServiceSend_ID DESC), '') AS Email
  FROM mobitels m JOIN internalmobitelconfig c ON c.ID = m.InternalMobitelConfig_ID
  WHERE (c.InternalMobitelConfig_ID = (
    SELECT TOP 1 intConf.InternalMobitelConfig_ID
    FROM internalmobitelconfig intConf
    WHERE (intConf.ID = c.ID)
    ORDER BY intConf.InternalMobitelConfig_ID DESC)) AND (c.devIdShort IS NOT NULL) AND (c.devIdShort <> '')
  ORDER BY m.Mobitel_ID;
END

GO

--
-- Создать представление "dbo.internalmobitelconfigview"
--
GO
PRINT (N'Создать представление "dbo.internalmobitelconfigview"')
GO

CREATE VIEW dbo.internalmobitelconfigview
AS (SELECT c.ID AS ID
         , max(c.InternalMobitelConfig_ID) AS LastRecord
   FROM
     internalmobitelconfig c
   WHERE
     (c.devIdShort <> '')
   GROUP BY
     c.ID);

GO

--
-- Создать представление "dbo.deviceview"
--
GO
PRINT (N'Создать представление "dbo.deviceview"')
GO

CREATE VIEW dbo.deviceview
AS (SELECT m.Mobitel_ID AS Mobitel_ID
         , m.Name AS Name
         , m.Descr AS Descr
         , c.devIdShort AS devIdShort
         , m.DeviceType AS DeviceType
         , m.ConfirmedID AS ConfirmedID
         , c.moduleIdRf AS moduleIdRf
   FROM
     ((mobitels m
     JOIN internalmobitelconfigview v
       ON v.ID = m.InternalMobitelConfig_ID)
     JOIN internalmobitelconfig c
       ON v.LastRecord = c.InternalMobitelConfig_ID));

GO

--
-- Создать таблицу "dbo.groupsconstlist"
--
PRINT (N'Создать таблицу "dbo.groupsconstlist"')
GO
CREATE TABLE dbo.groupsconstlist (
  GroupID int IDENTITY (28, 1),
  GroupName nvarchar(50) NOT NULL,
  CONSTRAINT PK_groupsconstlist_GroupID PRIMARY KEY CLUSTERED (GroupID)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.groupsconstlist" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.groupsconstlist" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.groupsconstlist', 'SCHEMA', N'dbo', 'TABLE', N'groupsconstlist'
GO

--
-- Создать таблицу "dbo.gpsmasks"
--
PRINT (N'Создать таблицу "dbo.gpsmasks"')
GO
CREATE TABLE dbo.gpsmasks (
  GpsMask_ID int IDENTITY,
  ID int NULL DEFAULT (0),
  Name char(200) NULL,
  Descr char(200) NULL,
  Mobitel_ID int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  LastRecords int NULL DEFAULT (0),
  LastTimes int NULL DEFAULT (0),
  LastTimesIndex int NULL DEFAULT (0),
  LastMeters int NULL DEFAULT (0),
  LastMetersIndex int NULL DEFAULT (0),
  MaxSmsNum int NULL DEFAULT (0),
  MaxSmsNumIndex int NULL DEFAULT (0),
  LatitudeStart int NULL DEFAULT (0),
  LatitudeEnd int NULL DEFAULT (0),
  LongitudeStart int NULL DEFAULT (0),
  LongitudeEnd int NULL DEFAULT (0),
  AltitudeStart int NULL DEFAULT (0),
  AltitudeEnd int NULL DEFAULT (0),
  UnixTimeStart int NULL DEFAULT (0),
  UnixTimeEnd int NULL DEFAULT (0),
  SpeedStart int NULL DEFAULT (0),
  SpeedEnd int NULL DEFAULT (0),
  DirectionStart int NULL DEFAULT (0),
  DirectionEnd int NULL DEFAULT (0),
  NumbersStart int NULL DEFAULT (0),
  NumbersEnd int NULL DEFAULT (0),
  WhatSend int NULL DEFAULT (0),
  CheckMask int NULL DEFAULT (0),
  InMobitelID int NULL DEFAULT (0),
  Events int NULL DEFAULT (0),
  Number1 int NULL DEFAULT (0),
  Number2 int NULL DEFAULT (0),
  Number3 int NULL DEFAULT (0),
  Number4 int NULL DEFAULT (0),
  Number5 int NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.driver_types"
--
PRINT (N'Создать таблицу "dbo.driver_types"')
GO
CREATE TABLE dbo.driver_types (
  Id int IDENTITY,
  TypeName varchar(50) NULL
)
ON [PRIMARY]
GO

--
-- Создать индекс "id" для объекта типа таблица "dbo.driver_types"
--
PRINT (N'Создать индекс "id" для объекта типа таблица "dbo.driver_types"')
GO
CREATE UNIQUE INDEX id
  ON dbo.driver_types (Id)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.driver_types.TypeName" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.driver_types.TypeName" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Специализация водителя', 'SCHEMA', N'dbo', 'TABLE', N'driver_types', 'COLUMN', N'TypeName'
GO

--
-- Создать таблицу "dbo.driver"
--
PRINT (N'Создать таблицу "dbo.driver"')
GO
CREATE TABLE dbo.driver (
  id int IDENTITY,
  Family char(40) NULL,
  Name char(40) NULL,
  ByrdDay date NULL,
  Category char(5) NULL,
  Permis char(20) NULL,
  foto varbinary(max) NULL,
  Identifier int NULL,
  IPN char(10) NULL,
  OutLinkId varchar(20) NULL,
  idType int NULL DEFAULT (0),
  numTelephone varchar(32) NOT NULL DEFAULT (''),
  Department varchar(max) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "id" для объекта типа таблица "dbo.driver"
--
PRINT (N'Создать индекс "id" для объекта типа таблица "dbo.driver"')
GO
CREATE UNIQUE INDEX id
  ON dbo.driver (id)
  ON [PRIMARY]
GO

--
-- Создать индекс "UQ_Identifier" для объекта типа таблица "dbo.driver"
--
PRINT (N'Создать индекс "UQ_Identifier" для объекта типа таблица "dbo.driver"')
GO
CREATE INDEX UQ_Identifier
  ON dbo.driver (Identifier)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.driver.Family" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.driver.Family" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Фамилия', 'SCHEMA', N'dbo', 'TABLE', N'driver', 'COLUMN', N'Family'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.driver.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.driver.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя', 'SCHEMA', N'dbo', 'TABLE', N'driver', 'COLUMN', N'Name'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.driver.ByrdDay" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.driver.ByrdDay" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата рождения', 'SCHEMA', N'dbo', 'TABLE', N'driver', 'COLUMN', N'ByrdDay'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.driver.Category" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.driver.Category" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Категория', 'SCHEMA', N'dbo', 'TABLE', N'driver', 'COLUMN', N'Category'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.driver.Permis" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.driver.Permis" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Права', 'SCHEMA', N'dbo', 'TABLE', N'driver', 'COLUMN', N'Permis'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.driver.foto" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.driver.foto" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'фото', 'SCHEMA', N'dbo', 'TABLE', N'driver', 'COLUMN', N'foto'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.driver.Identifier" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.driver.Identifier" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'10 битный идентификатор водителя', 'SCHEMA', N'dbo', 'TABLE', N'driver', 'COLUMN', N'Identifier'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.driver.IPN" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.driver.IPN" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ІПН', 'SCHEMA', N'dbo', 'TABLE', N'driver', 'COLUMN', N'IPN'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.driver.OutLinkId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.driver.OutLinkId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код внешней базы', 'SCHEMA', N'dbo', 'TABLE', N'driver', 'COLUMN', N'OutLinkId'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.driver.idType" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.driver.idType" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код специализации водителя', 'SCHEMA', N'dbo', 'TABLE', N'driver', 'COLUMN', N'idType'
GO

--
-- Создать таблицу "dbo.datagpsbuffer_on64"
--
PRINT (N'Создать таблицу "dbo.datagpsbuffer_on64"')
GO
CREATE TABLE dbo.datagpsbuffer_on64 (
  Mobitel_ID int NOT NULL,
  Latitude int NOT NULL,
  Longitude int NOT NULL,
  Direction tinyint NULL,
  Acceleration tinyint NULL,
  UnixTime int NOT NULL,
  Speed smallint NULL,
  Valid bit NOT NULL,
  Satellites smallint NULL,
  RssiGsm smallint NULL,
  Events int NULL,
  SensorsSet smallint NOT NULL,
  Sensors varbinary(50) NULL,
  Voltage smallint NULL,
  DGPS varchar(32) NULL,
  LogID int NULL,
  SrvPacketID bigint NULL
)
ON [PRIMARY]
GO

--
-- Создать процедуру "dbo.OnDeleteDuplicates64"
--
GO
PRINT (N'Создать процедуру "dbo.OnDeleteDuplicates64"')
GO


CREATE PROCEDURE dbo.OnDeleteDuplicates64
AS
BEGIN
  /* Удаление повторяющихся строк из datagpsbuffer_on64' */

  /* Текущий MobitelId в курсоре CursorDuplicateGroups*/
  DECLARE @MobitelIdInCursor INT;
  SET @MobitelIdInCursor = 0;
  /* Текущий LogID в курсоре CursorDuplicateGroups  */
  DECLARE @LogIdInCursor INT;
  SET @LogIdInCursor = 0;
  /* Max SrvPacketId в множестве строк текущей группы дубликатов */
  DECLARE @MaxSrvPacketId BIGINT;
  SET @MaxSrvPacketId = 0;
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /* Курсор для прохода по всем группам дубликатов */
  DECLARE CursorDuplicateGroups CURSOR LOCAL FOR
  SELECT Mobitel_Id
       , LogId
  FROM
    datagpsbuffer_on64
  GROUP BY
    Mobitel_Id
  , LogId
  HAVING
    count(LogId) > 1;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET @DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SELECT @MaxSrvPacketId = max(SrvPacketID)
    FROM
      datagpsbuffer_on64
    WHERE
      (Mobitel_ID = @MobitelIdInCursor)
      AND (LogId = @LogIdInCursor);
    DELETE
    FROM
      datagpsbuffer_on64
    WHERE
      (Mobitel_ID = @MobitelIdInCursor) AND
      (LogId = @LogIdInCursor) AND
      (SrvPacketID < @MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  END;
  CLOSE CursorDuplicateGroups;
--DEALLOCATE CursorDuplicateGroups;
END

GO

--
-- Создать процедуру "dbo.delDupSrvPacket"
--
GO
PRINT (N'Создать процедуру "dbo.delDupSrvPacket"')
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDUREdbo.delDupSrvPacket
AS
BEGIN
    -- Insert statements for procedure here
	if exists (
        select * from tempdb.dbo.sysobjects o
        where o.xtype in ('U') 
        and o.id = object_id(N'tempdb..#tmp64')
)
BEGIN
  DROP TABLE #tmp64; 
END;

if object_id('tempdb..#tmp64') is not null drop table #tmp64  -- проверка на существование таблицы

create table #tmp64 -- создание временной таблицы
    (mobi int, lat int, lng int, dir int, acc int, unixt int, spd int, vald int, stl int, rsg int, evn int, snsset int, sns varbinary(50), vlt int, dgp varchar(32), lid int, spid bigint)

insert into #tmp64 SELECT 
  d.Mobitel_ID,
  d.Latitude,
  d.Longitude,
  d.Direction,
  d.Acceleration,
  d.UnixTime,
  d.Speed,
  d.Valid,
  d.Satellites,
  d.RssiGsm,
  d.[Events],
  d.SensorsSet,
  d.Sensors,
  d.Voltage,
  d.DGPS,
  d.LogID,
  d.SrvPacketID
FROM datagpsbuffer_on64 d;
--GROUP BY d.SrvPacketID;

TRUNCATE TABLE datagpsbuffer_on64;

INSERT INTO dbo.datagpsbuffer_on64 SELECT d.mobi,
  d.lat,
  d.lng,
  d.dir,
  d.acc,
  d.unixt,
  d.spd,
  d.vald,
  d.stl,
  d.rsg,
  d.evn,
  d.snsset,
  d.sns,
  d.vlt,
  d.dgp,
  d.lid,
  d.spid
FROM #tmp64 d;

-- Insert statements for procedure here
	if exists (
        select * from tempdb.dbo.sysobjects o
        where o.xtype in ('U') 
        and o.id = object_id(N'tempdb..#tmp64')
)
BEGIN
  DROP TABLE #tmp64; 
END;

if object_id('tempdb..#tmp64') is not null drop table #tmp64  -- проверка на существование таблицы

END
GO

--
-- Создать таблицу "dbo.datagps64"
--
PRINT (N'Создать таблицу "dbo.datagps64"')
GO
CREATE TABLE dbo.datagps64 (
  DataGps_ID bigint IDENTITY,
  Mobitel_ID int NOT NULL CONSTRAINT DF__datagps64__Mobite__2D12A970 DEFAULT (0),
  Latitude int NOT NULL CONSTRAINT DF__datagps64__Latitu__2E06CDA9 DEFAULT (0),
  Longitude int NOT NULL CONSTRAINT DF__datagps64__Longit__2EFAF1E2 DEFAULT (0),
  Direction tinyint NULL CONSTRAINT DF__datagps64__Direct__2FEF161B DEFAULT (0),
  Acceleration tinyint NULL CONSTRAINT DF__datagps64__Accele__30E33A54 DEFAULT (0),
  UnixTime int NOT NULL CONSTRAINT DF__datagps64__UnixTi__31D75E8D DEFAULT (0),
  Speed smallint NOT NULL CONSTRAINT DF__datagps64__Speed__32CB82C6 DEFAULT (0),
  Valid bit NOT NULL CONSTRAINT DF__datagps64__Valid__33BFA6FF DEFAULT (0),
  Satellites smallint NULL CONSTRAINT DF__datagps64__Satell__34B3CB38 DEFAULT (0),
  RssiGsm smallint NULL CONSTRAINT DF__datagps64__RssiGs__35A7EF71 DEFAULT (0),
  Events int NULL CONSTRAINT DF__datagps64__Events__369C13AA DEFAULT (0),
  SensorsSet smallint NULL CONSTRAINT DF__datagps64__Sensor__379037E3 DEFAULT (0),
  Sensors varbinary(50) NULL CONSTRAINT DF__datagps64__Sensor__38845C1C DEFAULT (0),
  Voltage smallint NULL CONSTRAINT DF__datagps64__Voltag__39788055 DEFAULT (0),
  DGPS varchar(32) NULL CONSTRAINT DF__datagps64__DGPS__3A6CA48E DEFAULT (0),
  LogID int NULL CONSTRAINT DF__datagps64__LogID__3B60C8C7 DEFAULT (0),
  SrvPacketID bigint NOT NULL CONSTRAINT DF__datagps64__SrvPac__3C54ED00 DEFAULT (0),
  CONSTRAINT PK_datagps64 PRIMARY KEY CLUSTERED (DataGps_ID)
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelIDLogID64" для объекта типа таблица "dbo.datagps64"
--
PRINT (N'Создать индекс "IDX_MobitelIDLogID64" для объекта типа таблица "dbo.datagps64"')
GO
CREATE INDEX IDX_MobitelIDLogID64
  ON dbo.datagps64 (Mobitel_ID, LogID)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelIDSrvPacketID64" для объекта типа таблица "dbo.datagps64"
--
PRINT (N'Создать индекс "IDX_MobitelIDSrvPacketID64" для объекта типа таблица "dbo.datagps64"')
GO
CREATE INDEX IDX_MobitelIDSrvPacketID64
  ON dbo.datagps64 (Mobitel_ID, SrvPacketID)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelIDUnixTimeValid64" для объекта типа таблица "dbo.datagps64"
--
PRINT (N'Создать индекс "IDX_MobitelIDUnixTimeValid64" для объекта типа таблица "dbo.datagps64"')
GO
CREATE INDEX IDX_MobitelIDUnixTimeValid64
  ON dbo.datagps64 (Mobitel_ID, UnixTime, Valid)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Mobitel_ID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Mobitel_ID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код Телетрека', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'Mobitel_ID'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Latitude" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Latitude" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Широта', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'Latitude'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Longitude" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Longitude" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Долгота', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'Longitude'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Direction" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Direction" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'градусы/1.5', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'Direction'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Acceleration" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Acceleration" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Изменение скорости по GPS за последнюю секунду', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'Acceleration'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Speed" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Speed" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Сотен метров в час (0,1 км/ч)', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'Speed'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Satellites" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Satellites" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Количество спутников ', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'Satellites'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.RssiGsm" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.RssiGsm" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Уровень GSM сигнала', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'RssiGsm'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Events" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Events" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Флаги событий', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'Events'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.SensorsSet" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.SensorsSet" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код набора датчиков', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'SensorsSet'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Sensors" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Sensors" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Значения датчиков', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'Sensors'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Voltage" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.Voltage" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Напряжение питания текущего источника/1.5 от 0 до 38,2', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'Voltage'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.DGPS" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.DGPS" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Данные дифференциальной системы GPS - высота,широта,долгота', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'DGPS'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.LogID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.LogID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор записи в журнале Телетрека', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'LogID'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps64.SrvPacketID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps64.SrvPacketID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ID серверного пакета в состав которого входит эта запись', 'SCHEMA', N'dbo', 'TABLE', N'datagps64', 'COLUMN', N'SrvPacketID'
GO

--
-- Создать процедуру "dbo.OnDeleteExistingRecords64"
--
GO
PRINT (N'Создать процедуру "dbo.OnDeleteExistingRecords64"')
GO

CREATE PROCEDURE dbo.OnDeleteExistingRecords64
  --COMMENT 'Удаление из datagpsbuffer_on64 записей, уже присутсвующих в data'
  as
BEGIN

  /* Текущий MobitelId в курсоре CursorExistingRecords */
  DECLARE @MobitelIdInCursor int;
  SET @MobitelIdInCursor = 0;
  /* Текущий SrvPacketId в курсоре CursorExistingRecords */
  DECLARE @SrvPacketIdInCursor int;
  SET @SrvPacketIdInCursor = 0;

  DECLARE @DataExists tinyint;
  SET @DataExists = 1;

  /* Курсор для прохода по всем записям */
  DECLARE CursorExistingRecords CURSOR FOR
  SELECT
    datagpsbuffer_on64.Mobitel_ID,
    datagpsbuffer_on64.SrvPacketID
  FROM datagpsbuffer_on64
    INNER JOIN datagps64
      ON datagpsbuffer_on64.Mobitel_ID = datagps64.Mobitel_ID
      AND datagpsbuffer_on64.SrvPacketID = datagps64.SrvPacketID
  GROUP BY datagpsbuffer_on64.Mobitel_ID,
           datagpsbuffer_on64.SrvPacketID;

 -- DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET @DataExists = 1;
  OPEN CursorExistingRecords;
  FETCH CursorExistingRecords INTO @MobitelIdInCursor, @SrvPacketIdInCursor;
  WHILE @@fetch_status = 0 BEGIN
    DELETE
      FROM datagpsbuffer_on64
    WHERE (Mobitel_ID = @MobitelIdInCursor)
      AND (SrvPacketID = @SrvPacketIdInCursor);
    FETCH CursorExistingRecords INTO @MobitelIdInCursor, @SrvPacketIdInCursor;
  END;
  CLOSE CursorExistingRecords;
END

GO

--
-- Создать таблицу "dbo.constslist"
--
PRINT (N'Создать таблицу "dbo.constslist"')
GO
CREATE TABLE dbo.constslist (
  ConstID int IDENTITY (184, 1),
  ConstName nvarchar(50) NOT NULL,
  GroupConstID int NOT NULL,
  CONSTRAINT PK_constslist_ConstID PRIMARY KEY CLUSTERED (ConstID)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.constslist" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.constslist" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.constslist', 'SCHEMA', N'dbo', 'TABLE', N'constslist'
GO

--
-- Создать таблицу "dbo.configzoneset"
--
PRINT (N'Создать таблицу "dbo.configzoneset"')
GO
CREATE TABLE dbo.configzoneset (
  ConfigZoneSet_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.configunique"
--
PRINT (N'Создать таблицу "dbo.configunique"')
GO
CREATE TABLE dbo.configunique (
  ConfigUnique_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  pswd char(8) NULL,
  tmpPswd char(8) NULL,
  dsptId char(4) NULL
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.configunique"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.configunique"')
GO
CREATE INDEX Index_2
  ON dbo.configunique (ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.configtel"
--
PRINT (N'Создать таблицу "dbo.configtel"')
GO
CREATE TABLE dbo.configtel (
  ConfigTel_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  telSOS int NULL DEFAULT (0),
  telDspt int NULL DEFAULT (0),
  telAccept1 int NULL DEFAULT (0),
  telAccept2 int NULL DEFAULT (0),
  telAccept3 int NULL DEFAULT (0),
  telAnswer int NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.configtel"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.configtel"')
GO
CREATE INDEX Index_2
  ON dbo.configtel (ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.configsms"
--
PRINT (N'Создать таблицу "dbo.configsms"')
GO
CREATE TABLE dbo.configsms (
  ConfigSms_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  smsCenter_ID int NULL DEFAULT (0),
  smsDspt_ID int NULL DEFAULT (0),
  smsEmailGate_ID int NULL DEFAULT (0),
  dsptEmail char(14) NULL,
  dsptGprsEmail char(50) NULL
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.configsms"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.configsms"')
GO
CREATE INDEX Index_2
  ON dbo.configsms (ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.configsensorset"
--
PRINT (N'Создать таблицу "dbo.configsensorset"')
GO
CREATE TABLE dbo.configsensorset (
  ConfigSensorSet_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.configother"
--
PRINT (N'Создать таблицу "dbo.configother"')
GO
CREATE TABLE dbo.configother (
  ConfigOther_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  Pin int NULL DEFAULT (0),
  Lang int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.configother"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.configother"')
GO
CREATE INDEX Index_2
  ON dbo.configother (ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.configmain"
--
PRINT (N'Создать таблицу "dbo.configmain"')
GO
CREATE TABLE dbo.configmain (
  ConfigMain_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  ConfigOther_ID int NULL DEFAULT (0),
  ConfigDriverMessage_ID int NULL DEFAULT (0),
  ConfigEvent_ID int NULL DEFAULT (0),
  ConfigUnique_ID int NULL DEFAULT (0),
  ConfigZoneSet_ID int NULL DEFAULT (0),
  ConfigSensorSet_ID int NULL DEFAULT (0),
  ConfigTel_ID int NULL DEFAULT (0),
  ConfigSms_ID int NULL DEFAULT (0),
  Mobitel_ID int NULL DEFAULT (0),
  ConfigGprsMain_ID int NOT NULL DEFAULT (0),
  ConfigGprsEmail_ID int NOT NULL DEFAULT (0),
  ConfigGprsInit_ID int NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.configmain"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.configmain"')
GO
CREATE INDEX Index_2
  ON dbo.configmain (Mobitel_ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.configgprsmain"
--
PRINT (N'Создать таблицу "dbo.configgprsmain"')
GO
CREATE TABLE dbo.configgprsmain (
  ConfigGprsMain_ID int IDENTITY,
  Name varchar(50) NULL,
  Descr varchar(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  Mode int NOT NULL DEFAULT (0),
  Apnserv varchar(50) NOT NULL DEFAULT (''),
  Apnun varchar(50) NOT NULL DEFAULT (''),
  Apnpw varchar(50) NOT NULL DEFAULT (''),
  Dnsserv1 varchar(50) NOT NULL DEFAULT (''),
  Dialn1 varchar(50) NOT NULL DEFAULT (''),
  Ispun varchar(50) NOT NULL DEFAULT (''),
  Isppw varchar(50) NOT NULL DEFAULT ('')
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.configgprsinit"
--
PRINT (N'Создать таблицу "dbo.configgprsinit"')
GO
CREATE TABLE dbo.configgprsinit (
  ConfigGprsInit_ID int IDENTITY,
  Name varchar(50) NULL,
  Descr varchar(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  Domain varchar(50) NOT NULL DEFAULT ('0'),
  InitString varchar(200) NOT NULL DEFAULT ('')
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.configgprsinit"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.configgprsinit"')
GO
CREATE INDEX Index_2
  ON dbo.configgprsinit (ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.configgprsemail"
--
PRINT (N'Создать таблицу "dbo.configgprsemail"')
GO
CREATE TABLE dbo.configgprsemail (
  ConfigGprsEmail_ID int IDENTITY,
  Name varchar(50) NULL,
  Descr varchar(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  Smtpserv varchar(50) NOT NULL DEFAULT (''),
  Smtpun varchar(50) NOT NULL DEFAULT (''),
  Smtppw varchar(50) NOT NULL DEFAULT (''),
  Pop3serv varchar(50) NOT NULL DEFAULT (''),
  Pop3un varchar(50) NOT NULL DEFAULT (''),
  Pop3pw varchar(50) NOT NULL DEFAULT ('')
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.configgprsemail"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.configgprsemail"')
GO
CREATE INDEX Index_2
  ON dbo.configgprsemail (ID)
  ON [PRIMARY]
GO

--
-- Создать процедуру "dbo.DrListMobitelConfig"
--
GO
PRINT (N'Создать процедуру "dbo.DrListMobitelConfig"')
GO

CREATE PROCEDURE dbo.DrListMobitelConfig
--SQL SECURITY INVOKER
--COMMENT 'Выборка настроек телетреков в режиме DirectOnline'
AS
BEGIN
  SELECT m.Mobitel_id AS MobitelID
       , imc.DevIdShort AS DevIdShort
       , cge.Pop3un AS Login
       , cge.Pop3pw AS Password
  FROM
    Mobitels m
    JOIN ConfigMain cm
      ON m.Mobitel_id = cm.Mobitel_ID
    JOIN ConfigGprsEmail cge
      ON cm.ConfigGprsEmail_ID = cge.ID
    JOIN internalmobitelconfig imc
      ON m.InternalMobitelConfig_ID = imc.ID
  WHERE
    (cge.ConfigGprsEmail_ID = (SELECT max(intCge.ConfigGprsEmail_ID)
                               FROM
                                 ConfigGprsEmail intCge
                               WHERE
                                 intCge.ID = cge.ID))
    AND (imc.InternalMobitelConfig_ID = (SELECT max(intConf.InternalMobitelConfig_ID)
                                         FROM
                                           internalmobitelconfig intConf
                                         WHERE
                                           intConf.ID = imc.ID))
    AND (imc.devIdShort IS NOT NULL)
    AND (imc.devIdShort <> '')
    AND (cge.Pop3un IS NOT NULL)
    AND (cge.Pop3un <> '')
  ORDER BY
    m.Mobitel_ID;
END

GO

--
-- Создать таблицу "dbo.configevent"
--
PRINT (N'Создать таблицу "dbo.configevent"')
GO
CREATE TABLE dbo.configevent (
  ConfigEvent_ID int IDENTITY,
  Name varchar(max) NULL,
  Descr varchar(max) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  tmr1Log int NULL DEFAULT (0),
  tmr2Send int NULL DEFAULT (0),
  tmr3Zone int NULL DEFAULT (0),
  dist1Log int NULL DEFAULT (0),
  dist2Send int NULL DEFAULT (0),
  dist3Zone int NULL DEFAULT (0),
  deltaTimeZone int NULL DEFAULT (0),
  gprsEmail int NOT NULL DEFAULT (0),
  gprsFtp int NOT NULL DEFAULT (0),
  gprsSocket int NOT NULL DEFAULT (0),
  maskSensor char(50) NULL,
  maskEvent1 int NULL DEFAULT (0),
  maskEvent2 int NULL DEFAULT (0),
  maskEvent3 int NULL DEFAULT (0),
  maskEvent4 int NULL DEFAULT (0),
  maskEvent5 int NULL DEFAULT (0),
  maskEvent6 int NULL DEFAULT (0),
  maskEvent7 int NULL DEFAULT (0),
  maskEvent8 int NULL DEFAULT (0),
  maskEvent9 int NULL DEFAULT (0),
  maskEvent10 int NULL DEFAULT (0),
  maskEvent11 int NULL DEFAULT (0),
  maskEvent12 int NULL DEFAULT (0),
  maskEvent13 int NULL DEFAULT (0),
  maskEvent14 int NULL DEFAULT (0),
  maskEvent15 int NULL DEFAULT (0),
  maskEvent16 int NULL DEFAULT (0),
  maskEvent17 int NULL DEFAULT (0),
  maskEvent18 int NULL DEFAULT (0),
  maskEvent19 int NULL DEFAULT (0),
  maskEvent20 int NULL DEFAULT (0),
  maskEvent21 int NULL DEFAULT (0),
  maskEvent22 int NULL DEFAULT (0),
  maskEvent23 int NULL DEFAULT (0),
  maskEvent24 int NULL DEFAULT (0),
  maskEvent25 int NULL DEFAULT (0),
  maskEvent26 int NULL DEFAULT (0),
  maskEvent27 int NULL DEFAULT (0),
  maskEvent28 int NULL DEFAULT (0),
  maskEvent29 int NULL DEFAULT (0),
  maskEvent30 int NULL DEFAULT (0),
  maskEvent31 int NULL DEFAULT (0)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.configevent"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.configevent"')
GO
CREATE INDEX Index_2
  ON dbo.configevent (ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.configdrivermessage"
--
PRINT (N'Создать таблицу "dbo.configdrivermessage"')
GO
CREATE TABLE dbo.configdrivermessage (
  ConfigDriverMessage_ID int IDENTITY,
  Name char(50) NULL,
  Descr char(200) NULL,
  ID int NULL DEFAULT (0),
  Flags int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  DriverMessage char(200) NULL
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.configdrivermessage"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.configdrivermessage"')
GO
CREATE INDEX Index_2
  ON dbo.configdrivermessage (ID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.commands"
--
PRINT (N'Создать таблицу "dbo.commands"')
GO
CREATE TABLE dbo.commands (
  ID int NULL DEFAULT (0),
  Name char(50) NULL,
  Descr char(200) NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.agro_workgroup"
--
PRINT (N'Создать таблицу "dbo.agro_workgroup"')
GO
CREATE TABLE dbo.agro_workgroup (
  Id int IDENTITY,
  Name varchar(50) NOT NULL DEFAULT (''),
  Comment varchar(max) NULL,
  CONSTRAINT PK_agro_workgroup PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_workgroup.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_workgroup.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Название', 'SCHEMA', N'dbo', 'TABLE', N'agro_workgroup', 'COLUMN', N'Name'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_workgroup.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_workgroup.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_workgroup', 'COLUMN', N'Comment'
GO

--
-- Создать таблицу "dbo.agro_work_types"
--
PRINT (N'Создать таблицу "dbo.agro_work_types"')
GO
CREATE TABLE dbo.agro_work_types (
  Id int IDENTITY,
  Name varchar(50) NOT NULL DEFAULT (''),
  CONSTRAINT PK_agro_work_types PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.agro_work"
--
PRINT (N'Создать таблицу "dbo.agro_work"')
GO
CREATE TABLE dbo.agro_work (
  Id int IDENTITY,
  Name varchar(50) NOT NULL DEFAULT (''),
  SpeedBottom float NULL DEFAULT (0),
  SpeedTop float NULL DEFAULT (0),
  Comment varchar(max) NULL,
  OutLinkId varchar(255) NULL,
  TypeWork int NOT NULL DEFAULT (1),
  Id_main int NULL,
  TrackColor int NOT NULL DEFAULT (0),
  CONSTRAINT PK_agro_work PRIMARY KEY CLUSTERED (Id),
  CONSTRAINT fgn_key_FK_agro_work_agro_work_types_Id FOREIGN KEY (TypeWork) REFERENCES dbo.agro_work_types (Id),
  CONSTRAINT fgn_key_FK_agro_work_agro_workgroup_Id FOREIGN KEY (Id_main) REFERENCES dbo.agro_workgroup (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "FK_agro_work_agro_work_types_Id" для объекта типа таблица "dbo.agro_work"
--
PRINT (N'Создать индекс "FK_agro_work_agro_work_types_Id" для объекта типа таблица "dbo.agro_work"')
GO
CREATE INDEX FK_agro_work_agro_work_types_Id
  ON dbo.agro_work (TypeWork)
  ON [PRIMARY]
GO

--
-- Создать индекс "FK_agro_work_agro_workgroup_Id" для объекта типа таблица "dbo.agro_work"
--
PRINT (N'Создать индекс "FK_agro_work_agro_workgroup_Id" для объекта типа таблица "dbo.agro_work"')
GO
CREATE INDEX FK_agro_work_agro_workgroup_Id
  ON dbo.agro_work (Id_main)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_work.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_work.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Название', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'Name'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_work.SpeedBottom" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_work.SpeedBottom" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Нижний предел скорости', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'SpeedBottom'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_work.SpeedTop" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_work.SpeedTop" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Верхний предел скорости', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'SpeedTop'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_work.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_work.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'Comment'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_work.TypeWork" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_work.TypeWork" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Работа в поле, переезд вне поля,переезд по полю', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'TypeWork'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_work.Id_main" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_work.Id_main" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Группа видов работ', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'Id_main'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_work.TrackColor" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_work.TrackColor" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Цвет трека работы на карте', 'SCHEMA', N'dbo', 'TABLE', N'agro_work', 'COLUMN', N'TrackColor'
GO

--
-- Создать таблицу "dbo.ntf_agro_works"
--
PRINT (N'Создать таблицу "dbo.ntf_agro_works"')
GO
CREATE TABLE dbo.ntf_agro_works (
  Id int IDENTITY,
  Id_main int NOT NULL,
  WorkId int NOT NULL,
  CONSTRAINT PK_ntf_agro_works PRIMARY KEY CLUSTERED (Id),
  CONSTRAINT FK_ntf_agro_works_ntf_main_Id FOREIGN KEY (WorkId) REFERENCES dbo.agro_work (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.agro_unit"
--
PRINT (N'Создать таблицу "dbo.agro_unit"')
GO
CREATE TABLE dbo.agro_unit (
  Id int IDENTITY,
  Name char(100) NOT NULL DEFAULT (''),
  NameShort char(50) NOT NULL DEFAULT (''),
  Factor float NULL DEFAULT (0),
  Comment varchar(max) NULL,
  CONSTRAINT PK_agro_unit PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_unit.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_unit.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Название', 'SCHEMA', N'dbo', 'TABLE', N'agro_unit', 'COLUMN', N'Name'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_unit.NameShort" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_unit.NameShort" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Название краткое', 'SCHEMA', N'dbo', 'TABLE', N'agro_unit', 'COLUMN', N'NameShort'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_unit.Factor" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_unit.Factor" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Коэфициент пересчета к базовой', 'SCHEMA', N'dbo', 'TABLE', N'agro_unit', 'COLUMN', N'Factor'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_unit.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_unit.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_unit', 'COLUMN', N'Comment'
GO

--
-- Создать таблицу "dbo.agro_season"
--
PRINT (N'Создать таблицу "dbo.agro_season"')
GO
CREATE TABLE dbo.agro_season (
  Id int IDENTITY,
  DateStart date NOT NULL,
  DateEnd date NOT NULL,
  Id_culture int NOT NULL,
  SquarePlan decimal(10, 2) NOT NULL DEFAULT (0),
  Comment varchar(max) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_season.DateStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_season.DateStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала сезона', 'SCHEMA', N'dbo', 'TABLE', N'agro_season', 'COLUMN', N'DateStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_season.DateEnd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_season.DateEnd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата окончания сезона', 'SCHEMA', N'dbo', 'TABLE', N'agro_season', 'COLUMN', N'DateEnd'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_season.Id_culture" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_season.Id_culture" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Культура', 'SCHEMA', N'dbo', 'TABLE', N'agro_season', 'COLUMN', N'Id_culture'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_season.SquarePlan" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_season.SquarePlan" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Запланированная посевная площадь, га', 'SCHEMA', N'dbo', 'TABLE', N'agro_season', 'COLUMN', N'SquarePlan'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_season.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_season.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_season', 'COLUMN', N'Comment'
GO

--
-- Создать таблицу "dbo.agro_price"
--
PRINT (N'Создать таблицу "dbo.agro_price"')
GO
CREATE TABLE dbo.agro_price (
  Id int IDENTITY,
  DateInit date NULL,
  DateComand date NULL,
  Name char(50) NOT NULL DEFAULT (''),
  Comment varchar(max) NULL,
  CONSTRAINT PK_agro_price PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_price.DateInit" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_price.DateInit" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата приказа ', 'SCHEMA', N'dbo', 'TABLE', N'agro_price', 'COLUMN', N'DateInit'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_price.DateComand" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_price.DateComand" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата ,с котрой действуют цены ', 'SCHEMA', N'dbo', 'TABLE', N'agro_price', 'COLUMN', N'DateComand'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_price.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_price.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Название', 'SCHEMA', N'dbo', 'TABLE', N'agro_price', 'COLUMN', N'Name'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_price.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_price.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_price', 'COLUMN', N'Comment'
GO

--
-- Создать таблицу "dbo.agro_plan_state"
--
PRINT (N'Создать таблицу "dbo.agro_plan_state"')
GO
CREATE TABLE dbo.agro_plan_state (
  Id smallint IDENTITY,
  StatusName varchar(50) NOT NULL DEFAULT (''),
  StatusComment varchar(max) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "Id" для объекта типа таблица "dbo.agro_plan_state"
--
PRINT (N'Создать индекс "Id" для объекта типа таблица "dbo.agro_plan_state"')
GO
CREATE UNIQUE INDEX Id
  ON dbo.agro_plan_state (Id)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan_state.StatusName" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan_state.StatusName" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Причина простоя', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan_state', 'COLUMN', N'StatusName'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan_state.StatusComment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan_state.StatusComment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan_state', 'COLUMN', N'StatusComment'
GO

--
-- Создать таблицу "dbo.agro_plan_downtimereason"
--
PRINT (N'Создать таблицу "dbo.agro_plan_downtimereason"')
GO
CREATE TABLE dbo.agro_plan_downtimereason (
  Id int IDENTITY,
  ReasonName varchar(255) NOT NULL DEFAULT (''),
  ReasonComment varchar(max) NULL,
  CONSTRAINT PK_agro_plan_downtimereason PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan_downtimereason.ReasonName" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan_downtimereason.ReasonName" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Причина простоя', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan_downtimereason', 'COLUMN', N'ReasonName'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan_downtimereason.ReasonComment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan_downtimereason.ReasonComment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan_downtimereason', 'COLUMN', N'ReasonComment'
GO

--
-- Создать таблицу "dbo.agro_plan"
--
PRINT (N'Создать таблицу "dbo.agro_plan"')
GO
CREATE TABLE dbo.agro_plan (
  Id int IDENTITY,
  PlanDate datetime NOT NULL,
  Id_mobitel int NOT NULL DEFAULT (0),
  SquarePlanned float NOT NULL DEFAULT (0),
  FuelAtStart float NOT NULL DEFAULT (0),
  FuelForWork float NOT NULL DEFAULT (0),
  IdStatus smallint NULL,
  Id_order int NULL,
  PlanComment varchar(max) NULL,
  UserCreated varchar(127) NULL,
  IsDownTime bit NOT NULL DEFAULT (0),
  IdDowntimeReason int NULL,
  DowntimeStart date NULL,
  DowntimeEnd date NULL,
  CONSTRAINT fgn_key_FK_agro_plan_agro_plan_downtimereasons_Id FOREIGN KEY (IdDowntimeReason) REFERENCES dbo.agro_plan_downtimereason (Id),
  CONSTRAINT fgn_key_FK_agro_plan_agro_plan_status_Id FOREIGN KEY (IdStatus) REFERENCES dbo.agro_plan_state (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "FK_agro_plan_agro_plan_downtimereasons_Id" для объекта типа таблица "dbo.agro_plan"
--
PRINT (N'Создать индекс "FK_agro_plan_agro_plan_downtimereasons_Id" для объекта типа таблица "dbo.agro_plan"')
GO
CREATE INDEX FK_agro_plan_agro_plan_downtimereasons_Id
  ON dbo.agro_plan (IdDowntimeReason)
  ON [PRIMARY]
GO

--
-- Создать индекс "FK_agro_plan_agro_plan_status_Id" для объекта типа таблица "dbo.agro_plan"
--
PRINT (N'Создать индекс "FK_agro_plan_agro_plan_status_Id" для объекта типа таблица "dbo.agro_plan"')
GO
CREATE INDEX FK_agro_plan_agro_plan_status_Id
  ON dbo.agro_plan (IdStatus)
  ON [PRIMARY]
GO

--
-- Создать индекс "Id" для объекта типа таблица "dbo.agro_plan"
--
PRINT (N'Создать индекс "Id" для объекта типа таблица "dbo.agro_plan"')
GO
CREATE UNIQUE INDEX Id
  ON dbo.agro_plan (Id)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.PlanDate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.PlanDate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата планового задания', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'PlanDate'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.Id_mobitel" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.Id_mobitel" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Транспортное средство', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'Id_mobitel'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.SquarePlanned" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.SquarePlanned" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Планируемая для обработки площадь', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'SquarePlanned'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.FuelAtStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.FuelAtStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Топливо на начало планового задания', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'FuelAtStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.FuelForWork" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.FuelForWork" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Объем топлива, которое разрешено отпустить данной технике для выполнения планового задания, л', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'FuelForWork'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.IdStatus" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.IdStatus" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Режим наряд -ручной,автоматический, закрыт,простой', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'IdStatus'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.Id_order" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.Id_order" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Связь с нарядом', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'Id_order'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.PlanComment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.PlanComment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'PlanComment'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.UserCreated" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.UserCreated" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Пользователь, создавший план', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'UserCreated'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.IsDownTime" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.IsDownTime" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Техника не учавствует в полевых работах', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'IsDownTime'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.IdDowntimeReason" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.IdDowntimeReason" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Причина простоя техники', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'IdDowntimeReason'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.DowntimeStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.DowntimeStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала простоя', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'DowntimeStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.DowntimeEnd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plan.DowntimeEnd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата ввода в эксплуатацию', 'SCHEMA', N'dbo', 'TABLE', N'agro_plan', 'COLUMN', N'DowntimeEnd'
GO

--
-- Создать таблицу "dbo.agro_plant"
--
PRINT (N'Создать таблицу "dbo.agro_plant"')
GO
CREATE TABLE dbo.agro_plant (
  Id int IDENTITY,
  Id_main int NOT NULL DEFAULT (0),
  Id_field int NULL DEFAULT (0),
  Id_driver int NULL DEFAULT (0),
  Id_work int NULL DEFAULT (0),
  Id_agregat int NULL DEFAULT (0),
  Remark varchar(max) NULL,
  TimeStart datetime NULL,
  CONSTRAINT fgn_key_FK_agro_plant_agro_plan_Id FOREIGN KEY (Id_main) REFERENCES dbo.agro_plan (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "FK_agro_plant_agro_plan_Id" для объекта типа таблица "dbo.agro_plant"
--
PRINT (N'Создать индекс "FK_agro_plant_agro_plan_Id" для объекта типа таблица "dbo.agro_plant"')
GO
CREATE INDEX FK_agro_plant_agro_plan_Id
  ON dbo.agro_plant (Id_main)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Id_main" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Id_main" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на план', 'SCHEMA', N'dbo', 'TABLE', N'agro_plant', 'COLUMN', N'Id_main'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Id_field" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Id_field" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на поле', 'SCHEMA', N'dbo', 'TABLE', N'agro_plant', 'COLUMN', N'Id_field'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Id_driver" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Id_driver" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Водитель', 'SCHEMA', N'dbo', 'TABLE', N'agro_plant', 'COLUMN', N'Id_driver'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Id_work" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Id_work" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на работу', 'SCHEMA', N'dbo', 'TABLE', N'agro_plant', 'COLUMN', N'Id_work'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Id_agregat" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Id_agregat" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Навесное оборудование', 'SCHEMA', N'dbo', 'TABLE', N'agro_plant', 'COLUMN', N'Id_agregat'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Remark" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.Remark" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_plant', 'COLUMN', N'Remark'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.TimeStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_plant.TimeStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Время начала работы', 'SCHEMA', N'dbo', 'TABLE', N'agro_plant', 'COLUMN', N'TimeStart'
GO

--
-- Создать таблицу "dbo.agro_params"
--
PRINT (N'Создать таблицу "dbo.agro_params"')
GO
CREATE TABLE dbo.agro_params (
  Id int IDENTITY,
  Number int NOT NULL,
  Value varchar(100) NULL,
  Description varchar(50) NULL
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_params.Number" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_params.Number" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер параметра', 'SCHEMA', N'dbo', 'TABLE', N'agro_params', 'COLUMN', N'Number'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_params.Value" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_params.Value" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение', 'SCHEMA', N'dbo', 'TABLE', N'agro_params', 'COLUMN', N'Value'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_params.Description" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_params.Description" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Описание', 'SCHEMA', N'dbo', 'TABLE', N'agro_params', 'COLUMN', N'Description'
GO

--
-- Создать таблицу "dbo.agro_ordert"
--
PRINT (N'Создать таблицу "dbo.agro_ordert"')
GO
CREATE TABLE dbo.agro_ordert (
  Id int IDENTITY,
  Id_main int NULL,
  Id_field int NOT NULL DEFAULT (0),
  Id_zone int NOT NULL DEFAULT (0),
  Id_driver int NOT NULL DEFAULT (0),
  TimeStart datetime NULL,
  TimeEnd datetime NULL,
  TimeMove time NULL,
  TimeStop time NULL,
  TimeRate time NULL,
  Id_work int NOT NULL DEFAULT (0),
  FactTime time NULL,
  Distance float NULL DEFAULT (0),
  FactSquare float NOT NULL DEFAULT (0.00000),
  FactSquareCalc float NOT NULL DEFAULT (0.00000),
  Price float NOT NULL DEFAULT (0.00),
  Sum float NOT NULL DEFAULT (0.00),
  SpeedAvg float NULL DEFAULT (0),
  Comment varchar(max) NULL,
  FuelStart float NOT NULL DEFAULT (0.00),
  FuelAdd float NOT NULL DEFAULT (0.00),
  FuelSub float NOT NULL DEFAULT (0.00),
  FuelEnd float NOT NULL DEFAULT (0.00),
  FuelExpens float NOT NULL DEFAULT (0.00),
  FuelExpensAvg float NOT NULL DEFAULT (0.00),
  FuelExpensAvgRate float NOT NULL DEFAULT (0.00),
  Fuel_ExpensAvgRate float NOT NULL DEFAULT (0.00),
  Fuel_ExpensMove float NOT NULL DEFAULT (0.00),
  Fuel_ExpensStop float NOT NULL DEFAULT (0.00),
  Fuel_ExpensAvg float NOT NULL DEFAULT (0.00),
  Fuel_ExpensTotal float NOT NULL DEFAULT (0.00),
  Id_agregat int NOT NULL DEFAULT (0),
  Confirm smallint NOT NULL DEFAULT (1),
  PointsValidity int NOT NULL DEFAULT (0),
  LockRecord smallint NOT NULL DEFAULT (0),
  AgrInputSource smallint NOT NULL DEFAULT (0),
  DrvInputSource smallint NOT NULL DEFAULT (0),
  Id_event_manually int NULL,
  CONSTRAINT PK_agro_ordert PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "Id_main_Order_FK1" для объекта типа таблица "dbo.agro_ordert"
--
PRINT (N'Создать индекс "Id_main_Order_FK1" для объекта типа таблица "dbo.agro_ordert"')
GO
CREATE INDEX Id_main_Order_FK1
  ON dbo.agro_ordert (Id_main)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_main" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_main" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на наряд', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Id_main'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_field" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_field" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на поле', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Id_field'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_zone" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_zone" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на зону', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Id_zone'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_driver" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_driver" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Водитель', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Id_driver'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.TimeStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.TimeStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Факт время начала работы в зоне', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'TimeStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.TimeEnd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.TimeEnd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Факт время окончания работ в зоне', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'TimeEnd'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.TimeMove" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.TimeMove" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Время движения, ч', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'TimeMove'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.TimeStop" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.TimeStop" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Время стоянок, ч', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'TimeStop'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.TimeRate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.TimeRate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Моточасы, ч', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'TimeRate'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_work" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_work" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на работу', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Id_work'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FactTime" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FactTime" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Факт время работ', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'FactTime'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Distance" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Distance" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Пройденный путь, км', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Distance'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FactSquare" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FactSquare" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Факт обработанная площадь', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'FactSquare'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FactSquareCalc" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FactSquareCalc" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Факт обработанная площадь полученная расчетом', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'FactSquareCalc'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Price" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Price" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Расценка', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Price'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Sum" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Sum" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Сумма за площадь', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Sum'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.SpeedAvg" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.SpeedAvg" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Средняя скорость выполнения работ', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'SpeedAvg'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Comment'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Топливо в начале', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'FuelStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelAdd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelAdd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Заправлено', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'FuelAdd'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelSub" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelSub" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Слито', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'FuelSub'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelEnd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelEnd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Топливо в конце', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'FuelEnd'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelExpens" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelExpens" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Общий расход топлива', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'FuelExpens'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelExpensAvg" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelExpensAvg" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Общий расход топлива на гектар', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'FuelExpensAvg'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelExpensAvgRate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.FuelExpensAvgRate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДУТ Средний расход, л/моточас', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'FuelExpensAvgRate'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Fuel_ExpensAvgRate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Fuel_ExpensAvgRate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДРТ/CAN Средний расход, л/моточас', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Fuel_ExpensAvgRate'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Fuel_ExpensMove" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Fuel_ExpensMove" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДРТ/CAN Расход топлива в движении, л', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Fuel_ExpensMove'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Fuel_ExpensStop" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Fuel_ExpensStop" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДРТ/CAN Расход топлива на стоянках, л', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Fuel_ExpensStop'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Fuel_ExpensAvg" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Fuel_ExpensAvg" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДРТ/CAN Средний расход, л/га', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Fuel_ExpensAvg'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Fuel_ExpensTotal" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Fuel_ExpensTotal" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ДРТ/CAN Общий расход, л', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Fuel_ExpensTotal'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_agregat" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Id_agregat" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Навесное оборудование', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Id_agregat'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Confirm" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.Confirm" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Подтверждение обработки площади', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'Confirm'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.PointsValidity" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.PointsValidity" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Валидность данных', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'PointsValidity'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.LockRecord" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.LockRecord" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Запись заблокирована для пересчетов', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'LockRecord'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.AgrInputSource" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.AgrInputSource" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Источник появления навесного оборудование: RFID метка,ручной ввод,значение из установок', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'AgrInputSource'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.DrvInputSource" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_ordert.DrvInputSource" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Источник появления водителя: RFID метка,ручной ввод,значение из справочника', 'SCHEMA', N'dbo', 'TABLE', N'agro_ordert', 'COLUMN', N'DrvInputSource'
GO

--
-- Создать таблицу "dbo.agro_datagps"
--
PRINT (N'Создать таблицу "dbo.agro_datagps"')
GO
CREATE TABLE dbo.agro_datagps (
  Id int IDENTITY,
  Id_main int NOT NULL,
  Lon_base float NOT NULL,
  Lat_base float NOT NULL,
  Lon_dev varchar(255) NOT NULL DEFAULT (''),
  Lat_dev varchar(255) NOT NULL DEFAULT (''),
  Speed_base float NOT NULL,
  Speed_dev varchar(255) NOT NULL DEFAULT (''),
  CONSTRAINT fgn_key_FK_agro_datagps_agro_ordert_Id FOREIGN KEY (Id_main) REFERENCES dbo.agro_ordert (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "FK_agro_datagps_agro_ordert_Id" для объекта типа таблица "dbo.agro_datagps"
--
PRINT (N'Создать индекс "FK_agro_datagps_agro_ordert_Id" для объекта типа таблица "dbo.agro_datagps"')
GO
CREATE INDEX FK_agro_datagps_agro_ordert_Id
  ON dbo.agro_datagps (Id_main)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.agro_calc"
--
PRINT (N'Создать таблицу "dbo.agro_calc"')
GO
CREATE TABLE dbo.agro_calc (
  Id int IDENTITY,
  Id_orderT int NOT NULL DEFAULT (0),
  StepLon float NOT NULL DEFAULT (0.0001),
  StepLat float NOT NULL DEFAULT (0.0001),
  Time_First datetime NULL,
  Time_Last datetime NULL,
  CONSTRAINT PK_agro_calc PRIMARY KEY CLUSTERED (Id),
  CONSTRAINT fgn_key_agro_calc_FK1 FOREIGN KEY (Id_orderT) REFERENCES dbo.agro_ordert (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "agro_calc_FK1" для объекта типа таблица "dbo.agro_calc"
--
PRINT (N'Создать индекс "agro_calc_FK1" для объекта типа таблица "dbo.agro_calc"')
GO
CREATE INDEX agro_calc_FK1
  ON dbo.agro_calc (Id_orderT)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_calc.Id_orderT" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_calc.Id_orderT" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ссылка на запись наряда', 'SCHEMA', N'dbo', 'TABLE', N'agro_calc', 'COLUMN', N'Id_orderT'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_calc.StepLon" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_calc.StepLon" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'шаг квадрата по оси Х', 'SCHEMA', N'dbo', 'TABLE', N'agro_calc', 'COLUMN', N'StepLon'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_calc.StepLat" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_calc.StepLat" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'шаг квадрата по оси У', 'SCHEMA', N'dbo', 'TABLE', N'agro_calc', 'COLUMN', N'StepLat'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_calc.Time_First" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_calc.Time_First" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Время начала трека в площади', 'SCHEMA', N'dbo', 'TABLE', N'agro_calc', 'COLUMN', N'Time_First'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_calc.Time_Last" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_calc.Time_Last" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Время окончания трека в площади', 'SCHEMA', N'dbo', 'TABLE', N'agro_calc', 'COLUMN', N'Time_Last'
GO

--
-- Создать таблицу "dbo.agro_calct"
--
PRINT (N'Создать таблицу "dbo.agro_calct"')
GO
CREATE TABLE dbo.agro_calct (
  Id int IDENTITY,
  Id_main int NOT NULL DEFAULT (0),
  Lon float NOT NULL DEFAULT (0),
  Lat float NOT NULL DEFAULT (0),
  Selected smallint NOT NULL DEFAULT (0),
  Border smallint NOT NULL DEFAULT (0),
  BorderNode smallint NOT NULL DEFAULT (0),
  Number int NOT NULL DEFAULT (0),
  CONSTRAINT fgn_key_agro_calct_FK1 FOREIGN KEY (Id_main) REFERENCES dbo.agro_calc (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "agro_calct_FK1" для объекта типа таблица "dbo.agro_calct"
--
PRINT (N'Создать индекс "agro_calct_FK1" для объекта типа таблица "dbo.agro_calct"')
GO
CREATE INDEX agro_calct_FK1
  ON dbo.agro_calct (Id_main)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_calct.Selected" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_calct.Selected" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'квадрат обработан', 'SCHEMA', N'dbo', 'TABLE', N'agro_calct', 'COLUMN', N'Selected'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_calct.Border" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_calct.Border" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'квадрат граничный', 'SCHEMA', N'dbo', 'TABLE', N'agro_calct', 'COLUMN', N'Border'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_calct.BorderNode" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_calct.BorderNode" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'квадрат  - узловая точка', 'SCHEMA', N'dbo', 'TABLE', N'agro_calct', 'COLUMN', N'BorderNode'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_calct.Number" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_calct.Number" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'посл-ть узловых точек для прорисовки контура', 'SCHEMA', N'dbo', 'TABLE', N'agro_calct', 'COLUMN', N'Number'
GO

--
-- Создать таблицу "dbo.agro_fueling"
--
PRINT (N'Создать таблицу "dbo.agro_fueling"')
GO
CREATE TABLE dbo.agro_fueling (
  Id int IDENTITY,
  Id_main int NULL,
  LockRecord smallint NOT NULL DEFAULT (0),
  Lat float NOT NULL DEFAULT (0),
  Lng float NOT NULL DEFAULT (0),
  DateFueling datetime NOT NULL,
  ValueSystem float NOT NULL DEFAULT (0),
  ValueDisp float NOT NULL DEFAULT (0),
  ValueStart float NOT NULL DEFAULT (0),
  Location varchar(255) NULL,
  Remark varchar(255) NULL
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.Id_main" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.Id_main" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на наряд', 'SCHEMA', N'dbo', 'TABLE', N'agro_fueling', 'COLUMN', N'Id_main'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.LockRecord" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.LockRecord" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Запись заблокирована для редактирования', 'SCHEMA', N'dbo', 'TABLE', N'agro_fueling', 'COLUMN', N'LockRecord'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.DateFueling" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.DateFueling" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата заправки / слива', 'SCHEMA', N'dbo', 'TABLE', N'agro_fueling', 'COLUMN', N'DateFueling'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.ValueSystem" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.ValueSystem" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Данные системы', 'SCHEMA', N'dbo', 'TABLE', N'agro_fueling', 'COLUMN', N'ValueSystem'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.ValueDisp" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.ValueDisp" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Данные диспетчера', 'SCHEMA', N'dbo', 'TABLE', N'agro_fueling', 'COLUMN', N'ValueDisp'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.ValueStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.ValueStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение перед заправкой / сливом', 'SCHEMA', N'dbo', 'TABLE', N'agro_fueling', 'COLUMN', N'ValueStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.Remark" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fueling.Remark" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий к изменению значения диспетчером', 'SCHEMA', N'dbo', 'TABLE', N'agro_fueling', 'COLUMN', N'Remark'
GO

--
-- Создать таблицу "dbo.agro_fieldseason_tct_fact"
--
PRINT (N'Создать таблицу "dbo.agro_fieldseason_tct_fact"')
GO
CREATE TABLE dbo.agro_fieldseason_tct_fact (
  Id int IDENTITY,
  Id_main int NOT NULL,
  IdOrder int NOT NULL,
  IdDriver int NOT NULL DEFAULT (0),
  IdAgreg int NULL,
  SquareFactGa decimal(10, 3) NOT NULL DEFAULT (0),
  JointProcess bit NOT NULL DEFAULT (0),
  SquareAfterRecalc decimal(10, 3) NOT NULL DEFAULT (0),
  DateRecalc datetime NULL,
  UserRecalc varchar(127) NOT NULL DEFAULT ('')
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.IdOrder" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.IdOrder" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на наряд', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct_fact', 'COLUMN', N'IdOrder'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.SquareFactGa" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.SquareFactGa" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Фактически обработанная площадь (с учетом совм. обработки).', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct_fact', 'COLUMN', N'SquareFactGa'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.JointProcess" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.JointProcess" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Совместная обработка', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct_fact', 'COLUMN', N'JointProcess'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.SquareAfterRecalc" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.SquareAfterRecalc" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Фактически обработанная площадь (с учетом совм. обработки).', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct_fact', 'COLUMN', N'SquareAfterRecalc'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.DateRecalc" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.DateRecalc" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата пересчета совместной обработки', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct_fact', 'COLUMN', N'DateRecalc'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.UserRecalc" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct_fact.UserRecalc" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Пользователь, создавший совместную обработку', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct_fact', 'COLUMN', N'UserRecalc'
GO

--
-- Создать таблицу "dbo.agro_fieldseason_tct"
--
PRINT (N'Создать таблицу "dbo.agro_fieldseason_tct"')
GO
CREATE TABLE dbo.agro_fieldseason_tct (
  Id int IDENTITY,
  Id_main int NULL,
  IdWork int NOT NULL,
  DateStartPlan datetime NOT NULL,
  DateEndPlan datetime NOT NULL,
  WorkQtyPlan decimal(10, 2) NOT NULL DEFAULT (0),
  DateStartFact datetime NULL,
  DateEndFact datetime NULL,
  SquareGa decimal(10, 3) NOT NULL DEFAULT (0),
  SquarePercent decimal(5, 2) NOT NULL DEFAULT (0),
  Comment varchar(max) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.IdWork" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.IdWork" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид работ', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct', 'COLUMN', N'IdWork'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.DateStartPlan" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.DateStartPlan" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала (план)', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct', 'COLUMN', N'DateStartPlan'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.DateEndPlan" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.DateEndPlan" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата окончания (план)', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct', 'COLUMN', N'DateEndPlan'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.WorkQtyPlan" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.WorkQtyPlan" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Плановое кол-во моточасов ', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct', 'COLUMN', N'WorkQtyPlan'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.DateStartFact" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.DateStartFact" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала (факт)', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct', 'COLUMN', N'DateStartFact'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.DateEndFact" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.DateEndFact" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата окончания (факт)', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct', 'COLUMN', N'DateEndFact'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.SquareGa" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.SquareGa" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Фактически обработанная площадь, га', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct', 'COLUMN', N'SquareGa'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.SquarePercent" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.SquarePercent" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Фактически обработанная площадь, %', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct', 'COLUMN', N'SquarePercent'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tct.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tct', 'COLUMN', N'Comment'
GO

--
-- Создать таблицу "dbo.agro_fieldseason_tc"
--
PRINT (N'Создать таблицу "dbo.agro_fieldseason_tc"')
GO
CREATE TABLE dbo.agro_fieldseason_tc (
  Id int IDENTITY,
  IdSeason int NOT NULL,
  IdField int NOT NULL,
  Comment varchar(max) NULL,
  DateInit datetime NOT NULL,
  UserCreated varchar(127) NOT NULL DEFAULT (''),
  DateStart datetime NOT NULL,
  DateEnd datetime NOT NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tc.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tc.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tc', 'COLUMN', N'Comment'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tc.DateInit" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tc.DateInit" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата создания', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tc', 'COLUMN', N'DateInit'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tc.UserCreated" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tc.UserCreated" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Пользователь, создавший тех карту ', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tc', 'COLUMN', N'UserCreated'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tc.DateStart" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tc.DateStart" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала работ (по умолчанию - дата начала сезона)', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tc', 'COLUMN', N'DateStart'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tc.DateEnd" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldseason_tc.DateEnd" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала работ (по умолчанию - дата окончания сезона)', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldseason_tc', 'COLUMN', N'DateEnd'
GO

--
-- Создать таблицу "dbo.agro_fieldgroupe"
--
PRINT (N'Создать таблицу "dbo.agro_fieldgroupe"')
GO
CREATE TABLE dbo.agro_fieldgroupe (
  Id int IDENTITY,
  Name varchar(50) NOT NULL DEFAULT (''),
  Comment varchar(max) NULL,
  Id_Zonesgroup int NULL DEFAULT (0)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldgroupe.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldgroupe.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Название', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldgroupe', 'COLUMN', N'Name'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldgroupe.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldgroupe.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldgroupe', 'COLUMN', N'Comment'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldgroupe.Id_Zonesgroup" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldgroupe.Id_Zonesgroup" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Группа контрольных зон на основании которой создана группа полей', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldgroupe', 'COLUMN', N'Id_Zonesgroup'
GO

--
-- Создать таблицу "dbo.agro_field"
--
PRINT (N'Создать таблицу "dbo.agro_field"')
GO
CREATE TABLE dbo.agro_field (
  Id int IDENTITY,
  Id_main int NULL,
  Id_zone int NOT NULL,
  Name varchar(50) NOT NULL CONSTRAINT DF__agro_field__Name__286302EC DEFAULT (''),
  Comment varchar(max) NULL,
  IsOutOfDate bit NOT NULL CONSTRAINT DF__agro_fiel__IsOut__29572725 DEFAULT (0),
  CONSTRAINT PK_agro_field PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "Id_main_Field_FK1" для объекта типа таблица "dbo.agro_field"
--
PRINT (N'Создать индекс "Id_main_Field_FK1" для объекта типа таблица "dbo.agro_field"')
GO
CREATE INDEX Id_main_Field_FK1
  ON dbo.agro_field (Id_main)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_field.Id_main" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_field.Id_main" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на группу полей', 'SCHEMA', N'dbo', 'TABLE', N'agro_field', 'COLUMN', N'Id_main'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_field.Id_zone" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_field.Id_zone" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на контрольную зону', 'SCHEMA', N'dbo', 'TABLE', N'agro_field', 'COLUMN', N'Id_zone'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_field.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_field.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Название', 'SCHEMA', N'dbo', 'TABLE', N'agro_field', 'COLUMN', N'Name'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_field.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_field.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_field', 'COLUMN', N'Comment'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_field.IsOutOfDate" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_field.IsOutOfDate" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Поле устарело и в расчетах не учавствует', 'SCHEMA', N'dbo', 'TABLE', N'agro_field', 'COLUMN', N'IsOutOfDate'
GO

--
-- Создать таблицу "dbo.agro_event_manually"
--
PRINT (N'Создать таблицу "dbo.agro_event_manually"')
GO
CREATE TABLE dbo.agro_event_manually (
  Id int IDENTITY,
  Id_mobitel int NOT NULL,
  EventName varchar(127) NULL,
  TimeStart smalldatetime NULL,
  TimeEnd smalldatetime NULL,
  Id_work int NULL,
  Info_1 varchar(127) NULL,
  Info_2 varchar(127) NULL,
  Info_3 varchar(127) NULL,
  Info_4 varchar(127) NULL,
  Info_5 varchar(127) NULL,
  CONSTRAINT PK_agro_event_manually PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.agro_culture"
--
PRINT (N'Создать таблицу "dbo.agro_culture"')
GO
CREATE TABLE dbo.agro_culture (
  Id int IDENTITY,
  Name char(50) NOT NULL DEFAULT (''),
  Icon varbinary(max) NULL,
  Comment varchar(max) NULL,
  CONSTRAINT PK_agro_culture PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_culture.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_culture.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Название', 'SCHEMA', N'dbo', 'TABLE', N'agro_culture', 'COLUMN', N'Name'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_culture.Icon" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_culture.Icon" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Иконка культуры', 'SCHEMA', N'dbo', 'TABLE', N'agro_culture', 'COLUMN', N'Icon'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_culture.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_culture.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_culture', 'COLUMN', N'Comment'
GO

--
-- Создать таблицу "dbo.agro_fieldculture"
--
PRINT (N'Создать таблицу "dbo.agro_fieldculture"')
GO
CREATE TABLE dbo.agro_fieldculture (
  Id int IDENTITY,
  Id_main int NULL,
  Id_culture int NOT NULL,
  Year smallint NOT NULL,
  CONSTRAINT fgn_key_Id_culture_FK2 FOREIGN KEY (Id_culture) REFERENCES dbo.agro_culture (Id),
  CONSTRAINT fgn_key_Id_main_FieldCulture_FK1 FOREIGN KEY (Id_main) REFERENCES dbo.agro_field (Id)
)
ON [PRIMARY]
GO

--
-- Создать индекс "Id_culture_FK2" для объекта типа таблица "dbo.agro_fieldculture"
--
PRINT (N'Создать индекс "Id_culture_FK2" для объекта типа таблица "dbo.agro_fieldculture"')
GO
CREATE INDEX Id_culture_FK2
  ON dbo.agro_fieldculture (Id_culture)
  ON [PRIMARY]
GO

--
-- Создать индекс "Id_main_FieldCulture_FK1" для объекта типа таблица "dbo.agro_fieldculture"
--
PRINT (N'Создать индекс "Id_main_FieldCulture_FK1" для объекта типа таблица "dbo.agro_fieldculture"')
GO
CREATE INDEX Id_main_FieldCulture_FK1
  ON dbo.agro_fieldculture (Id_main)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldculture.Id_main" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldculture.Id_main" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на поле', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldculture', 'COLUMN', N'Id_main'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldculture.Id_culture" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldculture.Id_culture" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на культуру', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldculture', 'COLUMN', N'Id_culture'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldculture.Year" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_fieldculture.Year" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Год обработки', 'SCHEMA', N'dbo', 'TABLE', N'agro_fieldculture', 'COLUMN', N'Year'
GO

--
-- Создать таблицу "dbo.agro_agregat"
--
PRINT (N'Создать таблицу "dbo.agro_agregat"')
GO
CREATE TABLE dbo.agro_agregat (
  Id int IDENTITY,
  Name varchar(max) NOT NULL DEFAULT (''),
  Width float NULL DEFAULT (0),
  Comment varchar(max) NOT NULL,
  Identifier int NULL,
  Def smallint NULL DEFAULT (0),
  Id_main int NOT NULL DEFAULT (0),
  IsGroupe smallint NOT NULL DEFAULT (0),
  InvNumber char(50) NULL,
  Id_work int NOT NULL DEFAULT (0),
  OutLinkId varchar(20) NULL,
  HasSensor smallint NULL DEFAULT (0),
  SensorAngleValue float NOT NULL DEFAULT (0),
  IsUseAgregatState bit NOT NULL DEFAULT (0),
  LogicState bit NOT NULL DEFAULT (0),
  MinStateValue decimal NOT NULL DEFAULT (0),
  MaxStateValue decimal NOT NULL DEFAULT (0),
  SensorAlgorithm int NOT NULL DEFAULT (0),
  CONSTRAINT PK_agro_agregat PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Name" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Name" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Название', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat', 'COLUMN', N'Name'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Width" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Width" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ширина навесного оборудования для подсчета обработанной площади ', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat', 'COLUMN', N'Width'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat', 'COLUMN', N'Comment'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Identifier" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Identifier" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'10 битный идентификатор агрегата', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat', 'COLUMN', N'Identifier'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Def" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Def" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Агрегат по умолчанию для нарядов', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat', 'COLUMN', N'Def'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Id_main" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Id_main" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на элемент верхнего уровня в иерархии', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat', 'COLUMN', N'Id_main'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.IsGroupe" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.IsGroupe" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Запись - группа', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat', 'COLUMN', N'IsGroupe'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.InvNumber" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.InvNumber" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Инвентарный номер агрегата', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat', 'COLUMN', N'InvNumber'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Id_work" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.Id_work" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид работ по умолчанию', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat', 'COLUMN', N'Id_work'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.OutLinkId" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.OutLinkId" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Код внешней базы', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat', 'COLUMN', N'OutLinkId'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.HasSensor" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat.HasSensor" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Имеет датчик идентификации рабочего состояния', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat', 'COLUMN', N'HasSensor'
GO

--
-- Создать процедуру "dbo.zvit"
--
GO
PRINT (N'Создать процедуру "dbo.zvit"')
GO
CREATE PROCEDURE dbo.zvit (@z datetime, @po datetime, @blok char(50)) 
AS
	SELECT CAST(Date AS Date), SUBSTRING(Title, 1, CHARINDEX('_', Title, 1)-1), work, NumberPlate, agreg, zony, LockRecord, plos
FROM (SELECT TOP 10000 a_ord.Date, zgr.Title, a_wk.Name work, veh.NumberPlate, a_agr.Name agreg, zones.Name zony, a_ordt.LockRecord, Round(Sum(a_ordt.FactSquareCalc), 2) plos 
FROM agro_ordert a_ordt 
LEFT OUTER JOIN agro_order a_ord ON a_ordt.Id_main = a_ord.Id  
LEFT OUTER JOIN agro_agregat a_agr ON a_ordt.Id_agregat=a_agr.Id  
LEFT OUTER JOIN agro_work a_wk ON a_ordt.Id_work=a_wk.Id  
LEFT OUTER JOIN vehicle veh ON a_ord.Id_mobitel=veh.Mobitel_id  
LEFT OUTER JOIN team_new team ON veh.Team_id = team.id  
LEFT OUTER JOIN zones ON a_ordt.Id_zone=zones.Zone_ID  
LEFT OUTER JOIN zonesgroup zgr ON zones.ZonesGroupId=zgr.Id  
WHERE a_ord.Date between @z And @po  AND zgr.Title LIKE '%[_]%' AND (team.groupTeam=1) AND (a_ordt.Confirm=1)  
GROUP BY zgr.Title, a_wk.Name, a_ord.Date, veh.NumberPlate, a_agr.Name, zones.Name, a_agr.Width, a_ordt.LockRecord  
ORDER BY zgr.Title, a_wk.Name, veh.NumberPlate, a_agr.Name) t1  
WHERE SUBSTRING(Title, 1, CHARINDEX('_', Title, 1)-1) = @blok
GO

--
-- Создать представление "dbo.ua_agregati"
--
GO
PRINT (N'Создать представление "dbo.ua_agregati"')
GO
CREATE VIEW dbo.ua_agregati 
AS SELECT agro_agregat.Comment AS Blok
        , agro_agregat1.Name AS Vid_agregat
        , agro_agregat.Name AS agregat
        , agro_work.Name AS work
   FROM
     agro_agregat
     INNER JOIN agro_work
       ON agro_work.Id = agro_agregat.Id_work
     INNER JOIN agro_agregat agro_agregat1
       ON agro_agregat1.Id = agro_agregat.Id_main
   WHERE
     agro_agregat.Comment <> ''
GO

--
-- Создать таблицу "dbo.agro_pricet"
--
PRINT (N'Создать таблицу "dbo.agro_pricet"')
GO
CREATE TABLE dbo.agro_pricet (
  Id int IDENTITY,
  Id_main int NOT NULL,
  Id_work int NOT NULL,
  Price float NULL DEFAULT (0.00),
  Id_unit int NOT NULL,
  Id_mobitel int NOT NULL,
  Id_agregat int NULL DEFAULT (0),
  Comment varchar(max) NULL,
  CONSTRAINT fgn_key_Id_agregat_Price_FK5 FOREIGN KEY (Id_agregat) REFERENCES dbo.agro_agregat (Id),
  CONSTRAINT fgn_key_Id_main_FK1 FOREIGN KEY (Id_main) REFERENCES dbo.agro_price (Id),
  CONSTRAINT fgn_key_Id_mobitel_FK3 FOREIGN KEY (Id_mobitel) REFERENCES dbo.mobitels (Mobitel_ID),
  CONSTRAINT fgn_key_Id_unit_FK4 FOREIGN KEY (Id_unit) REFERENCES dbo.agro_unit (Id),
  CONSTRAINT fgn_key_Id_work_FK2 FOREIGN KEY (Id_work) REFERENCES dbo.agro_work (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

--
-- Создать индекс "Id_agregat_Price_FK5" для объекта типа таблица "dbo.agro_pricet"
--
PRINT (N'Создать индекс "Id_agregat_Price_FK5" для объекта типа таблица "dbo.agro_pricet"')
GO
CREATE INDEX Id_agregat_Price_FK5
  ON dbo.agro_pricet (Id_agregat)
  ON [PRIMARY]
GO

--
-- Создать индекс "Id_main_FK1" для объекта типа таблица "dbo.agro_pricet"
--
PRINT (N'Создать индекс "Id_main_FK1" для объекта типа таблица "dbo.agro_pricet"')
GO
CREATE INDEX Id_main_FK1
  ON dbo.agro_pricet (Id_main)
  ON [PRIMARY]
GO

--
-- Создать индекс "Id_mobitel_FK3" для объекта типа таблица "dbo.agro_pricet"
--
PRINT (N'Создать индекс "Id_mobitel_FK3" для объекта типа таблица "dbo.agro_pricet"')
GO
CREATE INDEX Id_mobitel_FK3
  ON dbo.agro_pricet (Id_mobitel)
  ON [PRIMARY]
GO

--
-- Создать индекс "Id_unit_FK4" для объекта типа таблица "dbo.agro_pricet"
--
PRINT (N'Создать индекс "Id_unit_FK4" для объекта типа таблица "dbo.agro_pricet"')
GO
CREATE INDEX Id_unit_FK4
  ON dbo.agro_pricet (Id_unit)
  ON [PRIMARY]
GO

--
-- Создать индекс "Id_work_FK2" для объекта типа таблица "dbo.agro_pricet"
--
PRINT (N'Создать индекс "Id_work_FK2" для объекта типа таблица "dbo.agro_pricet"')
GO
CREATE INDEX Id_work_FK2
  ON dbo.agro_pricet (Id_work)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Id_main" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Id_main" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на приказ', 'SCHEMA', N'dbo', 'TABLE', N'agro_pricet', 'COLUMN', N'Id_main'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Id_work" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Id_work" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на работу', 'SCHEMA', N'dbo', 'TABLE', N'agro_pricet', 'COLUMN', N'Id_work'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Price" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Price" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Расценка', 'SCHEMA', N'dbo', 'TABLE', N'agro_pricet', 'COLUMN', N'Price'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Id_unit" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Id_unit" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на единицу измерения', 'SCHEMA', N'dbo', 'TABLE', N'agro_pricet', 'COLUMN', N'Id_unit'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Id_mobitel" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Id_mobitel" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Транспортное средство', 'SCHEMA', N'dbo', 'TABLE', N'agro_pricet', 'COLUMN', N'Id_mobitel'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Id_agregat" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Id_agregat" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Навесное оборудование', 'SCHEMA', N'dbo', 'TABLE', N'agro_pricet', 'COLUMN', N'Id_agregat'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Comment" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_pricet.Comment" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'agro_pricet', 'COLUMN', N'Comment'
GO

--
-- Создать таблицу "dbo.agro_agregat_vehicle"
--
PRINT (N'Создать таблицу "dbo.agro_agregat_vehicle"')
GO
CREATE TABLE dbo.agro_agregat_vehicle (
  Id int IDENTITY,
  Id_agregat int NOT NULL DEFAULT (0),
  Id_vehicle int NOT NULL DEFAULT (0),
  Id_sensor int NOT NULL DEFAULT (0),
  Id_state bit NOT NULL DEFAULT (0),
  CONSTRAINT fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id FOREIGN KEY (Id_agregat) REFERENCES dbo.agro_agregat (Id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fgn_key_FK_agro_agregat_vehicle_vehicle_id FOREIGN KEY (Id_vehicle) REFERENCES dbo.vehicle (id) ON DELETE CASCADE ON UPDATE CASCADE
)
ON [PRIMARY]
GO

--
-- Создать индекс "FK_agro_agregat_vehicle_agro_agregat_Id" для объекта типа таблица "dbo.agro_agregat_vehicle"
--
PRINT (N'Создать индекс "FK_agro_agregat_vehicle_agro_agregat_Id" для объекта типа таблица "dbo.agro_agregat_vehicle"')
GO
CREATE INDEX FK_agro_agregat_vehicle_agro_agregat_Id
  ON dbo.agro_agregat_vehicle (Id_agregat)
  ON [PRIMARY]
GO

--
-- Создать индекс "FK_agro_agregat_vehicle_vehicle_id" для объекта типа таблица "dbo.agro_agregat_vehicle"
--
PRINT (N'Создать индекс "FK_agro_agregat_vehicle_vehicle_id" для объекта типа таблица "dbo.agro_agregat_vehicle"')
GO
CREATE INDEX FK_agro_agregat_vehicle_vehicle_id
  ON dbo.agro_agregat_vehicle (Id_vehicle)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat_vehicle.Id_sensor" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.agro_agregat_vehicle.Id_sensor" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Датчик транспортного средства', 'SCHEMA', N'dbo', 'TABLE', N'agro_agregat_vehicle', 'COLUMN', N'Id_sensor'
GO

--
-- Создать таблицу "dbo.online"
--
PRINT (N'Создать таблицу "dbo.online"')
GO
CREATE TABLE dbo.online (
  DataGps_ID int IDENTITY,
  Mobitel_ID int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  Latitude int NOT NULL DEFAULT (0),
  Longitude int NOT NULL DEFAULT (0),
  Altitude int NULL DEFAULT (0),
  UnixTime int NOT NULL DEFAULT (0),
  Speed smallint NOT NULL DEFAULT (0),
  Direction int NOT NULL DEFAULT (0),
  Valid smallint NOT NULL DEFAULT (1),
  InMobitelID int NULL DEFAULT (0),
  Events bigint NULL DEFAULT (0),
  Sensor1 tinyint NULL DEFAULT (0),
  Sensor2 tinyint NULL DEFAULT (0),
  Sensor3 tinyint NULL DEFAULT (0),
  Sensor4 tinyint NULL DEFAULT (0),
  Sensor5 tinyint NULL DEFAULT (0),
  Sensor6 tinyint NULL DEFAULT (0),
  Sensor7 tinyint NULL DEFAULT (0),
  Sensor8 tinyint NULL DEFAULT (0),
  LogID int NULL DEFAULT (0),
  isShow smallint NULL DEFAULT (0),
  whatIs int NOT NULL DEFAULT (0),
  Counter1 int NOT NULL DEFAULT (-1),
  Counter2 int NOT NULL DEFAULT (-1),
  Counter3 int NOT NULL DEFAULT (-1),
  Counter4 int NOT NULL DEFAULT (-1)
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_Mobitelid" для объекта типа таблица "dbo.online"
--
PRINT (N'Создать индекс "IDX_Mobitelid" для объекта типа таблица "dbo.online"')
GO
CREATE INDEX IDX_Mobitelid
  ON dbo.online (Mobitel_ID)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidLogid" для объекта типа таблица "dbo.online"
--
PRINT (N'Создать индекс "IDX_MobitelidLogid" для объекта типа таблица "dbo.online"')
GO
CREATE INDEX IDX_MobitelidLogid
  ON dbo.online (Mobitel_ID, LogID)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidUnixtime" для объекта типа таблица "dbo.online"
--
PRINT (N'Создать индекс "IDX_MobitelidUnixtime" для объекта типа таблица "dbo.online"')
GO
CREATE INDEX IDX_MobitelidUnixtime
  ON dbo.online (Mobitel_ID, UnixTime)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidUnixtimeValid" для объекта типа таблица "dbo.online"
--
PRINT (N'Создать индекс "IDX_MobitelidUnixtimeValid" для объекта типа таблица "dbo.online"')
GO
CREATE INDEX IDX_MobitelidUnixtimeValid
  ON dbo.online (Mobitel_ID, UnixTime, Valid)
  ON [PRIMARY]
GO

--
-- Создать представление "dbo.id_gps_id_mobitel"
--
GO
PRINT (N'Создать представление "dbo.id_gps_id_mobitel"')
GO
CREATE VIEW dbo.id_gps_id_mobitel 
AS



SELECT max(online.DataGps_ID) AS expr1
     , online.Mobitel_ID
FROM
  dbo.online
GROUP BY
  online.Mobitel_ID
GO

--
-- Создать таблицу "dbo.datagpsbuffer_ontmp"
--
PRINT (N'Создать таблицу "dbo.datagpsbuffer_ontmp"')
GO
CREATE TABLE dbo.datagpsbuffer_ontmp (
  Mobitel_ID int NOT NULL DEFAULT (0),
  LogID int NOT NULL DEFAULT (0),
  SrvPacketID bigint NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_Mobitelid" для объекта типа таблица "dbo.datagpsbuffer_ontmp"
--
PRINT (N'Создать индекс "IDX_Mobitelid" для объекта типа таблица "dbo.datagpsbuffer_ontmp"')
GO
CREATE INDEX IDX_Mobitelid
  ON dbo.datagpsbuffer_ontmp (Mobitel_ID)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidSrvpacketid" для объекта типа таблица "dbo.datagpsbuffer_ontmp"
--
PRINT (N'Создать индекс "IDX_MobitelidSrvpacketid" для объекта типа таблица "dbo.datagpsbuffer_ontmp"')
GO
CREATE INDEX IDX_MobitelidSrvpacketid
  ON dbo.datagpsbuffer_ontmp (Mobitel_ID, SrvPacketID)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.datagpslost_ontmp"
--
PRINT (N'Создать таблицу "dbo.datagpslost_ontmp"')
GO
CREATE TABLE dbo.datagpslost_ontmp (
  LogID int NOT NULL DEFAULT (0),
  SrvPacketID bigint NOT NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.datagpslost_on"
--
PRINT (N'Создать таблицу "dbo.datagpslost_on"')
GO
CREATE TABLE dbo.datagpslost_on (
  Mobitel_ID int NOT NULL DEFAULT (0),
  Begin_LogID int NOT NULL DEFAULT (0),
  End_LogID int NOT NULL DEFAULT (0),
  Begin_SrvPacketID bigint NOT NULL DEFAULT (0),
  End_SrvPacketID bigint NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelID" для объекта типа таблица "dbo.datagpslost_on"
--
PRINT (N'Создать индекс "IDX_MobitelID" для объекта типа таблица "dbo.datagpslost_on"')
GO
CREATE INDEX IDX_MobitelID
  ON dbo.datagpslost_on (Mobitel_ID)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelID_BeginSrvPacketID" для объекта типа таблица "dbo.datagpslost_on"
--
PRINT (N'Создать индекс "IDX_MobitelID_BeginSrvPacketID" для объекта типа таблица "dbo.datagpslost_on"')
GO
CREATE INDEX IDX_MobitelID_BeginSrvPacketID
  ON dbo.datagpslost_on (Mobitel_ID, Begin_SrvPacketID)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagpslost_on.Begin_LogID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagpslost_on.Begin_LogID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение LogID после которого начинается разрыв', 'SCHEMA', N'dbo', 'TABLE', N'datagpslost_on', 'COLUMN', N'Begin_LogID'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagpslost_on.End_LogID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagpslost_on.End_LogID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение LogID перед которым завершается разрыв', 'SCHEMA', N'dbo', 'TABLE', N'datagpslost_on', 'COLUMN', N'End_LogID'
GO

--
-- Создать процедуру "dbo.OnInsertDatagpsLost"
--
GO
PRINT (N'Создать процедуру "dbo.OnInsertDatagpsLost"')
GO


CREATE PROCEDURE dbo.OnInsertDatagpsLost
(
  @MobitelID        INT,
  @BeginLogID       INT,
  @EndLogID         INT,
  @BeginSrvPacketID BIGINT,
  @EndSrvPacketID   BIGINT
)
AS
--SQL SECURITY INVOKER
--COMMENT 'Вставка данных в таблицу datagpslost_on.'
BEGIN
  DECLARE @MobitelId_BeginLogID_Exists TINYINT;
  SET @MobitelId_BeginLogID_Exists = 0;
  DECLARE @MobitelId_BeginSrvPacketID_Exists TINYINT;
  SET @MobitelId_BeginSrvPacketID_Exists = 0;

  DECLARE @RowBeginSrvPacketID BIGINT;

  IF EXISTS (SELECT NULL
             FROM
               datagpslost_on
             WHERE
               (Mobitel_ID = @MobitelID)
               AND (Begin_LogID = @BeginLogID))
  BEGIN
    SET @MobitelId_BeginLogID_Exists = 1;
  END; -- IF

  IF EXISTS (SELECT NULL
             FROM
               datagpslost_on
             WHERE
               (Mobitel_ID = @MobitelID)
               AND (Begin_SrvPacketID = @BeginSrvPacketID))
  BEGIN
    SET @MobitelId_BeginSrvPacketID_Exists = 1;
  END; -- IF

  IF (@MobitelId_BeginLogID_Exists = 0) AND (@MobitelId_BeginSrvPacketID_Exists = 0)
  BEGIN
    /* ??????? ????? ?????? */
    INSERT INTO datagpslost_on(Mobitel_ID
                             , Begin_LogID
                             , End_LogID
                             , Begin_SrvPacketID
                             , End_SrvPacketID)
    VALUES
      (@MobitelID, @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID);
  END
  ELSE
  IF ((@MobitelId_BeginLogID_Exists = 1) AND (@MobitelId_BeginSrvPacketID_Exists = 1))
  BEGIN
    UPDATE datagpslost_on
    SET
      End_LogID = @EndLogID, End_SrvPacketID = @EndSrvPacketID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (Begin_LogID = @BeginLogID);
  END
  ELSE
  BEGIN
    SET @RowBeginSrvPacketID = (SELECT TOP 1 Begin_SrvPacketID
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (Begin_LogID = @BeginLogID));

    IF ((@MobitelId_BeginLogID_Exists = 1) AND (@MobitelId_BeginSrvPacketID_Exists = 0) AND (@BeginSrvPacketID < @RowBeginSrvPacketID))
    BEGIN
      UPDATE datagpslost_on
      SET
        End_LogID = @EndLogID, Begin_SrvPacketID = @BeginSrvPacketID, End_SrvPacketID = @EndSrvPacketID
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);
    END; -- IF
  END; -- ELSE
END
--END


GO

--
-- Создать процедуру "dbo.OnLostDataGPS64"
--
GO
PRINT (N'Создать процедуру "dbo.OnLostDataGPS64"')
GO


CREATE PROCEDURE dbo.OnLostDataGPS64
(
  @MobitelID    INT,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT 'Поиск пропусков в online данных заданного телетрека и заполнение'
BEGIN

  /********** DECLARE **********/

  DECLARE @CurrentConfirmedID INT; /* Текущее значение ConfirmedID  */
  SET @CurrentConfirmedID = 0;
  DECLARE @NewConfirmedID INT; /* Новое значение ConfirmedID */
  SET @NewConfirmedID = 0;
  DECLARE @StartLogIDSelect INT; /* min LogID для выборки */
  SET @StartLogIDSelect = 0;
  DECLARE @FinishLogIDSelect INT; /* max LogID для выборки */
  SET @FinishLogIDSelect = 0;
  DECLARE @MaxEndLogID INT; /* max End_LogID в пропущенных диапазонах данного телетрека */
  SET @MaxEndLogID = 0;

  DECLARE @MinLogID_n INT; /* Минимальный LogID после ConfirmedID в множестве новых данных */
  SET @MinLogID_n = 0;
  DECLARE @MaxLogID_n INT; /* Максимальный LogID в множестве новых данных */
  SET @MaxLogID_n = 0;

  DECLARE @DataExists TINYINT; /*  Флаг наличия данных после фетча */
  SET @DataExists = 1;

  DECLARE @NewLogID INT; /* Значение LogID в курсоре CursorNewLogID */
  DECLARE @NewSrvPacketID BIGINT; /*  Значение SrvPacketID в курсоре CursorNewLogID */
  DECLARE @FirstLogID INT; /* Первое значение LogID для анализа*/
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* Второе значение LogID для анализа*/
  DECLARE @FirstSrvPacketID BIGINT; /* Первое значение SrvPacketID*/
  DECLARE @SecondSrvPacketID BIGINT; /* Второе значение SrvPacketID*/

  DECLARE @tmpSrvPacketID BIGINT; /* Значение SrvPacketID для первой искусственной записи в анализе новых данных, не пересекающихся с существующими*/
  SET @tmpSrvPacketID = 0;

  /* Курсор для прохода по новым LogID  */
  DECLARE CursorNewLogID CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  SET @CurrentConfirmedID = (SELECT coalesce(ConfirmedID, 0)
                             FROM
                               mobitels
                             WHERE
                               Mobitel_ID = @MobitelID);

  /*  Минимальный LogID в новом наборе данных c которым будем работать */
  SELECT @MinLogID_n = min(LogID)
  FROM
    datagpsbuffer_on64
  WHERE
    (Mobitel_ID = @MobitelID)
    AND (LogID > @CurrentConfirmedID);

  /* Максимальный End_LogID в пропущенных диапазонах данного телетрека*/
  SET @MaxLogID_n = (SELECT max(LogID)
                     FROM
                       datagpsbuffer_on64
                     WHERE
                       Mobitel_ID = @MobitelID);

  /* Максимальный End_LogID в пропущенных диапазонах данного телетрека  */
  SELECT @MaxEndLogID = coalesce(max(End_LogID), 0)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* Проверим есть ли данные для анализа */
  IF @MinLogID_n IS NOT NULL
  BEGIN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF @MinLogID_n < @MaxEndLogID
    BEGIN
      /* Если  MinLogID_n < MaxEndLogID значит пришли данные, 
       которые возможно закроют часть пропусков. */
       
      /* Находим стартовый LogID для пересчета. */
      SET @StartLogIDSelect = (SELECT coalesce(Begin_LogID, @MinLogID_n)
                               FROM
                                 datagpslost_on
                               WHERE
                                 (Mobitel_ID = @MobitelID)
                                 AND (@MinLogID_n > Begin_LogID)
                                 AND (@MinLogID_n < End_LogID));

      /* Находим последний LogID для пересчета. */
      SET @FinishLogIDSelect = (SELECT coalesce(End_LogID, @MaxLogID_n)
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (@MaxLogID_n > Begin_LogID)
                                  AND (@MaxLogID_n < End_LogID));

      /* Удалим старые записи из реестра пропущенных записей 
         (стали неактуальными, надо пересчитать) */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID >= @StartLogIDSelect) AND
        (Begin_LogID <= @FinishLogIDSelect);

      /* Если первая запись LogID = 0 и этой записи нет в DataGps, тогда
         надо добавить первую фиктивную запись для анализа. */
      IF (@StartLogIDSelect = 0) AND (NOT EXISTS (SELECT 1
                                                  FROM
                                                    datagps64
                                                  WHERE
                                                    (Mobitel_ID = @MobitelID)
                                                    AND (LogID = @StartLogIDSelect)))
      BEGIN
        INSERT INTO datagpslost_ontmp(LogID
                                    , SrvPacketID)
        VALUES
          (0, 0);
      END;
    END;
    ELSE
    BEGIN

      /* Если  MinLogID_n < MaxEndLogID значит пришли данные, 
       которые возможно закроют часть пропусков. */
       
      /* Находим стартовый LogID для пересчета. */
      SELECT @StartLogIDSelect = coalesce(max(End_LogID), @CurrentConfirmedID)
      FROM
        datagpslost_on
      WHERE
        Mobitel_ID = @MobitelID;

      /* Находим последний LogID для пересчета. */  
      SELECT @FinishLogIDSelect = max(LogID)
      FROM
        datagpsbuffer_on64
      WHERE
        Mobitel_ID = @MobitelID;

      /* Удалим старые записи из реестра пропущенных записей 
         (стали неактуальными, надо пересчитать) */
      SELECT @tmpSrvPacketID = coalesce(SrvPacketID, 0)
      FROM
        datagps64
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (LogID = @StartLogIDSelect);

      /* Помещаем в таблицу datagpslost_ontmp для анализа пропусков 
         дополнительную первую запись */
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@StartLogIDSelect, @tmpSrvPacketID);
    END; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

    /* Заполнение временной таблицы последними данными телетрека
       для анализа разрывов */
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT LogID
         , SrvPacketID
    FROM
      datagps64
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (LogID >= @StartLogIDSelect)
      AND (LogID <= @FinishLogIDSelect)
    ORDER BY
      LogID;

     /* Формирование команды-вставки в таблицу пропущенных диапазонов */
    --SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstLogID = -9999999
      BEGIN
        /* Первое значение в списке */
        SET @FirstLogID = @NewLogID;
        SET @FirstSrvPacketID = @NewSrvPacketID;
      END
      ELSE
      BEGIN
        /* Основная ветка расчета*/
        SET @SecondLogID = @NewLogID;
        SET @SecondSrvPacketID = @NewSrvPacketID;

        IF (@SecondLogID - @FirstLogID) > 1
        BEGIN
          /* Найден разрыв */
          EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

          IF @NeedOptimize = 0
          BEGIN
            SET @NeedOptimize = 1;
          END; -- IF

        END; -- IF
        /* Переместим текущее второе значение на первую позицию
           для участия в следующей итерации в качестве начального значения */
        SET @FirstLogID = @SecondLogID;
        SET @FirstSrvPacketID = @SecondSrvPacketID;
      END; -- IF
      FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    END; -- WHILE
    CLOSE CursorNewLogID;
    --DEALLOCATE CursorNewLogID; 

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

    /* Если пропущенных записей у данного телетрека нет - 
       тогда ConfirmedID будет равен максимальному LogID 
       данного телетрека. Иначе - минимальный Begin_LogID
       из таблицы пропусков */
    SET @NewConfirmedID = (SELECT min(Begin_LogID)
                           FROM
                             datagpslost_on
                           WHERE
                             Mobitel_ID = @MobitelID);

    IF @NewConfirmedID IS NULL
    BEGIN
      SET @NewConfirmedID = (SELECT max(LogID)
                             FROM
                               datagps64
                             WHERE
                               Mobitel_ID = @MobitelID);
    END;

    /* Обновим ConfirmedID если надо */
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (ConfirmedID < @NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END; -- IF MaxEndLogID IS NOT NULL  
END

GO

--
-- Создать процедуру "dbo.OnLostDataGPS264"
--
GO
PRINT (N'Создать процедуру "dbo.OnLostDataGPS264"')
GO


CREATE PROCEDURE dbo.OnLostDataGPS264
(
  @MobitelID    INTEGER,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT 'Модификация диапазонов пропусков. Анализ дублирующих данных заданного телетрека.'
BEGIN
  /********** DECLARE **********/

  DECLARE @NewConfirmedID INT;
  SET @NewConfirmedID = 0; /* Новое значение ConfirmedID  */
  DECLARE @BeginLogID INT; /* Begin_LogID в курсоре CursorLostRanges */
  DECLARE @EndLogID INT; /* End_LogID в курсоре CursorLostRanges */
  DECLARE @BeginSrvPacketID BIGINT; /*  Begin_SrvPacketID в курсоре CursorLostRanges */
  DECLARE @EndSrvPacketID BIGINT; /* End_SrvPacketID в курсоре CursorLostRanges  */
  DECLARE @RowCountForAnalyze INT; /* Количество записей для анализа  */
  SET @RowCountForAnalyze = 0;
  DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  SET @DataExists = 1;
  DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
  DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
  DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
  DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
  DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

  /* Курсор для прохода по диапазонам пропусков телетрека */
  DECLARE CursorLostRanges CURSOR FOR
  SELECT Begin_LogID
       , End_LogID
       , Begin_SrvPacketID
       , End_SrvPacketID
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* Курсор для прохода по новым LogID */
  DECLARE CursorNewLogID2 CURSOR FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp
  ORDER BY
    SrvPacketID;

  /* Обработчик отсутствующей записи при FETCH следующей записи*/
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* Перебираем диапазоны и сокращаем разрывы если это возможно  */
  SET @DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;

  WHILE @@fetch_status = 0
  BEGIN
    /* Заполнение временной таблицы дублирующими данными телетрека
       для анализа разрывов */
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT DISTINCT LogID
                  , SrvPacketID
    FROM
      datagpsbuffer_ontmp64
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (SrvPacketID > @BeginSrvPacketID)
      AND (SrvPacketID < @EndSrvPacketID)
    ORDER BY
      SrvPacketID;

     /* Если есть данные модифицируем диапазон */  
    SET @RowCountForAnalyze = (SELECT count(1)
                               FROM
                                 datagpslost_ontmp);

    IF @RowCountForAnalyze > 0
    BEGIN
      /* Добавляем первую и последнюю записи для анализа */ 
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@BeginLogID, @BeginSrvPacketID)
      , (@EndLogID, @EndSrvPacketID);

       /* Удаляем старый диапазон */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        --DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
        --DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
        --DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --SET @FirstLogID = -9999999;
        --DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
        --DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

        /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
        --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* Формирование команды-вставки в таблицу пропущенных диапазонов */
        --SET DataExists = 1;
        OPEN CursorNewLogID2;
        FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;

        WHILE @@fetch_status = 0
        BEGIN
          IF @FirstLogID = -9999999
          BEGIN
            /* Первое значение в списке */
            SET @FirstLogID = @NewLogID;
            SET @FirstSrvPacketID = @NewSrvPacketID;
          END
          ELSE
          BEGIN
            /* Основная ветка расчета*/
            SET @SecondLogID = @NewLogID;
            SET @SecondSrvPacketID = @NewSrvPacketID;

            IF (@SecondSrvPacketID - @FirstSrvPacketID) > 1
            BEGIN
              /* Найден разрыв */
              EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

              IF @NeedOptimize = 0
              BEGIN
                SET @NeedOptimize = 1;
              END; -- IF

            END; -- IF
            /* Переместим текущее второе значение на первую позицию
               для участия в следующей итерации в качестве начального значения */
            SET @FirstLogID = @SecondLogID;
            SET @FirstSrvPacketID = @SecondSrvPacketID;
          END; -- ELSE  
          FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;
        END; -- WHILE;
        CLOSE CursorNewLogID2;
      --DEALLOCATE CursorNewLogID2;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END;

    FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;
  END;
  CLOSE CursorLostRanges;
  --DEALLOCATE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* Если пропущенных записей у данного телетрека нет - 
     тогда ConfirmedID будет равен максимальному LogID 
     данного телетрека. Иначе - минимальный Begin_LogID
     из таблицы пропусков */
  SELECT @NewConfirmedID = min(Begin_LogID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @NewConfirmedID IS NULL
  BEGIN
    SET @NewConfirmedID = (SELECT max(LogID)
                           FROM
                             datagps64
                           WHERE
                             Mobitel_ID = @MobitelID);
  END;

  /* Обновим ConfirmedID если надо */
  UPDATE mobitels
  SET
    ConfirmedID = @NewConfirmedID
  WHERE
    (Mobitel_ID = @MobitelID) AND
    (ConfirmedID < @NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END

GO

--
-- Создать процедуру "dbo.OnDeleteLostRange"
--
GO
PRINT (N'Создать процедуру "dbo.OnDeleteLostRange"')
GO

CREATE PROCEDURE dbo.OnDeleteLostRange(  @MobitelID int, @BeginSrvPacketID BIGINT)
--COMMENT 'Удаление указанного диапазона пропусков из datagpslost_on.'
AS
BEGIN

  DECLARE @MinBeginSrvPacketID BIGINT; -- 
  DECLARE @NewConfirmedID INT; -- 

  SELECT @MinBeginSrvPacketID = min(Begin_SrvPacketID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @MinBeginSrvPacketID IS NOT NULL
  BEGIN
    IF @MinBeginSrvPacketID = @BeginSrvPacketID
    BEGIN
      SELECT @NewConfirmedID = End_LogID
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (Begin_SrvPacketID = @BeginSrvPacketID);
    END; -- IF

    DELETE
    FROM
      datagpslost_on
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (Begin_SrvPacketID = @BeginSrvPacketID);

    IF @MinBeginSrvPacketID = @BeginSrvPacketID
    BEGIN
      UPDATE mobitels
      SET
        ConfirmedID = @NewConfirmedID
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (@NewConfirmedID > ConfirmedID);
    END; -- IF
  END; -- IF  
END

GO

--
-- Создать таблицу "dbo.datagpsbuffer_on"
--
PRINT (N'Создать таблицу "dbo.datagpsbuffer_on"')
GO
CREATE TABLE dbo.datagpsbuffer_on (
  Mobitel_ID int NOT NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  Latitude int NOT NULL DEFAULT (0),
  Longitude int NOT NULL DEFAULT (0),
  Altitude int NULL,
  UnixTime int NOT NULL DEFAULT (0),
  Speed smallint NOT NULL DEFAULT (0),
  Direction int NOT NULL DEFAULT (0),
  Valid smallint NOT NULL DEFAULT (1),
  InMobitelID int NULL,
  Events bigint NULL DEFAULT (0),
  Sensor1 tinyint NULL DEFAULT (0),
  Sensor2 tinyint NULL DEFAULT (0),
  Sensor3 tinyint NULL DEFAULT (0),
  Sensor4 tinyint NULL DEFAULT (0),
  Sensor5 tinyint NULL DEFAULT (0),
  Sensor6 tinyint NULL DEFAULT (0),
  Sensor7 tinyint NULL DEFAULT (0),
  Sensor8 tinyint NULL DEFAULT (0),
  LogID int NOT NULL DEFAULT (0),
  isShow smallint NULL DEFAULT (0),
  whatIs smallint NOT NULL DEFAULT (0),
  Counter1 int NOT NULL DEFAULT (-1),
  Counter2 int NOT NULL DEFAULT (-1),
  Counter3 int NOT NULL DEFAULT (-1),
  Counter4 int NOT NULL DEFAULT (-1),
  SrvPacketID bigint NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidLogid" для объекта типа таблица "dbo.datagpsbuffer_on"
--
PRINT (N'Создать индекс "IDX_MobitelidLogid" для объекта типа таблица "dbo.datagpsbuffer_on"')
GO
CREATE INDEX IDX_MobitelidLogid
  ON dbo.datagpsbuffer_on (Mobitel_ID, LogID)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidUnixtime" для объекта типа таблица "dbo.datagpsbuffer_on"
--
PRINT (N'Создать индекс "IDX_MobitelidUnixtime" для объекта типа таблица "dbo.datagpsbuffer_on"')
GO
CREATE INDEX IDX_MobitelidUnixtime
  ON dbo.datagpsbuffer_on (Mobitel_ID, UnixTime)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidUnixtimeValid" для объекта типа таблица "dbo.datagpsbuffer_on"
--
PRINT (N'Создать индекс "IDX_MobitelidUnixtimeValid" для объекта типа таблица "dbo.datagpsbuffer_on"')
GO
CREATE INDEX IDX_MobitelidUnixtimeValid
  ON dbo.datagpsbuffer_on (Mobitel_ID, UnixTime, Valid)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_SrvPacketID" для объекта типа таблица "dbo.datagpsbuffer_on"
--
PRINT (N'Создать индекс "IDX_SrvPacketID" для объекта типа таблица "dbo.datagpsbuffer_on"')
GO
CREATE INDEX IDX_SrvPacketID
  ON dbo.datagpsbuffer_on (SrvPacketID)
  ON [PRIMARY]
GO

--
-- Создать процедуру "dbo.tmpAddIndex"
--
GO
PRINT (N'Создать процедуру "dbo.tmpAddIndex"')
GO


CREATE PROCEDURE dbo.tmpAddIndex
AS
--COMMENT 'Временная процедура добавления индекса в DataGpsBuffer_on'
BEGIN
  DECLARE @DbName VARCHAR(64); /* Наименование текущей БД */
  DECLARE @IndexExist TINYINT;
  SET @IndexExist = 0;

  SET @DbName = (SELECT db_name(@@procid));

  SET @IndexExist = (SELECT count(1)
                     FROM
                       sys.indexes i
                     WHERE
                       i.name = 'IDX_SrvPacketID');

  IF @IndexExist = 0
  BEGIN
    CREATE INDEX IDX_SrvPacketID ON DataGpsBuffer_on (SrvPacketID);
    ALTER TABLE DataGpsBuffer_on ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON);
  END;
END

--EXEC tmpAddIndex
--DROP PROCEDURE IF EXISTS tmpAddIndex$$

GO

--
-- Создать процедуру "dbo.OnDeleteDuplicates"
--
GO
PRINT (N'Создать процедуру "dbo.OnDeleteDuplicates"')
GO

CREATE PROCEDURE dbo.OnDeleteDuplicates
AS
BEGIN
  DECLARE @MobitelIdInCursor INT;
  SET @MobitelIdInCursor = 0;
  DECLARE @LogIdInCursor INT;
  SET @LogIdInCursor = 0;
  DECLARE @MaxSrvPacketId BIGINT;
  SET @MaxSrvPacketId = 0;
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  DECLARE CursorDuplicateGroups CURSOR LOCAL FOR
  SELECT Mobitel_Id
       , LogId
  FROM
    datagpsbuffer_on
  GROUP BY
    Mobitel_Id
  , LogId
  HAVING
    count(LogId) > 1;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET @DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SELECT @MaxSrvPacketId = max(SrvPacketID)
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor)
      AND (LogId = @LogIdInCursor);
    DELETE
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor) AND
      (LogId = @LogIdInCursor) AND
      (SrvPacketID < @MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  END;
  CLOSE CursorDuplicateGroups;
--DEALLOCATE CursorDuplicateGroups;
END

GO

--
-- Создать таблицу "dbo.datagps"
--
PRINT (N'Создать таблицу "dbo.datagps"')
GO
CREATE TABLE dbo.datagps (
  DataGps_ID int IDENTITY,
  Mobitel_ID int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  Latitude int NULL DEFAULT (0),
  Longitude int NULL DEFAULT (0),
  Altitude int NULL,
  UnixTime int NULL DEFAULT (0),
  Speed smallint NULL DEFAULT (0),
  Direction int NOT NULL DEFAULT (0),
  Valid smallint NULL DEFAULT (1),
  InMobitelID int NULL,
  Events bigint NULL DEFAULT (0),
  Sensor1 tinyint NULL DEFAULT (0),
  Sensor2 tinyint NULL DEFAULT (0),
  Sensor3 tinyint NULL DEFAULT (0),
  Sensor4 tinyint NULL DEFAULT (0),
  Sensor5 tinyint NULL DEFAULT (0),
  Sensor6 tinyint NULL DEFAULT (0),
  Sensor7 tinyint NULL DEFAULT (0),
  Sensor8 tinyint NULL DEFAULT (0),
  LogID int NULL DEFAULT (0),
  isShow smallint NULL DEFAULT (0),
  whatIs int NULL DEFAULT (0),
  Counter1 int NOT NULL DEFAULT (-1),
  Counter2 int NOT NULL DEFAULT (-1),
  Counter3 int NOT NULL DEFAULT (-1),
  Counter4 int NOT NULL DEFAULT (-1),
  SrvPacketID bigint NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelIDSrvPacketID" для объекта типа таблица "dbo.datagps"
--
PRINT (N'Создать индекс "IDX_MobitelIDSrvPacketID" для объекта типа таблица "dbo.datagps"')
GO
CREATE INDEX IDX_MobitelIDSrvPacketID
  ON dbo.datagps (Mobitel_ID, SrvPacketID)
  ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.datagps"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.datagps"')
GO
CREATE INDEX Index_2
  ON dbo.datagps (Mobitel_ID, LogID)
  ON [PRIMARY]
GO

--
-- Создать индекс "Index_3" для объекта типа таблица "dbo.datagps"
--
PRINT (N'Создать индекс "Index_3" для объекта типа таблица "dbo.datagps"')
GO
CREATE INDEX Index_3
  ON dbo.datagps (Mobitel_ID, UnixTime, Valid)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagps.SrvPacketID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagps.SrvPacketID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ID серверного пакета в состав которого входит эта запись', 'SCHEMA', N'dbo', 'TABLE', N'datagps', 'COLUMN', N'SrvPacketID'
GO

--
-- Создать процедуру "dbo.OnLostDataGPS2"
--
GO
PRINT (N'Создать процедуру "dbo.OnLostDataGPS2"')
GO
CREATE PROCEDURE dbo.OnLostDataGPS2
(
  @MobitelID    INTEGER,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT 'Модификация диапазонов пропусков. Анализ дублирующих данных зада'
BEGIN
  /********** DECLARE **********/

  DECLARE @NewConfirmedID INT;
  SET @NewConfirmedID = 0; /* ????? ???????? ConfirmedID */
  DECLARE @BeginLogID INT; /* Begin_LogID ? ??????? CursorLostRanges */
  DECLARE @EndLogID INT; /* End_LogID ? ??????? CursorLostRanges */
  DECLARE @BeginSrvPacketID BIGINT; /* Begin_SrvPacketID ? ??????? CursorLostRanges */
  DECLARE @EndSrvPacketID BIGINT; /* End_SrvPacketID ? ??????? CursorLostRanges */
  DECLARE @RowCountForAnalyze INT; /* ?????????? ??????? ??? ??????? */
  SET @RowCountForAnalyze = 0;
  DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  SET @DataExists = 1;
  DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
  DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
  DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
  DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
  DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

  /* ?????? ??? ??????? ?? ?????????? ????????? ????????? */
  DECLARE CursorLostRanges CURSOR LOCAL FOR
  SELECT Begin_LogID
       , End_LogID
       , Begin_SrvPacketID
       , End_SrvPacketID
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* ?????? ??? ??????? ?? ????? LogID */
  DECLARE CursorNewLogID2 CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp
  ORDER BY
    SrvPacketID;

  /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* ?????????? ????????? ? ????????? ??????? ???? ??? ???????? */
  SET @DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;

  WHILE @@fetch_status = 0
  BEGIN
  
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT DISTINCT LogID
                  , SrvPacketID
    FROM
      datagpsbuffer_ontmp
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (SrvPacketID > @BeginSrvPacketID)
      AND (SrvPacketID < @EndSrvPacketID)
    ORDER BY
      SrvPacketID;

    /* ???? ???? ?????? ???????????? ???????? */
    SET @RowCountForAnalyze = (SELECT count(1)
                               FROM
                                 datagpslost_ontmp);

    IF @RowCountForAnalyze > 0
    BEGIN
      /* ????????? ?????? ? ????????? ?????? ??? ??????? */
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@BeginLogID, @BeginSrvPacketID)
      , (@EndLogID, @EndSrvPacketID);

      /* ??????? ?????? ???????? */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        --DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
        --DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
        --DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --SET @FirstLogID = -9999999;
        --DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
        --DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

        /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
        --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* ???????????? ???????-??????? ? ??????? ??????????? ?????????? */
        --SET DataExists = 1;
        OPEN CursorNewLogID2;
        FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;

        WHILE @@fetch_status = 0
        BEGIN
          IF @FirstLogID = -9999999
          BEGIN
            /* ?????? ???????? ? ?????? */
            SET @FirstLogID = @NewLogID;
            SET @FirstSrvPacketID = @NewSrvPacketID;
          END
          ELSE
          BEGIN
            /* ???????? ????? ???????*/
            SET @SecondLogID = @NewLogID;
            SET @SecondSrvPacketID = @NewSrvPacketID;

            IF (@SecondSrvPacketID - @FirstSrvPacketID) > 1
            BEGIN
              /* ?????? ?????? */
              EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

              IF @NeedOptimize = 0
              BEGIN
                SET @NeedOptimize = 1;
              END; -- IF

            END; -- IF
            /* ?????????? ??????? ?????? ???????? ?? ?????? ???????
               ??? ??????? ? ????????? ???????? ? ???????? ?????????? ???????? */
            SET @FirstLogID = @SecondLogID;
            SET @FirstSrvPacketID = @SecondSrvPacketID;
          END; -- ELSE  
          FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;
        END; -- WHILE;
        CLOSE CursorNewLogID2;
      --DEALLOCATE CursorNewLogID2;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END;

    FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;
  END;
  CLOSE CursorLostRanges;
  --DEALLOCATE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* ???? ??????????? ??????? ? ??????? ????????? ??? - 
     ????? ConfirmedID ????? ????? ????????????? LogID 
     ??????? ?????????. ????? - ??????????? Begin_LogID
     ?? ??????? ????????? */
  SELECT @NewConfirmedID = min(Begin_LogID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @NewConfirmedID IS NULL
  BEGIN
    SET @NewConfirmedID = (SELECT max(LogID)
                           FROM
                             datagps
                           WHERE
                             Mobitel_ID = @MobitelID);
  END;

  /* ??????? ConfirmedID ???? ???? */
  UPDATE mobitels
  SET
    ConfirmedID = @NewConfirmedID
  WHERE
    (Mobitel_ID = @MobitelID) AND
    (ConfirmedID < @NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END


GO

--
-- Создать процедуру "dbo.OnLostDataGPS"
--
GO
PRINT (N'Создать процедуру "dbo.OnLostDataGPS"')
GO


CREATE PROCEDURE dbo.OnLostDataGPS
(
  @MobitelID    INT,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT 'Поиск пропусков в online данных заданного телетрека и заполнение'
BEGIN

  /********** DECLARE **********/

  DECLARE @CurrentConfirmedID INT; 
  SET @CurrentConfirmedID = 0;
  DECLARE @NewConfirmedID INT; 
  SET @NewConfirmedID = 0;
  DECLARE @StartLogIDSelect INT;
  SET @StartLogIDSelect = 0;
  DECLARE @FinishLogIDSelect INT;
  SET @FinishLogIDSelect = 0;
  DECLARE @MaxEndLogID INT; 
  SET @MaxEndLogID = 0;

  DECLARE @MinLogID_n INT; 
  SET @MinLogID_n = 0;
  DECLARE @MaxLogID_n INT; 
  SET @MaxLogID_n = 0;

  DECLARE @DataExists TINYINT; 
  SET @DataExists = 1;

  DECLARE @NewLogID INT;
  DECLARE @NewSrvPacketID BIGINT; 
  DECLARE @FirstLogID INT;
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; 
  DECLARE @FirstSrvPacketID BIGINT; 
  DECLARE @SecondSrvPacketID BIGINT; 

  DECLARE @tmpSrvPacketID BIGINT;
  SET @tmpSrvPacketID = 0;

  DECLARE CursorNewLogID CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  SET @CurrentConfirmedID = (SELECT coalesce(ConfirmedID, 0)
                             FROM
                               mobitels
                             WHERE
                               Mobitel_ID = @MobitelID);


  SELECT @MinLogID_n = min(LogID)
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelID)
    AND (LogID > @CurrentConfirmedID);


  SET @MaxLogID_n = (SELECT max(LogID)
                     FROM
                       datagpsbuffer_on
                     WHERE
                       Mobitel_ID = @MobitelID);


  SELECT @MaxEndLogID = coalesce(max(End_LogID), 0)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;


  IF @MinLogID_n IS NOT NULL
  BEGIN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF @MinLogID_n < @MaxEndLogID
    BEGIN
     
      SET @StartLogIDSelect = (SELECT coalesce(Begin_LogID, @MinLogID_n)
                               FROM
                                 datagpslost_on
                               WHERE
                                 (Mobitel_ID = @MobitelID)
                                 AND (@MinLogID_n > Begin_LogID)
                                 AND (@MinLogID_n < End_LogID));


      SET @FinishLogIDSelect = (SELECT coalesce(End_LogID, @MaxLogID_n)
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (@MaxLogID_n > Begin_LogID)
                                  AND (@MaxLogID_n < End_LogID));

   
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID >= @StartLogIDSelect) AND
        (Begin_LogID <= @FinishLogIDSelect);

    
      IF (@StartLogIDSelect = 0) AND (NOT EXISTS (SELECT 1
                                                  FROM
                                                    datagps
                                                  WHERE
                                                    (Mobitel_ID = @MobitelID)
                                                    AND (LogID = @StartLogIDSelect)))
      BEGIN
        INSERT INTO datagpslost_ontmp(LogID
                                    , SrvPacketID)
        VALUES
          (0, 0);
      END;
    END;
    ELSE
    BEGIN

   
      SELECT @StartLogIDSelect = coalesce(max(End_LogID), @CurrentConfirmedID)
      FROM
        datagpslost_on
      WHERE
        Mobitel_ID = @MobitelID;

    
      SELECT @FinishLogIDSelect = max(LogID)
      FROM
        datagpsbuffer_on
      WHERE
        Mobitel_ID = @MobitelID;

    
      SELECT @tmpSrvPacketID = coalesce(SrvPacketID, 0)
      FROM
        datagps
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (LogID = @StartLogIDSelect);

     
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@StartLogIDSelect, @tmpSrvPacketID);
    END; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

  
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT LogID
         , SrvPacketID
    FROM
      datagps
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (LogID >= @StartLogIDSelect)
      AND (LogID <= @FinishLogIDSelect)
    ORDER BY
      LogID;

    
    --SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstLogID = -9999999
      BEGIN
      
        SET @FirstLogID = @NewLogID;
        SET @FirstSrvPacketID = @NewSrvPacketID;
      END
      ELSE
      BEGIN
      
        SET @SecondLogID = @NewLogID;
        SET @SecondSrvPacketID = @NewSrvPacketID;

        IF (@SecondLogID - @FirstLogID) > 1
        BEGIN
         
          EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

          IF @NeedOptimize = 0
          BEGIN
            SET @NeedOptimize = 1;
          END; -- IF

        END; -- IF
       
        SET @FirstLogID = @SecondLogID;
        SET @FirstSrvPacketID = @SecondSrvPacketID;
      END; -- IF
      FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    END; -- WHILE
    CLOSE CursorNewLogID;
    --DEALLOCATE CursorNewLogID; 

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

   
    SET @NewConfirmedID = (SELECT min(Begin_LogID)
                           FROM
                             datagpslost_on
                           WHERE
                             Mobitel_ID = @MobitelID);

    IF @NewConfirmedID IS NULL
    BEGIN
      SET @NewConfirmedID = (SELECT max(LogID)
                             FROM
                               datagps
                             WHERE
                               Mobitel_ID = @MobitelID);
    END;

   
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (ConfirmedID < @NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END; -- IF MaxEndLogID IS NOT NULL  
END



GO

--
-- Создать процедуру "dbo.OnListMobitelConfig64"
--
GO
PRINT (N'Создать процедуру "dbo.OnListMobitelConfig64"')
GO


CREATE PROCEDURE dbo.OnListMobitelConfig64
  -- COMMENT 'Выборка настроек телетреков которые могут работать в online режиме для datagps и datagps64'
  AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  DECLARE @Is64Cursor tinyint;
  DECLARE @DevIdShortInCursor CHAR(4);
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /* Курсор в множестве настроенных телетреков */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS MobitelID, m.Is64bitPackets AS Is64, c.devIdShort AS DevIdShort
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT TOP 1 intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID)
      ORDER BY intConf.InternalMobitelConfig_ID DESC)) AND
      (c.devIdShort IS NOT NULL) AND (c.devIdShort <> '')
    ORDER BY m.Mobitel_ID;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* Временная таблица настроек телетрека */
  CREATE TABLE dbo.tmpMobitelsConfig64 (
    MobitelID INT NOT NULL,
    Is64 tinyint NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL,
    LastPacketID64 BIGINT NOT NULL);

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor, @Is64Cursor, @DevIdShortInCursor;
  WHILE  @@fetch_status = 0 begin
    INSERT INTO dbo.tmpMobitelsConfig64 SELECT @MobitelIDInCursor, @Is64Cursor, @DevIdShortInCursor,
      (SELECT COALESCE(MAX(d.SrvPacketID), 0)
       FROM datagps d
       WHERE d.Mobitel_ID = @MobitelIDInCursor) AS LastPacketID,
      (SELECT COALESCE(MAX(t.SrvPacketID), 0)
       FROM datagps64 t
       WHERE t.Mobitel_ID = @MobitelIDInCursor) AS LastPacketID64;

    FETCH CursorMobitels INTO @MobitelIDInCursor, @Is64Cursor, @DevIdShortInCursor;
  END;
  CLOSE CursorMobitels;

  SELECT MobitelID, Is64, DevIdShort, LastPacketID, LastPacketID64
  FROM dbo.tmpMobitelsConfig64;

  DROP TABLE dbo.tmpMobitelsConfig64;
END

GO

--
-- Создать процедуру "dbo.OnListMobitelConfig"
--
GO
PRINT (N'Создать процедуру "dbo.OnListMobitelConfig"')
GO

CREATE PROCEDURE dbo.OnListMobitelConfig
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  DECLARE @DevIdShortInCursor CHAR(4);
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /*  */
  DECLARE CursorMobitels CURSOR LOCAL FOR
  SELECT m.Mobitel_ID AS MobitelID
       , c.devIdShort AS DevIdShort
  FROM
    mobitels m
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort IS NOT NULL)
    AND (c.devIdShort <> '')
  ORDER BY
    m.Mobitel_ID;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /*  */
  IF EXISTS(SELECT * FROM sys.tables WHERE name = 'tmpMobitelsConfig')
    DROP TABLE dbo.tmpMobitelsConfig;

CREATE TABLE dbo.tmpMobitelsConfig(
  MobitelID INT NOT NULL,
  DevIdShort CHAR(4) NOT NULL,
  LastPacketID BIGINT NOT NULL
);

OPEN CursorMobitels;
FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
WHILE @@fetch_status = 0
BEGIN
  INSERT INTO tmpMobitelsConfig
  SELECT @MobitelIDInCursor
       , @DevIdShortInCursor
       , (SELECT coalesce(max(SrvPacketID), 0)
          FROM
            datagps
          WHERE
            Mobitel_ID = @MobitelIDInCursor) AS LastPacketID;

  FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
END;
CLOSE CursorMobitels;
--DEALLOCATE CursorMobitels;

SELECT MobitelID
     , DevIdShort
     , LastPacketID
FROM
  tmpMobitelsConfig;

DROP TABLE dbo.tmpMobitelsConfig;
END

GO

--
-- Создать процедуру "dbo.OnFullRefreshLostRanges"
--
GO
PRINT (N'Создать процедуру "dbo.OnFullRefreshLostRanges"')
GO


CREATE PROCEDURE dbo.OnFullRefreshLostRanges
AS
--NOT DETERMINISTIC
--CONTAINS SQL
--SQL SECURITY INVOKER
--COMMENT 'Поиск всех пропусков для TDataManager'
BEGIN
  DECLARE @MobitelIDInCursor INT; /* Текущее значение MobitelID */
  --DECLARE @DataExists TINYINT; /* Флаг наличия данных после фетча */
  --SET @DataExists = 1;

  DECLARE @LogIdInCursor INT; /* Текущее значение LogId телетрека */
  DECLARE @SrvPacketIdInCursor BIGINT; /* Текущее значение SrvPacketID телетрека */
  DECLARE @PreviousLogId INT; /* Значение LogId телетрека на предыдущем шаге*/
  DECLARE @PreviousSrvPacketId BIGINT; /* Значение SrvPacketID телетрека на предыдущем шаге */

  DECLARE @FirstIteration TINYINT; /* Флаг первой итерации по данным */
  SET @FirstIteration = 1;
  DECLARE @NewRowCount INT; /* Количество новых записей с разрывами */
  SET @NewRowCount = 0;
  DECLARE @NewConfirmedID INT; /* Новое значение ConfirmedID */

  DECLARE @MaxNegativeDataGpsId INT; /* максимальный DataGpsId в множестве отрицательных LogId */
  DECLARE @MaxPositiveDataGpsId INT; /* максимальный DataGpsId в множестве положительных LogId */
  DECLARE @InsertLostDataStatement NVARCHAR;

  /* Курсор по всем телетрекам */
  DECLARE CursorMobitels CURSOR FOR
  SELECT DISTINCT (Mobitel_ID) AS Mobitel_ID
  FROM
    mobitels
  ORDER BY
    Mobitel_ID;

  /* Курсор по данным телетрека */
  DECLARE CursorDataGps CURSOR FOR
  SELECT LogId
       , SrvPacketID
  FROM
    DataGps
  WHERE
    Mobitel_ID = @MobitelIDInCursor
  ORDER BY
    LogId;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* Очистка таблиц, связанных учетом пропусков для TDataManager  */
  TRUNCATE TABLE datagpsbuffer_on;
  TRUNCATE TABLE datagpslost_on;
  TRUNCATE TABLE datagpslost_ontmp;

  /* ------------------- FETCH Mobitels ------------------------ */
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* Заголовок для запроса вставки  */
    SET @InsertLostDataStatement = 'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';

    /* ----------------- FETCH DataGPS --------------------- */
    SET @FirstIteration = 1;
    SET @NewRowCount = 0;

    OPEN CursorDataGps;
    FETCH CursorDataGps INTO @LogIdInCursor, @SrvPacketIdInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstIteration = 1
      BEGIN
        /* Первое значение в списке */
        SET @PreviousLogId = @LogIdInCursor;
        SET @PreviousSrvPacketId = @SrvPacketIdInCursor;
        SET @FirstIteration = 0;
      END
      ELSE /* Основная ветка расчета*/
      BEGIN
        IF (((@LogIdInCursor - @PreviousLogId) > 1) AND (@PreviousSrvPacketId < @SrvPacketIdInCursor))
        BEGIN
          /* Найден разрыв */
          IF @NewRowCount > 0
          BEGIN
            SET @InsertLostDataStatement = @InsertLostDataStatement + ',';
          END; --END IF;

          SET @InsertLostDataStatement = @InsertLostDataStatement + ' (' + @MobitelIDInCursor + ', ' + @PreviousLogId + ', ' + @LogIdInCursor + ', ' + @PreviousSrvPacketId + ', ' + @SrvPacketIdInCursor + ')';

          SET @NewRowCount = @NewRowCount + 1;
        END; --END IF;

        /* Переместим текущие значения в предыдущие */
        SET @PreviousLogId = @LogIdInCursor;
        SET @PreviousSrvPacketId = @SrvPacketIdInCursor;
      END; --END IF; -- IF @FirstIteration = 1 

      FETCH CursorDataGps INTO @LogIdInCursor, @SrvPacketIdInCursor;
    END; --END WHILE; --  WHILE DataExistsInner = 1 
    CLOSE CursorDataGps;

    /* Если у данного телетрека есть пропущенные записи - 
       тогда ConfirmedID будет равен минимальному Begin_LogID 
       из таблицы пропусков. 
       Иначе - максимальному LogID данного телетрека из 
       таблицы datagps в одном из диапазонов больше нуля или 
       меньше нуля, в зависимости от текущих LogId. */
    IF @NewRowCount > 0
    BEGIN
      /* Заполнение таблицы пропущенных данных */
      EXEC (@InsertLostDataStatement);

      SET @NewConfirmedID = (SELECT min(Begin_LogID)
                             FROM
                               datagpslost_on
                             WHERE
                               Mobitel_ID = @MobitelIDInCursor);
    END
    ELSE
    BEGIN
      /* В этом случае не все так радужно.
         Если есть отрицательные значения LogID, то для определения
         нового значения ConfirmedID необходим дополнительный анализ:
         оцениваем какие значения LogID положительные или отрицательные
         более новые (по DataGpsId). */
      IF EXISTS (SELECT TOP 1 1
                 FROM
                   dbo.datagps
                 WHERE
                   (Mobitel_ID = @MobitelIDInCursor)
                   AND (LogId < 0))
      BEGIN
        SET @MaxNegativeDataGpsId = (SELECT max(DataGps_ID)
                                     FROM
                                       DataGps
                                     WHERE
                                       (Mobitel_ID = @MobitelIDInCursor)
                                       AND (LogId < 0));

        SET @MaxPositiveDataGpsId = (SELECT coalesce(max(DataGps_ID), 0)
                                     FROM
                                       DataGps
                                     WHERE
                                       (Mobitel_ID = @MobitelIDInCursor)
                                       AND (LogId >= 0));

        IF (@MaxNegativeDataGpsId > @MaxPositiveDataGpsId)
        BEGIN
          SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                                 FROM
                                   datagps
                                 WHERE
                                   (Mobitel_ID = @MobitelIDInCursor)
                                   AND (LogId < 0));
        END
        ELSE
        BEGIN
          SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                                 FROM
                                   datagps
                                 WHERE
                                   (Mobitel_ID = @MobitelIDInCursor)
                                   AND (LogId >= 0));
        END --END IF; -- IF (@MaxNegativeDataGpsId > @MaxPositiveDataGpsId)
      END
      ELSE
      BEGIN
        /* существуют только положительные LogId */
        SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                               FROM
                                 datagps
                               WHERE
                                 Mobitel_ID = @MobitelIDInCursor);
      END; --END IF; -- IF EXISTS 
    END; --END IF; -- IF @NewRowCount > 0

    /* Обновим ConfirmedID */
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    --SET DataExists = 1;
    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END; --END WHILE;
  CLOSE CursorMobitels;
END;

GO

--
-- Создать процедуру "dbo.OnCorrectInfotrackLogId64"
--
GO
PRINT (N'Создать процедуру "dbo.OnCorrectInfotrackLogId64"')
GO

CREATE PROCEDURE dbo.OnCorrectInfotrackLogId64
AS
--SQL SECURITY INVOKER
--COMMENT 'Корректировка значений LogId и InMobitelID в данных инфотреков'
BEGIN
  DECLARE @MobitelIDInCursor INT; /* Текущее значение MobitelID  */
  DECLARE @DataExists TINYINT; /* Флаг наличия данных после фетча */
  SET @DataExists = 1;
  DECLARE @MaxLogId INT; /* Максимальный LogId для данного дивайса*/
  SET @MaxLogId = 0;
  DECLARE @SrvPacketIDInCursor BIGINT; /* Текущее значение серверного пакета  */
  SET @SrvPacketIDInCursor = 0;

  /* Курсор по инфотрекам в множестве новых данных. 
     Все инфотреки начинаются с буквы I  */
  DECLARE CursorInfoTrack CURSOR LOCAL FOR
  SELECT DISTINCT buf.Mobitel_ID
  FROM
    datagpsbuffer_on64 buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort LIKE 'I%')
    AND (buf.LogId = 0)
  ORDER BY
    1;

  DECLARE CursorInfoTrackUpdateSet CURSOR LOCAL FOR
  SELECT SrvPacketID
  FROM
    datagpsbuffer_on64
  WHERE
    (Mobitel_ID = @MobitelIDInCursor)
    AND (LogId = 0)
  ORDER BY
    UnixTime;

  /*  Курсор по данным телетрека */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* Находим максимальный LogId для данного инфотрека */
    SET @MaxLogId = (SELECT coalesce(max(LogID), 0)
                     FROM
                       DataGps
                     WHERE
                       Mobitel_ID = @MobitelIDInCursor);

    /* Обновляем LogId и InMobitelID в наборе новых данных текущего инфотрека */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      SET @MaxLogId = @MaxLogId + 1;

      UPDATE datagpsbuffer_on64
      SET
        LogId = @MaxLogId, Mobitel_ID = @MaxLogId
      WHERE
        SrvPacketID = @SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    END;
    CLOSE CursorInfoTrackUpdateSet;
    --DEALLOCATE CursorInfoTrackUpdateSet;

    --SET DataExists = 1;
    FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  END;
  CLOSE CursorInfoTrack;
--DEALLOCATE CursorInfoTrack;
END

GO

--
-- Создать процедуру "dbo.OnCorrectInfotrackLogId"
--
GO
PRINT (N'Создать процедуру "dbo.OnCorrectInfotrackLogId"')
GO


CREATE PROCEDURE dbo.OnCorrectInfotrackLogId
AS
--SQL SECURITY INVOKER
--COMMENT 'Корректировка значений LogId и InMobitelID в данных инфотреков'
BEGIN
  DECLARE @MobitelIDInCursor INT; 
  DECLARE @DataExists TINYINT; 
  SET @DataExists = 1;
  DECLARE @MaxLogId INT; 
  SET @MaxLogId = 0;
  DECLARE @SrvPacketIDInCursor BIGINT; 
  SET @SrvPacketIDInCursor = 0;

  DECLARE CursorInfoTrack CURSOR LOCAL FOR
  SELECT DISTINCT buf.Mobitel_ID
  FROM
    datagpsbuffer_on buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort LIKE 'I%')
    AND (buf.LogId = 0)
  ORDER BY
    1;

  DECLARE CursorInfoTrackUpdateSet CURSOR LOCAL FOR
  SELECT SrvPacketID
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelIDInCursor)
    AND (LogId = 0)
  ORDER BY
    UnixTime;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SET @MaxLogId = (SELECT coalesce(max(LogID), 0)
                     FROM
                       DataGps
                     WHERE
                       Mobitel_ID = @MobitelIDInCursor);

    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      SET @MaxLogId = @MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET
        LogId = @MaxLogId, InMobitelID = @MaxLogId
      WHERE
        SrvPacketID = @SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    END;
    CLOSE CursorInfoTrackUpdateSet;

    SET @DataExists = 1;
    FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  END;
  CLOSE CursorInfoTrack;
END

GO

--
-- Создать таблицу "dbo.datagpsbufferlite"
--
PRINT (N'Создать таблицу "dbo.datagpsbufferlite"')
GO
CREATE TABLE dbo.datagpsbufferlite (
  DataGps_ID int IDENTITY,
  Mobitel_ID int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  Latitude int NULL DEFAULT (0),
  Longitude int NULL DEFAULT (0),
  Altitude int NULL,
  UnixTime int NULL DEFAULT (0),
  Speed smallint NULL DEFAULT (0),
  Direction int NOT NULL DEFAULT (0),
  Valid smallint NULL DEFAULT (1),
  InMobitelID int NULL,
  Events bigint NULL DEFAULT (0),
  Sensor1 tinyint NULL DEFAULT (0),
  Sensor2 tinyint NULL DEFAULT (0),
  Sensor3 tinyint NULL DEFAULT (0),
  Sensor4 tinyint NULL DEFAULT (0),
  Sensor5 tinyint NULL DEFAULT (0),
  Sensor6 tinyint NULL DEFAULT (0),
  Sensor7 tinyint NULL DEFAULT (0),
  Sensor8 tinyint NULL DEFAULT (0),
  LogID int NULL DEFAULT (0),
  isShow smallint NULL DEFAULT (0),
  whatIs smallint NULL DEFAULT (0),
  Counter1 int NOT NULL DEFAULT (-1),
  Counter2 int NOT NULL DEFAULT (-1),
  Counter3 int NOT NULL DEFAULT (-1),
  Counter4 int NOT NULL DEFAULT (-1)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.datagpsbuffer"
--
PRINT (N'Создать таблицу "dbo.datagpsbuffer"')
GO
CREATE TABLE dbo.datagpsbuffer (
  DataGps_ID int IDENTITY,
  Mobitel_ID int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  Latitude int NULL DEFAULT (0),
  Longitude int NULL DEFAULT (0),
  Altitude int NULL,
  UnixTime int NULL DEFAULT (0),
  Speed smallint NULL DEFAULT (0),
  Direction int NOT NULL DEFAULT (0),
  Valid smallint NULL DEFAULT (1),
  InMobitelID int NULL,
  Events bigint NULL DEFAULT (0),
  Sensor1 tinyint NULL DEFAULT (0),
  Sensor2 tinyint NULL DEFAULT (0),
  Sensor3 tinyint NULL DEFAULT (0),
  Sensor4 tinyint NULL DEFAULT (0),
  Sensor5 tinyint NULL DEFAULT (0),
  Sensor6 tinyint NULL DEFAULT (0),
  Sensor7 tinyint NULL DEFAULT (0),
  Sensor8 tinyint NULL DEFAULT (0),
  LogID int NULL DEFAULT (0),
  isShow smallint NULL DEFAULT (0),
  whatIs smallint NULL DEFAULT (0),
  Counter1 int NOT NULL DEFAULT (-1),
  Counter2 int NOT NULL DEFAULT (-1),
  Counter3 int NOT NULL DEFAULT (-1),
  Counter4 int NOT NULL DEFAULT (-1)
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelIDLogID" для объекта типа таблица "dbo.datagpsbuffer"
--
PRINT (N'Создать индекс "IDX_MobitelIDLogID" для объекта типа таблица "dbo.datagpsbuffer"')
GO
CREATE INDEX IDX_MobitelIDLogID
  ON dbo.datagpsbuffer (Mobitel_ID, LogID)
  ON [PRIMARY]
GO

--
-- Создать процедуру "dbo.TransferLiteBuffer"
--
GO
PRINT (N'Создать процедуру "dbo.TransferLiteBuffer"')
GO

CREATE PROCEDURE dbo.TransferLiteBuffer
--COMMENT 'Переносит GPS данные из буферной таблицы в datagps.'
AS
BEGIN
  IF object_id('tempdb..#temp2') IS NOT NULL
    DROP TABLE #temp2;

  CREATE TABLE #temp2(
    DataGps_ID INT
  );
  INSERT INTO #temp2
  SELECT DataGPS_ID
  FROM
    datagpsbufferlite;

  UPDATE datagps
  SET
    Message_ID = b.Message_ID, Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = NULL, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, isShow = b.IsShow, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagps d, datagpsbuffer b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.Latitude = b.Latitude AND
    d.Longitude = b.Longitude AND
    d.Altitude = b.Altitude AND
    d.UnixTime = b.UnixTime;

  DELETE b
  FROM
    datagps d, datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.Latitude = b.Latitude AND
    d.Longitude = b.Longitude AND
    d.Altitude = b.Altitude AND
    d.UnixTime = b.UnixTime;

  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.[Events]
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs
  FROM
    datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID
  GROUP BY
    b.Mobitel_ID
  , b.LogID
  , b.Latitude
  , b.Longitude
  , b.Altitude
  , b.UnixTime;

  DELETE b
  FROM
    datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID;

  DROP TABLE #temp2
END

GO

--
-- Создать процедуру "dbo.TransferBuffer"
--
GO
PRINT (N'Создать процедуру "dbo.TransferBuffer"')
GO


CREATE PROCEDURE dbo.TransferBuffer
--SQL SECURITY INVOKER
--COMMENT 'Переносит GPS данные из буферной таблицы в datagps.'
AS
BEGIN

  --CREATE TEMPORARY TABLE IF NOT EXISTS temp1 ENGINE = HEAP

  IF object_id('tempdb..#temp1') IS NOT NULL
    DROP TABLE #temp1;

  CREATE TABLE #temp1(
    DataGps_ID INT
  );
  INSERT INTO #temp1
  SELECT DataGPS_ID
  FROM
    datagpsbuffer;

  UPDATE datagps
  SET
    Message_ID = 0, Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = NULL, [Events] = b.[Events], Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, isShow = 0, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagps d, datagpsbuffer b, #temp1 t
  WHERE
    (b.DataGps_ID = t.DataGps_ID) AND
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  DELETE b
  FROM
    datagps d, datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.LogID = b.LogID;

  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , [Events]
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.[Events]
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID
  GROUP BY
    b.Mobitel_ID
  , b.LogID;


  DELETE b
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID;

  DROP TABLE #temp1
END

GO

--
-- Создать таблицу "dbo.datagpsbuffer_pop"
--
PRINT (N'Создать таблицу "dbo.datagpsbuffer_pop"')
GO
CREATE TABLE dbo.datagpsbuffer_pop (
  Mobitel_ID int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  Latitude int NOT NULL DEFAULT (0),
  Longitude int NOT NULL DEFAULT (0),
  Altitude int NULL DEFAULT (0),
  UnixTime int NOT NULL DEFAULT (0),
  Speed smallint NOT NULL DEFAULT (0),
  Direction int NOT NULL DEFAULT (0),
  Valid smallint NOT NULL DEFAULT (1),
  InMobitelID int NULL DEFAULT (0),
  Events bigint NULL DEFAULT (0),
  Sensor1 tinyint NULL DEFAULT (0),
  Sensor2 tinyint NULL DEFAULT (0),
  Sensor3 tinyint NULL DEFAULT (0),
  Sensor4 tinyint NULL DEFAULT (0),
  Sensor5 tinyint NULL DEFAULT (0),
  Sensor6 tinyint NULL DEFAULT (0),
  Sensor7 tinyint NULL DEFAULT (0),
  Sensor8 tinyint NULL DEFAULT (0),
  LogID int NULL DEFAULT (0),
  isShow smallint NULL DEFAULT (0),
  whatIs int NOT NULL DEFAULT (0),
  Counter1 int NOT NULL DEFAULT (-1),
  Counter2 int NOT NULL DEFAULT (-1),
  Counter3 int NOT NULL DEFAULT (-1),
  Counter4 int NOT NULL DEFAULT (-1)
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelIDLogID" для объекта типа таблица "dbo.datagpsbuffer_pop"
--
PRINT (N'Создать индекс "IDX_MobitelIDLogID" для объекта типа таблица "dbo.datagpsbuffer_pop"')
GO
CREATE INDEX IDX_MobitelIDLogID
  ON dbo.datagpsbuffer_pop (Mobitel_ID, LogID)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidUnixtime" для объекта типа таблица "dbo.datagpsbuffer_pop"
--
PRINT (N'Создать индекс "IDX_MobitelidUnixtime" для объекта типа таблица "dbo.datagpsbuffer_pop"')
GO
CREATE INDEX IDX_MobitelidUnixtime
  ON dbo.datagpsbuffer_pop (Mobitel_ID, UnixTime)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidUnixtimeValid" для объекта типа таблица "dbo.datagpsbuffer_pop"
--
PRINT (N'Создать индекс "IDX_MobitelidUnixtimeValid" для объекта типа таблица "dbo.datagpsbuffer_pop"')
GO
CREATE INDEX IDX_MobitelidUnixtimeValid
  ON dbo.datagpsbuffer_pop (Mobitel_ID, UnixTime, Valid)
  ON [PRIMARY]
GO

--
-- Создать процедуру "dbo.PopTransferBuffer"
--
GO
PRINT (N'Создать процедуру "dbo.PopTransferBuffer"')
GO

CREATE PROCEDURE dbo.PopTransferBuffer
  --COMMENT 'Переносит GPS данные из буферной таблицы в datagps'
  AS
BEGIN
  DECLARE @MobitelIDInCursor INT; 
  --DECLARE @DataExists TINYINT;
  --SET @DataExists = 1;
  DECLARE @TmpMaxUnixTime INT; 

  DECLARE CursorMobitels CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_pop;

  
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  DELETE o
  FROM online o, datagpsbuffer_pop dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);
  
  --SET DataExists = 1;
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@FETCH_STATUS = 0 begin
    
    SET @TmpMaxUnixTime = (SELECT COALESCE(MAX(UnixTime), 0)
    FROM [online] WITH( INDEX (IDX_Mobitelid))
    WHERE Mobitel_ID = @MobitelIDInCursor);

    
    INSERT INTO [online] (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, [Events],
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, [Events],
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs
    FROM (
      SELECT TOP 100 *
      FROM datagpsbuffer_pop
      WHERE (Mobitel_ID = @MobitelIDInCursor)  AND 
        (UnixTime > @TmpMaxUnixTime) AND (Valid = 1)
      --GROUP BY Mobitel_ID, LogID
      ORDER BY UnixTime DESC) AS T
    ORDER BY UnixTime ASC;

    DELETE FROM [online]
    WHERE (Mobitel_ID = @MobitelIDInCursor) AND (UnixTime < 
      (SELECT COALESCE(MIN(UnixTime), 0) 
       FROM (
         SELECT TOP 100 UnixTime
         FROM online
         WHERE Mobitel_ID = @MobitelIDInCursor 
         ORDER BY UnixTime DESC) T1)); 
  
    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END;
  CLOSE CursorMobitels;
    
  UPDATE datagps SET
    Latitude = b.Latitude,
    Longitude = b.Longitude,
    Altitude = b.Altitude,
    UnixTime = b.UnixTime,
    Speed = b.Speed,
    Direction = b.Direction,
    Valid = b.Valid,
    InMobitelID = b.LogID,
    [Events] = b.[Events],
    Sensor1 = b.Sensor1,
    Sensor2 = b.Sensor2,
    Sensor3 = b.Sensor3,
    Sensor4 = b.Sensor4,
    Sensor5 = b.Sensor5,
    Sensor6 = b.Sensor6,
    Sensor7 = b.Sensor7,
    Sensor8 = b.Sensor8,
    whatIs = b.whatIs,
    Counter1 = b.Counter1,
    Counter2 = b.Counter2,
    Counter3 = b.Counter3,
    Counter4 = b.Counter4
	FROM datagps d, datagpsbuffer_pop b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  
  DELETE b
  FROM datagps d, datagpsbuffer_pop b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  
  INSERT INTO datagps (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, [Events], InMobitelID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs)
  SELECT
    p.Mobitel_ID, p.LogID, p.UnixTime, p.Latitude, p.Longitude, p.Altitude,
    p.Direction, p.Speed, p.Valid, [p].[Events], p.LogID,
    p.Sensor1, p.Sensor2, p.Sensor3, p.Sensor4, p.Sensor5, p.Sensor6, p.Sensor7, p.Sensor8,
    p.Counter1, p.Counter2, p.Counter3, p.Counter4, p.whatIs
  FROM datagps d, datagpsbuffer_pop p;
  --GROUP BY d.Mobitel_ID, d.LogID;

  
  TRUNCATE TABLE datagpsbuffer_pop;
END

GO

--
-- Создать таблицу "dbo.sensordata"
--
PRINT (N'Создать таблицу "dbo.sensordata"')
GO
CREATE TABLE dbo.sensordata (
  id bigint IDENTITY,
  datagps_id bigint NOT NULL,
  sensor_id bigint NOT NULL,
  Value float NOT NULL
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.sensordata"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.sensordata"')
GO
CREATE INDEX Index_2
  ON dbo.sensordata (datagps_id, sensor_id)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.datagpslosted"
--
PRINT (N'Создать таблицу "dbo.datagpslosted"')
GO
CREATE TABLE dbo.datagpslosted (
  Mobitel_ID int NOT NULL DEFAULT (0),
  Begin_LogID int NOT NULL DEFAULT (0),
  End_LogID int NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать процедуру "dbo.FillLostDataGPS"
--
GO
PRINT (N'Создать процедуру "dbo.FillLostDataGPS"')
GO

CREATE PROCEDURE dbo.FillLostDataGPS
AS
BEGIN
  DECLARE @LAST_RECORDS_COUNT INT;
  SET @LAST_RECORDS_COUNT = 32000;
  DECLARE @LastMobitelRecord INT; 

  DECLARE @MobitelIDInCursor INT;		
  DECLARE @ConfirmedIDInCursor INT;	
  DECLARE @NewConfirmedID INT;			
  DECLARE @NewStartIDSelect INT; 
  SET  @NewStartIDSelect = 0;
  DECLARE @MaxMobitelLogID INT;		

  DECLARE @BeginLogIDInCursor INT; 
  DECLARE @EndLogIDInCursor INT;		
  DECLARE @CountInLostRange INT;	
  --DECLARE @DataExists TINYINT;	
  --SET @DataExists = 1;
  DECLARE @DataChanged TINYINT;	
  DECLARE @NeedOptimize TINYINT; 
  SET @NeedOptimize = 0;
  DECLARE @TableName VARCHAR(255);

  DECLARE CursorMobitels CURSOR FOR
    SELECT Mobitel_ID, ConfirmedID FROM mobitels ORDER BY Mobitel_ID;
  DECLARE CursorLostData CURSOR FOR
    SELECT Begin_LogID, End_LogID FROM datagpslosted
    WHERE Mobitel_ID = @MobitelIDInCursor ORDER BY Begin_LogID;
  DECLARE CursorTable CURSOR FOR SELECT
    '#MUTEX_FILL_LOST_DATA_GPS'
  FROM information_schema.tables
  WHERE table_schema = DB_NAME();

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0;
  
 -- DECLARE EXIT HANDLER FOR SQLEXCEPTION
  --BEGIN
    
    --DROP TABLE IF EXISTS [#MUTEX_FILL_LOST_DATA_GPS];
  --END;

  
  OPEN CursorTable;
  FETCH CursorTable INTO @TableName;
  CLOSE CursorTable;
  
  IF not (@@fetch_status = 0) BEGIN
    
    CREATE TABLE #MUTEX_FILL_LOST_DATA_GPS (dummy char(1));
    --SET @DataExists = 1;

    --CREATE TABLE IF NOT EXISTS #tmp_datagpslosted LIKE dbo.datagps;
	if object_id('tempdb..#tmp_datagpslosted') is not null drop table #tmp_datagpslosted
	SELECT * INTO #tmp_datagpslosted FROM dbo.datagps WHERE 1 = 2

    OPEN CursorMobitels;
    FETCH CursorMobitels INTO @MobitelIDInCursor, @ConfirmedIDInCursor;
    WHILE @@fetch_status = 0
      SET @NewConfirmedID = 0;
      SET @NewStartIDSelect = 0;

      
      TRUNCATE TABLE #tmp_datagpslosted;

      
      SET @MaxMobitelLogID = (SELECT COALESCE(MAX(LogID), 0) 
      FROM dbo.datagps
      WHERE (Mobitel_ID = @MobitelIDInCursor));

      IF @ConfirmedIDInCursor <> @MaxMobitelLogID BEGIN

        SET @DataChanged = 0;
        
        SET @LastMobitelRecord = @MaxMobitelLogID - @LAST_RECORDS_COUNT;

        OPEN CursorLostData;
        FETCH CursorLostData INTO @BeginLogIDInCursor, @EndLogIDInCursor;

        IF  not(@@fetch_status = 0) BEGIN
          SET @DataChanged = 1;
          
          --SET @NewStartIDSelect = GREATEST(@ConfirmedIDInCursor, @LastMobitelRecord);

		   IF @ConfirmedIDInCursor > @LastMobitelRecord
      SET @NewStartIDSelect = @ConfirmedIDInCursor
    ELSE
      SET @NewStartIDSelect = @LastMobitelRecord
        END;

        
        WHILE @@fetch_status = 0 BEGIN
          
          SET  @CountInLostRange = (SELECT COUNT(1)
          FROM dbo.datagps
          WHERE (Mobitel_ID = @MobitelIDInCursor) AND
            (LogID > @BeginLogIDInCursor) AND (LogID < @EndLogIDInCursor));

          
          IF (@CountInLostRange > 0) OR ((@CountInLostRange = 0) AND
            (@BeginLogIDInCursor < @LastMobitelRecord)) BEGIN
            
            DELETE FROM datagpslosted
            WHERE (Mobitel_ID = @MobitelIDInCursor) AND
              (Begin_LogID >= @BeginLogIDInCursor);

            SET @DataChanged = 1;
            SET @NewStartIDSelect = @BeginLogIDInCursor;
          END;

          FETCH CursorLostData INTO @BeginLogIDInCursor, @EndLogIDInCursor;
        END;
        CLOSE CursorLostData;
        
        --SET @DataExists = 1;

        
        IF (@DataChanged = 0) AND (@EndLogIDInCursor < @MaxMobitelLogID) BEGIN
          SET @DataChanged = 1;
          SET @NewStartIDSelect = @EndLogIDInCursor;
        END;

        IF @DataChanged = 1 BEGIN
          
          INSERT INTO #tmp_datagpslosted
          SELECT *
          FROM datagps
          WHERE (Mobitel_ID = @MobitelIDInCursor) AND
            (LogID >= @NewStartIDSelect);

          
          INSERT INTO datagpslosted
          SELECT
            @MobitelIDInCursor AS Mobitel_ID,
            
            dg1.LogID AS Begin_LogID,
            
            dg3.LogID AS End_LogID
          FROM
            
            #tmp_datagpslosted dg1 LEFT JOIN #tmp_datagpslosted dg2 ON (
              (dg2.Mobitel_ID = dg1.Mobitel_ID) AND (dg2.LogID = (dg1.LogID + 1)))
            
            LEFT JOIN #tmp_datagpslosted dg3 ON (
              (dg3.Mobitel_ID = dg1.Mobitel_ID) AND
              
              (dg3.LogID = (
                SELECT MIN(dg.LogID)
                FROM #tmp_datagpslosted dg
                WHERE dg.LogID > dg1.LogID )))
          WHERE
            
            (dg2.LogID IS NULL) AND
            
            (dg3.LogID IS NOT NULL)
          ORDER BY Begin_LogID;

          SET @NeedOptimize = 1;

          
          SET  @NewConfirmedID = (SELECT COALESCE(MIN(Begin_LogID), -1)
          FROM datagpslosted
          WHERE Mobitel_ID = @MobitelIDInCursor);

          
          IF @NewConfirmedID = -1 BEGIN
            SET @NewConfirmedID = @MaxMobitelLogID;
          END;

          
          IF @NewConfirmedID > @ConfirmedIDInCursor BEGIN
            UPDATE mobitels
            SET ConfirmedID = @NewConfirmedID
            WHERE (Mobitel_ID = @MobitelIDInCursor) AND
              (ConfirmedID < @NewConfirmedID);
          END;
         END;
      END;

      FETCH CursorMobitels INTO @MobitelIDInCursor, @ConfirmedIDInCursor;
    END;
    CLOSE CursorMobitels;

	if object_id('tempdb..#tmp_datagpslosted') is not null drop table #tmp_datagpslosted
	  IF @NeedOptimize = 1 begin
    ALTER INDEX ALL ON datagpslosted REORGANIZE
    END;
    
	if object_id('tempdb..#tmp_datagpslosted') is not null drop table #MUTEX_FILL_LOST_DATA_GPS
  BEGIN
    SELECT '1';
  END;
END

GO

--
-- Создать таблицу "dbo.datagpsbuffer_dr"
--
PRINT (N'Создать таблицу "dbo.datagpsbuffer_dr"')
GO
CREATE TABLE dbo.datagpsbuffer_dr (
  Mobitel_ID int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  Latitude int NOT NULL DEFAULT (0),
  Longitude int NOT NULL DEFAULT (0),
  Altitude int NULL DEFAULT (0),
  UnixTime int NOT NULL DEFAULT (0),
  Speed smallint NOT NULL DEFAULT (0),
  Direction int NOT NULL DEFAULT (0),
  Valid smallint NOT NULL DEFAULT (1),
  InMobitelID int NULL DEFAULT (0),
  Events bigint NULL DEFAULT (0),
  Sensor1 tinyint NULL DEFAULT (0),
  Sensor2 tinyint NULL DEFAULT (0),
  Sensor3 tinyint NULL DEFAULT (0),
  Sensor4 tinyint NULL DEFAULT (0),
  Sensor5 tinyint NULL DEFAULT (0),
  Sensor6 tinyint NULL DEFAULT (0),
  Sensor7 tinyint NULL DEFAULT (0),
  Sensor8 tinyint NULL DEFAULT (0),
  LogID int NULL DEFAULT (0),
  isShow smallint NULL DEFAULT (0),
  whatIs int NOT NULL DEFAULT (0),
  Counter1 int NOT NULL DEFAULT (-1),
  Counter2 int NOT NULL DEFAULT (-1),
  Counter3 int NOT NULL DEFAULT (-1),
  Counter4 int NOT NULL DEFAULT (-1)
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelIDLogID" для объекта типа таблица "dbo.datagpsbuffer_dr"
--
PRINT (N'Создать индекс "IDX_MobitelIDLogID" для объекта типа таблица "dbo.datagpsbuffer_dr"')
GO
CREATE INDEX IDX_MobitelIDLogID
  ON dbo.datagpsbuffer_dr (Mobitel_ID, LogID)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidUnixtime" для объекта типа таблица "dbo.datagpsbuffer_dr"
--
PRINT (N'Создать индекс "IDX_MobitelidUnixtime" для объекта типа таблица "dbo.datagpsbuffer_dr"')
GO
CREATE INDEX IDX_MobitelidUnixtime
  ON dbo.datagpsbuffer_dr (Mobitel_ID, UnixTime)
  ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelidUnixtimeValid" для объекта типа таблица "dbo.datagpsbuffer_dr"
--
PRINT (N'Создать индекс "IDX_MobitelidUnixtimeValid" для объекта типа таблица "dbo.datagpsbuffer_dr"')
GO
CREATE INDEX IDX_MobitelidUnixtimeValid
  ON dbo.datagpsbuffer_dr (Mobitel_ID, UnixTime, Valid)
  ON [PRIMARY]
GO

--
-- Создать процедуру "dbo.DrTransferBuffer"
--
GO
PRINT (N'Создать процедуру "dbo.DrTransferBuffer"')
GO


CREATE PROCEDURE dbo.DrTransferBuffer
--SQL SECURITY INVOKER
--COMMENT 'Переносит GPS данные DirectOnline из буферной таблицы в datagps'
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  --DECLARE @DataExists TINYINT;
  --SET @DataExists = 1; 

  DECLARE @TmpMaxUnixTime INT;

  DECLARE CursorMobitels CURSOR FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_dr;

  DELETE o
  FROM
    online o, datagpsbuffer_dr dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    INSERT INTO online(Mobitel_ID
                     , LogID
                     , UnixTime
                     , Latitude
                     , Longitude
                     , Altitude
                     , Direction
                     , Speed
                     , Valid
                     , [Events]
                     , Sensor1
                     , Sensor2
                     , Sensor3
                     , Sensor4
                     , Sensor5
                     , Sensor6
                     , Sensor7
                     , Sensor8
                     , Counter1
                     , Counter2
                     , Counter3
                     , Counter4
                     , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , [Events]
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_dr
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;


    DELETE
    FROM
      online
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        [online]
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END; --END WHILE;
  CLOSE CursorMobitels;

  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, 
	Valid = b.Valid, InMobitelID = b.LogID, [Events] = b.[Events], Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, 
	Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, 
	Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, 
	Counter4 = b.Counter4
  FROM
    dbo.datagps d, dbo.datagpsbuffer_dr b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  DELETE b
  FROM
    datagps d, datagpsbuffer_dr b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);


  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , [Events]
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , [Events]
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
  FROM
    datagpsbuffer_dr;
	--GROUP BY  Mobitel_ID, LogID;

  TRUNCATE TABLE datagpsbuffer_dr;
END


GO

--
-- Создать функцию "dbo.UNIX_TIMESTAMPS"
--
GO
PRINT (N'Создать функцию "dbo.UNIX_TIMESTAMPS"')
GO

CREATE FUNCTION dbo.UNIX_TIMESTAMPS
(
  @ctimestamp DATETIME
)
RETURNS INTEGER
AS
BEGIN
  /* Function body */
  DECLARE @return INTEGER;
  DECLARE @GMT DATETIME;

  SET @GMT = getdate() - getutcdate();
  SET @ctimestamp = @ctimestamp - @GMT;
  SELECT @return = datediff(SECOND, { D '1970-01-01' }, @ctimestamp)
  RETURN @return
END

GO

--
-- Создать процедуру "dbo.OnTransferBuffer64"
--
GO
PRINT (N'Создать процедуру "dbo.OnTransferBuffer64"')
GO
CREATE PROCEDURE dbo.OnTransferBuffer64
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  DECLARE @DataExists TINYINT; /*  */
  SET @DataExists = 1;
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on64;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp64;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC delDupSrvPacket
  EXEC OnDeleteDuplicates64
  --EXEC OnCorrectInfotrackLogId64
  --EXEC OnCorrectInfotrackLogId64

   /* Сохраняем записи, которые уже есть в БД, для второго этапа анализа пропусков 
     Используется процедурой OnLostDataGPS264. */
  /*INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID)
    SELECT b.Mobitel_ID, b.LogID, b.SrvPacketID
    FROM datagpsbuffer_on64 b LEFT JOIN datagps64 d ON
      (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL  GROUP BY d.SrvPacketID;*/
    
  IF OBJECT_ID(N'tempdb..#temp1', N'U') IS NOT NULL   
	DROP TABLE #temp1;

  CREATE TABLE #temp1 (Mobitel_ID int, LogID int, SrvPacketID bigint);
  insert into #temp1  SELECT
      b.Mobitel_ID,
      b.LogID,
      b.SrvPacketID
    FROM datagpsbuffer_on64 b
      LEFT JOIN datagps64 d
        ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL; --GROUP BY d.SrvPacketID;

	INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID) SELECT * FROM #temp1 GROUP BY SrvPacketID;

	IF OBJECT_ID(N'tempdb..#temp1', N'U') IS NOT NULL   
		DROP TABLE #temp1;

  /* -------------------- Заполнение таблицы Online ---------------------- */
  /* Удаление из таблицы online дублирующих строк таблицы datagpsbuffer_on64 */
  /* по полям Mobitel_ID и LogID */
   DELETE o
  FROM online64 o, datagpsbuffer_on64 dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online64 WITH (INDEX (IDX_Mobitelid64))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
	      INSERT INTO online64 (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude,Acceleration, 
      Direction, Speed, Valid, [Events],
      Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude,Acceleration, 
      Direction, Speed, Valid, [Events],
      Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS
    FROM (
      SELECT TOP 100 *
      FROM datagpsbuffer_on64
      WHERE (Mobitel_ID = @MobitelIDInCursor)  AND 
        (UnixTime > @TmpMaxUnixTime) AND (Valid = 1)
      ORDER BY UnixTime DESC
      ) AS T
    ORDER BY UnixTime ASC;

     /* Удаление строк из таблицы оперативных данных не удовлетворяющие
       условиям хранения - не являются последними 100 строками */
    DELETE
    FROM
      online64
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        [online64]
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));


    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  UPDATE datagps64 SET
    Latitude = b.Latitude,
    Longitude = b.Longitude,
    Acceleration = b.Acceleration,
    UnixTime = b.UnixTime,
    Speed = b.Speed,
    Direction = b.Direction,
    Valid = b.Valid,
    LogID = b.LogID,
    [Events] = [b].[Events],
    Satellites = b.Satellites,
    RssiGsm = b.RssiGsm,
    SensorsSet = b.SensorsSet,
    Sensors = b.Sensors,
    Voltage = b.Voltage,
    DGPS = b.DGPS,
    SrvPacketID = b.SrvPacketID
  FROM
    datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    AND (d.SrvPacketID < b.SrvPacketID);
  /*  */
  DELETE b
  FROM datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);
  /* */
    INSERT INTO datagps64 (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, [Events], 
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, [Events], 
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID
  FROM datagpsbuffer_on64;
  --GROUP BY Mobitel_ID,LogID;
  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS64 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);*/

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on64;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET @DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS264 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = CREATEST(@NeedOptimize, @NeedOptimizeParam); */

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsUpdateSet;
  --DEALLOCATE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp64;

  /*  */
  IF @NeedOptimize = 1
    ALTER INDEX ALL ON datagpslost_on REORGANIZE
END

GO

--
-- Создать процедуру "dbo.OnTransferBuffer_orig"
--
GO
PRINT (N'Создать процедуру "dbo.OnTransferBuffer_orig"')
GO
CREATE PROCEDURE dbo.OnTransferBuffer_orig
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  --DECLARE @DataExists TINYINT; /*  */
  --SET @DataExists = 1;
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC OnDeleteDuplicates
  EXEC OnCorrectInfotrackLogId

  /* OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp(Mobitel_ID
                                , LogID
                                , SrvPacketID)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.SrvPacketID
  FROM
    datagpsbuffer_on b
    LEFT JOIN datagps d
      ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  WHERE
    d.LogID IS NOT NULL;

  /* --------------------  Online ---------------------- */
  /*  online  datagpsbuffer_on */
  /*  Mobitel_ID LogID */
  DELETE o
  FROM
    online o, datagpsbuffer_on dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
    INSERT INTO dbo.online(Mobitel_ID
                         , LogID
                         , UnixTime
                         , Latitude
                         , Longitude
                         , Altitude
                         , Direction
                         , Speed
                         , Valid
                         , Events
                         , Sensor1
                         , Sensor2
                         , Sensor3
                         , Sensor4
                         , Sensor5
                         , Sensor6
                         , Sensor7
                         , Sensor8
                         , Counter1
                         , Counter2
                         , Counter3
                         , Counter4
                         , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , Events
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_on
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;

    /*  100 */
    DELETE
    FROM
      online
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        online
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  /* .. */
  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = b.LogID, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4, SrvPacketID = b.SrvPacketID
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID) AND
    (d.SrvPacketID < b.SrvPacketID);

  /*  */
  DELETE b
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  /* */
  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs
                    , SrvPacketID)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , Events
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
       , SrvPacketID
  FROM
    datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);*/

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS2 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = CREATEST(@NeedOptimize, @NeedOptimizeParam); */

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsUpdateSet;
  --DEALLOCATE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  --/*  */
  --IF @NeedOptimize = 1
  --  ALTER INDEX ALL ON datagpslost_on REORGANIZE
END
GO

--
-- Создать процедуру "dbo.OnTransferBuffer"
--
GO
PRINT (N'Создать процедуру "dbo.OnTransferBuffer"')
GO


CREATE PROCEDURE dbo.OnTransferBuffer
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  DECLARE @DataExists TINYINT; /*  */
  SET @DataExists = 1;
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC OnDeleteDuplicates
  EXEC OnCorrectInfotrackLogId

  /* OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp(Mobitel_ID
                                , LogID
                                , SrvPacketID)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.SrvPacketID
  FROM
    datagpsbuffer_on b
    LEFT JOIN datagps d
      ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  WHERE
    d.LogID IS NOT NULL;

  /* --------------------  Online ---------------------- */
  /*  online  datagpsbuffer_on */
  /*  Mobitel_ID LogID */
  DELETE o
  FROM
    [online] o, datagpsbuffer_on dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             [online] WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
    INSERT INTO dbo.online(Mobitel_ID
                         , LogID
                         , UnixTime
                         , Latitude
                         , Longitude
                         , Altitude
                         , Direction
                         , Speed
                         , Valid
                         , Events
                         , Sensor1
                         , Sensor2
                         , Sensor3
                         , Sensor4
                         , Sensor5
                         , Sensor6
                         , Sensor7
                         , Sensor8
                         , Counter1
                         , Counter2
                         , Counter3
                         , Counter4
                         , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , Events
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_on
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;

    /*  100 */
    DELETE
    FROM
      [online]
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        [online]
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  /* .. */
  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = b.LogID, [Events] = [b].[Events], Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4, SrvPacketID = b.SrvPacketID
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID) AND
    (d.SrvPacketID < b.SrvPacketID);

  /*  */
  DELETE b
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  /* */
  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , [Events]
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs
                    , SrvPacketID)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , [Events]
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
       , SrvPacketID
  FROM
    datagpsbuffer_on;
	--GROUP BY Mobitel_ID, LogID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS @MobitelIDInCursor, @NeedOptimizeParam OUT
    
    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS2 @MobitelIDInCursor, @NeedOptimizeParam OUT

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END -- while
  CLOSE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  IF @NeedOptimize = 1 begin
    ALTER INDEX ALL ON datagpslost_on REORGANIZE
  END;

  --/*  */
  --IF @NeedOptimize = 1
  --  ALTER INDEX ALL ON datagpslost_on REORGANIZE
END

GO

--
-- Создать процедуру "dbo.DeleteDataGPS_1000"
--
GO
PRINT (N'Создать процедуру "dbo.DeleteDataGPS_1000"')
GO
CREATE PROCEDURE dbo.DeleteDataGPS_1000
 @BeginDate datetime = null
,@EndDate	datetime = null
AS
BEGIN
set nocount on
-- exec DeleteDataGPS '20130601', '20131231'
if	(	(@BeginDate is null )
	and (@EndDate is null )
	)
	begin
	 RAISERROR ('@BeginDate and @EndDate is null - I dont delete all data!!!!', -- Message text.
				   13, -- Severity.
				   1 -- State.
				   );
	return 0
	end 

  declare @BeginDateUnix int = dbo.UNIX_TIMESTAMPS(@BeginDate)
  declare @EndDateUnix int = dbo.UNIX_TIMESTAMPS(@EndDate)
  declare @rcount int = 0
			,@rtotal int = 0
			,@StartScript datetime = getdate()
  
	
	CREATE TABLE #Mobitels (
		[Mobitel_ID] [int] NULL
	)
	
	insert into #Mobitels
				(Mobitel_ID)
		select distinct 
			Mobitel_ID
		from dbo.datagps (nolock)
	
	Declare @Count int = 0,
			@CurNumber int = 0
	select	 @Count  = count(1) 
			,@CurNumber = 0
	from #Mobitels
	print 'Count Mobitels: ' + cast(@Count as varchar(255))
	
	DECLARE cur1 CURSOR
	READ_ONLY
	FOR   select Mobitel_ID
			from #Mobitels 
			order by 1

	DECLARE @Mobitel_ID int
	DECLARE @error int, @rc int
	OPEN cur1

	FETCH NEXT FROM cur1 INTO @Mobitel_ID 
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
			select @CurNumber = @CurNumber + 1	
			SELECT  @rc = 1
            
            WHILE @rc <> 0
			BEGIN
				BEGIN TRAN

				delete top (10000)
			    from datagps  
			    where	Mobitel_ID = @Mobitel_ID
				     and UnixTime >= coalesce(@BeginDateUnix, 0)
				     and (	@EndDateUnix is null
					 or (	@EndDateUnix is not null
						and UnixTime < @EndDateUnix 
						)
					)	
								    
			    select @rcount = @@ROWCOUNT,@rc = @@ROWCOUNT
			    select @rtotal = @rtotal + @rcount
			    print 'Current number: ' + cast(@CurNumber as varchar(255))+ 
		    		', MobitelID = ' + cast(@Mobitel_ID as varchar(255)) + 
			    	', Rows deleted: ' + cast(@rcount as varchar(255))

				SELECT @error = @@ERROR

				IF @error <> 0
				   BEGIN
				    ROLLBACK TRAN
					GOTO EXIT_PROC
				   END

				COMMIT TRAN
			END			
		END
		FETCH NEXT FROM cur1 INTO @Mobitel_ID 
	END
	
	EXIT_PROC:
    
	CLOSE cur1
	DEALLOCATE cur1
	
	drop table #Mobitels
	print '-------------------------------'
	print 'Total deleted rows: ' + cast(@rtotal as varchar(255))
	print 'Script work seconds: ' + cast(datediff(second, @StartScript, getdate()) as varchar(255))
END
GO

--
-- Создать процедуру "dbo.DeleteDataGPS"
--
GO
PRINT (N'Создать процедуру "dbo.DeleteDataGPS"')
GO
CREATE PROCEDURE dbo.DeleteDataGPS
 @BeginDate datetime = null
,@EndDate	datetime = null
AS
BEGIN
set nocount on
-- exec DeleteDataGPS '20130601', '20131231'
if	(	(@BeginDate is null )
	and (@EndDate is null )
	)
	begin
	 RAISERROR ('@BeginDate and @EndDate is null - I dont delete all data!!!!', -- Message text.
				   13, -- Severity.
				   1 -- State.
				   );
	return 0
	end 

  declare @BeginDateUnix int = dbo.UNIX_TIMESTAMPS(@BeginDate)
  declare @EndDateUnix int = dbo.UNIX_TIMESTAMPS(@EndDate)
  declare @rcount int = 0
			,@rtotal int = 0
			,@StartScript datetime = getdate()
  
	
	CREATE TABLE #Mobitels (
		[Mobitel_ID] [int] NULL
	)
	
	insert into #Mobitels
				(Mobitel_ID)
		select distinct 
			Mobitel_ID
		from dbo.datagps (nolock)
	
	Declare @Count int = 0,
			@CurNumber int = 0
	select	 @Count  = count(1) 
			,@CurNumber = 0
	from #Mobitels
	print 'Count Mobitels: ' + cast(@Count as varchar(255))
	
	DECLARE cur1 CURSOR
	READ_ONLY
	FOR   select Mobitel_ID
			from #Mobitels 
			order by 1

	DECLARE @Mobitel_ID int
	OPEN cur1

	FETCH NEXT FROM cur1 INTO @Mobitel_ID 
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
			select @CurNumber = @CurNumber + 1
			
			
			delete 
			from datagps  
			where	Mobitel_ID = @Mobitel_ID
				and UnixTime >= coalesce(@BeginDateUnix, 0)
				and (	@EndDateUnix is null
					or (	@EndDateUnix is not null
						and UnixTime < @EndDateUnix 
						)
					)
			
			select @rcount = @@ROWCOUNT
			select @rtotal = @rtotal + @rcount
			print 'Current number: ' + cast(@CurNumber as varchar(255))+ 
				', MobitelID = ' + cast(@Mobitel_ID as varchar(255)) + 
				', Rows deleted: ' + cast(@rcount as varchar(255))
		END
		FETCH NEXT FROM cur1 INTO @Mobitel_ID 
	END

	CLOSE cur1
	DEALLOCATE cur1
	
	drop table #Mobitels
	print '-------------------------------'
	print 'Total deleted rows: ' + cast(@rtotal as varchar(255))
	print 'Script work seconds: ' + cast(datediff(second, @StartScript, getdate()) as varchar(255))
END
GO

--
-- Создать функцию "dbo.From_UnixTime"
--
GO
PRINT (N'Создать функцию "dbo.From_UnixTime"')
GO
CREATE FUNCTION dbo.From_UnixTime
(
  @timestamp INTEGER
)
RETURNS DATETIME
AS
BEGIN
  /* Function body */
  DECLARE @return DATETIME
  DECLARE @GMT DATETIME;
  DECLARE @tm DATETIME;

  SET @GMT = getdate() - getutcdate();
  SET @tm = dateadd(SECOND, @timestamp, { D '1970-01-01' });
  SET @return = @tm + @GMT;
RETURN @return
END
GO

--
-- Создать процедуру "dbo.ua_dedez"
--
GO
PRINT (N'Создать процедуру "dbo.ua_dedez"')
GO
CREATE PROCEDURE dbo.ua_dedez AS
DECLARE @iidd int
DECLARE @cursor CURSOR

SET @cursor = CURSOR SCROLL
FOR 
  SELECT vehicle.Mobitel_id FROM vehicle WHERE NumberPlate LIKE 'ТГ%'
OPEN @cursor

FETCH NEXT FROM @cursor INTO @iidd
WHILE @@fetch_status=0
BEGIN
  IF NOt EXISTS(SELECT vehicle.Mobitel_id FROM vehicle WHERE Mobitel_id=@iidd)
BEGIN
INSERT INTO ua_gsp_on(taim, tz) SELECT DISTINCT dbo.From_UnixTime(round(datagps.UnixTime / 600, 0) * 600) AS expr1, vehicle.NumberPlate
FROM  dbo.vehicle
  INNER JOIN dbo.datagps WITH (INDEX (Index_3))
    ON vehicle.Mobitel_id = datagps.Mobitel_ID
WHERE
  datagps.Mobitel_ID = @iidd  AND dbo.from_unixtime(datagps.UnixTime) BETWEEN '08.08.2016 08:00' AND '08.09.2016 07:59'
  END
FETCH NEXT FROM @cursor INTO @iidd
  END
  CLOSE @cursor
GO

--
-- Создать процедуру "dbo.SelectDataGpsPeriodMonth"
--
GO
PRINT (N'Создать процедуру "dbo.SelectDataGpsPeriodMonth"')
GO

CREATE PROCEDURE dbo.SelectDataGpsPeriodMonth @mobile int, @beginperiod datetime, @endperiod datetime,
@daymonth varchar(82)
AS
BEGIN
IF object_id('tempdb..#tempperiod2') IS NOT NULL
    DROP TABLE #tempperiod2;

  CREATE TABLE #tempperiod2 (DataGps_ID int, Mobitel_ID int, Lat int, Lon int, Altitude int, [time] int, speed int, Valid int, 
  sensor bigint, [Events] bigint, LogID int);
  
  INSERT INTO #tempperiod2 SELECT
    DataGps_ID AS DataGps_ID,
    Mobitel_ID AS Mobitel_ID,
    ((Latitude * 1.0000) / 600000) AS Lat,
    ((Longitude * 1.0000) / 600000) AS Lon,
    Altitude AS Altitude,
    dbo.From_UnixTime(UnixTime) AS [time],
    (Speed * 1.852) AS speed,
    Valid AS Valid,
	(Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor,
    [Events] AS [Events],
    LogID AS LogID
  FROM datagps
  WHERE Mobitel_ID = @mobile AND
  (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@beginperiod) AND dbo.UNIX_TIMESTAMPS(@endperiod)) AND
  (Valid = 1) AND NOT (Latitude = 0 AND Longitude = 0)
  ORDER BY UnixTime;

  IF @daymonth = '' begin
    return;
  END;

  IF object_id('tempdb..#tempdays') IS NOT NULL
    DROP TABLE #tempdays;

  CREATE TABLE #tempdays (
    shead varchar(2)
  );

  DECLARE @saTail varchar(82);
  DECLARE @saHead varchar(82);
  DECLARE @indx int;
  SET @saTail = @daymonth;
  WHILE @saTail != '' begin
	 SET @indx = charindex(';', @saTail);
     SET @saHead = substring(@saTail, 1, @indx - 1);
     SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

    INSERT INTO #tempdays (shead)
      VALUES (@saHead);
  END;

  SELECT
    *
  FROM #tempperiod2 d
  WHERE DAY([d].[time]) IN (SELECT
    *
  FROM #tempdays);
  END

  
GO

--
-- Создать процедуру "dbo.SelectDataGpsPeriodAll"
--
GO
PRINT (N'Создать процедуру "dbo.SelectDataGpsPeriodAll"')
GO
CREATE PROCEDURE dbo.SelectDataGpsPeriodAll
(
  @mobile      INT,
  @beginperiod DATETIME,
  @endperiod   DATETIME,
  @timebegin   VARCHAR(9),
  @timeend     VARCHAR(9),
  @weekdays    VARCHAR(15),
  @daymonth    VARCHAR(90)
)
AS
BEGIN
  -- DECLARE @begin DATETIME;
  -- SET @begin = convert(DATETIME, '24/03/2013 00:00:00');
  -- DECLARE @end DATETIME;
  -- SET @end = convert(DATETIME, '1/04/2013 23:59:59');
  DECLARE @saTail VARCHAR(90);
  DECLARE @saHead VARCHAR(2);
  DECLARE @indx INT;
  DECLARE @time0 TIME;
  SET @time0 = '23:59:59';
  DECLARE @time1 TIME;
  SET @time1 = '00:00:00';

  IF object_id('tempdb..#tempperiod4') IS NOT NULL
    DROP TABLE #tempperiod4;

  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS time
       , (Speed * 1.852) AS speed
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
       , Events AS Events
       , LogID AS LogID
  INTO
    #tempperiod4
  FROM
    datagps
  WHERE
    Mobitel_ID = @mobile
    AND (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@beginperiod) AND dbo.UNIX_TIMESTAMPS(@endperiod))
    AND (Valid = 1)
    AND NOT (Latitude = 0
    AND Longitude = 0)
  ORDER BY
    UnixTime;

  --SELECT * FROM #tempperiod4;
  IF object_id('tempdb..#tempweeks') IS NOT NULL
    DROP TABLE #tempweeks;

  IF @weekdays != ''
  BEGIN
    CREATE TABLE #tempweeks(
      shead VARCHAR(2)
    );

    SET @saTail = @weekdays;

    WHILE @saTail != ''
    BEGIN
      SET @indx = charindex(';', @saTail);
      SET @saHead = substring(@saTail, 1, @indx - 1);
      SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

      INSERT INTO #tempweeks(shead)
      VALUES
        (@saHead);
    END;
  END -- IF

  IF object_id('tempdb..#tempday0') IS NOT NULL
    DROP TABLE #tempday0;

  IF @daymonth != ''
  BEGIN
    CREATE TABLE #tempday0(
      shead VARCHAR(2)
    );

    SET @saTail = @daymonth;

    WHILE @saTail != ''
    BEGIN
      SET @indx = charindex(';', @saTail);
      SET @saHead = substring(@saTail, 1, @indx - 1);
      SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

      INSERT INTO #tempday0(shead)
      VALUES
        (@saHead);
    END; -- while
  END; -- IF

  -- getting select data
  IF (@daymonth != '') AND (@weekdays != '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (datepart (dw, t.time) IN (SELECT *
                                   FROM
                                     #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (datepart (dw, t.time) IN (SELECT *
                                   FROM
                                     #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;

  IF (@daymonth = '') AND (@weekdays != '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((datepart (dw, t.time) IN (SELECT *
                                    FROM
                                      #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((datepart (dw, t.time) IN (SELECT *
                                    FROM
                                      #tempweeks))
        AND -- чтение по дням недели
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;

  IF (@daymonth != '') AND (@weekdays = '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- чтение по дням месяца
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;

  IF (@daymonth = '') AND (@weekdays = '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend))
      ORDER BY
        t.time; -- чтение дат
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend)))
      ORDER BY
        t.time; -- чтение дат
    END;
  END;
END
GO

--
-- Создать процедуру "dbo.online_table"
--
GO
PRINT (N'Создать процедуру "dbo.online_table"')
GO

CREATE PROCEDURE dbo.online_table
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT Log_ID
       , MobitelId
       , (SELECT TOP 1 DataGPS_ID
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS 'DataGPS_ID'
       , (SELECT TOP 1 dbo.from_unixtime(unixtime)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS 'time'
       , (SELECT TOP 1 round((Latitude / 600000.00000), 6)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lat
       , (SELECT TOP 1 round((Longitude / 600000.00000), 6)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lon
       , (SELECT TOP 1 Altitude
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Altitude
       , (SELECT TOP 1 (Speed * 1.852)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS speed
       , (SELECT TOP 1 ((Direction * 360) / 255)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS direction
       , (SELECT TOP 1 Valid
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Valid
       , (SELECT TOP 1 (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936))
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS sensor
       , (SELECT TOP 1 Events
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Events

  FROM
    (SELECT max(LogID) AS Log_ID
          , mobitel_id AS MobitelId
     FROM
       online
     GROUP BY
       Mobitel_ID) T1;
END
GO

--
-- Создать процедуру "dbo.new_proc"
--
GO
PRINT (N'Создать процедуру "dbo.new_proc"')
GO


CREATE PROCEDURE dbo.new_proc
(
  @sensor     INTEGER,
  @m_id       INTEGER,
  @s_id       INTEGER,
  @begin_time DATETIME,
  @end_time   DATETIME
)
--SQL SECURITY INVOKER
--READS SQL DATA
AS
BEGIN
  SELECT round((datagps.Longitude / 600000), 6) AS lon
       , round((datagps.Latitude / 600000), 6) AS lat
       , datagps.Mobitel_ID AS Mobitel_ID
       , sensordata.value AS Value
       , (datagps.Speed * 1.852) AS speed
       , dbo.from_unixtime(datagps.UnixTime) AS [time]
       , sensordata.sensor_id AS sensor_id
       , datagps.DataGps_ID AS datagps_id
       , datagps.Sensor1 & @sensor AS sensor
  FROM
    (sensordata
    JOIN datagps
      ON ((sensordata.datagps_id = datagps.DataGps_ID)))
  WHERE
    (datagps.Valid = 1)
    AND (datagps.UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@begin_time) AND dbo.UNIX_TIMESTAMPS(@end_time))
    AND datagps.Mobitel_ID = @m_id
    AND sensordata.sensor_id = @s_id
  ORDER BY
    datagps.UnixTime
  , datagps.Mobitel_ID
  , sensordata.sensor_id;
END

GO

--
-- Создать процедуру "dbo.hystoryOnline"
--
GO
PRINT (N'Создать процедуру "dbo.hystoryOnline"')
GO

CREATE PROCEDURE dbo.hystoryOnline
(
  @id INTEGER
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT LogID
       , Mobitel_Id
       , dbo.FROM_UNIXTIME(unixtime) AS time
       , round((Latitude / 600000.00000), 6) AS Lat
       , round((Longitude / 600000.00000), 6) AS Lon
       , Altitude AS Altitude
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
       , Events AS Events
       , DataGPS_ID AS 'DataGPS_ID'
  FROM
    online
  WHERE
    datagps_id > @id
  ORDER BY
    datagps_id DESC;
END
GO

--
-- Создать процедуру "dbo.dataview"
--
GO
PRINT (N'Создать процедуру "dbo.dataview"')
GO

CREATE PROCEDURE dbo.dataview
(
  @m_id       INTEGER,
  @val        SMALLINT,
  @begin_time DATETIME,
  @end_time   DATETIME
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS time
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
       , Events AS Events
       , LogID AS LogID
  FROM
    datagps
  WHERE
    Mobitel_ID = @m_id
    AND (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@begin_time) AND dbo.UNIX_TIMESTAMPS(@end_time))
    AND (Valid = @val)
    AND NOT (Latitude = 0
    AND Longitude = 0)
  ORDER BY
    UnixTime;
END
GO

--
-- Создать процедуру "dbo.CheckOdo"
--
GO
PRINT (N'Создать процедуру "dbo.CheckOdo"')
GO


CREATE PROCEDURE dbo.CheckOdo
(
  @m_id     INTEGER,
  @end_time DATETIME
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT dbo.FROM_UNIXTIME(dbo.datagps.UnixTime) AS [time]
       , (dbo.datagps.Latitude / 600000.00000) AS Lat
       , (dbo.datagps.Longitude / 600000.00000) AS Lon
  FROM
    dbo.datagps
  WHERE
    dbo.datagps.Mobitel_ID = @m_id
    AND dbo.datagps.UnixTime BETWEEN (SELECT dbo.UNIX_TIMESTAMPS(odoTime)
                                  FROM
                                    dbo.vehicle
                                  WHERE
                                    Mobitel_id = @m_id) AND dbo.UNIX_TIMESTAMPS(@end_time)
    AND Valid = 1;
END

GO

--
-- Создать представление "dbo.tz_position"
--
GO
PRINT (N'Создать представление "dbo.tz_position"')
GO
CREATE VIEW dbo.tz_position
AS
SELECT
  vehicle.id
 ,team.Name AS [group]
 ,vehicle.NumberPlate AS tz
 ,online.Latitude * 1.000000 / 600000 AS lat
 ,online.Longitude * 1.000000 / 600000 AS lon
 ,dbo.From_UnixTime(online.UnixTime) AS point_time
 ,vehicle_category.Name
FROM dbo.id_gps_id_mobitel
INNER JOIN dbo.online
  ON id_gps_id_mobitel.expr1 = online.DataGps_ID
  AND id_gps_id_mobitel.Mobitel_ID = online.Mobitel_ID
INNER JOIN dbo.vehicle
  ON vehicle.Mobitel_id = id_gps_id_mobitel.Mobitel_ID
INNER JOIN dbo.team
  ON vehicle.Team_id = team.id
LEFT OUTER JOIN dbo.vehicle_category
  ON vehicle.Category_id = vehicle_category.Id
WHERE (vehicle.Team_id = 2
OR vehicle.Team_id = 4
OR vehicle.Team_id = 6
OR vehicle.Team_id = 8
OR vehicle.Team_id = 12
OR vehicle.Team_id = 13
OR vehicle.Team_id = 14
OR vehicle.Team_id = 17
OR vehicle.Team_id = 18
  OR vehicle.Team_id = 10
  OR vehicle.Team_id = 24)
AND online.Latitude > 0
OR (vehicle.Team_id = 2
OR vehicle.Team_id = 4
OR vehicle.Team_id = 6
OR vehicle.Team_id = 8
OR vehicle.Team_id = 12
OR vehicle.Team_id = 13
OR vehicle.Team_id = 14
OR vehicle.Team_id = 17
OR vehicle.Team_id = 18
  OR vehicle.Team_id = 10
  OR vehicle.Team_id = 24)
AND online.Longitude > 0
GO

--
-- Добавить расширенное свойство "MS_DiagramPane1" для "dbo.tz_position" (представление)
--
PRINT (N'Добавить расширенное свойство "MS_DiagramPane1" для "dbo.tz_position" (представление)')
GO
EXEC sys.sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "id_gps_id_mobitel"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 84
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "online"
            Begin Extent = 
               Top = 16
               Left = 451
               Bottom = 240
               Right = 602
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vehicle"
            Begin Extent = 
               Top = 116
               Left = 28
               Bottom = 293
               Right = 179
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "team"
            Begin Extent = 
               Top = 114
               Left = 227
               Bottom = 222
               Right = 380
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      E', 'SCHEMA', N'dbo', 'VIEW', N'tz_position'
GO

--
-- Добавить расширенное свойство "MS_DiagramPane2" для "dbo.tz_position" (представление)
--
PRINT (N'Добавить расширенное свойство "MS_DiagramPane2" для "dbo.tz_position" (представление)')
GO
EXEC sys.sp_addextendedproperty N'MS_DiagramPane2', N'nd
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'tz_position'
GO

--
-- Добавить расширенное свойство "MS_DiagramPaneCount" для "dbo.tz_position" (представление)
--
PRINT (N'Добавить расширенное свойство "MS_DiagramPaneCount" для "dbo.tz_position" (представление)')
GO
EXEC sys.sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'tz_position'
GO

--
-- Создать представление "dbo.gps_check"
--
GO
PRINT (N'Создать представление "dbo.gps_check"')
GO
CREATE VIEW dbo.gps_check 
AS SELECT vehicle.NumberPlate AS tz
        , dbo.from_unixtime(max(online.UnixTime)) AS date_time
   FROM
     dbo.vehicle
     INNER JOIN dbo.online
       ON vehicle.Mobitel_id = online.Mobitel_ID
   WHERE
     Team_id = 13
   GROUP BY
     vehicle.NumberPlate
   , vehicle.Mobitel_id
   , online.Mobitel_ID
GO

--
-- Создать представление "dbo.dataview1"
--
GO
PRINT (N'Создать представление "dbo.dataview1"')
GO
CREATE VIEW dbo.dataview1 
AS SELECT datagps.DataGps_ID AS DataGps_ID
        , datagps.Mobitel_ID AS Mobitel_ID
        , (datagps.Latitude / 600000.00000) AS Lat
        , (datagps.Longitude / 600000.00000) AS Lon
        , datagps.Altitude AS Altitude
        , dbo.FROM_UNIXTIME(datagps.UnixTime) AS time
        , (datagps.Speed * 1.852) AS speed
        , ((datagps.Direction * 360) / 255) AS direction
        , datagps.Valid AS Valid
        , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
        , datagps.Events AS Events
        , datagps.LogID AS LogID
   FROM
     datagps
GO

--
-- Создать представление "dbo.datavalue1"
--
GO
PRINT (N'Создать представление "dbo.datavalue1"')
GO

CREATE VIEW dbo.datavalue1
AS SELECT round((dbo.datagps.Longitude / 600000), 6) AS lon
        , round((dbo.datagps.Latitude / 600000), 6) AS lat
        , dbo.datagps.Mobitel_ID AS Mobitel_ID
        , dbo.sensordata.value AS Value
        , (dbo.datagps.Speed * 1.852) AS speed
        , dbo.from_unixtime(dbo.datagps.UnixTime) AS [time]
        , dbo.sensordata.sensor_id AS sensor_id
        , dbo.datagps.DataGps_ID AS datavalue_id
   FROM
     (dbo.sensordata
     JOIN dbo.datagps
       ON ((dbo.sensordata.datagps_id = dbo.datagps.DataGps_ID)))
   WHERE
     (dbo.datagps.Valid = 1);

GO

--
-- Создать пользователя "user3c"
--
PRINT (N'Создать пользователя "user3c"')
GO
CREATE USER user3c
  WITHOUT LOGIN
GO

--
-- Создать пользователя "user2C"
--
PRINT (N'Создать пользователя "user2C"')
GO
CREATE USER user2C
  WITHOUT LOGIN
GO

--
-- Создать пользователя "user1C_zones"
--
PRINT (N'Создать пользователя "user1C_zones"')
GO
CREATE USER user1C_zones
  WITHOUT LOGIN
GO

--
-- Создать пользователя "[MG\vyarema]"
--
PRINT (N'Создать пользователя "[MG\vyarema]"')
GO
CREATE USER [MG\vyarema]
  WITHOUT LOGIN
GO

--
-- Создать пользователя "[MG\vbilyavskyi]"
--
PRINT (N'Создать пользователя "[MG\vbilyavskyi]"')
GO
CREATE USER [MG\vbilyavskyi]
  WITHOUT LOGIN
GO

--
-- Создать пользователя "mca2"
--
PRINT (N'Создать пользователя "mca2"')
GO
CREATE USER mca2
  WITHOUT LOGIN
GO

--
-- Создать пользователя "mca"
--
PRINT (N'Создать пользователя "mca"')
GO
CREATE USER mca
  WITHOUT LOGIN
GO

--
-- Создать пользователя "dev1"
--
PRINT (N'Создать пользователя "dev1"')
GO
CREATE USER dev1
  WITHOUT LOGIN
GO

--
-- Создать пользователя "callcentr"
--
PRINT (N'Создать пользователя "callcentr"')
GO
CREATE USER callcentr
  WITHOUT LOGIN
GO

--
-- Создать таблицу "dbo.zonerelations"
--
PRINT (N'Создать таблицу "dbo.zonerelations"')
GO
CREATE TABLE dbo.zonerelations (
  ZoneRelation_ID int IDENTITY,
  Zone_ID int NULL DEFAULT (0),
  ConfigZoneSet_ID int NULL DEFAULT (0),
  In_flag smallint NOT NULL DEFAULT (0),
  Out_flag smallint NOT NULL DEFAULT (0),
  In_flag2 smallint NOT NULL DEFAULT (0),
  Out_flag2 smallint NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.usersinroles"
--
PRINT (N'Создать таблицу "dbo.usersinroles"')
GO
CREATE TABLE dbo.usersinroles (
  UserID int NOT NULL,
  RoleID int NOT NULL
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.usersinroles" (таблица)
--
PRINT (N'Добавить расширенное свойство "MS_SSMA_SOURCE" для "dbo.usersinroles" (таблица)')
GO
EXEC sys.sp_addextendedproperty N'MS_SSMA_SOURCE', N'rcs.usersinroles', 'SCHEMA', N'dbo', 'TABLE', N'usersinroles'
GO

--
-- Создать таблицу "dbo.tmpdatagps"
--
PRINT (N'Создать таблицу "dbo.tmpdatagps"')
GO
CREATE TABLE dbo.tmpdatagps (
  DataGps_ID int NOT NULL DEFAULT (0),
  Mobitel_ID int NULL DEFAULT (0),
  Message_ID int NULL DEFAULT (0),
  Latitude int NULL DEFAULT (0),
  Longitude int NULL DEFAULT (0),
  Altitude int NULL,
  UnixTime int NULL DEFAULT (0),
  Speed smallint NULL DEFAULT (0),
  Direction int NOT NULL DEFAULT (0),
  Valid smallint NULL DEFAULT (1),
  InMobitelID int NULL,
  Events bigint NULL DEFAULT (0),
  Sensor1 tinyint NULL DEFAULT (0),
  Sensor2 tinyint NULL DEFAULT (0),
  Sensor3 tinyint NULL DEFAULT (0),
  Sensor4 tinyint NULL DEFAULT (0),
  Sensor5 tinyint NULL DEFAULT (0),
  Sensor6 tinyint NULL DEFAULT (0),
  Sensor7 tinyint NULL DEFAULT (0),
  Sensor8 tinyint NULL DEFAULT (0),
  LogID int NULL DEFAULT (0),
  isShow smallint NULL DEFAULT (0),
  whatIs int NULL DEFAULT (0),
  Counter1 int NOT NULL DEFAULT (-1),
  Counter2 int NOT NULL DEFAULT (-1),
  Counter3 int NOT NULL DEFAULT (-1),
  Counter4 int NOT NULL DEFAULT (-1),
  SrvPacketID bigint NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.tmpdatagps.SrvPacketID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.tmpdatagps.SrvPacketID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'ID серверного пакета в состав которого входит эта запись', 'SCHEMA', N'dbo', 'TABLE', N'tmpdatagps', 'COLUMN', N'SrvPacketID'
GO

--
-- Создать таблицу "dbo.tempgps"
--
PRINT (N'Создать таблицу "dbo.tempgps"')
GO
CREATE TABLE dbo.tempgps (
  DataGps_ID int IDENTITY,
  Mobitel_ID int NULL,
  time datetime NULL,
  speed numeric(10, 3) NULL,
  LogID int NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.temp"
--
PRINT (N'Создать таблицу "dbo.temp"')
GO
CREATE TABLE dbo.temp (
  datagps_id bigint NOT NULL DEFAULT (0),
  sensor_id bigint NOT NULL DEFAULT (0),
  Value float NOT NULL DEFAULT (0.00)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.sensor_hookups"
--
PRINT (N'Создать таблицу "dbo.sensor_hookups"')
GO
CREATE TABLE dbo.sensor_hookups (
  ID int IDENTITY,
  Mobitel_ID int NULL,
  Sensor_ID int NULL,
  FirstBit int NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.sensorcoefficient"
--
PRINT (N'Создать таблицу "dbo.sensorcoefficient"')
GO
CREATE TABLE dbo.sensorcoefficient (
  id bigint IDENTITY,
  Sensor_id bigint NOT NULL,
  UserValue float NOT NULL,
  SensorValue float NOT NULL,
  K float NOT NULL DEFAULT (1.00000),
  b float NOT NULL DEFAULT (0.00000)
)
ON [PRIMARY]
GO

--
-- Создать индекс "Index_2" для объекта типа таблица "dbo.sensorcoefficient"
--
PRINT (N'Создать индекс "Index_2" для объекта типа таблица "dbo.sensorcoefficient"')
GO
CREATE INDEX Index_2
  ON dbo.sensorcoefficient (Sensor_id, SensorValue)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.route_points"
--
PRINT (N'Создать таблицу "dbo.route_points"')
GO
CREATE TABLE dbo.route_points (
  ID int IDENTITY,
  Route_ID int NULL,
  Zone_ID int NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.route_mobitel_links"
--
PRINT (N'Создать таблицу "dbo.route_mobitel_links"')
GO
CREATE TABLE dbo.route_mobitel_links (
  ID int IDENTITY,
  List_ID int NULL,
  Route_ID int NULL,
  Mobitel_ID int NULL,
  Time1 int NULL,
  Time2 int NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.route_items"
--
PRINT (N'Создать таблицу "dbo.route_items"')
GO
CREATE TABLE dbo.route_items (
  ID int IDENTITY,
  Seq int NULL,
  Route_ID int NULL,
  Zone_ID int NULL,
  Zone_IO int NULL,
  GoNext_Min int NULL,
  GoNext_Max int NULL,
  Link_Item int NULL,
  Link_Info int NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.relationalgorithms"
--
PRINT (N'Создать таблицу "dbo.relationalgorithms"')
GO
CREATE TABLE dbo.relationalgorithms (
  ID int IDENTITY,
  AlgorithmID int NOT NULL DEFAULT (0),
  SensorID int NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "ID" для объекта типа таблица "dbo.relationalgorithms"
--
PRINT (N'Создать индекс "ID" для объекта типа таблица "dbo.relationalgorithms"')
GO
CREATE UNIQUE INDEX ID
  ON dbo.relationalgorithms (ID)
  WITH (FILLFACTOR = 80)
  ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.data_zones"
--
PRINT (N'Создать таблицу "dbo.data_zones"')
GO
CREATE TABLE dbo.data_zones (
  ID int IDENTITY,
  DataGps_ID int NULL,
  Zone_ID int NULL,
  Zone_IO int NULL
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.datagpslost_rftmp"
--
PRINT (N'Создать таблицу "dbo.datagpslost_rftmp"')
GO
CREATE TABLE dbo.datagpslost_rftmp (
  LogID int NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать таблицу "dbo.datagpslost"
--
PRINT (N'Создать таблицу "dbo.datagpslost"')
GO
CREATE TABLE dbo.datagpslost (
  Mobitel_ID int NOT NULL DEFAULT (0),
  Begin_LogID int NOT NULL DEFAULT (0),
  End_LogID int NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO

--
-- Создать индекс "IDX_MobitelID" для объекта типа таблица "dbo.datagpslost"
--
PRINT (N'Создать индекс "IDX_MobitelID" для объекта типа таблица "dbo.datagpslost"')
GO
CREATE INDEX IDX_MobitelID
  ON dbo.datagpslost (Mobitel_ID)
  ON [PRIMARY]
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagpslost.Begin_LogID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagpslost.Begin_LogID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение LogID после которого начинается разрыв', 'SCHEMA', N'dbo', 'TABLE', N'datagpslost', 'COLUMN', N'Begin_LogID'
GO

--
-- Добавить расширенное свойство "MS_Description" для "dbo.datagpslost.End_LogID" (столбец)
--
PRINT (N'Добавить расширенное свойство "MS_Description" для "dbo.datagpslost.End_LogID" (столбец)')
GO
EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение LogID перед которым завершается разрыв', 'SCHEMA', N'dbo', 'TABLE', N'datagpslost', 'COLUMN', N'End_LogID'
GO

--
-- Создать таблицу "dbo.agro_ordert_calc"
--
PRINT (N'Создать таблицу "dbo.agro_ordert_calc"')
GO
CREATE TABLE dbo.agro_ordert_calc (
  Id int IDENTITY,
  Id_main int NOT NULL,
  Id_field int NULL,
  Id_driver int NOT NULL,
  Id_agregat int NULL,
  Id_work int NULL,
  CONSTRAINT PK_agro_ordert_calc PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
GO

--
-- Создать процедуру "dbo.sp_sela"
--
GO
PRINT (N'Создать процедуру "dbo.sp_sela"')
GO
CREATE PROCEDURE dbo.sp_sela 
@selo varchar(50)
as 
begin
select tab1.id, tab1.name, tab2.name, tab3.name from (select id, name, left(id,2)+'00000000' as obl, left(id,5)+'00000' as rn 
from docs_agro.dbo.d_koatuu  where name like @selo  AND left(id,2) in ('05', '07', '18', '26', '46', '56', '61', '68', '73')) tab1
left join d_koatuu tab2 on tab1.obl = tab2.id
left join d_koatuu tab3 on tab1.rn = tab3.id
end
GO

--
-- Создать процедуру "dbo.MaintenanceRebuildIndex"
--
GO
PRINT (N'Создать процедуру "dbo.MaintenanceRebuildIndex"')
GO
-- =============================================
-- Description:	<Перестройка индексов>
-- =============================================
CREATE PROCEDURE dbo.MaintenanceRebuildIndex
AS
BEGIN

-- Использование функции sys.dm_db_index_physical_stats в сценарии 
-- для перестройки или реорганизации индексов
SET NOCOUNT ON;
DECLARE @objectid int;
DECLARE @indexid int;
DECLARE @partitioncount bigint;
DECLARE @schemaname nvarchar(130); 
DECLARE @objectname nvarchar(130); 
DECLARE @indexname nvarchar(130); 
DECLARE @partitionnum bigint;
DECLARE @partitions bigint;
DECLARE @frag float;
DECLARE @command nvarchar(4000);
DECLARE @indexType nvarchar(130);
DECLARE @engineEdition int; -- редакция движка сервера

-- Conditionally select tables and indexes from the sys.dm_db_index_physical_stats function 
-- and convert object and index IDs to names.
SELECT
  object_id AS objectid,
  index_id AS indexid,
  index_type_desc AS indexType,
  partition_number AS partitionnum,
  avg_fragmentation_in_percent AS frag
INTO #work_to_do
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, 'LIMITED')
WHERE avg_fragmentation_in_percent > 5.0 AND index_id > 0
ORDER BY object_id;

-- Определяем редакцию движка сервера
SELECT @engineEdition = CAST(SERVERPROPERTY('EngineEdition') AS INT); 

-- Declare the cursor for the list of partitions to be processed.
DECLARE partitions CURSOR FOR SELECT * FROM #work_to_do;

-- Open the cursor.
OPEN partitions;

-- Loop through the partitions.
WHILE (1=1)
BEGIN
  FETCH NEXT FROM partitions
     INTO @objectid, @indexid, @indexType, @partitionnum, @frag;

  IF @@FETCH_STATUS < 0 BREAK;

  SELECT @objectname = QUOTENAME(o.name), @schemaname = QUOTENAME(s.name)
  FROM sys.objects AS o
    JOIN sys.schemas as s ON s.schema_id = o.schema_id
  WHERE o.object_id = @objectid;

  -- Эта таблица нас не итересует
  IF @objectname = N'[sysdiagrams]'
    CONTINUE;

  SELECT @indexname = QUOTENAME(name)
  FROM sys.indexes
  WHERE (object_id = @objectid) AND (index_id = @indexid);

  SELECT @partitioncount = count(*)
  FROM sys.partitions
  WHERE (object_id = @objectid) AND (index_id = @indexid);

  -- 30 is an arbitrary decision point at which to switch between reorganizing and rebuilding.
  IF (@frag < 30.0)
  BEGIN
    SET @command = N'ALTER INDEX ' + @indexname + N' ON ' +  @schemaname + N'.' + @objectname + N' REORGANIZE';
  END ELSE
  BEGIN
    -- ONLINE = ON работает только в редакции движка сервера = 3 (Enterprise, Enterprise Evaluation, Developer).
    -- Кластерные индексы нельзя перестраивать с опцией ONLINE = ON, если в таблице присутсвуют 
    -- поля следующих типов: text, ntext, image, varchar(max), nvarchar(max), varbinary(max), xml. 
    IF (@engineEdition != 3) OR ((@indexType = N'CLUSTERED INDEX') AND 
       ((@objectname = N'[ClientInfo]') OR (@objectname = N'[Command]') OR (@objectname = N'[DealerInfo]') OR 
       (@objectname = N'[ExeptError]') OR (@objectname = N'[UserInfo]')))
    BEGIN   
      SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + 
        N' REBUILD WITH(FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = ON, ONLINE = OFF)';
    END ELSE
    BEGIN
      SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + 
        N' REBUILD WITH(FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON, SORT_IN_TEMPDB = ON, ONLINE = ON)'; 
    END
  END

  IF (@partitioncount > 1)
    SET @command = @command + N' PARTITION=' + CAST(@partitionnum AS nvarchar(10));
   
  -- PRINT N'Executed: ' + @command;
  EXEC (@command);
END

-- Close and deallocate the cursor.
CLOSE partitions;
DEALLOCATE partitions;

-- Drop the temporary table.
DROP TABLE #work_to_do; 

END
GO

--
-- Создать процедуру "dbo.IsTeletrackDatabase"
--
GO
PRINT (N'Создать процедуру "dbo.IsTeletrackDatabase"')
GO


CREATE PROCEDURE dbo.IsTeletrackDatabase
--SQL SECURITY INVOKER
--COMMENT 'Проверка базы данных на соответствие структуре системы Teletrack'
AS
BEGIN
  DECLARE @i TINYINT;
END

GO

--
-- Создать процедуру "dbo.ExecuteQuery"
--
GO
PRINT (N'Создать процедуру "dbo.ExecuteQuery"')
GO

CREATE PROCEDURE dbo.ExecuteQuery
(
  @sqlQuery VARCHAR
)
AS
BEGIN
  DECLARE @s VARCHAR;
  SET @s = @sqlQuery;
  EXECUTE (@s);
END

GO

--
-- Создать процедуру "dbo.CheckDB"
--
GO
PRINT (N'Создать процедуру "dbo.CheckDB"')
GO

CREATE PROCEDURE dbo.CheckDB
--SQL SECURITY INVOKER
--COMMENT 'Проверка БД при подключении.'
AS
BEGIN
  SELECT 'OK';
END

GO

--
-- Создать функцию "dbo.get_hh_min"
--
GO
PRINT (N'Создать функцию "dbo.get_hh_min"')
GO
CREATE FUNCTION dbo.get_hh_min
(
  @minute FLOAT
)
RETURNS TIME
AS
BEGIN
	DECLARE @tm VARCHAR(5);
	DECLARE @times TIME;
  if (@minute<0)
  BEGIN
	SET @tm = '00:00';
  END
  ELSE
	BEGIN			
	  DECLARE @hours INT;
	  SET @hours = @minute / 60.0;
	  DECLARE @mn INT;
	  SET @mn = @minute - @hours * 60;
	  SET @tm = convert(VARCHAR(2), @hours) + ':' + convert(VARCHAR(2), @mn);
	END
	SET @times = convert(TIME, @tm);
	RETURN @times;
END;
GO
SET NOEXEC OFF
GO