using System;
using System.ComponentModel;
using System.Configuration.Install;
using System.IO;
using System.Xml.Serialization;
using RCS.SrvKernel;
using RCS.SrvKernel.ErrorHandling;

namespace RCS.TDataMngr
{
  [RunInstaller(true)]
  public partial class TDataMngrInstaller : Installer
  {
    public TDataMngrInstaller()
    {
      InitializeComponent();
      InitServiceName();
    }

    private void InitServiceName()
    {
      if (!File.Exists(SrvSettings.FullPath))
        throw new ApplicationException(String.Format(
          AppError.SETTINGS_FILE_NOT_FOUND,
          SrvSettings.FullPath));

      XmlSerializer xmlSer = new XmlSerializer(typeof(Settings));
      StreamReader xmlFile = new StreamReader(SrvSettings.FullPath);
      Settings settings = (Settings)xmlSer.Deserialize(xmlFile);
      xmlFile.Close();

      if (!String.IsNullOrEmpty(settings.ServiceName))
        this.serviceInstaller.ServiceName = settings.ServiceName;
    }
  }
}