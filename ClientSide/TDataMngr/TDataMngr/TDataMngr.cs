#region Using directives

using RCS.SrvKernel;
using RCS.SrvKernel.SrvSystem;
using RCS.TDataMngrLib;
#endregion

namespace RCS.TDataMngr
{
  public partial class TDataMngr : SrvService
  {
    public TDataMngr()
    {
      InitializeComponent();
    }

    protected override void OnStart(string[] args)
    {
      base.OnStart(args);
    }

    protected override void OnStop()
    {
      base.OnStop();
    }

    /// <summary>
    /// �������� ��������. ������� ����������� ���
    /// �������� ����� ������������� �������
    /// </summary>
    protected override void LoadSettings()
    {
      base.LoadSettings();
      TSettings.LoadSettings();
    }

    #region HASP

    /// <summary>
    /// ����� �������� � Hasp-����� = 5
    /// </summary>
    private const int LN = 5;

    /// <summary>
    /// ����� �������� ��� ������ � Hasp-�����.
    /// </summary>
    /// <returns>Int</returns>
    protected override int GetLnumber()
    {
      return LN;
    }

    #endregion

    /// <summary>
    /// Create main controller
    /// </summary>
    /// <returns>IMain controller</returns>
    protected override IMainController CreateMainController()
    {
      return new TMainController(this);
    }

    #region ��������� �����/���������

    private const string RUNNING_MESSAGE =
        "\r\n ------------------------------\r\n �������� ������ �����������... \r\n ------------------------------\r\n";
    private const string RUN_MESSAGE = "\r\n\r\n �������� ������ ������� ��������� \r\n";

    /// <summary>
    /// ��������� ������ �������� ������
    /// </summary>
    /// <returns>String</returns>
    protected override string GetRunningMessage()
    {
      return RUNNING_MESSAGE;
    }

    /// <summary>
    /// ��������� � ������ � ������� �����
    /// </summary>
    /// <returns>String</returns>
    protected override string GetRunMessage()
    {
      return RUN_MESSAGE;
    }

    private const string STOPPING_MESSAGE = "\r\n\r\n �������� ������ ���������������...\r\n";
    private const string STOPPED_MESSAGE =
      "\r\n --------------------------\r\n �������� ������ ���������� \r\n --------------------------\r\n";

    /// <summary>
    /// ��������� ������ �������� ���������
    /// </summary>
    /// <returns>String</returns>
    protected override string GetStoppingMessage()
    {
      return STOPPING_MESSAGE;
    }

    /// <summary>
    /// ��������� �� ���������
    /// </summary>
    /// <returns>String</returns>
    protected override string GetStoppedMessage()
    {
      return STOPPED_MESSAGE;
    }

    #endregion
  }
}
