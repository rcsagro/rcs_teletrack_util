namespace RCS.TDataMngr
{
  partial class TDataMngrInstaller
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.TDataMngrServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
      this.serviceInstaller = new System.ServiceProcess.ServiceInstaller();
      // 
      // TDataMngrServiceProcessInstaller
      // 
      this.TDataMngrServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
      this.TDataMngrServiceProcessInstaller.Password = null;
      this.TDataMngrServiceProcessInstaller.Username = null;
      // 
      // serviceInstaller
      // 
      this.serviceInstaller.Description = "����� ������� � �����������, ����������� � online ������,  ����� �������������� �" +
          "�����";
      this.serviceInstaller.ServiceName = "TDataMngr";
      this.serviceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
      // 
      // TDataMngrInstaller
      // 
      this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.TDataMngrServiceProcessInstaller,
            this.serviceInstaller});

    }

    #endregion

    private System.ServiceProcess.ServiceProcessInstaller TDataMngrServiceProcessInstaller;
    private System.ServiceProcess.ServiceInstaller serviceInstaller;
  }
}