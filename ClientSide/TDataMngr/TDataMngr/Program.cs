using System;
using System.ServiceProcess;

namespace RCS.TDataMngr
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG
            if (Environment.UserInteractive)
            {
                // ������ � ������ ��� �������.
                new TDataMngr().RunDebug();
                Console.WriteLine("Press any key to stop Service1 and exit...");
                Console.ReadLine();
            }
            else
            {
                RunAsService();
            }
#else
      RunAsService();
#endif
        }

        private static void RunAsService()
        {
            ServiceBase[] ServicesToRun;

            // More than one user Service may run within the same process. To add
            // another service to this process, change the following line to
            // create a second service object. For example,
            //
            //   ServicesToRun = new ServiceBase[] {new Service1(), new MySecondUserService()};
            //
            ServicesToRun = new ServiceBase[] {new TDataMngr()};

            ServiceBase.Run(ServicesToRun);
        }
    }
}