﻿CREATE DEFINER = 'user'@'%'
PROCEDURE vesnyane_agro.delDupSrvPacket()
  COMMENT 'Удаляет дубликаты SrvPacketID из таблицы datagpsbuffer_on64'
BEGIN
  IF EXISTS( SELECT * FROM information_schema.table_constraints
             WHERE `TABLE_SCHEMA` = DATABASE() AND
                   `TABLE_NAME`   = 'datagpsbuffer_on64' )
        THEN
        IF ((SELECT COUNT(*) FROM datagpsbuffer_on64 d) <> 0)
        THEN
            DROP TABLE IF EXISTS tmp64;

            ALTER TABLE `datagpsbuffer_on64` DROP FOREIGN KEY `FK_datagps64_mobitels_Mobitel_ID_1`;
            ALTER TABLE `datagpsbuffer_on64` DROP INDEX `IDX_SrvPacketID_64`;
            ALTER TABLE `datagpsbuffer_on64` DROP INDEX `IDX_MobitelidUnixtimeValid_64`;
            ALTER TABLE `datagpsbuffer_on64` DROP INDEX `IDX_MobitelidUnixtime_64`;
            ALTER TABLE `datagpsbuffer_on64` DROP INDEX `IDX_MobitelidLogid_64`;

  CREATE TABLE tmp64 (
  Mobitel_ID int(11) NOT NULL DEFAULT 0 COMMENT 'Код Телетрека',
  Latitude int(11) NOT NULL DEFAULT 0 COMMENT 'Широта',
  Longitude int(11) NOT NULL DEFAULT 0 COMMENT 'Долгота',
  Direction tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'градусы/1.5',
  Acceleration int(11) NOT NULL DEFAULT 0 COMMENT 'Изменение скорости по GPS за последнюю секунду',
  UnixTime int(11) NOT NULL DEFAULT 0,
  Speed smallint(6) NOT NULL DEFAULT 0 COMMENT 'Сотен метров в час (0,1 км/ч)',
  Valid tinyint(1) NOT NULL DEFAULT 1,
  Satellites tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Количество спутников ',
  RssiGsm tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Уровень GSM сигнала',
  `Events` int(11) NOT NULL DEFAULT 0 COMMENT 'Флаги событий',
  SensorsSet tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Код набора датчиков',
  Sensors varbinary(50) NOT NULL DEFAULT '0' COMMENT 'Значения датчиков',
  Voltage tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Напряжение питания текущего источника/1.5 от 0 до 38,2',
  DGPS varchar(32) NOT NULL DEFAULT '0' COMMENT 'Данные дифференциальной системы GPS - высота,широта,долгота',
  LogID int(11) NOT NULL DEFAULT 0 COMMENT 'Идентификатор записи в журнале Телетрека',
  SrvPacketID bigint(20) NOT NULL DEFAULT 0 COMMENT 'ID серверного пакета в состав которого входит эта запись',
  INDEX IDX_MobitelidLogid_64 (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime_64 (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid_64 (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_SrvPacketID_64 (SrvPacketID),
  CONSTRAINT FK_datagps64_mobitels_Mobitel_ID_1 FOREIGN KEY (Mobitel_ID)
  REFERENCES vesnyane_agro.mobitels (Mobitel_ID) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;  

INSERT INTO tmp64 SELECT Mobitel_ID,
  Latitude,
  Longitude,
  Direction,
  Acceleration,
  UnixTime,
  Speed,
  Valid,
  Satellites,
  RssiGsm,
  `Events`,
  SensorsSet,
  Sensors,
  Voltage,
  DGPS,
  LogID,
  SrvPacketID
FROM datagpsbuffer_on64 d
GROUP BY d.SrvPacketID;

DROP TABLE IF EXISTS datagpsbuffer_on64;
ALTER TABLE tmp64 RENAME TO datagpsbuffer_on64;
END IF;
END IF;
END