DELIMITER $$
DROP PROCEDURE IF EXISTS OnDeleteLostRange$$
CREATE PROCEDURE OnDeleteLostRange(IN MobitelID INTEGER(11), IN BeginSrvPacketID BIGINT)
    NOT DETERMINISTIC
    CONTAINS SQL                          
    SQL SECURITY INVOKER
    COMMENT '�������� ���������� ��������� ��������� �� datagpslost_on.'
BEGIN
  /* ���������� �������� �� ������, ��������������� ��� ��������,
     ������ � ������ ��������� ���������. ���� �������� - �����
     ������ �������� � �������� ConfirmedID */  
     
  DECLARE MinBeginSrvPacketID BIGINT; -- ����������� �������� Begin_SrvPacketID ��� ������� ���������
  DECLARE NewConfirmedID INT; -- ����� �������� ConfirmedID  
  

  /* ���������� ������������ �������� Begin_SrvPacketID */  
  SELECT MIN(Begin_SrvPacketID) INTO MinBeginSrvPacketID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;
  
  /* ���� ���� ������ � ��������� � ��������� - ��������� */
  IF MinBeginSrvPacketID IS NOT NULL THEN
    /* ���� ������� ������ ������ - �������� ����� �������� ConfirmedID */
    IF MinBeginSrvPacketID = BeginSrvPacketID  THEN
      SELECT End_LogID INTO NewConfirmedID
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID);
    END IF;
  
    /* �������� ������ */
    DELETE FROM  datagpslost_on
    WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID);
    
    /* ���� ������� ������ ������ - ������� ConfirmedID */  
    IF MinBeginSrvPacketID = BeginSrvPacketID  THEN
      UPDATE mobitels
      SET ConfirmedID = NewConfirmedID
      WHERE (Mobitel_ID = MobitelID) AND (NewConfirmedID > ConfirmedID);
    END IF;
  END IF;  

END$$
DELIMITER ;