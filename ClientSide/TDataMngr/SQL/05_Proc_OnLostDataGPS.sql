DELIMITER $$
DROP PROCEDURE IF EXISTS OnLostDataGPS$$
CREATE PROCEDURE OnLostDataGPS(IN MobitelID INTEGER(11), OUT NeedOptimize TINYINT)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '����� ��������� � online ������ ��������� ��������� � ���������� ������.'
BEGIN

  /********** DECLARE **********/
   
  DECLARE CurrentConfirmedID INT DEFAULT 0; /* ������� �������� ConfirmedID */
  DECLARE NewConfirmedID INT DEFAULT 0; /* ����� �������� ConfirmedID */
  DECLARE StartLogIDSelect INT DEFAULT 0; /* min LogID ��� ������� */
  DECLARE FinishLogIDSelect INT DEFAULT 0; /* max LogID ��� ������� */
  DECLARE MaxEndLogID INT DEFAULT 0; /* max End_LogID � ����������� ���������� ������� ��������� */
  
  DECLARE MinLogID_n INT DEFAULT 0; /* ����������� LogID ����� ConfirmedID � ��������� ����� ������ */
  DECLARE MaxLogID_n INT DEFAULT 0; /* ������������ LogID � ��������� ����� ������ */
  
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
  DECLARE DataChanged TINYINT DEFAULT 0; /* ���� ��������� ��������� ���������� ������ */
    
  DECLARE NewRowCount INT DEFAULT 0; /* ���������� ����� ������� � ��������� */
  
  DECLARE NewLogID INT; /* �������� LogID � ������� CursorNewLogID */
  DECLARE NewSrvPacketID BIGINT; /* �������� SrvPacketID � ������� CursorNewLogID */
  DECLARE FirstLogID INT DEFAULT -9999999; /* ������ �������� LogID ��� ������� */
  DECLARE SecondLogID INT; /* ������ �������� LogID ��� ������� */
  DECLARE FirstSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
  DECLARE SecondSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
  
  DECLARE tmpSrvPacketID BIGINT DEFAULT 0; /* �������� SrvPacketID ��� ������ ������������� ������ � ������� ����� ������, �� �������������� � ������������� */  
  
  /* ������ ��� ������� �� ����� LogID */
  DECLARE CursorNewLogID CURSOR FOR
    SELECT LogID, SrvPacketID 
    FROM datagpslost_ontmp;
    
  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  /********** END DECLARE **********/

  SET NeedOptimize = 0;
 
  SELECT COALESCE(ConfirmedID, 0) INTO CurrentConfirmedID
  FROM mobitels
  WHERE Mobitel_ID = MobitelID;

  /* ����������� LogID � ����� ������ ������ c ������� ����� �������� */
  SELECT MIN(LogID) INTO MinLogID_n
  FROM datagpsbuffer_on
  WHERE (Mobitel_ID = MobitelID) AND (LogID > CurrentConfirmedID);
  
  /* ������������ LogID � ����� ������ ������ c ������� ����� �������� */
  SELECT MAX(LogID) INTO MaxLogID_n
  FROM datagpsbuffer_on 
  WHERE Mobitel_ID = MobitelID;  
  
  /* ������������ End_LogID � ����������� ���������� ������� ��������� */
  SELECT COALESCE(MAX(End_LogID), 0) INTO MaxEndLogID
  FROM datagpslost_on 
  WHERE Mobitel_ID = MobitelID;  
  
  /* �������� ���� �� ������ ��� ������� */
  IF MinLogID_n IS NOT NULL THEN
  
    /********** PREPARE **********/
    
    TRUNCATE TABLE datagpslost_ontmp;
  
    IF MinLogID_n < MaxEndLogID THEN
      /* ����  MinLogID_n < MaxEndLogID ������ ������ ������, 
       ������� �������� ������� ����� ���������. */
       
      /* ������� ��������� LogID ��� ���������. */
      SELECT COALESCE(Begin_LogID, MinLogID_n) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MinLogID_n > Begin_LogID) 
        AND (MinLogID_n < End_LogID);
      
      /* ������� ��������� LogID ��� ���������. */  
      SELECT COALESCE(End_LogID, MaxLogID_n) INTO FinishLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MaxLogID_n > Begin_LogID) 
        AND (MaxLogID_n < End_LogID);
       
      /* ������ ������ ������ �� ������� ����������� ������� 
         (����� �������������, ���� �����������) */
      DELETE FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID >= StartLogIDSelect)
        AND (Begin_LogID <= FinishLogIDSelect);
      
      /* ���� ������ ������ LogID = 0 � ���� ������ ��� � DataGps, �����
         ���� �������� ������ ��������� ������ ��� �������. */
      IF (StartLogIDSelect = 0) AND (NOT EXISTS
        (SELECT 1 
         FROM datagps 
         WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect))) 
      THEN
				INSERT INTO datagpslost_ontmp(LogID, SrvPacketID) VALUES (0, 0);  
      END IF;       
    ELSE
      /* ������ ����� ������. ���� �������� ��� ������� ������������� ������ - 
         ������ ������� */
      
      /* ������� ��������� LogID ��� ���������. */
      SELECT COALESCE(MAX(End_LogID), CurrentConfirmedID) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE Mobitel_ID = MobitelID;   
         
      /* ������� ��������� LogID ��� ���������. */  
      SELECT MAX(LogID)  INTO FinishLogIDSelect
      FROM datagpsbuffer_on
      WHERE Mobitel_ID = MobitelID;
      
      /* �������� �� datagps ��������������� �������� SrvPacketID  ��� ������
         ������������� ������ */
      SELECT COALESCE(SrvPacketID, 0) INTO tmpSrvPacketID
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect);
      
      /* �������� � ������� datagpslost_ontmp ��� ������� ��������� 
         �������������� ������ ������ */
      INSERT INTO datagpslost_ontmp(LogID, SrvPacketID)
      VALUES (StartLogIDSelect, tmpSrvPacketID);
    END IF; -- IF MinLogID_n < MaxEndLogID   
    
    /********** END PREPARE **********/
    
    /**********  INSERT ROWS **********/
    
    /* ���������� ��������� ������� ���������� ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      SELECT LogID, SrvPacketID
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND (LogID >= StartLogIDSelect)
        AND (LogID <= FinishLogIDSelect)
      ORDER BY LogID;
            
    /* ��������� ��� ������� �������  */
    SET @InsertLostDataStatement = 
      'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';
        
    /* ������������ �������-������� � ������� ����������� ���������� */
    SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    WHILE DataExists = 1 DO
      IF FirstLogID = -9999999 THEN
        /* ������ �������� � ������ */
        SET FirstLogID = NewLogID;
        SET FirstSrvPacketID = NewSrvPacketID;
      ELSE
        /* �������� ����� �������*/
        SET SecondLogID = NewLogID;
        SET SecondSrvPacketID = NewSrvPacketID;
        
        IF (((SecondLogID -  FirstLogID) > 1) AND (FirstSrvPacketID < SecondSrvPacketID)) THEN
          /* ������ ������ */
          IF NewRowCount > 0 THEN
            SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, ",");
          END IF;
          
          SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, 
            " (", MobitelID, ", ", FirstLogID, ", ", SecondLogID, ", ", 
            FirstSrvPacketID, ", ", SecondSrvPacketID, ")");
              
          SET NewRowCount = NewRowCount + 1;
        END IF;
        /* ���������� ������� ������ �������� �� ������ �������
           ��� ������� � ��������� �������� � �������� ���������� �������� */
        SET  FirstLogID = SecondLogID;
        SET FirstSrvPacketID = SecondSrvPacketID;
      END IF;  
      FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    END WHILE;
    CLOSE CursorNewLogID;
             
    IF NewRowCount > 0 THEN
      /*--------- ���������� ������� ����������� ������ ----------*/
      PREPARE stmt FROM @InsertLostDataStatement;
      EXECUTE stmt;
      DEALLOCATE PREPARE stmt;
          
      SET DataChanged = 1;
    END IF; -- NewRowCount > 0 
    
    /********** END INSERT ROWS **********/
               
    /********** UPDATE MOBITELS **********/
    
    /* ���� ����������� ������� � ������� ��������� ��� - 
       ����� ConfirmedID ����� ����� ������������� LogID 
       ������� ���������. ����� - ����������� Begin_LogID
       �� ������� ��������� */
    SELECT MIN(Begin_LogID) INTO NewConfirmedID
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;
       
    IF NewConfirmedID IS NULL THEN
      SELECT MAX(LogID) INTO NewConfirmedID
      FROM datagps
      WHERE Mobitel_ID = MobitelID;
    END IF;  
    
    /* ������� ConfirmedID ���� ���� */
    UPDATE mobitels
    SET ConfirmedID = NewConfirmedID
    WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);
    
    /********** END UPDATE MOBITELS **********/
        
    TRUNCATE TABLE datagpslost_ontmp;
  END IF; -- IF MaxEndLogID IS NOT NULL  
  
  IF DataChanged = 1 THEN
    SET NeedOptimize = 1;
  END IF;
END$$
DELIMITER ;
