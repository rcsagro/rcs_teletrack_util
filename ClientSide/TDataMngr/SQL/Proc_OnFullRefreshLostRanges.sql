/* ����� ���������� ��������� �� ���� ����������
   � ���������� ������� DataGpsLost_on.
   ����� �������� ��������� ���������� �����������
   ���������� TDataManager � ����������� ���������
   �������� �� ������. */

DELIMITER $$

DROP PROCEDURE IF EXISTS OnFullRefreshLostRanges$$

CREATE PROCEDURE OnFullRefreshLostRanges()
  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT '����� ���� ��������� ��� TDataManager'
BEGIN
  DECLARE MobitelIDInCursor INT; /* ������� �������� MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
    
  DECLARE LogIdInCursor INT; /* ������� �������� LogId ��������� */
  DECLARE SrvPacketIdInCursor BIGINT; /* ������� �������� SrvPacketID ��������� */
  DECLARE PreviousLogId INT; /* �������� LogId ��������� �� ���������� ����*/
  DECLARE PreviousSrvPacketId BIGINT; /* �������� SrvPacketID ��������� �� ���������� ���� */
  
  DECLARE FirstIteration TINYINT DEFAULT 1; /* ���� ������ �������� �� ������ */
  DECLARE NewRowCount INT DEFAULT 0; /* ���������� ����� ������� � ��������� */
  DECLARE NewConfirmedID INT; /* ����� �������� ConfirmedID */
  
  DECLARE MaxNegativeDataGpsId INT; /* ������������ DataGpsId � ��������� ������������� LogId */
  DECLARE MaxPositiveDataGpsId INT; /* ������������ DataGpsId � ��������� ������������� LogId */
     
  /* ������ �� ���� ���������� */
  DECLARE CursorMobitels CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) AS Mobitel_ID
    FROM mobitels
    ORDER BY Mobitel_ID;
    
  /* ������ �� ������ ��������� */
  DECLARE CursorDataGps CURSOR FOR
    SELECT LogId, SrvPacketID
    FROM DataGps
    WHERE Mobitel_ID = MobitelIDInCursor
    ORDER BY LogId;   
  
  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* ������� ������, ��������� ������ ��������� ��� TDataManager  */
  TRUNCATE TABLE datagpsbuffer_on;
  TRUNCATE TABLE datagpslost_on;
  TRUNCATE TABLE datagpslost_ontmp;

  /* ------------------- FETCH Mobitels ------------------------ */
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor;
  WHILE DataExists = 1
  DO
    /* ��������� ��� ������� �������  */
    SET @InsertLostDataStatement = 
      'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';  

    /* ----------------- FETCH DataGPS --------------------- */  
    SET FirstIteration = 1;
    SET NewRowCount = 0;
    
    OPEN CursorDataGps;
    FETCH CursorDataGps INTO LogIdInCursor, SrvPacketIdInCursor;
    WHILE DataExists = 1
    DO  
      IF FirstIteration = 1 THEN
        /* ������ �������� � ������ */
        SET PreviousLogId = LogIdInCursor;
        SET PreviousSrvPacketId = SrvPacketIdInCursor;
        SET FirstIteration = 0;
      ELSE
        /* �������� ����� �������*/
        IF (((LogIdInCursor -  PreviousLogId) > 1) AND 
             (PreviousSrvPacketId < SrvPacketIdInCursor)) 
        THEN
          /* ������ ������ */
          IF NewRowCount > 0 THEN
            SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, ",");
          END IF;
          
          SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, 
            " (", MobitelIDInCursor, ", ", PreviousLogId, ", ", LogIdInCursor, ", ", 
            PreviousSrvPacketId, ", ", SrvPacketIdInCursor, ")");
              
          SET NewRowCount = NewRowCount + 1;
        END IF;
          
        /* ���������� ������� �������� � ���������� */
        SET PreviousLogId = LogIdInCursor;
        SET PreviousSrvPacketId = SrvPacketIdInCursor;
      END IF; -- IF FirstIteration = 1 
        
      FETCH CursorDataGps INTO LogIdInCursor, SrvPacketIdInCursor;
    END WHILE; --  WHILE DataExistsInner = 1 
    CLOSE CursorDataGps;  

    /* ���� � ������� ��������� ���� ����������� ������ - 
       ����� ConfirmedID ����� ����� ������������ Begin_LogID 
       �� ������� ���������. 
       ����� - ������������� LogID ������� ��������� �� 
       ������� datagps � ����� �� ���������� ������ ���� ��� 
       ������ ����, � ����������� �� ������� LogId. */
    IF NewRowCount > 0 THEN
      /* ���������� ������� ����������� ������ */
      PREPARE stmt FROM @InsertLostDataStatement;
      EXECUTE stmt;
      DEALLOCATE PREPARE stmt;
        
      SELECT MIN(Begin_LogID) INTO NewConfirmedID
      FROM datagpslost_on
      WHERE Mobitel_ID = MobitelIDInCursor;
    ELSE
      /* � ���� ������ �� ��� ��� �������.
         ���� ���� ������������� �������� LogID, �� ��� �����������
         ������ �������� ConfirmedID ��������� �������������� ������:
         ��������� ����� �������� LogID ������������� ��� �������������
         ����� ����� (�� DataGpsId). */
      IF EXISTS(
        SELECT 1 
        FROM DataGps 
        WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId < 0)
        LIMIT 1)
      THEN   
        SELECT MAX(DataGps_ID) INTO MaxNegativeDataGpsId 
        FROM DataGps 
        WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId < 0);
        
        SELECT COALESCE(MAX(DataGps_ID), 0) INTO MaxPositiveDataGpsId 
        FROM DataGps 
        WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId >= 0);
        
        IF (MaxNegativeDataGpsId > MaxPositiveDataGpsId) THEN
          SELECT COALESCE(MAX(LogID), 0) INTO NewConfirmedID
          FROM datagps
          WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId < 0);
        ELSE
          SELECT COALESCE(MAX(LogID), 0) INTO NewConfirmedID
          FROM datagps
          WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId >= 0);
        END IF; -- IF (MaxNegativeDataGpsId > MaxPositiveDataGpsId)
      ELSE
        /* ���������� ������ ������������� LogId */  
        SELECT COALESCE(MAX(LogID), 0) INTO NewConfirmedID
        FROM datagps
        WHERE Mobitel_ID = MobitelIDInCursor;  
      END IF; -- IF EXISTS 
    END IF; -- IF NewRowCount > 0
      
    /* ������� ConfirmedID */
    UPDATE mobitels
    SET ConfirmedID = NewConfirmedID
    WHERE Mobitel_ID = MobitelIDInCursor;
       
    SET DataExists = 1; 
    FETCH CursorMobitels INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitels;

END$$

DELIMITER $$
