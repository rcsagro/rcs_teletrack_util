ALTER TABLE datagps ADD COLUMN SrvPacketID BIGINT NOT NULL DEFAULT '0'
  COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������';

ALTER TABLE datagps ADD INDEX 
  IDX_MobitelIDSrvPacketID (Mobitel_ID, SrvPacketID);

DROP TABLE IF EXISTS datagpsbuffer_on;
CREATE TABLE datagpsbuffer_on (
  Mobitel_ID int(11) NOT NULL default 0,
  Message_ID int(11) default 0,
  Latitude int(11) NOT NULL default 0,
  Longitude int(11) NOT NULL default 0,
  Altitude int(11) default NULL,
  UnixTime int(11) NOT NULL default 0,
  Speed smallint(6) NOT NULL default 0,
  Direction int(11) NOT NULL default 0,
  Valid tinyint(4) NOT NULL default 1,
  InMobitelID int(11) default NULL,
  Events int(10) unsigned default 0,
  Sensor1 tinyint(4) unsigned default 0,
  Sensor2 tinyint(4) unsigned default 0,
  Sensor3 tinyint(4) unsigned default 0,
  Sensor4 tinyint(4) unsigned default 0,
  Sensor5 tinyint(4) unsigned default 0,
  Sensor6 tinyint(4) unsigned default 0,
  Sensor7 tinyint(4) unsigned default 0,
  Sensor8 tinyint(4) unsigned default 0,
  LogID int(11) NOT NULL default 0,
  isShow tinyint(4) default 0,
  whatIs smallint(6) NOT NULL default 0,
  Counter1 int(11) NOT NULL default -1,
  Counter2 int(11) NOT NULL default -1,
  Counter3 int(11) NOT NULL default -1,
  Counter4 int(11) NOT NULL default -1,
  SrvPacketID BIGINT NOT NULL DEFAULT 0 COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������',
  INDEX IDX_MobitelidLogid USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid USING BTREE (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_SrvPacketID (SrvPacketID)
) ENGINE=MEMORY DEFAULT CHARSET=cp1251 COMMENT '����� ������ ������';


DROP TABLE IF EXISTS datagpsbuffer_ontmp;
CREATE TABLE datagpsbuffer_ontmp (
  Mobitel_ID INT(11) NOT NULL DEFAULT 0,
  LogID INT(11) NOT NULL DEFAULT 0,
  SrvPacketID BIGINT NOT NULL,
  INDEX IDX_MobitelidSrvpacketid USING BTREE (Mobitel_ID, SrvPacketID),
  INDEX IDX_Mobitelid USING BTREE (Mobitel_ID)
) ENGINE = MEMORY DEFAULT CHARSET=cp1251 COMMENT '��������� ��������������� ����� ������ ������';



DROP TABLE IF EXISTS online;
CREATE TABLE online (
  DataGps_ID INT(11) AUTO_INCREMENT,
  Mobitel_ID INT(11) DEFAULT 0,
  Message_ID INT(11) DEFAULT 0,
  Latitude INT(11) NOT NULL DEFAULT 0,
  Longitude INT(11) NOT NULL DEFAULT 0,
  Altitude INT(11) DEFAULT 0,
  UnixTime INT(11) NOT NULL DEFAULT 0,
  Speed SMALLINT(6) NOT NULL DEFAULT 0,
  Direction INT(11) NOT NULL DEFAULT 0,
  Valid TINYINT(4) NOT NULL DEFAULT 1,
  InMobitelID INT(11) DEFAULT 0,
  Events INT(10) UNSIGNED DEFAULT 0,
  Sensor1 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor2 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor3 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor4 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor5 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor6 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor7 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor8 TINYINT(4) UNSIGNED DEFAULT 0,
  LogID INT(11) DEFAULT 0,
  isShow TINYINT(4) DEFAULT 0,
  whatIs SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  Counter1 INT(11) NOT NULL DEFAULT -1,
  Counter2 INT(11) NOT NULL DEFAULT -1,
  Counter3 INT(11) NOT NULL DEFAULT -1,
  Counter4 INT(11) NOT NULL DEFAULT -1,
  PRIMARY KEY (DataGps_ID),
  INDEX IDX_MobitelidLogid USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtimeValid USING BTREE (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_MobitelidUnixtime USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_Mobitelid USING BTREE (Mobitel_ID)
) ENGINE = INNODB COMMENT '����������� ������';


DROP TABLE IF EXISTS datagpslost_on;
CREATE TABLE datagpslost_on (
  Mobitel_ID INT(11) NOT NULL DEFAULT 0,
  Begin_LogID INT(11) NOT NULL DEFAULT 0 COMMENT '�������� LogID ����� �������� ���������� ������',
  End_LogID INT(11) NOT NULL DEFAULT 0 COMMENT '�������� LogID ����� ������� ����������� ������',
  Begin_SrvPacketID BIGINT NOT NULL COMMENT '������������ ���������� ������ � ������� ������ ������ � Begin_LogID',
  End_SrvPacketID BIGINT NOT NULL COMMENT '������������ ���������� ������ � ������� ������ ������ � End_LogID',
  PRIMARY KEY PK_DatagpslostOn(Mobitel_ID, Begin_LogID),
  INDEX IDX_MobitelID (Mobitel_ID),
  UNIQUE INDEX IDX_MobitelID_BeginSrvPacketID (Mobitel_ID, Begin_SrvPacketID)
) ENGINE=InnoDB COMMENT='�������� � ������ DataGPS, ���������� �� Online �������';


DROP TABLE IF EXISTS datagpslost_ontmp;
CREATE TABLE datagpslost_ontmp (
  LogID INT(11) NOT NULL DEFAULT 0,
  SrvPacketID BIGINT NOT NULL
) ENGINE = MEMORY COMMENT = '������������ ������������� ���������� OnLostDataGPS';


DELIMITER $$

DROP PROCEDURE IF EXISTS OnListMobitelConfig$$
CREATE PROCEDURE OnListMobitelConfig()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '������� �������� ���������� ������� ����� �������� � online ������'
BEGIN
  DECLARE MobitelIDInCursor INT;
  DECLARE DevIdShortInCursor CHAR(4);
  DECLARE DataExists TINYINT DEFAULT 1;

  /* ������ � ��������� ����������� ���������� */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS MobitelID, c.devIdShort AS DevIdShort
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID)
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND
      (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')
    ORDER BY m.Mobitel_ID;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* ��������� ������� �������� ��������� */
  CREATE TEMPORARY TABLE tmpMobitelsConfig (
    MobitelID INT NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL) ENGINE = MEMORY;

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor, DevIdShortInCursor;
  WHILE DataExists = 1 DO

    INSERT INTO tmpMobitelsConfig SELECT MobitelIDInCursor, DevIdShortInCursor,
      (SELECT COALESCE(MAX(SrvPacketID), 0)
       FROM datagps
       WHERE Mobitel_ID = MobitelIDInCursor) AS LastPacketID;

    FETCH CursorMobitels INTO MobitelIDInCursor, DevIdShortInCursor;
  END WHILE;
  CLOSE CursorMobitels;

  SELECT MobitelID, DevIdShort, LastPacketID
  FROM tmpMobitelsConfig;

  DROP TEMPORARY TABLE tmpMobitelsConfig;  
END$$


DROP PROCEDURE IF EXISTS OnLostDataGPS$$
CREATE PROCEDURE OnLostDataGPS(IN MobitelID INTEGER(11), OUT NeedOptimize TINYINT)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '����� ��������� � online ������ ��������� ��������� � ���������� ������.'
BEGIN

  /********** DECLARE **********/
  
  DECLARE CurrentConfirmedID INT DEFAULT 0; /* ������� �������� ConfirmedID */
  DECLARE NewConfirmedID INT DEFAULT 0; /* ����� �������� ConfirmedID */
  DECLARE StartLogIDSelect INT DEFAULT 0; /* min LogID ��� ������� */
  DECLARE FinishLogIDSelect INT DEFAULT 0; /* max LogID ��� ������� */
  DECLARE MaxEndLogID INT DEFAULT 0; /* max End_LogID � ����������� ���������� ������� ��������� */
  
  DECLARE MinLogID_n INT DEFAULT 0; /* ����������� LogID ����� ConfirmedID � ��������� ����� ������ */
  DECLARE MaxLogID_n INT DEFAULT 0; /* ������������ LogID � ��������� ����� ������ */
  
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
  DECLARE DataChanged TINYINT DEFAULT 0; /* ���� ��������� ��������� ���������� ������ */
    
  DECLARE NewRowCount INT DEFAULT 0; /* ���������� ����� ������� � ��������� */
  
  DECLARE NewLogID INT; /* �������� LogID � ������� CursorNewLogID */
  DECLARE NewSrvPacketID BIGINT; /* �������� SrvPacketID � ������� CursorNewLogID */
  DECLARE FirstLogID INT DEFAULT -9999999; /* ������ �������� LogID ��� ������� */
  DECLARE SecondLogID INT; /* ������ �������� LogID ��� ������� */
  DECLARE FirstSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
  DECLARE SecondSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
  
  DECLARE tmpSrvPacketID BIGINT DEFAULT 0; /* �������� SrvPacketID ��� ������ ������������� ������ � ������� ����� ������, �� �������������� � ������������� */  
  
  /* ������ ��� ������� �� ����� LogID */
  DECLARE CursorNewLogID CURSOR FOR
    SELECT LogID, SrvPacketID 
    FROM datagpslost_ontmp;
    
  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  /********** END DECLARE **********/

  SET NeedOptimize = 0;
 
  SELECT COALESCE(ConfirmedID, 0) INTO CurrentConfirmedID
  FROM mobitels
  WHERE Mobitel_ID = MobitelID;

  /* ����������� LogID � ����� ������ ������ c ������� ����� �������� */
  SELECT MIN(LogID) INTO MinLogID_n
  FROM datagpsbuffer_on
  WHERE (Mobitel_ID = MobitelID) AND (LogID > CurrentConfirmedID);
  
  /* ������������ LogID � ����� ������ ������ c ������� ����� �������� */
  SELECT MAX(LogID) INTO MaxLogID_n
  FROM datagpsbuffer_on 
  WHERE Mobitel_ID = MobitelID;  
  
  /* ������������ End_LogID � ����������� ���������� ������� ��������� */
  SELECT COALESCE(MAX(End_LogID), 0) INTO MaxEndLogID
  FROM datagpslost_on 
  WHERE Mobitel_ID = MobitelID;  
  
  /* �������� ���� �� ������ ��� ������� */
  IF MinLogID_n IS NOT NULL THEN
  
    /********** PREPARE **********/
    
    TRUNCATE TABLE datagpslost_ontmp;
  
    IF MinLogID_n < MaxEndLogID THEN
      /* ����  MinLogID_n < MaxEndLogID ������ ������ ������, 
       ������� �������� ������� ����� ���������. */
       
      /* ������� ��������� LogID ��� ���������. */
      SELECT COALESCE(Begin_LogID, MinLogID_n) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MinLogID_n > Begin_LogID) 
        AND (MinLogID_n < End_LogID);
      
      /* ������� ��������� LogID ��� ���������. */  
      SELECT COALESCE(End_LogID, MaxLogID_n) INTO FinishLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MaxLogID_n > Begin_LogID) 
        AND (MaxLogID_n < End_LogID);
       
      /* ������ ������ ������ �� ������� ����������� ������� 
         (����� �������������, ���� �����������) */
      DELETE FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID >= StartLogIDSelect)
        AND (Begin_LogID <= FinishLogIDSelect);
      
      /* ���� ������ ������ LogID = 0 � ���� ������ ��� � DataGps, �����
         ���� �������� ������ ��������� ������ ��� �������. */
      IF (StartLogIDSelect = 0) AND (NOT EXISTS
        (Select 1 
         FROM datagps 
         WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect))) THEN
      
        INSERT INTO datagpslost_ontmp(LogID, SrvPacketID) VALUES (0, 0);  
      END IF;       
    ELSE
      /* ������ ����� ������. ���� �������� ��� ������� ������������� ������ - 
         ������ ������� */
      
      /* ������� ��������� LogID ��� ���������. */
      SELECT COALESCE(MAX(End_LogID), CurrentConfirmedID) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE Mobitel_ID = MobitelID;   
         
      /* ������� ��������� LogID ��� ���������. */  
      SELECT MAX(LogID)  INTO FinishLogIDSelect
      FROM datagpsbuffer_on
      WHERE Mobitel_ID = MobitelID;
      
      /* �������� �� datagps ��������������� �������� SrvPacketID  ��� ������
         ������������� ������ */
      SELECT COALESCE(SrvPacketID, 0) INTO tmpSrvPacketID
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect);
      
      /* �������� � ������� datagpslost_ontmp ��� ������� ��������� 
         �������������� ������ ������ */
      INSERT INTO datagpslost_ontmp(LogID, SrvPacketID)
      VALUES (StartLogIDSelect, tmpSrvPacketID);
    END IF; -- IF MinLogID_n < MaxEndLogID   
    
    /********** END PREPARE **********/
    
    /**********  INSERT ROWS **********/
    
    /* ���������� ��������� ������� ���������� ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      SELECT LogID, SrvPacketID
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND (LogID >= StartLogIDSelect)
        AND (LogID <= FinishLogIDSelect)
      ORDER BY LogID;
            
    /* ��������� ��� ������� �������  */
    SET @InsertLostDataStatement = 
      'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';
        
    /* ������������ �������-������� � ������� ����������� ���������� */
    SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    WHILE DataExists = 1 DO
      IF FirstLogID = -9999999 THEN
        /* ������ �������� � ������ */
        SET FirstLogID = NewLogID;
        SET FirstSrvPacketID = NewSrvPacketID;
      ELSE
        /* �������� ����� �������*/
        SET SecondLogID = NewLogID;
        SET SecondSrvPacketID = NewSrvPacketID;
        
        IF (SecondLogID -  FirstLogID) > 1 THEN
          /* ������ ������ */
          IF NewRowCount > 0 THEN
            SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, ",");
          END IF;
          
          SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, 
            " (", MobitelID, ", ", FirstLogID, ", ", SecondLogID, ", ", 
            FirstSrvPacketID, ", ", SecondSrvPacketID, ")");
              
          SET NewRowCount = NewRowCount + 1;
        END IF;
        /* ���������� ������� ������ �������� �� ������ �������
           ��� ������� � ��������� �������� � �������� ���������� �������� */
        SET  FirstLogID = SecondLogID;
        SET FirstSrvPacketID = SecondSrvPacketID;
      END IF;  
      FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    END WHILE;
    CLOSE CursorNewLogID;
             
    IF NewRowCount > 0 THEN
      /*--------- ���������� ������� ����������� ������ ----------*/
      PREPARE stmt FROM @InsertLostDataStatement;
      EXECUTE stmt;
      DEALLOCATE PREPARE stmt;
          
      SET DataChanged = 1;
    END IF; -- NewRowCount > 0 
    
    /********** END INSERT ROWS **********/
               
    /********** UPDATE MOBITELS **********/
    
    /* ���� ����������� ������� � ������� ��������� ��� - 
       ����� ConfirmedID ����� ����� ������������� LogID 
       ������� ���������. ����� - ����������� Begin_LogID
       �� ������� ��������� */
    SELECT MIN(Begin_LogID) INTO NewConfirmedID
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;
       
    IF NewConfirmedID IS NULL THEN
      SELECT MAX(LogID) INTO NewConfirmedID
      FROM datagps
      WHERE Mobitel_ID = MobitelID;
    END IF;  
    
    /* ������� ConfirmedID ���� ���� */
    UPDATE mobitels
    SET ConfirmedID = NewConfirmedID
    WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);
    
    /********** END UPDATE MOBITELS **********/
        
    TRUNCATE TABLE datagpslost_ontmp;
  END IF; -- IF MaxEndLogID IS NOT NULL  
  
  IF DataChanged = 1 THEN
    SET NeedOptimize = 1;
  END IF;
END$$

DROP PROCEDURE IF EXISTS OnLostDataGPS2$$
CREATE PROCEDURE OnLostDataGPS2(IN MobitelID INTEGER(11), OUT NeedOptimize TINYINT)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '����������� ���������� ���������. ������ ����������� ������ ��������� ���������.'
BEGIN
  /********** DECLARE **********/
    
  DECLARE NewConfirmedID INT DEFAULT 0; /* ����� �������� ConfirmedID */
  DECLARE BeginLogID INT; /* Begin_LogID � ������� CursorLostRanges */
  DECLARE EndLogID INT; /* End_LogID � ������� CursorLostRanges */
  DECLARE BeginSrvPacketID BIGINT; /* Begin_SrvPacketID � ������� CursorLostRanges */
  DECLARE EndSrvPacketID BIGINT; /* End_SrvPacketID � ������� CursorLostRanges */
  DECLARE NewRowCount INT DEFAULT 0; /* ���������� ����� ������� � ��������� */
  DECLARE RowCountForAnalyze INT DEFAULT 0; /* ���������� ������� ��� ������� */
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
    
  /* ������ ��� ������� �� ���������� ��������� ��������� */
  DECLARE CursorLostRanges CURSOR FOR
    SELECT Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID 
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;    

  /* ������ ��� ������� �� ����� LogID */
  DECLARE CursorNewLogID CURSOR FOR
    SELECT LogID, SrvPacketID 
    FROM datagpslost_ontmp
    ORDER BY SrvPacketID;    
    
  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  /********** END DECLARE **********/

  SET NeedOptimize = 0;
  /* ��������� ��� ������� �������  */
  SET @InsertLostDataStatement = 
    'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';
  
  TRUNCATE TABLE datagpslost_ontmp;
  
  /* ���������� ��������� � ��������� ������� ���� ��� �������� */
  SET DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  WHILE DataExists = 1 DO
    /* ���������� ��������� ������� ������������ ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      SELECT DISTINCT LogID, SrvPacketID
      FROM datagpsbuffer_ontmp
      WHERE (Mobitel_ID = MobitelID) AND (SrvPacketID > BeginSrvPacketID)
        AND (SrvPacketID < EndSrvPacketID)
      ORDER BY SrvPacketID;
      
    /* ���� ���� ������ ������������ �������� */  
    SELECT COUNT(1) INTO RowCountForAnalyze
    FROM datagpslost_ontmp;  
    
    IF RowCountForAnalyze > 0 THEN  
      /* ��������� ������ � ��������� ������ ��� ������� */  
      INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
      VALUES (BeginLogID, BeginSrvPacketID), (EndLogID, EndSrvPacketID);
      
      /* ������� ������ �������� */
      DELETE FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);
       
      /**********  INSERT ROWS **********/             
      BEGIN
        DECLARE NewLogID INT; /* �������� LogID � ������� CursorNewLogID */
        DECLARE NewSrvPacketID BIGINT; /* �������� SrvPacketID � ������� CursorNewLogID */
        DECLARE FirstLogID INT DEFAULT -9999999; /* ������ �������� LogID ��� ������� */
        DECLARE SecondLogID INT; /* ������ �������� LogID ��� ������� */
        DECLARE FirstSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
        DECLARE SecondSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
        
        /* ���������� ������������� ������ ��� FETCH ��������� ������ */
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
        
        /* ������������ �������-������� � ������� ����������� ���������� */
        SET DataExists = 1;
        OPEN CursorNewLogID;
        FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        WHILE DataExists = 1 DO
          IF FirstLogID = -9999999 THEN
            /* ������ �������� � ������ */
            SET FirstLogID = NewLogID;
            SET FirstSrvPacketID = NewSrvPacketID;
          ELSE
            /* �������� ����� �������*/
            SET SecondLogID = NewLogID;
            SET SecondSrvPacketID = NewSrvPacketID;
            
            IF (SecondSrvPacketID - FirstSrvPacketID) > 1 THEN
              /* ������ ������ */
              IF NewRowCount > 0 THEN
                SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, ",");
              END IF;
              
              SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, 
                " (", MobitelID, ", ", FirstLogID, ", ", SecondLogID, ", ", 
                FirstSrvPacketID, ", ", SecondSrvPacketID, ")");
                  
              SET NewRowCount = NewRowCount + 1;
            END IF;
            /* ���������� ������� ������ �������� �� ������ �������
               ��� ������� � ��������� �������� � �������� ���������� �������� */
            SET FirstLogID = SecondLogID;
            SET FirstSrvPacketID = SecondSrvPacketID;
          END IF;  
          FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        END WHILE;
        CLOSE CursorNewLogID;
      END;
      /********** END INSERT ROWS **********/  

      TRUNCATE TABLE datagpslost_ontmp;
    END IF;
      
    FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  END WHILE;
  CLOSE CursorLostRanges;
  
  IF NewRowCount > 0 THEN
    /*--------- ���������� ������� ����������� ������ ----------*/
    PREPARE stmt FROM @InsertLostDataStatement;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
          
    SET  NeedOptimize = 1; 
  END IF; -- NewRowCount > 0 
  
  /********** UPDATE MOBITELS **********/
    
  /* ���� ����������� ������� � ������� ��������� ��� - 
     ����� ConfirmedID ����� ����� ������������� LogID 
     ������� ���������. ����� - ����������� Begin_LogID
     �� ������� ��������� */
  SELECT MIN(Begin_LogID) INTO NewConfirmedID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;
       
  IF NewConfirmedID IS NULL THEN
    SELECT MAX(LogID) INTO NewConfirmedID
    FROM datagps
    WHERE Mobitel_ID = MobitelID;
  END IF;  
    
  /* ������� ConfirmedID ���� ���� */
  UPDATE mobitels
  SET ConfirmedID = NewConfirmedID
  WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);
    
  /********** END UPDATE MOBITELS **********/
END$$


/* ������� ��������� ����� �� ������� datagpsbuffer_on. ��������� ������
   �������� ���������� �������� � ����� Mobitel_Id + LogID. �� ���������
   ������������� ������� � ������� datagpsbuffer_on ��������� ������ ���� - 
   ��������� �������� �������������� ��������, �.�. � ������������ SrvPacketID. */
DROP PROCEDURE IF EXISTS OnDeleteDuplicates$$
CREATE PROCEDURE OnDeleteDuplicates()
  SQL SECURITY INVOKER
  COMMENT 'Delete duplicates rows from datagpsbuffer_on'
BEGIN
  /* ������� MobitelId � ������� CursorDuplicateGroups */
  DECLARE MobitelIdInCursor INT DEFAULT 0; 
  /* ������� LogID � ������� CursorDuplicateGroups */
  DECLARE LogIdInCursor INT DEFAULT 0; 
  /* Max SrvPacketId � ��������� ����� ������� ������ ���������� */
  DECLARE MaxSrvPacketId INT DEFAULT 0; 
  DECLARE DataExists TINYINT DEFAULT 1;

  /* ������ ��� ������� �� ���� ������� ���������� */  
  DECLARE CursorDuplicateGroups CURSOR FOR
    SELECT Mobitel_Id, LogId 
    FROM datagpsbuffer_on
    GROUP BY Mobitel_Id, LogId 
    HAVING COUNT(LogId) > 1;
    
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  WHILE DataExists = 1 DO
    SELECT MAX(SrvPacketID) INTO MaxSrvPacketId
    FROM datagpsbuffer_on 
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor);
         
    DELETE FROM datagpsbuffer_on  
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor) 
      AND (SrvPacketID < MaxSrvPacketId);
        
    FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  END WHILE;
  CLOSE CursorDuplicateGroups; 
END$$


DROP PROCEDURE IF EXISTS OnCorrectInfotrackLogId$$
CREATE PROCEDURE OnCorrectInfotrackLogId()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '������������� �������� LogId � InMobitelID � ������ ����������'
BEGIN
  DECLARE MobitelIDInCursor INT; /* ������� �������� MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
  DECLARE MaxLogId INT DEFAULT 0; /* ������������ LogId ��� ������� ������� */
  DECLARE SrvPacketIDInCursor BIGINT DEFAULT 0; /* ������� �������� ���������� ������ */
    
  /* ������ �� ���������� � ��������� ����� ������. 
     ��� ��������� ���������� � ����� I */
  DECLARE CursorInfoTrack CURSOR FOR
    SELECT DISTINCT buf.Mobitel_ID
    FROM datagpsbuffer_on buf 
      JOIN mobitels m ON (buf.mobitel_id = m.mobitel_id)
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID) 
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND 
      (c.devIdShort LIKE "I%") AND (buf.LogId = 0)
    ORDER BY 1;
    
  /* ������ �� ������ ��������� */
  DECLARE CursorInfoTrackUpdateSet CURSOR FOR
    SELECT SrvPacketID 
    FROM datagpsbuffer_on
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId = 0)
    ORDER BY UnixTime;  

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ������� ������������ LogId ��� ������� ��������� */
    SELECT COALESCE(MAX(LogID), 0) INTO	MaxLogId
    FROM DataGps
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ��������� LogId � InMobitelID � ������ ����� ������ �������� ��������� */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    WHILE DataExists = 1 DO
      SET MaxLogId = MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET LogId = MaxLogId, InMobitelID = MaxLogId
      WHERE SrvPacketID = SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    END WHILE;
    CLOSE CursorInfoTrackUpdateSet;
 
    SET DataExists = 1;
    FETCH CursorInfoTrack INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorInfoTrack;
END$$


DROP PROCEDURE IF EXISTS OnTransferBuffer$$
CREATE PROCEDURE OnTransferBuffer()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '��������� GPS ������ �� �������� ������� � datagps'
BEGIN
  DECLARE MobitelIDInCursor INT; /* ������� �������� MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
  DECLARE NeedOptimize TINYINT DEFAULT 0; /* ����� ����������� ��� ��� */
  DECLARE NeedOptimizeParam TINYINT;
  
  DECLARE TmpMaxUnixTime INT; /* ��������� ���������� ��� �������� Max UnixTime �� ������� online */

  /* ������ �� ���������� � ��������� ����� ������ */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_on;
  /* ������ �� ���������� � ��������� ��������� ������ */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_ontmp;  

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  CALL OnDeleteDuplicates();
  CALL OnCorrectInfotrackLogId();

  /* ��������� ������, ������� ��� ���� � ��, ��� ������� ����� ������� ��������� 
     ������������ ���������� OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp (Mobitel_ID, LogID, SrvPacketID)
    SELECT b.Mobitel_ID, b.LogID, b.SrvPacketID
    FROM datagpsbuffer_on b LEFT JOIN datagps d ON
      (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL;


  /* -------------------- ���������� ������� Online ---------------------- */  
  /* �������� �� ������� online ����������� ����� ������� datagpsbuffer_on */
  /* �� ����� Mobitel_ID � LogID */
  DELETE o
  FROM online o, datagpsbuffer_on dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);
  
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ���������� Max �������� UnixTime � ������� online ��� ���������
       ���������. p.s. UnixTime � ������� online ������ �������� */
    SELECT COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online FORCE INDEX (IDX_Mobitelid)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ������� ����� ������� (�������� 100)������� ��������� � online */
    INSERT INTO online (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs
    FROM (
      SELECT *
      FROM datagpsbuffer_on
      WHERE (Mobitel_ID = MobitelIDInCursor)  AND 
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
      GROUP BY Mobitel_ID, LogID
      ORDER BY UnixTime DESC
      LIMIT 100) AS T
    ORDER BY UnixTime ASC;

    /* �������� ����� �� ������� ����������� ������ �� ���������������
       �������� �������� - �� �������� ���������� 100 �������� */
    DELETE FROM online
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < 
      (SELECT COALESCE(MIN(UnixTime), 0) 
       FROM (
         SELECT UnixTime
         FROM online
         WHERE Mobitel_ID = MobitelIDInCursor 
         ORDER BY UnixTime DESC
         LIMIT 100) T1)); 
  
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */ 
  
  /* �����-�� ����� ������ ... */
  UPDATE datagps d, datagpsbuffer_on b SET
    d.Latitude = b.Latitude,
    d.Longitude = b.Longitude,
    d.Altitude = b.Altitude,
    d.UnixTime = b.UnixTime,
    d.Speed = b.Speed,
    d.Direction = b.Direction,
    d.Valid = b.Valid,
    d.InMobitelID = b.LogID,
    d.`Events` = b.`Events`,
    d.Sensor1 = b.Sensor1,
    d.Sensor2 = b.Sensor2,
    d.Sensor3 = b.Sensor3,
    d.Sensor4 = b.Sensor4,
    d.Sensor5 = b.Sensor5,
    d.Sensor6 = b.Sensor6,
    d.Sensor7 = b.Sensor7,
    d.Sensor8 = b.Sensor8,
    d.whatIs = b.whatIs,
    d.Counter1 = b.Counter1,
    d.Counter2 = b.Counter2,
    d.Counter3 = b.Counter3,
    d.Counter4 = b.Counter4,
    d.SrvPacketID = b.SrvPacketID
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    AND (d.SrvPacketID < b.SrvPacketID);

  /* ������ ������ ������� ����������� � ������� */
  DELETE b
  FROM datagps d, datagpsbuffer_on b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  /* ������� ����� �������*/
  INSERT INTO datagps (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, InMobitelID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs, SrvPacketID)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, LogID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs, SrvPacketID
  FROM datagpsbuffer_on
  GROUP BY Mobitel_ID, LogID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ------ ���������� ������� ��������� ������� ������� ------- */
    UPDATE mobitels SET LastInsertTime = UNIX_TIMESTAMP(NOW())
    WHERE Mobitel_ID = MobitelIDInCursor;
       
    /* ------ ���������� ������ ����������� ������� -------------- */
    CALL OnLostDataGPS(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;

  /* ������� ������ */
  TRUNCATE TABLE datagpsbuffer_on;
    
  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ---- ���������� ������ ����������� ������� �� ������� ��������� --- */
    CALL OnLostDataGPS2(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsUpdateSet;
  
  /* ������� ���������������� ������ */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  /* ���� ��������� - ������� ����������� */
  IF NeedOptimize = 1 THEN
    OPTIMIZE TABLE datagpslost_on;
  END IF;            
END$$


DROP PROCEDURE IF EXISTS OnDeleteLostRange$$
CREATE PROCEDURE OnDeleteLostRange(IN MobitelID INTEGER(11), IN BeginSrvPacketID BIGINT)
    NOT DETERMINISTIC
    CONTAINS SQL                          
    SQL SECURITY INVOKER
    COMMENT '�������� ���������� ��������� ��������� �� datagpslost_on.'
BEGIN
  /* ���������� �������� �� ������, ��������������� ��� ��������,
     ������ � ������ ��������� ���������. ���� �������� - �����
     ������ �������� � �������� ConfirmedID */  
     
  DECLARE MinBeginSrvPacketID BIGINT; -- ����������� �������� Begin_SrvPacketID ��� ������� ���������
  DECLARE NewConfirmedID INT; -- ����� �������� ConfirmedID  
  

  /* ���������� ������������ �������� Begin_SrvPacketID */  
  SELECT MIN(Begin_SrvPacketID) INTO MinBeginSrvPacketID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;
  
  /* ���� ���� ������ � ��������� � ��������� - ��������� */
  IF MinBeginSrvPacketID IS NOT NULL THEN
    /* ���� ������� ������ ������ - �������� ����� �������� ConfirmedID */
    IF MinBeginSrvPacketID = BeginSrvPacketID  THEN
      SELECT End_LogID INTO NewConfirmedID
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID);
    END IF;
  
    /* �������� ������ */
    DELETE FROM  datagpslost_on
    WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID);
    
    /* ���� ������� ������ ������ - ������� ConfirmedID */  
    IF MinBeginSrvPacketID = BeginSrvPacketID  THEN
      UPDATE mobitels
      SET ConfirmedID = NewConfirmedID
      WHERE (Mobitel_ID = MobitelID) AND (NewConfirmedID > ConfirmedID);
    END IF;
  END IF;  
END$$


DELIMITER ;

UPDATE ServiceTypes
SET Name = 'Online', Descr = 'Online', 
  D1 = 'IP', D2 = 'Port', D3 = 'Login', D4 = 'Password' 
WHERE ServiceType_ID = 3;