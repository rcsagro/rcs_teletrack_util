DELIMITER $$

DROP PROCEDURE IF EXISTS tmpAddIndex$$
CREATE PROCEDURE tmpAddIndex()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '��������� ��������� ���������� ������� � DataGpsBuffer_on'
BEGIN
  DECLARE DbName VARCHAR(64); /* ������������ ������� �� */
  DECLARE IndexExist TINYINT DEFAULT 0;

  SELECT DATABASE() INTO DbName;

  SELECT COUNT(1) INTO IndexExist
  FROM information_schema.STATISTICS
  WHERE (TABLE_SCHEMA = DbName) AND 
    (TABLE_NAME = "DataGpsBuffer_on") AND (INDEX_NAME = "IDX_SrvPacketID");

  IF IndexExist= 0 THEN
    ALTER TABLE DataGpsBuffer_on ADD INDEX IDX_SrvPacketID (SrvPacketID);
  END IF;
END$$

CALL tmpAddIndex$$
DROP PROCEDURE IF EXISTS tmpAddIndex$$


/* ������� ��������� ����� �� ������� datagpsbuffer_on. ��������� ������
   �������� ���������� �������� � ����� Mobitel_Id + LogID. �� ���������
   ������������� ������� � ������� datagpsbuffer_on ��������� ������ ���� - 
   ��������� �������� �������������� ��������, �.�. � ������������ SrvPacketID. */
DROP PROCEDURE IF EXISTS OnDeleteDuplicates$$
CREATE PROCEDURE OnDeleteDuplicates()
  SQL SECURITY INVOKER
  COMMENT 'Delete duplicates rows from datagpsbuffer_on'
BEGIN
  /* ������� MobitelId � ������� CursorDuplicateGroups */
  DECLARE MobitelIdInCursor INT DEFAULT 0; 
  /* ������� LogID � ������� CursorDuplicateGroups */
  DECLARE LogIdInCursor INT DEFAULT 0; 
  /* Max SrvPacketId � ��������� ����� ������� ������ ���������� */
  DECLARE MaxSrvPacketId INT DEFAULT 0; 
  DECLARE DataExists TINYINT DEFAULT 1;

  /* ������ ��� ������� �� ���� ������� ���������� */  
  DECLARE CursorDuplicateGroups CURSOR FOR
    SELECT Mobitel_Id, LogId 
    FROM datagpsbuffer_on
    GROUP BY Mobitel_Id, LogId 
    HAVING COUNT(LogId) > 1;
    
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  WHILE DataExists = 1 DO
    SELECT MAX(SrvPacketID) INTO MaxSrvPacketId
    FROM datagpsbuffer_on 
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor);
         
    DELETE FROM datagpsbuffer_on  
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor) 
      AND (SrvPacketID < MaxSrvPacketId);
        
    FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  END WHILE;
  CLOSE CursorDuplicateGroups; 
END$$

DROP PROCEDURE IF EXISTS OnCorrectInfotrackLogId$$
CREATE PROCEDURE OnCorrectInfotrackLogId()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '������������� �������� LogId � InMobitelID � ������ ����������'
BEGIN
  DECLARE MobitelIDInCursor INT; /* ������� �������� MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
  DECLARE MaxLogId INT DEFAULT 0; /* ������������ LogId ��� ������� ������� */
  DECLARE SrvPacketIDInCursor BIGINT DEFAULT 0; /* ������� �������� ���������� ������ */
    
  /* ������ �� ���������� � ��������� ����� ������. 
     ��� ��������� ���������� � ����� I */
  DECLARE CursorInfoTrack CURSOR FOR
    SELECT DISTINCT buf.Mobitel_ID
    FROM datagpsbuffer_on buf 
      JOIN mobitels m ON (buf.mobitel_id = m.mobitel_id)
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID) 
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND 
      (c.devIdShort LIKE "I%") AND (buf.LogId = 0)
    ORDER BY 1;
    
  /* ������ �� ������ ��������� */
  DECLARE CursorInfoTrackUpdateSet CURSOR FOR
    SELECT SrvPacketID 
    FROM datagpsbuffer_on
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId = 0)
    ORDER BY UnixTime;  

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ������� ������������ LogId ��� ������� ��������� */
    SELECT COALESCE(MAX(LogID), 0) INTO	MaxLogId
    FROM DataGps
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ��������� LogId � InMobitelID � ������ ����� ������ �������� ��������� */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    WHILE DataExists = 1 DO
      SET MaxLogId = MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET LogId = MaxLogId, InMobitelID = MaxLogId
      WHERE SrvPacketID = SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    END WHILE;
    CLOSE CursorInfoTrackUpdateSet;
 
    SET DataExists = 1;
    FETCH CursorInfoTrack INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorInfoTrack;
END$$


DROP PROCEDURE IF EXISTS OnTransferBuffer$$
CREATE PROCEDURE OnTransferBuffer()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '��������� GPS ������ �� �������� ������� � datagps'
BEGIN
  DECLARE MobitelIDInCursor INT; /* ������� �������� MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
  DECLARE NeedOptimize TINYINT DEFAULT 0; /* ����� ����������� ��� ��� */
  DECLARE NeedOptimizeParam TINYINT;
  
  DECLARE TmpMaxUnixTime INT; /* ��������� ���������� ��� �������� Max UnixTime �� ������� online */

  /* ������ �� ���������� � ��������� ����� ������ */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_on;
  /* ������ �� ���������� � ��������� ��������� ������ */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_ontmp;  

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  CALL OnDeleteDuplicates();
  CALL OnCorrectInfotrackLogId();
  
  /* ��������� ������, ������� ��� ���� � ��, ��� ������� ����� ������� ��������� 
     ������������ ���������� OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp (Mobitel_ID, LogID, SrvPacketID)
    SELECT b.Mobitel_ID, b.LogID, b.SrvPacketID
    FROM datagpsbuffer_on b LEFT JOIN datagps d ON
      (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL;
  
      
  /* -------------------- ���������� ������� Online ---------------------- */  
  /* �������� �� ������� online ����������� ����� ������� datagpsbuffer_on */
  /* �� ����� Mobitel_ID � LogID */
  DELETE o
  FROM online o, datagpsbuffer_on dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);
  
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ���������� Max �������� UnixTime � ������� online ��� ���������
       ���������. p.s. UnixTime � ������� online ������ �������� */
    SELECT COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online FORCE INDEX (IDX_Mobitelid)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ������� ����� ������� (�������� 100)������� ��������� � online */
    INSERT INTO online (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs
    FROM (
      SELECT *
      FROM datagpsbuffer_on
      WHERE (Mobitel_ID = MobitelIDInCursor)  AND 
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
      GROUP BY Mobitel_ID, LogID
      ORDER BY UnixTime DESC
      LIMIT 100) AS T
    ORDER BY UnixTime ASC;

    /* �������� ����� �� ������� ����������� ������ �� ���������������
       �������� �������� - �� �������� ���������� 100 �������� */
    DELETE FROM online
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < 
      (SELECT COALESCE(MIN(UnixTime), 0) 
       FROM (
         SELECT UnixTime
         FROM online
         WHERE Mobitel_ID = MobitelIDInCursor 
         ORDER BY UnixTime DESC
         LIMIT 100) T1)); 
  
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */ 
  
  /* �����-�� ����� ������ ... */
  UPDATE datagps d, datagpsbuffer_on b SET
    d.Latitude = b.Latitude,
    d.Longitude = b.Longitude,
    d.Altitude = b.Altitude,
    d.UnixTime = b.UnixTime,
    d.Speed = b.Speed,
    d.Direction = b.Direction,
    d.Valid = b.Valid,
    d.InMobitelID = b.LogID,
    d.`Events` = b.`Events`,
    d.Sensor1 = b.Sensor1,
    d.Sensor2 = b.Sensor2,
    d.Sensor3 = b.Sensor3,
    d.Sensor4 = b.Sensor4,
    d.Sensor5 = b.Sensor5,
    d.Sensor6 = b.Sensor6,
    d.Sensor7 = b.Sensor7,
    d.Sensor8 = b.Sensor8,
    d.whatIs = b.whatIs,
    d.Counter1 = b.Counter1,
    d.Counter2 = b.Counter2,
    d.Counter3 = b.Counter3,
    d.Counter4 = b.Counter4,
    d.SrvPacketID = b.SrvPacketID
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    AND (d.SrvPacketID < b.SrvPacketID);

  /* ������ ������ ������� ����������� � ������� */
  DELETE b
  FROM datagps d, datagpsbuffer_on b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  /* ������� ����� �������*/
  INSERT INTO datagps (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, InMobitelID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs, SrvPacketID)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, LogID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs, SrvPacketID
  FROM datagpsbuffer_on
  GROUP BY Mobitel_ID, LogID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ------ ���������� ������� ��������� ������� ������� ------- */
    UPDATE mobitels SET LastInsertTime = UNIX_TIMESTAMP(NOW())
    WHERE Mobitel_ID = MobitelIDInCursor;
       
    /* ------ ���������� ������ ����������� ������� -------------- */
    CALL OnLostDataGPS(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;

  /* ������� ������ */
  TRUNCATE TABLE datagpsbuffer_on;
    
  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ---- ���������� ������ ����������� ������� �� ������� ��������� --- */
    CALL OnLostDataGPS2(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsUpdateSet;
  
  /* ������� ���������������� ������ */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  /* ���� ��������� - ������� ����������� */
  IF NeedOptimize = 1 THEN
    OPTIMIZE TABLE datagpslost_on;
  END IF;            
END$$

DELIMITER ;