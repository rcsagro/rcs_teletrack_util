DROP TABLE IF EXISTS datagpslost_on;
CREATE TABLE datagpslost_on (
  Mobitel_ID INT(11) NOT NULL DEFAULT 0,
  Begin_LogID INT(11) NOT NULL DEFAULT 0 COMMENT '�������� LogID ����� �������� ���������� ������',
  End_LogID INT(11) NOT NULL DEFAULT 0 COMMENT '�������� LogID ����� ������� ����������� ������',
  Begin_SrvPacketID BIGINT NOT NULL COMMENT '������������ ���������� ������ � ������� ������ ������ � Begin_LogID',
  End_SrvPacketID BIGINT NOT NULL COMMENT '������������ ���������� ������ � ������� ������ ������ � End_LogID',
  PRIMARY KEY PK_DatagpslostOn(Mobitel_ID, Begin_LogID),
  INDEX IDX_MobitelID (Mobitel_ID),
  UNIQUE INDEX IDX_MobitelID_BeginSrvPacketID (Mobitel_ID, Begin_SrvPacketID)
) ENGINE=InnoDB COMMENT='�������� � ������ DataGPS, ���������� �� Online �������';


DROP TABLE IF EXISTS datagpslost_ontmp;
CREATE TABLE datagpslost_ontmp (
  LogID INT(11) NOT NULL DEFAULT 0,
  SrvPacketID BIGINT NOT NULL
) ENGINE = MEMORY COMMENT = '������������ ������������� ���������� OnLostDataGPS';