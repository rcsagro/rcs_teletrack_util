DELIMITER $$
DROP PROCEDURE IF EXISTS OnLostDataGPS2$$
CREATE PROCEDURE OnLostDataGPS2(IN MobitelID INTEGER(11), OUT NeedOptimize TINYINT)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '����������� ���������� ���������. ������ ����������� ������ ��������� ���������.'
BEGIN
  /* ������� ���� ��������� ����������� ���, ��� ������ ���������� ��������� �������������� 
     �� ���� LogId ������� DataGps, � � ��������� ������� ���������� ����������� DataGps 
     � ������� ����������� ��������������� ��������� � �������� ��������� �������� Id ������.
     ������ ���������� ��������� ��������: � ��� ���� ������������������ �������, � �� ������� 
     � ���� ��������� ID ����� ��������� ����� DataGps. ���� �� �� ����� ������������ ������	
     OnLostDataGPS2, �� �������� ������ �� ������� ������� (������ �������) � ���� ������� 
     ����� ��������� ������������� � ������� � �� ����� "������". ��� ��������� ��� ��� 
     � ����������� ��������� Id �������, ��� ����� �������� "������������". */

  /********** DECLARE **********/
    
  DECLARE NewConfirmedID INT DEFAULT 0; /* ����� �������� ConfirmedID */
  DECLARE BeginLogID INT; /* Begin_LogID � ������� CursorLostRanges */
  DECLARE EndLogID INT; /* End_LogID � ������� CursorLostRanges */
  DECLARE BeginSrvPacketID BIGINT; /* Begin_SrvPacketID � ������� CursorLostRanges */
  DECLARE EndSrvPacketID BIGINT; /* End_SrvPacketID � ������� CursorLostRanges */
  DECLARE NewRowCount INT DEFAULT 0; /* ���������� ����� ������� � ��������� */
  DECLARE RowCountForAnalyze INT DEFAULT 0; /* ���������� ������� ��� ������� */
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
    
  /* ������ ��� ������� �� ���������� ��������� ��������� */
  DECLARE CursorLostRanges CURSOR FOR
    SELECT Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID 
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;    

  /* ������ ��� ������� �� ����� LogID */
  DECLARE CursorNewLogID CURSOR FOR
    SELECT LogID, SrvPacketID 
    FROM datagpslost_ontmp
    ORDER BY SrvPacketID;    
    
  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  /********** END DECLARE **********/

  SET NeedOptimize = 0;
  /* ��������� ��� ������� �������  */
  SET @InsertLostDataStatement = 
    'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';
  
  TRUNCATE TABLE datagpslost_ontmp;
  
  /* ���������� ��������� � ��������� ������� ���� ��� �������� */
  SET DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  WHILE DataExists = 1 DO
    /* ���������� ��������� ������� ������������ ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      SELECT DISTINCT LogID, SrvPacketID
      FROM datagpsbuffer_ontmp
      WHERE (Mobitel_ID = MobitelID) AND (SrvPacketID > BeginSrvPacketID)
        AND (SrvPacketID < EndSrvPacketID)
      ORDER BY SrvPacketID;
      
    /* ���� ���� ������ ������������ �������� */  
    SELECT COUNT(1) INTO RowCountForAnalyze
    FROM datagpslost_ontmp;  
    
    IF RowCountForAnalyze > 0 THEN  
      /* ��������� ������ � ��������� ������ ��� ������� */  
      INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      VALUES (BeginLogID, BeginSrvPacketID), (EndLogID, EndSrvPacketID);
      
      /* ������� ������ �������� */
      DELETE FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);
       
      /**********  INSERT ROWS **********/             
      BEGIN
        DECLARE NewLogID INT; /* �������� LogID � ������� CursorNewLogID */
        DECLARE NewSrvPacketID BIGINT; /* �������� SrvPacketID � ������� CursorNewLogID */
        DECLARE FirstLogID INT DEFAULT -9999999; /* ������ �������� LogID ��� ������� */
        DECLARE SecondLogID INT; /* ������ �������� LogID ��� ������� */
        DECLARE FirstSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
        DECLARE SecondSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
        
        /* ���������� ������������� ������ ��� FETCH ��������� ������ */
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
        
        /* ������������ �������-������� � ������� ����������� ���������� */
        SET DataExists = 1;
        OPEN CursorNewLogID;
        FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        WHILE DataExists = 1 DO
          IF FirstLogID = -9999999 THEN
            /* ������ �������� � ������ */
            SET FirstLogID = NewLogID;
            SET FirstSrvPacketID = NewSrvPacketID;
          ELSE
            /* �������� ����� �������*/
            SET SecondLogID = NewLogID;
            SET SecondSrvPacketID = NewSrvPacketID;
            
            IF (SecondSrvPacketID -  FirstSrvPacketID) > 1 THEN
              /* ������ ������ */
              IF NewRowCount > 0 THEN
                SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, ",");
              END IF;
              
              SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, 
                " (", MobitelID, ", ", FirstLogID, ", ", SecondLogID, ", ", 
                FirstSrvPacketID, ", ", SecondSrvPacketID, ")");
                  
              SET NewRowCount = NewRowCount + 1;
            END IF;
            /* ���������� ������� ������ �������� �� ������ �������
               ��� ������� � ��������� �������� � �������� ���������� �������� */
            SET  FirstLogID = SecondLogID;
            SET FirstSrvPacketID = SecondSrvPacketID;
          END IF;  
          FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        END WHILE;
        CLOSE CursorNewLogID;
      END;
      /********** END INSERT ROWS **********/  

      TRUNCATE TABLE datagpslost_ontmp;
    END IF;
      
    FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  END WHILE;
  CLOSE CursorLostRanges;
  
  IF NewRowCount > 0 THEN
    /*--------- ���������� ������� ����������� ������ ----------*/
    PREPARE stmt FROM @InsertLostDataStatement;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
          
    SET  NeedOptimize = 1; 
  END IF; -- NewRowCount > 0 
  
  /********** UPDATE MOBITELS **********/
    
  /* ���� ����������� ������� � ������� ��������� ��� - 
     ����� ConfirmedID ����� ����� ������������� LogID 
     ������� ���������. ����� - ����������� Begin_LogID
     �� ������� ��������� */
  SELECT MIN(Begin_LogID) INTO NewConfirmedID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;
       
  IF NewConfirmedID IS NULL THEN
    SELECT MAX(LogID) INTO NewConfirmedID
    FROM datagps
    WHERE Mobitel_ID = MobitelID;
  END IF;  
    
  /* ������� ConfirmedID ���� ���� */
  UPDATE mobitels
  SET ConfirmedID = NewConfirmedID
  WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);
    
  /********** END UPDATE MOBITELS **********/
END$$
DELIMITER ;