DELIMITER $$
DROP PROCEDURE IF EXISTS OnListMobitelConfig$$
CREATE PROCEDURE OnListMobitelConfig()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '������� �������� ���������� ������� ����� �������� � online ������'
BEGIN
  DECLARE MobitelIDInCursor INT;
  DECLARE DevIdShortInCursor CHAR(4);
  DECLARE DataExists TINYINT DEFAULT 1;

  /* ������ � ��������� ����������� ���������� */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS MobitelID, c.devIdShort AS DevIdShort
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID)
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND
      (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')
    ORDER BY m.Mobitel_ID;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* ��������� ������� �������� ��������� */
  CREATE TEMPORARY TABLE tmpMobitelsConfig (
    MobitelID INT NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL) ENGINE = MEMORY;

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor, DevIdShortInCursor;
  WHILE DataExists = 1 DO

    INSERT INTO tmpMobitelsConfig SELECT MobitelIDInCursor, DevIdShortInCursor,
      (SELECT COALESCE(MAX(SrvPacketID), 0)
       FROM datagps
       WHERE Mobitel_ID = MobitelIDInCursor) AS LastPacketID;

    FETCH CursorMobitels INTO MobitelIDInCursor, DevIdShortInCursor;
  END WHILE;
  CLOSE CursorMobitels;

  SELECT MobitelID, DevIdShort, LastPacketID
  FROM tmpMobitelsConfig;

  DROP TEMPORARY TABLE tmpMobitelsConfig;
END$$
DELIMITER ;