
ALTER TABLE datagps ADD COLUMN SrvPacketID BIGINT NOT NULL DEFAULT '0'
  COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������';

UPDATE datagps SET SrvPacketID = 0
WHERE SrvPacketID IS NULL;
  
ALTER TABLE datagps ADD INDEX 
  IDX_MobitelIDSrvPacketID (Mobitel_ID, SrvPacketID);


  