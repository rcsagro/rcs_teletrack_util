/*  ������ ������� ��������� ����� �� ������� datagpsbuffer_on ��������� ������
    �������� ���������� �������� � ����� Mobitel_Id + LogID. �� ���������
    ������������� ������� � ������� datagpsbuffer_on ��������� ������ ���� - 
    ��������� �������� �������������� ��������, �.�. � ������������ DataGps_ID.  */

DROP PROCEDURE IF EXISTS OnDeleteDuplicates;

DELIMITER $$

CREATE PROCEDURE OnDeleteDuplicates()
  SQL SECURITY INVOKER
  COMMENT 'Delete duplicates rows from datagpsbuffer_on'
BEGIN
  /* ������� MobitelId � ������� CursorDuplicateGroups */
  DECLARE MobitelIdInCursor INT DEFAULT 0; 
  /* ������� LogID � ������� CursorDuplicateGroups */
  DECLARE LogIdInCursor INT DEFAULT 0; 
  /* Max SrvPacketId � ��������� ����� ������� ������ ���������� */
  DECLARE MaxSrvPacketId INT DEFAULT 0; 
  DECLARE DataExists TINYINT DEFAULT 1;

  /* ������ ��� ������� �� ���� ������� ���������� */  
  DECLARE CursorDuplicateGroups CURSOR FOR
    SELECT Mobitel_Id, LogId 
    FROM datagpsbuffer_on
    GROUP BY Mobitel_Id, LogId 
    HAVING COUNT(LogId) > 1;
    
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  WHILE DataExists = 1 DO
    SELECT MAX(SrvPacketID) INTO MaxSrvPacketId
    FROM datagpsbuffer_on 
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor);
         
    DELETE FROM datagpsbuffer_on  
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor) 
      AND (SrvPacketID < MaxSrvPacketId);
        
    FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  END WHILE;
  CLOSE CursorDuplicateGroups; 
END$$

DELIMITER ;
