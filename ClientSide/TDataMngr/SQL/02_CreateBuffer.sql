DROP TABLE IF EXISTS datagpsbuffer_on;
CREATE TABLE datagpsbuffer_on (
  Mobitel_ID int(11) NOT NULL default 0,
  Message_ID int(11) default 0,
  Latitude int(11) NOT NULL default 0,
  Longitude int(11) NOT NULL default 0,
  Altitude int(11) default NULL,
  UnixTime int(11) NOT NULL default 0,
  Speed smallint(6) NOT NULL default 0,
  Direction int(11) NOT NULL default 0,
  Valid tinyint(4) NOT NULL default 1,
  InMobitelID int(11) default NULL,
  Events int(10) unsigned default 0,
  Sensor1 tinyint(4) unsigned default 0,
  Sensor2 tinyint(4) unsigned default 0,
  Sensor3 tinyint(4) unsigned default 0,
  Sensor4 tinyint(4) unsigned default 0,
  Sensor5 tinyint(4) unsigned default 0,
  Sensor6 tinyint(4) unsigned default 0,
  Sensor7 tinyint(4) unsigned default 0,
  Sensor8 tinyint(4) unsigned default 0,
  LogID int(11) NOT NULL default 0,
  isShow tinyint(4) default 0,
  whatIs smallint(6) NOT NULL default 0,
  Counter1 int(11) NOT NULL default -1,
  Counter2 int(11) NOT NULL default -1,
  Counter3 int(11) NOT NULL default -1,
  Counter4 int(11) NOT NULL default -1,
  SrvPacketID BIGINT NOT NULL DEFAULT 0 COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������',
  INDEX IDX_MobitelidLogid USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid USING BTREE (Mobitel_ID, UnixTime, Valid)
) ENGINE=MEMORY DEFAULT CHARSET=cp1251 COMMENT '����� ������ ������';


DROP TABLE IF EXISTS datagpsbuffer_ontmp;
CREATE TABLE datagpsbuffer_ontmp (
  Mobitel_ID INT(11) NOT NULL DEFAULT 0,
  LogID INT(11) NOT NULL DEFAULT 0,
  SrvPacketID BIGINT NOT NULL,
  INDEX IDX_MobitelidSrvpacketid USING BTREE (Mobitel_ID, SrvPacketID),
  INDEX IDX_Mobitelid USING BTREE (Mobitel_ID)
)ENGINE = MEMORY DEFAULT CHARSET=cp1251 COMMENT '��������� ��������������� ����� ������ ������';


