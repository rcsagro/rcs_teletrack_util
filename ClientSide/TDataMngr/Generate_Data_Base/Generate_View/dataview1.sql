IF OBJECT_ID ('dataview1', 'V') IS NOT NULL
DROP VIEW dataview1;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE VIEW dbo.dataview1
AS SELECT datagps.DataGps_ID AS DataGps_ID
        , datagps.Mobitel_ID AS Mobitel_ID
        , (datagps.Latitude / 600000.00000) AS Lat
        , (datagps.Longitude / 600000.00000) AS Lon
        , datagps.Altitude AS Altitude
        , dbo.FROM_UNIXTIME(datagps.UnixTime) AS time
        , (datagps.Speed * 1.852) AS speed
        , ((datagps.Direction * 360) / 255) AS direction
        , datagps.Valid AS Valid
        , (((((((datagps.Sensor8 + (datagps.Sensor7 * 2 ^ 8)) + (datagps.Sensor6 * 2 ^ 16)) + (datagps.Sensor5 * 2 ^ 24)) + (datagps.Sensor4 * 2 ^ 32)) + (datagps.Sensor3 * 2 ^ 40)) + (datagps.Sensor2 * 2 ^ 48)) + (datagps.Sensor1 * 2 ^ 56)) AS sensor
        , datagps.Events AS Events
        , datagps.LogID AS LogID
   FROM
     datagps
GO