IF OBJECT_ID ('internalmobitelconfigview', 'V') IS NOT NULL
DROP VIEW internalmobitelconfigview;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE VIEW dbo.internalmobitelconfigview
AS (SELECT c.ID AS ID
         , max(c.InternalMobitelConfig_ID) AS LastRecord
   FROM
     internalmobitelconfig c
   WHERE
     (c.devIdShort <> '')
   GROUP BY
     c.ID)
GO