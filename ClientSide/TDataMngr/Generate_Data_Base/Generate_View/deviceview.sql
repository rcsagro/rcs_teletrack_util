IF OBJECT_ID ('deviceview', 'V') IS NOT NULL
DROP VIEW deviceview;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE VIEW dbo.deviceview
AS (SELECT m.Mobitel_ID AS Mobitel_ID
         , m.Name AS Name
         , m.Descr AS Descr
         , c.devIdShort AS devIdShort
         , m.DeviceType AS DeviceType
         , m.ConfirmedID AS ConfirmedID
         , c.moduleIdRf AS moduleIdRf
   FROM
     ((mobitels m
     JOIN internalmobitelconfigview v
       ON v.ID = m.InternalMobitelConfig_ID)
     JOIN internalmobitelconfig c
       ON v.LastRecord = c.InternalMobitelConfig_ID)
   --WHERE
   --  ((v.ID = m.InternalMobitelConfig_ID)
   --  AND (v.LastRecord = c.InternalMobitelConfig_ID))
   )
GO