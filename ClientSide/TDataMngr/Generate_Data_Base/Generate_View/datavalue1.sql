IF OBJECT_ID ('datavalue1', 'V') IS NOT NULL
DROP VIEW datavalue1;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE VIEW dbo.datavalue1
AS SELECT round((datagps.Longitude / 600000), 6) AS lon
        , round((datagps.Latitude / 600000), 6) AS lat
        , datagps.Mobitel_ID AS Mobitel_ID
        , sensordata.value AS Value
        , (datagps.Speed * 1.852) AS speed
        , dbo.from_unixtime(datagps.UnixTime) AS time
        , sensordata.sensor_id AS sensor_id
        , datagps.DataGps_ID AS datavalue_id
   FROM
     (sensordata
     JOIN datagps
       ON ((sensordata.datagps_id = datagps.DataGps_ID)))
   WHERE
     (datagps.Valid = 1)
GO