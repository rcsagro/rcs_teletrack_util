I) ��� ���� ����� ������� ����� ���� ������ �� ������� MS SQL 2008 R2
���������� ��������� � SQL Server Management Studio 
������� ����������� � ���� ��������� ������, � ��������� �������:
1. create_empty_database.sql
2. generate_functions.sql
3. generate_procedures.sql
4. generate_tables.sql
5. generate_views.sql
��������� ����� ��������� � ����� Generate_Objects.

II) ����� ���� ������ ����� ����� ������������� �� ���� ��� �������� ������ 
��������� � ����� generate_all_database_mssql.sql

III) � ���� ��������� ������ ��������� ����� �� ��������� ��� ��������� 
��������� �������� ���� ������.
Generate_Function - ��� ��������� ��������� �������;
Generate_Procedure - ��� ��������� �������� ��������;
Generate_Table - ��� ��������� ������;
Generate_View - ��� ��������� �������������.

IV) � ����� script_default_data_base.sql ��������� ������ SQL ��� ���������� ��������� ������ ���� 
���������� �� ���������.