IF OBJECT_ID ( 'dbo.TransferLiteBuffer', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.TransferLiteBuffer;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.TransferLiteBuffer
--SQL SECURITY INVOKER
--COMMENT '��������� GPS ������ �� �������� ������� � datagps.'
AS
BEGIN
  IF object_id('tempdb..#temp2') IS NOT NULL
    DROP TABLE #temp2;

  CREATE TABLE #temp2(
    DataGps_ID INT
  );
  INSERT INTO #temp2
  SELECT DataGPS_ID
  FROM
    datagpsbufferlite;

  UPDATE datagps
  SET
    Message_ID = b.Message_ID, Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = NULL, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, isShow = b.IsShow, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagpsbuffer b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    Mobitel_ID = b.Mobitel_ID AND
    Latitude = b.Latitude AND
    Longitude = b.Longitude AND
    Altitude = b.Altitude AND
    UnixTime = b.UnixTime;

  DELETE b
  FROM
    datagps d, datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.Latitude = b.Latitude AND
    d.Longitude = b.Longitude AND
    d.Altitude = b.Altitude AND
    d.UnixTime = b.UnixTime;

  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.Events
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs
  FROM
    datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID
  GROUP BY
    b.Mobitel_ID
  , b.LogID
  , b.Latitude
  , b.Longitude
  , b.Altitude
  , b.UnixTime;

  DELETE b
  FROM
    datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID;

  DROP TABLE #temp2
END
GO