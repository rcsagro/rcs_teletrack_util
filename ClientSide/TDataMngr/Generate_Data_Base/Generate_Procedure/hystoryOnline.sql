IF OBJECT_ID ( 'dbo.hystoryOnline', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.hystoryOnline;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.hystoryOnline
(
  @id INTEGER
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT LogID
       , Mobitel_Id
       , dbo.FROM_UNIXTIME(unixtime) AS time
       , round((Latitude / 600000.00000), 6) AS Lat
       , round((Longitude / 600000.00000), 6) AS Lon
       , Altitude AS Altitude
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 2 ^ 8) + (Sensor6 * 2 ^ 16) + (Sensor5 * 2 ^ 24) + (Sensor4 * 2 ^ 32) + (Sensor3 * 2 ^ 40) + (Sensor2 * 2 ^ 48) + (Sensor1 * 2 ^ 56)) AS sensor
       , Events AS Events
       , DataGPS_ID AS 'DataGPS_ID'
  FROM
    online
  WHERE
    datagps_id > @id
  ORDER BY
    datagps_id DESC;
END
GO