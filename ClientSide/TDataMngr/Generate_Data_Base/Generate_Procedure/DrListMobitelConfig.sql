IF OBJECT_ID ( 'dbo.DrListMobitelConfig', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.DrListMobitelConfig;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.DrListMobitelConfig
--SQL SECURITY INVOKER
--COMMENT '������� �������� ���������� � ������ DirectOnline'
AS
BEGIN
  SELECT m.Mobitel_id AS MobitelID
       , imc.DevIdShort AS DevIdShort
       , cge.Pop3un AS Login
       , cge.Pop3pw AS Password
  FROM
    Mobitels m
    JOIN ConfigMain cm
      ON m.Mobitel_id = cm.Mobitel_ID
    JOIN ConfigGprsEmail cge
      ON cm.ConfigGprsEmail_ID = cge.ID
    JOIN internalmobitelconfig imc
      ON m.InternalMobitelConfig_ID = imc.ID
  WHERE
    (cge.ConfigGprsEmail_ID = (SELECT max(intCge.ConfigGprsEmail_ID)
                               FROM
                                 ConfigGprsEmail intCge
                               WHERE
                                 intCge.ID = cge.ID))
    AND (imc.InternalMobitelConfig_ID = (SELECT max(intConf.InternalMobitelConfig_ID)
                                         FROM
                                           internalmobitelconfig intConf
                                         WHERE
                                           intConf.ID = imc.ID))
    AND (imc.devIdShort IS NOT NULL)
    AND (imc.devIdShort <> '')
    AND (cge.Pop3un IS NOT NULL)
    AND (cge.Pop3un <> '')
  ORDER BY
    m.Mobitel_ID;
END
GO