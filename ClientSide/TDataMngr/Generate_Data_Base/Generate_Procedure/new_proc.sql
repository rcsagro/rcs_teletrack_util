IF OBJECT_ID ( 'dbo.new_proc', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.new_proc;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.new_proc
(
  @sensor     INTEGER,
  @m_id       INTEGER,
  @s_id       INTEGER,
  @begin_time DATETIME,
  @end_time   DATETIME
)
--SQL SECURITY INVOKER
--READS SQL DATA
AS
BEGIN
  SELECT round((datagps.Longitude / 600000), 6) AS lon
       , round((datagps.Latitude / 600000), 6) AS lat
       , datagps.Mobitel_ID AS Mobitel_ID
       , sensordata.value AS Value
       , (datagps.Speed * 1.852) AS speed
       , dbo.from_unixtime(datagps.UnixTime) AS time
       , sensordata.sensor_id AS sensor_id
       , datagps.DataGps_ID AS datagps_id
       , datagps.Sensor1 & @sensor AS sensor
  FROM
    (sensordata
    JOIN datagps
      ON ((sensordata.datagps_id = datagps.DataGps_ID)))
  WHERE
    (datagps.Valid = 1)
    AND (datagps.UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@begin_time) AND dbo.UNIX_TIMESTAMPS(@end_time))
    AND datagps.Mobitel_ID = @m_id
    AND sensordata.sensor_id = @s_id
  ORDER BY
    datagps.UnixTime
  , datagps.Mobitel_ID
  , sensordata.sensor_id;
END
GO