IF OBJECT_ID ( 'dbo.OnListMobitelConfig', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnListMobitelConfig;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnListMobitelConfig
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  DECLARE @DevIdShortInCursor CHAR(4);
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /*  */
  DECLARE CursorMobitels CURSOR LOCAL FOR
  SELECT m.Mobitel_ID AS MobitelID
       , c.devIdShort AS DevIdShort
  FROM
    mobitels m
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort IS NOT NULL)
    AND (c.devIdShort <> '')
  ORDER BY
    m.Mobitel_ID;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /*  */
  CREATE TABLE tmpMobitelsConfig(
    MobitelID INT NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL
  );

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    INSERT INTO tmpMobitelsConfig
    SELECT @MobitelIDInCursor
         , @DevIdShortInCursor
         , (SELECT coalesce(max(SrvPacketID), 0)
            FROM
              datagps
            WHERE
              Mobitel_ID = @MobitelIDInCursor) AS LastPacketID;

    FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
  END;
  CLOSE CursorMobitels;
  --DEALLOCATE CursorMobitels;

  SELECT MobitelID
       , DevIdShort
       , LastPacketID
  FROM
    tmpMobitelsConfig;

  DROP TABLE tmpMobitelsConfig;
END

GO