IF OBJECT_ID ( 'dbo.DrTransferBuffer', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.DrTransferBuffer;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.DrTransferBuffer
--SQL SECURITY INVOKER
--COMMENT '��������� GPS ������ DirectOnline �� �������� ������� � datagps'
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  --DECLARE @DataExists TINYINT;
  --SET @DataExists = 1; 

  DECLARE @TmpMaxUnixTime INT;

  DECLARE CursorMobitels CURSOR FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_dr;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  DELETE o
  FROM
    online o, datagpsbuffer_dr dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  --SET DataExists = 1;
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    INSERT INTO online(Mobitel_ID
                     , LogID
                     , UnixTime
                     , Latitude
                     , Longitude
                     , Altitude
                     , Direction
                     , Speed
                     , Valid
                     , Events
                     , Sensor1
                     , Sensor2
                     , Sensor3
                     , Sensor4
                     , Sensor5
                     , Sensor6
                     , Sensor7
                     , Sensor8
                     , Counter1
                     , Counter2
                     , Counter3
                     , Counter4
                     , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , Events
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_dr
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;


    DELETE
    FROM
      online
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        online
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END; --END WHILE;
  CLOSE CursorMobitels;

  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = b.LogID, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagps d, datagpsbuffer_dr b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  DELETE b
  FROM
    datagps d, datagpsbuffer_dr b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);


  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , Events
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
  FROM
    datagpsbuffer_dr;

  TRUNCATE TABLE datagpsbuffer_dr;
END

GO