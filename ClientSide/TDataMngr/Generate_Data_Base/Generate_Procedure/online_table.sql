IF OBJECT_ID ( 'dbo.online_table', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.online_table;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.online_table
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT Log_ID
       , MobitelId
       , (SELECT TOP 1 DataGPS_ID
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS 'DataGPS_ID'
       , (SELECT TOP 1 dbo.from_unixtime(unixtime)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS 'time'
       , (SELECT TOP 1 round((Latitude / 600000.00000), 6)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lat
       , (SELECT TOP 1 round((Longitude / 600000.00000), 6)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lon
       , (SELECT TOP 1 Altitude
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Altitude
       , (SELECT TOP 1 (Speed * 1.852)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS speed
       , (SELECT TOP 1 ((Direction * 360) / 255)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS direction
       , (SELECT TOP 1 Valid
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Valid
       , (SELECT TOP 1 (((((((Sensor8 + (Sensor7 * 2 ^ 8)) + (Sensor6 * 2 ^ 16)) + (Sensor5 * 2 ^ 24)) + (Sensor4 * 2 ^ 32)) + (Sensor3 * 2 ^ 40)) + (Sensor2 * 2 ^ 48)) + (Sensor1 * 2 ^ 56))
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS sensor
       , (SELECT TOP 1 Events
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Events

  FROM
    (SELECT max(LogID) AS Log_ID
          , mobitel_id AS MobitelId
     FROM
       online
     GROUP BY
       Mobitel_ID) T1;
END
GO