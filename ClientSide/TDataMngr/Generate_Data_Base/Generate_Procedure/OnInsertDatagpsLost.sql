IF OBJECT_ID ( 'dbo.OnInsertDatagpsLost', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnInsertDatagpsLost;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnInsertDatagpsLost
(
  @MobitelID        INT,
  @BeginLogID       INT,
  @EndLogID         INT,
  @BeginSrvPacketID BIGINT,
  @EndSrvPacketID   BIGINT
)
AS
--SQL SECURITY INVOKER
--COMMENT '������� ������ � ������� datagpslost_on.'
BEGIN
  /* ???? ????????????? ?????? ? ????????? Mobitel_ID ? Begin_LogID
     ? ??????? datagpslost_on */
  DECLARE @MobitelId_BeginLogID_Exists TINYINT;
  SET @MobitelId_BeginLogID_Exists = 0;
  /* ???? ????????????? ?????? ? ????????? Mobitel_ID ? Begin_SrvPacketID
     ? ??????? datagpslost_on  */
  DECLARE @MobitelId_BeginSrvPacketID_Exists TINYINT;
  SET @MobitelId_BeginSrvPacketID_Exists = 0;

  DECLARE @RowBeginSrvPacketID BIGINT;

  IF EXISTS (SELECT NULL
             FROM
               datagpslost_on
             WHERE
               (Mobitel_ID = @MobitelID)
               AND (Begin_LogID = @BeginLogID))
  BEGIN
    SET @MobitelId_BeginLogID_Exists = 1;
  END; -- IF

  IF EXISTS (SELECT NULL
             FROM
               datagpslost_on
             WHERE
               (Mobitel_ID = @MobitelID)
               AND (Begin_SrvPacketID = @BeginSrvPacketID))
  BEGIN
    SET @MobitelId_BeginSrvPacketID_Exists = 1;
  END; -- IF

  IF (@MobitelId_BeginLogID_Exists = 0) AND (@MobitelId_BeginSrvPacketID_Exists = 0)
  BEGIN
    /* ??????? ????? ?????? */
    INSERT INTO datagpslost_on(Mobitel_ID
                             , Begin_LogID
                             , End_LogID
                             , Begin_SrvPacketID
                             , End_SrvPacketID)
    VALUES
      (@MobitelID, @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID);
  END
  ELSE
  IF ((@MobitelId_BeginLogID_Exists = 1) AND (@MobitelId_BeginSrvPacketID_Exists = 1))
  BEGIN
    /* ?????????? ???????????? ?????? */
    UPDATE datagpslost_on
    SET
      End_LogID = @EndLogID, End_SrvPacketID = @EndSrvPacketID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (Begin_LogID = @BeginLogID);
  END
  ELSE
  BEGIN
    /* ?????????? MobitelId_BeginLogID_Exists = 0, MobitelId_BeginSrvPacketID_Exists = 1
         ?????????? - ??? ?????????????? ????????, ??????? ?? ?????? ?????????. */
    SET @RowBeginSrvPacketID = (SELECT TOP 1 Begin_SrvPacketID
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (Begin_LogID = @BeginLogID));

    IF ((@MobitelId_BeginLogID_Exists = 1) AND (@MobitelId_BeginSrvPacketID_Exists = 0) AND (@BeginSrvPacketID < @RowBeginSrvPacketID))
    BEGIN
      /* ? ??? ????? ????????, ???? ???????? ?????? ???? ? ?? ?? ?????? ????????? ??? 
           (LogID ? ??????? ???? ? ?????, ? SrvPacketID ????????) */
      UPDATE datagpslost_on
      SET
        End_LogID = @EndLogID, Begin_SrvPacketID = @BeginSrvPacketID, End_SrvPacketID = @EndSrvPacketID
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);
    END; -- IF
  END; -- ELSE
END
--END

GO