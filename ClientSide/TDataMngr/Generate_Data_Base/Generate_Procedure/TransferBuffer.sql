IF OBJECT_ID ( 'dbo.TransferBuffer', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.TransferBuffer;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.TransferBuffer
--SQL SECURITY INVOKER
--COMMENT '��������� GPS ������ �� �������� ������� � datagps.'
AS
BEGIN

  --CREATE TEMPORARY TABLE IF NOT EXISTS temp1 ENGINE = HEAP

  IF object_id('tempdb..#temp1') IS NOT NULL
    DROP TABLE #temp1;

  CREATE TABLE #temp1(
    DataGps_ID INT
  );
  INSERT INTO #temp1
  SELECT DataGPS_ID
  FROM
    datagpsbuffer;

  UPDATE datagps
  SET
    Message_ID = 0, Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = NULL, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, isShow = 0, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.LogID = b.LogID;

  DELETE b
  FROM
    datagps d, datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.LogID = b.LogID;

  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.Events
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID
  GROUP BY
    b.Mobitel_ID
  , b.LogID;


  DELETE b
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID;

  DROP TABLE #temp1
END
GO