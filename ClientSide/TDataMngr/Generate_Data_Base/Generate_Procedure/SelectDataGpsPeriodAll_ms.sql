CREATE PROCEDURE [dbo].[SelectDataGpsPeriodAll]
(
  @mobile      INT,
  @beginperiod DATETIME,
  @endperiod   DATETIME,
  @timebegin   VARCHAR(9),
  @timeend     VARCHAR(9),
  @weekdays    VARCHAR(15),
  @daymonth    VARCHAR(90)
)
AS
BEGIN
  -- DECLARE @begin DATETIME;
  -- SET @begin = convert(DATETIME, '24/03/2013 00:00:00');
  -- DECLARE @end DATETIME;
  -- SET @end = convert(DATETIME, '1/04/2013 23:59:59');
  DECLARE @saTail VARCHAR(90);
  DECLARE @saHead VARCHAR(2);
  DECLARE @indx INT;
  DECLARE @time0 TIME;
  SET @time0 = '23:59:59';
  DECLARE @time1 TIME;
  SET @time1 = '00:00:00';

  IF object_id('tempdb..#tempperiod4') IS NOT NULL
    DROP TABLE #tempperiod4;

  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS time
       , (Speed * 1.852) AS speed
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * (2 ^ 8)) + (Sensor6 * (2 ^ 16)) + (Sensor5 * (2 ^ 24)) + (Sensor4 * (2 ^ 32)) + (Sensor3 * (2 ^ 40)) + (Sensor2 * (2 ^ 48)) + (Sensor1 * (2 ^ 56))) AS sensor
       , Events AS Events
       , LogID AS LogID
  INTO
    #tempperiod4
  FROM
    datagps
  WHERE
    Mobitel_ID = @mobile
    AND (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@beginperiod) AND dbo.UNIX_TIMESTAMPS(@endperiod))
    AND (Valid = 1)
    AND NOT (Latitude = 0
    AND Longitude = 0)
  ORDER BY
    UnixTime;

  --SELECT * FROM #tempperiod4;
  IF object_id('tempdb..#tempweeks') IS NOT NULL
    DROP TABLE #tempweeks;

  IF @weekdays != ''
  BEGIN
    CREATE TABLE #tempweeks(
      shead VARCHAR(2)
    );

    SET @saTail = @weekdays;

    WHILE @saTail != ''
    BEGIN
      SET @indx = charindex(';', @saTail);
      SET @saHead = substring(@saTail, 1, @indx - 1);
      SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

      INSERT INTO #tempweeks(shead)
      VALUES
        (@saHead);
    END;
  END -- IF

  IF object_id('tempdb..#tempday0') IS NOT NULL
    DROP TABLE #tempday0;

  IF @daymonth != ''
  BEGIN
    CREATE TABLE #tempday0(
      shead VARCHAR(2)
    );

    SET @saTail = @daymonth;

    WHILE @saTail != ''
    BEGIN
      SET @indx = charindex(';', @saTail);
      SET @saHead = substring(@saTail, 1, @indx - 1);
      SET @saTail = substring(@saTail, @indx + 1, datalength(@saTail) - @indx);

      INSERT INTO #tempday0(shead)
      VALUES
        (@saHead);
    END; -- while
  END; -- IF

  -- getting select data
  IF (@daymonth != '') AND (@weekdays != '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- ������ �� ���� ������
        (datepart (dw, t.time) IN (SELECT *
                                   FROM
                                     #tempweeks))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend))); -- ������ ���
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- ������ �� ���� ������
        (datepart (dw, t.time) IN (SELECT *
                                   FROM
                                     #tempweeks))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend)))); -- ������ ���
    END;
  END;

  IF (@daymonth = '') AND (@weekdays != '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((datepart (dw, t.time) IN (SELECT *
                                    FROM
                                      #tempweeks))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend))); -- ������ ���
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((datepart (dw, t.time) IN (SELECT *
                                    FROM
                                      #tempweeks))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend)))); -- ������ ���
    END;
  END;

  IF (@daymonth != '') AND (@weekdays = '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend))); -- ������ ���
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        ((day(t.time) IN (SELECT *
                          FROM
                            #tempday0))
        AND -- ������ �� ���� ������
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend)))); -- ������ ���
    END;
  END;

  IF (@daymonth = '') AND (@weekdays = '')
  BEGIN
    IF @timebegin < @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        (convert(TIME, t.time) BETWEEN convert(TIME, @timebegin) AND convert(TIME, @timeend)); -- ������ ���
    END;

    IF @timebegin > @timeend
    BEGIN
      SELECT *
      FROM
        #tempperiod4 t
      WHERE
        (convert(TIME, t.time) > convert(TIME, @timebegin)
        AND convert(TIME, t.time) < convert(TIME, @time0)
        OR (convert(TIME, t.time) > convert(TIME, @time1)
        AND convert(TIME, t.time) < convert(TIME, @timeend))); -- ������ ���
    END;
  END;
END
