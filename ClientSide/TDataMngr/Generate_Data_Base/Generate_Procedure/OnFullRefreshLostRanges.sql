IF OBJECT_ID ( 'dbo.OnFullRefreshLostRanges', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnFullRefreshLostRanges;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnFullRefreshLostRanges
AS
--NOT DETERMINISTIC
--CONTAINS SQL
--SQL SECURITY INVOKER
--COMMENT '����� ���� ��������� ��� TDataManager'
BEGIN
  DECLARE @MobitelIDInCursor INT; /* ������� �������� MobitelID */
  --DECLARE @DataExists TINYINT; /* ���� ������� ������ ����� ����� */
  --SET @DataExists = 1;

  DECLARE @LogIdInCursor INT; /* ������� �������� LogId ��������� */
  DECLARE @SrvPacketIdInCursor BIGINT; /* ������� �������� SrvPacketID ��������� */
  DECLARE @PreviousLogId INT; /* �������� LogId ��������� �� ���������� ����*/
  DECLARE @PreviousSrvPacketId BIGINT; /* �������� SrvPacketID ��������� �� ���������� ���� */

  DECLARE @FirstIteration TINYINT; /* ���� ������ �������� �� ������ */
  SET @FirstIteration = 1;
  DECLARE @NewRowCount INT; /* ���������� ����� ������� � ��������� */
  SET @NewRowCount = 0;
  DECLARE @NewConfirmedID INT; /* ����� �������� ConfirmedID */

  DECLARE @MaxNegativeDataGpsId INT; /* ������������ DataGpsId � ��������� ������������� LogId */
  DECLARE @MaxPositiveDataGpsId INT; /* ������������ DataGpsId � ��������� ������������� LogId */
  DECLARE @InsertLostDataStatement NVARCHAR;

  /* ������ �� ���� ���������� */
  DECLARE CursorMobitels CURSOR FOR
  SELECT DISTINCT (Mobitel_ID) AS Mobitel_ID
  FROM
    mobitels
  ORDER BY
    Mobitel_ID;

  /* ������ �� ������ ��������� */
  DECLARE CursorDataGps CURSOR FOR
  SELECT LogId
       , SrvPacketID
  FROM
    DataGps
  WHERE
    Mobitel_ID = @MobitelIDInCursor
  ORDER BY
    LogId;

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* ������� ������, ��������� ������ ��������� ��� TDataManager  */
  TRUNCATE TABLE datagpsbuffer_on;
  TRUNCATE TABLE datagpslost_on;
  TRUNCATE TABLE datagpslost_ontmp;

  /* ------------------- FETCH Mobitels ------------------------ */
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* ��������� ��� ������� �������  */
    SET @InsertLostDataStatement = 'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';

    /* ----------------- FETCH DataGPS --------------------- */
    SET @FirstIteration = 1;
    SET @NewRowCount = 0;

    OPEN CursorDataGps;
    FETCH CursorDataGps INTO @LogIdInCursor, @SrvPacketIdInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstIteration = 1
      BEGIN
        /* ������ �������� � ������ */
        SET @PreviousLogId = @LogIdInCursor;
        SET @PreviousSrvPacketId = @SrvPacketIdInCursor;
        SET @FirstIteration = 0;
      END
      ELSE /* �������� ����� �������*/
      BEGIN
        IF (((@LogIdInCursor - @PreviousLogId) > 1) AND (@PreviousSrvPacketId < @SrvPacketIdInCursor))
        BEGIN
          /* ������ ������ */
          IF @NewRowCount > 0
          BEGIN
            SET @InsertLostDataStatement = @InsertLostDataStatement + ',';
          END; --END IF;

          SET @InsertLostDataStatement = @InsertLostDataStatement + ' (' + @MobitelIDInCursor + ', ' + @PreviousLogId + ', ' + @LogIdInCursor + ', ' + @PreviousSrvPacketId + ', ' + @SrvPacketIdInCursor + ')';

          SET @NewRowCount = @NewRowCount + 1;
        END; --END IF;

        /* ���������� ������� �������� � ���������� */
        SET @PreviousLogId = @LogIdInCursor;
        SET @PreviousSrvPacketId = @SrvPacketIdInCursor;
      END; --END IF; -- IF @FirstIteration = 1 

      FETCH CursorDataGps INTO @LogIdInCursor, @SrvPacketIdInCursor;
    END; --END WHILE; --  WHILE DataExistsInner = 1 
    CLOSE CursorDataGps;

    /* ���� � ������� ��������� ���� ����������� ������ - 
       ����� ConfirmedID ����� ����� ������������ Begin_LogID 
       �� ������� ���������. 
       ����� - ������������� LogID ������� ��������� �� 
       ������� datagps � ����� �� ���������� ������ ���� ��� 
       ������ ����, � ����������� �� ������� LogId. */
    IF @NewRowCount > 0
    BEGIN
      /* ���������� ������� ����������� ������ */
      EXEC (@InsertLostDataStatement);

      SET @NewConfirmedID = (SELECT min(Begin_LogID)
                             FROM
                               datagpslost_on
                             WHERE
                               Mobitel_ID = @MobitelIDInCursor);
    END
    ELSE
    BEGIN
      /* � ���� ������ �� ��� ��� �������.
         ���� ���� ������������� �������� LogID, �� ��� �����������
         ������ �������� ConfirmedID ��������� �������������� ������:
         ��������� ����� �������� LogID ������������� ��� �������������
         ����� ����� (�� DataGpsId). */
      IF EXISTS (SELECT TOP 1 1
                 FROM
                   dbo.datagps
                 WHERE
                   (Mobitel_ID = @MobitelIDInCursor)
                   AND (LogId < 0))
      BEGIN
        SET @MaxNegativeDataGpsId = (SELECT max(DataGps_ID)
                                     FROM
                                       DataGps
                                     WHERE
                                       (Mobitel_ID = @MobitelIDInCursor)
                                       AND (LogId < 0));

        SET @MaxPositiveDataGpsId = (SELECT coalesce(max(DataGps_ID), 0)
                                     FROM
                                       DataGps
                                     WHERE
                                       (Mobitel_ID = @MobitelIDInCursor)
                                       AND (LogId >= 0));

        IF (@MaxNegativeDataGpsId > @MaxPositiveDataGpsId)
        BEGIN
          SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                                 FROM
                                   datagps
                                 WHERE
                                   (Mobitel_ID = @MobitelIDInCursor)
                                   AND (LogId < 0));
        END
        ELSE
        BEGIN
          SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                                 FROM
                                   datagps
                                 WHERE
                                   (Mobitel_ID = @MobitelIDInCursor)
                                   AND (LogId >= 0));
        END --END IF; -- IF (@MaxNegativeDataGpsId > @MaxPositiveDataGpsId)
      END
      ELSE
      BEGIN
        /* ���������� ������ ������������� LogId */
        SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                               FROM
                                 datagps
                               WHERE
                                 Mobitel_ID = @MobitelIDInCursor);
      END; --END IF; -- IF EXISTS 
    END; --END IF; -- IF @NewRowCount > 0

    /* ������� ConfirmedID */
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    --SET DataExists = 1;
    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END; --END WHILE;
  CLOSE CursorMobitels;
END;
GO