IF OBJECT_ID ( 'dbo.dataview', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.dataview;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.dataview
(
  @m_id       INTEGER,
  @val        SMALLINT,
  @begin_time DATETIME,
  @end_time   DATETIME
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS time
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 2 ^ 8) + (Sensor6 * 2 ^ 16) + (Sensor5 * 2 ^ 24) + (Sensor4 * 2 ^ 32) + (Sensor3 * 2 ^ 40) + (Sensor2 * 2 ^ 48) + (Sensor1 * 2 ^ 56)) AS sensor
       , Events AS Events
       , LogID AS LogID
  FROM
    datagps
  WHERE
    Mobitel_ID = @m_id
    AND (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@begin_time) AND dbo.UNIX_TIMESTAMPS(@end_time))
    AND (Valid = @val)
    AND NOT (Latitude = 0
    AND Longitude = 0)
  ORDER BY
    UnixTime;
END
GO