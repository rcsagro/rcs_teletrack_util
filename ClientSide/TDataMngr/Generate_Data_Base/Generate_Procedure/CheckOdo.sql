IF OBJECT_ID ( 'dbo.CheckOdo', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.CheckOdo;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.CheckOdo
(
  @m_id     INTEGER,
  @end_time DATETIME
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT dbo.FROM_UNIXTIME(datagps.UnixTime) AS time
       , (datagps.Latitude / 600000.00000) AS Lat
       , (datagps.Longitude / 600000.00000) AS Lon
  FROM
    datagps
  WHERE
    datagps.Mobitel_ID = @m_id
    AND datagps.UnixTime BETWEEN (SELECT dbo.UNIX_TIMESTAMPS(odoTime)
                                  FROM
                                    vehicle
                                  WHERE
                                    Mobitel_id = @m_id) AND dbo.UNIX_TIMESTAMPS(@end_time)
    AND Valid = 1;
END
GO