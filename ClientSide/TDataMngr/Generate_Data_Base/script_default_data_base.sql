INSERT INTO [dbo].[commands]
           ([ID]
           ,[Name]
           ,[Descr])
     VALUES
           (10,'DRIVER_MESSAGE_SET_D',NULL),
 (11,'CONFIG_SMS_SET_D',NULL),
 (12,'CONFIG_TEL_SET_D',NULL),
 (13,'CONFIG_EVENT_SET_D',NULL),
 (14,'CONFIG_UNIQUE_SET_D',NULL),
 (15,'CONFIG_ZONE_SET_D',NULL),
 (17,'CONFIG_ID_SET_D',NULL),
 (18,'CONFIG_OTHER_SET_D',NULL),
 (20,'DRIVER_MESSAGE_SET_M',NULL),
 (21,'CONFIG_SMS_SET_M',NULL),
 (22,'CONFIG_TEL_SET_M',NULL),
 (23,'CONFIG_EVENT_SET_M',NULL),
 (24,'CONFIG_UNIQUE_SET_M',NULL),
 (25,'CONFIG_ZONE_SET_M',NULL),
 (27,'CONFIG_ID_SET_M',NULL),
 (28,'CONFIG_OTHER_SET_M',NULL),
 (31,'CONFIG_SMS_GET_D',NULL),
 (32,'CONFIG_TEL_GET_D',NULL),
 (33,'CONFIG_EVENT_GET_D',NULL),
 (34,'CONFIG_UNIQUE_GET_D',NULL),
 (35,'CONFIG_ZONE_GET_D',NULL),
 (36,'DATA_GPS_GET_D',NULL),
 (37,'CONFIG_ID_GET_D',NULL),
 (38,'CONFIG_OTHER_GET_D',NULL),
 (41,'CONFIG_SMS_GET_M',NULL),
 (42,'CONFIG_TEL_GET_M',NULL),
 (43,'CONFIG_EVENT_GET_M',NULL),
 (44,'CONFIG_UNIQUE_GET_M',NULL),
 (45,'CONFIG_ZONE_GET_M',NULL),
 (46,'DATA_GPS_GET_M',NULL),
 (47,'CONFIG_ID_GET_M',NULL),
 (48,'CONFIG_OTHER_GET_M',NULL),
 (49,'AUTO_SEND_GET_M',NULL),
 (99,'ERROR_COMMAND','���������� ������������� �������� ���������'),
 (102,'MOBITEL_SERVICE_SEND',NULL),
 (103,'INTERNAL_1',NULL),
 (104,'INTERNAL_2',NULL),
 (105,'INTERNAL_3',NULL),
 (50,'CONFIG_GPRS_MAIN_SET_D',NULL),
 (51,'CONFIG_GPRS_EMAIL_SET_D',NULL),
 (52,'CONFIG_GPRS_SOCKET_SET_D',NULL),
 (53,'CONFIG_GPRS_FTP_SET_D',NULL),
 (60,'CONFIG_GPRS_MAIN_SET_M',NULL),
 (61,'CONFIG_GPRS_EMAIL_SET_M',NULL),
 (62,'CONFIG_GPRS_SOCKET_SET_M',NULL),
 (63,'CONFIG_GPRS_FTP_SET_M',NULL),
 (70,'CONFIG_GPRS_MAIN_GET_D',NULL),
 (71,'CONFIG_GPRS_EMAIL_GET_D',NULL),
 (72,'CONFIG_GPRS_SOCKET_GET_D',NULL),
 (73,'CONFIG_GPRS_FTP_GET_D',NULL),
 (80,'CONFIG_GPRS_MAIN_GET_M',NULL),
 (81,'CONFIG_GPRS_EMAIL_GET_M',NULL),
 (82,'CONFIG_GPRS_SOCKET_GET_M',NULL),
 (83,'CONFIG_GPRS_FTP_GET_M',NULL),
 (54,'CONFIG_GPRS_INIT_SET_D',NULL),
 (64,'CONFIG_GPRS_INIT_SET_M',NULL),
 (74,'CONFIG_GPRS_INIT_GET_D',NULL),
 (84,'CONFIG_GPRS_INIT_GET_M',NULL);
GO
--ok

INSERT INTO [dbo].[sensoralgorithms]
           ([Name]
           ,[Description]
           ,[AlgorithmID])
     VALUES
           ('������ ������������','��� ������� ������������ �������� ���. ����.',1),
 ('������ ��������� ���������','������ ������������ ��� ����������� � ���.',2),
 ('������� ������������','��� ������� ������������ �-�� ��������',3),
 ('����������','������ �������������� ���� ����',4),
 ('�������� ��������� ���������','��� ������� ������������ �-�� ��������',5),
 ('�����������','��� ������������� ��������',6),
 ('�������1','������� ������� � ����1',7),
 ('�������2','������� ������� � ����2',8),
 ('���������1','��� �������� ���������� ����������1',9),
 ('���������2','��� �������� ���������� ����������2',10),
 ('�����1','��� ����������1',11),
 ('�����2','��� ����������2',12),
 ('������������� ��������','��� ������������� ���������',13),
 ('������������� ���������� ����������','��� ������������� ��������� ���������',14),
 ('����������������','��� ������ ���������',15),
 ('�������� - ���������� ������','��� ����������� � ������ ��������',16),
 ('��������� ������������ - ���������� ������','��� �������� ��������� ������/�������',17),
 ('����������� - ���������� ��� X,','��� �������� ���� ������� �� ��� �',18),
 ('����������� - ���������� ��� Y,','��� �������� ���� ������� �� ��� Y',19),
 ('������������� ���������','��� ������������� �������� ���������� �������',20),
 ('������������� ������������� ��������','��� ������������� �����������������',21);
GO

--ok



