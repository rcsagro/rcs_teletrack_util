IF OBJECT_ID ( 'dbo.CheckDB', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.CheckDB;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.CheckDB
--SQL SECURITY INVOKER
--COMMENT '�������� �� ��� �����������.'
AS
BEGIN
  SELECT 'OK';
END
GO

IF OBJECT_ID ( 'dbo.CheckOdo', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.CheckOdo;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.CheckOdo
(
  @m_id     INTEGER,
  @end_time DATETIME
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT dbo.FROM_UNIXTIME(datagps.UnixTime) AS time
       , (datagps.Latitude / 600000.00000) AS Lat
       , (datagps.Longitude / 600000.00000) AS Lon
  FROM
    datagps
  WHERE
    datagps.Mobitel_ID = @m_id
    AND datagps.UnixTime BETWEEN (SELECT dbo.UNIX_TIMESTAMPS(odoTime)
                                  FROM
                                    vehicle
                                  WHERE
                                    Mobitel_id = @m_id) AND dbo.UNIX_TIMESTAMPS(@end_time)
    AND Valid = 1;
END
GO

IF OBJECT_ID ( 'dbo.dataview', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.dataview;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.dataview
(
  @m_id       INTEGER,
  @val        SMALLINT,
  @begin_time DATETIME,
  @end_time   DATETIME
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS time
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 2 ^ 8) + (Sensor6 * 2 ^ 16) + (Sensor5 * 2 ^ 24) + (Sensor4 * 2 ^ 32) + (Sensor3 * 2 ^ 40) + (Sensor2 * 2 ^ 48) + (Sensor1 * 2 ^ 56)) AS sensor
       , Events AS Events
       , LogID AS LogID
  FROM
    datagps
  WHERE
    Mobitel_ID = @m_id
    AND (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@begin_time) AND dbo.UNIX_TIMESTAMPS(@end_time))
    AND (Valid = @val)
    AND NOT (Latitude = 0
    AND Longitude = 0)
  ORDER BY
    UnixTime;
END
GO

IF OBJECT_ID ( 'dbo.DrListMobitelConfig', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.DrListMobitelConfig;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.DrListMobitelConfig
--SQL SECURITY INVOKER
--COMMENT '������� �������� ���������� � ������ DirectOnline'
AS
BEGIN
  SELECT m.Mobitel_id AS MobitelID
       , imc.DevIdShort AS DevIdShort
       , cge.Pop3un AS Login
       , cge.Pop3pw AS Password
  FROM
    Mobitels m
    JOIN ConfigMain cm
      ON m.Mobitel_id = cm.Mobitel_ID
    JOIN ConfigGprsEmail cge
      ON cm.ConfigGprsEmail_ID = cge.ID
    JOIN internalmobitelconfig imc
      ON m.InternalMobitelConfig_ID = imc.ID
  WHERE
    (cge.ConfigGprsEmail_ID = (SELECT max(intCge.ConfigGprsEmail_ID)
                               FROM
                                 ConfigGprsEmail intCge
                               WHERE
                                 intCge.ID = cge.ID))
    AND (imc.InternalMobitelConfig_ID = (SELECT max(intConf.InternalMobitelConfig_ID)
                                         FROM
                                           internalmobitelconfig intConf
                                         WHERE
                                           intConf.ID = imc.ID))
    AND (imc.devIdShort IS NOT NULL)
    AND (imc.devIdShort <> '')
    AND (cge.Pop3un IS NOT NULL)
    AND (cge.Pop3un <> '')
  ORDER BY
    m.Mobitel_ID;
END
GO

IF OBJECT_ID ( 'dbo.DrTransferBuffer', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.DrTransferBuffer;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.DrTransferBuffer
--SQL SECURITY INVOKER
--COMMENT '��������� GPS ������ DirectOnline �� �������� ������� � datagps'
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  --DECLARE @DataExists TINYINT;
  --SET @DataExists = 1; 

  DECLARE @TmpMaxUnixTime INT;

  DECLARE CursorMobitels CURSOR FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_dr;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  DELETE o
  FROM
    online o, datagpsbuffer_dr dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  --SET DataExists = 1;
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    INSERT INTO online(Mobitel_ID
                     , LogID
                     , UnixTime
                     , Latitude
                     , Longitude
                     , Altitude
                     , Direction
                     , Speed
                     , Valid
                     , Events
                     , Sensor1
                     , Sensor2
                     , Sensor3
                     , Sensor4
                     , Sensor5
                     , Sensor6
                     , Sensor7
                     , Sensor8
                     , Counter1
                     , Counter2
                     , Counter3
                     , Counter4
                     , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , Events
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_dr
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;


    DELETE
    FROM
      online
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        online
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END; --END WHILE;
  CLOSE CursorMobitels;

  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = b.LogID, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagps d, datagpsbuffer_dr b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  DELETE b
  FROM
    datagps d, datagpsbuffer_dr b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);


  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , Events
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
  FROM
    datagpsbuffer_dr;

  TRUNCATE TABLE datagpsbuffer_dr;
END

GO

IF OBJECT_ID ( 'dbo.ExecuteQuery', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.ExecuteQuery;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.ExecuteQuery
(
  @sqlQuery VARCHAR
)
--SQL SECURITY INVOKER
AS
BEGIN
  DECLARE @s VARCHAR;
  SET @s = @sqlQuery;
  --PREPARE stmt FROM @s;
  EXECUTE (@s);
--DEALLOCATE PREPARE stmt;
END
GO

IF OBJECT_ID ( 'dbo.hystoryOnline', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.hystoryOnline;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.hystoryOnline
(
  @id INTEGER
)
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT LogID
       , Mobitel_Id
       , dbo.FROM_UNIXTIME(unixtime) AS time
       , round((Latitude / 600000.00000), 6) AS Lat
       , round((Longitude / 600000.00000), 6) AS Lon
       , Altitude AS Altitude
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 2 ^ 8) + (Sensor6 * 2 ^ 16) + (Sensor5 * 2 ^ 24) + (Sensor4 * 2 ^ 32) + (Sensor3 * 2 ^ 40) + (Sensor2 * 2 ^ 48) + (Sensor1 * 2 ^ 56)) AS sensor
       , Events AS Events
       , DataGPS_ID AS 'DataGPS_ID'
  FROM
    online
  WHERE
    datagps_id > @id
  ORDER BY
    datagps_id DESC;
END
GO

IF OBJECT_ID ( 'dbo.IsTeletrackDatabase', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.IsTeletrackDatabase;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.IsTeletrackDatabase
--SQL SECURITY INVOKER
--COMMENT '�������� ���� ������ �� ������������ ��������� ������� Teletrack'
AS
BEGIN
  DECLARE @i TINYINT;
END
GO

IF OBJECT_ID ( 'dbo.new_proc', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.new_proc;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.new_proc
(
  @sensor     INTEGER,
  @m_id       INTEGER,
  @s_id       INTEGER,
  @begin_time DATETIME,
  @end_time   DATETIME
)
--SQL SECURITY INVOKER
--READS SQL DATA
AS
BEGIN
  SELECT round((datagps.Longitude / 600000), 6) AS lon
       , round((datagps.Latitude / 600000), 6) AS lat
       , datagps.Mobitel_ID AS Mobitel_ID
       , sensordata.value AS Value
       , (datagps.Speed * 1.852) AS speed
       , dbo.from_unixtime(datagps.UnixTime) AS time
       , sensordata.sensor_id AS sensor_id
       , datagps.DataGps_ID AS datagps_id
       , datagps.Sensor1 & @sensor AS sensor
  FROM
    (sensordata
    JOIN datagps
      ON ((sensordata.datagps_id = datagps.DataGps_ID)))
  WHERE
    (datagps.Valid = 1)
    AND (datagps.UnixTime BETWEEN dbo.UNIX_TIMESTAMPS(@begin_time) AND dbo.UNIX_TIMESTAMPS(@end_time))
    AND datagps.Mobitel_ID = @m_id
    AND sensordata.sensor_id = @s_id
  ORDER BY
    datagps.UnixTime
  , datagps.Mobitel_ID
  , sensordata.sensor_id;
END
GO

IF OBJECT_ID ( 'dbo.OnCorrectInfotrackLogId', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnCorrectInfotrackLogId;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnCorrectInfotrackLogId
AS
--SQL SECURITY INVOKER
--COMMENT '������������� �������� LogId � InMobitelID � ������ ����������'
BEGIN
  DECLARE @MobitelIDInCursor INT; /* ??????? ???????? MobitelID */
  DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  SET @DataExists = 1;
  DECLARE @MaxLogId INT; /* ???????????? LogId ??? ??????? ??????? */
  SET @MaxLogId = 0;
  DECLARE @SrvPacketIDInCursor BIGINT; /* ??????? ???????? ?????????? ?????? */
  SET @SrvPacketIDInCursor = 0;

  /* ?????? ?? ?????????? ? ????????? ????? ??????. 
     ??? ????????? ?????????? ? ????? I */
  DECLARE CursorInfoTrack CURSOR LOCAL FOR
  SELECT DISTINCT buf.Mobitel_ID
  FROM
    datagpsbuffer_on buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort LIKE 'I%')
    AND (buf.LogId = 0)
  ORDER BY
    1;

  /* ?????? ?? ?????? ????????? */
  DECLARE CursorInfoTrackUpdateSet CURSOR LOCAL FOR
  SELECT SrvPacketID
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelIDInCursor)
    AND (LogId = 0)
  ORDER BY
    UnixTime;

  /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* ??????? ???????????? LogId ??? ??????? ????????? */
    SET @MaxLogId = (SELECT coalesce(max(LogID), 0)
                     FROM
                       DataGps
                     WHERE
                       Mobitel_ID = @MobitelIDInCursor);

    /* ????????? LogId ? InMobitelID ? ?????? ????? ?????? ???????? ????????? */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      SET @MaxLogId = @MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET
        LogId = @MaxLogId, InMobitelID = @MaxLogId
      WHERE
        SrvPacketID = @SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    END;
    CLOSE CursorInfoTrackUpdateSet;
    --DEALLOCATE CursorInfoTrackUpdateSet;

    --SET DataExists = 1;
    FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  END;
  CLOSE CursorInfoTrack;
--DEALLOCATE CursorInfoTrack;
END
GO

IF OBJECT_ID ( 'dbo.OnDeleteDuplicates', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnDeleteDuplicates;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnDeleteDuplicates
AS
BEGIN
  /* ??????? ????????? ????? ?? ??????? datagpsbuffer_on. ????????? ??????
   ???????? ?????????? ???????? ? ????? Mobitel_Id + LogID. ?? ?????????
   ????????????? ??????? ? ??????? datagpsbuffer_on ????????? ?????? ???? - 
   ????????? ???????? ?????????????? ????????, ?.?. ? ???????????? SrvPacketID. */

  /* ??????? MobitelId ? ??????? CursorDuplicateGroups */
  DECLARE @MobitelIdInCursor INT;
  SET @MobitelIdInCursor = 0;
  /* ??????? LogID ? ??????? CursorDuplicateGroups */
  DECLARE @LogIdInCursor INT;
  SET @LogIdInCursor = 0;
  /* Max SrvPacketId ? ????????? ????? ??????? ?????? ?????????? */
  DECLARE @MaxSrvPacketId BIGINT;
  SET @MaxSrvPacketId = 0;
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /* ?????? ??? ??????? ?? ???? ??????? ?????????? */
  DECLARE CursorDuplicateGroups CURSOR LOCAL FOR
  SELECT Mobitel_Id
       , LogId
  FROM
    datagpsbuffer_on
  GROUP BY
    Mobitel_Id
  , LogId
  HAVING
    count(LogId) > 1;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  --SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SELECT @MaxSrvPacketId = max(SrvPacketID)
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor)
      AND (LogId = @LogIdInCursor);
    DELETE
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor) AND
      (LogId = @LogIdInCursor) AND
      (SrvPacketID < @MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  END;
  CLOSE CursorDuplicateGroups;
--DEALLOCATE CursorDuplicateGroups;
END

GO

IF OBJECT_ID ( 'dbo.OnDeleteLostRange', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnDeleteLostRange;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnDeleteLostRange
(
  @MobitelID        INTEGER,
  @BeginSrvPacketID BIGINT
)
--SQL SECURITY INVOKER
--COMMENT '�������� ���������� ��������� ��������� �� datagpslost_on.'
AS
BEGIN
  /* ?????????? ???????? ?? ??????, ??????????????? ??? ????????,
     ?????? ? ?????? ????????? ?????????. ???? ???????? - ?????
     ?????? ???????? ? ???????? ConfirmedID */

  DECLARE @MinBeginSrvPacketID BIGINT; -- ??????????? ???????? Begin_SrvPacketID ??? ??????? ?????????
  DECLARE @NewConfirmedID INT; -- ????? ???????? ConfirmedID  

  /* ?????????? ???????????? ???????? Begin_SrvPacketID */
  SELECT @MinBeginSrvPacketID = min(Begin_SrvPacketID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* ???? ???? ?????? ? ????????? ? ????????? - ????????? */
  IF @MinBeginSrvPacketID IS NOT NULL
  BEGIN
    /* ???? ??????? ?????? ?????? - ???????? ????? ???????? ConfirmedID */
    IF @MinBeginSrvPacketID = @BeginSrvPacketID
    BEGIN
      SELECT @NewConfirmedID = End_LogID
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (Begin_SrvPacketID = @BeginSrvPacketID);
    END; -- IF

    /* ???????? ?????? */
    DELETE
    FROM
      datagpslost_on
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (Begin_SrvPacketID = @BeginSrvPacketID);

    /* ???? ??????? ?????? ?????? - ??????? ConfirmedID */
    IF @MinBeginSrvPacketID = @BeginSrvPacketID
    BEGIN
      UPDATE mobitels
      SET
        ConfirmedID = @NewConfirmedID
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (@NewConfirmedID > ConfirmedID);
    END; -- IF
  END; -- IF  
END
GO

IF OBJECT_ID ( 'dbo.OnFullRefreshLostRanges', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnFullRefreshLostRanges;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnFullRefreshLostRanges
AS
--NOT DETERMINISTIC
--CONTAINS SQL
--SQL SECURITY INVOKER
--COMMENT '����� ���� ��������� ��� TDataManager'
BEGIN
  DECLARE @MobitelIDInCursor INT; /* ������� �������� MobitelID */
  --DECLARE @DataExists TINYINT; /* ���� ������� ������ ����� ����� */
  --SET @DataExists = 1;

  DECLARE @LogIdInCursor INT; /* ������� �������� LogId ��������� */
  DECLARE @SrvPacketIdInCursor BIGINT; /* ������� �������� SrvPacketID ��������� */
  DECLARE @PreviousLogId INT; /* �������� LogId ��������� �� ���������� ����*/
  DECLARE @PreviousSrvPacketId BIGINT; /* �������� SrvPacketID ��������� �� ���������� ���� */

  DECLARE @FirstIteration TINYINT; /* ���� ������ �������� �� ������ */
  SET @FirstIteration = 1;
  DECLARE @NewRowCount INT; /* ���������� ����� ������� � ��������� */
  SET @NewRowCount = 0;
  DECLARE @NewConfirmedID INT; /* ����� �������� ConfirmedID */

  DECLARE @MaxNegativeDataGpsId INT; /* ������������ DataGpsId � ��������� ������������� LogId */
  DECLARE @MaxPositiveDataGpsId INT; /* ������������ DataGpsId � ��������� ������������� LogId */
  DECLARE @InsertLostDataStatement NVARCHAR;

  /* ������ �� ���� ���������� */
  DECLARE CursorMobitels CURSOR FOR
  SELECT DISTINCT (Mobitel_ID) AS Mobitel_ID
  FROM
    mobitels
  ORDER BY
    Mobitel_ID;

  /* ������ �� ������ ��������� */
  DECLARE CursorDataGps CURSOR FOR
  SELECT LogId
       , SrvPacketID
  FROM
    DataGps
  WHERE
    Mobitel_ID = @MobitelIDInCursor
  ORDER BY
    LogId;

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* ������� ������, ��������� ������ ��������� ��� TDataManager  */
  TRUNCATE TABLE datagpsbuffer_on;
  TRUNCATE TABLE datagpslost_on;
  TRUNCATE TABLE datagpslost_ontmp;

  /* ------------------- FETCH Mobitels ------------------------ */
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* ��������� ��� ������� �������  */
    SET @InsertLostDataStatement = 'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';

    /* ----------------- FETCH DataGPS --------------------- */
    SET @FirstIteration = 1;
    SET @NewRowCount = 0;

    OPEN CursorDataGps;
    FETCH CursorDataGps INTO @LogIdInCursor, @SrvPacketIdInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstIteration = 1
      BEGIN
        /* ������ �������� � ������ */
        SET @PreviousLogId = @LogIdInCursor;
        SET @PreviousSrvPacketId = @SrvPacketIdInCursor;
        SET @FirstIteration = 0;
      END
      ELSE /* �������� ����� �������*/
      BEGIN
        IF (((@LogIdInCursor - @PreviousLogId) > 1) AND (@PreviousSrvPacketId < @SrvPacketIdInCursor))
        BEGIN
          /* ������ ������ */
          IF @NewRowCount > 0
          BEGIN
            SET @InsertLostDataStatement = @InsertLostDataStatement + ',';
          END; --END IF;

          SET @InsertLostDataStatement = @InsertLostDataStatement + ' (' + @MobitelIDInCursor + ', ' + @PreviousLogId + ', ' + @LogIdInCursor + ', ' + @PreviousSrvPacketId + ', ' + @SrvPacketIdInCursor + ')';

          SET @NewRowCount = @NewRowCount + 1;
        END; --END IF;

        /* ���������� ������� �������� � ���������� */
        SET @PreviousLogId = @LogIdInCursor;
        SET @PreviousSrvPacketId = @SrvPacketIdInCursor;
      END; --END IF; -- IF @FirstIteration = 1 

      FETCH CursorDataGps INTO @LogIdInCursor, @SrvPacketIdInCursor;
    END; --END WHILE; --  WHILE DataExistsInner = 1 
    CLOSE CursorDataGps;

    /* ���� � ������� ��������� ���� ����������� ������ - 
       ����� ConfirmedID ����� ����� ������������ Begin_LogID 
       �� ������� ���������. 
       ����� - ������������� LogID ������� ��������� �� 
       ������� datagps � ����� �� ���������� ������ ���� ��� 
       ������ ����, � ����������� �� ������� LogId. */
    IF @NewRowCount > 0
    BEGIN
      /* ���������� ������� ����������� ������ */
      EXEC (@InsertLostDataStatement);

      SET @NewConfirmedID = (SELECT min(Begin_LogID)
                             FROM
                               datagpslost_on
                             WHERE
                               Mobitel_ID = @MobitelIDInCursor);
    END
    ELSE
    BEGIN
      /* � ���� ������ �� ��� ��� �������.
         ���� ���� ������������� �������� LogID, �� ��� �����������
         ������ �������� ConfirmedID ��������� �������������� ������:
         ��������� ����� �������� LogID ������������� ��� �������������
         ����� ����� (�� DataGpsId). */
      IF EXISTS (SELECT TOP 1 1
                 FROM
                   dbo.datagps
                 WHERE
                   (Mobitel_ID = @MobitelIDInCursor)
                   AND (LogId < 0))
      BEGIN
        SET @MaxNegativeDataGpsId = (SELECT max(DataGps_ID)
                                     FROM
                                       DataGps
                                     WHERE
                                       (Mobitel_ID = @MobitelIDInCursor)
                                       AND (LogId < 0));

        SET @MaxPositiveDataGpsId = (SELECT coalesce(max(DataGps_ID), 0)
                                     FROM
                                       DataGps
                                     WHERE
                                       (Mobitel_ID = @MobitelIDInCursor)
                                       AND (LogId >= 0));

        IF (@MaxNegativeDataGpsId > @MaxPositiveDataGpsId)
        BEGIN
          SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                                 FROM
                                   datagps
                                 WHERE
                                   (Mobitel_ID = @MobitelIDInCursor)
                                   AND (LogId < 0));
        END
        ELSE
        BEGIN
          SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                                 FROM
                                   datagps
                                 WHERE
                                   (Mobitel_ID = @MobitelIDInCursor)
                                   AND (LogId >= 0));
        END --END IF; -- IF (@MaxNegativeDataGpsId > @MaxPositiveDataGpsId)
      END
      ELSE
      BEGIN
        /* ���������� ������ ������������� LogId */
        SET @NewConfirmedID = (SELECT coalesce(max(LogID), 0)
                               FROM
                                 datagps
                               WHERE
                                 Mobitel_ID = @MobitelIDInCursor);
      END; --END IF; -- IF EXISTS 
    END; --END IF; -- IF @NewRowCount > 0

    /* ������� ConfirmedID */
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    --SET DataExists = 1;
    FETCH CursorMobitels INTO @MobitelIDInCursor;
  END; --END WHILE;
  CLOSE CursorMobitels;
END;
GO

IF OBJECT_ID ( 'dbo.OnInsertDatagpsLost', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnInsertDatagpsLost;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnInsertDatagpsLost
(
  @MobitelID        INT,
  @BeginLogID       INT,
  @EndLogID         INT,
  @BeginSrvPacketID BIGINT,
  @EndSrvPacketID   BIGINT
)
AS
--SQL SECURITY INVOKER
--COMMENT '������� ������ � ������� datagpslost_on.'
BEGIN
  /* ???? ????????????? ?????? ? ????????? Mobitel_ID ? Begin_LogID
     ? ??????? datagpslost_on */
  DECLARE @MobitelId_BeginLogID_Exists TINYINT;
  SET @MobitelId_BeginLogID_Exists = 0;
  /* ???? ????????????? ?????? ? ????????? Mobitel_ID ? Begin_SrvPacketID
     ? ??????? datagpslost_on  */
  DECLARE @MobitelId_BeginSrvPacketID_Exists TINYINT;
  SET @MobitelId_BeginSrvPacketID_Exists = 0;

  DECLARE @RowBeginSrvPacketID BIGINT;

  IF EXISTS (SELECT NULL
             FROM
               datagpslost_on
             WHERE
               (Mobitel_ID = @MobitelID)
               AND (Begin_LogID = @BeginLogID))
  BEGIN
    SET @MobitelId_BeginLogID_Exists = 1;
  END; -- IF

  IF EXISTS (SELECT NULL
             FROM
               datagpslost_on
             WHERE
               (Mobitel_ID = @MobitelID)
               AND (Begin_SrvPacketID = @BeginSrvPacketID))
  BEGIN
    SET @MobitelId_BeginSrvPacketID_Exists = 1;
  END; -- IF

  IF (@MobitelId_BeginLogID_Exists = 0) AND (@MobitelId_BeginSrvPacketID_Exists = 0)
  BEGIN
    /* ??????? ????? ?????? */
    INSERT INTO datagpslost_on(Mobitel_ID
                             , Begin_LogID
                             , End_LogID
                             , Begin_SrvPacketID
                             , End_SrvPacketID)
    VALUES
      (@MobitelID, @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID);
  END
  ELSE
  IF ((@MobitelId_BeginLogID_Exists = 1) AND (@MobitelId_BeginSrvPacketID_Exists = 1))
  BEGIN
    /* ?????????? ???????????? ?????? */
    UPDATE datagpslost_on
    SET
      End_LogID = @EndLogID, End_SrvPacketID = @EndSrvPacketID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (Begin_LogID = @BeginLogID);
  END
  ELSE
  BEGIN
    /* ?????????? MobitelId_BeginLogID_Exists = 0, MobitelId_BeginSrvPacketID_Exists = 1
         ?????????? - ??? ?????????????? ????????, ??????? ?? ?????? ?????????. */
    SET @RowBeginSrvPacketID = (SELECT TOP 1 Begin_SrvPacketID
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (Begin_LogID = @BeginLogID));

    IF ((@MobitelId_BeginLogID_Exists = 1) AND (@MobitelId_BeginSrvPacketID_Exists = 0) AND (@BeginSrvPacketID < @RowBeginSrvPacketID))
    BEGIN
      /* ? ??? ????? ????????, ???? ???????? ?????? ???? ? ?? ?? ?????? ????????? ??? 
           (LogID ? ??????? ???? ? ?????, ? SrvPacketID ????????) */
      UPDATE datagpslost_on
      SET
        End_LogID = @EndLogID, Begin_SrvPacketID = @BeginSrvPacketID, End_SrvPacketID = @EndSrvPacketID
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);
    END; -- IF
  END; -- ELSE
END
--END

GO

IF OBJECT_ID ( 'dbo.online_table', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.online_table;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.online_table
--SQL SECURITY INVOKER
AS
BEGIN
  SELECT Log_ID
       , MobitelId
       , (SELECT TOP 1 DataGPS_ID
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS 'DataGPS_ID'
       , (SELECT TOP 1 dbo.from_unixtime(unixtime)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS 'time'
       , (SELECT TOP 1 round((Latitude / 600000.00000), 6)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lat
       , (SELECT TOP 1 round((Longitude / 600000.00000), 6)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Lon
       , (SELECT TOP 1 Altitude
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Altitude
       , (SELECT TOP 1 (Speed * 1.852)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS speed
       , (SELECT TOP 1 ((Direction * 360) / 255)
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS direction
       , (SELECT TOP 1 Valid
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Valid
       , (SELECT TOP 1 (((((((Sensor8 + (Sensor7 * 2 ^ 8)) + (Sensor6 * 2 ^ 16)) + (Sensor5 * 2 ^ 24)) + (Sensor4 * 2 ^ 32)) + (Sensor3 * 2 ^ 40)) + (Sensor2 * 2 ^ 48)) + (Sensor1 * 2 ^ 56))
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS sensor
       , (SELECT TOP 1 Events
          FROM
            online
          WHERE
            mobitel_id = MobitelId
            AND LogId = Log_ID) AS Events

  FROM
    (SELECT max(LogID) AS Log_ID
          , mobitel_id AS MobitelId
     FROM
       online
     GROUP BY
       Mobitel_ID) T1;
END
GO

IF OBJECT_ID ( 'dbo.OnListMobitelConfig', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnListMobitelConfig;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnListMobitelConfig
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  DECLARE @DevIdShortInCursor CHAR(4);
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /*  */
  DECLARE CursorMobitels CURSOR LOCAL FOR
  SELECT m.Mobitel_ID AS MobitelID
       , c.devIdShort AS DevIdShort
  FROM
    mobitels m
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort IS NOT NULL)
    AND (c.devIdShort <> '')
  ORDER BY
    m.Mobitel_ID;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /*  */
  CREATE TABLE tmpMobitelsConfig(
    MobitelID INT NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL
  );

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    INSERT INTO tmpMobitelsConfig
    SELECT @MobitelIDInCursor
         , @DevIdShortInCursor
         , (SELECT coalesce(max(SrvPacketID), 0)
            FROM
              datagps
            WHERE
              Mobitel_ID = @MobitelIDInCursor) AS LastPacketID;

    FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
  END;
  CLOSE CursorMobitels;
  --DEALLOCATE CursorMobitels;

  SELECT MobitelID
       , DevIdShort
       , LastPacketID
  FROM
    tmpMobitelsConfig;

  DROP TABLE tmpMobitelsConfig;
END

GO

IF OBJECT_ID ( 'dbo.OnLostDataGPS', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnLostDataGPS;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnLostDataGPS
(
  @MobitelID    INT,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT '����� ��������� � online ������ ��������� ��������� � ����������'
BEGIN

  /********** DECLARE **********/

  DECLARE @CurrentConfirmedID INT; /* ??????? ???????? ConfirmedID */
  SET @CurrentConfirmedID = 0;
  DECLARE @NewConfirmedID INT; /* ????? ???????? ConfirmedID */
  SET @NewConfirmedID = 0;
  DECLARE @StartLogIDSelect INT; /* min LogID ??? ??????? */
  SET @StartLogIDSelect = 0;
  DECLARE @FinishLogIDSelect INT; /* max LogID ??? ??????? */
  SET @FinishLogIDSelect = 0;
  DECLARE @MaxEndLogID INT; /* max End_LogID ? ??????????? ?????????? ??????? ????????? */
  SET @MaxEndLogID = 0;

  DECLARE @MinLogID_n INT; /* ??????????? LogID ????? ConfirmedID ? ????????? ????? ?????? */
  SET @MinLogID_n = 0;
  DECLARE @MaxLogID_n INT; /* ???????????? LogID ? ????????? ????? ?????? */
  SET @MaxLogID_n = 0;

  DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  SET @DataExists = 1;

  DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID */
  DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID */
  DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
  DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
  DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

  DECLARE @tmpSrvPacketID BIGINT; /* ???????? SrvPacketID ??? ?????? ????????????? ?????? ? ??????? ????? ??????, ?? ?????????????? ? ????????????? */
  SET @tmpSrvPacketID = 0;

  /* ?????? ??? ??????? ?? ????? LogID */
  DECLARE CursorNewLogID CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp;

  /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  SET @CurrentConfirmedID = (SELECT coalesce(ConfirmedID, 0)
                             FROM
                               mobitels
                             WHERE
                               Mobitel_ID = @MobitelID);

  /* ??????????? LogID ? ????? ?????? ?????? c ??????? ????? ???????? */
  SELECT @MinLogID_n = min(LogID)
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelID)
    AND (LogID > @CurrentConfirmedID);

  /* ???????????? LogID ? ????? ?????? ?????? c ??????? ????? ???????? */
  SET @MaxLogID_n = (SELECT max(LogID)
                     FROM
                       datagpsbuffer_on
                     WHERE
                       Mobitel_ID = @MobitelID);

  /* ???????????? End_LogID ? ??????????? ?????????? ??????? ????????? */
  SELECT @MaxEndLogID = coalesce(max(End_LogID), 0)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* ???????? ???? ?? ?????? ??? ??????? */
  IF @MinLogID_n IS NOT NULL
  BEGIN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF @MinLogID_n < @MaxEndLogID
    BEGIN
      /* ????  MinLogID_n < MaxEndLogID ?????? ?????? ??????, 
       ??????? ???????? ??????? ????? ?????????. */

      /* ??????? ????????? LogID ??? ?????????. */
      SET @StartLogIDSelect = (SELECT coalesce(Begin_LogID, @MinLogID_n)
                               FROM
                                 datagpslost_on
                               WHERE
                                 (Mobitel_ID = @MobitelID)
                                 AND (@MinLogID_n > Begin_LogID)
                                 AND (@MinLogID_n < End_LogID));

      /* ??????? ????????? LogID ??? ?????????. */
      SET @FinishLogIDSelect = (SELECT coalesce(End_LogID, @MaxLogID_n)
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (@MaxLogID_n > Begin_LogID)
                                  AND (@MaxLogID_n < End_LogID));

      /* ?????? ?????? ?????? ?? ??????? ??????????? ??????? 
         (????? ?????????????, ???? ???????????) */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID >= @StartLogIDSelect) AND
        (Begin_LogID <= @FinishLogIDSelect);

      /* ???? ?????? ?????? LogID = 0 ? ???? ?????? ??? ? DataGps, ?????
         ???? ???????? ?????? ????????? ?????? ??? ???????. */
      IF (@StartLogIDSelect = 0) AND (NOT EXISTS (SELECT 1
                                                  FROM
                                                    datagps
                                                  WHERE
                                                    (Mobitel_ID = @MobitelID)
                                                    AND (LogID = @StartLogIDSelect)))
      BEGIN
        INSERT INTO datagpslost_ontmp(LogID
                                    , SrvPacketID)
        VALUES
          (0, 0);
      END;
    END;
    ELSE
    BEGIN

      /* ?????? ????? ??????. ???? ???????? ??? ??????? ????????????? ?????? - 
         ?????? ??????? */

      /* ??????? ????????? LogID ??? ?????????. */
      SELECT @StartLogIDSelect = coalesce(max(End_LogID), @CurrentConfirmedID)
      FROM
        datagpslost_on
      WHERE
        Mobitel_ID = @MobitelID;

      /* ??????? ????????? LogID ??? ?????????. */
      SELECT @FinishLogIDSelect = max(LogID)
      FROM
        datagpsbuffer_on
      WHERE
        Mobitel_ID = @MobitelID;

      /* ???????? ?? datagps ??????????????? ???????? SrvPacketID  ??? ??????
????????????? ?????? */
      SELECT @tmpSrvPacketID = coalesce(SrvPacketID, 0)
      FROM
        datagps
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (LogID = @StartLogIDSelect);

      /* ???????? ? ??????? datagpslost_ontmp ??? ??????? ????????? 
?????????????? ?????? ?????? */
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@StartLogIDSelect, @tmpSrvPacketID);
    END; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

    /* ?????????? ????????? ??????? ?????????? ??????? ?????????
??? ??????? ???????? */
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT LogID
         , SrvPacketID
    FROM
      datagps
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (LogID >= @StartLogIDSelect)
      AND (LogID <= @FinishLogIDSelect)
    ORDER BY
      LogID;

    /* ???????????? ???????-??????? ? ??????? ??????????? ?????????? */
    --SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstLogID = -9999999
      BEGIN
        /* ?????? ???????? ? ?????? */
        SET @FirstLogID = @NewLogID;
        SET @FirstSrvPacketID = @NewSrvPacketID;
      END
      ELSE
      BEGIN
        /* ???????? ????? ???????*/
        SET @SecondLogID = @NewLogID;
        SET @SecondSrvPacketID = @NewSrvPacketID;

        IF (@SecondLogID - @FirstLogID) > 1
        BEGIN
          /* ?????? ?????? */
          EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

          IF @NeedOptimize = 0
          BEGIN
            SET @NeedOptimize = 1;
          END; -- IF

        END; -- IF
        /* ?????????? ??????? ?????? ???????? ?? ?????? ???????
??? ??????? ? ????????? ???????? ? ???????? ?????????? ???????? */
        SET @FirstLogID = @SecondLogID;
        SET @FirstSrvPacketID = @SecondSrvPacketID;
      END; -- IF
      FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    END; -- WHILE
    CLOSE CursorNewLogID;
    --DEALLOCATE CursorNewLogID; 

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

    /* ???? ??????????? ??????? ? ??????? ????????? ??? - 
????? ConfirmedID ????? ????? ????????????? LogID 
??????? ?????????. ????? - ??????????? Begin_LogID
?? ??????? ????????? */
    SET @NewConfirmedID = (SELECT min(Begin_LogID)
                           FROM
                             datagpslost_on
                           WHERE
                             Mobitel_ID = @MobitelID);

    IF @NewConfirmedID IS NULL
    BEGIN
      SET @NewConfirmedID = (SELECT max(LogID)
                             FROM
                               datagps
                             WHERE
                               Mobitel_ID = @MobitelID);
    END;

    /* ??????? ConfirmedID ???? ???? */
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (ConfirmedID < @NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END; -- IF MaxEndLogID IS NOT NULL  
END


GO

IF OBJECT_ID ( 'dbo.OnLostDataGPS2', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnLostDataGPS2;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnLostDataGPS2
(
  @MobitelID    INTEGER,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT '����������� ���������� ���������. ������ ����������� ������ ����'
BEGIN
  /********** DECLARE **********/

  DECLARE @NewConfirmedID INT;
  SET @NewConfirmedID = 0; /* ????? ???????? ConfirmedID */
  DECLARE @BeginLogID INT; /* Begin_LogID ? ??????? CursorLostRanges */
  DECLARE @EndLogID INT; /* End_LogID ? ??????? CursorLostRanges */
  DECLARE @BeginSrvPacketID BIGINT; /* Begin_SrvPacketID ? ??????? CursorLostRanges */
  DECLARE @EndSrvPacketID BIGINT; /* End_SrvPacketID ? ??????? CursorLostRanges */
  DECLARE @RowCountForAnalyze INT; /* ?????????? ??????? ??? ??????? */
  SET @RowCountForAnalyze = 0;
  --DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  --SET @DataExists = 1;
  DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
  DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
  DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
  DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
  DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

  /* ?????? ??? ??????? ?? ?????????? ????????? ????????? */
  DECLARE CursorLostRanges CURSOR LOCAL FOR
  SELECT Begin_LogID
       , End_LogID
       , Begin_SrvPacketID
       , End_SrvPacketID
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* ?????? ??? ??????? ?? ????? LogID */
  DECLARE CursorNewLogID2 CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp
  ORDER BY
    SrvPacketID;

  /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* ?????????? ????????? ? ????????? ??????? ???? ??? ???????? */
  --SET DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;

  WHILE @@fetch_status = 0
  BEGIN
    /* ?????????? ????????? ??????? ???????????? ??????? ?????????
       ??? ??????? ???????? */
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT DISTINCT LogID
                  , SrvPacketID
    FROM
      datagpsbuffer_ontmp
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (SrvPacketID > @BeginSrvPacketID)
      AND (SrvPacketID < @EndSrvPacketID)
    ORDER BY
      SrvPacketID;

    /* ???? ???? ?????? ???????????? ???????? */
    SET @RowCountForAnalyze = (SELECT count(1)
                               FROM
                                 datagpslost_ontmp);

    IF @RowCountForAnalyze > 0
    BEGIN
      /* ????????? ?????? ? ????????? ?????? ??? ??????? */
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@BeginLogID, @BeginSrvPacketID)
      , (@EndLogID, @EndSrvPacketID);

      /* ??????? ?????? ???????? */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        --DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
        --DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
        --DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --SET @FirstLogID = -9999999;
        --DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
        --DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

        /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
        --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* ???????????? ???????-??????? ? ??????? ??????????? ?????????? */
        --SET DataExists = 1;
        OPEN CursorNewLogID2;
        FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;

        WHILE @@fetch_status = 0
        BEGIN
          IF @FirstLogID = -9999999
          BEGIN
            /* ?????? ???????? ? ?????? */
            SET @FirstLogID = @NewLogID;
            SET @FirstSrvPacketID = @NewSrvPacketID;
          END
          ELSE
          BEGIN
            /* ???????? ????? ???????*/
            SET @SecondLogID = @NewLogID;
            SET @SecondSrvPacketID = @NewSrvPacketID;

            IF (@SecondSrvPacketID - @FirstSrvPacketID) > 1
            BEGIN
              /* ?????? ?????? */
              EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

              IF @NeedOptimize = 0
              BEGIN
                SET @NeedOptimize = 1;
              END; -- IF

            END; -- IF
            /* ?????????? ??????? ?????? ???????? ?? ?????? ???????
               ??? ??????? ? ????????? ???????? ? ???????? ?????????? ???????? */
            SET @FirstLogID = @SecondLogID;
            SET @FirstSrvPacketID = @SecondSrvPacketID;
          END; -- ELSE  
          FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;
        END; -- WHILE;
        CLOSE CursorNewLogID2;
      --DEALLOCATE CursorNewLogID2;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END;

    FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;
  END;
  CLOSE CursorLostRanges;
  --DEALLOCATE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* ???? ??????????? ??????? ? ??????? ????????? ??? - 
     ????? ConfirmedID ????? ????? ????????????? LogID 
     ??????? ?????????. ????? - ??????????? Begin_LogID
     ?? ??????? ????????? */
  SELECT @NewConfirmedID = min(Begin_LogID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @NewConfirmedID IS NULL
  BEGIN
    SET @NewConfirmedID = (SELECT max(LogID)
                           FROM
                             datagps
                           WHERE
                             Mobitel_ID = @MobitelID);
  END;

  /* ??????? ConfirmedID ???? ???? */
  UPDATE mobitels
  SET
    ConfirmedID = @NewConfirmedID
  WHERE
    (Mobitel_ID = @MobitelID) AND
    (ConfirmedID < @NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END

GO

IF OBJECT_ID ( 'dbo.OnTransferBuffer', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.OnTransferBuffer;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnTransferBuffer
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  --DECLARE @DataExists TINYINT; /*  */
  --SET @DataExists = 1;
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC OnDeleteDuplicates
  EXEC OnCorrectInfotrackLogId

  /* OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp(Mobitel_ID
                                , LogID
                                , SrvPacketID)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.SrvPacketID
  FROM
    datagpsbuffer_on b
    LEFT JOIN datagps d
      ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  WHERE
    d.LogID IS NOT NULL;

  /* --------------------  Online ---------------------- */
  /*  online  datagpsbuffer_on */
  /*  Mobitel_ID LogID */
  DELETE o
  FROM
    online o, datagpsbuffer_on dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
    INSERT INTO dbo.online(Mobitel_ID
                         , LogID
                         , UnixTime
                         , Latitude
                         , Longitude
                         , Altitude
                         , Direction
                         , Speed
                         , Valid
                         , Events
                         , Sensor1
                         , Sensor2
                         , Sensor3
                         , Sensor4
                         , Sensor5
                         , Sensor6
                         , Sensor7
                         , Sensor8
                         , Counter1
                         , Counter2
                         , Counter3
                         , Counter4
                         , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , Events
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_on
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;

    /*  100 */
    DELETE
    FROM
      online
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        online
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  /* .. */
  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = b.LogID, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4, SrvPacketID = b.SrvPacketID
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID) AND
    (d.SrvPacketID < b.SrvPacketID);

  /*  */
  DELETE b
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  /* */
  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs
                    , SrvPacketID)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , Events
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
       , SrvPacketID
  FROM
    datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);*/

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS2 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = CREATEST(@NeedOptimize, @NeedOptimizeParam); */

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsUpdateSet;
  --DEALLOCATE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  /*  */
  IF @NeedOptimize = 1
    ALTER INDEX ALL ON datagpslost_on REORGANIZE
END


GO

IF OBJECT_ID ( 'dbo.tmpAddIndex', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.tmpAddIndex;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.tmpAddIndex
AS
--NOT DETERMINISTIC
--CONTAINS SQL
--SQL SECURITY INVOKER
--COMMENT '��������� ��������� ���������� ������� � DataGpsBuffer_on'
BEGIN
  DECLARE @DbName VARCHAR(64); /* ������������ ������� �� */
  DECLARE @IndexExist TINYINT;
  SET @IndexExist = 0;

  SET @DbName = (SELECT db_name(@@procid));

  SET @IndexExist = (SELECT count(1)
                     FROM
                       sys.indexes i
                     WHERE
                       i.name = 'IDX_SrvPacketID');

  IF @IndexExist = 0
  BEGIN
    CREATE INDEX IDX_SrvPacketID ON DataGpsBuffer_on (SrvPacketID);
    ALTER TABLE DataGpsBuffer_on ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON);
  END;
END

--EXEC tmpAddIndex
--DROP PROCEDURE IF EXISTS tmpAddIndex$$


GO

IF OBJECT_ID ( 'dbo.TransferBuffer', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.TransferBuffer;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.TransferBuffer
--SQL SECURITY INVOKER
--COMMENT '��������� GPS ������ �� �������� ������� � datagps.'
AS
BEGIN

  --CREATE TEMPORARY TABLE IF NOT EXISTS temp1 ENGINE = HEAP

  IF object_id('tempdb..#temp1') IS NOT NULL
    DROP TABLE #temp1;

  CREATE TABLE #temp1(
    DataGps_ID INT
  );
  INSERT INTO #temp1
  SELECT DataGPS_ID
  FROM
    datagpsbuffer;

  UPDATE datagps
  SET
    Message_ID = 0, Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = NULL, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, isShow = 0, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.LogID = b.LogID;

  DELETE b
  FROM
    datagps d, datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.LogID = b.LogID;

  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.Events
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID
  GROUP BY
    b.Mobitel_ID
  , b.LogID;


  DELETE b
  FROM
    datagpsbuffer b, #temp1 t
  WHERE
    b.DataGps_ID = t.DataGps_ID;

  DROP TABLE #temp1
END
GO

IF OBJECT_ID ( 'dbo.TransferLiteBuffer', 'P' ) IS NOT NULL 
    DROP PROCEDURE dbo.TransferLiteBuffer;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.TransferLiteBuffer
--SQL SECURITY INVOKER
--COMMENT '��������� GPS ������ �� �������� ������� � datagps.'
AS
BEGIN
  IF object_id('tempdb..#temp2') IS NOT NULL
    DROP TABLE #temp2;

  CREATE TABLE #temp2(
    DataGps_ID INT
  );
  INSERT INTO #temp2
  SELECT DataGPS_ID
  FROM
    datagpsbufferlite;

  UPDATE datagps
  SET
    Message_ID = b.Message_ID, Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = NULL, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, isShow = b.IsShow, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4
  FROM
    datagpsbuffer b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    Mobitel_ID = b.Mobitel_ID AND
    Latitude = b.Latitude AND
    Longitude = b.Longitude AND
    Altitude = b.Altitude AND
    UnixTime = b.UnixTime;

  DELETE b
  FROM
    datagps d, datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID AND
    d.Mobitel_ID = b.Mobitel_ID AND
    d.Latitude = b.Latitude AND
    d.Longitude = b.Longitude AND
    d.Altitude = b.Altitude AND
    d.UnixTime = b.UnixTime;

  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.UnixTime
       , b.Latitude
       , b.Longitude
       , b.Altitude
       , b.Direction
       , b.Speed
       , b.Valid
       , b.Events
       , b.Sensor1
       , b.Sensor2
       , b.Sensor3
       , b.Sensor4
       , b.Sensor5
       , b.Sensor6
       , b.Sensor7
       , b.Sensor8
       , b.Counter1
       , b.Counter2
       , b.Counter3
       , b.Counter4
       , b.whatIs
  FROM
    datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID
  GROUP BY
    b.Mobitel_ID
  , b.LogID
  , b.Latitude
  , b.Longitude
  , b.Altitude
  , b.UnixTime;

  DELETE b
  FROM
    datagpsbufferlite b, #temp2 t
  WHERE
    b.DataGps_ID = t.DataGps_ID;

  DROP TABLE #temp2
END
GO
