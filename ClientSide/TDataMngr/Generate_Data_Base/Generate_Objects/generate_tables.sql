/****** Object:  ForeignKey [fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_agro_agregat_vehicle_vehicle_id]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_vehicle_id]
GO
/****** Object:  ForeignKey [fgn_key_FK_agro_datagps_agro_ordert_Id]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_datagps_agro_ordert_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_datagps]'))
ALTER TABLE [dbo].[agro_datagps] DROP CONSTRAINT [fgn_key_FK_agro_datagps_agro_ordert_Id]
GO
/****** Object:  ForeignKey [fgn_key_Id_culture_FK2]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_culture_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] DROP CONSTRAINT [fgn_key_Id_culture_FK2]
GO
/****** Object:  ForeignKey [fgn_key_Id_main_FieldCulture_FK1]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FieldCulture_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] DROP CONSTRAINT [fgn_key_Id_main_FieldCulture_FK1]
GO
/****** Object:  ForeignKey [fgn_key_Id_mobitel_Order_FK1]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_order]'))
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [fgn_key_Id_mobitel_Order_FK1]
GO
/****** Object:  ForeignKey [fgn_key_Id_main_Order_FK1]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert]'))
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [fgn_key_Id_main_Order_FK1]
GO
/****** Object:  ForeignKey [fgn_key_FK_agro_ordert_control_agro_order_Id]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_ordert_control_agro_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]'))
ALTER TABLE [dbo].[agro_ordert_control] DROP CONSTRAINT [fgn_key_FK_agro_ordert_control_agro_order_Id]
GO
/****** Object:  ForeignKey [fgn_key_Id_agregat_Price_FK5]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_agregat_Price_FK5]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_agregat_Price_FK5]
GO
/****** Object:  ForeignKey [fgn_key_Id_main_FK1]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_main_FK1]
GO
/****** Object:  ForeignKey [fgn_key_Id_mobitel_FK3]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_FK3]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_mobitel_FK3]
GO
/****** Object:  ForeignKey [fgn_key_Id_unit_FK4]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_unit_FK4]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_unit_FK4]
GO
/****** Object:  ForeignKey [fgn_key_Id_work_FK2]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_work_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_work_FK2]
GO
/****** Object:  ForeignKey [fgn_key_FK_agro_work_agro_work_types_Id]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_work_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [fgn_key_FK_agro_work_agro_work_types_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_agro_work_agro_workgroup_Id]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_workgroup_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [fgn_key_FK_agro_work_agro_workgroup_Id]
GO
/****** Object:  ForeignKey [fgn_key_lines_FK1]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_lines_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[lines]'))
ALTER TABLE [dbo].[lines] DROP CONSTRAINT [fgn_key_lines_FK1]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_loading_time_md_cargo_Id]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [fgn_key_FK_md_loading_time_md_cargo_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_loading_time_md_object_Id]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [fgn_key_FK_md_loading_time_md_object_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_object_md_object_group_Id]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_md_object_group_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [fgn_key_FK_md_object_md_object_group_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_object_zones_Zone_ID]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_zones_Zone_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [fgn_key_FK_md_object_zones_Zone_ID]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_object_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_object_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_objectStart_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_objectStart_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_objectStart_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_order_category_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_order_category_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_order_priority_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_priority_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_order_priority_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_transportation_types_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_transportation_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_transportation_types_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_waybill_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_waybill_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_waybill_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_job_md_object_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_job_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order_job]'))
ALTER TABLE [dbo].[md_order_job] DROP CONSTRAINT [fgn_key_FK_md_order_job_md_object_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_ordert_md_cargo_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_cargo_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_ordert_md_cargoUnload_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargoUnload_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_cargoUnload_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_ordert_md_object_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_object_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_ordert_md_order_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_order_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_route_md_object_from]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_route_md_object_from]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_route]'))
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [fgn_key_FK_md_route_md_object_from]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_waybill_driver_id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_driver_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_driver_id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_waybill_md_enterprise_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_enterprise_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_md_enterprise_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_waybill_md_working_shift_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_working_shift_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_md_working_shift_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_waybill_vehicle_id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_vehicle_id]
GO
/****** Object:  ForeignKey [fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]'))
ALTER TABLE [dbo].[mobitels_rotate_bands] DROP CONSTRAINT [fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]
GO
/****** Object:  ForeignKey [fgn_key_FK_ntf_email_ntf_main_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_email_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_emails]'))
ALTER TABLE [dbo].[ntf_emails] DROP CONSTRAINT [fgn_key_FK_ntf_email_ntf_main_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_ntf_events_ntf_main_Id]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_events_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_events]'))
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [fgn_key_FK_ntf_events_ntf_main_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_ntf_log_ntf_main_Id]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_log_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_log]'))
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [fgn_key_FK_ntf_log_ntf_main_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_ntf_mobitels_ntf_main_Id]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_mobitels_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]'))
ALTER TABLE [dbo].[ntf_mobitels] DROP CONSTRAINT [fgn_key_FK_ntf_mobitels_ntf_main_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_points_zones]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_points_zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[points]'))
ALTER TABLE [dbo].[points] DROP CONSTRAINT [fgn_key_FK_points_zones]
GO
/****** Object:  ForeignKey [FK_RPZ_Zones]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPZ_Zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[report_pass_zones]'))
ALTER TABLE [dbo].[report_pass_zones] DROP CONSTRAINT [FK_RPZ_Zones]
GO
/****** Object:  ForeignKey [fgn_key_rt_route_fuel_FK1]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_fuel_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_fuel]'))
ALTER TABLE [dbo].[rt_route_fuel] DROP CONSTRAINT [fgn_key_rt_route_fuel_FK1]
GO
/****** Object:  ForeignKey [fgn_key_rt_route_sensors_FK1]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_sensors_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]'))
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [fgn_key_rt_route_sensors_FK1]
GO
/****** Object:  ForeignKey [fgn_key_rt_route_stops_FK1]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_stops_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_stops]'))
ALTER TABLE [dbo].[rt_route_stops] DROP CONSTRAINT [fgn_key_rt_route_stops_FK1]
GO
/****** Object:  ForeignKey [fgn_key_rt_routet_FK1]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_routet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_routet]'))
ALTER TABLE [dbo].[rt_routet] DROP CONSTRAINT [fgn_key_rt_routet_FK1]
GO
/****** Object:  ForeignKey [fgn_key_rt_samplet_FK1]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_samplet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_samplet]'))
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [fgn_key_rt_samplet_FK1]
GO
/****** Object:  ForeignKey [fgn_key_FK_users_acs_objects_users_acs_roles_Id]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_users_acs_objects_users_acs_roles_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[users_acs_objects]'))
ALTER TABLE [dbo].[users_acs_objects] DROP CONSTRAINT [fgn_key_FK_users_acs_objects_users_acs_roles_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_vehicle_mobitels_Mobitel_ID]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_mobitels_Mobitel_ID]
GO
/****** Object:  ForeignKey [fgn_key_FK_vehicle_team_id]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_team_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_team_id]
GO
/****** Object:  ForeignKey [fgn_key_FK_vehicle_vehicle_category_Id]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_vehicle_category_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_vehicle_vehicle_state_Id]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_state_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_vehicle_state_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_zones_zonesgroup]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_zones_zonesgroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[zones]'))
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [fgn_key_FK_zones_zonesgroup]
GO
/****** Object:  Table [dbo].[md_ordert]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_cargo_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargoUnload_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_cargoUnload_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_object_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_order_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__IdObj__0EC32C7A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__IdObj__0EC32C7A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__IdCar__0FB750B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__IdCar__0FB750B3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__QtyLo__10AB74EC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__QtyLo__10AB74EC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__TimeL__119F9925]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__TimeL__119F9925]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__IdCar__1293BD5E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__IdCar__1293BD5E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__QtyUn__1387E197]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__QtyUn__1387E197]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__TimeU__147C05D0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__TimeU__147C05D0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__TimeD__15702A09]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__TimeD__15702A09]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__TimeT__16644E42]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__TimeT__16644E42]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__Dista__1758727B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__Dista__1758727B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__IsClo__184C96B4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__IsClo__184C96B4]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND type in (N'U'))
DROP TABLE [dbo].[md_ordert]
GO
/****** Object:  Table [dbo].[md_route]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_route_md_object_from]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_route]'))
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [fgn_key_FK_md_route_md_object_from]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_route__Id_obj__1A34DF26]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [DF__md_route__Id_obj__1A34DF26]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_route__Id_obj__1B29035F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [DF__md_route__Id_obj__1B29035F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_route__Distan__1C1D2798]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [DF__md_route__Distan__1C1D2798]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_route__Time__1D114BD1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [DF__md_route__Time__1D114BD1]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_route]') AND type in (N'U'))
DROP TABLE [dbo].[md_route]
GO
/****** Object:  Table [dbo].[md_order]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_object_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_objectStart_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_objectStart_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_order_category_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_priority_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_order_priority_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_transportation_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_transportation_types_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_waybill_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_waybill_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdObje__7F80E8EA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdObje__7F80E8EA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdObje__00750D23]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdObje__00750D23]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdPrio__0169315C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdPrio__0169315C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdCate__025D5595]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdCate__025D5595]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdTran__035179CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdTran__035179CE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdWayB__04459E07]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdWayB__04459E07]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IsClos__0539C240]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IsClos__0539C240]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND type in (N'U'))
DROP TABLE [dbo].[md_order]
GO
/****** Object:  Table [dbo].[md_order_job]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_job_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order_job]'))
ALTER TABLE [dbo].[md_order_job] DROP CONSTRAINT [fgn_key_FK_md_order_job_md_object_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order___IdObj__090A5324]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order_job] DROP CONSTRAINT [DF__md_order___IdObj__090A5324]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order___IdOrd__09FE775D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order_job] DROP CONSTRAINT [DF__md_order___IdOrd__09FE775D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order___IdWay__0AF29B96]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order_job] DROP CONSTRAINT [DF__md_order___IdWay__0AF29B96]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order_job]') AND type in (N'U'))
DROP TABLE [dbo].[md_order_job]
GO
/****** Object:  Table [dbo].[agro_datagps]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_datagps_agro_ordert_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_datagps]'))
ALTER TABLE [dbo].[agro_datagps] DROP CONSTRAINT [fgn_key_FK_agro_datagps_agro_ordert_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_data__Lon_d__0BC6C43E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_datagps] DROP CONSTRAINT [DF__agro_data__Lon_d__0BC6C43E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_data__Lat_d__0CBAE877]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_datagps] DROP CONSTRAINT [DF__agro_data__Lat_d__0CBAE877]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_data__Speed__0DAF0CB0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_datagps] DROP CONSTRAINT [DF__agro_data__Speed__0DAF0CB0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_datagps]') AND type in (N'U'))
DROP TABLE [dbo].[agro_datagps]
GO
/****** Object:  Table [dbo].[md_loading_time]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [fgn_key_FK_md_loading_time_md_cargo_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [fgn_key_FK_md_loading_time_md_object_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_loadin__IdObj__75035A77]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [DF__md_loadin__IdObj__75035A77]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_loadin__IdCar__75F77EB0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [DF__md_loadin__IdCar__75F77EB0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_loading_time]') AND type in (N'U'))
DROP TABLE [dbo].[md_loading_time]
GO
/****** Object:  Table [dbo].[md_object]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_md_object_group_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [fgn_key_FK_md_object_md_object_group_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_zones_Zone_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [fgn_key_FK_md_object_zones_Zone_ID]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object__Id_gr__77DFC722]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [DF__md_object__Id_gr__77DFC722]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object__Name__78D3EB5B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [DF__md_object__Name__78D3EB5B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object__Numbe__79C80F94]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [DF__md_object__Numbe__79C80F94]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object__Numbe__7ABC33CD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [DF__md_object__Numbe__7ABC33CD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object__Delay__7BB05806]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [DF__md_object__Delay__7BB05806]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_object]') AND type in (N'U'))
DROP TABLE [dbo].[md_object]
GO
/****** Object:  Table [dbo].[agro_agregat_vehicle]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_vehicle_id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Id_ag__060DEAE8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [DF__agro_agre__Id_ag__060DEAE8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Id_ve__07020F21]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [DF__agro_agre__Id_ve__07020F21]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Id_se__07F6335A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [DF__agro_agre__Id_se__07F6335A]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]') AND type in (N'U'))
DROP TABLE [dbo].[agro_agregat_vehicle]
GO
/****** Object:  Table [dbo].[agro_ordert]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert]'))
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [fgn_key_Id_main_Order_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_ma__2D27B809]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_ma__2D27B809]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_fi__2E1BDC42]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_fi__2E1BDC42]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_zo__2F10007B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_zo__2F10007B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_dr__300424B4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_dr__300424B4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_wo__30F848ED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_wo__30F848ED]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Dista__31EC6D26]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Dista__31EC6D26]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FactS__32E0915F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FactS__32E0915F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FactS__33D4B598]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FactS__33D4B598]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Price__34C8D9D1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Price__34C8D9D1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_ordert__Sum__35BCFE0A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_ordert__Sum__35BCFE0A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Speed__36B12243]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Speed__36B12243]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelS__37A5467C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelS__37A5467C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelA__38996AB5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelA__38996AB5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelS__398D8EEE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelS__398D8EEE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelE__3A81B327]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelE__3A81B327]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelE__3B75D760]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelE__3B75D760]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelE__3C69FB99]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelE__3C69FB99]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelE__3D5E1FD2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelE__3D5E1FD2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Fuel___3E52440B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Fuel___3E52440B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Fuel___3F466844]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Fuel___3F466844]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Fuel___403A8C7D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Fuel___403A8C7D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Fuel___412EB0B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Fuel___412EB0B6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Fuel___4222D4EF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Fuel___4222D4EF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_ag__4316F928]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_ag__4316F928]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Confi__440B1D61]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Confi__440B1D61]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Point__44FF419A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Point__44FF419A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__LockR__45F365D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__LockR__45F365D3]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert]') AND type in (N'U'))
DROP TABLE [dbo].[agro_ordert]
GO
/****** Object:  Table [dbo].[agro_ordert_control]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_ordert_control_agro_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]'))
ALTER TABLE [dbo].[agro_ordert_control] DROP CONSTRAINT [fgn_key_FK_agro_ordert_control_agro_order_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_ma__47DBAE45]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert_control] DROP CONSTRAINT [DF__agro_orde__Id_ma__47DBAE45]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_ob__48CFD27E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert_control] DROP CONSTRAINT [DF__agro_orde__Id_ob__48CFD27E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Type___49C3F6B7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert_control] DROP CONSTRAINT [DF__agro_orde__Type___49C3F6B7]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]') AND type in (N'U'))
DROP TABLE [dbo].[agro_ordert_control]
GO
/****** Object:  Table [dbo].[agro_pricet]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_agregat_Price_FK5]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_agregat_Price_FK5]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_main_FK1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_FK3]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_mobitel_FK3]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_unit_FK4]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_unit_FK4]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_work_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_work_FK2]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_pric__Price__4E88ABD4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [DF__agro_pric__Price__4E88ABD4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_pric__Id_ag__4F7CD00D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [DF__agro_pric__Id_ag__4F7CD00D]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND type in (N'U'))
DROP TABLE [dbo].[agro_pricet]
GO
/****** Object:  Table [dbo].[points]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_points_zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[points]'))
ALTER TABLE [dbo].[points] DROP CONSTRAINT [fgn_key_FK_points_zones]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__points__ID__7A8729A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[points] DROP CONSTRAINT [DF__points__ID__7A8729A3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__points__Latitude__7B7B4DDC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[points] DROP CONSTRAINT [DF__points__Latitude__7B7B4DDC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__points__Longitud__7C6F7215]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[points] DROP CONSTRAINT [DF__points__Longitud__7C6F7215]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__points__Zone_ID__7D63964E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[points] DROP CONSTRAINT [DF__points__Zone_ID__7D63964E]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[points]') AND type in (N'U'))
DROP TABLE [dbo].[points]
GO
/****** Object:  Table [dbo].[md_waybill]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_driver_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_driver_id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_enterprise_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_md_enterprise_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_working_shift_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_md_working_shift_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_vehicle_id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_waybil__IdDri__1FEDB87C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [DF__md_waybil__IdDri__1FEDB87C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_waybil__IdVeh__20E1DCB5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [DF__md_waybil__IdVeh__20E1DCB5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_waybil__IdShi__21D600EE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [DF__md_waybil__IdShi__21D600EE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_waybil__IsClo__22CA2527]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [DF__md_waybil__IsClo__22CA2527]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND type in (N'U'))
DROP TABLE [dbo].[md_waybill]
GO
/****** Object:  Table [dbo].[report_pass_zones]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPZ_Zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[report_pass_zones]'))
ALTER TABLE [dbo].[report_pass_zones] DROP CONSTRAINT [FK_RPZ_Zones]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[report_pass_zones]') AND type in (N'U'))
DROP TABLE [dbo].[report_pass_zones]
GO
/****** Object:  Table [dbo].[mobitels_rotate_bands]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]'))
ALTER TABLE [dbo].[mobitels_rotate_bands] DROP CONSTRAINT [fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels___Mobit__4336F4B9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels_rotate_bands] DROP CONSTRAINT [DF__mobitels___Mobit__4336F4B9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels___Bound__442B18F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels_rotate_bands] DROP CONSTRAINT [DF__mobitels___Bound__442B18F2]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]') AND type in (N'U'))
DROP TABLE [dbo].[mobitels_rotate_bands]
GO
/****** Object:  Table [dbo].[ntf_emails]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_email_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_emails]'))
ALTER TABLE [dbo].[ntf_emails] DROP CONSTRAINT [fgn_key_FK_ntf_email_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_email__IsAct__5D16C24D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_emails] DROP CONSTRAINT [DF__ntf_email__IsAct__5D16C24D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_email__Email__5E0AE686]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_emails] DROP CONSTRAINT [DF__ntf_email__Email__5E0AE686]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_emails]') AND type in (N'U'))
DROP TABLE [dbo].[ntf_emails]
GO
/****** Object:  Table [dbo].[ntf_events]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_events_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_events]'))
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [fgn_key_FK_ntf_events_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_event__Event__48EFCE0F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [DF__ntf_event__Event__48EFCE0F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_event__ParIn__49E3F248]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [DF__ntf_event__ParIn__49E3F248]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_event__ParDb__4AD81681]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [DF__ntf_event__ParDb__4AD81681]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_event__ParDb__4BCC3ABA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [DF__ntf_event__ParDb__4BCC3ABA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_event__ParBo__4CC05EF3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [DF__ntf_event__ParBo__4CC05EF3]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_events]') AND type in (N'U'))
DROP TABLE [dbo].[ntf_events]
GO
/****** Object:  Table [dbo].[ntf_log]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_log_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_log]'))
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [fgn_key_FK_ntf_log_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Mobitel__4EA8A765]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Mobitel__4EA8A765]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Zone_id__4F9CCB9E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Zone_id__4F9CCB9E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Lat__5090EFD7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Lat__5090EFD7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Lng__51851410]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Lng__51851410]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__IsRead__52793849]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__IsRead__52793849]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__DataGps__536D5C82]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__DataGps__536D5C82]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Speed__546180BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Speed__546180BB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Value__5555A4F4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Value__5555A4F4]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_log]') AND type in (N'U'))
DROP TABLE [dbo].[ntf_log]
GO
/****** Object:  Table [dbo].[rt_routet]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_routet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_routet]'))
ALTER TABLE [dbo].[rt_routet] DROP CONSTRAINT [fgn_key_rt_routet_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_routet__Id_zo__2C1E8537]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_routet] DROP CONSTRAINT [DF__rt_routet__Id_zo__2C1E8537]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_routet__Id_ev__2D12A970]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_routet] DROP CONSTRAINT [DF__rt_routet__Id_ev__2D12A970]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_routet__Dista__2E06CDA9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_routet] DROP CONSTRAINT [DF__rt_routet__Dista__2E06CDA9]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_routet]') AND type in (N'U'))
DROP TABLE [dbo].[rt_routet]
GO
/****** Object:  Table [dbo].[users_acs_objects]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_users_acs_objects_users_acs_roles_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[users_acs_objects]'))
ALTER TABLE [dbo].[users_acs_objects] DROP CONSTRAINT [fgn_key_FK_users_acs_objects_users_acs_roles_Id]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_acs_objects]') AND type in (N'U'))
DROP TABLE [dbo].[users_acs_objects]
GO
/****** Object:  Table [dbo].[ntf_mobitels]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_mobitels_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]'))
ALTER TABLE [dbo].[ntf_mobitels] DROP CONSTRAINT [fgn_key_FK_ntf_mobitels_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_mobit__Senso__5CF6C6BC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_mobitels] DROP CONSTRAINT [DF__ntf_mobit__Senso__5CF6C6BC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_mobit__Senso__5DEAEAF5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_mobitels] DROP CONSTRAINT [DF__ntf_mobit__Senso__5DEAEAF5]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]') AND type in (N'U'))
DROP TABLE [dbo].[ntf_mobitels]
GO
/****** Object:  Table [dbo].[rt_route_fuel]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_fuel_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_fuel]'))
ALTER TABLE [dbo].[rt_route_fuel] DROP CONSTRAINT [fgn_key_rt_route_fuel_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___Locat__1FB8AE52]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_fuel] DROP CONSTRAINT [DF__rt_route___Locat__1FB8AE52]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___dValu__20ACD28B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_fuel] DROP CONSTRAINT [DF__rt_route___dValu__20ACD28B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___dValu__21A0F6C4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_fuel] DROP CONSTRAINT [DF__rt_route___dValu__21A0F6C4]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_fuel]') AND type in (N'U'))
DROP TABLE [dbo].[rt_route_fuel]
GO
/****** Object:  Table [dbo].[rt_route_sensors]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_sensors_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]'))
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [fgn_key_rt_route_sensors_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___Senso__23893F36]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [DF__rt_route___Senso__23893F36]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___Locat__247D636F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [DF__rt_route___Locat__247D636F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___Speed__257187A8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [DF__rt_route___Speed__257187A8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___Dista__2665ABE1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [DF__rt_route___Dista__2665ABE1]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]') AND type in (N'U'))
DROP TABLE [dbo].[rt_route_sensors]
GO
/****** Object:  Table [dbo].[rt_route_stops]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_stops_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_stops]'))
ALTER TABLE [dbo].[rt_route_stops] DROP CONSTRAINT [fgn_key_rt_route_stops_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___Check__284DF453]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_stops] DROP CONSTRAINT [DF__rt_route___Check__284DF453]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_stops]') AND type in (N'U'))
DROP TABLE [dbo].[rt_route_stops]
GO
/****** Object:  Table [dbo].[rt_samplet]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_samplet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_samplet]'))
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [fgn_key_rt_samplet_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__TimeR__369C13AA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [DF__rt_sample__TimeR__369C13AA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__TimeR__379037E3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [DF__rt_sample__TimeR__379037E3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__TimeR__38845C1C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [DF__rt_sample__TimeR__38845C1C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__TimeA__39788055]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [DF__rt_sample__TimeA__39788055]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_samplet]') AND type in (N'U'))
DROP TABLE [dbo].[rt_samplet]
GO
/****** Object:  Table [dbo].[agro_order]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_order]'))
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [fgn_key_Id_mobitel_Order_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_mo__1BFD2C07]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Id_mo__1BFD2C07]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Regim__1CF15040]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Regim__1CF15040]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Dista__1DE57479]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Dista__1DE57479]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__PathW__1ED998B2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__PathW__1ED998B2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__1FCDBCEB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__1FCDBCEB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__20C1E124]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__20C1E124]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__21B6055D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__21B6055D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__22AA2996]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__22AA2996]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__239E4DCF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__239E4DCF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__24927208]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__24927208]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__25869641]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__25869641]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__267ABA7A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__267ABA7A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FactS__276EDEB3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FactS__276EDEB3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Point__286302EC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Point__286302EC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Point__29572725]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Point__29572725]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__State__2A4B4B5E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__State__2A4B4B5E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Block__2B3F6F97]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Block__2B3F6F97]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_order]') AND type in (N'U'))
DROP TABLE [dbo].[agro_order]
GO
/****** Object:  Table [dbo].[agro_fieldculture]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_culture_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] DROP CONSTRAINT [fgn_key_Id_culture_FK2]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FieldCulture_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] DROP CONSTRAINT [fgn_key_Id_main_FieldCulture_FK1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]') AND type in (N'U'))
DROP TABLE [dbo].[agro_fieldculture]
GO
/****** Object:  Table [dbo].[lines]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_lines_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[lines]'))
ALTER TABLE [dbo].[lines] DROP CONSTRAINT [fgn_key_lines_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lines__Color__351DDF8C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lines] DROP CONSTRAINT [DF__lines__Color__351DDF8C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lines__Width__361203C5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lines] DROP CONSTRAINT [DF__lines__Width__361203C5]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lines]') AND type in (N'U'))
DROP TABLE [dbo].[lines]
GO
/****** Object:  Table [dbo].[agro_work]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_work_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [fgn_key_FK_agro_work_agro_work_types_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_workgroup_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [fgn_key_FK_agro_work_agro_workgroup_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_work__Name__5535A963]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [DF__agro_work__Name__5535A963]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_work__Speed__5629CD9C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [DF__agro_work__Speed__5629CD9C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_work__Speed__571DF1D5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [DF__agro_work__Speed__571DF1D5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_work__TypeW__5812160E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [DF__agro_work__TypeW__5812160E]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_work]') AND type in (N'U'))
DROP TABLE [dbo].[agro_work]
GO
/****** Object:  Table [dbo].[vehicle]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_mobitels_Mobitel_ID]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_team_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_team_id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_vehicle_category_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_state_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_vehicle_state_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle__Odometr__47919582]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [DF__vehicle__Odometr__47919582]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle__setting__4885B9BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [DF__vehicle__setting__4885B9BB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle__driver___4979DDF4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [DF__vehicle__driver___4979DDF4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle__State__4A6E022D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [DF__vehicle__State__4A6E022D]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND type in (N'U'))
DROP TABLE [dbo].[vehicle]
GO
/****** Object:  Table [dbo].[zones]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_zones_zonesgroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[zones]'))
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [fgn_key_FK_zones_zonesgroup]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__ID__5C8CB268]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__ID__5C8CB268]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__Status__5D80D6A1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__Status__5D80D6A1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__Square__5E74FADA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__Square__5E74FADA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__ZoneColor__5F691F13]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__ZoneColor__5F691F13]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__TextColor__605D434C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__TextColor__605D434C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__TextFontN__61516785]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__TextFontN__61516785]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__TextFontS__62458BBE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__TextFontS__62458BBE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__TextFontP__6339AFF7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__TextFontP__6339AFF7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__HatchStyl__642DD430]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__HatchStyl__642DD430]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__Id_main__6521F869]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__Id_main__6521F869]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__Level__66161CA2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__Level__66161CA2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__StyleId__670A40DB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__StyleId__670A40DB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__ZonesGrou__67FE6514]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__ZonesGrou__67FE6514]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zones]') AND type in (N'U'))
DROP TABLE [dbo].[zones]
GO
/****** Object:  Table [dbo].[zonesgroup]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonesgrou__Title__69E6AD86]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonesgroup] DROP CONSTRAINT [DF__zonesgrou__Title__69E6AD86]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonesgrou__Descr__6ADAD1BF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonesgroup] DROP CONSTRAINT [DF__zonesgrou__Descr__6ADAD1BF]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zonesgroup]') AND type in (N'U'))
DROP TABLE [dbo].[zonesgroup]
GO
/****** Object:  Table [dbo].[vehicle_category]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_ca__Name__4C564A9F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_category] DROP CONSTRAINT [DF__vehicle_ca__Name__4C564A9F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_c__Tonna__4D4A6ED8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_category] DROP CONSTRAINT [DF__vehicle_c__Tonna__4D4A6ED8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_c__FuelN__4E3E9311]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_category] DROP CONSTRAINT [DF__vehicle_c__FuelN__4E3E9311]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_c__FuelN__4F32B74A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_category] DROP CONSTRAINT [DF__vehicle_c__FuelN__4F32B74A]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle_category]') AND type in (N'U'))
DROP TABLE [dbo].[vehicle_category]
GO
/****** Object:  Table [dbo].[vehicle_state]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_st__Name__511AFFBC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_state] DROP CONSTRAINT [DF__vehicle_st__Name__511AFFBC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_s__MaxTi__520F23F5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_state] DROP CONSTRAINT [DF__vehicle_s__MaxTi__520F23F5]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle_state]') AND type in (N'U'))
DROP TABLE [dbo].[vehicle_state]
GO
/****** Object:  Table [dbo].[ver]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ver__Num__53F76C67]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ver] DROP CONSTRAINT [DF__ver__Num__53F76C67]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ver]') AND type in (N'U'))
DROP TABLE [dbo].[ver]
GO
/****** Object:  Table [dbo].[zonerelations]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__Zone___55DFB4D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__Zone___55DFB4D9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__Confi__56D3D912]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__Confi__56D3D912]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__In_fl__57C7FD4B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__In_fl__57C7FD4B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__Out_f__58BC2184]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__Out_f__58BC2184]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__In_fl__59B045BD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__In_fl__59B045BD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__Out_f__5AA469F6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__Out_f__5AA469F6]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zonerelations]') AND type in (N'U'))
DROP TABLE [dbo].[zonerelations]
GO
/****** Object:  Table [dbo].[agro_work_types]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_work___Name__59FA5E80]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_work_types] DROP CONSTRAINT [DF__agro_work___Name__59FA5E80]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_work_types]') AND type in (N'U'))
DROP TABLE [dbo].[agro_work_types]
GO
/****** Object:  Table [dbo].[agro_workgroup]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_workg__Name__5BE2A6F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_workgroup] DROP CONSTRAINT [DF__agro_workg__Name__5BE2A6F2]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_workgroup]') AND type in (N'U'))
DROP TABLE [dbo].[agro_workgroup]
GO
/****** Object:  Table [dbo].[commands]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__commands__ID__5DCAEF64]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[commands] DROP CONSTRAINT [DF__commands__ID__5DCAEF64]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[commands]') AND type in (N'U'))
DROP TABLE [dbo].[commands]
GO
/****** Object:  Table [dbo].[configdrivermessage]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configdriver__ID__5FB337D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configdrivermessage] DROP CONSTRAINT [DF__configdriver__ID__5FB337D6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configdri__Flags__60A75C0F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configdrivermessage] DROP CONSTRAINT [DF__configdri__Flags__60A75C0F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configdri__Messa__619B8048]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configdrivermessage] DROP CONSTRAINT [DF__configdri__Messa__619B8048]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configdrivermessage]') AND type in (N'U'))
DROP TABLE [dbo].[configdrivermessage]
GO
/****** Object:  Table [dbo].[configevent]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configevent__ID__6383C8BA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configevent__ID__6383C8BA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__Flags__6477ECF3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__Flags__6477ECF3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__Messa__656C112C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__Messa__656C112C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__tmr1L__66603565]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__tmr1L__66603565]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__tmr2S__6754599E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__tmr2S__6754599E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__tmr3Z__68487DD7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__tmr3Z__68487DD7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__dist1__693CA210]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__dist1__693CA210]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__dist2__6A30C649]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__dist2__6A30C649]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__dist3__6B24EA82]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__dist3__6B24EA82]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__delta__6C190EBB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__delta__6C190EBB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__gprsE__6D0D32F4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__gprsE__6D0D32F4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__gprsF__6E01572D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__gprsF__6E01572D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__gprsS__6EF57B66]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__gprsS__6EF57B66]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__6FE99F9F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__6FE99F9F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__70DDC3D8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__70DDC3D8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__71D1E811]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__71D1E811]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__72C60C4A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__72C60C4A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__73BA3083]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__73BA3083]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__74AE54BC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__74AE54BC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__75A278F5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__75A278F5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__76969D2E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__76969D2E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__778AC167]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__778AC167]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__787EE5A0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__787EE5A0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__797309D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__797309D9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7A672E12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7A672E12]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7B5B524B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7B5B524B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7C4F7684]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7C4F7684]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7D439ABD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7D439ABD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7E37BEF6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7E37BEF6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7F2BE32F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7F2BE32F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__00200768]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__00200768]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__01142BA1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__01142BA1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__02084FDA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__02084FDA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__02FC7413]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__02FC7413]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__03F0984C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__03F0984C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__04E4BC85]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__04E4BC85]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__05D8E0BE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__05D8E0BE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__06CD04F7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__06CD04F7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__07C12930]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__07C12930]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__08B54D69]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__08B54D69]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__09A971A2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__09A971A2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__0A9D95DB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__0A9D95DB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__0B91BA14]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__0B91BA14]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__0C85DE4D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__0C85DE4D]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configevent]') AND type in (N'U'))
DROP TABLE [dbo].[configevent]
GO
/****** Object:  Table [dbo].[configgprsemail]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgprsem__ID__0E6E26BF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgprsem__ID__0E6E26BF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Flags__0F624AF8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Flags__0F624AF8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Messa__10566F31]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Messa__10566F31]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Smtps__114A936A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Smtps__114A936A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Smtpu__123EB7A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Smtpu__123EB7A3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Smtpp__1332DBDC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Smtpp__1332DBDC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Pop3s__14270015]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Pop3s__14270015]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Pop3u__151B244E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Pop3u__151B244E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Pop3p__160F4887]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Pop3p__160F4887]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsemail]') AND type in (N'U'))
DROP TABLE [dbo].[configgprsemail]
GO
/****** Object:  Table [dbo].[configgprsinit]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgprsin__ID__17F790F9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsinit] DROP CONSTRAINT [DF__configgprsin__ID__17F790F9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Flags__18EBB532]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsinit] DROP CONSTRAINT [DF__configgpr__Flags__18EBB532]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Messa__19DFD96B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsinit] DROP CONSTRAINT [DF__configgpr__Messa__19DFD96B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Domai__1AD3FDA4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsinit] DROP CONSTRAINT [DF__configgpr__Domai__1AD3FDA4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__InitS__1BC821DD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsinit] DROP CONSTRAINT [DF__configgpr__InitS__1BC821DD]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsinit]') AND type in (N'U'))
DROP TABLE [dbo].[configgprsinit]
GO
/****** Object:  Table [dbo].[configgprsmain]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgprsma__ID__1DB06A4F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgprsma__ID__1DB06A4F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Flags__1EA48E88]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Flags__1EA48E88]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Messa__1F98B2C1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Messa__1F98B2C1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgprs__Mode__208CD6FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgprs__Mode__208CD6FA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Apnse__2180FB33]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Apnse__2180FB33]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Apnun__22751F6C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Apnun__22751F6C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Apnpw__236943A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Apnpw__236943A5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Dnsse__245D67DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Dnsse__245D67DE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Dialn__25518C17]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Dialn__25518C17]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Ispun__2645B050]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Ispun__2645B050]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Isppw__2739D489]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Isppw__2739D489]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsmain]') AND type in (N'U'))
DROP TABLE [dbo].[configgprsmain]
GO
/****** Object:  Table [dbo].[configmain]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmain__ID__29221CFB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmain__ID__29221CFB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Flags__2A164134]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Flags__2A164134]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2B0A656D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2B0A656D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2BFE89A6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2BFE89A6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2CF2ADDF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2CF2ADDF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2DE6D218]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2DE6D218]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2EDAF651]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2EDAF651]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2FCF1A8A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2FCF1A8A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__30C33EC3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__30C33EC3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__31B762FC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__31B762FC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Mobit__32AB8735]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Mobit__32AB8735]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__339FAB6E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__339FAB6E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__3493CFA7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__3493CFA7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__3587F3E0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__3587F3E0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configmain]') AND type in (N'U'))
DROP TABLE [dbo].[configmain]
GO
/****** Object:  Table [dbo].[configother]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configother__ID__37703C52]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configother] DROP CONSTRAINT [DF__configother__ID__37703C52]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configoth__Flags__3864608B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configother] DROP CONSTRAINT [DF__configoth__Flags__3864608B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configother__Pin__395884C4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configother] DROP CONSTRAINT [DF__configother__Pin__395884C4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configothe__Lang__3A4CA8FD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configother] DROP CONSTRAINT [DF__configothe__Lang__3A4CA8FD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configoth__Messa__3B40CD36]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configother] DROP CONSTRAINT [DF__configoth__Messa__3B40CD36]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configother]') AND type in (N'U'))
DROP TABLE [dbo].[configother]
GO
/****** Object:  Table [dbo].[configsensorset]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsensor__ID__3D2915A8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsensorset] DROP CONSTRAINT [DF__configsensor__ID__3D2915A8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsen__Flags__3E1D39E1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsensorset] DROP CONSTRAINT [DF__configsen__Flags__3E1D39E1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsen__Messa__3F115E1A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsensorset] DROP CONSTRAINT [DF__configsen__Messa__3F115E1A]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configsensorset]') AND type in (N'U'))
DROP TABLE [dbo].[configsensorset]
GO
/****** Object:  Table [dbo].[configsms]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__ID__40F9A68C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__ID__40F9A68C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__Flags__41EDCAC5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__Flags__41EDCAC5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__Messa__42E1EEFE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__Messa__42E1EEFE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__smsCe__43D61337]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__smsCe__43D61337]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__smsDs__44CA3770]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__smsDs__44CA3770]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__smsEm__45BE5BA9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__smsEm__45BE5BA9]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configsms]') AND type in (N'U'))
DROP TABLE [dbo].[configsms]
GO
/****** Object:  Table [dbo].[configtel]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__ID__47A6A41B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__ID__47A6A41B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__Flags__489AC854]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__Flags__489AC854]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__Messa__498EEC8D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__Messa__498EEC8D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telSO__4A8310C6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telSO__4A8310C6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telDs__4B7734FF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telDs__4B7734FF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telAc__4C6B5938]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telAc__4C6B5938]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telAc__4D5F7D71]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telAc__4D5F7D71]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telAc__4E53A1AA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telAc__4E53A1AA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telAn__4F47C5E3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telAn__4F47C5E3]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configtel]') AND type in (N'U'))
DROP TABLE [dbo].[configtel]
GO
/****** Object:  Table [dbo].[configunique]    Script Date: 04/11/2013 10:56:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configunique__ID__51300E55]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configunique] DROP CONSTRAINT [DF__configunique__ID__51300E55]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configuni__Flags__5224328E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configunique] DROP CONSTRAINT [DF__configuni__Flags__5224328E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configuni__Messa__531856C7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configunique] DROP CONSTRAINT [DF__configuni__Messa__531856C7]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configunique]') AND type in (N'U'))
DROP TABLE [dbo].[configunique]
GO
/****** Object:  Table [dbo].[configzoneset]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configzonese__ID__55009F39]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configzoneset] DROP CONSTRAINT [DF__configzonese__ID__55009F39]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configzon__Flags__55F4C372]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configzoneset] DROP CONSTRAINT [DF__configzon__Flags__55F4C372]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configzon__Messa__56E8E7AB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configzoneset] DROP CONSTRAINT [DF__configzon__Messa__56E8E7AB]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configzoneset]') AND type in (N'U'))
DROP TABLE [dbo].[configzoneset]
GO
/****** Object:  Table [dbo].[data_zones]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[data_zones]') AND type in (N'U'))
DROP TABLE [dbo].[data_zones]
GO
/****** Object:  Table [dbo].[datagps]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Mobitel__59C55456]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Mobitel__59C55456]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Message__5AB9788F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Message__5AB9788F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Latitud__5BAD9CC8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Latitud__5BAD9CC8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Longitu__5CA1C101]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Longitu__5CA1C101]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__UnixTim__5D95E53A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__UnixTim__5D95E53A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Speed__5E8A0973]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Speed__5E8A0973]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Directi__5F7E2DAC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Directi__5F7E2DAC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Valid__607251E5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Valid__607251E5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Events__6166761E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Events__6166761E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor1__625A9A57]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor1__625A9A57]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor2__634EBE90]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor2__634EBE90]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor3__6442E2C9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor3__6442E2C9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor4__65370702]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor4__65370702]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor5__662B2B3B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor5__662B2B3B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor6__671F4F74]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor6__671F4F74]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor7__681373AD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor7__681373AD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor8__690797E6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor8__690797E6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__LogID__69FBBC1F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__LogID__69FBBC1F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__isShow__6AEFE058]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__isShow__6AEFE058]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__whatIs__6BE40491]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__whatIs__6BE40491]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Counter__6CD828CA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Counter__6CD828CA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Counter__6DCC4D03]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Counter__6DCC4D03]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Counter__6EC0713C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Counter__6EC0713C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Counter__6FB49575]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Counter__6FB49575]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__SrvPack__70A8B9AE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__SrvPack__70A8B9AE]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND type in (N'U'))
DROP TABLE [dbo].[datagps]
GO
/****** Object:  Table [dbo].[datagpsbuffer]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__72910220]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Mobit__72910220]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Messa__73852659]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Messa__73852659]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Latit__74794A92]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Latit__74794A92]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Longi__756D6ECB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Longi__756D6ECB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__UnixT__76619304]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__UnixT__76619304]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Speed__7755B73D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Speed__7755B73D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Direc__7849DB76]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Direc__7849DB76]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Valid__793DFFAF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Valid__793DFFAF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Event__7A3223E8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Event__7A3223E8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7B264821]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7B264821]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7C1A6C5A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7C1A6C5A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7D0E9093]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7D0E9093]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7E02B4CC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7E02B4CC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7EF6D905]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7EF6D905]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7FEAFD3E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7FEAFD3E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__00DF2177]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__00DF2177]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__01D345B0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__01D345B0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__02C769E9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__LogID__02C769E9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__isSho__03BB8E22]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__isSho__03BB8E22]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__whatI__04AFB25B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__whatI__04AFB25B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__05A3D694]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Count__05A3D694]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__0697FACD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Count__0697FACD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__078C1F06]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Count__078C1F06]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__0880433F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Count__0880433F]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbuffer]
GO
/****** Object:  Table [dbo].[datagpsbuffer_dr]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__0A688BB1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Mobit__0A688BB1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Messa__0B5CAFEA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Messa__0B5CAFEA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Latit__0C50D423]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Latit__0C50D423]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Longi__0D44F85C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Longi__0D44F85C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Altit__0E391C95]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Altit__0E391C95]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__UnixT__0F2D40CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__UnixT__0F2D40CE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Speed__10216507]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Speed__10216507]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Direc__11158940]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Direc__11158940]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Valid__1209AD79]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Valid__1209AD79]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__InMob__12FDD1B2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__InMob__12FDD1B2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Event__13F1F5EB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Event__13F1F5EB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__14E61A24]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__14E61A24]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__15DA3E5D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__15DA3E5D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__16CE6296]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__16CE6296]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__17C286CF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__17C286CF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__18B6AB08]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__18B6AB08]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__19AACF41]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__19AACF41]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__1A9EF37A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__1A9EF37A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__1B9317B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__1B9317B3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__1C873BEC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__LogID__1C873BEC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__isSho__1D7B6025]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__isSho__1D7B6025]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__whatI__1E6F845E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__whatI__1E6F845E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__1F63A897]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Count__1F63A897]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__2057CCD0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Count__2057CCD0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__214BF109]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Count__214BF109]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__22401542]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Count__22401542]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_dr]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbuffer_dr]
GO
/****** Object:  Table [dbo].[datagpsbuffer_on]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__24285DB4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Mobit__24285DB4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Messa__251C81ED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Messa__251C81ED]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Latit__2610A626]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Latit__2610A626]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Longi__2704CA5F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Longi__2704CA5F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__UnixT__27F8EE98]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__UnixT__27F8EE98]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Speed__28ED12D1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Speed__28ED12D1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Direc__29E1370A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Direc__29E1370A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Valid__2AD55B43]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Valid__2AD55B43]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Event__2BC97F7C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Event__2BC97F7C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__2CBDA3B5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__2CBDA3B5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__2DB1C7EE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__2DB1C7EE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__2EA5EC27]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__2EA5EC27]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__2F9A1060]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__2F9A1060]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__308E3499]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__308E3499]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__318258D2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__318258D2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__32767D0B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__32767D0B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__336AA144]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__336AA144]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__345EC57D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__LogID__345EC57D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__isSho__3552E9B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__isSho__3552E9B6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__whatI__36470DEF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__whatI__36470DEF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__373B3228]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Count__373B3228]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__382F5661]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Count__382F5661]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__39237A9A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Count__39237A9A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__3A179ED3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Count__3A179ED3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__SrvPa__3B0BC30C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__SrvPa__3B0BC30C]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbuffer_on]
GO
/****** Object:  Table [dbo].[datagpsbuffer_ontmp]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__3CF40B7E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_ontmp] DROP CONSTRAINT [DF__datagpsbu__Mobit__3CF40B7E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__3DE82FB7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_ontmp] DROP CONSTRAINT [DF__datagpsbu__LogID__3DE82FB7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__SrvPa__3EDC53F0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_ontmp] DROP CONSTRAINT [DF__datagpsbu__SrvPa__3EDC53F0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_ontmp]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbuffer_ontmp]
GO
/****** Object:  Table [dbo].[datagpsbuffer_pop]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__40C49C62]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Mobit__40C49C62]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Messa__41B8C09B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Messa__41B8C09B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Latit__42ACE4D4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Latit__42ACE4D4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Longi__43A1090D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Longi__43A1090D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Altit__44952D46]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Altit__44952D46]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__UnixT__4589517F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__UnixT__4589517F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Speed__467D75B8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Speed__467D75B8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Direc__477199F1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Direc__477199F1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Valid__4865BE2A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Valid__4865BE2A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__InMob__4959E263]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__InMob__4959E263]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Event__4A4E069C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Event__4A4E069C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__4B422AD5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__4B422AD5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__4C364F0E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__4C364F0E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__4D2A7347]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__4D2A7347]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__4E1E9780]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__4E1E9780]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__4F12BBB9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__4F12BBB9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__5006DFF2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__5006DFF2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__50FB042B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__50FB042B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__51EF2864]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__51EF2864]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__52E34C9D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__LogID__52E34C9D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__isSho__53D770D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__isSho__53D770D6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__whatI__54CB950F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__whatI__54CB950F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__55BFB948]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Count__55BFB948]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__56B3DD81]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Count__56B3DD81]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__57A801BA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Count__57A801BA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__589C25F3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Count__589C25F3]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_pop]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbuffer_pop]
GO
/****** Object:  Table [dbo].[datagpsbufferlite]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__5A846E65]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Mobit__5A846E65]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Messa__5B78929E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Messa__5B78929E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Latit__5C6CB6D7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Latit__5C6CB6D7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Longi__5D60DB10]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Longi__5D60DB10]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__UnixT__5E54FF49]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__UnixT__5E54FF49]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Speed__5F492382]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Speed__5F492382]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Direc__603D47BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Direc__603D47BB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Valid__61316BF4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Valid__61316BF4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Event__6225902D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Event__6225902D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__6319B466]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__6319B466]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__640DD89F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__640DD89F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__6501FCD8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__6501FCD8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__65F62111]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__65F62111]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__66EA454A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__66EA454A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__67DE6983]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__67DE6983]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__68D28DBC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__68D28DBC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__69C6B1F5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__69C6B1F5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__6ABAD62E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__LogID__6ABAD62E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__isSho__6BAEFA67]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__isSho__6BAEFA67]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__whatI__6CA31EA0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__whatI__6CA31EA0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__6D9742D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Count__6D9742D9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__6E8B6712]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Count__6E8B6712]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__6F7F8B4B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Count__6F7F8B4B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__7073AF84]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Count__7073AF84]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbufferlite]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbufferlite]
GO
/****** Object:  Table [dbo].[datagpslost]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Mobit__725BF7F6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost] DROP CONSTRAINT [DF__datagpslo__Mobit__725BF7F6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Begin__73501C2F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost] DROP CONSTRAINT [DF__datagpslo__Begin__73501C2F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__End_L__74444068]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost] DROP CONSTRAINT [DF__datagpslo__End_L__74444068]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost]') AND type in (N'U'))
DROP TABLE [dbo].[datagpslost]
GO
/****** Object:  Table [dbo].[datagpslost_on]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Mobit__762C88DA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_on] DROP CONSTRAINT [DF__datagpslo__Mobit__762C88DA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Begin__7720AD13]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_on] DROP CONSTRAINT [DF__datagpslo__Begin__7720AD13]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__End_L__7814D14C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_on] DROP CONSTRAINT [DF__datagpslo__End_L__7814D14C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Begin__7908F585]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_on] DROP CONSTRAINT [DF__datagpslo__Begin__7908F585]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__End_S__79FD19BE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_on] DROP CONSTRAINT [DF__datagpslo__End_S__79FD19BE]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_on]') AND type in (N'U'))
DROP TABLE [dbo].[datagpslost_on]
GO
/****** Object:  Table [dbo].[datagpslost_ontmp]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__LogID__7BE56230]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_ontmp] DROP CONSTRAINT [DF__datagpslo__LogID__7BE56230]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_ontmp]') AND type in (N'U'))
DROP TABLE [dbo].[datagpslost_ontmp]
GO
/****** Object:  Table [dbo].[datagpslost_rftmp]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__LogID__7DCDAAA2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_rftmp] DROP CONSTRAINT [DF__datagpslo__LogID__7DCDAAA2]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_rftmp]') AND type in (N'U'))
DROP TABLE [dbo].[datagpslost_rftmp]
GO
/****** Object:  Table [dbo].[datagpslosted]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Mobit__7FB5F314]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslosted] DROP CONSTRAINT [DF__datagpslo__Mobit__7FB5F314]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Begin__00AA174D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslosted] DROP CONSTRAINT [DF__datagpslo__Begin__00AA174D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__End_L__019E3B86]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslosted] DROP CONSTRAINT [DF__datagpslo__End_L__019E3B86]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslosted]') AND type in (N'U'))
DROP TABLE [dbo].[datagpslosted]
GO
/****** Object:  Table [dbo].[driver]    Script Date: 04/11/2013 10:56:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__driver__idType__038683F8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[driver] DROP CONSTRAINT [DF__driver__idType__038683F8]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[driver]') AND type in (N'U'))
DROP TABLE [dbo].[driver]
GO
/****** Object:  Table [dbo].[driver_types]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[driver_types]') AND type in (N'U'))
DROP TABLE [dbo].[driver_types]
GO
/****** Object:  Table [dbo].[gpsmasks]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__ID__0662F0A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__ID__0662F0A3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Mobite__075714DC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Mobite__075714DC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Messag__084B3915]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Messag__084B3915]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__LastRe__093F5D4E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__LastRe__093F5D4E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__LastTi__0A338187]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__LastTi__0A338187]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__LastTi__0B27A5C0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__LastTi__0B27A5C0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__LastMe__0C1BC9F9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__LastMe__0C1BC9F9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__LastMe__0D0FEE32]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__LastMe__0D0FEE32]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__MaxSms__0E04126B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__MaxSms__0E04126B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__MaxSms__0EF836A4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__MaxSms__0EF836A4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Latitu__0FEC5ADD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Latitu__0FEC5ADD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Latitu__10E07F16]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Latitu__10E07F16]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Longit__11D4A34F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Longit__11D4A34F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Longit__12C8C788]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Longit__12C8C788]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Altitu__13BCEBC1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Altitu__13BCEBC1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Altitu__14B10FFA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Altitu__14B10FFA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__UnixTi__15A53433]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__UnixTi__15A53433]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__UnixTi__1699586C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__UnixTi__1699586C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__SpeedS__178D7CA5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__SpeedS__178D7CA5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__SpeedE__1881A0DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__SpeedE__1881A0DE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Direct__1975C517]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Direct__1975C517]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Direct__1A69E950]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Direct__1A69E950]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__1B5E0D89]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__1B5E0D89]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__1C5231C2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__1C5231C2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__WhatSe__1D4655FB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__WhatSe__1D4655FB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__CheckM__1E3A7A34]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__CheckM__1E3A7A34]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__InMobi__1F2E9E6D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__InMobi__1F2E9E6D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Events__2022C2A6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Events__2022C2A6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__2116E6DF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__2116E6DF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__220B0B18]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__220B0B18]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__22FF2F51]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__22FF2F51]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__23F3538A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__23F3538A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__24E777C3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__24E777C3]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gpsmasks]') AND type in (N'U'))
DROP TABLE [dbo].[gpsmasks]
GO
/****** Object:  Table [dbo].[internalmobitelconfig]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__internalmobi__ID__26CFC035]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[internalmobitelconfig] DROP CONSTRAINT [DF__internalmobi__ID__26CFC035]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__internalm__Messa__27C3E46E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[internalmobitelconfig] DROP CONSTRAINT [DF__internalm__Messa__27C3E46E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__internalm__RfTyp__28B808A7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[internalmobitelconfig] DROP CONSTRAINT [DF__internalm__RfTyp__28B808A7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__internalm__Activ__29AC2CE0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[internalmobitelconfig] DROP CONSTRAINT [DF__internalm__Activ__29AC2CE0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__internalm__modul__2AA05119]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[internalmobitelconfig] DROP CONSTRAINT [DF__internalm__modul__2AA05119]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[internalmobitelconfig]') AND type in (N'U'))
DROP TABLE [dbo].[internalmobitelconfig]
GO
/****** Object:  Table [dbo].[lastmeters]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lastmeter__LastM__2C88998B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lastmeters] DROP CONSTRAINT [DF__lastmeter__LastM__2C88998B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lastmeters__ID__2D7CBDC4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lastmeters] DROP CONSTRAINT [DF__lastmeters__ID__2D7CBDC4]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lastmeters]') AND type in (N'U'))
DROP TABLE [dbo].[lastmeters]
GO
/****** Object:  Table [dbo].[lastrecords]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lastrecor__LastR__2F650636]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lastrecords] DROP CONSTRAINT [DF__lastrecor__LastR__2F650636]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lastrecords__ID__30592A6F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lastrecords] DROP CONSTRAINT [DF__lastrecords__ID__30592A6F]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lastrecords]') AND type in (N'U'))
DROP TABLE [dbo].[lastrecords]
GO
/****** Object:  Table [dbo].[lasttimes]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lasttimes__LastT__324172E1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lasttimes] DROP CONSTRAINT [DF__lasttimes__LastT__324172E1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lasttimes__ID__3335971A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lasttimes] DROP CONSTRAINT [DF__lasttimes__ID__3335971A]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lasttimes]') AND type in (N'U'))
DROP TABLE [dbo].[lasttimes]
GO
/****** Object:  Table [dbo].[mapstyleglobal]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleglob__ID__37FA4C37]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleglob__ID__37FA4C37]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Defau__38EE7070]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Defau__38EE7070]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Defau__39E294A9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Defau__39E294A9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3AD6B8E2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3AD6B8E2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3BCADD1B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3BCADD1B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3CBF0154]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3CBF0154]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3DB3258D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3DB3258D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3EA749C6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3EA749C6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3F9B6DFF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3F9B6DFF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__408F9238]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__408F9238]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4183B671]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4183B671]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4277DAAA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4277DAAA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__436BFEE3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__436BFEE3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4460231C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4460231C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__45544755]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__45544755]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__46486B8E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__46486B8E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__473C8FC7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__473C8FC7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4830B400]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4830B400]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4924D839]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4924D839]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4A18FC72]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4A18FC72]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4B0D20AB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4B0D20AB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4C0144E4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4C0144E4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4CF5691D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4CF5691D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4DE98D56]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4DE98D56]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4EDDB18F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4EDDB18F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4FD1D5C8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4FD1D5C8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__50C5FA01]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__50C5FA01]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__51BA1E3A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__51BA1E3A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__52AE4273]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__52AE4273]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__53A266AC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__53A266AC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__54968AE5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__54968AE5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__558AAF1E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__558AAF1E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__567ED357]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__567ED357]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__5772F790]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__5772F790]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__58671BC9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__58671BC9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__595B4002]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__595B4002]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5A4F643B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5A4F643B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5B438874]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5B438874]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5C37ACAD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5C37ACAD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5D2BD0E6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5D2BD0E6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5E1FF51F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5E1FF51F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5F141958]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5F141958]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__60083D91]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__60083D91]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstyleglobal]') AND type in (N'U'))
DROP TABLE [dbo].[mapstyleglobal]
GO
/****** Object:  Table [dbo].[mapstylelines]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleline__ID__61F08603]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylelines] DROP CONSTRAINT [DF__mapstyleline__ID__61F08603]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylel__Color__62E4AA3C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylelines] DROP CONSTRAINT [DF__mapstylel__Color__62E4AA3C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylel__Width__63D8CE75]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylelines] DROP CONSTRAINT [DF__mapstylel__Width__63D8CE75]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylel__Style__64CCF2AE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylelines] DROP CONSTRAINT [DF__mapstylel__Style__64CCF2AE]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstylelines]') AND type in (N'U'))
DROP TABLE [dbo].[mapstylelines]
GO
/****** Object:  Table [dbo].[mapstylepoints]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylepoin__ID__66B53B20]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylepoin__ID__66B53B20]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylep__Symbo__67A95F59]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylep__Symbo__67A95F59]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylep__Color__689D8392]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylep__Color__689D8392]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylepo__Size__6991A7CB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylepo__Size__6991A7CB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylep__Rotat__6A85CC04]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylep__Rotat__6A85CC04]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylep__Bitma__6B79F03D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylep__Bitma__6B79F03D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylep__Style__6C6E1476]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylep__Style__6C6E1476]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstylepoints]') AND type in (N'U'))
DROP TABLE [dbo].[mapstylepoints]
GO
/****** Object:  Table [dbo].[md_alarming_types]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_alarmin__Name__6E565CE8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_alarming_types] DROP CONSTRAINT [DF__md_alarmin__Name__6E565CE8]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_alarming_types]') AND type in (N'U'))
DROP TABLE [dbo].[md_alarming_types]
GO
/****** Object:  Table [dbo].[md_cargo]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_cargo]') AND type in (N'U'))
DROP TABLE [dbo].[md_cargo]
GO
/****** Object:  Table [dbo].[md_enterprise]    Script Date: 04/11/2013 10:56:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_enterpr__Name__7132C993]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_enterprise] DROP CONSTRAINT [DF__md_enterpr__Name__7132C993]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_enterp__Curre__7226EDCC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_enterprise] DROP CONSTRAINT [DF__md_enterp__Curre__7226EDCC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_enterp__Dispa__731B1205]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_enterprise] DROP CONSTRAINT [DF__md_enterp__Dispa__731B1205]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_enterprise]') AND type in (N'U'))
DROP TABLE [dbo].[md_enterprise]
GO
/****** Object:  Table [dbo].[md_object_group]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object___Name__7D98A078]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object_group] DROP CONSTRAINT [DF__md_object___Name__7D98A078]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_object_group]') AND type in (N'U'))
DROP TABLE [dbo].[md_object_group]
GO
/****** Object:  Table [dbo].[agro_fieldgroupe]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_field__Name__1273C1CD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fieldgroupe] DROP CONSTRAINT [DF__agro_field__Name__1273C1CD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fiel__Id_Zo__1367E606]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fieldgroupe] DROP CONSTRAINT [DF__agro_fiel__Id_Zo__1367E606]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldgroupe]') AND type in (N'U'))
DROP TABLE [dbo].[agro_fieldgroupe]
GO
/****** Object:  Table [dbo].[agro_fueling]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuel__LockR__15502E78]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuel__LockR__15502E78]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuelin__Lat__164452B1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuelin__Lat__164452B1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuelin__Lng__173876EA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuelin__Lng__173876EA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuel__Value__182C9B23]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuel__Value__182C9B23]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuel__Value__1920BF5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuel__Value__1920BF5C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuel__Value__1A14E395]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuel__Value__1A14E395]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fueling]') AND type in (N'U'))
DROP TABLE [dbo].[agro_fueling]
GO
/****** Object:  Table [dbo].[agro_field]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_field__Name__0F975522]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_field] DROP CONSTRAINT [DF__agro_field__Name__0F975522]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_field]') AND type in (N'U'))
DROP TABLE [dbo].[agro_field]
GO
/****** Object:  Table [dbo].[agro_culture]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_cultu__Name__09DE7BCC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_culture] DROP CONSTRAINT [DF__agro_cultu__Name__09DE7BCC]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_culture]') AND type in (N'U'))
DROP TABLE [dbo].[agro_culture]
GO
/****** Object:  Table [dbo].[agro_agregat]    Script Date: 04/11/2013 10:56:40 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agreg__Name__7E6CC920]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agreg__Name__7E6CC920]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Width__7F60ED59]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agre__Width__7F60ED59]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agrega__Def__00551192]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agrega__Def__00551192]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Id_ma__014935CB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agre__Id_ma__014935CB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__IsGro__023D5A04]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agre__IsGro__023D5A04]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Id_wo__03317E3D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agre__Id_wo__03317E3D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__HasSe__0425A276]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agre__HasSe__0425A276]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat]') AND type in (N'U'))
DROP TABLE [dbo].[agro_agregat]
GO
/****** Object:  Table [dbo].[agro_unit]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_unit__Name__5165187F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_unit] DROP CONSTRAINT [DF__agro_unit__Name__5165187F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_unit__NameS__52593CB8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_unit] DROP CONSTRAINT [DF__agro_unit__NameS__52593CB8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_unit__Facto__534D60F1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_unit] DROP CONSTRAINT [DF__agro_unit__Facto__534D60F1]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_unit]') AND type in (N'U'))
DROP TABLE [dbo].[agro_unit]
GO
/****** Object:  Table [dbo].[agro_params]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_params]') AND type in (N'U'))
DROP TABLE [dbo].[agro_params]
GO
/****** Object:  Table [dbo].[agro_price]    Script Date: 04/11/2013 10:56:41 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_price__Name__4CA06362]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_price] DROP CONSTRAINT [DF__agro_price__Name__4CA06362]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_price]') AND type in (N'U'))
DROP TABLE [dbo].[agro_price]
GO
/****** Object:  Table [dbo].[rules]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__ID__3B60C8C7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__ID__3B60C8C7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule1__3C54ED00]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule1__3C54ED00]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule2__3D491139]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule2__3D491139]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule3__3E3D3572]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule3__3E3D3572]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule4__3F3159AB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule4__3F3159AB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule5__40257DE4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule5__40257DE4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule6__4119A21D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule6__4119A21D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule7__420DC656]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule7__420DC656]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule8__4301EA8F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule8__4301EA8F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule9__43F60EC8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule9__43F60EC8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule10__44EA3301]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule10__44EA3301]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule11__45DE573A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule11__45DE573A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule12__46D27B73]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule12__46D27B73]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule13__47C69FAC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule13__47C69FAC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule14__48BAC3E5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule14__48BAC3E5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule15__49AEE81E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule15__49AEE81E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule16__4AA30C57]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule16__4AA30C57]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rules]') AND type in (N'U'))
DROP TABLE [dbo].[rules]
GO
/****** Object:  Table [dbo].[sensor_hookups]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensor_hookups]') AND type in (N'U'))
DROP TABLE [dbo].[sensor_hookups]
GO
/****** Object:  Table [dbo].[sensor_list]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensor_list]') AND type in (N'U'))
DROP TABLE [dbo].[sensor_list]
GO
/****** Object:  Table [dbo].[sensoralgorithms]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensoralgo__Name__4E739D3B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensoralgorithms] DROP CONSTRAINT [DF__sensoralgo__Name__4E739D3B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensoralg__Algor__4F67C174]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensoralgorithms] DROP CONSTRAINT [DF__sensoralg__Algor__4F67C174]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensoralgorithms]') AND type in (N'U'))
DROP TABLE [dbo].[sensoralgorithms]
GO
/****** Object:  Table [dbo].[sensorcoefficient]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensorcoeffic__K__515009E6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensorcoefficient] DROP CONSTRAINT [DF__sensorcoeffic__K__515009E6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensorcoeffic__b__52442E1F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensorcoefficient] DROP CONSTRAINT [DF__sensorcoeffic__b__52442E1F]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensorcoefficient]') AND type in (N'U'))
DROP TABLE [dbo].[sensorcoefficient]
GO
/****** Object:  Table [dbo].[sensordata]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensordata]') AND type in (N'U'))
DROP TABLE [dbo].[sensordata]
GO
/****** Object:  Table [dbo].[sensors]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensors__Name__55209ACA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensors] DROP CONSTRAINT [DF__sensors__Name__55209ACA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensors__Descrip__5614BF03]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensors] DROP CONSTRAINT [DF__sensors__Descrip__5614BF03]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensors__NameUni__5708E33C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensors] DROP CONSTRAINT [DF__sensors__NameUni__5708E33C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensors__K__57FD0775]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensors] DROP CONSTRAINT [DF__sensors__K__57FD0775]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensors]') AND type in (N'U'))
DROP TABLE [dbo].[sensors]
GO
/****** Object:  Table [dbo].[serviceinit]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__serviceinit__ID__59E54FE7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__serviceinit__ID__59E54FE7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__Flags__5AD97420]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__Flags__5AD97420]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__Servi__5BCD9859]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__Servi__5BCD9859]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__CAtoS__5CC1BC92]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__CAtoS__5CC1BC92]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__CAtoS__5DB5E0CB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__CAtoS__5DB5E0CB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__SAtoC__5EAA0504]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__SAtoC__5EAA0504]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__SAtoC__5F9E293D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__SAtoC__5F9E293D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__SAtoC__60924D76]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__SAtoC__60924D76]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__CAtoS__618671AF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__CAtoS__618671AF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__Mobit__627A95E8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__Mobit__627A95E8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__Activ__636EBA21]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__Activ__636EBA21]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[serviceinit]') AND type in (N'U'))
DROP TABLE [dbo].[serviceinit]
GO
/****** Object:  Table [dbo].[servicesend]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicesend__ID__65570293]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[servicesend] DROP CONSTRAINT [DF__servicesend__ID__65570293]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicese__Flags__664B26CC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[servicesend] DROP CONSTRAINT [DF__servicese__Flags__664B26CC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicese__PortM__673F4B05]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[servicesend] DROP CONSTRAINT [DF__servicese__PortM__673F4B05]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicese__Messa__68336F3E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[servicesend] DROP CONSTRAINT [DF__servicese__Messa__68336F3E]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[servicesend]') AND type in (N'U'))
DROP TABLE [dbo].[servicesend]
GO
/****** Object:  Table [dbo].[servicetypes]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicety__Flags__6A1BB7B0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[servicetypes] DROP CONSTRAINT [DF__servicety__Flags__6A1BB7B0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[servicetypes]') AND type in (N'U'))
DROP TABLE [dbo].[servicetypes]
GO
/****** Object:  Table [dbo].[setting]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__TimeBre__6C040022]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__TimeBre__6C040022]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Rotatio__6CF8245B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Rotatio__6CF8245B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Rotatio__6DEC4894]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Rotatio__6DEC4894]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Fueling__6EE06CCD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Fueling__6EE06CCD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__band__6FD49106]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__band__6FD49106]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Fueling__70C8B53F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Fueling__70C8B53F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__accelMa__71BCD978]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__accelMa__71BCD978]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__breakMa__72B0FDB1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__breakMa__72B0FDB1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__speedMa__73A521EA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__speedMa__73A521EA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__idleRun__74994623]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__idleRun__74994623]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__idleRun__758D6A5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__idleRun__758D6A5C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Name__76818E95]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Name__76818E95]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Desc__7775B2CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Desc__7775B2CE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__AvgFuel__7869D707]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__AvgFuel__7869D707]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__FuelApp__795DFB40]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__FuelApp__795DFB40]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__TimeGet__7A521F79]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__TimeGet__7A521F79]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__TimeGet__7B4643B2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__TimeGet__7B4643B2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__CzMinCr__7C3A67EB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__CzMinCr__7C3A67EB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__FuelerR__7D2E8C24]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__FuelerR__7D2E8C24]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__FuelerM__7E22B05D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__FuelerM__7E22B05D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__FuelerM__7F16D496]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__FuelerM__7F16D496]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__FuelerC__000AF8CF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__FuelerC__000AF8CF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Fuelrat__00FF1D08]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Fuelrat__00FF1D08]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Fueling__01F34141]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Fueling__01F34141]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__TimeBre__02E7657A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__TimeBre__02E7657A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Inclino__03DB89B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Inclino__03DB89B3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Inclino__04CFADEC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Inclino__04CFADEC]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[setting]') AND type in (N'U'))
DROP TABLE [dbo].[setting]
GO
/****** Object:  Table [dbo].[smsnumbers]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__smsnumbers__ID__06B7F65E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[smsnumbers] DROP CONSTRAINT [DF__smsnumbers__ID__06B7F65E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__smsnumber__Flags__07AC1A97]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[smsnumbers] DROP CONSTRAINT [DF__smsnumber__Flags__07AC1A97]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[smsnumbers]') AND type in (N'U'))
DROP TABLE [dbo].[smsnumbers]
GO
/****** Object:  Table [dbo].[sources]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sources]') AND type in (N'U'))
DROP TABLE [dbo].[sources]
GO
/****** Object:  Table [dbo].[specialmessages]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__specialme__DataI__0A888742]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[specialmessages] DROP CONSTRAINT [DF__specialme__DataI__0A888742]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__specialme__isNew__0B7CAB7B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[specialmessages] DROP CONSTRAINT [DF__specialme__isNew__0B7CAB7B]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[specialmessages]') AND type in (N'U'))
DROP TABLE [dbo].[specialmessages]
GO
/****** Object:  Table [dbo].[state]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__state__Title__0D64F3ED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[state] DROP CONSTRAINT [DF__state__Title__0D64F3ED]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[state]') AND type in (N'U'))
DROP TABLE [dbo].[state]
GO
/****** Object:  Table [dbo].[swupdate]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__swupdate__ShortI__0F4D3C5F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[swupdate] DROP CONSTRAINT [DF__swupdate__ShortI__0F4D3C5F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__swupdate__Insert__10416098]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[swupdate] DROP CONSTRAINT [DF__swupdate__Insert__10416098]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[swupdate]') AND type in (N'U'))
DROP TABLE [dbo].[swupdate]
GO
/****** Object:  Table [dbo].[team]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[team]') AND type in (N'U'))
DROP TABLE [dbo].[team]
GO
/****** Object:  Table [dbo].[telnumbers]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__telnumbers__ID__131DCD43]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[telnumbers] DROP CONSTRAINT [DF__telnumbers__ID__131DCD43]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__telnumber__Flags__1411F17C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[telnumbers] DROP CONSTRAINT [DF__telnumber__Flags__1411F17C]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[telnumbers]') AND type in (N'U'))
DROP TABLE [dbo].[telnumbers]
GO
/****** Object:  Table [dbo].[temp]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__temp__datagps_id__15FA39EE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[temp] DROP CONSTRAINT [DF__temp__datagps_id__15FA39EE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__temp__sensor_id__16EE5E27]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[temp] DROP CONSTRAINT [DF__temp__sensor_id__16EE5E27]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__temp__Value__17E28260]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[temp] DROP CONSTRAINT [DF__temp__Value__17E28260]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp]') AND type in (N'U'))
DROP TABLE [dbo].[temp]
GO
/****** Object:  Table [dbo].[tmpdatagps]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__DataG__19CACAD2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__DataG__19CACAD2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Mobit__1ABEEF0B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Mobit__1ABEEF0B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Messa__1BB31344]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Messa__1BB31344]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Latit__1CA7377D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Latit__1CA7377D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Longi__1D9B5BB6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Longi__1D9B5BB6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__UnixT__1E8F7FEF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__UnixT__1E8F7FEF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Speed__1F83A428]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Speed__1F83A428]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Direc__2077C861]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Direc__2077C861]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Valid__216BEC9A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Valid__216BEC9A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Event__226010D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Event__226010D3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__2354350C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__2354350C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__24485945]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__24485945]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__253C7D7E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__253C7D7E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__2630A1B7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__2630A1B7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__2724C5F0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__2724C5F0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__2818EA29]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__2818EA29]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__290D0E62]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__290D0E62]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__2A01329B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__2A01329B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__LogID__2AF556D4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__LogID__2AF556D4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__isSho__2BE97B0D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__isSho__2BE97B0D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__whatI__2CDD9F46]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__whatI__2CDD9F46]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Count__2DD1C37F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Count__2DD1C37F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Count__2EC5E7B8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Count__2EC5E7B8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Count__2FBA0BF1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Count__2FBA0BF1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Count__30AE302A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Count__30AE302A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__SrvPa__31A25463]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__SrvPa__31A25463]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmpdatagps]') AND type in (N'U'))
DROP TABLE [dbo].[tmpdatagps]
GO
/****** Object:  Table [dbo].[transition]    Script Date: 04/11/2013 10:56:48 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__transitio__Title__338A9CD5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transition] DROP CONSTRAINT [DF__transitio__Title__338A9CD5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__transitio__IconN__347EC10E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transition] DROP CONSTRAINT [DF__transitio__IconN__347EC10E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__transitio__Sound__3572E547]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transition] DROP CONSTRAINT [DF__transitio__Sound__3572E547]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__transitio__ViewI__36670980]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transition] DROP CONSTRAINT [DF__transitio__ViewI__36670980]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[transition]') AND type in (N'U'))
DROP TABLE [dbo].[transition]
GO
/****** Object:  Table [dbo].[rt_route_type]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route_t__Name__2A363CC5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_type] DROP CONSTRAINT [DF__rt_route_t__Name__2A363CC5]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_type]') AND type in (N'U'))
DROP TABLE [dbo].[rt_route_type]
GO
/****** Object:  Table [dbo].[online]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Mobitel___29971E47]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Mobitel___29971E47]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Message___2A8B4280]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Message___2A8B4280]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Latitude__2B7F66B9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Latitude__2B7F66B9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Longitud__2C738AF2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Longitud__2C738AF2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Altitude__2D67AF2B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Altitude__2D67AF2B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__UnixTime__2E5BD364]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__UnixTime__2E5BD364]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Speed__2F4FF79D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Speed__2F4FF79D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Directio__30441BD6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Directio__30441BD6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Valid__3138400F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Valid__3138400F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__InMobite__322C6448]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__InMobite__322C6448]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Events__33208881]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Events__33208881]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor1__3414ACBA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor1__3414ACBA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor2__3508D0F3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor2__3508D0F3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor3__35FCF52C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor3__35FCF52C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor4__36F11965]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor4__36F11965]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor5__37E53D9E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor5__37E53D9E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor6__38D961D7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor6__38D961D7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor7__39CD8610]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor7__39CD8610]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor8__3AC1AA49]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor8__3AC1AA49]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__LogID__3BB5CE82]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__LogID__3BB5CE82]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__isShow__3CA9F2BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__isShow__3CA9F2BB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__whatIs__3D9E16F4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__whatIs__3D9E16F4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Counter1__3E923B2D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Counter1__3E923B2D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Counter2__3F865F66]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Counter2__3F865F66]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Counter3__407A839F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Counter3__407A839F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Counter4__416EA7D8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Counter4__416EA7D8]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[online]') AND type in (N'U'))
DROP TABLE [dbo].[online]
GO
/****** Object:  Table [dbo].[parameters]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[parameters]') AND type in (N'U'))
DROP TABLE [dbo].[parameters]
GO
/****** Object:  Table [dbo].[md_order_priority]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order_p__Name__0CDAE408]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order_priority] DROP CONSTRAINT [DF__md_order_p__Name__0CDAE408]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order_priority]') AND type in (N'U'))
DROP TABLE [dbo].[md_order_priority]
GO
/****** Object:  Table [dbo].[users_acs_roles]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_acs___Name__3943762B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_acs_roles] DROP CONSTRAINT [DF__users_acs___Name__3943762B]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_acs_roles]') AND type in (N'U'))
DROP TABLE [dbo].[users_acs_roles]
GO
/****** Object:  Table [dbo].[users_actions_log]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_act__TypeL__3B2BBE9D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_actions_log] DROP CONSTRAINT [DF__users_act__TypeL__3B2BBE9D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_act__Id_do__3C1FE2D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_actions_log] DROP CONSTRAINT [DF__users_act__Id_do__3C1FE2D6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_act__TextL__3D14070F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_actions_log] DROP CONSTRAINT [DF__users_act__TextL__3D14070F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_acti__User__3E082B48]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_actions_log] DROP CONSTRAINT [DF__users_acti__User__3E082B48]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_act__Compu__3EFC4F81]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_actions_log] DROP CONSTRAINT [DF__users_act__Compu__3EFC4F81]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_actions_log]') AND type in (N'U'))
DROP TABLE [dbo].[users_actions_log]
GO
/****** Object:  Table [dbo].[users_list]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_list__Name__40E497F3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_list] DROP CONSTRAINT [DF__users_list__Name__40E497F3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_lis__Admin__41D8BC2C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_list] DROP CONSTRAINT [DF__users_lis__Admin__41D8BC2C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_lis__Id_ro__42CCE065]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_list] DROP CONSTRAINT [DF__users_lis__Id_ro__42CCE065]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_list]') AND type in (N'U'))
DROP TABLE [dbo].[users_list]
GO
/****** Object:  Table [dbo].[users_reports_list]    Script Date: 04/11/2013 10:56:49 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_rep__Repor__44B528D7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_reports_list] DROP CONSTRAINT [DF__users_rep__Repor__44B528D7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_rep__Repor__45A94D10]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_reports_list] DROP CONSTRAINT [DF__users_rep__Repor__45A94D10]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_reports_list]') AND type in (N'U'))
DROP TABLE [dbo].[users_reports_list]
GO
/****** Object:  Table [dbo].[rt_sample]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__Name__2FEF161B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__Name__2FEF161B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__Id_mo__30E33A54]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__Id_mo__30E33A54]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__Dista__31D75E8D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__Dista__31D75E8D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__Id_ma__32CB82C6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__Id_ma__32CB82C6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__IsGro__33BFA6FF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__IsGro__33BFA6FF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__AutoC__34B3CB38]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__AutoC__34B3CB38]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_sample]') AND type in (N'U'))
DROP TABLE [dbo].[rt_sample]
GO
/****** Object:  Table [dbo].[relationalgorithms]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__relationa__Algor__7F4BDEC0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[relationalgorithms] DROP CONSTRAINT [DF__relationa__Algor__7F4BDEC0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__relationa__Senso__004002F9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[relationalgorithms] DROP CONSTRAINT [DF__relationa__Senso__004002F9]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[relationalgorithms]') AND type in (N'U'))
DROP TABLE [dbo].[relationalgorithms]
GO
/****** Object:  Table [dbo].[ntf_main]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_main__IsActi__573DED66]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_main] DROP CONSTRAINT [DF__ntf_main__IsActi__573DED66]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_main__IsPopu__5832119F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_main] DROP CONSTRAINT [DF__ntf_main__IsPopu__5832119F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_main__Title__592635D8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_main] DROP CONSTRAINT [DF__ntf_main__Title__592635D8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_main__TimeFo__5A1A5A11]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_main] DROP CONSTRAINT [DF__ntf_main__TimeFo__5A1A5A11]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_main__IsEmai__5B0E7E4A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_main] DROP CONSTRAINT [DF__ntf_main__IsEmai__5B0E7E4A]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_main]') AND type in (N'U'))
DROP TABLE [dbo].[ntf_main]
GO
/****** Object:  Table [dbo].[route_items]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_items]') AND type in (N'U'))
DROP TABLE [dbo].[route_items]
GO
/****** Object:  Table [dbo].[route_list]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_list]') AND type in (N'U'))
DROP TABLE [dbo].[route_list]
GO
/****** Object:  Table [dbo].[route_lists]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_lists]') AND type in (N'U'))
DROP TABLE [dbo].[route_lists]
GO
/****** Object:  Table [dbo].[route_mobitel_links]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_mobitel_links]') AND type in (N'U'))
DROP TABLE [dbo].[route_mobitel_links]
GO
/****** Object:  Table [dbo].[route_mobitel_plans]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_mobitel_plans]') AND type in (N'U'))
DROP TABLE [dbo].[route_mobitel_plans]
GO
/****** Object:  Table [dbo].[route_points]    Script Date: 04/11/2013 10:56:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_points]') AND type in (N'U'))
DROP TABLE [dbo].[route_points]
GO
/****** Object:  Table [dbo].[rt_events]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_events__Name__08D548FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_events] DROP CONSTRAINT [DF__rt_events__Name__08D548FA]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_events]') AND type in (N'U'))
DROP TABLE [dbo].[rt_events]
GO
/****** Object:  Table [dbo].[rt_route]    Script Date: 04/11/2013 10:56:47 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Id_mob__0ABD916C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Id_mob__0ABD916C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Id_dri__0BB1B5A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Id_dri__0BB1B5A5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Distan__0CA5D9DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Distan__0CA5D9DE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelSt__0D99FE17]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelSt__0D99FE17]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelAd__0E8E2250]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelAd__0E8E2250]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelSu__0F824689]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelSu__0F824689]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelEn__10766AC2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelEn__10766AC2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelEx__116A8EFB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelEx__116A8EFB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelEx__125EB334]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelEx__125EB334]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Fuel_E__1352D76D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Fuel_E__1352D76D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Fuel_E__1446FBA6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Fuel_E__1446FBA6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Fuel_E__153B1FDF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Fuel_E__153B1FDF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Fuel_E__162F4418]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Fuel_E__162F4418]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__SpeedA__17236851]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__SpeedA__17236851]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelAd__18178C8A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelAd__18178C8A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelSu__190BB0C3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelSu__190BB0C3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Points__19FFD4FC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Points__19FFD4FC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Points__1AF3F935]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Points__1AF3F935]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Points__1BE81D6E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Points__1BE81D6E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Type__1CDC41A7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Type__1CDC41A7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__DistLo__1DD065E0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__DistLo__1DD065E0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route]') AND type in (N'U'))
DROP TABLE [dbo].[rt_route]
GO
/****** Object:  Table [dbo].[md_working_shift]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_working__Name__24B26D99]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_working_shift] DROP CONSTRAINT [DF__md_working__Name__24B26D99]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_working_shift]') AND type in (N'U'))
DROP TABLE [dbo].[md_working_shift]
GO
/****** Object:  Table [dbo].[messages]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Time1__269AB60B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Time1__269AB60B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Time2__278EDA44]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Time2__278EDA44]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Time3__2882FE7D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Time3__2882FE7D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Time4__297722B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Time4__297722B6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Mobite__2A6B46EF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Mobite__2A6B46EF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Servic__2B5F6B28]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Servic__2B5F6B28]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Comman__2C538F61]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Comman__2C538F61]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Comman__2D47B39A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Comman__2D47B39A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Messag__2E3BD7D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Messag__2E3BD7D3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Source__2F2FFC0C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Source__2F2FFC0C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Crc__30242045]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Crc__30242045]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__SmsId__3118447E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__SmsId__3118447E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__AdvCou__320C68B7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__AdvCou__320C68B7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__isNewF__33008CF0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__isNewF__33008CF0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND type in (N'U'))
DROP TABLE [dbo].[messages]
GO
/****** Object:  Table [dbo].[mobitels]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__ID__34E8D562]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__ID__34E8D562]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Flags__35DCF99B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Flags__35DCF99B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Servic__36D11DD4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Servic__36D11DD4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Servic__37C5420D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Servic__37C5420D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Intern__38B96646]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Intern__38B96646]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__MapSty__39AD8A7F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__MapSty__39AD8A7F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__MapSty__3AA1AEB8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__MapSty__3AA1AEB8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__MapSty__3B95D2F1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__MapSty__3B95D2F1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Route___3C89F72A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Route___3C89F72A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Device__3D7E1B63]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Device__3D7E1B63]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Device__3E723F9C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Device__3E723F9C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Confir__3F6663D5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Confir__3F6663D5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__LastIn__405A880E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__LastIn__405A880E]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels]') AND type in (N'U'))
DROP TABLE [dbo].[mobitels]
GO
/****** Object:  Table [dbo].[mobitels_info]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels_info]') AND type in (N'U'))
DROP TABLE [dbo].[mobitels_info]
GO
/****** Object:  Table [dbo].[md_order_category]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order_c__Name__07220AB2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order_category] DROP CONSTRAINT [DF__md_order_c__Name__07220AB2]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order_category]') AND type in (N'U'))
DROP TABLE [dbo].[md_order_category]
GO
/****** Object:  Table [dbo].[md_transportation_types]    Script Date: 04/11/2013 10:56:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_transportation_types]') AND type in (N'U'))
DROP TABLE [dbo].[md_transportation_types]
GO
/****** Object:  Table [dbo].[md_transportation_types]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_transportation_types]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_transportation_types](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_transportation_types] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_transportation_types', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_transportation_types', @level2type=N'COLUMN',@level2name=N'Name'
GO
/****** Object:  Table [dbo].[md_order_category]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order_category]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_order_category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_order_category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mobitels_info]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels_info]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mobitels_info](
	[Mobitel_ID] [bigint] NOT NULL,
	[Map_Style_Car] [varchar](max) NULL,
	[Track_Color] [bigint] NULL,
 CONSTRAINT [PK_mobitels_info] PRIMARY KEY CLUSTERED 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mobitels]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mobitels](
	[Mobitel_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [bigint] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[ServiceInit_ID] [int] NULL DEFAULT ((0)),
	[ServiceSend_ID] [int] NULL DEFAULT ((0)),
	[InternalMobitelConfig_ID] [int] NULL DEFAULT ((0)),
	[MapStyleLine_ID] [int] NULL DEFAULT ((0)),
	[MapStyleLastPoint_ID] [int] NULL DEFAULT ((0)),
	[MapStylePoint_ID] [int] NULL DEFAULT ((0)),
	[Route_ID] [int] NULL DEFAULT ((0)),
	[DeviceType] [int] NULL DEFAULT ((0)),
	[DeviceEngine] [int] NULL DEFAULT ((0)),
	[ConfirmedID] [int] NOT NULL DEFAULT ((0)),
	[LastInsertTime] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_mobitels] PRIMARY KEY CLUSTERED 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[mobitels]') AND name = N'IDX_InternalmobitelconfigID')
CREATE NONCLUSTERED INDEX [IDX_InternalmobitelconfigID] ON [dbo].[mobitels] 
(
	[InternalMobitelConfig_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'mobitels', N'COLUMN',N'ConfirmedID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MIN �������� LogID ����� �������� ��������� ��� ��������� ��� ��� ���������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mobitels', @level2type=N'COLUMN',@level2name=N'ConfirmedID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'mobitels', N'COLUMN',N'LastInsertTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UNIX_TIMESTAMP ����� ��������� ������� ������ � DataGPS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mobitels', @level2type=N'COLUMN',@level2name=N'LastInsertTime'
GO
/****** Object:  Table [dbo].[messages]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[messages](
	[Message_ID] [int] IDENTITY(1,1) NOT NULL,
	[Time1] [int] NULL DEFAULT ((0)),
	[Time2] [int] NULL DEFAULT ((0)),
	[Time3] [int] NULL DEFAULT ((0)),
	[Time4] [int] NULL DEFAULT ((0)),
	[isSend] [smallint] NULL,
	[isDelivered] [smallint] NULL,
	[isNew] [smallint] NULL,
	[Direction] [smallint] NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[ServiceInit_ID] [int] NULL DEFAULT ((0)),
	[Command_ID] [smallint] NULL DEFAULT ((0)),
	[CommandMask_ID] [int] NULL DEFAULT ((0)),
	[MessageCounter] [int] NULL DEFAULT ((0)),
	[DataFromService] [char](32) NULL,
	[Address] [char](20) NULL,
	[Source_ID] [int] NULL DEFAULT ((0)),
	[Crc] [smallint] NULL DEFAULT ((1)),
	[SmsId] [int] NULL DEFAULT ((0)),
	[AdvCounter] [int] NOT NULL DEFAULT ((0)),
	[isNewFromMob] [smallint] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_messages] PRIMARY KEY CLUSTERED 
(
	[Message_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[messages] 
(
	[isNewFromMob] ASC,
	[Message_ID] ASC,
	[Time1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND name = N'Index_4')
CREATE NONCLUSTERED INDEX [Index_4] ON [dbo].[messages] 
(
	[Mobitel_ID] ASC,
	[Command_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND name = N'Index_5')
CREATE NONCLUSTERED INDEX [Index_5] ON [dbo].[messages] 
(
	[Mobitel_ID] ASC,
	[MessageCounter] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND name = N'Index_6')
CREATE NONCLUSTERED INDEX [Index_6] ON [dbo].[messages] 
(
	[isNew] ASC,
	[Direction] ASC,
	[ServiceInit_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[md_working_shift]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_working_shift]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_working_shift](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[Start] [time](7) NOT NULL,
	[End] [time](7) NOT NULL,
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_working_shift] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_working_shift', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_working_shift', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_working_shift', N'COLUMN',N'Start'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_working_shift', @level2type=N'COLUMN',@level2name=N'Start'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_working_shift', N'COLUMN',N'End'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_working_shift', @level2type=N'COLUMN',@level2name=N'End'
GO
/****** Object:  Table [dbo].[rt_route]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_route](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_sample] [int] NOT NULL,
	[Id_mobitel] [int] NOT NULL DEFAULT ((0)),
	[Id_driver] [int] NOT NULL DEFAULT ((0)),
	[TimeStartPlan] [datetime] NULL,
	[Distance] [float] NOT NULL DEFAULT ((0)),
	[TimePlanTotal] [varchar](20) NULL,
	[TimeFactTotal] [varchar](20) NULL,
	[Deviation] [varchar](20) NULL,
	[DeviationAr] [varchar](20) NULL,
	[Remark] [varchar](max) NULL,
	[FuelStart] [float] NOT NULL DEFAULT ((0.00)),
	[FuelAdd] [float] NOT NULL DEFAULT ((0.00)),
	[FuelSub] [float] NOT NULL DEFAULT ((0.00)),
	[FuelEnd] [float] NOT NULL DEFAULT ((0.00)),
	[FuelExpens] [float] NOT NULL DEFAULT ((0.00)),
	[FuelExpensAvg] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensMove] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensStop] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensTotal] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensAvg] [float] NOT NULL DEFAULT ((0.00)),
	[TimeMove] [varchar](20) NULL,
	[TimeStop] [varchar](20) NULL,
	[SpeedAvg] [float] NULL DEFAULT ((0.00)),
	[FuelAddQty] [int] NULL DEFAULT ((0)),
	[FuelSubQty] [int] NULL DEFAULT ((0)),
	[PointsValidity] [int] NULL DEFAULT ((0)),
	[PointsCalc] [int] NULL DEFAULT ((0)),
	[PointsFact] [int] NULL DEFAULT ((0)),
	[PointsIntervalMax] [varchar](20) NULL,
	[Type] [smallint] NOT NULL DEFAULT ((1)),
	[TimeEndFact] [datetime] NULL,
	[DistLogicSensor] [float] NULL DEFAULT ((0.00)),
 CONSTRAINT [PK_rt_route] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Id_sample'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������ �������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Id_sample'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Id_mobitel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Id_mobitel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Id_driver'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Id_driver'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimeStartPlan'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeStartPlan'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimePlanTotal'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����� �� �������� (����)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimePlanTotal'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimeFactTotal'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����� �� �������� (����)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeFactTotal'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Deviation'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Deviation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'DeviationAr'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �� ������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'DeviationAr'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Remark'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Remark'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� � ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelAdd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelAdd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelSub'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelSub'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� � �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelEnd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelExpens'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelExpens'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelExpensAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ������,  �/100 ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelExpensAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Fuel_ExpensMove'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������ ������� � ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensMove'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Fuel_ExpensStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������ ������� �� ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Fuel_ExpensTotal'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ����� ������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensTotal'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Fuel_ExpensAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������� ������, �/100 ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimeMove'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeMove'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimeStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'SpeedAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'SpeedAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelAddQty'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ���������� - ���-��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelAddQty'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelSubQty'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� - ���-��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelSubQty'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'PointsValidity'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsValidity'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'PointsCalc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����� (�� Log_ID) ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsCalc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'PointsFact'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����� (�� Log_ID) �����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsFact'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'PointsIntervalMax'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������� ����� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsIntervalMax'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����������� ����� - �� ��� ���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Type'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimeEndFact'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����� ��������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeEndFact'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'DistLogicSensor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ���������� ���������� ��������, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'DistLogicSensor'
GO
/****** Object:  Table [dbo].[rt_events]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_events]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_events](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_rt_events] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[route_points]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_points]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[route_points](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Route_ID] [int] NULL,
	[Zone_ID] [int] NULL,
 CONSTRAINT [PK_route_points] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[route_mobitel_plans]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_mobitel_plans]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[route_mobitel_plans](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](70) NULL,
	[Time1] [int] NULL,
	[Time2] [int] NULL,
 CONSTRAINT [PK_route_mobitel_plans] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[route_mobitel_links]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_mobitel_links]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[route_mobitel_links](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[List_ID] [int] NULL,
	[Route_ID] [int] NULL,
	[Mobitel_ID] [int] NULL,
	[Time1] [int] NULL,
	[Time2] [int] NULL,
 CONSTRAINT [PK_route_mobitel_links] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[route_lists]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_lists]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[route_lists](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Parent] [int] NULL,
	[Title] [varchar](70) NULL,
	[Data] [varchar](max) NULL,
 CONSTRAINT [PK_route_lists] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[route_list]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_list]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[route_list](
	[Route_ID] [int] IDENTITY(1,1) NOT NULL,
	[GIS] [varbinary](max) NULL,
	[Name] [varchar](50) NULL,
	[Descr] [varchar](200) NULL,
	[Modified] [int] NULL,
 CONSTRAINT [PK_route_list] PRIMARY KEY CLUSTERED 
(
	[Route_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[route_items]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_items]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[route_items](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Seq] [int] NULL,
	[Route_ID] [int] NULL,
	[Zone_ID] [int] NULL,
	[Zone_IO] [int] NULL,
	[GoNext_Min] [int] NULL,
	[GoNext_Max] [int] NULL,
	[Link_Item] [int] NULL,
	[Link_Info] [int] NULL,
 CONSTRAINT [PK_route_items] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ntf_main]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_main]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ntf_main](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsActive] [smallint] NOT NULL DEFAULT ((0)),
	[IsPopup] [smallint] NOT NULL DEFAULT ((0)),
	[Title] [varchar](255) NOT NULL DEFAULT (''),
	[IconSmall] [varbinary](max) NULL,
	[IconLarge] [varbinary](max) NULL,
	[SoundName] [varchar](50) NULL,
	[TimeForControl] [int] NULL DEFAULT ((0)),
	[IsEmailActive] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_ntf_main] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_main', N'COLUMN',N'IsEmailActive'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� E-mail �������� �����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_main', @level2type=N'COLUMN',@level2name=N'IsEmailActive'
GO
/****** Object:  Table [dbo].[relationalgorithms]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[relationalgorithms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[relationalgorithms](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AlgorithmID] [int] NOT NULL DEFAULT ((0)),
	[SensorID] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_relationalgorithms] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[relationalgorithms]') AND name = N'ID')
CREATE UNIQUE NONCLUSTERED INDEX [ID] ON [dbo].[relationalgorithms] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rt_sample]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_sample]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_sample](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL DEFAULT (''),
	[Id_mobitel] [int] NULL DEFAULT ((0)),
	[TimeStart] [time](7) NULL,
	[Distance] [float] NULL DEFAULT ((0)),
	[Remark] [varchar](max) NULL,
	[Id_main] [int] NOT NULL DEFAULT ((0)),
	[IsGroupe] [smallint] NOT NULL DEFAULT ((0)),
	[AutoCreate] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_rt_sample] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'Id_mobitel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������� (���)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Id_mobitel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'TimeStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������ �������� (���)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'TimeStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'Remark'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Remark'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������� �������� ������ � ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'IsGroupe'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ - ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'IsGroupe'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'AutoCreate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ����� ��������������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'AutoCreate'
GO
/****** Object:  Table [dbo].[users_reports_list]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_reports_list]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[users_reports_list](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReportType] [varchar](127) NOT NULL DEFAULT (''),
	[ReportCaption] [varchar](127) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_users_reports_list] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[users_list]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_list]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[users_list](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL DEFAULT (''),
	[WinLogin] [varchar](50) NULL,
	[Password] [varbinary](max) NOT NULL,
	[Admin] [smallint] NOT NULL DEFAULT ((0)),
	[Id_role] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_users_list] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'users_list', N'COLUMN',N'Id_role'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ���� ������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users_list', @level2type=N'COLUMN',@level2name=N'Id_role'
GO
/****** Object:  Table [dbo].[users_actions_log]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_actions_log]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[users_actions_log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeLog] [int] NOT NULL DEFAULT ((0)),
	[DateAction] [datetime] NOT NULL,
	[Id_document] [int] NOT NULL DEFAULT ((0)),
	[TextLog] [varchar](255) NOT NULL DEFAULT (''),
	[User] [varchar](127) NOT NULL DEFAULT (''),
	[Computer] [varchar](63) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_users_actions_log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[users_acs_roles]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_acs_roles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[users_acs_roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_users_acs_roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[md_order_priority]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order_priority]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_order_priority](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_order_priority] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[parameters]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[parameters]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[parameters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [int] NOT NULL,
	[Value] [varchar](255) NULL,
 CONSTRAINT [PK_parameters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'parameters', N'COLUMN',N'Number'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'parameters', @level2type=N'COLUMN',@level2name=N'Number'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'parameters', N'COLUMN',N'Value'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'parameters', @level2type=N'COLUMN',@level2name=N'Value'
GO
/****** Object:  Table [dbo].[online]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[online]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[online](
	[DataGps_ID] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NOT NULL DEFAULT ((0)),
	[Longitude] [int] NOT NULL DEFAULT ((0)),
	[Altitude] [int] NULL DEFAULT ((0)),
	[UnixTime] [int] NOT NULL DEFAULT ((0)),
	[Speed] [smallint] NOT NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NOT NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL DEFAULT ((0)),
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [int] NOT NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
 CONSTRAINT [PK_online] PRIMARY KEY CLUSTERED 
(
	[DataGps_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[rt_route_type]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_type]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_route_type](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_rt_route_type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route_type', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route_type', @level2type=N'COLUMN',@level2name=N'Name'
GO
/****** Object:  Table [dbo].[transition]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[transition]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[transition](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InitialStateId] [int] NULL,
	[FinalStateId] [int] NULL,
	[Title] [varchar](50) NOT NULL DEFAULT (''),
	[SensorId] [int] NULL,
	[IconName] [varchar](50) NOT NULL DEFAULT (''),
	[SoundName] [varchar](50) NOT NULL DEFAULT (''),
	[Enabled] [smallint] NOT NULL,
	[ViewInReport] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_transition] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[transition]') AND name = N'Sensors_FK')
CREATE NONCLUSTERED INDEX [Sensors_FK] ON [dbo].[transition] 
(
	[SensorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tmpdatagps]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmpdatagps]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tmpdatagps](
	[DataGps_ID] [int] NOT NULL DEFAULT ((0)),
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NULL DEFAULT ((0)),
	[Longitude] [int] NULL DEFAULT ((0)),
	[Altitude] [int] NULL,
	[UnixTime] [int] NULL DEFAULT ((0)),
	[Speed] [smallint] NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL,
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [int] NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
	[SrvPacketID] [bigint] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'tmpdatagps', N'COLUMN',N'SrvPacketID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID ���������� ������ � ������ �������� ������ ��� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tmpdatagps', @level2type=N'COLUMN',@level2name=N'SrvPacketID'
GO
/****** Object:  Table [dbo].[temp]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[temp](
	[datagps_id] [bigint] NOT NULL DEFAULT ((0)),
	[sensor_id] [bigint] NOT NULL DEFAULT ((0)),
	[Value] [float] NOT NULL DEFAULT ((0.00))
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[telnumbers]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[telnumbers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[telnumbers](
	[TelNumber_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[TelNumber] [char](50) NULL,
 CONSTRAINT [PK_telnumbers] PRIMARY KEY CLUSTERED 
(
	[TelNumber_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[team]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[team]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[team](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Descr] [varchar](max) NULL,
	[Setting_id] [int] NULL,
	[OutLinkId] [varchar](20) NULL,
 CONSTRAINT [PK_team] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[team]') AND name = N'id')
CREATE UNIQUE NONCLUSTERED INDEX [id] ON [dbo].[team] 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[team]') AND name = N'Setting_id')
CREATE NONCLUSTERED INDEX [Setting_id] ON [dbo].[team] 
(
	[Setting_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'team', N'COLUMN',N'Descr'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'team', @level2type=N'COLUMN',@level2name=N'Descr'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'team', N'COLUMN',N'OutLinkId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'team', @level2type=N'COLUMN',@level2name=N'OutLinkId'
GO
/****** Object:  Table [dbo].[swupdate]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[swupdate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[swupdate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ShortID] [char](4) NOT NULL DEFAULT (''),
	[Soft] [varchar](max) NOT NULL,
	[InsertTime] [datetime] NOT NULL DEFAULT ('GETDATE()'),
 CONSTRAINT [PK_swupdate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'swupdate', N'COLUMN',N'ShortID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ���������(�����)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'swupdate', @level2type=N'COLUMN',@level2name=N'ShortID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'swupdate', N'COLUMN',N'Soft'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'swupdate', @level2type=N'COLUMN',@level2name=N'Soft'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'swupdate', N'COLUMN',N'InsertTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ������� � �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'swupdate', @level2type=N'COLUMN',@level2name=N'InsertTime'
GO
/****** Object:  Table [dbo].[state]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[state]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[state](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MinValue] [float] NOT NULL,
	[MaxValue] [float] NOT NULL,
	[Title] [varchar](50) NOT NULL DEFAULT (''),
	[SensorId] [int] NULL,
 CONSTRAINT [PK_state] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[state]') AND name = N'State_FK')
CREATE NONCLUSTERED INDEX [State_FK] ON [dbo].[state] 
(
	[SensorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[specialmessages]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[specialmessages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[specialmessages](
	[SpecMessage_ID] [int] IDENTITY(1,1) NOT NULL,
	[DataString] [char](50) NULL,
	[DataInteger] [int] NULL DEFAULT ((0)),
	[unixtime] [char](50) NULL,
	[isNew] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_specialmessages] PRIMARY KEY CLUSTERED 
(
	[SpecMessage_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sources]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sources]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sources](
	[Source_ID] [int] IDENTITY(1,1) NOT NULL,
	[SourceText] [char](200) NULL,
	[SourceError] [char](200) NULL,
 CONSTRAINT [PK_sources] PRIMARY KEY CLUSTERED 
(
	[Source_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[smsnumbers]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[smsnumbers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[smsnumbers](
	[SmsNumber_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[smsNumber] [char](14) NULL,
 CONSTRAINT [PK_smsnumbers] PRIMARY KEY CLUSTERED 
(
	[SmsNumber_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[setting]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[setting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[setting](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TimeBreak] [time](7) NULL DEFAULT ('00:01:00'),
	[RotationMain] [int] NULL DEFAULT ((100)),
	[RotationAdd] [int] NULL DEFAULT ((100)),
	[FuelingEdge] [int] NULL DEFAULT ((0)),
	[band] [int] NULL DEFAULT ((25)),
	[FuelingDischarge] [int] NULL DEFAULT ((0)),
	[accelMax] [float] NULL DEFAULT ((1)),
	[breakMax] [float] NULL DEFAULT ((1)),
	[speedMax] [float] NULL DEFAULT ((70)),
	[idleRunningMain] [int] NULL DEFAULT ((0)),
	[idleRunningAdd] [int] NULL DEFAULT ((0)),
	[Name] [char](20) NULL DEFAULT ('���������'),
	[Desc] [char](80) NULL DEFAULT ('�������� ���������'),
	[AvgFuelRatePerHour] [float] NOT NULL DEFAULT ((15.00000)),
	[FuelApproximationTime] [time](7) NOT NULL DEFAULT ('00:02:00'),
	[TimeGetFuelAfterStop] [time](7) NOT NULL DEFAULT ('00:00:00'),
	[TimeGetFuelBeforeMotion] [time](7) NOT NULL DEFAULT ('00:00:00'),
	[CzMinCrossingPairTime] [time](7) NOT NULL DEFAULT ('00:01:00'),
	[FuelerRadiusFinds] [int] NOT NULL DEFAULT ((10)),
	[FuelerMaxTimeStop] [time](7) NOT NULL DEFAULT ('00:02:00'),
	[FuelerMinFuelrate] [int] NOT NULL DEFAULT ((1)),
	[FuelerCountInMotion] [int] NOT NULL DEFAULT ((0)),
	[FuelrateWithDischarge] [int] NOT NULL DEFAULT ((0)),
	[FuelingMinMaxAlgorithm] [int] NOT NULL DEFAULT ((0)),
	[TimeBreakMaxPermitted] [time](7) NOT NULL DEFAULT ('03:00:00'),
	[InclinometerMaxAngleAxisX] [float] NOT NULL DEFAULT ((5)),
	[InclinometerMaxAngleAxisY] [float] NOT NULL DEFAULT ((5)),
 CONSTRAINT [PK_setting] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[setting]') AND name = N'id')
CREATE UNIQUE NONCLUSTERED INDEX [id] ON [dbo].[setting] 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'TimeBreak'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'TimeBreak'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'RotationMain'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ��������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'RotationMain'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'RotationAdd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ���. ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'RotationAdd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelingEdge'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelingEdge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'band'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'band'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelingDischarge'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ���� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelingDischarge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'accelMax'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'accelMax'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'breakMax'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'breakMax'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'speedMax'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'speedMax'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'idleRunningMain'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ��� ��������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'idleRunningMain'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'idleRunningAdd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ��� ���. ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'idleRunningAdd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'Desc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'Desc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'AvgFuelRatePerHour'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ������ ������� � ���' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'AvgFuelRatePerHour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelApproximationTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �������� ����� ��������, �� ����� �������� ���������� ������� ������� � ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelApproximationTime'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'TimeGetFuelAfterStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �������� ����� ������� ������� � ������� ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'TimeGetFuelAfterStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'TimeGetFuelBeforeMotion'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �������� ����� ������� ������ ������� � ������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'TimeGetFuelBeforeMotion'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'CzMinCrossingPairTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Min ��������� �������� ����� ����� ������������� ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'CzMinCrossingPairTime'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelerRadiusFinds'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������ ������������ �� ������ �����������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelerRadiusFinds'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelerMaxTimeStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����� �� �������� ���������� ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelerMaxTimeStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelerMinFuelrate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� �������� ��������, ��� ����� �������� � �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelerMinFuelrate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelerCountInMotion'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �� ��������, ��� �� ������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelerCountInMotion'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelrateWithDischarge'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �� � ������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelrateWithDischarge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelingMinMaxAlgorithm'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �������� �� MAX-MIN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelingMinMaxAlgorithm'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'TimeBreakMaxPermitted'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����. ����� ����������� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'TimeBreakMaxPermitted'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'InclinometerMaxAngleAxisX'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ���� ������� ������������ ���������� ��� � ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'InclinometerMaxAngleAxisX'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'InclinometerMaxAngleAxisY'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ���� ������� ������������ ���������� ��� Y ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'InclinometerMaxAngleAxisY'
GO
/****** Object:  Table [dbo].[servicetypes]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[servicetypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[servicetypes](
	[ServiceType_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[Flags] [int] NULL DEFAULT ((0)),
	[D1] [char](200) NULL,
	[D2] [char](200) NULL,
	[D3] [char](200) NULL,
	[D4] [char](200) NULL,
	[D5] [char](200) NULL,
	[D6] [char](200) NULL,
	[D7] [char](200) NULL,
	[D8] [char](200) NULL,
	[D9] [char](200) NULL,
	[D10] [char](200) NULL,
 CONSTRAINT [PK_servicetypes] PRIMARY KEY CLUSTERED 
(
	[ServiceType_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[servicesend]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[servicesend]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[servicesend](
	[ServiceSend_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Descr] [varchar](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[telMobitel] [varchar](64) NULL,
	[smsMobitel] [varchar](64) NULL,
	[emailMobitel] [varchar](64) NULL,
	[IPMobitel] [varchar](64) NULL,
	[URLMobitel] [varchar](64) NULL,
	[PortMobitel] [int] NULL DEFAULT ((0)),
	[RfMobitel] [varchar](50) NULL,
	[Message_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_servicesend] PRIMARY KEY CLUSTERED 
(
	[ServiceSend_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[servicesend]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[servicesend] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[serviceinit]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[serviceinit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[serviceinit](
	[ServiceInit_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[ServiceType_ID] [int] NULL DEFAULT ((0)),
	[A1] [char](50) NULL,
	[A2] [char](50) NULL,
	[A3] [char](50) NULL,
	[A4] [char](50) NULL,
	[A5] [char](50) NULL,
	[A6] [char](50) NULL,
	[A7] [char](50) NULL,
	[A8] [char](50) NULL,
	[A9] [char](50) NULL,
	[A10] [char](200) NULL,
	[CAtoSAint] [int] NULL DEFAULT ((0)),
	[CAtoSAtime] [int] NULL DEFAULT ((0)),
	[SAtoCAint] [int] NULL DEFAULT ((0)),
	[SAtoCAtime] [int] NULL DEFAULT ((0)),
	[SAtoCAtext] [char](50) NULL DEFAULT ('STOPPED'),
	[CAtoSAtext] [char](50) NULL DEFAULT ('STOPPED'),
	[MobitelActive_ID] [int] NOT NULL DEFAULT ((0)),
	[Active] [tinyint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_serviceinit] PRIMARY KEY CLUSTERED 
(
	[ServiceInit_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sensors]    Script Date: 04/11/2013 10:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensors](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mobitel_id] [int] NOT NULL,
	[Name] [varchar](45) NOT NULL DEFAULT (''),
	[Description] [varchar](200) NOT NULL DEFAULT (''),
	[StartBit] [bigint] NOT NULL,
	[Length] [bigint] NOT NULL,
	[NameUnit] [varchar](45) NOT NULL DEFAULT (''),
	[K] [float] NOT NULL DEFAULT ((0.00000)),
 CONSTRAINT [PK_sensors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sensors]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[sensors] 
(
	[mobitel_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sensordata]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensordata]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensordata](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[datagps_id] [bigint] NOT NULL,
	[sensor_id] [bigint] NOT NULL,
	[Value] [float] NOT NULL,
 CONSTRAINT [PK_sensordata] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sensordata]') AND name = N'Index_2')
CREATE UNIQUE NONCLUSTERED INDEX [Index_2] ON [dbo].[sensordata] 
(
	[datagps_id] ASC,
	[sensor_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sensorcoefficient]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensorcoefficient]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensorcoefficient](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Sensor_id] [bigint] NOT NULL,
	[UserValue] [float] NOT NULL,
	[SensorValue] [float] NOT NULL,
	[K] [float] NOT NULL DEFAULT ((1.00000)),
	[b] [float] NOT NULL DEFAULT ((0.00000)),
 CONSTRAINT [PK_sensorcoefficient] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sensorcoefficient]') AND name = N'Index_2')
CREATE UNIQUE NONCLUSTERED INDEX [Index_2] ON [dbo].[sensorcoefficient] 
(
	[Sensor_id] ASC,
	[UserValue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sensoralgorithms]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensoralgorithms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensoralgorithms](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](45) NOT NULL DEFAULT (''),
	[Description] [char](45) NULL,
	[AlgorithmID] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_sensoralgorithms] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sensoralgorithms]') AND name = N'ID')
CREATE UNIQUE NONCLUSTERED INDEX [ID] ON [dbo].[sensoralgorithms] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sensor_list]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensor_list]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensor_list](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](70) NULL,
	[Kind] [varchar](20) NULL,
	[BitsCount] [int] NULL,
	[FirstBit] [int] NULL,
	[Data] [varchar](max) NULL,
 CONSTRAINT [PK_sensor_list] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sensor_hookups]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensor_hookups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensor_hookups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NULL,
	[Sensor_ID] [int] NULL,
	[FirstBit] [int] NULL,
 CONSTRAINT [PK_sensor_hookups] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[rules]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rules]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rules](
	[Rule_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Password] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NOT NULL DEFAULT ((0)),
	[Rule1] [smallint] NOT NULL DEFAULT ((0)),
	[Rule2] [smallint] NOT NULL DEFAULT ((0)),
	[Rule3] [smallint] NOT NULL DEFAULT ((0)),
	[Rule4] [smallint] NOT NULL DEFAULT ((0)),
	[Rule5] [smallint] NOT NULL DEFAULT ((0)),
	[Rule6] [smallint] NOT NULL DEFAULT ((0)),
	[Rule7] [smallint] NOT NULL DEFAULT ((0)),
	[Rule8] [smallint] NOT NULL DEFAULT ((0)),
	[Rule9] [smallint] NOT NULL DEFAULT ((0)),
	[Rule10] [smallint] NOT NULL DEFAULT ((0)),
	[Rule11] [smallint] NOT NULL DEFAULT ((0)),
	[Rule12] [smallint] NOT NULL DEFAULT ((0)),
	[Rule13] [smallint] NOT NULL DEFAULT ((0)),
	[Rule14] [smallint] NOT NULL DEFAULT ((0)),
	[Rule15] [smallint] NOT NULL DEFAULT ((0)),
	[Rule16] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_rules] PRIMARY KEY CLUSTERED 
(
	[Rule_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[agro_price]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_price]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_price](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DateInit] [date] NULL,
	[DateComand] [date] NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_agro_price] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_price', N'COLUMN',N'DateInit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_price', @level2type=N'COLUMN',@level2name=N'DateInit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_price', N'COLUMN',N'DateComand'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ,� ������ ��������� ���� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_price', @level2type=N'COLUMN',@level2name=N'DateComand'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_price', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_price', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_price', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_price', @level2type=N'COLUMN',@level2name=N'Comment'
GO
/****** Object:  Table [dbo].[agro_params]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_params]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_params](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [int] NOT NULL,
	[Value] [varchar](100) NULL,
	[Description] [varchar](50) NULL,
 CONSTRAINT [PK_agro_params] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_params', N'COLUMN',N'Number'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_params', @level2type=N'COLUMN',@level2name=N'Number'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_params', N'COLUMN',N'Value'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_params', @level2type=N'COLUMN',@level2name=N'Value'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_params', N'COLUMN',N'Description'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_params', @level2type=N'COLUMN',@level2name=N'Description'
GO
/****** Object:  Table [dbo].[agro_unit]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_unit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_unit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](100) NOT NULL DEFAULT (''),
	[NameShort] [char](50) NOT NULL DEFAULT (''),
	[Factor] [float] NULL DEFAULT ((0)),
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_agro_unit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_unit', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_unit', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_unit', N'COLUMN',N'NameShort'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_unit', @level2type=N'COLUMN',@level2name=N'NameShort'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_unit', N'COLUMN',N'Factor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ��������� � �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_unit', @level2type=N'COLUMN',@level2name=N'Factor'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_unit', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_unit', @level2type=N'COLUMN',@level2name=N'Comment'
GO
/****** Object:  Table [dbo].[agro_agregat]    Script Date: 04/11/2013 10:56:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_agregat](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[Width] [float] NULL DEFAULT ((0)),
	[Identifier] [int] NULL,
	[Def] [smallint] NULL DEFAULT ((0)),
	[Comment] [varchar](max) NULL,
	[Id_main] [int] NOT NULL DEFAULT ((0)),
	[IsGroupe] [smallint] NOT NULL DEFAULT ((0)),
	[InvNumber] [char](50) NULL,
	[Id_work] [int] NOT NULL DEFAULT ((0)),
	[OutLinkId] [varchar](20) NULL,
	[HasSensor] [smallint] NULL DEFAULT ((0)),
 CONSTRAINT [PK_agro_agregat] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Width'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ��������� ������������ ��� �������� ������������ ������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Width'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Identifier'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'10 ������ ������������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Identifier'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Def'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �� ��������� ��� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Def'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Comment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������� �������� ������ � ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'IsGroupe'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ - ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'IsGroupe'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'InvNumber'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'InvNumber'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Id_work'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� �� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Id_work'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'OutLinkId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'OutLinkId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'HasSensor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������ ������������� �������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'HasSensor'
GO
/****** Object:  Table [dbo].[agro_culture]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_culture]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_culture](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[Icon] [varbinary](max) NULL,
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_agro_culture] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_culture', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_culture', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_culture', N'COLUMN',N'Icon'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_culture', @level2type=N'COLUMN',@level2name=N'Icon'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_culture', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_culture', @level2type=N'COLUMN',@level2name=N'Comment'
GO
/****** Object:  Table [dbo].[agro_field]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_field]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_field](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NULL,
	[Id_zone] [int] NOT NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_agro_field] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_field]') AND name = N'Id_main_Field_FK1')
CREATE NONCLUSTERED INDEX [Id_main_Field_FK1] ON [dbo].[agro_field] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_field', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������ �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_field', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_field', N'COLUMN',N'Id_zone'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_field', @level2type=N'COLUMN',@level2name=N'Id_zone'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_field', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_field', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_field', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_field', @level2type=N'COLUMN',@level2name=N'Comment'
GO
/****** Object:  Table [dbo].[agro_fueling]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fueling]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_fueling](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NULL,
	[LockRecord] [smallint] NOT NULL DEFAULT ((0)),
	[Lat] [float] NOT NULL DEFAULT ((0)),
	[Lng] [float] NOT NULL DEFAULT ((0)),
	[DateFueling] [datetime] NOT NULL,
	[ValueSystem] [float] NOT NULL DEFAULT ((0)),
	[ValueDisp] [float] NOT NULL DEFAULT ((0)),
	[ValueStart] [float] NOT NULL DEFAULT ((0)),
	[Location] [varchar](255) NULL,
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_agro_fueling] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'LockRecord'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������������� ��� ��������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'LockRecord'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'DateFueling'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� �������� / �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'DateFueling'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'ValueSystem'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'ValueSystem'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'ValueDisp'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'ValueDisp'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'ValueStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ����� ��������� / ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'ValueStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'Remark'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� � ��������� �������� �����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'Remark'
GO
/****** Object:  Table [dbo].[agro_fieldgroupe]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldgroupe]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_fieldgroupe](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[Comment] [varchar](max) NULL,
	[Id_Zonesgroup] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_agro_fieldgroupe] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldgroupe', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldgroupe', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldgroupe', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldgroupe', @level2type=N'COLUMN',@level2name=N'Comment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldgroupe', N'COLUMN',N'Id_Zonesgroup'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ����������� ��� �� ��������� ������� ������� ������ �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldgroupe', @level2type=N'COLUMN',@level2name=N'Id_Zonesgroup'
GO
/****** Object:  Table [dbo].[md_object_group]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_object_group]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_object_group](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_object_group] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[md_enterprise]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_enterprise]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_enterprise](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL DEFAULT (''),
	[Current] [smallint] NOT NULL DEFAULT ((0)),
	[Dispartchable] [smallint] NOT NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_enterprise] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_enterprise', N'COLUMN',N'Current'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ����������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_enterprise', @level2type=N'COLUMN',@level2name=N'Current'
GO
/****** Object:  Table [dbo].[md_cargo]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_cargo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_cargo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Unit] [varchar](10) NULL,
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_cargo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_cargo', N'COLUMN',N'Unit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_cargo', @level2type=N'COLUMN',@level2name=N'Unit'
GO
/****** Object:  Table [dbo].[md_alarming_types]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_alarming_types]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_alarming_types](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_alarming_types] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mapstylepoints]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstylepoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mapstylepoints](
	[MapStylePoint_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[isBitmap] [smallint] NULL,
	[SymbolCode] [smallint] NULL DEFAULT ((0)),
	[Color] [int] NULL DEFAULT ((0)),
	[Font] [char](200) NULL,
	[Size] [int] NULL DEFAULT ((0)),
	[Rotation] [int] NULL DEFAULT ((0)),
	[BitmapName] [char](200) NULL,
	[BitmapStyle] [smallint] NULL DEFAULT ((0)),
	[Style] [smallint] NULL DEFAULT ((0)),
 CONSTRAINT [PK_mapstylepoints] PRIMARY KEY CLUSTERED 
(
	[MapStylePoint_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mapstylelines]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstylelines]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mapstylelines](
	[MapStyleLine_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Color] [int] NULL DEFAULT ((0)),
	[Width] [smallint] NULL DEFAULT ((0)),
	[Style] [smallint] NULL DEFAULT ((0)),
 CONSTRAINT [PK_mapstylelines] PRIMARY KEY CLUSTERED 
(
	[MapStyleLine_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mapstyleglobal]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstyleglobal]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mapstyleglobal](
	[MapStyleGlobal_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[DefaultLineStyle_ID] [int] NULL DEFAULT ((0)),
	[DefaulPointStyle_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle1_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle2_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle3_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle4_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle5_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle6_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle7_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle8_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle9_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle10_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle11_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle12_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle13_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle14_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle15_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle16_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle17_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle18_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle19_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle20_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle21_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle22_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle23_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle24_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle25_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle26_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle27_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle28_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle29_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle30_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle31_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle32_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle1_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle2_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle3_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle4_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle5_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle6_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle7_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle8_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_mapstyleglobal] PRIMARY KEY CLUSTERED 
(
	[MapStyleGlobal_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasttimes]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lasttimes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[lasttimes](
	[LastTime_ID] [int] NULL DEFAULT ((0)),
	[ID] [int] NULL DEFAULT ((0)),
	[Name] [char](200) NULL,
	[Descr] [char](200) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lastrecords]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lastrecords]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[lastrecords](
	[LastRecord_ID] [int] NULL DEFAULT ((0)),
	[ID] [int] NULL DEFAULT ((0)),
	[Name] [char](200) NULL,
	[Descr] [char](200) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lastmeters]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lastmeters]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[lastmeters](
	[LastMeter_ID] [int] NULL DEFAULT ((0)),
	[ID] [int] NULL DEFAULT ((0)),
	[Name] [char](200) NULL,
	[Descr] [char](200) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[internalmobitelconfig]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[internalmobitelconfig]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[internalmobitelconfig](
	[InternalMobitelConfig_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[devIdLong] [char](16) NULL,
	[devIdShort] [char](4) NULL,
	[verProtocolLong] [char](16) NULL,
	[verProtocolShort] [char](2) NULL,
	[moduleIdGps] [char](50) NULL,
	[moduleIdGsm] [char](50) NULL,
	[moduleIdRf] [char](50) NULL,
	[RfType] [smallint] NOT NULL DEFAULT ((0)),
	[ActiveRf] [smallint] NOT NULL DEFAULT ((0)),
	[moduleIdSs] [char](50) NULL,
	[moduleIdMm] [char](50) NOT NULL DEFAULT ('moduleIdMm'),
 CONSTRAINT [PK_internalmobitelconfig] PRIMARY KEY CLUSTERED 
(
	[InternalMobitelConfig_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[internalmobitelconfig]') AND name = N'IDX_InternalmobitelconfigID')
CREATE NONCLUSTERED INDEX [IDX_InternalmobitelconfigID] ON [dbo].[internalmobitelconfig] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[internalmobitelconfig]') AND name = N'Index_3')
CREATE NONCLUSTERED INDEX [Index_3] ON [dbo].[internalmobitelconfig] 
(
	[Message_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[internalmobitelconfig]') AND name = N'Index_4')
CREATE NONCLUSTERED INDEX [Index_4] ON [dbo].[internalmobitelconfig] 
(
	[devIdShort] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gpsmasks]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gpsmasks]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[gpsmasks](
	[GpsMask_ID] [int] IDENTITY(1,1) NOT NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Name] [char](200) NULL,
	[Descr] [char](200) NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[LastRecords] [int] NULL DEFAULT ((0)),
	[LastTimes] [int] NULL DEFAULT ((0)),
	[LastTimesIndex] [int] NULL DEFAULT ((0)),
	[LastMeters] [int] NULL DEFAULT ((0)),
	[LastMetersIndex] [int] NULL DEFAULT ((0)),
	[MaxSmsNum] [int] NULL DEFAULT ((0)),
	[MaxSmsNumIndex] [int] NULL DEFAULT ((0)),
	[LatitudeStart] [int] NULL DEFAULT ((0)),
	[LatitudeEnd] [int] NULL DEFAULT ((0)),
	[LongitudeStart] [int] NULL DEFAULT ((0)),
	[LongitudeEnd] [int] NULL DEFAULT ((0)),
	[AltitudeStart] [int] NULL DEFAULT ((0)),
	[AltitudeEnd] [int] NULL DEFAULT ((0)),
	[UnixTimeStart] [int] NULL DEFAULT ((0)),
	[UnixTimeEnd] [int] NULL DEFAULT ((0)),
	[SpeedStart] [int] NULL DEFAULT ((0)),
	[SpeedEnd] [int] NULL DEFAULT ((0)),
	[DirectionStart] [int] NULL DEFAULT ((0)),
	[DirectionEnd] [int] NULL DEFAULT ((0)),
	[NumbersStart] [int] NULL DEFAULT ((0)),
	[NumbersEnd] [int] NULL DEFAULT ((0)),
	[WhatSend] [int] NULL DEFAULT ((0)),
	[CheckMask] [int] NULL DEFAULT ((0)),
	[InMobitelID] [int] NULL DEFAULT ((0)),
	[Events] [int] NULL DEFAULT ((0)),
	[Number1] [int] NULL DEFAULT ((0)),
	[Number2] [int] NULL DEFAULT ((0)),
	[Number3] [int] NULL DEFAULT ((0)),
	[Number4] [int] NULL DEFAULT ((0)),
	[Number5] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_gpsmasks] PRIMARY KEY CLUSTERED 
(
	[GpsMask_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[driver_types]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[driver_types]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[driver_types](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [varchar](50) NULL,
 CONSTRAINT [PK_driver_types] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[driver_types]') AND name = N'id')
CREATE UNIQUE NONCLUSTERED INDEX [id] ON [dbo].[driver_types] 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'driver_types', N'COLUMN',N'TypeName'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'driver_types', @level2type=N'COLUMN',@level2name=N'TypeName'
GO
/****** Object:  Table [dbo].[driver]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[driver]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[driver](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Family] [char](40) NULL,
	[Name] [char](40) NULL,
	[ByrdDay] [date] NULL,
	[Category] [char](5) NULL,
	[Permis] [char](20) NULL,
	[foto] [varbinary](max) NULL,
	[Identifier] [int] NULL,
	[OutLinkId] [varchar](20) NULL,
	[idType] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_driver] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[driver]') AND name = N'id')
CREATE UNIQUE NONCLUSTERED INDEX [id] ON [dbo].[driver] 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'driver', N'COLUMN',N'Family'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'driver', @level2type=N'COLUMN',@level2name=N'Family'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'driver', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'driver', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'driver', N'COLUMN',N'ByrdDay'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'driver', @level2type=N'COLUMN',@level2name=N'ByrdDay'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'driver', N'COLUMN',N'Category'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'driver', @level2type=N'COLUMN',@level2name=N'Category'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'driver', N'COLUMN',N'Permis'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'driver', @level2type=N'COLUMN',@level2name=N'Permis'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'driver', N'COLUMN',N'foto'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'driver', @level2type=N'COLUMN',@level2name=N'foto'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'driver', N'COLUMN',N'Identifier'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'10 ������ ������������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'driver', @level2type=N'COLUMN',@level2name=N'Identifier'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'driver', N'COLUMN',N'OutLinkId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'driver', @level2type=N'COLUMN',@level2name=N'OutLinkId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'driver', N'COLUMN',N'idType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'driver', @level2type=N'COLUMN',@level2name=N'idType'
GO
/****** Object:  Table [dbo].[datagpslosted]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslosted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpslosted](
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[Begin_LogID] [int] NOT NULL DEFAULT ((0)),
	[End_LogID] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_datagpslosted] PRIMARY KEY CLUSTERED 
(
	[Mobitel_ID] ASC,
	[Begin_LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[datagpslost_rftmp]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_rftmp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpslost_rftmp](
	[LogID] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[datagpslost_ontmp]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_ontmp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpslost_ontmp](
	[LogID] [int] NOT NULL DEFAULT ((0)),
	[SrvPacketID] [bigint] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[datagpslost_on]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_on]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpslost_on](
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[Begin_LogID] [int] NOT NULL DEFAULT ((0)),
	[End_LogID] [int] NOT NULL DEFAULT ((0)),
	[Begin_SrvPacketID] [bigint] NOT NULL DEFAULT ((0)),
	[End_SrvPacketID] [bigint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_datagpslost_on] PRIMARY KEY CLUSTERED 
(
	[Mobitel_ID] ASC,
	[Begin_LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_on]') AND name = N'IDX_MobitelID')
CREATE NONCLUSTERED INDEX [IDX_MobitelID] ON [dbo].[datagpslost_on] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_on]') AND name = N'IDX_MobitelID_BeginSrvPacketID')
CREATE UNIQUE NONCLUSTERED INDEX [IDX_MobitelID_BeginSrvPacketID] ON [dbo].[datagpslost_on] 
(
	[Mobitel_ID] ASC,
	[Begin_SrvPacketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagpslost_on', N'COLUMN',N'Begin_LogID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� LogID ����� �������� ���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagpslost_on', @level2type=N'COLUMN',@level2name=N'Begin_LogID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagpslost_on', N'COLUMN',N'End_LogID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� LogID ����� ������� ����������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagpslost_on', @level2type=N'COLUMN',@level2name=N'End_LogID'
GO
/****** Object:  Table [dbo].[datagpslost]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpslost](
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[Begin_LogID] [int] NOT NULL DEFAULT ((0)),
	[End_LogID] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_datagpslost] PRIMARY KEY CLUSTERED 
(
	[Mobitel_ID] ASC,
	[Begin_LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost]') AND name = N'IDX_MobitelID')
CREATE NONCLUSTERED INDEX [IDX_MobitelID] ON [dbo].[datagpslost] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagpslost', N'COLUMN',N'Begin_LogID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� LogID ����� �������� ���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagpslost', @level2type=N'COLUMN',@level2name=N'Begin_LogID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagpslost', N'COLUMN',N'End_LogID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� LogID ����� ������� ����������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagpslost', @level2type=N'COLUMN',@level2name=N'End_LogID'
GO
/****** Object:  Table [dbo].[datagpsbufferlite]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbufferlite]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbufferlite](
	[DataGps_ID] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NULL DEFAULT ((0)),
	[Longitude] [int] NULL DEFAULT ((0)),
	[Altitude] [int] NULL,
	[UnixTime] [int] NULL DEFAULT ((0)),
	[Speed] [smallint] NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL,
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [smallint] NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
 CONSTRAINT [PK_datagpsbufferlite] PRIMARY KEY CLUSTERED 
(
	[DataGps_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[datagpsbuffer_pop]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_pop]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer_pop](
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NOT NULL DEFAULT ((0)),
	[Longitude] [int] NOT NULL DEFAULT ((0)),
	[Altitude] [int] NULL DEFAULT ((0)),
	[UnixTime] [int] NOT NULL DEFAULT ((0)),
	[Speed] [smallint] NOT NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NOT NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL DEFAULT ((0)),
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [int] NOT NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1))
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_pop]') AND name = N'IDX_MobitelIDLogID')
CREATE NONCLUSTERED INDEX [IDX_MobitelIDLogID] ON [dbo].[datagpsbuffer_pop] 
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_pop]') AND name = N'IDX_MobitelidUnixtime')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtime] ON [dbo].[datagpsbuffer_pop] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_pop]') AND name = N'IDX_MobitelidUnixtimeValid')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtimeValid] ON [dbo].[datagpsbuffer_pop] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC,
	[Valid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[datagpsbuffer_ontmp]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_ontmp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer_ontmp](
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[LogID] [int] NOT NULL DEFAULT ((0)),
	[SrvPacketID] [bigint] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_ontmp]') AND name = N'IDX_Mobitelid')
CREATE NONCLUSTERED INDEX [IDX_Mobitelid] ON [dbo].[datagpsbuffer_ontmp] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_ontmp]') AND name = N'IDX_MobitelidSrvpacketid')
CREATE NONCLUSTERED INDEX [IDX_MobitelidSrvpacketid] ON [dbo].[datagpsbuffer_ontmp] 
(
	[Mobitel_ID] ASC,
	[SrvPacketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[datagpsbuffer_on]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer_on](
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NOT NULL DEFAULT ((0)),
	[Longitude] [int] NOT NULL DEFAULT ((0)),
	[Altitude] [int] NULL,
	[UnixTime] [int] NOT NULL DEFAULT ((0)),
	[Speed] [smallint] NOT NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NOT NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL,
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NOT NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [smallint] NOT NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
	[SrvPacketID] [bigint] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND name = N'IDX_MobitelidLogid')
CREATE NONCLUSTERED INDEX [IDX_MobitelidLogid] ON [dbo].[datagpsbuffer_on] 
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND name = N'IDX_MobitelidUnixtime')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtime] ON [dbo].[datagpsbuffer_on] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND name = N'IDX_MobitelidUnixtimeValid')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtimeValid] ON [dbo].[datagpsbuffer_on] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC,
	[Valid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND name = N'IDX_SrvPacketID')
CREATE NONCLUSTERED INDEX [IDX_SrvPacketID] ON [dbo].[datagpsbuffer_on] 
(
	[SrvPacketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[datagpsbuffer_dr]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_dr]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer_dr](
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NOT NULL DEFAULT ((0)),
	[Longitude] [int] NOT NULL DEFAULT ((0)),
	[Altitude] [int] NULL DEFAULT ((0)),
	[UnixTime] [int] NOT NULL DEFAULT ((0)),
	[Speed] [smallint] NOT NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NOT NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL DEFAULT ((0)),
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [int] NOT NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1))
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_dr]') AND name = N'IDX_MobitelIDLogID')
CREATE NONCLUSTERED INDEX [IDX_MobitelIDLogID] ON [dbo].[datagpsbuffer_dr] 
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_dr]') AND name = N'IDX_MobitelidUnixtime')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtime] ON [dbo].[datagpsbuffer_dr] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_dr]') AND name = N'IDX_MobitelidUnixtimeValid')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtimeValid] ON [dbo].[datagpsbuffer_dr] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC,
	[Valid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[datagpsbuffer]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer](
	[DataGps_ID] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NULL DEFAULT ((0)),
	[Longitude] [int] NULL DEFAULT ((0)),
	[Altitude] [int] NULL,
	[UnixTime] [int] NULL DEFAULT ((0)),
	[Speed] [smallint] NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL,
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [smallint] NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
 CONSTRAINT [PK_datagpsbuffer] PRIMARY KEY CLUSTERED 
(
	[DataGps_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer]') AND name = N'IDX_MobitelIDLogID')
CREATE NONCLUSTERED INDEX [IDX_MobitelIDLogID] ON [dbo].[datagpsbuffer] 
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[datagps]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagps](
	[DataGps_ID] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NULL DEFAULT ((0)),
	[Longitude] [int] NULL DEFAULT ((0)),
	[Altitude] [int] NULL,
	[UnixTime] [int] NULL DEFAULT ((0)),
	[Speed] [smallint] NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL,
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [int] NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
	[SrvPacketID] [bigint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_datagps] PRIMARY KEY CLUSTERED 
(
	[DataGps_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND name = N'IDX_MobitelIDSrvPacketID')
CREATE NONCLUSTERED INDEX [IDX_MobitelIDSrvPacketID] ON [dbo].[datagps] 
(
	[Mobitel_ID] ASC,
	[SrvPacketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[datagps] 
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND name = N'Index_3')
CREATE NONCLUSTERED INDEX [Index_3] ON [dbo].[datagps] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC,
	[Valid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND name = N'Index_4')
CREATE NONCLUSTERED INDEX [Index_4] ON [dbo].[datagps] 
(
	[Message_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND name = N'Index_5')
CREATE NONCLUSTERED INDEX [Index_5] ON [dbo].[datagps] 
(
	[Mobitel_ID] ASC,
	[Counter1] ASC,
	[Counter2] ASC,
	[Counter3] ASC,
	[Counter4] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps', N'COLUMN',N'SrvPacketID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID ���������� ������ � ������ �������� ������ ��� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps', @level2type=N'COLUMN',@level2name=N'SrvPacketID'
GO
/****** Object:  Table [dbo].[data_zones]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[data_zones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[data_zones](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataGps_ID] [int] NULL,
	[Zone_ID] [int] NULL,
	[Zone_IO] [int] NULL,
 CONSTRAINT [PK_data_zones] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[configzoneset]    Script Date: 04/11/2013 10:56:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configzoneset]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configzoneset](
	[ConfigZoneSet_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_configzoneset] PRIMARY KEY CLUSTERED 
(
	[ConfigZoneSet_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[configunique]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configunique]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configunique](
	[ConfigUnique_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[pswd] [char](8) NULL,
	[tmpPswd] [char](8) NULL,
	[dsptId] [char](4) NULL,
 CONSTRAINT [PK_configunique] PRIMARY KEY CLUSTERED 
(
	[ConfigUnique_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configunique]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configunique] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[configtel]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configtel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configtel](
	[ConfigTel_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[telSOS] [int] NULL DEFAULT ((0)),
	[telDspt] [int] NULL DEFAULT ((0)),
	[telAccept1] [int] NULL DEFAULT ((0)),
	[telAccept2] [int] NULL DEFAULT ((0)),
	[telAccept3] [int] NULL DEFAULT ((0)),
	[telAnswer] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_configtel] PRIMARY KEY CLUSTERED 
(
	[ConfigTel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configtel]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configtel] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[configsms]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configsms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configsms](
	[ConfigSms_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[smsCenter_ID] [int] NULL DEFAULT ((0)),
	[smsDspt_ID] [int] NULL DEFAULT ((0)),
	[smsEmailGate_ID] [int] NULL DEFAULT ((0)),
	[dsptEmail] [char](14) NULL,
	[dsptGprsEmail] [char](50) NULL,
 CONSTRAINT [PK_configsms] PRIMARY KEY CLUSTERED 
(
	[ConfigSms_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configsms]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configsms] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[configsensorset]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configsensorset]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configsensorset](
	[ConfigSensorSet_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_configsensorset] PRIMARY KEY CLUSTERED 
(
	[ConfigSensorSet_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[configother]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configother]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configother](
	[ConfigOther_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Pin] [int] NULL DEFAULT ((0)),
	[Lang] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_configother] PRIMARY KEY CLUSTERED 
(
	[ConfigOther_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configother]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configother] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[configmain]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configmain]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configmain](
	[ConfigMain_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[ConfigOther_ID] [int] NULL DEFAULT ((0)),
	[ConfigDriverMessage_ID] [int] NULL DEFAULT ((0)),
	[ConfigEvent_ID] [int] NULL DEFAULT ((0)),
	[ConfigUnique_ID] [int] NULL DEFAULT ((0)),
	[ConfigZoneSet_ID] [int] NULL DEFAULT ((0)),
	[ConfigSensorSet_ID] [int] NULL DEFAULT ((0)),
	[ConfigTel_ID] [int] NULL DEFAULT ((0)),
	[ConfigSms_ID] [int] NULL DEFAULT ((0)),
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[ConfigGprsMain_ID] [int] NOT NULL DEFAULT ((0)),
	[ConfigGprsEmail_ID] [int] NOT NULL DEFAULT ((0)),
	[ConfigGprsInit_ID] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_configmain] PRIMARY KEY CLUSTERED 
(
	[ConfigMain_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configmain]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configmain] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[configgprsmain]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsmain]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configgprsmain](
	[ConfigGprsMain_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Descr] [varchar](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Mode] [int] NOT NULL DEFAULT ((0)),
	[Apnserv] [varchar](50) NOT NULL DEFAULT (''),
	[Apnun] [varchar](50) NOT NULL DEFAULT (''),
	[Apnpw] [varchar](50) NOT NULL DEFAULT (''),
	[Dnsserv1] [varchar](50) NOT NULL DEFAULT (''),
	[Dialn1] [varchar](50) NOT NULL DEFAULT (''),
	[Ispun] [varchar](50) NOT NULL DEFAULT (''),
	[Isppw] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_configgprsmain] PRIMARY KEY CLUSTERED 
(
	[ConfigGprsMain_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[configgprsinit]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsinit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configgprsinit](
	[ConfigGprsInit_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Descr] [varchar](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Domain] [varchar](50) NOT NULL DEFAULT ('0'),
	[InitString] [varchar](200) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_configgprsinit] PRIMARY KEY CLUSTERED 
(
	[ConfigGprsInit_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configgprsinit]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configgprsinit] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[configgprsemail]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsemail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configgprsemail](
	[ConfigGprsEmail_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Descr] [varchar](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Smtpserv] [varchar](50) NOT NULL DEFAULT (''),
	[Smtpun] [varchar](50) NOT NULL DEFAULT (''),
	[Smtppw] [varchar](50) NOT NULL DEFAULT (''),
	[Pop3serv] [varchar](50) NOT NULL DEFAULT (''),
	[Pop3un] [varchar](50) NOT NULL DEFAULT (''),
	[Pop3pw] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_configgprsemail] PRIMARY KEY CLUSTERED 
(
	[ConfigGprsEmail_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configgprsemail]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configgprsemail] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[configevent]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configevent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configevent](
	[ConfigEvent_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[tmr1Log] [int] NULL DEFAULT ((0)),
	[tmr2Send] [int] NULL DEFAULT ((0)),
	[tmr3Zone] [int] NULL DEFAULT ((0)),
	[dist1Log] [int] NULL DEFAULT ((0)),
	[dist2Send] [int] NULL DEFAULT ((0)),
	[dist3Zone] [int] NULL DEFAULT ((0)),
	[deltaTimeZone] [int] NULL DEFAULT ((0)),
	[gprsEmail] [int] NOT NULL DEFAULT ((0)),
	[gprsFtp] [int] NOT NULL DEFAULT ((0)),
	[gprsSocket] [int] NOT NULL DEFAULT ((0)),
	[maskSensor] [char](50) NULL,
	[maskEvent1] [int] NULL DEFAULT ((0)),
	[maskEvent2] [int] NULL DEFAULT ((0)),
	[maskEvent3] [int] NULL DEFAULT ((0)),
	[maskEvent4] [int] NULL DEFAULT ((0)),
	[maskEvent5] [int] NULL DEFAULT ((0)),
	[maskEvent6] [int] NULL DEFAULT ((0)),
	[maskEvent7] [int] NULL DEFAULT ((0)),
	[maskEvent8] [int] NULL DEFAULT ((0)),
	[maskEvent9] [int] NULL DEFAULT ((0)),
	[maskEvent10] [int] NULL DEFAULT ((0)),
	[maskEvent11] [int] NULL DEFAULT ((0)),
	[maskEvent12] [int] NULL DEFAULT ((0)),
	[maskEvent13] [int] NULL DEFAULT ((0)),
	[maskEvent14] [int] NULL DEFAULT ((0)),
	[maskEvent15] [int] NULL DEFAULT ((0)),
	[maskEvent16] [int] NULL DEFAULT ((0)),
	[maskEvent17] [int] NULL DEFAULT ((0)),
	[maskEvent18] [int] NULL DEFAULT ((0)),
	[maskEvent19] [int] NULL DEFAULT ((0)),
	[maskEvent20] [int] NULL DEFAULT ((0)),
	[maskEvent21] [int] NULL DEFAULT ((0)),
	[maskEvent22] [int] NULL DEFAULT ((0)),
	[maskEvent23] [int] NULL DEFAULT ((0)),
	[maskEvent24] [int] NULL DEFAULT ((0)),
	[maskEvent25] [int] NULL DEFAULT ((0)),
	[maskEvent26] [int] NULL DEFAULT ((0)),
	[maskEvent27] [int] NULL DEFAULT ((0)),
	[maskEvent28] [int] NULL DEFAULT ((0)),
	[maskEvent29] [int] NULL DEFAULT ((0)),
	[maskEvent30] [int] NULL DEFAULT ((0)),
	[maskEvent31] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_configevent] PRIMARY KEY CLUSTERED 
(
	[ConfigEvent_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configevent]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configevent] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[configdrivermessage]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configdrivermessage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configdrivermessage](
	[ConfigDriverMessage_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[DriverMessage] [char](200) NULL,
 CONSTRAINT [PK_configdrivermessage] PRIMARY KEY CLUSTERED 
(
	[ConfigDriverMessage_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configdrivermessage]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configdrivermessage] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[commands]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[commands]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[commands](
	[ID] [int] NULL DEFAULT ((0)),
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[agro_workgroup]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_workgroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_workgroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_agro_workgroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_workgroup', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_workgroup', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_workgroup', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_workgroup', @level2type=N'COLUMN',@level2name=N'Comment'
GO
/****** Object:  Table [dbo].[agro_work_types]    Script Date: 04/11/2013 10:56:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_work_types]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_work_types](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_agro_work_types] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[zonerelations]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zonerelations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zonerelations](
	[ZoneRelation_ID] [int] IDENTITY(1,1) NOT NULL,
	[Zone_ID] [int] NULL DEFAULT ((0)),
	[ConfigZoneSet_ID] [int] NULL DEFAULT ((0)),
	[In_flag] [smallint] NOT NULL DEFAULT ((0)),
	[Out_flag] [smallint] NOT NULL DEFAULT ((0)),
	[In_flag2] [smallint] NOT NULL DEFAULT ((0)),
	[Out_flag2] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_zonerelations] PRIMARY KEY CLUSTERED 
(
	[ZoneRelation_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ver]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ver]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ver](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Num] [bigint] NOT NULL DEFAULT ((0)),
	[Desc] [varchar](max) NOT NULL,
	[time] [datetime] NULL,
 CONSTRAINT [PK_ver] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ver', N'COLUMN',N'Num'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������� ������ ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ver', @level2type=N'COLUMN',@level2name=N'Num'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ver', N'COLUMN',N'Desc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ������� ������ ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ver', @level2type=N'COLUMN',@level2name=N'Desc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ver', N'COLUMN',N'time'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ver', @level2type=N'COLUMN',@level2name=N'time'
GO
/****** Object:  Table [dbo].[vehicle_state]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle_state]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[vehicle_state](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[MaxTime] [decimal](10, 2) NOT NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_vehicle_state] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vehicle_category]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle_category]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[vehicle_category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[Tonnage] [decimal](10, 3) NOT NULL DEFAULT ((0)),
	[FuelNormMoving] [float] NOT NULL DEFAULT ((0)),
	[FuelNormParking] [float] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_vehicle_category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle_category', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ������������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle_category', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle_category', N'COLUMN',N'Tonnage'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������������,  ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle_category', @level2type=N'COLUMN',@level2name=N'Tonnage'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle_category', N'COLUMN',N'FuelNormMoving'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������� ������� � �������� �/100 ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle_category', @level2type=N'COLUMN',@level2name=N'FuelNormMoving'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle_category', N'COLUMN',N'FuelNormParking'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������� ������� �� �������� �������� �/���' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle_category', @level2type=N'COLUMN',@level2name=N'FuelNormParking'
GO
/****** Object:  Table [dbo].[zonesgroup]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zonesgroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zonesgroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL DEFAULT (''),
	[Description] [varchar](200) NOT NULL DEFAULT (''),
	[OutLinkId] [varchar](20) NULL,
 CONSTRAINT [PK_zonesgroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zonesgroup', N'COLUMN',N'OutLinkId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zonesgroup', @level2type=N'COLUMN',@level2name=N'OutLinkId'
GO
/****** Object:  Table [dbo].[zones]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zones](
	[Zone_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Status] [smallint] NULL DEFAULT ((0)),
	[Square] [float] NULL DEFAULT ((0.00000)),
	[ZoneColor] [int] NULL DEFAULT ((0)),
	[TextColor] [int] NULL DEFAULT ((0)),
	[TextFontName] [char](50) NULL DEFAULT ('Microsoft Sans Serif'),
	[TextFontSize] [float] NULL DEFAULT ((25.00)),
	[TextFontParams] [char](8) NULL DEFAULT ('0;0;0;0;'),
	[Icon] [varbinary](max) NULL,
	[HatchStyle] [int] NULL DEFAULT ((0)),
	[DateCreate] [datetime] NULL,
	[Id_main] [int] NULL DEFAULT ((0)),
	[Level] [smallint] NULL DEFAULT ((1)),
	[StyleId] [bigint] NOT NULL DEFAULT ((0)),
	[ZonesGroupId] [bigint] NOT NULL DEFAULT ((1)),
	[OutLinkId] [varchar](20) NULL,
 CONSTRAINT [PK_zones] PRIMARY KEY CLUSTERED 
(
	[Zone_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[zones]') AND name = N'FK_zones_zonesgroup')
CREATE NONCLUSTERED INDEX [FK_zones_zonesgroup] ON [dbo].[zones] 
(
	[ZonesGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'Square'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'Square'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'ZoneColor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'ZoneColor'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'TextColor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'TextColor'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'TextFontName'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'TextFontName'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'TextFontSize'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������� ������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'TextFontSize'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'TextFontParams'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ������� ������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'TextFontParams'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'Icon'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'Icon'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'HatchStyle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'HatchStyle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'DateCreate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'DateCreate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������� �������� ������ � ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'Level'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� � ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'Level'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'StyleId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID ����� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'StyleId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'ZonesGroupId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID ������ ���' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'ZonesGroupId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'OutLinkId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'OutLinkId'
GO
/****** Object:  Table [dbo].[vehicle]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[vehicle](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[MakeCar] [varchar](max) NULL,
	[NumberPlate] [varchar](max) NULL,
	[Team_id] [int] NULL,
	[Mobitel_id] [int] NULL,
	[Odometr] [int] NULL DEFAULT ((0)),
	[CarModel] [varchar](max) NULL,
	[setting_id] [int] NULL DEFAULT ((1)),
	[odoTime] [datetime] NULL,
	[driver_id] [int] NULL DEFAULT ((-1)),
	[State] [int] NULL DEFAULT ((2)),
	[OutLinkId] [varchar](20) NULL,
	[Category_id] [int] NULL,
 CONSTRAINT [PK_vehicle] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'FK_vehicle_mobitels_Mobitel_ID')
CREATE NONCLUSTERED INDEX [FK_vehicle_mobitels_Mobitel_ID] ON [dbo].[vehicle] 
(
	[Mobitel_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'FK_vehicle_team_id')
CREATE NONCLUSTERED INDEX [FK_vehicle_team_id] ON [dbo].[vehicle] 
(
	[Team_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'FK_vehicle_vehicle_category_Id')
CREATE NONCLUSTERED INDEX [FK_vehicle_vehicle_category_Id] ON [dbo].[vehicle] 
(
	[Category_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'FK_vehicle_vehicle_state_Id')
CREATE NONCLUSTERED INDEX [FK_vehicle_vehicle_state_Id] ON [dbo].[vehicle] 
(
	[State] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'id')
CREATE UNIQUE NONCLUSTERED INDEX [id] ON [dbo].[vehicle] 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'setting_id')
CREATE NONCLUSTERED INDEX [setting_id] ON [dbo].[vehicle] 
(
	[setting_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'MakeCar'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'MakeCar'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'NumberPlate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'NumberPlate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'Odometr'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'Odometr'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'CarModel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'CarModel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'odoTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'odoTime'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'OutLinkId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'OutLinkId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'Category_id'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'Category_id'
GO
/****** Object:  Table [dbo].[agro_work]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_work]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_work](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[SpeedBottom] [float] NULL DEFAULT ((0)),
	[SpeedTop] [float] NULL DEFAULT ((0)),
	[Comment] [varchar](max) NULL,
	[TypeWork] [int] NOT NULL DEFAULT ((1)),
	[Id_main] [int] NULL,
 CONSTRAINT [PK_agro_work] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_work]') AND name = N'FK_agro_work_agro_work_types_Id')
CREATE NONCLUSTERED INDEX [FK_agro_work_agro_work_types_Id] ON [dbo].[agro_work] 
(
	[TypeWork] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_work]') AND name = N'FK_agro_work_agro_workgroup_Id')
CREATE NONCLUSTERED INDEX [FK_agro_work_agro_workgroup_Id] ON [dbo].[agro_work] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'SpeedBottom'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'SpeedBottom'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'SpeedTop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'SpeedTop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'Comment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'TypeWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ � ����, ������� ��� ����,������� �� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'TypeWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ����� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
/****** Object:  Table [dbo].[lines]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lines]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[lines](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Color] [int] NULL DEFAULT ((0)),
	[Width] [int] NULL DEFAULT ((3)),
	[Mobitel_ID] [int] NULL,
	[Icon] [varbinary](max) NULL,
 CONSTRAINT [PK_lines] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[lines]') AND name = N'lines_FK1')
CREATE NONCLUSTERED INDEX [lines_FK1] ON [dbo].[lines] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'lines', N'COLUMN',N'Color'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lines', @level2type=N'COLUMN',@level2name=N'Color'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'lines', N'COLUMN',N'Width'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lines', @level2type=N'COLUMN',@level2name=N'Width'
GO
/****** Object:  Table [dbo].[agro_fieldculture]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_fieldculture](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NULL,
	[Id_culture] [int] NOT NULL,
	[Year] [smallint] NOT NULL,
 CONSTRAINT [PK_agro_fieldculture] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]') AND name = N'Id_culture_FK2')
CREATE NONCLUSTERED INDEX [Id_culture_FK2] ON [dbo].[agro_fieldculture] 
(
	[Id_culture] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]') AND name = N'Id_main_FieldCulture_FK1')
CREATE NONCLUSTERED INDEX [Id_main_FieldCulture_FK1] ON [dbo].[agro_fieldculture] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldculture', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldculture', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldculture', N'COLUMN',N'Id_culture'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldculture', @level2type=N'COLUMN',@level2name=N'Id_culture'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldculture', N'COLUMN',N'Year'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldculture', @level2type=N'COLUMN',@level2name=N'Year'
GO
/****** Object:  Table [dbo].[agro_order]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_order]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NULL,
	[Id_mobitel] [int] NOT NULL DEFAULT ((0)),
	[LocationStart] [varchar](255) NULL,
	[LocationEnd] [varchar](255) NULL,
	[TimeStart] [datetime] NULL,
	[TimeEnd] [datetime] NULL,
	[Regime] [smallint] NULL DEFAULT ((0)),
	[TimeWork] [char](10) NULL,
	[TimeMove] [char](10) NULL,
	[Distance] [float] NULL DEFAULT ((0)),
	[SpeedAvg] [float] NULL,
	[Comment] [varchar](max) NULL,
	[PathWithoutWork] [float] NULL DEFAULT ((0)),
	[SquareWorkDescript] [varchar](max) NULL,
	[FuelDUTExpensSquare] [float] NULL DEFAULT ((0)),
	[FuelDRTExpensSquare] [float] NULL DEFAULT ((0)),
	[FuelDUTExpensAvgSquare] [float] NULL DEFAULT ((0)),
	[FuelDRTExpensAvgSquare] [float] NULL DEFAULT ((0)),
	[FuelDUTExpensWithoutWork] [float] NULL DEFAULT ((0)),
	[FuelDRTExpensWithoutWork] [float] NULL DEFAULT ((0)),
	[FuelDUTExpensAvgWithoutWork] [float] NULL DEFAULT ((0)),
	[FuelDRTExpensAvgWithoutWork] [float] NULL DEFAULT ((0)),
	[SquareWorkDescripOverlap] [varchar](max) NULL,
	[FactSquareCalcOverlap] [float] NULL DEFAULT ((0)),
	[DateLastRecalc] [datetime] NULL,
	[UserLastRecalc] [varchar](127) NULL,
	[UserCreated] [varchar](127) NULL,
	[PointsValidity] [int] NULL,
	[PointsCalc] [int] NULL DEFAULT ((0)),
	[PointsFact] [int] NULL DEFAULT ((0)),
	[PointsIntervalMax] [varchar](20) NULL,
	[StateOrder] [int] NOT NULL DEFAULT ((0)),
	[BlockUserId] [int] NOT NULL DEFAULT ((0)),
	[BlockDate] [datetime] NULL,
 CONSTRAINT [PK_agro_order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_order]') AND name = N'Id_mobitel_Order_FK1')
CREATE NONCLUSTERED INDEX [Id_mobitel_Order_FK1] ON [dbo].[agro_order] 
(
	[Id_mobitel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'Date'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������ �� ������(new type - DATE->DATETIME)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'Date'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'Id_mobitel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'Id_mobitel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'LocationStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'LocationStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'LocationEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'LocationEnd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'TimeStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'TimeStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'TimeEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'TimeEnd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'Regime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����� -������,��������������, ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'Regime'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'TimeWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������������� �����, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'TimeWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'TimeMove'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����� ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'TimeMove'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'SpeedAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ��������, ��/�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'SpeedAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'Comment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'PathWithoutWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'PathWithoutWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'SquareWorkDescript'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'SquareWorkDescript'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDUTExpensSquare'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ � �����,  �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDUTExpensSquare'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDRTExpensSquare'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ � �����,  �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDRTExpensSquare'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDUTExpensAvgSquare'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ � �����, �/��:' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDUTExpensAvgSquare'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDRTExpensAvgSquare'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ � �����, �/��:' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDRTExpensAvgSquare'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDUTExpensWithoutWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ���������, �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDUTExpensWithoutWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDRTExpensWithoutWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ���������, �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDRTExpensWithoutWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDUTExpensAvgWithoutWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDUTExpensAvgWithoutWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDRTExpensAvgWithoutWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDRTExpensAvgWithoutWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'SquareWorkDescripOverlap'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ������� � ������ ���������� ���� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'SquareWorkDescripOverlap'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FactSquareCalcOverlap'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������������ ������� ���������� �������� � ������ ���������� ���� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FactSquareCalcOverlap'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'DateLastRecalc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ���������� ��������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'DateLastRecalc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'UserLastRecalc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������, ��������� ������������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'UserLastRecalc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'UserCreated'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������, ��������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'UserCreated'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'PointsValidity'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'PointsValidity'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'PointsCalc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����� (�� Log_ID) ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'PointsCalc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'PointsFact'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����� (�� Log_ID) �����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'PointsFact'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'PointsIntervalMax'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������� ����� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'PointsIntervalMax'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'StateOrder'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'StateOrder'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'BlockUserId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������������, ����������� � ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'BlockUserId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'BlockDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������ ������ � ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'BlockDate'
GO
/****** Object:  Table [dbo].[rt_samplet]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_samplet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_samplet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Position] [smallint] NOT NULL,
	[Id_event] [int] NOT NULL,
	[Id_zone] [int] NULL,
	[TimeRel] [char](5) NULL DEFAULT ('00:00'),
	[TimeRelP] [char](5) NULL DEFAULT ('00:00'),
	[TimeRelM] [char](5) NULL DEFAULT ('00:00'),
	[TimeAbs] [char](5) NULL DEFAULT ('00:00'),
 CONSTRAINT [PK_rt_samplet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[rt_samplet]') AND name = N'rt_samplet_FK1')
CREATE NONCLUSTERED INDEX [rt_samplet_FK1] ON [dbo].[rt_samplet] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'Position'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����� ������ ������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'Position'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'Id_event'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� - ����� / ����� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'Id_event'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'Id_zone'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'Id_zone'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'TimeRel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ����� �� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'TimeRel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'TimeRelP'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ���������� (+)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'TimeRelP'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'TimeRelM'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ���������� (-)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'TimeRelM'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'TimeAbs'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �����  ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'TimeAbs'
GO
/****** Object:  Table [dbo].[rt_route_stops]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_stops]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_route_stops](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Location] [varchar](255) NULL,
	[InitialTime] [datetime] NULL,
	[FinalTime] [datetime] NULL,
	[Interval] [time](7) NULL,
	[CheckZone] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_rt_route_stops] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_stops]') AND name = N'rt_route_stops_FK1')
CREATE NONCLUSTERED INDEX [rt_route_stops_FK1] ON [dbo].[rt_route_stops] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route_stops', N'COLUMN',N'CheckZone'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� � ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route_stops', @level2type=N'COLUMN',@level2name=N'CheckZone'
GO
/****** Object:  Table [dbo].[rt_route_sensors]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_route_sensors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[SensorName] [varchar](127) NOT NULL DEFAULT (''),
	[Location] [varchar](255) NOT NULL DEFAULT (''),
	[EventTime] [datetime] NOT NULL,
	[Duration] [time](7) NOT NULL,
	[Description] [varchar](255) NULL,
	[Speed] [float] NULL DEFAULT ((0)),
	[Distance] [float] NULL DEFAULT ((0.00)),
 CONSTRAINT [PK_rt_route_sensors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]') AND name = N'rt_route_sensors_FK1')
CREATE NONCLUSTERED INDEX [rt_route_sensors_FK1] ON [dbo].[rt_route_sensors] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route_sensors', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route_sensors', @level2type=N'COLUMN',@level2name=N'Distance'
GO
/****** Object:  Table [dbo].[rt_route_fuel]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_fuel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_route_fuel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Location] [varchar](255) NOT NULL DEFAULT (''),
	[time_] [datetime] NOT NULL,
	[dValueCalc] [float] NOT NULL DEFAULT ((0)),
	[dValueHandle] [float] NULL DEFAULT ((0)),
 CONSTRAINT [PK_rt_route_fuel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_fuel]') AND name = N'Id')
CREATE UNIQUE NONCLUSTERED INDEX [Id] ON [dbo].[rt_route_fuel] 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_fuel]') AND name = N'rt_route_fuel_FK1')
CREATE NONCLUSTERED INDEX [rt_route_fuel_FK1] ON [dbo].[rt_route_fuel] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route_fuel', N'COLUMN',N'dValueCalc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route_fuel', @level2type=N'COLUMN',@level2name=N'dValueCalc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route_fuel', N'COLUMN',N'dValueHandle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route_fuel', @level2type=N'COLUMN',@level2name=N'dValueHandle'
GO
/****** Object:  Table [dbo].[ntf_mobitels]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ntf_mobitels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[MobitelId] [int] NOT NULL,
	[SensorLogicId] [int] NULL DEFAULT ((0)),
	[SensorId] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_ntf_mobitels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]') AND name = N'FK_ntf_mobitels_ntf_main_Id')
CREATE NONCLUSTERED INDEX [FK_ntf_mobitels_ntf_main_Id] ON [dbo].[ntf_mobitels] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users_acs_objects]    Script Date: 04/11/2013 10:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_acs_objects]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[users_acs_objects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_role] [int] NOT NULL,
	[Id_type] [int] NOT NULL,
	[Id_object] [int] NOT NULL,
 CONSTRAINT [PK_users_acs_objects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[users_acs_objects]') AND name = N'FK_users_acs_objects_users_acs_roles_Id')
CREATE NONCLUSTERED INDEX [FK_users_acs_objects_users_acs_roles_Id] ON [dbo].[users_acs_objects] 
(
	[Id_role] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rt_routet]    Script Date: 04/11/2013 10:56:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_routet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_routet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Id_zone] [int] NOT NULL DEFAULT ((0)),
	[Id_event] [int] NOT NULL DEFAULT ((1)),
	[DatePlan] [datetime] NULL,
	[DateFact] [datetime] NULL,
	[Deviation] [varchar](10) NULL,
	[Remark] [varchar](max) NULL,
	[Distance] [float] NULL DEFAULT ((0)),
	[Location] [varchar](255) NULL,
 CONSTRAINT [PK_rt_routet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[rt_routet]') AND name = N'rt_routet_FK1')
CREATE NONCLUSTERED INDEX [rt_routet_FK1] ON [dbo].[rt_routet] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Id_zone'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Id_zone'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Id_event'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� - ����� / ����� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Id_event'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'DatePlan'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'DatePlan'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'DateFact'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'DateFact'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Deviation'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Deviation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Remark'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Remark'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Location'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Location'
GO
/****** Object:  Table [dbo].[ntf_log]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_log]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ntf_log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[DateEvent] [datetime] NOT NULL,
	[DateWrite] [datetime] NOT NULL,
	[TypeEvent] [smallint] NOT NULL,
	[Mobitel_id] [int] NOT NULL DEFAULT ((0)),
	[Zone_id] [int] NOT NULL DEFAULT ((0)),
	[Location] [varchar](255) NULL,
	[Lat] [float] NULL DEFAULT ((0)),
	[Lng] [float] NULL DEFAULT ((0)),
	[Infor] [varchar](255) NULL,
	[IsRead] [bit] NULL DEFAULT ((0)),
	[DataGps_ID] [int] NULL DEFAULT ((0)),
	[Speed] [float] NULL DEFAULT ((0)),
	[Value] [float] NULL DEFAULT ((0)),
 CONSTRAINT [PK_ntf_log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ntf_log]') AND name = N'FK_ntf_log_ntf_main_Id')
CREATE NONCLUSTERED INDEX [FK_ntf_log_ntf_main_Id] ON [dbo].[ntf_log] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_log', N'COLUMN',N'IsRead'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ��������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_log', @level2type=N'COLUMN',@level2name=N'IsRead'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_log', N'COLUMN',N'Value'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� �������� ������� (�������)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_log', @level2type=N'COLUMN',@level2name=N'Value'
GO
/****** Object:  Table [dbo].[ntf_events]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_events]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ntf_events](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[TypeEvent] [smallint] NOT NULL,
	[EventId] [int] NOT NULL DEFAULT ((0)),
	[ParInt1] [int] NOT NULL DEFAULT ((0)),
	[ParDbl1] [float] NOT NULL DEFAULT ((0)),
	[ParDbl2] [float] NOT NULL DEFAULT ((0)),
	[ParBool] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_ntf_events] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ntf_events]') AND name = N'FK_ntf_events_ntf_main_Id')
CREATE NONCLUSTERED INDEX [FK_ntf_events_ntf_main_Id] ON [dbo].[ntf_events] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_events', N'COLUMN',N'ParInt1'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'INT �������� ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_events', @level2type=N'COLUMN',@level2name=N'ParInt1'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_events', N'COLUMN',N'ParDbl1'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DOUBLE �������� �1 ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_events', @level2type=N'COLUMN',@level2name=N'ParDbl1'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_events', N'COLUMN',N'ParDbl2'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DOUBLE �������� �2 ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_events', @level2type=N'COLUMN',@level2name=N'ParDbl2'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_events', N'COLUMN',N'ParBool'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BOOL �������� ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_events', @level2type=N'COLUMN',@level2name=N'ParBool'
GO
/****** Object:  Table [dbo].[ntf_emails]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_emails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ntf_emails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[IsActive] [smallint] NOT NULL DEFAULT ((0)),
	[Email] [varchar](127) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_ntf_emails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mobitels_rotate_bands]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mobitels_rotate_bands](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[Bound] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_mobitels_rotate_bands] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]') AND name = N'FK_mobitels_rotate_bands_mobitels_Mobitel_ID')
CREATE NONCLUSTERED INDEX [FK_mobitels_rotate_bands_mobitels_Mobitel_ID] ON [dbo].[mobitels_rotate_bands] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[report_pass_zones]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[report_pass_zones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[report_pass_zones](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ZoneID] [int] NOT NULL,
 CONSTRAINT [PK_report_pass_zones] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[report_pass_zones]') AND name = N'IDX_ZoneID')
CREATE NONCLUSTERED INDEX [IDX_ZoneID] ON [dbo].[report_pass_zones] 
(
	[ZoneID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'report_pass_zones', N'COLUMN',N'ZoneID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'report_pass_zones', @level2type=N'COLUMN',@level2name=N'ZoneID'
GO
/****** Object:  Table [dbo].[md_waybill]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_waybill](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[IdDriver] [int] NULL DEFAULT ((0)),
	[IdVehicle] [int] NOT NULL DEFAULT ((0)),
	[IdEnterprise] [int] NULL,
	[IdShift] [int] NULL DEFAULT ((0)),
	[IsClose] [smallint] NOT NULL DEFAULT ((0)),
	[IsBusinessTrip] [smallint] NOT NULL,
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_waybill] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND name = N'FK_md_waybill_driver_id')
CREATE NONCLUSTERED INDEX [FK_md_waybill_driver_id] ON [dbo].[md_waybill] 
(
	[IdDriver] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND name = N'FK_md_waybill_md_enterprise_Id')
CREATE NONCLUSTERED INDEX [FK_md_waybill_md_enterprise_Id] ON [dbo].[md_waybill] 
(
	[IdEnterprise] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND name = N'FK_md_waybill_md_working_shift_Id')
CREATE NONCLUSTERED INDEX [FK_md_waybill_md_working_shift_Id] ON [dbo].[md_waybill] 
(
	[IdShift] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND name = N'FK_md_waybill_vehicle_id')
CREATE NONCLUSTERED INDEX [FK_md_waybill_vehicle_id] ON [dbo].[md_waybill] 
(
	[IdVehicle] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_waybill', N'COLUMN',N'IsClose'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ���� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_waybill', @level2type=N'COLUMN',@level2name=N'IsClose'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_waybill', N'COLUMN',N'IsBusinessTrip'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_waybill', @level2type=N'COLUMN',@level2name=N'IsBusinessTrip'
GO
/****** Object:  Table [dbo].[points]    Script Date: 04/11/2013 10:56:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[points]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[points](
	[Point_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NULL DEFAULT ((0)),
	[Longitude] [int] NULL DEFAULT ((0)),
	[Zone_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_points] PRIMARY KEY CLUSTERED 
(
	[Point_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[points]') AND name = N'FK_points_zones')
CREATE NONCLUSTERED INDEX [FK_points_zones] ON [dbo].[points] 
(
	[Zone_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[agro_pricet]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_pricet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Id_work] [int] NOT NULL,
	[Price] [float] NULL DEFAULT ((0.00)),
	[Id_unit] [int] NOT NULL,
	[Id_mobitel] [int] NOT NULL,
	[Id_agregat] [int] NULL DEFAULT ((0)),
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_agro_pricet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND name = N'Id_agregat_Price_FK5')
CREATE NONCLUSTERED INDEX [Id_agregat_Price_FK5] ON [dbo].[agro_pricet] 
(
	[Id_agregat] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND name = N'Id_main_FK1')
CREATE NONCLUSTERED INDEX [Id_main_FK1] ON [dbo].[agro_pricet] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND name = N'Id_mobitel_FK3')
CREATE NONCLUSTERED INDEX [Id_mobitel_FK3] ON [dbo].[agro_pricet] 
(
	[Id_mobitel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND name = N'Id_unit_FK4')
CREATE NONCLUSTERED INDEX [Id_unit_FK4] ON [dbo].[agro_pricet] 
(
	[Id_unit] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND name = N'Id_work_FK2')
CREATE NONCLUSTERED INDEX [Id_work_FK2] ON [dbo].[agro_pricet] 
(
	[Id_work] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Id_work'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Id_work'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Price'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Price'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Id_unit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Id_unit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Id_mobitel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Id_mobitel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Id_agregat'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Id_agregat'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Comment'
GO
/****** Object:  Table [dbo].[agro_ordert_control]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_ordert_control](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL DEFAULT ((0)),
	[Id_object] [int] NOT NULL DEFAULT ((0)),
	[Type_object] [smallint] NOT NULL DEFAULT ((0)),
	[TimeStart] [datetime] NOT NULL,
	[TimeEnd] [datetime] NOT NULL,
 CONSTRAINT [PK_agro_ordert_control] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]') AND name = N'FK_agro_ordert_control_agro_order_Id')
CREATE NONCLUSTERED INDEX [FK_agro_ordert_control_agro_order_Id] ON [dbo].[agro_ordert_control] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert_control', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert_control', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert_control', N'COLUMN',N'Id_object'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert_control', @level2type=N'COLUMN',@level2name=N'Id_object'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert_control', N'COLUMN',N'Type_object'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert_control', @level2type=N'COLUMN',@level2name=N'Type_object'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert_control', N'COLUMN',N'TimeStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������ ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert_control', @level2type=N'COLUMN',@level2name=N'TimeStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert_control', N'COLUMN',N'TimeEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ��������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert_control', @level2type=N'COLUMN',@level2name=N'TimeEnd'
GO
/****** Object:  Table [dbo].[agro_ordert]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_ordert](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL DEFAULT ((0)),
	[Id_field] [int] NOT NULL DEFAULT ((0)),
	[Id_zone] [int] NOT NULL DEFAULT ((0)),
	[Id_driver] [int] NOT NULL DEFAULT ((0)),
	[TimeStart] [datetime] NULL,
	[TimeEnd] [datetime] NULL,
	[TimeMove] [time](7) NULL,
	[TimeStop] [time](7) NULL,
	[TimeRate] [time](7) NULL,
	[Id_work] [int] NOT NULL DEFAULT ((0)),
	[FactTime] [time](7) NULL,
	[Distance] [float] NOT NULL DEFAULT ((0)),
	[FactSquare] [float] NOT NULL DEFAULT ((0.00000)),
	[FactSquareCalc] [float] NOT NULL DEFAULT ((0.00000)),
	[Price] [float] NOT NULL DEFAULT ((0.00)),
	[Sum] [float] NOT NULL DEFAULT ((0.00)),
	[SpeedAvg] [float] NOT NULL DEFAULT ((0)),
	[Comment] [varchar](max) NULL,
	[FuelStart] [float] NOT NULL DEFAULT ((0.00)),
	[FuelAdd] [float] NOT NULL DEFAULT ((0.00)),
	[FuelSub] [float] NOT NULL DEFAULT ((0.00)),
	[FuelEnd] [float] NOT NULL DEFAULT ((0.00)),
	[FuelExpens] [float] NOT NULL DEFAULT ((0.00)),
	[FuelExpensAvg] [float] NOT NULL DEFAULT ((0.00)),
	[FuelExpensAvgRate] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensAvgRate] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensMove] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensStop] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensAvg] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensTotal] [float] NOT NULL DEFAULT ((0.00)),
	[Id_agregat] [int] NOT NULL DEFAULT ((0)),
	[Confirm] [smallint] NOT NULL DEFAULT ((1)),
	[PointsValidity] [int] NOT NULL DEFAULT ((0)),
	[LockRecord] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_agro_ordert] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert]') AND name = N'Id_main_Order_FK1')
CREATE NONCLUSTERED INDEX [Id_main_Order_FK1] ON [dbo].[agro_ordert] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_field'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_field'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_zone'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_zone'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_driver'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_driver'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'TimeStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������ ������ � ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'TimeStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'TimeEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ��������� ����� � ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'TimeEnd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'TimeMove'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'TimeMove'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'TimeStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� �������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'TimeStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'TimeRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'TimeRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_work'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_work'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FactTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FactTime'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FactSquare'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FactSquare'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FactSquareCalc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������������ ������� ���������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FactSquareCalc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Price'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Price'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Sum'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� �� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Sum'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'SpeedAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �������� ���������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'SpeedAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Comment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� � ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelAdd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelAdd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelSub'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelSub'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� � �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelEnd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelExpens'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelExpens'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelExpensAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� ������ ������� �� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelExpensAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelExpensAvgRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ������, �/�������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelExpensAvgRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Fuel_ExpensAvgRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������� ������, �/�������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensAvgRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Fuel_ExpensMove'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������ ������� � ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensMove'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Fuel_ExpensStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������ ������� �� ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Fuel_ExpensAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������� ������, �/��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Fuel_ExpensTotal'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ����� ������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensTotal'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_agregat'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_agregat'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Confirm'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ��������� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Confirm'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'PointsValidity'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'PointsValidity'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'LockRecord'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������������� ��� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'LockRecord'
GO
/****** Object:  Table [dbo].[agro_agregat_vehicle]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_agregat_vehicle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_agregat] [int] NOT NULL DEFAULT ((0)),
	[Id_vehicle] [int] NOT NULL DEFAULT ((0)),
	[Id_sensor] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_agro_agregat_vehicle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]') AND name = N'FK_agro_agregat_vehicle_agro_agregat_Id')
CREATE NONCLUSTERED INDEX [FK_agro_agregat_vehicle_agro_agregat_Id] ON [dbo].[agro_agregat_vehicle] 
(
	[Id_agregat] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]') AND name = N'FK_agro_agregat_vehicle_vehicle_id')
CREATE NONCLUSTERED INDEX [FK_agro_agregat_vehicle_vehicle_id] ON [dbo].[agro_agregat_vehicle] 
(
	[Id_vehicle] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat_vehicle', N'COLUMN',N'Id_sensor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat_vehicle', @level2type=N'COLUMN',@level2name=N'Id_sensor'
GO
/****** Object:  Table [dbo].[md_object]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_object]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_object](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_group] [int] NOT NULL DEFAULT ((0)),
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[Number] [int] NOT NULL DEFAULT ((0)),
	[NumberParent] [int] NOT NULL DEFAULT ((0)),
	[DelayTime] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
	[IdZone] [int] NULL,
 CONSTRAINT [PK_md_object] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_object]') AND name = N'FK_md_object_md_object_group_Id')
CREATE NONCLUSTERED INDEX [FK_md_object_md_object_group_Id] ON [dbo].[md_object] 
(
	[Id_group] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_object]') AND name = N'FK_md_object_zones_Zone_ID')
CREATE NONCLUSTERED INDEX [FK_md_object_zones_Zone_ID] ON [dbo].[md_object] 
(
	[IdZone] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_object', N'COLUMN',N'Id_group'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_object', @level2type=N'COLUMN',@level2name=N'Id_group'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_object', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_object', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_object', N'COLUMN',N'Number'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� �������/����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_object', @level2type=N'COLUMN',@level2name=N'Number'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_object', N'COLUMN',N'NumberParent'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_object', @level2type=N'COLUMN',@level2name=N'NumberParent'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_object', N'COLUMN',N'DelayTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_object', @level2type=N'COLUMN',@level2name=N'DelayTime'
GO
/****** Object:  Table [dbo].[md_loading_time]    Script Date: 04/11/2013 10:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_loading_time]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_loading_time](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdObject] [int] NOT NULL DEFAULT ((0)),
	[IdCargo] [int] NOT NULL DEFAULT ((0)),
	[TimeLoading] [decimal](10, 2) NOT NULL,
	[TimeUnLoading] [decimal](10, 2) NOT NULL,
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_loading_time] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_loading_time]') AND name = N'FK_md_loading_time_md_cargo_Id')
CREATE NONCLUSTERED INDEX [FK_md_loading_time_md_cargo_Id] ON [dbo].[md_loading_time] 
(
	[IdCargo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_loading_time]') AND name = N'FK_md_loading_time_md_object_Id')
CREATE NONCLUSTERED INDEX [FK_md_loading_time_md_object_Id] ON [dbo].[md_loading_time] 
(
	[IdObject] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_loading_time', N'COLUMN',N'IdObject'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_loading_time', @level2type=N'COLUMN',@level2name=N'IdObject'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_loading_time', N'COLUMN',N'IdCargo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_loading_time', @level2type=N'COLUMN',@level2name=N'IdCargo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_loading_time', N'COLUMN',N'TimeLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_loading_time', @level2type=N'COLUMN',@level2name=N'TimeLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_loading_time', N'COLUMN',N'TimeUnLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_loading_time', @level2type=N'COLUMN',@level2name=N'TimeUnLoading'
GO
/****** Object:  Table [dbo].[agro_datagps]    Script Date: 04/11/2013 10:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_datagps]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_datagps](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Lon_base] [float] NOT NULL,
	[Lat_base] [float] NOT NULL,
	[Lon_dev] [varchar](255) NOT NULL DEFAULT (''),
	[Lat_dev] [varchar](255) NOT NULL DEFAULT (''),
	[Speed_base] [float] NOT NULL,
	[Speed_dev] [varchar](255) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_agro_datagps] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_datagps]') AND name = N'FK_agro_datagps_agro_ordert_Id')
CREATE NONCLUSTERED INDEX [FK_agro_datagps_agro_ordert_Id] ON [dbo].[agro_datagps] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[md_order_job]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order_job]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_order_job](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdObject] [int] NOT NULL DEFAULT ((0)),
	[IdOrder] [int] NOT NULL DEFAULT ((0)),
	[IdWayBill] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_md_order_job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order_job]') AND name = N'FK_md_order_job_md_object_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_job_md_object_Id] ON [dbo].[md_order_job] 
(
	[IdObject] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order_job]') AND name = N'FK_md_order_job_md_order_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_job_md_order_Id] ON [dbo].[md_order_job] 
(
	[IdOrder] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order_job]') AND name = N'FK_md_order_job_md_waybill_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_job_md_waybill_Id] ON [dbo].[md_order_job] 
(
	[IdWayBill] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order_job', N'COLUMN',N'IdObject'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order_job', @level2type=N'COLUMN',@level2name=N'IdObject'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order_job', N'COLUMN',N'IdOrder'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order_job', @level2type=N'COLUMN',@level2name=N'IdOrder'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order_job', N'COLUMN',N'IdWayBill'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order_job', @level2type=N'COLUMN',@level2name=N'IdWayBill'
GO
/****** Object:  Table [dbo].[md_order]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [varchar](13) NULL,
	[DateInit] [datetime] NULL,
	[IdObjectCustomer] [int] NULL DEFAULT ((0)),
	[IdObjectStart] [int] NULL DEFAULT ((0)),
	[IdPriority] [int] NOT NULL DEFAULT ((0)),
	[DateCreate] [datetime] NOT NULL,
	[DateRefuse] [datetime] NULL,
	[DateEdit] [datetime] NULL,
	[DateDelete] [datetime] NULL,
	[DateExecute] [datetime] NULL,
	[DateStartWork] [datetime] NULL,
	[IdCategory] [int] NOT NULL DEFAULT ((0)),
	[IdTransportationYipe] [int] NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
	[IdWayBill] [int] NULL DEFAULT ((0)),
	[TimeDelay] [time](7) NULL,
	[IsClose] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_md_order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_object_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_object_Id] ON [dbo].[md_order] 
(
	[IdObjectCustomer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_objectStart_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_objectStart_Id] ON [dbo].[md_order] 
(
	[IdObjectStart] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_order_category_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_order_category_Id] ON [dbo].[md_order] 
(
	[IdCategory] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_order_priority_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_order_priority_Id] ON [dbo].[md_order] 
(
	[IdPriority] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_transportation_types_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_transportation_types_Id] ON [dbo].[md_order] 
(
	[IdTransportationYipe] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_waybill_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_waybill_Id] ON [dbo].[md_order] 
(
	[IdWayBill] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateInit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ��������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateInit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdObjectCustomer'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdObjectCustomer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdObjectStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdObjectStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdPriority'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdPriority'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateCreate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� �������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateCreate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateRefuse'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ������ �� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateRefuse'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateEdit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� �������������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateEdit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateDelete'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� �������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateDelete'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateExecute'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateExecute'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateStartWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ������ ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateStartWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdCategory'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdCategory'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdTransportationYipe'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdTransportationYipe'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdWayBill'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdWayBill'
GO
/****** Object:  Table [dbo].[md_route]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_route]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_route](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_object_from] [int] NOT NULL DEFAULT ((0)),
	[Id_object_to] [int] NOT NULL DEFAULT ((0)),
	[Distance] [decimal](10, 2) NOT NULL DEFAULT ((0)),
	[Time] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_route] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_route]') AND name = N'FK_md_route_md_object_from')
CREATE NONCLUSTERED INDEX [FK_md_route_md_object_from] ON [dbo].[md_route] 
(
	[Id_object_from] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_route]') AND name = N'FK_md_route_md_object_to')
CREATE NONCLUSTERED INDEX [FK_md_route_md_object_to] ON [dbo].[md_route] 
(
	[Id_object_to] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_route', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_route', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_route', N'COLUMN',N'Time'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_route', @level2type=N'COLUMN',@level2name=N'Time'
GO
/****** Object:  Table [dbo].[md_ordert]    Script Date: 04/11/2013 10:56:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_ordert](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[IdObject] [int] NOT NULL DEFAULT ((0)),
	[IdCargoLoading] [int] NULL DEFAULT ((0)),
	[QtyLoading] [decimal](10, 2) NOT NULL DEFAULT ((0)),
	[TimeLoading] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[IdCargoUnLoading] [int] NULL DEFAULT ((0)),
	[QtyUnLoading] [decimal](10, 2) NOT NULL DEFAULT ((0)),
	[TimeUnLoading] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[TimeDelay] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[TimeToObject] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[DistanceToObject] [decimal](10, 2) NOT NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
	[IsClose] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_md_ordert] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND name = N'FK_md_ordert_md_cargo_Id')
CREATE NONCLUSTERED INDEX [FK_md_ordert_md_cargo_Id] ON [dbo].[md_ordert] 
(
	[IdCargoLoading] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND name = N'FK_md_ordert_md_cargoUnload_Id')
CREATE NONCLUSTERED INDEX [FK_md_ordert_md_cargoUnload_Id] ON [dbo].[md_ordert] 
(
	[IdCargoUnLoading] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND name = N'FK_md_ordert_md_object_Id')
CREATE NONCLUSTERED INDEX [FK_md_ordert_md_object_Id] ON [dbo].[md_ordert] 
(
	[IdObject] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND name = N'FK_md_ordert_md_order_Id')
CREATE NONCLUSTERED INDEX [FK_md_ordert_md_order_Id] ON [dbo].[md_ordert] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'IdCargoLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'IdCargoLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'QtyLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'QtyLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'TimeLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'TimeLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'IdCargoUnLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'IdCargoUnLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'QtyUnLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������ ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'QtyUnLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'TimeUnLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'TimeUnLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'TimeDelay'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'TimeDelay'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'TimeToObject'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� �� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'TimeToObject'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'DistanceToObject'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'DistanceToObject'
GO
/****** Object:  ForeignKey [fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id] FOREIGN KEY([Id_agregat])
REFERENCES [dbo].[agro_agregat] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] CHECK CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_agro_agregat_vehicle_vehicle_id]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_vehicle_id] FOREIGN KEY([Id_vehicle])
REFERENCES [dbo].[vehicle] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] CHECK CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_vehicle_id]
GO
/****** Object:  ForeignKey [fgn_key_FK_agro_datagps_agro_ordert_Id]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_datagps_agro_ordert_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_datagps]'))
ALTER TABLE [dbo].[agro_datagps]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_datagps_agro_ordert_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_ordert] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_datagps_agro_ordert_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_datagps]'))
ALTER TABLE [dbo].[agro_datagps] CHECK CONSTRAINT [fgn_key_FK_agro_datagps_agro_ordert_Id]
GO
/****** Object:  ForeignKey [fgn_key_Id_culture_FK2]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_culture_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_culture_FK2] FOREIGN KEY([Id_culture])
REFERENCES [dbo].[agro_culture] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_culture_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] CHECK CONSTRAINT [fgn_key_Id_culture_FK2]
GO
/****** Object:  ForeignKey [fgn_key_Id_main_FieldCulture_FK1]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FieldCulture_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_main_FieldCulture_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_field] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FieldCulture_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] CHECK CONSTRAINT [fgn_key_Id_main_FieldCulture_FK1]
GO
/****** Object:  ForeignKey [fgn_key_Id_mobitel_Order_FK1]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_order]'))
ALTER TABLE [dbo].[agro_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_mobitel_Order_FK1] FOREIGN KEY([Id_mobitel])
REFERENCES [dbo].[mobitels] ([Mobitel_ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_order]'))
ALTER TABLE [dbo].[agro_order] CHECK CONSTRAINT [fgn_key_Id_mobitel_Order_FK1]
GO
/****** Object:  ForeignKey [fgn_key_Id_main_Order_FK1]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert]'))
ALTER TABLE [dbo].[agro_ordert]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_main_Order_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_order] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert]'))
ALTER TABLE [dbo].[agro_ordert] CHECK CONSTRAINT [fgn_key_Id_main_Order_FK1]
GO
/****** Object:  ForeignKey [fgn_key_FK_agro_ordert_control_agro_order_Id]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_ordert_control_agro_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]'))
ALTER TABLE [dbo].[agro_ordert_control]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_ordert_control_agro_order_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_order] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_ordert_control_agro_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]'))
ALTER TABLE [dbo].[agro_ordert_control] CHECK CONSTRAINT [fgn_key_FK_agro_ordert_control_agro_order_Id]
GO
/****** Object:  ForeignKey [fgn_key_Id_agregat_Price_FK5]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_agregat_Price_FK5]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_agregat_Price_FK5] FOREIGN KEY([Id_agregat])
REFERENCES [dbo].[agro_agregat] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_agregat_Price_FK5]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] CHECK CONSTRAINT [fgn_key_Id_agregat_Price_FK5]
GO
/****** Object:  ForeignKey [fgn_key_Id_main_FK1]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_main_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_price] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] CHECK CONSTRAINT [fgn_key_Id_main_FK1]
GO
/****** Object:  ForeignKey [fgn_key_Id_mobitel_FK3]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_FK3]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_mobitel_FK3] FOREIGN KEY([Id_mobitel])
REFERENCES [dbo].[mobitels] ([Mobitel_ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_FK3]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] CHECK CONSTRAINT [fgn_key_Id_mobitel_FK3]
GO
/****** Object:  ForeignKey [fgn_key_Id_unit_FK4]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_unit_FK4]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_unit_FK4] FOREIGN KEY([Id_unit])
REFERENCES [dbo].[agro_unit] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_unit_FK4]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] CHECK CONSTRAINT [fgn_key_Id_unit_FK4]
GO
/****** Object:  ForeignKey [fgn_key_Id_work_FK2]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_work_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_work_FK2] FOREIGN KEY([Id_work])
REFERENCES [dbo].[agro_work] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_work_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] CHECK CONSTRAINT [fgn_key_Id_work_FK2]
GO
/****** Object:  ForeignKey [fgn_key_FK_agro_work_agro_work_types_Id]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_work_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_work_agro_work_types_Id] FOREIGN KEY([TypeWork])
REFERENCES [dbo].[agro_work_types] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_work_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] CHECK CONSTRAINT [fgn_key_FK_agro_work_agro_work_types_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_agro_work_agro_workgroup_Id]    Script Date: 04/11/2013 10:56:41 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_workgroup_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_work_agro_workgroup_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_workgroup] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_workgroup_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] CHECK CONSTRAINT [fgn_key_FK_agro_work_agro_workgroup_Id]
GO
/****** Object:  ForeignKey [fgn_key_lines_FK1]    Script Date: 04/11/2013 10:56:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_lines_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[lines]'))
ALTER TABLE [dbo].[lines]  WITH CHECK ADD  CONSTRAINT [fgn_key_lines_FK1] FOREIGN KEY([Mobitel_ID])
REFERENCES [dbo].[mobitels] ([Mobitel_ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_lines_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[lines]'))
ALTER TABLE [dbo].[lines] CHECK CONSTRAINT [fgn_key_lines_FK1]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_loading_time_md_cargo_Id]    Script Date: 04/11/2013 10:56:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_loading_time_md_cargo_Id] FOREIGN KEY([IdCargo])
REFERENCES [dbo].[md_cargo] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] CHECK CONSTRAINT [fgn_key_FK_md_loading_time_md_cargo_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_loading_time_md_object_Id]    Script Date: 04/11/2013 10:56:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_loading_time_md_object_Id] FOREIGN KEY([IdObject])
REFERENCES [dbo].[md_object] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] CHECK CONSTRAINT [fgn_key_FK_md_loading_time_md_object_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_object_md_object_group_Id]    Script Date: 04/11/2013 10:56:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_md_object_group_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_object_md_object_group_Id] FOREIGN KEY([Id_group])
REFERENCES [dbo].[md_object_group] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_md_object_group_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] CHECK CONSTRAINT [fgn_key_FK_md_object_md_object_group_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_object_zones_Zone_ID]    Script Date: 04/11/2013 10:56:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_zones_Zone_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_object_zones_Zone_ID] FOREIGN KEY([IdZone])
REFERENCES [dbo].[zones] ([Zone_ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_zones_Zone_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] CHECK CONSTRAINT [fgn_key_FK_md_object_zones_Zone_ID]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_object_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_object_Id] FOREIGN KEY([IdObjectCustomer])
REFERENCES [dbo].[md_object] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_object_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_objectStart_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_objectStart_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_objectStart_Id] FOREIGN KEY([IdObjectStart])
REFERENCES [dbo].[md_object] ([Id])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_objectStart_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_objectStart_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_order_category_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_order_category_Id] FOREIGN KEY([IdCategory])
REFERENCES [dbo].[md_order_category] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_order_category_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_order_priority_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_priority_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_order_priority_Id] FOREIGN KEY([IdPriority])
REFERENCES [dbo].[md_order_priority] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_priority_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_order_priority_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_transportation_types_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_transportation_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_transportation_types_Id] FOREIGN KEY([IdTransportationYipe])
REFERENCES [dbo].[md_transportation_types] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_transportation_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_transportation_types_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_md_waybill_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_waybill_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_waybill_Id] FOREIGN KEY([IdWayBill])
REFERENCES [dbo].[md_waybill] ([Id])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_waybill_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_waybill_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_order_job_md_object_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_job_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order_job]'))
ALTER TABLE [dbo].[md_order_job]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_job_md_object_Id] FOREIGN KEY([IdObject])
REFERENCES [dbo].[md_object] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_job_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order_job]'))
ALTER TABLE [dbo].[md_order_job] CHECK CONSTRAINT [fgn_key_FK_md_order_job_md_object_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_ordert_md_cargo_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_ordert_md_cargo_Id] FOREIGN KEY([IdCargoLoading])
REFERENCES [dbo].[md_cargo] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] CHECK CONSTRAINT [fgn_key_FK_md_ordert_md_cargo_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_ordert_md_cargoUnload_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargoUnload_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_ordert_md_cargoUnload_Id] FOREIGN KEY([IdCargoUnLoading])
REFERENCES [dbo].[md_cargo] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargoUnload_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] CHECK CONSTRAINT [fgn_key_FK_md_ordert_md_cargoUnload_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_ordert_md_object_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_ordert_md_object_Id] FOREIGN KEY([IdObject])
REFERENCES [dbo].[md_object] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] CHECK CONSTRAINT [fgn_key_FK_md_ordert_md_object_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_ordert_md_order_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_ordert_md_order_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[md_order] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] CHECK CONSTRAINT [fgn_key_FK_md_ordert_md_order_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_route_md_object_from]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_route_md_object_from]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_route]'))
ALTER TABLE [dbo].[md_route]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_route_md_object_from] FOREIGN KEY([Id_object_from])
REFERENCES [dbo].[md_object] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_route_md_object_from]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_route]'))
ALTER TABLE [dbo].[md_route] CHECK CONSTRAINT [fgn_key_FK_md_route_md_object_from]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_waybill_driver_id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_driver_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_waybill_driver_id] FOREIGN KEY([IdDriver])
REFERENCES [dbo].[driver] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_driver_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] CHECK CONSTRAINT [fgn_key_FK_md_waybill_driver_id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_waybill_md_enterprise_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_enterprise_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_waybill_md_enterprise_Id] FOREIGN KEY([IdEnterprise])
REFERENCES [dbo].[md_enterprise] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_enterprise_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] CHECK CONSTRAINT [fgn_key_FK_md_waybill_md_enterprise_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_waybill_md_working_shift_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_working_shift_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_waybill_md_working_shift_Id] FOREIGN KEY([IdShift])
REFERENCES [dbo].[md_working_shift] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_working_shift_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] CHECK CONSTRAINT [fgn_key_FK_md_waybill_md_working_shift_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_md_waybill_vehicle_id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_waybill_vehicle_id] FOREIGN KEY([IdVehicle])
REFERENCES [dbo].[vehicle] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] CHECK CONSTRAINT [fgn_key_FK_md_waybill_vehicle_id]
GO
/****** Object:  ForeignKey [fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]'))
ALTER TABLE [dbo].[mobitels_rotate_bands]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID] FOREIGN KEY([Mobitel_ID])
REFERENCES [dbo].[mobitels] ([Mobitel_ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]'))
ALTER TABLE [dbo].[mobitels_rotate_bands] CHECK CONSTRAINT [fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]
GO
/****** Object:  ForeignKey [fgn_key_FK_ntf_email_ntf_main_Id]    Script Date: 04/11/2013 10:56:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_email_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_emails]'))
ALTER TABLE [dbo].[ntf_emails]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_ntf_email_ntf_main_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[ntf_main] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_email_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_emails]'))
ALTER TABLE [dbo].[ntf_emails] CHECK CONSTRAINT [fgn_key_FK_ntf_email_ntf_main_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_ntf_events_ntf_main_Id]    Script Date: 04/11/2013 10:56:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_events_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_events]'))
ALTER TABLE [dbo].[ntf_events]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_ntf_events_ntf_main_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[ntf_main] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_events_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_events]'))
ALTER TABLE [dbo].[ntf_events] CHECK CONSTRAINT [fgn_key_FK_ntf_events_ntf_main_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_ntf_log_ntf_main_Id]    Script Date: 04/11/2013 10:56:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_log_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_log]'))
ALTER TABLE [dbo].[ntf_log]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_ntf_log_ntf_main_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[ntf_main] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_log_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_log]'))
ALTER TABLE [dbo].[ntf_log] CHECK CONSTRAINT [fgn_key_FK_ntf_log_ntf_main_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_ntf_mobitels_ntf_main_Id]    Script Date: 04/11/2013 10:56:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_mobitels_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]'))
ALTER TABLE [dbo].[ntf_mobitels]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_ntf_mobitels_ntf_main_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[ntf_main] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_mobitels_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]'))
ALTER TABLE [dbo].[ntf_mobitels] CHECK CONSTRAINT [fgn_key_FK_ntf_mobitels_ntf_main_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_points_zones]    Script Date: 04/11/2013 10:56:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_points_zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[points]'))
ALTER TABLE [dbo].[points]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_points_zones] FOREIGN KEY([Zone_ID])
REFERENCES [dbo].[zones] ([Zone_ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_points_zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[points]'))
ALTER TABLE [dbo].[points] CHECK CONSTRAINT [fgn_key_FK_points_zones]
GO
/****** Object:  ForeignKey [FK_RPZ_Zones]    Script Date: 04/11/2013 10:56:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPZ_Zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[report_pass_zones]'))
ALTER TABLE [dbo].[report_pass_zones]  WITH CHECK ADD  CONSTRAINT [FK_RPZ_Zones] FOREIGN KEY([ZoneID])
REFERENCES [dbo].[zones] ([Zone_ID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPZ_Zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[report_pass_zones]'))
ALTER TABLE [dbo].[report_pass_zones] CHECK CONSTRAINT [FK_RPZ_Zones]
GO
/****** Object:  ForeignKey [fgn_key_rt_route_fuel_FK1]    Script Date: 04/11/2013 10:56:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_fuel_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_fuel]'))
ALTER TABLE [dbo].[rt_route_fuel]  WITH CHECK ADD  CONSTRAINT [fgn_key_rt_route_fuel_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[rt_route] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_fuel_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_fuel]'))
ALTER TABLE [dbo].[rt_route_fuel] CHECK CONSTRAINT [fgn_key_rt_route_fuel_FK1]
GO
/****** Object:  ForeignKey [fgn_key_rt_route_sensors_FK1]    Script Date: 04/11/2013 10:56:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_sensors_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]'))
ALTER TABLE [dbo].[rt_route_sensors]  WITH CHECK ADD  CONSTRAINT [fgn_key_rt_route_sensors_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[rt_route] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_sensors_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]'))
ALTER TABLE [dbo].[rt_route_sensors] CHECK CONSTRAINT [fgn_key_rt_route_sensors_FK1]
GO
/****** Object:  ForeignKey [fgn_key_rt_route_stops_FK1]    Script Date: 04/11/2013 10:56:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_stops_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_stops]'))
ALTER TABLE [dbo].[rt_route_stops]  WITH CHECK ADD  CONSTRAINT [fgn_key_rt_route_stops_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[rt_route] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_stops_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_stops]'))
ALTER TABLE [dbo].[rt_route_stops] CHECK CONSTRAINT [fgn_key_rt_route_stops_FK1]
GO
/****** Object:  ForeignKey [fgn_key_rt_routet_FK1]    Script Date: 04/11/2013 10:56:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_routet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_routet]'))
ALTER TABLE [dbo].[rt_routet]  WITH CHECK ADD  CONSTRAINT [fgn_key_rt_routet_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[rt_route] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_routet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_routet]'))
ALTER TABLE [dbo].[rt_routet] CHECK CONSTRAINT [fgn_key_rt_routet_FK1]
GO
/****** Object:  ForeignKey [fgn_key_rt_samplet_FK1]    Script Date: 04/11/2013 10:56:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_samplet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_samplet]'))
ALTER TABLE [dbo].[rt_samplet]  WITH CHECK ADD  CONSTRAINT [fgn_key_rt_samplet_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[rt_sample] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_samplet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_samplet]'))
ALTER TABLE [dbo].[rt_samplet] CHECK CONSTRAINT [fgn_key_rt_samplet_FK1]
GO
/****** Object:  ForeignKey [fgn_key_FK_users_acs_objects_users_acs_roles_Id]    Script Date: 04/11/2013 10:56:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_users_acs_objects_users_acs_roles_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[users_acs_objects]'))
ALTER TABLE [dbo].[users_acs_objects]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_users_acs_objects_users_acs_roles_Id] FOREIGN KEY([Id_role])
REFERENCES [dbo].[users_acs_roles] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_users_acs_objects_users_acs_roles_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[users_acs_objects]'))
ALTER TABLE [dbo].[users_acs_objects] CHECK CONSTRAINT [fgn_key_FK_users_acs_objects_users_acs_roles_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_vehicle_mobitels_Mobitel_ID]    Script Date: 04/11/2013 10:56:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_vehicle_mobitels_Mobitel_ID] FOREIGN KEY([Mobitel_id])
REFERENCES [dbo].[mobitels] ([Mobitel_ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] CHECK CONSTRAINT [fgn_key_FK_vehicle_mobitels_Mobitel_ID]
GO
/****** Object:  ForeignKey [fgn_key_FK_vehicle_team_id]    Script Date: 04/11/2013 10:56:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_team_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_vehicle_team_id] FOREIGN KEY([Team_id])
REFERENCES [dbo].[team] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_team_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] CHECK CONSTRAINT [fgn_key_FK_vehicle_team_id]
GO
/****** Object:  ForeignKey [fgn_key_FK_vehicle_vehicle_category_Id]    Script Date: 04/11/2013 10:56:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_vehicle_vehicle_category_Id] FOREIGN KEY([Category_id])
REFERENCES [dbo].[vehicle_category] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] CHECK CONSTRAINT [fgn_key_FK_vehicle_vehicle_category_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_vehicle_vehicle_state_Id]    Script Date: 04/11/2013 10:56:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_state_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_vehicle_vehicle_state_Id] FOREIGN KEY([State])
REFERENCES [dbo].[vehicle_state] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_state_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] CHECK CONSTRAINT [fgn_key_FK_vehicle_vehicle_state_Id]
GO
/****** Object:  ForeignKey [fgn_key_FK_zones_zonesgroup]    Script Date: 04/11/2013 10:56:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_zones_zonesgroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[zones]'))
ALTER TABLE [dbo].[zones]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_zones_zonesgroup] FOREIGN KEY([ZonesGroupId])
REFERENCES [dbo].[zonesgroup] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_zones_zonesgroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[zones]'))
ALTER TABLE [dbo].[zones] CHECK CONSTRAINT [fgn_key_FK_zones_zonesgroup]
GO
