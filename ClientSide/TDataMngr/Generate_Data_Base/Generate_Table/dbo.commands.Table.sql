/****** Object:  Table [dbo].[commands]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__commands__ID__5DCAEF64]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[commands] DROP CONSTRAINT [DF__commands__ID__5DCAEF64]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[commands]') AND type in (N'U'))
DROP TABLE [dbo].[commands]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[commands]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[commands](
	[ID] [int] NULL DEFAULT ((0)),
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
