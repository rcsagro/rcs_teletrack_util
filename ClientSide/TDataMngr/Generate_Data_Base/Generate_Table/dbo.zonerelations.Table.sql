/****** Object:  Table [dbo].[zonerelations]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__Zone___55DFB4D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__Zone___55DFB4D9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__Confi__56D3D912]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__Confi__56D3D912]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__In_fl__57C7FD4B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__In_fl__57C7FD4B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__Out_f__58BC2184]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__Out_f__58BC2184]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__In_fl__59B045BD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__In_fl__59B045BD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonerelat__Out_f__5AA469F6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonerelations] DROP CONSTRAINT [DF__zonerelat__Out_f__5AA469F6]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zonerelations]') AND type in (N'U'))
DROP TABLE [dbo].[zonerelations]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zonerelations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zonerelations](
	[ZoneRelation_ID] [int] IDENTITY(1,1) NOT NULL,
	[Zone_ID] [int] NULL DEFAULT ((0)),
	[ConfigZoneSet_ID] [int] NULL DEFAULT ((0)),
	[In_flag] [smallint] NOT NULL DEFAULT ((0)),
	[Out_flag] [smallint] NOT NULL DEFAULT ((0)),
	[In_flag2] [smallint] NOT NULL DEFAULT ((0)),
	[Out_flag2] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_zonerelations] PRIMARY KEY CLUSTERED 
(
	[ZoneRelation_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
