/****** Object:  Table [dbo].[md_order_job]    Script Date: 04/11/2013 11:22:25 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_job_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order_job]'))
ALTER TABLE [dbo].[md_order_job] DROP CONSTRAINT [fgn_key_FK_md_order_job_md_object_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_job_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order_job]'))
ALTER TABLE [dbo].[md_order_job] DROP CONSTRAINT [fgn_key_FK_md_order_job_md_object_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order___IdObj__090A5324]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order_job] DROP CONSTRAINT [DF__md_order___IdObj__090A5324]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order___IdOrd__09FE775D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order_job] DROP CONSTRAINT [DF__md_order___IdOrd__09FE775D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order___IdWay__0AF29B96]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order_job] DROP CONSTRAINT [DF__md_order___IdWay__0AF29B96]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order_job]') AND type in (N'U'))
DROP TABLE [dbo].[md_order_job]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order_job]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_order_job](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdObject] [int] NOT NULL DEFAULT ((0)),
	[IdOrder] [int] NOT NULL DEFAULT ((0)),
	[IdWayBill] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_md_order_job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order_job]') AND name = N'FK_md_order_job_md_object_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_job_md_object_Id] ON [dbo].[md_order_job] 
(
	[IdObject] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order_job]') AND name = N'FK_md_order_job_md_order_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_job_md_order_Id] ON [dbo].[md_order_job] 
(
	[IdOrder] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order_job]') AND name = N'FK_md_order_job_md_waybill_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_job_md_waybill_Id] ON [dbo].[md_order_job] 
(
	[IdWayBill] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order_job', N'COLUMN',N'IdObject'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order_job', @level2type=N'COLUMN',@level2name=N'IdObject'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order_job', N'COLUMN',N'IdOrder'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order_job', @level2type=N'COLUMN',@level2name=N'IdOrder'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order_job', N'COLUMN',N'IdWayBill'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order_job', @level2type=N'COLUMN',@level2name=N'IdWayBill'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_job_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order_job]'))
ALTER TABLE [dbo].[md_order_job]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_job_md_object_Id] FOREIGN KEY([IdObject])
REFERENCES [dbo].[md_object] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_job_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order_job]'))
ALTER TABLE [dbo].[md_order_job] CHECK CONSTRAINT [fgn_key_FK_md_order_job_md_object_Id]
GO
