/****** Object:  Table [dbo].[data_zones]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[data_zones]') AND type in (N'U'))
DROP TABLE [dbo].[data_zones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[data_zones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[data_zones](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataGps_ID] [int] NULL,
	[Zone_ID] [int] NULL,
	[Zone_IO] [int] NULL,
 CONSTRAINT [PK_data_zones] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
