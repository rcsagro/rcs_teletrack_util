/****** Object:  Table [dbo].[lasttimes]    Script Date: 04/11/2013 11:22:24 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lasttimes__LastT__324172E1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lasttimes] DROP CONSTRAINT [DF__lasttimes__LastT__324172E1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lasttimes__ID__3335971A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lasttimes] DROP CONSTRAINT [DF__lasttimes__ID__3335971A]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lasttimes]') AND type in (N'U'))
DROP TABLE [dbo].[lasttimes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lasttimes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[lasttimes](
	[LastTime_ID] [int] NULL DEFAULT ((0)),
	[ID] [int] NULL DEFAULT ((0)),
	[Name] [char](200) NULL,
	[Descr] [char](200) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
