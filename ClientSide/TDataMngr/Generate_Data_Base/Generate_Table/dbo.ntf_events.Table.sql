/****** Object:  Table [dbo].[ntf_events]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_events_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_events]'))
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [fgn_key_FK_ntf_events_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_events_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_events]'))
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [fgn_key_FK_ntf_events_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_event__Event__48EFCE0F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [DF__ntf_event__Event__48EFCE0F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_event__ParIn__49E3F248]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [DF__ntf_event__ParIn__49E3F248]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_event__ParDb__4AD81681]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [DF__ntf_event__ParDb__4AD81681]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_event__ParDb__4BCC3ABA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [DF__ntf_event__ParDb__4BCC3ABA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_event__ParBo__4CC05EF3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_events] DROP CONSTRAINT [DF__ntf_event__ParBo__4CC05EF3]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_events]') AND type in (N'U'))
DROP TABLE [dbo].[ntf_events]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_events]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ntf_events](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[TypeEvent] [smallint] NOT NULL,
	[EventId] [int] NOT NULL DEFAULT ((0)),
	[ParInt1] [int] NOT NULL DEFAULT ((0)),
	[ParDbl1] [float] NOT NULL DEFAULT ((0)),
	[ParDbl2] [float] NOT NULL DEFAULT ((0)),
	[ParBool] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_ntf_events] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ntf_events]') AND name = N'FK_ntf_events_ntf_main_Id')
CREATE NONCLUSTERED INDEX [FK_ntf_events_ntf_main_Id] ON [dbo].[ntf_events] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_events', N'COLUMN',N'ParInt1'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'INT �������� ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_events', @level2type=N'COLUMN',@level2name=N'ParInt1'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_events', N'COLUMN',N'ParDbl1'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DOUBLE �������� �1 ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_events', @level2type=N'COLUMN',@level2name=N'ParDbl1'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_events', N'COLUMN',N'ParDbl2'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DOUBLE �������� �2 ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_events', @level2type=N'COLUMN',@level2name=N'ParDbl2'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_events', N'COLUMN',N'ParBool'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BOOL �������� ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_events', @level2type=N'COLUMN',@level2name=N'ParBool'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_events_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_events]'))
ALTER TABLE [dbo].[ntf_events]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_ntf_events_ntf_main_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[ntf_main] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_events_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_events]'))
ALTER TABLE [dbo].[ntf_events] CHECK CONSTRAINT [fgn_key_FK_ntf_events_ntf_main_Id]
GO
