/****** Object:  Table [dbo].[configsms]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__ID__40F9A68C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__ID__40F9A68C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__Flags__41EDCAC5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__Flags__41EDCAC5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__Messa__42E1EEFE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__Messa__42E1EEFE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__smsCe__43D61337]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__smsCe__43D61337]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__smsDs__44CA3770]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__smsDs__44CA3770]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsms__smsEm__45BE5BA9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsms] DROP CONSTRAINT [DF__configsms__smsEm__45BE5BA9]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configsms]') AND type in (N'U'))
DROP TABLE [dbo].[configsms]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configsms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configsms](
	[ConfigSms_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[smsCenter_ID] [int] NULL DEFAULT ((0)),
	[smsDspt_ID] [int] NULL DEFAULT ((0)),
	[smsEmailGate_ID] [int] NULL DEFAULT ((0)),
	[dsptEmail] [char](14) NULL,
	[dsptGprsEmail] [char](50) NULL,
 CONSTRAINT [PK_configsms] PRIMARY KEY CLUSTERED 
(
	[ConfigSms_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configsms]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configsms] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
