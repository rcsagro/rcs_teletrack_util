/****** Object:  Table [dbo].[datagpslost]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Mobit__725BF7F6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost] DROP CONSTRAINT [DF__datagpslo__Mobit__725BF7F6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Begin__73501C2F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost] DROP CONSTRAINT [DF__datagpslo__Begin__73501C2F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__End_L__74444068]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost] DROP CONSTRAINT [DF__datagpslo__End_L__74444068]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost]') AND type in (N'U'))
DROP TABLE [dbo].[datagpslost]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpslost](
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[Begin_LogID] [int] NOT NULL DEFAULT ((0)),
	[End_LogID] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_datagpslost] PRIMARY KEY CLUSTERED 
(
	[Mobitel_ID] ASC,
	[Begin_LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost]') AND name = N'IDX_MobitelID')
CREATE NONCLUSTERED INDEX [IDX_MobitelID] ON [dbo].[datagpslost] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagpslost', N'COLUMN',N'Begin_LogID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� LogID ����� �������� ���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagpslost', @level2type=N'COLUMN',@level2name=N'Begin_LogID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagpslost', N'COLUMN',N'End_LogID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� LogID ����� ������� ����������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagpslost', @level2type=N'COLUMN',@level2name=N'End_LogID'
GO
