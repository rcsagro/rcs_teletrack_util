/****** Object:  Table [dbo].[agro_ordert]    Script Date: 04/11/2013 11:22:21 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert]'))
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [fgn_key_Id_main_Order_FK1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert]'))
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [fgn_key_Id_main_Order_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_ma__2D27B809]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_ma__2D27B809]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_fi__2E1BDC42]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_fi__2E1BDC42]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_zo__2F10007B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_zo__2F10007B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_dr__300424B4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_dr__300424B4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_wo__30F848ED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_wo__30F848ED]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Dista__31EC6D26]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Dista__31EC6D26]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FactS__32E0915F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FactS__32E0915F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FactS__33D4B598]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FactS__33D4B598]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Price__34C8D9D1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Price__34C8D9D1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_ordert__Sum__35BCFE0A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_ordert__Sum__35BCFE0A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Speed__36B12243]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Speed__36B12243]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelS__37A5467C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelS__37A5467C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelA__38996AB5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelA__38996AB5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelS__398D8EEE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelS__398D8EEE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelE__3A81B327]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelE__3A81B327]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelE__3B75D760]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelE__3B75D760]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelE__3C69FB99]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelE__3C69FB99]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelE__3D5E1FD2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__FuelE__3D5E1FD2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Fuel___3E52440B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Fuel___3E52440B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Fuel___3F466844]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Fuel___3F466844]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Fuel___403A8C7D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Fuel___403A8C7D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Fuel___412EB0B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Fuel___412EB0B6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Fuel___4222D4EF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Fuel___4222D4EF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_ag__4316F928]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Id_ag__4316F928]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Confi__440B1D61]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Confi__440B1D61]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Point__44FF419A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__Point__44FF419A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__LockR__45F365D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert] DROP CONSTRAINT [DF__agro_orde__LockR__45F365D3]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert]') AND type in (N'U'))
DROP TABLE [dbo].[agro_ordert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_ordert](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL DEFAULT ((0)),
	[Id_field] [int] NOT NULL DEFAULT ((0)),
	[Id_zone] [int] NOT NULL DEFAULT ((0)),
	[Id_driver] [int] NOT NULL DEFAULT ((0)),
	[TimeStart] [datetime] NULL,
	[TimeEnd] [datetime] NULL,
	[TimeMove] [time](7) NULL,
	[TimeStop] [time](7) NULL,
	[TimeRate] [time](7) NULL,
	[Id_work] [int] NOT NULL DEFAULT ((0)),
	[FactTime] [time](7) NULL,
	[Distance] [float] NOT NULL DEFAULT ((0)),
	[FactSquare] [float] NOT NULL DEFAULT ((0.00000)),
	[FactSquareCalc] [float] NOT NULL DEFAULT ((0.00000)),
	[Price] [float] NOT NULL DEFAULT ((0.00)),
	[Sum] [float] NOT NULL DEFAULT ((0.00)),
	[SpeedAvg] [float] NOT NULL DEFAULT ((0)),
	[Comment] [varchar](max) NULL,
	[FuelStart] [float] NOT NULL DEFAULT ((0.00)),
	[FuelAdd] [float] NOT NULL DEFAULT ((0.00)),
	[FuelSub] [float] NOT NULL DEFAULT ((0.00)),
	[FuelEnd] [float] NOT NULL DEFAULT ((0.00)),
	[FuelExpens] [float] NOT NULL DEFAULT ((0.00)),
	[FuelExpensAvg] [float] NOT NULL DEFAULT ((0.00)),
	[FuelExpensAvgRate] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensAvgRate] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensMove] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensStop] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensAvg] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensTotal] [float] NOT NULL DEFAULT ((0.00)),
	[Id_agregat] [int] NOT NULL DEFAULT ((0)),
	[Confirm] [smallint] NOT NULL DEFAULT ((1)),
	[PointsValidity] [int] NOT NULL DEFAULT ((0)),
	[LockRecord] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_agro_ordert] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert]') AND name = N'Id_main_Order_FK1')
CREATE NONCLUSTERED INDEX [Id_main_Order_FK1] ON [dbo].[agro_ordert] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_field'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_field'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_zone'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_zone'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_driver'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_driver'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'TimeStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������ ������ � ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'TimeStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'TimeEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ��������� ����� � ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'TimeEnd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'TimeMove'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'TimeMove'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'TimeStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� �������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'TimeStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'TimeRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'TimeRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_work'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_work'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FactTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FactTime'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FactSquare'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FactSquare'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FactSquareCalc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������������ ������� ���������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FactSquareCalc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Price'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Price'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Sum'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� �� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Sum'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'SpeedAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �������� ���������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'SpeedAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Comment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� � ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelAdd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelAdd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelSub'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelSub'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� � �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelEnd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelExpens'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelExpens'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelExpensAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� ������ ������� �� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelExpensAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'FuelExpensAvgRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ������, �/�������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'FuelExpensAvgRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Fuel_ExpensAvgRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������� ������, �/�������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensAvgRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Fuel_ExpensMove'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������ ������� � ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensMove'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Fuel_ExpensStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������ ������� �� ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Fuel_ExpensAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������� ������, �/��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Fuel_ExpensTotal'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ����� ������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensTotal'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Id_agregat'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Id_agregat'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'Confirm'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ��������� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'Confirm'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'PointsValidity'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'PointsValidity'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert', N'COLUMN',N'LockRecord'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������������� ��� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert', @level2type=N'COLUMN',@level2name=N'LockRecord'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert]'))
ALTER TABLE [dbo].[agro_ordert]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_main_Order_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_order] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert]'))
ALTER TABLE [dbo].[agro_ordert] CHECK CONSTRAINT [fgn_key_Id_main_Order_FK1]
GO
