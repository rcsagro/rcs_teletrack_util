/****** Object:  Table [dbo].[mobitels]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__ID__34E8D562]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__ID__34E8D562]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Flags__35DCF99B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Flags__35DCF99B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Servic__36D11DD4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Servic__36D11DD4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Servic__37C5420D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Servic__37C5420D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Intern__38B96646]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Intern__38B96646]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__MapSty__39AD8A7F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__MapSty__39AD8A7F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__MapSty__3AA1AEB8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__MapSty__3AA1AEB8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__MapSty__3B95D2F1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__MapSty__3B95D2F1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Route___3C89F72A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Route___3C89F72A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Device__3D7E1B63]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Device__3D7E1B63]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Device__3E723F9C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Device__3E723F9C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__Confir__3F6663D5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__Confir__3F6663D5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels__LastIn__405A880E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels] DROP CONSTRAINT [DF__mobitels__LastIn__405A880E]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels]') AND type in (N'U'))
DROP TABLE [dbo].[mobitels]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mobitels](
	[Mobitel_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [bigint] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[ServiceInit_ID] [int] NULL DEFAULT ((0)),
	[ServiceSend_ID] [int] NULL DEFAULT ((0)),
	[InternalMobitelConfig_ID] [int] NULL DEFAULT ((0)),
	[MapStyleLine_ID] [int] NULL DEFAULT ((0)),
	[MapStyleLastPoint_ID] [int] NULL DEFAULT ((0)),
	[MapStylePoint_ID] [int] NULL DEFAULT ((0)),
	[Route_ID] [int] NULL DEFAULT ((0)),
	[DeviceType] [int] NULL DEFAULT ((0)),
	[DeviceEngine] [int] NULL DEFAULT ((0)),
	[ConfirmedID] [int] NOT NULL DEFAULT ((0)),
	[LastInsertTime] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_mobitels] PRIMARY KEY CLUSTERED 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[mobitels]') AND name = N'IDX_InternalmobitelconfigID')
CREATE NONCLUSTERED INDEX [IDX_InternalmobitelconfigID] ON [dbo].[mobitels] 
(
	[InternalMobitelConfig_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'mobitels', N'COLUMN',N'ConfirmedID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MIN �������� LogID ����� �������� ��������� ��� ��������� ��� ��� ���������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mobitels', @level2type=N'COLUMN',@level2name=N'ConfirmedID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'mobitels', N'COLUMN',N'LastInsertTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UNIX_TIMESTAMP ����� ��������� ������� ������ � DataGPS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mobitels', @level2type=N'COLUMN',@level2name=N'LastInsertTime'
GO
