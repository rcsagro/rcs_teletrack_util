/****** Object:  Table [dbo].[users_acs_objects]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_users_acs_objects_users_acs_roles_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[users_acs_objects]'))
ALTER TABLE [dbo].[users_acs_objects] DROP CONSTRAINT [fgn_key_FK_users_acs_objects_users_acs_roles_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_users_acs_objects_users_acs_roles_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[users_acs_objects]'))
ALTER TABLE [dbo].[users_acs_objects] DROP CONSTRAINT [fgn_key_FK_users_acs_objects_users_acs_roles_Id]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_acs_objects]') AND type in (N'U'))
DROP TABLE [dbo].[users_acs_objects]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_acs_objects]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[users_acs_objects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_role] [int] NOT NULL,
	[Id_type] [int] NOT NULL,
	[Id_object] [int] NOT NULL,
 CONSTRAINT [PK_users_acs_objects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[users_acs_objects]') AND name = N'FK_users_acs_objects_users_acs_roles_Id')
CREATE NONCLUSTERED INDEX [FK_users_acs_objects_users_acs_roles_Id] ON [dbo].[users_acs_objects] 
(
	[Id_role] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_users_acs_objects_users_acs_roles_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[users_acs_objects]'))
ALTER TABLE [dbo].[users_acs_objects]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_users_acs_objects_users_acs_roles_Id] FOREIGN KEY([Id_role])
REFERENCES [dbo].[users_acs_roles] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_users_acs_objects_users_acs_roles_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[users_acs_objects]'))
ALTER TABLE [dbo].[users_acs_objects] CHECK CONSTRAINT [fgn_key_FK_users_acs_objects_users_acs_roles_Id]
GO
