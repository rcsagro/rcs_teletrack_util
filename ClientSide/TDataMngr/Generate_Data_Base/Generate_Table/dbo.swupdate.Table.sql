/****** Object:  Table [dbo].[swupdate]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__swupdate__ShortI__0F4D3C5F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[swupdate] DROP CONSTRAINT [DF__swupdate__ShortI__0F4D3C5F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__swupdate__Insert__10416098]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[swupdate] DROP CONSTRAINT [DF__swupdate__Insert__10416098]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[swupdate]') AND type in (N'U'))
DROP TABLE [dbo].[swupdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[swupdate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[swupdate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ShortID] [char](4) NOT NULL DEFAULT (''),
	[Soft] [varchar](max) NOT NULL,
	[InsertTime] [datetime] NOT NULL DEFAULT ('GETDATE()'),
 CONSTRAINT [PK_swupdate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'swupdate', N'COLUMN',N'ShortID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ���������(�����)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'swupdate', @level2type=N'COLUMN',@level2name=N'ShortID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'swupdate', N'COLUMN',N'Soft'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'swupdate', @level2type=N'COLUMN',@level2name=N'Soft'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'swupdate', N'COLUMN',N'InsertTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ������� � �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'swupdate', @level2type=N'COLUMN',@level2name=N'InsertTime'
GO
