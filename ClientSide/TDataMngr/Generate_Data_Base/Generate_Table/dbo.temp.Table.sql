/****** Object:  Table [dbo].[temp]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__temp__datagps_id__15FA39EE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[temp] DROP CONSTRAINT [DF__temp__datagps_id__15FA39EE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__temp__sensor_id__16EE5E27]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[temp] DROP CONSTRAINT [DF__temp__sensor_id__16EE5E27]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__temp__Value__17E28260]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[temp] DROP CONSTRAINT [DF__temp__Value__17E28260]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp]') AND type in (N'U'))
DROP TABLE [dbo].[temp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[temp](
	[datagps_id] [bigint] NOT NULL DEFAULT ((0)),
	[sensor_id] [bigint] NOT NULL DEFAULT ((0)),
	[Value] [float] NOT NULL DEFAULT ((0.00))
) ON [PRIMARY]
END
GO
