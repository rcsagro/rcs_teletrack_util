/****** Object:  Table [dbo].[datagpslost_ontmp]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__LogID__7BE56230]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_ontmp] DROP CONSTRAINT [DF__datagpslo__LogID__7BE56230]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_ontmp]') AND type in (N'U'))
DROP TABLE [dbo].[datagpslost_ontmp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_ontmp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpslost_ontmp](
	[LogID] [int] NOT NULL DEFAULT ((0)),
	[SrvPacketID] [bigint] NOT NULL
) ON [PRIMARY]
END
GO
