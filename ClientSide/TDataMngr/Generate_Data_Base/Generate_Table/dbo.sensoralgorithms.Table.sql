/****** Object:  Table [dbo].[sensoralgorithms]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensoralgo__Name__4E739D3B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensoralgorithms] DROP CONSTRAINT [DF__sensoralgo__Name__4E739D3B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensoralg__Algor__4F67C174]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensoralgorithms] DROP CONSTRAINT [DF__sensoralg__Algor__4F67C174]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensoralgorithms]') AND type in (N'U'))
DROP TABLE [dbo].[sensoralgorithms]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensoralgorithms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensoralgorithms](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](45) NOT NULL DEFAULT (''),
	[Description] [char](45) NULL,
	[AlgorithmID] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_sensoralgorithms] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sensoralgorithms]') AND name = N'ID')
CREATE UNIQUE NONCLUSTERED INDEX [ID] ON [dbo].[sensoralgorithms] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
