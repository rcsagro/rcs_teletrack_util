/****** Object:  Table [dbo].[users_actions_log]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_act__TypeL__3B2BBE9D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_actions_log] DROP CONSTRAINT [DF__users_act__TypeL__3B2BBE9D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_act__Id_do__3C1FE2D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_actions_log] DROP CONSTRAINT [DF__users_act__Id_do__3C1FE2D6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_act__TextL__3D14070F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_actions_log] DROP CONSTRAINT [DF__users_act__TextL__3D14070F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_acti__User__3E082B48]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_actions_log] DROP CONSTRAINT [DF__users_acti__User__3E082B48]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_act__Compu__3EFC4F81]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_actions_log] DROP CONSTRAINT [DF__users_act__Compu__3EFC4F81]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_actions_log]') AND type in (N'U'))
DROP TABLE [dbo].[users_actions_log]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_actions_log]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[users_actions_log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeLog] [int] NOT NULL DEFAULT ((0)),
	[DateAction] [datetime] NOT NULL,
	[Id_document] [int] NOT NULL DEFAULT ((0)),
	[TextLog] [varchar](255) NOT NULL DEFAULT (''),
	[User] [varchar](127) NOT NULL DEFAULT (''),
	[Computer] [varchar](63) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_users_actions_log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
