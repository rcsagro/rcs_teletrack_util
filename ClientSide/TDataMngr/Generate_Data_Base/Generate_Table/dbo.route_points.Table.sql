/****** Object:  Table [dbo].[route_points]    Script Date: 04/11/2013 11:22:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_points]') AND type in (N'U'))
DROP TABLE [dbo].[route_points]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_points]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[route_points](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Route_ID] [int] NULL,
	[Zone_ID] [int] NULL,
 CONSTRAINT [PK_route_points] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
