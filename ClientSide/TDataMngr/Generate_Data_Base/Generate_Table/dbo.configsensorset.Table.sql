/****** Object:  Table [dbo].[configsensorset]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsensor__ID__3D2915A8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsensorset] DROP CONSTRAINT [DF__configsensor__ID__3D2915A8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsen__Flags__3E1D39E1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsensorset] DROP CONSTRAINT [DF__configsen__Flags__3E1D39E1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configsen__Messa__3F115E1A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configsensorset] DROP CONSTRAINT [DF__configsen__Messa__3F115E1A]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configsensorset]') AND type in (N'U'))
DROP TABLE [dbo].[configsensorset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configsensorset]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configsensorset](
	[ConfigSensorSet_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_configsensorset] PRIMARY KEY CLUSTERED 
(
	[ConfigSensorSet_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
