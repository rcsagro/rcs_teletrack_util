/****** Object:  Table [dbo].[configgprsemail]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgprsem__ID__0E6E26BF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgprsem__ID__0E6E26BF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Flags__0F624AF8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Flags__0F624AF8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Messa__10566F31]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Messa__10566F31]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Smtps__114A936A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Smtps__114A936A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Smtpu__123EB7A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Smtpu__123EB7A3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Smtpp__1332DBDC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Smtpp__1332DBDC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Pop3s__14270015]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Pop3s__14270015]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Pop3u__151B244E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Pop3u__151B244E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Pop3p__160F4887]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsemail] DROP CONSTRAINT [DF__configgpr__Pop3p__160F4887]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsemail]') AND type in (N'U'))
DROP TABLE [dbo].[configgprsemail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsemail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configgprsemail](
	[ConfigGprsEmail_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Descr] [varchar](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Smtpserv] [varchar](50) NOT NULL DEFAULT (''),
	[Smtpun] [varchar](50) NOT NULL DEFAULT (''),
	[Smtppw] [varchar](50) NOT NULL DEFAULT (''),
	[Pop3serv] [varchar](50) NOT NULL DEFAULT (''),
	[Pop3un] [varchar](50) NOT NULL DEFAULT (''),
	[Pop3pw] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_configgprsemail] PRIMARY KEY CLUSTERED 
(
	[ConfigGprsEmail_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configgprsemail]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configgprsemail] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
