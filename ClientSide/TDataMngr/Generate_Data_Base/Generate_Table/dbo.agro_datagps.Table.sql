/****** Object:  Table [dbo].[agro_datagps]    Script Date: 04/11/2013 11:22:21 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_datagps_agro_ordert_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_datagps]'))
ALTER TABLE [dbo].[agro_datagps] DROP CONSTRAINT [fgn_key_FK_agro_datagps_agro_ordert_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_datagps_agro_ordert_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_datagps]'))
ALTER TABLE [dbo].[agro_datagps] DROP CONSTRAINT [fgn_key_FK_agro_datagps_agro_ordert_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_data__Lon_d__0BC6C43E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_datagps] DROP CONSTRAINT [DF__agro_data__Lon_d__0BC6C43E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_data__Lat_d__0CBAE877]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_datagps] DROP CONSTRAINT [DF__agro_data__Lat_d__0CBAE877]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_data__Speed__0DAF0CB0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_datagps] DROP CONSTRAINT [DF__agro_data__Speed__0DAF0CB0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_datagps]') AND type in (N'U'))
DROP TABLE [dbo].[agro_datagps]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_datagps]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_datagps](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Lon_base] [float] NOT NULL,
	[Lat_base] [float] NOT NULL,
	[Lon_dev] [varchar](255) NOT NULL DEFAULT (''),
	[Lat_dev] [varchar](255) NOT NULL DEFAULT (''),
	[Speed_base] [float] NOT NULL,
	[Speed_dev] [varchar](255) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_agro_datagps] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_datagps]') AND name = N'FK_agro_datagps_agro_ordert_Id')
CREATE NONCLUSTERED INDEX [FK_agro_datagps_agro_ordert_Id] ON [dbo].[agro_datagps] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_datagps_agro_ordert_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_datagps]'))
ALTER TABLE [dbo].[agro_datagps]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_datagps_agro_ordert_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_ordert] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_datagps_agro_ordert_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_datagps]'))
ALTER TABLE [dbo].[agro_datagps] CHECK CONSTRAINT [fgn_key_FK_agro_datagps_agro_ordert_Id]
GO
