/****** Object:  Table [dbo].[md_ordert]    Script Date: 04/11/2013 11:22:25 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_cargo_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargoUnload_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_cargoUnload_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_object_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_order_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_cargo_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargoUnload_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_cargoUnload_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_object_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [fgn_key_FK_md_ordert_md_order_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__IdObj__0EC32C7A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__IdObj__0EC32C7A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__IdCar__0FB750B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__IdCar__0FB750B3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__QtyLo__10AB74EC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__QtyLo__10AB74EC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__TimeL__119F9925]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__TimeL__119F9925]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__IdCar__1293BD5E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__IdCar__1293BD5E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__QtyUn__1387E197]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__QtyUn__1387E197]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__TimeU__147C05D0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__TimeU__147C05D0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__TimeD__15702A09]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__TimeD__15702A09]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__TimeT__16644E42]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__TimeT__16644E42]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__Dista__1758727B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__Dista__1758727B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_ordert__IsClo__184C96B4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_ordert] DROP CONSTRAINT [DF__md_ordert__IsClo__184C96B4]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND type in (N'U'))
DROP TABLE [dbo].[md_ordert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_ordert](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[IdObject] [int] NOT NULL DEFAULT ((0)),
	[IdCargoLoading] [int] NULL DEFAULT ((0)),
	[QtyLoading] [decimal](10, 2) NOT NULL DEFAULT ((0)),
	[TimeLoading] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[IdCargoUnLoading] [int] NULL DEFAULT ((0)),
	[QtyUnLoading] [decimal](10, 2) NOT NULL DEFAULT ((0)),
	[TimeUnLoading] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[TimeDelay] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[TimeToObject] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[DistanceToObject] [decimal](10, 2) NOT NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
	[IsClose] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_md_ordert] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND name = N'FK_md_ordert_md_cargo_Id')
CREATE NONCLUSTERED INDEX [FK_md_ordert_md_cargo_Id] ON [dbo].[md_ordert] 
(
	[IdCargoLoading] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND name = N'FK_md_ordert_md_cargoUnload_Id')
CREATE NONCLUSTERED INDEX [FK_md_ordert_md_cargoUnload_Id] ON [dbo].[md_ordert] 
(
	[IdCargoUnLoading] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND name = N'FK_md_ordert_md_object_Id')
CREATE NONCLUSTERED INDEX [FK_md_ordert_md_object_Id] ON [dbo].[md_ordert] 
(
	[IdObject] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_ordert]') AND name = N'FK_md_ordert_md_order_Id')
CREATE NONCLUSTERED INDEX [FK_md_ordert_md_order_Id] ON [dbo].[md_ordert] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'IdCargoLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'IdCargoLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'QtyLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'QtyLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'TimeLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'TimeLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'IdCargoUnLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'IdCargoUnLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'QtyUnLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������ ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'QtyUnLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'TimeUnLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'TimeUnLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'TimeDelay'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'TimeDelay'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'TimeToObject'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� �� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'TimeToObject'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_ordert', N'COLUMN',N'DistanceToObject'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_ordert', @level2type=N'COLUMN',@level2name=N'DistanceToObject'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_ordert_md_cargo_Id] FOREIGN KEY([IdCargoLoading])
REFERENCES [dbo].[md_cargo] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] CHECK CONSTRAINT [fgn_key_FK_md_ordert_md_cargo_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargoUnload_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_ordert_md_cargoUnload_Id] FOREIGN KEY([IdCargoUnLoading])
REFERENCES [dbo].[md_cargo] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_cargoUnload_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] CHECK CONSTRAINT [fgn_key_FK_md_ordert_md_cargoUnload_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_ordert_md_object_Id] FOREIGN KEY([IdObject])
REFERENCES [dbo].[md_object] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] CHECK CONSTRAINT [fgn_key_FK_md_ordert_md_object_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_ordert_md_order_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[md_order] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_ordert_md_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_ordert]'))
ALTER TABLE [dbo].[md_ordert] CHECK CONSTRAINT [fgn_key_FK_md_ordert_md_order_Id]
GO
