/****** Object:  Table [dbo].[report_pass_zones]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPZ_Zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[report_pass_zones]'))
ALTER TABLE [dbo].[report_pass_zones] DROP CONSTRAINT [FK_RPZ_Zones]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPZ_Zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[report_pass_zones]'))
ALTER TABLE [dbo].[report_pass_zones] DROP CONSTRAINT [FK_RPZ_Zones]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[report_pass_zones]') AND type in (N'U'))
DROP TABLE [dbo].[report_pass_zones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[report_pass_zones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[report_pass_zones](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ZoneID] [int] NOT NULL,
 CONSTRAINT [PK_report_pass_zones] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[report_pass_zones]') AND name = N'IDX_ZoneID')
CREATE NONCLUSTERED INDEX [IDX_ZoneID] ON [dbo].[report_pass_zones] 
(
	[ZoneID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'report_pass_zones', N'COLUMN',N'ZoneID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'report_pass_zones', @level2type=N'COLUMN',@level2name=N'ZoneID'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPZ_Zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[report_pass_zones]'))
ALTER TABLE [dbo].[report_pass_zones]  WITH CHECK ADD  CONSTRAINT [FK_RPZ_Zones] FOREIGN KEY([ZoneID])
REFERENCES [dbo].[zones] ([Zone_ID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPZ_Zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[report_pass_zones]'))
ALTER TABLE [dbo].[report_pass_zones] CHECK CONSTRAINT [FK_RPZ_Zones]
GO
