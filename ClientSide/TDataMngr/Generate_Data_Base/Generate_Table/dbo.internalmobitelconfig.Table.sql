/****** Object:  Table [dbo].[internalmobitelconfig]    Script Date: 04/11/2013 11:22:24 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__internalmobi__ID__26CFC035]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[internalmobitelconfig] DROP CONSTRAINT [DF__internalmobi__ID__26CFC035]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__internalm__Messa__27C3E46E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[internalmobitelconfig] DROP CONSTRAINT [DF__internalm__Messa__27C3E46E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__internalm__RfTyp__28B808A7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[internalmobitelconfig] DROP CONSTRAINT [DF__internalm__RfTyp__28B808A7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__internalm__Activ__29AC2CE0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[internalmobitelconfig] DROP CONSTRAINT [DF__internalm__Activ__29AC2CE0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__internalm__modul__2AA05119]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[internalmobitelconfig] DROP CONSTRAINT [DF__internalm__modul__2AA05119]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[internalmobitelconfig]') AND type in (N'U'))
DROP TABLE [dbo].[internalmobitelconfig]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[internalmobitelconfig]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[internalmobitelconfig](
	[InternalMobitelConfig_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[devIdLong] [char](16) NULL,
	[devIdShort] [char](4) NULL,
	[verProtocolLong] [char](16) NULL,
	[verProtocolShort] [char](2) NULL,
	[moduleIdGps] [char](50) NULL,
	[moduleIdGsm] [char](50) NULL,
	[moduleIdRf] [char](50) NULL,
	[RfType] [smallint] NOT NULL DEFAULT ((0)),
	[ActiveRf] [smallint] NOT NULL DEFAULT ((0)),
	[moduleIdSs] [char](50) NULL,
	[moduleIdMm] [char](50) NOT NULL DEFAULT ('moduleIdMm'),
 CONSTRAINT [PK_internalmobitelconfig] PRIMARY KEY CLUSTERED 
(
	[InternalMobitelConfig_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[internalmobitelconfig]') AND name = N'IDX_InternalmobitelconfigID')
CREATE NONCLUSTERED INDEX [IDX_InternalmobitelconfigID] ON [dbo].[internalmobitelconfig] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[internalmobitelconfig]') AND name = N'Index_3')
CREATE NONCLUSTERED INDEX [Index_3] ON [dbo].[internalmobitelconfig] 
(
	[Message_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[internalmobitelconfig]') AND name = N'Index_4')
CREATE NONCLUSTERED INDEX [Index_4] ON [dbo].[internalmobitelconfig] 
(
	[devIdShort] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
