/****** Object:  Table [dbo].[agro_work]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_work_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [fgn_key_FK_agro_work_agro_work_types_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_workgroup_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [fgn_key_FK_agro_work_agro_workgroup_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_work_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [fgn_key_FK_agro_work_agro_work_types_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_workgroup_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [fgn_key_FK_agro_work_agro_workgroup_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_work__Name__5535A963]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [DF__agro_work__Name__5535A963]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_work__Speed__5629CD9C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [DF__agro_work__Speed__5629CD9C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_work__Speed__571DF1D5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [DF__agro_work__Speed__571DF1D5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_work__TypeW__5812160E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_work] DROP CONSTRAINT [DF__agro_work__TypeW__5812160E]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_work]') AND type in (N'U'))
DROP TABLE [dbo].[agro_work]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_work]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_work](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[SpeedBottom] [float] NULL DEFAULT ((0)),
	[SpeedTop] [float] NULL DEFAULT ((0)),
	[Comment] [varchar](max) NULL,
	[TypeWork] [int] NOT NULL DEFAULT ((1)),
	[Id_main] [int] NULL,
 CONSTRAINT [PK_agro_work] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_work]') AND name = N'FK_agro_work_agro_work_types_Id')
CREATE NONCLUSTERED INDEX [FK_agro_work_agro_work_types_Id] ON [dbo].[agro_work] 
(
	[TypeWork] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_work]') AND name = N'FK_agro_work_agro_workgroup_Id')
CREATE NONCLUSTERED INDEX [FK_agro_work_agro_workgroup_Id] ON [dbo].[agro_work] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'SpeedBottom'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'SpeedBottom'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'SpeedTop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'SpeedTop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'Comment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'TypeWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ � ����, ������� ��� ����,������� �� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'TypeWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_work', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ����� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_work', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_work_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_work_agro_work_types_Id] FOREIGN KEY([TypeWork])
REFERENCES [dbo].[agro_work_types] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_work_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] CHECK CONSTRAINT [fgn_key_FK_agro_work_agro_work_types_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_workgroup_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_work_agro_workgroup_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_workgroup] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_work_agro_workgroup_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_work]'))
ALTER TABLE [dbo].[agro_work] CHECK CONSTRAINT [fgn_key_FK_agro_work_agro_workgroup_Id]
GO
