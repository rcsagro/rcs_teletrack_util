/****** Object:  Table [dbo].[users_acs_roles]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_acs___Name__3943762B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_acs_roles] DROP CONSTRAINT [DF__users_acs___Name__3943762B]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_acs_roles]') AND type in (N'U'))
DROP TABLE [dbo].[users_acs_roles]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_acs_roles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[users_acs_roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_users_acs_roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
