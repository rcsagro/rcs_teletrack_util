/****** Object:  Table [dbo].[zonesgroup]    Script Date: 04/11/2013 11:22:30 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonesgrou__Title__69E6AD86]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonesgroup] DROP CONSTRAINT [DF__zonesgrou__Title__69E6AD86]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zonesgrou__Descr__6ADAD1BF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zonesgroup] DROP CONSTRAINT [DF__zonesgrou__Descr__6ADAD1BF]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zonesgroup]') AND type in (N'U'))
DROP TABLE [dbo].[zonesgroup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zonesgroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zonesgroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL DEFAULT (''),
	[Description] [varchar](200) NOT NULL DEFAULT (''),
	[OutLinkId] [varchar](20) NULL,
 CONSTRAINT [PK_zonesgroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zonesgroup', N'COLUMN',N'OutLinkId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zonesgroup', @level2type=N'COLUMN',@level2name=N'OutLinkId'
GO
