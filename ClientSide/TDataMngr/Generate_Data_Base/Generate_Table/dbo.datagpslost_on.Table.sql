/****** Object:  Table [dbo].[datagpslost_on]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Mobit__762C88DA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_on] DROP CONSTRAINT [DF__datagpslo__Mobit__762C88DA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Begin__7720AD13]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_on] DROP CONSTRAINT [DF__datagpslo__Begin__7720AD13]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__End_L__7814D14C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_on] DROP CONSTRAINT [DF__datagpslo__End_L__7814D14C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Begin__7908F585]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_on] DROP CONSTRAINT [DF__datagpslo__Begin__7908F585]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__End_S__79FD19BE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_on] DROP CONSTRAINT [DF__datagpslo__End_S__79FD19BE]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_on]') AND type in (N'U'))
DROP TABLE [dbo].[datagpslost_on]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_on]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpslost_on](
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[Begin_LogID] [int] NOT NULL DEFAULT ((0)),
	[End_LogID] [int] NOT NULL DEFAULT ((0)),
	[Begin_SrvPacketID] [bigint] NOT NULL DEFAULT ((0)),
	[End_SrvPacketID] [bigint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_datagpslost_on] PRIMARY KEY CLUSTERED 
(
	[Mobitel_ID] ASC,
	[Begin_LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_on]') AND name = N'IDX_MobitelID')
CREATE NONCLUSTERED INDEX [IDX_MobitelID] ON [dbo].[datagpslost_on] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_on]') AND name = N'IDX_MobitelID_BeginSrvPacketID')
CREATE UNIQUE NONCLUSTERED INDEX [IDX_MobitelID_BeginSrvPacketID] ON [dbo].[datagpslost_on] 
(
	[Mobitel_ID] ASC,
	[Begin_SrvPacketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagpslost_on', N'COLUMN',N'Begin_LogID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� LogID ����� �������� ���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagpslost_on', @level2type=N'COLUMN',@level2name=N'Begin_LogID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagpslost_on', N'COLUMN',N'End_LogID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� LogID ����� ������� ����������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagpslost_on', @level2type=N'COLUMN',@level2name=N'End_LogID'
GO
