/****** Object:  Table [dbo].[lastrecords]    Script Date: 04/11/2013 11:22:24 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lastrecor__LastR__2F650636]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lastrecords] DROP CONSTRAINT [DF__lastrecor__LastR__2F650636]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lastrecords__ID__30592A6F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lastrecords] DROP CONSTRAINT [DF__lastrecords__ID__30592A6F]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lastrecords]') AND type in (N'U'))
DROP TABLE [dbo].[lastrecords]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lastrecords]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[lastrecords](
	[LastRecord_ID] [int] NULL DEFAULT ((0)),
	[ID] [int] NULL DEFAULT ((0)),
	[Name] [char](200) NULL,
	[Descr] [char](200) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
