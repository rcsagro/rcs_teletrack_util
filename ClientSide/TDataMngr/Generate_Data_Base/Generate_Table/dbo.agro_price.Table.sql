/****** Object:  Table [dbo].[agro_price]    Script Date: 04/11/2013 11:22:21 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_price__Name__4CA06362]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_price] DROP CONSTRAINT [DF__agro_price__Name__4CA06362]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_price]') AND type in (N'U'))
DROP TABLE [dbo].[agro_price]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_price]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_price](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DateInit] [date] NULL,
	[DateComand] [date] NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_agro_price] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_price', N'COLUMN',N'DateInit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_price', @level2type=N'COLUMN',@level2name=N'DateInit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_price', N'COLUMN',N'DateComand'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ,� ������ ��������� ���� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_price', @level2type=N'COLUMN',@level2name=N'DateComand'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_price', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_price', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_price', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_price', @level2type=N'COLUMN',@level2name=N'Comment'
GO
