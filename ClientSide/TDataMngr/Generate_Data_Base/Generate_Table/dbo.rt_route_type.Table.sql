/****** Object:  Table [dbo].[rt_route_type]    Script Date: 04/11/2013 11:22:27 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route_t__Name__2A363CC5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_type] DROP CONSTRAINT [DF__rt_route_t__Name__2A363CC5]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_type]') AND type in (N'U'))
DROP TABLE [dbo].[rt_route_type]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_type]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_route_type](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_rt_route_type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route_type', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route_type', @level2type=N'COLUMN',@level2name=N'Name'
GO
