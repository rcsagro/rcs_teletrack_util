/****** Object:  Table [dbo].[serviceinit]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__serviceinit__ID__59E54FE7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__serviceinit__ID__59E54FE7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__Flags__5AD97420]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__Flags__5AD97420]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__Servi__5BCD9859]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__Servi__5BCD9859]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__CAtoS__5CC1BC92]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__CAtoS__5CC1BC92]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__CAtoS__5DB5E0CB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__CAtoS__5DB5E0CB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__SAtoC__5EAA0504]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__SAtoC__5EAA0504]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__SAtoC__5F9E293D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__SAtoC__5F9E293D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__SAtoC__60924D76]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__SAtoC__60924D76]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__CAtoS__618671AF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__CAtoS__618671AF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__Mobit__627A95E8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__Mobit__627A95E8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicein__Activ__636EBA21]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[serviceinit] DROP CONSTRAINT [DF__servicein__Activ__636EBA21]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[serviceinit]') AND type in (N'U'))
DROP TABLE [dbo].[serviceinit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[serviceinit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[serviceinit](
	[ServiceInit_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[ServiceType_ID] [int] NULL DEFAULT ((0)),
	[A1] [char](50) NULL,
	[A2] [char](50) NULL,
	[A3] [char](50) NULL,
	[A4] [char](50) NULL,
	[A5] [char](50) NULL,
	[A6] [char](50) NULL,
	[A7] [char](50) NULL,
	[A8] [char](50) NULL,
	[A9] [char](50) NULL,
	[A10] [char](200) NULL,
	[CAtoSAint] [int] NULL DEFAULT ((0)),
	[CAtoSAtime] [int] NULL DEFAULT ((0)),
	[SAtoCAint] [int] NULL DEFAULT ((0)),
	[SAtoCAtime] [int] NULL DEFAULT ((0)),
	[SAtoCAtext] [char](50) NULL DEFAULT ('STOPPED'),
	[CAtoSAtext] [char](50) NULL DEFAULT ('STOPPED'),
	[MobitelActive_ID] [int] NOT NULL DEFAULT ((0)),
	[Active] [tinyint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_serviceinit] PRIMARY KEY CLUSTERED 
(
	[ServiceInit_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
