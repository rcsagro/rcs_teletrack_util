/****** Object:  Table [dbo].[agro_order]    Script Date: 04/11/2013 11:22:21 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_order]'))
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [fgn_key_Id_mobitel_Order_FK1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_order]'))
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [fgn_key_Id_mobitel_Order_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_mo__1BFD2C07]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Id_mo__1BFD2C07]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Regim__1CF15040]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Regim__1CF15040]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Dista__1DE57479]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Dista__1DE57479]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__PathW__1ED998B2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__PathW__1ED998B2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__1FCDBCEB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__1FCDBCEB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__20C1E124]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__20C1E124]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__21B6055D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__21B6055D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__22AA2996]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__22AA2996]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__239E4DCF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__239E4DCF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__24927208]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__24927208]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__25869641]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__25869641]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FuelD__267ABA7A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FuelD__267ABA7A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__FactS__276EDEB3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__FactS__276EDEB3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Point__286302EC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Point__286302EC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Point__29572725]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Point__29572725]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__State__2A4B4B5E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__State__2A4B4B5E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Block__2B3F6F97]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_order] DROP CONSTRAINT [DF__agro_orde__Block__2B3F6F97]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_order]') AND type in (N'U'))
DROP TABLE [dbo].[agro_order]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_order]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NULL,
	[Id_mobitel] [int] NOT NULL DEFAULT ((0)),
	[LocationStart] [varchar](255) NULL,
	[LocationEnd] [varchar](255) NULL,
	[TimeStart] [datetime] NULL,
	[TimeEnd] [datetime] NULL,
	[Regime] [smallint] NULL DEFAULT ((0)),
	[TimeWork] [char](10) NULL,
	[TimeMove] [char](10) NULL,
	[Distance] [float] NULL DEFAULT ((0)),
	[SpeedAvg] [float] NULL,
	[Comment] [varchar](max) NULL,
	[PathWithoutWork] [float] NULL DEFAULT ((0)),
	[SquareWorkDescript] [varchar](max) NULL,
	[FuelDUTExpensSquare] [float] NULL DEFAULT ((0)),
	[FuelDRTExpensSquare] [float] NULL DEFAULT ((0)),
	[FuelDUTExpensAvgSquare] [float] NULL DEFAULT ((0)),
	[FuelDRTExpensAvgSquare] [float] NULL DEFAULT ((0)),
	[FuelDUTExpensWithoutWork] [float] NULL DEFAULT ((0)),
	[FuelDRTExpensWithoutWork] [float] NULL DEFAULT ((0)),
	[FuelDUTExpensAvgWithoutWork] [float] NULL DEFAULT ((0)),
	[FuelDRTExpensAvgWithoutWork] [float] NULL DEFAULT ((0)),
	[SquareWorkDescripOverlap] [varchar](max) NULL,
	[FactSquareCalcOverlap] [float] NULL DEFAULT ((0)),
	[DateLastRecalc] [datetime] NULL,
	[UserLastRecalc] [varchar](127) NULL,
	[UserCreated] [varchar](127) NULL,
	[PointsValidity] [int] NULL,
	[PointsCalc] [int] NULL DEFAULT ((0)),
	[PointsFact] [int] NULL DEFAULT ((0)),
	[PointsIntervalMax] [varchar](20) NULL,
	[StateOrder] [int] NOT NULL DEFAULT ((0)),
	[BlockUserId] [int] NOT NULL DEFAULT ((0)),
	[BlockDate] [datetime] NULL,
 CONSTRAINT [PK_agro_order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_order]') AND name = N'Id_mobitel_Order_FK1')
CREATE NONCLUSTERED INDEX [Id_mobitel_Order_FK1] ON [dbo].[agro_order] 
(
	[Id_mobitel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'Date'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������ �� ������(new type - DATE->DATETIME)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'Date'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'Id_mobitel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'Id_mobitel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'LocationStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'LocationStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'LocationEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'LocationEnd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'TimeStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'TimeStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'TimeEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'TimeEnd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'Regime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����� -������,��������������, ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'Regime'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'TimeWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������������� �����, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'TimeWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'TimeMove'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����� ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'TimeMove'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'SpeedAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ��������, ��/�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'SpeedAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'Comment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'PathWithoutWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'PathWithoutWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'SquareWorkDescript'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'SquareWorkDescript'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDUTExpensSquare'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ � �����,  �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDUTExpensSquare'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDRTExpensSquare'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ � �����,  �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDRTExpensSquare'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDUTExpensAvgSquare'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ � �����, �/��:' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDUTExpensAvgSquare'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDRTExpensAvgSquare'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ � �����, �/��:' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDRTExpensAvgSquare'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDUTExpensWithoutWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ���������, �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDUTExpensWithoutWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDRTExpensWithoutWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ���������, �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDRTExpensWithoutWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDUTExpensAvgWithoutWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDUTExpensAvgWithoutWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FuelDRTExpensAvgWithoutWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FuelDRTExpensAvgWithoutWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'SquareWorkDescripOverlap'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ������� � ������ ���������� ���� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'SquareWorkDescripOverlap'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'FactSquareCalcOverlap'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������������ ������� ���������� �������� � ������ ���������� ���� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'FactSquareCalcOverlap'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'DateLastRecalc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ���������� ��������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'DateLastRecalc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'UserLastRecalc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������, ��������� ������������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'UserLastRecalc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'UserCreated'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������, ��������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'UserCreated'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'PointsValidity'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'PointsValidity'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'PointsCalc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����� (�� Log_ID) ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'PointsCalc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'PointsFact'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����� (�� Log_ID) �����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'PointsFact'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'PointsIntervalMax'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������� ����� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'PointsIntervalMax'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'StateOrder'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'StateOrder'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'BlockUserId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������������, ����������� � ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'BlockUserId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_order', N'COLUMN',N'BlockDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������ ������ � ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_order', @level2type=N'COLUMN',@level2name=N'BlockDate'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_order]'))
ALTER TABLE [dbo].[agro_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_mobitel_Order_FK1] FOREIGN KEY([Id_mobitel])
REFERENCES [dbo].[mobitels] ([Mobitel_ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_Order_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_order]'))
ALTER TABLE [dbo].[agro_order] CHECK CONSTRAINT [fgn_key_Id_mobitel_Order_FK1]
GO
