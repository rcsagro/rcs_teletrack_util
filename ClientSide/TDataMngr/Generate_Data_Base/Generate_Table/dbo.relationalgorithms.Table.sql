/****** Object:  Table [dbo].[relationalgorithms]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__relationa__Algor__7F4BDEC0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[relationalgorithms] DROP CONSTRAINT [DF__relationa__Algor__7F4BDEC0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__relationa__Senso__004002F9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[relationalgorithms] DROP CONSTRAINT [DF__relationa__Senso__004002F9]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[relationalgorithms]') AND type in (N'U'))
DROP TABLE [dbo].[relationalgorithms]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[relationalgorithms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[relationalgorithms](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AlgorithmID] [int] NOT NULL DEFAULT ((0)),
	[SensorID] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_relationalgorithms] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[relationalgorithms]') AND name = N'ID')
CREATE UNIQUE NONCLUSTERED INDEX [ID] ON [dbo].[relationalgorithms] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
