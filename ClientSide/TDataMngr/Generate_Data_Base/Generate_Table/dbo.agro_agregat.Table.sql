/****** Object:  Table [dbo].[agro_agregat]    Script Date: 04/11/2013 11:22:21 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agreg__Name__7E6CC920]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agreg__Name__7E6CC920]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Width__7F60ED59]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agre__Width__7F60ED59]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agrega__Def__00551192]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agrega__Def__00551192]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Id_ma__014935CB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agre__Id_ma__014935CB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__IsGro__023D5A04]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agre__IsGro__023D5A04]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Id_wo__03317E3D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agre__Id_wo__03317E3D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__HasSe__0425A276]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat] DROP CONSTRAINT [DF__agro_agre__HasSe__0425A276]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat]') AND type in (N'U'))
DROP TABLE [dbo].[agro_agregat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_agregat](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[Width] [float] NULL DEFAULT ((0)),
	[Identifier] [int] NULL,
	[Def] [smallint] NULL DEFAULT ((0)),
	[Comment] [varchar](max) NULL,
	[Id_main] [int] NOT NULL DEFAULT ((0)),
	[IsGroupe] [smallint] NOT NULL DEFAULT ((0)),
	[InvNumber] [char](50) NULL,
	[Id_work] [int] NOT NULL DEFAULT ((0)),
	[OutLinkId] [varchar](20) NULL,
	[HasSensor] [smallint] NULL DEFAULT ((0)),
 CONSTRAINT [PK_agro_agregat] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Width'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ��������� ������������ ��� �������� ������������ ������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Width'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Identifier'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'10 ������ ������������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Identifier'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Def'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �� ��������� ��� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Def'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Comment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������� �������� ������ � ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'IsGroupe'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ - ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'IsGroupe'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'InvNumber'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'InvNumber'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'Id_work'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� �� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'Id_work'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'OutLinkId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'OutLinkId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat', N'COLUMN',N'HasSensor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������ ������������� �������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat', @level2type=N'COLUMN',@level2name=N'HasSensor'
GO
