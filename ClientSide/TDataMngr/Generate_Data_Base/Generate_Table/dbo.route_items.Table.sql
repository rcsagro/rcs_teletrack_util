/****** Object:  Table [dbo].[route_items]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_items]') AND type in (N'U'))
DROP TABLE [dbo].[route_items]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_items]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[route_items](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Seq] [int] NULL,
	[Route_ID] [int] NULL,
	[Zone_ID] [int] NULL,
	[Zone_IO] [int] NULL,
	[GoNext_Min] [int] NULL,
	[GoNext_Max] [int] NULL,
	[Link_Item] [int] NULL,
	[Link_Info] [int] NULL,
 CONSTRAINT [PK_route_items] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
