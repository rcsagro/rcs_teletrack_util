/****** Object:  Table [dbo].[datagpslost_rftmp]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__LogID__7DCDAAA2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslost_rftmp] DROP CONSTRAINT [DF__datagpslo__LogID__7DCDAAA2]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_rftmp]') AND type in (N'U'))
DROP TABLE [dbo].[datagpslost_rftmp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslost_rftmp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpslost_rftmp](
	[LogID] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
END
GO
