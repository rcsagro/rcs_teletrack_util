/****** Object:  Table [dbo].[datagpslosted]    Script Date: 04/11/2013 11:22:24 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Mobit__7FB5F314]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslosted] DROP CONSTRAINT [DF__datagpslo__Mobit__7FB5F314]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__Begin__00AA174D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslosted] DROP CONSTRAINT [DF__datagpslo__Begin__00AA174D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpslo__End_L__019E3B86]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpslosted] DROP CONSTRAINT [DF__datagpslo__End_L__019E3B86]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslosted]') AND type in (N'U'))
DROP TABLE [dbo].[datagpslosted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpslosted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpslosted](
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[Begin_LogID] [int] NOT NULL DEFAULT ((0)),
	[End_LogID] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_datagpslosted] PRIMARY KEY CLUSTERED 
(
	[Mobitel_ID] ASC,
	[Begin_LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
