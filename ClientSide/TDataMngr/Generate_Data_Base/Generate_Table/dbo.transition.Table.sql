/****** Object:  Table [dbo].[transition]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__transitio__Title__338A9CD5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transition] DROP CONSTRAINT [DF__transitio__Title__338A9CD5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__transitio__IconN__347EC10E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transition] DROP CONSTRAINT [DF__transitio__IconN__347EC10E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__transitio__Sound__3572E547]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transition] DROP CONSTRAINT [DF__transitio__Sound__3572E547]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__transitio__ViewI__36670980]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transition] DROP CONSTRAINT [DF__transitio__ViewI__36670980]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[transition]') AND type in (N'U'))
DROP TABLE [dbo].[transition]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[transition]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[transition](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InitialStateId] [int] NULL,
	[FinalStateId] [int] NULL,
	[Title] [varchar](50) NOT NULL DEFAULT (''),
	[SensorId] [int] NULL,
	[IconName] [varchar](50) NOT NULL DEFAULT (''),
	[SoundName] [varchar](50) NOT NULL DEFAULT (''),
	[Enabled] [smallint] NOT NULL,
	[ViewInReport] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_transition] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[transition]') AND name = N'Sensors_FK')
CREATE NONCLUSTERED INDEX [Sensors_FK] ON [dbo].[transition] 
(
	[SensorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
