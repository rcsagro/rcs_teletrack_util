/****** Object:  Table [dbo].[online]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Mobitel___29971E47]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Mobitel___29971E47]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Message___2A8B4280]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Message___2A8B4280]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Latitude__2B7F66B9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Latitude__2B7F66B9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Longitud__2C738AF2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Longitud__2C738AF2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Altitude__2D67AF2B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Altitude__2D67AF2B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__UnixTime__2E5BD364]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__UnixTime__2E5BD364]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Speed__2F4FF79D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Speed__2F4FF79D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Directio__30441BD6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Directio__30441BD6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Valid__3138400F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Valid__3138400F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__InMobite__322C6448]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__InMobite__322C6448]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Events__33208881]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Events__33208881]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor1__3414ACBA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor1__3414ACBA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor2__3508D0F3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor2__3508D0F3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor3__35FCF52C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor3__35FCF52C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor4__36F11965]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor4__36F11965]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor5__37E53D9E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor5__37E53D9E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor6__38D961D7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor6__38D961D7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor7__39CD8610]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor7__39CD8610]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Sensor8__3AC1AA49]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Sensor8__3AC1AA49]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__LogID__3BB5CE82]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__LogID__3BB5CE82]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__isShow__3CA9F2BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__isShow__3CA9F2BB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__whatIs__3D9E16F4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__whatIs__3D9E16F4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Counter1__3E923B2D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Counter1__3E923B2D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Counter2__3F865F66]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Counter2__3F865F66]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Counter3__407A839F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Counter3__407A839F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__online__Counter4__416EA7D8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[online] DROP CONSTRAINT [DF__online__Counter4__416EA7D8]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[online]') AND type in (N'U'))
DROP TABLE [dbo].[online]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[online]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[online](
	[DataGps_ID] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NOT NULL DEFAULT ((0)),
	[Longitude] [int] NOT NULL DEFAULT ((0)),
	[Altitude] [int] NULL DEFAULT ((0)),
	[UnixTime] [int] NOT NULL DEFAULT ((0)),
	[Speed] [smallint] NOT NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NOT NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL DEFAULT ((0)),
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [int] NOT NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
 CONSTRAINT [PK_online] PRIMARY KEY CLUSTERED 
(
	[DataGps_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
