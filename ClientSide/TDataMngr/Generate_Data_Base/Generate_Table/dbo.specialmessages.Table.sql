/****** Object:  Table [dbo].[specialmessages]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__specialme__DataI__0A888742]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[specialmessages] DROP CONSTRAINT [DF__specialme__DataI__0A888742]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__specialme__isNew__0B7CAB7B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[specialmessages] DROP CONSTRAINT [DF__specialme__isNew__0B7CAB7B]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[specialmessages]') AND type in (N'U'))
DROP TABLE [dbo].[specialmessages]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[specialmessages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[specialmessages](
	[SpecMessage_ID] [int] IDENTITY(1,1) NOT NULL,
	[DataString] [char](50) NULL,
	[DataInteger] [int] NULL DEFAULT ((0)),
	[unixtime] [char](50) NULL,
	[isNew] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_specialmessages] PRIMARY KEY CLUSTERED 
(
	[SpecMessage_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
