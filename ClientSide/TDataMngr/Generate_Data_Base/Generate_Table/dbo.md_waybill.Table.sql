/****** Object:  Table [dbo].[md_waybill]    Script Date: 04/11/2013 11:22:25 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_driver_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_driver_id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_enterprise_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_md_enterprise_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_working_shift_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_md_working_shift_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_vehicle_id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_driver_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_driver_id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_enterprise_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_md_enterprise_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_working_shift_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_md_working_shift_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [fgn_key_FK_md_waybill_vehicle_id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_waybil__IdDri__1FEDB87C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [DF__md_waybil__IdDri__1FEDB87C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_waybil__IdVeh__20E1DCB5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [DF__md_waybil__IdVeh__20E1DCB5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_waybil__IdShi__21D600EE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [DF__md_waybil__IdShi__21D600EE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_waybil__IsClo__22CA2527]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_waybill] DROP CONSTRAINT [DF__md_waybil__IsClo__22CA2527]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND type in (N'U'))
DROP TABLE [dbo].[md_waybill]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_waybill](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[IdDriver] [int] NULL DEFAULT ((0)),
	[IdVehicle] [int] NOT NULL DEFAULT ((0)),
	[IdEnterprise] [int] NULL,
	[IdShift] [int] NULL DEFAULT ((0)),
	[IsClose] [smallint] NOT NULL DEFAULT ((0)),
	[IsBusinessTrip] [smallint] NOT NULL,
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_waybill] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND name = N'FK_md_waybill_driver_id')
CREATE NONCLUSTERED INDEX [FK_md_waybill_driver_id] ON [dbo].[md_waybill] 
(
	[IdDriver] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND name = N'FK_md_waybill_md_enterprise_Id')
CREATE NONCLUSTERED INDEX [FK_md_waybill_md_enterprise_Id] ON [dbo].[md_waybill] 
(
	[IdEnterprise] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND name = N'FK_md_waybill_md_working_shift_Id')
CREATE NONCLUSTERED INDEX [FK_md_waybill_md_working_shift_Id] ON [dbo].[md_waybill] 
(
	[IdShift] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_waybill]') AND name = N'FK_md_waybill_vehicle_id')
CREATE NONCLUSTERED INDEX [FK_md_waybill_vehicle_id] ON [dbo].[md_waybill] 
(
	[IdVehicle] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_waybill', N'COLUMN',N'IsClose'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ���� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_waybill', @level2type=N'COLUMN',@level2name=N'IsClose'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_waybill', N'COLUMN',N'IsBusinessTrip'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_waybill', @level2type=N'COLUMN',@level2name=N'IsBusinessTrip'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_driver_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_waybill_driver_id] FOREIGN KEY([IdDriver])
REFERENCES [dbo].[driver] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_driver_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] CHECK CONSTRAINT [fgn_key_FK_md_waybill_driver_id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_enterprise_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_waybill_md_enterprise_Id] FOREIGN KEY([IdEnterprise])
REFERENCES [dbo].[md_enterprise] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_enterprise_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] CHECK CONSTRAINT [fgn_key_FK_md_waybill_md_enterprise_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_working_shift_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_waybill_md_working_shift_Id] FOREIGN KEY([IdShift])
REFERENCES [dbo].[md_working_shift] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_md_working_shift_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] CHECK CONSTRAINT [fgn_key_FK_md_waybill_md_working_shift_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_waybill_vehicle_id] FOREIGN KEY([IdVehicle])
REFERENCES [dbo].[vehicle] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_waybill_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_waybill]'))
ALTER TABLE [dbo].[md_waybill] CHECK CONSTRAINT [fgn_key_FK_md_waybill_vehicle_id]
GO
