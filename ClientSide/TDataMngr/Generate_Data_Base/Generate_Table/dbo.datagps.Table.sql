/****** Object:  Table [dbo].[datagps]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Mobitel__59C55456]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Mobitel__59C55456]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Message__5AB9788F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Message__5AB9788F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Latitud__5BAD9CC8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Latitud__5BAD9CC8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Longitu__5CA1C101]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Longitu__5CA1C101]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__UnixTim__5D95E53A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__UnixTim__5D95E53A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Speed__5E8A0973]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Speed__5E8A0973]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Directi__5F7E2DAC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Directi__5F7E2DAC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Valid__607251E5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Valid__607251E5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Events__6166761E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Events__6166761E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor1__625A9A57]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor1__625A9A57]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor2__634EBE90]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor2__634EBE90]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor3__6442E2C9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor3__6442E2C9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor4__65370702]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor4__65370702]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor5__662B2B3B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor5__662B2B3B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor6__671F4F74]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor6__671F4F74]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor7__681373AD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor7__681373AD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Sensor8__690797E6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Sensor8__690797E6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__LogID__69FBBC1F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__LogID__69FBBC1F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__isShow__6AEFE058]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__isShow__6AEFE058]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__whatIs__6BE40491]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__whatIs__6BE40491]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Counter__6CD828CA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Counter__6CD828CA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Counter__6DCC4D03]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Counter__6DCC4D03]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Counter__6EC0713C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Counter__6EC0713C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__Counter__6FB49575]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__Counter__6FB49575]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagps__SrvPack__70A8B9AE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagps] DROP CONSTRAINT [DF__datagps__SrvPack__70A8B9AE]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND type in (N'U'))
DROP TABLE [dbo].[datagps]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagps](
	[DataGps_ID] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NULL DEFAULT ((0)),
	[Longitude] [int] NULL DEFAULT ((0)),
	[Altitude] [int] NULL,
	[UnixTime] [int] NULL DEFAULT ((0)),
	[Speed] [smallint] NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL,
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [int] NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
	[SrvPacketID] [bigint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_datagps] PRIMARY KEY CLUSTERED 
(
	[DataGps_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND name = N'IDX_MobitelIDSrvPacketID')
CREATE NONCLUSTERED INDEX [IDX_MobitelIDSrvPacketID] ON [dbo].[datagps] 
(
	[Mobitel_ID] ASC,
	[SrvPacketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[datagps] 
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND name = N'Index_3')
CREATE NONCLUSTERED INDEX [Index_3] ON [dbo].[datagps] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC,
	[Valid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND name = N'Index_4')
CREATE NONCLUSTERED INDEX [Index_4] ON [dbo].[datagps] 
(
	[Message_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps]') AND name = N'Index_5')
CREATE NONCLUSTERED INDEX [Index_5] ON [dbo].[datagps] 
(
	[Mobitel_ID] ASC,
	[Counter1] ASC,
	[Counter2] ASC,
	[Counter3] ASC,
	[Counter4] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps', N'COLUMN',N'SrvPacketID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID ���������� ������ � ������ �������� ������ ��� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps', @level2type=N'COLUMN',@level2name=N'SrvPacketID'
GO
