/****** Object:  Table [dbo].[rules]    Script Date: 04/11/2013 11:22:27 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__ID__3B60C8C7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__ID__3B60C8C7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule1__3C54ED00]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule1__3C54ED00]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule2__3D491139]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule2__3D491139]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule3__3E3D3572]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule3__3E3D3572]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule4__3F3159AB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule4__3F3159AB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule5__40257DE4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule5__40257DE4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule6__4119A21D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule6__4119A21D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule7__420DC656]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule7__420DC656]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule8__4301EA8F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule8__4301EA8F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule9__43F60EC8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule9__43F60EC8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule10__44EA3301]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule10__44EA3301]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule11__45DE573A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule11__45DE573A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule12__46D27B73]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule12__46D27B73]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule13__47C69FAC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule13__47C69FAC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule14__48BAC3E5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule14__48BAC3E5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule15__49AEE81E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule15__49AEE81E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rules__Rule16__4AA30C57]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rules] DROP CONSTRAINT [DF__rules__Rule16__4AA30C57]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rules]') AND type in (N'U'))
DROP TABLE [dbo].[rules]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rules]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rules](
	[Rule_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Password] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NOT NULL DEFAULT ((0)),
	[Rule1] [smallint] NOT NULL DEFAULT ((0)),
	[Rule2] [smallint] NOT NULL DEFAULT ((0)),
	[Rule3] [smallint] NOT NULL DEFAULT ((0)),
	[Rule4] [smallint] NOT NULL DEFAULT ((0)),
	[Rule5] [smallint] NOT NULL DEFAULT ((0)),
	[Rule6] [smallint] NOT NULL DEFAULT ((0)),
	[Rule7] [smallint] NOT NULL DEFAULT ((0)),
	[Rule8] [smallint] NOT NULL DEFAULT ((0)),
	[Rule9] [smallint] NOT NULL DEFAULT ((0)),
	[Rule10] [smallint] NOT NULL DEFAULT ((0)),
	[Rule11] [smallint] NOT NULL DEFAULT ((0)),
	[Rule12] [smallint] NOT NULL DEFAULT ((0)),
	[Rule13] [smallint] NOT NULL DEFAULT ((0)),
	[Rule14] [smallint] NOT NULL DEFAULT ((0)),
	[Rule15] [smallint] NOT NULL DEFAULT ((0)),
	[Rule16] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_rules] PRIMARY KEY CLUSTERED 
(
	[Rule_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
