/****** Object:  Table [dbo].[servicesend]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicesend__ID__65570293]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[servicesend] DROP CONSTRAINT [DF__servicesend__ID__65570293]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicese__Flags__664B26CC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[servicesend] DROP CONSTRAINT [DF__servicese__Flags__664B26CC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicese__PortM__673F4B05]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[servicesend] DROP CONSTRAINT [DF__servicese__PortM__673F4B05]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicese__Messa__68336F3E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[servicesend] DROP CONSTRAINT [DF__servicese__Messa__68336F3E]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[servicesend]') AND type in (N'U'))
DROP TABLE [dbo].[servicesend]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[servicesend]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[servicesend](
	[ServiceSend_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Descr] [varchar](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[telMobitel] [varchar](64) NULL,
	[smsMobitel] [varchar](64) NULL,
	[emailMobitel] [varchar](64) NULL,
	[IPMobitel] [varchar](64) NULL,
	[URLMobitel] [varchar](64) NULL,
	[PortMobitel] [int] NULL DEFAULT ((0)),
	[RfMobitel] [varchar](50) NULL,
	[Message_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_servicesend] PRIMARY KEY CLUSTERED 
(
	[ServiceSend_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[servicesend]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[servicesend] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
