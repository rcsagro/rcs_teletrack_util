/****** Object:  Table [dbo].[agro_fieldgroupe]    Script Date: 04/11/2013 11:22:21 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_field__Name__1273C1CD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fieldgroupe] DROP CONSTRAINT [DF__agro_field__Name__1273C1CD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fiel__Id_Zo__1367E606]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fieldgroupe] DROP CONSTRAINT [DF__agro_fiel__Id_Zo__1367E606]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldgroupe]') AND type in (N'U'))
DROP TABLE [dbo].[agro_fieldgroupe]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldgroupe]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_fieldgroupe](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[Comment] [varchar](max) NULL,
	[Id_Zonesgroup] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_agro_fieldgroupe] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldgroupe', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldgroupe', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldgroupe', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldgroupe', @level2type=N'COLUMN',@level2name=N'Comment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldgroupe', N'COLUMN',N'Id_Zonesgroup'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ����������� ��� �� ��������� ������� ������� ������ �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldgroupe', @level2type=N'COLUMN',@level2name=N'Id_Zonesgroup'
GO
