/****** Object:  Table [dbo].[agro_workgroup]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_workg__Name__5BE2A6F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_workgroup] DROP CONSTRAINT [DF__agro_workg__Name__5BE2A6F2]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_workgroup]') AND type in (N'U'))
DROP TABLE [dbo].[agro_workgroup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_workgroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_workgroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NOT NULL DEFAULT (''),
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_agro_workgroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_workgroup', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_workgroup', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_workgroup', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_workgroup', @level2type=N'COLUMN',@level2name=N'Comment'
GO
