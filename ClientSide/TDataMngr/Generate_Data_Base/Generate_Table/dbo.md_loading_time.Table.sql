/****** Object:  Table [dbo].[md_loading_time]    Script Date: 04/11/2013 11:22:25 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [fgn_key_FK_md_loading_time_md_cargo_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [fgn_key_FK_md_loading_time_md_object_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [fgn_key_FK_md_loading_time_md_cargo_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [fgn_key_FK_md_loading_time_md_object_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_loadin__IdObj__75035A77]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [DF__md_loadin__IdObj__75035A77]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_loadin__IdCar__75F77EB0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_loading_time] DROP CONSTRAINT [DF__md_loadin__IdCar__75F77EB0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_loading_time]') AND type in (N'U'))
DROP TABLE [dbo].[md_loading_time]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_loading_time]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_loading_time](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdObject] [int] NOT NULL DEFAULT ((0)),
	[IdCargo] [int] NOT NULL DEFAULT ((0)),
	[TimeLoading] [decimal](10, 2) NOT NULL,
	[TimeUnLoading] [decimal](10, 2) NOT NULL,
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_loading_time] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_loading_time]') AND name = N'FK_md_loading_time_md_cargo_Id')
CREATE NONCLUSTERED INDEX [FK_md_loading_time_md_cargo_Id] ON [dbo].[md_loading_time] 
(
	[IdCargo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_loading_time]') AND name = N'FK_md_loading_time_md_object_Id')
CREATE NONCLUSTERED INDEX [FK_md_loading_time_md_object_Id] ON [dbo].[md_loading_time] 
(
	[IdObject] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_loading_time', N'COLUMN',N'IdObject'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_loading_time', @level2type=N'COLUMN',@level2name=N'IdObject'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_loading_time', N'COLUMN',N'IdCargo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_loading_time', @level2type=N'COLUMN',@level2name=N'IdCargo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_loading_time', N'COLUMN',N'TimeLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_loading_time', @level2type=N'COLUMN',@level2name=N'TimeLoading'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_loading_time', N'COLUMN',N'TimeUnLoading'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_loading_time', @level2type=N'COLUMN',@level2name=N'TimeUnLoading'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_loading_time_md_cargo_Id] FOREIGN KEY([IdCargo])
REFERENCES [dbo].[md_cargo] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_cargo_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] CHECK CONSTRAINT [fgn_key_FK_md_loading_time_md_cargo_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_loading_time_md_object_Id] FOREIGN KEY([IdObject])
REFERENCES [dbo].[md_object] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_loading_time_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_loading_time]'))
ALTER TABLE [dbo].[md_loading_time] CHECK CONSTRAINT [fgn_key_FK_md_loading_time_md_object_Id]
GO
