/****** Object:  Table [dbo].[sources]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sources]') AND type in (N'U'))
DROP TABLE [dbo].[sources]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sources]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sources](
	[Source_ID] [int] IDENTITY(1,1) NOT NULL,
	[SourceText] [char](200) NULL,
	[SourceError] [char](200) NULL,
 CONSTRAINT [PK_sources] PRIMARY KEY CLUSTERED 
(
	[Source_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
