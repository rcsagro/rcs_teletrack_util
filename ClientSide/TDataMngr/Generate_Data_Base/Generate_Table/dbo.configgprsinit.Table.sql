/****** Object:  Table [dbo].[configgprsinit]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgprsin__ID__17F790F9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsinit] DROP CONSTRAINT [DF__configgprsin__ID__17F790F9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Flags__18EBB532]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsinit] DROP CONSTRAINT [DF__configgpr__Flags__18EBB532]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Messa__19DFD96B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsinit] DROP CONSTRAINT [DF__configgpr__Messa__19DFD96B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Domai__1AD3FDA4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsinit] DROP CONSTRAINT [DF__configgpr__Domai__1AD3FDA4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__InitS__1BC821DD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsinit] DROP CONSTRAINT [DF__configgpr__InitS__1BC821DD]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsinit]') AND type in (N'U'))
DROP TABLE [dbo].[configgprsinit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsinit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configgprsinit](
	[ConfigGprsInit_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Descr] [varchar](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Domain] [varchar](50) NOT NULL DEFAULT ('0'),
	[InitString] [varchar](200) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_configgprsinit] PRIMARY KEY CLUSTERED 
(
	[ConfigGprsInit_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configgprsinit]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configgprsinit] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
