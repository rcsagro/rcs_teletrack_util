/****** Object:  Table [dbo].[agro_unit]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_unit__Name__5165187F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_unit] DROP CONSTRAINT [DF__agro_unit__Name__5165187F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_unit__NameS__52593CB8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_unit] DROP CONSTRAINT [DF__agro_unit__NameS__52593CB8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_unit__Facto__534D60F1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_unit] DROP CONSTRAINT [DF__agro_unit__Facto__534D60F1]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_unit]') AND type in (N'U'))
DROP TABLE [dbo].[agro_unit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_unit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_unit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](100) NOT NULL DEFAULT (''),
	[NameShort] [char](50) NOT NULL DEFAULT (''),
	[Factor] [float] NULL DEFAULT ((0)),
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_agro_unit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_unit', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_unit', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_unit', N'COLUMN',N'NameShort'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_unit', @level2type=N'COLUMN',@level2name=N'NameShort'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_unit', N'COLUMN',N'Factor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ��������� � �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_unit', @level2type=N'COLUMN',@level2name=N'Factor'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_unit', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_unit', @level2type=N'COLUMN',@level2name=N'Comment'
GO
