/****** Object:  Table [dbo].[agro_pricet]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_agregat_Price_FK5]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_agregat_Price_FK5]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_main_FK1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_FK3]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_mobitel_FK3]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_unit_FK4]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_unit_FK4]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_work_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_work_FK2]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_agregat_Price_FK5]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_agregat_Price_FK5]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_main_FK1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_FK3]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_mobitel_FK3]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_unit_FK4]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_unit_FK4]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_work_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [fgn_key_Id_work_FK2]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_pric__Price__4E88ABD4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [DF__agro_pric__Price__4E88ABD4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_pric__Id_ag__4F7CD00D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_pricet] DROP CONSTRAINT [DF__agro_pric__Id_ag__4F7CD00D]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND type in (N'U'))
DROP TABLE [dbo].[agro_pricet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_pricet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Id_work] [int] NOT NULL,
	[Price] [float] NULL DEFAULT ((0.00)),
	[Id_unit] [int] NOT NULL,
	[Id_mobitel] [int] NOT NULL,
	[Id_agregat] [int] NULL DEFAULT ((0)),
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_agro_pricet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND name = N'Id_agregat_Price_FK5')
CREATE NONCLUSTERED INDEX [Id_agregat_Price_FK5] ON [dbo].[agro_pricet] 
(
	[Id_agregat] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND name = N'Id_main_FK1')
CREATE NONCLUSTERED INDEX [Id_main_FK1] ON [dbo].[agro_pricet] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND name = N'Id_mobitel_FK3')
CREATE NONCLUSTERED INDEX [Id_mobitel_FK3] ON [dbo].[agro_pricet] 
(
	[Id_mobitel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND name = N'Id_unit_FK4')
CREATE NONCLUSTERED INDEX [Id_unit_FK4] ON [dbo].[agro_pricet] 
(
	[Id_unit] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_pricet]') AND name = N'Id_work_FK2')
CREATE NONCLUSTERED INDEX [Id_work_FK2] ON [dbo].[agro_pricet] 
(
	[Id_work] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Id_work'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Id_work'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Price'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Price'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Id_unit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Id_unit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Id_mobitel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Id_mobitel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Id_agregat'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Id_agregat'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_pricet', N'COLUMN',N'Comment'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_pricet', @level2type=N'COLUMN',@level2name=N'Comment'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_agregat_Price_FK5]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_agregat_Price_FK5] FOREIGN KEY([Id_agregat])
REFERENCES [dbo].[agro_agregat] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_agregat_Price_FK5]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] CHECK CONSTRAINT [fgn_key_Id_agregat_Price_FK5]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_main_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_price] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] CHECK CONSTRAINT [fgn_key_Id_main_FK1]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_FK3]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_mobitel_FK3] FOREIGN KEY([Id_mobitel])
REFERENCES [dbo].[mobitels] ([Mobitel_ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_mobitel_FK3]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] CHECK CONSTRAINT [fgn_key_Id_mobitel_FK3]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_unit_FK4]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_unit_FK4] FOREIGN KEY([Id_unit])
REFERENCES [dbo].[agro_unit] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_unit_FK4]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] CHECK CONSTRAINT [fgn_key_Id_unit_FK4]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_work_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_work_FK2] FOREIGN KEY([Id_work])
REFERENCES [dbo].[agro_work] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_work_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_pricet]'))
ALTER TABLE [dbo].[agro_pricet] CHECK CONSTRAINT [fgn_key_Id_work_FK2]
GO
