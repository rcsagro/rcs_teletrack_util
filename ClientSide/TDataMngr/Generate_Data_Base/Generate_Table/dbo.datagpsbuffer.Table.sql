/****** Object:  Table [dbo].[datagpsbuffer]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__72910220]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Mobit__72910220]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Messa__73852659]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Messa__73852659]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Latit__74794A92]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Latit__74794A92]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Longi__756D6ECB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Longi__756D6ECB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__UnixT__76619304]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__UnixT__76619304]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Speed__7755B73D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Speed__7755B73D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Direc__7849DB76]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Direc__7849DB76]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Valid__793DFFAF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Valid__793DFFAF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Event__7A3223E8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Event__7A3223E8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7B264821]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7B264821]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7C1A6C5A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7C1A6C5A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7D0E9093]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7D0E9093]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7E02B4CC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7E02B4CC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7EF6D905]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7EF6D905]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__7FEAFD3E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__7FEAFD3E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__00DF2177]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__00DF2177]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__01D345B0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Senso__01D345B0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__02C769E9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__LogID__02C769E9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__isSho__03BB8E22]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__isSho__03BB8E22]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__whatI__04AFB25B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__whatI__04AFB25B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__05A3D694]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Count__05A3D694]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__0697FACD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Count__0697FACD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__078C1F06]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Count__078C1F06]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__0880433F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer] DROP CONSTRAINT [DF__datagpsbu__Count__0880433F]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbuffer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer](
	[DataGps_ID] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NULL DEFAULT ((0)),
	[Longitude] [int] NULL DEFAULT ((0)),
	[Altitude] [int] NULL,
	[UnixTime] [int] NULL DEFAULT ((0)),
	[Speed] [smallint] NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL,
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [smallint] NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
 CONSTRAINT [PK_datagpsbuffer] PRIMARY KEY CLUSTERED 
(
	[DataGps_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer]') AND name = N'IDX_MobitelIDLogID')
CREATE NONCLUSTERED INDEX [IDX_MobitelIDLogID] ON [dbo].[datagpsbuffer] 
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
