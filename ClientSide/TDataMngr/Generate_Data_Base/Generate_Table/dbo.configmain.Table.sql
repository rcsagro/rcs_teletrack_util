/****** Object:  Table [dbo].[configmain]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmain__ID__29221CFB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmain__ID__29221CFB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Flags__2A164134]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Flags__2A164134]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2B0A656D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2B0A656D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2BFE89A6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2BFE89A6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2CF2ADDF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2CF2ADDF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2DE6D218]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2DE6D218]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2EDAF651]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2EDAF651]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__2FCF1A8A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__2FCF1A8A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__30C33EC3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__30C33EC3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__31B762FC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__31B762FC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Mobit__32AB8735]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Mobit__32AB8735]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__339FAB6E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__339FAB6E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__3493CFA7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__3493CFA7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configmai__Confi__3587F3E0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configmain] DROP CONSTRAINT [DF__configmai__Confi__3587F3E0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configmain]') AND type in (N'U'))
DROP TABLE [dbo].[configmain]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configmain]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configmain](
	[ConfigMain_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[ConfigOther_ID] [int] NULL DEFAULT ((0)),
	[ConfigDriverMessage_ID] [int] NULL DEFAULT ((0)),
	[ConfigEvent_ID] [int] NULL DEFAULT ((0)),
	[ConfigUnique_ID] [int] NULL DEFAULT ((0)),
	[ConfigZoneSet_ID] [int] NULL DEFAULT ((0)),
	[ConfigSensorSet_ID] [int] NULL DEFAULT ((0)),
	[ConfigTel_ID] [int] NULL DEFAULT ((0)),
	[ConfigSms_ID] [int] NULL DEFAULT ((0)),
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[ConfigGprsMain_ID] [int] NOT NULL DEFAULT ((0)),
	[ConfigGprsEmail_ID] [int] NOT NULL DEFAULT ((0)),
	[ConfigGprsInit_ID] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_configmain] PRIMARY KEY CLUSTERED 
(
	[ConfigMain_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configmain]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configmain] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
