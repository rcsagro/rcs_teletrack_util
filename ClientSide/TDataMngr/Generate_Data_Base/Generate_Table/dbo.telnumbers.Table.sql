/****** Object:  Table [dbo].[telnumbers]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__telnumbers__ID__131DCD43]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[telnumbers] DROP CONSTRAINT [DF__telnumbers__ID__131DCD43]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__telnumber__Flags__1411F17C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[telnumbers] DROP CONSTRAINT [DF__telnumber__Flags__1411F17C]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[telnumbers]') AND type in (N'U'))
DROP TABLE [dbo].[telnumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[telnumbers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[telnumbers](
	[TelNumber_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[TelNumber] [char](50) NULL,
 CONSTRAINT [PK_telnumbers] PRIMARY KEY CLUSTERED 
(
	[TelNumber_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
