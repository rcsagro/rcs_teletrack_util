/****** Object:  Table [dbo].[sensors]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensors__Name__55209ACA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensors] DROP CONSTRAINT [DF__sensors__Name__55209ACA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensors__Descrip__5614BF03]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensors] DROP CONSTRAINT [DF__sensors__Descrip__5614BF03]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensors__NameUni__5708E33C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensors] DROP CONSTRAINT [DF__sensors__NameUni__5708E33C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensors__K__57FD0775]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensors] DROP CONSTRAINT [DF__sensors__K__57FD0775]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensors]') AND type in (N'U'))
DROP TABLE [dbo].[sensors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensors](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mobitel_id] [int] NOT NULL,
	[Name] [varchar](45) NOT NULL DEFAULT (''),
	[Description] [varchar](200) NOT NULL DEFAULT (''),
	[StartBit] [bigint] NOT NULL,
	[Length] [bigint] NOT NULL,
	[NameUnit] [varchar](45) NOT NULL DEFAULT (''),
	[K] [float] NOT NULL DEFAULT ((0.00000)),
 CONSTRAINT [PK_sensors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sensors]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[sensors] 
(
	[mobitel_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
