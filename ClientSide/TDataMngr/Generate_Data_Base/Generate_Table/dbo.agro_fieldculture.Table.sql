/****** Object:  Table [dbo].[agro_fieldculture]    Script Date: 04/11/2013 11:22:21 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_culture_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] DROP CONSTRAINT [fgn_key_Id_culture_FK2]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FieldCulture_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] DROP CONSTRAINT [fgn_key_Id_main_FieldCulture_FK1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_culture_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] DROP CONSTRAINT [fgn_key_Id_culture_FK2]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FieldCulture_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] DROP CONSTRAINT [fgn_key_Id_main_FieldCulture_FK1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]') AND type in (N'U'))
DROP TABLE [dbo].[agro_fieldculture]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_fieldculture](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NULL,
	[Id_culture] [int] NOT NULL,
	[Year] [smallint] NOT NULL,
 CONSTRAINT [PK_agro_fieldculture] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]') AND name = N'Id_culture_FK2')
CREATE NONCLUSTERED INDEX [Id_culture_FK2] ON [dbo].[agro_fieldculture] 
(
	[Id_culture] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]') AND name = N'Id_main_FieldCulture_FK1')
CREATE NONCLUSTERED INDEX [Id_main_FieldCulture_FK1] ON [dbo].[agro_fieldculture] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldculture', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldculture', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldculture', N'COLUMN',N'Id_culture'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldculture', @level2type=N'COLUMN',@level2name=N'Id_culture'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fieldculture', N'COLUMN',N'Year'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fieldculture', @level2type=N'COLUMN',@level2name=N'Year'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_culture_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_culture_FK2] FOREIGN KEY([Id_culture])
REFERENCES [dbo].[agro_culture] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_culture_FK2]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] CHECK CONSTRAINT [fgn_key_Id_culture_FK2]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FieldCulture_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture]  WITH CHECK ADD  CONSTRAINT [fgn_key_Id_main_FieldCulture_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_field] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_Id_main_FieldCulture_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_fieldculture]'))
ALTER TABLE [dbo].[agro_fieldculture] CHECK CONSTRAINT [fgn_key_Id_main_FieldCulture_FK1]
GO
