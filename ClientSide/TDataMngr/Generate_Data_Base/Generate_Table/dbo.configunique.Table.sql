/****** Object:  Table [dbo].[configunique]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configunique__ID__51300E55]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configunique] DROP CONSTRAINT [DF__configunique__ID__51300E55]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configuni__Flags__5224328E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configunique] DROP CONSTRAINT [DF__configuni__Flags__5224328E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configuni__Messa__531856C7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configunique] DROP CONSTRAINT [DF__configuni__Messa__531856C7]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configunique]') AND type in (N'U'))
DROP TABLE [dbo].[configunique]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configunique]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configunique](
	[ConfigUnique_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[pswd] [char](8) NULL,
	[tmpPswd] [char](8) NULL,
	[dsptId] [char](4) NULL,
 CONSTRAINT [PK_configunique] PRIMARY KEY CLUSTERED 
(
	[ConfigUnique_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configunique]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configunique] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
