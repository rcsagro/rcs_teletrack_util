/****** Object:  Table [dbo].[rt_route]    Script Date: 04/11/2013 11:22:27 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Id_mob__0ABD916C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Id_mob__0ABD916C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Id_dri__0BB1B5A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Id_dri__0BB1B5A5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Distan__0CA5D9DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Distan__0CA5D9DE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelSt__0D99FE17]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelSt__0D99FE17]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelAd__0E8E2250]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelAd__0E8E2250]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelSu__0F824689]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelSu__0F824689]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelEn__10766AC2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelEn__10766AC2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelEx__116A8EFB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelEx__116A8EFB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelEx__125EB334]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelEx__125EB334]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Fuel_E__1352D76D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Fuel_E__1352D76D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Fuel_E__1446FBA6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Fuel_E__1446FBA6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Fuel_E__153B1FDF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Fuel_E__153B1FDF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Fuel_E__162F4418]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Fuel_E__162F4418]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__SpeedA__17236851]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__SpeedA__17236851]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelAd__18178C8A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelAd__18178C8A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__FuelSu__190BB0C3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__FuelSu__190BB0C3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Points__19FFD4FC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Points__19FFD4FC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Points__1AF3F935]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Points__1AF3F935]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Points__1BE81D6E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Points__1BE81D6E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__Type__1CDC41A7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__Type__1CDC41A7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route__DistLo__1DD065E0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route] DROP CONSTRAINT [DF__rt_route__DistLo__1DD065E0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route]') AND type in (N'U'))
DROP TABLE [dbo].[rt_route]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_route](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_sample] [int] NOT NULL,
	[Id_mobitel] [int] NOT NULL DEFAULT ((0)),
	[Id_driver] [int] NOT NULL DEFAULT ((0)),
	[TimeStartPlan] [datetime] NULL,
	[Distance] [float] NOT NULL DEFAULT ((0)),
	[TimePlanTotal] [varchar](20) NULL,
	[TimeFactTotal] [varchar](20) NULL,
	[Deviation] [varchar](20) NULL,
	[DeviationAr] [varchar](20) NULL,
	[Remark] [varchar](max) NULL,
	[FuelStart] [float] NOT NULL DEFAULT ((0.00)),
	[FuelAdd] [float] NOT NULL DEFAULT ((0.00)),
	[FuelSub] [float] NOT NULL DEFAULT ((0.00)),
	[FuelEnd] [float] NOT NULL DEFAULT ((0.00)),
	[FuelExpens] [float] NOT NULL DEFAULT ((0.00)),
	[FuelExpensAvg] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensMove] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensStop] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensTotal] [float] NOT NULL DEFAULT ((0.00)),
	[Fuel_ExpensAvg] [float] NOT NULL DEFAULT ((0.00)),
	[TimeMove] [varchar](20) NULL,
	[TimeStop] [varchar](20) NULL,
	[SpeedAvg] [float] NULL DEFAULT ((0.00)),
	[FuelAddQty] [int] NULL DEFAULT ((0)),
	[FuelSubQty] [int] NULL DEFAULT ((0)),
	[PointsValidity] [int] NULL DEFAULT ((0)),
	[PointsCalc] [int] NULL DEFAULT ((0)),
	[PointsFact] [int] NULL DEFAULT ((0)),
	[PointsIntervalMax] [varchar](20) NULL,
	[Type] [smallint] NOT NULL DEFAULT ((1)),
	[TimeEndFact] [datetime] NULL,
	[DistLogicSensor] [float] NULL DEFAULT ((0.00)),
 CONSTRAINT [PK_rt_route] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Id_sample'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������ �������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Id_sample'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Id_mobitel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Id_mobitel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Id_driver'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Id_driver'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimeStartPlan'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeStartPlan'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimePlanTotal'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����� �� �������� (����)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimePlanTotal'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimeFactTotal'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����� �� �������� (����)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeFactTotal'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Deviation'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Deviation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'DeviationAr'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �� ������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'DeviationAr'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Remark'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Remark'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� � ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelAdd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelAdd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelSub'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelSub'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� � �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelEnd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelExpens'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelExpens'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelExpensAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ������,  �/100 ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelExpensAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Fuel_ExpensMove'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������ ������� � ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensMove'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Fuel_ExpensStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������ ������� �� ��������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Fuel_ExpensTotal'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ����� ������, �' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensTotal'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Fuel_ExpensAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���/CAN ������� ������, �/100 ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Fuel_ExpensAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimeMove'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeMove'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimeStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'SpeedAvg'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'SpeedAvg'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelAddQty'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ���������� - ���-��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelAddQty'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'FuelSubQty'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����� - ���-��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'FuelSubQty'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'PointsValidity'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsValidity'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'PointsCalc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����� (�� Log_ID) ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsCalc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'PointsFact'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����� (�� Log_ID) �����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsFact'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'PointsIntervalMax'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������� ����� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'PointsIntervalMax'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'Type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ����������� ����� - �� ��� ���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'Type'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'TimeEndFact'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����� ��������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'TimeEndFact'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route', N'COLUMN',N'DistLogicSensor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ���������� ���������� ��������, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route', @level2type=N'COLUMN',@level2name=N'DistLogicSensor'
GO
