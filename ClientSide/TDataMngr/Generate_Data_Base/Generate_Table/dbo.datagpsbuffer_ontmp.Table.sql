/****** Object:  Table [dbo].[datagpsbuffer_ontmp]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__3CF40B7E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_ontmp] DROP CONSTRAINT [DF__datagpsbu__Mobit__3CF40B7E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__3DE82FB7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_ontmp] DROP CONSTRAINT [DF__datagpsbu__LogID__3DE82FB7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__SrvPa__3EDC53F0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_ontmp] DROP CONSTRAINT [DF__datagpsbu__SrvPa__3EDC53F0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_ontmp]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbuffer_ontmp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_ontmp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer_ontmp](
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[LogID] [int] NOT NULL DEFAULT ((0)),
	[SrvPacketID] [bigint] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_ontmp]') AND name = N'IDX_Mobitelid')
CREATE NONCLUSTERED INDEX [IDX_Mobitelid] ON [dbo].[datagpsbuffer_ontmp] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_ontmp]') AND name = N'IDX_MobitelidSrvpacketid')
CREATE NONCLUSTERED INDEX [IDX_MobitelidSrvpacketid] ON [dbo].[datagpsbuffer_ontmp] 
(
	[Mobitel_ID] ASC,
	[SrvPacketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
