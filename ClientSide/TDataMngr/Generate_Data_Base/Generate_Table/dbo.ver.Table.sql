/****** Object:  Table [dbo].[ver]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ver__Num__53F76C67]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ver] DROP CONSTRAINT [DF__ver__Num__53F76C67]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ver]') AND type in (N'U'))
DROP TABLE [dbo].[ver]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ver]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ver](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Num] [bigint] NOT NULL DEFAULT ((0)),
	[Desc] [varchar](max) NOT NULL,
	[time] [datetime] NULL,
 CONSTRAINT [PK_ver] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ver', N'COLUMN',N'Num'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������� ������ ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ver', @level2type=N'COLUMN',@level2name=N'Num'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ver', N'COLUMN',N'Desc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ������� ������ ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ver', @level2type=N'COLUMN',@level2name=N'Desc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ver', N'COLUMN',N'time'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ver', @level2type=N'COLUMN',@level2name=N'time'
GO
