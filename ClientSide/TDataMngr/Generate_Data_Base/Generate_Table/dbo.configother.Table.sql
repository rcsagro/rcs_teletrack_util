/****** Object:  Table [dbo].[configother]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configother__ID__37703C52]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configother] DROP CONSTRAINT [DF__configother__ID__37703C52]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configoth__Flags__3864608B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configother] DROP CONSTRAINT [DF__configoth__Flags__3864608B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configother__Pin__395884C4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configother] DROP CONSTRAINT [DF__configother__Pin__395884C4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configothe__Lang__3A4CA8FD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configother] DROP CONSTRAINT [DF__configothe__Lang__3A4CA8FD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configoth__Messa__3B40CD36]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configother] DROP CONSTRAINT [DF__configoth__Messa__3B40CD36]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configother]') AND type in (N'U'))
DROP TABLE [dbo].[configother]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configother]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configother](
	[ConfigOther_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Pin] [int] NULL DEFAULT ((0)),
	[Lang] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_configother] PRIMARY KEY CLUSTERED 
(
	[ConfigOther_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configother]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configother] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
