/****** Object:  Table [dbo].[md_working_shift]    Script Date: 04/11/2013 11:22:25 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_working__Name__24B26D99]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_working_shift] DROP CONSTRAINT [DF__md_working__Name__24B26D99]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_working_shift]') AND type in (N'U'))
DROP TABLE [dbo].[md_working_shift]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_working_shift]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_working_shift](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[Start] [time](7) NOT NULL,
	[End] [time](7) NOT NULL,
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_working_shift] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_working_shift', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_working_shift', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_working_shift', N'COLUMN',N'Start'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_working_shift', @level2type=N'COLUMN',@level2name=N'Start'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_working_shift', N'COLUMN',N'End'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_working_shift', @level2type=N'COLUMN',@level2name=N'End'
GO
