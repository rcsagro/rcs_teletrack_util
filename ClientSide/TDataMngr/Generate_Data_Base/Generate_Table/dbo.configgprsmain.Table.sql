/****** Object:  Table [dbo].[configgprsmain]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgprsma__ID__1DB06A4F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgprsma__ID__1DB06A4F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Flags__1EA48E88]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Flags__1EA48E88]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Messa__1F98B2C1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Messa__1F98B2C1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgprs__Mode__208CD6FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgprs__Mode__208CD6FA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Apnse__2180FB33]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Apnse__2180FB33]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Apnun__22751F6C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Apnun__22751F6C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Apnpw__236943A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Apnpw__236943A5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Dnsse__245D67DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Dnsse__245D67DE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Dialn__25518C17]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Dialn__25518C17]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Ispun__2645B050]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Ispun__2645B050]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configgpr__Isppw__2739D489]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configgprsmain] DROP CONSTRAINT [DF__configgpr__Isppw__2739D489]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsmain]') AND type in (N'U'))
DROP TABLE [dbo].[configgprsmain]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configgprsmain]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configgprsmain](
	[ConfigGprsMain_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Descr] [varchar](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Mode] [int] NOT NULL DEFAULT ((0)),
	[Apnserv] [varchar](50) NOT NULL DEFAULT (''),
	[Apnun] [varchar](50) NOT NULL DEFAULT (''),
	[Apnpw] [varchar](50) NOT NULL DEFAULT (''),
	[Dnsserv1] [varchar](50) NOT NULL DEFAULT (''),
	[Dialn1] [varchar](50) NOT NULL DEFAULT (''),
	[Ispun] [varchar](50) NOT NULL DEFAULT (''),
	[Isppw] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_configgprsmain] PRIMARY KEY CLUSTERED 
(
	[ConfigGprsMain_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
