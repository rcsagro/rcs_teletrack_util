/****** Object:  Table [dbo].[setting]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__TimeBre__6C040022]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__TimeBre__6C040022]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Rotatio__6CF8245B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Rotatio__6CF8245B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Rotatio__6DEC4894]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Rotatio__6DEC4894]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Fueling__6EE06CCD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Fueling__6EE06CCD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__band__6FD49106]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__band__6FD49106]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Fueling__70C8B53F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Fueling__70C8B53F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__accelMa__71BCD978]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__accelMa__71BCD978]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__breakMa__72B0FDB1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__breakMa__72B0FDB1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__speedMa__73A521EA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__speedMa__73A521EA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__idleRun__74994623]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__idleRun__74994623]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__idleRun__758D6A5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__idleRun__758D6A5C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Name__76818E95]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Name__76818E95]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Desc__7775B2CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Desc__7775B2CE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__AvgFuel__7869D707]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__AvgFuel__7869D707]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__FuelApp__795DFB40]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__FuelApp__795DFB40]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__TimeGet__7A521F79]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__TimeGet__7A521F79]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__TimeGet__7B4643B2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__TimeGet__7B4643B2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__CzMinCr__7C3A67EB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__CzMinCr__7C3A67EB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__FuelerR__7D2E8C24]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__FuelerR__7D2E8C24]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__FuelerM__7E22B05D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__FuelerM__7E22B05D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__FuelerM__7F16D496]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__FuelerM__7F16D496]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__FuelerC__000AF8CF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__FuelerC__000AF8CF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Fuelrat__00FF1D08]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Fuelrat__00FF1D08]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Fueling__01F34141]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Fueling__01F34141]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__TimeBre__02E7657A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__TimeBre__02E7657A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Inclino__03DB89B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Inclino__03DB89B3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__setting__Inclino__04CFADEC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[setting] DROP CONSTRAINT [DF__setting__Inclino__04CFADEC]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[setting]') AND type in (N'U'))
DROP TABLE [dbo].[setting]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[setting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[setting](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TimeBreak] [time](7) NULL DEFAULT ('00:01:00'),
	[RotationMain] [int] NULL DEFAULT ((100)),
	[RotationAdd] [int] NULL DEFAULT ((100)),
	[FuelingEdge] [int] NULL DEFAULT ((0)),
	[band] [int] NULL DEFAULT ((25)),
	[FuelingDischarge] [int] NULL DEFAULT ((0)),
	[accelMax] [float] NULL DEFAULT ((1)),
	[breakMax] [float] NULL DEFAULT ((1)),
	[speedMax] [float] NULL DEFAULT ((70)),
	[idleRunningMain] [int] NULL DEFAULT ((0)),
	[idleRunningAdd] [int] NULL DEFAULT ((0)),
	[Name] [char](20) NULL DEFAULT ('���������'),
	[Desc] [char](80) NULL DEFAULT ('�������� ���������'),
	[AvgFuelRatePerHour] [float] NOT NULL DEFAULT ((15.00000)),
	[FuelApproximationTime] [time](7) NOT NULL DEFAULT ('00:02:00'),
	[TimeGetFuelAfterStop] [time](7) NOT NULL DEFAULT ('00:00:00'),
	[TimeGetFuelBeforeMotion] [time](7) NOT NULL DEFAULT ('00:00:00'),
	[CzMinCrossingPairTime] [time](7) NOT NULL DEFAULT ('00:01:00'),
	[FuelerRadiusFinds] [int] NOT NULL DEFAULT ((10)),
	[FuelerMaxTimeStop] [time](7) NOT NULL DEFAULT ('00:02:00'),
	[FuelerMinFuelrate] [int] NOT NULL DEFAULT ((1)),
	[FuelerCountInMotion] [int] NOT NULL DEFAULT ((0)),
	[FuelrateWithDischarge] [int] NOT NULL DEFAULT ((0)),
	[FuelingMinMaxAlgorithm] [int] NOT NULL DEFAULT ((0)),
	[TimeBreakMaxPermitted] [time](7) NOT NULL DEFAULT ('03:00:00'),
	[InclinometerMaxAngleAxisX] [float] NOT NULL DEFAULT ((5)),
	[InclinometerMaxAngleAxisY] [float] NOT NULL DEFAULT ((5)),
 CONSTRAINT [PK_setting] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[setting]') AND name = N'id')
CREATE UNIQUE NONCLUSTERED INDEX [id] ON [dbo].[setting] 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'TimeBreak'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'TimeBreak'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'RotationMain'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ��������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'RotationMain'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'RotationAdd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ���. ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'RotationAdd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelingEdge'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelingEdge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'band'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'band'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelingDischarge'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ���� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelingDischarge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'accelMax'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'accelMax'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'breakMax'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'breakMax'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'speedMax'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'speedMax'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'idleRunningMain'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ��� ��������� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'idleRunningMain'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'idleRunningAdd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ��� ���. ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'idleRunningAdd'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'Desc'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'Desc'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'AvgFuelRatePerHour'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ������ ������� � ���' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'AvgFuelRatePerHour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelApproximationTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �������� ����� ��������, �� ����� �������� ���������� ������� ������� � ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelApproximationTime'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'TimeGetFuelAfterStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �������� ����� ������� ������� � ������� ������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'TimeGetFuelAfterStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'TimeGetFuelBeforeMotion'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �������� ����� ������� ������ ������� � ������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'TimeGetFuelBeforeMotion'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'CzMinCrossingPairTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Min ��������� �������� ����� ����� ������������� ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'CzMinCrossingPairTime'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelerRadiusFinds'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������ ������������ �� ������ �����������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelerRadiusFinds'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelerMaxTimeStop'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����� �� �������� ���������� ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelerMaxTimeStop'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelerMinFuelrate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� �������� ��������, ��� ����� �������� � �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelerMinFuelrate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelerCountInMotion'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �� ��������, ��� �� ������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelerCountInMotion'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelrateWithDischarge'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �� � ������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelrateWithDischarge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'FuelingMinMaxAlgorithm'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �������� �� MAX-MIN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'FuelingMinMaxAlgorithm'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'TimeBreakMaxPermitted'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����. ����� ����������� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'TimeBreakMaxPermitted'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'InclinometerMaxAngleAxisX'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ���� ������� ������������ ���������� ��� � ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'InclinometerMaxAngleAxisX'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'setting', N'COLUMN',N'InclinometerMaxAngleAxisY'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ���� ������� ������������ ���������� ��� Y ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'setting', @level2type=N'COLUMN',@level2name=N'InclinometerMaxAngleAxisY'
GO
