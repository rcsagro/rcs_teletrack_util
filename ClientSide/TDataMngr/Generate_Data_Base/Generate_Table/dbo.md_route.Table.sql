/****** Object:  Table [dbo].[md_route]    Script Date: 04/11/2013 11:22:25 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_route_md_object_from]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_route]'))
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [fgn_key_FK_md_route_md_object_from]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_route_md_object_from]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_route]'))
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [fgn_key_FK_md_route_md_object_from]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_route__Id_obj__1A34DF26]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [DF__md_route__Id_obj__1A34DF26]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_route__Id_obj__1B29035F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [DF__md_route__Id_obj__1B29035F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_route__Distan__1C1D2798]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [DF__md_route__Distan__1C1D2798]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_route__Time__1D114BD1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_route] DROP CONSTRAINT [DF__md_route__Time__1D114BD1]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_route]') AND type in (N'U'))
DROP TABLE [dbo].[md_route]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_route]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_route](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_object_from] [int] NOT NULL DEFAULT ((0)),
	[Id_object_to] [int] NOT NULL DEFAULT ((0)),
	[Distance] [decimal](10, 2) NOT NULL DEFAULT ((0)),
	[Time] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_route] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_route]') AND name = N'FK_md_route_md_object_from')
CREATE NONCLUSTERED INDEX [FK_md_route_md_object_from] ON [dbo].[md_route] 
(
	[Id_object_from] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_route]') AND name = N'FK_md_route_md_object_to')
CREATE NONCLUSTERED INDEX [FK_md_route_md_object_to] ON [dbo].[md_route] 
(
	[Id_object_to] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_route', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_route', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_route', N'COLUMN',N'Time'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_route', @level2type=N'COLUMN',@level2name=N'Time'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_route_md_object_from]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_route]'))
ALTER TABLE [dbo].[md_route]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_route_md_object_from] FOREIGN KEY([Id_object_from])
REFERENCES [dbo].[md_object] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_route_md_object_from]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_route]'))
ALTER TABLE [dbo].[md_route] CHECK CONSTRAINT [fgn_key_FK_md_route_md_object_from]
GO
