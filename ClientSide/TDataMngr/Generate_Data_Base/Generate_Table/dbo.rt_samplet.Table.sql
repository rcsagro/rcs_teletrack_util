/****** Object:  Table [dbo].[rt_samplet]    Script Date: 04/11/2013 11:22:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_samplet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_samplet]'))
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [fgn_key_rt_samplet_FK1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_samplet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_samplet]'))
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [fgn_key_rt_samplet_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__TimeR__369C13AA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [DF__rt_sample__TimeR__369C13AA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__TimeR__379037E3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [DF__rt_sample__TimeR__379037E3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__TimeR__38845C1C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [DF__rt_sample__TimeR__38845C1C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__TimeA__39788055]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_samplet] DROP CONSTRAINT [DF__rt_sample__TimeA__39788055]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_samplet]') AND type in (N'U'))
DROP TABLE [dbo].[rt_samplet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_samplet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_samplet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Position] [smallint] NOT NULL,
	[Id_event] [int] NOT NULL,
	[Id_zone] [int] NULL,
	[TimeRel] [char](5) NULL DEFAULT ('00:00'),
	[TimeRelP] [char](5) NULL DEFAULT ('00:00'),
	[TimeRelM] [char](5) NULL DEFAULT ('00:00'),
	[TimeAbs] [char](5) NULL DEFAULT ('00:00'),
 CONSTRAINT [PK_rt_samplet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[rt_samplet]') AND name = N'rt_samplet_FK1')
CREATE NONCLUSTERED INDEX [rt_samplet_FK1] ON [dbo].[rt_samplet] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'Position'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ����� ������ ������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'Position'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'Id_event'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� - ����� / ����� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'Id_event'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'Id_zone'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'Id_zone'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'TimeRel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������� ����� �� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'TimeRel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'TimeRelP'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ���������� (+)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'TimeRelP'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'TimeRelM'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� ���������� (-)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'TimeRelM'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_samplet', N'COLUMN',N'TimeAbs'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �����  ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_samplet', @level2type=N'COLUMN',@level2name=N'TimeAbs'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_samplet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_samplet]'))
ALTER TABLE [dbo].[rt_samplet]  WITH CHECK ADD  CONSTRAINT [fgn_key_rt_samplet_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[rt_sample] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_samplet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_samplet]'))
ALTER TABLE [dbo].[rt_samplet] CHECK CONSTRAINT [fgn_key_rt_samplet_FK1]
GO
