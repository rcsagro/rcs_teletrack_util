/****** Object:  Table [dbo].[route_list]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_list]') AND type in (N'U'))
DROP TABLE [dbo].[route_list]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[route_list]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[route_list](
	[Route_ID] [int] IDENTITY(1,1) NOT NULL,
	[GIS] [varbinary](max) NULL,
	[Name] [varchar](50) NULL,
	[Descr] [varchar](200) NULL,
	[Modified] [int] NULL,
 CONSTRAINT [PK_route_list] PRIMARY KEY CLUSTERED 
(
	[Route_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
