/****** Object:  Table [dbo].[ntf_main]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_main__IsActi__573DED66]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_main] DROP CONSTRAINT [DF__ntf_main__IsActi__573DED66]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_main__IsPopu__5832119F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_main] DROP CONSTRAINT [DF__ntf_main__IsPopu__5832119F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_main__Title__592635D8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_main] DROP CONSTRAINT [DF__ntf_main__Title__592635D8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_main__TimeFo__5A1A5A11]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_main] DROP CONSTRAINT [DF__ntf_main__TimeFo__5A1A5A11]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_main__IsEmai__5B0E7E4A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_main] DROP CONSTRAINT [DF__ntf_main__IsEmai__5B0E7E4A]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_main]') AND type in (N'U'))
DROP TABLE [dbo].[ntf_main]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_main]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ntf_main](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsActive] [smallint] NOT NULL DEFAULT ((0)),
	[IsPopup] [smallint] NOT NULL DEFAULT ((0)),
	[Title] [varchar](255) NOT NULL DEFAULT (''),
	[IconSmall] [varbinary](max) NULL,
	[IconLarge] [varbinary](max) NULL,
	[SoundName] [varchar](50) NULL,
	[TimeForControl] [int] NULL DEFAULT ((0)),
	[IsEmailActive] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_ntf_main] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_main', N'COLUMN',N'IsEmailActive'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� E-mail �������� �����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_main', @level2type=N'COLUMN',@level2name=N'IsEmailActive'
GO
