/****** Object:  Table [dbo].[mapstyleglobal]    Script Date: 04/11/2013 11:22:24 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleglob__ID__37FA4C37]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleglob__ID__37FA4C37]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Defau__38EE7070]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Defau__38EE7070]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Defau__39E294A9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Defau__39E294A9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3AD6B8E2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3AD6B8E2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3BCADD1B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3BCADD1B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3CBF0154]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3CBF0154]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3DB3258D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3DB3258D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3EA749C6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3EA749C6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__3F9B6DFF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__3F9B6DFF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__408F9238]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__408F9238]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4183B671]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4183B671]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4277DAAA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4277DAAA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__436BFEE3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__436BFEE3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4460231C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4460231C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__45544755]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__45544755]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__46486B8E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__46486B8E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__473C8FC7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__473C8FC7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4830B400]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4830B400]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4924D839]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4924D839]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4A18FC72]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4A18FC72]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4B0D20AB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4B0D20AB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4C0144E4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4C0144E4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4CF5691D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4CF5691D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4DE98D56]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4DE98D56]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4EDDB18F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4EDDB18F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__4FD1D5C8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__4FD1D5C8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__50C5FA01]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__50C5FA01]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__51BA1E3A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__51BA1E3A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__52AE4273]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__52AE4273]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__53A266AC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__53A266AC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__54968AE5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__54968AE5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__558AAF1E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__558AAF1E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__567ED357]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__567ED357]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__5772F790]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__5772F790]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Event__58671BC9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Event__58671BC9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__595B4002]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__595B4002]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5A4F643B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5A4F643B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5B438874]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5B438874]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5C37ACAD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5C37ACAD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5D2BD0E6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5D2BD0E6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5E1FF51F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5E1FF51F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__5F141958]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__5F141958]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleg__Senso__60083D91]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstyleglobal] DROP CONSTRAINT [DF__mapstyleg__Senso__60083D91]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstyleglobal]') AND type in (N'U'))
DROP TABLE [dbo].[mapstyleglobal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstyleglobal]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mapstyleglobal](
	[MapStyleGlobal_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[DefaultLineStyle_ID] [int] NULL DEFAULT ((0)),
	[DefaulPointStyle_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle1_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle2_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle3_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle4_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle5_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle6_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle7_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle8_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle9_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle10_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle11_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle12_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle13_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle14_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle15_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle16_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle17_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle18_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle19_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle20_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle21_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle22_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle23_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle24_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle25_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle26_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle27_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle28_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle29_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle30_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle31_ID] [int] NULL DEFAULT ((0)),
	[EventPointStyle32_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle1_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle2_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle3_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle4_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle5_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle6_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle7_ID] [int] NULL DEFAULT ((0)),
	[SensorPointStyle8_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_mapstyleglobal] PRIMARY KEY CLUSTERED 
(
	[MapStyleGlobal_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
