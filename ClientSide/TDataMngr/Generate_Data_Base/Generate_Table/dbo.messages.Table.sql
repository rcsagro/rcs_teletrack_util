/****** Object:  Table [dbo].[messages]    Script Date: 04/11/2013 11:22:25 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Time1__269AB60B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Time1__269AB60B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Time2__278EDA44]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Time2__278EDA44]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Time3__2882FE7D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Time3__2882FE7D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Time4__297722B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Time4__297722B6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Mobite__2A6B46EF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Mobite__2A6B46EF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Servic__2B5F6B28]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Servic__2B5F6B28]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Comman__2C538F61]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Comman__2C538F61]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Comman__2D47B39A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Comman__2D47B39A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Messag__2E3BD7D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Messag__2E3BD7D3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Source__2F2FFC0C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Source__2F2FFC0C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__Crc__30242045]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__Crc__30242045]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__SmsId__3118447E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__SmsId__3118447E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__AdvCou__320C68B7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__AdvCou__320C68B7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__messages__isNewF__33008CF0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[messages] DROP CONSTRAINT [DF__messages__isNewF__33008CF0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND type in (N'U'))
DROP TABLE [dbo].[messages]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[messages](
	[Message_ID] [int] IDENTITY(1,1) NOT NULL,
	[Time1] [int] NULL DEFAULT ((0)),
	[Time2] [int] NULL DEFAULT ((0)),
	[Time3] [int] NULL DEFAULT ((0)),
	[Time4] [int] NULL DEFAULT ((0)),
	[isSend] [smallint] NULL,
	[isDelivered] [smallint] NULL,
	[isNew] [smallint] NULL,
	[Direction] [smallint] NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[ServiceInit_ID] [int] NULL DEFAULT ((0)),
	[Command_ID] [smallint] NULL DEFAULT ((0)),
	[CommandMask_ID] [int] NULL DEFAULT ((0)),
	[MessageCounter] [int] NULL DEFAULT ((0)),
	[DataFromService] [char](32) NULL,
	[Address] [char](20) NULL,
	[Source_ID] [int] NULL DEFAULT ((0)),
	[Crc] [smallint] NULL DEFAULT ((1)),
	[SmsId] [int] NULL DEFAULT ((0)),
	[AdvCounter] [int] NOT NULL DEFAULT ((0)),
	[isNewFromMob] [smallint] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_messages] PRIMARY KEY CLUSTERED 
(
	[Message_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[messages] 
(
	[isNewFromMob] ASC,
	[Message_ID] ASC,
	[Time1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND name = N'Index_4')
CREATE NONCLUSTERED INDEX [Index_4] ON [dbo].[messages] 
(
	[Mobitel_ID] ASC,
	[Command_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND name = N'Index_5')
CREATE NONCLUSTERED INDEX [Index_5] ON [dbo].[messages] 
(
	[Mobitel_ID] ASC,
	[MessageCounter] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[messages]') AND name = N'Index_6')
CREATE NONCLUSTERED INDEX [Index_6] ON [dbo].[messages] 
(
	[isNew] ASC,
	[Direction] ASC,
	[ServiceInit_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
