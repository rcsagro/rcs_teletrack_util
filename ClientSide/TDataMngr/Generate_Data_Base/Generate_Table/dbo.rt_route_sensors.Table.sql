/****** Object:  Table [dbo].[rt_route_sensors]    Script Date: 04/11/2013 11:22:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_sensors_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]'))
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [fgn_key_rt_route_sensors_FK1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_sensors_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]'))
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [fgn_key_rt_route_sensors_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___Senso__23893F36]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [DF__rt_route___Senso__23893F36]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___Locat__247D636F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [DF__rt_route___Locat__247D636F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___Speed__257187A8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [DF__rt_route___Speed__257187A8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___Dista__2665ABE1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_sensors] DROP CONSTRAINT [DF__rt_route___Dista__2665ABE1]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]') AND type in (N'U'))
DROP TABLE [dbo].[rt_route_sensors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_route_sensors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[SensorName] [varchar](127) NOT NULL DEFAULT (''),
	[Location] [varchar](255) NOT NULL DEFAULT (''),
	[EventTime] [datetime] NOT NULL,
	[Duration] [time](7) NOT NULL,
	[Description] [varchar](255) NULL,
	[Speed] [float] NULL DEFAULT ((0)),
	[Distance] [float] NULL DEFAULT ((0.00)),
 CONSTRAINT [PK_rt_route_sensors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]') AND name = N'rt_route_sensors_FK1')
CREATE NONCLUSTERED INDEX [rt_route_sensors_FK1] ON [dbo].[rt_route_sensors] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route_sensors', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route_sensors', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_sensors_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]'))
ALTER TABLE [dbo].[rt_route_sensors]  WITH CHECK ADD  CONSTRAINT [fgn_key_rt_route_sensors_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[rt_route] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_sensors_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_sensors]'))
ALTER TABLE [dbo].[rt_route_sensors] CHECK CONSTRAINT [fgn_key_rt_route_sensors_FK1]
GO
