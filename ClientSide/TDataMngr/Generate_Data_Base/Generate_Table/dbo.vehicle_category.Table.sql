/****** Object:  Table [dbo].[vehicle_category]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_ca__Name__4C564A9F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_category] DROP CONSTRAINT [DF__vehicle_ca__Name__4C564A9F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_c__Tonna__4D4A6ED8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_category] DROP CONSTRAINT [DF__vehicle_c__Tonna__4D4A6ED8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_c__FuelN__4E3E9311]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_category] DROP CONSTRAINT [DF__vehicle_c__FuelN__4E3E9311]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_c__FuelN__4F32B74A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_category] DROP CONSTRAINT [DF__vehicle_c__FuelN__4F32B74A]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle_category]') AND type in (N'U'))
DROP TABLE [dbo].[vehicle_category]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle_category]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[vehicle_category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[Tonnage] [decimal](10, 3) NOT NULL DEFAULT ((0)),
	[FuelNormMoving] [float] NOT NULL DEFAULT ((0)),
	[FuelNormParking] [float] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_vehicle_category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle_category', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ������������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle_category', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle_category', N'COLUMN',N'Tonnage'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������������,  ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle_category', @level2type=N'COLUMN',@level2name=N'Tonnage'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle_category', N'COLUMN',N'FuelNormMoving'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������� ������� � �������� �/100 ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle_category', @level2type=N'COLUMN',@level2name=N'FuelNormMoving'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle_category', N'COLUMN',N'FuelNormParking'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������� ������� �� �������� �������� �/���' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle_category', @level2type=N'COLUMN',@level2name=N'FuelNormParking'
GO
