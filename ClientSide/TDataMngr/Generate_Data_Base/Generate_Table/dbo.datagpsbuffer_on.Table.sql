/****** Object:  Table [dbo].[datagpsbuffer_on]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__24285DB4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Mobit__24285DB4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Messa__251C81ED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Messa__251C81ED]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Latit__2610A626]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Latit__2610A626]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Longi__2704CA5F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Longi__2704CA5F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__UnixT__27F8EE98]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__UnixT__27F8EE98]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Speed__28ED12D1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Speed__28ED12D1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Direc__29E1370A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Direc__29E1370A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Valid__2AD55B43]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Valid__2AD55B43]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Event__2BC97F7C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Event__2BC97F7C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__2CBDA3B5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__2CBDA3B5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__2DB1C7EE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__2DB1C7EE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__2EA5EC27]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__2EA5EC27]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__2F9A1060]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__2F9A1060]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__308E3499]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__308E3499]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__318258D2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__318258D2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__32767D0B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__32767D0B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__336AA144]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Senso__336AA144]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__345EC57D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__LogID__345EC57D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__isSho__3552E9B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__isSho__3552E9B6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__whatI__36470DEF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__whatI__36470DEF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__373B3228]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Count__373B3228]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__382F5661]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Count__382F5661]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__39237A9A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Count__39237A9A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__3A179ED3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__Count__3A179ED3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__SrvPa__3B0BC30C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_on] DROP CONSTRAINT [DF__datagpsbu__SrvPa__3B0BC30C]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbuffer_on]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer_on](
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NOT NULL DEFAULT ((0)),
	[Longitude] [int] NOT NULL DEFAULT ((0)),
	[Altitude] [int] NULL,
	[UnixTime] [int] NOT NULL DEFAULT ((0)),
	[Speed] [smallint] NOT NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NOT NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL,
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NOT NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [smallint] NOT NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
	[SrvPacketID] [bigint] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND name = N'IDX_MobitelidLogid')
CREATE NONCLUSTERED INDEX [IDX_MobitelidLogid] ON [dbo].[datagpsbuffer_on] 
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND name = N'IDX_MobitelidUnixtime')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtime] ON [dbo].[datagpsbuffer_on] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND name = N'IDX_MobitelidUnixtimeValid')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtimeValid] ON [dbo].[datagpsbuffer_on] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC,
	[Valid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on]') AND name = N'IDX_SrvPacketID')
CREATE NONCLUSTERED INDEX [IDX_SrvPacketID] ON [dbo].[datagpsbuffer_on] 
(
	[SrvPacketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
