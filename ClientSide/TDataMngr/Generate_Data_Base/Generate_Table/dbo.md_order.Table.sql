/****** Object:  Table [dbo].[md_order]    Script Date: 04/11/2013 11:22:25 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_object_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_objectStart_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_objectStart_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_order_category_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_priority_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_order_priority_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_transportation_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_transportation_types_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_waybill_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_waybill_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_object_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_objectStart_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_objectStart_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_order_category_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_priority_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_order_priority_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_transportation_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_transportation_types_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_waybill_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [fgn_key_FK_md_order_md_waybill_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdObje__7F80E8EA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdObje__7F80E8EA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdObje__00750D23]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdObje__00750D23]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdPrio__0169315C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdPrio__0169315C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdCate__025D5595]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdCate__025D5595]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdTran__035179CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdTran__035179CE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IdWayB__04459E07]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IdWayB__04459E07]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_order__IsClos__0539C240]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_order] DROP CONSTRAINT [DF__md_order__IsClos__0539C240]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND type in (N'U'))
DROP TABLE [dbo].[md_order]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [varchar](13) NULL,
	[DateInit] [datetime] NULL,
	[IdObjectCustomer] [int] NULL DEFAULT ((0)),
	[IdObjectStart] [int] NULL DEFAULT ((0)),
	[IdPriority] [int] NOT NULL DEFAULT ((0)),
	[DateCreate] [datetime] NOT NULL,
	[DateRefuse] [datetime] NULL,
	[DateEdit] [datetime] NULL,
	[DateDelete] [datetime] NULL,
	[DateExecute] [datetime] NULL,
	[DateStartWork] [datetime] NULL,
	[IdCategory] [int] NOT NULL DEFAULT ((0)),
	[IdTransportationYipe] [int] NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
	[IdWayBill] [int] NULL DEFAULT ((0)),
	[TimeDelay] [time](7) NULL,
	[IsClose] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_md_order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_object_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_object_Id] ON [dbo].[md_order] 
(
	[IdObjectCustomer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_objectStart_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_objectStart_Id] ON [dbo].[md_order] 
(
	[IdObjectStart] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_order_category_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_order_category_Id] ON [dbo].[md_order] 
(
	[IdCategory] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_order_priority_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_order_priority_Id] ON [dbo].[md_order] 
(
	[IdPriority] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_transportation_types_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_transportation_types_Id] ON [dbo].[md_order] 
(
	[IdTransportationYipe] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_order]') AND name = N'FK_md_order_md_waybill_Id')
CREATE NONCLUSTERED INDEX [FK_md_order_md_waybill_Id] ON [dbo].[md_order] 
(
	[IdWayBill] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateInit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ��������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateInit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdObjectCustomer'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdObjectCustomer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdObjectStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdObjectStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdPriority'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdPriority'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateCreate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� �������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateCreate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateRefuse'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ������ �� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateRefuse'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateEdit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� �������������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateEdit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateDelete'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� �������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateDelete'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateExecute'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ���������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateExecute'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'DateStartWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ������ ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'DateStartWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdCategory'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdCategory'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdTransportationYipe'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ���������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdTransportationYipe'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_order', N'COLUMN',N'IdWayBill'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_order', @level2type=N'COLUMN',@level2name=N'IdWayBill'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_object_Id] FOREIGN KEY([IdObjectCustomer])
REFERENCES [dbo].[md_object] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_object_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_object_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_objectStart_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_objectStart_Id] FOREIGN KEY([IdObjectStart])
REFERENCES [dbo].[md_object] ([Id])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_objectStart_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_objectStart_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_order_category_Id] FOREIGN KEY([IdCategory])
REFERENCES [dbo].[md_order_category] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_order_category_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_priority_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_order_priority_Id] FOREIGN KEY([IdPriority])
REFERENCES [dbo].[md_order_priority] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_order_priority_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_order_priority_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_transportation_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_transportation_types_Id] FOREIGN KEY([IdTransportationYipe])
REFERENCES [dbo].[md_transportation_types] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_transportation_types_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_transportation_types_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_waybill_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_order_md_waybill_Id] FOREIGN KEY([IdWayBill])
REFERENCES [dbo].[md_waybill] ([Id])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_order_md_waybill_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_order]'))
ALTER TABLE [dbo].[md_order] CHECK CONSTRAINT [fgn_key_FK_md_order_md_waybill_Id]
GO
