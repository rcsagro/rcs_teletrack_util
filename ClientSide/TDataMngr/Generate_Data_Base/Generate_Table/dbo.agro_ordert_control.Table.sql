/****** Object:  Table [dbo].[agro_ordert_control]    Script Date: 04/11/2013 11:22:21 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_ordert_control_agro_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]'))
ALTER TABLE [dbo].[agro_ordert_control] DROP CONSTRAINT [fgn_key_FK_agro_ordert_control_agro_order_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_ordert_control_agro_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]'))
ALTER TABLE [dbo].[agro_ordert_control] DROP CONSTRAINT [fgn_key_FK_agro_ordert_control_agro_order_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_ma__47DBAE45]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert_control] DROP CONSTRAINT [DF__agro_orde__Id_ma__47DBAE45]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Id_ob__48CFD27E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert_control] DROP CONSTRAINT [DF__agro_orde__Id_ob__48CFD27E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_orde__Type___49C3F6B7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_ordert_control] DROP CONSTRAINT [DF__agro_orde__Type___49C3F6B7]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]') AND type in (N'U'))
DROP TABLE [dbo].[agro_ordert_control]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_ordert_control](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL DEFAULT ((0)),
	[Id_object] [int] NOT NULL DEFAULT ((0)),
	[Type_object] [smallint] NOT NULL DEFAULT ((0)),
	[TimeStart] [datetime] NOT NULL,
	[TimeEnd] [datetime] NOT NULL,
 CONSTRAINT [PK_agro_ordert_control] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]') AND name = N'FK_agro_ordert_control_agro_order_Id')
CREATE NONCLUSTERED INDEX [FK_agro_ordert_control_agro_order_Id] ON [dbo].[agro_ordert_control] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert_control', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert_control', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert_control', N'COLUMN',N'Id_object'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert_control', @level2type=N'COLUMN',@level2name=N'Id_object'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert_control', N'COLUMN',N'Type_object'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert_control', @level2type=N'COLUMN',@level2name=N'Type_object'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert_control', N'COLUMN',N'TimeStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������ ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert_control', @level2type=N'COLUMN',@level2name=N'TimeStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_ordert_control', N'COLUMN',N'TimeEnd'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ��������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_ordert_control', @level2type=N'COLUMN',@level2name=N'TimeEnd'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_ordert_control_agro_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]'))
ALTER TABLE [dbo].[agro_ordert_control]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_ordert_control_agro_order_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[agro_order] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_ordert_control_agro_order_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_ordert_control]'))
ALTER TABLE [dbo].[agro_ordert_control] CHECK CONSTRAINT [fgn_key_FK_agro_ordert_control_agro_order_Id]
GO
