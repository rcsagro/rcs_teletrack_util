/****** Object:  Table [dbo].[configevent]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configevent__ID__6383C8BA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configevent__ID__6383C8BA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__Flags__6477ECF3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__Flags__6477ECF3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__Messa__656C112C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__Messa__656C112C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__tmr1L__66603565]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__tmr1L__66603565]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__tmr2S__6754599E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__tmr2S__6754599E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__tmr3Z__68487DD7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__tmr3Z__68487DD7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__dist1__693CA210]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__dist1__693CA210]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__dist2__6A30C649]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__dist2__6A30C649]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__dist3__6B24EA82]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__dist3__6B24EA82]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__delta__6C190EBB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__delta__6C190EBB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__gprsE__6D0D32F4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__gprsE__6D0D32F4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__gprsF__6E01572D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__gprsF__6E01572D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__gprsS__6EF57B66]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__gprsS__6EF57B66]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__6FE99F9F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__6FE99F9F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__70DDC3D8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__70DDC3D8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__71D1E811]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__71D1E811]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__72C60C4A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__72C60C4A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__73BA3083]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__73BA3083]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__74AE54BC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__74AE54BC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__75A278F5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__75A278F5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__76969D2E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__76969D2E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__778AC167]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__778AC167]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__787EE5A0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__787EE5A0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__797309D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__797309D9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7A672E12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7A672E12]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7B5B524B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7B5B524B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7C4F7684]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7C4F7684]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7D439ABD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7D439ABD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7E37BEF6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7E37BEF6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__7F2BE32F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__7F2BE32F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__00200768]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__00200768]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__01142BA1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__01142BA1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__02084FDA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__02084FDA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__02FC7413]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__02FC7413]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__03F0984C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__03F0984C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__04E4BC85]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__04E4BC85]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__05D8E0BE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__05D8E0BE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__06CD04F7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__06CD04F7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__07C12930]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__07C12930]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__08B54D69]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__08B54D69]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__09A971A2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__09A971A2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__0A9D95DB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__0A9D95DB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__0B91BA14]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__0B91BA14]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configeve__maskE__0C85DE4D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configevent] DROP CONSTRAINT [DF__configeve__maskE__0C85DE4D]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configevent]') AND type in (N'U'))
DROP TABLE [dbo].[configevent]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configevent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configevent](
	[ConfigEvent_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[tmr1Log] [int] NULL DEFAULT ((0)),
	[tmr2Send] [int] NULL DEFAULT ((0)),
	[tmr3Zone] [int] NULL DEFAULT ((0)),
	[dist1Log] [int] NULL DEFAULT ((0)),
	[dist2Send] [int] NULL DEFAULT ((0)),
	[dist3Zone] [int] NULL DEFAULT ((0)),
	[deltaTimeZone] [int] NULL DEFAULT ((0)),
	[gprsEmail] [int] NOT NULL DEFAULT ((0)),
	[gprsFtp] [int] NOT NULL DEFAULT ((0)),
	[gprsSocket] [int] NOT NULL DEFAULT ((0)),
	[maskSensor] [char](50) NULL,
	[maskEvent1] [int] NULL DEFAULT ((0)),
	[maskEvent2] [int] NULL DEFAULT ((0)),
	[maskEvent3] [int] NULL DEFAULT ((0)),
	[maskEvent4] [int] NULL DEFAULT ((0)),
	[maskEvent5] [int] NULL DEFAULT ((0)),
	[maskEvent6] [int] NULL DEFAULT ((0)),
	[maskEvent7] [int] NULL DEFAULT ((0)),
	[maskEvent8] [int] NULL DEFAULT ((0)),
	[maskEvent9] [int] NULL DEFAULT ((0)),
	[maskEvent10] [int] NULL DEFAULT ((0)),
	[maskEvent11] [int] NULL DEFAULT ((0)),
	[maskEvent12] [int] NULL DEFAULT ((0)),
	[maskEvent13] [int] NULL DEFAULT ((0)),
	[maskEvent14] [int] NULL DEFAULT ((0)),
	[maskEvent15] [int] NULL DEFAULT ((0)),
	[maskEvent16] [int] NULL DEFAULT ((0)),
	[maskEvent17] [int] NULL DEFAULT ((0)),
	[maskEvent18] [int] NULL DEFAULT ((0)),
	[maskEvent19] [int] NULL DEFAULT ((0)),
	[maskEvent20] [int] NULL DEFAULT ((0)),
	[maskEvent21] [int] NULL DEFAULT ((0)),
	[maskEvent22] [int] NULL DEFAULT ((0)),
	[maskEvent23] [int] NULL DEFAULT ((0)),
	[maskEvent24] [int] NULL DEFAULT ((0)),
	[maskEvent25] [int] NULL DEFAULT ((0)),
	[maskEvent26] [int] NULL DEFAULT ((0)),
	[maskEvent27] [int] NULL DEFAULT ((0)),
	[maskEvent28] [int] NULL DEFAULT ((0)),
	[maskEvent29] [int] NULL DEFAULT ((0)),
	[maskEvent30] [int] NULL DEFAULT ((0)),
	[maskEvent31] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_configevent] PRIMARY KEY CLUSTERED 
(
	[ConfigEvent_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configevent]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configevent] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
