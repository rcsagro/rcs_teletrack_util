/****** Object:  Table [dbo].[datagpsbuffer_pop]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__40C49C62]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Mobit__40C49C62]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Messa__41B8C09B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Messa__41B8C09B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Latit__42ACE4D4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Latit__42ACE4D4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Longi__43A1090D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Longi__43A1090D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Altit__44952D46]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Altit__44952D46]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__UnixT__4589517F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__UnixT__4589517F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Speed__467D75B8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Speed__467D75B8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Direc__477199F1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Direc__477199F1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Valid__4865BE2A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Valid__4865BE2A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__InMob__4959E263]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__InMob__4959E263]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Event__4A4E069C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Event__4A4E069C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__4B422AD5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__4B422AD5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__4C364F0E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__4C364F0E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__4D2A7347]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__4D2A7347]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__4E1E9780]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__4E1E9780]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__4F12BBB9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__4F12BBB9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__5006DFF2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__5006DFF2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__50FB042B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__50FB042B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__51EF2864]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Senso__51EF2864]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__52E34C9D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__LogID__52E34C9D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__isSho__53D770D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__isSho__53D770D6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__whatI__54CB950F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__whatI__54CB950F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__55BFB948]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Count__55BFB948]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__56B3DD81]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Count__56B3DD81]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__57A801BA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Count__57A801BA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__589C25F3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_pop] DROP CONSTRAINT [DF__datagpsbu__Count__589C25F3]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_pop]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbuffer_pop]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_pop]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer_pop](
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NOT NULL DEFAULT ((0)),
	[Longitude] [int] NOT NULL DEFAULT ((0)),
	[Altitude] [int] NULL DEFAULT ((0)),
	[UnixTime] [int] NOT NULL DEFAULT ((0)),
	[Speed] [smallint] NOT NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NOT NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL DEFAULT ((0)),
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [int] NOT NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1))
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_pop]') AND name = N'IDX_MobitelIDLogID')
CREATE NONCLUSTERED INDEX [IDX_MobitelIDLogID] ON [dbo].[datagpsbuffer_pop] 
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_pop]') AND name = N'IDX_MobitelidUnixtime')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtime] ON [dbo].[datagpsbuffer_pop] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_pop]') AND name = N'IDX_MobitelidUnixtimeValid')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtimeValid] ON [dbo].[datagpsbuffer_pop] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC,
	[Valid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
