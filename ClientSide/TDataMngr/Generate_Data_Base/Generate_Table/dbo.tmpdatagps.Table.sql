/****** Object:  Table [dbo].[tmpdatagps]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__DataG__19CACAD2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__DataG__19CACAD2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Mobit__1ABEEF0B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Mobit__1ABEEF0B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Messa__1BB31344]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Messa__1BB31344]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Latit__1CA7377D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Latit__1CA7377D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Longi__1D9B5BB6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Longi__1D9B5BB6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__UnixT__1E8F7FEF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__UnixT__1E8F7FEF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Speed__1F83A428]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Speed__1F83A428]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Direc__2077C861]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Direc__2077C861]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Valid__216BEC9A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Valid__216BEC9A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Event__226010D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Event__226010D3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__2354350C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__2354350C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__24485945]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__24485945]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__253C7D7E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__253C7D7E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__2630A1B7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__2630A1B7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__2724C5F0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__2724C5F0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__2818EA29]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__2818EA29]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__290D0E62]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__290D0E62]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Senso__2A01329B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Senso__2A01329B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__LogID__2AF556D4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__LogID__2AF556D4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__isSho__2BE97B0D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__isSho__2BE97B0D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__whatI__2CDD9F46]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__whatI__2CDD9F46]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Count__2DD1C37F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Count__2DD1C37F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Count__2EC5E7B8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Count__2EC5E7B8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Count__2FBA0BF1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Count__2FBA0BF1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__Count__30AE302A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__Count__30AE302A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__tmpdatagp__SrvPa__31A25463]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tmpdatagps] DROP CONSTRAINT [DF__tmpdatagp__SrvPa__31A25463]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmpdatagps]') AND type in (N'U'))
DROP TABLE [dbo].[tmpdatagps]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmpdatagps]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tmpdatagps](
	[DataGps_ID] [int] NOT NULL DEFAULT ((0)),
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NULL DEFAULT ((0)),
	[Longitude] [int] NULL DEFAULT ((0)),
	[Altitude] [int] NULL,
	[UnixTime] [int] NULL DEFAULT ((0)),
	[Speed] [smallint] NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL,
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [int] NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
	[SrvPacketID] [bigint] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'tmpdatagps', N'COLUMN',N'SrvPacketID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID ���������� ������ � ������ �������� ������ ��� ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tmpdatagps', @level2type=N'COLUMN',@level2name=N'SrvPacketID'
GO
