/****** Object:  Table [dbo].[sensordata]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensordata]') AND type in (N'U'))
DROP TABLE [dbo].[sensordata]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensordata]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensordata](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[datagps_id] [bigint] NOT NULL,
	[sensor_id] [bigint] NOT NULL,
	[Value] [float] NOT NULL,
 CONSTRAINT [PK_sensordata] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sensordata]') AND name = N'Index_2')
CREATE UNIQUE NONCLUSTERED INDEX [Index_2] ON [dbo].[sensordata] 
(
	[datagps_id] ASC,
	[sensor_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
