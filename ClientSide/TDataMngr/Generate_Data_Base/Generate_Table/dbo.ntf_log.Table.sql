/****** Object:  Table [dbo].[ntf_log]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_log_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_log]'))
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [fgn_key_FK_ntf_log_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_log_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_log]'))
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [fgn_key_FK_ntf_log_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Mobitel__4EA8A765]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Mobitel__4EA8A765]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Zone_id__4F9CCB9E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Zone_id__4F9CCB9E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Lat__5090EFD7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Lat__5090EFD7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Lng__51851410]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Lng__51851410]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__IsRead__52793849]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__IsRead__52793849]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__DataGps__536D5C82]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__DataGps__536D5C82]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Speed__546180BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Speed__546180BB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_log__Value__5555A4F4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_log] DROP CONSTRAINT [DF__ntf_log__Value__5555A4F4]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_log]') AND type in (N'U'))
DROP TABLE [dbo].[ntf_log]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_log]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ntf_log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[DateEvent] [datetime] NOT NULL,
	[DateWrite] [datetime] NOT NULL,
	[TypeEvent] [smallint] NOT NULL,
	[Mobitel_id] [int] NOT NULL DEFAULT ((0)),
	[Zone_id] [int] NOT NULL DEFAULT ((0)),
	[Location] [varchar](255) NULL,
	[Lat] [float] NULL DEFAULT ((0)),
	[Lng] [float] NULL DEFAULT ((0)),
	[Infor] [varchar](255) NULL,
	[IsRead] [bit] NULL DEFAULT ((0)),
	[DataGps_ID] [int] NULL DEFAULT ((0)),
	[Speed] [float] NULL DEFAULT ((0)),
	[Value] [float] NULL DEFAULT ((0)),
 CONSTRAINT [PK_ntf_log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ntf_log]') AND name = N'FK_ntf_log_ntf_main_Id')
CREATE NONCLUSTERED INDEX [FK_ntf_log_ntf_main_Id] ON [dbo].[ntf_log] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_log', N'COLUMN',N'IsRead'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ��������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_log', @level2type=N'COLUMN',@level2name=N'IsRead'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ntf_log', N'COLUMN',N'Value'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� �������� ������� (�������)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ntf_log', @level2type=N'COLUMN',@level2name=N'Value'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_log_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_log]'))
ALTER TABLE [dbo].[ntf_log]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_ntf_log_ntf_main_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[ntf_main] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_log_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_log]'))
ALTER TABLE [dbo].[ntf_log] CHECK CONSTRAINT [fgn_key_FK_ntf_log_ntf_main_Id]
GO
