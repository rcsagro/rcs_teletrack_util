/****** Object:  Table [dbo].[sensor_hookups]    Script Date: 04/11/2013 11:22:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensor_hookups]') AND type in (N'U'))
DROP TABLE [dbo].[sensor_hookups]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensor_hookups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensor_hookups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NULL,
	[Sensor_ID] [int] NULL,
	[FirstBit] [int] NULL,
 CONSTRAINT [PK_sensor_hookups] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
