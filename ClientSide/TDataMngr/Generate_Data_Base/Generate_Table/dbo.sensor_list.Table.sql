/****** Object:  Table [dbo].[sensor_list]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensor_list]') AND type in (N'U'))
DROP TABLE [dbo].[sensor_list]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensor_list]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensor_list](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](70) NULL,
	[Kind] [varchar](20) NULL,
	[BitsCount] [int] NULL,
	[FirstBit] [int] NULL,
	[Data] [varchar](max) NULL,
 CONSTRAINT [PK_sensor_list] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
