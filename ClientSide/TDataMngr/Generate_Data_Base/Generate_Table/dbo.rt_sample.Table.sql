/****** Object:  Table [dbo].[rt_sample]    Script Date: 04/11/2013 11:22:27 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__Name__2FEF161B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__Name__2FEF161B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__Id_mo__30E33A54]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__Id_mo__30E33A54]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__Dista__31D75E8D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__Dista__31D75E8D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__Id_ma__32CB82C6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__Id_ma__32CB82C6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__IsGro__33BFA6FF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__IsGro__33BFA6FF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_sample__AutoC__34B3CB38]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_sample] DROP CONSTRAINT [DF__rt_sample__AutoC__34B3CB38]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_sample]') AND type in (N'U'))
DROP TABLE [dbo].[rt_sample]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_sample]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_sample](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL DEFAULT (''),
	[Id_mobitel] [int] NULL DEFAULT ((0)),
	[TimeStart] [time](7) NULL,
	[Distance] [float] NULL DEFAULT ((0)),
	[Remark] [varchar](max) NULL,
	[Id_main] [int] NOT NULL DEFAULT ((0)),
	[IsGroupe] [smallint] NOT NULL DEFAULT ((0)),
	[AutoCreate] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_rt_sample] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'Id_mobitel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������� (���)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Id_mobitel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'TimeStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ������ �������� (���)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'TimeStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����, ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'Remark'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Remark'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������� �������� ������ � ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'IsGroupe'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ - ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'IsGroupe'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_sample', N'COLUMN',N'AutoCreate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ����� ��������������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_sample', @level2type=N'COLUMN',@level2name=N'AutoCreate'
GO
