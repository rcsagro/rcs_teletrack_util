/****** Object:  Table [dbo].[vehicle_state]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_st__Name__511AFFBC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_state] DROP CONSTRAINT [DF__vehicle_st__Name__511AFFBC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle_s__MaxTi__520F23F5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle_state] DROP CONSTRAINT [DF__vehicle_s__MaxTi__520F23F5]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle_state]') AND type in (N'U'))
DROP TABLE [dbo].[vehicle_state]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle_state]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[vehicle_state](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[MaxTime] [decimal](10, 2) NOT NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_vehicle_state] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
