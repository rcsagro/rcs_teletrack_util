/****** Object:  Table [dbo].[mapstylelines]    Script Date: 04/11/2013 11:22:24 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstyleline__ID__61F08603]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylelines] DROP CONSTRAINT [DF__mapstyleline__ID__61F08603]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylel__Color__62E4AA3C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylelines] DROP CONSTRAINT [DF__mapstylel__Color__62E4AA3C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylel__Width__63D8CE75]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylelines] DROP CONSTRAINT [DF__mapstylel__Width__63D8CE75]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylel__Style__64CCF2AE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylelines] DROP CONSTRAINT [DF__mapstylel__Style__64CCF2AE]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstylelines]') AND type in (N'U'))
DROP TABLE [dbo].[mapstylelines]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstylelines]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mapstylelines](
	[MapStyleLine_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Color] [int] NULL DEFAULT ((0)),
	[Width] [smallint] NULL DEFAULT ((0)),
	[Style] [smallint] NULL DEFAULT ((0)),
 CONSTRAINT [PK_mapstylelines] PRIMARY KEY CLUSTERED 
(
	[MapStyleLine_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
