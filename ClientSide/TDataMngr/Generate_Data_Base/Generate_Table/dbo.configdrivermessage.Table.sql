/****** Object:  Table [dbo].[configdrivermessage]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configdriver__ID__5FB337D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configdrivermessage] DROP CONSTRAINT [DF__configdriver__ID__5FB337D6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configdri__Flags__60A75C0F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configdrivermessage] DROP CONSTRAINT [DF__configdri__Flags__60A75C0F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configdri__Messa__619B8048]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configdrivermessage] DROP CONSTRAINT [DF__configdri__Messa__619B8048]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configdrivermessage]') AND type in (N'U'))
DROP TABLE [dbo].[configdrivermessage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configdrivermessage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configdrivermessage](
	[ConfigDriverMessage_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[DriverMessage] [char](200) NULL,
 CONSTRAINT [PK_configdrivermessage] PRIMARY KEY CLUSTERED 
(
	[ConfigDriverMessage_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configdrivermessage]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configdrivermessage] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
