/****** Object:  Table [dbo].[agro_agregat_vehicle]    Script Date: 04/11/2013 11:22:21 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_vehicle_id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_vehicle_id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Id_ag__060DEAE8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [DF__agro_agre__Id_ag__060DEAE8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Id_ve__07020F21]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [DF__agro_agre__Id_ve__07020F21]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_agre__Id_se__07F6335A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_agregat_vehicle] DROP CONSTRAINT [DF__agro_agre__Id_se__07F6335A]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]') AND type in (N'U'))
DROP TABLE [dbo].[agro_agregat_vehicle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_agregat_vehicle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_agregat] [int] NOT NULL DEFAULT ((0)),
	[Id_vehicle] [int] NOT NULL DEFAULT ((0)),
	[Id_sensor] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_agro_agregat_vehicle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]') AND name = N'FK_agro_agregat_vehicle_agro_agregat_Id')
CREATE NONCLUSTERED INDEX [FK_agro_agregat_vehicle_agro_agregat_Id] ON [dbo].[agro_agregat_vehicle] 
(
	[Id_agregat] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]') AND name = N'FK_agro_agregat_vehicle_vehicle_id')
CREATE NONCLUSTERED INDEX [FK_agro_agregat_vehicle_vehicle_id] ON [dbo].[agro_agregat_vehicle] 
(
	[Id_vehicle] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_agregat_vehicle', N'COLUMN',N'Id_sensor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_agregat_vehicle', @level2type=N'COLUMN',@level2name=N'Id_sensor'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id] FOREIGN KEY([Id_agregat])
REFERENCES [dbo].[agro_agregat] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] CHECK CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_agro_agregat_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_vehicle_id] FOREIGN KEY([Id_vehicle])
REFERENCES [dbo].[vehicle] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_agro_agregat_vehicle_vehicle_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[agro_agregat_vehicle]'))
ALTER TABLE [dbo].[agro_agregat_vehicle] CHECK CONSTRAINT [fgn_key_FK_agro_agregat_vehicle_vehicle_id]
GO
