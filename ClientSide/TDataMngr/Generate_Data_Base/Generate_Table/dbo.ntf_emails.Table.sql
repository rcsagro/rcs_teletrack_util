/****** Object:  Table [dbo].[ntf_emails]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_email_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_emails]'))
ALTER TABLE [dbo].[ntf_emails] DROP CONSTRAINT [fgn_key_FK_ntf_email_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_email_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_emails]'))
ALTER TABLE [dbo].[ntf_emails] DROP CONSTRAINT [fgn_key_FK_ntf_email_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_email__IsAct__5D16C24D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_emails] DROP CONSTRAINT [DF__ntf_email__IsAct__5D16C24D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_email__Email__5E0AE686]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_emails] DROP CONSTRAINT [DF__ntf_email__Email__5E0AE686]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_emails]') AND type in (N'U'))
DROP TABLE [dbo].[ntf_emails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_emails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ntf_emails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[IsActive] [smallint] NOT NULL DEFAULT ((0)),
	[Email] [varchar](127) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_ntf_emails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_email_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_emails]'))
ALTER TABLE [dbo].[ntf_emails]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_ntf_email_ntf_main_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[ntf_main] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_email_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_emails]'))
ALTER TABLE [dbo].[ntf_emails] CHECK CONSTRAINT [fgn_key_FK_ntf_email_ntf_main_Id]
GO
