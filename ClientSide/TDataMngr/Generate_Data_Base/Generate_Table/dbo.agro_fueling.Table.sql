/****** Object:  Table [dbo].[agro_fueling]    Script Date: 04/11/2013 11:22:21 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuel__LockR__15502E78]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuel__LockR__15502E78]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuelin__Lat__164452B1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuelin__Lat__164452B1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuelin__Lng__173876EA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuelin__Lng__173876EA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuel__Value__182C9B23]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuel__Value__182C9B23]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuel__Value__1920BF5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuel__Value__1920BF5C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_fuel__Value__1A14E395]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_fueling] DROP CONSTRAINT [DF__agro_fuel__Value__1A14E395]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fueling]') AND type in (N'U'))
DROP TABLE [dbo].[agro_fueling]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_fueling]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_fueling](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NULL,
	[LockRecord] [smallint] NOT NULL DEFAULT ((0)),
	[Lat] [float] NOT NULL DEFAULT ((0)),
	[Lng] [float] NOT NULL DEFAULT ((0)),
	[DateFueling] [datetime] NOT NULL,
	[ValueSystem] [float] NOT NULL DEFAULT ((0)),
	[ValueDisp] [float] NOT NULL DEFAULT ((0)),
	[ValueStart] [float] NOT NULL DEFAULT ((0)),
	[Location] [varchar](255) NULL,
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_agro_fueling] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'LockRecord'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������������� ��� ��������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'LockRecord'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'DateFueling'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� �������� / �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'DateFueling'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'ValueSystem'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'ValueSystem'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'ValueDisp'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'ValueDisp'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'ValueStart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ����� ��������� / ������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'ValueStart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'agro_fueling', N'COLUMN',N'Remark'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� � ��������� �������� �����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'agro_fueling', @level2type=N'COLUMN',@level2name=N'Remark'
GO
