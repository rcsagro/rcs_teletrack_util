/****** Object:  Table [dbo].[lastmeters]    Script Date: 04/11/2013 11:22:24 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lastmeter__LastM__2C88998B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lastmeters] DROP CONSTRAINT [DF__lastmeter__LastM__2C88998B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lastmeters__ID__2D7CBDC4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lastmeters] DROP CONSTRAINT [DF__lastmeters__ID__2D7CBDC4]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lastmeters]') AND type in (N'U'))
DROP TABLE [dbo].[lastmeters]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lastmeters]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[lastmeters](
	[LastMeter_ID] [int] NULL DEFAULT ((0)),
	[ID] [int] NULL DEFAULT ((0)),
	[Name] [char](200) NULL,
	[Descr] [char](200) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
