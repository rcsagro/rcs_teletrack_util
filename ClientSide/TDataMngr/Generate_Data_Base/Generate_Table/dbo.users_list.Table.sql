/****** Object:  Table [dbo].[users_list]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_list__Name__40E497F3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_list] DROP CONSTRAINT [DF__users_list__Name__40E497F3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_lis__Admin__41D8BC2C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_list] DROP CONSTRAINT [DF__users_lis__Admin__41D8BC2C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__users_lis__Id_ro__42CCE065]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[users_list] DROP CONSTRAINT [DF__users_lis__Id_ro__42CCE065]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_list]') AND type in (N'U'))
DROP TABLE [dbo].[users_list]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users_list]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[users_list](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL DEFAULT (''),
	[WinLogin] [varchar](50) NULL,
	[Password] [varbinary](max) NOT NULL,
	[Admin] [smallint] NOT NULL DEFAULT ((0)),
	[Id_role] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_users_list] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'users_list', N'COLUMN',N'Id_role'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ���� ������������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users_list', @level2type=N'COLUMN',@level2name=N'Id_role'
GO
