/****** Object:  Table [dbo].[mobitels_info]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels_info]') AND type in (N'U'))
DROP TABLE [dbo].[mobitels_info]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels_info]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mobitels_info](
	[Mobitel_ID] [bigint] NOT NULL,
	[Map_Style_Car] [varchar](max) NULL,
	[Track_Color] [bigint] NULL,
 CONSTRAINT [PK_mobitels_info] PRIMARY KEY CLUSTERED 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
