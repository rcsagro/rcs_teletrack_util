/****** Object:  Table [dbo].[md_object_group]    Script Date: 04/11/2013 11:22:25 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object___Name__7D98A078]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object_group] DROP CONSTRAINT [DF__md_object___Name__7D98A078]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_object_group]') AND type in (N'U'))
DROP TABLE [dbo].[md_object_group]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_object_group]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_object_group](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_object_group] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
