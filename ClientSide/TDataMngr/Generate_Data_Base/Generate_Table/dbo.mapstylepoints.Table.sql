/****** Object:  Table [dbo].[mapstylepoints]    Script Date: 04/11/2013 11:22:24 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylepoin__ID__66B53B20]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylepoin__ID__66B53B20]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylep__Symbo__67A95F59]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylep__Symbo__67A95F59]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylep__Color__689D8392]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylep__Color__689D8392]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylepo__Size__6991A7CB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylepo__Size__6991A7CB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylep__Rotat__6A85CC04]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylep__Rotat__6A85CC04]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylep__Bitma__6B79F03D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylep__Bitma__6B79F03D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mapstylep__Style__6C6E1476]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mapstylepoints] DROP CONSTRAINT [DF__mapstylep__Style__6C6E1476]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstylepoints]') AND type in (N'U'))
DROP TABLE [dbo].[mapstylepoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapstylepoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mapstylepoints](
	[MapStylePoint_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[isBitmap] [smallint] NULL,
	[SymbolCode] [smallint] NULL DEFAULT ((0)),
	[Color] [int] NULL DEFAULT ((0)),
	[Font] [char](200) NULL,
	[Size] [int] NULL DEFAULT ((0)),
	[Rotation] [int] NULL DEFAULT ((0)),
	[BitmapName] [char](200) NULL,
	[BitmapStyle] [smallint] NULL DEFAULT ((0)),
	[Style] [smallint] NULL DEFAULT ((0)),
 CONSTRAINT [PK_mapstylepoints] PRIMARY KEY CLUSTERED 
(
	[MapStylePoint_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
