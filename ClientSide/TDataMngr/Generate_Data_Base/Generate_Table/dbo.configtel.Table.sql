/****** Object:  Table [dbo].[configtel]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__ID__47A6A41B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__ID__47A6A41B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__Flags__489AC854]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__Flags__489AC854]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__Messa__498EEC8D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__Messa__498EEC8D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telSO__4A8310C6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telSO__4A8310C6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telDs__4B7734FF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telDs__4B7734FF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telAc__4C6B5938]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telAc__4C6B5938]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telAc__4D5F7D71]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telAc__4D5F7D71]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telAc__4E53A1AA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telAc__4E53A1AA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configtel__telAn__4F47C5E3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configtel] DROP CONSTRAINT [DF__configtel__telAn__4F47C5E3]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configtel]') AND type in (N'U'))
DROP TABLE [dbo].[configtel]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configtel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configtel](
	[ConfigTel_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[telSOS] [int] NULL DEFAULT ((0)),
	[telDspt] [int] NULL DEFAULT ((0)),
	[telAccept1] [int] NULL DEFAULT ((0)),
	[telAccept2] [int] NULL DEFAULT ((0)),
	[telAccept3] [int] NULL DEFAULT ((0)),
	[telAnswer] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_configtel] PRIMARY KEY CLUSTERED 
(
	[ConfigTel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[configtel]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[configtel] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
