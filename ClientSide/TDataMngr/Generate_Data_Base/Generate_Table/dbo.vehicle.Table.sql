/****** Object:  Table [dbo].[vehicle]    Script Date: 04/11/2013 11:22:29 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_mobitels_Mobitel_ID]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_team_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_team_id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_vehicle_category_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_state_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_vehicle_state_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_mobitels_Mobitel_ID]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_team_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_team_id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_vehicle_category_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_state_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [fgn_key_FK_vehicle_vehicle_state_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle__Odometr__47919582]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [DF__vehicle__Odometr__47919582]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle__setting__4885B9BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [DF__vehicle__setting__4885B9BB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle__driver___4979DDF4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [DF__vehicle__driver___4979DDF4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__vehicle__State__4A6E022D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[vehicle] DROP CONSTRAINT [DF__vehicle__State__4A6E022D]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND type in (N'U'))
DROP TABLE [dbo].[vehicle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[vehicle](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[MakeCar] [varchar](max) NULL,
	[NumberPlate] [varchar](max) NULL,
	[Team_id] [int] NULL,
	[Mobitel_id] [int] NULL,
	[Odometr] [int] NULL DEFAULT ((0)),
	[CarModel] [varchar](max) NULL,
	[setting_id] [int] NULL DEFAULT ((1)),
	[odoTime] [datetime] NULL,
	[driver_id] [int] NULL DEFAULT ((-1)),
	[State] [int] NULL DEFAULT ((2)),
	[OutLinkId] [varchar](20) NULL,
	[Category_id] [int] NULL,
 CONSTRAINT [PK_vehicle] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'FK_vehicle_mobitels_Mobitel_ID')
CREATE NONCLUSTERED INDEX [FK_vehicle_mobitels_Mobitel_ID] ON [dbo].[vehicle] 
(
	[Mobitel_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'FK_vehicle_team_id')
CREATE NONCLUSTERED INDEX [FK_vehicle_team_id] ON [dbo].[vehicle] 
(
	[Team_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'FK_vehicle_vehicle_category_Id')
CREATE NONCLUSTERED INDEX [FK_vehicle_vehicle_category_Id] ON [dbo].[vehicle] 
(
	[Category_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'FK_vehicle_vehicle_state_Id')
CREATE NONCLUSTERED INDEX [FK_vehicle_vehicle_state_Id] ON [dbo].[vehicle] 
(
	[State] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'id')
CREATE UNIQUE NONCLUSTERED INDEX [id] ON [dbo].[vehicle] 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[vehicle]') AND name = N'setting_id')
CREATE NONCLUSTERED INDEX [setting_id] ON [dbo].[vehicle] 
(
	[setting_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'MakeCar'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'MakeCar'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'NumberPlate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'NumberPlate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'Odometr'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'Odometr'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'CarModel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'CarModel'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'odoTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� � ����� ������ ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'odoTime'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'OutLinkId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'OutLinkId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'vehicle', N'COLUMN',N'Category_id'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� �����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'vehicle', @level2type=N'COLUMN',@level2name=N'Category_id'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_vehicle_mobitels_Mobitel_ID] FOREIGN KEY([Mobitel_id])
REFERENCES [dbo].[mobitels] ([Mobitel_ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] CHECK CONSTRAINT [fgn_key_FK_vehicle_mobitels_Mobitel_ID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_team_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_vehicle_team_id] FOREIGN KEY([Team_id])
REFERENCES [dbo].[team] ([id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_team_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] CHECK CONSTRAINT [fgn_key_FK_vehicle_team_id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_vehicle_vehicle_category_Id] FOREIGN KEY([Category_id])
REFERENCES [dbo].[vehicle_category] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_category_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] CHECK CONSTRAINT [fgn_key_FK_vehicle_vehicle_category_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_state_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_vehicle_vehicle_state_Id] FOREIGN KEY([State])
REFERENCES [dbo].[vehicle_state] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_vehicle_vehicle_state_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[vehicle]'))
ALTER TABLE [dbo].[vehicle] CHECK CONSTRAINT [fgn_key_FK_vehicle_vehicle_state_Id]
GO
