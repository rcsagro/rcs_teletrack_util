/****** Object:  Table [dbo].[mobitels_rotate_bands]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]'))
ALTER TABLE [dbo].[mobitels_rotate_bands] DROP CONSTRAINT [fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]'))
ALTER TABLE [dbo].[mobitels_rotate_bands] DROP CONSTRAINT [fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels___Mobit__4336F4B9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels_rotate_bands] DROP CONSTRAINT [DF__mobitels___Mobit__4336F4B9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__mobitels___Bound__442B18F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[mobitels_rotate_bands] DROP CONSTRAINT [DF__mobitels___Bound__442B18F2]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]') AND type in (N'U'))
DROP TABLE [dbo].[mobitels_rotate_bands]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mobitels_rotate_bands](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NOT NULL DEFAULT ((0)),
	[Bound] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_mobitels_rotate_bands] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]') AND name = N'FK_mobitels_rotate_bands_mobitels_Mobitel_ID')
CREATE NONCLUSTERED INDEX [FK_mobitels_rotate_bands_mobitels_Mobitel_ID] ON [dbo].[mobitels_rotate_bands] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]'))
ALTER TABLE [dbo].[mobitels_rotate_bands]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID] FOREIGN KEY([Mobitel_ID])
REFERENCES [dbo].[mobitels] ([Mobitel_ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[mobitels_rotate_bands]'))
ALTER TABLE [dbo].[mobitels_rotate_bands] CHECK CONSTRAINT [fgn_key_FK_mobitels_rotate_bands_mobitels_Mobitel_ID]
GO
