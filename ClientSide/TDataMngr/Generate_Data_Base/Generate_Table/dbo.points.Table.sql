/****** Object:  Table [dbo].[points]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_points_zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[points]'))
ALTER TABLE [dbo].[points] DROP CONSTRAINT [fgn_key_FK_points_zones]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_points_zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[points]'))
ALTER TABLE [dbo].[points] DROP CONSTRAINT [fgn_key_FK_points_zones]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__points__ID__7A8729A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[points] DROP CONSTRAINT [DF__points__ID__7A8729A3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__points__Latitude__7B7B4DDC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[points] DROP CONSTRAINT [DF__points__Latitude__7B7B4DDC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__points__Longitud__7C6F7215]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[points] DROP CONSTRAINT [DF__points__Longitud__7C6F7215]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__points__Zone_ID__7D63964E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[points] DROP CONSTRAINT [DF__points__Zone_ID__7D63964E]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[points]') AND type in (N'U'))
DROP TABLE [dbo].[points]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[points]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[points](
	[Point_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NULL DEFAULT ((0)),
	[Longitude] [int] NULL DEFAULT ((0)),
	[Zone_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_points] PRIMARY KEY CLUSTERED 
(
	[Point_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[points]') AND name = N'FK_points_zones')
CREATE NONCLUSTERED INDEX [FK_points_zones] ON [dbo].[points] 
(
	[Zone_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_points_zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[points]'))
ALTER TABLE [dbo].[points]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_points_zones] FOREIGN KEY([Zone_ID])
REFERENCES [dbo].[zones] ([Zone_ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_points_zones]') AND parent_object_id = OBJECT_ID(N'[dbo].[points]'))
ALTER TABLE [dbo].[points] CHECK CONSTRAINT [fgn_key_FK_points_zones]
GO
