/****** Object:  Table [dbo].[smsnumbers]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__smsnumbers__ID__06B7F65E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[smsnumbers] DROP CONSTRAINT [DF__smsnumbers__ID__06B7F65E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__smsnumber__Flags__07AC1A97]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[smsnumbers] DROP CONSTRAINT [DF__smsnumber__Flags__07AC1A97]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[smsnumbers]') AND type in (N'U'))
DROP TABLE [dbo].[smsnumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[smsnumbers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[smsnumbers](
	[SmsNumber_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[smsNumber] [char](14) NULL,
 CONSTRAINT [PK_smsnumbers] PRIMARY KEY CLUSTERED 
(
	[SmsNumber_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
