/****** Object:  Table [dbo].[ntf_mobitels]    Script Date: 04/11/2013 11:22:26 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_mobitels_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]'))
ALTER TABLE [dbo].[ntf_mobitels] DROP CONSTRAINT [fgn_key_FK_ntf_mobitels_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_mobitels_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]'))
ALTER TABLE [dbo].[ntf_mobitels] DROP CONSTRAINT [fgn_key_FK_ntf_mobitels_ntf_main_Id]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_mobit__Senso__5CF6C6BC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_mobitels] DROP CONSTRAINT [DF__ntf_mobit__Senso__5CF6C6BC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ntf_mobit__Senso__5DEAEAF5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ntf_mobitels] DROP CONSTRAINT [DF__ntf_mobit__Senso__5DEAEAF5]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]') AND type in (N'U'))
DROP TABLE [dbo].[ntf_mobitels]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ntf_mobitels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[MobitelId] [int] NOT NULL,
	[SensorLogicId] [int] NULL DEFAULT ((0)),
	[SensorId] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_ntf_mobitels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]') AND name = N'FK_ntf_mobitels_ntf_main_Id')
CREATE NONCLUSTERED INDEX [FK_ntf_mobitels_ntf_main_Id] ON [dbo].[ntf_mobitels] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_mobitels_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]'))
ALTER TABLE [dbo].[ntf_mobitels]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_ntf_mobitels_ntf_main_Id] FOREIGN KEY([Id_main])
REFERENCES [dbo].[ntf_main] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_ntf_mobitels_ntf_main_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[ntf_mobitels]'))
ALTER TABLE [dbo].[ntf_mobitels] CHECK CONSTRAINT [fgn_key_FK_ntf_mobitels_ntf_main_Id]
GO
