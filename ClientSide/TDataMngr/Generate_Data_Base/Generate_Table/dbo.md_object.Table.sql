/****** Object:  Table [dbo].[md_object]    Script Date: 04/11/2013 11:22:25 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_md_object_group_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [fgn_key_FK_md_object_md_object_group_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_zones_Zone_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [fgn_key_FK_md_object_zones_Zone_ID]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_md_object_group_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [fgn_key_FK_md_object_md_object_group_Id]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_zones_Zone_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [fgn_key_FK_md_object_zones_Zone_ID]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object__Id_gr__77DFC722]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [DF__md_object__Id_gr__77DFC722]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object__Name__78D3EB5B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [DF__md_object__Name__78D3EB5B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object__Numbe__79C80F94]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [DF__md_object__Numbe__79C80F94]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object__Numbe__7ABC33CD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [DF__md_object__Numbe__7ABC33CD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_object__Delay__7BB05806]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_object] DROP CONSTRAINT [DF__md_object__Delay__7BB05806]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_object]') AND type in (N'U'))
DROP TABLE [dbo].[md_object]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_object]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_object](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_group] [int] NOT NULL DEFAULT ((0)),
	[Name] [varchar](50) NOT NULL DEFAULT (''),
	[Number] [int] NOT NULL DEFAULT ((0)),
	[NumberParent] [int] NOT NULL DEFAULT ((0)),
	[DelayTime] [decimal](5, 2) NOT NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
	[IdZone] [int] NULL,
 CONSTRAINT [PK_md_object] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_object]') AND name = N'FK_md_object_md_object_group_Id')
CREATE NONCLUSTERED INDEX [FK_md_object_md_object_group_Id] ON [dbo].[md_object] 
(
	[Id_group] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[md_object]') AND name = N'FK_md_object_zones_Zone_ID')
CREATE NONCLUSTERED INDEX [FK_md_object_zones_Zone_ID] ON [dbo].[md_object] 
(
	[IdZone] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_object', N'COLUMN',N'Id_group'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_object', @level2type=N'COLUMN',@level2name=N'Id_group'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_object', N'COLUMN',N'Name'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������������ �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_object', @level2type=N'COLUMN',@level2name=N'Name'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_object', N'COLUMN',N'Number'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� �������/����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_object', @level2type=N'COLUMN',@level2name=N'Number'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_object', N'COLUMN',N'NumberParent'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_object', @level2type=N'COLUMN',@level2name=N'NumberParent'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_object', N'COLUMN',N'DelayTime'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_object', @level2type=N'COLUMN',@level2name=N'DelayTime'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_md_object_group_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_object_md_object_group_Id] FOREIGN KEY([Id_group])
REFERENCES [dbo].[md_object_group] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_md_object_group_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] CHECK CONSTRAINT [fgn_key_FK_md_object_md_object_group_Id]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_zones_Zone_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_md_object_zones_Zone_ID] FOREIGN KEY([IdZone])
REFERENCES [dbo].[zones] ([Zone_ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_md_object_zones_Zone_ID]') AND parent_object_id = OBJECT_ID(N'[dbo].[md_object]'))
ALTER TABLE [dbo].[md_object] CHECK CONSTRAINT [fgn_key_FK_md_object_zones_Zone_ID]
GO
