/****** Object:  Table [dbo].[configzoneset]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configzonese__ID__55009F39]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configzoneset] DROP CONSTRAINT [DF__configzonese__ID__55009F39]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configzon__Flags__55F4C372]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configzoneset] DROP CONSTRAINT [DF__configzon__Flags__55F4C372]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__configzon__Messa__56E8E7AB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[configzoneset] DROP CONSTRAINT [DF__configzon__Messa__56E8E7AB]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configzoneset]') AND type in (N'U'))
DROP TABLE [dbo].[configzoneset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[configzoneset]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[configzoneset](
	[ConfigZoneSet_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Flags] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_configzoneset] PRIMARY KEY CLUSTERED 
(
	[ConfigZoneSet_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
