/****** Object:  Table [dbo].[rt_route_stops]    Script Date: 04/11/2013 11:22:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_stops_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_stops]'))
ALTER TABLE [dbo].[rt_route_stops] DROP CONSTRAINT [fgn_key_rt_route_stops_FK1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_stops_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_stops]'))
ALTER TABLE [dbo].[rt_route_stops] DROP CONSTRAINT [fgn_key_rt_route_stops_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_route___Check__284DF453]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_route_stops] DROP CONSTRAINT [DF__rt_route___Check__284DF453]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_stops]') AND type in (N'U'))
DROP TABLE [dbo].[rt_route_stops]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_stops]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_route_stops](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Location] [varchar](255) NULL,
	[InitialTime] [datetime] NULL,
	[FinalTime] [datetime] NULL,
	[Interval] [time](7) NULL,
	[CheckZone] [smallint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_rt_route_stops] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[rt_route_stops]') AND name = N'rt_route_stops_FK1')
CREATE NONCLUSTERED INDEX [rt_route_stops_FK1] ON [dbo].[rt_route_stops] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_route_stops', N'COLUMN',N'CheckZone'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� � ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_route_stops', @level2type=N'COLUMN',@level2name=N'CheckZone'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_stops_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_stops]'))
ALTER TABLE [dbo].[rt_route_stops]  WITH CHECK ADD  CONSTRAINT [fgn_key_rt_route_stops_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[rt_route] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_route_stops_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_route_stops]'))
ALTER TABLE [dbo].[rt_route_stops] CHECK CONSTRAINT [fgn_key_rt_route_stops_FK1]
GO
