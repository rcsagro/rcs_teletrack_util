/****** Object:  Table [dbo].[lines]    Script Date: 04/11/2013 11:22:24 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_lines_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[lines]'))
ALTER TABLE [dbo].[lines] DROP CONSTRAINT [fgn_key_lines_FK1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_lines_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[lines]'))
ALTER TABLE [dbo].[lines] DROP CONSTRAINT [fgn_key_lines_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lines__Color__351DDF8C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lines] DROP CONSTRAINT [DF__lines__Color__351DDF8C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__lines__Width__361203C5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[lines] DROP CONSTRAINT [DF__lines__Width__361203C5]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lines]') AND type in (N'U'))
DROP TABLE [dbo].[lines]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lines]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[lines](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Color] [int] NULL DEFAULT ((0)),
	[Width] [int] NULL DEFAULT ((3)),
	[Mobitel_ID] [int] NULL,
	[Icon] [varbinary](max) NULL,
 CONSTRAINT [PK_lines] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[lines]') AND name = N'lines_FK1')
CREATE NONCLUSTERED INDEX [lines_FK1] ON [dbo].[lines] 
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'lines', N'COLUMN',N'Color'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lines', @level2type=N'COLUMN',@level2name=N'Color'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'lines', N'COLUMN',N'Width'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lines', @level2type=N'COLUMN',@level2name=N'Width'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_lines_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[lines]'))
ALTER TABLE [dbo].[lines]  WITH CHECK ADD  CONSTRAINT [fgn_key_lines_FK1] FOREIGN KEY([Mobitel_ID])
REFERENCES [dbo].[mobitels] ([Mobitel_ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_lines_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[lines]'))
ALTER TABLE [dbo].[lines] CHECK CONSTRAINT [fgn_key_lines_FK1]
GO
