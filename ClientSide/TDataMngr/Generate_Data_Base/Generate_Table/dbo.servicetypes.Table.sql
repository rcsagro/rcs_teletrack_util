/****** Object:  Table [dbo].[servicetypes]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__servicety__Flags__6A1BB7B0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[servicetypes] DROP CONSTRAINT [DF__servicety__Flags__6A1BB7B0]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[servicetypes]') AND type in (N'U'))
DROP TABLE [dbo].[servicetypes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[servicetypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[servicetypes](
	[ServiceType_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[Flags] [int] NULL DEFAULT ((0)),
	[D1] [char](200) NULL,
	[D2] [char](200) NULL,
	[D3] [char](200) NULL,
	[D4] [char](200) NULL,
	[D5] [char](200) NULL,
	[D6] [char](200) NULL,
	[D7] [char](200) NULL,
	[D8] [char](200) NULL,
	[D9] [char](200) NULL,
	[D10] [char](200) NULL,
 CONSTRAINT [PK_servicetypes] PRIMARY KEY CLUSTERED 
(
	[ServiceType_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
