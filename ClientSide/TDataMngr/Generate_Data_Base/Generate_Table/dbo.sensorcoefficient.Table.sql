/****** Object:  Table [dbo].[sensorcoefficient]    Script Date: 04/11/2013 11:22:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensorcoeffic__K__515009E6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensorcoefficient] DROP CONSTRAINT [DF__sensorcoeffic__K__515009E6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__sensorcoeffic__b__52442E1F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sensorcoefficient] DROP CONSTRAINT [DF__sensorcoeffic__b__52442E1F]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensorcoefficient]') AND type in (N'U'))
DROP TABLE [dbo].[sensorcoefficient]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sensorcoefficient]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sensorcoefficient](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Sensor_id] [bigint] NOT NULL,
	[UserValue] [float] NOT NULL,
	[SensorValue] [float] NOT NULL,
	[K] [float] NOT NULL DEFAULT ((1.00000)),
	[b] [float] NOT NULL DEFAULT ((0.00000)),
 CONSTRAINT [PK_sensorcoefficient] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sensorcoefficient]') AND name = N'Index_2')
CREATE UNIQUE NONCLUSTERED INDEX [Index_2] ON [dbo].[sensorcoefficient] 
(
	[Sensor_id] ASC,
	[UserValue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
