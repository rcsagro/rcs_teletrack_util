/****** Object:  Table [dbo].[gpsmasks]    Script Date: 04/11/2013 11:22:24 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__ID__0662F0A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__ID__0662F0A3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Mobite__075714DC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Mobite__075714DC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Messag__084B3915]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Messag__084B3915]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__LastRe__093F5D4E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__LastRe__093F5D4E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__LastTi__0A338187]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__LastTi__0A338187]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__LastTi__0B27A5C0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__LastTi__0B27A5C0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__LastMe__0C1BC9F9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__LastMe__0C1BC9F9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__LastMe__0D0FEE32]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__LastMe__0D0FEE32]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__MaxSms__0E04126B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__MaxSms__0E04126B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__MaxSms__0EF836A4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__MaxSms__0EF836A4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Latitu__0FEC5ADD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Latitu__0FEC5ADD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Latitu__10E07F16]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Latitu__10E07F16]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Longit__11D4A34F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Longit__11D4A34F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Longit__12C8C788]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Longit__12C8C788]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Altitu__13BCEBC1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Altitu__13BCEBC1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Altitu__14B10FFA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Altitu__14B10FFA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__UnixTi__15A53433]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__UnixTi__15A53433]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__UnixTi__1699586C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__UnixTi__1699586C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__SpeedS__178D7CA5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__SpeedS__178D7CA5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__SpeedE__1881A0DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__SpeedE__1881A0DE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Direct__1975C517]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Direct__1975C517]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Direct__1A69E950]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Direct__1A69E950]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__1B5E0D89]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__1B5E0D89]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__1C5231C2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__1C5231C2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__WhatSe__1D4655FB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__WhatSe__1D4655FB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__CheckM__1E3A7A34]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__CheckM__1E3A7A34]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__InMobi__1F2E9E6D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__InMobi__1F2E9E6D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Events__2022C2A6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Events__2022C2A6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__2116E6DF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__2116E6DF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__220B0B18]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__220B0B18]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__22FF2F51]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__22FF2F51]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__23F3538A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__23F3538A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__gpsmasks__Number__24E777C3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gpsmasks] DROP CONSTRAINT [DF__gpsmasks__Number__24E777C3]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gpsmasks]') AND type in (N'U'))
DROP TABLE [dbo].[gpsmasks]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gpsmasks]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[gpsmasks](
	[GpsMask_ID] [int] IDENTITY(1,1) NOT NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Name] [char](200) NULL,
	[Descr] [char](200) NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[LastRecords] [int] NULL DEFAULT ((0)),
	[LastTimes] [int] NULL DEFAULT ((0)),
	[LastTimesIndex] [int] NULL DEFAULT ((0)),
	[LastMeters] [int] NULL DEFAULT ((0)),
	[LastMetersIndex] [int] NULL DEFAULT ((0)),
	[MaxSmsNum] [int] NULL DEFAULT ((0)),
	[MaxSmsNumIndex] [int] NULL DEFAULT ((0)),
	[LatitudeStart] [int] NULL DEFAULT ((0)),
	[LatitudeEnd] [int] NULL DEFAULT ((0)),
	[LongitudeStart] [int] NULL DEFAULT ((0)),
	[LongitudeEnd] [int] NULL DEFAULT ((0)),
	[AltitudeStart] [int] NULL DEFAULT ((0)),
	[AltitudeEnd] [int] NULL DEFAULT ((0)),
	[UnixTimeStart] [int] NULL DEFAULT ((0)),
	[UnixTimeEnd] [int] NULL DEFAULT ((0)),
	[SpeedStart] [int] NULL DEFAULT ((0)),
	[SpeedEnd] [int] NULL DEFAULT ((0)),
	[DirectionStart] [int] NULL DEFAULT ((0)),
	[DirectionEnd] [int] NULL DEFAULT ((0)),
	[NumbersStart] [int] NULL DEFAULT ((0)),
	[NumbersEnd] [int] NULL DEFAULT ((0)),
	[WhatSend] [int] NULL DEFAULT ((0)),
	[CheckMask] [int] NULL DEFAULT ((0)),
	[InMobitelID] [int] NULL DEFAULT ((0)),
	[Events] [int] NULL DEFAULT ((0)),
	[Number1] [int] NULL DEFAULT ((0)),
	[Number2] [int] NULL DEFAULT ((0)),
	[Number3] [int] NULL DEFAULT ((0)),
	[Number4] [int] NULL DEFAULT ((0)),
	[Number5] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_gpsmasks] PRIMARY KEY CLUSTERED 
(
	[GpsMask_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
