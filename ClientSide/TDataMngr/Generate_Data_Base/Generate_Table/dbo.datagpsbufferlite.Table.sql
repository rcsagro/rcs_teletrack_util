/****** Object:  Table [dbo].[datagpsbufferlite]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__5A846E65]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Mobit__5A846E65]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Messa__5B78929E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Messa__5B78929E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Latit__5C6CB6D7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Latit__5C6CB6D7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Longi__5D60DB10]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Longi__5D60DB10]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__UnixT__5E54FF49]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__UnixT__5E54FF49]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Speed__5F492382]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Speed__5F492382]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Direc__603D47BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Direc__603D47BB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Valid__61316BF4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Valid__61316BF4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Event__6225902D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Event__6225902D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__6319B466]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__6319B466]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__640DD89F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__640DD89F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__6501FCD8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__6501FCD8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__65F62111]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__65F62111]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__66EA454A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__66EA454A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__67DE6983]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__67DE6983]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__68D28DBC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__68D28DBC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__69C6B1F5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Senso__69C6B1F5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__6ABAD62E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__LogID__6ABAD62E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__isSho__6BAEFA67]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__isSho__6BAEFA67]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__whatI__6CA31EA0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__whatI__6CA31EA0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__6D9742D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Count__6D9742D9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__6E8B6712]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Count__6E8B6712]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__6F7F8B4B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Count__6F7F8B4B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__7073AF84]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbufferlite] DROP CONSTRAINT [DF__datagpsbu__Count__7073AF84]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbufferlite]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbufferlite]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbufferlite]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbufferlite](
	[DataGps_ID] [int] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NULL DEFAULT ((0)),
	[Longitude] [int] NULL DEFAULT ((0)),
	[Altitude] [int] NULL,
	[UnixTime] [int] NULL DEFAULT ((0)),
	[Speed] [smallint] NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL,
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [smallint] NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1)),
 CONSTRAINT [PK_datagpsbufferlite] PRIMARY KEY CLUSTERED 
(
	[DataGps_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
