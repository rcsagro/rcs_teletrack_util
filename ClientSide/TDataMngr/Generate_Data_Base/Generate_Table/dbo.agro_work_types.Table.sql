/****** Object:  Table [dbo].[agro_work_types]    Script Date: 04/11/2013 11:22:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__agro_work___Name__59FA5E80]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[agro_work_types] DROP CONSTRAINT [DF__agro_work___Name__59FA5E80]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_work_types]') AND type in (N'U'))
DROP TABLE [dbo].[agro_work_types]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[agro_work_types]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[agro_work_types](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_agro_work_types] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
