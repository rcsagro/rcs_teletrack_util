/****** Object:  Table [dbo].[rt_routet]    Script Date: 04/11/2013 11:22:27 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_routet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_routet]'))
ALTER TABLE [dbo].[rt_routet] DROP CONSTRAINT [fgn_key_rt_routet_FK1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_routet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_routet]'))
ALTER TABLE [dbo].[rt_routet] DROP CONSTRAINT [fgn_key_rt_routet_FK1]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_routet__Id_zo__2C1E8537]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_routet] DROP CONSTRAINT [DF__rt_routet__Id_zo__2C1E8537]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_routet__Id_ev__2D12A970]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_routet] DROP CONSTRAINT [DF__rt_routet__Id_ev__2D12A970]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__rt_routet__Dista__2E06CDA9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[rt_routet] DROP CONSTRAINT [DF__rt_routet__Dista__2E06CDA9]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_routet]') AND type in (N'U'))
DROP TABLE [dbo].[rt_routet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rt_routet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rt_routet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_main] [int] NOT NULL,
	[Id_zone] [int] NOT NULL DEFAULT ((0)),
	[Id_event] [int] NOT NULL DEFAULT ((1)),
	[DatePlan] [datetime] NULL,
	[DateFact] [datetime] NULL,
	[Deviation] [varchar](10) NULL,
	[Remark] [varchar](max) NULL,
	[Distance] [float] NULL DEFAULT ((0)),
	[Location] [varchar](255) NULL,
 CONSTRAINT [PK_rt_routet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[rt_routet]') AND name = N'rt_routet_FK1')
CREATE NONCLUSTERED INDEX [rt_routet_FK1] ON [dbo].[rt_routet] 
(
	[Id_main] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Id_zone'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Id_zone'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Id_event'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� - ����� / ����� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Id_event'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'DatePlan'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'DatePlan'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'DateFact'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'DateFact'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Deviation'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �� �������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Deviation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Remark'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'����������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Remark'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Distance'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����� ������� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Distance'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'rt_routet', N'COLUMN',N'Location'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���������� �����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rt_routet', @level2type=N'COLUMN',@level2name=N'Location'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_routet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_routet]'))
ALTER TABLE [dbo].[rt_routet]  WITH CHECK ADD  CONSTRAINT [fgn_key_rt_routet_FK1] FOREIGN KEY([Id_main])
REFERENCES [dbo].[rt_route] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_rt_routet_FK1]') AND parent_object_id = OBJECT_ID(N'[dbo].[rt_routet]'))
ALTER TABLE [dbo].[rt_routet] CHECK CONSTRAINT [fgn_key_rt_routet_FK1]
GO
