/****** Object:  Table [dbo].[zones]    Script Date: 04/11/2013 11:22:30 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_zones_zonesgroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[zones]'))
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [fgn_key_FK_zones_zonesgroup]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_zones_zonesgroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[zones]'))
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [fgn_key_FK_zones_zonesgroup]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__ID__5C8CB268]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__ID__5C8CB268]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__Status__5D80D6A1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__Status__5D80D6A1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__Square__5E74FADA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__Square__5E74FADA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__ZoneColor__5F691F13]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__ZoneColor__5F691F13]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__TextColor__605D434C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__TextColor__605D434C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__TextFontN__61516785]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__TextFontN__61516785]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__TextFontS__62458BBE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__TextFontS__62458BBE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__TextFontP__6339AFF7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__TextFontP__6339AFF7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__HatchStyl__642DD430]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__HatchStyl__642DD430]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__Id_main__6521F869]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__Id_main__6521F869]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__Level__66161CA2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__Level__66161CA2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__StyleId__670A40DB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__StyleId__670A40DB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__zones__ZonesGrou__67FE6514]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zones] DROP CONSTRAINT [DF__zones__ZonesGrou__67FE6514]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zones]') AND type in (N'U'))
DROP TABLE [dbo].[zones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zones](
	[Zone_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [char](50) NULL,
	[Descr] [char](200) NULL,
	[ID] [int] NULL DEFAULT ((0)),
	[Status] [smallint] NULL DEFAULT ((0)),
	[Square] [float] NULL DEFAULT ((0.00000)),
	[ZoneColor] [int] NULL DEFAULT ((0)),
	[TextColor] [int] NULL DEFAULT ((0)),
	[TextFontName] [char](50) NULL DEFAULT ('Microsoft Sans Serif'),
	[TextFontSize] [float] NULL DEFAULT ((25.00)),
	[TextFontParams] [char](8) NULL DEFAULT ('0;0;0;0;'),
	[Icon] [varbinary](max) NULL,
	[HatchStyle] [int] NULL DEFAULT ((0)),
	[DateCreate] [datetime] NULL,
	[Id_main] [int] NULL DEFAULT ((0)),
	[Level] [smallint] NULL DEFAULT ((1)),
	[StyleId] [bigint] NOT NULL DEFAULT ((0)),
	[ZonesGroupId] [bigint] NOT NULL DEFAULT ((1)),
	[OutLinkId] [varchar](20) NULL,
 CONSTRAINT [PK_zones] PRIMARY KEY CLUSTERED 
(
	[Zone_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[zones]') AND name = N'FK_zones_zonesgroup')
CREATE NONCLUSTERED INDEX [FK_zones_zonesgroup] ON [dbo].[zones] 
(
	[ZonesGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'Square'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'Square'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'ZoneColor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'ZoneColor'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'TextColor'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'TextColor'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'TextFontName'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'TextFontName'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'TextFontSize'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ������� ������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'TextFontSize'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'TextFontParams'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��������� ������� ������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'TextFontParams'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'Icon'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'Icon'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'HatchStyle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'�������� ����������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'HatchStyle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'DateCreate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'���� ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'DateCreate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'Id_main'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������ �� ������� �������� ������ � ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'Id_main'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'Level'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� � ��������' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'Level'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'StyleId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID ����� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'StyleId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'ZonesGroupId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID ������ ���' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'ZonesGroupId'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'zones', N'COLUMN',N'OutLinkId'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��� ������� ����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zones', @level2type=N'COLUMN',@level2name=N'OutLinkId'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_zones_zonesgroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[zones]'))
ALTER TABLE [dbo].[zones]  WITH CHECK ADD  CONSTRAINT [fgn_key_FK_zones_zonesgroup] FOREIGN KEY([ZonesGroupId])
REFERENCES [dbo].[zonesgroup] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fgn_key_FK_zones_zonesgroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[zones]'))
ALTER TABLE [dbo].[zones] CHECK CONSTRAINT [fgn_key_FK_zones_zonesgroup]
GO
