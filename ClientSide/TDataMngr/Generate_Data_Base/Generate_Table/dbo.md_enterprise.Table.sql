/****** Object:  Table [dbo].[md_enterprise]    Script Date: 04/11/2013 11:22:25 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_enterpr__Name__7132C993]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_enterprise] DROP CONSTRAINT [DF__md_enterpr__Name__7132C993]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_enterp__Curre__7226EDCC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_enterprise] DROP CONSTRAINT [DF__md_enterp__Curre__7226EDCC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__md_enterp__Dispa__731B1205]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[md_enterprise] DROP CONSTRAINT [DF__md_enterp__Dispa__731B1205]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_enterprise]') AND type in (N'U'))
DROP TABLE [dbo].[md_enterprise]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[md_enterprise]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[md_enterprise](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL DEFAULT (''),
	[Current] [smallint] NOT NULL DEFAULT ((0)),
	[Dispartchable] [smallint] NOT NULL DEFAULT ((0)),
	[Remark] [varchar](255) NULL,
 CONSTRAINT [PK_md_enterprise] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'md_enterprise', N'COLUMN',N'Current'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'������� ����������� ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'md_enterprise', @level2type=N'COLUMN',@level2name=N'Current'
GO
