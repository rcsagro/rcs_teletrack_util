/****** Object:  Table [dbo].[datagpsbuffer_dr]    Script Date: 04/11/2013 11:22:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Mobit__0A688BB1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Mobit__0A688BB1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Messa__0B5CAFEA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Messa__0B5CAFEA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Latit__0C50D423]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Latit__0C50D423]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Longi__0D44F85C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Longi__0D44F85C]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Altit__0E391C95]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Altit__0E391C95]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__UnixT__0F2D40CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__UnixT__0F2D40CE]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Speed__10216507]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Speed__10216507]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Direc__11158940]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Direc__11158940]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Valid__1209AD79]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Valid__1209AD79]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__InMob__12FDD1B2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__InMob__12FDD1B2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Event__13F1F5EB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Event__13F1F5EB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__14E61A24]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__14E61A24]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__15DA3E5D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__15DA3E5D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__16CE6296]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__16CE6296]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__17C286CF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__17C286CF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__18B6AB08]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__18B6AB08]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__19AACF41]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__19AACF41]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__1A9EF37A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__1A9EF37A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Senso__1B9317B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Senso__1B9317B3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__LogID__1C873BEC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__LogID__1C873BEC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__isSho__1D7B6025]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__isSho__1D7B6025]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__whatI__1E6F845E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__whatI__1E6F845E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__1F63A897]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Count__1F63A897]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__2057CCD0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Count__2057CCD0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__214BF109]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Count__214BF109]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__datagpsbu__Count__22401542]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[datagpsbuffer_dr] DROP CONSTRAINT [DF__datagpsbu__Count__22401542]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_dr]') AND type in (N'U'))
DROP TABLE [dbo].[datagpsbuffer_dr]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_dr]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer_dr](
	[Mobitel_ID] [int] NULL DEFAULT ((0)),
	[Message_ID] [int] NULL DEFAULT ((0)),
	[Latitude] [int] NOT NULL DEFAULT ((0)),
	[Longitude] [int] NOT NULL DEFAULT ((0)),
	[Altitude] [int] NULL DEFAULT ((0)),
	[UnixTime] [int] NOT NULL DEFAULT ((0)),
	[Speed] [smallint] NOT NULL DEFAULT ((0)),
	[Direction] [int] NOT NULL DEFAULT ((0)),
	[Valid] [smallint] NOT NULL DEFAULT ((1)),
	[InMobitelID] [int] NULL DEFAULT ((0)),
	[Events] [bigint] NULL DEFAULT ((0)),
	[Sensor1] [tinyint] NULL DEFAULT ((0)),
	[Sensor2] [tinyint] NULL DEFAULT ((0)),
	[Sensor3] [tinyint] NULL DEFAULT ((0)),
	[Sensor4] [tinyint] NULL DEFAULT ((0)),
	[Sensor5] [tinyint] NULL DEFAULT ((0)),
	[Sensor6] [tinyint] NULL DEFAULT ((0)),
	[Sensor7] [tinyint] NULL DEFAULT ((0)),
	[Sensor8] [tinyint] NULL DEFAULT ((0)),
	[LogID] [int] NULL DEFAULT ((0)),
	[isShow] [smallint] NULL DEFAULT ((0)),
	[whatIs] [int] NOT NULL DEFAULT ((0)),
	[Counter1] [int] NOT NULL DEFAULT ((-1)),
	[Counter2] [int] NOT NULL DEFAULT ((-1)),
	[Counter3] [int] NOT NULL DEFAULT ((-1)),
	[Counter4] [int] NOT NULL DEFAULT ((-1))
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_dr]') AND name = N'IDX_MobitelIDLogID')
CREATE NONCLUSTERED INDEX [IDX_MobitelIDLogID] ON [dbo].[datagpsbuffer_dr] 
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_dr]') AND name = N'IDX_MobitelidUnixtime')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtime] ON [dbo].[datagpsbuffer_dr] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_dr]') AND name = N'IDX_MobitelidUnixtimeValid')
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtimeValid] ON [dbo].[datagpsbuffer_dr] 
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC,
	[Valid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
