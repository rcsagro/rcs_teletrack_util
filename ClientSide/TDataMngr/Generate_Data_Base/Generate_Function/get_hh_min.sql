CREATE FUNCTION [dbo].[get_hh_min]
(
  @minute FLOAT
)
RETURNS TIME
AS
BEGIN
  DECLARE @hours INT;
  SET @hours = @minute / 60.0;
  DECLARE @mn INT;
  SET @mn = @minute - @hours * 60;
  DECLARE @tm VARCHAR(5);
  SET @tm = convert(VARCHAR(2), @hours) + ':' + convert(VARCHAR(2), @mn);
  DECLARE @times TIME;
  SET @times = convert(TIME, @tm);

  RETURN @times;
END;

