IF OBJECT_ID (N'dbo.UNIX_TIMESTAMPS', N'FN') IS NOT NULL
    DROP FUNCTION dbo.UNIX_TIMESTAMPS;
    GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION dbo.UNIX_TIMESTAMPS
(
  @ctimestamp DATETIME
)
RETURNS INTEGER
AS
BEGIN
  /* Function body */
  DECLARE @return INTEGER
  SELECT @return = datediff(SECOND, { D '1970-01-01' }, @ctimestamp)
  RETURN @return
END

GO