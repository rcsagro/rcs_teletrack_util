IF OBJECT_ID (N'dbo.From_UnixTime', N'FN') IS NOT NULL
    DROP FUNCTION dbo.From_UnixTime;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION dbo.From_UnixTime
(
  @timestamp INTEGER
)
RETURNS DATETIME
AS
BEGIN
  /* Function body */
  DECLARE @return DATETIME
  SELECT @return = dateadd(SECOND, @timestamp, { D '1970-01-01' });
  RETURN @return
END
GO