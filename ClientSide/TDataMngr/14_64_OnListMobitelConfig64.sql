﻿CREATE DEFINER = 'user'@'%'
PROCEDURE vesnyane_agro.OnListMobitelConfig64()
  COMMENT 'Выборка настроек телетреков которые могут работать в online режиме для datagps и datagps64'
BEGIN
 DECLARE MobitelIDInCursor INT;
  DECLARE Is64Cursor tinyint;
  DECLARE DevIdShortInCursor CHAR(4);
  DECLARE DataExists TINYINT DEFAULT 1;

  /* Курсор в множестве настроенных телетреков */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS MobitelID, m.Is64bitPackets AS Is64, c.devIdShort AS DevIdShort
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID)
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND
      (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')
    ORDER BY m.Mobitel_ID;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* Временная таблица настроек телетрека */
  CREATE TEMPORARY TABLE tmpMobitelsConfig (
    MobitelID INT NOT NULL,
    Is64 tinyint NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL,
    LastPacketID64 BIGINT NOT NULL) ENGINE = MEMORY;

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor, Is64Cursor, DevIdShortInCursor;
  WHILE DataExists = 1 DO

    INSERT INTO tmpMobitelsConfig SELECT MobitelIDInCursor, Is64Cursor, DevIdShortInCursor,
      (SELECT COALESCE(MAX(d.SrvPacketID), 0)
       FROM datagps d
       WHERE d.Mobitel_ID = MobitelIDInCursor) AS LastPacketID,
      (SELECT COALESCE(MAX(t.SrvPacketID), 0)
       FROM datagps64 t
       WHERE t.Mobitel_ID = MobitelIDInCursor) AS LastPacketID64;

    FETCH CursorMobitels INTO MobitelIDInCursor, Is64Cursor, DevIdShortInCursor;
  END WHILE;
  CLOSE CursorMobitels;

  SELECT MobitelID, Is64, DevIdShort, LastPacketID, LastPacketID64
  FROM tmpMobitelsConfig;

  DROP TEMPORARY TABLE tmpMobitelsConfig;
END