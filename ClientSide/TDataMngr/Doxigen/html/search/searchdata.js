var indexSectionsWithContent =
{
  0: "abcdefghilmnopqrstuvwﮮ",
  1: "acdhilmopst",
  2: "r",
  3: "acdhilmopst",
  4: "acdfghiloprstvw",
  5: "abcdehilmoprstuw",
  6: "t",
  7: "aiqrs",
  8: "alns",
  9: "epﮮ"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Events"
};

