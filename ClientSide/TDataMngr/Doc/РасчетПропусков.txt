��� ������� ��������� ������������ ���� �� ���� ��� ��� ��������� � 
����������� �� ������� ������ ���������� � �����:
  - 1-� �������� ������������ ���� ��������� ����� ������ 
    (�������� �������� OnLostDataGPS);
  - 2-�� �������� ������������ ���� ������ ������� 
    (�������� ��������� OnLostDataGPS2).
������� ������ ��������� ����������� ��������� (OnLostDataGPS2) ����������� 
���, ��� ������ ���������� ��������� �������������� �� ���� LogId �������� 
DataGps, � � ��������� ������� ���������� ����������� DataGps � ������� 
����������� ��������������� ��������� � �������� ��������� �������� Id ������.
������ ���������� ��������� ��������: � ��� ���� ������������������ �������,
� �� ������� � ���� ��������� ID ����� ��������� ����� DataGps. 
���� �� �� ����� ������������ ������ OnLostDataGPS2, �� �������� ������ 
�� ������� ������� (������ �������) � ���� ������� ����� ��������� 
������������� � ������� � �� ����� "������". ������ �������� ��� ��� 
� ����������� ��������� Id �������, ��� ����� �������� "������������".


I. ����������:

n - ID ��������� (Mobitel_ID)          
LogID_n - �������� ���� LogID ������ � ��������� ����� ������ ��������� n.
LogID - �������� ���� LogID ������ � ������� DataGPS ��������� n.
ConfirmedID - �������� ���� ConfirmedID ������ � ������� mobitels,
  ������������ ����� ������� (LogID) ��� ������� ��������� ��� ��������� n.

MinLogID_n = MIN(LogID) from datagpsbuffer_on
  WHERE (Mobitel_ID = n) AND (LogID > ConfirmedID)- 
  ����������� LogID ����� ConfirmedID � ��������� ����� ������.
  ������������ ������ �� ConfirmedID ��� ������, �.� ��� ��� ���������.
MaxLogID_n = MAX(LogID) FROM datagpsbuffer_on WHERE Mobitel_ID = n - 
  ������������ LogID � ��������� ����� ������ ��������� ���������.  

StartLogIDSelect - �������� ���������� LogID � ������� DataGps ������������
  ��� ��������� ���������.
FinishLogIDSelect - �������� ��������� LogID � ������� DataGps ������������ 
  ��� ��������� ���������.


II. �������, ������� ������ �����������:

1. ConfirmedID <= SELECT MAX(LogID) FROM DataGps WHERE Mobitel_ID = n.
2. ConfirmedID < MinLogID_n. 

���������� ��������� ���������� � "����� ������"(datagpslost_on), 
� max �������� LogID, �.�. � ������ ������� ���� ���������� ��������� 
������. � ����� � ���� �������� ConfirmedID ��������� ������ �����,
����� ����� ��������� ��� ��������� ���������	�	� ������� datagpslost_on
� �� ��������� �� ����� ������, ����������� � ������� ���������. 
�������� ConfirmedID ������ ������ ������������� �������� LogId ��� 
������� ���������:
  ConfirmedID = SELECT MAX(LogID) FROM DataGps WHERE Mobitel_ID = n.
�������� ConfirmedID ��� online ������� ������������ ���
�������������� �������� ����������, �������� ��� ��������� ���������.


III. �������� ������� � ������ ������� ����� ������ (�� OnLostDataGPS):

1. ��������� ����� ������ � ���� (PROCEDURE OnTransferBuffer).

2. ������� MinLogID_n = MIN(LogID)from datagpbuffer_on 
  WHERE (Mobitel_ID = n) AND (LogID > ConfirmedID).

3. ������� MaxLogID_n = MAX(LogID) FROM datagpsbuffer_on WHERE Mobitel_ID = n.

4. ���� MinLogID_n < MAX(End_LogID) FROM datagpslost_on WHERE Mobitel_ID = n
   ������ ������ ������, ������� �������� ������� ����� ���������.
  4.1. TRUE
    4.1.1. ������� ��������� LogID ��� ���������. ��� ���������� �����������
      ��������� ������ ������� �������� � ������������ ��� ��������
      StartLogIDSelect =
        SELECT COALESCE(Begin_LogID, MinLogID_n)
        FROM datagpslost_on
        WHERE (Mobitel_ID = n) AND (MinLogID_n > Begin_LogID)
          AND (MinLogID_n < End_LogID).
    4.1.2 ������� ��������� LogID ��� ���������. ��� ���������� �����������
      ��������� ������ ������� �������� � ������������ ��� ��������
      FinishLogIDSelect = 
        SELECT COALESCE(End_LogID, MaxLogID_n)
        FROM datagpslost_on
        WHERE (Mobitel_ID = n) AND (MaxLogID_n > Begin_LogID)
          AND (MaxLogID_n < End_LogID).
    4.1.3. ������� �� ������� DataGpsLost_on ������ � ��������
      WHERE (Mobitel_ID = n) AND (Begin_LogID >= StartLogIDSelect)
        AND (Begin_LogID <= FinishLogIDSelect).
  4.2. FALSE
    4.2.1. ��� ����������� ��������� ����� ������ � ���������� � datagpslost_on,
      ������� ���� �������� ��� ������� ������������� ������ - ������ �������.
    4.2.2. ������� StartLogIDSelect =
      SELECT COALESCE(MAX(End_LogID), ConfirmedID)
      FROM datagpslost_on
      WHERE Mobitel_ID = n.
    4.2.3. FinishLogIDSelect = 
      SELECT MAX(LogID)
      FROM datagpsbuffer_on
      WHERE Mobitel_ID = n.
    4.2.4. �������� �� datagps ��������������� �������� SrvPacketID ��� ������
      ������������� ������ tmpSrvPacketID =
        SELECT COALESCE(SrvPacketID, 0)
        FROM datagps
        WHERE (Mobitel_ID = n) AND (LogID = StartLogIDSelect).
  4.2.5. �������� � ������� datagpslost_ontmp �������������� ������:
    INSERT INTO datagpslost_ontmp(LogID, SrvPacketID)
    VALUES (StartLogIDSelect, tmpSrvPacketID).

5. ������ ������� �� DataGps �� ��������� ������� datagpslost_ontmp
  WHERE (Mobitel_ID = n) AND (LogID >= StartLogIDSelect) 
    AND (LogID <= FinishLogIDSelect).

6. ���� ������� � datagpslost_ontmp � ��������� ���������� � ��������
  � ������� datagpslost_on.  
   
7. ���� ����� ���� ��������� � datagpslost_on �� �������� �� ����� ������,
  ���� �������� �������� ConfirmedID = 
    SELECT MAX(LogID) FROM DataGps WHERE Mobitel_ID = n.


IV. �������� ������� � ������ ���������� ����� ������ (�� OnLostDataGPS2):

1. ���������� ��������� ��������� ��������� ��������� � ������
  ������� �� ������� datagpsbuffer_ontmp 
  WHERE (SrvPacketID > datagpslost_on.Begin_SrvPacketID) 
    AND (SrvPacketID < datagpslost_on.End_SrvPacketID)
   
2. ���� ������ ����, ����� ���������� ������ 1, ����� ������������� ���������
  � datagpslost_on 