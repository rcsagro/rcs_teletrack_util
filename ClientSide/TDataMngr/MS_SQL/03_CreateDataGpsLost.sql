IF EXISTS (SELECT * FROM sys.objects WHERE name = 'datagpslost_on')
    DROP TABLE datagpslost_on
GO

CREATE TABLE dbo.datagpslost_on(
  Mobitel_ID INT NOT NULL DEFAULT (0),
  Begin_LogID INT NOT NULL DEFAULT (0),
  End_LogID INT NOT NULL DEFAULT (0),
  Begin_SrvPacketID BIGINT NOT NULL DEFAULT (0),
  End_SrvPacketID BIGINT NOT NULL DEFAULT (0),
  CONSTRAINT PK_datagpslost_on PRIMARY KEY (Mobitel_ID, Begin_LogID)
) ON [PRIMARY]
GO

EXEC sp_addextendedproperty N'MS_Description', '�������� LogID ����� �������� ���������� ������', 'SCHEMA', N'dbo', 'TABLE', N'datagpslost_on', 'COLUMN', N'Begin_LogID'
GO

EXEC sp_addextendedproperty N'MS_Description', '�������� LogID ����� ������� ����������� ������', 'SCHEMA', N'dbo', 'TABLE', N'datagpslost_on', 'COLUMN', N'End_LogID'
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'datagpslost_ontmp')
    DROP TABLE datagpslost_ontmp
GO

CREATE TABLE dbo.datagpslost_ontmp(
  LogID INT NOT NULL DEFAULT (0),
  SrvPacketID BIGINT NOT NULL
) ON [PRIMARY]
GO
