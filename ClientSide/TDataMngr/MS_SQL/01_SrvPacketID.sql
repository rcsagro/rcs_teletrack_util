ALTER TABLE dbo.datagps ADD SrvPacketID BIGINT NOT NULL DEFAULT '0';
  --COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������';

UPDATE datagps
SET
  SrvPacketID = 0
WHERE
  SrvPacketID IS NULL;
  
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IDX_MobitelIDSrvPacketID')
  DROP INDEX IDX_MobitelIDSrvPacketID ON dbo.datagps;

CREATE INDEX IDX_MobitelIDSrvPacketID ON dbo.datagps (Mobitel_ID, SrvPacketID);

