IF EXISTS (SELECT * FROM sys.objects WHERE name = 'tmpAddIndex')
    DROP TABLE dbo.tmpAddIndex
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.tmpAddIndex
AS
--NOT DETERMINISTIC
--CONTAINS SQL
--SQL SECURITY INVOKER
--COMMENT '��������� ��������� ���������� ������� � DataGpsBuffer_on'
BEGIN
  DECLARE @DbName VARCHAR(64); /* ������������ ������� �� */
  DECLARE @IndexExist TINYINT;
  SET @IndexExist = 0;

  SET @DbName = (SELECT db_name(@@procid));

  SET @IndexExist = (SELECT count(1)
                     FROM
                       sys.indexes i
                     WHERE
                       i.name = 'IDX_SrvPacketID');

  IF @IndexExist = 0
  BEGIN
    CREATE INDEX IDX_SrvPacketID ON DataGpsBuffer_on (SrvPacketID);
    ALTER TABLE DataGpsBuffer_on ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON);
  END;
END

--EXEC tmpAddIndex
--DROP PROCEDURE IF EXISTS tmpAddIndex$$


GO