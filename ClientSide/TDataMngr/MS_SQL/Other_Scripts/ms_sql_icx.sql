﻿-- MS SQL Administrator 2008 R2
--
-- ------------------------------------------------------
-- Server version	2008 R2
--
-- Create schema mca_dispatcher
--

CREATE DATABASE IF NOT EXISTS mca_dispatcher;
USE mca_dispatcher;

--
-- Temporary table structure for view `datavalue1`
--
DROP TABLE IF EXISTS `datavalue1`;
DROP VIEW IF EXISTS `datavalue1`;
CREATE TABLE `datavalue1` (
  `lon` decimal(16,6),
  `lat` decimal(16,6),
  `Mobitel_ID` int(11),
  `Value` double(10,2),
  `speed` decimal(9,3),
  `time` datetime,
  `sensor_id` int(10) unsigned,
  `datavalue_id` int(11)
);

--
-- Temporary table structure for view `dataview`
--
DROP TABLE IF EXISTS `dataview`;
DROP VIEW IF EXISTS `dataview`;
CREATE TABLE `dataview` (
  `DataGps_ID` int(11),
  `Mobitel_ID` int(11),
  `Lat` decimal(14,4),
  `Lon` decimal(14,4),
  `Altitude` int(11),
  `time` datetime,
  `speed` decimal(9,3),
  `direction` decimal(17,4),
  `Valid` tinyint(4),
  `sensor` bigint(28) unsigned,
  `Events` int(10) unsigned,
  `LogID` int(11)
);

--
-- Temporary table structure for view `deviceview`
--
DROP TABLE IF EXISTS `deviceview`;
DROP VIEW IF EXISTS `deviceview`;
CREATE TABLE `deviceview` (
  `Mobitel_ID` int(11),
  `Name` char(50),
  `Descr` char(200),
  `devIdShort` char(4),
  `DeviceType` int(11),
  `ConfirmedID` int(11),
  `moduleIdRf` char(50)
);

--
-- Temporary table structure for view `internalmobitelconfigview`
--
DROP TABLE IF EXISTS `internalmobitelconfigview`;
DROP VIEW IF EXISTS `internalmobitelconfigview`;
CREATE TABLE `internalmobitelconfigview` (
  `ID` int(11),
  `LastRecord` int(11)
);

--
-- Definition of table `commands`
--

DROP TABLE IF EXISTS `commands`;
CREATE TABLE `commands` (
  `ID` int(11) default '0',
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `commands`
--

/*!40000 ALTER TABLE `commands` DISABLE KEYS */;
INSERT INTO `commands` (`ID`,`Name`,`Descr`) VALUES 
 (10,'DRIVER_MESSAGE_SET_D',NULL),
 (11,'CONFIG_SMS_SET_D',NULL),
 (12,'CONFIG_TEL_SET_D',NULL),
 (13,'CONFIG_EVENT_SET_D',NULL),
 (14,'CONFIG_UNIQUE_SET_D',NULL),
 (15,'CONFIG_ZONE_SET_D',NULL),
 (17,'CONFIG_ID_SET_D',NULL),
 (18,'CONFIG_OTHER_SET_D',NULL),
 (20,'DRIVER_MESSAGE_SET_M',NULL),
 (21,'CONFIG_SMS_SET_M',NULL),
 (22,'CONFIG_TEL_SET_M',NULL),
 (23,'CONFIG_EVENT_SET_M',NULL),
 (24,'CONFIG_UNIQUE_SET_M',NULL),
 (25,'CONFIG_ZONE_SET_M',NULL),
 (27,'CONFIG_ID_SET_M',NULL),
 (28,'CONFIG_OTHER_SET_M',NULL),
 (31,'CONFIG_SMS_GET_D',NULL),
 (32,'CONFIG_TEL_GET_D',NULL),
 (33,'CONFIG_EVENT_GET_D',NULL),
 (34,'CONFIG_UNIQUE_GET_D',NULL),
 (35,'CONFIG_ZONE_GET_D',NULL),
 (36,'DATA_GPS_GET_D',NULL),
 (37,'CONFIG_ID_GET_D',NULL),
 (38,'CONFIG_OTHER_GET_D',NULL),
 (41,'CONFIG_SMS_GET_M',NULL),
 (42,'CONFIG_TEL_GET_M',NULL),
 (43,'CONFIG_EVENT_GET_M',NULL),
 (44,'CONFIG_UNIQUE_GET_M',NULL),
 (45,'CONFIG_ZONE_GET_M',NULL),
 (46,'DATA_GPS_GET_M',NULL),
 (47,'CONFIG_ID_GET_M',NULL),
 (48,'CONFIG_OTHER_GET_M',NULL),
 (49,'AUTO_SEND_GET_M',NULL),
 (99,'ERROR_COMMAND','Невозможно раскодировать входящее сообщение'),
 (102,'MOBITEL_SERVICE_SEND',NULL),
 (103,'INTERNAL_1',NULL),
 (104,'INTERNAL_2',NULL),
 (105,'INTERNAL_3',NULL),
 (50,'CONFIG_GPRS_MAIN_SET_D',NULL),
 (51,'CONFIG_GPRS_EMAIL_SET_D',NULL),
 (52,'CONFIG_GPRS_SOCKET_SET_D',NULL),
 (53,'CONFIG_GPRS_FTP_SET_D',NULL),
 (60,'CONFIG_GPRS_MAIN_SET_M',NULL),
 (61,'CONFIG_GPRS_EMAIL_SET_M',NULL),
 (62,'CONFIG_GPRS_SOCKET_SET_M',NULL),
 (63,'CONFIG_GPRS_FTP_SET_M',NULL),
 (70,'CONFIG_GPRS_MAIN_GET_D',NULL),
 (71,'CONFIG_GPRS_EMAIL_GET_D',NULL),
 (72,'CONFIG_GPRS_SOCKET_GET_D',NULL),
 (73,'CONFIG_GPRS_FTP_GET_D',NULL),
 (80,'CONFIG_GPRS_MAIN_GET_M',NULL),
 (81,'CONFIG_GPRS_EMAIL_GET_M',NULL),
 (82,'CONFIG_GPRS_SOCKET_GET_M',NULL),
 (83,'CONFIG_GPRS_FTP_GET_M',NULL),
 (54,'CONFIG_GPRS_INIT_SET_D',NULL),
 (64,'CONFIG_GPRS_INIT_SET_M',NULL),
 (74,'CONFIG_GPRS_INIT_GET_D',NULL),
 (84,'CONFIG_GPRS_INIT_GET_M',NULL);
/*!40000 ALTER TABLE `commands` ENABLE KEYS */;


--
-- Definition of table `configdrivermessage`
--

DROP TABLE IF EXISTS `configdrivermessage`;
CREATE TABLE `configdrivermessage` (
  `ConfigDriverMessage_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `Message_ID` int(11) default '0',
  `DriverMessage` char(200) default NULL,
  PRIMARY KEY  (`ConfigDriverMessage_ID`),
  KEY `Index_2` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configdrivermessage`
--

/*!40000 ALTER TABLE `configdrivermessage` DISABLE KEYS */;
INSERT INTO `configdrivermessage` (`ConfigDriverMessage_ID`,`Name`,`Descr`,`ID`,`Flags`,`Message_ID`,`DriverMessage`) VALUES 
 (216,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',216,0,0,NULL);
/*!40000 ALTER TABLE `configdrivermessage` ENABLE KEYS */;


--
-- Definition of table `configevent`
--

DROP TABLE IF EXISTS `configevent`;
CREATE TABLE `configevent` (
  `ConfigEvent_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `Message_ID` int(11) default '0',
  `tmr1Log` int(11) default '0',
  `tmr2Send` int(11) default '0',
  `tmr3Zone` int(11) default '0',
  `dist1Log` int(11) default '0',
  `dist2Send` int(11) default '0',
  `dist3Zone` int(11) default '0',
  `deltaTimeZone` int(11) default '0',
  `gprsEmail` int(11) NOT NULL default '0',
  `gprsFtp` int(11) NOT NULL default '0',
  `gprsSocket` int(11) NOT NULL default '0',
  `maskSensor` char(50) default NULL,
  `maskEvent1` int(11) default '0',
  `maskEvent2` int(11) default '0',
  `maskEvent3` int(11) default '0',
  `maskEvent4` int(11) default '0',
  `maskEvent5` int(11) default '0',
  `maskEvent6` int(11) default '0',
  `maskEvent7` int(11) default '0',
  `maskEvent8` int(11) default '0',
  `maskEvent9` int(11) default '0',
  `maskEvent10` int(11) default '0',
  `maskEvent11` int(11) default '0',
  `maskEvent12` int(11) default '0',
  `maskEvent13` int(11) default '0',
  `maskEvent14` int(11) default '0',
  `maskEvent15` int(11) default '0',
  `maskEvent16` int(11) default '0',
  `maskEvent17` int(11) default '0',
  `maskEvent18` int(11) default '0',
  `maskEvent19` int(11) default '0',
  `maskEvent20` int(11) default '0',
  `maskEvent21` int(11) default '0',
  `maskEvent22` int(11) default '0',
  `maskEvent23` int(11) default '0',
  `maskEvent24` int(11) default '0',
  `maskEvent25` int(11) default '0',
  `maskEvent26` int(11) default '0',
  `maskEvent27` int(11) default '0',
  `maskEvent28` int(11) default '0',
  `maskEvent29` int(11) default '0',
  `maskEvent30` int(11) default '0',
  `maskEvent31` int(11) default '0',
  PRIMARY KEY  (`ConfigEvent_ID`),
  KEY `Index_2` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1793 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configevent`
--

/*!40000 ALTER TABLE `configevent` DISABLE KEYS */;
INSERT INTO `configevent` (`ConfigEvent_ID`,`Name`,`Descr`,`ID`,`Flags`,`Message_ID`,`tmr1Log`,`tmr2Send`,`tmr3Zone`,`dist1Log`,`dist2Send`,`dist3Zone`,`deltaTimeZone`,`gprsEmail`,`gprsFtp`,`gprsSocket`,`maskSensor`,`maskEvent1`,`maskEvent2`,`maskEvent3`,`maskEvent4`,`maskEvent5`,`maskEvent6`,`maskEvent7`,`maskEvent8`,`maskEvent9`,`maskEvent10`,`maskEvent11`,`maskEvent12`,`maskEvent13`,`maskEvent14`,`maskEvent15`,`maskEvent16`,`maskEvent17`,`maskEvent18`,`maskEvent19`,`maskEvent20`,`maskEvent21`,`maskEvent22`,`maskEvent23`,`maskEvent24`,`maskEvent25`,`maskEvent26`,`maskEvent27`,`maskEvent28`,`maskEvent29`,`maskEvent30`,`maskEvent31`) VALUES 
 (1785,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',1785,0,0,0,0,0,0,0,0,0,0,0,0,NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
 (1786,'Default','Настройки по умолчанию',1785,0,200799,3,120,600,100,1000,5000,2,162,104,225,'',1,1,1,1,1,1,1,1,1,1,1,1,25,25,25,25,273,273,1,1,1,1,1,1,0,0,0,0,0,0,0),
 (1787,'Default','Настройки по умолчанию',1785,0,200800,0,3600,300,30,5000,5000,3,1,104,225,'',1,1,1,1,1,1,0,1,1,1,1,1,25,25,25,25,273,273,1,0,0,0,1,1,0,0,0,0,0,0,0),
 (1788,'Default','Настройки по умолчанию',1785,0,200804,0,3600,300,30,5000,5000,3,1,104,225,'',1,5,5,1,5,1,0,1,1,1,1,0,25,25,25,25,273,273,0,0,0,0,5,1,0,0,0,0,0,0,0),
 (1789,'Default','Настройки по умолчанию',1785,0,200805,0,3600,300,30,5000,5000,3,1,104,225,'',1,5,5,1,5,1,0,1,1,1,1,0,25,25,25,25,273,273,0,0,0,0,5,1,0,0,0,0,0,0,0),
 (1790,'','',1785,0,200806,0,3600,300,30,5000,5000,3,1,10,10,'----',1,5,5,1,5,1,0,1,1,1,1,0,25,25,25,25,273,273,0,0,0,0,5,0,0,0,0,0,0,0,0),
 (1791,'Default','Настройки по умолчанию',1785,0,200968,0,3600,300,30,5000,5000,3,1,104,225,'',1,5,0,1,5,1,0,1,1,1,1,0,25,25,25,25,273,273,0,0,0,0,5,1,0,0,0,0,0,0,0),
 (1792,'','',1785,0,200969,0,3600,300,30,5000,5000,3,1,10,10,'----',1,5,0,1,5,1,0,1,1,1,1,0,25,25,25,25,273,273,0,0,0,0,5,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `configevent` ENABLE KEYS */;


--
-- Definition of table `configgprsemail`
--

DROP TABLE IF EXISTS `configgprsemail`;
CREATE TABLE `configgprsemail` (
  `ConfigGprsEmail_ID` int(11) NOT NULL auto_increment,
  `Name` varchar(50) default NULL,
  `Descr` varchar(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `Message_ID` int(11) default '0',
  `Smtpserv` varchar(50) NOT NULL default '',
  `Smtpun` varchar(50) NOT NULL default '',
  `Smtppw` varchar(50) NOT NULL default '',
  `Pop3serv` varchar(50) NOT NULL default '',
  `Pop3un` varchar(50) NOT NULL default '',
  `Pop3pw` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`ConfigGprsEmail_ID`),
  KEY `Index_2` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=353 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configgprsemail`
--

/*!40000 ALTER TABLE `configgprsemail` DISABLE KEYS */;
INSERT INTO `configgprsemail` (`ConfigGprsEmail_ID`,`Name`,`Descr`,`ID`,`Flags`,`Message_ID`,`Smtpserv`,`Smtpun`,`Smtppw`,`Pop3serv`,`Pop3un`,`Pop3pw`) VALUES 
 (341,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',341,0,0,'','','','','',''),
 (342,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',341,0,200887,'smtp.p314.net','','','pop.p314.net','3051@','1%NDzZnt'),
 (343,'','',341,0,200888,'smtp.p314.net','','','pop.p314.net','3051@','1%NDzZnt'),
 (352,'','',341,0,218876,'','','','','','');
/*!40000 ALTER TABLE `configgprsemail` ENABLE KEYS */;


--
-- Definition of table `configgprsinit`
--

DROP TABLE IF EXISTS `configgprsinit`;
CREATE TABLE `configgprsinit` (
  `ConfigGprsInit_ID` int(11) NOT NULL auto_increment,
  `Name` varchar(50) default NULL,
  `Descr` varchar(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `Message_ID` int(11) default '0',
  `Domain` varchar(50) NOT NULL default '0',
  `InitString` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`ConfigGprsInit_ID`),
  KEY `Index_2` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configgprsinit`
--

/*!40000 ALTER TABLE `configgprsinit` DISABLE KEYS */;
INSERT INTO `configgprsinit` (`ConfigGprsInit_ID`,`Name`,`Descr`,`ID`,`Flags`,`Message_ID`,`Domain`,`InitString`) VALUES 
 (299,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',299,0,0,'0',''),
 (300,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',299,0,200889,'smtp.p314.net',''),
 (301,'','',299,0,200890,'smtp.p314.net',''),
 (310,'','',299,0,218877,'','');
/*!40000 ALTER TABLE `configgprsinit` ENABLE KEYS */;


--
-- Definition of table `configgprsmain`
--

DROP TABLE IF EXISTS `configgprsmain`;
CREATE TABLE `configgprsmain` (
  `ConfigGprsMain_ID` int(11) NOT NULL auto_increment,
  `Name` varchar(50) default NULL,
  `Descr` varchar(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `Message_ID` int(11) default '0',
  `Mode` int(11) NOT NULL default '0',
  `Apnserv` varchar(50) NOT NULL default '',
  `Apnun` varchar(50) NOT NULL default '',
  `Apnpw` varchar(50) NOT NULL default '',
  `Dnsserv1` varchar(50) NOT NULL default '',
  `Dialn1` varchar(50) NOT NULL default '',
  `Ispun` varchar(50) NOT NULL default '',
  `Isppw` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`ConfigGprsMain_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=397 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configgprsmain`
--

/*!40000 ALTER TABLE `configgprsmain` DISABLE KEYS */;
INSERT INTO `configgprsmain` (`ConfigGprsMain_ID`,`Name`,`Descr`,`ID`,`Flags`,`Message_ID`,`Mode`,`Apnserv`,`Apnun`,`Apnpw`,`Dnsserv1`,`Dialn1`,`Ispun`,`Isppw`) VALUES 
 (382,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',382,0,0,0,'','','','','','',''),
 (383,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',382,0,200882,1,'www.kyivstar.net','igprs','internet','193.41.60.55','80672222901','internet','kyivstar'),
 (384,'','',382,0,200883,1,'www.kyivstar.net','igprs','internet','193.41.60.55','Ђg\"\"ђ000?0','internet','kyivstar'),
 (396,'','',382,0,218875,1,'www.kyivstar.net','igprs','internet','193.41.60.55','','internet','kyivstar');
/*!40000 ALTER TABLE `configgprsmain` ENABLE KEYS */;


--
-- Definition of table `configmain`
--

DROP TABLE IF EXISTS `configmain`;
CREATE TABLE `configmain` (
  `ConfigMain_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `ConfigOther_ID` int(11) default '0',
  `ConfigDriverMessage_ID` int(11) default '0',
  `ConfigEvent_ID` int(11) default '0',
  `ConfigUnique_ID` int(11) default '0',
  `ConfigZoneSet_ID` int(11) default '0',
  `ConfigSensorSet_ID` int(11) default '0',
  `ConfigTel_ID` int(11) default '0',
  `ConfigSms_ID` int(11) default '0',
  `Mobitel_ID` int(11) default '0',
  `ConfigGprsMain_ID` int(11) NOT NULL default '0',
  `ConfigGprsEmail_ID` int(11) NOT NULL default '0',
  `ConfigGprsInit_ID` int(11) NOT NULL default '0',
  PRIMARY KEY  (`ConfigMain_ID`),
  KEY `Index_2` (`Mobitel_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configmain`
--

/*!40000 ALTER TABLE `configmain` DISABLE KEYS */;
INSERT INTO `configmain` (`ConfigMain_ID`,`Name`,`Descr`,`ID`,`Flags`,`ConfigOther_ID`,`ConfigDriverMessage_ID`,`ConfigEvent_ID`,`ConfigUnique_ID`,`ConfigZoneSet_ID`,`ConfigSensorSet_ID`,`ConfigTel_ID`,`ConfigSms_ID`,`Mobitel_ID`,`ConfigGprsMain_ID`,`ConfigGprsEmail_ID`,`ConfigGprsInit_ID`) VALUES 
 (94,'Mobitel1','Descr',0,0,115,216,1785,128,247,103,514,708,97,382,341,299);
/*!40000 ALTER TABLE `configmain` ENABLE KEYS */;


--
-- Definition of table `configother`
--

DROP TABLE IF EXISTS `configother`;
CREATE TABLE `configother` (
  `ConfigOther_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `Pin` int(11) default '0',
  `Lang` int(11) default '0',
  `Message_ID` int(11) default '0',
  PRIMARY KEY  (`ConfigOther_ID`),
  KEY `Index_2` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configother`
--

/*!40000 ALTER TABLE `configother` DISABLE KEYS */;
INSERT INTO `configother` (`ConfigOther_ID`,`Name`,`Descr`,`ID`,`Flags`,`Pin`,`Lang`,`Message_ID`) VALUES 
 (115,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',115,0,0,0,0);
/*!40000 ALTER TABLE `configother` ENABLE KEYS */;


--
-- Definition of table `configsensorset`
--

DROP TABLE IF EXISTS `configsensorset`;
CREATE TABLE `configsensorset` (
  `ConfigSensorSet_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `Message_ID` int(11) default '0',
  PRIMARY KEY  (`ConfigSensorSet_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configsensorset`
--

/*!40000 ALTER TABLE `configsensorset` DISABLE KEYS */;
INSERT INTO `configsensorset` (`ConfigSensorSet_ID`,`Name`,`Descr`,`ID`,`Flags`,`Message_ID`) VALUES 
 (103,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',103,0,0);
/*!40000 ALTER TABLE `configsensorset` ENABLE KEYS */;


--
-- Definition of table `configsms`
--

DROP TABLE IF EXISTS `configsms`;
CREATE TABLE `configsms` (
  `ConfigSms_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `Message_ID` int(11) default '0',
  `smsCenter_ID` int(11) default '0',
  `smsDspt_ID` int(11) default '0',
  `smsEmailGate_ID` int(11) default '0',
  `dsptEmail` char(14) default NULL,
  `dsptGprsEmail` char(50) default NULL,
  PRIMARY KEY  (`ConfigSms_ID`),
  KEY `Index_2` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=721 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configsms`
--

/*!40000 ALTER TABLE `configsms` DISABLE KEYS */;
INSERT INTO `configsms` (`ConfigSms_ID`,`Name`,`Descr`,`ID`,`Flags`,`Message_ID`,`smsCenter_ID`,`smsDspt_ID`,`smsEmailGate_ID`,`dsptEmail`,`dsptGprsEmail`) VALUES 
 (708,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',708,0,0,0,0,0,NULL,NULL),
 (709,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',708,0,200809,9,62,74,'d000@p314.net','d000@p314.net'),
 (710,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',708,0,200810,9,62,74,'d000@p314.net','d000@p314.net'),
 (711,'','',708,0,200811,-1,62,74,'d000@p314.net','d000@p314.net'),
 (720,'','',708,0,218874,0,0,0,'','');
/*!40000 ALTER TABLE `configsms` ENABLE KEYS */;


--
-- Definition of table `configtel`
--

DROP TABLE IF EXISTS `configtel`;
CREATE TABLE `configtel` (
  `ConfigTel_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `Message_ID` int(11) default '0',
  `telSOS` int(11) default '0',
  `telDspt` int(11) default '0',
  `telAccept1` int(11) default '0',
  `telAccept2` int(11) default '0',
  `telAccept3` int(11) default '0',
  `telAnswer` int(11) default '0',
  PRIMARY KEY  (`ConfigTel_ID`),
  KEY `Index_2` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=528 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configtel`
--

/*!40000 ALTER TABLE `configtel` DISABLE KEYS */;
INSERT INTO `configtel` (`ConfigTel_ID`,`Name`,`Descr`,`ID`,`Flags`,`Message_ID`,`telSOS`,`telDspt`,`telAccept1`,`telAccept2`,`telAccept3`,`telAnswer`) VALUES 
 (514,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',514,0,0,0,0,0,0,0,0),
 (515,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',514,0,200807,57,57,71,89,75,0),
 (516,'','',514,0,200808,57,57,71,89,75,0),
 (527,'','',514,0,218873,58,58,58,58,58,0);
/*!40000 ALTER TABLE `configtel` ENABLE KEYS */;


--
-- Definition of table `configunique`
--

DROP TABLE IF EXISTS `configunique`;
CREATE TABLE `configunique` (
  `ConfigUnique_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `Message_ID` int(11) default '0',
  `pswd` char(8) default NULL,
  `tmpPswd` char(8) default NULL,
  `dsptId` char(4) default NULL,
  PRIMARY KEY  (`ConfigUnique_ID`),
  KEY `Index_2` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configunique`
--

/*!40000 ALTER TABLE `configunique` DISABLE KEYS */;
INSERT INTO `configunique` (`ConfigUnique_ID`,`Name`,`Descr`,`ID`,`Flags`,`Message_ID`,`pswd`,`tmpPswd`,`dsptId`) VALUES 
 (128,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',128,0,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `configunique` ENABLE KEYS */;


--
-- Definition of table `configzoneset`
--

DROP TABLE IF EXISTS `configzoneset`;
CREATE TABLE `configzoneset` (
  `ConfigZoneSet_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `Message_ID` int(11) default '0',
  PRIMARY KEY  (`ConfigZoneSet_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `configzoneset`
--

/*!40000 ALTER TABLE `configzoneset` DISABLE KEYS */;
INSERT INTO `configzoneset` (`ConfigZoneSet_ID`,`Name`,`Descr`,`ID`,`Flags`,`Message_ID`) VALUES 
 (247,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',247,0,0),
 (248,'','',247,0,200812),
 (249,'','',247,0,200847);
/*!40000 ALTER TABLE `configzoneset` ENABLE KEYS */;


--
-- Definition of table `data_zones`
--

DROP TABLE IF EXISTS `data_zones`;
CREATE TABLE `data_zones` (
  `ID` int(11) NOT NULL auto_increment,
  `DataGps_ID` int(11) default NULL,
  `Zone_ID` int(11) default NULL,
  `Zone_IO` int(11) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `data_zones`
--

/*!40000 ALTER TABLE `data_zones` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_zones` ENABLE KEYS */;


--
-- Definition of table `datagps`
-- 02.06.2011 удалены индексы   KEY `Index_5` (`Mobitel_ID`,`Counter1`,`Counter2`,`Counter3`,`Counter4`),  KEY `Index_4` (`Message_ID`),

DROP TABLE IF EXISTS `datagps`;
CREATE TABLE `datagps` (
  `DataGps_ID` int(11) NOT NULL auto_increment,
  `Mobitel_ID` int(11) default '0',
  `Message_ID` int(11) default '0',
  `Latitude` int(11) default '0',
  `Longitude` int(11) default '0',
  `Altitude` int(11) default NULL,
  `UnixTime` int(11) default '0',
  `Speed` smallint(6) default '0',
  `Direction` int(11) NOT NULL default '0',
  `Valid` tinyint(4) default '1',
  `InMobitelID` int(11) default NULL,
  `Events` int(10) unsigned default '0',
  `Sensor1` tinyint(4) unsigned default '0',
  `Sensor2` tinyint(4) unsigned default '0',
  `Sensor3` tinyint(4) unsigned default '0',
  `Sensor4` tinyint(4) unsigned default '0',
  `Sensor5` tinyint(4) unsigned default '0',
  `Sensor6` tinyint(4) unsigned default '0',
  `Sensor7` tinyint(4) unsigned default '0',
  `Sensor8` tinyint(4) unsigned default '0',
  `LogID` int(11) default '0',
  `isShow` tinyint(4) default '0',
  `whatIs` smallint(6) unsigned default '0',
  `Counter1` int(11) NOT NULL default '-1',
  `Counter2` int(11) NOT NULL default '-1',
  `Counter3` int(11) NOT NULL default '-1',
  `Counter4` int(11) NOT NULL default '-1',
  `SrvPacketID` bigint(20) NOT NULL default '0' COMMENT 'ID серверного пакета в состав которого входит эта запись',
  PRIMARY KEY  (`DataGps_ID`),
  KEY `Index_2` (`Mobitel_ID`,`LogID`),
  KEY `Index_3` (`Mobitel_ID`,`UnixTime`,`Valid`),
  KEY `IDX_MobitelIDSrvPacketID` (`Mobitel_ID`,`SrvPacketID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `datagps`
--

/*!40000 ALTER TABLE `datagps` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagps` ENABLE KEYS */;


--
-- Definition of table `datagpsbuffer`
--

DROP TABLE IF EXISTS `datagpsbuffer`;
CREATE TABLE `datagpsbuffer` (
  `DataGps_ID` int(11) NOT NULL auto_increment,
  `Mobitel_ID` int(11) default '0',
  `Message_ID` int(11) default '0',
  `Latitude` int(11) default '0',
  `Longitude` int(11) default '0',
  `Altitude` int(11) default NULL,
  `UnixTime` int(11) default '0',
  `Speed` smallint(6) default '0',
  `Direction` int(11) NOT NULL default '0',
  `Valid` tinyint(4) default '1',
  `InMobitelID` int(11) default NULL,
  `Events` int(10) unsigned default '0',
  `Sensor1` tinyint(4) unsigned default '0',
  `Sensor2` tinyint(4) unsigned default '0',
  `Sensor3` tinyint(4) unsigned default '0',
  `Sensor4` tinyint(4) unsigned default '0',
  `Sensor5` tinyint(4) unsigned default '0',
  `Sensor6` tinyint(4) unsigned default '0',
  `Sensor7` tinyint(4) unsigned default '0',
  `Sensor8` tinyint(4) unsigned default '0',
  `LogID` int(11) default '0',
  `isShow` tinyint(4) default '0',
  `whatIs` smallint(6) default '0',
  `Counter1` int(11) NOT NULL default '-1',
  `Counter2` int(11) NOT NULL default '-1',
  `Counter3` int(11) NOT NULL default '-1',
  `Counter4` int(11) NOT NULL default '-1',
  PRIMARY KEY  (`DataGps_ID`),
  KEY `IDX_MobitelIDLogID` (`Mobitel_ID`,`LogID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `datagpsbuffer`
--

/*!40000 ALTER TABLE `datagpsbuffer` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagpsbuffer` ENABLE KEYS */;


--
-- Definition of table `datagpsbuffer_dr`
--

DROP TABLE IF EXISTS `datagpsbuffer_dr`;
CREATE TABLE `datagpsbuffer_dr` (
  `Mobitel_ID` int(11) default '0',
  `Message_ID` int(11) default '0',
  `Latitude` int(11) NOT NULL default '0',
  `Longitude` int(11) NOT NULL default '0',
  `Altitude` int(11) default '0',
  `UnixTime` int(11) NOT NULL default '0',
  `Speed` smallint(6) NOT NULL default '0',
  `Direction` int(11) NOT NULL default '0',
  `Valid` tinyint(4) NOT NULL default '1',
  `InMobitelID` int(11) default '0',
  `Events` int(10) unsigned default '0',
  `Sensor1` tinyint(4) unsigned default '0',
  `Sensor2` tinyint(4) unsigned default '0',
  `Sensor3` tinyint(4) unsigned default '0',
  `Sensor4` tinyint(4) unsigned default '0',
  `Sensor5` tinyint(4) unsigned default '0',
  `Sensor6` tinyint(4) unsigned default '0',
  `Sensor7` tinyint(4) unsigned default '0',
  `Sensor8` tinyint(4) unsigned default '0',
  `LogID` int(11) default '0',
  `isShow` tinyint(4) default '0',
  `whatIs` smallint(6) unsigned NOT NULL default '0',
  `Counter1` int(11) NOT NULL default '-1',
  `Counter2` int(11) NOT NULL default '-1',
  `Counter3` int(11) NOT NULL default '-1',
  `Counter4` int(11) NOT NULL default '-1',
  KEY `IDX_MobitelIDLogID` USING BTREE (`Mobitel_ID`,`LogID`),
  KEY `IDX_MobitelidUnixtime` USING BTREE (`Mobitel_ID`,`UnixTime`),
  KEY `IDX_MobitelidUnixtimeValid` USING BTREE (`Mobitel_ID`,`UnixTime`,`Valid`)
) ENGINE=MEMORY DEFAULT CHARSET=cp1251 COMMENT='Буфер приема данных поступающих по socket-ам от телетреков';

--
-- Dumping data for table `datagpsbuffer_dr`
--

/*!40000 ALTER TABLE `datagpsbuffer_dr` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagpsbuffer_dr` ENABLE KEYS */;


--
-- Definition of table `datagpsbuffer_on`
--

DROP TABLE IF EXISTS `datagpsbuffer_on`;
CREATE TABLE `datagpsbuffer_on` (
  `Mobitel_ID` int(11) NOT NULL default '0',
  `Message_ID` int(11) default '0',
  `Latitude` int(11) NOT NULL default '0',
  `Longitude` int(11) NOT NULL default '0',
  `Altitude` int(11) default NULL,
  `UnixTime` int(11) NOT NULL default '0',
  `Speed` smallint(6) NOT NULL default '0',
  `Direction` int(11) NOT NULL default '0',
  `Valid` tinyint(4) NOT NULL default '1',
  `InMobitelID` int(11) default NULL,
  `Events` int(10) unsigned default '0',
  `Sensor1` tinyint(4) unsigned default '0',
  `Sensor2` tinyint(4) unsigned default '0',
  `Sensor3` tinyint(4) unsigned default '0',
  `Sensor4` tinyint(4) unsigned default '0',
  `Sensor5` tinyint(4) unsigned default '0',
  `Sensor6` tinyint(4) unsigned default '0',
  `Sensor7` tinyint(4) unsigned default '0',
  `Sensor8` tinyint(4) unsigned default '0',
  `LogID` int(11) NOT NULL default '0',
  `isShow` tinyint(4) default '0',
  `whatIs` smallint(6) NOT NULL default '0',
  `Counter1` int(11) NOT NULL default '-1',
  `Counter2` int(11) NOT NULL default '-1',
  `Counter3` int(11) NOT NULL default '-1',
  `Counter4` int(11) NOT NULL default '-1',
  `SrvPacketID` bigint(20) NOT NULL default '0' COMMENT 'ID серверного пакета в состав которого входит эта запись',
  KEY `IDX_MobitelidLogid` USING BTREE (`Mobitel_ID`,`LogID`),
  KEY `IDX_MobitelidUnixtime` USING BTREE (`Mobitel_ID`,`UnixTime`),
  KEY `IDX_MobitelidUnixtimeValid` USING BTREE (`Mobitel_ID`,`UnixTime`,`Valid`),
  KEY `IDX_SrvPacketID` USING BTREE (`SrvPacketID`)
) ENGINE=MEMORY DEFAULT CHARSET=cp1251 COMMENT='Буфер онлайн данных';

--
-- Dumping data for table `datagpsbuffer_on`
--

/*!40000 ALTER TABLE `datagpsbuffer_on` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagpsbuffer_on` ENABLE KEYS */;


--
-- Definition of table `datagpsbuffer_ontmp`
--

DROP TABLE IF EXISTS `datagpsbuffer_ontmp`;
CREATE TABLE `datagpsbuffer_ontmp` (
  `Mobitel_ID` int(11) NOT NULL default '0',
  `LogID` int(11) NOT NULL default '0',
  `SrvPacketID` bigint(20) NOT NULL,
  KEY `IDX_MobitelidSrvpacketid` USING BTREE (`Mobitel_ID`,`SrvPacketID`),
  KEY `IDX_Mobitelid` USING BTREE (`Mobitel_ID`)
) ENGINE=MEMORY DEFAULT CHARSET=cp1251 COMMENT='Временный вспомогательный буфер онлайн данных';

--
-- Dumping data for table `datagpsbuffer_ontmp`
--

/*!40000 ALTER TABLE `datagpsbuffer_ontmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagpsbuffer_ontmp` ENABLE KEYS */;


--
-- Definition of table `datagpsbuffer_pop`
--

DROP TABLE IF EXISTS `datagpsbuffer_pop`;
CREATE TABLE `datagpsbuffer_pop` (
  `Mobitel_ID` int(11) default '0',
  `Message_ID` int(11) default '0',
  `Latitude` int(11) NOT NULL default '0',
  `Longitude` int(11) NOT NULL default '0',
  `Altitude` int(11) default '0',
  `UnixTime` int(11) NOT NULL default '0',
  `Speed` smallint(6) NOT NULL default '0',
  `Direction` int(11) NOT NULL default '0',
  `Valid` tinyint(4) NOT NULL default '1',
  `InMobitelID` int(11) default '0',
  `Events` int(10) unsigned default '0',
  `Sensor1` tinyint(4) unsigned default '0',
  `Sensor2` tinyint(4) unsigned default '0',
  `Sensor3` tinyint(4) unsigned default '0',
  `Sensor4` tinyint(4) unsigned default '0',
  `Sensor5` tinyint(4) unsigned default '0',
  `Sensor6` tinyint(4) unsigned default '0',
  `Sensor7` tinyint(4) unsigned default '0',
  `Sensor8` tinyint(4) unsigned default '0',
  `LogID` int(11) default '0',
  `isShow` tinyint(4) default '0',
  `whatIs` smallint(6) unsigned NOT NULL default '0',
  `Counter1` int(11) NOT NULL default '-1',
  `Counter2` int(11) NOT NULL default '-1',
  `Counter3` int(11) NOT NULL default '-1',
  `Counter4` int(11) NOT NULL default '-1',
  KEY `IDX_MobitelIDLogID` USING BTREE (`Mobitel_ID`,`LogID`),
  KEY `IDX_MobitelidUnixtime` USING BTREE (`Mobitel_ID`,`UnixTime`),
  KEY `IDX_MobitelidUnixtimeValid` USING BTREE (`Mobitel_ID`,`UnixTime`,`Valid`)
) ENGINE=MEMORY DEFAULT CHARSET=cp1251 COMMENT='Буфер приема данных поступающих по POP3 протоколу';

--
-- Dumping data for table `datagpsbuffer_pop`
--

/*!40000 ALTER TABLE `datagpsbuffer_pop` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagpsbuffer_pop` ENABLE KEYS */;


--
-- Definition of table `datagpsbufferlite`
--

DROP TABLE IF EXISTS `datagpsbufferlite`;
CREATE TABLE `datagpsbufferlite` (
  `DataGps_ID` int(11) NOT NULL auto_increment,
  `Mobitel_ID` int(11) default '0',
  `Message_ID` int(11) default '0',
  `Latitude` int(11) default '0',
  `Longitude` int(11) default '0',
  `Altitude` int(11) default NULL,
  `UnixTime` int(11) default '0',
  `Speed` smallint(6) default '0',
  `Direction` int(11) NOT NULL default '0',
  `Valid` tinyint(4) default '1',
  `InMobitelID` int(11) default NULL,
  `Events` int(10) unsigned default '0',
  `Sensor1` tinyint(4) unsigned default '0',
  `Sensor2` tinyint(4) unsigned default '0',
  `Sensor3` tinyint(4) unsigned default '0',
  `Sensor4` tinyint(4) unsigned default '0',
  `Sensor5` tinyint(4) unsigned default '0',
  `Sensor6` tinyint(4) unsigned default '0',
  `Sensor7` tinyint(4) unsigned default '0',
  `Sensor8` tinyint(4) unsigned default '0',
  `LogID` int(11) default '0',
  `isShow` tinyint(4) default '0',
  `whatIs` smallint(6) default '0',
  `Counter1` int(11) NOT NULL default '-1',
  `Counter2` int(11) NOT NULL default '-1',
  `Counter3` int(11) NOT NULL default '-1',
  `Counter4` int(11) NOT NULL default '-1',
  PRIMARY KEY  (`DataGps_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `datagpsbufferlite`
--

/*!40000 ALTER TABLE `datagpsbufferlite` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagpsbufferlite` ENABLE KEYS */;


--
-- Definition of table `datagpslost`
--

DROP TABLE IF EXISTS `datagpslost`;
CREATE TABLE `datagpslost` (
  `Mobitel_ID` int(11) NOT NULL default '0',
  `Begin_LogID` int(11) NOT NULL default '0' COMMENT 'Значение LogID после которого начинается разрыв',
  `End_LogID` int(11) NOT NULL default '0' COMMENT 'Значение LogID перед которым завершается разрыв',
  PRIMARY KEY  (`Mobitel_ID`,`Begin_LogID`),
  KEY `IDX_MobitelID` (`Mobitel_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='Список пропусков в данных DataGPS пришодящих от RF-модемов';

--
-- Dumping data for table `datagpslost`
--

/*!40000 ALTER TABLE `datagpslost` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagpslost` ENABLE KEYS */;


--
-- Definition of table `datagpslost_on`
--

DROP TABLE IF EXISTS `datagpslost_on`;
CREATE TABLE `datagpslost_on` (
  `Mobitel_ID` int(11) NOT NULL default '0',
  `Begin_LogID` int(11) NOT NULL default '0' COMMENT 'Значение LogID после которого начинается разрыв',
  `End_LogID` int(11) NOT NULL default '0' COMMENT 'Значение LogID перед которым завершается разрыв',
  `Begin_SrvPacketID` bigint(20) NOT NULL COMMENT 'Идентификаор серверного пакета в который входит запись с Begin_LogID',
  `End_SrvPacketID` bigint(20) NOT NULL COMMENT 'Идентификаор серверного пакета в который входит запись с End_LogID',
  PRIMARY KEY  (`Mobitel_ID`,`Begin_LogID`),
  UNIQUE KEY `IDX_MobitelID_BeginSrvPacketID` (`Mobitel_ID`,`Begin_SrvPacketID`),
  KEY `IDX_MobitelID` (`Mobitel_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='Пропуски в данных DataGPS, приходящих от Online сервера';

--
-- Dumping data for table `datagpslost_on`
--

/*!40000 ALTER TABLE `datagpslost_on` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagpslost_on` ENABLE KEYS */;


--
-- Definition of table `datagpslost_ontmp`
--

DROP TABLE IF EXISTS `datagpslost_ontmp`;
CREATE TABLE `datagpslost_ontmp` (
  `LogID` int(11) NOT NULL default '0',
  `SrvPacketID` bigint(20) NOT NULL
) ENGINE=MEMORY DEFAULT CHARSET=cp1251 COMMENT='Используется исключительно процедурой OnLostDataGPS';

--
-- Dumping data for table `datagpslost_ontmp`
--

/*!40000 ALTER TABLE `datagpslost_ontmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagpslost_ontmp` ENABLE KEYS */;


--
-- Definition of table `datagpslost_rftmp`
--

DROP TABLE IF EXISTS `datagpslost_rftmp`;
CREATE TABLE `datagpslost_rftmp` (
  `LogID` int(11) default '0'
) ENGINE=MEMORY DEFAULT CHARSET=cp1251 COMMENT='Используется исключительно процедурой LostDataGPS';

--
-- Dumping data for table `datagpslost_rftmp`
--

/*!40000 ALTER TABLE `datagpslost_rftmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagpslost_rftmp` ENABLE KEYS */;


--
-- Definition of table `datagpslosted`
--

DROP TABLE IF EXISTS `datagpslosted`;
CREATE TABLE `datagpslosted` (
  `Mobitel_ID` int(11) NOT NULL default '0',
  `Begin_LogID` int(11) NOT NULL default '0',
  `End_LogID` int(11) NOT NULL default '0',
  PRIMARY KEY  (`Mobitel_ID`,`Begin_LogID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `datagpslosted`
--

/*!40000 ALTER TABLE `datagpslosted` DISABLE KEYS */;
/*!40000 ALTER TABLE `datagpslosted` ENABLE KEYS */;


--
-- Definition of table `driver`
--

DROP TABLE IF EXISTS `driver`;
CREATE TABLE `driver` (
  `id` int(11) NOT NULL auto_increment,
  `Family` char(40) default NULL COMMENT 'Фамилия',
  `Name` char(40) default NULL COMMENT 'Имя',
  `ByrdDay` date default NULL COMMENT 'Дата рождения',
  `Category` char(5) default NULL COMMENT 'Категория',
  `Permis` char(20) default NULL COMMENT 'Права',
  `foto` blob COMMENT 'фото',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id` USING BTREE (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `driver`
--

/*!40000 ALTER TABLE `driver` DISABLE KEYS */;
INSERT INTO `driver` (`id`,`Family`,`Name`,`ByrdDay`,`Category`,`Permis`,`foto`) VALUES 
 (1,'','','2009-01-01','с','1111',NULL),
 (2,'Водитель','','2009-01-22','с','1111',NULL);
/*!40000 ALTER TABLE `driver` ENABLE KEYS */;


--
-- Definition of table `gpsmasks`
--

DROP TABLE IF EXISTS `gpsmasks`;
CREATE TABLE `gpsmasks` (
  `GpsMask_ID` int(11) NOT NULL auto_increment,
  `ID` int(11) default '0',
  `Name` char(200) default NULL,
  `Descr` char(200) default NULL,
  `Mobitel_ID` int(11) default '0',
  `Message_ID` int(11) default '0',
  `LastRecords` int(11) default '0',
  `LastTimes` int(11) default '0',
  `LastTimesIndex` int(11) default '0',
  `LastMeters` int(11) default '0',
  `LastMetersIndex` int(11) default '0',
  `MaxSmsNum` int(11) default '0',
  `MaxSmsNumIndex` int(11) default '0',
  `LatitudeStart` int(11) default '0',
  `LatitudeEnd` int(11) default '0',
  `LongitudeStart` int(11) default '0',
  `LongitudeEnd` int(11) default '0',
  `AltitudeStart` int(11) default '0',
  `AltitudeEnd` int(11) default '0',
  `UnixTimeStart` int(11) default '0',
  `UnixTimeEnd` int(11) default '0',
  `SpeedStart` int(11) default '0',
  `SpeedEnd` int(11) default '0',
  `DirectionStart` int(11) default '0',
  `DirectionEnd` int(11) default '0',
  `NumbersStart` int(11) default '0',
  `NumbersEnd` int(11) default '0',
  `WhatSend` int(1) default '0',
  `CheckMask` int(11) default '0',
  `InMobitelID` int(11) default '0',
  `Events` int(11) default '0',
  `Number1` int(11) default '0',
  `Number2` int(11) default '0',
  `Number3` int(11) default '0',
  `Number4` int(11) default '0',
  `Number5` int(11) default '0',
  PRIMARY KEY  (`GpsMask_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1134 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `gpsmasks`
--

/*!40000 ALTER TABLE `gpsmasks` DISABLE KEYS */;
INSERT INTO `gpsmasks` (`GpsMask_ID`,`ID`,`Name`,`Descr`,`Mobitel_ID`,`Message_ID`,`LastRecords`,`LastTimes`,`LastTimesIndex`,`LastMeters`,`LastMetersIndex`,`MaxSmsNum`,`MaxSmsNumIndex`,`LatitudeStart`,`LatitudeEnd`,`LongitudeStart`,`LongitudeEnd`,`AltitudeStart`,`AltitudeEnd`,`UnixTimeStart`,`UnixTimeEnd`,`SpeedStart`,`SpeedEnd`,`DirectionStart`,`DirectionEnd`,`NumbersStart`,`NumbersEnd`,`WhatSend`,`CheckMask`,`InMobitelID`,`Events`,`Number1`,`Number2`,`Number3`,`Number4`,`Number5`) VALUES 
 (1132,0,'','',97,200893,8,-10800,0,0,0,0,0,0,0,0,0,0,0,-10800,-10800,0,0,0,0,0,0,1023,1,0,0,0,0,0,0,0),
 (1133,0,'','',97,201039,100,-10800,0,0,0,0,0,0,0,0,0,0,0,-10800,-10800,0,0,0,0,0,0,1023,1,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `gpsmasks` ENABLE KEYS */;


--
-- Definition of table `internalmobitelconfig`
--

DROP TABLE IF EXISTS `internalmobitelconfig`;
CREATE TABLE `internalmobitelconfig` (
  `InternalMobitelConfig_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Message_ID` int(11) default '0',
  `devIdLong` char(16) default NULL,
  `devIdShort` char(4) default NULL,
  `verProtocolLong` char(16) default NULL,
  `verProtocolShort` char(2) default NULL,
  `moduleIdGps` char(50) default NULL,
  `moduleIdGsm` char(50) default NULL,
  `moduleIdRf` char(50) default NULL,
  `RfType` smallint(6) NOT NULL default '0',
  `ActiveRf` tinyint(1) NOT NULL default '0',
  `moduleIdSs` char(50) default NULL,
  `moduleIdMm` char(50) NOT NULL default 'moduleIdMm',
  PRIMARY KEY  (`InternalMobitelConfig_ID`),
  KEY `Index_3` (`Message_ID`),
  KEY `Index_4` (`devIdShort`),
  KEY `IDX_InternalmobitelconfigID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=802 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `internalmobitelconfig`
--

/*!40000 ALTER TABLE `internalmobitelconfig` DISABLE KEYS */;
INSERT INTO `internalmobitelconfig` (`InternalMobitelConfig_ID`,`Name`,`Descr`,`ID`,`Message_ID`,`devIdLong`,`devIdShort`,`verProtocolLong`,`verProtocolShort`,`moduleIdGps`,`moduleIdGsm`,`moduleIdRf`,`RfType`,`ActiveRf`,`moduleIdSs`,`moduleIdMm`) VALUES 
 (789,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',789,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,'moduleIdMm'),
 (790,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',789,200801,'','3051','','','','','',0,0,'','moduleIdMm'),
 (791,'','',789,200803,'5040003TT201','3051','1.18','яя','яяяя','яяяя','яяяя',0,0,'яяяя','яяяя'),
 (801,'','',789,218878,'','','1.18','яя','яяяя','яяяя','яяяя',0,0,'яяяя','moduleIdMm');
/*!40000 ALTER TABLE `internalmobitelconfig` ENABLE KEYS */;


--
-- Definition of table `lastmeters`
--

DROP TABLE IF EXISTS `lastmeters`;
CREATE TABLE `lastmeters` (
  `LastMeter_ID` int(11) default '0',
  `ID` int(11) default '0',
  `Name` char(200) default NULL,
  `Descr` char(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `lastmeters`
--

/*!40000 ALTER TABLE `lastmeters` DISABLE KEYS */;
INSERT INTO `lastmeters` (`LastMeter_ID`,`ID`,`Name`,`Descr`) VALUES 
 (200,2,'200 метров',NULL),
 (1000,3,'1 километр',NULL),
 (5000,4,'5 километров',NULL),
 (25000,5,'25 километров',NULL),
 (100000,6,'100 километров',NULL);
/*!40000 ALTER TABLE `lastmeters` ENABLE KEYS */;


--
-- Definition of table `lastrecords`
--

DROP TABLE IF EXISTS `lastrecords`;
CREATE TABLE `lastrecords` (
  `LastRecord_ID` int(11) default '0',
  `ID` int(11) default '0',
  `Name` char(200) default NULL,
  `Descr` char(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `lastrecords`
--

/*!40000 ALTER TABLE `lastrecords` DISABLE KEYS */;
INSERT INTO `lastrecords` (`LastRecord_ID`,`ID`,`Name`,`Descr`) VALUES 
 (5,1,'5 записей',NULL),
 (20,2,'20 записей',NULL),
 (50,3,'50 записей',NULL),
 (200,4,'200 записей',NULL),
 (1000,6,'1000 записей',NULL),
 (500,5,'500 записей',NULL),
 (3000,7,'3000 записей','3000 записей');
/*!40000 ALTER TABLE `lastrecords` ENABLE KEYS */;


--
-- Definition of table `lasttimes`
--

DROP TABLE IF EXISTS `lasttimes`;
CREATE TABLE `lasttimes` (
  `LastTime_ID` int(11) default '0',
  `ID` int(11) default '0',
  `Name` char(200) default NULL,
  `Descr` char(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `lasttimes`
--

/*!40000 ALTER TABLE `lasttimes` DISABLE KEYS */;
INSERT INTO `lasttimes` (`LastTime_ID`,`ID`,`Name`,`Descr`) VALUES 
 (5,1,'5 минут',NULL),
 (20,2,'20 минут',NULL),
 (120,3,'2 часа',NULL),
 (360,4,'6 часов',NULL),
 (1440,5,'сутки',NULL);
/*!40000 ALTER TABLE `lasttimes` ENABLE KEYS */;


--
-- Definition of table `lines`
--

DROP TABLE IF EXISTS `lines`;
CREATE TABLE `lines` (
  `id` int(11) NOT NULL auto_increment,
  `Color` int(11) default '0' COMMENT 'Цвет',
  `Width` int(11) default '3' COMMENT 'Толщина линии',
  `Mobitel_ID` int(11) default NULL,
  `Icon` blob,
  PRIMARY KEY  (`id`),
  KEY `lines_FK1` USING BTREE (`Mobitel_ID`),
  CONSTRAINT `lines_FK1` FOREIGN KEY (`Mobitel_ID`) REFERENCES `mobitels` (`Mobitel_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='Описание линии';

--
-- Dumping data for table `lines`
--

/*!40000 ALTER TABLE `lines` DISABLE KEYS */;
/*!40000 ALTER TABLE `lines` ENABLE KEYS */;


--
-- Definition of table `mapstyleglobal`
--

DROP TABLE IF EXISTS `mapstyleglobal`;
CREATE TABLE `mapstyleglobal` (
  `MapStyleGlobal_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `DefaultLineStyle_ID` int(11) default '0',
  `DefaulPointStyle_ID` int(11) default '0',
  `EventPointStyle1_ID` int(11) default '0',
  `EventPointStyle2_ID` int(11) default '0',
  `EventPointStyle3_ID` int(11) default '0',
  `EventPointStyle4_ID` int(11) default '0',
  `EventPointStyle5_ID` int(11) default '0',
  `EventPointStyle6_ID` int(11) default '0',
  `EventPointStyle7_ID` int(11) default '0',
  `EventPointStyle8_ID` int(11) default '0',
  `EventPointStyle9_ID` int(11) default '0',
  `EventPointStyle10_ID` int(11) default '0',
  `EventPointStyle11_ID` int(11) default '0',
  `EventPointStyle12_ID` int(11) default '0',
  `EventPointStyle13_ID` int(11) default '0',
  `EventPointStyle14_ID` int(11) default '0',
  `EventPointStyle15_ID` int(11) default '0',
  `EventPointStyle16_ID` int(11) default '0',
  `EventPointStyle17_ID` int(11) default '0',
  `EventPointStyle18_ID` int(11) default '0',
  `EventPointStyle19_ID` int(11) default '0',
  `EventPointStyle20_ID` int(11) default '0',
  `EventPointStyle21_ID` int(11) default '0',
  `EventPointStyle22_ID` int(11) default '0',
  `EventPointStyle23_ID` int(11) default '0',
  `EventPointStyle24_ID` int(11) default '0',
  `EventPointStyle25_ID` int(11) default '0',
  `EventPointStyle26_ID` int(11) default '0',
  `EventPointStyle27_ID` int(11) default '0',
  `EventPointStyle28_ID` int(11) default '0',
  `EventPointStyle29_ID` int(11) default '0',
  `EventPointStyle30_ID` int(11) default '0',
  `EventPointStyle31_ID` int(11) default '0',
  `EventPointStyle32_ID` int(11) default '0',
  `SensorPointStyle1_ID` int(11) default '0',
  `SensorPointStyle2_ID` int(11) default '0',
  `SensorPointStyle3_ID` int(11) default '0',
  `SensorPointStyle4_ID` int(11) default '0',
  `SensorPointStyle5_ID` int(11) default '0',
  `SensorPointStyle6_ID` int(11) default '0',
  `SensorPointStyle7_ID` int(11) default '0',
  `SensorPointStyle8_ID` int(11) default '0',
  PRIMARY KEY  (`MapStyleGlobal_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `mapstyleglobal`
--

/*!40000 ALTER TABLE `mapstyleglobal` DISABLE KEYS */;
INSERT INTO `mapstyleglobal` (`MapStyleGlobal_ID`,`Name`,`Descr`,`ID`,`DefaultLineStyle_ID`,`DefaulPointStyle_ID`,`EventPointStyle1_ID`,`EventPointStyle2_ID`,`EventPointStyle3_ID`,`EventPointStyle4_ID`,`EventPointStyle5_ID`,`EventPointStyle6_ID`,`EventPointStyle7_ID`,`EventPointStyle8_ID`,`EventPointStyle9_ID`,`EventPointStyle10_ID`,`EventPointStyle11_ID`,`EventPointStyle12_ID`,`EventPointStyle13_ID`,`EventPointStyle14_ID`,`EventPointStyle15_ID`,`EventPointStyle16_ID`,`EventPointStyle17_ID`,`EventPointStyle18_ID`,`EventPointStyle19_ID`,`EventPointStyle20_ID`,`EventPointStyle21_ID`,`EventPointStyle22_ID`,`EventPointStyle23_ID`,`EventPointStyle24_ID`,`EventPointStyle25_ID`,`EventPointStyle26_ID`,`EventPointStyle27_ID`,`EventPointStyle28_ID`,`EventPointStyle29_ID`,`EventPointStyle30_ID`,`EventPointStyle31_ID`,`EventPointStyle32_ID`,`SensorPointStyle1_ID`,`SensorPointStyle2_ID`,`SensorPointStyle3_ID`,`SensorPointStyle4_ID`,`SensorPointStyle5_ID`,`SensorPointStyle6_ID`,`SensorPointStyle7_ID`,`SensorPointStyle8_ID`) VALUES 
 (1,'Настройка 1','Настнойки для Мобител',0,1,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
 (2,'Настнойки 4','Настнойки mob 4',0,2,2,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
 (3,'Настнойки 3','Настнойки для Мобител 3',0,3,3,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
 (4,'Tune 2','Настнойки для Мобител 2',0,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `mapstyleglobal` ENABLE KEYS */;


--
-- Definition of table `mapstylelines`
--

DROP TABLE IF EXISTS `mapstylelines`;
CREATE TABLE `mapstylelines` (
  `MapStyleLine_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Color` int(11) default '0',
  `Width` tinyint(4) default '0',
  `Style` tinyint(4) default '0',
  PRIMARY KEY  (`MapStyleLine_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `mapstylelines`
--

/*!40000 ALTER TABLE `mapstylelines` DISABLE KEYS */;
INSERT INTO `mapstylelines` (`MapStyleLine_ID`,`Name`,`Descr`,`ID`,`Color`,`Width`,`Style`) VALUES 
 (1,'Стиль линий Black','Описание',1,0,1,0),
 (2,'Стиль линий Blue','Стиль линий 2',2,16711680,4,0),
 (3,'Стиль линий Green','Стиль линий 3',3,32768,5,0),
 (4,'Стиль линий Red','Стиль линий 4',4,255,1,0),
 (6,'DEFAULT','Стиль по умолчанию',0,8388736,2,0),
 (8,'Style Fuchsia','Description for linestyle created at 13.08.2004 10:39:31',0,16711935,1,0);
/*!40000 ALTER TABLE `mapstylelines` ENABLE KEYS */;


--
-- Definition of table `mapstylepoints`
--

DROP TABLE IF EXISTS `mapstylepoints`;
CREATE TABLE `mapstylepoints` (
  `MapStylePoint_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `isBitmap` tinyint(1) default NULL,
  `SymbolCode` tinyint(4) default '0',
  `Color` int(11) default '0',
  `Font` char(200) default NULL,
  `Size` int(11) default '0',
  `Rotation` int(11) default '0',
  `BitmapName` char(200) default NULL,
  `BitmapStyle` tinyint(4) default '0',
  `Style` tinyint(4) default '0',
  PRIMARY KEY  (`MapStylePoint_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `mapstylepoints`
--

/*!40000 ALTER TABLE `mapstylepoints` DISABLE KEYS */;
INSERT INTO `mapstylepoints` (`MapStylePoint_ID`,`Name`,`Descr`,`ID`,`isBitmap`,`SymbolCode`,`Color`,`Font`,`Size`,`Rotation`,`BitmapName`,`BitmapStyle`,`Style`) VALUES 
 (2,'Blue Style','Стиль точек 1',2,0,0,16711680,NULL,3,0,NULL,0,0),
 (3,'Red Style','Стиль точек 2',3,0,0,255,NULL,4,0,NULL,0,0),
 (5,'DEFAULT','Стиль по умолчанию',0,0,0,128,NULL,5,0,NULL,0,0),
 (6,'Yellow Style','Стиль точек 3',4,0,0,65535,NULL,4,0,NULL,0,0),
 (8,'Fuchisia Style','Description for pointstyle created at 13.08.2004 10:39:45',0,0,0,16711935,NULL,3,0,NULL,0,0),
 (9,'Lime Style','Description for pointstyle created at 13.08.2004 10:41:19',0,0,0,65280,NULL,2,0,NULL,0,0),
 (10,'test','Description for pointstyle created at 06.12.2004 17:36:40',0,NULL,0,8421376,NULL,3,0,NULL,0,0);
/*!40000 ALTER TABLE `mapstylepoints` ENABLE KEYS */;


--
-- Definition of table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `Message_ID` int(11) NOT NULL auto_increment,
  `Time1` int(11) default '0',
  `Time2` int(11) default '0',
  `Time3` int(11) default '0',
  `Time4` int(11) default '0',
  `isSend` tinyint(1) default NULL,
  `isDelivered` tinyint(1) default NULL,
  `isNew` tinyint(1) default NULL,
  `Direction` tinyint(1) default NULL,
  `Mobitel_ID` int(11) default '0',
  `ServiceInit_ID` int(11) default '0',
  `Command_ID` tinyint(4) default '0',
  `CommandMask_ID` int(11) default '0',
  `MessageCounter` int(11) default '0',
  `DataFromService` char(32) default NULL,
  `Address` char(20) default NULL,
  `Source_ID` int(11) default '0',
  `Crc` tinyint(4) default '1',
  `SmsId` int(11) default '0',
  `AdvCounter` int(11) NOT NULL default '0',
  `isNewFromMob` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`Message_ID`),
  KEY `Index_2` USING BTREE (`isNewFromMob`,`Message_ID`,`Time1`),
  KEY `Index_6` (`isNew`,`Direction`,`ServiceInit_ID`),
  KEY `Index_4` (`Mobitel_ID`,`Command_ID`),
  KEY `Index_5` (`Mobitel_ID`,`MessageCounter`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

--
-- Dumping data for table `messages`
--

/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;


--
-- Definition of table `mobitels`
--

DROP TABLE IF EXISTS `mobitels`;
CREATE TABLE `mobitels` (
  `Mobitel_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` bigint(20) default '0',
  `Flags` int(11) default '0',
  `ServiceInit_ID` int(11) default '0',
  `ServiceSend_ID` int(11) default '0',
  `InternalMobitelConfig_ID` int(11) default '0',
  `MapStyleLine_ID` int(11) default '0',
  `MapStyleLastPoint_ID` int(11) default '0',
  `MapStylePoint_ID` int(11) default '0',
  `Route_ID` int(11) default '0',
  `DeviceType` int(11) default '0',
  `DeviceEngine` int(11) default '0',
  `ConfirmedID` int(11) NOT NULL default '0' COMMENT 'MIN значение LogID менее которого считается нет пропусков или уже невозможно загрузить',
  `LastInsertTime` int(11) NOT NULL default '0' COMMENT 'UNIX_TIMESTAMP Время последней вставки данных в DataGPS',
  PRIMARY KEY  (`Mobitel_ID`),
  KEY `IDX_InternalmobitelconfigID` (`InternalMobitelConfig_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `mobitels`
--

/*!40000 ALTER TABLE `mobitels` DISABLE KEYS */;
INSERT INTO `mobitels` (`Mobitel_ID`,`Name`,`Descr`,`ID`,`Flags`,`ServiceInit_ID`,`ServiceSend_ID`,`InternalMobitelConfig_ID`,`MapStyleLine_ID`,`MapStyleLastPoint_ID`,`MapStylePoint_ID`,`Route_ID`,`DeviceType`,`DeviceEngine`,`ConfirmedID`,`LastInsertTime`) VALUES 
 (97,'Телетрек 1','',0,0,0,250,789,6,8,3,0,0,0,0,0);
/*!40000 ALTER TABLE `mobitels` ENABLE KEYS */;


--
-- Definition of table `mobitels_info`
--

DROP TABLE IF EXISTS `mobitels_info`;
CREATE TABLE `mobitels_info` (
  `Mobitel_ID` int(10) unsigned NOT NULL,
  `Map_Style_Car` text,
  `Track_Color` int(10) unsigned default NULL,
  PRIMARY KEY  (`Mobitel_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

--
-- Dumping data for table `mobitels_info`
--

/*!40000 ALTER TABLE `mobitels_info` DISABLE KEYS */;
INSERT INTO `mobitels_info` (`Mobitel_ID`,`Map_Style_Car`,`Track_Color`) VALUES 
 (79,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (97,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (98,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (99,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (100,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (101,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (102,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (103,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (104,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (105,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (106,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (107,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (108,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (109,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL),
 (110,'object TDrawersGroup\r\n  DrawMode = gdmDrawAll\r\n  object TPointIconDrawer\r\n    AdjustSize = False\r\n    Image_Data = {\r\n      0000010001001313000000000000E80400001600000028000000130000002600\r\n      00000100180000000000C0040000000000000000000000000000000000000000\r\n      00FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284000000000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000FFA28400000000000000\r\n      0000000000000000000000800000800000800000800000800000000000000000\r\n      000000000000000000000000FFA284000000FFA2840000000000000000000000\r\n      00800000800000800000FFFFFFFFFFFFFFFFFF80000080000080000000000000\r\n      0000000000000000FFA284000000FFA284000000000000000000800000800000\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000000000000000\r\n      00000000FFA284000000FFA284000000000000800000800000800000FFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000000000000000\r\n      FFA284000000FFA284000000000000800000FFFFFF800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFFFFFFF800000800000FFFFFF800000000000000000FFA28400\r\n      0000FFA284000000800000800000FFFFFFFFFFFF800000800000800000FFFFFF\r\n      800000800000800000FFFFFFFFFFFF800000800000000000FFA284000000FFA2\r\n      84000000800000FFFFFFFFFFFFFFFFFFFFFFFF80000080000080000080000080\r\n      0000FFFFFFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA284000000\r\n      800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFF\r\n      FFFFFFFFFFFFFFF0FBFF800000000000FFA284000000FFA28400000080000080\r\n      0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000800000FFFFFFFFFFFFFFFFFF\r\n      FFFFFF800000800000000000FFA284000000FFA284000000000000800000FFFF\r\n      FFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80\r\n      0000000000000000FFA284000000FFA284000000000000800000800000FFFFFF\r\n      FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF8000008000000000\r\n      00000000FFA284000000FFA284000000000000000000800000FFFFFFFFFFFFFF\r\n      FFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000\r\n      FFA284000000FFA284000000000000000000000000800000800000FFFFFFFFFF\r\n      FF800000FFFFFFFFFFFF800000800000000000000000000000000000FFA28400\r\n      0000FFA284000000000000000000000000000000800000800000800000800000\r\n      800000800000800000000000000000000000000000000000FFA284000000FFA2\r\n      8400000000000000000000000000000000000000000000000000000000000000\r\n      0000000000000000000000000000000000000000FFA284000000FFA284FFA284\r\n      0000000000000000000000000000000000000000000000000000000000000000\r\n      00000000000000000000000000FFA284FFA284000000000000FFA284FFA284FF\r\n      A284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284FFA284\r\n      FFA284FFA284FFA284FFA28400000000000080003FFF3FFF9FFF7E0FDFFF7803\r\n      DFFF7001DFFF6000DFFF6000DFFF40005FFF40005FFF40005FFF40005FFF6000\r\n      DFFF6000DFFF7001DFFF7803DFFF7C07DFFF7FFFDFFF3FFF9FFF80003FFF}\r\n  end\r\n  object TPointTextDrawer\r\n    Font.Charset = RUSSIAN_CHARSET\r\n    Font.Color = clBlack\r\n    Font.Height = -11\r\n    Font.Name = \'Arial\'\r\n    Font.Style = []\r\n    OutlineColor = clWhite\r\n    VAlignment = vaTop\r\n    HAlignment = haLeft\r\n  end\r\nend',NULL);
/*!40000 ALTER TABLE `mobitels_info` ENABLE KEYS */;


--
-- Definition of table `online`
--

DROP TABLE IF EXISTS `online`;
CREATE TABLE `online` (
  `DataGps_ID` int(11) NOT NULL auto_increment,
  `Mobitel_ID` int(11) default '0',
  `Message_ID` int(11) default '0',
  `Latitude` int(11) NOT NULL default '0',
  `Longitude` int(11) NOT NULL default '0',
  `Altitude` int(11) default '0',
  `UnixTime` int(11) NOT NULL default '0',
  `Speed` smallint(6) NOT NULL default '0',
  `Direction` int(11) NOT NULL default '0',
  `Valid` tinyint(4) NOT NULL default '1',
  `InMobitelID` int(11) default '0',
  `Events` int(10) unsigned default '0',
  `Sensor1` tinyint(4) unsigned default '0',
  `Sensor2` tinyint(4) unsigned default '0',
  `Sensor3` tinyint(4) unsigned default '0',
  `Sensor4` tinyint(4) unsigned default '0',
  `Sensor5` tinyint(4) unsigned default '0',
  `Sensor6` tinyint(4) unsigned default '0',
  `Sensor7` tinyint(4) unsigned default '0',
  `Sensor8` tinyint(4) unsigned default '0',
  `LogID` int(11) default '0',
  `isShow` tinyint(4) default '0',
  `whatIs` smallint(6) unsigned NOT NULL default '0',
  `Counter1` int(11) NOT NULL default '-1',
  `Counter2` int(11) NOT NULL default '-1',
  `Counter3` int(11) NOT NULL default '-1',
  `Counter4` int(11) NOT NULL default '-1',
  PRIMARY KEY  (`DataGps_ID`),
  KEY `IDX_MobitelidLogid` USING BTREE (`Mobitel_ID`,`LogID`),
  KEY `IDX_MobitelidUnixtimeValid` USING BTREE (`Mobitel_ID`,`UnixTime`,`Valid`),
  KEY `IDX_MobitelidUnixtime` USING BTREE (`Mobitel_ID`,`UnixTime`),
  KEY `IDX_Mobitelid` USING BTREE (`Mobitel_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='Оперативные данные';

--
-- Dumping data for table `online`
--

/*!40000 ALTER TABLE `online` DISABLE KEYS */;
/*!40000 ALTER TABLE `online` ENABLE KEYS */;


--
-- Definition of table `points`
--

DROP TABLE IF EXISTS `points`;
CREATE TABLE `points` (
  `Point_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Latitude` int(11) default '0',
  `Longitude` int(11) default '0',
  `Zone_ID` int(11) default '0',
  PRIMARY KEY  (`Point_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `points`
--

/*!40000 ALTER TABLE `points` DISABLE KEYS */;
/*!40000 ALTER TABLE `points` ENABLE KEYS */;


--
-- Definition of table `relationalgorithms`
--

DROP TABLE IF EXISTS `relationalgorithms`;
CREATE TABLE `relationalgorithms` (
  `ID` int(11) NOT NULL auto_increment,
  `AlgorithmID` int(11) NOT NULL default '0',
  `SensorID` int(11) NOT NULL default '0',
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

--
-- Dumping data for table `relationalgorithms`
--

/*!40000 ALTER TABLE `relationalgorithms` DISABLE KEYS */;
/*!40000 ALTER TABLE `relationalgorithms` ENABLE KEYS */;


--
-- Definition of table `route_items`
--

DROP TABLE IF EXISTS `route_items`;
CREATE TABLE `route_items` (
  `ID` int(11) NOT NULL auto_increment,
  `Seq` int(11) default NULL,
  `Route_ID` int(11) default NULL,
  `Zone_ID` int(11) default NULL,
  `Zone_IO` int(11) default NULL,
  `GoNext_Min` int(11) default NULL,
  `GoNext_Max` int(11) default NULL,
  `Link_Item` int(11) default NULL,
  `Link_Info` int(11) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `route_items`
--

/*!40000 ALTER TABLE `route_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `route_items` ENABLE KEYS */;


--
-- Definition of table `route_list`
--

DROP TABLE IF EXISTS `route_list`;
CREATE TABLE `route_list` (
  `Route_ID` int(11) NOT NULL auto_increment,
  `GIS` blob,
  `Name` varchar(50) default NULL,
  `Descr` varchar(200) default NULL,
  `Modified` int(11) default NULL,
  PRIMARY KEY  (`Route_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `route_list`
--

/*!40000 ALTER TABLE `route_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `route_list` ENABLE KEYS */;


--
-- Definition of table `route_lists`
--

DROP TABLE IF EXISTS `route_lists`;
CREATE TABLE `route_lists` (
  `ID` int(11) NOT NULL auto_increment,
  `Parent` int(11) default NULL,
  `Title` varchar(70) default NULL,
  `Data` text,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `route_lists`
--

/*!40000 ALTER TABLE `route_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `route_lists` ENABLE KEYS */;


--
-- Definition of table `route_mobitel_links`
--

DROP TABLE IF EXISTS `route_mobitel_links`;
CREATE TABLE `route_mobitel_links` (
  `ID` int(11) NOT NULL auto_increment,
  `List_ID` int(11) default NULL,
  `Route_ID` int(11) default NULL,
  `Mobitel_ID` int(11) default NULL,
  `Time1` int(11) default NULL,
  `Time2` int(11) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `route_mobitel_links`
--

/*!40000 ALTER TABLE `route_mobitel_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `route_mobitel_links` ENABLE KEYS */;


--
-- Definition of table `route_mobitel_plans`
--

DROP TABLE IF EXISTS `route_mobitel_plans`;
CREATE TABLE `route_mobitel_plans` (
  `ID` int(11) NOT NULL auto_increment,
  `Title` varchar(70) default NULL,
  `Time1` int(11) default NULL,
  `Time2` int(11) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `route_mobitel_plans`
--

/*!40000 ALTER TABLE `route_mobitel_plans` DISABLE KEYS */;
/*!40000 ALTER TABLE `route_mobitel_plans` ENABLE KEYS */;


--
-- Definition of table `route_points`
--

DROP TABLE IF EXISTS `route_points`;
CREATE TABLE `route_points` (
  `ID` int(11) NOT NULL auto_increment,
  `Route_ID` int(11) default NULL,
  `Zone_ID` int(11) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `route_points`
--

/*!40000 ALTER TABLE `route_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `route_points` ENABLE KEYS */;


--
-- Definition of table `rules`
--

DROP TABLE IF EXISTS `rules`;
CREATE TABLE `rules` (
  `Rule_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Password` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) NOT NULL default '0',
  `Rule1` tinyint(4) NOT NULL default '0',
  `Rule2` tinyint(4) NOT NULL default '0',
  `Rule3` tinyint(4) NOT NULL default '0',
  `Rule4` tinyint(4) NOT NULL default '0',
  `Rule5` tinyint(4) NOT NULL default '0',
  `Rule6` tinyint(4) NOT NULL default '0',
  `Rule7` tinyint(4) NOT NULL default '0',
  `Rule8` tinyint(4) NOT NULL default '0',
  `Rule9` tinyint(4) NOT NULL default '0',
  `Rule10` tinyint(4) NOT NULL default '0',
  `Rule11` tinyint(4) NOT NULL default '0',
  `Rule12` tinyint(4) NOT NULL default '0',
  `Rule13` tinyint(4) NOT NULL default '0',
  `Rule14` tinyint(4) NOT NULL default '0',
  `Rule15` tinyint(4) NOT NULL default '0',
  `Rule16` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`Rule_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `rules`
--

/*!40000 ALTER TABLE `rules` DISABLE KEYS */;
INSERT INTO `rules` (`Rule_ID`,`Name`,`Password`,`Descr`,`ID`,`Rule1`,`Rule2`,`Rule3`,`Rule4`,`Rule5`,`Rule6`,`Rule7`,`Rule8`,`Rule9`,`Rule10`,`Rule11`,`Rule12`,`Rule13`,`Rule14`,`Rule15`,`Rule16`) VALUES 
 (1,'guest','guest','guest account',0,1,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0),
 (8,'power','power','Description for Power User Profile',0,1,1,1,1,1,0,0,1,1,0,0,0,0,0,0,0),
 (9,'root','root','Description for root  Profile',0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0);
/*!40000 ALTER TABLE `rules` ENABLE KEYS */;


--
-- Definition of table `sensor_hookups`
--

DROP TABLE IF EXISTS `sensor_hookups`;
CREATE TABLE `sensor_hookups` (
  `ID` int(11) NOT NULL auto_increment,
  `Mobitel_ID` int(11) default NULL,
  `Sensor_ID` int(11) default NULL,
  `FirstBit` int(11) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `sensor_hookups`
--

/*!40000 ALTER TABLE `sensor_hookups` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensor_hookups` ENABLE KEYS */;


--
-- Definition of table `sensor_list`
--

DROP TABLE IF EXISTS `sensor_list`;
CREATE TABLE `sensor_list` (
  `ID` int(11) NOT NULL auto_increment,
  `Title` varchar(70) default NULL,
  `Kind` varchar(20) default NULL,
  `BitsCount` int(11) default NULL,
  `FirstBit` int(11) default NULL,
  `Data` text,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `sensor_list`
--

/*!40000 ALTER TABLE `sensor_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensor_list` ENABLE KEYS */;


--
-- Definition of table `sensoralgorithms`
--

DROP TABLE IF EXISTS `sensoralgorithms`;
CREATE TABLE `sensoralgorithms` (
  `ID` int(11) NOT NULL auto_increment,
  `Name` char(45) NOT NULL,
  `Description` char(45) default NULL,
  `AlgorithmID` int(11) NOT NULL default '0',
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `ID` USING BTREE (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `sensoralgorithms`
--

/*!40000 ALTER TABLE `sensoralgorithms` DISABLE KEYS */;
INSERT INTO `sensoralgorithms` (`ID`,`Name`,`Description`,`AlgorithmID`) VALUES 
 (1,'Работа оборудования','Для анализа используется критерий вкл. выкл.',1),
 (2,'Работа основного двигателя','Обычно используется при подключении к заж.',2),
 (3,'Обороты оборудования','Для анализа используются к-во оборотов',3),
 (4,'Напряжение','Обычно контролируется борт сеть',4),
 (5,'Оборотты основного двигателя','Для анализа используются к-во оборотов',5),
 (6,'Температура','при использовании оборотов',6),
 (7,'Топливо1','Уровень топлива в баке1',7),
 (8,'Топливо2','Уровень топлива в баке2',8),
 (9,'Пассажиры1','Для подсчета количества пассажиров1',9),
 (10,'Пассажиры2','Для подсчета количества пассажиров2',10),
 (11,'Дверь1','Для пассажиров1',11),
 (12,'Дверь2','Для пассажиров2',12);
/*!40000 ALTER TABLE `sensoralgorithms` ENABLE KEYS */;


--
-- Definition of table `sensorcoefficient`
--

DROP TABLE IF EXISTS `sensorcoefficient`;
CREATE TABLE `sensorcoefficient` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `Sensor_id` int(10) unsigned NOT NULL,
  `UserValue` double(10,5) NOT NULL,
  `SensorValue` double(10,5) NOT NULL,
  `K` double(10,5) NOT NULL default '1.00000',
  `b` double(10,5) NOT NULL default '0.00000',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `Index_2` USING BTREE (`Sensor_id`,`UserValue`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

--
-- Dumping data for table `sensorcoefficient`
--

/*!40000 ALTER TABLE `sensorcoefficient` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensorcoefficient` ENABLE KEYS */;


--
-- Definition of table `sensordata`
--

DROP TABLE IF EXISTS `sensordata`;
CREATE TABLE `sensordata` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `datagps_id` int(10) unsigned NOT NULL,
  `sensor_id` int(10) unsigned NOT NULL,
  `Value` double(10,2) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `Index_2` USING BTREE (`datagps_id`,`sensor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

--
-- Dumping data for table `sensordata`
--

/*!40000 ALTER TABLE `sensordata` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensordata` ENABLE KEYS */;


--
-- Definition of table `sensors`
--

DROP TABLE IF EXISTS `sensors`;
CREATE TABLE `sensors` (
  `id` int(11) NOT NULL auto_increment,
  `mobitel_id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Description` varchar(200) NOT NULL,
  `StartBit` int(10) unsigned NOT NULL,
  `Length` int(10) unsigned NOT NULL,
  `NameUnit` varchar(45) NOT NULL,
  `K` double(10,5) NOT NULL default '0.00000',
  PRIMARY KEY  (`id`),
  KEY `Index_2` (`mobitel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

--
-- Dumping data for table `sensors`
--

/*!40000 ALTER TABLE `sensors` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensors` ENABLE KEYS */;


--
-- Definition of table `serviceinit`
--

DROP TABLE IF EXISTS `serviceinit`;
CREATE TABLE `serviceinit` (
  `ServiceInit_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `ServiceType_ID` int(11) default '0',
  `A1` char(50) default NULL,
  `A2` char(50) default NULL,
  `A3` char(50) default NULL,
  `A4` char(50) default NULL,
  `A5` char(50) default NULL,
  `A6` char(50) default NULL,
  `A7` char(50) default NULL,
  `A8` char(50) default NULL,
  `A9` char(50) default NULL,
  `A10` char(200) default NULL,
  `CAtoSAint` int(11) default '0',
  `CAtoSAtime` int(11) default '0',
  `SAtoCAint` int(11) default '0',
  `SAtoCAtime` int(11) default '0',
  `SAtoCAtext` char(50) default 'STOPPED',
  `CAtoSAtext` char(50) default 'STOPPED',
  `MobitelActive_ID` int(11) NOT NULL default '0',
  `Active` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`ServiceInit_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `serviceinit`
--

/*!40000 ALTER TABLE `serviceinit` DISABLE KEYS */;
INSERT INTO `serviceinit` (`ServiceInit_ID`,`Name`,`Descr`,`ID`,`Flags`,`ServiceType_ID`,`A1`,`A2`,`A3`,`A4`,`A5`,`A6`,`A7`,`A8`,`A9`,`A10`,`CAtoSAint`,`CAtoSAtime`,`SAtoCAint`,`SAtoCAtime`,`SAtoCAtext`,`CAtoSAtext`,`MobitelActive_ID`,`Active`) VALUES 
 (1,'Direct Cable','Сервис для прямого кабельного соединения',0,0,5,'COM1','115200','','','','','','','','',0,0,1,1222352358,'Running',NULL,83,0),
 (7,'GSM модем','Отправка SMS по  GSM каналу',0,0,2,'COM2','115200','','','','','','','','XXX',0,0,1,1127307060,'Running',NULL,75,0),
 (29,'EMail','Сервис отправки данных через E-mail',29,1125886839,6,'','','','smtp.p314.net','pop.p314.net','25','110','60','','XXX',0,0,1,1128962351,'Running',NULL,68,0),
 (30,'Online','Онлайн через телематический сервер',30,0,3,'','9010','','','','','','','','',0,0,0,0,'STOPPED','STOPPED',0,0);
/*!40000 ALTER TABLE `serviceinit` ENABLE KEYS */;


--
-- Definition of table `servicesend`
--

DROP TABLE IF EXISTS `servicesend`;
CREATE TABLE `servicesend` (
  `ServiceSend_ID` int(11) NOT NULL auto_increment,
  `Name` varchar(50) default NULL,
  `Descr` varchar(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `telMobitel` varchar(64) default NULL,
  `smsMobitel` varchar(64) default NULL,
  `emailMobitel` varchar(64) default NULL,
  `IPMobitel` varchar(64) default NULL,
  `URLMobitel` varchar(64) default NULL,
  `PortMobitel` int(11) default '0',
  `RfMobitel` varchar(50) default NULL,
  `Message_ID` int(11) default '0',
  PRIMARY KEY  (`ServiceSend_ID`),
  KEY `Index_2` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `servicesend`
--

/*!40000 ALTER TABLE `servicesend` DISABLE KEYS */;
INSERT INTO `servicesend` (`ServiceSend_ID`,`Name`,`Descr`,`ID`,`Flags`,`telMobitel`,`smsMobitel`,`emailMobitel`,`IPMobitel`,`URLMobitel`,`PortMobitel`,`RfMobitel`,`Message_ID`) VALUES 
 (250,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',250,0,NULL,NULL,NULL,NULL,NULL,0,NULL,0),
 (252,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',250,0,'98','106','3051@p314.net','',NULL,0,'',200892),
 (255,'Record 22.09.2005 12:14:57','Description for record created at 22.09.2005 12:14:57',250,0,'96','80','','',NULL,0,'',218879);
/*!40000 ALTER TABLE `servicesend` ENABLE KEYS */;


--
-- Definition of table `servicetypes`
--

DROP TABLE IF EXISTS `servicetypes`;
CREATE TABLE `servicetypes` (
  `ServiceType_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `Flags` int(11) default '0',
  `D1` char(200) default NULL,
  `D2` char(200) default NULL,
  `D3` char(200) default NULL,
  `D4` char(200) default NULL,
  `D5` char(200) default NULL,
  `D6` char(200) default NULL,
  `D7` char(200) default NULL,
  `D8` char(200) default NULL,
  `D9` char(200) default NULL,
  `D10` char(200) default NULL,
  PRIMARY KEY  (`ServiceType_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `servicetypes`
--

/*!40000 ALTER TABLE `servicetypes` DISABLE KEYS */;
INSERT INTO `servicetypes` (`ServiceType_ID`,`Name`,`Descr`,`Flags`,`D1`,`D2`,`D3`,`D4`,`D5`,`D6`,`D7`,`D8`,`D9`,`D10`) VALUES 
 (1,'reserved','Зарезервировано',0,'Parameter 1','Parameter 2','Parameter 3','','','','','','',''),
 (2,'GSM','Тип сервиса для отправки SMS через GSM соединение',0,'Номер порта','Скорость порта','','','','','','','',''),
 (3,'Online','Online',0,'IP','Port','Login','Password','','','','','',''),
 (4,'RF modem','Радиомодем (не реализовано)',0,'Номер порта','Скорость порта','','','','','','','',''),
 (5,'DC','Тип сервиса \"прямое кабельное соединения\"',0,'Номер порта','Скорость порта',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
 (6,'E-Mail','Тип сервиса \" E-Mail \" для отправки SMS через электронную почту',0,'EMAIL','Login','Password','Smtp Server','Pop3 Server','Smtp Port','Pop3 Port','Check Out time',NULL,NULL);
/*!40000 ALTER TABLE `servicetypes` ENABLE KEYS */;


--
-- Definition of table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL auto_increment,
  `TimeBreak` time default '00:01:00' COMMENT 'Минимальное время остановки',
  `RotationMain` int(11) default '100' COMMENT 'Обороты основного двигателя',
  `RotationAdd` int(11) default '100' COMMENT 'Обороты доп. двигателя',
  `FuelingEdge` int(11) default '0' COMMENT 'Минимальная заправка',
  `band` int(11) default '25' COMMENT 'Полоса осреднения',
  `FuelingDischarge` int(11) default '0' COMMENT 'Минимальный слив топлива',
  `accelMax` double default '1' COMMENT 'Максимальное ускорение',
  `breakMax` double default '1' COMMENT 'Максимальное торможение',
  `speedMax` double default '70' COMMENT 'Максимальная скорость',
  `idleRunningMain` int(11) default '0' COMMENT 'Холостой ход основного двигателя',
  `idleRunningAdd` int(11) default '0' COMMENT 'Холосой ход доп. двигателя',
  `Name` char(20) default 'Установки' COMMENT 'Название',
  `Desc` char(80) default 'Описание установки' COMMENT 'Описание ',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id` USING BTREE (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `setting`
--

/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` (`id`,`TimeBreak`,`RotationMain`,`RotationAdd`,`FuelingEdge`,`band`,`FuelingDischarge`,`accelMax`,`breakMax`,`speedMax`,`idleRunningMain`,`idleRunningAdd`,`Name`,`Desc`) VALUES 
 (1,'00:01:00',100,100,50,25,10,1,1,70,0,0,'Настройка 1','Настройка по умолчанию');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;


--
-- Definition of table `smsnumbers`
--

DROP TABLE IF EXISTS `smsnumbers`;
CREATE TABLE `smsnumbers` (
  `SmsNumber_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `smsNumber` char(14) default NULL,
  PRIMARY KEY  (`SmsNumber_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `smsnumbers`
--

/*!40000 ALTER TABLE `smsnumbers` DISABLE KEYS */;
INSERT INTO `smsnumbers` (`SmsNumber_ID`,`Name`,`Descr`,`ID`,`Flags`,`smsNumber`) VALUES 
 (9,'UMC SMS Center','UMC SMS Center descr',0,0,'+380500005010'),
 (42,'Golden SMS Center','Golden SMS Center descr',0,0,'+380390001008'),
 (53,'GOLDEN SMS Шлюз','Номер телефона, на который отправляются данные с ТТ через E_MAIL',0,0,'025'),
 (80,'SMS',' (SMS)',0,0,'+38067'),
 (81,'RCSB (SMS)','Телефон (SMS)',0,0,'+380672452724'),
 (82,'RCSB2 (SMS)',' (SMS)',0,0,'+380672452734'),
 (83,'RCS1 (SMS)',' (SMS)',0,0,'+380637562677'),
 (84,'RCS2 (SMS)',' (SMS)',0,0,'+380933222277'),
 (85,'RCS3 (SMS)',' (SMS)',0,0,'+380637562678');
/*!40000 ALTER TABLE `smsnumbers` ENABLE KEYS */;


--
-- Definition of table `sources`
--

DROP TABLE IF EXISTS `sources`;
CREATE TABLE `sources` (
  `Source_ID` int(11) NOT NULL auto_increment,
  `SourceText` char(200) default NULL,
  `SourceError` char(200) default NULL,
  PRIMARY KEY  (`Source_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `sources`
--

/*!40000 ALTER TABLE `sources` DISABLE KEYS */;
/*!40000 ALTER TABLE `sources` ENABLE KEYS */;


--
-- Definition of table `specialmessages`
--

DROP TABLE IF EXISTS `specialmessages`;
CREATE TABLE `specialmessages` (
  `SpecMessage_ID` int(11) NOT NULL auto_increment,
  `DataString` char(50) default NULL,
  `DataInteger` int(11) default '0',
  `unixtime` char(50) default NULL,
  `isNew` int(11) default '0',
  PRIMARY KEY  (`SpecMessage_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `specialmessages`
--

/*!40000 ALTER TABLE `specialmessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `specialmessages` ENABLE KEYS */;


--
-- Definition of table `swupdate`
--

DROP TABLE IF EXISTS `swupdate`;
CREATE TABLE `swupdate` (
  `ID` int(11) NOT NULL auto_increment,
  `ShortID` char(4) NOT NULL COMMENT 'Идентификатор телетрека(логин)',
  `Soft` mediumtext NOT NULL COMMENT 'Прошивка',
  `InsertTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT 'Дата и время вставки в таблицу',
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COMMENT='Обновление прошивки телетрека';

--
-- Dumping data for table `swupdate`
--

/*!40000 ALTER TABLE `swupdate` DISABLE KEYS */;
/*!40000 ALTER TABLE `swupdate` ENABLE KEYS */;


--
-- Definition of table `team`
--

DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` int(11) NOT NULL auto_increment,
  `Name` char(20) default NULL COMMENT 'Название группы',
  `Descr` text COMMENT 'Описание',
  `Setting_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `Setting_id` USING BTREE (`Setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `team`
--

/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` (`id`,`Name`,`Descr`,`Setting_id`) VALUES 
 (1,'Автомобиль',NULL,1),
 (2,'Трактор',NULL,1),
 (3,'Комбайн',NULL,1),
 (4,'Заправщик',NULL,1),
 (5,'Кран',NULL,1);
/*!40000 ALTER TABLE `team` ENABLE KEYS */;


--
-- Definition of table `telnumbers`
--

DROP TABLE IF EXISTS `telnumbers`;
CREATE TABLE `telnumbers` (
  `TelNumber_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Flags` int(11) default '0',
  `TelNumber` char(50) default NULL,
  PRIMARY KEY  (`TelNumber_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `telnumbers`
--

/*!40000 ALTER TABLE `telnumbers` DISABLE KEYS */;
INSERT INTO `telnumbers` (`TelNumber_ID`,`Name`,`Descr`,`ID`,`Flags`,`TelNumber`) VALUES 
 (56,'RCSB','Телефон',0,0,'80672452724'),
 (57,'Телефон','Телефон',0,0,'22222222222'),
 (58,'Телефон','Телефон',0,0,'33333333333'),
 (59,'Телефон','Телефон',0,0,'44444444444'),
 (68,'Телефон','Телефон',0,0,'55555555555'),
 (71,'Телефон','',0,0,'66666666666'),
 (73,'Телефон','Телефон',0,0,'77777777777'),
 (74,'Телефон','Телефон',0,0,'88888888888'),
 (75,'Телефон','',0,0,'99999999999'),
 (81,'RCS3','',0,0,'80637562678'),
 (89,'RCS2','',0,0,'80933222277'),
 (90,'RCS1','',0,0,'80637562677'),
 (97,'RCSB2','',0,0,'80672452734'),
 (101,'Телефон','',0,0,'1');
/*!40000 ALTER TABLE `telnumbers` ENABLE KEYS */;


--
-- Definition of table `temp`
--

DROP TABLE IF EXISTS `temp`;
CREATE TABLE `temp` (
  `datagps_id` int(10) unsigned NOT NULL default '0',
  `sensor_id` int(10) unsigned NOT NULL default '0',
  `Value` double(10,2) NOT NULL default '0.00'
) ENGINE=MEMORY DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `temp`
--

/*!40000 ALTER TABLE `temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp` ENABLE KEYS */;


--
-- Definition of table `tmpdatagps`
--

DROP TABLE IF EXISTS `tmpdatagps`;
CREATE TABLE `tmpdatagps` (
  `DataGps_ID` int(11) NOT NULL default '0',
  `Mobitel_ID` int(11) default '0',
  `Message_ID` int(11) default '0',
  `Latitude` int(11) default '0',
  `Longitude` int(11) default '0',
  `Altitude` int(11) default NULL,
  `UnixTime` int(11) default '0',
  `Speed` smallint(6) default '0',
  `Direction` int(11) NOT NULL default '0',
  `Valid` tinyint(4) default '1',
  `InMobitelID` int(11) default NULL,
  `Events` int(10) unsigned default '0',
  `Sensor1` tinyint(4) unsigned default '0',
  `Sensor2` tinyint(4) unsigned default '0',
  `Sensor3` tinyint(4) unsigned default '0',
  `Sensor4` tinyint(4) unsigned default '0',
  `Sensor5` tinyint(4) unsigned default '0',
  `Sensor6` tinyint(4) unsigned default '0',
  `Sensor7` tinyint(4) unsigned default '0',
  `Sensor8` tinyint(4) unsigned default '0',
  `LogID` int(11) default '0',
  `isShow` tinyint(4) default '0',
  `whatIs` smallint(6) unsigned default '0',
  `Counter1` int(11) NOT NULL default '-1',
  `Counter2` int(11) NOT NULL default '-1',
  `Counter3` int(11) NOT NULL default '-1',
  `Counter4` int(11) NOT NULL default '-1'
) ENGINE=MEMORY DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `tmpdatagps`
--

/*!40000 ALTER TABLE `tmpdatagps` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmpdatagps` ENABLE KEYS */;


--
-- Definition of table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE `vehicle` (
  `id` int(11) NOT NULL auto_increment,
  `MakeCar` text COMMENT 'Марка автомобиля',
  `NumberPlate` text COMMENT 'Номерной знак',
  `Team_id` int(11) default NULL,
  `Mobitel_id` int(11) default NULL,
  `Odometr` int(11) default '0' COMMENT 'Показания одометра',
  `CarModel` text COMMENT 'Модель',
  `setting_id` int(11) default '1',
  `odoTime` datetime default NULL COMMENT 'Дата и время сверки одометра',
  `driver_id` int(11) default '-1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `Mobitel_id` USING BTREE (`Mobitel_id`),
  KEY `Team_id` USING BTREE (`Team_id`),
  KEY `setting_id` USING BTREE (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `vehicle`
--

/*!40000 ALTER TABLE `vehicle` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle` ENABLE KEYS */;


--
-- Definition of table `ver`
--

DROP TABLE IF EXISTS `ver`;
CREATE TABLE `ver` (
  `id` int(11) NOT NULL auto_increment,
  `Num` int(11) unsigned NOT NULL default '0' COMMENT 'Номер текущей сборки БД',
  `Desc` text NOT NULL COMMENT 'Описание текущей сборки БД',
  `time` datetime default NULL COMMENT 'Время обновления',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `ver`
--

/*!40000 ALTER TABLE `ver` DISABLE KEYS */;
INSERT INTO `ver` (`id`,`Num`,`Desc`,`time`) VALUES 
 (1,1,'Сборка от 9.12.2008','2008-12-09 00:00:00'),
 (2,2,'Сборка от 10.02.2009','2009-02-10 00:00:00');
/*!40000 ALTER TABLE `ver` ENABLE KEYS */;


--
-- Definition of table `zonerelations`
--

DROP TABLE IF EXISTS `zonerelations`;
CREATE TABLE `zonerelations` (
  `ZoneRelation_ID` int(11) NOT NULL auto_increment,
  `Zone_ID` int(11) default '0',
  `ConfigZoneSet_ID` int(11) default '0',
  `In_flag` tinyint(4) NOT NULL default '0',
  `Out_flag` tinyint(4) NOT NULL default '0',
  `In_flag2` tinyint(4) NOT NULL default '0',
  `Out_flag2` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`ZoneRelation_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `zonerelations`
--

/*!40000 ALTER TABLE `zonerelations` DISABLE KEYS */;
INSERT INTO `zonerelations` (`ZoneRelation_ID`,`Zone_ID`,`ConfigZoneSet_ID`,`In_flag`,`Out_flag`,`In_flag2`,`Out_flag2`) VALUES 
 (220,78,248,1,0,0,0),
 (221,78,249,1,0,0,0);
/*!40000 ALTER TABLE `zonerelations` ENABLE KEYS */;


--
-- Definition of table `zones`
--

DROP TABLE IF EXISTS `zones`;
CREATE TABLE `zones` (
  `Zone_ID` int(11) NOT NULL auto_increment,
  `Name` char(50) default NULL,
  `Descr` char(200) default NULL,
  `ID` int(11) default '0',
  `Status` tinyint(4) default '0',
  PRIMARY KEY  (`Zone_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `zones`
--

/*!40000 ALTER TABLE `zones` DISABLE KEYS */;
/*!40000 ALTER TABLE `zones` ENABLE KEYS */;


--
-- Definition of function `CheckVersion`
--

DROP FUNCTION IF EXISTS `CheckVersion`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */ $$
CREATE DEFINER=`root`@`localhost` FUNCTION `CheckVersion`(maskVersion TINYTEXT) RETURNS tinyint(4)
    SQL SECURITY INVOKER
    COMMENT 'Проверка версии СУБД. Должна быть maskVersion или выше. Результа'
BEGIN
  DECLARE currentVersion TINYTEXT;
  SELECT Left(VERSION(), Length(maskVersion)) INTO currentVersion;
  RETURN currentVersion >= maskVersion;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of function `HaveSuperPrivileges`
--

DROP FUNCTION IF EXISTS `HaveSuperPrivileges`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */ $$
CREATE DEFINER=`root`@`localhost` FUNCTION `HaveSuperPrivileges`() RETURNS tinyint(4)
    SQL SECURITY INVOKER
    COMMENT 'Проверка, имеет ли пользователь привилегию SUPER. Результат: 1 -'
BEGIN
  DECLARE usrstr TINYTEXT DEFAULT '';

  DECLARE usr TINYTEXT DEFAULT '';
  DECLARE hst TINYTEXT DEFAULT '';
  DECLARE pos TINYINT;

  SELECT USER() INTO usrstr;

  SELECT INSTR(usrstr, '@') INTO pos;


  SELECT Left(usrstr, pos - 1), Right(usrstr, Length(usrstr) - pos) INTO usr, hst;

  RETURN (
    SELECT COUNT(*) > 0
    FROM mysql.`user` u
    WHERE u.`User` = usr AND u.Host = hst AND Super_priv = 'Y'
  );
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `CheckDB`
--

DROP PROCEDURE IF EXISTS `CheckDB`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CheckDB`()
    SQL SECURITY INVOKER
    COMMENT 'Проверка БД при подключении.'
BEGIN

  SELECT 'OK';

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `CheckOdo`
--

DROP PROCEDURE IF EXISTS `CheckOdo`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CheckOdo`(IN m_id INTEGER(11), IN end_time DATETIME)
    SQL SECURITY INVOKER
BEGIN
SELECT 
 FROM_UNIXTIME(datagps.UnixTime) AS `time` ,
 (datagps.Latitude / 600000.00000) AS Lat,
 (datagps.Longitude / 600000.00000) AS Lon
 
FROM
  datagps
WHERE
  datagps.Mobitel_ID = m_id AND 
  datagps.UnixTime BETWEEN (SELECT UNIX_TIMESTAMP(odoTime)  FROM vehicle WHERE Mobitel_id = m_id) AND 
  UNIX_TIMESTAMP(end_time) AND 
  Valid = 1  ;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `dataview`
--

DROP PROCEDURE IF EXISTS `dataview`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `dataview`(IN m_id INTEGER(11), IN val BOOL, IN begin_time DATETIME, IN end_time DATETIME)
    SQL SECURITY INVOKER
BEGIN
select `datagps`.`DataGps_ID` AS `DataGps_ID`,
`datagps`.`Mobitel_ID` AS `Mobitel_ID`,
(`datagps`.`Latitude` / 600000.00000) AS `Lat`,
(`datagps`.`Longitude` / 600000.00000) AS `Lon`,
`datagps`.`Altitude` AS `Altitude`,
from_unixtime(`datagps`.`UnixTime`) AS `time`,
(`datagps`.`Speed` * 1.852) AS `speed`,
((`datagps`.`Direction` * 360) / 255) AS `direction`,
`datagps`.`Valid` AS `Valid`,
(((((((`datagps`.`Sensor8` + 
(`datagps`.`Sensor7` << 8)) + 
(`datagps`.`Sensor6` << 16)) + 
(`datagps`.`Sensor5` << 24)) + 
(`datagps`.`Sensor4` << 32)) + 
(`datagps`.`Sensor3` << 40)) + 
(`datagps`.`Sensor2` << 48)) + 
(`datagps`.`Sensor1` << 56)) AS `sensor`,
`datagps`.`Events` AS `Events`,
`datagps`.`LogID` AS `LogID` 
from `datagps` 
where                                                                                                                                    
    (`datagps`.`Valid` = val) and                                                                                                            
    ( datagps.UnixTime BETWEEN UNIX_TIMESTAMP(begin_time) and UNIX_TIMESTAMP(end_time))                                                    
    and datagps.Mobitel_ID = m_id       
order by `datagps`.`LogID`;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `DrListMobitelConfig`
--

DROP PROCEDURE IF EXISTS `DrListMobitelConfig`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DrListMobitelConfig`()
    SQL SECURITY INVOKER
    COMMENT 'Выборка настроек телетреков в режиме DirectOnline'
BEGIN
  SELECT m.Mobitel_id AS MobitelID, 
    imc.DevIdShort AS DevIdShort,
    cge.Pop3un AS Login, 
    cge.Pop3pw AS Password
  FROM Mobitels m JOIN ConfigMain cm ON m.Mobitel_id = cm.Mobitel_ID
    JOIN ConfigGprsEmail cge ON cm.ConfigGprsEmail_ID = cge.ID
    JOIN internalmobitelconfig imc ON m.InternalMobitelConfig_ID = imc.ID 
  WHERE (cge.ConfigGprsEmail_ID = (
      SELECT MAX(intCge.ConfigGprsEmail_ID)
      FROM ConfigGprsEmail intCge
      WHERE intCge.ID = cge.ID)) AND
    (imc.InternalMobitelConfig_ID = (
      SELECT MAX(intConf.InternalMobitelConfig_ID)
      FROM internalmobitelconfig intConf
      WHERE intConf.ID = imc.ID)) AND 
    (imc.devIdShort IS NOT NULL) AND (imc.devIdShort <> _cp1251 '')	AND 
    (cge.Pop3un IS NOT NULL) AND (cge.Pop3un <> _cp1251 '') 
  ORDER BY m.Mobitel_ID;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `DrTransferBuffer`
--

DROP PROCEDURE IF EXISTS `DrTransferBuffer`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DrTransferBuffer`()
    SQL SECURITY INVOKER
    COMMENT 'Переносит GPS данные DirectOnline из буферной таблицы в datagps'
BEGIN
  DECLARE MobitelIDInCursor INT; /* Текущее значение MobitelID в курсоре*/
  DECLARE DataExists TINYINT DEFAULT 1; /* Флаг наличия данных после фетча */
  
  DECLARE TmpMaxUnixTime INT; /* Временная переменная для хранения Max UnixTime из таблицы online */

  DECLARE CursorMobitels CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_dr;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;


  /* -------------------- Заполнение таблицы Online ---------------------- */  
  /* Удаление из таблицы online дублирующих строк таблицы datagpsbuffer_dr */
  /* по полям Mobitel_ID и LogID */
  DELETE o
  FROM online o, datagpsbuffer_dr dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);
  
  SET DataExists = 1;
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* Нахождение Max значения UnixTime в таблице online для заданного
       телетрека. p.s. UnixTime в таблице online только валидный */
    SELECT COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online FORCE INDEX (IDX_Mobitelid)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* Вставка новых записей (максимум 100) каждого телетрека в online */
    INSERT INTO online (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs
    FROM (
      SELECT *
      FROM datagpsbuffer_dr
      WHERE (Mobitel_ID = MobitelIDInCursor)  AND 
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
      GROUP BY Mobitel_ID, LogID
      ORDER BY UnixTime DESC
      LIMIT 100) AS T
    ORDER BY UnixTime ASC;

    /* Удаление строк из таблицы оперативных данных не удовлетворяющие
       условиям хранения - не являются последними 100 строками */
    DELETE FROM online
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < 
      (SELECT COALESCE(MIN(UnixTime), 0) 
       FROM (
         SELECT UnixTime
         FROM online
         WHERE Mobitel_ID = MobitelIDInCursor 
         ORDER BY UnixTime DESC
         LIMIT 100) T1)); 
  
    FETCH CursorMobitels INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitels;
  /* ---------------- Online ---------------- */ 


  /* Зачем-то нужен апдейт ... */
  UPDATE datagps d, datagpsbuffer_dr b SET
    d.Latitude = b.Latitude,
    d.Longitude = b.Longitude,
    d.Altitude = b.Altitude,
    d.UnixTime = b.UnixTime,
    d.Speed = b.Speed,
    d.Direction = b.Direction,
    d.Valid = b.Valid,
    d.InMobitelID = b.LogID,
    d.`Events` = b.`Events`,
    d.Sensor1 = b.Sensor1,
    d.Sensor2 = b.Sensor2,
    d.Sensor3 = b.Sensor3,
    d.Sensor4 = b.Sensor4,
    d.Sensor5 = b.Sensor5,
    d.Sensor6 = b.Sensor6,
    d.Sensor7 = b.Sensor7,
    d.Sensor8 = b.Sensor8,
    d.whatIs = b.whatIs,
    d.Counter1 = b.Counter1,
    d.Counter2 = b.Counter2,
    d.Counter3 = b.Counter3,
    d.Counter4 = b.Counter4
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  /* Удалим записи которые участвовали в апдейте */
  DELETE b
  FROM datagps d, datagpsbuffer_dr b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  /* Вставка новых записей */
  INSERT INTO datagps (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, InMobitelID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, LogID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs
  FROM datagpsbuffer_dr
  GROUP BY Mobitel_ID, LogID;

  
  TRUNCATE TABLE datagpsbuffer_dr;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `ExecuteQuery`
--

DROP PROCEDURE IF EXISTS `ExecuteQuery`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ExecuteQuery`(IN sqlQuery TINYTEXT)
    SQL SECURITY INVOKER
BEGIN
  SET @s = sqlQuery;
  PREPARE stmt FROM @s;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `FillLostDataGPS`
--

DROP PROCEDURE IF EXISTS `FillLostDataGPS`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `FillLostDataGPS`()
BEGIN
  
  DECLARE LAST_RECORDS_COUNT INT DEFAULT 32000;
  DECLARE LastMobitelRecord INT; 

  DECLARE MobitelIDInCursor INT;		
  DECLARE ConfirmedIDInCursor INT;	
  DECLARE NewConfirmedID INT;			
  DECLARE NewStartIDSelect INT DEFAULT 0; 
  DECLARE MaxMobitelLogID INT;		

  DECLARE BeginLogIDInCursor INT; 
  DECLARE EndLogIDInCursor INT;		
  DECLARE CountInLostRange INT;	
  DECLARE DataExists TINYINT DEFAULT 1;	
  DECLARE DataChanged TINYINT;	
  DECLARE NeedOptimize TINYINT DEFAULT 0; 
  DECLARE TableName VARCHAR(255);

  DECLARE CursorMobitels CURSOR FOR
    SELECT Mobitel_ID, ConfirmedID FROM mobitels ORDER BY Mobitel_ID;
  DECLARE CursorLostData CURSOR FOR
    SELECT Begin_LogID, End_LogID FROM datagpslosted
    WHERE Mobitel_ID = MobitelIDInCursor ORDER BY Begin_LogID;
  DECLARE CursorTable CURSOR FOR SHOW TABLES LIKE "#MUTEX_FILL_LOST_DATA_GPS";

  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    
    DROP TABLE IF EXISTS `#MUTEX_FILL_LOST_DATA_GPS`;
  END;

  
  OPEN CursorTable;
  FETCH CursorTable INTO TableName;
  CLOSE CursorTable;
  
  IF DataExists = 0 THEN
    
    CREATE TABLE `#MUTEX_FILL_LOST_DATA_GPS` (dummy char(1));
    SET DataExists = 1;

    
    CREATE TABLE IF NOT EXISTS `tmp_datagpslosted` LIKE datagps;
    ALTER TABLE `tmp_datagpslosted` Engine = HEAP;

    
    OPEN CursorMobitels;
    FETCH CursorMobitels INTO MobitelIDInCursor, ConfirmedIDInCursor;
    WHILE DataExists = 1 DO
      SET NewConfirmedID = 0;
      SET NewStartIDSelect = 0;

      
      TRUNCATE TABLE `tmp_datagpslosted`;

      
      SELECT COALESCE(MAX(LogID), 0) INTO MaxMobitelLogID
      FROM datagps
      WHERE (Mobitel_ID = MobitelIDInCursor);

      IF ConfirmedIDInCursor <> MaxMobitelLogID THEN

        SET DataChanged = 0;
        
        SET LastMobitelRecord = MaxMobitelLogID - LAST_RECORDS_COUNT;

        

        OPEN CursorLostData;
        FETCH CursorLostData INTO BeginLogIDInCursor, EndLogIDInCursor;

        
        IF  DataExists = 0 THEN
          SET DataChanged = 1;
          
          SET NewStartIDSelect = GREATEST(ConfirmedIDInCursor, LastMobitelRecord);
        END IF;

        
        WHILE DataExists = 1 DO
          
          SELECT COUNT(1) INTO CountInLostRange
          FROM datagps
          WHERE (Mobitel_ID = MobitelIDInCursor) AND
            (LogID > BeginLogIDInCursor) AND (LogID < EndLogIDInCursor);

          
          IF (CountInLostRange > 0) OR ((CountInLostRange = 0) AND
            (BeginLogIDInCursor < LastMobitelRecord)) THEN
            
            DELETE FROM datagpslosted
            WHERE (Mobitel_ID = MobitelIDInCursor) AND
              (Begin_LogID >= BeginLogIDInCursor);

            SET DataChanged = 1;
            SET NewStartIDSelect = BeginLogIDInCursor;
          END IF;

          FETCH CursorLostData INTO BeginLogIDInCursor, EndLogIDInCursor;
        END WHILE;
        CLOSE CursorLostData;
        
        SET DataExists = 1;

        
        IF (DataChanged = 0) AND (EndLogIDInCursor < MaxMobitelLogID) THEN
          SET DataChanged = 1;
          SET NewStartIDSelect = EndLogIDInCursor;
        END IF;

        IF DataChanged = 1 THEN
          
          INSERT INTO `tmp_datagpslosted`
          SELECT *
          FROM datagps
          WHERE (Mobitel_ID = MobitelIDInCursor) AND
            (LogID >= NewStartIDSelect);

          
          INSERT INTO datagpslosted
          SELECT HIGH_PRIORITY DISTINCT
            MobitelIDInCursor AS Mobitel_ID,
            
            dg1.LogID AS Begin_LogID,
            
            dg3.LogID AS End_LogID
          FROM
            
            `tmp_datagpslosted` dg1 LEFT JOIN `tmp_datagpslosted` dg2 ON (
              (dg2.Mobitel_ID = dg1.Mobitel_ID) AND (dg2.LogID = (dg1.LogID + 1)))
            
            LEFT JOIN `tmp_datagpslosted` dg3 ON (
              (dg3.Mobitel_ID = dg1.Mobitel_ID) AND
              
              (dg3.LogID = (
                SELECT MIN(dg.LogID)
                FROM `tmp_datagpslosted` dg
                WHERE dg.LogID > dg1.LogID )))
          WHERE
            
            (dg2.LogID IS NULL) AND
            
            (dg3.LogID IS NOT NULL)
          ORDER BY Begin_LogID;

          SET NeedOptimize = 1;

          
          SELECT COALESCE(MIN(Begin_LogID), -1) INTO NewConfirmedID
          FROM datagpslosted
          WHERE Mobitel_ID = MobitelIDInCursor;

          
          IF NewConfirmedID = -1 THEN
            SET NewConfirmedID = MaxMobitelLogID;
          END IF;

          
          IF NewConfirmedID > ConfirmedIDInCursor THEN
            UPDATE mobitels
            SET ConfirmedID = NewConfirmedID
            WHERE (Mobitel_ID = MobitelIDInCursor) AND
              (ConfirmedID < NewConfirmedID);
          END IF;
         END IF;
      END IF;

      FETCH CursorMobitels INTO MobitelIDInCursor, ConfirmedIDInCursor;
    END WHILE;
    CLOSE CursorMobitels;
    DROP TABLE IF EXISTS `tmp_datagpslosted`;

    
    IF NeedOptimize = 1 THEN
      OPTIMIZE TABLE datagpslosted;
    END IF;
    
    DROP TABLE IF EXISTS `#MUTEX_FILL_LOST_DATA_GPS`;
  ELSE
    SELECT "1";
  END IF;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `hystoryOnline`
--

DROP PROCEDURE IF EXISTS `hystoryOnline`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `hystoryOnline`(IN id INTEGER(11))
    SQL SECURITY INVOKER
BEGIN
select LogID, Mobitel_Id, 
from_unixtime(unixtime)  as 'time',
round((`Latitude` / 600000.00000),6) AS `Lat`,
round((`Longitude` / 600000.00000),6)  AS `Lon`,
`Altitude`  AS `Altitude`,
(`Speed` * 1.852) AS `speed`,
((`Direction` * 360) / 255) AS `direction`,
`Valid` AS `Valid`,
(((((((`Sensor8` + 
(`Sensor7` << 8)) + 
(`Sensor6` << 16)) + 
(`Sensor5` << 24)) + 
(`Sensor4` << 32)) + 
(`Sensor3` << 40)) + 
(`Sensor2` << 48)) + 
(`Sensor1` << 56)) AS `sensor`,
`Events` AS `Events`,
DataGPS_ID  as 'DataGPS_ID'
from online
where  datagps_id > id
order by datagps_id DESC;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `IsTeletrackDatabase`
--

DROP PROCEDURE IF EXISTS `IsTeletrackDatabase`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `IsTeletrackDatabase`()
    SQL SECURITY INVOKER
    COMMENT 'Проверка базы данных на соответствие структуре системы Teletrack'
BEGIN
  DECLARE i TINYINT;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `new_proc`
--

DROP PROCEDURE IF EXISTS `new_proc`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_proc`(IN sensor INTEGER(11), IN m_id INTEGER(11), IN s_id INTEGER(11), IN begin_time DATETIME, IN end_time DATETIME)
    READS SQL DATA
    SQL SECURITY INVOKER
BEGIN                                                                                                                                      
select                                                                                                                                     
    round((`datagps`.`Longitude` / 600000),6) AS `lon`,                                                                                    
    round((`datagps`.`Latitude` / 600000),6) AS `lat`,                                                                                     
    `datagps`.`Mobitel_ID` AS `Mobitel_ID`,                                                                                                
    `sensordata`.`Value` AS `Value`,                                                                                                       
    (`datagps`.`Speed` * 1.852) AS `speed`,                                                                                                
    from_unixtime(`datagps`.`UnixTime`) AS `time`,                                                                                         
    `sensordata`.`sensor_id` AS `sensor_id`,                                                                                               
    `datagps`.`DataGps_ID` AS `datagps_id`,                                                                                                
    `datagps`.`Sensor1`&`sensor` AS `sensor`                                                                                              
  from                                                                                                                                     
    (`sensordata` join `datagps` on((`sensordata`.`datagps_id` = `datagps`.`DataGps_ID`)))                                                 
  where                                                                                                                                    
    (`datagps`.`Valid` = 1) and                                                                                                            
    ( datagps.UnixTime BETWEEN UNIX_TIMESTAMP(begin_time) and UNIX_TIMESTAMP(end_time))                                                    
    and datagps.Mobitel_ID = m_id AND sensordata.sensor_id = s_id                                                                          
  order by                                                                                                                                 
    `datagps`.`UnixTime`,`datagps`.`Mobitel_ID`,`sensordata`.`sensor_id`;                                                                  
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `OnCorrectInfotrackLogId`
--

DROP PROCEDURE IF EXISTS `OnCorrectInfotrackLogId`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `OnCorrectInfotrackLogId`()
    SQL SECURITY INVOKER
    COMMENT 'Корректировка значений LogId и InMobitelID в данных инфотреков'
BEGIN
  DECLARE MobitelIDInCursor INT; /* Текущее значение MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* Флаг наличия данных после фетча */
  DECLARE MaxLogId INT DEFAULT 0; /* Максимальный LogId для данного дивайса */
  DECLARE SrvPacketIDInCursor BIGINT DEFAULT 0; /* Текущее значение серверного пакета */
    
  /* Курсор по инфотрекам в множестве новых данных. 
     Все инфотреки начинаются с буквы I */
  DECLARE CursorInfoTrack CURSOR FOR
    SELECT DISTINCT buf.Mobitel_ID
    FROM datagpsbuffer_on buf 
      JOIN mobitels m ON (buf.mobitel_id = m.mobitel_id)
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID) 
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND 
      (c.devIdShort LIKE "I%") AND (buf.LogId = 0)
    ORDER BY 1;
    
  /* Курсор по данным телетрека */
  DECLARE CursorInfoTrackUpdateSet CURSOR FOR
    SELECT SrvPacketID 
    FROM datagpsbuffer_on
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId = 0)
    ORDER BY UnixTime;  

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* Находим максимальный LogId для данного инфотрека */
    SELECT COALESCE(MAX(LogID), 0) INTO	MaxLogId
    FROM DataGps
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* Обновляем LogId и InMobitelID в наборе новых данных текущего инфотрека */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    WHILE DataExists = 1 DO
      SET MaxLogId = MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET LogId = MaxLogId, InMobitelID = MaxLogId
      WHERE SrvPacketID = SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    END WHILE;
    CLOSE CursorInfoTrackUpdateSet;
 
    SET DataExists = 1;
    FETCH CursorInfoTrack INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorInfoTrack;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `OnDeleteLostRange`
--

DROP PROCEDURE IF EXISTS `OnDeleteLostRange`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `OnDeleteLostRange`(IN MobitelID INTEGER(11), IN BeginSrvPacketID BIGINT)
    SQL SECURITY INVOKER
    COMMENT 'Удаление указанного диапазона пропусков из datagpslost_on.'
BEGIN
    
     
  DECLARE MinBeginSrvPacketID BIGINT; 
  DECLARE NewConfirmedID INT; 
  

    
    
  SELECT MIN(Begin_SrvPacketID) INTO MinBeginSrvPacketID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;
  
  
  IF MinBeginSrvPacketID IS NOT NULL THEN
    
    IF MinBeginSrvPacketID = BeginSrvPacketID  THEN
      SELECT End_LogID INTO NewConfirmedID
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID);
    END IF;
  
    
    DELETE FROM  datagpslost_on
    WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID);
    
    
    IF MinBeginSrvPacketID = BeginSrvPacketID  THEN
      UPDATE mobitels
      SET ConfirmedID = NewConfirmedID
      WHERE (Mobitel_ID = MobitelID) AND (NewConfirmedID > ConfirmedID);
    END IF;
  END IF;  
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `online`
--

DROP PROCEDURE IF EXISTS `online`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `online`()
    SQL SECURITY INVOKER
BEGIN
select Log_ID, MobitelId, 
(select DataGPS_ID
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) as 'DataGPS_ID',
(select from_unixtime(unixtime) 
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) as 'time',
( select round((`Latitude` / 600000.00000),6)
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `Lat`,
(select round((`Longitude` / 600000.00000),6) 
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `Lon`,
(select `Altitude` 
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `Altitude`,
(select (`Speed` * 1.852)
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `speed`,
(select ((`Direction` * 360) / 255)
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `direction`,
(select `Valid`
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `Valid`,
(select (((((((`Sensor8` + 
(`Sensor7` << 8)) + 
(`Sensor6` << 16)) + 
(`Sensor5` << 24)) + 
(`Sensor4` << 32)) + 
(`Sensor3` << 40)) + 
(`Sensor2` << 48)) + 
(`Sensor1` << 56)) 
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `sensor`,
(select `Events`
from `online` 
where mobitel_id = MobitelId and LogId = Log_ID Limit 1) AS `Events`
 
from(
select max(`LogID`) AS `Log_ID`, mobitel_id as MobitelId
from `online` 
group by `Mobitel_ID`) T1;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `OnListMobitelConfig`
--

DROP PROCEDURE IF EXISTS `OnListMobitelConfig`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `OnListMobitelConfig`()
    SQL SECURITY INVOKER
    COMMENT 'Выборка настроек телетреков которые могут работать в online режи'
BEGIN
  DECLARE MobitelIDInCursor INT;
  DECLARE DevIdShortInCursor CHAR(4);
  DECLARE DataExists TINYINT DEFAULT 1;

  /* Курсор в множестве настроенных телетреков */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS MobitelID, c.devIdShort AS DevIdShort
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID)
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND
      (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')
    ORDER BY m.Mobitel_ID;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* Временная таблица настроек телетрека */
  CREATE TEMPORARY TABLE tmpMobitelsConfig (
    MobitelID INT NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL) ENGINE = MEMORY;

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor, DevIdShortInCursor;
  WHILE DataExists = 1 DO

    INSERT INTO tmpMobitelsConfig SELECT MobitelIDInCursor, DevIdShortInCursor,
      (SELECT COALESCE(MAX(SrvPacketID), 0)
       FROM datagps
       WHERE Mobitel_ID = MobitelIDInCursor) AS LastPacketID;

    FETCH CursorMobitels INTO MobitelIDInCursor, DevIdShortInCursor;
  END WHILE;
  CLOSE CursorMobitels;

  SELECT MobitelID, DevIdShort, LastPacketID
  FROM tmpMobitelsConfig;

  DROP TEMPORARY TABLE tmpMobitelsConfig;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `OnLostDataGPS`
--

DROP PROCEDURE IF EXISTS `OnLostDataGPS`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `OnLostDataGPS`(IN MobitelID INTEGER(11), OUT NeedOptimize TINYINT)
    SQL SECURITY INVOKER
    COMMENT 'Поиск пропусков в online данных заданного телетрека и заполнение'
BEGIN
  DECLARE CurrentConfirmedID INT DEFAULT 0; 
  DECLARE NewConfirmedID INT DEFAULT 0; 
  DECLARE StartLogIDSelect INT DEFAULT 0; 
  DECLARE FinishLogIDSelect INT DEFAULT 0; 
  DECLARE MaxEndLogID INT DEFAULT 0; 
  DECLARE MinLogID_n INT DEFAULT 0; 
  DECLARE MaxLogID_n INT DEFAULT 0; 
  DECLARE DataExists TINYINT DEFAULT 1; 
  DECLARE DataChanged TINYINT DEFAULT 0; 
  DECLARE NewRowCount INT DEFAULT 0; 
  DECLARE NewLogID INT; 
  DECLARE NewSrvPacketID BIGINT; 
  DECLARE FirstLogID INT DEFAULT -9999999; 
  DECLARE SecondLogID INT; 
  DECLARE FirstSrvPacketID BIGINT; 
  DECLARE SecondSrvPacketID BIGINT; 
  DECLARE tmpSrvPacketID BIGINT DEFAULT 0;   
  
  DECLARE CursorNewLogID CURSOR FOR
    SELECT LogID, SrvPacketID 
    FROM datagpslost_ontmp;
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET NeedOptimize = 0;
 
  SELECT COALESCE(ConfirmedID, 0) INTO CurrentConfirmedID
  FROM mobitels
  WHERE Mobitel_ID = MobitelID;
  
  SELECT MIN(LogID) INTO MinLogID_n
  FROM datagpsbuffer_on
  WHERE (Mobitel_ID = MobitelID) AND (LogID > CurrentConfirmedID);
  
  SELECT MAX(LogID) INTO MaxLogID_n
  FROM datagpsbuffer_on 
  WHERE Mobitel_ID = MobitelID;  
  
  SELECT COALESCE(MAX(End_LogID), 0) INTO MaxEndLogID
  FROM datagpslost_on 
  WHERE Mobitel_ID = MobitelID;  
  
  IF MinLogID_n IS NOT NULL THEN
    TRUNCATE TABLE datagpslost_ontmp;
  
    IF MinLogID_n < MaxEndLogID THEN
      SELECT COALESCE(Begin_LogID, MinLogID_n) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MinLogID_n > Begin_LogID) 
        AND (MinLogID_n < End_LogID);
        
      SELECT COALESCE(End_LogID, MaxLogID_n) INTO FinishLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MaxLogID_n > Begin_LogID) 
        AND (MaxLogID_n < End_LogID);
      
      DELETE FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID >= StartLogIDSelect)
        AND (Begin_LogID <= FinishLogIDSelect);
      
      IF (StartLogIDSelect = 0) AND (NOT EXISTS
        (Select 1 
         FROM datagps 
         WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect))) THEN
      
        INSERT INTO datagpslost_ontmp(LogID, SrvPacketID) VALUES (0, 0);  
      END IF;       
    ELSE
      SELECT COALESCE(MAX(End_LogID), CurrentConfirmedID) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE Mobitel_ID = MobitelID;   
        
      SELECT MAX(LogID)  INTO FinishLogIDSelect
      FROM datagpsbuffer_on
      WHERE Mobitel_ID = MobitelID;
      
      SELECT COALESCE(SrvPacketID, 0) INTO tmpSrvPacketID
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect);
      
      INSERT INTO datagpslost_ontmp(LogID, SrvPacketID)
      VALUES (StartLogIDSelect, tmpSrvPacketID);
    END IF; 
    
    INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      SELECT LogID, SrvPacketID
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND (LogID >= StartLogIDSelect)
        AND (LogID <= FinishLogIDSelect)
      ORDER BY LogID;
    
    SET @InsertLostDataStatement = 
      'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';
    
    SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    WHILE DataExists = 1 DO
      IF FirstLogID = -9999999 THEN
        SET FirstLogID = NewLogID;
        SET FirstSrvPacketID = NewSrvPacketID;
      ELSE
        SET SecondLogID = NewLogID;
        SET SecondSrvPacketID = NewSrvPacketID;
        
        IF (SecondLogID - FirstLogID) > 1 THEN
          
          IF NewRowCount > 0 THEN
            SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, ",");
          END IF;
          
          SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, 
            " (", MobitelID, ", ", FirstLogID, ", ", SecondLogID, ", ", 
            FirstSrvPacketID, ", ", SecondSrvPacketID, ")");
              
          SET NewRowCount = NewRowCount + 1;
        END IF;
        
        SET  FirstLogID = SecondLogID;
        SET FirstSrvPacketID = SecondSrvPacketID;
      END IF;  
      FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    END WHILE;
    CLOSE CursorNewLogID;
             
    IF NewRowCount > 0 THEN
      PREPARE stmt FROM @InsertLostDataStatement;
      EXECUTE stmt;
      DEALLOCATE PREPARE stmt;
          
      SET DataChanged = 1;
    END IF; 
    
    SELECT MIN(Begin_LogID) INTO NewConfirmedID
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;
       
    IF NewConfirmedID IS NULL THEN
      SELECT MAX(LogID) INTO NewConfirmedID
      FROM datagps
      WHERE Mobitel_ID = MobitelID;
    END IF;  
    
    UPDATE mobitels
    SET ConfirmedID = NewConfirmedID
    WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);
        
    TRUNCATE TABLE datagpslost_ontmp;
  END IF; 
  
  IF DataChanged = 1 THEN
    SET NeedOptimize = 1;
  END IF;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `OnLostDataGPS2`
--

DROP PROCEDURE IF EXISTS `OnLostDataGPS2`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `OnLostDataGPS2`(IN MobitelID INTEGER(11), OUT NeedOptimize TINYINT)
    SQL SECURITY INVOKER
    COMMENT 'Модификация диапазонов пропусков. Анализ дублирующих данных зада'
BEGIN
  DECLARE NewConfirmedID INT DEFAULT 0; 
  DECLARE BeginLogID INT; 
  DECLARE EndLogID INT; 
  DECLARE BeginSrvPacketID BIGINT; 
  DECLARE EndSrvPacketID BIGINT; 
  DECLARE NewRowCount INT DEFAULT 0; 
  DECLARE RowCountForAnalyze INT DEFAULT 0; 
  DECLARE DataExists TINYINT DEFAULT 1; 
  
  DECLARE CursorLostRanges CURSOR FOR
    SELECT Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID 
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;    
  
  DECLARE CursorNewLogID CURSOR FOR
    SELECT LogID, SrvPacketID 
    FROM datagpslost_ontmp
    ORDER BY SrvPacketID;    
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET NeedOptimize = 0;
  SET @InsertLostDataStatement = 
    'INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID) VALUES ';
  
  TRUNCATE TABLE datagpslost_ontmp;
  
  SET DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  WHILE DataExists = 1 DO
    
    INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      SELECT DISTINCT LogID, SrvPacketID
      FROM datagpsbuffer_ontmp
      WHERE (Mobitel_ID = MobitelID) AND (SrvPacketID > BeginSrvPacketID)
        AND (SrvPacketID < EndSrvPacketID)
      ORDER BY SrvPacketID;
      
    SELECT COUNT(1) INTO RowCountForAnalyze
    FROM datagpslost_ontmp;  
    
    IF RowCountForAnalyze > 0 THEN  
      INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      VALUES (BeginLogID, BeginSrvPacketID), (EndLogID, EndSrvPacketID);
      
      DELETE FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);
                   
      BEGIN
        DECLARE NewLogID INT; 
        DECLARE NewSrvPacketID BIGINT; 
        DECLARE FirstLogID INT DEFAULT -9999999; 
        DECLARE SecondLogID INT; 
        DECLARE FirstSrvPacketID BIGINT; 
        DECLARE SecondSrvPacketID BIGINT; 
        
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
        
        SET DataExists = 1;
        OPEN CursorNewLogID;
        FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        WHILE DataExists = 1 DO
          IF FirstLogID = -9999999 THEN
            SET FirstLogID = NewLogID;
            SET FirstSrvPacketID = NewSrvPacketID;
          ELSE
            SET SecondLogID = NewLogID;
            SET SecondSrvPacketID = NewSrvPacketID;
            
            IF (SecondSrvPacketID - FirstSrvPacketID) > 1 THEN
              
              IF NewRowCount > 0 THEN
                SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, ",");
              END IF;
              
              SET @InsertLostDataStatement = CONCAT(@InsertLostDataStatement, 
                " (", MobitelID, ", ", FirstLogID, ", ", SecondLogID, ", ", 
                FirstSrvPacketID, ", ", SecondSrvPacketID, ")");
                  
              SET NewRowCount = NewRowCount + 1;
            END IF;
            
            SET  FirstLogID = SecondLogID;
            SET FirstSrvPacketID = SecondSrvPacketID;
          END IF;  
          FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        END WHILE;
        CLOSE CursorNewLogID;
      END;

      TRUNCATE TABLE datagpslost_ontmp;
    END IF;
      
    FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  END WHILE;
  CLOSE CursorLostRanges;
  
  IF NewRowCount > 0 THEN
    PREPARE stmt FROM @InsertLostDataStatement;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
          
    SET  NeedOptimize = 1; 
  END IF; 
  
  SELECT MIN(Begin_LogID) INTO NewConfirmedID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;
       
  IF NewConfirmedID IS NULL THEN
    SELECT MAX(LogID) INTO NewConfirmedID
    FROM datagps
    WHERE Mobitel_ID = MobitelID;
  END IF;  
  
  UPDATE mobitels
  SET ConfirmedID = NewConfirmedID
  WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

DROP PROCEDURE IF EXISTS OnDeleteDuplicates;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE PROCEDURE OnDeleteDuplicates()
  SQL SECURITY INVOKER
  COMMENT 'Delete duplicates rows from datagpsbuffer_on'
BEGIN
  /* Удаляет дубликаты строк из таблицы datagpsbuffer_on. Критерием поиска
   являются одинаковые значения в полях Mobitel_Id + LogID. Из множества
   повторяющихся записей в таблице datagpsbuffer_on останется только одна - 
   последняя принятая телематическим сервером, т.е. с максимальным SrvPacketID. */
   
  /* Текущий MobitelId в курсоре CursorDuplicateGroups */
  DECLARE MobitelIdInCursor INT DEFAULT 0; 
  /* Текущий LogID в курсоре CursorDuplicateGroups */
  DECLARE LogIdInCursor INT DEFAULT 0; 
  /* Max SrvPacketId в множестве строк текущей группы дубликатов */
  DECLARE MaxSrvPacketId BIGINT DEFAULT 0; 
  DECLARE DataExists TINYINT DEFAULT 1;

  /* Курсор для прохода по всем группам дубликатов */  
  DECLARE CursorDuplicateGroups CURSOR FOR
    SELECT Mobitel_Id, LogId 
    FROM datagpsbuffer_on
    GROUP BY Mobitel_Id, LogId 
    HAVING COUNT(LogId) > 1;
    
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  WHILE DataExists = 1 DO
    SELECT MAX(SrvPacketID) INTO MaxSrvPacketId
    FROM datagpsbuffer_on 
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor);
         
    DELETE FROM datagpsbuffer_on  
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor) 
      AND (SrvPacketID < MaxSrvPacketId);
        
    FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  END WHILE;
  CLOSE CursorDuplicateGroups; 
END$$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `OnTransferBuffer`
--

DROP PROCEDURE IF EXISTS `OnTransferBuffer`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `OnTransferBuffer`()
    SQL SECURITY INVOKER
    COMMENT 'Переносит GPS данные из буферной таблицы в datagps'
BEGIN
  DECLARE MobitelIDInCursor INT; /* Текущее значение MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* Флаг наличия данных после фетча */
  DECLARE NeedOptimize TINYINT DEFAULT 0; /* Нужна оптимизация или нет */
  DECLARE NeedOptimizeParam TINYINT;
  
  DECLARE TmpMaxUnixTime INT; /* Временная переменная для хранения Max UnixTime из таблицы online */

  /* Курсор по телетрекам в множестве новых данных */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_on;
  /* Курсор по телетрекам в множестве повторных данных */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_ontmp;  

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  CALL OnDeleteDuplicates();
  CALL OnCorrectInfotrackLogId();
  
  /* Сохраняем записи, которые уже есть в БД, для второго этапа анализа пропусков 
     Используется процедурой OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp (Mobitel_ID, LogID, SrvPacketID)
  SELECT MobitelID, LogID, SrvPacketID
  FROM (
    SELECT b.Mobitel_ID AS MobitelID, b.LogID AS LogID, 
      b.SrvPacketID AS SrvPacketID, d.LogID AS DataGpsLogID
    FROM datagpsbuffer_on b LEFT JOIN datagps d ON 
      (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL) T1;
  
      
  /* -------------------- Заполнение таблицы Online ---------------------- */  
  /* Удаление из таблицы online дублирующих строк таблицы datagpsbuffer_on */
  /* по полям Mobitel_ID и LogID */
  DELETE o
  FROM online o, datagpsbuffer_on dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);
  
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* Нахождение Max значения UnixTime в таблице online для заданного
       телетрека. p.s. UnixTime в таблице online только валидный */
    SELECT COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online FORCE INDEX (IDX_Mobitelid)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* Вставка новых записей (максимум 100)каждого телетрека в online */
    INSERT INTO online (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs
    FROM (
      SELECT *
      FROM datagpsbuffer_on
      WHERE (Mobitel_ID = MobitelIDInCursor)  AND 
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
      GROUP BY Mobitel_ID, LogID
      ORDER BY UnixTime DESC
      LIMIT 100) AS T
    ORDER BY UnixTime ASC;

    /* Удаление строк из таблицы оперативных данных не удовлетворяющие
       условиям хранения - не являются последними 100 строками */
    DELETE FROM online
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < 
      (SELECT COALESCE(MIN(UnixTime), 0) 
       FROM (
         SELECT UnixTime
         FROM online
         WHERE Mobitel_ID = MobitelIDInCursor 
         ORDER BY UnixTime DESC
         LIMIT 100) T1)); 
  
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */ 
  
  /* Зачем-то нужен апдейт ... */
  UPDATE datagps d, datagpsbuffer_on b SET
    d.Latitude = b.Latitude,
    d.Longitude = b.Longitude,
    d.Altitude = b.Altitude,
    d.UnixTime = b.UnixTime,
    d.Speed = b.Speed,
    d.Direction = b.Direction,
    d.Valid = b.Valid,
    d.InMobitelID = b.LogID,
    d.`Events` = b.`Events`,
    d.Sensor1 = b.Sensor1,
    d.Sensor2 = b.Sensor2,
    d.Sensor3 = b.Sensor3,
    d.Sensor4 = b.Sensor4,
    d.Sensor5 = b.Sensor5,
    d.Sensor6 = b.Sensor6,
    d.Sensor7 = b.Sensor7,
    d.Sensor8 = b.Sensor8,
    d.whatIs = b.whatIs,
    d.Counter1 = b.Counter1,
    d.Counter2 = b.Counter2,
    d.Counter3 = b.Counter3,
    d.Counter4 = b.Counter4,
    d.SrvPacketID = b.SrvPacketID
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    AND (d.SrvPacketID < b.SrvPacketID);

  /* Удалим записи которые участвовали в апдейте */
  DELETE b
  FROM datagps d, datagpsbuffer_on b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  /* Вставка новых записей*/
  INSERT INTO datagps (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, InMobitelID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs, SrvPacketID)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, LogID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs, SrvPacketID
  FROM datagpsbuffer_on
  GROUP BY Mobitel_ID, LogID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ------ Обновление времени последней вставки записей ------- */
    UPDATE mobitels SET LastInsertTime = UNIX_TIMESTAMP(NOW())
    WHERE Mobitel_ID = MobitelIDInCursor;
       
    /* ------ Обновление списка пропущенных записей -------------- */
    CALL OnLostDataGPS(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;

  /* Очистка буфера */
  TRUNCATE TABLE datagpsbuffer_on;
    
  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ---- Обновление списка пропущенных записей по второму алгоритму --- */
    CALL OnLostDataGPS2(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsUpdateSet;
  
  /* Очистка вспомогательного буфера */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  /* Были изменения - сделаем оптимизацию */
  IF NeedOptimize = 1 THEN
    OPTIMIZE TABLE datagpslost_on;
  END IF;            
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `PopListMobitelConfig`
--

DROP PROCEDURE IF EXISTS `PopListMobitelConfig`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `PopListMobitelConfig`()
    SQL SECURITY INVOKER
    COMMENT 'Выборка настроек телетреков для работы в режиме POP3'
BEGIN
  SELECT m.Mobitel_ID AS MobitelID, c.devIdShort AS DevIdShort,
    COALESCE(
      (SELECT s.EmailMobitel 
       FROM Mobitels m2 JOIN ServiceSend s ON s.ID = m2.ServiceSend_ID 
       WHERE m2.Mobitel_ID = m.Mobitel_ID
       ORDER BY s.ServiceSend_ID DESC
       LIMIT 1), "") AS Email
  FROM mobitels m JOIN internalmobitelconfig c ON c.ID = m.InternalMobitelConfig_ID
  WHERE (c.InternalMobitelConfig_ID = (
    SELECT intConf.InternalMobitelConfig_ID
    FROM internalmobitelconfig intConf
    WHERE (intConf.ID = c.ID)
    ORDER BY intConf.InternalMobitelConfig_ID DESC
    LIMIT 1)) AND (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251 '')
  ORDER BY m.Mobitel_ID;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `PopTransferBuffer`
--

DROP PROCEDURE IF EXISTS `PopTransferBuffer`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `PopTransferBuffer`()
    SQL SECURITY INVOKER
    COMMENT 'Переносит GPS данные из буферной таблицы в datagps'
BEGIN
  DECLARE MobitelIDInCursor INT; /* Текущее значение MobitelID в курсоре*/
  DECLARE DataExists TINYINT DEFAULT 1; /* Флаг наличия данных после фетча */
  
  DECLARE TmpMaxUnixTime INT; /* Временная переменная для хранения Max UnixTime из таблицы online */

  DECLARE CursorMobitels CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_pop;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* -------------------- Заполнение таблицы Online ---------------------- */  
  /* Удаление из таблицы online дублирующих строк таблицы datagpsbuffer_dr */
  /* по полям Mobitel_ID и LogID */
  DELETE o
  FROM online o, datagpsbuffer_pop dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);
  
  SET DataExists = 1;
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* Нахождение Max значения UnixTime в таблице online для заданного
       телетрека. p.s. UnixTime в таблице online только валидный */
    SELECT COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online FORCE INDEX (IDX_Mobitelid)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* Вставка новых записей (максимум 100) каждого телетрека в online */
    INSERT INTO online (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs
    FROM (
      SELECT *
      FROM datagpsbuffer_pop
      WHERE (Mobitel_ID = MobitelIDInCursor)  AND 
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
      GROUP BY Mobitel_ID, LogID
      ORDER BY UnixTime DESC
      LIMIT 100) AS T
    ORDER BY UnixTime ASC;

    /* Удаление строк из таблицы оперативных данных не удовлетворяющие
       условиям хранения - не являются последними 100 строками */
    DELETE FROM online
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < 
      (SELECT COALESCE(MIN(UnixTime), 0) 
       FROM (
         SELECT UnixTime
         FROM online
         WHERE Mobitel_ID = MobitelIDInCursor 
         ORDER BY UnixTime DESC
         LIMIT 100) T1)); 
  
    FETCH CursorMobitels INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitels;
  /* ---------------- Online ---------------- */  

  /* Зачем-то нужен апдейт ... */
  UPDATE datagps d, datagpsbuffer_pop b SET
    d.Latitude = b.Latitude,
    d.Longitude = b.Longitude,
    d.Altitude = b.Altitude,
    d.UnixTime = b.UnixTime,
    d.Speed = b.Speed,
    d.Direction = b.Direction,
    d.Valid = b.Valid,
    d.InMobitelID = b.LogID,
    d.`Events` = b.`Events`,
    d.Sensor1 = b.Sensor1,
    d.Sensor2 = b.Sensor2,
    d.Sensor3 = b.Sensor3,
    d.Sensor4 = b.Sensor4,
    d.Sensor5 = b.Sensor5,
    d.Sensor6 = b.Sensor6,
    d.Sensor7 = b.Sensor7,
    d.Sensor8 = b.Sensor8,
    d.whatIs = b.whatIs,
    d.Counter1 = b.Counter1,
    d.Counter2 = b.Counter2,
    d.Counter3 = b.Counter3,
    d.Counter4 = b.Counter4
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  /* Удалим записи которые участвовали в апдейте */
  DELETE b
  FROM datagps d, datagpsbuffer_pop b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  /* Вставка новых записей */
  INSERT INTO datagps (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, InMobitelID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, LogID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs
  FROM datagpsbuffer_pop
  GROUP BY Mobitel_ID, LogID;

  
  TRUNCATE TABLE datagpsbuffer_pop;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RfFillLostDataGPS`
--

DROP PROCEDURE IF EXISTS `RfFillLostDataGPS`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RfFillLostDataGPS`()
    COMMENT 'Поиск пропусков в RF данных и заполнение таблицы datagpslost'
BEGIN
  DECLARE MobitelIDInCursor INT; 
  DECLARE DataExists TINYINT DEFAULT 1; 
  DECLARE NeedOptimize TINYINT DEFAULT 0; 
  DECLARE NeedOptimizeParam TINYINT;

  
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS Mobitel_ID
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE intConf.ID = c.ID 
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND
      (c.moduleIdRf IS NOT NULL) AND (c.moduleIdRf <> _cp1251'')
    ORDER BY m.Mobitel_ID;
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  SET DataExists = 1;
  
  
  DELETE d
  FROM datagpslost d
  WHERE NOT EXISTS(
    SELECT 1
    FROM mobitels m
    WHERE m.Mobitel_ID = d.Mobitel_ID);
  
  
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    CALL RfLostDataGPS(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitels INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitels;

  
  IF NeedOptimize = 1 THEN
    OPTIMIZE TABLE datagpslost;
  END IF;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RfListMobitelConfig`
--

DROP PROCEDURE IF EXISTS `RfListMobitelConfig`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RfListMobitelConfig`()
    COMMENT 'Выборка RF настроек телетреков'
BEGIN
  DECLARE MobitelIDInCursor INT;
  DECLARE NameInCursor CHAR(50);
  DECLARE DescrInCursor CHAR(200);
  DECLARE ConfirmedIDInCursor INT;
  DECLARE moduleIdRfInCursor CHAR(50);
  DECLARE LastInsertTimeInCursor DateTime;
  DECLARE DataExists TINYINT DEFAULT 1;

  /* Курсор в множестве настроенных телетреков */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS Mobitel_ID,
      m.Name AS Name,
      m.Descr AS Descr,
      m.ConfirmedID AS ConfirmedID,
      c.moduleIdRf AS moduleIdRf,
      FROM_UNIXTIME(m.LastInsertTime) AS LastInsertTime
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE intConf.ID = c.ID 
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND
      (c.moduleIdRf IS NOT NULL) AND (c.moduleIdRf <> _cp1251'')
    ORDER BY m.Mobitel_ID;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* Временная таблица настроек телетрека */
  CREATE TEMPORARY TABLE tmpMobitelsConfig (
    Mobitel_ID INT NOT NULL,
    Name CHAR(50),
    Descr CHAR(200),
    ConfirmedID INT NOT NULL,
    moduleIdRf CHAR(50) NOT NULL,
    LastInsertTime DateTime NOT NULL,
    LastLogID INT NOT NULL) ENGINE = MEMORY;

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor, NameInCursor, DescrInCursor, 
    ConfirmedIDInCursor, moduleIdRfInCursor, LastInsertTimeInCursor;
  WHILE DataExists = 1 DO

    INSERT INTO tmpMobitelsConfig SELECT MobitelIDInCursor, NameInCursor, DescrInCursor, 
      ConfirmedIDInCursor, moduleIdRfInCursor, LastInsertTimeInCursor,
      (SELECT COALESCE(MAX(LogID), 0)
       FROM datagps
       WHERE Mobitel_ID = MobitelIDInCursor) AS LastLogID;

    FETCH CursorMobitels INTO MobitelIDInCursor, NameInCursor, DescrInCursor, 
    ConfirmedIDInCursor, moduleIdRfInCursor, LastInsertTimeInCursor;
  END WHILE;
  CLOSE CursorMobitels;

  SELECT Mobitel_ID, Name, Descr, ConfirmedID, moduleIdRf, LastInsertTime, LastLogID
  FROM tmpMobitelsConfig;

  DROP TEMPORARY TABLE tmpMobitelsConfig;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RfLostDataGPS`
--

DROP PROCEDURE IF EXISTS `RfLostDataGPS`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RfLostDataGPS`(IN MobitelID INTEGER(11), OUT NeedOptimize TINYINT)
    COMMENT 'Поиск пропусков в RF данных заданного телетрека и заполнение таб'
BEGIN
  
  DECLARE MAX_RECORD_COUNT INT DEFAULT 33792;
  DECLARE FirstActualLogID INT; 

  DECLARE CurrentConfirmedID INT; 
  DECLARE NewConfirmedID INT DEFAULT 0; 
  DECLARE NewStartLogIDSelect INT DEFAULT 0; 
  DECLARE MaxMobitelLogID INT; 

  DECLARE BeginLogIDInCursor INT; 
  DECLARE EndLogIDInCursor INT; 
  DECLARE CountInLostRange INT; 
  DECLARE DataExists TINYINT DEFAULT 1; 
  DECLARE DataChanged TINYINT DEFAULT 0; 
  DECLARE NeedAnalyze TINYINT DEFAULT 0; 
  
  DECLARE NewRowCount INT DEFAULT 0; 
  
  DECLARE NewLogID INT; 
  DECLARE FirstLogID INT DEFAULT -9999999; 
  DECLARE SecondLogID INT; 
                                                                                    
  
  
  DECLARE CursorLostData CURSOR FOR
    SELECT Begin_LogID, End_LogID
    FROM datagpslost
    WHERE Mobitel_ID = MobitelID
    ORDER BY Begin_LogID;
    
  
  DECLARE CursorNewLogID CURSOR FOR
    SELECT LogID 
    FROM datagpslost_rftmp;
    
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET DataExists = 1;
  SET NeedOptimize = 0;
 
  SELECT COALESCE(ConfirmedID, 0) INTO CurrentConfirmedID
  FROM mobitels
  WHERE Mobitel_ID = MobitelID;

  
  SELECT COALESCE(MAX(LogID), 0) INTO MaxMobitelLogID
  FROM datagps
  WHERE (Mobitel_ID = MobitelID);

  IF CurrentConfirmedID <> MaxMobitelLogID THEN
    
    SET FirstActualLogID = MaxMobitelLogID - MAX_RECORD_COUNT;
    IF FirstActualLogID < 0 THEN
      SET FirstActualLogID = 0;
    END IF;

    
    DELETE FROM datagpslost
    WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID < FirstActualLogID);
    
    
    OPEN CursorLostData;
    FETCH CursorLostData INTO BeginLogIDInCursor, EndLogIDInCursor;
    
    
    IF DataExists = 0 THEN
      SET DataChanged = 0;
      SET NeedAnalyze = 1;
      
      SET NewStartLogIDSelect = GREATEST(CurrentConfirmedID, FirstActualLogID);
    END IF;

    
    WHILE DataExists = 1 DO
      
      SELECT COUNT(1) INTO CountInLostRange
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND
        (LogID > BeginLogIDInCursor) AND (LogID < EndLogIDInCursor);

      
      IF (CountInLostRange > 0)  THEN
        
        DELETE FROM datagpslost
        WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID >= BeginLogIDInCursor);

        SET DataChanged = 1;
        SET NeedAnalyze = 1;
        SET NewStartLogIDSelect = BeginLogIDInCursor;
        
        
        SET DataExists = 0;
      ELSE
        FETCH CursorLostData INTO BeginLogIDInCursor, EndLogIDInCursor;
      END IF;	
    END WHILE;
    CLOSE CursorLostData;
   
    
       
    IF (NeedAnalyze = 0) AND (EndLogIDInCursor < MaxMobitelLogID) THEN
      SET NewStartLogIDSelect = EndLogIDInCursor;
      SET NeedAnalyze = 1;
    END IF;
   
    

    IF NeedAnalyze = 1 THEN
      TRUNCATE TABLE datagpslost_rftmp;

      
      INSERT INTO datagpslost_rftmp	(LogID)
      SELECT DISTINCT(LogID)
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND (LogID >= NewStartLogIDSelect)
      ORDER BY LogID;
      
      
      SET @InsertLostDataStatement = 
        "INSERT INTO datagpslost(Mobitel_ID, Begin_LogID, End_LogID) VALUES ";
      
      
      SET DataExists = 1;
      OPEN CursorNewLogID;
      FETCH CursorNewLogID INTO NewLogID;
      WHILE DataExists = 1 DO
        IF FirstLogID = -9999999 THEN
          
          SET FirstLogID = NewLogID;
        ELSE
          
          SET SecondLogID = NewLogID;
          IF (SecondLogID -	FirstLogID) > 1 THEN
            
            IF NewRowCount > 0 THEN
              SET @InsertLostDataStatement = 
                CONCAT(@InsertLostDataStatement, ",");
            END IF;
            SET @InsertLostDataStatement = 
              CONCAT(@InsertLostDataStatement, 
                " (", MobitelID, ", ", FirstLogID, ", ", SecondLogID, ")");
            
            SET NewRowCount = NewRowCount + 1;
          END IF;
          
          SET	FirstLogID = SecondLogID;
        END IF;	
        FETCH CursorNewLogID INTO NewLogID;
      END WHILE;
      CLOSE CursorNewLogID;
      
        
      IF NewRowCount > 0 THEN
        
        PREPARE stmt FROM @InsertLostDataStatement;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
        
        SET DataChanged = 1;
      END IF; 
             
      
      
      SELECT COALESCE(MIN(Begin_LogID), -1) INTO NewConfirmedID
      FROM datagpslost
      WHERE Mobitel_ID = MobitelID;

      
      IF NewConfirmedID = -1 THEN
        SET NewConfirmedID = MaxMobitelLogID;
      END IF;

      
      IF NewConfirmedID > CurrentConfirmedID THEN
        UPDATE mobitels
        SET ConfirmedID = NewConfirmedID
        WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);
      END IF;
      
      TRUNCATE TABLE datagpslost_rftmp;
    END IF;	 
    
    IF DataChanged = 1 THEN
      SET NeedOptimize = 1;
    END IF;
  
  END IF;	
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RfMobitelConfig`
--

DROP PROCEDURE IF EXISTS `RfMobitelConfig`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RfMobitelConfig`(IN paramMobitelID INTEGER(11))
    COMMENT 'Выборка RF настроек заданного телетрека'
BEGIN
SELECT
  m.Mobitel_ID AS Mobitel_ID,
  m.Name AS Name,
  m.Descr AS Descr,
  m.ConfirmedID AS ConfirmedID,
  c.moduleIdRf AS moduleIdRf,
  FROM_UNIXTIME(m.LastInsertTime) AS LastInsertTime,
  ( SELECT COALESCE(d.LogID, 0)
    FROM datagps d
    WHERE d.Mobitel_ID = m.Mobitel_ID
    ORDER BY d.LogID DESC
    LIMIT 1 ) AS LastLogID
FROM mobitels m
  JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
WHERE (m.Mobitel_ID = paramMobitelID) AND
  (c.InternalMobitelConfig_ID = (
    SELECT intConf.InternalMobitelConfig_ID
    FROM internalmobitelconfig intConf
    WHERE intConf.ID = c.ID
    ORDER BY intConf.InternalMobitelConfig_ID DESC
    LIMIT 1)) AND
  (c.moduleIdRf IS NOT NULL) AND (c.moduleIdRf <> _cp1251'')
ORDER BY m.Mobitel_ID;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RfTransferBuffer`
--

DROP PROCEDURE IF EXISTS `RfTransferBuffer`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RfTransferBuffer`()
    SQL SECURITY INVOKER
    COMMENT 'Переносит GPS данные из буферной таблицы в datagps.'
BEGIN

  DECLARE MobitelIDInCursor INT; /* Текущее значение MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* Флаг наличия данных после фетча */
  DECLARE NeedOptimize TINYINT DEFAULT 0; /* Нужна оптимизация или нет */
  DECLARE NeedOptimizeParam TINYINT;
  DECLARE TmpMaxUnixTime INT; /* Временная переменная для хранения Max UnixTime из таблицы online */

  DECLARE CursorMobitels CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  /* -------------------- Заполнение таблицы Online ---------------------- */  
  /* Удаление из таблицы online дублирующих строк таблицы datagpsbuffer_dr */
  /* по полям Mobitel_ID и LogID */
  DELETE o
  FROM online o, datagpsbuffer_pop dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);
  
  SET DataExists = 1;
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* Нахождение Max значения UnixTime в таблице online для заданного
       телетрека. p.s. UnixTime в таблице online только валидный */
    SELECT COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online FORCE INDEX (IDX_Mobitelid)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* Вставка новых записей (максимум 100) каждого телетрека в online */
    INSERT INTO online (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs
    FROM (
      SELECT * 
      FROM datagpsbuffer
      WHERE (Mobitel_ID = MobitelIDInCursor)  AND 
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
      GROUP BY Mobitel_ID, LogID
      ORDER BY UnixTime DESC
      LIMIT 100) AS T
    ORDER BY UnixTime ASC; 

    /* Удаление строк из таблицы оперативных данных не удовлетворяющие
       условиям хранения - не являются последними 100 строками */
    DELETE FROM online
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < 
      (SELECT COALESCE(MIN(UnixTime), 0) 
       FROM (
         SELECT UnixTime
         FROM online
         WHERE Mobitel_ID = MobitelIDInCursor 
         ORDER BY UnixTime DESC
         LIMIT 100) T1)); 
  
    FETCH CursorMobitels INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitels;
  /* ---------------- Online ---------------- */  


  /* Зачем-то нужен апдейт ... по всей видимости из-за клонов*/
  UPDATE datagps d, datagpsbuffer b SET
    d.Message_ID = 0, d.Latitude = b.Latitude,
    d.Longitude = b.Longitude,
    d.Altitude = b.Altitude,
    d.UnixTime = b.UnixTime,
    d.Speed = b.Speed,
    d.Direction = b.Direction,
    d.Valid = b.Valid,
    d.InMobitelID = b.LogID,
    d.`Events` = b.`Events`,
    d.Sensor1 = b.Sensor1,
    d.Sensor2 = b.Sensor2,
    d.Sensor3 = b.Sensor3,
    d.Sensor4 = b.Sensor4,
    d.Sensor5 = b.Sensor5,
    d.Sensor6 = b.Sensor6,
    d.Sensor7 = b.Sensor7,
    d.Sensor8 = b.Sensor8,
    d.isShow = 0,
    d.whatIs = b.whatIs,
    d.Counter1 = b.Counter1,
    d.Counter2 = b.Counter2,
    d.Counter3 = b.Counter3,
    d.Counter4 = b.Counter4
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  /* Удалим записи которые участвовали в апдейте */
  DELETE b
  FROM datagps d, datagpsbuffer b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  /* Вставка новых записей*/
  INSERT INTO datagps (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, InMobitelID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, LogID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4,
    whatIs
  FROM datagpsbuffer
  GROUP BY Mobitel_ID, LogID;

  /* ------------------- FETCH Mobitels ------------------------ */
  SET DataExists = 1;
  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ------ Обновление времени последней вставки записей ------- */
    UPDATE mobitels SET LastInsertTime = UNIX_TIMESTAMP(NOW())
    WHERE Mobitel_ID = MobitelIDInCursor;
        
    /* ------ Обновление списка пропущенных записей -------------- */
    CALL RfLostDataGPS(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    
    FETCH CursorMobitels INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitels;

  /* Очистка буфера */
  TRUNCATE TABLE datagpsbuffer;

  /* Были изменения - сделаем оптимизацию */
  IF NeedOptimize = 1 THEN
    OPTIMIZE TABLE datagpslost;
  END IF;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `TestP`
--

DROP PROCEDURE IF EXISTS `TestP`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `TestP`()
BEGIN
  SELECT VERSION();
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `TransferBuffer`
--

DROP PROCEDURE IF EXISTS `TransferBuffer`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `TransferBuffer`()
    SQL SECURITY INVOKER
    COMMENT 'Переносит GPS данные из буферной таблицы в datagps.'
BEGIN





CREATE TEMPORARY TABLE IF NOT EXISTS temp1 ENGINE=HEAP(
  SELECT DataGPS_ID FROM datagpsbuffer
  );



UPDATE datagps d, datagpsbuffer b, temp1 t SET
  d.Message_ID = 0, 
  d.Latitude = b.Latitude,
  d.Longitude = b.Longitude,
  d.Altitude = b.Altitude,
  d.UnixTime = b.UnixTime,
  d.Speed = b.Speed,
  d.Direction = b.Direction,
  d.Valid = b.Valid,
  d.InMobitelID = NULL, 
  d.`Events` = b.`Events`,
  d.Sensor1 = b.Sensor1,
  d.Sensor2 = b.Sensor2,
  d.Sensor3 = b.Sensor3,
  d.Sensor4 = b.Sensor4,
  d.Sensor5 = b.Sensor5,
  d.Sensor6 = b.Sensor6,
  d.Sensor7 = b.Sensor7,
  d.Sensor8 = b.Sensor8,
  d.isShow = 0, 
  d.whatIs = b.whatIs,
  d.Counter1 = b.Counter1,
  d.Counter2 = b.Counter2,
  d.Counter3 = b.Counter3,
  d.Counter4 = b.Counter4
WHERE b.DataGps_ID = t.DataGps_ID
  AND d.Mobitel_ID = b.Mobitel_ID
  AND d.LogID = b.LogID;


DELETE b
FROM datagps d, datagpsbuffer b, temp1 t
WHERE b.DataGps_ID = t.DataGps_ID
  AND d.Mobitel_ID = b.Mobitel_ID
  AND d.LogID = b.LogID;


INSERT INTO datagps
  (
  Mobitel_ID,
  LogID,
  UnixTime,
  Latitude,
  Longitude,
  Altitude,
  Direction,
  Speed,
  Valid,
  `Events`,
  Sensor1,
  Sensor2,
  Sensor3,
  Sensor4,
  Sensor5,
  Sensor6,
  Sensor7,
  Sensor8,
  Counter1,
  Counter2,
  Counter3,
  Counter4,
  whatIs
  )
SELECT
  b.Mobitel_ID,
  b.LogID,
  b.UnixTime,
  b.Latitude,
  b.Longitude,
  b.Altitude,
  b.Direction,
  b.Speed,
  b.Valid,
  b.`Events`,
  b.Sensor1,
  b.Sensor2,
  b.Sensor3,
  b.Sensor4,
  b.Sensor5,
  b.Sensor6,
  b.Sensor7,
  b.Sensor8,
  b.Counter1,
  b.Counter2,
  b.Counter3,
  b.Counter4,
  b.whatIs
FROM datagpsbuffer b, temp1 t
WHERE b.DataGps_ID = t.DataGps_ID
GROUP BY b.Mobitel_ID, b.LogID;


DELETE b
FROM datagpsbuffer b, temp1 t
WHERE b.DataGps_ID = t.DataGps_ID;


DROP TEMPORARY TABLE temp1;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `TransferLiteBuffer`
--

DROP PROCEDURE IF EXISTS `TransferLiteBuffer`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `TransferLiteBuffer`()
    SQL SECURITY INVOKER
    COMMENT 'Переносит GPS данные из буферной таблицы в datagps.'
BEGIN





CREATE TEMPORARY TABLE IF NOT EXISTS temp2 ENGINE=HEAP(
  SELECT DataGPS_ID FROM datagpsbufferlite
  );



UPDATE datagps d, datagpsbuffer b, temp2 t SET
  d.Message_ID = b.Message_ID,
  d.Latitude = b.Latitude,
  d.Longitude = b.Longitude,
  d.Altitude = b.Altitude,
  d.UnixTime = b.UnixTime,
  d.Speed = b.Speed,
  d.Direction = b.Direction,
  d.Valid = b.Valid,
  d.InMobitelID = NULL, 
  d.`Events` = b.`Events`,
  d.Sensor1 = b.Sensor1,
  d.Sensor2 = b.Sensor2,
  d.Sensor3 = b.Sensor3,
  d.Sensor4 = b.Sensor4,
  d.Sensor5 = b.Sensor5,
  d.Sensor6 = b.Sensor6,
  d.Sensor7 = b.Sensor7,
  d.Sensor8 = b.Sensor8,
  d.isShow = b.IsShow,
  d.whatIs = b.whatIs,
  d.Counter1 = b.Counter1,
  d.Counter2 = b.Counter2,
  d.Counter3 = b.Counter3,
  d.Counter4 = b.Counter4
WHERE b.DataGps_ID = t.DataGps_ID
  AND d.Mobitel_ID = b.Mobitel_ID
  AND d.Latitude = b.Latitude
  AND d.Longitude = b.Longitude
  AND d.Altitude = b.Altitude
  AND d.UnixTime = b.UnixTime;


DELETE b
FROM datagps d, datagpsbufferlite b, temp2 t
WHERE b.DataGps_ID = t.DataGps_ID
  AND d.Mobitel_ID = b.Mobitel_ID
  AND d.Latitude = b.Latitude
  AND d.Longitude = b.Longitude
  AND d.Altitude = b.Altitude
  AND d.UnixTime = b.UnixTime;


INSERT INTO datagps
  (
  Mobitel_ID,
  LogID,
  UnixTime,
  Latitude,
  Longitude,
  Altitude,
  Direction,
  Speed,
  Valid,
  `Events`,
  Sensor1,
  Sensor2,
  Sensor3,
  Sensor4,
  Sensor5,
  Sensor6,
  Sensor7,
  Sensor8,
  Counter1,
  Counter2,
  Counter3,
  Counter4,
  whatIs
  )
SELECT
  b.Mobitel_ID,
  b.LogID,
  b.UnixTime,
  b.Latitude,
  b.Longitude,
  b.Altitude,
  b.Direction,
  b.Speed,
  b.Valid,
  b.`Events`,
  b.Sensor1,
  b.Sensor2,
  b.Sensor3,
  b.Sensor4,
  b.Sensor5,
  b.Sensor6,
  b.Sensor7,
  b.Sensor8,
  b.Counter1,
  b.Counter2,
  b.Counter3,
  b.Counter4,
  b.whatIs
FROM datagpsbufferlite b, temp2 t
WHERE b.DataGps_ID = t.DataGps_ID
GROUP BY b.Mobitel_ID, b.LogID, b.Latitude, b.Longitude, b.Altitude, b.UnixTime;


DELETE b
FROM datagpsbufferlite b, temp2 t
WHERE b.DataGps_ID = t.DataGps_ID;


DROP TEMPORARY TABLE temp2;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `UpdateDatabase`
--

DROP PROCEDURE IF EXISTS `UpdateDatabase`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateDatabase`()
    SQL SECURITY INVOKER
    COMMENT 'Установка привилегий пользователей и модификация базы данных.'
BEGIN
  DECLARE need_exit BOOL DEFAULT FALSE;
  DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN END;

  IF (CheckVersion('5.0.37') = 0) THEN
    SELECT 'Ошибка! Версия СУБД должна быть 5.0.37 или выше!';
  ELSEIF (HaveSuperPrivileges() = 0) THEN
    SELECT 'Ошибка! Пользователь не имеет привилегии SUPER!';
  ELSE
    BEGIN
      DECLARE need_exit BOOL DEFAULT FALSE;

      BEGIN
        DECLARE EXIT HANDLER FOR SQLEXCEPTION
          BEGIN
            SELECT 'Ошибка! База данных имеет неправильную структуру.';
            SET need_exit = TRUE;
          END;
        CALL IsTeletrackDatabase();
      END;

      IF (need_exit = FALSE) THEN
        BEGIN

        DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN END;
        CREATE USER 'gradient' IDENTIFIED BY 'gradient';
        CREATE USER 'user';

        GRANT CREATE TEMPORARY TABLES,DELETE,INDEX,INSERT,LOCK TABLES,SELECT,
          SHOW DATABASES,UPDATE,CREATE,DROP,GRANT OPTION,ALTER, CREATE ROUTINE,
          ALTER ROUTINE, EXECUTE, CREATE VIEW
          ON *.* TO 'gradient' IDENTIFIED BY 'gradient';

        GRANT CREATE TEMPORARY TABLES,DELETE,INDEX,INSERT,LOCK TABLES,SELECT,
          SHOW DATABASES,UPDATE,CREATE,DROP,GRANT OPTION,ALTER, CREATE ROUTINE,
          ALTER ROUTINE, EXECUTE, CREATE VIEw
          ON *.* TO 'user';

        DROP TABLE IF EXISTS tmpdatagps;

        CREATE OR REPLACE ALGORITHM = MERGE VIEW internalmobitelconfigview AS
          (SELECT c.ID, MAX(c.InternalMobitelConfig_ID) AS LastRecord
          FROM internalmobitelconfig c
          WHERE c.devIdShort <> ''
          GROUP BY c.ID);

        RESET QUERY CACHE;

        CREATE OR REPLACE ALGORITHM = MERGE VIEW deviceview AS
          (SELECT m.Mobitel_ID, m.Name, m.Descr, c.devIdShort, m.DeviceEngine,
          c.RfEngine, m.ConfirmedID, c.moduleIdRf, c.ActiveRf
          FROM mobitels m, internalmobitelconfig c, internalmobitelconfigview v
          WHERE v.ID = m.InternalMobitelConfig_ID
          AND v.LastRecord = c.InternalMobitelConfig_ID
          ORDER BY m.Name);

        DELETE FROM datagps WHERE Mobitel_ID = -1;
        DELETE FROM datagps
          WHERE (Sign(`Longitude`/600000 - 180) = 1 or Sign(`Latitude`/600000 - 90) = 1) and `Valid` = 1;

        DROP TABLE IF EXISTS `datagpslosted`;
        CREATE TABLE `datagpslosted` (
        `Mobitel_ID` int(11)  NOT NULL DEFAULT '0',
        `Begin_LogID` int(11)  NOT NULL DEFAULT '0',
        `End_LogID` int(11)  NOT NULL DEFAULT '0',
        PRIMARY KEY `PK_DATAGPSLOSTED` (`Mobitel_ID`, `Begin_LogID`)
        );

        DROP TABLE IF EXISTS `#MUTEX_FILL_LOST_DATA_GPS`;

        ALTER TABLE datagps MODIFY COLUMN `Events` INTEGER UNSIGNED DEFAULT 0,
          MODIFY COLUMN `Sensor1` TINYINT(4) UNSIGNED DEFAULT 0,
          MODIFY COLUMN `Sensor2` TINYINT(4) UNSIGNED DEFAULT 0,
          MODIFY COLUMN `Sensor3` TINYINT(4) UNSIGNED DEFAULT 0,
          MODIFY COLUMN `Sensor4` TINYINT(4) UNSIGNED DEFAULT 0,
          MODIFY COLUMN `Sensor5` TINYINT(4) UNSIGNED DEFAULT 0,
          MODIFY COLUMN `Sensor6` TINYINT(4) UNSIGNED DEFAULT 0,
          MODIFY COLUMN `Sensor7` TINYINT(4) UNSIGNED DEFAULT 0,
          MODIFY COLUMN `Sensor8` TINYINT(4) UNSIGNED DEFAULT 0;

        ALTER TABLE mobitels ADD COLUMN `DeviceEngine` int(11) DEFAULT 0;
        ALTER TABLE mobitels ADD COLUMN `ConfirmedID` int(11) DEFAULT 0;
        ALTER TABLE mobitels MODIFY COLUMN `ID` BIGINT DEFAULT 0;
        ALTER TABLE internalmobitelconfig
          ADD COLUMN `RfEngine` SMALLINT NOT NULL DEFAULT 0 AFTER `moduleIdRf`,
          ADD COLUMN `ActiveRf` BOOL NOT NULL DEFAULT 0 AFTER `RfEngine`;
        
        ALTER TABLE serviceinit ADD COLUMN `Active` TINYINT UNSIGNED
          NOT NULL DEFAULT 0 AFTER `MobitelActive_ID`;
        ALTER IGNORE TABLE datagps DROP INDEX Index_2;
        ALTER IGNORE TABLE datagps DROP INDEX Index_3;
        ALTER TABLE datagps ADD INDEX Index_2(`Mobitel_ID`, `LogID`);

        
        CREATE TABLE IF NOT EXISTS datagpsbuffer ENGINE=MyISAM
          (SELECT * FROM datagps WHERE DataGps_ID = -1);

        ALTER TABLE datagpsbuffer MODIFY COLUMN DataGps_ID
          INTEGER NOT NULL DEFAULT NULL AUTO_INCREMENT,
          ADD PRIMARY KEY (DataGps_ID);

        CREATE TABLE IF NOT EXISTS datagpsbufferlite ENGINE=MyISAM
          (SELECT * FROM datagps WHERE DataGps_ID = -1);

        ALTER TABLE datagpsbufferlite MODIFY COLUMN DataGps_ID
          INTEGER NOT NULL DEFAULT NULL AUTO_INCREMENT,
          ADD PRIMARY KEY (DataGps_ID);

        SELECT 'OK';

        END;
      END IF;
    END;
  END IF;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of view `datavalue1`
--

DROP TABLE IF EXISTS `datavalue1`;
DROP VIEW IF EXISTS `datavalue1`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `datavalue1` AS select round((`datagps`.`Longitude` / 600000),6) AS `lon`,round((`datagps`.`Latitude` / 600000),6) AS `lat`,`datagps`.`Mobitel_ID` AS `Mobitel_ID`,`sensordata`.`Value` AS `Value`,(`datagps`.`Speed` * 1.852) AS `speed`,from_unixtime(`datagps`.`UnixTime`) AS `time`,`sensordata`.`sensor_id` AS `sensor_id`,`datagps`.`DataGps_ID` AS `datavalue_id` from (`sensordata` join `datagps` on((`sensordata`.`datagps_id` = `datagps`.`DataGps_ID`))) where (`datagps`.`Valid` = 1) order by `datagps`.`UnixTime`,`datagps`.`Mobitel_ID`,`sensordata`.`sensor_id`;

--
-- Definition of view `dataview`
--

DROP TABLE IF EXISTS `dataview`;
DROP VIEW IF EXISTS `dataview`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dataview` AS select `datagps`.`DataGps_ID` AS `DataGps_ID`,`datagps`.`Mobitel_ID` AS `Mobitel_ID`,(`datagps`.`Latitude` / 600000.00000) AS `Lat`,(`datagps`.`Longitude` / 600000.00000) AS `Lon`,`datagps`.`Altitude` AS `Altitude`,from_unixtime(`datagps`.`UnixTime`) AS `time`,(`datagps`.`Speed` * 1.852) AS `speed`,((`datagps`.`Direction` * 360) / 255) AS `direction`,`datagps`.`Valid` AS `Valid`,(((((((`datagps`.`Sensor8` + (`datagps`.`Sensor7` << 8)) + (`datagps`.`Sensor6` << 16)) + (`datagps`.`Sensor5` << 24)) + (`datagps`.`Sensor4` << 32)) + (`datagps`.`Sensor3` << 40)) + (`datagps`.`Sensor2` << 48)) + (`datagps`.`Sensor1` << 56)) AS `sensor`,`datagps`.`Events` AS `Events`,`datagps`.`LogID` AS `LogID` from `datagps` order by `datagps`.`LogID`;

--
-- Definition of view `deviceview`
--

DROP TABLE IF EXISTS `deviceview`;
DROP VIEW IF EXISTS `deviceview`;
CREATE ALGORITHM=MERGE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `deviceview` AS (select `m`.`Mobitel_ID` AS `Mobitel_ID`,`m`.`Name` AS `Name`,`m`.`Descr` AS `Descr`,`c`.`devIdShort` AS `devIdShort`,`m`.`DeviceType` AS `DeviceType`,`m`.`ConfirmedID` AS `ConfirmedID`,`c`.`moduleIdRf` AS `moduleIdRf` from ((`mobitels` `m` join `internalmobitelconfig` `c`) join `internalmobitelconfigview` `v`) where ((`v`.`ID` = `m`.`InternalMobitelConfig_ID`) and (`v`.`LastRecord` = `c`.`InternalMobitelConfig_ID`)) order by `m`.`Name`);

--
-- Definition of view `internalmobitelconfigview`
--

DROP TABLE IF EXISTS `internalmobitelconfigview`;
DROP VIEW IF EXISTS `internalmobitelconfigview`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `internalmobitelconfigview` AS (select `c`.`ID` AS `ID`,max(`c`.`InternalMobitelConfig_ID`) AS `LastRecord` from `internalmobitelconfig` `c` where (`c`.`devIdShort` <> _cp1251'') group by `c`.`ID`);

