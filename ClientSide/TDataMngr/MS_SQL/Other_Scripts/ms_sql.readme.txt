02.06.2011 

������� �������   KEY `Index_5` (`Mobitel_ID`,`Counter1`,`Counter2`,`Counter3`,`Counter4`),  KEY `Index_4` (`Message_ID`)
��  ������� DataGPS


17.05.2010
  1. ������ ���������� ������ IDX_MobitelID_BeginLogID �� ������� 
    datagpslost_on, ����������� primary key.
  2. ��������� �������� ��������� OnDeleteDuplicates. ������� ��������� �����
    �� ������� datagpsbuffer_on. ��������� ������ �������� ���������� ��������
    � ����� Mobitel_Id + LogID. �� ��������� ������������� ������� � �������
    datagpsbuffer_on ��������� ������ ���� - ��������� �������� ��������������
    ��������, �.�. � ������������ SrvPacketID.
  3. ��������� �������� ��������� OnTransferBuffer: �������� ����� ��
    OnDeleteDuplicates. 

10.12.2009
  ����������� ������ �������� �������� OnListMobitelConfig � 
  RfListMobitelConfig.

04.09.2009
  ������� � ������� online �������������� � ��������������� ������� 
  (������ ������������� �� unixtime ASC). ���������� ���������:
  nTransferBuffer, DrTransferBuffer, PopTransferBuffer, RfTransferBuffer.

02.09.2009
  ��������� ���������� ������ ������� online � ��������� ����������:
  OnTransferBuffer, DrTransferBuffer, PopTransferBuffer, RfTransferBuffer.

08.07.2009
  1. ��������� ������� ����������� ��� ������ ���������� TrackControl 1.25:
    * ��������� �������:
      - driver - ���������� ���������;
      - team - ���������� ������� �����;
      - vehicle - ���������� �����������;
      - lines - �����;
      - setting - ��������� �������� ��� ���������� �������;
      - ver - ������ ��.
    * �������� ������� ������� sensoralgorithms:
      - COLUMN `Name` CHAR(45) COLLATE cp1251_general_ci NOT NULL;
      - COLUMN `AlgorithmID` INTEGER(11) NOT NULL DEFAULT '0';
    * ��������� �������� ��������� CheckOdo, dataview, hystoryOnline, online.
    * ������� sensoralgorithms ����������� ������ �������:
      - '�������2','������� ������� � ����2',8;
      - '���������1','��� �������� ���������� ����������1',9;
      - '���������2','��� �������� ���������� ����������2',10;
      - '�����1','��� ����������1',11;
      - '�����2','��� ����������2',12.
  2. ��������� ��������� ��������� Infotrack:  
    * �������� ������ �� ���� SrvPacketID � ������� datagpsbuffer_on.
    * ��������� �������� ��������� OnCorrectInfotrackLogId, ������� ������� 
      ����������� �������� LogId � InMobitelID ��� ������� ��������� �� 
      ����������;
    * ���������� �������� ��������� OnTransferBuffer - �������� ����� 
      OnCorrectInfotrackLogId().

13.03.2009
  1. ��������� ������� Online ��� ����������� ����������� ����������
  2. ��������� ��������� RfTransferBuffer - ���������� ������� Online.
  3. ��������� ���������� Pop3Pump:
    * ����� ������� - datagpsbuffer_pop;
    * ����� �������� ��������� - PopListMobitelConfig, PopTransferBuffer.
  4. ��������� ���������� TDataManager:
    * ����������� ������� datagps:
      - ADD COLUMN SrvPacketID BIGINT
      - ADD INDEX IDX_MobitelIDSrvPacketID(Mobitel_ID, SrvPacketID)
    * ����� ������� - datagpsbuffer_on, datagpsbuffer_ontmp, datagpslost_on,
      datagpslost_ontmp;
    * ����� �������� ��������� - OnListMobitelConfig, OnLostDataGPS, 
      OnLostDataGPS2, OnTransferBuffer, OnDeleteLostRange;
    * ������� �������� ���� ������� Online � ������� ServiceTypes;
  3. ��������� ���������� DirectOnline:
    * ����� ������� - datagpsbuffer_dr, SwUpdate;
    * ����� �������� ��������� - DrListMobitelConfig, DrTransferBuffer.

16.01.2009
  ��������� ��������� RfTransferBuffer - ���������� ��������� ���������� ������
  � ��������� logId ��� �������������� � ���� ��� ������� ���������. ����� ����
  InMobitelID � ���� ������ �������������� NULL, ������ = LogID.

08.01.2009
  ��������� ��������� RfTransferBuffer - ��� ������� ����� ������
  � ������� DataGps ����� ����������� ���� InMobitelID ������ LogID,
  ��� ����, ����� ��������� ������������ ������ � MCA3.

02.10.2008
  ���������� RfListMobitelConfig, RfMobitelConfig, RfFillLostDataGPS - 
  ��������� � ������ �������� (��� ���� �� 30.07.2008). 

25.09.2008
  ����������������� ������ ��� ����������� ������, ����������� � ������
  �������� ������ ���������.

22.09.2008
  �������� ��������� ������ ��� ������ � ���������.
  ���������������� ���� ������������ � �������� ������:
    * ���� sensoralgorithms.AlgorithmID ����������� � SIGNED;
    * ���� sensorcoefficient.Sensor_ID ����������� � SIGNED.

16.09.2008
  ������� ���������� ������. � ���� ������ �������� 1 �������� � 
  ������������� RCS.

30.07.2008
  ���������� ���������� � ���������� RfListMobitelConfig, RfMobitelConfig,
  RfFillLostDataGPS - ������� ���������� ���������� ��� ������ � RF ��������. 
  ����� ���������� �� ������� internalmobitelconfig �������� ������, � ������� 
  ���� ��������� ���� moduleIdRf. ��� �� ������ ��������� �.� ���� ����������� 
  "��������" ��� ��������, � ����� ��������� ������ � internalmobitelconfig 
  ��� ������� ��������� ����� ��������� ������ �������� moduleIdRf, ��� 
  ���������� ��� "������ �������� �� ��������� � RF �������".

10.07.2008
  ��������� ������������� dataview.

05.06.2008
  ��������� ������� sensoralgorithms � relationalgorithms.
  �������������� ������� sensors.

05.06.2008
  ���������:
    * ������� datagpslost, datagpslost_rftmp;
    * ���� � ������� mobitels: LastInsertTime INT NOT NULL DEFAULT '0' 
      (����� ��������� ������� ������ � DataGPS);
    * ������ � ������� datagpsbuffer: IDX_MobitelIDLogID(Mobitel_ID, LogID);
    * ��������� RfFillLostDataGPS, RfLostDataGPS, RfTransferBuffer, 
      RfListMobitelConfig, RfMobitelConfig

06.05.2008
  � ������� internalmobitelconfig ������� ���� RfType � ActiveRf.

�����!
��� �������������� ���� ������ ���������� ��������� Restore 
� MySQL Administrator-� � ������� �����������������.