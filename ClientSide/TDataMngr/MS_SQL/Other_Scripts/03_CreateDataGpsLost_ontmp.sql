IF EXISTS (SELECT * FROM sys.objects WHERE name = 'datagpslost_ontmp')
    DROP TABLE datagpslost_ontmp
GO

CREATE TABLE dbo.datagpslost_ontmp(
  LogID INT NOT NULL DEFAULT (0),
  SrvPacketID BIGINT NOT NULL
) ON [PRIMARY]
GO