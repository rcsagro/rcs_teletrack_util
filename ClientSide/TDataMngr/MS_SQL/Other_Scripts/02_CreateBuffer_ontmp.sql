IF EXISTS (SELECT * FROM sys.objects WHERE name = 'datagpsbuffer_ontmp')
    DROP TABLE datagpsbuffer_ontmp
GO

CREATE TABLE dbo.datagpsbuffer_ontmp(
  Mobitel_ID INT NOT NULL DEFAULT (0),
  LogID INT NOT NULL DEFAULT (0),
  SrvPacketID BIGINT NOT NULL DEFAULT (0)
) ON [PRIMARY]
GO