IF EXISTS (SELECT * FROM sys.objects WHERE name = 'tmpAddIndex')
    DROP TABLE dbo.tmpAddIndex
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.tmpAddIndex
AS
--NOT DETERMINISTIC
--CONTAINS SQL
--SQL SECURITY INVOKER
--COMMENT '��������� ��������� ���������� ������� � DataGpsBuffer_on'
BEGIN
  DECLARE @DbName VARCHAR(64); /* ������������ ������� �� */
  DECLARE @IndexExist TINYINT;
  SET @IndexExist = 0;

  SET @DbName = (SELECT db_name(@@procid));

  SET @IndexExist = (SELECT count(1)
                     FROM
                       sys.indexes i
                     WHERE
                       i.name = 'IDX_SrvPacketID');

  IF @IndexExist = 0
  BEGIN
    CREATE INDEX IDX_SrvPacketID ON DataGpsBuffer_on (SrvPacketID);
    ALTER TABLE DataGpsBuffer_on ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON);
  END;
END

EXEC tmpAddIndex
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'tmpAddIndex')
    DROP TABLE dbo.tmpAddIndex
GO
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'OnDeleteDuplicates')
    DROP TABLE dbo.OnDeleteDuplicates
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnDeleteDuplicates
AS
BEGIN
  /* ??????? ????????? ????? ?? ??????? datagpsbuffer_on. ????????? ??????
   ???????? ?????????? ???????? ? ????? Mobitel_Id + LogID. ?? ?????????
   ????????????? ??????? ? ??????? datagpsbuffer_on ????????? ?????? ???? - 
   ????????? ???????? ?????????????? ????????, ?.?. ? ???????????? SrvPacketID. */

  /* ??????? MobitelId ? ??????? CursorDuplicateGroups */
  DECLARE @MobitelIdInCursor INT;
  SET @MobitelIdInCursor = 0;
  /* ??????? LogID ? ??????? CursorDuplicateGroups */
  DECLARE @LogIdInCursor INT;
  SET @LogIdInCursor = 0;
  /* Max SrvPacketId ? ????????? ????? ??????? ?????? ?????????? */
  DECLARE @MaxSrvPacketId BIGINT;
  SET @MaxSrvPacketId = 0;
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /* ?????? ??? ??????? ?? ???? ??????? ?????????? */
  DECLARE CursorDuplicateGroups CURSOR LOCAL FOR
  SELECT Mobitel_Id
       , LogId
  FROM
    datagpsbuffer_on
  GROUP BY
    Mobitel_Id
  , LogId
  HAVING
    count(LogId) > 1;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  --SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SELECT @MaxSrvPacketId = max(SrvPacketID)
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor)
      AND (LogId = @LogIdInCursor);
    DELETE
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor) AND
      (LogId = @LogIdInCursor) AND
      (SrvPacketID < @MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  END;
  CLOSE CursorDuplicateGroups;
--DEALLOCATE CursorDuplicateGroups;
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'OnCorrectInfotrackLogId')
    DROP TABLE dbo.OnCorrectInfotrackLogId
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnCorrectInfotrackLogId
AS
--SQL SECURITY INVOKER
--COMMENT '������������� �������� LogId � InMobitelID � ������ ����������'
BEGIN
  DECLARE @MobitelIDInCursor INT; /* ??????? ???????? MobitelID */
  DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  SET @DataExists = 1;
  DECLARE @MaxLogId INT; /* ???????????? LogId ??? ??????? ??????? */
  SET @MaxLogId = 0;
  DECLARE @SrvPacketIDInCursor BIGINT; /* ??????? ???????? ?????????? ?????? */
  SET @SrvPacketIDInCursor = 0;

  /* ?????? ?? ?????????? ? ????????? ????? ??????. 
     ??? ????????? ?????????? ? ????? I */
  DECLARE CursorInfoTrack CURSOR LOCAL FOR
  SELECT DISTINCT buf.Mobitel_ID
  FROM
    datagpsbuffer_on buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort LIKE 'I%')
    AND (buf.LogId = 0)
  ORDER BY
    1;

  /* ?????? ?? ?????? ????????? */
  DECLARE CursorInfoTrackUpdateSet CURSOR LOCAL FOR
  SELECT SrvPacketID
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelIDInCursor)
    AND (LogId = 0)
  ORDER BY
    UnixTime;

  /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* ??????? ???????????? LogId ??? ??????? ????????? */
    SET @MaxLogId = (SELECT coalesce(max(LogID), 0)
                     FROM
                       DataGps
                     WHERE
                       Mobitel_ID = @MobitelIDInCursor);

    /* ????????? LogId ? InMobitelID ? ?????? ????? ?????? ???????? ????????? */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      SET @MaxLogId = @MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET
        LogId = @MaxLogId, InMobitelID = @MaxLogId
      WHERE
        SrvPacketID = @SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    END;
    CLOSE CursorInfoTrackUpdateSet;
    --DEALLOCATE CursorInfoTrackUpdateSet;

    --SET DataExists = 1;
    FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  END;
  CLOSE CursorInfoTrack;
--DEALLOCATE CursorInfoTrack;
END
GO



IF EXISTS (SELECT * FROM sys.objects WHERE name = 'OnTransferBuffer')
    DROP TABLE dbo.OnTransferBuffer
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnTransferBuffer
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  --DECLARE @DataExists TINYINT; /*  */
  --SET @DataExists = 1;
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC OnDeleteDuplicates
  EXEC OnCorrectInfotrackLogId

  /* OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp(Mobitel_ID
                                , LogID
                                , SrvPacketID)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.SrvPacketID
  FROM
    datagpsbuffer_on b
    LEFT JOIN datagps d
      ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  WHERE
    d.LogID IS NOT NULL;

  /* --------------------  Online ---------------------- */
  /*  online  datagpsbuffer_on */
  /*  Mobitel_ID LogID */
  DELETE o
  FROM
    online o, datagpsbuffer_on dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
    INSERT INTO dbo.online(Mobitel_ID
                         , LogID
                         , UnixTime
                         , Latitude
                         , Longitude
                         , Altitude
                         , Direction
                         , Speed
                         , Valid
                         , Events
                         , Sensor1
                         , Sensor2
                         , Sensor3
                         , Sensor4
                         , Sensor5
                         , Sensor6
                         , Sensor7
                         , Sensor8
                         , Counter1
                         , Counter2
                         , Counter3
                         , Counter4
                         , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , Events
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_on
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;

    /*  100 */
    DELETE
    FROM
      online
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        online
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  /* .. */
  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = b.LogID, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4, SrvPacketID = b.SrvPacketID
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID) AND
    (d.SrvPacketID < b.SrvPacketID);

  /*  */
  DELETE b
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  /* */
  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs
                    , SrvPacketID)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , Events
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
       , SrvPacketID
  FROM
    datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);*/

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS2 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = CREATEST(@NeedOptimize, @NeedOptimizeParam); */

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsUpdateSet;
  --DEALLOCATE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  /*  */
  IF @NeedOptimize = 1
    ALTER INDEX ALL ON datagpslost_on REORGANIZE
END

GO