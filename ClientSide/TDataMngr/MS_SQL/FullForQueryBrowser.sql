ALTER TABLE dbo.datagps ADD SrvPacketID BIGINT NOT NULL DEFAULT '0';
  --COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������';

UPDATE datagps
SET
  SrvPacketID = 0
WHERE
  SrvPacketID IS NULL;
  
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'IDX_MobitelIDSrvPacketID')
  DROP INDEX IDX_MobitelIDSrvPacketID ON dbo.datagps;

CREATE INDEX IDX_MobitelIDSrvPacketID ON dbo.datagps (Mobitel_ID, SrvPacketID);

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'datagpsbuffer_on')
    DROP TABLE dbo.datagpsbuffer_on
GO

CREATE TABLE dbo.datagpsbuffer_on(
  Mobitel_ID INT NOT NULL DEFAULT (0),
  Message_ID INT NULL DEFAULT (0),
  Latitude INT NOT NULL DEFAULT (0),
  Longitude INT NOT NULL DEFAULT (0),
  Altitude INT NULL,
  UnixTime INT NOT NULL DEFAULT (0),
  Speed SMALLINT NOT NULL DEFAULT (0),
  Direction INT NOT NULL DEFAULT (0),
  Valid SMALLINT NOT NULL DEFAULT (1),
  InMobitelID INT NULL,
  Events BIGINT NULL DEFAULT (0),
  Sensor1 TINYINT NULL DEFAULT (0),
  Sensor2 TINYINT NULL DEFAULT (0),
  Sensor3 TINYINT NULL DEFAULT (0),
  Sensor4 TINYINT NULL DEFAULT (0),
  Sensor5 TINYINT NULL DEFAULT (0),
  Sensor6 TINYINT NULL DEFAULT (0),
  Sensor7 TINYINT NULL DEFAULT (0),
  Sensor8 TINYINT NULL DEFAULT (0),
  LogID INT NOT NULL DEFAULT (0),
  isShow SMALLINT NULL DEFAULT (0),
  whatIs SMALLINT NOT NULL DEFAULT (0),
  Counter1 INT NOT NULL DEFAULT (-1),
  Counter2 INT NOT NULL DEFAULT (-1),
  Counter3 INT NOT NULL DEFAULT (-1),
  Counter4 INT NOT NULL DEFAULT (-1),
  SrvPacketID BIGINT NOT NULL DEFAULT (0)
) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'datagpsbuffer_ontmp')
    DROP TABLE datagpsbuffer_ontmp
GO

CREATE TABLE dbo.datagpsbuffer_ontmp(
  Mobitel_ID INT NOT NULL DEFAULT (0),
  LogID INT NOT NULL DEFAULT (0),
  SrvPacketID BIGINT NOT NULL DEFAULT (0)
) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'online')
    DROP TABLE online
GO

CREATE TABLE dbo.online(
  DataGps_ID INT IDENTITY,
  Mobitel_ID INT NULL DEFAULT (0),
  Message_ID INT NULL DEFAULT (0),
  Latitude INT NOT NULL DEFAULT (0),
  Longitude INT NOT NULL DEFAULT (0),
  Altitude INT NULL DEFAULT (0),
  UnixTime INT NOT NULL DEFAULT (0),
  Speed SMALLINT NOT NULL DEFAULT (0),
  Direction INT NOT NULL DEFAULT (0),
  Valid SMALLINT NOT NULL DEFAULT (1),
  InMobitelID INT NULL DEFAULT (0),
  Events BIGINT NULL DEFAULT (0),
  Sensor1 TINYINT NULL DEFAULT (0),
  Sensor2 TINYINT NULL DEFAULT (0),
  Sensor3 TINYINT NULL DEFAULT (0),
  Sensor4 TINYINT NULL DEFAULT (0),
  Sensor5 TINYINT NULL DEFAULT (0),
  Sensor6 TINYINT NULL DEFAULT (0),
  Sensor7 TINYINT NULL DEFAULT (0),
  Sensor8 TINYINT NULL DEFAULT (0),
  LogID INT NULL DEFAULT (0),
  isShow SMALLINT NULL DEFAULT (0),
  whatIs INT NOT NULL DEFAULT (0),
  Counter1 INT NOT NULL DEFAULT (-1),
  Counter2 INT NOT NULL DEFAULT (-1),
  Counter3 INT NOT NULL DEFAULT (-1),
  Counter4 INT NOT NULL DEFAULT (-1),
  CONSTRAINT PK_online PRIMARY KEY (DataGps_ID)
) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'datagpslost_on')
    DROP TABLE datagpslost_on
GO

CREATE TABLE dbo.datagpslost_on(
  Mobitel_ID INT NOT NULL DEFAULT (0),
  Begin_LogID INT NOT NULL DEFAULT (0),
  End_LogID INT NOT NULL DEFAULT (0),
  Begin_SrvPacketID BIGINT NOT NULL DEFAULT (0),
  End_SrvPacketID BIGINT NOT NULL DEFAULT (0),
  CONSTRAINT PK_datagpslost_on PRIMARY KEY (Mobitel_ID, Begin_LogID)
) ON [PRIMARY]
GO

EXEC sp_addextendedproperty N'MS_Description', '�������� LogID ����� �������� ���������� ������', 'SCHEMA', N'dbo', 'TABLE', N'datagpslost_on', 'COLUMN', N'Begin_LogID'
GO

EXEC sp_addextendedproperty N'MS_Description', '�������� LogID ����� ������� ����������� ������', 'SCHEMA', N'dbo', 'TABLE', N'datagpslost_on', 'COLUMN', N'End_LogID'
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'datagpslost_ontmp')
    DROP TABLE datagpslost_ontmp
GO

CREATE TABLE dbo.datagpslost_ontmp(
  LogID INT NOT NULL DEFAULT (0),
  SrvPacketID BIGINT NOT NULL
) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'OnListMobitelConfig')
    DROP TABLE dbo.OnListMobitelConfig
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnListMobitelConfig
AS
BEGIN
  DECLARE @MobitelIDInCursor INT;
  DECLARE @DevIdShortInCursor CHAR(4);
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /*  */
  DECLARE CursorMobitels CURSOR LOCAL FOR
  SELECT m.Mobitel_ID AS MobitelID
       , c.devIdShort AS DevIdShort
  FROM
    mobitels m
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort IS NOT NULL)
    AND (c.devIdShort <> '')
  ORDER BY
    m.Mobitel_ID;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /*  */
  CREATE TABLE tmpMobitelsConfig(
    MobitelID INT NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL
  );

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    INSERT INTO tmpMobitelsConfig
    SELECT @MobitelIDInCursor
         , @DevIdShortInCursor
         , (SELECT coalesce(max(SrvPacketID), 0)
            FROM
              datagps
            WHERE
              Mobitel_ID = @MobitelIDInCursor) AS LastPacketID;

    FETCH CursorMobitels INTO @MobitelIDInCursor, @DevIdShortInCursor;
  END;
  CLOSE CursorMobitels;
  --DEALLOCATE CursorMobitels;

  SELECT MobitelID
       , DevIdShort
       , LastPacketID
  FROM
    tmpMobitelsConfig;

  DROP TABLE tmpMobitelsConfig;
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'OnLostDataGPS')
    DROP TABLE dbo.OnLostDataGPS
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnLostDataGPS
(
  @MobitelID    INT,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT '����� ��������� � online ������ ��������� ��������� � ����������'
BEGIN

  /********** DECLARE **********/

  DECLARE @CurrentConfirmedID INT; /* ??????? ???????? ConfirmedID */
  SET @CurrentConfirmedID = 0;
  DECLARE @NewConfirmedID INT; /* ????? ???????? ConfirmedID */
  SET @NewConfirmedID = 0;
  DECLARE @StartLogIDSelect INT; /* min LogID ??? ??????? */
  SET @StartLogIDSelect = 0;
  DECLARE @FinishLogIDSelect INT; /* max LogID ??? ??????? */
  SET @FinishLogIDSelect = 0;
  DECLARE @MaxEndLogID INT; /* max End_LogID ? ??????????? ?????????? ??????? ????????? */
  SET @MaxEndLogID = 0;

  DECLARE @MinLogID_n INT; /* ??????????? LogID ????? ConfirmedID ? ????????? ????? ?????? */
  SET @MinLogID_n = 0;
  DECLARE @MaxLogID_n INT; /* ???????????? LogID ? ????????? ????? ?????? */
  SET @MaxLogID_n = 0;

  DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  SET @DataExists = 1;

  DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID */
  DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID */
  DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
  DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
  DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

  DECLARE @tmpSrvPacketID BIGINT; /* ???????? SrvPacketID ??? ?????? ????????????? ?????? ? ??????? ????? ??????, ?? ?????????????? ? ????????????? */
  SET @tmpSrvPacketID = 0;

  /* ?????? ??? ??????? ?? ????? LogID */
  DECLARE CursorNewLogID CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp;

  /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  SET @CurrentConfirmedID = (SELECT coalesce(ConfirmedID, 0)
                             FROM
                               mobitels
                             WHERE
                               Mobitel_ID = @MobitelID);

  /* ??????????? LogID ? ????? ?????? ?????? c ??????? ????? ???????? */
  SELECT @MinLogID_n = min(LogID)
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelID)
    AND (LogID > @CurrentConfirmedID);

  /* ???????????? LogID ? ????? ?????? ?????? c ??????? ????? ???????? */
  SET @MaxLogID_n = (SELECT max(LogID)
                     FROM
                       datagpsbuffer_on
                     WHERE
                       Mobitel_ID = @MobitelID);

  /* ???????????? End_LogID ? ??????????? ?????????? ??????? ????????? */
  SELECT @MaxEndLogID = coalesce(max(End_LogID), 0)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* ???????? ???? ?? ?????? ??? ??????? */
  IF @MinLogID_n IS NOT NULL
  BEGIN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF @MinLogID_n < @MaxEndLogID
    BEGIN
      /* ????  MinLogID_n < MaxEndLogID ?????? ?????? ??????, 
       ??????? ???????? ??????? ????? ?????????. */

      /* ??????? ????????? LogID ??? ?????????. */
      SET @StartLogIDSelect = (SELECT coalesce(Begin_LogID, @MinLogID_n)
                               FROM
                                 datagpslost_on
                               WHERE
                                 (Mobitel_ID = @MobitelID)
                                 AND (@MinLogID_n > Begin_LogID)
                                 AND (@MinLogID_n < End_LogID));

      /* ??????? ????????? LogID ??? ?????????. */
      SET @FinishLogIDSelect = (SELECT coalesce(End_LogID, @MaxLogID_n)
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (@MaxLogID_n > Begin_LogID)
                                  AND (@MaxLogID_n < End_LogID));

      /* ?????? ?????? ?????? ?? ??????? ??????????? ??????? 
         (????? ?????????????, ???? ???????????) */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID >= @StartLogIDSelect) AND
        (Begin_LogID <= @FinishLogIDSelect);

      /* ???? ?????? ?????? LogID = 0 ? ???? ?????? ??? ? DataGps, ?????
         ???? ???????? ?????? ????????? ?????? ??? ???????. */
      IF (@StartLogIDSelect = 0) AND (NOT EXISTS (SELECT 1
                                                  FROM
                                                    datagps
                                                  WHERE
                                                    (Mobitel_ID = @MobitelID)
                                                    AND (LogID = @StartLogIDSelect)))
      BEGIN
        INSERT INTO datagpslost_ontmp(LogID
                                    , SrvPacketID)
        VALUES
          (0, 0);
      END;
    END;
    ELSE
    BEGIN

      /* ?????? ????? ??????. ???? ???????? ??? ??????? ????????????? ?????? - 
         ?????? ??????? */

      /* ??????? ????????? LogID ??? ?????????. */
      SELECT @StartLogIDSelect = coalesce(max(End_LogID), @CurrentConfirmedID)
      FROM
        datagpslost_on
      WHERE
        Mobitel_ID = @MobitelID;

      /* ??????? ????????? LogID ??? ?????????. */
      SELECT @FinishLogIDSelect = max(LogID)
      FROM
        datagpsbuffer_on
      WHERE
        Mobitel_ID = @MobitelID;

      /* ???????? ?? datagps ??????????????? ???????? SrvPacketID  ??? ??????
????????????? ?????? */
      SELECT @tmpSrvPacketID = coalesce(SrvPacketID, 0)
      FROM
        datagps
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (LogID = @StartLogIDSelect);

      /* ???????? ? ??????? datagpslost_ontmp ??? ??????? ????????? 
?????????????? ?????? ?????? */
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@StartLogIDSelect, @tmpSrvPacketID);
    END; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

    /* ?????????? ????????? ??????? ?????????? ??????? ?????????
??? ??????? ???????? */
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT LogID
         , SrvPacketID
    FROM
      datagps
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (LogID >= @StartLogIDSelect)
      AND (LogID <= @FinishLogIDSelect)
    ORDER BY
      LogID;

    /* ???????????? ???????-??????? ? ??????? ??????????? ?????????? */
    --SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstLogID = -9999999
      BEGIN
        /* ?????? ???????? ? ?????? */
        SET @FirstLogID = @NewLogID;
        SET @FirstSrvPacketID = @NewSrvPacketID;
      END
      ELSE
      BEGIN
        /* ???????? ????? ???????*/
        SET @SecondLogID = @NewLogID;
        SET @SecondSrvPacketID = @NewSrvPacketID;

        IF (@SecondLogID - @FirstLogID) > 1
        BEGIN
          /* ?????? ?????? */
          EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

          IF @NeedOptimize = 0
          BEGIN
            SET @NeedOptimize = 1;
          END; -- IF

        END; -- IF
        /* ?????????? ??????? ?????? ???????? ?? ?????? ???????
??? ??????? ? ????????? ???????? ? ???????? ?????????? ???????? */
        SET @FirstLogID = @SecondLogID;
        SET @FirstSrvPacketID = @SecondSrvPacketID;
      END; -- IF
      FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    END; -- WHILE
    CLOSE CursorNewLogID;
    --DEALLOCATE CursorNewLogID; 

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

    /* ???? ??????????? ??????? ? ??????? ????????? ??? - 
????? ConfirmedID ????? ????? ????????????? LogID 
??????? ?????????. ????? - ??????????? Begin_LogID
?? ??????? ????????? */
    SET @NewConfirmedID = (SELECT min(Begin_LogID)
                           FROM
                             datagpslost_on
                           WHERE
                             Mobitel_ID = @MobitelID);

    IF @NewConfirmedID IS NULL
    BEGIN
      SET @NewConfirmedID = (SELECT max(LogID)
                             FROM
                               datagps
                             WHERE
                               Mobitel_ID = @MobitelID);
    END;

    /* ??????? ConfirmedID ???? ???? */
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (ConfirmedID < @NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END; -- IF MaxEndLogID IS NOT NULL  
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'OnLostDataGPS2')
    DROP TABLE dbo.OnLostDataGPS2
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnLostDataGPS2
(
  @MobitelID    INTEGER,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
--COMMENT '����������� ���������� ���������. ������ ����������� ������ ����'
BEGIN
  /********** DECLARE **********/

  DECLARE @NewConfirmedID INT;
  SET @NewConfirmedID = 0; /* ????? ???????? ConfirmedID */
  DECLARE @BeginLogID INT; /* Begin_LogID ? ??????? CursorLostRanges */
  DECLARE @EndLogID INT; /* End_LogID ? ??????? CursorLostRanges */
  DECLARE @BeginSrvPacketID BIGINT; /* Begin_SrvPacketID ? ??????? CursorLostRanges */
  DECLARE @EndSrvPacketID BIGINT; /* End_SrvPacketID ? ??????? CursorLostRanges */
  DECLARE @RowCountForAnalyze INT; /* ?????????? ??????? ??? ??????? */
  SET @RowCountForAnalyze = 0;
  --DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  --SET @DataExists = 1;
  DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
  DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
  DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
  DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
  DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

  /* ?????? ??? ??????? ?? ?????????? ????????? ????????? */
  DECLARE CursorLostRanges CURSOR LOCAL FOR
  SELECT Begin_LogID
       , End_LogID
       , Begin_SrvPacketID
       , End_SrvPacketID
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* ?????? ??? ??????? ?? ????? LogID */
  DECLARE CursorNewLogID2 CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp
  ORDER BY
    SrvPacketID;

  /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* ?????????? ????????? ? ????????? ??????? ???? ??? ???????? */
  --SET DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;

  WHILE @@fetch_status = 0
  BEGIN
    /* ?????????? ????????? ??????? ???????????? ??????? ?????????
       ??? ??????? ???????? */
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT DISTINCT LogID
                  , SrvPacketID
    FROM
      datagpsbuffer_ontmp
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (SrvPacketID > @BeginSrvPacketID)
      AND (SrvPacketID < @EndSrvPacketID)
    ORDER BY
      SrvPacketID;

    /* ???? ???? ?????? ???????????? ???????? */
    SET @RowCountForAnalyze = (SELECT count(1)
                               FROM
                                 datagpslost_ontmp);

    IF @RowCountForAnalyze > 0
    BEGIN
      /* ????????? ?????? ? ????????? ?????? ??? ??????? */
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@BeginLogID, @BeginSrvPacketID)
      , (@EndLogID, @EndSrvPacketID);

      /* ??????? ?????? ???????? */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        --DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
        --DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
        --DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --SET @FirstLogID = -9999999;
        --DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
        --DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

        /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
        --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* ???????????? ???????-??????? ? ??????? ??????????? ?????????? */
        --SET DataExists = 1;
        OPEN CursorNewLogID2;
        FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;

        WHILE @@fetch_status = 0
        BEGIN
          IF @FirstLogID = -9999999
          BEGIN
            /* ?????? ???????? ? ?????? */
            SET @FirstLogID = @NewLogID;
            SET @FirstSrvPacketID = @NewSrvPacketID;
          END
          ELSE
          BEGIN
            /* ???????? ????? ???????*/
            SET @SecondLogID = @NewLogID;
            SET @SecondSrvPacketID = @NewSrvPacketID;

            IF (@SecondSrvPacketID - @FirstSrvPacketID) > 1
            BEGIN
              /* ?????? ?????? */
              EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;

              IF @NeedOptimize = 0
              BEGIN
                SET @NeedOptimize = 1;
              END; -- IF

            END; -- IF
            /* ?????????? ??????? ?????? ???????? ?? ?????? ???????
               ??? ??????? ? ????????? ???????? ? ???????? ?????????? ???????? */
            SET @FirstLogID = @SecondLogID;
            SET @FirstSrvPacketID = @SecondSrvPacketID;
          END; -- ELSE  
          FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;
        END; -- WHILE;
        CLOSE CursorNewLogID2;
      --DEALLOCATE CursorNewLogID2;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END;

    FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;
  END;
  CLOSE CursorLostRanges;
  --DEALLOCATE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* ???? ??????????? ??????? ? ??????? ????????? ??? - 
     ????? ConfirmedID ????? ????? ????????????? LogID 
     ??????? ?????????. ????? - ??????????? Begin_LogID
     ?? ??????? ????????? */
  SELECT @NewConfirmedID = min(Begin_LogID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @NewConfirmedID IS NULL
  BEGIN
    SET @NewConfirmedID = (SELECT max(LogID)
                           FROM
                             datagps
                           WHERE
                             Mobitel_ID = @MobitelID);
  END;

  /* ??????? ConfirmedID ???? ???? */
  UPDATE mobitels
  SET
    ConfirmedID = @NewConfirmedID
  WHERE
    (Mobitel_ID = @MobitelID) AND
    (ConfirmedID < @NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'OnDeleteDuplicates')
    DROP TABLE dbo.OnDeleteDuplicates
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnDeleteDuplicates
AS
BEGIN
  /* ??????? ????????? ????? ?? ??????? datagpsbuffer_on. ????????? ??????
   ???????? ?????????? ???????? ? ????? Mobitel_Id + LogID. ?? ?????????
   ????????????? ??????? ? ??????? datagpsbuffer_on ????????? ?????? ???? - 
   ????????? ???????? ?????????????? ????????, ?.?. ? ???????????? SrvPacketID. */

  /* ??????? MobitelId ? ??????? CursorDuplicateGroups */
  DECLARE @MobitelIdInCursor INT;
  SET @MobitelIdInCursor = 0;
  /* ??????? LogID ? ??????? CursorDuplicateGroups */
  DECLARE @LogIdInCursor INT;
  SET @LogIdInCursor = 0;
  /* Max SrvPacketId ? ????????? ????? ??????? ?????? ?????????? */
  DECLARE @MaxSrvPacketId BIGINT;
  SET @MaxSrvPacketId = 0;
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /* ?????? ??? ??????? ?? ???? ??????? ?????????? */
  DECLARE CursorDuplicateGroups CURSOR LOCAL FOR
  SELECT Mobitel_Id
       , LogId
  FROM
    datagpsbuffer_on
  GROUP BY
    Mobitel_Id
  , LogId
  HAVING
    count(LogId) > 1;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  --SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SELECT @MaxSrvPacketId = max(SrvPacketID)
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor)
      AND (LogId = @LogIdInCursor);
    DELETE
    FROM
      datagpsbuffer_on
    WHERE
      (Mobitel_ID = @MobitelIdInCursor) AND
      (LogId = @LogIdInCursor) AND
      (SrvPacketID < @MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  END;
  CLOSE CursorDuplicateGroups;
--DEALLOCATE CursorDuplicateGroups;
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'OnCorrectInfotrackLogId')
    DROP TABLE dbo.OnCorrectInfotrackLogId
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnCorrectInfotrackLogId
AS
--SQL SECURITY INVOKER
--COMMENT '������������� �������� LogId � InMobitelID � ������ ����������'
BEGIN
  DECLARE @MobitelIDInCursor INT; /* ??????? ???????? MobitelID */
  DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  SET @DataExists = 1;
  DECLARE @MaxLogId INT; /* ???????????? LogId ??? ??????? ??????? */
  SET @MaxLogId = 0;
  DECLARE @SrvPacketIDInCursor BIGINT; /* ??????? ???????? ?????????? ?????? */
  SET @SrvPacketIDInCursor = 0;

  /* ?????? ?? ?????????? ? ????????? ????? ??????. 
     ??? ????????? ?????????? ? ????? I */
  DECLARE CursorInfoTrack CURSOR LOCAL FOR
  SELECT DISTINCT buf.Mobitel_ID
  FROM
    datagpsbuffer_on buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort LIKE 'I%')
    AND (buf.LogId = 0)
  ORDER BY
    1;

  /* ?????? ?? ?????? ????????? */
  DECLARE CursorInfoTrackUpdateSet CURSOR LOCAL FOR
  SELECT SrvPacketID
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelIDInCursor)
    AND (LogId = 0)
  ORDER BY
    UnixTime;

  /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* ??????? ???????????? LogId ??? ??????? ????????? */
    SET @MaxLogId = (SELECT coalesce(max(LogID), 0)
                     FROM
                       DataGps
                     WHERE
                       Mobitel_ID = @MobitelIDInCursor);

    /* ????????? LogId ? InMobitelID ? ?????? ????? ?????? ???????? ????????? */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      SET @MaxLogId = @MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET
        LogId = @MaxLogId, InMobitelID = @MaxLogId
      WHERE
        SrvPacketID = @SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    END;
    CLOSE CursorInfoTrackUpdateSet;
    --DEALLOCATE CursorInfoTrackUpdateSet;

    --SET DataExists = 1;
    FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  END;
  CLOSE CursorInfoTrack;
--DEALLOCATE CursorInfoTrack;
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'OnTransferBuffer')
    DROP TABLE dbo.OnTransferBuffer
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnTransferBuffer
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  --DECLARE @DataExists TINYINT; /*  */
  --SET @DataExists = 1;
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC OnDeleteDuplicates
  EXEC OnCorrectInfotrackLogId

  /* OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp(Mobitel_ID
                                , LogID
                                , SrvPacketID)
  SELECT b.Mobitel_ID
       , b.LogID
       , b.SrvPacketID
  FROM
    datagpsbuffer_on b
    LEFT JOIN datagps d
      ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  WHERE
    d.LogID IS NOT NULL;

  /* --------------------  Online ---------------------- */
  /*  online  datagpsbuffer_on */
  /*  Mobitel_ID LogID */
  DELETE o
  FROM
    online o, datagpsbuffer_on dgb
  WHERE
    (o.Mobitel_ID = dgb.Mobitel_ID) AND
    (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online WITH (INDEX (IDX_Mobitelid))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
    INSERT INTO dbo.online(Mobitel_ID
                         , LogID
                         , UnixTime
                         , Latitude
                         , Longitude
                         , Altitude
                         , Direction
                         , Speed
                         , Valid
                         , Events
                         , Sensor1
                         , Sensor2
                         , Sensor3
                         , Sensor4
                         , Sensor5
                         , Sensor6
                         , Sensor7
                         , Sensor8
                         , Counter1
                         , Counter2
                         , Counter3
                         , Counter4
                         , whatIs)
    SELECT Mobitel_ID
         , LogID
         , UnixTime
         , Latitude
         , Longitude
         , Altitude
         , Direction
         , Speed
         , Valid
         , Events
         , Sensor1
         , Sensor2
         , Sensor3
         , Sensor4
         , Sensor5
         , Sensor6
         , Sensor7
         , Sensor8
         , Counter1
         , Counter2
         , Counter3
         , Counter4
         , whatIs
    FROM
      (SELECT TOP 100 *
       FROM
         datagpsbuffer_on
       WHERE
         (Mobitel_ID = @MobitelIDInCursor)
         AND (UnixTime > @TmpMaxUnixTime)
         AND (Valid = 1)
       ORDER BY
         UnixTime DESC) AS T
    ORDER BY
      UnixTime ASC;

    /*  100 */
    DELETE
    FROM
      online
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        online
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  /* .. */
  UPDATE datagps
  SET
    Latitude = b.Latitude, Longitude = b.Longitude, Altitude = b.Altitude, UnixTime = b.UnixTime, Speed = b.Speed, Direction = b.Direction, Valid = b.Valid, InMobitelID = b.LogID, Events = b.Events, Sensor1 = b.Sensor1, Sensor2 = b.Sensor2, Sensor3 = b.Sensor3, Sensor4 = b.Sensor4, Sensor5 = b.Sensor5, Sensor6 = b.Sensor6, Sensor7 = b.Sensor7, Sensor8 = b.Sensor8, whatIs = b.whatIs, Counter1 = b.Counter1, Counter2 = b.Counter2, Counter3 = b.Counter3, Counter4 = b.Counter4, SrvPacketID = b.SrvPacketID
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID) AND
    (d.SrvPacketID < b.SrvPacketID);

  /*  */
  DELETE b
  FROM
    datagps d, datagpsbuffer_on b
  WHERE
    (d.Mobitel_ID = b.Mobitel_ID) AND
    (d.LogID = b.LogID);

  /* */
  INSERT INTO datagps(Mobitel_ID
                    , LogID
                    , UnixTime
                    , Latitude
                    , Longitude
                    , Altitude
                    , Direction
                    , Speed
                    , Valid
                    , Events
                    , InMobitelID
                    , Sensor1
                    , Sensor2
                    , Sensor3
                    , Sensor4
                    , Sensor5
                    , Sensor6
                    , Sensor7
                    , Sensor8
                    , Counter1
                    , Counter2
                    , Counter3
                    , Counter4
                    , whatIs
                    , SrvPacketID)
  SELECT Mobitel_ID
       , LogID
       , UnixTime
       , Latitude
       , Longitude
       , Altitude
       , Direction
       , Speed
       , Valid
       , Events
       , LogID
       , Sensor1
       , Sensor2
       , Sensor3
       , Sensor4
       , Sensor5
       , Sensor6
       , Sensor7
       , Sensor8
       , Counter1
       , Counter2
       , Counter3
       , Counter4
       , whatIs
       , SrvPacketID
  FROM
    datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);*/

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS2 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = CREATEST(@NeedOptimize, @NeedOptimizeParam); */

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsUpdateSet;
  --DEALLOCATE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  /*  */
  IF @NeedOptimize = 1
    ALTER INDEX ALL ON datagpslost_on REORGANIZE
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'OnDeleteLostRange')
    DROP TABLE dbo.OnDeleteLostRange
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.OnDeleteLostRange
(
  @MobitelID        INTEGER,
  @BeginSrvPacketID BIGINT
)
--SQL SECURITY INVOKER
--COMMENT '�������� ���������� ��������� ��������� �� datagpslost_on.'
AS
BEGIN
  /* ?????????? ???????? ?? ??????, ??????????????? ??? ????????,
     ?????? ? ?????? ????????? ?????????. ???? ???????? - ?????
     ?????? ???????? ? ???????? ConfirmedID */

  DECLARE @MinBeginSrvPacketID BIGINT; -- ??????????? ???????? Begin_SrvPacketID ??? ??????? ?????????
  DECLARE @NewConfirmedID INT; -- ????? ???????? ConfirmedID  

  /* ?????????? ???????????? ???????? Begin_SrvPacketID */
  SELECT @MinBeginSrvPacketID = min(Begin_SrvPacketID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* ???? ???? ?????? ? ????????? ? ????????? - ????????? */
  IF @MinBeginSrvPacketID IS NOT NULL
  BEGIN
    /* ???? ??????? ?????? ?????? - ???????? ????? ???????? ConfirmedID */
    IF @MinBeginSrvPacketID = @BeginSrvPacketID
    BEGIN
      SELECT @NewConfirmedID = End_LogID
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (Begin_SrvPacketID = @BeginSrvPacketID);
    END; -- IF

    /* ???????? ?????? */
    DELETE
    FROM
      datagpslost_on
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (Begin_SrvPacketID = @BeginSrvPacketID);

    /* ???? ??????? ?????? ?????? - ??????? ConfirmedID */
    IF @MinBeginSrvPacketID = @BeginSrvPacketID
    BEGIN
      UPDATE mobitels
      SET
        ConfirmedID = @NewConfirmedID
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (@NewConfirmedID > ConfirmedID);
    END; -- IF
  END; -- IF  
END
GO

UPDATE ServiceTypes
SET
  Name = 'Online', Descr = 'Online', D1 = 'IP', D2 = 'Port', D3 = 'Login', D4 = 'Password'
WHERE
  ServiceType_ID = 3

