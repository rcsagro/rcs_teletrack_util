#region Using directives
using System;
using System.IO;
using System.Xml.Serialization;
using RCS.Proxy;
using RCS.SrvKernel;
using RCS.SrvKernel.ErrorHandling;
#endregion

namespace RCS.TDataMngrLib
{
  /// <summary>
  /// ��������� ���������� � ��������. �������� � ��
  /// </summary>
  public struct SrvConnectionSettings
  {
    public string Host;
    public int Port;
    public string User;
    public string Password;
  }

  /// <summary>
  /// ��������� ����������� � ������ �������.
  /// </summary>
  public struct ProxyConnectionSettings
  {
    public bool Enabled;
    public ProxyType Type;
    public string Host;
    public int Port;
    public string Login;
    public string Password;
    public int AnswerTimeout;
  }

  public struct Settings
  {
    /// <summary>
    /// ��������� ���������� � ��������.
    /// </summary>
    public SrvConnectionSettings SrvConnection;
    /// <summary>
    /// ��������� ���������� � ������-��������.
    /// </summary>
    public ProxyConnectionSettings ProxyConnection;
    /// <summary>
    /// ������� �������� ������ �������, � ������������
    /// </summary>
    public int SrvAnswerTimeout;

    /// <summary>
    /// �������� �� ��������� �������� �������� ������ ������� = 15 ���.
    /// </summary>
    private const int DEFAULT_SRV_ANSWER_TIMEOUT = 15 * 60 * 1000;
    /// <summary>
    /// �������� �� ��������� �������� �������� ������ ������-������� = 5 ���.
    /// </summary>
    private const int DEFAULT_PROXY_ANSWER_TIMEOUT = 5;

    /// <summary>
    /// �������� �������� ��������
    /// </summary>
    public void Validate()
    {
      SrvAnswerTimeout = SrvAnswerTimeout < 30 ?
        DEFAULT_SRV_ANSWER_TIMEOUT : SrvAnswerTimeout * 1000;

      if (ProxyConnection.AnswerTimeout < 1)
      {
        ProxyConnection.AnswerTimeout = DEFAULT_PROXY_ANSWER_TIMEOUT;
      }
    }
  }

  public static class TSettings
  {
    private static Settings settings;

    private static object syncObject = new Object();

    /// <summary>
    /// �������� ��������
    /// </summary>
    public static void LoadSettings()
    {
      if (!File.Exists(SrvSettings.FullPath))
      {
        throw new ApplicationException(String.Format(
          AppError.SETTINGS_FILE_NOT_FOUND, SrvSettings.FullPath));
      }

      lock (syncObject)
      {
        try
        {
          using (StreamReader xmlFile = new StreamReader(SrvSettings.FullPath))
          {
            settings = (Settings)new XmlSerializer(typeof(Settings)).Deserialize(xmlFile);
          }
        }
        catch (Exception ex)
        {
          throw new ApplicationException(String.Format(
            AppError.SETTINGS_DESERIALIZE, SrvSettings.FullPath, ex.Message), ex);
        }
        settings.Validate();
      }
    }

    /// <summary>
    /// ��������� �������� ����������� � �������
    /// </summary>
    /// <param name="cfg">SrvConnectionSettings</param>
    public static void SetConnectionSrvSettings(SrvConnectionSettings cfg)
    {
      lock (syncObject)
      {
        settings.SrvConnection.Host = cfg.Host;
        settings.SrvConnection.Port = cfg.Port;
        settings.SrvConnection.User = cfg.User;
        settings.SrvConnection.Password = cfg.Password;
      }
    }

    /// <summary>
    /// ���/����� �������
    /// </summary>
    /// <returns>String</returns>
    public static string ServerHost
    {
      get { lock (syncObject) return settings.SrvConnection.Host; }
    }

    /// <summary>
    /// ���� �������
    /// </summary>
    /// <returns>String</returns>
    public static int ServerPort
    {
      get { lock (syncObject) return settings.SrvConnection.Port; }
    }

    /// <summary>
    /// �����
    /// </summary>
    /// <returns>String</returns>
    public static string User
    {
      get { lock (syncObject) return settings.SrvConnection.User; }
    }

    /// <summary>
    /// ������
    /// </summary>
    /// <returns>String</returns>
    public static string Password
    {
      get { lock (syncObject) return settings.SrvConnection.Password; }
    }

    /// <summary>
    /// ������� �������� ������ �������, � ������������
    /// </summary>
    /// <returns>Uint</returns>
    public static int SrvAnswerTimeout
    {
      get { lock (syncObject) return settings.SrvAnswerTimeout; }
    }

    /// <summary>
    /// ������� �� ������������ ������-������.
    /// </summary>
    public static bool ProxyEnabled
    {
      get { lock (syncObject) return settings.ProxyConnection.Enabled; }
    }

    /// <summary>
    /// ��� ��������� ������-�������.
    /// </summary>
    public static ProxyType ProxyProtocolType
    {
      get { lock (syncObject) return settings.ProxyConnection.Type; }
    }

    /// <summary>
    /// �����, ��� ����� ������-�������.
    /// </summary>
    public static string ProxyHost
    {
      get { lock (syncObject) return settings.ProxyConnection.Host; }
    }

    /// <summary>
    /// ���� ������-�������.
    /// </summary>
    public static int ProxyPort
    {
      get { lock (syncObject) return settings.ProxyConnection.Port; }
    }

    /// <summary>
    /// ����� ������-�������.
    /// </summary>
    public static string ProxyLogin
    {
      get { lock (syncObject) return settings.ProxyConnection.Login; }
    }

    /// <summary>
    /// ������ ������-�������.
    /// </summary>
    public static string ProxyPassword
    {
      get { lock (syncObject) return settings.ProxyConnection.Password; }
    }

    /// <summary>
    /// ������� �������� ������ ������-�������. 
    /// </summary>
    public static int ProxyAnswerTimeout
    {
      get { lock (syncObject) return settings.ProxyConnection.AnswerTimeout; }
    }
  }
}
