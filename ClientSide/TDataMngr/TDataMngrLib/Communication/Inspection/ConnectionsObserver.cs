#region Using directives
using System;
using System.Threading;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvSystem.Messages;
using RCS.TDataMngrLib.ErrorHandling;
#endregion

namespace RCS.TDataMngrLib.Communication.Inspection
{
    /// <summary>
    /// ������ �� ������������ ����������� � �� � ��������.
    /// ����� ��� ���������� ���������� ���������� - ����������
    /// �����������.
    /// </summary>
    class ConnectionsObserver : IConnectionsObserver
    {
        /// <summary>
        /// ���������� � ����� ������ ��������
        /// </summary>
        private const string db_connection_available =
            "���������� � ����� ������ ��������";

        /// <summary>
        /// ���������� � �������� ��������
        /// </summary>
        private const string srv_connection_available =
            "���������� � �������� ��������";


        /// <summary>
        /// ������� ������� ����������� � �� - 30 ������
        /// </summary>
        private const int reconnectTimeOut = 30*1000;

        /// <summary>
        /// ��������� ��� �������� ���������� � ��
        /// </summary>
        private volatile IMethodsInspect iDbMethodsInspect;

        /// <summary>
        /// ������ ������������� ��������� ������ � �������
        /// </summary>
        private volatile EventWaitHandle waitEvent;

        private static object syncEventWaitHandle = new object();

        /// <summary>
        /// ���� ��������������� � ������������� ������������
        /// </summary>
        private volatile bool shouldStop;

        /// <summary>
        /// ��������� ��������� ������� ����������� � ��������
        /// True - ���������� �����������
        /// </summary>
        private volatile bool srvConnectionEstablished;

        /// <summary>
        /// ��������-������� ����������, ���������� ��� ���������
        /// </summary>
        protected IMsgDispatcher owner;

        private volatile bool active;

        /// <summary>
        /// ��������� � �������� ��������� ��� ���
        /// </summary>
        /// <returns>true - ���� ������������� ��������</returns>
        public bool Active
        {
            get { return active; }
        }

        /// <summary>
        /// Create connections observer
        /// </summary>
        /// <param name="iDbMethodsInspect">����� �������� ����������� ���������� � ��</param>
        /// <param name="owner">IMsgDispatcher</param>
        public ConnectionsObserver(IMethodsInspect iDbMethodsInspect, IMsgDispatcher owner)
        {
            if (iDbMethodsInspect == null)
            {
                Exception ex = new ArgumentNullException(String.Format(
                    AppError.ARGUMENT_NULL, "constructor", this.GetType().Name, "IMethodsInspect iDbMethodsInspect"));
                ErrorHandler.Handle(ex);
                throw ex;
            }
            if (owner == null)
            {
                Exception ex = new ArgumentNullException(String.Format(
                    AppError.ARGUMENT_NULL, "constructor", this.GetType().Name, "IMsgDispatcher owner"));
                ErrorHandler.Handle(ex);
                throw ex;
            }

            this.owner = owner;
            this.iDbMethodsInspect = iDbMethodsInspect;
        }

        /// <summary>
        /// �������� ��������� ����������
        /// </summary>
        /// <param name="messageType">��� ������������ ���������</param>
        private void PostMessage(MessageType messageType)
        {
            MessageSpec msg = new MessageSpec();
            msg.msgType = messageType;
            owner.Post(msg);
        }

        /// <summary>
        /// ������ ��������.
        /// ������� ����������� ���������� � ��, � ����� � ��������
        /// </summary>
        public void Observe()
        {
            active = true;
            shouldStop = false;
            waitEvent = new AutoResetEvent(false);

            try
            {
                while (!shouldStop)
                {
                    // �������� ���������� � ��
                    if (!iDbMethodsInspect.TryToConnect())
                    {
                        WriteErrorMessage(TDMError.CONNECT_DB_NOT_AVAILABLE);
                    }
                    else
                    {
                        if (shouldStop)
                        {
                            break;
                        }

                        WriteAvMessage(db_connection_available);

                        if (shouldStop)
                        {
                            break;
                        }

                        // �������� �� �� ��������� ���������� � ��������
                        ((ITMainController) owner).LoadSrvConnectionSettings();

                        srvConnectionEstablished = false;

                        if (shouldStop)
                        {
                            break;
                        }

                        // �������� ���������� � �������� - ���������� ��������� 
                        // ����������� � ���� �����
                        PostMessage(MessageType.CONNECT);

                        // ������� ������ �� �������� ����������� - 
                        // ����� SetSrvConnectionResult 
                        waitEvent.WaitOne();

                        if (shouldStop)
                        {
                            break;
                        }

                        if (srvConnectionEstablished)
                        {
                            if (!shouldStop)
                            {
                                PostMessage(MessageType.CONNECTIONS_AVAILABLE);
                            }
                            break;
                        }
                    }

                    // ����� ����� ��������� ��������
                    waitEvent.WaitOne(reconnectTimeOut, false);
                }
            }
            finally
            {
                lock (syncEventWaitHandle)
                {
                    if (waitEvent != null)
                    {
                        waitEvent.Close();
                        waitEvent = null;
                    }
                }
                active = false;
            }
        }

        /// <summary>
        /// ����� �� ����������� TMainController ���������� ���������� � ��������
        /// </summary>
        /// <param name="isOk">True - ���������� �����������</param>
        public void SetSrvConnectionResult(bool isOk)
        {
            if (isOk)
            {
                WriteAvMessage(srv_connection_available);
                srvConnectionEstablished = true;
            }
            else
            {
                WriteErrorMessage(TDMError.CONNECT_SRV_NOT_AVAILABLE);
                srvConnectionEstablished = false;
            }
            WaitEventSet();
        }

        /// <summary>
        /// ���������� ��������
        /// </summary>
        public void StopObserve()
        {
            shouldStop = true;
            WaitEventSet();
        }

        private void WaitEventSet()
        {
            lock (syncEventWaitHandle)
            {
                if (waitEvent != null)
                {
                    waitEvent.Set();
                }
            }
        }

        /// <summary>           
        /// ������ � ��� ��������� � ����������� ����������
        /// </summary>
        /// <param name="message">����� ���������</param>
        protected virtual void WriteAvMessage(string message)
        {
            if (!shouldStop)
            {
                Logger.WriteDebug(message);
            }
        }

        /// <summary>
        /// ������ � ��� ��������� � ������������� ����������
        /// </summary>
        /// <param name="message">����� ���������</param>
        protected virtual void WriteErrorMessage(string message)
        {
            if (!shouldStop)
            {
                ErrorHandler.Handle(new ApplicationException(message),
                    ErrorLevel.WARNING);
            }
        }
    }
}
