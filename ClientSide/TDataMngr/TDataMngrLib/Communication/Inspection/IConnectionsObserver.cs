﻿
namespace RCS.TDataMngrLib.Communication.Inspection
{
  /// <summary>
  /// Интерфейс инспектора соединений. Событие
  /// EventConnectionsAvailable появляется только
  /// при доступности двух соединений: к БД и Серверу
  /// </summary>
  interface IConnectionsObserver
  {
    /// <summary>
    /// Начать слежение.
    /// Вначале проверяется соединение с БД, а затем с Сервером.
    /// </summary>
    void Observe();
    /// <summary>
    /// Остановить слежение
    /// </summary>
    void StopObserve();
    /// <summary>
    /// В активном состоянии или нет
    /// </summary>
    /// <returns>true - идет периодическая проверка</returns>
    bool Active { get; }
    /// <summary>
    /// Передача результата соединения с сервером
    /// </summary>
    /// <param name="isOk">True - соединение установлено</param>
    void SetSrvConnectionResult(bool isOk);
  }
}
