using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using RCS.Protocol.Online;
using RCS.Proxy;
using RCS.Proxy.Client;
using RCS.Sockets;
using RCS.Sockets.Clnt;
using RCS.Sockets.Connection;
using RCS.SrvKernel.Logging;
using RemoteInteractionLib;
using TransitServisLib;

namespace RCS.TDataMngrLib.Communication
{
    public class HandlingOfIncomDataSrv
    {
        /// <summary>
        /// �������: ������� ����� �� �������.
        /// </summary>
        public event IncomPacketFromSrv packFromSrv;

        public event ConnectedStateEventHandler �onnectedState;

        /// <summary>
        /// �������: ������ �� ������������ ������
        /// </summary>
        public event MessageErrorEventHandler errorOfLevel;

        private const int MAX_PACKET_NAMBER = int.MaxValue - 1000;

        /// <summary>
        /// ����� ���������� ���������
        /// </summary>
        protected int Numb_packet_out
        {
            get
            {
                if (numb_packet > MAX_PACKET_NAMBER)
                {
                    numb_packet = 0;
                }
                return Interlocked.Increment(ref numb_packet);
            }
        }

        private static int numb_packet;

        private ClientSocket socket;

        private List<ConnectionInfo> taskList = new List<ConnectionInfo>();

        public HandlingOfIncomDataSrv()
        {
            socket = new ClientSocket();
            socket.IncomMessage += new IncomingMessageEventHandler(EnqueueToParser);
            socket.�onnectedStateMsg += new ConnectedStateEventHandler(ConnStateMsg);
            socket.messageError += new MessageErrorEventHandler(ErrorFromTrLevel);
        }

        /// <summary>
        /// ������ � ������� ����������� ������ �� ����������� ���������
        /// </summary>
        /// <param name="id_teletrack">����� ���������</param>
        /// <param name="startNum">��������� ����� ������ � �� �������</param>
        /// <param name="endNum">���. ����� ������ � �� �������</param>
        /// <returns>����� ������������� ������</returns>
        public int SendQuery(string id_teletrack, long startNum, long endNum)
        {
            int num_pack = Numb_packet_out;
            byte[] data = PacketSendQuery.AssemblePacket(id_teletrack, startNum, endNum, num_pack);

            socket.Send(socket.Cl_Connection, data);
            return num_pack;
        }

        /// <summary>
        /// ���������� �� ������ ������� ��� ����������� ���������
        /// </summary>
        /// <param name="loginTT">����� ���������</param>
        /// <param name="id_cmdInClDB">����� ������� � �� �������</param>
        /// <param name="cmd">�������</param>
        /// <returns>����� ������������� ������</returns>
        public int SendCommand(string loginTT, int id_cmdInClDB, byte[] cmd)
        {
            int num_pack = Numb_packet_out;
            byte[] data = new PacketSendCommand().AssemblePacket(
                num_pack, loginTT, id_cmdInClDB, cmd);

            socket.Send(socket.Cl_Connection, data);
            return num_pack;
        }

        /// <summary>
        /// ���������� �� ������ ����� AckExtendCl
        /// </summary>
        /// <param name="answCod">��� ������</param>
        /// <param name="numIncomPack">����� ������, ����������� �� �������</param>
        /// <returns>����� ������������� ������</returns>
        public int SendAckExtendCl(int answCod, int numIncomPack)
        {
            int num_pack = Numb_packet_out;
            byte[] data = new PacketAckExCl().AssemblePacket(
                answCod, numIncomPack, num_pack);

            socket.Send(socket.Cl_Connection, data);
            return num_pack;
        }

        /// <summary>
        /// ������������� ���������� � �������� ����� Proxy.
        /// </summary>
        /// <exception cref="ApplicationException">���������� � �������� ��� ����������</exception>
        /// <param name="proxyCfg">��������� ����������� � Proxy-�������.</param>
        /// <param name="proxyType">��� ������ �������.</param>
        /// <param name="address">����� �������.</param>
        /// <param name="port">���� �������.</param>
        /// <returns>true - ���������� �����������.</returns>
        public bool ConnectToServerViaProxy(
            ProxyCfg proxyCfg, ProxyType proxyType, string address, int port)
        {
            return socket.SetupClientConnectionViaProxy(
                proxyCfg, proxyType, address, port) != null
                ? true
                : false;
        }

        string IP = "";

        /// <summary>
        /// ������������� ���������� � �������� (������������ ��� �������).
        /// </summary>
        /// <exception cref="ApplicationException">���������� � �������� ��� ����������</exception>
        /// <param name="address">����� �������.</param>
        /// <param name="port">���� �������.</param>
        /// <returns>true - ���������� �����������.</returns>
        public bool ConnectToServer(string ip_address, int port)
        {
            var result = socket.SetupClientConnect(ip_address, port) != null ? true : false;

            IP = ip_address;

            if(result)
            {
                Logger.Write("ConnectToServer: Geting connection with server " + IP + " success!");
            }
            else
            {
                Logger.Write("ConnectToServer: Can not geting connection with server " + IP);
            }

            return result;
        }

        /// <summary>
        /// ��������� ���������� ������� � ��������
        /// </summary>
        public void CloseConnection()
        {
            //socket.CloseSocket(socket.Cl_Connection);
            Logger.Write("CloseConnection: Connection with server " + IP + " clousing!");
        }

        /// <summary>
        /// ���������� �� ������ ����������������� ������
        /// </summary>
        /// <param name="login">�����</param>
        /// <param name="password">������</param>
        /// <returns>����� ������������� ������</returns>
        public int Authentication(string login, string password)
        {
            socket.Send(socket.Cl_Connection, new PacketAuth(login, password).AssemblePacket());
            Logger.Write("Authentication. Send login: " + login + " and pssword: " + password + " to online server " + IP);
            return 0;
        }

        /// <summary>
        /// �������� �� ������� ���� ��������� ����������
        /// </summary>
        /// <param name="con_state"></param>
        private void ConnStateMsg(ConnectedStateEvArg con_state)
        {
            if (�onnectedState != null)
            {
                �onnectedState(con_state);
            }
        }

        private void ErrorFromTrLevel(object sender, MessageErrorEventArgs e)
        {
            if (errorOfLevel != null)
            {
                errorOfLevel(sender, e);
            }
        }

        private void OnPackFromSrv(IPacket pk)
        {
            if (packFromSrv != null)
            {
                packFromSrv(pk);
            }
        }

        /// ���������� � ������� �� ���������
        /// (�������������� ��� �������)
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="connection"></param>
        private void EnqueueToParser(object obj, ConnectionInfo connection)
        {
            IncomToParser(connection);
        }

        /// <summary>
        /// ���������� �����, ���������� �� ������
        /// </summary>
        /// <param name="conn">����� ����������</param>
        private void IncomToParser(ConnectionInfo conn)
        {
            byte[] task;
            do
            {
                int header_pos = 0;
                task = conn.DeQueueIncomPack();

                if (task != null)
                {
                    if (conn.incompl_pack.Len > 0)
                    {
                        byte[] bytes = conn.incompl_pack.AddHeadOfPacket(task);
                        ExtractPacket(conn, bytes, header_pos);
                    }
                    else
                    {
                        ExtractPacket(conn, task, header_pos);
                    }
                }
                else
                {
                    taskList.Remove(conn);
                }
            } while (task != null);
        }

        /// <summary>
        /// ������ ���������
        /// </summary>
        /// <param name="conn">�� ���� �������� ������</param>
        /// <param name="data">������</param>
        /// <param name="pos">������� � ���. ����� ������ ������ ������</param>
        private void ExtractPacket(ConnectionInfo conn, byte[] data, int pos)
        {
            int header_pos = pos;
            try
            {
                IPacket iptr = Packet.PacketIdentifyNew(data, ref header_pos);
                if (iptr != null)
                {
                    HandlingOfPack(iptr, conn, data, header_pos);
                }
                //else ������ � ���� ������
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void HandlingOfPack(IPacket pk, ConnectionInfo conn, byte[] bytes, int header_pos)
        {
            int pack_size = pk.DisassemblePacket(bytes, header_pos);

            if (pack_size != (-1))
            {
                int cur_pos = pack_size + header_pos;
                if (cur_pos <= bytes.Length) //����� ����
                {
                    pk.PacketSize = pack_size; //��� ����������
                    PutStatDataToReposit(conn, pk);

                    Logger.Write("HandlingofPack. Getting bytes " + bytes.Length + " from server: " + IP);

                    if ((cur_pos + 8) < bytes.Length)
                    {
                        ExtractPacket(conn, bytes, cur_pos);
                    }
                    else if (cur_pos != bytes.Length)
                    {
                        //������� ������ 8 ���� -> ���������� ���������� ������ ������
                        conn.incompl_pack.PutBufIncompletePack(bytes, header_pos);
                    }
                }
                else
                {
                    conn.incompl_pack.PutBufIncompletePack(bytes, header_pos);
                }
            }
            else
            {
                throw (new Exception("������ ��������� � DisassemblePacket"));
            }
        }

        /// <summary>
        /// ���������� �������������� ������ � �����������
        /// � �������� ���������� ����� �� ��������� �������.
        /// </summary>
        /// <param name="sender">�����������.</param>
        /// <param name="data">����� ������.</param>
        private void PutStatDataToReposit(ConnectionInfo sender, IPacket packet)
        {
            switch (packet.GetType().Name)
            {
                case "PacketAckExSr":
                    RepositOfStat.AddSizeOfIncomTraffic((packet as PacketAckExSr).PacketSize);
                    break;
                case "PacketInfo":
                    HandlePacketInfo(packet);
                    break;
                case "PacketResult":
                    HandlePacketResult(packet);
                    break;
                case "PacketAnswCommand":
                    //����� AnswCommand -  ����� �� ������� �1
                    RepositOfStat.AddLettInfo("", "A1", 160, 1);
                    RepositOfStat.AddSizeOfIncomTraffic(new PacketAnswCommand().PacketSize);
                    break;
            }
            OnPackFromSrv(packet);
        }

        /// <summary>
        /// ��������� ������ PacketInfo - ������ ������.
        /// </summary>
        /// <param name="packet">����� ������.</param>
        private void HandlePacketInfo(IPacket packet)
        {
            PacketInfo pk_inf = packet as PacketInfo;

            if (pk_inf.lstNewBin != null)
            {
                Dictionary<string, int> dict = new Dictionary<string, int>();
                foreach (NewPacketFromTT pk in pk_inf.lstNewBin)
                {
                    if (dict.ContainsKey(pk.LoginTT))
                    {
                        dict[pk.LoginTT]++;
                    }
                    else
                    {
                        dict.Add(pk.LoginTT, 1);
                    }
                }
                foreach (string key in dict.Keys)
                {
                    RepositOfStat.AddLettInfo(key, "������", dict[key]*32, dict[key]);
                }
            }
            else
            {
                RepositOfStat.AddLettInfo(pk_inf.LoginTT, "������", 32, 1);
            }
            RepositOfStat.AddSizeOfIncomTraffic(pk_inf.PacketSize);
        }

        /// <summary>
        /// ��������� ������ PacketResult - ��������� ���������.
        /// </summary>
        /// <param name="packet">����� ������.</param>
        private static void HandlePacketResult(IPacket packet)
        {
            PacketResult pk_result = packet as PacketResult;

            if (pk_result.res_query.Count > 0)
            {
                RepositOfStat.AddLettInfo(
                    Encoding.ASCII.GetString(BitConverter.GetBytes(pk_result.MessageID)),
                    "��������", pk_result.res_query.Count*32, pk_result.res_query.Count);
            }
            RepositOfStat.AddSizeOfIncomTraffic(pk_result.PacketSize);
        }
    }
}
