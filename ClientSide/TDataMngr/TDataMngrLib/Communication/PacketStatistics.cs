using System;
using RCS.SrvKernel.Communication;
using RCS.SrvKernel.Logging;


namespace RCS.TDataMngrLib.Communication
{
  class PacketStatistics : BasePacketStatistics
  {
    /// <summary>
    /// ������ � ��� ����������
    /// </summary>
    protected override void WriteStatInfo()
    {
      const string stat = "\r\n ����������. \r\n" +
        " �������� ������������ ������� {0}, ���������/����� - {1}. \r\n" +
        " ���������� ������� {2}.\r\n";

      Logger.Write(String.Format(stat,
        Convert.ToString(ReceivePacketCount),
        Convert.ToString(ReceiveErrorPacketCount),
        Convert.ToString(SendPacketCount)));
    }
  }
}
