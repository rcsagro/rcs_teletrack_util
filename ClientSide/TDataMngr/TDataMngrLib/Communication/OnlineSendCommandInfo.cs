namespace RCS.TDataMngrLib.Communication
{
  /// <summary>
  /// ���������� � ��������� ����������� ������.
  /// </summary>
  struct OnlineSendCommandInfo
  {
    /// <summary>
    /// ID ������.
    /// </summary>
    public int PacketID;
    /// <summary>
    /// ��� ������.
    /// </summary>
    public PKType PacketType;
    /// <summary>
    /// ���������.
    /// </summary>
    public object[] Parameters;
  }
}
