#region Using directives
using System;
using System.Net.Sockets;
using RCS.Protocol.Online;
using RCS.Proxy.Client;
using RCS.Sockets;
using RCS.Sockets.Connection;
using RCS.SrvKernel;
using RCS.SrvKernel.Communication;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand.Sending;
using RCS.SrvKernel.SrvSystem.Messages;
using RCS.SrvKernel.Util;
using RCS.TDataMngrLib;
using RCS.TDataMngrLib.ErrorHandling;
using RemoteInteractionLib;
using TransitServisLib;
#endregion

namespace RCS.TDataMngrLib.Communication
{
    /// <summary>
    /// ���������� � ��������� ����������� ������
    /// </summary>
    struct OnlineSendCommandInfo
    {
        /// <summary>
        /// ID ������
        /// </summary>
        public int PacketID;

        /// <summary>
        /// ��� ������
        /// </summary>
        public PKType PacketType;

        /// <summary>
        /// ���������
        /// </summary>
        public object[] Parameters;
    }


    /// <summary>
    /// ������� ��� �������, �������������� ������� ����������.
    /// <para>����� �������� ������� ����������� ���� �������� � ���� �������
    /// �, �������������, � GPRSTransportLevel � ���������� �������, ��������:
    /// ������� � ������ �1, �������� ����� Connect (���������� � ��������)
    /// � ������ �2. ����� ����� ������ ����� �� ��������� �� �������
    /// GPRSTransportLevel - ��� ������� � ��������� ����� �������</para>
    /// <para>������� ������ � ��������, � ����������, � ���������� ������������
    /// ������� ���������� � ����� ������</para>
    /// </summary>
    class Communicator : BaseCommunicator, ITCommunicator
    {
        /// <summary>
        /// �������� ������������� ������.
        /// </summary>
        private volatile HandlingOfIncomDataSrv transportLevelMngr;

        /// <summary>
        /// ��������� �������������� � ������� �����������.
        /// </summary>
        private ITMainController iTMainController;

        private volatile bool connected;

        private object syncLastSentCommandObject = new object();

        /// <summary>
        /// ��������� ������������ �������.
        /// </summary>
        public OnlineSendCommandInfo LastSentCommand
        {
            get { lock (syncLastSentCommandObject) return lastSentCommand; }
            set { lock (syncLastSentCommandObject) lastSentCommand = value; }
        }

        private OnlineSendCommandInfo lastSentCommand;

        private IncomPacketFromSrv incomPacketFromSrvEvent;
        private ConnectedStateEventHandler connectStateEvent;
        private MessageErrorEventHandler transportErrorEvent;

        /// <summary>
        /// ������� ������������� ����������. �������������
        /// ������� ���������� TMainController (��. �����������).
        /// </summary>
        internal event SimpleEventDelegate eventSrvNotAvailable;

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="mCtrlCommandHandler">�����-���������� �������.</param>
        /// <param name="tMainController">��������� �������������� � ������� �����������.</param>
        public Communicator(HandleCommandDelegate mCtrlCommandHandler,
            ITMainController tMainController)
            : base(mCtrlCommandHandler)
        {
            iTMainController = tMainController;
        }

        /// <summary>
        /// �������� �������� ����������.
        /// </summary>
        /// <returns>Packet statistics.</returns>
        protected override BasePacketStatistics CreatePacketStatistics()
        {
            return new PacketStatistics();
        }

        /// <summary>                                               
        /// ��������� ������� ��������� � �������.
        /// </summary>
        /// <param name="pk">IPacket.</param>
        private void OnIncomPacketFromSrv(IPacket pk)
        {
            statistics.IncrementReceivePacketCount();

            WriteInfoMessageAboutPacket(pk);

            HandleCmdParamObject parameters = new HandleCmdParamObject();
            parameters.Packet = pk;
            parameters.ConnectionCtx = null;

            MessageSpec message = new MessageSpec();
            message.msgType = MessageType.PACKET_RECEIVED;
            message.Params = parameters;
            ((IMsgDispatcher) iTMainController).Post(message);
        }

        /// <summary>
        /// ������� ����� �� {0}, ����� ������ {1}, ������������� ������� {2}
        /// </summary>
        protected const string RECEIVE_DATA_INFO =
            "������� ����� �� {0}, ����� ������ {1}, ������������� ������� {2}";

        /// <summary>
        /// ���������� ���������� � �������� ������.
        /// </summary>
        /// <param name="pk">IPacket.</param>
        private void WriteInfoMessageAboutPacket(IPacket pk)
        {
            int lenght = 0;
            string typeName = pk.GetType().Name;
            if (typeName == (typeof (PacketInfo)).Name)
            {
                lenght = ((PacketInfo) pk).lstNewBin != null ? ((PacketInfo) pk).lstNewBin.Count : 1;
            }
            else if (typeName == (typeof (PacketResult)).Name)
            {
                lenght = ((PacketResult) pk).res_query.Count;
            }

            WriteInfo("Geting packet from online server", String.Format(RECEIVE_DEBUG_INFO,
                TSettings.ServerHost, Convert.ToString(lenght), typeName));
        }

        /// <summary>
        /// �������� ������� ������������� ������.
        /// </summary>
        protected override void CreateTransportLevelObject()
        {
            transportLevelMngr = new HandlingOfIncomDataSrv();
        }

        /// <summary>
        /// �������� �� �������.
        /// </summary>
        protected override void InnerStart()
        {
            lastSentCommand.PacketID = -1;
            lastSentCommand.Parameters = null;

            transportErrorEvent = new MessageErrorEventHandler(HandleTransportError);
            connectStateEvent = new ConnectedStateEventHandler(HandleConnectStateChanged);
            // ������������� �� ������ ����� ������
            incomPacketFromSrvEvent = new IncomPacketFromSrv(OnIncomPacketFromSrv);
            transportLevelMngr.packFromSrv += incomPacketFromSrvEvent;
        }

        string IP = ""; 

        /// <summary>
        /// ��������� IP ������.
        /// </summary>
        /// <returns>String</returns>
        private string GetIpAddress()
        {
            string ipAddress = "";
            if (TcpIP.Resolve(TSettings.ServerHost, false, out ipAddress))
            {
                Logger.Write(String.Format("��������� ������� ��������. IP ����� �������: {0} .", ipAddress));
                IP = ipAddress;
            }
            else
            {
                ErrorHandler.Handle(new ApplicationException(String.Format(
                    TDMError.SERVER_RESOLVING, TSettings.ServerHost)));
            }

            return ipAddress;
        }

        /// <summary>
        /// ��������� ����������� ����������.
        /// </summary>
        /// <returns>������ ���� ���������� �����������.</returns>
        public bool Connect()
        {
            connected = false;
            try
            {
                Start();

                connected = TSettings.ProxyEnabled ? ConnectViaProxy() : ConnectDirectly();

                if (connected)
                {
                    transportLevelMngr.errorOfLevel += transportErrorEvent;
                    transportLevelMngr.�onnectedState += connectStateEvent;

                    RepositOfStat.ConnectionCount++;
                }
            }
            catch (Proxy.Exceptions.ProxyClientException ex)
            {
                ErrorHandler.Handle(ex, ErrorLevel.WARNING);
            }
            catch (SocketException ex)
            {
                ErrorHandler.Handle(ex, ErrorLevel.WARNING);
            }
            catch (Exception ex)
            {
                ErrorHandler.Handle(ex);
            }
            return connected;
        }

        private bool ConnectDirectly()
        {
            string ip = GetIpAddress();

            WriteInfo("+++", String.Format("����������� � ������� {0}:{1}.", ip, TSettings.ServerPort));

            return transportLevelMngr.ConnectToServer(ip, TSettings.ServerPort);
        }

        private bool ConnectViaProxy()
        {
            string ip = GetIpAddress();

            WriteInfo("+++", String.Format(
                "����������� � ������� {0}:{1} ����� {2} Proxy {3}:{4}.",
                ip, TSettings.ServerPort, TSettings.ProxyProtocolType,
                TSettings.ProxyHost, TSettings.ProxyPort));

            return transportLevelMngr.ConnectToServerViaProxy(
                new ProxyCfg(TSettings.ProxyHost, TSettings.ProxyPort,
                    TSettings.ProxyLogin, TSettings.ProxyPassword,
                    TSettings.ProxyAnswerTimeout),
                TSettings.ProxyProtocolType, ip, TSettings.ServerPort);
        }

        protected override void InnerStop()
        {
            if ((transportLevelMngr != null) && (connected))
            {
                transportLevelMngr.errorOfLevel -= transportErrorEvent;
                transportLevelMngr.�onnectedState -= connectStateEvent;
                transportLevelMngr.packFromSrv -= incomPacketFromSrvEvent;
                transportErrorEvent = null;
                connectStateEvent = null;
                incomPacketFromSrvEvent = null;

                try
                {
                    //transportLevelMngr.CloseConnection();
                    Logger.Write("InnerStop: Connection with server clousing!");
                }
                catch (Exception Ex)
                {
                    ErrorHandler.Handle(Ex);
                }
            }
            transportLevelMngr = null;
        }

        #region �������� �������

        /// <summary>
        /// �������� ������ ��������������
        /// </summary>
        /// <param name="login">�����</param>
        /// <param name="password">������</param>
        public void SendAuthentication(string login, string password)
        {
            try
            {
                transportLevelMngr.Authentication(login, password);

                RegisterSendPacket(-1, PKType.PACK_AUTH,
                    new object[2] {login, password});

                WriteInfo(SENDING_SYMBOL, String.Format(
                    "��������� ����� �������������� ������� {0}.", TSettings.ServerHost));
            }
            catch (Exception Ex)
            {
                ErrorHandler.Handle(Ex);
            }
        }

        /// <summary>
        /// �������� ������ AckExtendCl
        /// </summary>
        /// <param name="answCode">��� ������</param>
        /// <param name="srvProtocolPacketID">����� ������ ������������� ���������, ����������� �� �������</param>
        public void SendAckExtendCl(int answCode, int srvProtocolPacketID)
        {
            try
            {
                int lastPacketID = transportLevelMngr.SendAckExtendCl(
                    answCode, srvProtocolPacketID);

                RegisterSendPacket(lastPacketID, PKType.ACK_EX_CL,
                    new object[2] {answCode, srvProtocolPacketID});

                WriteInfo(SENDING_SYMBOL, String.Format(
                    "��������� ����� ACK_EX_CL ������� {0} � ����� ������ {1} � ����� �� ����� {2}.",
                    TSettings.ServerHost, answCode, srvProtocolPacketID));
            }
            catch (Exception Ex)
            {
                ErrorHandler.Handle(Ex);
            }
        }

        /// <summary>
        /// �������� ������� ��� ����������� ���������
        /// </summary>
        /// <param name="teletrackID">�������� ������������� ���������</param>
        /// <param name="commandID">����� ������� � �� �������</param>
        /// <param name="command">������� byte[]</param>
        public void SendCommand(string teletrackID, int commandID, byte[] command)
        {
            try
            {
                int lastPacketID = transportLevelMngr.SendCommand(
                    teletrackID, commandID, command);

                RegisterSendPacket(lastPacketID, PKType.PACK_SN_COM,
                    new object[3] {teletrackID, commandID, command});

                WriteInfo(SENDING_SYMBOL, String.Format(
                    "���������� ������� ����� ������ {0} ��� ��������� {1} (ID � ��  {2}).",
                    TSettings.ServerHost, teletrackID, commandID));
            }
            catch (Exception Ex)
            {
                ErrorHandler.Handle(Ex);
            }
        }

        /// <summary>
        /// �������� ������� � ������� �� ����������� ������ ����������� ���������
        /// </summary>
        /// <param name="teletrackID">�������� ������������� ���������</param>
        /// <param name="beginPacketID">��������� ����� ������ � �� �������</param>
        /// <param name="endPacketID">���. ����� ������ � �� �������</param>
        public void SendQuery(string teletrackID, long beginPacketID, long endPacketID)
        {
            try
            {
                int lastPacketID = transportLevelMngr.SendQuery(
                    teletrackID, beginPacketID, endPacketID);

                RegisterSendPacket(lastPacketID, PKType.PACK_SN_QU,
                    new object[3] {teletrackID, beginPacketID, endPacketID});

                WriteInfo(SENDING_SYMBOL, String.Format(
                    "��������� ������ ������� {0} �� ��������� ��������� {1} � ID ������ {2} �� {3}.",
                    TSettings.ServerHost, teletrackID, beginPacketID, endPacketID));
            }
            catch (Exception Ex)
            {
                ErrorHandler.Handle(Ex);
            }
        }

        /// <summary>
        /// ����������� ���������� ������������� ������
        /// </summary>
        /// <param name="packetID">ID ������</param>
        /// <param name="packetType">��� ������</param>
        /// <param name="parameters">���������</param>
        private void RegisterSendPacket(int packetID, PKType packetType, params object[] parameters)
        {
            lastSentCommand.PacketID = packetID;
            lastSentCommand.PacketType = packetType;
            lastSentCommand.Parameters = parameters;
            statistics.IncrementSendPacketCount();
        }

        /// <summary>
        /// ��������� ���������
        /// </summary>
        /// <param name="response">������-����� � ����������� ��� ��������</param>
        protected override void InnerSend(SendingInfo response)
        {
            throw new ApplicationException(TDMError.SEND_METHOD_DEPRECATED);
        }

        #endregion

        /// <summary>
        /// ���������� ������� ��������� ��������� ����������� 
        /// </summary>
        protected void HandleConnectStateChanged(ConnectedStateEvArg con_state)
        {
            WriteInfo("<=> ", String.Format(
                "������������ ��������� �����������: {0}", con_state.Message));

            if (!con_state.conn.socket.Connected)
            {
                connected = false;
                SendEventSrvNotAvailable();
            }
        }

        /// <summary>
        /// ��������� ������ ������������� ������ 
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="ex">MessageErrorEventArgs</param>
        protected void HandleTransportError(object sender, MessageErrorEventArgs ex)
        {
            if (ex.ErrorCode != (int) SocketError.ConnectionReset)
            {
                ErrorHandler.Handle(new ApplicationException(String.Format(
                    AppError.TRANSPORT_INTERNAL, ex.Message, ex.IPaddress)));
            }
            if (ex.Message.Contains("����� �� ���������"))
            {
                statistics.IncrementReceiveErrorPacketCount();
            }
            if (connected)
            {
                connected = false;
                SendEventSrvNotAvailable();
            }
        }

        /// <summary>
        /// Send event server not available
        /// </summary>
        private void SendEventSrvNotAvailable()
        {
            if (eventSrvNotAvailable != null)
            {
                eventSrvNotAvailable();
            }
        }
    }
}
