// Project: TDataMngrLib, File: ITCommunicator.cs
// Namespace: RCS.TDataMngrLib.Communication, Class: 
// Path: D:\Development\TDataManager_Head\TDataMngrLib\Communication, Author: guschin
// Code lines: 46, Size of file: 1.69 KB
// Creation date: 6/18/2008 3:20 PM
// Last modified: 8/8/2008 4:44 PM
// Generated with Commenter by abi.exDream.com


namespace RCS.TDataMngrLib.Communication
{
  /// <summary>
  /// ��������� ������������� �������� Communicator'�
  /// ������� ����������� �������
  /// </summary>
  interface ITCommunicator
  {
    /// <summary>
    /// ��������� ������������ �������
    /// </summary>
    OnlineSendCommandInfo LastSentCommand { get; }
    /// <summary>
    /// �������� ������ ��������������
    /// </summary>
    /// <param name="login">�����</param>
    /// <param name="password">������</param>
    void SendAuthentication(string login, string password);
    /// <summary>
    /// �������� ������ AckExtendCl
    /// </summary>
    /// <param name="answCode">��� ������</param>
    /// <param name="srvProtocolPacketID">����� ������ ������������� ���������, ����������� �� �������</param>
    void SendAckExtendCl(int answCode, int srvProtocolPacketID);
    /// <summary>
    /// �������� ������� ��� ����������� ���������
    /// </summary>
    /// <param name="teletrackID">�������� ������������� ���������</param>
    /// <param name="commandID">����� ������� � �� �������</param>
    /// <param name="command">������� byte[]</param>
    void SendCommand(string teletrackID, int commandID, byte[] command);
    /// <summary>
    /// �������� ������� � ������� �� ����������� ������ ����������� ���������
    /// </summary>
    /// <param name="teletrackID">�������� ������������� ���������</param>
    /// <param name="beginPacketID">��������� ����� ������ � �� �������</param>
    /// <param name="endPacketID">���. ����� ������ � �� �������</param>
    void SendQuery(string teletrackID, long beginPacketID, long endPacketID);
  }
}
