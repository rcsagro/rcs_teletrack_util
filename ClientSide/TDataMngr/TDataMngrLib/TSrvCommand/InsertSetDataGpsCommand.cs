using System;
using System.Collections.Generic;
using RCS.Protocol.Online;
using RCS.Sockets.Connection;
using RCS.SrvKernel.ErrorHandling;
using RCS.TDataMngrLib.DataManagement;
using RCS.TDataMngrLib.ErrorHandling;
using TransitServisLib;

namespace RCS.TDataMngrLib.TSrvCommand
{
	/// <summary>
	/// ��������� ������ ������� � ������ ������ ������
	/// </summary>
	class InsertSetDataGpsCommand : TCommand
	{
		private List<NewPacketFromTT> listPacket;

		public InsertSetDataGpsCommand(DataManager dataMngr,
			ConnectionInfo connectionContext, List<NewPacketFromTT> packets)
			: base(dataMngr, connectionContext)
		{
			if (packets == null) {
				throw new ArgumentNullException(" List<NewPacketFromTT> packets",
					String.Format(AppError.CMD_NULL_PARAM_CTOR,
						"InsertSetDataGpsCommand"));
			}
			listPacket = packets;
		}

		protected override void InternalExecute()
		{
			foreach (NewPacketFromTT row in listPacket) {
				int mobitelID;
				string devIdShort = Convert.ToString(row.LoginTT);

				// �������� ��������������� �� �������� � ������� MobitelID
				if ((dataManager.TeletrackExist(devIdShort)) &&
				        (dataManager.TryGetMobitelID(devIdShort, out mobitelID))) {
					string parsRow;
					if (GetParsedRow(mobitelID, row.Packet, row.LoginTT,
						         new string[] { Convert.ToString(row.ID_packet) }, out parsRow)) {
						dataManager.InsertDataGPS(new string[1] { parsRow });
					}
				} else {
					ErrorHandler.Handle(new ApplicationException(
						String.Format(TDMError.PACKET_FROM_UNKNOWN_TELETRACK, devIdShort)),
						ErrorLevel.WARNING);
				}
			}
			dataManager.ManualFlushDataGpsBuffer();
		}

		/// <summary>
		/// ��� ������� 
		/// </summary>
		protected override int GetCommandID()
		{
			return (int)PKType.PACK_INFO;
		}

		/// <summary>
		/// ���������� ��������� �������
		/// </summary>
		protected override string GetCommandDescr()
		{
			return Enum.GetName(typeof(PKType), PKType.PACK_INFO);
		}

		protected override bool GetNeedResponse()
		{
			return false;
		}

		public override bool IsInsertFunc()
		{
			return true;
		}
	}
}
