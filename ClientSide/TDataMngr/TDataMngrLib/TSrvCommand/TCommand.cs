using RCS.Sockets.Connection;
using RCS.SrvKernel.DataManagement;
using RCS.SrvKernel.SrvCommand;
using RCS.TDataMngrLib.DataManagement;

namespace RCS.TDataMngrLib.TSrvCommand
{
    class TCommand : SrvCommand
    {
        protected DataManager dataManager;

        /// <summary>
        /// Create command
        /// </summary>
        internal TCommand(BaseDataManager dataMngr, ConnectionInfo connectionContext)
            : base(dataMngr, connectionContext)
        {
        }

        /// <summary>
        /// ������������ ��������� ��������� ������
        /// </summary>
        /// <param name="dataMngr">BaseDataManager</param>
        protected override void SetDataManager(BaseDataManager dataMngr)
        {
            dataManager = (DataManager) dataMngr;
            base.SetDataManager(dataMngr);
        }
    }
}
