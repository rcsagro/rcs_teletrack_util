using System;
using System.Collections.Generic;
using RCS.Protocol.Online;
using RCS.Sockets.Connection;
using RCS.SrvKernel.ErrorHandling;
using RCS.TDataMngrLib.DataManagement;

namespace RCS.TDataMngrLib.TSrvCommand
{
	/// <summary>
	/// ��������� ������ ���������� �� �������
	/// </summary>
	class InsertQueryDataGpsCommand : TCommand
	{
		private Dictionary<long, byte[]> gpsData;
		private int mobitelID;
		private string devIdShort;

		public InsertQueryDataGpsCommand(DataManager dataMngr, ConnectionInfo connectionContext,
			Dictionary<long, byte[]> gpsData, int mobitelID, string devIdShort)
			: base(dataMngr, connectionContext)
		{
			if (gpsData == null)
				throw new ArgumentNullException("Dictionary<int, byte[]> gpsData",
					String.Format(AppError.CMD_NULL_PARAM_CTOR,
						"InsertQueryDataGpsCommand"));

			this.gpsData = gpsData;
			this.mobitelID = mobitelID;
			this.devIdShort = devIdShort;
		}

		protected override void InternalExecute()
		{
			List<string> newData = new List<string>();

			foreach (KeyValuePair<long, byte[]> row in gpsData) {
				string parsRow;
				if (GetParsedRow(mobitelID, row.Value, devIdShort,
					        new string[] { Convert.ToString(row.Key) }, out parsRow)) {
					newData.Add(parsRow);
				}
			}

			if (newData.Count > 0) {
				dataManager.InsertDataGPS(newData.ToArray());
			}
		}

		/// <summary>
		/// ��� ������� 
		/// </summary>
		protected override int GetCommandID()
		{
			return (int)PKType.PACK_RES_QU;
		}

		/// <summary>
		/// ���������� ��������� �������
		/// </summary>
		protected override string GetCommandDescr()
		{
			return Enum.GetName(typeof(PKType), PKType.PACK_RES_QU);
		}

		protected override bool GetNeedResponse()
		{
			return false;
		}

		public override bool IsInsertFunc()
		{
			return true;
		}
	}
}
