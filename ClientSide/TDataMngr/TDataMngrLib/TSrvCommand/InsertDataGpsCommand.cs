using System;
using RCS.Protocol.Online;
using RCS.Sockets.Connection;
using RCS.SrvKernel.ErrorHandling;
using RCS.TDataMngrLib.DataManagement;
using RCS.TDataMngrLib.ErrorHandling;

namespace RCS.TDataMngrLib.TSrvCommand
{
	/// <summary>
	/// ��������� ��������� ����� � ������ ������ ������
	/// </summary>
	class InsertDataGpsCommand : TCommand
	{
		private PacketInfo packet;

		public InsertDataGpsCommand(DataManager dataMngr,
			ConnectionInfo connectionContext, PacketInfo packet)
			: base(dataMngr, connectionContext)
		{
			if (packet == null) {
				throw new ArgumentNullException("PacketInfo packet",
					String.Format(AppError.CMD_NULL_PARAM_CTOR,
						"InsertDataGpsCommand"));
			}
			this.packet = packet;
		}

		protected override void InternalExecute()
		{
			int mobitelID;
			string devIdShort = Convert.ToString(packet.LoginTT);

			// �������� ��������������� �� �������� � ������� MobitelID
			if ((dataManager.TeletrackExist(devIdShort)) &&
			       (dataManager.TryGetMobitelID(devIdShort, out mobitelID))) {
				string parsRow = "";

				if (GetParsedRow(mobitelID, packet.Packet, devIdShort,
					        new string[] { Convert.ToString(packet.ID_Packet) }, out parsRow)) {
					dataManager.InsertDataGPS(new string[1] { parsRow });
				} // if (parsRow)
			} // if (dataManager.TeletrackExist)
      else {
				ErrorHandler.Handle(new ApplicationException(
					String.Format(TDMError.PACKET_FROM_UNKNOWN_TELETRACK, devIdShort)),
					ErrorLevel.WARNING);
			} // else
		}

		/// <summary>
		/// ��� ������� 
		/// </summary>
		protected override int GetCommandID()
		{
			return (int)PKType.PACK_INFO;
		}

		/// <summary>
		/// ���������� ��������� �������
		/// </summary>
		protected override string GetCommandDescr()
		{
			return Enum.GetName(typeof(PKType), PKType.PACK_INFO);
		}

		protected override bool GetNeedResponse()
		{
			return false;
		}

		public override bool IsInsertFunc()
		{
			return true;
		}
	}
}
