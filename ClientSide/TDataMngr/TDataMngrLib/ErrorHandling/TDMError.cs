// Project: TDataMngrLib, File: TDMError.cs.cs
// Namespace: RCS.TDataMngrLib.ErrorHandling, Class: TDMError
// Path: D:\Development\TDataManager_Head\TDataMngrLib\ErrorHandling, Author: guschin
// Code lines: 102, Size of file: 4,44 KB
// Creation date: 25.07.2008 11:56
// Last modified: 18.11.2008 9:54
// Generated with Commenter by abi.exDream.com

using System;

namespace RCS.TDataMngrLib.ErrorHandling
{
  /// <summary>
  /// ������ ������ ����������� � ������ ������
  /// </summary>
  static class TDMError
  {
    /// <summary>
    /// TDM_1. �� ����� ���������� ������� {0} ������ {1}
    /// </summary>
    public const string EVENT_HANDLER_NOT_DEFINED =
      "TDM_1. �� ����� ���������� ������� {0} ������ {1}.";
    /// <summary>
    /// TDM_2. ������ �������� ������ ���������� �� {0}. ���������� ���������: {1}
    /// </summary>
    public const string PACKET_PARSING =
      "TDM_2. ������ �������� ������ ���������� �� {0}. ���������� ���������: {1}";
    /// <summary>
    /// TDM_3. �������������� �� ��������
    /// </summary>
    public const string AUTHENTICATION_FAILED =
      "TDM_3. �������������� �� ��������";
    /// <summary>
    /// TDM_4. ���������� � ����� ������ �� ��������
    /// </summary>
    public const string CONNECT_DB_NOT_AVAILABLE =
      "TDM_4. ���������� � ����� ������ ����������";
    /// <summary>
    /// TDM_5. ���������� � �������� �� ��������
    /// </summary>
    public const string CONNECT_SRV_NOT_AVAILABLE =
      "TDM_5. ���������� � �������� ����������";
    /// <summary>
    ///TDM_6. ��������� ������� {0} �� ����������� � ������ ������ ��������� 
    /// </summary>
    public const string COMMAND_NOT_SUPPORTING =
      "TDM_6. ��������� ������� {0} �� ����������� � ������ ������ ���������.";
    /// <summary>
    /// TDM_7. ��������� ������� {0} �� ����������� � ������ ��������� {1} 
    /// </summary>
    public const string COMMAND_NOT_SUPPORTING_IN_STATE =
      "TDM_7. ��������� ������� {0} �� ����������� � ������ ��������� {1}.";
    /// <summary>
    /// TDM_8. �������� � ��������������� {0} �� ��������������� � ���� ������.
    /// </summary>
    public const string PACKET_FROM_UNKNOWN_TELETRACK =
      "TDM_8. ������ ����� �� ��������������������� � ���� ������ ��������� (������������� {0}).";
    /// <summary>
    /// TDM_9. ����������������: ������������� ��������� {0} � ������ �� ��������� ������� �1 �� ������������� ���������: {1}"; 
    /// </summary>
    public const string ANSWER_COMMAND_MESSAGEID =
      "TDM_9. ����������������: ������������� ��������� {0} � ������ �� ��������� ������� �1 �� ������������� ���������: {1}";

    /// <summary>
    /// TDM_10. ������� ������������ ������� {0} � �������� ��������������
    /// </summary>
    public const string AUTH_BAD_COMMAND =
      "TDM_10. ������� ������������ ������� {0} � �������� ��������������";
    /// <summary>
    /// TDM_11. ������� �������� ������������ ������� {0}
    /// </summary>
    public const string SEND_BAD_COMMAND =
      "TDM_11. ������� �������� ������������ ������� {0}.";
    /// <summary>
    /// TDM_12. ����� Send �� ������������ � ������ ����������. ������ ���� ������� ������������ ������ �������� �������� ��� ������� ������ � �����������.
    /// </summary>
    public const string SEND_METHOD_DEPRECATED =
      "TDM_12. ����� Send �� ������������ � ������ ����������. ������ ���� ������� ������������ ������ �������� �������� ��� ������� ������ � �����������.";
    /// <summary>
    /// TDM_14. ������������ ������� ����� ����������� stateObject = null. ������� ���������: {0}, ��������������� ���������: {1}.
    /// </summary>
    public const string SWITCH_STATE =
      "TDM_14. ������������ ������� ����� ����������� stateObject = null. ������� ���������: {0}, ��������������� ���������: {1}.";
    /// <summary>
    /// TDM_15. ��������� ��������� ��������� CONNECTIONS_AVAILABLE
    /// </summary>
    public const string PHANTOM_CONNECTIONS_AVAILABLE =
      "TDM_15. ��������� ��������� ��������� CONNECTIONS_AVAILABLE.";
    /// <summary>
    /// TDM_16. ����������������: �� ������ ��������� ������� ��������� {0} (�������� � {1} �� {2}) ������ ������ ��������� {3}
    /// </summary>
    public const string QUERY_LAST_PACKETS_MISTIMING =
      "TDM_16. ����������������: �� ������ ������� ��������� ������ ��������� {0} (�������� � {1} �� {2}) ������ ������ ��������� {3}";
    /// <summary>
    /// TDM_17. ����������������: �� ������ ������� ����������� ������ ��������� {0} (�������� � {1} �� {2}) ������ ������ ��������� {3} (�������� � {4} �� {5})
    /// </summary>
    public const string QUERY_LOST_PACKETS_MISTIMING =
      "TDM_17. ����������������: �� ������ ������� ����������� ������ ��������� {0} (�������� � {1} �� {2}) ������ ������ ��������� {3} (�������� � {4} �� {5})";
    /// <summary>
    /// TDM_18. ������� {0} ���. �������� ������ ������� ����� (��������� {1})
    /// </summary>
    public const string ANSWER_TIMEOUT =
      "TDM_18. ������� {0} ���. �������� ������ ������� ����� (��������� {1}).";

    /// <summary>
    /// TDM_20. ��������� ���������� �� ���������
    /// </summary>
    public const string INIT_MOBITEL_CFG_CACHE =
      "TDM_20. ��������� ���������� �� ���������";
    /// <summary>
    /// TDM_21. �� ������� ����� (��������� ���������) ������� {0}.
    /// </summary>
    public const string SERVER_RESOLVING =
      "TDM_21. �� ������� ����� (��������� ���������) ������� {0}.";
    /// <summary>
    /// TDM_22. ������������ ��������� �1 {0} c PacketID={1}(MessageId={2}) �� ������� ��������
    /// </summary>
    public const string A1_MESSAGE_NOT_SUBMITED =
      "TDM_22. ������������ ��������� �1 {0} c PacketID={1}(MessageId={2}) �� ������� ��������.";
    /// <summary>
    /// TDM_23. �� ��������� �� ���� ������ ��������� ����������� � �������. \n ������ �����������: {0}
    /// </summary>
    public const string SRV_CONNECT_CFG_NOT_LOADED =
      "TDM_23. �� ��������� �� ���� ������ ��������� ����������� � �������. \r\n ������ �����������: {0}";

    /// <summary>
    /// TDM_30. �������������� ���������� ������ {0}.\r\n {1}
    /// </summary>
    public const string Thread_ABORT_EXCEPTION =
      "TDM_30. �������������� ���������� ������ {0}.\r\n {1}";
  }
}
