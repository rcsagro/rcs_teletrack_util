
namespace RCS.TDataMngrLib.DataManagement
{
    /// <summary>
    /// ��������� ���������
    /// </summary>
    struct MobitelCfg
    {
        /// <summary>
        /// ID ��������� � ����
        /// </summary>
        public int MobitelID;

        /// <summary>
        /// ������� 64 ���������
        /// </summary>
        public byte Is64;

        /// <summary>
        /// ������������� ������� ����� ������ (����� ���������)
        /// </summary>
        public string DevIdShort;

        /// <summary>
        /// ��������� �������� �����
        /// </summary>
        public long LastPacketID;
    }
}
