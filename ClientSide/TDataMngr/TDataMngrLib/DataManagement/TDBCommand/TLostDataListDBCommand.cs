using System.Data;
using System.Diagnostics;
using MySql.Data.MySqlClient;
using RCS.SrvKernel;
using RCS.SrvKernel.Logging;
using RWLog.DriverDataBase;

namespace RCS.TDataMngrLib.DataManagement.TDBCommand
{
    /// <summary>
    /// Загрузка пропущенных данных
    /// </summary>
    class TLostDataListDBCommand : TDbCommand
    {
        private const string SQL_QUERY =
            "SELECT Mobitel_ID, Begin_SrvPacketID, End_SrvPacketID " +
            "FROM datagpslost_on " +
            "ORDER BY Mobitel_ID, Begin_SrvPacketID";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        
       public TLostDataListDBCommand(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandText("");
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            commdb.CommandText(SQL_QUERY);
            db.SetNewCommand = commdb.GetCommand;
        }

        protected override object InternalExecuteReader()
        {
            System.Diagnostics.Stopwatch sw = new Stopwatch();
            sw.Start();

            db.SetNewCommand = commdb.GetCommand;

            // MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //(( MySqlConnection ) Connection).Open();
            //db.CommandConnectionOpen();
            //return db.CommandExecuteReader(); //MyDbCommand.ExecuteReader();
            sw.Stop();
            Logger.Write("TLostDataListDBCommand: time executing " + sw.ElapsedMilliseconds + " msec");
            return db.Data_Table(commdb.GetCommand);
        }
    }
}
