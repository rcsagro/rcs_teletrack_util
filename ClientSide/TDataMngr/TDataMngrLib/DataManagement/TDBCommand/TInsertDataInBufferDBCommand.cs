using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using MySql.Data.MySqlClient;
using RCS.SrvKernel;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.TDataMngrLib.DataManagement.TDBCommand
{
    internal class TInsertDataInBufferDBCommand : TDbCommand
    {
        private const string SQL_QUERY = "INSERT INTO datagpsbuffer_on (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Speed, Direction, Altitude,Valid, Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,`Events`, whatIs, SrvPacketID) VALUES ";

        private const string msSQL_QUERY = "INSERT INTO datagpsbuffer_on (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Speed, Direction, Altitude, Valid, Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,  Events, whatIs, SrvPacketID) VALUES ";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public TInsertDataInBufferDBCommand(DriverDb connection)
            : base(connection)
        {
            InitQuery(connection);
        }

        private void InitQuery(DriverDb connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandText("");
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;
        }

        /// <summary>
        /// Init
        /// </summary>
        /// <param name="string">������ ����� ������</param>
        public override void Init(params object[] initObjects)
        {
            db.SetNewCommand = commdb.GetCommand;

            string[] rows_array = (string[]) initObjects[0];

            if ((rows_array == null) || (rows_array.Length == 0))
                throw new ArgumentNullException("string[] Rows", AppError.DB_NULL_DATA_INSERT_COMMAND);

            // ���� ����� ������ ������ 1000 �����
            if (rows_array.Length > 1000)
            {
                int iCount = 0;
                int k;
                int length = 1000; // ����������� ���������� ����� ������� � �������
                string[] rows = null;

                do
                {
                    rows = new string[length];

                    for (k = 0; k < length; iCount++, k++)
                    {
                        rows[k] = rows_array[iCount];
                    }

                    // �������� ������ ������

                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        StringBuilder insertRows = new StringBuilder(SQL_QUERY);
                        insertRows.Append(GetRowsFowsRorInsert(rows));
                        db.CommandText(insertRows.ToString());
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        StringBuilder insertRows = new StringBuilder(msSQL_QUERY);
                        insertRows.Append(GetRowsFowsRorInsert(rows));
                        db.CommandText(insertRows.ToString());
                    }

                    InternalExecute();

                    if (rows_array.Length - iCount > 1000)
                    {
                        length = 1000;
                    }
                    else
                    {
                        length = rows_array.Length - iCount;
                    }

                } while (rows_array.Length - iCount > 1000);

                rows = new string[length];

                for (k = 0; k < length; iCount++, k++)
                {
                    rows[k] = rows_array[iCount];
                }

                // �������� �������� ������

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    StringBuilder insertRows = new StringBuilder(SQL_QUERY);
                    insertRows.Append(GetRowsFowsRorInsert(rows));
                    db.CommandText(insertRows.ToString());
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    StringBuilder insertRows = new StringBuilder(msSQL_QUERY);
                    insertRows.Append(GetRowsFowsRorInsert(rows));
                    db.CommandText(insertRows.ToString());
                }

                return;
            } // if

            // ���������� ��� ������

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                StringBuilder insertRows = new StringBuilder(SQL_QUERY);
                insertRows.Append(GetRowsFowsRorInsert(rows_array));
                db.CommandText(insertRows.ToString());
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                StringBuilder insertRows = new StringBuilder(msSQL_QUERY);
                insertRows.Append(GetRowsFowsRorInsert(rows_array));
                db.CommandText(insertRows.ToString());
            }

            //MyDbCommand.CommandText = insertRows.ToString();
        } // Init

        protected override bool NeedExecuteAfterFailure()
        {
            return false;
        }

        /// <summary>
        /// ��������� ������ ������� ����
        /// INSERT INTO datagpsbuffer (...)
        /// VALUES (..., ..., ...), (..., ..., ...)
        /// </summary>
        /// <param name="rows"></param>
        /// <returns>(..., ..., ...), (..., ..., ...)</returns>
        private string GetRowsFowsRorInsert(params string[] rows)
        {
            StringBuilder insertRows = new StringBuilder("");

            foreach (string row in rows)
            {
                if (insertRows.Length != 0)
                    insertRows.Append(",");

                insertRows.Append("(").Append(row).Append(")");
            }
            return insertRows.ToString();
        }

        protected Stopwatch myStopwatch = new Stopwatch(); // ��� ������� �������

        protected override ExecutionStatus InternalExecute()
        {
            myStopwatch.Reset(); // �����
            myStopwatch.Start(); // ������

            //MyDbCommand.Connection = ( MySqlConnection ) Connection;

            db.CommandSqlConnection(db.SqlConnection);
            //db.SetCommandTimeout(120); // ��������� ������� � ���
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            try
            {
                db.CommandExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.Write("datagpsbuffer_on:: (ms) ", "Have error: " + ex.Message);
            }

            myStopwatch.Stop(); // ����������

            long time_tick = 1000*myStopwatch.ElapsedTicks/Stopwatch.Frequency; // � ������������
            Logger.Write("datagpsbuffer_on:: (ms) ", time_tick.ToString());

            return ExecutionStatus.OK;
        }
    }
}
