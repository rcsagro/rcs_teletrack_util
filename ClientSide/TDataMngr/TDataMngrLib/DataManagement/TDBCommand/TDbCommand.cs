
using RCS.SrvKernel.DataManagement.SrvDBCommand;
using RWLog.DriverDataBase;

namespace RCS.TDataMngrLib.DataManagement.TDBCommand
{
    class TDbCommand : SrvDbCommand
    {
        protected TDbCommand()
        {
        }

        public TDbCommand(DriverDb connection) : base(connection)
        {
        }
    }
}
