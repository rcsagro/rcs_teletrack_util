// Project: TDataMngrLib, File: TLostDataDelDBCommand.cs.cs
// Namespace: RCS.TDataMngrLib.DataManagement.TDBCommand, Class: TLostDataDelDBCommand
// Path: D:\Development\TDataManager_Head\TDataMngrLib\DataManagement\TDBCommand, Author: guschin
// Code lines: 59, Size of file: 2.10 KB
// Creation date: 8/8/2008 9:37 AM
// Last modified: 8/8/2008 9:52 AM
// Generated with Commenter by abi.exDream.com

using System;
using System.Data;
using System.Diagnostics;
using MySql.Data.MySqlClient;
using RCS.SrvKernel;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.TDataMngrLib.DataManagement.TDBCommand
{
    /// <summary>
    /// �������� ��������� ��������� �� ������� datagpslost_on 
    /// </summary>
    class TLostDataDelDBCommand : TDbCommand
    {
        private const string PROC_NAME = "OnDeleteLostRange";
        private const string PARAM_MOBITEL_ID = "MobitelID";
        private const string PARAM_BEGIN_SRVPACKET_ID = "BeginSrvPacketID";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public TLostDataDelDBCommand(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandText("");
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.StoredProcedure);
            commdb.CommandText(PROC_NAME);
            commdb.CommandParametersClear();
            commdb.NewSqlParameter(PARAM_MOBITEL_ID, db.GettingInt32());
            commdb.CommandParametersAdd(commdb.GetSqlParameter);
            commdb.NewSqlParameter(PARAM_BEGIN_SRVPACKET_ID, db.GettingInt64());
            commdb.CommandParametersAdd(commdb.GetSqlParameter);

            db.SetNewCommand = commdb.GetCommand;    
        }

        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(AppError.ARGUMENT_NULL,
                        "Init", "TLostDataDelDBCommand", "object[] initObjects"));
            if (initObjects.Length != 2)
                throw new ArgumentException(String.Format(
                    AppError.DB_WRONG_INIT_PARAMS_LENGTH, "TLostDataDelDBCommand",
                    initObjects.Length, 2), "object[] initObjects");

            db.SetNewCommand = commdb.GetCommand; 
            //MyDbCommand.Parameters[PARAM_MOBITEL_ID].Value = (int)initObjects[0];
            db.CommandParametersValue(PARAM_MOBITEL_ID, (int) initObjects[0]);
            //MyDbCommand.Parameters[PARAM_BEGIN_SRVPACKET_ID].Value = (long)initObjects[1];
            db.CommandParametersValue(PARAM_BEGIN_SRVPACKET_ID, (long) initObjects[1]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            System.Diagnostics.Stopwatch sw = new Stopwatch();
            sw.Start();

            try
            {
                //MyDbCommand.Connection = ( MySqlConnection ) Connection;
                db.CommandSqlConnection(db.SqlConnection);
                //((MySqlConnection)Connection).Open();
                //db.CommandConnectionOpen();
                //MyDbCommand.ExecuteNonQuery();
                db.CommandExecuteNonQuery();
                sw.Stop();
                long result = sw.ElapsedMilliseconds;
                Logger.Write("��������� ���������� ������ TLostDataDelDBCommand.InternalExecute � ��: " + result);
                return ExecutionStatus.OK;
            }
            catch (Exception e)
            {
                sw.Stop();
                long result = sw.ElapsedMilliseconds;
                Logger.Write("TLostDataDelDBCommand.InternalExecute: Have error data!");
                Logger.Write("��������� ���������� ������ TLostDataDelDBCommand.InternalExecute � ��: " + result);
                return ExecutionStatus.ERROR_DATA;
            }
        }
    }
}
