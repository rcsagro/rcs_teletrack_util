using System.Data;
using MySql.Data.MySqlClient;
using RCS.SrvKernel;
using RWLog.DriverDataBase;

namespace RCS.TDataMngrLib.DataManagement.TDBCommand
{
    /// <summary>
    /// Список телетреков
    /// </summary>
    class TListMobitelCfgDBCommand : TDbCommand
    {
        private const string PROC_NAME = "OnListMobitelConfig64";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public TListMobitelCfgDBCommand(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandText("");
            commdb.CommandParametersClear();
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.StoredProcedure);
            commdb.CommandText(PROC_NAME);
            db.SetNewCommand = commdb.GetCommand;
        }

        protected override object InternalExecuteReader()
        {
            db.SetNewCommand = commdb.GetCommand;

            // MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            return db.CommandExecuteReader(); //MyDbCommand.ExecuteReader();
        }
    }
}
