﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using MySql.Data.MySqlClient;
using RCS.SrvKernel;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.TDataMngrLib.DataManagement.TDBCommand
{
    class TTransferBuffer64DBCommand : TDbCommand
    {
        private string PROC_NAME = "OnTransferBuffer64";

        DriverDb db = new DriverDb();
        DriverDb commdb = new DriverDb();

        public TTransferBuffer64DBCommand(DriverDb connection)
            : base(connection)
        {
            InitQuery(connection);
        }

        private void InitQuery(DriverDb connection)
        {
            PROC_NAME = "OnTransferBuffer64";
            if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
            {
                PROC_NAME = string.Format("dbo.{0}", PROC_NAME);
            }
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandText("");
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.StoredProcedure);
            commdb.CommandParametersClear();
            commdb.CommandText(PROC_NAME);
            db.SetNewCommand = commdb.GetCommand;
        }

        protected override ExecutionStatus InternalExecute()
        {
            System.Diagnostics.Stopwatch sw = new Stopwatch();
            sw.Start();
            db.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
         
            try
            {
                if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
                {
                    MySqlCommand command = db.GetCommand as MySqlCommand;
                    command.ExecuteNonQuery();
                }

                if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
                {
                    SqlCommand command = db.GetCommand as SqlCommand;
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Logger.Write("OnTransferBuffer64: " + e.Message + ". Stacktrace:" + e.StackTrace + ". Source:" + e.Source + ". TargetSite:" + e.TargetSite);
            }

            sw.Stop();
            long result = sw.ElapsedMilliseconds;

            Logger.Write("Результат выполнения вызова OnTransferBuffer64 в мс: " + result);
            //Logger.Write("Результат выполнения тела OnTransferBuffer64 в мс: " + param);

            return ExecutionStatus.OK;
        }
    }
}
