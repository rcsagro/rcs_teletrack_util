using System;
using System.Data;
using RCS.SrvKernel;
using RWLog.DriverDataBase;

namespace RCS.TDataMngrLib.DataManagement.TDBCommand
{
    class TOnlineCfgDBCommand : TDbCommand
    {
        private const string QUERY =
            "SELECT * FROM ServiceInit WHERE ServiceType_ID = 3";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public TOnlineCfgDBCommand(DriverDb connection)
            : base(connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandText("");
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            commdb.CommandText(QUERY);
            db.SetNewCommand = commdb.GetCommand;  
        }

        protected override object InternalExecuteReader()
        {
            db.SetNewCommand = commdb.GetCommand;

            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            try
            {
                return db.CommandExecuteReader();
            }
            catch (Exception)
            {
                return null;
            } 
        }
    }
}
