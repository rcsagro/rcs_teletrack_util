using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using MySql.Data.MySqlClient;
using RCS.SrvKernel;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;

namespace RCS.TDataMngrLib.DataManagement.TDBCommand
{
    internal class TTransferBufferDBCommand : TDbCommand
    {

        private string _procName = "OnTransferBuffer";

        private DriverDb db;
        private DriverDb commdb = new DriverDb();

        public TTransferBufferDBCommand(DriverDb connection)
            : base(connection)
        {

            InitDb(connection);
        }

        private void InitDb(DriverDb connection)
        {
            _procName = "OnTransferBuffer";

            if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
            {
                _procName = string.Format("dbo.{0}", _procName);
            }

            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandText("");
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.StoredProcedure);
            commdb.CommandParametersClear();
            commdb.CommandText(_procName);
            db.SetNewCommand = commdb.GetCommand;
        }

        private bool MySqlWaitForEndOfRead(MySqlConnection _connection)
        {
            int count = 0;
            while (_connection.State == ConnectionState.Fetching)
            {
                Thread.Sleep(1000);
                count++;
                if (count == 10)
                {
                    return false;
                }
            }

            return true;
        }

        private bool SqlWaitForEndOfRead(SqlConnection _connection)
        {
            int count = 0;
            while (_connection.State == ConnectionState.Fetching)
            {
                Thread.Sleep(1000);
                count++;
                if (count == 10)
                {
                    return false;
                }
            }

            return true;
        }

        protected override ExecutionStatus InternalExecute()
        {
            System.Diagnostics.Stopwatch sw = new Stopwatch();
            sw.Start();

            db.SetNewCommand = commdb.GetCommand;
            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            // ((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            //db.CommandExecuteNonQuery();

            //int param = 0;
            try
            {
                if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
                {
                    bool flagFatching = MySqlWaitForEndOfRead(db.SqlConnection as MySqlConnection);
                    if (flagFatching == false)
                        throw new Exception("TTransferBufferDBCommand.InternalExecute: Can not using Reader it very busy!");
                    MySqlCommand command = db.GetCommand as MySqlCommand;
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        //try
                        //{
                            if (reader.HasRows)
                            {
                                //while (reader.Read())
                                //{
                                    //try
                                    //{
                                        //param = reader.GetInt32(0);
                                    //}
                                    //catch (Exception e)
                                    //{
                                        //int y = 0;
                                    //}
                                //}
                            }
                        //}
                        //catch (Exception ex)
                        //{
                            //int i = 0;
                        //}
                        reader.Close();
                    }
                }

                if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
                {
                    bool flagFatching = SqlWaitForEndOfRead(db.SqlConnection as SqlConnection);
                    if (flagFatching == false)
                        throw new Exception("TTransferBufferDBCommand.InternalExecute: Can not using Reader it very busy!");
                    SqlCommand command = db.GetCommand as SqlCommand;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            //while (reader.Read())
                            //{
                                //param = reader.GetInt32(0);
                            //}
                        }
                        reader.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Write("OnTransferBuffer: " + e.Message);
            }

            sw.Stop();
            long result = sw.ElapsedMilliseconds;

            Logger.Write("��������� ���������� ������ OnTransferBuffer � ��: " + result);
            //Logger.Write("��������� ���������� ���� OnTransferBuffer � ��: " + param);

            return ExecutionStatus.OK;
        }
    }
}
