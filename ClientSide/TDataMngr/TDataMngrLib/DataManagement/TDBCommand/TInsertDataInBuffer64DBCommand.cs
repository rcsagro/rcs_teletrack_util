﻿using System.Collections.Generic;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand.Sending;
using RWLog.DriverDataBase;
using System;
using System.Data;
using System.Diagnostics;
using System.Text;
using RCS.SrvKernel;

namespace RCS.TDataMngrLib.DataManagement.TDBCommand
{
    internal class TInsertDataInBuffer64DBCommand : TDbCommand
    {
        private const string SQL_QUERY = "INSERT INTO datagpsbuffer_on64 (Mobitel_ID,LogID, Latitude, Longitude,  Direction, Acceleration, UnixTime, Speed, Valid, Satellites, RssiGsm, `Events`, SensorsSet, Voltage,DGPS,Sensors, SrvPacketID) VALUES  ";

        private const string msSQL_QUERY = @"INSERT INTO datagpsbuffer_on64 (Mobitel_ID,LogID, Latitude, Longitude,  Direction, Acceleration, UnixTime, Speed, Valid, Satellites, RssiGsm, Events, SensorsSet, Voltage,DGPS,Sensors, SrvPacketID) VALUES  ";

        DriverDb db;
        DriverDb commdb = new DriverDb();

        public TInsertDataInBuffer64DBCommand(DriverDb connection)
            : base(connection)
        {
            InitQuery(connection);
        }

        private void InitQuery(DriverDb connection)
        {
            db = connection;
            commdb.NewSqlCommand();
            commdb.CommandText("");
            commdb.CommandTimeout(SrvSettings.DataBaseCommandTimeout);
            commdb.CommandType(CommandType.Text);
            commdb.CommandParametersClear();
            db.SetNewCommand = commdb.GetCommand;
        }

        /// <summary>
        /// Init
        /// </summary>
        /// <param name="string">массив строк данных</param>
        public override void Init(params object[] initObjects)
        {
            db.SetNewCommand = commdb.GetCommand;

            string[] rows_array = (string[])initObjects[0];

            if ((rows_array == null) || (rows_array.Length == 0))
                throw new ArgumentNullException("string[] Rows", AppError.DB_NULL_DATA_INSERT_COMMAND);

            // если строк данных больше 1000 строк
            if (rows_array.Length > 1000)
            {
                int iCount = 0;
                int k;
                int length = 1000; // максимально допустимое число записей в запросе
                string[] rows = null;

                do
                {
                    rows = new string[length];

                    for (k = 0; k < length; iCount++, k++)
                    {
                        rows[k] = rows_array[iCount];
                    }

                    // отправка партии данных

                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        StringBuilder insertRows = new StringBuilder(SQL_QUERY);
                        insertRows.Append(GetRowsFowsRorInsert(rows));
                        db.CommandText(insertRows.ToString());
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        StringBuilder insertRows = new StringBuilder(msSQL_QUERY);
                        insertRows.Append(GetRowsFowsRorInsert(rows));
                        db.CommandText(insertRows.ToString());
                    }

                    InternalExecute();

                    if (rows_array.Length - iCount > 1000)
                    {
                        length = 1000;
                    }
                    else
                    {
                        length = rows_array.Length - iCount;
                    }

                } while (rows_array.Length - iCount > 1000);

                rows = new string[length];

                for (k = 0; k < length; iCount++, k++)
                {
                    rows[k] = rows_array[iCount];
                }

                // отправка остатков данных

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    StringBuilder insertRows = new StringBuilder(SQL_QUERY);
                    insertRows.Append(GetRowsFowsRorInsert(rows));
                    db.CommandText(insertRows.ToString());
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    StringBuilder insertRows = new StringBuilder(msSQL_QUERY);
                    insertRows.Append(GetRowsFowsRorInsert(rows));
                    db.CommandText(insertRows.ToString());
                }

                return;
            } // if

            // отправляем как обычно

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                StringBuilder insertRows = new StringBuilder(SQL_QUERY);
                insertRows.Append(GetRowsFowsRorInsert(rows_array));
                db.CommandText(insertRows.ToString());
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                StringBuilder insertRows = new StringBuilder(msSQL_QUERY);
                insertRows.Append(GetRowsFowsRorInsert(rows_array));
                db.CommandText(insertRows.ToString());
            }

            //MyDbCommand.CommandText = insertRows.ToString();
        } // Init

        protected override bool NeedExecuteAfterFailure()
        {
            return false;
        }

        /// <summary>
        /// Формируем строку инсерта вида
        /// INSERT INTO datagpsbuffer (...)
        /// VALUES (..., ..., ...), (..., ..., ...)
        /// </summary>
        /// <param name="rows"></param>
        /// <returns>(..., ..., ...), (..., ..., ...)</returns>
        private string GetRowsFowsRorInsert(params string[] rows)
        {
            StringBuilder insertRows = new StringBuilder("");

            List<String> rowsNotDoubled = new List<string>();
            foreach (string row in rows)
            {
                if (!rowsNotDoubled.Contains(row))
                    {
                    if (insertRows.Length != 0)
                        insertRows.Append(",");
                        insertRows.Append("(").Append(row).Append(")");
                        rowsNotDoubled.Add(row);
                    }
                else
                {
                    Logger.Write("datagpsbuffer_on64:: задвоение данных ", row);
                }
            }
            rowsNotDoubled.Clear();
            return insertRows.ToString();
        }

        protected Stopwatch myStopwatch = new Stopwatch(); // для замеров времени

        protected override ExecutionStatus InternalExecute()
        {

            myStopwatch.Reset(); // сброс
            myStopwatch.Start(); // запуск

            //MyDbCommand.Connection = ( MySqlConnection ) Connection;
            db.CommandSqlConnection(db.SqlConnection);
            //db.SetCommandTimeout(120); // установим таймаут в сек
            //((MySqlConnection)Connection).Open();
            //db.CommandConnectionOpen();
            //MyDbCommand.ExecuteNonQuery();
            try
            {
                db.CommandExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.Write("datagpsbuffer_on64:: (ms) ", "Have error: " + ex.Message);
            }

            myStopwatch.Stop(); // остановить

            long time_tick = 1000 * myStopwatch.ElapsedTicks / Stopwatch.Frequency; // в милисекундах
            Logger.Write("datagpsbuffer_on64:: (ms) ", time_tick.ToString());

            return ExecutionStatus.OK;
        }
    }
}
