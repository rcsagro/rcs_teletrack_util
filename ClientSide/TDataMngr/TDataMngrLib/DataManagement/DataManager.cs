#region Using directives
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.SqlServer.Server;
using MySql.Data.MySqlClient;
using RCS.SrvKernel.DataManagement;
using RCS.SrvKernel.DataManagement.SrvDBCommand;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand.Sending;
using RCS.SrvKernel.Util;
using RCS.TDataMngrLib.DataManagement.Cache;
using RCS.TDataMngrLib.DataManagement.TDBCommand;
using RCS.TDataMngrLib.ErrorHandling;
using RWLog.DriverDataBase;

#endregion

namespace RCS.TDataMngrLib.DataManagement
{
    class DataManager : BaseDataManager, IMethodsInspect
    {
        /// <summary>
        /// ������������ ���-�� ����� �������� � ������ = 100
        /// </summary>
        private const int MAX_ROW_COUNT = 100;

        /// <summary>
        /// ������ ������������� ��� ����� �������
        /// </summary>
        private static object _syncObject = new object();

        /// <summary>
        /// ������ ������������� ��� ������ � ����������� ���������
        /// </summary>
        private static object _syncCfgMobitelObject = new object();

        /// <summary>
        /// ������ ������������� ��� ������ � ����� ���������
        /// </summary>
        private static object _syncLostDataObject = new object();

        /// <summary>
        /// ��� � ����������� ����������
        /// </summary>
        private CacheMobitelsCfg _mobitelsCfgCache;
        /// <summary>
        /// ��� ���������� ������������� ������
        /// </summary>
        private CacheLostData _lostDataCache;

        public DataManager()
            : base()
        {
            _mobitelsCfgCache = new CacheMobitelsCfg();
            _lostDataCache = new CacheLostData();
        }

        /// <summary>
        /// �������� ���������� � ��������
        /// </summary>
        /// <returns>bool</returns>
        public bool TryToConnect()
        {
            return PossibleTo�onnect();
        }

        #region ��������� ��������� (��������� ���)

        /// <summary>
        /// ��������: ��������������� �� ��������
        /// </summary>
        /// <param name="devIdShort"></param>
        /// <returns>bool</returns>
        internal bool TeletrackExist(string devIdShort)
        {
            lock (_syncCfgMobitelObject)
            {
                return _mobitelsCfgCache.TeletrackExist(devIdShort);
            }
        }

        /// <summary>
        /// ���������� mobitelID ��������� ��� ������ 
        /// �������� ������������� ���������
        /// </summary>
        /// <param name="devIdShort">�������� ������������� ���������</param>
        /// <param name="mobitelID">MobitelID</param>
        /// <returns>true ���� ������</returns>
        internal bool TryGetMobitelID(string devIdShort, out int mobitelID)
        {
            lock (_syncCfgMobitelObject)
            {
                return _mobitelsCfgCache.TryGetMobitelID(devIdShort, out mobitelID);
            }
        }

        /// <summary>
        /// ���������� �������� ������������� ��������� ���������
        /// ��� ������ mobitelID
        /// </summary>
        /// <param name="mobitelID">Mobitel ID</param>
        /// <param name="devIdShort">�������� ������������� ���������</param>
        /// <returns>true ���� ������</returns>
        internal bool TryGetDevIdShort(int mobitelID, out string devIdShort)
        {
            lock (_syncCfgMobitelObject)
            {
                return _mobitelsCfgCache.TryGetDevIdShort(mobitelID, out devIdShort);
            }
        }

        /// <summary>
        /// �������� ���� ������ �������� ����������
        /// </summary>
        /// <returns>Dictionary</returns>
        internal List<MobitelCfg> GetMobitelsCfg()
        {
            lock (_syncCfgMobitelObject)
            {
                return _mobitelsCfgCache.GetMobitelsCfg();
            }
        }

        #endregion

        #region �������� � ������ (��������� ���)

        /// <summary>
        /// ����� ���������� ��������� ����������� ������ ��� ��������� ��������� 
        /// </summary>
        /// <param name="mobitelID">������������� ���������</param>
        /// <param name="lostDataRange">��������� ��������� ��������</param>
        /// <returns>true - ���� ���� ������ � ������� ���������</returns>
        internal bool GetLastLostRange(int mobitelID, out LostDataRange lostDataRange)
        {
            lock (_syncLostDataObject)
            {
                return _lostDataCache.GetLastRange(mobitelID, out lostDataRange);
            }
        }

        /// <summary>
        /// ������ ���� ��������� ���������
        /// </summary>
        /// <param name="mobitelID">Mobitel ID</param>
        /// <returns>List</returns>
        internal List<LostDataRange> GetLostRanges(int mobitelID)
        {
            lock (_syncLostDataObject)
            {
                return _lostDataCache.GetLostRanges(mobitelID);
            }
        }

        /// <summary>
        /// ������� ������ � ���� ����������� ������
        /// </summary>
        /// <param name="mobitelID">������������� ���������</param>
        /// <param name="beginSrvPacketID">��������� �������� ��������� ��������</param>
        internal void RemoveLostRangeInCache(int mobitelID, long beginSrvPacketID)
        {
            lock (_syncLostDataObject)
            {
                _lostDataCache.RemoveRow(mobitelID, beginSrvPacketID);
            }
        }

        /// <summary>
        /// ���������� ������� ��������� � ����
        /// </summary>
        /// <returns>Int</returns>
        internal int GetLostRangesCount()
        {
            lock (_syncLostDataObject)
            {
                return _lostDataCache.GetRowsCount();
            }
        }

        #endregion

        #region ���������� ���������� ����

        /// <summary>
        /// ���������� ���������� ����
        /// </summary>
        /// <returns>true ���� ��� ������ ������</returns>
        internal bool InitLocalCache()
        {
            if (FillMobitelCfg() != ExecutionStatus.OK)
            {
                ErrorHandler.Handle(
                  new ApplicationException(TDMError.INIT_MOBITEL_CFG_CACHE));
                return false;
            }

            return true;
        }

        private const string TELETRACK_CFG_DEBUG_INFO =
          "��������� �� ���� ������ ��������� ��������� DevIdShort: {0}; MobitelID: {1}; LastPacketID: {2}";

        DriverDb db = new DriverDb();

        /// <summary>
        /// ���������� ���������� ���� �������� ����������.
        /// ��� ���������� ��������� ����� ���������: 
        /// ������� ����������� ��������� �����, � ���� �� ���������
        /// ������ - ��������� ���.
        /// </summary>
        /// <returns>FetchDataStatus</returns>
        private ExecutionStatus FillMobitelCfg()
        {
            lock (_syncCfgMobitelObject)
            {
                ISrvDbCommand command = _commandCache.GetDbCommand<TListMobitelCfgDBCommand>();

                ExecutionStatus result = ExecutionStatus.OK;
                try
                {
                    // ��������� �����
                    List<MobitelCfg> tmpBuffer = new List<MobitelCfg>();

                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                        {
                            if ((result == ExecutionStatus.OK) && (reader != null))
                            {
                                while (reader.Read())
                                {
                                    MobitelCfg config;

                                    config.DevIdShort = reader.GetString("DevIdShort");
                                    config.MobitelID = reader.GetInt32("MobitelID");
                                    config.Is64 = reader.GetByte("Is64");

                                    if(config.Is64 == 0)
                                        config.LastPacketID = reader.GetInt64("LastPacketID");
                                    else if (config.Is64 == 1)
                                        config.LastPacketID = reader.GetInt64("LastPacketID64");
                                    else
                                        config.LastPacketID = 0;

                                    tmpBuffer.Add(config);

                                    Logger.Write(String.Format(TELETRACK_CFG_DEBUG_INFO,
                                                               config.DevIdShort, config.MobitelID, config.LastPacketID));
                                }
                            }
                        }
                    } // else if
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                        {
                            if ((result == ExecutionStatus.OK) && (reader != null))
                            {
                                while (reader.Read())
                                {
                                    MobitelCfg config;

                                    int k = reader.GetOrdinal("DevIdShort");
                                    config.DevIdShort = reader.GetString(k);
                                    k = reader.GetOrdinal("Is64");
                                    config.Is64 = reader.GetByte(k);
                                    k = reader.GetOrdinal("MobitelID");
                                    config.MobitelID = reader.GetInt32(k);
                                    if (config.Is64 == 0)
                                    {
                                        k = reader.GetOrdinal("LastPacketID");
                                        config.LastPacketID = reader.GetInt64(k);
                                    }
                                    else if (config.Is64 == 1)
                                    {
                                        k = reader.GetOrdinal("LastPacketID64");
                                        config.LastPacketID = reader.GetInt64(k);
                                    }
                                    else
                                    {
                                        config.LastPacketID = 0;
                                    }

                                    tmpBuffer.Add(config);

                                    Logger.Write(String.Format(TELETRACK_CFG_DEBUG_INFO,
                                                               config.DevIdShort, config.MobitelID, config.LastPacketID));
                                }
                            }
                        }
                    }
                    // � ��� ������ ������ ����� �������� ��� �������� ����������
                    if (tmpBuffer.Count > 0)
                    {
                        _mobitelsCfgCache.Clear();
                        foreach (MobitelCfg cfg in tmpBuffer)
                        {
                            _mobitelsCfgCache.AddRow(cfg);
                        }
                    }
                }
                catch (Exception Ex)
                {
                    result = ExecutionStatus.ERROR_DB;
                    ErrorHandler.Handle(Ex);
                }
                finally
                {
                    //command.CloseConnection();
                    Logger.Write("FillMobitelCfg: Connection with server attempt clousing!");
                }

                return result;
            }
        }

        /// <summary>
        /// ��������� �� ���� ������ ��������� ����������� � ��������������� �������. \r\n
        /// ������: {0}; ����: {1}; �����: {2}; ������: {3}
        /// </summary>
        private const string GOOD_SRVCONNECTION_CFG_DEBUG_INFO =
          "��������� �� ���� ������ ��������� ����������� � ��������������� �������. \r\n" +
          "������: {0}; ����: {1}; �����: {2}; ������: {3}";

        /// <summary>
        /// � ���� ������ ��������� ������������ ��������� ����������� � ��������������� �������. 
        /// ������: {0}; ����: {1}; �����: {2}; ������: {3} 
        /// ��� ��������� ���������������.
        /// </summary>
        private const string BAD_SRVCONNECTION_CFG_DEBUG_INFO =
          "� ���� ������ ��������� ������������ ��������� ����������� � ��������������� �������. \r\n" +
          "������: {0}; ����: {1}; �����: {2}; ������: {3} \r\n" +
          "��� ��������� ���������������.";

        /// <summary>
        /// ��������� ����������� � ��������������� �������
        /// ��� ���������� ��������� ����� ���������: 
        /// ������� ����������� ��������� �����, � ���� �� ���������
        /// ������ - ���������� ���������� ���������
        /// </summary>
        /// <param name="cfg">��������� ��� ���������� �������� �����������</param>
        /// <returns>FetchDataStatus</returns>
        internal ExecutionStatus GetSrvConnectionCfg(ref SrvConnectionSettings cfg)
        {
            lock (_syncObject)
            {
                ISrvDbCommand command = _commandCache.GetDbCommand<TOnlineCfgDBCommand>();

                ExecutionStatus result = ExecutionStatus.OK;
                try
                {
                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                        {
                            if ((result == ExecutionStatus.OK) && (reader != null))
                            {
                                if (reader.Read())
                                {
                                    SrvConnectionSettings tmpBuffer;
                                    tmpBuffer.Host = "";
                                    tmpBuffer.Port = -1;
                                    tmpBuffer.User = "";
                                    tmpBuffer.Password = "";

                                    if (!reader.IsDBNull(reader.GetOrdinal("A1")))
                                    {
                                        tmpBuffer.Host = reader.GetString("A1");
                                    }
                                    if (!reader.IsDBNull(reader.GetOrdinal("A2")))
                                    {
                                        tmpBuffer.Port = reader.GetInt32("A2");
                                    }
                                    if (!reader.IsDBNull(reader.GetOrdinal("A3")))
                                    {
                                        tmpBuffer.User = reader.GetString("A3");
                                    }
                                    if (!reader.IsDBNull(reader.GetOrdinal("A4")))
                                    {
                                        tmpBuffer.Password = reader.GetString("A4");
                                    }
                                    // �������� ������ ���������� ���������
                                    if (!String.IsNullOrEmpty(tmpBuffer.Host) &&
                                        TcpIP.ValidateTcpPort(tmpBuffer.Port) && !String.IsNullOrEmpty(tmpBuffer.User))
                                    {
                                        cfg.Host = tmpBuffer.Host;
                                        cfg.Port = tmpBuffer.Port;
                                        cfg.User = tmpBuffer.User;
                                        cfg.Password = tmpBuffer.Password;

                                        Logger.Write(String.Format(GOOD_SRVCONNECTION_CFG_DEBUG_INFO,
                                                                   cfg.Host, cfg.Port, cfg.User, cfg.Password));
                                    }
                                    else
                                    {
                                        ErrorHandler.Handle(new ApplicationException(
                                                                String.Format(BAD_SRVCONNECTION_CFG_DEBUG_INFO,
                                                                              tmpBuffer.Host, tmpBuffer.Port, tmpBuffer.User,
                                                                              tmpBuffer.Password)),
                                                            ErrorLevel.WARNING);
                                    }
                                }
                                else
                                {
                                    result = ExecutionStatus.ERROR_DATA;
                                }
                            }

                            reader.Close();
                        }
                    } // if
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                        {
                            if ((result == ExecutionStatus.OK) && (reader != null))
                            {
                                if (reader.Read())
                                {
                                    SrvConnectionSettings tmpBuffer;
                                    tmpBuffer.Host = "";
                                    tmpBuffer.Port = -1;
                                    tmpBuffer.User = "";
                                    tmpBuffer.Password = "";

                                    int k = 0;
                                    if (!reader.IsDBNull(reader.GetOrdinal("A1")))
                                    {
                                        k = reader.GetOrdinal("A1");
                                        tmpBuffer.Host = DeleteSpace(reader.GetString(k));
                                    }
                                    if (!reader.IsDBNull(reader.GetOrdinal("A2")))
                                    {
                                        k = reader.GetOrdinal("A2");
                                        string str = DeleteSpace(reader.GetString(k));
                                        tmpBuffer.Port = Convert.ToInt32(str);
                                    }
                                    if (!reader.IsDBNull(reader.GetOrdinal("A3")))
                                    {
                                        k = reader.GetOrdinal("A3");
                                        tmpBuffer.User = DeleteSpace(reader.GetString(k));
                                    }
                                    if (!reader.IsDBNull(reader.GetOrdinal("A4")))
                                    {
                                        k = reader.GetOrdinal("A4");
                                        tmpBuffer.Password = DeleteSpace(reader.GetString(k));
                                    }
                                    // �������� ������ ���������� ���������
                                    if (!String.IsNullOrEmpty(tmpBuffer.Host) &&
                                        TcpIP.ValidateTcpPort(tmpBuffer.Port) && !String.IsNullOrEmpty(tmpBuffer.User))
                                    {
                                        cfg.Host = tmpBuffer.Host;
                                        cfg.Port = tmpBuffer.Port;
                                        cfg.User = tmpBuffer.User;
                                        cfg.Password = tmpBuffer.Password;

                                        Logger.Write(String.Format(GOOD_SRVCONNECTION_CFG_DEBUG_INFO,
                                                                   cfg.Host, cfg.Port, cfg.User, cfg.Password));
                                    }
                                    else
                                    {
                                        ErrorHandler.Handle(new ApplicationException(
                                                                String.Format(BAD_SRVCONNECTION_CFG_DEBUG_INFO,
                                                                              tmpBuffer.Host, tmpBuffer.Port, tmpBuffer.User,
                                                                              tmpBuffer.Password)),
                                                            ErrorLevel.WARNING);
                                    }
                                }
                                else
                                {
                                    result = ExecutionStatus.ERROR_DATA;
                                }
                            }

                            reader.Close();
                        }
                    }
                }
                catch (Exception Ex)
                {
                    result = ExecutionStatus.ERROR_DATA;
                    ErrorHandler.Handle(Ex);
                }
                finally
                {
                    //command.CloseConnection();
                    Logger.Write("GetSrvConnectionCfg: Connection with server attempt clousing!");
                }

                return result;
            }
        }

        // �������� ������� � ����� ������
        private string DeleteSpace(string str)
        {
            string tmp = "";
            string space = " ";

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != space[0])
                {
                    tmp = tmp + str[i];
                }
            }

            return tmp;
        }

        /// <summary>
        /// ���������� ���������� ���� ���������� ���������
        /// </summary>
        /// <returns>ExecutionStatus</returns>
        internal ExecutionStatus FillLostData()
        {
            lock (_syncLostDataObject)
            {
                System.Diagnostics.Stopwatch sw = new Stopwatch();
                sw.Start();

                _lostDataCache.Clear();

                ISrvDbCommand command = _commandCache.GetDbCommand<TLostDataListDBCommand>();

                ExecutionStatus result = ExecutionStatus.OK;
                try
                {
                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        //using (MySqlDataReader reader = (MySqlDataReader)command.ExecuteReader(out result))
                        DataTable reader = (DataTable) command.ExecuteReader(out result);
                        {
                            if ((result == ExecutionStatus.OK) && (reader != null))
                            {
                                //while (reader.Read())
                                Logger.Write("FillLostData: TLostDataListDBCommand have records: " + reader.Rows.Count);

                                int MobitelID = 0;
                                long BeginSrvPacketID = 0;
                                long EndSrvPacketID = 0;

                                for (int i = 0; i < reader.Rows.Count; i++)
                                {
                                    //LostDataRange range = new LostDataRange();
                                    //range.MobitelID = reader.GetInt32("Mobitel_ID");
                                    //range.BeginSrvPacketID = reader.GetInt64("Begin_SrvPacketID");
                                    //range.EndSrvPacketID = reader.GetInt64("End_SrvPacketID");

                                    MobitelID = Convert.ToInt32(reader.Rows[i]["Mobitel_ID"].ToString());
                                    BeginSrvPacketID = Convert.ToInt64(reader.Rows[i]["Begin_SrvPacketID"].ToString());
                                    EndSrvPacketID = Convert.ToInt64(reader.Rows[i]["End_SrvPacketID"].ToString());

                                    //_lostDataCache.AddRow(range);

                                    _lostDataCache.AddRow(MobitelID, BeginSrvPacketID, EndSrvPacketID);
                                }
                            }
                        }
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        //using (SqlDataReader reader = (SqlDataReader)command.ExecuteReader(out result))
                        DataTable reader = (DataTable)command.ExecuteReader(out result);
                        {
                            if ((result == ExecutionStatus.OK) && (reader != null))
                            {
                                int MobitelID = 0;
                                long BeginSrvPacketID = 0;
                                long EndSrvPacketID = 0;

                                //while (reader.Read())
                                Logger.Write("FillLostData: TLostDataListDBCommand have records: " + reader.Rows.Count);

                                for (int i = 0; i < reader.Rows.Count; i++)
                                {
                                    //LostDataRange range = new LostDataRange();

                                    //int k = reader.GetOrdinal("Mobitel_ID");
                                    //range.MobitelID = reader.GetInt32(k);
                                    //k = reader.GetOrdinal("Begin_SrvPacketID");
                                    //range.BeginSrvPacketID = reader.GetInt64(k);
                                    //k = reader.GetOrdinal("End_SrvPacketID");
                                    //range.EndSrvPacketID = reader.GetInt64(k);
                                    //range.MobitelID = Convert.ToInt32(reader.Rows[i]["Mobitel_ID"].ToString());
                                    //range.BeginSrvPacketID = Convert.ToInt64(reader.Rows[i]["Begin_SrvPacketID"].ToString());
                                    //range.EndSrvPacketID = Convert.ToInt64(reader.Rows[i]["End_SrvPacketID"].ToString());

                                    MobitelID = Convert.ToInt32(reader.Rows[i]["Mobitel_ID"].ToString());
                                    BeginSrvPacketID = Convert.ToInt64(reader.Rows[i]["Begin_SrvPacketID"].ToString());
                                    EndSrvPacketID = Convert.ToInt64(reader.Rows[i]["End_SrvPacketID"].ToString());

                                    //_lostDataCache.AddRow(range);
                                    _lostDataCache.AddRow(MobitelID, BeginSrvPacketID, EndSrvPacketID);
                                }
                            }
                        }
                    }

                    _lostDataCache.AcceptChanges();
                    int rows = _lostDataCache.SizeTable();
                    Logger.Write("FillLostData: Getting data from datagpslost_on have records: " + rows);
                }
                catch (Exception Ex)
                {
                    //result = ExecutionStatus.ERROR_DATA;
                    ErrorHandler.Handle(Ex);
                }
                finally
                {
                    //command.CloseConnection();
                    sw.Stop();
                    Logger.Write("FillLostData: function completed with " + sw.ElapsedMilliseconds + " msec");
                }

                return result;
            }
        }

        #endregion

        /// <summary>
        /// ������� ��� ������� � ����� ��
        /// </summary>
        /// <returns>ISrvDbCommand</returns>
        protected override ISrvDbCommand GetInsertDataInBufferDBCommand()
        {
            return _commandCache.GetDbCommand<TInsertDataInBufferDBCommand>();
        }

        /// <summary>
        /// ������� ��� ������� � �����64 ��
        /// </summary>
        /// <returns>ISrvDbCommand</returns>
        protected override ISrvDbCommand GetInsertDataInBuffer64DBCommand()
        {
            return _commandCache.GetDbCommand<TInsertDataInBuffer64DBCommand>();
        }

        /// <summary>
        /// ������� ��� ����������� ������ �� � ������� DataGPS
        /// </summary>
        /// <returns>ISrvDbCommand</returns>
        protected override ISrvDbCommand GetTransferBufferDBCommand()
        {
            return _commandCache.GetDbCommand<TTransferBufferDBCommand>();
        }

        /// <summary>
        /// ������� ��� ����������� ������ �� � ������� DataGPS64
        /// </summary>
        /// <returns>ISrvDbCommand</returns>
        protected override ISrvDbCommand GetTransferBuffer64DBCommand()
        {
            return _commandCache.GetDbCommand<TTransferBuffer64DBCommand>();
        }

        /// <summary>
        /// ������ ������� �������� ������������ ��������� �� ������� datagpslost_on
        /// </summary>
        /// <param name="mobitelID">������������� ���������</param>
        /// <param name="beginSrvPacketID">��������� �������� �������� (ID ������ �� �������)</param>
        /// <returns>ExecutionStatus</returns>
        internal ExecutionStatus DeleteLostRegion(int mobitelID, long beginSrvPacketID)
        {
            try
            {
                ISrvDbCommand command =
                    _commandCache.GetDbCommand<TLostDataDelDBCommand>();
                command.Init(new object[2] {mobitelID, beginSrvPacketID});
                return command.Execute();
            }
            catch (Exception e)
            {
                Logger.Write("DeleteLostRegion: => " + e.Message);
                throw new Exception("DeleteLostRegion: Error allocate TLostDataDelDBCommand!" );
            }
        }
    }
}
