
namespace RCS.TDataMngrLib.DataManagement
{
    /// <summary>
    /// ��������� ��������� ��������
    /// </summary>
    public struct LostDataRange
    {
        /// <summary>
        /// ������������� ���������
        /// </summary>
        public int MobitelID;

        /// <summary>
        /// ��������� �������� ��������� ��������
        /// </summary>
        public long BeginSrvPacketID;

        /// <summary>
        /// �������� �������� ��������� ��������
        /// </summary>
        public long EndSrvPacketID;
    }
}
