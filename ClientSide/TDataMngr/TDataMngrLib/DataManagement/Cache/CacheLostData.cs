// Project: TDataMngrLib, File: CacheLostData.cs
// Namespace: RCS.TDataMngrLib.DataManagement.Cache, Class: CacheLostData
// Path: D:\Development\TDataManager_Head\TDataMngrLib\DataManagement\Cache, Author: guschin
// Code lines: 187, Size of file: 6.37 KB
// Creation date: 8/14/2008 4:57 PM
// Last modified: 8/19/2008 4:49 PM
// Generated with Commenter by abi.exDream.com

#region Using directives
using System;
using System.Collections.Generic;
using System.Data;
#endregion

namespace RCS.TDataMngrLib.DataManagement.Cache
{
    internal class CacheLostData
    {
        /// <summary>
        /// �������� ���� MobitelID
        /// </summary>
        internal const string MOBITELID_FIELD_NAME = "MobitelID";

        /// <summary>
        /// �������� ���� BeginSrvPacketID
        /// </summary>
        internal const string BEGINSRVPACKETID_FIELD_NAME = "BeginSrvPacketID";

        /// <summary>
        /// �������� ���� EndSrvPacketID
        /// </summary>
        internal const string ENDSRVPACKETID_FIELD_NAME = "EndSrvPacketID";

        /// <summary>
        /// ������� c ������������ �����������
        /// </summary>
        private DataTable lostDataTable;

        internal CacheLostData()
        {
            lostDataTable = new DataTable();
            lostDataTable.Columns.AddRange(new DataColumn[]
            {
                new DataColumn(MOBITELID_FIELD_NAME, typeof (Int32)),
                new DataColumn(BEGINSRVPACKETID_FIELD_NAME, typeof (Int64)),
                new DataColumn(ENDSRVPACKETID_FIELD_NAME, typeof (Int64))
            });

            lostDataTable.PrimaryKey = new DataColumn[2]
            {
                lostDataTable.Columns[0], lostDataTable.Columns[1]
            };
        }

        /// <summary>
        /// �������� ������ � ��� ���������. ���� ����� ������ ������������
        /// � ����, �� ��� ����� �������, � ����� ��������� �����
        /// </summary>
        /// <param name="lostDataRange">��������� ��������� ��������</param>
        internal void AddRow(LostDataRange lostDataRange)
        {
            // ������ ������ �� ���������� �����, ����� �������
            internalRemove(lostDataRange.MobitelID, lostDataRange.BeginSrvPacketID);

            // ������ ����� �������� ����� ������
            lostDataTable.Rows.Add(new object[] {lostDataRange.MobitelID, lostDataRange.BeginSrvPacketID, lostDataRange.EndSrvPacketID});

            try
            {
                lostDataTable.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal int SizeTable()
        {
            return lostDataTable.Rows.Count;
        }

        internal void AcceptChanges()
        {
            lostDataTable.AcceptChanges();
        }

        internal void AddRow(int mobi, long beginsrv, long endsrv)
        {
            // ������ ������ �� ���������� �����, ����� �������
            internalRemove(mobi, beginsrv);

            // ������ ����� �������� ����� ������
            lostDataTable.Rows.Add(new object[] { mobi, beginsrv, endsrv });

            //try
            //{
            //    lostDataTable.AcceptChanges();
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }

        /// <summary>
        /// MAX({0})
        /// </summary>
        private const string MAX_EXPR = "MAX({0})";

        /// <summary>
        /// {0}={1}
        /// </summary>
        private const string EQUAL_EXPR = "{0}={1}";

        /// <summary>
        /// MOBITELID_FIELD_NAME + "={0} AND " + BEGINSRVPACKETID_FIELD_NAME + "={1}"
        /// </summary>
        private const string WHERE_MOBITELID_BEGINSRVPACKETID =
            MOBITELID_FIELD_NAME + "={0} AND " + BEGINSRVPACKETID_FIELD_NAME + "={1}";

        /// <summary>
        /// ����� ���������� ��������� ����������� ������ ��� ��������� ��������� 
        /// </summary>
        /// <param name="mobitelID">������������� ���������</param>
        /// <param name="lostDataRange">��������� ��������� ��������</param>
        /// <returns>true - ���� ���� ������ � ������� ���������</returns>
        internal bool GetLastRange(int mobitelID, out LostDataRange lostDataRange)
        {
            // ������� ������������ id ������� ���������, 
            // ��� ���� ����� �������� ��������� ������
            object maxIDObj = lostDataTable.Compute(
                String.Format(MAX_EXPR, BEGINSRVPACKETID_FIELD_NAME),
                String.Format(EQUAL_EXPR, MOBITELID_FIELD_NAME, mobitelID));

            if ((maxIDObj != null) && (maxIDObj.GetType() != typeof (DBNull)))
            {
                DataRow[] rowsResult = lostDataTable.Select(
                    String.Format(WHERE_MOBITELID_BEGINSRVPACKETID, mobitelID, maxIDObj));

                if (rowsResult.Length > 0)
                {
                    lostDataRange.MobitelID = Convert.ToInt32(rowsResult[0][MOBITELID_FIELD_NAME]);
                    lostDataRange.BeginSrvPacketID = Convert.ToInt64(rowsResult[0][BEGINSRVPACKETID_FIELD_NAME]);
                    lostDataRange.EndSrvPacketID = Convert.ToInt64(rowsResult[0][ENDSRVPACKETID_FIELD_NAME]);
                    return true;
                } // if (rowsForDelete.Length)
            } // if (maxIDObj)

            lostDataRange.MobitelID = -1;
            lostDataRange.BeginSrvPacketID = -1;
            lostDataRange.EndSrvPacketID = -1;
            return false;
        } // GetLastRange(mobitelID, lostDataRange)

        /// <summary>
        /// ������ ���� ��������� ���������
        /// </summary>
        /// <param name="mobitelID">Mobitel ID</param>
        /// <returns>List</returns>
        internal List<LostDataRange> GetLostRanges(int mobitelID)
        {
            List<LostDataRange> result = new List<LostDataRange>();

            DataRow[] rowsResult = lostDataTable.Select(
                String.Format(EQUAL_EXPR, MOBITELID_FIELD_NAME, mobitelID),
                BEGINSRVPACKETID_FIELD_NAME);

            foreach (DataRow row in rowsResult)
            {
                LostDataRange item = new LostDataRange();
                item.MobitelID = Convert.ToInt32(row[MOBITELID_FIELD_NAME]);
                item.BeginSrvPacketID = Convert.ToInt64(row[BEGINSRVPACKETID_FIELD_NAME]);
                item.EndSrvPacketID = Convert.ToInt64(row[ENDSRVPACKETID_FIELD_NAME]);

                result.Add(item);
            }

            return result;
        } // GetLostRanges(mobitelID)

        /// <summary>
        /// ������� ������
        /// </summary>
        /// <param name="mobitelID">������������� ���������</param>
        /// <param name="beginSrvPacketID">��������� �������� ��������� ��������</param>
        internal void RemoveRow(int mobitelID, long beginSrvPacketID)
        {
            internalRemove(mobitelID, beginSrvPacketID);
            lostDataTable.AcceptChanges();
        }

        /// <summary>
        /// �������� �����
        /// </summary>
        /// <param name="mobitelID">������������� ���������</param>
        /// <param name="beginSrvPacketID">��������� �������� ��������� ��������</param>
        private void internalRemove(int mobitelID, long beginSrvPacketID)
        {
            DataRow[] rowsForDelete = lostDataTable.Select(String.Format(WHERE_MOBITELID_BEGINSRVPACKETID, mobitelID, beginSrvPacketID));

            foreach (DataRow row in rowsForDelete)
                row.Delete();
        }

        /// <summary>
        /// ������� ���� ���������
        /// </summary>
        internal void Clear()
        {
            lostDataTable.Clear();
            lostDataTable.AcceptChanges();
        }

        /// <summary>
        /// ���������� ����� � ����
        /// </summary>
        internal int GetRowsCount()
        {
            return lostDataTable.Rows.Count;
        }
    }
}
