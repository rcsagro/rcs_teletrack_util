// Project: TDataMngrLib, File: CacheMobitelsCfg.cs
// Namespace: RCS.TDataMngrLib.DataManagement.Cache, Class: CacheMobitelsCfg
// Path: D:\Development\TDataManager_Head\TDataMngrLib\DataManagement\Cache, Author: guschin
// Code lines: 167, Size of file: 4.85 KB
// Creation date: 7/8/2008 4:42 PM
// Last modified: 8/8/2008 3:57 PM
// Generated with Commenter by abi.exDream.com

#region Using directives
using System;
using System.Collections.Generic;
#endregion

namespace RCS.TDataMngrLib.DataManagement.Cache
{
  /// <summary>
  /// ��� �������� ����������
  /// </summary>
  class CacheMobitelsCfg
  {
    /// ���������� ��� ������� ��� ����� �������� 
    /// ������������ ������

    /// <summary>
    /// �������� ������� � ����������� devIdShort - ���������
    /// </summary>
    private Dictionary<string, MobitelCfg> mobitelsCfgDevIdKey;
    /// <summary>
    /// ��������������� ������� ������ mobitelId - devIdShort
    /// </summary>
    private Dictionary<int, string> mobitelsCfgMobIdKey;

    internal CacheMobitelsCfg()
    {
      mobitelsCfgMobIdKey = new Dictionary<int, string>();
      mobitelsCfgDevIdKey = new Dictionary<string, MobitelCfg>();
    }

    /// <summary>
    /// ������ �������� ����������
    /// </summary>
    /// <returns>List</returns>
    internal List<MobitelCfg> GetMobitelsCfg()
    {
      List<MobitelCfg> result = new List<MobitelCfg>();

      foreach (KeyValuePair<string, MobitelCfg> row in mobitelsCfgDevIdKey)
      {
        result.Add(row.Value);
      } // foreach (row)

      return result;
    }

    /// <summary>
    /// ���������� ������. ���� ������ � �������� ������ ����������,
    /// �� ������ ������ ����� �������
    /// </summary>
    /// <param name="�onfig">��������� � ����������� ���������</param>
    internal void AddRow(MobitelCfg �onfig)
    {
      if (mobitelsCfgMobIdKey.ContainsKey(�onfig.MobitelID))
        mobitelsCfgMobIdKey.Remove(�onfig.MobitelID);

      if (mobitelsCfgDevIdKey.ContainsKey(�onfig.DevIdShort))
        mobitelsCfgDevIdKey.Remove(�onfig.DevIdShort);

      if (�onfig.DevIdShort != "")
      {
        mobitelsCfgMobIdKey.Add(�onfig.MobitelID, �onfig.DevIdShort);
        mobitelsCfgDevIdKey.Add(�onfig.DevIdShort, �onfig);
      } // if (�onfig.DevIdShort)
    }


    /// <summary>
    /// �������� ������
    /// </summary>
    /// <param name="mobitelID">Mobitel ID</param>
    internal void RemoveRow(int mobitelID)
    {
      if (mobitelsCfgMobIdKey.ContainsKey(mobitelID))
      {
        string devIdShort = mobitelsCfgMobIdKey[mobitelID];
        mobitelsCfgMobIdKey.Remove(mobitelID);
        mobitelsCfgDevIdKey.Remove(devIdShort);
      }
    }

    /// <summary>
    /// ������� ���� ��������
    /// </summary>
    internal void Clear()
    {
      mobitelsCfgMobIdKey.Clear();
      mobitelsCfgDevIdKey.Clear();
    }

    /// <summary>
    /// ��������: ��������������� �� ��������
    /// </summary>
    /// <param name="devIdShort">�������������� ������������� ���������</param>
    /// <returns>Bool</returns>
    internal bool TeletrackExist(string devIdShort)
    {
      return mobitelsCfgDevIdKey.ContainsKey(devIdShort);
    }

    /// <summary>
    /// ���������� �������� ������������� ��������� ���������
    /// ��� ������ mobitelID
    /// </summary>
    /// <param name="mobitelID">Mobitel ID</param>
    /// <param name="devIdShort">�������� ������������� ���������</param>
    /// <returns>true ���� ������</returns>
    internal bool TryGetDevIdShort(int mobitelID, out string devIdShort)
    {
      return mobitelsCfgMobIdKey.TryGetValue(mobitelID, out devIdShort);
    }

    /// <summary>
    /// ���������� mobitelID ��������� ��� ������ 
    /// �������� ������������� ���������
    /// </summary>
    /// <param name="devIdShort">�������� ������������� ���������</param>
    /// <param name="mobitelID">MobitelID</param>
    /// <returns>true ���� ������</returns>
    internal bool TryGetMobitelID(string devIdShort, out int mobitelID)
    {
      bool result = false;
      MobitelCfg �onfig;
      mobitelID = -1;
      if (mobitelsCfgDevIdKey.TryGetValue(devIdShort, out �onfig))
      {
        mobitelID = �onfig.MobitelID;
        result = true;
      }
      return result;
    }

  }
}
