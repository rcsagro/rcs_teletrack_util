#region Using directives
using System;
using System.Threading;
using RCS.Protocol.Online;
using RCS.SrvKernel.A1Handler;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand;
using RCS.SrvKernel.SrvSystem.Messages;
using RCS.TDataMngrLib.Communication;
using RCS.TDataMngrLib.DataManagement;
using RCS.TDataMngrLib.TSrvCommand;
#endregion

namespace RCS.TDataMngrLib.State
{
    /// <summary>
    /// ������ � ��������� ������ ����������� ������. 
    /// ������������ ����� ������ ������ = 1 ������, ����� ���������
    /// ������ � ���� ���������, ����� �������� �� ������ ����������.
    /// ���� � ������� ������ 30 ������ ���� ������� ����� 3-��
    /// �������������� ������(������, ������ �� �������), 
    /// �� ������ ������������ ��� ����� 30 ������.
    /// </summary>
    class StateReceiveOnlineGpsData : AbstractCtrlState
    {
        private DataManager dataManager;
        private volatile int lastProtocolPacketID;
        private volatile MsgHandler a1MsgHandler;

        /// <summary>
        /// ���������� �������������� ������� �������
        /// (������, ������ �� �������) � ������ �������
        /// </summary>
        private volatile byte efficientResponseCount;

        /// <summary>
        /// Create state receive gps data
        /// </summary>
        /// <param name="iMsgDispatcher">I message dispatcher</param>
        /// <param name="iCommunicator">Communicator</param>
        /// <param name="dataManager">Data manager</param>
        /// <param name="a1MsgHandler">MsgHandler</param>
        public StateReceiveOnlineGpsData(IMsgDispatcher iMsgDispatcher,
            ITCommunicator iCommunicator, DataManager dataManager, MsgHandler a1MsgHandler)
            : base(iMsgDispatcher, iCommunicator)
        {
            this.dataManager = dataManager;
            this.a1MsgHandler = a1MsgHandler;
        }

        /// <summary>
        /// ������������ ���������
        /// </summary>
        /// <returns>TController state</returns>
        protected override TControllerState GetStateName()
        {
            return TControllerState.ReceiveOnlineGpsData;
        }

        /// <summary>
        /// ���������������� �������� ����� �������� InnerRun
        /// </summary>
        protected override void PrepareRun()
        {
            base.PrepareRun();
            efficientResponseCount = 0;
        }

        /// <summary>
        /// ������ ������ �� ������ ������
        /// </summary>
        protected override void InnerRun()
        {
            // ������������ ����� ������ - 1 ������
            DateTime maxFinishWorkingTime = DateTime.Now.AddMinutes(1);
            // ������������� �������� �� ����������������� ������� ������� 30 ������
            DateTime milestoneTime = DateTime.Now.AddSeconds(30);

            // ���� �������� ����� 30-�� ������ ������������� ��������� ����������������
            // ������� �������(����� ���� ������� - �������� ������ � ���� ���������)
            while ((!shouldStop) && ((DateTime.Now < milestoneTime) ||
                                     ((DateTime.Now < maxFinishWorkingTime) && (efficientResponseCount >= 3))))
            {
                answerReceived = false;
                communicator.SendAckExtendCl(0, lastProtocolPacketID);
                waitEvent.WaitOne(TSettings.SrvAnswerTimeout, false);

                if (!answerReceived)
                {
                    // �� ������ ����� �� ����� ��������
                    WriteTimeoutError();
                }
            }
        }

        /// <summary>
        /// ����� ������� ����� ��������� �������� ������ = 5 ���.
        /// </summary>
        private const int SLEEP_TIME_IN_RECEIVE_STATE = 5 * 1000;

        /// <summary>
        /// ���������� ��������� �������
        /// </summary>
        /// <param name="parameters">���������� � ������</param>
        public override void HandleCommand(object parameters)
        {
            HandleCmdParamObject handleCmdParam = (HandleCmdParamObject) parameters;
            string packetTypeName = handleCmdParam.Packet.GetType().Name;

            answerReceived = true;

            if (packetTypeName == typeof (PacketInfo).Name)
            {
                // ������ ����� ������ ��������
                efficientResponseCount++;
                SaveNewDataGps(handleCmdParam);
            }
            else if (packetTypeName == typeof (PacketAnswCommand).Name)
            {
                // ����� ��������� �� �������
                efficientResponseCount++;
                HandleA1Answer(handleCmdParam);
            }
            else if (packetTypeName == typeof (PacketAckExSr).Name)
            {
                // ������ PacketAckExSr - ������ ������ ��� � ����� ���������
                Thread.Sleep(SLEEP_TIME_IN_RECEIVE_STATE);
            }
            else
            {
                WriteErrorCommand(packetTypeName);
                lastProtocolPacketID = 0;
            }

            if (waitEvent != null)
                waitEvent.Set();
        }

        /// <summary>
        /// ��������� ������ ��������� �� ������� �1
        /// </summary>
        /// <param name="handleCmdParam">HandleCmdParamObject</param>
        private void HandleA1Answer(HandleCmdParamObject handleCmdParam)
        {
            PacketAnswCommand packetAnswCommand = (PacketAnswCommand) handleCmdParam.Packet;

            a1MsgHandler.HandleIncomingMessage(packetAnswCommand.ComText,
                packetAnswCommand.Answer_code, packetAnswCommand.ID_CommandCl);

            lastProtocolPacketID = packetAnswCommand.MessageID;
        }

        /// <summary>
        /// ���������� ����� ������
        /// </summary>
        /// <param name="handleCmdParam">HandleCmdParamObject</param>
        private void SaveNewDataGps(HandleCmdParamObject handleCmdParam)
        {
            ISrvCommand command;
            PacketInfo packetInfo = (PacketInfo) handleCmdParam.Packet;

            // �������� ������� ������� � ����������� �� ���� ��������� �������
            // ��� ��������
            if (packetInfo.lstNewBin != null)
            {
                command = new InsertSetDataGpsCommand(
                    dataManager, handleCmdParam.ConnectionCtx, packetInfo.lstNewBin);
            }
            else
            {
                command = new InsertDataGpsCommand(
                    dataManager, handleCmdParam.ConnectionCtx, packetInfo);
            }

            try
            {
                command.Execute();
            }
            catch (Exception Ex)
            {
                ErrorHandler.Handle(Ex);
            }

            lastProtocolPacketID = packetInfo.MessageID;
        }
    }
}
