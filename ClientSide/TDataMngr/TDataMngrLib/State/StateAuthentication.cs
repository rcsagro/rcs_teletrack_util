using System;
using System.Threading;
using RCS.Protocol.Online;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvSystem.Messages;
using RCS.TDataMngrLib.Communication;
using RCS.TDataMngrLib.ErrorHandling;

namespace RCS.TDataMngrLib.State
{
  /// <summary>
  /// �������� ������������� �� �������
  /// </summary>
  class StateAuthentication : AbstractCtrlState
  {
    /// <summary>
    /// ��������� ID ������ �� ������� ������������ LastPacketID
    /// </summary>
    private long lastPacketID;
    /// <summary>
    /// ��������� ID ������ �� �������
    /// </summary>
    private long LastPacketID
    {
      get { return Interlocked.Read(ref lastPacketID); }
      set { Interlocked.Exchange(ref lastPacketID, value); }
    }

    /// <summary>
    /// ���������� �������� �� ��������������
    /// </summary>
    private volatile bool passed = false;

    public StateAuthentication(IMsgDispatcher iMsgDispatcher, ITCommunicator iCommunicator)
      : base(iMsgDispatcher, iCommunicator)
    { }

    /// <summary>
    /// ������������ ���������
    /// </summary>
    /// <returns>TController state</returns>
    protected override TControllerState GetStateName()
    {
      return TControllerState.Authentication;
    }

    /// <summary>
    /// ���������� ������.
    /// ���� �������� PacketAckExSr - ��� � ������� �������������� ��������.
    /// ���� ������ �������������� �� ������, �� ���������� ������ ����������
    /// </summary>
    /// <param name="parameter">���������� � ������</param>
    public override void HandleCommand(object parameters)
    {
      HandleCmdParamObject param = (HandleCmdParamObject)parameters;

      answerReceived = true;

      if (param.Packet.GetType().Name == typeof(PacketAckExSr).Name)
      {
        PacketAckExSr srvResponse = (PacketAckExSr)param.Packet;
        srvResponseCode = srvResponse.ConState;
        srvMessageID = srvResponse.MessageID;
        LastPacketID = srvResponse.IDLastPack;
        passed = true;
      }
      else
      {
        WriteErrorCommand(param.Packet.GetType().Name);
      }

      // ������ � ����������� ������ Authenticate() - ������ passed
      if (waitEvent != null)
      {
        waitEvent.Set();
      }
    }

    #region IAuthenticationState Members

    /// <summary>
    /// �������������� ���������. ����� ���������� ������ �� ������� {0}
    /// </summary>
    private const string AUTH_PROC_OK =
      "�������������� ���������. ����� ���������� ������ �� ������� {0}";

    /// <summary>
    /// ������ ��������������
    /// </summary>
    protected override void InnerRun()
    {
      const string start_auth_proc = "����� ������� ��������������...";
      Logger.Write(start_auth_proc);
      passed = false;

      if (!shouldStop)
      {
        answerReceived = false;

        communicator.SendAuthentication(TSettings.User, TSettings.Password);
        // �������� ������ �� ������� �������� ���-�� ������
        waitEvent.WaitOne(TSettings.SrvAnswerTimeout, false);

        if (!answerReceived)
        {
          WriteTimeoutError();
        }
      }

      if (passed)
      {
        Logger.Write(String.Format(AUTH_PROC_OK, LastPacketID));
      }
      else
      {
        ErrorHandler.Handle(new ApplicationException(TDMError.AUTHENTICATION_FAILED));
      }
    }

    protected override void PostDoneMessage()
    {
      AuthenticationResult msg = new AuthenticationResult();
      msg.IsAuthenticationPassed = passed;
      msg.LastPacketID = LastPacketID;
      PostDoneMessage(msg);
    }

    #endregion
  }
}
