using System;
using RCS.SrvKernel.SrvSystem.Messages;

namespace RCS.TDataMngrLib.State
{
  /// <summary>
  /// ��������� ������� - ������ �� ������
  /// </summary>
  class StateIdle : AbstractCtrlState
  {
    /// <summary>
    /// ������� = 1 ���
    /// </summary>
    private const int IDLE_TIMEOUT = 1000;

    public StateIdle(IMsgDispatcher iMsgDispatcher)
      : base(iMsgDispatcher, null)
    {
    }

    /// <summary>
    /// ���� �������� ������ � ������ ���������
    /// </summary>
    protected override void InnerRun()
    {
      while (!shouldStop)
        waitEvent.WaitOne(IDLE_TIMEOUT, false);
    }

    /// <summary>
    /// ������������ ���������
    /// </summary>
    /// <returns>TController state</returns>
    protected override TControllerState GetStateName()
    {
      return TControllerState.Idle;
    }

    /// <summary>
    /// ���������� ������
    /// </summary>
    /// <param name="parameter">���������� � ������</param>
    public override void HandleCommand(object parameters)
    {
      WriteErrorCommand(((HandleCmdParamObject)parameters).Packet.GetType().Name);
    }

  }
}
