#region Using directives
using System;
using System.Threading;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvSystem.Messages;
using RCS.TDataMngrLib.Communication;
using RCS.TDataMngrLib.ErrorHandling;
#endregion

namespace RCS.TDataMngrLib.State
{
    /// <summary>
    /// ������� ����� ��� ��������, ����������� ������ ������ 
    /// ����������� ���������
    /// </summary>
    class AbstractCtrlState : ICtrlState
    {
        /// <summary>
        /// ��������� ���������� ������� �������� ������
        /// </summary>
        protected volatile ITCommunicator communicator;

        /// <summary>
        /// ������� �� �������� ������
        /// </summary>
        protected volatile bool shouldStop;

        /// <summary>
        /// ���������� ��������� �� ������� ���������
        /// </summary>
        private volatile IMsgDispatcher msgDispatcher;

        /// <summary>
        /// ������� ������������� ����� �������� �������� ������ � ������ ������.
        /// ������������ ��� �������� ������ �� �������
        /// </summary>
        protected volatile EventWaitHandle waitEvent;

        /// <summary>
        /// ������� �� ����� �� �������
        /// </summary>
        protected volatile bool answerReceived;

        /// <summary>
        /// Create abstract ctrl state
        /// </summary>
        /// <param name="iMsgDispatcher">��������� ����������� ��������� �� ������� ���������</param>
        /// <param name="communicator">��������� ���������� ������� �������� ������</param>
        public AbstractCtrlState(IMsgDispatcher iMsgDispatcher, ITCommunicator iCommunicator)
        {
            msgDispatcher = iMsgDispatcher;
            communicator = iCommunicator;
            shouldStop = false;
            active = false;
        }

        /// <summary>
        /// �������� ��������� ����������� � ���������� ������ 
        /// </summary>
        protected void PostDoneMessage(object parameter)
        {
            MessageSpec msg = new MessageSpec();
            msg.msgType = MessageType.STATE_EXECUTING_DONE;
            msg.Sender = this;
            msg.Params = parameter;
            msgDispatcher.Post(msg);
        }

        /// <summary>
        /// ������������ ��������� � ���������� ������.
        /// ������������� ��������� ��� ������������ 
        /// ��������� � �����������
        /// </summary>
        protected virtual void PostDoneMessage()
        {
            PostDoneMessage(null);
        }

        /// <summary>
        /// ���������������� �������� ����� �������� InnerRun
        /// </summary>
        protected virtual void PrepareRun()
        {
            shouldStop = false;
            waitEvent = new AutoResetEvent(false);
        }

        /// <summary>
        /// ����������� �������� ����� ���������� ������ InnerRun
        /// </summary>
        protected virtual void FinalizeRun()
        {
            if (waitEvent != null)
            {
                waitEvent.Close();
                waitEvent = null;
            }
        }

        /// <summary>
        /// � ���� ������ ����������� ������ ������ ���������
        /// </summary>
        protected virtual void InnerRun()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "InnerRun", "AbstractCtrlState"));
        }

        #region ICtrlState Members

        /// <summary>
        /// ���������� �������� �� ������ ������ ���������
        /// </summary>
        private volatile bool active;

        /// <summary>
        /// ���������� �������� �� ������ ������ ���������
        /// </summary>
        public bool Active
        {
            get { return active; }
        }

        /// <summary>
        /// ��� ������ �������
        /// </summary>
        protected volatile int srvResponseCode;

        /// <summary>
        /// ��� ������ �������
        /// </summary>
        public int SrvResponseCode
        {
            get { return srvResponseCode; }
        }

        /// <summary>
        /// ID ������������� ������ ���������� �� �������
        /// </summary>
        protected volatile int srvMessageID;

        /// <summary>
        /// ID ������������� ������ ���������� �� �������
        /// </summary>
        public int SrvMessageID
        {
            get { return srvMessageID; }
        }

        /// <summary>
        /// ������������ ���������
        /// </summary>
        public TControllerState StateName
        {
            get { return GetStateName(); }
        }

        /// <summary>
        /// ������������ ���������
        /// </summary>
        /// <returns>TController state</returns>
        protected virtual TControllerState GetStateName()
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "GetStateName", "AbstractCtrlState"));
        }

        /// <summary>
        /// ���� �������� ������ � ������ ���������
        /// </summary>
        public void Run()
        {
            active = true;

            try
            {
                Logger.WriteDebug(String.Format(
                  "����� ��������� {0} �������", GetStateName()));

                PrepareRun();
                try
                {
                    InnerRun();
                }
                finally
                {
                    FinalizeRun();
                }
            }
            finally
            {
                PostDoneMessage();
                active = false;
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        /// <param name="parameter">���������� � ������</param>
        public virtual void HandleCommand(object parameters)
        {
            throw new NotImplementedException(String.Format(
                AppError.NOT_IMPLEMENTED, "HandleCommand", "AbstractCtrlState"));
        }

        /// <summary>
        /// ��������� ������ � ������ ���������
        /// </summary>
        public virtual void Stop()
        {
            shouldStop = true;
            // �������� ���������� ������
            if (waitEvent != null)
                waitEvent.Set();
        }

        #endregion


        /// <summary>
        /// ������ � ��� ��������� �� ��������� �������
        /// </summary>
        /// <param name="commandTypeName"></param>
        protected void WriteErrorCommand(string commandTypeName)
        {
            ErrorHandler.Handle(new ApplicationException(String.Format(
                TDMError.COMMAND_NOT_SUPPORTING_IN_STATE,
                commandTypeName, StateName)), ErrorLevel.WARNING);
        }

        /// <summary>
        /// ������ � ��� ��������� �� ��������� ������� �������� ������ �������
        /// </summary>
        protected void WriteTimeoutError()
        {
            // �� ������ ����� �� ����� ��������
            if (!shouldStop)
                ErrorHandler.Handle(new ApplicationException(String.Format(
                    TDMError.ANSWER_TIMEOUT, TSettings.SrvAnswerTimeout/1000, StateName)));
        }

        /// <summary>
        /// ������ � ��� �������� ���������� � �������� ������.
        /// �� ������ ������ ���������� ������ ���������� � ������ 
        /// ���������.
        /// </summary>
        /// <param name="loginInAnswer">����� ��������� � ������ �� ������</param>
        protected void WriteDebugSendQueryAnswer(string loginInAnswer)
        {
            Logger.WriteDebug(String.Format(
                "� ������ �� ������ �������, ��� ������ ������ ��������� {0}. ��������� {1}",
                loginInAnswer, StateName));
        }
    }
}
