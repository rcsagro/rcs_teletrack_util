using System.Collections.Generic;


namespace RCS.TDataMngrLib.State
{
  /// <summary>
  /// ������ ������ �������� ����������� (���������)
  /// </summary>
  enum TControllerState
  {
    /// <summary>
    /// ����� ��������, ��� ���������� ���������� 
    /// c �������� �/��� �� (��� �� �������� ��������������
    /// �� �������)
    /// </summary>
    Idle,
    /// <summary>
    /// ��������������
    /// </summary>
    Authentication,
    /// <summary>
    /// ������ ����������� ������  
    /// </summary>
    QueryLostRanges,
    /// <summary>
    /// ������ ��������� ������  
    /// </summary>
    QueryLastPackets,
    /// <summary>
    /// �������� ������ ����������
    /// </summary>
    SendCommands,
    /// <summary>
    /// ����� ����������� ������ 
    /// </summary>
    ReceiveOnlineGpsData
  } // enum WorkMode


  /// <summary>
  /// ��������� �� ������ �������� �������� ������� � 
  /// ������������ ��� ���������.
  /// 
  /// <para>NewState_\_CurState__|_Idle_|_Authentication_|_QueryLastPackets|_QueryLostRanges__|_SendCommands|_ReceiveOnlineGpsData
  /// <para>Idle_________________|__0___|_______1________|_______1_________|________1_________|______1______|______1_____________
  /// <para>Authentication_______|__1___|_______0________|_______0_________|________0_________|______0______|______0_____________
  /// <para>QueryLastPackets_____|__0___|_______1________|_______0_________|________0_________|______0______|______0_____________
  /// <para>QueryLostRanges______|__0___|_______0________|_______1_________|________0_________|______0______|______1_____________
  /// <para>SendCommands_________|__0___|_______0________|_______0_________|________1_________|______0______|______0_____________
  /// <para>ReceiveOnlineGpsData_|__0___|_______0________|_______0_________|________0_________|______1______|______0_____________
  /// </para></para></para></para></para></para></para>
  /// </summary>
  static class StateTransition
  {
    /// <summary>
    /// ���������� ��������� �� ������ � ����� ��������� ����� �������
    /// </summary>
    private static Dictionary<TControllerState, TControllerState> dictTransition;

    /// <summary>
    /// ������������� ����������� ���������
    /// </summary>
    private static void Init()
    {
      dictTransition = new Dictionary<TControllerState, TControllerState>();
      dictTransition.Add(TControllerState.Idle, TControllerState.Authentication);
      dictTransition.Add(TControllerState.Authentication, TControllerState.QueryLastPackets);
      dictTransition.Add(TControllerState.QueryLastPackets, TControllerState.QueryLostRanges);
      dictTransition.Add(TControllerState.QueryLostRanges, TControllerState.SendCommands);
      dictTransition.Add(TControllerState.SendCommands, TControllerState.ReceiveOnlineGpsData);
      dictTransition.Add(TControllerState.ReceiveOnlineGpsData, TControllerState.QueryLostRanges);
    }

    /// <summary>
    /// ����� ��������� ��������� ������� �� �������
    /// </summary>
    /// <param name="currentState">������� ���������</param>
    /// <returns>��������� ���������</returns>
    internal static TControllerState GetNextState(TControllerState currentState)
    {
      if (dictTransition == null)
        Init();

      return dictTransition[currentState];
    }

  }
}
