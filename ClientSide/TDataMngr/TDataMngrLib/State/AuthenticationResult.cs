
namespace RCS.TDataMngrLib.State
{
  /// <summary>
  /// ��������� ��������������
  /// </summary>
  struct AuthenticationResult
  {
    /// <summary>
    /// ��������� ID ������ �� �������
    /// </summary>
    public long LastPacketID;
    /// <summary>
    /// �������� �� ��������������
    /// </summary>
    public bool IsAuthenticationPassed;
  }
}
