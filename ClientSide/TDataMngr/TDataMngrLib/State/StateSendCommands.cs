using System;
using System.Collections.Generic;
using RCS.Protocol.A1.Entity;
using RCS.Protocol.Online;
using RCS.SrvKernel;
using RCS.SrvKernel.A1Handler;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.Logging;
using RCS.SrvKernel.SrvCommand;
using RCS.SrvKernel.SrvSystem.Messages;
using RCS.TDataMngrLib.Communication;
using RCS.TDataMngrLib.DataManagement;
using RCS.TDataMngrLib.ErrorHandling;

namespace RCS.TDataMngrLib.State
{
  /// <summary>
  /// ������ � ��������� �������� ������ A1
  /// </summary>
  class StateSendCommands : AbstractCtrlState
  {
    private DataManager dataManager;
    private MsgHandler a1MsgHandler;

    /// <summary>
    /// ���� �� ������ ��������� ����������� ���
    /// </summary>
    private volatile bool zoneConfigSetProcess;
    /// <summary>
    /// ������ �� ���������� ��������� ������������ �����
    /// ������������ ����������� ���
    /// </summary>
    private volatile bool isLastZoneConfigPacketAccepted;
    /// <summary>
    /// ������� ������������ ������� ������������ ����������� ��� 
    /// </summary>
    private volatile int zoneConfigPacketCounter;
    /// <summary>
    /// ��������� ������������ ������� �1
    /// </summary>
    private volatile OutgoingMsg lastSentMsg;
    /// <summary>
    /// ������ ������������� ��� lastSentMsg
    /// </summary>
    private static volatile object syncObject = new object();

    /// <summary>
    /// ������������ ����� ������ � ���� ��������� = 3 ���.
    /// </summary>
    private const byte MAX_WORKING_TIME = 3;

    /// <summary>
    /// Create 
    /// </summary>
    /// <param name="iMsgDispatcher">IMsgDispatcher</param>
    /// <param name="iCommunicator">Communicator</param>
    /// <param name="dataManager">Data manager</param>
    /// <param name="a1MsgHandler">MsgHandler</param>
    public StateSendCommands(IMsgDispatcher iMsgDispatcher,
      ITCommunicator iCommunicator, DataManager dataManager, MsgHandler a1MsgHandler)
      : base(iMsgDispatcher, iCommunicator)
    {
      this.dataManager = dataManager;
      this.a1MsgHandler = a1MsgHandler;
    }

    /// <summary>
    /// ������������ ���������
    /// </summary>
    /// <returns>TController state</returns>
    protected override TControllerState GetStateName()
    {
      return TControllerState.SendCommands;
    }

    /// <summary>
    /// ���� ���� ��������� ��������� �� ���� ����������
    /// </summary>
    /// <returns>������ OutgoingMsg</returns>
    private List<OutgoingMsg> CollectAllOutMessages()
    {
      List<OutgoingMsg> result = new List<OutgoingMsg>();
      // ���������� ������ ID ���������� ��� ������� ���������� �������.
      // ���� �������� ��� �� �������� � ��������� ���(����� �������� ��������,
      // ���� ������ ����� ��������, � ���������� �������� ��� �� ���������),
      // �� �������� ������ ����� ��������� �� ������������ �� ��� ��� ����
      // ��� ��������� �� ����� ��������� � ��������� ���.
      foreach (int mobitelId in dataManager.GetMobitelsInNewMessages())
      {
        if (shouldStop)
        {
          break;
        }

        string devIdShort;
        if (dataManager.TryGetDevIdShort(mobitelId, out devIdShort))
        {
          foreach (OutgoingMsg message in
            a1MsgHandler.GetOutgoingA1Messages(mobitelId, devIdShort))
          {
            if (shouldStop || SrvSettings.StopProcess)
            {
              break;
            }
            result.Add(message);
          }
        }
      }
      return result;
    }

    /// <summary>
    /// ������ ������ �� �������� ������-���������
    /// </summary>
    protected override void InnerRun()
    {
      if (IsNewCommandsExist())
      {
        // �����, ����� ������� ��������� ������
        DateTime milestoneTime = DateTime.Now.AddMinutes(MAX_WORKING_TIME);

        // ��������� ��� ��������
        foreach (OutgoingMsg msg in CollectAllOutMessages())
        {
          if (shouldStop)
            break;

          // �������� ����������� ������� ���������
          dataManager.FixRepeatedMessages(msg.Message.MobitelID,
            (int)msg.Message.CommandID, msg.Message.MessageID);

          // �������� Message ��� ����������� ������� ��� ������ ������
          lock (syncObject)
          {
            lastSentMsg = msg;
          }
          Logger.WriteDebug("A1. �������� ���������: " + msg.Message.GetDebugInfo());

          // �������� ������������ ����������� ����
          if (msg.Message.CommandID == CommandDescriptor.ZoneConfigSet)
          {
            zoneConfigSetProcess = true;
            SendZoneConfig(msg);
            zoneConfigSetProcess = false;
          }
          else
          {
            answerReceived = false;

            communicator.SendCommand(msg.Message.ShortID, msg.MessageId,
              msg.Message.Source);

            waitEvent.WaitOne(TSettings.SrvAnswerTimeout, false);

            if (!answerReceived)
            {
              WriteTimeoutError();
              break;
            }
          }

          if (milestoneTime < DateTime.Now)
          {
            // ����� ������ � ���� ��������� �����c��� ���������� ��������
            return;
          }
        }
      }
    }

    /// <summary>
    /// ���� �� ������� ��� ��������
    /// </summary>
    /// <returns>Bool</returns>
    private bool IsNewCommandsExist()
    {
      return dataManager.GetNewMessageCount() > 0 ? true : false;
    }

    /// <summary>
    /// �������� 25 ������� � ����������� ����������� ��� 
    /// </summary>
    /// <param name="msg">OutgoingMsg</param>
    private void SendZoneConfig(OutgoingMsg msg)
    {
      zoneConfigPacketCounter = 0;
      ZoneConfig zoneConfig = (ZoneConfig)msg.Message;
      foreach (byte[] zone in zoneConfig.Source)
      {
        if (shouldStop)
        {
          break;
        }

        isLastZoneConfigPacketAccepted = false;
        zoneConfigPacketCounter++;

        answerReceived = false;

        communicator.SendCommand(msg.Message.ShortID, msg.MessageId, zone);

        waitEvent.WaitOne(TSettings.SrvAnswerTimeout, false);

        if (!answerReceived)
        {
          WriteTimeoutError();
          break;
        }
        // ���� ��������� ������������ ����� �� ��������� ����������
        // ���������� ���������� ������ �� �������� ���
        else if (!answerReceived || !isLastZoneConfigPacketAccepted)
        {
          break;
        }
      }
    }

    /// <summary>
    /// ���������� ��������� �������
    /// </summary>
    /// <param name="parameters">���������� � ������</param>
    public override void HandleCommand(object parameters)
    {
      HandleCmdParamObject handleCmdParam = (HandleCmdParamObject)parameters;
      string packetTypeName = handleCmdParam.Packet.GetType().Name;
      isLastZoneConfigPacketAccepted = false;
      answerReceived = true;

      if (packetTypeName == typeof(PacketAckExSr).Name)
      {
        PacketAckExSr packetAckExSr = (PacketAckExSr)handleCmdParam.Packet;
        if (packetAckExSr.MessageID_In == communicator.LastSentCommand.PacketID)
        {
          lock (syncObject)
          {
            HandlePacketAckExSr(packetAckExSr);
          }
        } // if (packetAnswCommand.ID_CommandCl)
        else
        {
          // ��������� ���������������� - ���������� ���������� ������ 
          // �� �������� ������
          ErrorHandler.Handle(new ApplicationException(String.Format(
            TDMError.ANSWER_COMMAND_MESSAGEID,
            packetAckExSr.MessageID_In, communicator.LastSentCommand.PacketID)));

          shouldStop = true;
        }
      }
      else
      {
        WriteErrorCommand(packetTypeName);
      }

      if (waitEvent != null)
      {
        waitEvent.Set();
      }
    }

    /// <summary>
    /// ��������� ������ ������� �� ��������� ������� ��������� A1
    /// </summary>
    /// <param name="packetAckExSr"></param>
    private void HandlePacketAckExSr(PacketAckExSr packetAckExSr)
    {
      if (packetAckExSr.ConState >= 0)
      {
        if (!zoneConfigSetProcess)
        {
          // ������� ������� � ���� � ��� ��� ���������
          dataManager.SaveOutMessage(lastSentMsg.MessageId, lastSentMsg.Message.Message);
        }
        else
        {
          isLastZoneConfigPacketAccepted = true;
          // �������� ������������ ����������� ����
          // ����� �������� ���� ��������� - ��������� ��� ������� � ��
          if (zoneConfigPacketCounter == ZoneConfig.PACKET_COUNT)
          {
            ZoneConfig zoneCongig = (ZoneConfig)lastSentMsg.Message;
            dataManager.SaveOutMessage(zoneCongig.MessageID,
              zoneCongig.Message[zoneCongig.Message.Count - 1]);
          }
        }
      }
      else
      {
        ErrorHandler.Handle(new ApplicationException(String.Format(
          TDMError.A1_MESSAGE_NOT_SUBMITED, lastSentMsg.Message.CommandID,
          packetAckExSr.MessageID_In, lastSentMsg.MessageId)));
      }
    }
  }
}
