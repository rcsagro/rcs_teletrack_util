
namespace RCS.TDataMngrLib.State
{
  /// <summary>
  /// ����������� ������ ����������� 
  /// ��������� ��������� �����������
  /// </summary>
  interface ICtrlState
  {
    /// <summary>
    /// ���������� �������� �� ������ ������ ���������
    /// </summary>
    bool Active { get; }
    /// <summary>
    /// ��� ������ �������
    /// </summary>
    int SrvResponseCode { get; }
    /// <summary>
    /// ID ������������� ������ ���������� �� �������
    /// </summary>
    int SrvMessageID { get; }
    /// <summary>
    /// ������������ ���������
    /// </summary>
    TControllerState StateName { get; }
    /// <summary>
    /// ���� �������� ������ � ������ ���������
    /// </summary>
    void Run();
    /// <summary>
    /// ��������� ������ � ������ ���������
    /// </summary>
    void Stop();
    /// <summary>
    /// ���������� ������
    /// </summary>
    /// <param name="parameter">���������� � ������</param>
    void HandleCommand(object parameters);
  }
}
