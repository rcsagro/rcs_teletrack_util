#region Using directives
using System;
using System.Text;
using System.Threading;
using RCS.Protocol.Online;
using RCS.Sockets.Connection;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand;
using RCS.SrvKernel.SrvSystem.Messages;
using RCS.TDataMngrLib.Communication;
using RCS.TDataMngrLib.DataManagement;
using RCS.TDataMngrLib.ErrorHandling;
using RCS.TDataMngrLib.TSrvCommand;
#endregion

namespace RCS.TDataMngrLib.State
{
    /// <summary>
    /// ������ ��������� ������ �� ������������� SrvPacketID � �������
    /// ��������� �� ������������� PacketID �� �������.
    /// <para>� ��� ��������� ���������� ��������� ���� ���
    /// ����� ����� ��������������.</para>
    /// <para>����� �����, ����� ������ ������� �� ���������� �������.
    /// ���� ������ �� ����� �������� - ������ �����������. � ���� 
    /// ��������� ��� ����������� �� ����� ������, �.� �������� ���������
    /// 1000 ������� ������� ��������� �� ������� �������� �� 
    /// ����������� �����</para>
    /// </summary>
    class StateQueryLastPackets : AbstractCtrlState
    {
        private DataManager dataManager;

        private long lastSrvPacketID;

        /// <summary>
        /// ��������� ID ������ �� �������
        /// </summary>
        public long LastSrvPacketID
        {
            get { return Interlocked.Read(ref lastSrvPacketID); }
            set { Interlocked.Exchange(ref lastSrvPacketID, value); }
        }

        /// <summary>
        /// ������ �������������
        /// </summary>
        private static volatile object syncObject = new object();

        /// <summary>
        /// ������� ����������� �������� 
        /// </summary>
        private LostDataRange currentLostDataRange;

        /// <summary>
        /// ����� �������� ���������
        /// </summary>
        private string currentDevIdShort;

        /// <summary>
        /// Create state query last packets
        /// </summary>
        /// <param name="iMsgDispatcher">I message dispatcher</param>
        /// <param name="communicator">Communicator</param>
        /// <param name="dataManager">Data manager</param>
        /// <param name="lastSrvPacketID">������������� ���������� ������ �� �������</param>
        public StateQueryLastPackets(IMsgDispatcher iMsgDispatcher,
            ITCommunicator communicator, DataManager dataManager)
            : base(iMsgDispatcher, communicator)
        {
            this.dataManager = dataManager;
        }

        /// <summary>
        /// ������������ ���������
        /// </summary>
        /// <returns>TController state</returns>
        protected override TControllerState GetStateName()
        {
            return TControllerState.QueryLastPackets;
        }

        /// <summary>
        /// ������ ������ �� �������� ��������
        /// </summary>
        protected override void InnerRun()
        {
            // ������� ������ ��������� ������
            foreach (MobitelCfg row in dataManager.GetMobitelsCfg())
            {
                if (LastSrvPacketID > row.LastPacketID)
                {
                    if (shouldStop)
                    {
                        break;
                    }
                    lock (syncObject)
                    {
                        SendQuery(row);
                    }
                    waitEvent.WaitOne(TSettings.SrvAnswerTimeout, false);

                    if (!answerReceived)
                    {
                        WriteTimeoutError();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// �������� ������� �������
        /// </summary>
        /// <param name="mobitelCfgRow">������ ��������� ���������</param>
        private void SendQuery(MobitelCfg mobitelCfgRow)
        {
            answerReceived = false;

            currentLostDataRange.MobitelID = mobitelCfgRow.MobitelID;
            currentLostDataRange.BeginSrvPacketID = mobitelCfgRow.LastPacketID;
            currentLostDataRange.EndSrvPacketID = LastSrvPacketID;
            currentDevIdShort = mobitelCfgRow.DevIdShort;

            communicator.SendQuery(mobitelCfgRow.DevIdShort,
                mobitelCfgRow.LastPacketID, LastSrvPacketID);
        }

        /// <summary>
        /// ���������� ��������� �������
        /// </summary>
        /// <param name="parameters">���������� � ������</param>
        public override void HandleCommand(object parameters)
        {
            HandleCmdParamObject handleCmdParam = (HandleCmdParamObject) parameters;
            string packetTypeName = handleCmdParam.Packet.GetType().Name;

            answerReceived = true;

            if (packetTypeName == typeof (PacketResult).Name)
            {
                lock (syncObject)
                {
                    HandlePacketResult(
                        handleCmdParam.Packet as PacketResult,
                        handleCmdParam.ConnectionCtx);
                }
            }
            else
            {
                WriteErrorCommand(packetTypeName);
            }

            if (waitEvent != null)
            {
                waitEvent.Set();
            }
        }

        /// <summary>
        /// ��������� ������ � �������
        /// </summary>
        /// <param name="packet">PacketResult</param>
        /// <param name="connectionContext">ConnectionInfo</param>
        private void HandlePacketResult(PacketResult packet, ConnectionInfo connectionContext)
        {
            // � MessageID ������ ��������� ����� ���������
            string loginInPacket = Encoding.ASCII.GetString(
                BitConverter.GetBytes(packet.MessageID));

            WriteDebugSendQueryAnswer(loginInPacket);

            // �������� �� ����������� ������� ��������� � ������ � � �������
            if (loginInPacket == currentDevIdShort)
            {
                // ������ ����� ������ ��������
                if (packet.res_query.Count > 0)
                {
                    ISrvCommand command = new InsertQueryDataGpsCommand(
                        dataManager, connectionContext, packet.res_query,
                        currentLostDataRange.MobitelID, currentDevIdShort);
                    command.Execute();
                }
            }
            else
            {
                // ��������� ����������������
                // ���������� ������ (�������� ��������) � ���� ��������� �� ����� ������
                ErrorHandler.Handle(new ApplicationException(String.Format(
                    TDMError.QUERY_LAST_PACKETS_MISTIMING,
                    currentDevIdShort,
                    currentLostDataRange.BeginSrvPacketID,
                    currentLostDataRange.EndSrvPacketID,
                    loginInPacket)));

                shouldStop = true;
            }
        }
    }
}
