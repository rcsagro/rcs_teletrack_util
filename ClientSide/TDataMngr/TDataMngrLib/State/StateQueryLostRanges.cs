#region Using directives
using System;
using System.Collections.Generic;
using System.Text;
using RCS.Protocol.Online;
using RCS.Sockets.Connection;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand;
using RCS.SrvKernel.SrvCommand.Sending;
using RCS.SrvKernel.SrvSystem.Messages;
using RCS.TDataMngrLib.Communication;
using RCS.TDataMngrLib.DataManagement;
using RCS.TDataMngrLib.ErrorHandling;
using RCS.TDataMngrLib.TSrvCommand;
#endregion

namespace RCS.TDataMngrLib.State
{
    /// <summary>
    /// ������ � ��������� ���������� ���������
    /// ����� ��������� ������ ������ ������ ��������,
    /// ���� ������ ������ ������� ������ � ������� �� ����
    /// (�������� ��������� ����� ���������� ���������� ���� ������),
    /// � ���� ������ ������ ������� - ����� ������� ������ ���� � � ��
    /// (������ �� ������� ���)
    /// <para>����� �����, ����� ������ ������� �� ���������� �������.
    /// ���� ������ �� ����� �������� - ������ �����������. �����
    /// ���������� ����������� �� ������� ������ � ���� ���������,
    /// ��. ��������� MAX_WORKING_TIME</para>
    /// </summary>
    class StateQueryLostRanges : AbstractCtrlState
    {
        private DataManager dataManager;

        /// <summary>
        /// ������ �������������
        /// </summary>
        private static volatile object syncObject = new object();

        /// <summary>
        /// ������� ����������� �������� 
        /// </summary>
        private LostDataRange currentLostDataRange;

        /// <summary>
        /// ������� ����� ��������� �� �������� �������� ������ 
        /// </summary>
        private string currentDevIdShort;

        /// <summary>
        /// Create state query lost ranges
        /// </summary>
        /// <param name="iMsgDispatcher">I message dispatcher</param>
        /// <param name="iCommunicator">Communicator</param>
        /// <param name="dataManager">Data manager</param>
        /// <param name="lastSrvPacketID">Last srv packet ID</param>
        public StateQueryLostRanges(IMsgDispatcher iMsgDispatcher,
            ITCommunicator iCommunicator, DataManager dataManager)
            : base(iMsgDispatcher, iCommunicator)
        {
            this.dataManager = dataManager;
        }

        /// <summary>
        /// ������������ ���������
        /// </summary>
        /// <returns>TController state</returns>
        protected override TControllerState GetStateName()
        {
            return TControllerState.QueryLostRanges;
        }

        /// <summary>
        /// ������������ ����� ������ � ���� ��������� = 3 ���.
        /// </summary>
        private const byte MAX_WORKING_TIME = 6;

        /// <summary>
        /// ������ ������ �� �������� ��������
        /// </summary>
        protected override void InnerRun()
        {
            // �����, � ������� �������� ����� ��������� ��������� ����������
            DateTime maxIterationsTime = DateTime.Now.AddMinutes(1);

            while ((!shouldStop) && (DateTime.Now < maxIterationsTime) && (IsLostRangesExist()))
            {
                RunIteration();
            }
        }

        /// <summary>
        /// �������� ������� ���� ���������, ������������ � ����������.
        /// </summary>
        private void RunIteration()
        {
            // �����, ����� ������� ��������� ������
            DateTime milestoneTime = DateTime.Now.AddMinutes(MAX_WORKING_TIME);

            foreach (MobitelCfg row in dataManager.GetMobitelsCfg())
            {
                foreach (LostDataRange item in dataManager.GetLostRanges(row.MobitelID))
                {
                    if (shouldStop)
                    {
                        return;
                    }
                    lock (syncObject)
                    {
                        SendQuery(row.DevIdShort, item);
                    }
                    waitEvent.WaitOne(TSettings.SrvAnswerTimeout, false);

                    if (!answerReceived)
                    {
                        // �� ������ ����� �� ����� ��������
                        WriteTimeoutError();
                        return;
                    }
                    else if (milestoneTime < DateTime.Now)
                    {
                        // ����� ������ � ���� ��������� �����c��� ���������� ��������
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// �������� ������� �������
        /// </summary>
        /// <param name="devIdShort">������������� (�����) ���������</param>
        /// <param name="item">�������� ���������</param>
        private void SendQuery(string devIdShort, LostDataRange lostDataRange)
        {
            answerReceived = false;

            currentLostDataRange.MobitelID = lostDataRange.MobitelID;
            currentLostDataRange.BeginSrvPacketID = lostDataRange.BeginSrvPacketID;
            currentLostDataRange.EndSrvPacketID = lostDataRange.EndSrvPacketID;
            currentDevIdShort = devIdShort;
            // ������������ �������� ������ ������� ��������� �������� ���
            // �������� �� ������, �.� ������ ���������� ������� �� ����������
            // �������: ������� ������ > ����� ������� � <= ������ �������
            // ������ ��������� �������� ������������ � ����,
            // ������� ��� ������ ����������� ��� ������ ��� ���.
            // ��� �����, ��� ��� ������� ��������� ������ �� ������, ����������
            // �������� ������� ������� ���������� � ����������� ��������� �� �������
            // ��� ���. ���� �� �������������� �������, �� ��� ��������� ������ ��
            // ������� �������� ������� �� ����� ������.
            communicator.SendQuery(devIdShort, lostDataRange.BeginSrvPacketID, lostDataRange.EndSrvPacketID - 1);
        }

        /// <summary>
        /// ���������� �� �������� 
        /// </summary>
        /// <returns>True - ���� ��������� �������</returns>
        private bool IsLostRangesExist()
        {
            // ������� ������������� ������� ����� � ������� ������ ���������
            dataManager.ManualFlushDataGpsBuffer();
            return ((dataManager.FillLostData() == ExecutionStatus.OK) &&
                    (dataManager.GetLostRangesCount() > 0))
                ? true
                : false;
        }

        /// <summary>
        /// ���������� ��������� �������
        /// </summary>
        /// <param name="parameters">���������� � ������</param>
        public override void HandleCommand(object parameters)
        {
            HandleCmdParamObject handleCmdParam = (HandleCmdParamObject) parameters;
            string packetTypeName = handleCmdParam.Packet.GetType().Name;

            answerReceived = true;

            if (packetTypeName == typeof (PacketResult).Name)
            {
                lock (syncObject)
                {
                    HandlePacketResult(
                        handleCmdParam.Packet as PacketResult,
                        handleCmdParam.ConnectionCtx);
                }
            }
            else
            {
                WriteErrorCommand(packetTypeName);
            }

            if (waitEvent != null)
            {
                waitEvent.Set();
            }
        }

        /// <summary>
        /// ��������� ������ � �������
        /// </summary>
        /// <param name="packet">PacketResult</param>
        /// <param name="connectionContext">ConnectionInfo</param>
        private void HandlePacketResult(PacketResult packet, ConnectionInfo connectionContext)
        {
            // ����������� � ������������ �������� � ���������
            // ��������� ��������� ��������������� �������
            long minSrvPacketId, maxSrvPacketId;
            FindMinMaxSrvPacketID(packet, out minSrvPacketId, out maxSrvPacketId);

            // � MessageID ������ ��������� ����� ���������
            string loginInPacket = Encoding.ASCII.GetString(
                BitConverter.GetBytes(packet.MessageID));

            WriteDebugSendQueryAnswer(loginInPacket);

            // �������� �� ����������� ������� ��������� � ������ � � �������
            if (loginInPacket == currentDevIdShort)
            {
                if (packet.res_query.Count > 0)
                {
                    SaveTDataInDb(connectionContext, packet.res_query,
                        minSrvPacketId, maxSrvPacketId);
                }
                else
                {
                    // ������ �� ������� ���, ������ ����� ������� ����������
                    // � ����������� ��������� �� ����
                    try
                    {
                        dataManager.DeleteLostRegion(
                            currentLostDataRange.MobitelID, currentLostDataRange.BeginSrvPacketID);
                    } // try
                    catch (Exception Ex)
                    {
                        ErrorHandler.Handle(Ex);
                    }
                }

                // ������� �� ���������� ����
                dataManager.RemoveLostRangeInCache(
                    currentLostDataRange.MobitelID, currentLostDataRange.BeginSrvPacketID);
            }
            else
            {
                // ��������� ����������������
                // ���������� ������ (�������� ��������) � ���� ��������� �� ����� ������
                WriteMistimingError(loginInPacket, minSrvPacketId, maxSrvPacketId);
                shouldStop = true;
            }
        }

        /// <summary>
        /// ���������� ������ � ��
        /// </summary>
        /// <param name="connectionContext">Connection context</param>
        /// <param name="gpsData">Dictionary long, byte[]</param>
        /// <param name="minSrvPacketId">Minimum srv packet id</param>
        /// <param name="maxSrvPacketId">Maximum srv packet id</param>
        private void SaveTDataInDb(ConnectionInfo connectionContext,
            Dictionary<long, byte[]> gpsData, long minSrvPacketId, long maxSrvPacketId)
        {
            // �������� ����� �� ������ ������ ������������ ���������.
            // ���� ������ ������� �� ������� - ��������� ����������������
            if ((minSrvPacketId >= currentLostDataRange.BeginSrvPacketID) &&
                (maxSrvPacketId <= currentLostDataRange.EndSrvPacketID))
            {
                // ���������� ������
                ISrvCommand command = new InsertQueryDataGpsCommand(
                    dataManager, connectionContext, gpsData,
                    currentLostDataRange.MobitelID, currentDevIdShort);
                command.Execute();
            }
            else
            {
                // ��������� ����������������
                // ���������� ������ (�������� ��������) � ���� ��������� �� ����� ������
                WriteMistimingError(currentDevIdShort, minSrvPacketId, maxSrvPacketId);
                shouldStop = true;
            }
        }

        /// <summary>
        /// ����� ������������ � ������������� �������� � ���������
        /// ��������� ��������� ��������������� �������
        /// </summary>
        /// <param name="packet">����� �������</param>
        /// <param name="minSrvPacketId">����������� ��������</param>
        /// <param name="maxSrvPacketId">������������ ��������</param>
        private void FindMinMaxSrvPacketID(PacketResult packet, out long minSrvPacketId, out long maxSrvPacketId)
        {
            minSrvPacketId = 0;
            maxSrvPacketId = 0;

            if (packet.res_query.Count > 0)
            {
                SortedList<long, byte[]> tmp = new SortedList<long, byte[]>(packet.res_query);
                minSrvPacketId = tmp.Keys[0];
                maxSrvPacketId = tmp.Keys[tmp.Count - 1];
            }
        }

        /// <summary>
        /// ������ � ��� ������ ����������������
        /// </summary>
        /// <param name="loginInPacket">����� ��������� � ��������� ������</param>
        /// <param name="minSrvPacketId">����������� �������� SrvPacketId � ��������� ������</param>
        /// <param name="maxSrvPacketId">������������ �������� SrvPacketId � ��������� ������</param>
        private void WriteMistimingError(string loginInPacket, long minSrvPacketId, long maxSrvPacketId)
        {
            ErrorHandler.Handle(new ApplicationException(String.Format(
                TDMError.QUERY_LOST_PACKETS_MISTIMING,
                currentDevIdShort,
                currentLostDataRange.BeginSrvPacketID,
                currentLostDataRange.EndSrvPacketID,
                loginInPacket, minSrvPacketId, maxSrvPacketId)));
        }
    }
}
