#region Using directives
using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using RCS.SrvKernel;
using RCS.SrvKernel.Communication;
using RCS.SrvKernel.DataManagement;
using RCS.SrvKernel.ErrorHandling;
using RCS.SrvKernel.SrvCommand.Sending;
using RCS.SrvKernel.SrvSystem.Messages;
using RCS.TDataMngrLib.Communication;
using RCS.TDataMngrLib.Communication.Inspection;
using RCS.TDataMngrLib.DataManagement;
using RCS.TDataMngrLib.ErrorHandling;
using RCS.TDataMngrLib.State;
#endregion

namespace RCS.TDataMngrLib
{
    /// <summary>
    /// TMain controller
    /// </summary>
    public class TMainController : MainController, ITMainController
    {
        /// <summary>
        /// ������� ������������� ���������� � ��
        /// </summary>
        private SimpleEventDelegate eventDbNotAvailable;

        /// <summary>
        /// ������� ������������� ���������� � ��������
        /// </summary>
        private SimpleEventDelegate eventSrvNotAvailable;

        /// <summary>
        /// �������� ������
        /// </summary>
        private DataManager dataManager;

        /// <summary>
        /// ��������� ����������.
        /// </summary>
        private volatile IConnectionsObserver iConnectionsObserver;

        /// <summary>
        /// ����� ���������� ����������.
        /// </summary>
        private volatile Thread ThreadConnectionsObserver;

        /// <summary>
        /// ����� ��������� ������ � ������� ���������
        /// </summary>
        private volatile Thread ThreadStateHandler;

        private volatile object observerSyncObject;

        /// <summary>
        /// ������������ ������������� ������, ������� ���� 
        /// � ������ ������ �� �������. �� �������� �������� - 
        /// ������������ LastSrvPacketID
        /// </summary>
        private long lastSrvPacketID;

        /// <summary>
        /// ������������ ������������� ������, ������� ���� 
        /// � ������ ������ �� �������. ������������ � ��������
        /// ���������� ������, � �������� ��������� �����
        /// </summary>
        private long LastSrvPacketID
        {
            get { return Interlocked.Read(ref lastSrvPacketID); }
            set { Interlocked.Exchange(ref lastSrvPacketID, value); }
        }

        /// <summary>
        /// �������� ���������� �������� = 60 ���.
        /// </summary>
        private const int SETTINGS_REFRESH_INTERVAL = 60*60*1000;

        /// <summary>
        /// ������ ��� ���������� ��������
        /// </summary>
        private System.Timers.Timer timerRefreshSettings;

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="owner">�������� ��������� IMsgDispatcher</param>
        public TMainController(IMsgDispatcher owner)
            : base(owner)
        {
            dataManager = (DataManager) baseDataManager;

            observerSyncObject = new object();
            stateSyncObject = new object();

            timerRefreshSettings = new System.Timers.Timer(SETTINGS_REFRESH_INTERVAL);
            timerRefreshSettings.AutoReset = false;
            timerRefreshSettings.Elapsed += new ElapsedEventHandler(OnTimerRefreshSettingsElapsed);

            // �������� ������� ���������� ����������
            iConnectionsObserver = new ConnectionsObserver((IMethodsInspect) dataManager, this);

            // �������� �� ������� ������������� ��
            eventDbNotAvailable = new SimpleEventDelegate(OnDbNotAvailable);
            dataManager.eventDbNotAvailable += eventDbNotAvailable;
            // �������� �� ������� ������������� �������
            eventSrvNotAvailable = new SimpleEventDelegate(OnSrvNotAvailable);
            ((Communicator) communicator).eventSrvNotAvailable += eventSrvNotAvailable;

            InitStates();
        }

        /// <summary>
        /// Create data manager
        /// </summary>
        /// <returns>Base data manager</returns>
        protected override BaseDataManager CreateDataManager()
        {
            return new DataManager();
        }

        /// <summary>
        /// Create communicator
        /// </summary>
        /// <returns>Base communicator</returns>
        protected override BaseCommunicator CreateCommunicator()
        {
            return new Communicator(OnHandleCommand, this);
        }

        /// <summary>
        /// MainController
        /// </summary>
        private const string OBJECT_NAME = "MainController";

        /// <summary>
        /// ��� �������
        /// </summary>
        /// <returns>String</returns>
        protected override string GetName()
        {
            return OBJECT_NAME;
        }

        /// <summary>
        /// �������� ������������� � �������� ������
        /// </summary>
        protected override void InnerStarting()
        {
            SetState(TControllerState.Idle);

            LoadSrvConnectionSettings();
            timerRefreshSettings.Start();
            CreateObserverThread();
        }

        /// <summary>
        /// �������� �� �� �������� ����������� � ��������������� �������
        /// </summary>
        public void LoadSrvConnectionSettings()
        {
            SrvConnectionSettings cfg = GetSrvConnectionSettingsTemplate();
            if (dataManager.GetSrvConnectionCfg(ref cfg) == ExecutionStatus.OK)
            {
                TSettings.SetConnectionSrvSettings(cfg);
                // ��������� ��� ������������ ��� ������ ��� � ������� �����������
                RemoteInteractionLib.RepositOfStat.DispetchBoxName = cfg.User;
            }
            else
            {
                ErrorHandler.Handle(new ApplicationException(String.Format(
                    TDMError.SRV_CONNECT_CFG_NOT_LOADED, dataManager.ConnectionStringBase)));
            }
        }

        /// <summary>
        /// ��������� � ����������� SrvConnectionSettings
        /// </summary>
        /// <returns>SrvConnectionSettings</returns>
        private SrvConnectionSettings GetSrvConnectionSettingsTemplate()
        {
            SrvConnectionSettings result = new SrvConnectionSettings();
            result.Host = "";
            result.Port = 9010;
            result.User = "";
            result.Password = "";
            return result;
        }

        /// <summary>
        /// �������� � ������ ������ ���������� ����������
        /// </summary>
        private void CreateObserverThread()
        {
            if (State == ServiceState.Stop)
                return;

            lock (observerSyncObject)
            {
                // �������� ������ ��� ����������
                ThreadConnectionsObserver = new Thread(
                    new ThreadStart(iConnectionsObserver.Observe));
                ThreadConnectionsObserver.Name = "ConnectionsObserver";
                //ThreadConnectionsObserver.Priority = ThreadPriority.AboveNormal;
                //ThreadConnectionsObserver.IsBackground = true;
                ThreadConnectionsObserver.Start();
            }
        }

        /// <summary>
        /// ������� ����� ���������� ����������
        /// </summary>
        private void CloseConnectionObserver()
        {
            lock (observerSyncObject)
            {
                if (SrvSettings.DebugMode)
                    WriteToLog("  START CloseConnectionObserver");

                if ((ThreadConnectionsObserver != null) && (ThreadConnectionsObserver.IsAlive))
                {
                    if (iConnectionsObserver != null)
                    {
                        DateTime startTime = DateTime.Now;

                        iConnectionsObserver.StopObserve();
                        // ���� �� ����� 10 ���
                        while ((iConnectionsObserver.Active) &&
                               ((DateTime.Now - startTime).Seconds <= 10))
                        {
                            Thread.Sleep(100);
                        }
                    }

                    // ���� ��� �������� 5 ������ � ������������� ��������� �����
                    if (!ThreadConnectionsObserver.Join(5000))
                    {
                        try
                        {
                            ThreadConnectionsObserver.Abort();
                        }
                        catch
                        {
                        }
                    }
                    ThreadConnectionsObserver = null;
                }

                if (SrvSettings.DebugMode)
                {
                    WriteToLog("  FINISH CloseConnectionObserver");
                }
            }
        }

        /// <summary>
        /// ��������� �������. 
        /// ����� ������ ����� ������ ���������� �����
        /// �������� ��������� ����� ������
        /// </summary>
        protected override void Stop()
        {
            DisposeStateObjectThread();
            CloseConnectionObserver();

            base.Stop();
        }

        /// <summary>
        /// �������� ��� ��������� ������(� �������� ���������)
        /// </summary>
        protected override void InnerStopping()
        {
            if (timerRefreshSettings.Enabled)
                timerRefreshSettings.Stop();

            if (eventDbNotAvailable != null)
            {
                dataManager.eventDbNotAvailable -= eventDbNotAvailable;
                eventDbNotAvailable = null;
            }

            if (eventSrvNotAvailable != null)
            {
                ((Communicator) communicator).eventSrvNotAvailable -=
                    eventSrvNotAvailable;
                eventSrvNotAvailable = null;
            }

            // ����������� ����� ������ �� �������
            // � �������� ������� DataGPS ���� ���������������� 
            // �������� ����������
            if ((currentState != TControllerState.Idle) && (dataManager.TryToConnect()))
            {
                try
                {
                    dataManager.ManualFlushDataGpsBuffer();
                    dataManager.TransferBuffer();
                }
                catch (Exception Ex)
                {
                    HandleException(Ex);
                }
            }
        }

        #region ���������� � ��������

        /// <summary>
        /// ���������� � ��������
        /// </summary>
        private void ConnectToServer()
        {
            bool result = ((Communicator) communicator).Connect();

            if (iConnectionsObserver.Active)
            {
                iConnectionsObserver.SetSrvConnectionResult(result);
            }
        }

        #endregion

        #region C��������

        /// <summary>
        /// ��������� ���������
        /// </summary>
        /// <param name="message">Message</param>
        protected override void HandleMessage(MessageSpec message)
        {
            if (State == ServiceState.Stop)
            {
                return;
            }

            switch (message.msgType)
            {
                case MessageType.CONNECTIONS_AVAILABLE:
                    OnConnectionsAvailable();
                    break;
                case MessageType.PACKET_RECEIVED:
                    InnerHandleCommand(message.Params);
                    break;
                case MessageType.STATE_EXECUTING_DONE:
                    AnalyseStateExecutingResult((AbstractCtrlState) message.Sender, message.Params);
                    break;
                case MessageType.REFRESH_SETTINGS:
                    RefreshSettings();
                    break;
                case MessageType.CONNECT:
                    ConnectToServer();
                    break;
                default:
                    base.HandleMessage(message);
                    break;
            }
        }

        /// <summary>
        /// ���������� ��������
        /// </summary>
        private void OnConnectionsAvailable()
        {
            if (currentState == TControllerState.Idle)
            {
                state = ServiceState.Operate;
                CloseConnectionObserver();
                dictStates[currentState].Stop();
            }
            else
            {
                ErrorHandler.Handle(new ApplicationException(
                    TDMError.PHANTOM_CONNECTIONS_AVAILABLE), ErrorLevel.WARNING);
            }
        }

        #endregion

        #region ������������ ����� �����������

        private static readonly string MESSAGE_STATE = String.Concat(
            "\r\n                    ---- ", OBJECT_NAME, " ������� � ��������� {0} ----");

        private volatile object stateSyncObject;

        /// <summary>
        /// ������� ������-���������
        /// </summary>
        private volatile TControllerState currentState;

        /// <summary>
        /// ������� ���� �������� ���������
        /// </summary>
        private volatile Dictionary<TControllerState, ICtrlState> dictStates;

        /// <summary>
        /// �������� �������� ��������� � ���������� ������� dictStates
        /// </summary>
        private void InitStates()
        {
            dictStates = new Dictionary<TControllerState, ICtrlState>();
            dictStates.Add(TControllerState.Idle, new StateIdle(this));
            dictStates.Add(TControllerState.Authentication,
                new StateAuthentication(this, (ITCommunicator) communicator));
            dictStates.Add(TControllerState.QueryLastPackets,
                new StateQueryLastPackets(this, (ITCommunicator) communicator, dataManager));
            dictStates.Add(TControllerState.QueryLostRanges,
                new StateQueryLostRanges(this, (ITCommunicator) communicator, dataManager));
            dictStates.Add(TControllerState.SendCommands,
                new StateSendCommands(this, (ITCommunicator) communicator, dataManager, A1MsgHandler));
            dictStates.Add(TControllerState.ReceiveOnlineGpsData,
                new StateReceiveOnlineGpsData(this, (ITCommunicator) communicator, dataManager, A1MsgHandler));
        }

        private delegate void SetStateDelegate(TControllerState tState);

        /// <summary>
        /// ��������� �������� ��������� � ����������� ������
        /// </summary>
        /// <param name="tState">State</param>
        private void SetStateAsync(TControllerState tState)
        {
            new SetStateDelegate(SetState).BeginInvoke(tState, null, null);
        }

        /// <summary>
        /// ��������� �������� ���������
        /// </summary>
        /// <param name="tState">State</param>
        private void SetState(TControllerState tState)
        {
            lock (stateSyncObject)
            {
                DisposeStateObjectThread();
                currentState = tState;

                if (currentState == TControllerState.QueryLastPackets)
                {
                    ((StateQueryLastPackets) dictStates[currentState]).LastSrvPacketID = LastSrvPacketID;
                }

                WriteToLog(String.Format(MESSAGE_STATE, tState));

                ThreadStateHandler = new Thread(new ThreadStart(dictStates[currentState].Run));
                ThreadStateHandler.Name = String.Concat("State", currentState);
                ThreadStateHandler.Start();
            }
        }

        /// <summary>
        /// ����������� ������, � ������� ������� ������-��������� 
        /// </summary>
        private void DisposeStateObjectThread()
        {
            lock (stateSyncObject)
            {
                if (State == ServiceState.Start)
                {
                    return;
                }
                if (SrvSettings.DebugMode)
                {
                    WriteToLog("  START DisposeStateObjectThread " + currentState);
                }
                if (dictStates[currentState] != null)
                {
                    dictStates[currentState].Stop();
                }
                if (ThreadStateHandler != null)
                {
                    // �������������� ������ - ������������ ����� ��������
                    // ���������� ������ = 5 ���. ���� ����� �� �������� ������
                    // �� ���������� ����� - ������������� ����������.
                    //ThreadStateHandler.Join(300000);
                    ThreadStateHandler.Join(600000);
                    if (ThreadStateHandler.IsAlive)
                    {
                        try
                        {
                            ThreadStateHandler.Abort();
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler.Handle(new ApplicationException(String.Format(
                                TDMError.Thread_ABORT_EXCEPTION, ThreadStateHandler.Name, ex.Message)),
                                ErrorLevel.WARNING);
                        }
                    }

                    ThreadStateHandler = null;
                }

                if (SrvSettings.DebugMode)
                {
                    WriteToLog("  FINISH DisposeStateObjectThread " + currentState);
                }
            }
        }

        private static readonly string MESSAGE_STATE_DONE = String.Concat(
            "\r\n                    ---- ", OBJECT_NAME, " �������� ������ � ��������� {0} ----");

        /// <summary>
        /// ������ ���������� ������ ���������
        /// </summary>
        /// <param name="senderState">������-���������, ����������� ������ � ����������� ���������</param>
        /// <param name="parameter">�������������� �������� ���������� ���������� ����� ����������������</param>
        private void AnalyseStateExecutingResult(AbstractCtrlState senderState, object parameter)
        {
            WriteToLog(String.Format(MESSAGE_STATE_DONE, senderState.StateName));

            if ((State == ServiceState.Stop) || (State == ServiceState.ConnectionWaiting))
            {
                return;
            }
            if (senderState.StateName == TControllerState.Authentication)
            {
                // ��������� ����������� ��������������
                HandleAuthenticationResult((AuthenticationResult) parameter);
            }

            if (!dataManager.TryToConnect())
            {
                OnDbNotAvailable();
            }
            else if ((State != ServiceState.Stop) && (State != ServiceState.ConnectionWaiting))
            {
                // ������� � ��������� ���������, ���� �������� ���������� � ���������
                SetState(StateTransition.GetNextState(senderState.StateName));
            }
        }

        /// <summary>
        /// ������ ����������� ��������������
        /// </summary>
        private void HandleAuthenticationResult(AuthenticationResult result)
        {
            if (result.IsAuthenticationPassed)
            {
                LastSrvPacketID = result.LastPacketID;
                // ����� ������ � �� (TransferBuffer)
                dataManager.TransferBuffer();
                dataManager.TransferBuffer64();
                // ���������� ���������� ����
                if (!dataManager.InitLocalCache())
                {
                    OnDbNotAvailable();
                }
            }
            else
            {
                OnSrvNotAvailable(false);
            }
        }

        #endregion

        #region ���������� ��������

        /// <summary>
        /// ���������� ������� ������������ ������� ���������� ��������.
        /// �������� ��������� �� ���������� �������� � �������.
        /// </summary>
        /// <param name="sender">Object</param>
        /// <param name="e">ElapsedEventArgs</param>
        private void OnTimerRefreshSettingsElapsed(Object sender, ElapsedEventArgs e)
        {
            try
            {
                MessageSpec msg = new MessageSpec();
                msg.msgType = MessageType.REFRESH_SETTINGS;
                Post(msg);
            }
            finally
            {
                timerRefreshSettings.Start();
            }
        }

        /// <summary>
        /// ���������� �������� �� ���� ������
        /// </summary>
        private const string REFRESH_SETTINGS_MSG = "���������� �������� �� ���� ������";

        /// <summary>
        /// ���������� �������� ���������� � ��.
        /// ��������� ����������� � ������� �  �������� ����������
        /// ����������� ������ ��� �������� ���� ������ �����������, 
        /// �.�. ��� ���������� ���������� ��� ��������� �� �����.
        /// </summary>
        private void RefreshSettings()
        {
            if ((currentState != TControllerState.Idle) &&
                (currentState != TControllerState.Authentication))
            {
                WriteToLog(REFRESH_SETTINGS_MSG);

                LoadSrvConnectionSettings();
                dataManager.InitLocalCache();
            }
        }

        #endregion

        #region ��������� ��������� �������

        /// <summary>
        /// ��������� �������
        /// </summary>
        /// <param name="parameter">Parameter</param>
        protected override void InnerHandleCommand(object parameter)
        {
            if (SrvSettings.DebugMode)
            {
                WriteToLog(String.Format(
                    "MainController ������� ����� � �������� {0}. ������� ���������: {1}",
                    ((HandleCmdParamObject) parameter).Packet.GetType().Name, currentState));
            }
            if ((State != ServiceState.Stop) && (dictStates[currentState] != null))
            {
                dictStates[currentState].HandleCommand(parameter);
            }
        }

        #endregion

        #region ������� ������������� ��������

        /// <summary>
        /// ��������� ������� ������������� ��
        /// </summary>
        private void OnDbNotAvailable()
        {
            state = ServiceState.ConnectionWaiting;

            // ������� ���������� � ��������
            communicator.Stop();

            SetState(TControllerState.Idle);

            if ((ThreadConnectionsObserver == null) ||
                (!ThreadConnectionsObserver.IsAlive))
            {
                ErrorHandler.Handle(new ApplicationException(
                    TDMError.CONNECT_DB_NOT_AVAILABLE), ErrorLevel.WARNING);

                CreateObserverThread();
            }
        }

        /// <summary>
        /// ��������� ������� ������������� �������
        /// </summary>
        private void OnSrvNotAvailable()
        {
            OnSrvNotAvailable(true);
        }

        /// <summary>
        /// ��������� ������� ������������� �������
        /// </summary>
        /// <param name="writeMessage">������� �� ���������� � ��� ��������� 
        /// � ������������� �������</param>
        private void OnSrvNotAvailable(bool writeMessage)
        {
            state = ServiceState.ConnectionWaiting;

            if (ThreadConnectionsObserver != null)
            {
                CloseConnectionObserver();
            }

            SetStateAsync(TControllerState.Idle);

            if (writeMessage)
            {
                ErrorHandler.Handle(new ApplicationException(
                    TDMError.CONNECT_SRV_NOT_AVAILABLE), ErrorLevel.WARNING);
            }

            communicator.Stop();

            // ����� ������� ���������...
            Thread.Sleep(10000);
            CreateObserverThread();
        }

        #endregion
    }
}
