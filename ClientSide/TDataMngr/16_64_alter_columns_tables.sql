﻿IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "datagps64")
	THEN
	ALTER TABLE datagps64 MODIFY DGPS varchar(32);

	IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "datagpsbuffer_on64")
	THEN
	ALTER TABLE datagpsbuffer_on64 MODIFY DGPS varchar(32);

	IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "online64")
	THEN
	ALTER TABLE online64 MODIFY DGPS varchar(32);



