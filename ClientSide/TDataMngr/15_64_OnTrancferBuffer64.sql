﻿CREATE PROCEDURE vesnyane_agro.OnTransferBuffer64()
  SQL SECURITY INVOKER
  COMMENT 'Переносит GPS данные из буферной таблицы в datagps64'
BEGIN
  DECLARE MobitelIDInCursor int; /* Текущее значение MobitelID */
  DECLARE DataExists tinyint DEFAULT 1; /* Флаг наличия данных после фетча */
  DECLARE NeedOptimize tinyint DEFAULT 0; /* Нужна оптимизация или нет */
  DECLARE NeedOptimizeParam tinyint;

  DECLARE TmpMaxUnixTime int; /* Временная переменная для хранения Max UnixTime из таблицы online */

  /* Курсор по телетрекам в множестве новых данных */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
  SELECT DISTINCT
    (Mobitel_ID)
  FROM datagpsbuffer_on64;
  /* Курсор по телетрекам в множестве повторных данных */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
  SELECT DISTINCT
    (Mobitel_ID)
  FROM datagpsbuffer_ontmp64;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  CALL delDupSrvPacket();
  CALL OnDeleteDuplicates64();
  /*CALL OnCorrectInfotrackLogId64();*/

  /* Сохраняем записи, которые уже есть в БД, для второго этапа анализа пропусков 
     Используется процедурой OnLostDataGPS264. ?????*/
  INSERT IGNORE INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID)
    SELECT
      b.Mobitel_ID,
      b.LogID,
      b.SrvPacketID
    FROM datagpsbuffer_on64 b
      LEFT JOIN datagps64 d
        ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL;

  /* -------------------- Заполнение таблицы Online ---------------------- */
  /* Удаление из таблицы online дублирующих строк таблицы datagpsbuffer_on64 */
  /* по полям Mobitel_ID и LogID */
  DELETE o
    FROM online64 o, datagpsbuffer_on64 dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);

  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* Нахождение Max значения UnixTime в таблице online для заданного
       телетрека. p.s. UnixTime в таблице online только валидный */
    SELECT
      COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online64 FORCE INDEX (IDX_Mobitelid64)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* Вставка новых записей (максимум 100)каждого телетрека в online  ????*/
    INSERT IGNORE INTO online64 (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, `Events`,
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS)
      SELECT
        Mobitel_ID,
        LogID,
        UnixTime,
        Latitude,
        Longitude,
        Acceleration,
        Direction,
        Speed,
        Valid,
        `Events`,
        Satellites,
        RssiGsm,
        SensorsSet,
        Sensors,
        Voltage,
        DGPS
      FROM (SELECT
          *
        FROM datagpsbuffer_on64
        WHERE (Mobitel_ID = MobitelIDInCursor) AND
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
        GROUP BY Mobitel_ID,
                 LogID
        ORDER BY UnixTime DESC
        LIMIT 100) AS T
      ORDER BY UnixTime ASC;

    /* Удаление строк из таблицы оперативных данных не удовлетворяющие
       условиям хранения - не являются последними 100 строками */
    DELETE
      FROM online64
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < (SELECT
          COALESCE(MIN(UnixTime), 0)
        FROM (SELECT
            UnixTime
          FROM online64
          WHERE Mobitel_ID = MobitelIDInCursor
          ORDER BY UnixTime DESC
          LIMIT 100) T1));

    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */
  /* Зачем-то нужен апдейт ... */
  UPDATE datagps64 d, datagpsbuffer_on64 b
  SET d.Latitude = b.Latitude,
      d.Longitude = b.Longitude,
      d.Acceleration = b.Acceleration,
      d.UnixTime = b.UnixTime,
      d.Speed = b.Speed,
      d.Direction = b.Direction,
      d.Valid = b.Valid,
      d.LogID = b.LogID,
      d.`Events` = b.`Events`,
      d.Satellites = b.Satellites,
      d.RssiGsm = b.RssiGsm,
      d.SensorsSet = b.SensorsSet,
      d.Sensors = b.Sensors,
      d.Voltage = b.Voltage,
      d.DGPS = b.DGPS,
      d.SrvPacketID = b.SrvPacketID
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  AND (d.SrvPacketID < b.SrvPacketID);

  /* Удалим записи которые участвовали в апдейте */
  DELETE b
    FROM datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

    /* Вставка новых записей ????*/ 
  INSERT IGNORE INTO datagps64 (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
  Direction, Speed, Valid, `Events`,
  Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID)
    SELECT
      Mobitel_ID,
      LogID,
      UnixTime,
      Latitude,
      Longitude,
      Acceleration,
      Direction,
      Speed,
      Valid,
      `Events`,
      Satellites,
      RssiGsm,
      SensorsSet,
      Sensors,
      Voltage,
      DGPS,
      SrvPacketID
    FROM datagpsbuffer_on64
    GROUP BY Mobitel_ID,
             LogID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ------ Обновление времени последней вставки записей ------- */
    UPDATE mobitels
    SET LastInsertTime = UNIX_TIMESTAMP(NOW())
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ------ Обновление списка пропущенных записей -------------- */
    CALL OnLostDataGPS64(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;

  /* Очистка буфера */
  TRUNCATE TABLE datagpsbuffer_on64;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ---- Обновление списка пропущенных записей по второму алгоритму --- */
    CALL OnLostDataGPS264(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsUpdateSet;

  /* Очистка вспомогательного буфера */
  TRUNCATE TABLE datagpsbuffer_ontmp64;

  /* Были изменения - сделаем оптимизацию */
  IF NeedOptimize = 1 THEN
    OPTIMIZE TABLE datagpslost_on;
  END IF;
END