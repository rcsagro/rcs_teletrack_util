﻿namespace TeletrackTuning
{
  partial class FormBase
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBase));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ButtonDBSetting = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.ComboBoxInterfaceSettings = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ButtonConnect = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.ButtonDisconnect = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.checkBoxPing = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxID_TT = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonSetFromFile = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxBaudrate = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxComPort = new System.Windows.Forms.ComboBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.tabControlSettings = new System.Windows.Forms.TabControl();
            this.tabPageEvents = new System.Windows.Forms.TabPage();
            this.dataGridViewEvent = new System.Windows.Forms.DataGridView();
            this.ColumnEventsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEventsValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEventsUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEventsCheckWritePoint = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnEventsCheckSendToServer = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnEventsCheckSendSMS = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnEventsRecomended = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnEventsMaxValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageGPRS = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.comboBoxGsmOperator = new System.Windows.Forms.ComboBox();
            this.buttonAddGsmOperator = new System.Windows.Forms.Button();
            this.buttonApplyGsmOperator = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewGprsSetting = new System.Windows.Forms.DataGridView();
            this.ColumnGprsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnGprsValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageServer = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dataGVServiceSetting = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonSaveServiceSetting = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridViewServer = new System.Windows.Forms.DataGridView();
            this.ColumnServerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnServerValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonGeneratePass = new System.Windows.Forms.Button();
            this.tabPagePhones = new System.Windows.Forms.TabPage();
            this.buttonCopyPhone = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.buttonAddNewPhone = new System.Windows.Forms.Button();
            this.dataGVPhonesInDataBase = new System.Windows.Forms.DataGridView();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.buttonSetTtPhone = new System.Windows.Forms.Button();
            this.textBoxTtPhone = new System.Windows.Forms.TextBox();
            this.dataGridViewPhones = new System.Windows.Forms.DataGridView();
            this.ColumnPhoneName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPhoneValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageTextCmd = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxReadText = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxSendText = new System.Windows.Forms.TextBox();
            this.tabPageGPS = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.splitContainer8 = new System.Windows.Forms.SplitContainer();
            this.buttonReadParameter = new System.Windows.Forms.Button();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.buttonWriteParameter = new System.Windows.Forms.Button();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dataGridViewTeletracks = new System.Windows.Forms.DataGridView();
            this.ColumnNameTT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDescriptionTT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnShortIDTT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonEditTt = new System.Windows.Forms.Button();
            this.buttonDeleteTt = new System.Windows.Forms.Button();
            this.buttonAddNewTt = new System.Windows.Forms.Button();
            this.timerThreadRx = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.toolStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.tabControlSettings.SuspendLayout();
            this.tabPageEvents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvent)).BeginInit();
            this.tabPageGPRS.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGprsSetting)).BeginInit();
            this.tabPageServer.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVServiceSetting)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewServer)).BeginInit();
            this.tabPagePhones.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVPhonesInDataBase)).BeginInit();
            this.panel4.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPhones)).BeginInit();
            this.tabPageTextCmd.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPageGPS.SuspendLayout();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.splitContainer7.Panel1.SuspendLayout();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            this.splitContainer8.Panel1.SuspendLayout();
            this.splitContainer8.SuspendLayout();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTeletracks)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 555);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(978, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonOpen,
            this.toolStripSeparator2,
            this.toolStripButtonSave,
            this.toolStripSeparator1,
            this.ButtonDBSetting,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.toolStripSeparator4,
            this.toolStripLabel1,
            this.ComboBoxInterfaceSettings,
            this.toolStripSeparator5,
            this.ButtonConnect,
            this.toolStripSeparator6,
            this.ButtonDisconnect,
            this.toolStripSeparator7});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(978, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonOpen
            // 
            this.toolStripButtonOpen.Image = global::TeletrackTuning.Properties.Resources.Folder_Open_icon;
            this.toolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpen.Name = "toolStripButtonOpen";
            this.toolStripButtonOpen.Size = new System.Drawing.Size(56, 22);
            this.toolStripButtonOpen.Text = "Open";
            this.toolStripButtonOpen.Click += new System.EventHandler(this.toolStripButtonOpen_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonSave
            // 
            this.toolStripButtonSave.Image = global::TeletrackTuning.Properties.Resources.Save_icon;
            this.toolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSave.Name = "toolStripButtonSave";
            this.toolStripButtonSave.Size = new System.Drawing.Size(51, 22);
            this.toolStripButtonSave.Text = "Save";
            this.toolStripButtonSave.Click += new System.EventHandler(this.toolStripButtonSave_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ButtonDBSetting
            // 
            this.ButtonDBSetting.Image = global::TeletrackTuning.Properties.Resources.database_gear_icon;
            this.ButtonDBSetting.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonDBSetting.Name = "ButtonDBSetting";
            this.ButtonDBSetting.Size = new System.Drawing.Size(82, 22);
            this.ButtonDBSetting.Text = "DB Setting";
            this.ButtonDBSetting.ToolTipText = "DadaBase Setting";
            this.ButtonDBSetting.Click += new System.EventHandler(this.ButtonDBSetting_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(73, 22);
            this.toolStripLabel2.Text = "                      ";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(100, 22);
            this.toolStripLabel1.Text = "Interface settings:";
            // 
            // ComboBoxInterfaceSettings
            // 
            this.ComboBoxInterfaceSettings.Items.AddRange(new object[] {
            "Dicect Cable",
            "Socket",
            "SMS"});
            this.ComboBoxInterfaceSettings.Name = "ComboBoxInterfaceSettings";
            this.ComboBoxInterfaceSettings.Size = new System.Drawing.Size(150, 25);
            this.ComboBoxInterfaceSettings.Text = "Direct Cable";
            this.ComboBoxInterfaceSettings.SelectedIndexChanged += new System.EventHandler(this.ComboBoxInterfaceSettings_SelectedIndexChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // ButtonConnect
            // 
            this.ButtonConnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ButtonConnect.Image = ((System.Drawing.Image)(resources.GetObject("ButtonConnect.Image")));
            this.ButtonConnect.ImageTransparentColor = System.Drawing.Color.SpringGreen;
            this.ButtonConnect.Name = "ButtonConnect";
            this.ButtonConnect.Size = new System.Drawing.Size(56, 22);
            this.ButtonConnect.Text = "Connect";
            this.ButtonConnect.ToolTipText = "Connect ";
            this.ButtonConnect.Click += new System.EventHandler(this.ButtonConnect_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // ButtonDisconnect
            // 
            this.ButtonDisconnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ButtonDisconnect.Image = ((System.Drawing.Image)(resources.GetObject("ButtonDisconnect.Image")));
            this.ButtonDisconnect.ImageTransparentColor = System.Drawing.Color.Plum;
            this.ButtonDisconnect.Name = "ButtonDisconnect";
            this.ButtonDisconnect.Size = new System.Drawing.Size(70, 22);
            this.ButtonDisconnect.Text = "Disconnect";
            this.ButtonDisconnect.Click += new System.EventHandler(this.ButtonDisconnect_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.checkBoxPing);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(978, 530);
            this.splitContainer1.TabIndex = 3;
            // 
            // checkBoxPing
            // 
            this.checkBoxPing.AutoSize = true;
            this.checkBoxPing.Checked = true;
            this.checkBoxPing.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPing.Location = new System.Drawing.Point(462, 20);
            this.checkBoxPing.Name = "checkBoxPing";
            this.checkBoxPing.Size = new System.Drawing.Size(47, 17);
            this.checkBoxPing.TabIndex = 5;
            this.checkBoxPing.Text = "Ping";
            this.checkBoxPing.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.textBoxID_TT);
            this.groupBox2.Location = new System.Drawing.Point(314, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(142, 50);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "TT_ID:";
            // 
            // textBoxID_TT
            // 
            this.textBoxID_TT.Location = new System.Drawing.Point(53, 17);
            this.textBoxID_TT.Name = "textBoxID_TT";
            this.textBoxID_TT.Size = new System.Drawing.Size(76, 20);
            this.textBoxID_TT.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonSetFromFile);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(597, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(381, 50);
            this.panel1.TabIndex = 3;
            // 
            // buttonSetFromFile
            // 
            this.buttonSetFromFile.Enabled = false;
            this.buttonSetFromFile.Location = new System.Drawing.Point(268, 10);
            this.buttonSetFromFile.Name = "buttonSetFromFile";
            this.buttonSetFromFile.Size = new System.Drawing.Size(75, 23);
            this.buttonSetFromFile.TabIndex = 0;
            this.buttonSetFromFile.Text = "SetFromFile";
            this.buttonSetFromFile.UseVisualStyleBackColor = true;
            this.buttonSetFromFile.Click += new System.EventHandler(this.buttonSetFromFile_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBoxBaudrate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBoxComPort);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 50);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Com Port Setting";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(156, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Speed:";
            // 
            // comboBoxBaudrate
            // 
            this.comboBoxBaudrate.FormattingEnabled = true;
            this.comboBoxBaudrate.Items.AddRange(new object[] {
            "115200",
            "56000",
            "19200",
            "9600",
            "4800"});
            this.comboBoxBaudrate.Location = new System.Drawing.Point(203, 17);
            this.comboBoxBaudrate.Name = "comboBoxBaudrate";
            this.comboBoxBaudrate.Size = new System.Drawing.Size(89, 21);
            this.comboBoxBaudrate.TabIndex = 2;
            this.comboBoxBaudrate.Text = "115200";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Com Port:";
            // 
            // comboBoxComPort
            // 
            this.comboBoxComPort.FormattingEnabled = true;
            this.comboBoxComPort.Location = new System.Drawing.Point(62, 17);
            this.comboBoxComPort.Name = "comboBoxComPort";
            this.comboBoxComPort.Size = new System.Drawing.Size(78, 21);
            this.comboBoxComPort.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer2.Size = new System.Drawing.Size(978, 476);
            this.splitContainer2.SplitterDistance = 735;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.tabControlSettings);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer5);
            this.splitContainer3.Size = new System.Drawing.Size(735, 476);
            this.splitContainer3.SplitterDistance = 369;
            this.splitContainer3.TabIndex = 2;
            // 
            // tabControlSettings
            // 
            this.tabControlSettings.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControlSettings.Controls.Add(this.tabPageEvents);
            this.tabControlSettings.Controls.Add(this.tabPageGPRS);
            this.tabControlSettings.Controls.Add(this.tabPageServer);
            this.tabControlSettings.Controls.Add(this.tabPagePhones);
            this.tabControlSettings.Controls.Add(this.tabPageTextCmd);
            this.tabControlSettings.Controls.Add(this.tabPageGPS);
            this.tabControlSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlSettings.ItemSize = new System.Drawing.Size(100, 25);
            this.tabControlSettings.Location = new System.Drawing.Point(0, 0);
            this.tabControlSettings.Multiline = true;
            this.tabControlSettings.Name = "tabControlSettings";
            this.tabControlSettings.SelectedIndex = 0;
            this.tabControlSettings.Size = new System.Drawing.Size(735, 369);
            this.tabControlSettings.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControlSettings.TabIndex = 0;
            // 
            // tabPageEvents
            // 
            this.tabPageEvents.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPageEvents.Controls.Add(this.dataGridViewEvent);
            this.tabPageEvents.Location = new System.Drawing.Point(4, 29);
            this.tabPageEvents.Name = "tabPageEvents";
            this.tabPageEvents.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEvents.Size = new System.Drawing.Size(727, 336);
            this.tabPageEvents.TabIndex = 0;
            this.tabPageEvents.Text = "Events";
            this.tabPageEvents.UseVisualStyleBackColor = true;
            // 
            // dataGridViewEvent
            // 
            this.dataGridViewEvent.AllowUserToAddRows = false;
            this.dataGridViewEvent.AllowUserToDeleteRows = false;
            this.dataGridViewEvent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEvent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnEventsName,
            this.ColumnEventsValue,
            this.ColumnEventsUnit,
            this.ColumnEventsCheckWritePoint,
            this.ColumnEventsCheckSendToServer,
            this.ColumnEventsCheckSendSMS,
            this.ColumnEventsRecomended,
            this.ColumnEventsMaxValue});
            this.dataGridViewEvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewEvent.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewEvent.Name = "dataGridViewEvent";
            this.dataGridViewEvent.RowHeadersVisible = false;
            this.dataGridViewEvent.Size = new System.Drawing.Size(719, 328);
            this.dataGridViewEvent.TabIndex = 0;
            // 
            // ColumnEventsName
            // 
            this.ColumnEventsName.FillWeight = 130F;
            this.ColumnEventsName.HeaderText = "Parameter name";
            this.ColumnEventsName.MinimumWidth = 100;
            this.ColumnEventsName.Name = "ColumnEventsName";
            this.ColumnEventsName.ReadOnly = true;
            this.ColumnEventsName.Width = 130;
            // 
            // ColumnEventsValue
            // 
            this.ColumnEventsValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.NullValue = "----";
            this.ColumnEventsValue.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColumnEventsValue.FillWeight = 80F;
            this.ColumnEventsValue.HeaderText = "Parameter value";
            this.ColumnEventsValue.MinimumWidth = 50;
            this.ColumnEventsValue.Name = "ColumnEventsValue";
            this.ColumnEventsValue.ReadOnly = true;
            // 
            // ColumnEventsUnit
            // 
            dataGridViewCellStyle2.NullValue = "----";
            this.ColumnEventsUnit.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColumnEventsUnit.FillWeight = 65F;
            this.ColumnEventsUnit.HeaderText = "Unit";
            this.ColumnEventsUnit.MinimumWidth = 25;
            this.ColumnEventsUnit.Name = "ColumnEventsUnit";
            this.ColumnEventsUnit.ReadOnly = true;
            this.ColumnEventsUnit.Width = 65;
            // 
            // ColumnEventsCheckWritePoint
            // 
            this.ColumnEventsCheckWritePoint.FillWeight = 50F;
            this.ColumnEventsCheckWritePoint.HeaderText = "Fixing point";
            this.ColumnEventsCheckWritePoint.MinimumWidth = 25;
            this.ColumnEventsCheckWritePoint.Name = "ColumnEventsCheckWritePoint";
            this.ColumnEventsCheckWritePoint.Width = 50;
            // 
            // ColumnEventsCheckSendToServer
            // 
            this.ColumnEventsCheckSendToServer.FillWeight = 55F;
            this.ColumnEventsCheckSendToServer.HeaderText = "Sent to the server";
            this.ColumnEventsCheckSendToServer.MinimumWidth = 25;
            this.ColumnEventsCheckSendToServer.Name = "ColumnEventsCheckSendToServer";
            this.ColumnEventsCheckSendToServer.Width = 55;
            // 
            // ColumnEventsCheckSendSMS
            // 
            this.ColumnEventsCheckSendSMS.FillWeight = 55F;
            this.ColumnEventsCheckSendSMS.HeaderText = "SMS on phone";
            this.ColumnEventsCheckSendSMS.MinimumWidth = 25;
            this.ColumnEventsCheckSendSMS.Name = "ColumnEventsCheckSendSMS";
            this.ColumnEventsCheckSendSMS.Width = 55;
            // 
            // ColumnEventsRecomended
            // 
            dataGridViewCellStyle3.NullValue = "----";
            this.ColumnEventsRecomended.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColumnEventsRecomended.HeaderText = "The recommended values";
            this.ColumnEventsRecomended.MinimumWidth = 50;
            this.ColumnEventsRecomended.Name = "ColumnEventsRecomended";
            this.ColumnEventsRecomended.ReadOnly = true;
            // 
            // ColumnEventsMaxValue
            // 
            dataGridViewCellStyle4.NullValue = "----";
            this.ColumnEventsMaxValue.DefaultCellStyle = dataGridViewCellStyle4;
            this.ColumnEventsMaxValue.HeaderText = "Valid values";
            this.ColumnEventsMaxValue.MinimumWidth = 50;
            this.ColumnEventsMaxValue.Name = "ColumnEventsMaxValue";
            this.ColumnEventsMaxValue.ReadOnly = true;
            // 
            // tabPageGPRS
            // 
            this.tabPageGPRS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPageGPRS.Controls.Add(this.groupBox6);
            this.tabPageGPRS.Controls.Add(this.panel2);
            this.tabPageGPRS.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabPageGPRS.Location = new System.Drawing.Point(4, 29);
            this.tabPageGPRS.Name = "tabPageGPRS";
            this.tabPageGPRS.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGPRS.Size = new System.Drawing.Size(727, 336);
            this.tabPageGPRS.TabIndex = 1;
            this.tabPageGPRS.Text = "GPRS";
            this.tabPageGPRS.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.comboBoxGsmOperator);
            this.groupBox6.Controls.Add(this.buttonAddGsmOperator);
            this.groupBox6.Controls.Add(this.buttonApplyGsmOperator);
            this.groupBox6.Location = new System.Drawing.Point(488, 52);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(186, 139);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Operator Setting";
            // 
            // comboBoxGsmOperator
            // 
            this.comboBoxGsmOperator.FormattingEnabled = true;
            this.comboBoxGsmOperator.Location = new System.Drawing.Point(15, 31);
            this.comboBoxGsmOperator.Name = "comboBoxGsmOperator";
            this.comboBoxGsmOperator.Size = new System.Drawing.Size(148, 21);
            this.comboBoxGsmOperator.TabIndex = 2;
            // 
            // buttonAddGsmOperator
            // 
            this.buttonAddGsmOperator.Location = new System.Drawing.Point(54, 110);
            this.buttonAddGsmOperator.Name = "buttonAddGsmOperator";
            this.buttonAddGsmOperator.Size = new System.Drawing.Size(42, 23);
            this.buttonAddGsmOperator.TabIndex = 1;
            this.buttonAddGsmOperator.Text = ">>>";
            this.buttonAddGsmOperator.UseVisualStyleBackColor = true;
            this.buttonAddGsmOperator.Click += new System.EventHandler(this.buttonAddGsmOperator_Click);
            // 
            // buttonApplyGsmOperator
            // 
            this.buttonApplyGsmOperator.Location = new System.Drawing.Point(37, 72);
            this.buttonApplyGsmOperator.Name = "buttonApplyGsmOperator";
            this.buttonApplyGsmOperator.Size = new System.Drawing.Size(75, 23);
            this.buttonApplyGsmOperator.TabIndex = 1;
            this.buttonApplyGsmOperator.Text = "<<<";
            this.buttonApplyGsmOperator.UseVisualStyleBackColor = true;
            this.buttonApplyGsmOperator.Click += new System.EventHandler(this.buttonApplyGsmOperator_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridViewGprsSetting);
            this.panel2.Location = new System.Drawing.Point(26, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(439, 186);
            this.panel2.TabIndex = 0;
            // 
            // dataGridViewGprsSetting
            // 
            this.dataGridViewGprsSetting.AllowUserToAddRows = false;
            this.dataGridViewGprsSetting.AllowUserToDeleteRows = false;
            this.dataGridViewGprsSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGprsSetting.ColumnHeadersVisible = false;
            this.dataGridViewGprsSetting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnGprsName,
            this.ColumnGprsValue});
            this.dataGridViewGprsSetting.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewGprsSetting.Name = "dataGridViewGprsSetting";
            this.dataGridViewGprsSetting.RowHeadersVisible = false;
            this.dataGridViewGprsSetting.Size = new System.Drawing.Size(433, 178);
            this.dataGridViewGprsSetting.TabIndex = 0;
            // 
            // ColumnGprsName
            // 
            this.ColumnGprsName.FillWeight = 175F;
            this.ColumnGprsName.HeaderText = "Название парамметра";
            this.ColumnGprsName.MinimumWidth = 150;
            this.ColumnGprsName.Name = "ColumnGprsName";
            this.ColumnGprsName.ReadOnly = true;
            this.ColumnGprsName.Width = 175;
            // 
            // ColumnGprsValue
            // 
            this.ColumnGprsValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnGprsValue.HeaderText = "Значение парамметра";
            this.ColumnGprsValue.Name = "ColumnGprsValue";
            // 
            // tabPageServer
            // 
            this.tabPageServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPageServer.Controls.Add(this.groupBox10);
            this.tabPageServer.Controls.Add(this.groupBox9);
            this.tabPageServer.Location = new System.Drawing.Point(4, 29);
            this.tabPageServer.Name = "tabPageServer";
            this.tabPageServer.Size = new System.Drawing.Size(727, 336);
            this.tabPageServer.TabIndex = 2;
            this.tabPageServer.Text = "Server";
            this.tabPageServer.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.panel5);
            this.groupBox10.Controls.Add(this.buttonSaveServiceSetting);
            this.groupBox10.Location = new System.Drawing.Point(381, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(341, 198);
            this.groupBox10.TabIndex = 4;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Service Setting";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dataGVServiceSetting);
            this.panel5.Location = new System.Drawing.Point(6, 19);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(315, 123);
            this.panel5.TabIndex = 1;
            // 
            // dataGVServiceSetting
            // 
            this.dataGVServiceSetting.AllowUserToAddRows = false;
            this.dataGVServiceSetting.AllowUserToDeleteRows = false;
            this.dataGVServiceSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGVServiceSetting.ColumnHeadersVisible = false;
            this.dataGVServiceSetting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dataGVServiceSetting.Location = new System.Drawing.Point(0, 3);
            this.dataGVServiceSetting.Name = "dataGVServiceSetting";
            this.dataGVServiceSetting.RowHeadersVisible = false;
            this.dataGVServiceSetting.Size = new System.Drawing.Size(310, 117);
            this.dataGVServiceSetting.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.FillWeight = 155F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Название парамметра";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 150;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 155;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.HeaderText = "Значение парамметра";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // buttonSaveServiceSetting
            // 
            this.buttonSaveServiceSetting.Enabled = false;
            this.buttonSaveServiceSetting.Location = new System.Drawing.Point(119, 148);
            this.buttonSaveServiceSetting.Name = "buttonSaveServiceSetting";
            this.buttonSaveServiceSetting.Size = new System.Drawing.Size(96, 38);
            this.buttonSaveServiceSetting.TabIndex = 2;
            this.buttonSaveServiceSetting.Text = "Save Service setting";
            this.buttonSaveServiceSetting.UseVisualStyleBackColor = true;
            this.buttonSaveServiceSetting.Click += new System.EventHandler(this.buttonSaveServiceSetting_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.panel3);
            this.groupBox9.Controls.Add(this.buttonGeneratePass);
            this.groupBox9.Location = new System.Drawing.Point(7, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(368, 198);
            this.groupBox9.TabIndex = 3;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Telematic Server";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridViewServer);
            this.panel3.Location = new System.Drawing.Point(6, 19);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(355, 123);
            this.panel3.TabIndex = 1;
            // 
            // dataGridViewServer
            // 
            this.dataGridViewServer.AllowUserToAddRows = false;
            this.dataGridViewServer.AllowUserToDeleteRows = false;
            this.dataGridViewServer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewServer.ColumnHeadersVisible = false;
            this.dataGridViewServer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnServerName,
            this.ColumnServerValue});
            this.dataGridViewServer.Location = new System.Drawing.Point(3, 0);
            this.dataGridViewServer.Name = "dataGridViewServer";
            this.dataGridViewServer.RowHeadersVisible = false;
            this.dataGridViewServer.Size = new System.Drawing.Size(352, 117);
            this.dataGridViewServer.TabIndex = 0;
            // 
            // ColumnServerName
            // 
            this.ColumnServerName.FillWeight = 175F;
            this.ColumnServerName.HeaderText = "Название парамметра";
            this.ColumnServerName.MinimumWidth = 150;
            this.ColumnServerName.Name = "ColumnServerName";
            this.ColumnServerName.ReadOnly = true;
            this.ColumnServerName.Width = 175;
            // 
            // ColumnServerValue
            // 
            this.ColumnServerValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnServerValue.HeaderText = "Значение парамметра";
            this.ColumnServerValue.Name = "ColumnServerValue";
            // 
            // buttonGeneratePass
            // 
            this.buttonGeneratePass.Location = new System.Drawing.Point(137, 148);
            this.buttonGeneratePass.Name = "buttonGeneratePass";
            this.buttonGeneratePass.Size = new System.Drawing.Size(96, 38);
            this.buttonGeneratePass.TabIndex = 2;
            this.buttonGeneratePass.Text = "Generate Login/Password";
            this.buttonGeneratePass.UseVisualStyleBackColor = true;
            this.buttonGeneratePass.Click += new System.EventHandler(this.buttonGeneratePass_Click);
            // 
            // tabPagePhones
            // 
            this.tabPagePhones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPagePhones.Controls.Add(this.buttonCopyPhone);
            this.tabPagePhones.Controls.Add(this.groupBox7);
            this.tabPagePhones.Controls.Add(this.panel4);
            this.tabPagePhones.Location = new System.Drawing.Point(4, 29);
            this.tabPagePhones.Name = "tabPagePhones";
            this.tabPagePhones.Size = new System.Drawing.Size(727, 336);
            this.tabPagePhones.TabIndex = 3;
            this.tabPagePhones.Text = "Phones";
            this.tabPagePhones.UseVisualStyleBackColor = true;
            // 
            // buttonCopyPhone
            // 
            this.buttonCopyPhone.Location = new System.Drawing.Point(374, 115);
            this.buttonCopyPhone.Name = "buttonCopyPhone";
            this.buttonCopyPhone.Size = new System.Drawing.Size(30, 60);
            this.buttonCopyPhone.TabIndex = 2;
            this.buttonCopyPhone.Text = "<<";
            this.buttonCopyPhone.UseVisualStyleBackColor = true;
            this.buttonCopyPhone.Click += new System.EventHandler(this.buttonCopyPhone_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.buttonAddNewPhone);
            this.groupBox7.Controls.Add(this.dataGVPhonesInDataBase);
            this.groupBox7.Location = new System.Drawing.Point(410, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(297, 291);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Phones in DataBase";
            // 
            // buttonAddNewPhone
            // 
            this.buttonAddNewPhone.Location = new System.Drawing.Point(108, 264);
            this.buttonAddNewPhone.Name = "buttonAddNewPhone";
            this.buttonAddNewPhone.Size = new System.Drawing.Size(75, 23);
            this.buttonAddNewPhone.TabIndex = 1;
            this.buttonAddNewPhone.Text = "Add Phone";
            this.buttonAddNewPhone.UseVisualStyleBackColor = true;
            this.buttonAddNewPhone.Click += new System.EventHandler(this.buttonAddNewPhone_Click);
            // 
            // dataGVPhonesInDataBase
            // 
            this.dataGVPhonesInDataBase.AllowUserToAddRows = false;
            this.dataGVPhonesInDataBase.AllowUserToDeleteRows = false;
            this.dataGVPhonesInDataBase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGVPhonesInDataBase.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnName,
            this.ColumnNumber});
            this.dataGVPhonesInDataBase.Location = new System.Drawing.Point(6, 19);
            this.dataGVPhonesInDataBase.Name = "dataGVPhonesInDataBase";
            this.dataGVPhonesInDataBase.RowHeadersVisible = false;
            this.dataGVPhonesInDataBase.Size = new System.Drawing.Size(284, 239);
            this.dataGVPhonesInDataBase.TabIndex = 0;
            // 
            // ColumnName
            // 
            this.ColumnName.HeaderText = "Name";
            this.ColumnName.Name = "ColumnName";
            // 
            // ColumnNumber
            // 
            this.ColumnNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnNumber.HeaderText = "Phone";
            this.ColumnNumber.Name = "ColumnNumber";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBox8);
            this.panel4.Controls.Add(this.dataGridViewPhones);
            this.panel4.Location = new System.Drawing.Point(7, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(361, 300);
            this.panel4.TabIndex = 0;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.buttonSetTtPhone);
            this.groupBox8.Controls.Add(this.textBoxTtPhone);
            this.groupBox8.Location = new System.Drawing.Point(3, 244);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(354, 52);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "SIM Card Phone";
            // 
            // buttonSetTtPhone
            // 
            this.buttonSetTtPhone.Enabled = false;
            this.buttonSetTtPhone.Location = new System.Drawing.Point(250, 19);
            this.buttonSetTtPhone.Name = "buttonSetTtPhone";
            this.buttonSetTtPhone.Size = new System.Drawing.Size(96, 23);
            this.buttonSetTtPhone.TabIndex = 1;
            this.buttonSetTtPhone.Text = "Set Phone";
            this.buttonSetTtPhone.UseVisualStyleBackColor = true;
            this.buttonSetTtPhone.Click += new System.EventHandler(this.buttonSetTtPhone_Click);
            // 
            // textBoxTtPhone
            // 
            this.textBoxTtPhone.Location = new System.Drawing.Point(6, 19);
            this.textBoxTtPhone.Name = "textBoxTtPhone";
            this.textBoxTtPhone.Size = new System.Drawing.Size(215, 20);
            this.textBoxTtPhone.TabIndex = 0;
            // 
            // dataGridViewPhones
            // 
            this.dataGridViewPhones.AllowUserToAddRows = false;
            this.dataGridViewPhones.AllowUserToDeleteRows = false;
            this.dataGridViewPhones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPhones.ColumnHeadersVisible = false;
            this.dataGridViewPhones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnPhoneName,
            this.ColumnPhoneValue});
            this.dataGridViewPhones.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewPhones.Name = "dataGridViewPhones";
            this.dataGridViewPhones.RowHeadersVisible = false;
            this.dataGridViewPhones.Size = new System.Drawing.Size(346, 218);
            this.dataGridViewPhones.TabIndex = 0;
            // 
            // ColumnPhoneName
            // 
            this.ColumnPhoneName.FillWeight = 175F;
            this.ColumnPhoneName.HeaderText = "Назначение номера";
            this.ColumnPhoneName.MinimumWidth = 150;
            this.ColumnPhoneName.Name = "ColumnPhoneName";
            this.ColumnPhoneName.ReadOnly = true;
            this.ColumnPhoneName.Width = 175;
            // 
            // ColumnPhoneValue
            // 
            this.ColumnPhoneValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnPhoneValue.HeaderText = "Номер телефона";
            this.ColumnPhoneValue.Name = "ColumnPhoneValue";
            // 
            // tabPageTextCmd
            // 
            this.tabPageTextCmd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPageTextCmd.Controls.Add(this.groupBox4);
            this.tabPageTextCmd.Controls.Add(this.groupBox3);
            this.tabPageTextCmd.Location = new System.Drawing.Point(4, 29);
            this.tabPageTextCmd.Name = "tabPageTextCmd";
            this.tabPageTextCmd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTextCmd.Size = new System.Drawing.Size(727, 336);
            this.tabPageTextCmd.TabIndex = 4;
            this.tabPageTextCmd.Text = "TextCmd";
            this.tabPageTextCmd.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBoxReadText);
            this.groupBox4.Location = new System.Drawing.Point(6, 150);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(711, 58);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Text Request";
            // 
            // textBoxReadText
            // 
            this.textBoxReadText.Location = new System.Drawing.Point(1, 19);
            this.textBoxReadText.Name = "textBoxReadText";
            this.textBoxReadText.ReadOnly = true;
            this.textBoxReadText.Size = new System.Drawing.Size(704, 20);
            this.textBoxReadText.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxSendText);
            this.groupBox3.Location = new System.Drawing.Point(2, 78);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(715, 56);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Text Command";
            // 
            // textBoxSendText
            // 
            this.textBoxSendText.Location = new System.Drawing.Point(5, 19);
            this.textBoxSendText.Name = "textBoxSendText";
            this.textBoxSendText.Size = new System.Drawing.Size(704, 20);
            this.textBoxSendText.TabIndex = 0;
            // 
            // tabPageGPS
            // 
            this.tabPageGPS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPageGPS.Controls.Add(this.label4);
            this.tabPageGPS.Location = new System.Drawing.Point(4, 29);
            this.tabPageGPS.Name = "tabPageGPS";
            this.tabPageGPS.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGPS.Size = new System.Drawing.Size(727, 336);
            this.tabPageGPS.TabIndex = 5;
            this.tabPageGPS.Text = "GPS";
            this.tabPageGPS.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(241, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(246, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "Request all data from device";
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.splitContainer7);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer5.Size = new System.Drawing.Size(735, 103);
            this.splitContainer5.SplitterDistance = 618;
            this.splitContainer5.TabIndex = 3;
            // 
            // splitContainer7
            // 
            this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer7.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer7.Location = new System.Drawing.Point(0, 0);
            this.splitContainer7.Name = "splitContainer7";
            // 
            // splitContainer7.Panel1
            // 
            this.splitContainer7.Panel1.Controls.Add(this.splitContainer8);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.Controls.Add(this.listBoxLog);
            this.splitContainer7.Size = new System.Drawing.Size(618, 103);
            this.splitContainer7.SplitterDistance = 112;
            this.splitContainer7.TabIndex = 0;
            // 
            // splitContainer8
            // 
            this.splitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer8.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer8.Location = new System.Drawing.Point(0, 0);
            this.splitContainer8.Name = "splitContainer8";
            this.splitContainer8.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer8.Panel1
            // 
            this.splitContainer8.Panel1.Controls.Add(this.buttonReadParameter);
            this.splitContainer8.Size = new System.Drawing.Size(112, 103);
            this.splitContainer8.SplitterDistance = 35;
            this.splitContainer8.TabIndex = 0;
            // 
            // buttonReadParameter
            // 
            this.buttonReadParameter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonReadParameter.Enabled = false;
            this.buttonReadParameter.Location = new System.Drawing.Point(0, 0);
            this.buttonReadParameter.Name = "buttonReadParameter";
            this.buttonReadParameter.Size = new System.Drawing.Size(112, 35);
            this.buttonReadParameter.TabIndex = 0;
            this.buttonReadParameter.Text = "Read Parameter";
            this.buttonReadParameter.UseVisualStyleBackColor = true;
            this.buttonReadParameter.Click += new System.EventHandler(this.buttonReadParameter_Click);
            // 
            // listBoxLog
            // 
            this.listBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(0, 0);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(502, 103);
            this.listBoxLog.TabIndex = 2;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            this.splitContainer6.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.buttonWriteParameter);
            this.splitContainer6.Size = new System.Drawing.Size(113, 103);
            this.splitContainer6.SplitterDistance = 35;
            this.splitContainer6.TabIndex = 2;
            // 
            // buttonWriteParameter
            // 
            this.buttonWriteParameter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonWriteParameter.Enabled = false;
            this.buttonWriteParameter.Location = new System.Drawing.Point(0, 0);
            this.buttonWriteParameter.Name = "buttonWriteParameter";
            this.buttonWriteParameter.Size = new System.Drawing.Size(113, 35);
            this.buttonWriteParameter.TabIndex = 1;
            this.buttonWriteParameter.Text = "Write Parameter";
            this.buttonWriteParameter.UseVisualStyleBackColor = true;
            this.buttonWriteParameter.Click += new System.EventHandler(this.buttonWriteParameter_Click);
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.groupBox5);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.buttonEditTt);
            this.splitContainer4.Panel2.Controls.Add(this.buttonDeleteTt);
            this.splitContainer4.Panel2.Controls.Add(this.buttonAddNewTt);
            this.splitContainer4.Size = new System.Drawing.Size(239, 476);
            this.splitContainer4.SplitterDistance = 426;
            this.splitContainer4.TabIndex = 2;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dataGridViewTeletracks);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(239, 426);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Teletracks";
            // 
            // dataGridViewTeletracks
            // 
            this.dataGridViewTeletracks.AllowUserToAddRows = false;
            this.dataGridViewTeletracks.AllowUserToDeleteRows = false;
            this.dataGridViewTeletracks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTeletracks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnNameTT,
            this.ColumnDescriptionTT,
            this.ColumnShortIDTT});
            this.dataGridViewTeletracks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTeletracks.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewTeletracks.Name = "dataGridViewTeletracks";
            this.dataGridViewTeletracks.RowHeadersVisible = false;
            this.dataGridViewTeletracks.Size = new System.Drawing.Size(233, 407);
            this.dataGridViewTeletracks.TabIndex = 1;
            this.dataGridViewTeletracks.SelectionChanged += new System.EventHandler(this.dataGridViewTeletracks_SelectionChanged);
            // 
            // ColumnNameTT
            // 
            this.ColumnNameTT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnNameTT.HeaderText = "Name";
            this.ColumnNameTT.Name = "ColumnNameTT";
            // 
            // ColumnDescriptionTT
            // 
            this.ColumnDescriptionTT.HeaderText = "Description";
            this.ColumnDescriptionTT.Name = "ColumnDescriptionTT";
            this.ColumnDescriptionTT.Width = 65;
            // 
            // ColumnShortIDTT
            // 
            this.ColumnShortIDTT.HeaderText = "Short ID";
            this.ColumnShortIDTT.Name = "ColumnShortIDTT";
            this.ColumnShortIDTT.Width = 65;
            // 
            // buttonEditTt
            // 
            this.buttonEditTt.Location = new System.Drawing.Point(4, 3);
            this.buttonEditTt.Name = "buttonEditTt";
            this.buttonEditTt.Size = new System.Drawing.Size(66, 40);
            this.buttonEditTt.TabIndex = 4;
            this.buttonEditTt.Text = "Edit Name";
            this.buttonEditTt.UseVisualStyleBackColor = true;
            this.buttonEditTt.Click += new System.EventHandler(this.buttonEditTt_Click);
            // 
            // buttonDeleteTt
            // 
            this.buttonDeleteTt.BackgroundImage = global::TeletrackTuning.Properties.Resources.IconX;
            this.buttonDeleteTt.Location = new System.Drawing.Point(180, 9);
            this.buttonDeleteTt.Name = "buttonDeleteTt";
            this.buttonDeleteTt.Size = new System.Drawing.Size(36, 29);
            this.buttonDeleteTt.TabIndex = 3;
            this.buttonDeleteTt.UseVisualStyleBackColor = true;
            this.buttonDeleteTt.Click += new System.EventHandler(this.buttonDeleteTt_Click);
            // 
            // buttonAddNewTt
            // 
            this.buttonAddNewTt.Location = new System.Drawing.Point(85, 3);
            this.buttonAddNewTt.Name = "buttonAddNewTt";
            this.buttonAddNewTt.Size = new System.Drawing.Size(75, 40);
            this.buttonAddNewTt.TabIndex = 2;
            this.buttonAddNewTt.Text = "Add New Teletrack";
            this.buttonAddNewTt.UseVisualStyleBackColor = true;
            this.buttonAddNewTt.Click += new System.EventHandler(this.buttonAddNewTt_Click);
            // 
            // timerThreadRx
            // 
            this.timerThreadRx.Interval = 10;
            this.timerThreadRx.Tick += new System.EventHandler(this.timerThreadRx_Tick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Teletrack Tuning file (*.tt)|*.tt|All files (*.*)|*.*";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Teletrack Tuning file (*.tt)|*.tt|All files (*.*)|*.*";
            // 
            // FormBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(978, 577);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.MinimumSize = new System.Drawing.Size(750, 550);
            this.Name = "FormBase";
            this.Text = "TeletrackConfigurator v 1.4";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.tabControlSettings.ResumeLayout(false);
            this.tabPageEvents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvent)).EndInit();
            this.tabPageGPRS.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGprsSetting)).EndInit();
            this.tabPageServer.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGVServiceSetting)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewServer)).EndInit();
            this.tabPagePhones.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGVPhonesInDataBase)).EndInit();
            this.panel4.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPhones)).EndInit();
            this.tabPageTextCmd.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPageGPS.ResumeLayout(false);
            this.tabPageGPS.PerformLayout();
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            this.splitContainer5.ResumeLayout(false);
            this.splitContainer7.Panel1.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            this.splitContainer7.ResumeLayout(false);
            this.splitContainer8.Panel1.ResumeLayout(false);
            this.splitContainer8.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTeletracks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.ToolStripButton toolStripButtonOpen;
    private System.Windows.Forms.ToolStripButton toolStripButtonSave;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripButton ButtonDBSetting;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripLabel toolStripLabel1;
    private System.Windows.Forms.ToolStripComboBox ComboBoxInterfaceSettings;
    private System.Windows.Forms.TextBox textBoxID_TT;
    private System.Windows.Forms.ToolStripLabel toolStripLabel2;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
    private System.Windows.Forms.ToolStripButton ButtonConnect;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
    private System.Windows.Forms.ToolStripButton ButtonDisconnect;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.ComboBox comboBoxBaudrate;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox comboBoxComPort;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Timer timerThreadRx;
    private System.Windows.Forms.SplitContainer splitContainer2;
    private System.Windows.Forms.SplitContainer splitContainer3;
    private System.Windows.Forms.TabControl tabControlSettings;
    private System.Windows.Forms.TabPage tabPageEvents;
    private System.Windows.Forms.TabPage tabPageGPRS;
    private System.Windows.Forms.TabPage tabPageServer;
    private System.Windows.Forms.TabPage tabPagePhones;
    private System.Windows.Forms.Button buttonReadParameter;
    private System.Windows.Forms.Button buttonWriteParameter;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.DataGridView dataGridViewGprsSetting;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.DataGridView dataGridViewServer;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnServerName;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnServerValue;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnGprsName;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnGprsValue;
    private System.Windows.Forms.Panel panel4;
    private System.Windows.Forms.DataGridView dataGridViewPhones;
    private System.Windows.Forms.ListBox listBoxLog;
    private System.Windows.Forms.DataGridView dataGridViewEvent;
    private System.Windows.Forms.TabPage tabPageTextCmd;
    private System.Windows.Forms.TextBox textBoxReadText;
    private System.Windows.Forms.TextBox textBoxSendText;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.Button buttonAddNewTt;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.Button buttonSetFromFile;
    private System.Windows.Forms.OpenFileDialog openFileDialog1;
    private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    private System.Windows.Forms.ComboBox comboBoxGsmOperator;
    private System.Windows.Forms.Button buttonAddGsmOperator;
    private System.Windows.Forms.Button buttonApplyGsmOperator;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.GroupBox groupBox7;
    private System.Windows.Forms.DataGridView dataGVPhonesInDataBase;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNumber;
    private System.Windows.Forms.Button buttonAddNewPhone;
    private System.Windows.Forms.Button buttonGeneratePass;
    private System.Windows.Forms.SplitContainer splitContainer4;
    private System.Windows.Forms.SplitContainer splitContainer5;
    private System.Windows.Forms.SplitContainer splitContainer7;
    private System.Windows.Forms.SplitContainer splitContainer8;
    private System.Windows.Forms.SplitContainer splitContainer6;
    private System.Windows.Forms.GroupBox groupBox8;
    private System.Windows.Forms.Button buttonSetTtPhone;
    private System.Windows.Forms.TextBox textBoxTtPhone;
    private System.Windows.Forms.GroupBox groupBox10;
    private System.Windows.Forms.GroupBox groupBox9;
    private System.Windows.Forms.Panel panel5;
    private System.Windows.Forms.DataGridView dataGVServiceSetting;
    private System.Windows.Forms.Button buttonSaveServiceSetting;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    private System.Windows.Forms.Button buttonDeleteTt;
    private System.Windows.Forms.TabPage tabPageGPS;
    private System.Windows.Forms.CheckBox checkBoxPing;
    private System.Windows.Forms.Button buttonCopyPhone;
    private System.Windows.Forms.DataGridView dataGridViewTeletracks;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNameTT;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDescriptionTT;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnShortIDTT;
    private System.Windows.Forms.Button buttonEditTt;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPhoneName;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPhoneValue;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEventsName;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEventsValue;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEventsUnit;
    private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnEventsCheckWritePoint;
    private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnEventsCheckSendToServer;
    private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnEventsCheckSendSMS;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEventsRecomended;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnEventsMaxValue;
  }
}

