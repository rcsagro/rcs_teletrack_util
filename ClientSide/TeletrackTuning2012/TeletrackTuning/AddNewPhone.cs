﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TeletrackTuning
{
  public partial class AddNewPhone : Form
  {
    private string name = "";
    public string getName
    {
      get { return name; }
    }
    private string phone = "";
    public string getPhone
    {
      get { return phone; }
    }

    public AddNewPhone()
    {
      InitializeComponent();
    }

    private void buttonAddPhoneCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void buttonAddPhoneOk_Click(object sender, EventArgs e)
    {
      name = textBoxNamePhone.Text;
      phone = textBoxNumber.Text;
      Close();
    }
  }
}
