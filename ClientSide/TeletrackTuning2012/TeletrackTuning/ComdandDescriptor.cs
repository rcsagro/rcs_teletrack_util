﻿namespace TeletrackTuning
{
  /// <summary>
  /// Описание идентификаторов команд
  /// </summary>
  public enum CommandDescriptor
  {
    /// <summary>
    /// Неизвестная команда 0
    /// </summary>
    Unknown = 0,

    TextCommand = 1,
    /// <summary>
    /// Ping, при приходе от телетрека игнорируем 8
    /// </summary>
    Ping = 8,
    /// <summary>
    /// Установка параметров для адресов 
    /// SMS, E-MAIL и WEB SERVER (GPRS) сообщений 11
    /// </summary>
    SMSConfigSet = 11,
    /// <summary>
    /// Подтверждение установки параметров для адресов 
    /// SMS, E-MAIL и WEB SERVER (GPRS) сообщений 21.
    /// </summary>
    SMSConfigConfirm = 21,
    /// <summary>
    /// Запрос параметров для адресов 
    /// SMS, E-MAIL и WEB SERVER (GPRS) сообщений 31.
    /// </summary>
    SMSConfigQuery = 31,
    /// <summary>
    /// Ответ на запрос параметров для адресов 
    /// SMS, E-MAIL и WEB SERVER (GPRS) сообщений 41.
    /// </summary>
    SMSConfigAnswer = 41,
    /// <summary>
    /// Установка конфигурации телефонных номеров 12.
    /// </summary>
    PhoneConfigSet = 12,
    /// <summary>
    /// Подтверждение установки конфигурации телефонных номеров 22. 
    /// </summary>
    PhoneConfigConfirm = 22,
    /// <summary>
    /// Запрос конфигурации телефонных номеров 32. 
    /// </summary>
    PhoneConfigQuery = 32,
    /// <summary>
    /// Ответ на запрос конфигурации телефонных номеров 42. 
    /// </summary>
    PhoneConfigAnswer = 42,
    /// <summary>
    /// Установка конфигурации событий 13.
    /// </summary>
    EventConfigSet = 13,
    /// <summary>
    /// Подтверждение установки конфигурации событий 23.
    /// </summary>
    EventConfigConfirm = 23,
    /// <summary>
    /// Запрос конфигурации событий 33.
    /// </summary>
    EventConfigQuery = 33,
    /// <summary>
    /// Ответ на запрос конфигурации событий 43.
    /// </summary>
    EventConfigAnswer = 43,
    /// <summary>
    /// Установка конфигурации уникальных параметров 14.
    /// </summary>
    UniqueConfigSet = 14,
    /// <summary>
    /// Подтверждение установки конфигурации уникальных параметров 24.
    /// </summary>
    UniqueConfigConfirm = 24,
    /// <summary>
    /// Запрос конфигурации уникальных параметров 34.
    /// </summary>
    UniqueConfigQuery = 34,
    /// <summary>
    /// Ответ на запрос конфигурации уникальных параметров 44.
    /// </summary>
    UniqueConfigAnswer = 44,
    /// <summary>
    /// Установка конфигурации идентификационных параметров 17.
    /// </summary>
    IdConfigSet = 17,
    /// <summary>
    /// Подтверждение установки конфигурации идентификационных параметров 27.
    /// </summary>
    IdConfigConfirm = 27,
    /// <summary>
    /// Запрос конфигурации идентификационных параметров 37.
    /// </summary>
    IdConfigQuery = 37,
    /// <summary>
    /// Ответ на запрос конфигурации идентификационных параметров 47.
    /// </summary>
    IdConfigAnswer = 47,
    /// <summary>
    /// Установка конфигурации контрольных зон 15. 
    /// </summary>
    ZoneConfigSet = 15,
    /// <summary>
    /// Подтверждение установки конфигурации контрольных зон 25.  
    /// </summary>
    ZoneConfigConfirm = 25,
    /// <summary>
    /// Запрос GPS данных 36. 
    /// </summary>
    DataGpsQuery = 36,
    /// <summary>
    /// Ответ на запрос GPS данных 46. 
    /// </summary>
    DataGpsAnswer = 46,
    /// <summary>
    /// Автоматическая отправка GPS данных 49. 
    /// </summary>
    DataGpsAuto = 49,
    /// <summary>
    /// Сообщение водителю
    /// </summary>
    MessageToDriver = 10,
    /// <summary>
    /// Установка основных настроек GPRS 50.
    /// </summary>
    GprsBaseConfigSet = 50,
    /// <summary>
    /// Подтверждение установки основных настроек GPRS 60.
    /// </summary>
    GprsBaseConfigConfirm = 60,
    /// <summary>
    /// Запрос основных настроек GPRS 70.
    /// </summary>
    GprsBaseConfigQuery = 70,
    /// <summary>
    /// Ответ на запрос основных настроек GPRS 80.
    /// </summary>
    GprsBaseConfigAnswer = 80,
    /// <summary>
    /// Установка настроек подключения к провайдеру GPRS 54. 
    /// </summary>
    GprsProviderConfigSet = 54,
    /// <summary>
    /// Подтверждение установки настроек подключения к провайдеру GPRS 64. 
    /// </summary>
    GprsProviderConfigConfirm = 64,
    /// <summary>
    /// Запрос настроек подключения к провайдеру GPRS 74. 
    /// </summary>
    GprsProviderConfigQuery = 74,
    /// <summary>
    /// Ответ на запрос настроек подключения к провайдеру GPRS 84. 
    /// </summary>
    GprsProviderConfigAnswer = 84,
    /// <summary>
    /// Установка настроек Email GPRS 51.
    /// </summary>
    GprsEmailConfigSet = 51,
    /// <summary>
    /// Подтверждение установки настроек Email GPRS 61.
    /// </summary>
    GprsEmailConfigConfirm = 61,
    /// <summary>
    /// Запрос настроек Email GPRS 71.
    /// </summary>
    GprsEmailConfigQuery = 71,
    /// <summary>
    /// Ответ на запрос настроек Email GPRS 81.
    /// </summary>
    GprsEmailConfigAnswer = 81,
    /// <summary>
    /// Установка настроек Socket GPRS 52.
    /// </summary>
    GprsSocketConfigSet = 52,
    /// <summary>
    /// Подтверждение установки настроек Socket GPRS 62.
    /// </summary>
    GprsSocketConfigConfirm = 62,
    /// <summary>
    /// Запрос настроек Socket GPRS 72.
    /// </summary>
    GprsSocketConfigQuery = 72,
    /// <summary>
    /// Ответ на запрос настроек Socket GPRS 82.
    /// </summary>
    GprsSocketConfigAnswer = 82,
    /// <summary>
    /// Установка настроек FTP GPRS 53.
    /// </summary>
    GprsFtpConfigSet = 53,
    /// <summary>
    /// Подтверждение установки настроек FTP GPRS 63.
    /// </summary>
    GprsFtpConfigConfirm = 63,
    /// <summary>
    /// Запрос настроек FTP GPRS 73.
    /// </summary>
    GprsFtpConfigQuery = 73,
    /// <summary>
    /// Ответ на запрос настроек FTP GPRS 83.
    /// </summary>
    GprsFtpConfigAnswer = 83
  }
}
