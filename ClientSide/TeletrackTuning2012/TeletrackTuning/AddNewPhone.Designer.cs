﻿namespace TeletrackTuning
{
  partial class AddNewPhone
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.buttonAddPhoneCancel = new System.Windows.Forms.Button();
      this.buttonAddPhoneOk = new System.Windows.Forms.Button();
      this.textBoxNumber = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.textBoxNamePhone = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.buttonAddPhoneCancel);
      this.groupBox1.Controls.Add(this.buttonAddPhoneOk);
      this.groupBox1.Controls.Add(this.textBoxNumber);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.textBoxNamePhone);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox1.Location = new System.Drawing.Point(0, 0);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(292, 119);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      // 
      // buttonAddPhoneCancel
      // 
      this.buttonAddPhoneCancel.Location = new System.Drawing.Point(168, 84);
      this.buttonAddPhoneCancel.Name = "buttonAddPhoneCancel";
      this.buttonAddPhoneCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonAddPhoneCancel.TabIndex = 3;
      this.buttonAddPhoneCancel.Text = "Cancel";
      this.buttonAddPhoneCancel.UseVisualStyleBackColor = true;
      this.buttonAddPhoneCancel.Click += new System.EventHandler(this.buttonAddPhoneCancel_Click);
      // 
      // buttonAddPhoneOk
      // 
      this.buttonAddPhoneOk.Location = new System.Drawing.Point(47, 84);
      this.buttonAddPhoneOk.Name = "buttonAddPhoneOk";
      this.buttonAddPhoneOk.Size = new System.Drawing.Size(75, 23);
      this.buttonAddPhoneOk.TabIndex = 2;
      this.buttonAddPhoneOk.Text = "Ok";
      this.buttonAddPhoneOk.UseVisualStyleBackColor = true;
      this.buttonAddPhoneOk.Click += new System.EventHandler(this.buttonAddPhoneOk_Click);
      // 
      // textBoxNumber
      // 
      this.textBoxNumber.Location = new System.Drawing.Point(65, 49);
      this.textBoxNumber.Name = "textBoxNumber";
      this.textBoxNumber.Size = new System.Drawing.Size(192, 20);
      this.textBoxNumber.TabIndex = 1;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 52);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(47, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "Number:";
      // 
      // textBoxNamePhone
      // 
      this.textBoxNamePhone.Location = new System.Drawing.Point(65, 23);
      this.textBoxNamePhone.Name = "textBoxNamePhone";
      this.textBoxNamePhone.Size = new System.Drawing.Size(192, 20);
      this.textBoxNamePhone.TabIndex = 1;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 26);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(38, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Name:";
      // 
      // AddNewPhone
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(292, 119);
      this.Controls.Add(this.groupBox1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button buttonAddPhoneCancel;
    private System.Windows.Forms.Button buttonAddPhoneOk;
    private System.Windows.Forms.TextBox textBoxNumber;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox textBoxNamePhone;
    private System.Windows.Forms.Label label1;
  }
}