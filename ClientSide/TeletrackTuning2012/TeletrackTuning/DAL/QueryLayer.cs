using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Reflection;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace TeletrackTuning.DAL
{
    public static class QueryLayer
    {
        public static int TypeDataBaseForUsing = -1;
        private const int MySqlUse = 0;
        private const int MssqlUse = 1;

        public static string SelectConfigEvent
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT * FROM ConfigEvent " +
                               "WHERE ID = COALESCE((SELECT ConfigEvent_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1) " +
                               "ORDER by ConfigEvent_ID DESC LIMIT 1";

                    case MssqlUse:
                        return "SELECT TOP 1 * FROM ConfigEvent " +
                               "WHERE ID = COALESCE((SELECT TOP 1 ConfigEvent_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1) " +
                               "ORDER by ConfigEvent_ID DESC";
                }

                return "";
            }
        }

        public static string SelectFromServiceInit
        {
            get
            {
                return "SELECT * FROM ServiceInit WHERE ServiceType_ID = 3";
            }
        }

        public static string SelectFromDataGpsLostOn
        {
            get
            {
                return "SELECT Mobitel_ID, Begin_SrvPacketID, End_SrvPacketID " +
                       "FROM datagpslost_on " +
                       "ORDER BY Mobitel_ID, Begin_SrvPacketID";
            }
        }

        public static string OnListMobitelConfig
        {
            get
            {
                return "OnListMobitelConfig";
            }
        }

        public static string SelectFromTelNumbers
        {
            get
            {
                return "SELECT Name, TelNumber FROM TelNumbers";
            }
        }

        public static string SelectFromMobitels
        {
            get
            {
                return "SELECT Name, Descr, Mobitel_ID FROM mobitels ORDER BY Mobitel_ID ";
            }
        }

        public static string SelectFromServiceInitType
        {
            get
            {
                return "SELECT A1, A2, A3, A4 FROM serviceinit s WHERE ServiceType_ID = 3";
            }
        }

        public static string SelectFromConfigZoneSet
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return
                            "SELECT zr.* FROM ConfigZoneSet cfg JOIN ZoneRelations zr ON zr.ConfigZoneSet_ID = cfg.ConfigZoneSet_ID WHERE cfg.Message_ID = ?MessageId";

                    case MssqlUse:
                        return
                            "SELECT zr.* FROM ConfigZoneSet cfg JOIN ZoneRelations zr ON zr.ConfigZoneSet_ID = cfg.ConfigZoneSet_ID WHERE cfg.Message_ID = @MessageId";
                }

                return "";
            }
        }

        public static string SelectFromPoints
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT Latitude, Longitude FROM Points WHERE Zone_ID = ?ZoneId";

                    case MssqlUse:
                        return "SELECT Latitude, Longitude FROM Points WHERE Zone_ID = @ZoneId";
                }

                return "";
            }
        }

        public static string SelectFromMessages
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return @"SELECT m1.Message_ID AS MessageID, m1.Command_ID AS CommandID, 
                                m1.MessageCounter AS MessageCounter 
                                FROM Messages m1 JOIN ( 
                                SELECT Command_ID, MAX(Message_ID) AS MessageId 
                                FROM Messages WHERE (Mobitel_ID = ?MobitelId) AND (isNew = 1) AND (Direction = 1) GROUP BY Command_ID) m2 
                                ON m1.Message_ID = m2.MessageId";

                    case MssqlUse:
                        return @"SELECT m1.Message_ID AS MessageID, m1.Command_ID AS CommandID, 
                                m1.MessageCounter AS MessageCounter 
                                FROM Messages m1 JOIN ( 
                                SELECT Command_ID, MAX(Message_ID) AS MessageId 
                                FROM Messages WHERE (Mobitel_ID = @MobitelId) AND (isNew = 1) AND (Direction = 1) GROUP BY Command_ID) m2 
                                ON m1.Message_ID = m2.MessageId";
                }

                return "";
            }
        }

        public static string SelectDistinctMsg
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT DISTINCT msg.Mobitel_ID AS MobitelId " +
                               "FROM Messages msg JOIN Mobitels m ON m.Mobitel_id = msg.Mobitel_id " +
                               " JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID) " +
                               "WHERE (msg.isNew = 1) AND (msg.Direction = 1) AND " +
                               " (c.InternalMobitelConfig_ID = ( " +
                               "   SELECT intConf.InternalMobitelConfig_ID " +
                               "   FROM internalmobitelconfig intConf " +
                               "   WHERE (intConf.ID = c.ID)  " +
                               "   ORDER BY intConf.InternalMobitelConfig_ID DESC " +
                               "   LIMIT 1)) AND " +
                               " (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')";

                    case MssqlUse:
                        return @"SELECT DISTINCT msg.Mobitel_ID AS MobitelId
                                FROM Messages msg JOIN Mobitels m ON m.Mobitel_id = msg.Mobitel_id
                                JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
                                WHERE (msg.isNew = 1) AND (msg.Direction = 1) AND
                                (c.InternalMobitelConfig_ID = ( 
                                SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                FROM internalmobitelconfig intConf
                                WHERE (intConf.ID = c.ID)
                                ORDER BY intConf.InternalMobitelConfig_ID DESC)) AND
                                (c.devIdShort IS NOT NULL)";
                }

                return "";
            }
        }

        public static string SelectCoalesceMessage
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return
                            "SELECT COALESCE(DriverMessage, '') AS Message FROM ConfigDriverMessage WHERE Message_ID = ?MessageId";

                    case MssqlUse:
                        return
                            "SELECT COALESCE(DriverMessage, '') AS Message FROM ConfigDriverMessage WHERE Message_ID = @MessageId";
                }

                return "";
            }
        }

        public static string SelectGpsMask
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT * FROM GpsMasks WHERE message_id = ?MessageId";

                    case MssqlUse:
                        return "SELECT * FROM GpsMasks WHERE message_id = @MessageId";
                }

                return "";
            }
        }

        public static string SelectCoalesceDispatcher
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT COALESCE(dsptId, '') AS DispatcherID, " +
                               "COALESCE(pswd, '') AS Password, " +
                               "COALESCE(tmpPswd, '') AS TmpPassword " +
                               "FROM ConfigUnique " +
                               "WHERE Message_ID = ?MessageId " +
                               "LIMIT 1";

                    case MssqlUse:
                        return "SELECT TOP 1 COALESCE(dsptId, '') AS DispatcherID, " +
                               " COALESCE(pswd, '') AS Password, " +
                               " COALESCE(tmpPswd, '') AS TmpPassword " +
                               "FROM ConfigUnique " +
                               "WHERE Message_ID = @MessageId";
                }

                return "";
            }
        }

        public static string SelectConfigSms
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT COALESCE(cfg.DsptEmail, '') AS DsptEmailSMS, " +
                               " COALESCE(cfg.DsptGprsEmail, '') AS DsptEmailGprs, " +
                               " COALESCE(num1.SmsNumber, '') AS SmsCentre, " +
                               " COALESCE(num2.SmsNumber, '') AS SmsDspt, " +
                               " COALESCE(num3.SmsNumber, '') AS SmsEmailGate " +
                               "FROM ConfigSms cfg LEFT JOIN SmsNumbers num1 ON num1.SmsNumber_ID = cfg.SmsCenter_ID  " +
                               " LEFT JOIN SmsNumbers num2 ON num2.SmsNumber_ID = cfg.SmsDspt_ID  " +
                               " LEFT JOIN SmsNumbers num3 ON num3.SmsNumber_ID = cfg.SmsEmailGate_ID " +
                               "WHERE cfg.Message_ID = ?MessageId " +
                               "LIMIT 1";

                    case MssqlUse:
                        return
                            @"SELECT TOP 1 COALESCE(cfg.DsptEmail, '') AS DsptEmailSMS, COALESCE(cfg.DsptGprsEmail, '') AS DsptEmailGprs, 
                                   COALESCE(num1.SmsNumber, '') AS SmsCentre, 
                                   COALESCE(num2.SmsNumber, '') AS SmsDspt, 
                                   COALESCE(num3.SmsNumber, '') AS SmsEmailGate 
                                  FROM ConfigSms cfg LEFT JOIN SmsNumbers num1 ON num1.SmsNumber_ID = cfg.SmsCenter_ID  
                                   LEFT JOIN SmsNumbers num2 ON num2.SmsNumber_ID = cfg.SmsDspt_ID  
                                   LEFT JOIN SmsNumbers num3 ON num3.SmsNumber_ID = cfg.SmsEmailGate_ID 
                                  WHERE cfg.Message_ID = @MessageId";
                }

                return "";
            }
        }

        public static string SelectFromConfigTel
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT COALESCE(num1.TelNumber, '') AS NumberSOS, " +
                               " COALESCE(num2.TelNumber, '') AS NumberDspt, " +
                               " COALESCE(num3.TelNumber, '') AS NumberAccept1, " +
                               " COALESCE(num3.Name, '') AS Name1, " +
                               " COALESCE(num4.TelNumber, '') AS NumberAccept2, " +
                               " COALESCE(num4.Name, '') AS Name2, " +
                               " COALESCE(num5.TelNumber, '') AS NumberAccept3, " +
                               " COALESCE(num5.Name, '') AS Name3 " +
                               "FROM ConfigTel cfg LEFT JOIN TelNumbers num1 ON num1.TelNumber_ID = cfg.TelSOS  " +
                               " LEFT JOIN TelNumbers num2 ON num2.TelNumber_ID = cfg.TelDspt " +
                               " LEFT JOIN TelNumbers num3 ON num3.TelNumber_ID = cfg.telAccept1 " +
                               " LEFT JOIN TelNumbers num4 ON num4.TelNumber_ID = cfg.telAccept2 " +
                               " LEFT JOIN TelNumbers num5 ON num5.TelNumber_ID = cfg.telAccept3 " +
                               "WHERE cfg.ID = COALESCE((SELECT ConfigTel_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1) " +
                               "ORDER by ConfigTel_ID DESC LIMIT 1";

                    case MssqlUse:
                        return @"SELECT TOP 1 COALESCE(num1.TelNumber, '') AS NumberSOS,
                                   COALESCE(num2.TelNumber, '') AS NumberDspt,
                                   COALESCE(num3.TelNumber, '') AS NumberAccept1,
                                   COALESCE(num3.Name, '') AS Name1,
                                   COALESCE(num4.TelNumber, '') AS NumberAccept2,
                                   COALESCE(num4.Name, '') AS Name2,
                                   COALESCE(num5.TelNumber, '') AS NumberAccept3,
                                   COALESCE(num5.Name, '') AS Name3
                                   FROM ConfigTel cfg LEFT JOIN TelNumbers num1 ON num1.TelNumber_ID = cfg.TelSOS
                                   LEFT JOIN TelNumbers num2 ON num2.TelNumber_ID = cfg.TelDspt
                                   LEFT JOIN TelNumbers num3 ON num3.TelNumber_ID = cfg.telAccept1
                                   LEFT JOIN TelNumbers num4 ON num4.TelNumber_ID = cfg.telAccept2
                                   LEFT JOIN TelNumbers num5 ON num5.TelNumber_ID = cfg.telAccept3
                                  WHERE cfg.ID = COALESCE((SELECT TOP 1 ConfigTel_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1)
                                  ORDER by ConfigTel_ID DESC";
                }

                return "";
            }
        }

        public static string SelectFromInternalMobitelConfig
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT * FROM InternalMobitelConfig WHERE Message_ID = ?MessageId";

                    case MssqlUse:
                        return "SELECT * FROM InternalMobitelConfig WHERE Message_ID = @MessageId";
                }

                return "";
            }
        }

        public static string SelectFromConfigGprsInit
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT Domain, InitString FROM ConfigGprsInit WHERE Message_ID = ?MessageId";

                    case MssqlUse:
                        return "SELECT Domain, InitString FROM ConfigGprsInit WHERE Message_ID = @MessageId";
                }

                return "";
            }
        }

        public static string SelectFromConfigGprsEmail
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT Smtpserv, Smtpun, Smtppw, " +
                               " Pop3serv, Pop3un, Pop3pw " +
                               "FROM ConfigGprsEmail " +
                               "WHERE ID = COALESCE((SELECT ConfigGprsEmail_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1) " +
                               "ORDER by ConfigGprsEmail_ID DESC LIMIT 1";

                    case MssqlUse:
                        return @"SELECT TOP 1 Smtpserv, Smtpun, Smtppw, 
                                Pop3serv, Pop3un, Pop3pw 
                                FROM ConfigGprsEmail 
                                WHERE ID = COALESCE((SELECT TOP 1 ConfigGprsEmail_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1)
                                ORDER by ConfigGprsEmail_ID DESC";
                }

                return "";
            }
        }

        public static string SelectFromConfigGprsMain
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT Mode, Apnserv, Apnun, Apnpw, " +
                               "Dnsserv1, Dialn1, Ispun, Isppw " +
                               "FROM ConfigGprsMain " +
                               "WHERE ID = COALESCE((SELECT ConfigGprsMain_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1) " +
                               "ORDER by ConfigGprsMain_ID DESC LIMIT 1";

                    case MssqlUse:
                        return @"SELECT TOP 1 Mode, Apnserv, Apnun, Apnpw, 
                                Dnsserv1, Dialn1, Ispun, Isppw
                                FROM ConfigGprsMain
                                WHERE ID = COALESCE((SELECT TOP 1 ConfigGprsMain_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1)
                                ORDER by ConfigGprsMain_ID DESC";
                }

                return "";
            }
        }

        public static string UpdateServiceinit
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "UPDATE serviceinit SET A1 = ?A1, A2 = ?A2, A3 = ?A3, A4 = ?A4 WHERE ServiceType_ID = 3";

                    case MssqlUse:
                        return "UPDATE serviceinit SET A1 = @A1, A2 = @A2, A3 = @A3, A4 = @A4 WHERE ServiceType_ID = 3";
                }

                return "";
            }
        }

        public static string UpdateMobitels
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "UPDATE mobitels SET Name = ?NewName, Descr = ?NewDescr WHERE Mobitel_ID = ?MobitelID";

                    case MssqlUse:
                        return "UPDATE mobitels SET Name = @NewName, Descr = @NewDescr WHERE Mobitel_ID = @MobitelID";
                }

                return "";
            }
        }

        public static string OnDeleteLostRange
        {
            get
            {
                return "OnDeleteLostRange";
            }
        }

        public static string MobitelID
        {
            get
            {
                return "MobitelID";
            }
        }

        public static string BeginSrvPacketID
        {
            get
            {
                return "BeginSrvPacketID";
            }
        }

        public static string UpdateMessages
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "UPDATE Messages SET AdvCounter = ?ZoneResult WHERE Message_ID = ?ZoneMsgId";
                        ;

                    case MssqlUse:
                        return "UPDATE Messages SET AdvCounter = @ZoneResult WHERE Message_ID = @ZoneMsgId";
                }

                return "";
            }
        }

        public static string UpdateMessagesID
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "UPDATE Messages " +
                               "SET IsNew = 0, IsSend = 1, Time3 = Unix_Timestamp(?Time), Source_ID = ?SourceId " +
                               "WHERE Message_ID = ?MessageId";

                    case MssqlUse:
                        return "UPDATE Messages " +
                               "SET IsNew = 0, IsSend = 1, Time3 = Unix_Timestamp(@Time), Source_ID = @SourceId " +
                               "WHERE Message_ID = @MessageId";
                }

                return "";
            }
        }

        public static string InsertIntoConfigUnique
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO ConfigUnique (Name, Descr, Message_ID, pswd, ID) " +
                               "VALUES (?Name, ?Descr, ?Message_ID, ?pswd, " +
                               "(COALESCE((SELECT ConfigUnique_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1)))";

                    case MssqlUse:
                        return "INSERT INTO ConfigUnique (Name, Descr, Message_ID, pswd, ID) " +
                               "VALUES (@Name, @Descr, @Message_ID, @pswd, " +
                               "(COALESCE((SELECT TOP 1 ConfigUnique_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1)))";
                }

                return "";
            }
        }

        public static string UpdateMessagesSetIsNew
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "UPDATE messages " +
                               "SET isNew = 0, DataFromService = 'Error Settings' " +
                               "WHERE Message_ID = ?MessageId";

                    case MssqlUse:
                        return "UPDATE messages " +
                               "SET isNew = 0, DataFromService = 'Error Settings' " +
                               "WHERE Message_ID = @MessageId";
                }

                return "";
            }
        }

        public static string InsertIntoConfigGprsInit
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO ConfigGprsInit (Name, Descr, Message_ID, Domain, InitString, ID) " +
                               "VALUES (?Name, ?Descr, ?Message_ID, ?Domain, ?InitString, " +
                               "COALESCE((SELECT ConfigGprsInit_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

                    case MssqlUse:
                        return "INSERT INTO ConfigGprsInit (Name, Descr, Message_ID, Domain, InitString, ID) " +
                               "VALUES (@Name, @Descr, @Message_ID, @Domain, @InitString, " +
                               "COALESCE((SELECT TOP 1 ConfigGprsInit_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1))";
                }

                return "";
            }
        }

        public static string UpdateMessagesDelivered
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "UPDATE Messages " +
                               "SET isDelivered = 1, Time4 = Unix_Timestamp(?Time) " +
                               "WHERE Message_ID = ?MessageId";

                    case MssqlUse:
                        return "UPDATE Messages " +
                               "SET isDelivered = 1, Time4 = Unix_Timestamp(@Time) " +
                               "WHERE Message_ID = @MessageId";
                }

                return "";
            }
        }

        public static string InsertIntoConfigEvent
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO ConfigEvent (Name, Descr, Message_ID, " +
                               " tmr1Log, tmr2Send, tmr3Zone, dist1Log, dist2Send, dist3Zone,  " +
                               " gprsEmail, gprsFtp, gprsSocket,  " +
                               " maskEvent1, maskEvent2, maskEvent3, maskEvent4, maskEvent5, maskEvent6, " +
                               " maskEvent7, maskEvent8, maskEvent9, maskEvent10, maskEvent11, maskEvent12, " +
                               " maskEvent13, maskEvent14, maskEvent15, maskEvent16, maskEvent17, maskEvent18, " +
                               " maskEvent19, maskEvent20, maskEvent21, maskEvent22, maskEvent23, maskEvent24, " +
                               " maskEvent25, maskEvent26, maskEvent27, maskEvent28, maskEvent29, maskEvent30, " +
                               " maskEvent31, deltaTimeZone, maskSensor, ID) " +
                               " VALUES (?Name, ?Descr, ?Message_ID, " +
                               " ?tmr1Log, ?tmr2Send, ?tmr3Zone, ?dist1Log, ?dist2Send, ?dist3Zone, " +
                               " ?gprsEmail, ?gprsFtp, ?gprsSocket, " +
                               " ?maskEvent1, ?maskEvent2, ?maskEvent3, ?maskEvent4, ?maskEvent5, ?maskEvent6, " +
                               " ?maskEvent7, ?maskEvent8, ?maskEvent9, ?maskEvent10, ?maskEvent11, ?maskEvent12, " +
                               " ?maskEvent13, ?maskEvent14, ?maskEvent15, ?maskEvent16, ?maskEvent17, ?maskEvent18, " +
                               " ?maskEvent19, ?maskEvent20, ?maskEvent21, ?maskEvent22, ?maskEvent23, ?maskEvent24, " +
                               " ?maskEvent25, ?maskEvent26, ?maskEvent27, ?maskEvent28, ?maskEvent29, ?maskEvent30, " +
                               " ?maskEvent31, ?deltaTimeZone, ?maskSensor,  " +
                               " (COALESCE((SELECT ConfigEvent_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))) ";

                    case MssqlUse:
                        return "INSERT INTO ConfigEvent (Name, Descr, Message_ID, " +
                               " tmr1Log, tmr2Send, tmr3Zone, dist1Log, dist2Send, dist3Zone,  " +
                               " gprsEmail, gprsFtp, gprsSocket,  " +
                               " maskEvent1, maskEvent2, maskEvent3, maskEvent4, maskEvent5, maskEvent6, " +
                               " maskEvent7, maskEvent8, maskEvent9, maskEvent10, maskEvent11, maskEvent12, " +
                               " maskEvent13, maskEvent14, maskEvent15, maskEvent16, maskEvent17, maskEvent18, " +
                               " maskEvent19, maskEvent20, maskEvent21, maskEvent22, maskEvent23, maskEvent24, " +
                               " maskEvent25, maskEvent26, maskEvent27, maskEvent28, maskEvent29, maskEvent30, " +
                               " maskEvent31, deltaTimeZone, maskSensor, ID) " +
                               " VALUES (@Name, @Descr, @Message_ID, " +
                               " @tmr1Log, @tmr2Send, @tmr3Zone, @dist1Log, @dist2Send, @dist3Zone, " +
                               " @gprsEmail, @gprsFtp, @gprsSocket, " +
                               " @maskEvent1, @maskEvent2, @maskEvent3, @maskEvent4, @maskEvent5, @maskEvent6, " +
                               " @maskEvent7, @maskEvent8, @maskEvent9, @maskEvent10, @maskEvent11, @maskEvent12, " +
                               " @maskEvent13, @maskEvent14, @maskEvent15, @maskEvent16, @maskEvent17, @maskEvent18, " +
                               " @maskEvent19, @maskEvent20, @maskEvent21, @maskEvent22, @maskEvent23, @maskEvent24, " +
                               " @maskEvent25, @maskEvent26, @maskEvent27, @maskEvent28, @maskEvent29, @maskEvent30, " +
                               " @maskEvent31, @deltaTimeZone, @maskSensor,  " +
                               " (COALESCE((SELECT TOP 1 ConfigEvent_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1))) ";
                }

                return "";
            }
        }

        public static string InsertIntoConfigSms
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT Into configsms (Name, Descr, ID) " +
                               "VALUES (?Name, ?Descr, COALESCE((SELECT max(ConfigSms_ID) + 1 from configsms c), 0)) ";

                    case MssqlUse:
                        return "INSERT Into configsms (Name, Descr, ID) " +
                               "VALUES (@Name, @Descr, COALESCE((SELECT max(ConfigSms_ID) + 1 from configsms c), 0)) ";
                }

                return "";
            }
        }

        public static string InsertIntoConfiggprsmain
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT Into configgprsmain (Name, Descr, ID) " +
                               "VALUES (?Name, ?Descr, COALESCE((SELECT max(ConfigGprsMain_ID) + 1 from configgprsmain c), 0)) ";

                    case MssqlUse:
                        return "INSERT Into configgprsmain (Name, Descr, ID) " +
                               "VALUES (@Name, @Descr, COALESCE((SELECT max(ConfigGprsMain_ID) + 1 from configgprsmain c), 0)) ";
                }

                return "";
            }
        }

        public static string InsertIntoConfiggprsemail
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT Into configgprsemail (Name, Descr, ID) " +
                               "VALUES (?Name, ?Descr, COALESCE((SELECT max(ConfigGprsEmail_ID) + 1 from configgprsemail c), 0)) ";

                    case MssqlUse:
                        return "INSERT Into configgprsemail (Name, Descr, ID) " +
                               "VALUES (@Name, @Descr, COALESCE((SELECT max(ConfigGprsEmail_ID) + 1 from configgprsemail c), 0)) ";
                }

                return "";
            }
        }

        public static string UpdateMessagesIsDelivered
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "UPDATE Messages " +
                               "SET isDelivered = 1, Time4 = Unix_Timestamp(?Time), " +
                               "DataFromService = 'Teletrack reported about ERROR' " +
                               "WHERE Message_ID = ?MessageId";

                    case MssqlUse:
                        return "UPDATE Messages " +
                               "SET isDelivered = 1, Time4 = Unix_Timestamp(@Time), " +
                               "DataFromService = 'Teletrack reported about ERROR' " +
                               "WHERE Message_ID = @MessageId";
                }

                return "";
            }
        }

        public static string InsertIntoGpsmasks
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return
                            "INSERT INTO gpsmasks (Name, Descr, Mobitel_ID, Message_ID, LastRecords, WhatSend, MaxSmsNum ) " +
                            "VALUES (?Name, ?Descr, ?Mobitel_ID, ?Message_ID, ?LastRecords, ?WhatSend, ?MaxSmsNum )";

                    case MssqlUse:
                        return
                            "INSERT INTO gpsmasks (Name, Descr, Mobitel_ID, Message_ID, LastRecords, WhatSend, MaxSmsNum ) " +
                            "VALUES (@Name, @Descr, @Mobitel_ID, @Message_ID, @LastRecords, @WhatSend, @MaxSmsNum )";
                }

                return "";
            }
        }

        public static string InsertIntoConfigGprsEmail
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO ConfigGprsEmail (Name, Descr, Message_ID, " +
                               " Smtpserv, Smtpun, Smtppw, Pop3serv, Pop3un, Pop3pw, ID) " +
                               "VALUES (?Name, ?Descr, ?Message_ID,  " +
                               " ?Smtpserv, ?Smtpun, ?Smtppw, ?Pop3serv, ?Pop3un, ?Pop3pw,  " +
                               " COALESCE((SELECT ConfigGprsEmail_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

                    case MssqlUse:
                        return "INSERT INTO ConfigGprsEmail (Name, Descr, Message_ID, " +
                               " Smtpserv, Smtpun, Smtppw, Pop3serv, Pop3un, Pop3pw, ID) " +
                               "VALUES (@Name, @Descr, @Message_ID,  " +
                               " @Smtpserv, @Smtpun, @Smtppw, @Pop3serv, @Pop3un, @Pop3pw,  " +
                               " COALESCE((SELECT TOP 1 ConfigGprsEmail_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1))";
                }

                return "";
            }
        }

        public static string InsertIntoServicesend
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO servicesend (Name, Descr, ID) " +
                               "VALUES (?Name, ?Descr, COALESCE((SELECT max(ServiceSend_ID) + 1 from servicesend c), 0))";

                    case MssqlUse:
                        return "INSERT INTO servicesend (Name, Descr, ID) " +
                               "VALUES (@Name, @Descr, COALESCE((SELECT max(ServiceSend_ID) + 1 from servicesend c), 0))";
                }

                return "";
            }
        }

        public static string InsertIntoServicesendCoalesce
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO servicesend (Name, Descr, ID, telMobitel) " +
                               "VALUES (?Name, ?Descr, COALESCE((SELECT m.ServiceSend_ID FROM mobitels m WHERE m.Mobitel_ID = ?Mobitel), 0), " +
                               "COALESCE((SELECT TelNumber_ID from telnumbers t WHERE TelNumber = ?telMobitel), 0))";

                    case MssqlUse:
                        return "INSERT INTO servicesend (Name, Descr, ID, telMobitel) " +
                               "VALUES (@Name, @Descr, COALESCE((SELECT m.ServiceSend_ID FROM mobitels m WHERE m.Mobitel_ID = @Mobitel), 0), " +
                               "COALESCE((SELECT TelNumber_ID from telnumbers t WHERE TelNumber = @telMobitel), 0))";
                }

                return "";
            }
        }

        public static string InsertIntoConfigTel
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO ConfigTel (Name, Descr, ID) " +
                               "VALUES (?Name, ?Descr, COALESCE((SELECT max(ConfigTel_ID) + 1 from ConfigTel c), 0)) ";

                    case MssqlUse:
                        return "INSERT INTO ConfigTel (Name, Descr, ID) " +
                               "VALUES (@Name, @Descr, COALESCE((SELECT max(ConfigTel_ID) + 1 from ConfigTel c), 0)) ";
                }

                return "";
            }
        }

        public static string InsertIntoTelNumbers
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO TelNumbers (Name, TelNumber) VALUES (?Name, ?TelNumber)";

                    case MssqlUse:
                        return "INSERT INTO TelNumbers (Name, TelNumber) VALUES (@Name, @TelNumber)";
                }

                return "";
            }
        }

        public static string SelectFromTelnumbers
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT t.TelNumber FROM telnumbers t " +
                               "WHERE t.TelNumber_ID = COALESCE((" +
                               "SELECT s.telMobitel FROM servicesend s WHERE s.ID = COALESCE((" +
                               "SELECT m.ServiceSend_ID FROM mobitels m WHERE m.Mobitel_ID = ?MobitelId LIMIT 1), 0) ORDER by s.ServiceSend_ID DESC LIMIT 1), 0) ";

                    case MssqlUse:
                        return "SELECT t.TelNumber FROM telnumbers t " +
                               "WHERE t.TelNumber_ID = COALESCE((" +
                               "SELECT TOP 1 s.telMobitel FROM servicesend s WHERE s.ID = COALESCE((" +
                               "SELECT TOP 1 m.ServiceSend_ID FROM mobitels m WHERE m.Mobitel_ID = @MobitelId), 0) ORDER by s.ServiceSend_ID DESC), 0) ";
                }

                return "";
            }
        }

        public static string UpdateMessagesOne
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "UPDATE Messages " +
                               "SET IsNew = 0, Source_ID = 0  " +
                               "WHERE (Mobitel_ID = ?MobitelId) AND (Command_id = ?CommandId) AND " +
                               "  (IsNew = 1) AND (Direction = 1) AND (IsSend = 0) AND (MessageCounter < ?MessageCounter)";

                    case MssqlUse:
                        return "UPDATE Messages " +
                               "SET IsNew = 0, Source_ID = 0  " +
                               "WHERE (Mobitel_ID = @MobitelId) AND (Command_id = @CommandId) AND " +
                               "  (IsNew = 1) AND (Direction = 1) AND (IsSend = 0) AND (MessageCounter < @MessageCounter)";
                }

                return "";
            }
        }

        public static string InsertIntoConfigsms
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT Into configsms (Name, Descr, Message_ID, " +
                               "smsCenter_ID, smsDspt_ID, smsEmailGate_ID, dsptEmail, dsptGprsEmail, ID) " +
                               "VALUES (?Name, ?Descr, ?Message_ID, " +
                               "(COALESCE((SELECT SmsNumber_ID FROM SmsNumbers WHERE smsNumber = ?SmsCenterNumber LIMIT 1), -1)), " +
                               "(COALESCE((SELECT SmsNumber_ID FROM SmsNumbers WHERE smsNumber = ?SmsDsptNumber LIMIT 1), -1)), " +
                               "(COALESCE((SELECT SmsNumber_ID FROM SmsNumbers WHERE smsNumber = ?SmsEmailGateNumber LIMIT 1), -1)), " +
                               "?dsptEmail, ?dsptGprsEmail, " +
                               "(COALESCE((SELECT ConfigSms_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1)))";

                    case MssqlUse:
                        return "INSERT Into configsms (Name, Descr, Message_ID, " +
                               "smsCenter_ID, smsDspt_ID, smsEmailGate_ID, dsptEmail, dsptGprsEmail, ID) " +
                               "VALUES (@Name, @Descr, @Message_ID, " +
                               "(COALESCE((SELECT TOP 1 SmsNumber_ID FROM SmsNumbers WHERE smsNumber = @SmsCenterNumber), -1)), " +
                               "(COALESCE((SELECT TOP 1 SmsNumber_ID FROM SmsNumbers WHERE smsNumber = @SmsDsptNumber), -1)), " +
                               "(COALESCE((SELECT TOP 1 SmsNumber_ID FROM SmsNumbers WHERE smsNumber = @SmsEmailGateNumber), -1)), " +
                               "@dsptEmail, @dsptGprsEmail, " +
                               "(COALESCE((SELECT TOP 1 ConfigSms_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1)))";
                }

                return "";
            }
        }

        public static string InsertIntoInternalMobitelConfig
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO InternalMobitelConfig (Name, Descr, Message_ID, " +
                               " devIdShort, devIdLong, verProtocolShort, verProtocolLong, " +
                               " moduleIdGps, moduleIdGsm, moduleIdRf, moduleIdSs, moduleIdMm, ID) " +
                               "VALUES (?Name, ?Descr, ?Message_ID, " +
                               " ?devIdShort, ?devIdLong, ?verProtocolShort, ?verProtocolLong, " +
                               " ?moduleIdGps, ?moduleIdGsm, ?moduleIdRf, ?moduleIdSs, ?moduleIdMm, " +
                               " COALESCE((SELECT InternalMobitelConfig_ID FROM Mobitels WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

                    case MssqlUse:
                        return "INSERT INTO InternalMobitelConfig (Name, Descr, Message_ID, " +
                               " devIdShort, devIdLong, verProtocolShort, verProtocolLong, " +
                               " moduleIdGps, moduleIdGsm, moduleIdRf, moduleIdSs, moduleIdMm, ID) " +
                               "VALUES (@Name, @Descr, @Message_ID, " +
                               " @devIdShort, @devIdLong, @verProtocolShort, @verProtocolLong, " +
                               " @moduleIdGps, @moduleIdGsm, @moduleIdRf, @moduleIdSs, @moduleIdMm, " +
                               " COALESCE((SELECT TOP 1 InternalMobitelConfig_ID FROM Mobitels WHERE Mobitel_ID = @MobitelId), -1))";
                }

                return "";
            }
        }

        public static string InsertIntoConfigGprsMain
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO ConfigGprsMain (Name, Descr, Message_ID, " +
                               " Mode, Apnserv, Apnun, Apnpw, Dnsserv1, Dialn1, Ispun, Isppw, ID)  " +
                               "VALUES (?Name, ?Descr, ?Message_ID,  " +
                               " ?Mode, ?Apnserv, ?Apnun, ?Apnpw, ?Dnsserv1, ?Dialn1, ?Ispun, ?Isppw, " +
                               " COALESCE((SELECT ConfigGprsMain_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

                    case MssqlUse:
                        return "INSERT INTO ConfigGprsMain (Name, Descr, Message_ID, " +
                               " Mode, Apnserv, Apnun, Apnpw, Dnsserv1, Dialn1, Ispun, Isppw, ID)  " +
                               "VALUES (@Name, @Descr, @Message_ID,  " +
                               " @Mode, @Apnserv, @Apnun, @Apnpw, @Dnsserv1, @Dialn1, @Ispun, @Isppw, " +
                               " COALESCE((SELECT TOP 1 ConfigGprsMain_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1))";
                }

                return "";
            }
        }

        public static string InsertIntoMobitelsValue
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO configmain (Name, Descr, Mobitel_ID, ConfigEvent_ID, " +
                               " ConfigTel_ID, ConfigSms_ID, ConfigGprsMain_ID, ConfigGprsEmail_ID, " +
                               " ConfigOther_ID, ConfigDriverMessage_ID, ConfigUnique_ID, ConfigZoneSet_ID, " +
                               " ConfigSensorSet_ID, ConfigGprsInit_ID) " +
                               "VALUES (?Name, ?Descr, ?Mobitel_ID, ?ConfigEvent_ID, " +
                               " ?ConfigTel_ID, ?ConfigSms_ID, ?ConfigGprsMain_ID, ?ConfigGprsEmail_ID, " +
                               " ?ConfigOther_ID, ?ConfigDriverMessage_ID, ?ConfigUnique_ID, ?ConfigZoneSet_ID, " +
                               " ?ConfigSensorSet_ID, ?ConfigGprsInit_ID) ";

                    case MssqlUse:
                        return "INSERT INTO configmain (Name, Descr, Mobitel_ID, ConfigEvent_ID, " +
                               " ConfigTel_ID, ConfigSms_ID, ConfigGprsMain_ID, ConfigGprsEmail_ID, " +
                               " ConfigOther_ID, ConfigDriverMessage_ID, ConfigUnique_ID, ConfigZoneSet_ID, " +
                               " ConfigSensorSet_ID, ConfigGprsInit_ID) " +
                               " VALUES (@Name, @Descr, @Mobitel_ID, @ConfigEvent_ID, " +
                               " @ConfigTel_ID, @ConfigSms_ID, @ConfigGprsMain_ID, @ConfigGprsEmail_ID, " +
                               " @ConfigOther_ID, @ConfigDriverMessage_ID, @ConfigUnique_ID, @ConfigZoneSet_ID, " +
                               " @ConfigSensorSet_ID, @ConfigGprsInit_ID) ";
                }

                return "";
            }
        }

        public static string InsertConfigEvent
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO ConfigEvent (Name, Descr, ID) " +
                               "VALUES (?Name, ?Descr, " +
                               "COALESCE((SELECT max(ConfigEvent_ID) + 1 from ConfigEvent c), 0)) ";

                    case MssqlUse:
                        return "INSERT INTO ConfigEvent (Name, Descr, ID) " +
                               "VALUES (@Name, @Descr, " +
                               "COALESCE((SELECT max(ConfigEvent_ID) + 1 from ConfigEvent c), 0)) ";
                }

                return "";
            }
        }

        public static string InsertConfigmain
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO configmain (Name, Descr, Mobitel_ID, ConfigEvent_ID, " +
                               " ConfigTel_ID, ConfigSms_ID, ConfigGprsMain_ID, ConfigGprsEmail_ID, " +
                               " ConfigOther_ID, ConfigDriverMessage_ID, ConfigUnique_ID, ConfigZoneSet_ID, " +
                               " ConfigSensorSet_ID, ConfigGprsInit_ID) " +
                               " VALUES (?Name, ?Descr, ?Mobitel_ID, ?ConfigEvent_ID, " +
                               " ?ConfigTel_ID, ?ConfigSms_ID, ?ConfigGprsMain_ID, ?ConfigGprsEmail_ID, " +
                               " ?ConfigOther_ID, ?ConfigDriverMessage_ID, ?ConfigUnique_ID, ?ConfigZoneSet_ID, " +
                               " ?ConfigSensorSet_ID, ?ConfigGprsInit_ID) ";

                    case MssqlUse:
                        return "INSERT INTO configmain (Name, Descr, Mobitel_ID, ConfigEvent_ID, " +
                               " ConfigTel_ID, ConfigSms_ID, ConfigGprsMain_ID, ConfigGprsEmail_ID, " +
                               " ConfigOther_ID, ConfigDriverMessage_ID, ConfigUnique_ID, ConfigZoneSet_ID, " +
                               " ConfigSensorSet_ID, ConfigGprsInit_ID) " +
                               " VALUES (@Name, @Descr, @Mobitel_ID, @ConfigEvent_ID, " +
                               " @ConfigTel_ID, @ConfigSms_ID, @ConfigGprsMain_ID, @ConfigGprsEmail_ID, " +
                               " @ConfigOther_ID, @ConfigDriverMessage_ID, @ConfigUnique_ID, @ConfigZoneSet_ID, " +
                               " @ConfigSensorSet_ID, @ConfigGprsInit_ID) ";
                }

                return "";
            }
        }

        public static string InsertIntoMobitels
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return " INSERT INTO ConfigTel (Name, Descr, Message_ID, " +
                               " telSOS, telDspt, telAccept1, telAccept2, telAccept3, ID) " +
                               " VALUES (?Name, ?Descr, ?Message_ID,  " +
                               " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telSOS LIMIT 1), -1)), " +
                               " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telDspt LIMIT 1), -1)), " +
                               " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telAccept1 LIMIT 1), -1)), " +
                               " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telAccept2 LIMIT 1), -1)), " +
                               " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telAccept3 LIMIT 1), -1)), " +
                               " (COALESCE((SELECT ConfigTel_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))) ";

                    case MssqlUse:
                        return " INSERT INTO ConfigTel (Name, Descr, Message_ID, " +
                               " telSOS, telDspt, telAccept1, telAccept2, telAccept3, ID) " +
                               " VALUES (@Name, @Descr, @Message_ID,  " +
                               " (COALESCE((SELECT TOP 1 TelNumber_ID FROM Telnumbers WHERE TelNumber = @telSOS), -1)), " +
                               " (COALESCE((SELECT TOP 1 TelNumber_ID FROM Telnumbers WHERE TelNumber = @telDspt), -1)), " +
                               " (COALESCE((SELECT TOP 1 TelNumber_ID FROM Telnumbers WHERE TelNumber = @telAccept1), -1)), " +
                               " (COALESCE((SELECT TOP 1 TelNumber_ID FROM Telnumbers WHERE TelNumber = @telAccept2), -1)), " +
                               " (COALESCE((SELECT TOP 1 TelNumber_ID FROM Telnumbers WHERE TelNumber = @telAccept3), -1)), " +
                               " (COALESCE((SELECT TOP 1 ConfigTel_ID FROM ConfigMain WHERE Mobitel_ID = @MobitelId), -1))) ";
                }

                return "";
            }
        }

        public static string InsertIntoSources
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO Sources (SourceText, SourceError) VALUES (?SourceText, ?SourceError)";

                    case MssqlUse:
                        return "INSERT INTO Sources (SourceText, SourceError) VALUES (@SourceText, @SourceError)";
                }

                return "";
            }
        }

        public static string UpdateID
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "UPDATE InternalMobitelConfig SET ID=?LastID WHERE InternalMobitelConfig_ID = ?LastID";

                    case MssqlUse:
                        return "UPDATE InternalMobitelConfig SET ID=@LastID WHERE InternalMobitelConfig_ID = @LastID";
                }

                return "";
            }
        }

        public static string InsertInternalMobitelConfig
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO InternalMobitelConfig (Name, Descr, Message_ID, " +
                               " devIdShort, devIdLong, verProtocolShort, verProtocolLong, " +
                               " moduleIdGps, moduleIdGsm, moduleIdRf, moduleIdSs, moduleIdMm, ID) " +
                               "VALUES (?Name, ?Descr, ?Message_ID, " +
                               " ?devIdShort, ?devIdLong, ?verProtocolShort, ?verProtocolLong, " +
                               " ?moduleIdGps, ?moduleIdGsm, ?moduleIdRf, ?moduleIdSs, ?moduleIdMm, 0)";

                    case MssqlUse:
                        return "INSERT INTO InternalMobitelConfig (Name, Descr, Message_ID, " +
                               " devIdShort, devIdLong, verProtocolShort, verProtocolLong, " +
                               " moduleIdGps, moduleIdGsm, moduleIdRf, moduleIdSs, moduleIdMm, ID) " +
                               "VALUES (@Name, @Descr, @Message_ID, " +
                               " @devIdShort, @devIdLong, @verProtocolShort, @verProtocolLong, " +
                               " @moduleIdGps, @moduleIdGsm, @moduleIdRf, @moduleIdSs, @moduleIdMm, 0)";
                }

                return "";
            }
        }

        public static string InsertMessages
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "INSERT INTO Messages " +
                               "(Time1, Time2, isNew, Direction, Command_ID, Source_ID, " +
                               "Address, DataFromService, Mobitel_ID, crc, MessageCounter) " +
                               "VALUES (?Time1, ?Time2, ?isNew, ?Direction, ?Command_ID, ?Source_ID, " +
                               "?Address, ?DataFromService, ?Mobitel_ID, ?crc, ?MessageCounter)";

                    case MssqlUse:
                        return "INSERT INTO Messages " +
                               "(Time1, Time2, isNew, Direction, Command_ID, Source_ID, " +
                               "Address, DataFromService, Mobitel_ID, crc, MessageCounter) " +
                               "VALUES (@Time1, @Time2, @isNew, @Direction, @Command_ID, @Source_ID, " +
                               "@Address, @DataFromService, @Mobitel_ID, @crc, @MessageCounter)";
                }

                return "";
            }
        }

        public static string SelectMobitel_ID
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return "SELECT m.Mobitel_ID AS MobitelID " +
                               "FROM mobitels m JOIN internalmobitelconfig c  " +
                               "  ON (c.ID = m.InternalMobitelConfig_ID) " +
                               "WHERE (c.InternalMobitelConfig_ID = ( " +
                               "  SELECT intConf.InternalMobitelConfig_ID " +
                               "  FROM internalmobitelconfig intConf " +
                               "  WHERE (intConf.ID = c.ID)  " +
                               "  ORDER BY intConf.InternalMobitelConfig_ID DESC " +
                               "  LIMIT 1)) AND (c.devIdShort = ?devIdShort) ";

                    case MssqlUse:
                        return "SELECT m.Mobitel_ID AS MobitelID " +
                               "FROM mobitels m JOIN internalmobitelconfig c  " +
                               "  ON (c.ID = m.InternalMobitelConfig_ID) " +
                               "WHERE (c.InternalMobitelConfig_ID = ( " +
                               "  SELECT TOP 1 intConf.InternalMobitelConfig_ID " +
                               "  FROM internalmobitelconfig intConf " +
                               "  WHERE (intConf.ID = c.ID)  " +
                               "  ORDER BY intConf.InternalMobitelConfig_ID DESC " +
                               "  )) AND (c.devIdShort = @devIdShort) ";
                }

                return "";
            }
        }

        public static string DeleteInternalMobitelConfig
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return
                            "DELETE c FROM internalmobitelconfig c JOIN mobitels m ON m.InternalMobitelConfig_ID = c.ID " +
                            "WHERE m.Mobitel_ID = ?MobitelID";

                    case MssqlUse:
                        return
                            "DELETE c FROM internalmobitelconfig c JOIN mobitels m ON m.InternalMobitelConfig_ID = c.ID " +
                            "WHERE m.Mobitel_ID = @MobitelID";
                }

                return "";
            }
        }

        public static string DeleteFromMobitels
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return
                            "DELETE FROM mobitels WHERE Mobitel_ID = ?MobitelID";

                    case MssqlUse:
                        return
                            "DELETE FROM mobitels WHERE Mobitel_ID = @MobitelID";
                }

                return "";
            }
        }

        public static string OnTransferBuffer
        {
            get
            {
                return "OnTransferBuffer";
            }
        }

        public static string SelectCountMessage_Id
        {
            get
            {
                switch( TypeDataBaseForUsing )
                {
                    case MySqlUse:
                        return
                            "SELECT COUNT(msg.Message_ID) " +
                            "FROM Messages msg JOIN Mobitels m ON m.Mobitel_id = msg.Mobitel_id " +
                            " JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID) " +
                            "WHERE (msg.isNew = 1) AND (msg.Direction = 1) AND " +
                            " (c.InternalMobitelConfig_ID = ( " +
                            "   SELECT intConf.InternalMobitelConfig_ID " +
                            "   FROM internalmobitelconfig intConf " +
                            "   WHERE (intConf.ID = c.ID)  " +
                            "   ORDER BY intConf.InternalMobitelConfig_ID DESC " +
                            "   LIMIT 1)) AND " +
                            " (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')";

                    case MssqlUse:
                        return
                            "SELECT COUNT(msg.Message_ID) " +
                            "FROM Messages msg JOIN Mobitels m ON m.Mobitel_id = msg.Mobitel_id " +
                            " JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID) " +
                            "WHERE (msg.isNew = 1) AND (msg.Direction = 1) AND " +
                            " (c.InternalMobitelConfig_ID = ( " +
                            "   SELECT TOP 1 intConf.InternalMobitelConfig_ID " +
                            "   FROM internalmobitelconfig intConf " +
                            "   WHERE (intConf.ID = c.ID)  " +
                            "   ORDER BY intConf.InternalMobitelConfig_ID DESC " +
                            "   )) AND " +
                            " (c.devIdShort IS NOT NULL)";
                }

                return "";
            }
        }

        public static string InsertIntoDatagpsBuffer_on
        {
            get
            {
                switch (TypeDataBaseForUsing)
                {
                    case MySqlUse:
                        return @"INSERT INTO datagpsbuffer_on 
		                (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Speed, Direction, Altitude,Valid,
		                Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,  
		                `Events`, whatIs, SrvPacketID) VALUES ";
                    case MssqlUse:
                        return @"INSERT INTO datagpsbuffer_on 
		                (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Speed, Direction, Altitude,Valid,
		                Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,  
		                [Events], whatIs, SrvPacketID) VALUES ";
                }

                return "";
            }
        }

        public static string InsertIntoNewMobitels
        {
            get
            {
                switch( TypeDataBaseForUsing )
                {
                    case MySqlUse:
                        return "INSERT INTO mobitels (Name, Descr, InternalMobitelConfig_ID, " +
                               " MapStyleLine_ID, MapStyleLastPoint_ID, MapStylePoint_ID, ServiceSend_ID, ConfirmedID, LastInsertTime, Is64bitPackets, IsNotDrawDgps) " +
                               "VALUES (?Name, ?Descr, ?InternalMobitelConfig_ID, " +
                               " ?MapStyleLine_ID, ?MapStyleLastPoint_ID, ?MapStylePoint_ID, ?ServiceSend_ID, ?ConfirmedID, ?LastInsertTime, ?Is64bitPackets, ?IsNotDrawDgps)";

                    case MssqlUse:
                        return "INSERT INTO mobitels (Name, Descr, InternalMobitelConfig_ID, " +
                               " MapStyleLine_ID, MapStyleLastPoint_ID, MapStylePoint_ID, ServiceSend_ID) " +
                               "VALUES (@Name, @Descr, @InternalMobitelConfig_ID, " +
                               " @MapStyleLine_ID, @MapStyleLastPoint_ID, @MapStylePoint_ID, @ServiceSend_ID, @ConfirmedID, @LastInsertTime, @Is64bitPackets, @IsNotDrawDgps)";
                }

                return "";
            }
        }
    }
}
