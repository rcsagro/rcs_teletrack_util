﻿using System;
using System.Text;
using TeletrackTuning.Error;
using TeletrackTuning.Entity;

namespace TeletrackTuning
{
  /// <summary>
  /// Пакет MainData32 пришедший от телетрека
  /// </summary>
  public class DataGpsInfo
  {
    private string mobitelID;
    /// <summary>
    /// Идентификатор телетрека MobitelID
    /// </summary>
    public string MobitelID
    {
      get { return mobitelID; }
      set { mobitelID = value; }
    }

    private string logID;
    /// <summary>
    /// Идентификатор записи
    /// </summary>
    public string LogID
    {
      get { return logID; }
      set { logID = value; }
    }

    private string unixTime;
    /// <summary>
    /// Время в формате UnixTime
    /// </summary>
    public string UnixTime
    {
      get { return unixTime; }
      set { unixTime = value; }
    }

    private string latitude;
    /// <summary>
    /// Щирота
    /// </summary>
    public string Latitude
    {
      get { return latitude; }
      set { latitude = value; }
    }

    private string longitude;
    /// <summary>
    /// Долгота
    /// </summary>
    public string Longitude
    {
      get { return longitude; }
      set { longitude = value; }
    }

    private string altitude;
    /// <summary>
    /// Высота
    /// </summary>
    public string Altitude
    {
      get { return altitude; }
      set { altitude = value; }
    }

    private string speed;
    /// <summary>
    /// Скорость
    /// </summary>
    public string Speed
    {
      get { return speed; }
      set { speed = value; }
    }

    private string direction;
    /// <summary>
    /// Напрвление
    /// </summary>
    public string Direction
    {
      get { return direction; }
      set { direction = value; }
    }

    private string valid;
    /// <summary>
    /// Валидность
    /// </summary>
    public string Valid
    {
      get { return valid; }
      set { valid = value; }
    }

    private string sensor1_8;
    /// <summary>
    /// Сенсоры с 1 по 8
    /// </summary>
    public string Sensor1_8
    {
      get { return sensor1_8; }
      set { sensor1_8 = value; }
    }

    private string events;
    /// <summary>
    /// События
    /// </summary>
    public string Events
    {
      get { return events; }
      set { events = value; }
    }

    /// <summary>
    /// Значение для поля WhatIs = 1023
    /// </summary>
    public const string DEFAULT_WHATIS_VALUE = "1023";

    public string WhatIs
    {
      get { return DEFAULT_WHATIS_VALUE; }
    }

    /// <summary>
    /// Разделитель запятая
    /// </summary>
    public const string DELIMITER = ",";

    /// <summary>
    /// Собирает строку готовую для подстановки в команду 
    /// INSERT INTO DataGps
    /// </summary>
    /// <returns>Строка готовая для подстановки в команду INSERT INTO DataGps</returns>
    public string GetAssembledRow()
    {
      return String.Concat(
        MobitelID, DELIMITER,
        LogID, DELIMITER,
        UnixTime, DELIMITER,
        Latitude, DELIMITER,
        Longitude, DELIMITER,
        Speed, DELIMITER,
        Direction, DELIMITER,
        Altitude, DELIMITER,
        Valid, DELIMITER,
        Sensor1_8, DELIMITER,
        Events, DELIMITER,
        WhatIs);
    }

    /// <summary>
    /// Собирает дебажную строку для записи в лог
    /// </summary>
    /// <returns>Дебажная строка для записи в лог</returns>
    public string GetDebugRow()
    {
      return String.Concat(
        "MobitelID=", MobitelID, "; ",
        "LogID=", LogID, "; ",
        "Valid=", Valid, "; ",
        "Latitude=", Latitude, "; ",
        "Longitude=", Longitude, "; ",
        "Speed=", Speed, "; ",
        "UnixTime=", UnixTime, "; ",
        "Sensor1_8=", Sensor1_8, "; ",
        "Direction=", Direction, "; ",
        "Altitude=", Altitude, "; ",
        "Events=", Events, "; ",
        "WhatIs=", WhatIs);
    }
  } // struct DebugPacketInfo



  /// <summary>
  /// Персер данных для вставки в DataGps
  /// </summary>
  public static class DataGpsParser
  {
    /// <summary>
    /// Кол-во байт в DataGPS пакете
    /// </summary>
    private const int DATA_GPS_BYTE_COUNT = 32;
    /// <summary>
    /// Пустое значение = NULL
    /// </summary>
    public const string NULL_VALUE = "NULL";
    /// <summary>
    /// Максимальное абсолютное значение долготы = 108000000 (180)
    /// </summary>
    private const int MAX_LONGITUDE = 108000000;
    /// <summary>
    /// Максимальное абсолютное значение широты = 54000000 (90)
    /// </summary>
    private const int MAX_LATITUDE = 54000000;
    /// <summary>
    /// Максимальное значение скорости = 110 миль в час (примерно 200 км/ч)
    /// </summary>
    private const int MAX_SPEED = 110;
    /// <summary>
    /// Минимальное значение скорости = 0
    /// </summary>
    private const int MIN_SPEED = 0;

    /// <summary>
    /// Формирование сроки для INSERTа в таблицу DataGPSBuffer.
    /// </summary>
    /// <param name="mobitelID">Mobitel ID</param>
    /// <param name="packet">Packet который надо парсить (32 байта)</param>
    /// <returns>String</returns>
    public static DataGpsInfo GetDataGpsRow(int mobitelID, params byte[] packet)
    {
      if (packet.Length != DATA_GPS_BYTE_COUNT)
        throw new ArgumentOutOfRangeException("byte[] PacketToParse",
         String.Format(Error.ErrorText.PARSING_UNEXPECTED_LENGTH_ARRAY,
         "GetDataGpsRow", "DataGpsParser", Convert.ToString(DATA_GPS_BYTE_COUNT),
         Convert.ToString(packet.Length)));

      DataGpsInfo result = new DataGpsInfo();

      // MobitelID
      result.MobitelID = Convert.ToString(mobitelID);
      // LogID
      result.LogID = Convert.ToString(BitConverter.ToInt32(packet, 0));
      // UnixTime
      result.UnixTime = Convert.ToString(BitConverter.ToInt32(packet, 4));
      // Latitude
      int latitude = BitConverter.ToInt32(packet, 8) * 10;
      result.Latitude = Convert.ToString(latitude);
      // Longitude
      int longitude = BitConverter.ToInt32(packet, 12) * 10;
      result.Longitude = Convert.ToString(longitude);
      // Speed
      int speed = Convert.ToInt32(packet[16]);
      result.Speed = Convert.ToString(speed);
      // Direction
      result.Direction = Convert.ToString(packet[17]);
      // Altitude
      result.Altitude = Convert.ToString(packet[18]);

      // Valid Дополнительная проверка
      if ((Math.Abs(longitude) > MAX_LONGITUDE) ||
          (Math.Abs(latitude) > MAX_LATITUDE) ||
          (speed > MAX_SPEED) || (speed < MIN_SPEED))
        result.Valid = "0";
      else
        result.Valid = Convert.ToString(Convert.ToByte((packet[19] & 0x08) != 0));

      // Sensor1 .. Sensor8
      StringBuilder sensors = new StringBuilder();
      for (int sensor = 20; sensor < 28; sensor++)
      {
        if (sensors.Length > 0)
          sensors.Append(DataGpsInfo.DELIMITER);

        sensors.Append(Convert.ToString(packet[sensor]));
      } // for (sensor)
      result.Sensor1_8 = sensors.ToString();

      // Events
      result.Events = Convert.ToString(
        (int)(packet[30] << 16 | packet[29] << 8 | packet[28]));

      return result;
    } // GetDataGpsRows(mobitelID, packet, logID)

    /// <summary>
    /// Формирование сроки для INSERTа в таблицу DataGPSBuffer.
    /// </summary>
    /// <param name="mobitelID"></param>
    /// <param name="dataGps"></param>
    /// <returns></returns>
    public static DataGpsInfo GetDataGpsRow(int mobitelID, DataGps dataGps)
    {
      DataGpsInfo result = new DataGpsInfo();
      result.MobitelID = Convert.ToString(mobitelID);
      result.LogID = Convert.ToString(dataGps.LogID);
      result.UnixTime = Convert.ToString(dataGps.Time);
      result.Latitude = Convert.ToString(dataGps.Latitude*10);//добавление последнего нуля
      result.Longitude = Convert.ToString(dataGps.Longitude*10);// -//-
      result.Speed = Convert.ToString(dataGps.Speed);
      result.Direction = Convert.ToString(dataGps.Direction);
      result.Altitude = Convert.ToString(dataGps.Altitude);

      // Valid Дополнительная проверка
      if ((Math.Abs(dataGps.Longitude * 10) > MAX_LONGITUDE) ||
          (Math.Abs(dataGps.Latitude * 10) > MAX_LATITUDE) ||
          (dataGps.Speed > MAX_SPEED) || (dataGps.Speed < MIN_SPEED))
        result.Valid = "0";
      else
        result.Valid = Convert.ToString(dataGps.Valid == true ? 1:0);

      result.Sensor1_8 = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", dataGps.Sensor1, dataGps.Sensor2, dataGps.Sensor3, dataGps.Sensor4, dataGps.Sensor5, dataGps.Sensor6, dataGps.Sensor7, dataGps.Sensor8);
      result.Events = Convert.ToString(dataGps.Events);
      return result;
    }
  }
} 
