using System.Collections.Generic;

namespace TeletrackTuning.SrvCommands
{
  /// <summary>
  /// ��������� ������������������ ������.
  /// <para>� ���������� �������� ���������
  /// ����� ��������� MainController'�,
  /// � ������� ������������ ������������ �������
  /// </para>
  /// </summary>
  public static class CommandDescriptor
  {
    private static Dictionary<int, string> commands;

    /// <summary>
    /// ������������� ����������� � MainController'� ���������� �������� 
    /// </summary>
    /// <param name="cmdDescription">Dictionary(int, string) � ������������������� ���������</param>
    public static void Init(Dictionary<int, string> cmdDescription)
    {
      commands = cmdDescription;
    }
  
    /// <summary>
    /// ���������������� �� �������
    /// </summary>
    /// <param name="cmdCode">��� �������</param>
    /// <returns>Bool</returns>
    public static bool IsCommandExist(int cmdCode)
    {
      if (commands.ContainsKey(cmdCode))
        return true;
      else
        return false;
    }

    /// <summary>
    /// ���������� ��������� �������
    /// </summary>
    /// <param name="cmdCode">Cmd code</param>
    /// <returns>String</returns>
    public static string GetDescription(int cmdCode)
    {
      if (commands.ContainsKey(cmdCode))
        return commands[cmdCode];
      else
        return "UNKNOWN COMMAND";
    }
  }
}
