using System;
//using RCS.Sockets.Connection;
//using RCS.TDataMngr.Config;
//using RCS.TDataMngr.OnlineClient.DataManagement;
//using RCS.TDataMngr.OnlineClient.ErrorHandling;
//using RCS.TDataMngr.OnlineClient.Logging;
//using RCS.TDataMngr.OnlineClient.Util;
using System.Windows.Forms;
using TeletrackTuning.Properties;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvCommands
{
  /// <summary>
  /// Базовый класс команд
  /// </summary>
  public class SrvCommand : ISrvCommand
  {
    /// <summary>
    /// Статистика
    /// </summary>
    //private Statistics statistics;

    /// <summary>
    /// Код команды 
    /// </summary>
    public int CommandID
    {
      get { return GetCommandID(); }
    }

    protected virtual int GetCommandID()
    {
      throw new NotImplementedException(String.Format(
        ErrorText.NOT_IMPLEMENTED, "GetCommandID", "SrvCommand"));
    }

    /// <summary>
    /// Символьный описатель команды
    /// </summary>
    public string CommandDescr
    {
      get { return GetCommandDescr(); }
    }

    protected virtual string GetCommandDescr()
    {
      throw new NotImplementedException(String.Format(
        ErrorText.NOT_IMPLEMENTED, "GetCommandDescr", "SrvCommand"));
    }

    #region Ответ

    /// <summary>
    /// Код ответа
    /// </summary>
    protected virtual int GetResponseCode()
    {
      throw new NotImplementedException(String.Format(
        ErrorText.NOT_IMPLEMENTED, "GetResponseCode", "SrvCommand"));
    }

    /// <summary>
    /// Статус ответа
    /// </summary>
    protected ExecutionStatus responseStatus;

    #endregion

    private SrvCommand() { }

    //public SrvCommand(BaseDataManager dataMngr, ConnectionInfo connectionContext)
    //{
    //  SetDataManager(dataMngr);

    //  //statistics = new Statistics();
    //  //statistics.StartTime = DateTime.Now;
    //}

    //private BaseDataManager dataMngr;

    ///// <summary>
    ///// Обязательная установка менеджера данных
    ///// </summary>
    ///// <param name="dataMngr">BaseDataManager</param>
    //protected virtual void SetDataManager(BaseDataManager dataMngr)
    //{
    //  this.dataMngr = dataMngr;
    //}

    /// <summary>
    /// Выполнить команду
    /// </summary>
    /// <returns>Response info</returns>
    public void Execute()
    {
      try
      {
        InternalExecute();
        FinallCommit();
      }
      catch (Exception Ex)
      {
        MessageBox.Show(Ex.Message, "Exception Execute");//ErrorHandler.Handle(Ex);
      }
    }

    /// <summary>
    /// Производит ли команда вставку новых данных
    /// </summary>
    /// <returns>Bool</returns>
    public virtual bool IsInsertFunc()
    {
      return false;
    }

    /// <summary>
    /// Основная обработка выполнения команды
    /// </summary>
    protected virtual void InternalExecute()
    {
      throw new NotImplementedException(String.Format(
        ErrorText.NOT_IMPLEMENTED, "InternalExecute", "SrvCommand"));
    }

    /// <summary>
    /// Получить тело ответа
    /// </summary>
    /// <returns>byte[]</returns>
    protected virtual byte[] GetResponseBody()
    {
      throw new NotImplementedException(String.Format(
        ErrorText.NOT_IMPLEMENTED, "GetResponseBody", "SrvCommand"));
    }

    /// <summary>
    /// Последние действия 
    /// </summary>
    protected virtual void FinallCommit()
    {
      //statistics.FinishTime = DateTime.Now;
    }

    /// <summary>
    /// Парсинг строки новых данных
    /// </summary>
    /// <param name="mobitelID">Mobitel ID</param>
    /// <param name="packet">Пакет DataGps - массив байт</param>
    /// <param name="devIdShort">короткий идентификатор - для дебага</param>
    /// <param name="extFields">Дополнительные поля, которые надо пристыковать к строке DataGps</param>
    /// <param name="parsedRow">распарсенная строка</param>
    /// <returns>True если все в порядке</returns>
    //protected bool GetParsedRow(int mobitelID, byte[] packet, string devIdShort, string[] extFields, out string parsedRow)
    //{
    //  if (packet == null)
    //  {
    //    throw new ArgumentNullException(String.Format(AppError.ARGUMENT_NULL,
    //      "GetParsedRow", "SrvCommand", "byte[] packet"));
    //  }
    //  if (String.IsNullOrEmpty(devIdShort))
    //  {
    //    throw new ArgumentNullException(String.Format(AppError.ARGUMENT_NULL,
    //      "GetParsedRow", "SrvCommand", "string devIdShort"));
    //  }
    //  bool result = true;
    //  parsedRow = "";
    //  try
    //  {
    //    DataGpsInfo dataGps = DataGpsParser.GetDataGpsRow(mobitelID, packet);
    //    parsedRow = dataGps.GetAssembledRow();
    //    // Добавление дополнительных полей
    //    if ((extFields != null) && (extFields.Length > 0))
    //    {
    //      foreach (string field in extFields)
    //      {
    //        parsedRow = String.Concat(parsedRow, DataGpsInfo.DELIMITER, field);
    //      }
    //    }
    //    if (LogCfg.GetConfig().Debug)
    //    {
    //      const string received_msg = "Пакет от телетрека {0}({1}) принят для сохранения в БД {2}: \r\n  {3}";
    //      string tmp = dataGps.GetDebugRow();
    //      // Добавление дополнительных полей
    //      if ((extFields != null) && (extFields.Length > 0))
    //      {
    //        foreach (string field in extFields)
    //        {
    //          tmp = String.Concat(tmp, "; ", field);
    //        }
    //      }
    //      Logger.Write(String.Format(received_msg, mobitelID, devIdShort,
    //        dataMngr.ConnectionString, tmp));
    //    }
    //  }
    //  catch (Exception Ex)
    //  {
    //    result = false;
    //    ErrorHandler.Handle(Ex);
    //  }
    //  return result;
    //}
  }
}
