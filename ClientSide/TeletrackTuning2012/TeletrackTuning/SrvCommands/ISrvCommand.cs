//using RCS.Sockets.Connection;

namespace TeletrackTuning.SrvCommands
{
  /// <summary>
  /// ��������� ����� ��������� �������
  /// </summary>
  public interface ISrvCommand
  {
    /// <summary>
    /// ��� �������
    /// </summary>
    int CommandID { get; }
    /// <summary>
    /// ���������� ��������� �������
    /// </summary>
    string CommandDescr { get; }
    /// <summary>
    /// ���������� �� ������� ������� ����� ������
    /// </summary>
    bool IsInsertFunc();
    /// <summary>
    /// ��������� �������
    /// </summary>
    void Execute();
  }
}
