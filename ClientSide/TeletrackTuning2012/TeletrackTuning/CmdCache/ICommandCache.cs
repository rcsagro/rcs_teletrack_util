using TeletrackTuning.SrvDBCommand;

namespace TeletrackTuning.Cache
{
  /// <summary>
  /// ��� ������.
  /// </summary>
  public interface ICommandCache
  {
    /// <summary>
    /// �������� DB �������
    /// </summary>
    /// <typeparam name="T">��� �������</typeparam>
    /// <returns>DbCommand</returns>
    ISrvDbCommand GetDbCommand<T>() where T : SrvDbCommand;
    /// <summary>
    /// ������ ����.
    /// </summary>
    int Size { get; }
  }
}
