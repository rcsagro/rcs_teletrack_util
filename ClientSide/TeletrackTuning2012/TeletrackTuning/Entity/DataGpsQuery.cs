using System;
using TeletrackTuning.Error;

namespace TeletrackTuning.Entity
{
  /// <summary>
  /// ������ GPS ������. �������������� ������: 36
  /// </summary>
  public class DataGpsQuery : CommonDescription
  {
    /// <summary>
    /// �����, �� ������� ������������, ����� �������� ����� ������������ ��� ���������� ������� ��� ������ ���������.
    /// ����������� ������� ���:
    /// 0 - ��������� ������
    /// 1 - ��������� �������
    /// 2 - ��������� �����
    /// 3 - ������������ ���-�� ���������
    /// 4 - �������
    /// 5 - �������
    /// 6 - ������
    /// 7 - ������ 
    /// 8 - ������
    /// 9 - ������
    /// 10 - �������� �� �������
    /// 11 - �������� �� ��������
    /// 12 - �������� �� ������
    /// 13 - �������� �� �������
    /// 14 - �������� �� ������
    /// 15 - �������� �� �����������
    /// 16 - �������� �� ��������
    /// 17 - �����
    /// </summary>
    public uint CheckMask
    {
      get { return checkMask; }
      set { checkMask = value; }
    }
    private uint checkMask;

    /// <summary>
    /// ������������� ��� ������� ����� �������, 
    /// �.�. � ������ ����� ��������������� � ������� �� 
    /// ����(�������), ������������ ������� ������� ���������
    /// </summary>
    public uint Events
    {
      get { return events; }
      set { events = value; }
    }
    private uint events;

    /// <summary>
    /// ���-�� ��������� �������, ������� ������� ����������.
    /// </summary>
    public uint LastRecords
    {
      get { return lastRecords; }
      set { lastRecords = value; }
    }
    private uint lastRecords;

    /// <summary>
    /// ���-�� ��������� ������, ������� ���� 
    /// ���������� � ��� �����
    /// </summary>
    public uint LastTimes
    {
      get { return lastTimes; }
      set { lastTimes = value; }
    }
    private uint lastTimes;

    /// <summary>
    /// ���������������
    /// </summary>
    public uint LastMeters
    {
      get { return lastMeters; }
    }
    private uint lastMeters;

    /// <summary>
    /// MAX ���-�� ���������, 
    /// ������� ����� ���� ���������� ��� ����� �� ������
    /// </summary>
    public uint MaxSmsNumber
    {
      get { return maxSmsNumber; }
      set { maxSmsNumber = value; }
    }
    private uint maxSmsNumber;

    /// <summary>
    /// �����, � ������� �������, ����� ���� ��������� GPS ������  ������� ��������.
    /// ����������� ������� ��� �����:
    /// 0 - �����
    /// 1 - ������
    /// 2 - �������
    /// 3 - ������
    /// 4 - �����������
    /// 5 - ��������
    /// 6 - ������ ���� (������ ����� 1)
    /// 7 - �����
    /// 8 - �������
    /// 9 - �������
    /// 10 - ��������
    /// </summary>
    public uint WhatSend
    {
      get { return whatSend; }
      set { whatSend = value; }
    }
    private uint whatSend;

    /// <summary>
    /// ������ ��������� �������, UnixTime
    /// </summary>
    public int StartTime
    {
      get { return startTime; }
      set { startTime = value; }
    }
    private int startTime;

    /// <summary>
    /// ������ ��������� ��������, ���� � ���
    /// </summary>
    public byte StartSpeed
    {
      get { return startSpeed; }
      set { startSpeed = value; }
    }
    private byte startSpeed;

    /// <summary>
    /// ������ ��������� ������, ������� * 600000
    /// </summary>
    public int StartLatitude
    {
      get { return startLatitude; }
      set { startLatitude = value; }
    }
    private int startLatitude;

    /// <summary>
    /// ������ ��������� �������, ������� * 600000
    /// </summary>
    public int StartLongitude
    {
      get { return startLongitude; }
      set { startLongitude = value; }
    }
    private int startLongitude;

    /// <summary>
    /// ������ ��������� ������, ����� 
    /// </summary>
    public byte StartAltitude
    {
      get { return startAltitude; }
      set { startAltitude = value; }
    }
    private byte startAltitude;

    /// <summary>
    /// ������ ��������� ����������� ��������, ������� / 1.5
    /// </summary>
    public byte StartDirection
    {
      get { return startDirection; }
      set { startDirection = value; }
    }
    private byte startDirection;

    /// <summary>
    /// ������ ��������� ������� ����
    /// </summary>
    public int StartLogId
    {
      get { return startLogId; }
      set { startLogId = value; }
    }
    private int startLogId;

    /// <summary>
    /// ���������������
    /// </summary>
    public byte StartFlags
    {
      get { return startFlags; }
    }
    private byte startFlags;

    /// <summary>
    /// ����� ��������� �������, UnixTime
    /// </summary>
    public int EndTime
    {
      get { return endTime; }
      set { endTime = value; }
    }
    private int endTime;

    /// <summary>
    /// ����� ��������� ��������, ���� � ���
    /// </summary>
    public byte EndSpeed
    {
      get { return endSpeed; }
      set { endSpeed = value; }
    }
    private byte endSpeed;

    /// <summary>
    /// ����� ��������� ������, ������� * 600000
    /// </summary>
    public int EndLatitude
    {
      get { return endLatitude; }
      set { endLatitude = value; }
    }
    private int endLatitude;

    /// <summary>
    /// ����� ��������� �������, ������� * 600000
    /// </summary>
    public int EndLongitude
    {
      get { return endLongitude; }
      set { endLongitude = value; }
    }
    private int endLongitude;

    /// <summary>
    /// ����� ��������� ������, �����
    /// </summary>
    public byte EndAltitude
    {
      get { return endAltitude; }
      set { endAltitude = value; }
    }
    private byte endAltitude;

    /// <summary>
    /// ����� ��������� ����������� ��������, ������� / 1.5
    /// </summary>
    public byte EndDirection
    {
      get { return endDirection; }
      set { endDirection = value; }
    }
    private byte endDirection;

    /// <summary>
    /// ����� ��������� ������� ����
    /// </summary>
    public int EndLogId
    {
      get { return endLogId; }
      set { endLogId = value; }
    }
    private int endLogId;

    /// <summary>
    /// ���������������
    /// </summary>
    public byte EndFlags
    {
      get { return endFlags; }
    }
    private byte endFlags;

    /// <summary>
    /// ������ ���� �1, �������� �������� ������� ������� �� ���� 
    /// � ������ ���� ��������� ������� ����
    /// </summary>
    public int LogId1
    {
      get { return logId1; }
      set { logId1 = value; }
    }
    private int logId1;

    /// <summary>
    /// ������ ���� �2, �������� �������� ������� ������� �� ���� 
    /// � ������ ���� ��������� ������� ����
    /// </summary>
    public int LogId2
    {
      get { return logId2; }
      set { logId2 = value; }
    }
    private int logId2;

    /// <summary>
    /// ������ ���� �3, �������� �������� ������� ������� �� ���� 
    /// � ������ ���� ��������� ������� ����
    /// </summary>
    public int LogId3
    {
      get { return logId3; }
      set { logId3 = value; }
    }
    private int logId3;

    /// <summary>
    /// ������ ���� �4, �������� �������� ������� ������� �� ���� 
    /// � ������ ���� ��������� ������� ����
    /// </summary>
    public int LogId4
    {
      get { return logId4; }
      set { logId4 = value; }
    }
    private int logId4;

    /// <summary>
    /// ������ ���� �5 �������� �������� ������� ������� �� ���� 
    /// � ������ ���� ��������� ������� ����
    /// </summary>
    public int LogId5
    {
      get { return logId5; }
      set { logId5 = value; }
    }
    private int logId5;

    /// <summary>
    /// �����������
    /// </summary>
    public DataGpsQuery()
    {
      lastMeters = 0;
      startFlags = 0;
      endFlags = 0;
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;

      if (Math.Abs(StartLongitude) > Const.MAX_LONGITUDE)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "StartLongitude(������ ��������� �������)",
          "DataGpsQuery(������ GPS ������)",
          Const.MAX_LONGITUDE * -1, Const.MAX_LONGITUDE);
        return false;
      }
      if (Math.Abs(StartLatitude) > Const.MAX_LATITUDE)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "StartLatitude(������ ��������� ������)",
          "DataGpsQuery(������ GPS ������)",
          Const.MAX_LATITUDE * -1, Const.MAX_LATITUDE);
        return false;
      }

      if (Math.Abs(EndLongitude) > Const.MAX_LONGITUDE)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "EndLongitude(����� ��������� �������)",
          "DataGpsQuery(������ GPS ������)",
          Const.MAX_LONGITUDE * -1, Const.MAX_LONGITUDE);
        return false;
      }
      if (Math.Abs(EndLatitude) > Const.MAX_LATITUDE)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "EndLatitude(����� ��������� ������)",
          "DataGpsQuery(������ GPS ������)",
          Const.MAX_LATITUDE * -1, Const.MAX_LATITUDE);
        return false;
      }
      return true;
    }
  }
}
