using System;
using TeletrackTuning.Error;

namespace TeletrackTuning.Entity
{
  /// <summary>
  /// ������������ �������.
  /// �������������� ������: 13, 23, 43
  /// </summary>
  public class EventConfig : CommonDescription
  {
    /// <summary>        
    /// ���������� �������� ��������� �������� �� ���� �������, � ��/�.
    /// ������� ��������� ��� ����������� �������� ��� ��� ��������� 
    /// ��� � ��� ����������.
    /// ��� ����� �������� �� ���������� �����. 
    /// ���� �������� ������� �� ����, �� ��� ����������� ��������� 
    /// ������ � ��� ������������ ����� � �� ���� ��������� ������� �����������. 
    /// ���� �������� ����� ���� - ������� �������� �� ������������.
    /// </summary>
    public sbyte SpeedChange
    {
      get { return speedChange; }
      set { speedChange = value; }
    }
    private sbyte speedChange;

    /// <summary>
    /// ���������� �� ���������� �����, � ������. 
    /// �������� ������ ������� ������������ ���� ��������������. 
    /// ���� �������� ��������������, ���������� �� ���������,
    /// ����������� �� �������� ���-�� ������ - ��������� �������.
    /// <para>���������� �������� 1..100</para>
    /// </summary>
    public ushort CourseBend
    {
      get { return courseBend; }
      set { courseBend = value; }
    }
    private ushort courseBend;

    /// <summary>
    /// ��������� �1, �� ����������� ������� ������������ �������, � ������.
    /// <para>���������� �������� 100..65535</para>
    /// </summary>
    public ushort Distance1
    {
      get { return distance1; }
      set { distance1 = value; }
    }
    private ushort distance1;

    /// <summary>
    /// ��������� �2, �� ����������� ������� ������������ �������, � ������.
    /// <para>���������� �������� 100..65535</para>
    /// </summary>
    public ushort Distance2
    {
      get { return distance2; }
      set { distance2 = value; }
    }
    private ushort distance2;

    /// <summary>
    /// ������ ����� �������, ������ 32.
    /// ����� ������� ���������, ��� ���������� ������� ��� 
    /// ����������� ������� �������.
    /// ������� ������ ������������ ������ 21, � � ���� 31 ����.
    /// � ������ ������� ���������� ������� 32*16, � ������� ������������ 
    /// ����� �������� (16 ����) ������ ������������� �� ������ �� ������� (32 �����).
    /// <para>   
    /// <list type="table">
    /// <listheader><description>������ ����� ������� � �������</description></listheader>
    public const int BITMASK_EVENT_COUNT = 32;
    public const int INDEX_MIN_SPEED = 0;/// <item><term>0</term><description>����������� �������� (MinSpeed)</description></item>
    public const int INDEX_TIMER1 = 1;/// <item><term>1</term><description>������ 1 (Timer1)</description></item>
    public const int INDEX_TIMER2 = 2;/// <item><term>2</term><description>������ 2 (Timer2)</description></item>
    public const int INDEX_COURSE = 3;/// <item><term>3</term><description>���������� �� ����� (CourseBend)</description></item>
    public const int INDEX_DISTANCE1 = 4;/// <item><term>4</term><description>��������� 1 (Distance1)</description></item>
    public const int INDEX_DISTANCE2 = 5;/// <item><term>5</term><description>��������� 2 (Distance2)</description></item>
    public const int INDEX_ACCELERATION = 6;/// <item><term>6</term><description>������->ACC</description></item>
    public const int INDEX_POWER_ON = 7;/// <item><term>7</term><description>������� ��������</description></item>
    public const int INDEX_POWER_OFF = 8;/// <item><term>8</term><description>������� ���������</description></item>
    public const int INDEX_GSM_FIND = 9;/// <item><term>9</term><description>GSM ���������</description></item>
    public const int INDEX_GSM_LOST = 10;/// <item><term>10</term><description>GSM �������</description></item>
    public const int INDEX_LOG_FULL = 11;/// <item><term>11</term><description>��� ��������</description></item>
    /// <item><term>12</term><description>SMS ��������</description></item>
    public const int INDEX_RING_PHONE1 = 13;/// <item><term>13</term><description>������ � �������� 1</description></item>
    public const int INDEX_RING_PHONE2 = 14;/// <item><term>14</term><description>������ � �������� 2</description></item>
    public const int INDEX_RING_PHONE3 = 15;/// <item><term>15</term><description>������ � �������� 3</description></item>
    /// <item><term>16</term><description>������ SOS</description></item>
    public const int INDEX_RING_DISPETCHER = 17;/// <item><term>17</term><description>������ ����������</description></item>
    public const int INDEX_SENSORS = 18;/// <item><term>18</term><description>�������</description></item>
    public const int INDEX_ON = 19;/// <item><term>19</term><description>���������</description></item>
    public const int INDEX_REBOOT = 20;/// <item><term>20</term><description>�����</description></item>
    /// <item><term>21</term><description>������</description></item>
    /// <item><term>22</term><description>�����-����� �� ����������� ����</description></item>
    /// <item><term>23-31</term><description>������</description></item>
    
    /// </list>
    /// <para>
    /// <list type="table">
    /// <listheader><description>������ ����� - ������������ ����� ���������</description></listheader>
    public const ushort BIT_WRITE_LOG = 1 << 0;/// <item><term>0</term><description>������ � ���</description></item>
    public const ushort BIT_SEND_SMS = 1 << 1;/// <item><term>1</term><description>�������� SMS �� �������</description></item>
    public const ushort BIT_SEND_TO_SERVER = 1 << 2;/// <item><term>2</term><description>�������� GPRS �� Email</description></item>
    /// <item><term>3</term><description>�������� ������</description></item>
    /// <item><term>4</term><description>����������� �� ���</description></item>
    /// <item><term>5</term><description>������</description></item>
    /// <item><term>6</term><description>������</description></item>
    /// <item><term>7</term><description>������</description></item>
    /// <item><term>8</term><description>����� ����������</description></item>
    /// <item><term>9</term><description>SMS �� EMail</description></item>
    /// <item><term>10</term><description>������</description></item>
    /// <item><term>11</term><description>������</description></item>
    /// <item><term>12</term><description>������</description></item>
    /// <item><term>13</term><description>������</description></item>
    /// <item><term>14</term><description>������</description></item>
    /// <item><term>15</term><description>������</description></item>
    /// </list>
    /// </para></para>
    /// </summary>
    public ushort[] EventMask
    {
      get { return eventMask; }
      set { eventMask = value; }
    }
    private ushort[] eventMask;

    /// <summary>
    /// ����������� �������� ��������, � ��/�.
    /// ��� ����������� ����� �������� ��� ��� ������� 
    /// ��� � ��� ���������� ��������� �������.
    /// <para>���������� �������� 0..250</para>
    /// </summary>
    public ushort MinSpeed
    {
      get { return minSpeed; }
      set { minSpeed = value; }
    }
    private ushort minSpeed;

    /// <summary>
    /// ������ �1, �� ����������� ��������� ������� �������� ������������ �������. 
    /// <para>���������� �������� 1..65535</para>
    /// </summary>
    public ushort Timer1
    {
      get { return timer1; }
      set { timer1 = value; }
    }
    private ushort timer1;

    /// <summary>
    /// ������ �2, �� ����������� ��������� ������� �������� ������������ �������. 
    /// <para>���������� �������� 1..65535</para>
    /// </summary>
    public ushort Timer2
    {
      get { return timer2; }
      set { timer2 = value; }
    }
    private ushort timer2;

    /// <summary>
    /// ���������������
    /// </summary>
    public ushort GprsEmail
    {
      get { return gprsEmail; }
      set { gprsEmail = value; }
    }
    private ushort gprsEmail;

    /// <summary>
    /// �����������
    /// </summary>
    public EventConfig()
    {
      courseBend = 1;
      distance1 = 100;
      distance2 = 100;
      eventMask = new ushort[32];
      timer1 = 60;
      timer2 = 600;
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;

      if ((CourseBend < 1) || (CourseBend > 100))
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "CourseBend(���������� �� �����)",
          "EventConfig(������������ �������)",
          1, 100);
        return false;
      }

      if (Distance1 < 100)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "Distance1(��������� �1)",
          "EventConfig(������������ �������)",
          100, 65535);
        return false;
      }

      if (Distance2 < 100)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "Distance2(��������� �2)",
          "EventConfig(������������ �������)",
          100, 65535);
        return false;
      }

      if (EventMask.Length > 32)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "EventMask(������ ����� �������)",
          "EventConfig(������������ �������)",
          32);
        return false;
      }

      if (MinSpeed > 250)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "MinSpeed(����������� �������� ��������)",
          "EventConfig(������������ �������)",
          0, 250);
        return false;
      }

      if (Timer1 < 1)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "Timer1(������ �1)",
          "EventConfig(������������ �������)",
          1, 65535);
        return false;
      }

      if (Timer2 < 1)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "Timer2(������ �2)",
          "EventConfig(������������ �������)",
          1, 65535);
        return false;
      }
      return true;
    }
  }
}
