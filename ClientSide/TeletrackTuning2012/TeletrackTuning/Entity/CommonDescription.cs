using System;
using System.Reflection;
using System.Text;
using TeletrackTuning;

namespace TeletrackTuning.Entity
{
  /// <summary>
  /// ������� ����� ��� ���������� ������ ������������ ���������� � �������� ������.
  /// </summary>
  public class CommonDescription
  {
    /// <summary>
    /// �������� ������������� ���������
    /// </summary>
    public string ShortID
    {
      get { return shortID; }
      set { shortID = value; }
    }
    private string shortID;

    /// <summary>
    /// �����������. ���������� ������������� ���������,
    /// �������� ID � ���� ������. �� ��������� � ������
    /// �� ��������� A1.
    /// </summary>
    public int MobitelID
    {
      get { return mobitelID; }
      set { mobitelID = value; }
    }
    private int mobitelID;

    /// <summary>
    /// ������������� ���������. ������������� ���� MessageCounter 
    /// ������� Messages � ���� ������ �������� RCS. ��������� ����� ���������� ������� 
    /// ��������-��������� ��������� ��� ��������� ���������. ��� �������� 
    /// ��������� ��������� �� �������������� ������ ������� ��������� 
    /// ��� ������� ��������� ���������������� � ������������ � ��� ����. 
    /// �������� ����� ��������� ��������� ������� ����� � � ���� MessageID
    /// ����������� �������� MessageID ��������� ������.
    /// ������ ����� ��������� ������������� ������� ������������� �� ���������
    /// ����������� �������, ����� ��������� ��� ���. 
    /// ���� �������� ��������� �������� �������, � ������ �������� ������ 
    /// ���������� ���������, � ����� ������ ���� � ��������� �����.
    /// </summary>
    public ushort MessageID
    {
      get { return messageID; }
      set { messageID = value; }
    }
    private ushort messageID;

    /// <summary>
    /// ������������� ������������� ���� ������� - ����� ���������� ����������
    /// � ���������(���������, �������, GPS ������).
    /// </summary>
    public CommandDescriptor CommandID
    {
      get { return commandID; }
      set { commandID = value; }
    }
    private CommandDescriptor commandID;

    /// <summary>
    /// �������� ��������� � �������� ���� ������� ��� �������� ��������� 
    /// ��� �������� �� ���������
    /// </summary>
    public byte[] Source
    {
      get { return source; }
      set { source = value; }
    }
    private byte[] source;

    /// <summary>
    /// ��������� ������������� ���������, ������������ � Source
    /// </summary>
    public string Message
    {
      get { return message; }
      set { message = value; }
    }
    private string message;

    /// <summary>
    /// �����������
    /// </summary>
    public CommonDescription()
    {
      commandID = CommandDescriptor.Unknown;
      shortID = "";
      message = "";
    }

    /// <summary>
    /// ���������� ��������� �������� ���� ����� ������ ��� ��������� 
    /// �������� ���������.
    /// ���������� ����� �������������� ���� ����� � ������� ������������� 
    /// ��� ��� ����������.
    /// </summary>
    /// <returns>string</returns>
    public virtual string GetDebugInfo()
    {
      StringBuilder result = new StringBuilder();

      result.Append("���������� �� ������� ����: " + this.GetType().Name + ".\r\n");
      foreach (PropertyInfo prop in this.GetType().GetProperties())
      {
        result.Append(prop.Name).Append(": ");
        result.Append(prop.GetValue(this, null)).Append("; ");
      }
      return result.ToString();
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ������������� ������������ ��� ���������� ��������� ��������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ 
    /// ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.</returns>
    public virtual bool Validate(out string errorMessage)
    {
      throw new NotImplementedException("Not emplemented yet");
    }
  }
}
