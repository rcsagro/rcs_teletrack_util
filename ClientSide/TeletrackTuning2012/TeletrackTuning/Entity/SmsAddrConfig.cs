using System;
using TeletrackTuning.Error;

namespace TeletrackTuning.Entity
{
  /// <summary>
  /// ���������������� ��������� ��� ������� SMS, E-MAIL � 
  /// WEB SERVER (GPRS) ���������.
  /// �������������� ������: 11, 21, 41
  /// </summary>
  public class SmsAddrConfig : CommonDescription
  {
    /// <summary>
    /// Email ����� ����������, �� ������� ������������ ��������� �� ���������
    /// ����� ����� Email-GPRS.
    /// �������� 30 ��������.
    /// </summary>
    public string DsptEmailGprs
    {
      get { return dsptEmailGprs; }
      set { dsptEmailGprs = value; }
    }
    private string dsptEmailGprs;

    /// <summary>
    /// Email ����� ����������, �� ������� ������������ ��������� �� ���������
    /// ����� ����� Email-SMS.
    /// �������� 14 ��������.
    /// </summary>
    public string DsptEmailSMS
    {
      get { return dsptEmailSMS; }
      set { dsptEmailSMS = value; }
    }
    private string dsptEmailSMS;

    /// <summary>
    /// ����� SMS ������ ���������. ������������� ����� � GSM ������ ���������.
    /// �������� 11 ��������.
    /// </summary>
    public string SmsCentre
    {
      get { return smsCentre; }
      set { smsCentre = value; }
    }
    private string smsCentre;

    /// <summary>
    /// ����� �������� ����������, �� ������� ������������ ��������� 
    /// �� ��������� ����� ����� SMS-SMS.
    /// �������� 11 ��������.
    /// </summary>
    public string SmsDspt
    {
      get { return smsDspt; }
      set { smsDspt = value; }
    }
    private string smsDspt;

    /// <summary>
    /// ����� ����� ���������, �� ������� ������� ���������� SMS ���������
    /// �� ���������, ����� ����� ��� ���� ��������� �� Email.
    /// �������� 11 ��������.
    /// </summary>
    public string SmsEmailGate
    {
      get { return smsEmailGate; }
      set { smsEmailGate = value; }
    }
    private string smsEmailGate;

    /// <summary>
    /// �����������
    /// </summary>
    public SmsAddrConfig()
    {
      dsptEmailGprs = "";
      dsptEmailSMS = "";
      smsCentre = "";
      smsDspt = "";
      smsEmailGate = "";
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;

      if (DsptEmailGprs.Length > 30)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "DsptEmailGprs(Email ����� ���������� Email-GPRS)",
          "SmsAddrConfig(������������ SMS �������)",
          30);
        return false;
      } // if (DsptEmailGprs.Length)
      if (DsptEmailSMS.Length > 14)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "DsptEmailSMS(Email ����� ���������� Email-SMS)",
          "SmsAddrConfig(������������ SMS �������)",
          14);
        return false;
      }
      if (SmsCentre.Length > 11)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "SmsCentre(����� SMS ������ ���������)",
          "SmsAddrConfig(������������ SMS �������)",
          11);
        return false;
      }
      if (SmsDspt.Length > 11)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "SmsDspt(����� �������� ����������)",
          "SmsAddrConfig(������������ SMS �������)",
          11);
        return false;
      }
      if (SmsEmailGate.Length > 11)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "SmsEmailGate(����� ����� ���������)",
          "SmsAddrConfig(������������ SMS �������)",
          11);
        return false;
      }
      return true;
    }
  }
}
