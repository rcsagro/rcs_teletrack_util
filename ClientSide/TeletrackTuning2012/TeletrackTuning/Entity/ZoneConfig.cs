#region Using directives
using System;
using System.Collections.Generic;
using TeletrackTuning.Error;
#endregion

namespace TeletrackTuning.Entity
{
  /// <summary>
  /// ������������ ����������� ���. 
  /// ����������� ����� ������� 256 �����, 64 ����������� ����, 
  /// 64 ����� ��� ����������� ���.
  /// �������������� ������: 15
  /// </summary>
  public class ZoneConfig : CommonDescription
  {
    /// <summary>
    /// ���-�� ������� � �����������
    /// </summary>
    public static int PACKET_COUNT = 25;

    /// <summary>
    /// ������������� ��������� (�������� ������ ������� ����������)
    /// </summary>
    public int ZoneMsgID
    {
      get { return zoneMsgID; }
      set { zoneMsgID = value; }
    }
    private int zoneMsgID;

    /// <summary>
    /// ���������� ��� ������� ����� �������������� = 0..64
    /// </summary>
    public int ZoneCount
    {
      get { return zoneList.Count; }
    }

    /// <summary>
    /// ������ ��� ��������������� �� �������� ������������� �����(PointList), 
    /// �.� �������� ������ ������ ��������� ������ � ������������ �������. 
    /// ���������� �������� �������� ������������ ����� ������ ����� �� ������ 
    /// PointList, ������� �������� ��������� � �������� ����������� ����. 
    /// �.� ����� ������ ���� ��������� �� �������� �� ������� ����������� 
    /// � ���������� ���� (�� �������), �� ������� ���������� � �������� 
    /// ������� ���� (�������).
    /// ���-�� ��������� = 64.
    /// </summary>
    public List<Zone> ZoneList
    {
      get { return zoneList; }
    }
    private List<Zone> zoneList;

    /// <summary>
    /// 25 �������� ��������� ������� ��� �������� ���������.  
    /// </summary>
    public new List<byte[]> Source
    {
      get { return source; }
    }
    private List<byte[]> source;

    /// <summary>
    /// ��������� ������������� 25-� ���������, ������������ � Source
    /// </summary>
    public new List<string> Message
    {
      get { return message; }
    }
    private List<string> message;

    /// <summary>
    /// �����������
    /// </summary>
    public ZoneConfig()
    {
      zoneList = new List<Zone>();
      source = new List<byte[]>();
      message = new List<string>();
    }

    /// <summary>
    /// ���������� ����. ������������ ���-�� �������� ��� = 64
    /// </summary>
    /// <exception cref="ArgumentNullException">�� �������� ����������� ����</exception> 
    /// <exception cref="A1Exception">������������ ���-�� ����������� ���</exception> 
    /// <param name="zone">����������� ����</param>
    public void AddZone(Zone zone)
    {
      if (zone == null)
      {
        throw new ArgumentNullException("zone", "�� ������� �������� zone");
      }
      if (ZoneCount >= 64)
      {
        throw new A1Exception(ErrorText.ZONE_FULL);
      }
      zoneList.Add(zone);
    }

    /// <summary>
    /// ���������� ��������� ���������. ������������ ���-�� = 25
    /// </summary>
    /// <exception cref="ArgumentNullException">�� �������� �������� ���������</exception> 
    /// <exception cref="A1Exception">������������ ���-�� �������� ���������</exception> 
    /// <param name="source">�������� ��������� � �������� ����</param>
    internal void AddSource(byte[] source)
    {
      if (source == null)
      {
        throw new ArgumentNullException("source", "�� ������� �������� source");
      }
      if (this.source.Count >= 25)
      {
        throw new A1Exception("���������� ��������� �������� ��������� - ��������� ���-�� ��������� 25");
      }
      this.source.Add(source);
    }

    /// <summary>
    /// ���������� ��������� ��������� � ��������� ����. ������������ ���-�� = 25
    /// </summary>
    /// <exception cref="ArgumentNullException">�� �������� �������� ���������</exception> 
    /// <exception cref="A1Exception">������������ ���-�� �������� ���������</exception> 
    /// <param name="message">�������� ��������� � ��������� ����</param>
    internal void AddMessage(string message)
    {
      if (message == null)
      {
        throw new ArgumentNullException("message", "�� ������� �������� message");
      }
      if (this.message.Count >= 25)
      {
        throw new A1Exception("���������� ��������� �������� ��������� - ��������� ���-�� ��������� 25");
      }
      this.message.Add(message);
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;
      int pointCount = 0;

      foreach (Zone zone in zoneList)
      {
        pointCount += zone.Points.Length;

        if (pointCount > 256)
        {
          errorMessage = ErrorText.ZONE_POINT_COUNT;
          return false;
        }
      }
      return true;
    }
  }
}
