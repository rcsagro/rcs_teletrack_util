﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;
//using TeletrackTuning.TDBCommand;

namespace TeletrackTuning.SrvDBCommand
{
    internal class TransferBufferDBCommand : SrvDbCommand
    {
        private string PROC_NAME = QueryLayer.OnTransferBuffer;
        DriverDb driverDb = new DriverDb();

        public TransferBufferDBCommand(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.StoredProcedure;
            //MyDbCommand.CommandText = PROC_NAME;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.StoredProcedure );
            driverDb.CommandText( PROC_NAME );
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
