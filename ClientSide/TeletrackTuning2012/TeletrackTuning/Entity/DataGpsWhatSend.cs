namespace TeletrackTuning.Entity
{
  /// <summary>
  /// �������� ����� ����� WhatSend, WhatWrite,
  /// ������� ���������� ����� ������ ����������
  /// ���������� � ������ DataGps
  /// </summary>
  public enum DataGpsWhatSend
  {
    /// <summary>
    /// ����� = 0
    /// </summary>
    TIME = 0,
    /// <summary>
    /// ������ = 1
    /// </summary>
    LATITUDE = 1,
    /// <summary>
    /// ������� = 2
    /// </summary>
    LONGITUDE = 2,
    /// <summary>
    /// ������ = 3
    /// </summary>
    ALTITUDE = 3,
    /// <summary>
    /// ����������� = 4
    /// </summary>
    DIRECTION = 4,
    /// <summary>
    /// �������� = 5
    /// </summary>
    SPEED = 5,
    /// <summary>
    /// ������ ���� = 6
    /// </summary>
    LOGID = 6,
    /// <summary>
    /// ����� = 7
    /// </summary>
    FLAGS = 7,
    /// <summary>
    /// �������  =8
    /// </summary>
    EVENTS = 8,
    /// <summary>
    /// ������� = 9
    /// </summary>
    SENSORS = 9,
    /// <summary>
    /// �������� = 10
    /// </summary>
    COUNTERS = 10
  }
}
