using System;
using TeletrackTuning.Error;

namespace TeletrackTuning.Entity
{
  /// <summary>
  /// �� ������������
  /// ������������ �������� GPRS. ��������� socket.
  /// �������������� ������: 52, 62, 82
  /// </summary>
  public class GprsSocketConfig : CommonDescription
  {
    /// <summary>
    /// ������, �� ������� ��������� ���������� 
    /// TCP/IP ������ �����. �������� 20 ��������.
    /// </summary>
    public string Server
    {
      get { return server; }
      set { server = value; }
    }
    private string server;

    /// <summary>
    /// ���� �������, �� ������� ��������� ���������� 
    /// TCP/IP ������ �����.
    /// </summary>
    public ushort Port
    {
      get { return port; }
      set { port = value; }
    }
    private ushort port;

    /// <summary>
    /// �����������
    /// </summary>
    public GprsSocketConfig()
    {
      server = "";
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;

      if (server.Length > 20)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "server(������)",
          "GprsSocketConfig(��������� socket GPRS)",
          20);
        return false;
      }
      return true;
    }
  }
}
