﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Windows.Forms;

namespace TeletrackTuning
{
  public class GsmOperator
  {
    private StructureSaveOperator[] operators = null;
    public StructureSaveOperator[] Operators
    {
      get { return operators; }
      set { operators = value; }
    }
  }


  class GsmOperatorSetting
  {
    public Dictionary<string, StructureSaveOperator> dictionaryGsm = new Dictionary<string, StructureSaveOperator>();
    //private GsmOperator gsmOperator = new GsmOperator();
    private string filename = "gsmOperators.xml";

    public void LoadDictionary()
    {
      try
      {
        if (File.Exists(filename))
        {
          TextReader reader = new StreamReader(filename);
          XmlSerializer serializer = new XmlSerializer((typeof(GsmOperator)));
          GsmOperator gsmOperator = (GsmOperator)serializer.Deserialize(reader);
          reader.Close();

          dictionaryGsm.Clear();
          foreach(StructureSaveOperator save in gsmOperator.Operators)
          {
            dictionaryGsm.Add(save.AccessPointName, save);
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "error read file gsmOperators");
      }
    }

    public void SaveDictionary()
    {
      try
      {
        StructureSaveOperator[] tmpSaveStr = new StructureSaveOperator[dictionaryGsm.Values.Count];
        dictionaryGsm.Values.CopyTo(tmpSaveStr, 0); 
        GsmOperator gsmOperator = new GsmOperator();
        gsmOperator.Operators = tmpSaveStr;

        TextWriter writer = new StreamWriter(filename, false);
        XmlSerializer serializer = new XmlSerializer((typeof(GsmOperator)));
        serializer.Serialize(writer, gsmOperator);//gsmOperator);
        writer.Close();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "error save file gsmOperators");
      }
    }

    public StructureSaveOperator GetSetting(string apn)
    {
      if (dictionaryGsm != null && dictionaryGsm.ContainsKey(apn))
        return dictionaryGsm[apn];
      else
        return null;
    }

    public string[] GetEnumOperators()
    {
      string[] enumGsm = null;
      if (dictionaryGsm != null && dictionaryGsm.Keys.Count > 0)
      {
        enumGsm = new string[dictionaryGsm.Keys.Count];
        dictionaryGsm.Keys.CopyTo(enumGsm, 0);
      }
      return enumGsm;
    }

    public void AddNewOperator(StructureSaveOperator value)
    {
      if (dictionaryGsm != null && value.AccessPointName != null)
      {
        if (dictionaryGsm.ContainsKey(value.AccessPointName))
          dictionaryGsm.Remove(value.AccessPointName);
        dictionaryGsm.Add(value.AccessPointName, value);

        SaveDictionary();
      }
    }
  }

  public class StructureSaveOperator
  {
    public string AccessPointName = "";
    public string ApnLogin = "";
    public string ApnPass = "";
    public string dnsServ = "";
  }


}
