﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TeletrackTuning.DAL;
using TeletrackTuning.Properties;

namespace TeletrackTuning
{
    public partial class DbSettingForm : Form
    {
        public DbSettingForm()
        {
            InitializeComponent();

            comboDBType.SelectedIndexChanged += comboDBType_SelectedIndexChanged;
            comboLogType.SelectedIndexChanged += comboLogType_SelectedIndexChanged;

            textBoxDataBase.Text = Settings.Default.DataBase;
            textBoxServer.Text = Settings.Default.Server;
            textBoxUser.Text = Settings.Default.User;
            textBoxPassword.Text = Settings.Default.Password;
            comboDBType.Text = Settings.Default.DBType;
            comboLogType.Text = Settings.Default.Authentication;

            if (Settings.Default.Authentication == "SQL Server")
            {
                comboLogType.SelectedIndex = 0;
            }
            else if (Settings.Default.Authentication == "Windows")
            {
                comboLogType.SelectedIndex = 1;
            }

            if (Settings.Default.DBType == "MYSQL")
            {
                comboDBType.SelectedIndex = 0;
            }
            else if (Settings.Default.DBType == "MSSQL")
            {
                comboDBType.SelectedIndex = 1;
            }

            if (comboDBType.Text == "MYSQL")
            {
                comboLogType.Enabled = false;
                textBoxUser.Enabled = true;
                textBoxPassword.Enabled = true;
            }
            else if (comboDBType.Text == "MSSQL")
            {
                comboLogType.Enabled = true;
            }

            if( comboLogType.Text == "SQL Server" )
            {
                textBoxUser.Enabled = true;
                textBoxPassword.Enabled = true;
            }
            else if( comboLogType.Text == "Windows" && comboDBType.Text == "MSSQL" )
            {
                textBoxUser.Enabled = false;
                textBoxPassword.Enabled = false;
            }
        }

        void comboLogType_SelectedIndexChanged( object sender, EventArgs e )
        {
            if (comboLogType.Text == "SQL Server")
            {
                textBoxUser.Enabled = true;
                textBoxPassword.Enabled = true;
            }
            else if (comboLogType.Text == "Windows")
            {
                textBoxUser.Enabled = false;
                textBoxPassword.Enabled = false;
            }
        }

        void comboDBType_SelectedIndexChanged( object sender, EventArgs e )
        {
            if (comboDBType.Text == "MYSQL")
            {
                comboLogType.Enabled = false;
                textBoxUser.Enabled = true;
                textBoxPassword.Enabled = true;
            }
            else if (comboDBType.Text == "MSSQL")
            {
                comboLogType.Enabled = true;

                if( comboLogType.Text == "Windows" )
                {
                    textBoxUser.Enabled = false;
                    textBoxPassword.Enabled = false;
                }
                else if( comboLogType.Text == "SQL Server" )
                {
                    textBoxUser.Enabled = true;
                    textBoxPassword.Enabled = true;
                }
            }
        }

        private void buttonCansel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            Settings.Default.DataBase = textBoxDataBase.Text;
            Settings.Default.Server = textBoxServer.Text;
            Settings.Default.User = textBoxUser.Text;
            Settings.Default.Password = textBoxPassword.Text;
            Settings.Default.DBType = comboDBType.Text;
            Settings.Default.Authentication = comboLogType.Text;
            Settings.Default.Save();

            if (Settings.Default.DBType == "MYSQL")
            {
                DriverDb.TypeDataBaseUsing = 0;
                QueryLayer.TypeDataBaseForUsing = 0;
            }
            else if (Settings.Default.DBType == "MSSQL")
            {
                DriverDb.TypeDataBaseUsing = 1;
                QueryLayer.TypeDataBaseForUsing = 1;
            }

            Close();
        }
    }
}
