using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;
using MySql.Data;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� �������� GPRS 
    /// </summary>
    internal class A1InsertConfigGprsBase : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoConfigGprsMain;
        DriverDb driverDb = new DriverDb();

        public A1InsertConfigGprsBase(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Name", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Descr", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Message_ID", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32());

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Mode", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Apnserv", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Apnun", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Apnpw", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Dnsserv1", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Dialn1", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Ispun", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Isppw", driverDb.GettingString());
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigGprsMain_ID)
        /// 3. GprsBaseConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            try
            {
                if (initObjects == null)
                    throw new ArgumentNullException("object[] initObjects",
                        String.Format(ErrorText.ARGUMENT_NULL,
                            "Init", "A1InsertConfigGprsBase", "object[] initObjects"));

                if (initObjects.Length != 3)
                    throw new ArgumentException(String.Format(
                        ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigGprsBase",
                        initObjects.Length, 3), "object[] initObjects");

                driverDb.CommandParametersValue(0, "");
                driverDb.CommandParametersValue(1, "");
                driverDb.CommandParametersValue(2, (int) initObjects[0]);
                driverDb.CommandParametersValue(3, (int) initObjects[1]);

                GprsBaseConfig config = (GprsBaseConfig) initObjects[2];
                driverDb.CommandParametersValue(4, config.Mode);
                driverDb.CommandParametersValue(5, config.ApnServer);
                driverDb.CommandParametersValue(6, config.ApnLogin);
                driverDb.CommandParametersValue(7, config.ApnPassword);
                driverDb.CommandParametersValue(8, config.DnsServer);
                driverDb.CommandParametersValue(9, config.DialNumber);
                driverDb.CommandParametersValue(10, config.GprsLogin);
                driverDb.CommandParametersValue(11, config.GprsPassword);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
            }
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
