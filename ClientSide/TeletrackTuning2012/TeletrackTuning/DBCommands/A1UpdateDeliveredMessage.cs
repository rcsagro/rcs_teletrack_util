using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ���������� ����� ������� Messages �������� ������ 
    /// ����� �������� ������ �� ���������
    /// </summary>
    internal class A1UpdateDeliveredMessage : SrvDbCommand
    {
        private string QUERY = QueryLayer.UpdateMessagesDelivered;
        DriverDb driverDb = new DriverDb();

        public A1UpdateDeliveredMessage(string connectionString)
            : base(connectionString)
        {

            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );

            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "MessageId", driverDb.GettingInt32() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "Time", driverDb.GettingDateTime() );
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "A1UpdateDeliveredMessage", "object[] initObjects"));

            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1UpdateDeliveredMessage",
                    initObjects.Length, 1), "object[] initObjects");

            driverDb.CommandParametersValue( 0, (int) initObjects[0]);
            driverDb.CommandParametersValue( 1, DateTime.Now);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
