﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    internal class InsertNewConfigGprsEmail : SrvDbCommand
    {
        private int newId;

        public int NewId
        {
            get { return newId; }
        }

        private string QUERY = QueryLayer.InsertIntoConfiggprsemail;
        DriverDb driverDb = new DriverDb();

        public InsertNewConfigGprsEmail(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;
            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );

            //MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));

            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "Name", driverDb.GettingString() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "Descr", driverDb.GettingString() );
        }

        /// <summary>
        /// Порядок передачи параметров:
        /// 1. Name
        /// 2. Description
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "InsertNewConfigGprsEmail", "object[] initObjects"));

            if (initObjects.Length != 2)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertNewConfigGprsEmail",
                    initObjects.Length, 2), "object[] initObjects");

            //MyDbCommand.Parameters["?Name"].Value = (string) initObjects[0];
            //MyDbCommand.Parameters["?Descr"].Value = (string) initObjects[1];

            driverDb.CommandParametersValue( 0, ( string )initObjects[0] );
            driverDb.CommandParametersValue( 1, ( string )initObjects[1] );
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();
            //newId = (int) MyDbCommand.LastInsertedId;

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();
            newId = driverDb.LastInsertedId;

            return ExecutionStatus.OK;
        }
    }
}
