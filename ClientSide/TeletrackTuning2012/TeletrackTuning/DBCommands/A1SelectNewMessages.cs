using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ������� ����� ���������.
    /// �������� ������ ��������� ��������� ��� ������ �� ������ ������
    /// ����������, � ��� ���� ���������, ����� �������� ��� ��������� ������� 
    /// </summary>
    internal class A1SelectNewMessages : SrvDbCommand
    {
        private string QUERY = QueryLayer.SelectFromMessages;
        DriverDb driverDb = new DriverDb();

        /// <summary>
        /// Create A1 select new messages
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public A1SelectNewMessages(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;
            //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32() );
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MobitelId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "A1SelectNewMessages", "object[] initObjects"));

            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectNewMessages",
                    initObjects.Length, 1), "object[] initObjects");

            //MyDbCommand.Parameters["?MobitelId"].Value = (int) initObjects[0];
            driverDb.CommandParametersValue( 0, ( int )initObjects[0] );
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //return MyDbCommand.ExecuteReader();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            return driverDb.CommandExecuteReader();
        }
    }
}
