using System;
using System.Windows.Forms;

namespace TeletrackTuning.SrvDBCommand.Factory
{
    /// <summary>
    /// �������� �������� ������ �� ��������� ����.
    /// </summary>
    public class DbCommandFactory : IDbCommandFactory
    {
        /// <summary>
        /// ������ �����������.
        /// </summary>
        private readonly string _connectionString;

        private DbCommandFactory()
        {
        }

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="�onnectionString">������ �����������.</param>
        public DbCommandFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        #region IDbCommandFactory Members

        /// <summary>
        /// �������� ��������� �������.
        /// </summary>
        /// <typeparam name="T">��� �������.</typeparam>
        /// <returns>ISrvDbCommand.</returns>
        public ISrvDbCommand GetDbCommand<T>() where T : SrvDbCommand
        {
            try
            {
                return (SrvDbCommand) Activator.CreateInstance(typeof (T), new object[1] {_connectionString});
            }
            catch (Exception e)
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace, "Error connection" + typeof( T ).Name, MessageBoxButtons.OK );
                return null; 
            }
        }

        #endregion
    }
}
