using System;

namespace TeletrackTuning.SrvDBCommand.Factory
{
  /// <summary>
  /// �������� �������� ������ �� ��������� ����.
  /// </summary>
  public interface IDbCommandFactory
  {
    /// <summary>
    /// �������� ��������� �������.
    /// </summary>
    /// <typeparam name="T">��� �������.</typeparam>
    /// <returns>ISrvDbCommand.</returns>
    ISrvDbCommand GetDbCommand<T>() where T : SrvDbCommand;
  }
}
