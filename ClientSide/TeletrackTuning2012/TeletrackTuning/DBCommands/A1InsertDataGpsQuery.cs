﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;
using MySql.Data;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// Сохранение основных настроек GPRS 
    /// </summary>
    internal class A1InsertDataGpsQuery : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoGpsmasks;
        DriverDb driverDb = new DriverDb();

        public A1InsertDataGpsQuery(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Name", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Descr", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Mobitel_ID", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Message_ID", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "LastRecords", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "WhatSend", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MaxSmsNum", driverDb.GettingInt32());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
            }
        }

        /// <summary>
        /// Порядок передачи параметров:
        /// 1. Message_ID
        /// 2. Mobitel_Id 
        /// 3. Description
        /// 4. LastRecords
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "A1InsertDataGpsQuery", "object[] initObjects"));

            if (initObjects.Length != 4)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertDataGpsQuery",
                    initObjects.Length, 4), "object[] initObjects");

            driverDb.CommandParametersValue( 0, "");
            driverDb.CommandParametersValue( 1, (string) initObjects[2]);
            driverDb.CommandParametersValue( 3, (int) initObjects[0]);
            driverDb.CommandParametersValue( 2, (int) initObjects[1]);

            driverDb.CommandParametersValue( 4, (int) initObjects[3]);
            driverDb.CommandParametersValue( 5, 0x3FF); // 1023=all
            driverDb.CommandParametersValue( 6, 0);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
