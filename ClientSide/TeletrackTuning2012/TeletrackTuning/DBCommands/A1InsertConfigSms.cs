using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� SMS �������
    /// </summary>
    internal class A1InsertConfigSms : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoConfigsms;
        DriverDb driverDb = new DriverDb();

        public A1InsertConfigSms(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Name", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Descr", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Message_ID", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32());

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "SmsCenterNumber", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "SmsDsptNumber", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "SmsEmailGateNumber", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "dsptEmail", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "dsptGprsEmail", driverDb.GettingString());
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigSms_ID)
        /// 3. SmsAddrConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            try
            {
                if (initObjects == null)
                    throw new ArgumentNullException("object[] initObjects",
                        String.Format(ErrorText.ARGUMENT_NULL,
                            "Init", "A1InsertConfigSms", "object[] initObjects"));

                if (initObjects.Length != 3)
                    throw new ArgumentException(String.Format(
                        ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigSms",
                        initObjects.Length, 3), "object[] initObjects");

                driverDb.CommandParametersValue(0, "");
                driverDb.CommandParametersValue(1, "");
                driverDb.CommandParametersValue(2, (int) initObjects[0]);
                driverDb.CommandParametersValue(3, (int) initObjects[1]);

                SmsAddrConfig config = (SmsAddrConfig) initObjects[2];
                driverDb.CommandParametersValue(4, config.SmsCentre);
                driverDb.CommandParametersValue(5, config.SmsDspt);
                driverDb.CommandParametersValue(6, config.SmsEmailGate);
                driverDb.CommandParametersValue(7, config.DsptEmailSMS);
                driverDb.CommandParametersValue(8, config.DsptEmailGprs);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
            }
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
