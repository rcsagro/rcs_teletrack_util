using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� ���������� �������
    /// </summary>
    internal class A1InsertConfigPhone : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoMobitels;
        DriverDb driverDb = new DriverDb();

        public A1InsertConfigPhone(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Name", driverDb.GettingVString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Descr", driverDb.GettingVString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Message_ID", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32());

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "telSOS", driverDb.GettingVString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "telDspt", driverDb.GettingVString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "telAccept1", driverDb.GettingVString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "telAccept2", driverDb.GettingVString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "telAccept3", driverDb.GettingVString());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigTel_ID)
        /// 3. PhoneNumberConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            try
            { 
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "A1InsertConfigPhoneDbCommand", "object[] initObjects"));

            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigPhoneDbCommand",
                    initObjects.Length, 3), "object[] initObjects");

            driverDb.CommandParametersValue(0, "");
            driverDb.CommandParametersValue(1, "");
            driverDb.CommandParametersValue(2, (int) initObjects[0]);
            driverDb.CommandParametersValue(3, (int) initObjects[1]);

            PhoneNumberConfig config = (PhoneNumberConfig) initObjects[2];
            driverDb.CommandParametersValue(4, config.NumberSOS);
            driverDb.CommandParametersValue(5, config.NumberDspt);
            driverDb.CommandParametersValue(6, config.NumberAccept1);
            driverDb.CommandParametersValue(7, config.NumberAccept2);
            driverDb.CommandParametersValue(8, config.NumberAccept3);
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
