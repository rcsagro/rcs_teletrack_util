using System;
using System.Data;
using System.Text;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.TDBCommand
{
    internal class TInsertDataInBufferDBCommand : TDbCommand
    {
        private string SQL_QUERY = QueryLayer.InsertIntoDatagpsBuffer_on;
        DriverDb driverDb = new DriverDb();

        public TInsertDataInBufferDBCommand(string connectionString)
            : base(connectionString)
        {
        }

        /// <summary>
        /// Init
        /// </summary>
        /// <param name="string">������ ����� ������</param>
        public override void Init(params object[] initObjects)
        {
            string[] rows = (string[]) initObjects[0];
            if ((rows == null) || (rows.Length == 0))
                throw new ArgumentNullException("string[] Rows",
                    ErrorText.DB_NULL_DATA_INSERT_COMMAND);

            StringBuilder insertRows = new StringBuilder(SQL_QUERY);
            insertRows.Append(GetRowsFowsRorInsert(rows));

            //MyDbCommand.CommandText = insertRows.ToString();

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( insertRows.ToString() );
        }

        protected override bool NeedExecuteAfterFailure()
        {
            return false;
        }

        /// <summary>
        /// ��������� ������ ������� ����
        /// INSERT INTO datagpsbuffer (...)
        /// VALUES (..., ..., ...), (..., ..., ...)
        /// </summary>
        /// <param name="rows"></param>
        /// <returns>(..., ..., ...), (..., ..., ...)</returns>
        private string GetRowsFowsRorInsert(params string[] rows)
        {
            StringBuilder insertRows = new StringBuilder("");
            foreach (string row in rows)
            {
                if (insertRows.Length != 0)
                    insertRows.Append(",");

                insertRows.Append("(").Append(row).Append(")");
            }

            return insertRows.ToString();
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
