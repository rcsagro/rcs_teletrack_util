﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// Выборка из БД настроек телефонных номеров
    /// </summary>
    internal class InsertNewConfigMain : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoMobitelsValue;
        DriverDb driverDb = new DriverDb();

        //"  COALESCE((SELECT max(InternalMobitelConfig_ID) + 1 from InternalMobitelConfig c), 0))";

        public InsertNewConfigMain(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );

            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "Name", driverDb.GettingString() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "Descr", driverDb.GettingString() );
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Mobitel_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfigEvent_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfigTel_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfigSms_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfigGprsMain_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfigGprsEmail_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfigOther_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfigDriverMessage_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfigUnique_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfigZoneSet_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfigSensorSet_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfigGprsInit_ID", driverDb.GettingInt32());
        }

        /// <summary>
        /// Порядок передачи параметров:
        /// 1. Name
        /// 2. Descr
        /// 3. Mobitel_ID
        /// 4. ConfigEvent_ID
        /// 5. ConfigTel_ID
        /// 6. ConfigSms_ID
        /// 7. ConfigGprsMain_ID
        /// 8. ConfigGprsEmail_ID
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "InsertNewConfigMain", "object[] initObjects"));

            if (initObjects.Length != 8)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertNewConfigMain",
                    initObjects.Length, 8), "object[] initObjects");

            driverDb.CommandParametersValue(0, (string) initObjects[0]);
            driverDb.CommandParametersValue(1, (string) initObjects[1]);
            driverDb.CommandParametersValue(2, (int) initObjects[2]);

            driverDb.CommandParametersValue(3, (int) initObjects[3]);
            driverDb.CommandParametersValue(4, (int) initObjects[4]);
            driverDb.CommandParametersValue(5, (int) initObjects[5]);
            driverDb.CommandParametersValue(6, (int) initObjects[6]);
            driverDb.CommandParametersValue(7, (int) initObjects[7]);

            driverDb.CommandParametersValue(8, 50);
            driverDb.CommandParametersValue(9, 50);
            driverDb.CommandParametersValue(10, 50);
            driverDb.CommandParametersValue(11, 50);
            driverDb.CommandParametersValue(12, 50);
            driverDb.CommandParametersValue(13, 50);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
