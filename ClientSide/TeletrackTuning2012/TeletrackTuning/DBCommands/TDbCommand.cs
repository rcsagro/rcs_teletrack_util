using TeletrackTuning.SrvDBCommand;

namespace TeletrackTuning.TDBCommand
{
    internal class TDbCommand : SrvDbCommand
    {
        protected TDbCommand()
        {
        }

        public TDbCommand(string connectionString) : base(connectionString)
        {
        }
    }
}
