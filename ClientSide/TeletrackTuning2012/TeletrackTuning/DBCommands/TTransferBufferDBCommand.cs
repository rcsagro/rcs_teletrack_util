using System.Data;
using TeletrackTuning.DAL;

namespace TeletrackTuning.TDBCommand
{
    internal class TTransferBufferDBCommand : TDbCommand
    {
        private string PROC_NAME = QueryLayer.OnTransferBuffer;
        DriverDb driverDb = new DriverDb();

        public TTransferBufferDBCommand(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.StoredProcedure;
            //MyDbCommand.CommandText = PROC_NAME;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.StoredProcedure );
            driverDb.CommandText( PROC_NAME );
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
