﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{

    internal class DeleteMobitel : SrvDbCommand
    {
        private string DeleteInterMobConf = QueryLayer.DeleteInternalMobitelConfig;

        private string DeleteMob = QueryLayer.DeleteFromMobitels;

        DriverDb driverDb = new DriverDb();

        private int mobitelId = -1;

        public DeleteMobitel(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            //driverDb.CommandText( QUERY );
            
            //MyDbCommand.CommandText = QUERY;
            //MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
        }

        /// <summary>
        /// Порядок передачи параметров:
        /// 1. mobitelID
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "DeleteMobitel", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "DeleteMobitel",
                    initObjects.Length, 1), "object[] initObjects");

            mobitelId = (Int32) initObjects[0];
        }

        protected override ExecutionStatus InternalExecute()
        {
            ExecutionStatus result = ExecutionStatus.ERROR_DB;
            //delete internalmobitelconfig
            
            if (driverDb.TextCommand != "")
                driverDb.ParametersClear();

            //MyDbCommand.CommandText = DeleteInterMobConf;
            //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelID", MySqlDbType.Int32));
            //MyDbCommand.Parameters["?MobitelID"].Value = mobitelId;

            driverDb.CommandText( DeleteInterMobConf );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "MobitelID", driverDb.GettingInt32() );
            driverDb.CommandParametersValue(0, mobitelId);

            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            //delete mobitel
            //MyDbCommand.CommandText = DeleteMob;
            //MyDbCommand.ExecuteNonQuery();

            driverDb.CommandText(DeleteMob);
            driverDb.CommandExecuteNonQuery();
            result = ExecutionStatus.OK;
            return result;
        }
    }
}

