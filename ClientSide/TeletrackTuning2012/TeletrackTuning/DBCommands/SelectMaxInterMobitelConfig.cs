﻿using System;
using System.Data;
using System.Text;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  class SelectMaxInterMobitelConfig : SrvDbCommand
  {
    private const string QUERY = "SELECT max(InternalMobitelConfig_ID) as InMobCfg FROM internalmobitelconfig i";

    public SelectMaxInterMobitelConfig(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
