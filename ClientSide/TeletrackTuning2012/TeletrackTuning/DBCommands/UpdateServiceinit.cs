﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    internal class UpdateServiceinit : SrvDbCommand
    {
        private string QUERY = QueryLayer.UpdateServiceinit;
        DriverDb driverDb = new DriverDb();

        public UpdateServiceinit(string connectionString)
            : base(connectionString)
        {
            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType(CommandType.Text);
            driverDb.CommandText(QUERY);

            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "A1", driverDb.GettingString() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "A2", driverDb.GettingString() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "A3", driverDb.GettingString() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "A4", driverDb.GettingString() );
        }

        /// <summary>
        /// Порядок передачи параметров:
        /// 1. ip
        /// 2. port
        /// 3. login
        /// 4. password
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "UpdateServiceinit", "object[] initObjects"));
            if (initObjects.Length != 4)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "UpdateServiceinit",
                    initObjects.Length, 4), "object[] initObjects");

            driverDb.CommandParametersValue(0, (string) initObjects[0]);
            driverDb.CommandParametersValue(1, ( string )initObjects[1]);
            driverDb.CommandParametersValue(2, (string) initObjects[2]);
            driverDb.CommandParametersValue(3, ( string )initObjects[3]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}