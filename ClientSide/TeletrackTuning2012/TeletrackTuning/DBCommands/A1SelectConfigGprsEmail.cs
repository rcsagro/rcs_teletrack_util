using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ������� �� �� �������� GPRS Email
    /// </summary>
    internal class A1SelectConfigGprsEmail : SrvDbCommand
    {
        private string QUERY = QueryLayer.SelectFromConfigGprsEmail;
        DriverDb driverDb = new DriverDb();

        //"WHERE Message_ID = ?MessageId";

        public A1SelectConfigGprsEmail(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            try
            {
                if (initObjects == null)
                    throw new ArgumentNullException("object[] initObjects",
                        String.Format(ErrorText.ARGUMENT_NULL,
                            "Init", "A1SelectConfigGprsEmail", "object[] initObjects"));
                if (initObjects.Length != 1)
                    throw new ArgumentException(String.Format(
                        ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigGprsEmail",
                        initObjects.Length, 1), "object[] initObjects");

                //MyDbCommand.Parameters["?MobitelId"].Value = (int) initObjects[0];
                driverDb.CommandParametersValue(0, (int) initObjects[0]);
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }

        }

        protected override object InternalExecuteReader()
        {
            try
            {
                //MyDbCommand.Connection = Connection;
                //Connection.Open();
                //return MyDbCommand.ExecuteReader();

                driverDb.Command_Connect = Connection;
                driverDb.ConnectionOpen();
                return driverDb.CommandExecuteReader();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
                return null;
            }
        }
    }
}
