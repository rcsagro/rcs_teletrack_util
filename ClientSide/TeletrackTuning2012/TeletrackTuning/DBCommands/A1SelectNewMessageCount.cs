using System;
using System.Data;
using TeletrackTuning.DAL;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ������� ���-�� ����� �������������� ��������� ������ ��� ����������� ����������
    /// </summary>
    internal class A1SelectNewMessageCount : SrvDbCommand
    {
        private string QUERY = QueryLayer.SelectCountMessage_Id;
        DriverDb driverDb = new DriverDb();

        /// <summary>
        /// ���-�� ���������
        /// </summary>
        private int count;

        /// <summary>
        /// ���-�� ���������
        /// </summary>
        public int Count
        {
            get { return count; }
        }

        /// <summary>
        /// Create A1 select new messages
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public A1SelectNewMessageCount(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //count = Convert.ToInt32(MyDbCommand.ExecuteScalar());

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            count = Convert.ToInt32(driverDb.CommandExecuteScalar());

            return ExecutionStatus.OK;
        }
    }
}
