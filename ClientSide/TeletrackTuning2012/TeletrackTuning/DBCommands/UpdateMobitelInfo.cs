﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    internal class UpdateMobitelInfo : SrvDbCommand
    {
        private string UpdateMobInf = QueryLayer.UpdateMobitels;
        DriverDb driverDb = new DriverDb();

        private int mobitelId = -1;

        public UpdateMobitelInfo(string connectionString)
            : base(connectionString)
        {
            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType(CommandType.Text);
            driverDb.CommandText(UpdateMobInf);
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "MobitelID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "NewName", driverDb.GettingString() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "NewDescr", driverDb.GettingString() );
        }

        /// <summary>
        /// Порядок передачи параметров:
        /// 1. mobitelID
        /// 2. Name
        /// 3. Description
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "UpdateMobitelInfo", "object[] initObjects"));
            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "UpdateMobitelInfo",
                    initObjects.Length, 3), "object[] initObjects");

            driverDb.CommandParametersValue(0, (Int32) initObjects[0]);
            driverDb.CommandParametersValue(1, (string) initObjects[1]);
            driverDb.CommandParametersValue(2, (string) initObjects[2]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            ExecutionStatus result = ExecutionStatus.ERROR_DB;
            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery(); 
            result = ExecutionStatus.OK;
            return result;
        }
    }
}

