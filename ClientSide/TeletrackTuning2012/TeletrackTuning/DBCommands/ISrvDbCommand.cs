using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;

namespace TeletrackTuning.SrvDBCommand
{
    public interface ISrvDbCommand
    {
        /// <summary>
        /// Закрыть соединение, если требуется.
        /// Обязательно закрывать после использования reader
        /// </summary>
        void CloseConnection();

        /// <summary>
        /// Выполнить команду.
        /// <para>
        /// При возникновении исключения тихо его обрабатывает,
        /// регестрирует
        /// </para>
        /// </summary>
        ExecutionStatus Execute();

        /// <summary>
        /// Выполнить команду и возвращает DataReader и ResponseStatus
        /// </summary>
        /// <param name="status">ResponseStatus</param>
        /// <returns>MySqlDataReader</returns>
        
        //MySqlDataReader ExecuteReader(out ExecutionStatus status);
        object ExecuteReader( out ExecutionStatus status );

        /// <summary>
        /// Инициализация команды перед выполнением
        /// </summary>
        /// <param name="initObjects">массив объектов инициализации</param>
        void Init(params object[] initObjects);

        /// <summary>
        /// Показывает занятость команды
        /// </summary>
        bool IsBusy { get; }
    }
}
