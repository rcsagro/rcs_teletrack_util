using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ����� Mobitel_ID �� �������� DevIdShort
    /// </summary>
    internal class GetMobitelIdDbCommand : SrvDbCommand
    {
        private string QUERY = QueryLayer.SelectMobitel_ID;
        DriverDb driverDb = new DriverDb();


        private int mobitelId;

        /// <summary>
        /// MobitelId
        /// </summary>
        public int MobitelId
        {
            get { return mobitelId; }
        }

        public GetMobitelIdDbCommand(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );

            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "devIdShort", driverDb.GettingVarChar());
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. devIdShort
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "GetMobitelIdDbCommand", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "GetMobitelIdDbCommand",
                    initObjects.Length, 1), "object[] initObjects");

            mobitelId = -1;
            driverDb.CommandParametersValue(0, (string) initObjects[0]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //object mobitelIdValue = MyDbCommand.ExecuteScalar();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            object mobitelIdValue = driverDb.CommandExecuteScalar();

            if (mobitelIdValue == DBNull.Value)
            {
                return ExecutionStatus.ERROR_DATA;
            }
            else
            {
                mobitelId = Convert.ToInt32(mobitelIdValue);
                return ExecutionStatus.OK;
            }
        }
    }
}
