using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ���������� ����� ������� Messages �������� ������ ����� �������� ���������
    /// </summary>
    internal class A1UpdateSentMessage : SrvDbCommand
    {
        private string QUERY = QueryLayer.UpdateMessagesID;
        private DriverDb driverDb = new DriverDb();

        public A1UpdateSentMessage(string connectionString)
            : base(connectionString)
        {
            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType(CommandType.Text);
            driverDb.CommandText(QUERY);

            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "MessageId", driverDb.GettingInt32() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "SourceId", driverDb.GettingInt32() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "Time", driverDb.GettingDateTime());
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// 2. SourceId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "A1UpdateSentMessage", "object[] initObjects"));
            if (initObjects.Length != 2)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1UpdateSentMessage",
                    initObjects.Length, 2), "object[] initObjects");

            driverDb.CommandParametersValue(0,  (int) initObjects[0]);
            driverDb.CommandParametersValue(1,  (int) initObjects[1]);
            driverDb.CommandParametersValue(2,  DateTime.Now);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
