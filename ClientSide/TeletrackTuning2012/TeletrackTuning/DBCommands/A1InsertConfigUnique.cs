using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� ���������� ����������
    /// </summary>
    internal class A1InsertConfigUnique : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoConfigUnique;
        private DriverDb driverDb = new DriverDb();

        public A1InsertConfigUnique(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Name", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Descr", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Message_ID", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "pswd", driverDb.GettingString());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigUnique_ID)
        /// 3. ������
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            try
            {
                if (initObjects == null)
                    throw new ArgumentNullException("object[] initObjects",
                        String.Format(ErrorText.ARGUMENT_NULL,
                            "Init", "A1InsertConfigUnique", "object[] initObjects"));
                if (initObjects.Length != 3)
                    throw new ArgumentException(String.Format(
                        ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigUnique",
                        initObjects.Length, 3), "object[] initObjects");

                driverDb.CommandParametersValue(0, "");
                driverDb.CommandParametersValue(1, "");
                driverDb.CommandParametersValue(2, (int) initObjects[0]);
                driverDb.CommandParametersValue(3, (int) initObjects[1]);
                driverDb.CommandParametersValue(4, (string) initObjects[2]);
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
