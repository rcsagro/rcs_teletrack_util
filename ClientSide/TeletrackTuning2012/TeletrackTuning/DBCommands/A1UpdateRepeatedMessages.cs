using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ��������� �������� IsNew = 0 ��� ������������� ������ ���������,
    /// ��������������� ��� ��������. 
    /// �������������� ������ ����� ��������� ���������. ����������
    /// ������� ���� ������, ������� ������ ���������� ����������� �
    /// ���������� ��������� IsNew = 0 � ������ �� ��������������.
    /// </summary>
    internal class A1UpdateRepeatedMessages : SrvDbCommand
    {
        private string QUERY = QueryLayer.UpdateMessagesOne;
        private DriverDb driverDb = new DriverDb();

        public A1UpdateRepeatedMessages(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType(CommandType.Text);
            driverDb.CommandText(QUERY);

            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "CommandId", driverDb.GettingInt32() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "MessageCounter", driverDb.GettingInt32() );
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MobitelId
        /// 2. CommandId
        /// 3. MessageCounter
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "A1UpdateRepeatedMessages", "object[] initObjects"));

            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1UpdateRepeatedMessages",
                    initObjects.Length, 3), "object[] initObjects");

            driverDb.CommandParametersValue(0, (int) initObjects[0]);
            driverDb.CommandParametersValue(1, (int) initObjects[1]);
            driverDb.CommandParametersValue(2, (int) initObjects[2]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
