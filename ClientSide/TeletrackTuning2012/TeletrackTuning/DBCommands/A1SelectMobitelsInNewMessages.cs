using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ������� ��������������� ���������� � ��������� �����
    /// ���������-������ ��������������� ��� ��������� ����������
    /// </summary>
    internal class A1SelectMobitelsInNewMessages : SrvDbCommand
    {
        private string QUERY = QueryLayer.SelectDistinctMsg;
        private DriverDb driverDb = new DriverDb();

        public A1SelectMobitelsInNewMessages(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //return MyDbCommand.ExecuteReader();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            return driverDb.CommandExecuteReader();
        }
    }
}
