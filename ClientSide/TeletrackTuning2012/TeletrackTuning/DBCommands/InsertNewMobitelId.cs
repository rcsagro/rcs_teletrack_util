﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{

    internal class InsertNewMobitelId : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoNewMobitels;
        DriverDb driverDb = new DriverDb();

        private int newMobitelId;

        public int NewMobitelId
        {
            get { return newMobitelId; }
        }

        public InsertNewMobitelId(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );

            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "Name", driverDb.GettingString() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "Descr", driverDb.GettingString() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "InternalMobitelConfig_ID", driverDb.GettingInt32() );
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MapStyleLine_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MapStyleLastPoint_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MapStylePoint_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ServiceSend_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ConfirmedID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "LastInsertTime", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Is64bitPackets", driverDb.GettingByte());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "IsNotDrawDgps", driverDb.GettingByte());
        }

        /// <summary>
        /// Порядок передачи параметров:
        /// 1. InternalMobitelConfig_ID
        /// 2. Name
        /// 3. Description
        /// 4. serviceSend_ID
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "InsertNewMobitelId", "object[] initObjects"));

            if (initObjects.Length != 8)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertNewMobitelId",
                    initObjects.Length, 8), "object[] initObjects");

            driverDb.CommandParametersValue(0, (string) initObjects[1]);
            driverDb.CommandParametersValue(1, (string) initObjects[2]);
            driverDb.CommandParametersValue(2, (int) initObjects[0]);
            driverDb.CommandParametersValue(3, 6);
            driverDb.CommandParametersValue(4, 5);
            driverDb.CommandParametersValue(5, 5);
            driverDb.CommandParametersValue(6, (int) initObjects[3]);
            driverDb.CommandParametersValue(7, (int) initObjects[4]);
            driverDb.CommandParametersValue(8, (int) initObjects[5]);
            driverDb.CommandParametersValue(9, (int) initObjects[6]);
            driverDb.CommandParametersValue(10, (int) initObjects[7]);

            newMobitelId = 0;
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();
            //newMobitelId = (int) MyDbCommand.LastInsertedId;

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();
            newMobitelId = driverDb.LastInsertedId;

            return ExecutionStatus.OK;
        }
    }
}

