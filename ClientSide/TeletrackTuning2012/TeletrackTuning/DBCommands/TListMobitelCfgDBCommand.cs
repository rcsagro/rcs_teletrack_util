using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;

namespace TeletrackTuning.TDBCommand
{
    /// <summary>
    /// Список телетреков
    /// </summary>
    internal class TListMobitelCfgDBCommand : TDbCommand
    {
        private string PROC_NAME = QueryLayer.OnListMobitelConfig;
        DriverDb driverDb = new DriverDb();

        public TListMobitelCfgDBCommand(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.StoredProcedure;
                //MyDbCommand.CommandText = PROC_NAME;

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.StoredProcedure);
                driverDb.CommandText(PROC_NAME);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error TListMobitelCfgDBCommand");
            }
        }

        protected override object InternalExecuteReader()
        {
            try
            {
                //MyDbCommand.Connection = Connection;
                //Connection.Open();
                //return MyDbCommand.ExecuteReader();

                driverDb.Command_Connect = Connection;
                driverDb.ConnectionOpen();
                return driverDb.CommandExecuteReader();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace, "Error TListMobitelCfgDBCommand");
                return null;
            }
        }
    }
}
