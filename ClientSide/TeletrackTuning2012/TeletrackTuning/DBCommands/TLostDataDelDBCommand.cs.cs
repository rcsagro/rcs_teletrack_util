using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.TDBCommand
{
    /// <summary>
    /// �������� ��������� ��������� �� ������� datagpslost_on 
    /// </summary>
    internal class TLostDataDelDBCommand : TDbCommand
    {
        private string PROC_NAME = QueryLayer.OnDeleteLostRange;
        private string PARAM_MOBITEL_ID = QueryLayer.MobitelID;
        private string PARAM_BEGIN_SRVPACKET_ID = QueryLayer.BeginSrvPacketID;
        DriverDb driverDb = new DriverDb();

        public TLostDataDelDBCommand(string connectionString)
            : base(connectionString)
        {
            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType(CommandType.StoredProcedure);
            driverDb.CommandText(PROC_NAME);
            driverDb.CommandParametersAdd( PARAM_MOBITEL_ID, driverDb.GettingInt32());
            driverDb.CommandParametersAdd( PARAM_BEGIN_SRVPACKET_ID, driverDb.GettingInt64());
        }

        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "TLostDataDelDBCommand", "object[] initObjects"));

            if (initObjects.Length != 2)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "TLostDataDelDBCommand",
                    initObjects.Length, 2), "object[] initObjects");

            driverDb.CommandParametersValue(0, (int) initObjects[0]);
            driverDb.CommandParametersValue(1, (long) initObjects[1]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery(); 

            return ExecutionStatus.OK;
        }
    }
}
