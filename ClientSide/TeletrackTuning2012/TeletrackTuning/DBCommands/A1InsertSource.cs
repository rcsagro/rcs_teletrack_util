using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ������� ������ � ������� Source
    /// </summary>
    internal class A1InsertSource : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoSources;
        DriverDb driverDb = new DriverDb();

        /// <summary>
        /// �������� ���������� ����� Source_ID ����� ������,
        /// ������� ������ ��� ���������
        /// </summary>
        private int newSourceId;

        /// <summary>
        /// �������� ���������� ����� Source_ID ����� ������,
        /// ������� ������ ��� ���������
        /// </summary>
        public int NewSourceId
        {
            get { return newSourceId; }
        }

        public A1InsertSource(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );

            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "SourceText", driverDb.GettingString());
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "SourceError", driverDb.GettingString());
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. SourceText
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "A1InsertSource", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertSource",
                    initObjects.Length, 1), "object[] initObjects");

            newSourceId = 0;

            driverDb.CommandParametersValue(0, (string) initObjects[0]);
            driverDb.CommandParametersValue(1, "No error");
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();
            //newSourceId = (int) MyDbCommand.LastInsertedId;

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();
            newSourceId = driverDb.LastInsertedId;

            return ExecutionStatus.OK;
        }
    }
}
