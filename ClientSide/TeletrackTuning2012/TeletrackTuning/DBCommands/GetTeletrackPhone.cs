﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    internal class GetTeletrackPhone : SrvDbCommand
    {
        private string QUERY = QueryLayer.SelectFromTelnumbers;
        private DriverDb driverDb = new DriverDb();

        private string phone;

        public string Phone
        {
            get { return phone; }
        }

        public GetTeletrackPhone(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
            }
        }

        /// <summary>
        /// Порядок передачи параметров:
        /// 1. mobitelID
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            try
            {
                if (initObjects == null)
                    throw new ArgumentNullException("object[] initObjects",
                        String.Format(ErrorText.ARGUMENT_NULL,
                            "Init", "GetTeletrackPhone", "object[] initObjects"));

                if (initObjects.Length != 1)
                    throw new ArgumentException(String.Format(
                        ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "GetTeletrackPhone",
                        initObjects.Length, 1), "object[] initObjects");

                driverDb.CommandParametersValue(0, (int) initObjects[0]);

                phone = "";
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
            }
        }

        protected override ExecutionStatus InternalExecute()
        {
            ExecutionStatus result = ExecutionStatus.ERROR_DB;;

            try
            {
                //MyDbCommand.Connection = Connection;
                //Connection.Open();

                driverDb.Command_Connect = Connection;
                driverDb.ConnectionOpen();

                object reader = driverDb.CommandExecuteReader();

                if ((reader != null)) //(result == ExecutionStatus.OK) && 
                {
                    driverDb.SqlDataReader = reader;

                    if (driverDb.Read())
                    {
                        phone = driverDb.GetString("TelNumber");
                        result = ExecutionStatus.OK;
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
                return result;
            }
        }
    }
}

