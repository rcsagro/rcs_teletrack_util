using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ���������� ����������������� ����������
    /// </summary>
    internal class A1InsertConfigId : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoInternalMobitelConfig;
        DriverDb driverDb = new DriverDb();

        public A1InsertConfigId(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Name", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Descr", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Message_ID", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32());

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "devIdShort", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "devIdLong", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "verProtocolShort", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "verProtocolLong", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "moduleIdGps", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "moduleIdGsm", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "moduleIdRf", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "moduleIdSs", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "moduleIdMm", driverDb.GettingString());
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigTel_ID)
        /// 3. IdConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            try
            {
                if (initObjects == null)
                    throw new ArgumentNullException("object[] initObjects",
                        String.Format(ErrorText.ARGUMENT_NULL,
                            "Init", "A1InsertConfigId", "object[] initObjects"));

                if (initObjects.Length != 3)
                    throw new ArgumentException(String.Format(
                        ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigId",
                        initObjects.Length, 3), "object[] initObjects");

                driverDb.CommandParametersValue(0, "");
                driverDb.CommandParametersValue(1, "");
                driverDb.CommandParametersValue(2, (int) initObjects[0]);
                driverDb.CommandParametersValue(3, (int) initObjects[1]);

                IdConfig config = (IdConfig) initObjects[2];
                driverDb.CommandParametersValue(4, config.DevIdShort);
                driverDb.CommandParametersValue(5, config.DevIdLong);
                driverDb.CommandParametersValue(6, config.VerProtocolShort);
                driverDb.CommandParametersValue(7, config.VerProtocolLong);
                driverDb.CommandParametersValue(8, config.ModuleIdGps);
                driverDb.CommandParametersValue(9, config.ModuleIdGsm);
                driverDb.CommandParametersValue(10, config.ModuleIdRf);
                driverDb.CommandParametersValue(11, config.ModuleIdSs);
                driverDb.CommandParametersValue(12, config.ModuleIdMm);
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
