using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ������� �� �� �������� ����������� ���
    /// </summary>
    internal class A1SelectZones : SrvDbCommand
    {
        private string QUERY = QueryLayer.SelectFromConfigZoneSet;
        DriverDb driverDb = new DriverDb();

        public A1SelectZones(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;
            //MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "MessageId", driverDb.GettingInt32() );
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "A1SelectZones", "object[] initObjects"));

            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectZones",
                    initObjects.Length, 1), "object[] initObjects");

            //MyDbCommand.Parameters["?MessageId"].Value = (int) initObjects[0];
            driverDb.CommandParametersValue( 0, ( int )initObjects[0] );
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //return MyDbCommand.ExecuteReader();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            return driverDb.CommandExecuteReader();
        }
    }
}
