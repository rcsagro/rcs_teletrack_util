using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ������� �� �� �������� ���������� �������
    /// </summary>
    internal class A1SelectConfigPhone : SrvDbCommand
    {
        private string QUERY = QueryLayer.SelectFromConfigTel;
        DriverDb driverDb = new DriverDb();

        //"WHERE cfg.Message_ID = ?MessageId " +
        //"LIMIT 1";

        public A1SelectConfigPhone(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;
                //MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32());
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }

        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. MessageId
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            try
            {
                if (initObjects == null)
                    throw new ArgumentNullException("object[] initObjects",
                        String.Format(ErrorText.ARGUMENT_NULL,
                            "Init", "A1SelectConfigPhone", "object[] initObjects"));
                if (initObjects.Length != 1)
                    throw new ArgumentException(String.Format(
                        ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigPhone",
                        initObjects.Length, 1), "object[] initObjects");

                //MyDbCommand.Parameters["?MobitelId"].Value = (int) initObjects[0];
                driverDb.CommandParametersValue(0, (int) initObjects[0]);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
            }
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //return MyDbCommand.ExecuteReader();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            return driverDb.CommandExecuteReader();
        }
    }
}
