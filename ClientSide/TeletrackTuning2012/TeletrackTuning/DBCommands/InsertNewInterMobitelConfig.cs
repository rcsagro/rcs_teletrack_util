﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    internal class InsertNewInterMobitelConfig : SrvDbCommand
    {
        private string updateID = QueryLayer.UpdateID;

        private string QUERY = QueryLayer.InsertInternalMobitelConfig;
        DriverDb driverDb = new DriverDb();

        private int newId;

        public int NewId
        {
            get { return newId; }
        }

        public InsertNewInterMobitelConfig(string connectionString)
            : base(connectionString)
        {
            InitCommandQUERY();
        }

        private void InitCommandQUERY()
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );

            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Name", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Descr", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Message_ID", driverDb.GettingInt32());

            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "devIdShort", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "devIdLong", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "verProtocolShort", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "verProtocolLong", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "moduleIdGps", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "moduleIdGsm", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "moduleIdRf", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "moduleIdSs", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "moduleIdMm", driverDb.GettingString());
        }

        /// <summary>
        /// Порядок передачи параметров:
        /// 1. shortID
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "InsertNewInterMobitelConfig", "object[] initObjects"));
            if (initObjects.Length != 1)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertNewInterMobitelConfig",
                    initObjects.Length, 1), "object[] initObjects");

            driverDb.CommandParametersValue(0, "");
            driverDb.CommandParametersValue(1, "");
            driverDb.CommandParametersValue(2, 0);
            string tmp = initObjects[0].ToString();
            driverDb.CommandParametersValue(3, tmp);
            driverDb.CommandParametersValue(4, "0");
            driverDb.CommandParametersValue(5, "0");
            driverDb.CommandParametersValue(6, "0");
            driverDb.CommandParametersValue(7, "0");
            driverDb.CommandParametersValue(8, "0");
            driverDb.CommandParametersValue(9, "0");
            driverDb.CommandParametersValue(10, "0");
            driverDb.CommandParametersValue(11, "0");

            newId = 0;
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();
            //newId = (int) MyDbCommand.LastInsertedId;

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();
            newId = driverDb.LastInsertedId;

            if (newId > 0)
            {
                if (driverDb.TextCommand != "")
                    driverDb.ParametersClear();

                //MyDbCommand.CommandText = updateID;

                driverDb.CommandText( updateID );

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "LastID", MySqlDbType.Int32);

                driverDb.CommandParametersValue(0, newId);

                //MyDbCommand.ExecuteNonQuery();
                driverDb.CommandExecuteNonQuery();

                //MyDbCommand.Parameters.Clear();
                driverDb.ParametersClear();

                InitCommandQUERY();

                return ExecutionStatus.OK;
            }
            else
                return ExecutionStatus.ERROR_DB;
        }
    }
}

