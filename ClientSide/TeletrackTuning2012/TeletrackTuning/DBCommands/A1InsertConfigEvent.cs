using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� �������
    /// </summary>
    internal class A1InsertConfigEvent : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoConfigEvent;
        private DriverDb driverDb = new DriverDb();

        public A1InsertConfigEvent(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Name", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Descr", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Message_ID", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32());

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "tmr1Log", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "tmr2Send", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "tmr3Zone", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "dist1Log", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "dist2Send", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "dist3Zone", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "gprsEmail", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "gprsFtp", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "gprsSocket", driverDb.GettingInt32());

                for (int i = 1; i < 32; i++)
                    driverDb.CommandParametersAdd(driverDb.ParamPrefics + "maskEvent" + Convert.ToString(i),
                        driverDb.GettingInt32());

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "deltaTimeZone", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "maskSensor", driverDb.GettingString());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigEvent_ID)
        /// 3. EventConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            try
            {
                if (initObjects == null)
                    throw new ArgumentNullException("object[] initObjects",
                        String.Format(ErrorText.ARGUMENT_NULL,
                            "Init", "A1InsertConfigEvent", "object[] initObjects"));

                if (initObjects.Length != 3)
                    throw new ArgumentException(String.Format(
                        ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigEvent",
                        initObjects.Length, 3), "object[] initObjects");

                driverDb.CommandParametersValue(0, "");
                driverDb.CommandParametersValue(1, "");
                driverDb.CommandParametersValue(2, (int) initObjects[0]);
                driverDb.CommandParametersValue(3, (int) initObjects[1]);

                EventConfig config = (EventConfig) initObjects[2];
                driverDb.CommandParametersValue(4, config.MinSpeed);
                driverDb.CommandParametersValue(5, config.Timer1);
                driverDb.CommandParametersValue(6, config.Timer2);
                driverDb.CommandParametersValue(7, config.CourseBend);
                driverDb.CommandParametersValue(8, config.Distance1);
                driverDb.CommandParametersValue(9, config.Distance2);
                driverDb.CommandParametersValue(10, config.GprsEmail);
                driverDb.CommandParametersValue(11, 10);
                driverDb.CommandParametersValue(12, 10);

                for (int i = 1; i < 32; i++)
                    driverDb.CommandParametersValue(12 + i, config.EventMask[i - 1]);

                driverDb.CommandParametersValue(44, config.SpeedChange);
                driverDb.CommandParametersValue(45, "----");
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
