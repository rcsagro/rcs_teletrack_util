﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    internal class InsertServiceSend : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoServicesendCoalesce;
        DriverDb driverDb = new DriverDb();

        private int newId;

        public int NewId
        {
            get { return newId; }
        }

        public InsertServiceSend(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( QUERY );

            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "Name", driverDb.GettingVarChar() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "Descr", driverDb.GettingVarChar() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "Mobitel", driverDb.GettingInt32() );
            driverDb.CommandParametersAdd( driverDb.ParamPrefics + "telMobitel", driverDb.GettingVarChar() );
        }

        /// <summary>
        /// Порядок передачи параметров:
        /// 1. name
        /// 2. description
        /// 3. mobitelID
        /// 4. telMobitels
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "InsertServiceSend", "object[] initObjects"));

            if (initObjects.Length != 4)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertServiceSend",
                    initObjects.Length, 4), "object[] initObjects");

            driverDb.CommandParametersValue( 0,  (string) initObjects[0]);
            driverDb.CommandParametersValue( 1,  (string) initObjects[1]);
            driverDb.CommandParametersValue( 2,  (int) initObjects[2]);
            driverDb.CommandParametersValue( 3,  (string) initObjects[3]);
            newId = 0;
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();
            //newId = (int) MyDbCommand.LastInsertedId;

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();
            newId = driverDb.LastInsertedId;

            return ExecutionStatus.OK;
        }
    }
}
