using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ���������� ������ �� ��������� ���
    /// </summary>
    internal class A1SaveConfigZoneConfirm : SrvDbCommand
    {
        private string QUERY = QueryLayer.UpdateMessages;
        DriverDb driverDb = new DriverDb();

        public A1SaveConfigZoneConfirm(string connectionString)
            : base(connectionString)
        {
        	driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType(CommandType.Text);
            driverDb.CommandText(QUERY);
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ZoneResult", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "ZoneMsgId", driverDb.GettingInt32());
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. ZoneMsgId
        /// 2. ZoneResult
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "A1SaveConfigZoneConfirm", "object[] initObjects"));
            if (initObjects.Length != 2)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SaveConfigZoneConfirm",
                    initObjects.Length, 2), "object[] initObjects");

            driverDb.CommandParametersValue(1, (int) initObjects[0]);
            driverDb.CommandParametersValue(0,  (int) initObjects[1]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery(); 

            return ExecutionStatus.OK;
        }
    }
}
