using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ������� ������ � ������� Messages
    /// </summary>
    internal class A1InsertMessages : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertMessages;
        DriverDb driverDb = new DriverDb();

        /// <summary>
        /// �������� ���������� ����� Message_ID ����� ������,
        /// ������� ������ ��� ���������
        /// </summary>
        private int newMessageId;

        /// <summary>
        /// �������� ���������� ����� Message_ID ����� ������,
        /// ������� ������ ��� ���������
        /// </summary>
        public int NewMessageId
        {
            get { return newMessageId; }
        }

        public A1InsertMessages(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);

                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Time1", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Time2", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "isNew", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Direction", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Command_ID", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Source_ID", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Address", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "DataFromService", driverDb.GettingString());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Mobitel_ID", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "crc", driverDb.GettingInt32());
                driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MessageCounter", driverDb.GettingInt32());
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }
        }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Time1 
        /// 2. Command_ID
        /// 3. Source_ID
        /// 4. Address(Email)
        /// 5. Mobitel_ID
        /// 6. MessageCounter
        /// 7. Direction 1-to TT, 0-from TT
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "A1InsertMessages", "object[] initObjects"));
            if (initObjects.Length != 7)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertMessages",
                    initObjects.Length, 7), "object[] initObjects");

            newMessageId = 0;
            driverDb.CommandParametersValue(0, (int) initObjects[0]);
            driverDb.CommandParametersValue(1, 0);
            driverDb.CommandParametersValue(2, 1);
            driverDb.CommandParametersValue(3, (int) initObjects[6]);
            driverDb.CommandParametersValue(4, (int) initObjects[1]);
            driverDb.CommandParametersValue(5, (int) initObjects[2]);
            driverDb.CommandParametersValue(6, (string) initObjects[3]);
            driverDb.CommandParametersValue(7, "Data");
            driverDb.CommandParametersValue(8, (int) initObjects[4]);
            driverDb.CommandParametersValue(9, 1);
            driverDb.CommandParametersValue(10, (int) initObjects[5]);
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();
            //newMessageId = (int) MyDbCommand.LastInsertedId;

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();
            newMessageId = driverDb.LastInsertedId;

            return ExecutionStatus.OK;
        }
    }
}
