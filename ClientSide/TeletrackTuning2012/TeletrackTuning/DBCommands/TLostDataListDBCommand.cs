using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;

namespace TeletrackTuning.TDBCommand
{
    /// <summary>
    /// Загрузка пропущенных данных
    /// </summary>
    internal class TLostDataListDBCommand : TDbCommand
    {
        private string SQL_QUERY = QueryLayer.SelectFromDataGpsLostOn;
        DriverDb driverDb = new DriverDb();

        public TLostDataListDBCommand(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = SQL_QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType( CommandType.Text );
            driverDb.CommandText( SQL_QUERY );
        }

        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //return MyDbCommand.ExecuteReader();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            return driverDb.CommandExecuteReader();
        }
    }
}
