﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    internal class GetServiceinit : SrvDbCommand
    {
        private string QUERY = QueryLayer.SelectFromServiceInitType;
        DriverDb driverDb = new DriverDb();

        public GetServiceinit(string connectionString)
            : base(connectionString)
        {
            try
            {
                //MyDbCommand.CommandType = CommandType.Text;
                //MyDbCommand.CommandText = QUERY;

                driverDb.GetCommand = MyDbCommand;
                driverDb.CommandType(CommandType.Text);
                driverDb.CommandText(QUERY);
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }
        }

        protected override object InternalExecuteReader()
        {
            try
            {
                //MyDbCommand.Connection = Connection;
                //Connection.Open();
                //return MyDbCommand.ExecuteReader();

                driverDb.Command_Connect = Connection;
                driverDb.ConnectionOpen();
                return driverDb.CommandExecuteReader();
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
                return null;
            }
        }
    }
}

