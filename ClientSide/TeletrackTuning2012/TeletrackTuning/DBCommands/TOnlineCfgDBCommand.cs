using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;

namespace TeletrackTuning.TDBCommand
{
    internal class TOnlineCfgDBCommand : TDbCommand
    {
        private string QUERY = QueryLayer.SelectFromServiceInit;
        DriverDb driverDb = new DriverDb();

        public TOnlineCfgDBCommand(string connectionString)
            : base(connectionString)
        {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType(CommandType.Text);
            driverDb.CommandText(QUERY);
        }

        //protected override MySqlDataReader InternalExecuteReader()
        protected override object InternalExecuteReader()
        {
            //MyDbCommand.Connection = Connection;
            driverDb.Command_Connect = Connection;
            //Connection.Open();
            driverDb.ConnectionOpen();
            //return MyDbCommand.ExecuteReader();
            return driverDb.CommandExecuteReader();
        }
    }
}
