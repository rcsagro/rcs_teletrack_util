using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TeletrackTuning.DAL;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
    /// <summary>
    /// ���������� �������� GPRS Email
    /// </summary>
    internal class A1InsertConfigGprsEmail : SrvDbCommand
    {
        private string QUERY = QueryLayer.InsertIntoConfigGprsEmail;
        DriverDb driverDb = new DriverDb();

        public A1InsertConfigGprsEmail(string connectionString)
            : base(connectionString)
        {
            try
            {
            //MyDbCommand.CommandType = CommandType.Text;
            //MyDbCommand.CommandText = QUERY;

            driverDb.GetCommand = MyDbCommand;
            driverDb.CommandType(CommandType.Text);
            driverDb.CommandText(QUERY);

            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Name", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Descr", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Message_ID", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "MobitelId", driverDb.GettingInt32());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Smtpserv", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Smtpun", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Smtppw", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Pop3serv", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Pop3un", driverDb.GettingString());
            driverDb.CommandParametersAdd(driverDb.ParamPrefics + "Pop3pw", driverDb.GettingString());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\n" + e.StackTrace);
            }

    }

        /// <summary>
        /// ������� �������� ����������:
        /// 1. Message_ID
        /// 2. MobitelId (����� � ConfigMain ���� ConfigGprsEmail_ID)
        /// 3. GprsEmailConfig
        /// </summary>
        /// <param name="initObjects"></param>
        public override void Init(params object[] initObjects)
        {
            try
            { 
            if (initObjects == null)
                throw new ArgumentNullException("object[] initObjects",
                    String.Format(ErrorText.ARGUMENT_NULL,
                        "Init", "A1InsertConfigGprsEmail", "object[] initObjects"));

            if (initObjects.Length != 3)
                throw new ArgumentException(String.Format(
                    ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigGprsEmail",
                    initObjects.Length, 3), "object[] initObjects");

            driverDb.CommandParametersValue( 0,  "");
            driverDb.CommandParametersValue( 1,  "");
            driverDb.CommandParametersValue( 2, (int) initObjects[0]);
            driverDb.CommandParametersValue( 3, (int) initObjects[1]);

            GprsEmailConfig config = (GprsEmailConfig) initObjects[2];
            driverDb.CommandParametersValue( 4, config.SmtpServer);
            driverDb.CommandParametersValue( 5, config.SmtpLogin);
            driverDb.CommandParametersValue( 6, config.SmtpPassword);
            driverDb.CommandParametersValue( 7, config.Pop3Server);
            driverDb.CommandParametersValue( 8, config.Pop3Login);
            driverDb.CommandParametersValue( 9, config.Pop3Password);
            }
            catch( Exception e )
            {
                MessageBox.Show( e.Message + "\n" + e.StackTrace );
            }
        }

        protected override ExecutionStatus InternalExecute()
        {
            //MyDbCommand.Connection = Connection;
            //Connection.Open();
            //MyDbCommand.ExecuteNonQuery();

            driverDb.Command_Connect = Connection;
            driverDb.ConnectionOpen();
            driverDb.CommandExecuteNonQuery();

            return ExecutionStatus.OK;
        }
    }
}
