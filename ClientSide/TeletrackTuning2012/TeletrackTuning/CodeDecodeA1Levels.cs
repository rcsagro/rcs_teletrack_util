﻿using System;
using System.Collections.Generic;
using System.Text;
using TeletrackTuning.Error;
using TeletrackTuning.Entity;

namespace TeletrackTuning
{
    public class CodeDecodeA1Levels
    {
        #region A1_Lelel1
        /// <summary>
        /// Преобразование Unicode строки в массив байт,
        /// в котором каждый символ представлен ASCII байтом.
        /// <para>Размер массива = размеру строки</para>
        /// </summary>
        /// <exception cref="ArgumentNullException">Не передана строка source</exception>
        /// <param name="source">Source строка</param>
        /// <returns>массив байт byte[]</returns>
        public static byte[] StringToByteArray(string source)
        {
            if (source == null)
                throw new ArgumentNullException("source", "Не передана строка source");

            byte[] result = new byte[source.Length];

            for (int i = 0; i < source.Length - 1; i++)
                result[i] = Convert.ToByte(source[i]);

            return result;
        }

        public static string GetA1Email(string source)
        {
            string ret = "";
            if (source.Length == Const.LEVEL0_PACKET_LENGTH)
                ret = source.Substring(0, Const.LEVEL0_EMAIL_LENGTH);
            else
                throw new Exception("Неправильный размер строки с пакетом А1, для получения Email отправителя");
            return ret;
        }

        public static string GetA1IDTT(PacketBlock source)
        {
            string ret_idtt = "";
            //myException = "Получение ИД_ТТ";
            for (int i = 0; i < source.id_tt.Length; i++)
            {
                ret_idtt += (Char)source.id_tt[i];
            }
            return ret_idtt;
        }

        /// <summary>
        /// Формирование строки из массива байт уровня LEVEL1
        /// </summary>
        /// <exception cref="ArgumentNullException">Не передан массив байт source</exception>
        /// <param name="source">byte[]</param>
        /// <returns>Строка</returns>
        public static string BytesToString(byte[] source)
        {
            if (source == null)
                throw new ArgumentNullException("source", "Не передан массив байт source");

            StringBuilder result = new StringBuilder();

            for (int i = 0; i < source.Length; i++)
                result.Append((char)SymbolToValue(source[i]));

            return result.ToString();
        }

        /// <summary>
        /// Преобразование принятого от телетрека байта в код ASCII 
        /// в соответствии с таблицей LEVEL1
        /// <para>Используется при декодировании</para>
        /// </summary>
        /// <param name="symbolCode">byte</param>
        /// <returns>byte</returns>
        public static byte SymbolToValue(byte symbolCode)
        {
            byte value = 0;
            char symbol = Convert.ToChar(symbolCode);

            if ((symbol >= 'A') && (symbol <= 'Z'))
                value = (byte)(symbolCode - ASCII_CODE_A);

            else if ((symbol >= 'a') && (symbol <= 'z'))
                value = (byte)(symbolCode - Convert.ToByte('a') + 26);

            else if ((symbol >= '0') && (symbol <= '9'))
                value = (byte)(symbolCode - Convert.ToByte('0') + 52);

            else if (symbol == '+')
                value = 62;

            else if (symbol == '-')
                value = 63;

            else if ((symbolCode >= 58) && (symbolCode <= 63))
                value = (byte)(symbolCode - 58 + ASCII_CODE_A);

            return value;
        }

        private static readonly byte ASCII_CODE_A =
          Convert.ToByte('A');

        /// <summary>
        /// Преобразование ASCII код в код символа А1, 
        /// в процессе кодирования сообщения,
        /// <para>в соответствии с таблицей LEVEL1</para>
        /// </summary>
        /// <param name="value">byte</param>
        /// <returns>byte</returns>
        public static byte ValueToSymbol(byte value)
        {
            byte symbol = Convert.ToByte('.');
            if (value <= 25)
                symbol = (byte)(ASCII_CODE_A + value);

            else if ((value >= 26) && (value <= 51))
                symbol = (byte)(Convert.ToByte('a') + value - 26);

            else if ((value >= 52) && (value <= 61))
                symbol = (byte)(Convert.ToByte('0') + value - 52);

            else if (value == 62)
                symbol = Convert.ToByte('+');

            else if (value == 63)
                symbol = Convert.ToByte('-');

            else if ((value >= ASCII_CODE_A) && (value <= Convert.ToByte('F')))
                symbol = (byte)(value - ASCII_CODE_A + 58);

            return symbol;
        }

        #endregion

        /// <summary>
        /// Перекодировка символов согласно таблице кодирования
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static PacketBlock ConvertSymbolToPacketBlock(byte[] source)
        {
            PacketBlock packBlock = new PacketBlock();
            if (source.Length != Const.LEVEL0_PACKET_LENGTH)
                throw new Exception("Неправильный размер строки с пакетом А1, для конвертации в 6 бит");
            if (source[Const.LEVEL0_START_INDICATOR_POSITION] != Const.START_INDICATOR_SYMBOL ||
              source[Const.LEVEL0_START_INDICATOR_POSITION + 1] != Const.START_INDICATOR_SYMBOL)
                throw new Exception("Не найден признак начала пакета");

            int startPosition = Const.LEVEL0_PACKET_LENGTH - Const.LEVEL1_PACKET_LENGHT;
            packBlock.version[0] = SymbolToValue(source[startPosition++]);
            packBlock.version[1] = SymbolToValue(source[startPosition++]);

            packBlock.lenght = SymbolToValue(source[startPosition++]);
            for (int i = 0; i < Const.LEVEL0_IDTT_LENGHT; i++)
                packBlock.id_tt[i] = SymbolToValue(source[startPosition++]);
            packBlock.crc = SymbolToValue(source[startPosition++]);
            for (int i = 0; i < Const.LEVEL1_DATA_BLOCK_LENGHT; i++)
            {
                packBlock.data[i] = SymbolToValue(source[i + startPosition]);
            }

            packBlock.isCrcOk = TestCRC6(source);
            return packBlock;
        }

        private static bool TestCRC6(byte[] source)
        {
            bool isOk = false;
            byte tmpCRC = 0;
            byte[] massPacket = new byte[Const.LEVEL1_PACKET_LENGHT];
            Array.Copy(source, Const.LEVEL0_START_INDICATOR_POSITION + 2, massPacket, 0, Const.LEVEL1_PACKET_LENGHT);
            int lenght = SymbolToValue(massPacket[Const.LEVEL1_LENGTH_POSITION]);
            for (int i = 0; i <= Const.LEVEL1_DATA_BLOCK_LENGHT; i++)
            {
                // Исключаем из рассмотрения байт в котором записан CRC
                if (i != Const.LEVEL1_CRC_POSITION)
                {
                    tmpCRC += massPacket[i];
                }
            }
            // Делим на 4, чтобы запихнуть в 6 bit
            tmpCRC = (byte)(tmpCRC >> 2);
            if (SymbolToValue(massPacket[Const.LEVEL1_CRC_POSITION]) == tmpCRC)
                isOk = true;
            return isOk;
        }

        /// <summary>
        /// Конвертация 6-ти битных данных в 8-ми битные
        /// </summary>
        /// <param name="block"></param>
        /// <returns></returns>
        public static DataBlock8bit ConverPacket6bitTo8bit(PacketBlock packBlock)
        {
            DataBlock8bit dBlock = new DataBlock8bit();
            for (int i = 0, f = 0; i < dBlock.data.Length; i += 3, f += 4)//Конвертируем из 6-ти битных в 8-ми байтные
            {
                dBlock.data[i] = (byte)((packBlock.data[f] << 2) + (packBlock.data[f + 3] & 0x03));
                dBlock.data[i + 1] = (byte)((packBlock.data[f + 1] << 2) + ((packBlock.data[f + 3] >> 2) & 0x03));
                dBlock.data[i + 2] = (byte)((packBlock.data[f + 2] << 2) + ((packBlock.data[f + 3] >> 4) & 0x03));
            }
            return dBlock;
        }

        /// <summary>
        /// Преобразование блока данных из 8 битной в 6 битную систему. 
        /// <para>Длина кодируемого блока = 102 байта(размер пакета на уровне LEVEL3).</para>
        /// </summary>
        /// <exception cref="ArgumentException">Передан пустой массив байт source</exception>
        /// <exception cref="ArgumentNullException">Длина массива source не равна 102</exception>
        /// <param name="source">источник byte[102]</param>
        /// <returns>byte[136]</returns>
        public static byte[] Encode8BitTo6(byte[] source)
        {
            if (source == null)
                throw new ArgumentNullException("source", "Не передан массив байт source");
            if (source.Length != Const.LEVEL3_PACKET_LENGHT)
                throw new ArgumentException("source", "Длина переданного массива source (" +
                  source.Length + ") не соответсвует требуемой длине - " +
                  Const.LEVEL3_PACKET_LENGHT);

            byte[] result = new byte[Const.LEVEL1_DATA_BLOCK_LENGHT];

            const int blocks = Const.LEVEL1_DATA_BLOCK_LENGHT >> 2;

            for (int i = 0; i < blocks; i++)
            {
                byte sX, sY, sZ, sA = 0, sB = 0, sC = 0, sD = 0;
                int ind = 3 * i;

                sX = source[ind];
                sY = source[ind + 1];
                sZ = source[ind + 2];

                sA = (byte)(sX >> 2);
                sB = (byte)(sY >> 2);
                sC = (byte)(sZ >> 2);

                sX = (byte)(sX << 6);
                sX = (byte)(sX >> 6);
                sY = (byte)(sY << 6);
                sY = (byte)(sY >> 4);
                sZ = (byte)(sZ << 6);
                sZ = (byte)(sZ >> 2);

                sD = (byte)(sX + sY + sZ);

                result[i * 4] = ValueToSymbol(sA);
                result[i * 4 + 1] = ValueToSymbol(sB);
                result[i * 4 + 2] = ValueToSymbol(sC);
                result[i * 4 + 3] = ValueToSymbol(sD);
            }
            return result;
        }

        /// <summary>
        /// Преобразование блока данных из 6 битной в 8 битную систему на уровне LEVEL1.
        /// <para>Длина декодируемого блока данных должна быть равна 136 байт
        /// (Data block на уровне LEVEL1).</para>
        /// </summary>
        /// <exception cref="ArgumentException">Длина переданного массива dataBlock не соответсвует требуемой длине</exception>
        /// <exception cref="ArgumentNullException">Не передан массив байт dataBlock</exception>
        /// <param name="dataBlock">byte[136] Data block на уровне LEVEL1</param>
        /// <returns>byte[102] сообщение 3-его уровня</returns>
        public static byte[] Decode6BitTo8(byte[] dataBlock)
        {
            if (dataBlock == null)
                throw new ArgumentNullException("dataBlock", "Не передан массив байт dataBlock");
            if (dataBlock.Length != Const.LEVEL1_DATA_BLOCK_LENGHT)
                throw new ArgumentException("dataBlock", "Длина переданного массива dataBlock (" +
                  dataBlock.Length + ") не соответсвует требуемой длине - " +
                  Const.LEVEL1_DATA_BLOCK_LENGHT);

            byte[] result = new byte[Const.LEVEL3_PACKET_LENGHT];
            int blockCount = Const.LEVEL1_DATA_BLOCK_LENGHT >> 2;

            for (int i = 0; i < blockCount; i++)
            {
                byte sX, sY, sZ, sA, sB, sC, sD, AD, BD, CD;
                int ind = 4 * i;

                sA = SymbolToValue(dataBlock[ind]);
                sB = SymbolToValue(dataBlock[ind + 1]);
                sC = SymbolToValue(dataBlock[ind + 2]);
                sD = SymbolToValue(dataBlock[ind + 3]);

                sX = (byte)(sA << 2);
                sY = (byte)(sB << 2);
                sZ = (byte)(sC << 2);

                AD = (byte)(sD << 6);
                AD = (byte)(AD >> 6);

                BD = (byte)(sD << 4);
                BD = (byte)(BD >> 6);

                CD = (byte)(sD << 2);
                CD = (byte)(CD >> 6);

                sX = (byte)(sX + AD);
                sY = (byte)(sY + BD);
                sZ = (byte)(sZ + CD);

                result[i * 3] = (byte)(sX & 0xff);
                result[i * 3 + 1] = (byte)(sY & 0xff);
                result[i * 3 + 2] = (byte)(sZ & 0xff);
            }

            return result;
        }

        #region A1_Level3

        /// <summary>
        /// Обработка сообщения на 3-ем уровне LEVEL3
        /// </summary>
        /// <exception cref="A1Exception">A1Exception</exception>
        /// <param name="command">byte[102] Пакет команды LEVEL3</param>
        /// <returns>CommonDescription</returns>
        public static CommonDescription DecodeLevel3Message(byte[] command)
        {
            if (command.Length != Const.LEVEL3_PACKET_LENGHT)
                throw new A1Exception(String.Format(ErrorText.LENGTH_PACKET,
                  "LEVEL3", Const.LEVEL3_PACKET_LENGHT, command.Length));

            byte commandId = command[0];
            ushort messageId = Level4Converter.BytesToUShort(command, Const.LEVEL3_MESSAGEID_POSITION);

            CommonDescription result;
            // Блок данных на уровне LEVEL3
            byte[] dataBlock = new byte[Const.LEVEL3_DATA_BLOCK_LENGTH];
            Array.Copy(command, Const.LEVEL3_DATA_BLOCK_POSITION, dataBlock, 0, Const.LEVEL3_DATA_BLOCK_LENGTH);

            switch (commandId)
            {
                case (byte)CommandDescriptor.SMSConfigConfirm:
                case (byte)CommandDescriptor.SMSConfigAnswer:
                    result = GetSmsAddrConfig(dataBlock);
                    break;

                case (byte)CommandDescriptor.PhoneConfigConfirm:
                case (byte)CommandDescriptor.PhoneConfigAnswer:
                    result = GetPhoneNumberConfig(dataBlock);
                    break;

                case (byte)CommandDescriptor.EventConfigConfirm:
                case (byte)CommandDescriptor.EventConfigAnswer:
                    result = GetEventConfig(dataBlock);
                    break;

                case (byte)CommandDescriptor.UniqueConfigConfirm:
                case (byte)CommandDescriptor.UniqueConfigAnswer:
                    result = GetUniqueConfig(dataBlock);
                    break;

                case (byte)CommandDescriptor.IdConfigConfirm:
                case (byte)CommandDescriptor.IdConfigAnswer:
                    result = GetIdConfig(dataBlock);
                    break;

                case (byte)CommandDescriptor.ZoneConfigConfirm:
                    result = GetZoneConfigConfirm(dataBlock);
                    break;

                case (byte)CommandDescriptor.DataGpsAnswer:
                case (byte)CommandDescriptor.DataGpsAuto:
                    result = GetDataGps(dataBlock);
                    break;

                case (byte)CommandDescriptor.GprsBaseConfigConfirm:
                case (byte)CommandDescriptor.GprsBaseConfigAnswer:
                    result = GetGprsBaseConfig(dataBlock);
                    break;

                case (byte)CommandDescriptor.GprsEmailConfigConfirm:
                case (byte)CommandDescriptor.GprsEmailConfigAnswer:
                    result = GetGprsEmailConfig(dataBlock);
                    break;

                case (byte)CommandDescriptor.GprsFtpConfigConfirm:
                case (byte)CommandDescriptor.GprsFtpConfigAnswer:
                    result = GetGprsFtpConfig(dataBlock);
                    break;

                case (byte)CommandDescriptor.GprsSocketConfigConfirm:
                case (byte)CommandDescriptor.GprsSocketConfigAnswer:
                    result = GetGprsSocketConfig(dataBlock);
                    break;

                case (byte)CommandDescriptor.GprsProviderConfigConfirm:
                case (byte)CommandDescriptor.GprsProviderConfigAnswer:
                    result = GetGprsProviderConfig(dataBlock);
                    break;
                case (byte)CommandDescriptor.Ping:
                    result = new CommonDescription();
                    break;
                default:
                    throw new A1Exception(String.Format(
                      ErrorText.COMMAND_UNKNOWN, commandId));
            }

            result.CommandID = (CommandDescriptor)commandId;
            result.MessageID = messageId;
            return result;
        }

        /// <summary>
        /// Декодирование сообщений кофигурации SMS адресов 11, 21, 41
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns>SmsAddrConfig параметры для адресов SMS, E-MAIL и 
        /// WEB SERVER (GPRS) сообщений.</returns>
        private static SmsAddrConfig GetSmsAddrConfig(byte[] command)
        {
            SmsAddrConfig result = new SmsAddrConfig();
            result.DsptEmailGprs = Level4Converter.BytesToString(command, 0, 30);
            result.DsptEmailSMS = Level4Converter.BytesToString(command, 30, 14);
            result.SmsCentre = Level4Converter.BytesToTelNumber(command, 44, 11);
            result.SmsDspt = Level4Converter.BytesToTelNumber(command, 55, 11);
            result.SmsEmailGate = Level4Converter.BytesToTelNumber(command, 66, 11);
            return result;
        }

        /// <summary>
        /// Декодирование сообщения кофигурации  телефонных номеров 12, 22, 42
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns> Конфигурация номеров телефонов</returns>
        private static PhoneNumberConfig GetPhoneNumberConfig(byte[] command)
        {
            PhoneNumberConfig result = new PhoneNumberConfig();
            result.NumberAccept1 = Level4Converter.BytesToTelNumber(command, 0, 11);
            result.NumberAccept2 = Level4Converter.BytesToTelNumber(command, 11, 11);
            result.NumberAccept3 = Level4Converter.BytesToTelNumber(command, 22, 11);
            result.NumberDspt = Level4Converter.BytesToTelNumber(command, 33, 11);
            result.Name1 = Level4Converter.BytesToString(command, 45, 8);
            result.Name2 = Level4Converter.BytesToString(command, 53, 8);
            result.Name3 = Level4Converter.BytesToString(command, 61, 8);
            result.NumberSOS = Level4Converter.BytesToTelNumber(command, 69, 11);
            return result;
        }

        /// <summary>
        /// Декодирование сообщения кофигурации событий 13, 23, 43
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns>Конфигурация событий</returns>
        private static EventConfig GetEventConfig(byte[] command)
        {
            EventConfig result = new EventConfig();
            result.SpeedChange = Convert.ToSByte(command[0]);
            result.CourseBend = Level4Converter.BytesToUShort(command, 1);
            result.Distance1 = Level4Converter.BytesToUShort(command, 3);
            result.Distance2 = Level4Converter.BytesToUShort(command, 5);
            for (int i = 0; i < 32; i++)
                result.EventMask[i] = Level4Converter.BytesToUShort(command, (i << 1) + 7);
            result.MinSpeed = Level4Converter.BytesToUShort(command, 71);
            result.Timer1 = Level4Converter.BytesToUShort(command, 73);
            result.Timer2 = Level4Converter.BytesToUShort(command, 75);
            return result;
        }

        /// <summary>
        /// Декодирование сообщения кофигурации уникальных параметров 14, 24, 44
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns>Конфигурация уникальных параметров</returns>
        private static UniqueConfig GetUniqueConfig(byte[] command)
        {
            UniqueConfig result = new UniqueConfig();
            result.DispatcherID = Level4Converter.BytesToString(command, 0, 4);
            result.Password = Level4Converter.BytesToString(command, 4, 8);
            result.TmpPassword = Level4Converter.BytesToString(command, 12, 8);
            return result;
        }

        /// <summary>
        /// Декодирование сообщения кофигурации идентификационных параметров 17, 27, 47
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns>Конфигурация идентификационных параметров</returns>
        private static IdConfig GetIdConfig(byte[] command)
        {
            IdConfig result = new IdConfig();
            result.DevIdShort = Level4Converter.BytesToString(command, 0, 4);
            result.DevIdLong = Level4Converter.BytesToString(command, 4, 16);
            result.ModuleIdGps = Level4Converter.BytesToString(command, 20, 4);
            result.ModuleIdGsm = Level4Converter.BytesToString(command, 24, 4);
            //result.ModuleIdMm = Level4Converter.BytesToString(command, 28, 4);
            result.ModuleIdRf = Level4Converter.BytesToString(command, 32, 4);
            result.ModuleIdSs = Level4Converter.BytesToString(command, 36, 4);
            result.VerProtocolLong = Level4Converter.BytesToString(command, 40, 16);
            result.VerProtocolShort = Level4Converter.BytesToString(command, 56, 2);
            return result;
        }

        /// <summary>
        /// Декодирование подтверждения обработки команды кофигурации контрольных зон 25
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns>Ответа на команду кофигурации контрольных зон</returns>
        private static ZoneConfigConfirm GetZoneConfigConfirm(byte[] command)
        {
            ZoneConfigConfirm result = new ZoneConfigConfirm();
            result.ZoneMsgID = Level4Converter.BytesToInt(command, 0);
            result.ZoneState = command[4];
            result.Result = command[5];
            return result;
        }

        /// <summary>
        /// Декодирование GPS данных 46, 49
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns>GPS данные</returns>
        private static DataGpsAnswer GetDataGps(byte[] command)
        {
            DataGpsAnswer result = new DataGpsAnswer();
            short index = 0;
            result.WhatWrite = Level4Converter.BytesToUShort(command, index);
            index += 2;

            byte packetCount = command[index];
            index++;

            for (int i = 0; i < packetCount; i++)
            {
                DataGps data = new DataGps();
                if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.TIME))
                {
                    data.Time = Level4Converter.BytesToInt(command, index);
                    index += 4;
                }
                if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.LATITUDE))
                {
                    data.Latitude = Level4Converter.BytesToInt(command, index) * 10;
                    index += 4;
                }
                if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.LONGITUDE))
                {
                    data.Longitude = Level4Converter.BytesToInt(command, index) * 10;
                    index += 4;
                }
                if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.ALTITUDE))
                {
                    data.Altitude = command[index];
                    index++;
                }
                if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.DIRECTION))
                {
                    data.Direction = command[index];
                    index++;
                }
                if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.SPEED))
                {
                    data.Speed = command[index];
                    index++;
                }
                if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.LOGID))
                {
                    data.LogID = Level4Converter.BytesToInt(command, index);
                    index += 4;
                }
                if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.FLAGS))
                {
                    data.Flags = command[index];
                    index++;
                }
                if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.EVENTS))
                {
                    data.Events = Level4Converter.BytesToUInt(command, index);
                    index += 4;
                }
                if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.SENSORS))
                {
                    data.Sensor1 = command[index];
                    index++;
                    data.Sensor2 = command[index];
                    index++;
                    data.Sensor3 = command[index];
                    index++;
                    data.Sensor4 = command[index];
                    index++;
                    data.Sensor5 = command[index];
                    index++;
                    data.Sensor6 = command[index];
                    index++;
                    data.Sensor7 = command[index];
                    index++;
                    data.Sensor8 = command[index];
                    index++;
                }
                if (Util.IsBitSetInMask(result.WhatWrite, (byte)DataGpsWhatSend.COUNTERS))
                {
                    data.Counter1 = Level4Converter.BytesToUShort(command, index);
                    index += 2;
                    data.Counter2 = Level4Converter.BytesToUShort(command, index);
                    index += 2;
                    data.Counter3 = Level4Converter.BytesToUShort(command, index);
                    index += 2;
                    data.Counter4 = Level4Converter.BytesToUShort(command, index);
                    index += 2;
                } // if (Util.IsBitSetInMask)

                // Установка валидности
                if ((Math.Abs(data.Longitude) > Const.MAX_LONGITUDE) ||
                  (Math.Abs(data.Latitude) > Const.MAX_LATITUDE))
                    data.Valid = false;
                else
                    data.Valid = (data.Flags & 0x08) != 0;

                result.Add(data);
            }

            return result;
        }

        /// <summary>
        /// Декодирование сообщения кофигурации основных настроек GPRS 50, 60, 80
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns>Основные настройки GPRS</returns>
        private static GprsBaseConfig GetGprsBaseConfig(byte[] command)
        {
            GprsBaseConfig result = new GprsBaseConfig();
            result.Mode = Level4Converter.BytesToUShort(command, 0);
            result.ApnServer = Level4Converter.BytesToString(command, 2, 25);
            result.ApnLogin = Level4Converter.BytesToString(command, 27, 10);
            result.ApnPassword = Level4Converter.BytesToString(command, 37, 10);
            result.DnsServer = Level4Converter.BytesToString(command, 47, 16);
            result.DialNumber = Level4Converter.BytesToString(command, 63, 11);
            result.GprsLogin = Level4Converter.BytesToString(command, 74, 10);
            result.GprsPassword = Level4Converter.BytesToString(command, 84, 10);
            return result;
        }

        /// <summary>
        /// Декодирование сообщения кофигурации настроек Email GPRS 51, 61, 81. 
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns>Настройки Email GPRS</returns>
        private static GprsEmailConfig GetGprsEmailConfig(byte[] command)
        {
            GprsEmailConfig result = new GprsEmailConfig();
            result.SmtpServer = Level4Converter.BytesToString(command, 0, 25);
            result.SmtpLogin = Level4Converter.BytesToString(command, 25, 10);
            result.SmtpPassword = Level4Converter.BytesToString(command, 35, 10);
            result.Pop3Server = Level4Converter.BytesToString(command, 45, 25);
            result.Pop3Login = Level4Converter.BytesToString(command, 70, 10);
            result.Pop3Password = Level4Converter.BytesToString(command, 80, 10);
            return result;
        }

        /// <summary>
        /// Декодирование сообщения кофигурации настроек FTP GPRS 53, 63, 83. 
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns>Настройки  FTP GPRS</returns>
        private static GprsFtpConfig GetGprsFtpConfig(byte[] command)
        {
            GprsFtpConfig result = new GprsFtpConfig();
            result.Server = Level4Converter.BytesToString(command, 0, 25);
            result.Login = Level4Converter.BytesToString(command, 25, 10);
            result.Password = Level4Converter.BytesToString(command, 35, 10);
            result.ConfigPath = Level4Converter.BytesToString(command, 45, 20);
            result.PutPath = Level4Converter.BytesToString(command, 65, 20);
            return result;
        }

        /// <summary>
        /// Декодирование сообщения кофигурации настроек Socket GPRS 52, 62, 82. 
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns>Настройки  Socket GPRS</returns>
        private static GprsSocketConfig GetGprsSocketConfig(byte[] command)
        {
            GprsSocketConfig result = new GprsSocketConfig();
            result.Server = Level4Converter.BytesToString(command, 0, 20);
            result.Port = Level4Converter.BytesToUShort(command, 20);
            return result;
        }

        /// <summary>
        /// Декодирование сообщения настроек подключения к провайдеру GPRS 54, 64, 84. 
        /// </summary>
        /// <param name="command">Сообщение</param>
        /// <returns>Конфигурация подключения к провайдеру GPRS</returns>
        private static GprsProviderConfig GetGprsProviderConfig(byte[] command)
        {
            GprsProviderConfig result = new GprsProviderConfig();
            result.InitString = Level4Converter.BytesToString(command, 0, 50);
            result.Domain = Level4Converter.BytesToString(command, 50, 25);
            return result;
        }

        #endregion
    }

    /// <summary>
    /// Клас с разбивкой пакета на данные, версию протокола, масив id_tt[4], длину данных, CRC и флаг правильности CRC
    /// </summary>
    public class PacketBlock
    {
        public byte[] data = new byte[Const.LEVEL1_DATA_BLOCK_LENGHT];
        public byte crc = 0;
        public byte lenght = 0;
        public byte[] version = new byte[2];
        public byte[] id_tt = new byte[Const.LEVEL0_IDTT_LENGHT];
        public bool isCrcOk = false;
    };

    /// <summary>
    /// Масив байт нужного размера 102
    /// </summary>
    public class DataBlock8bit
    {
        public byte[] data = new byte[Const.LEVEL3_PACKET_LENGHT];
    };
}
