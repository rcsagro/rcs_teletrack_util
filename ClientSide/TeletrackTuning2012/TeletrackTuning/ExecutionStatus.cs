﻿namespace TeletrackTuning
{
  /// <summary>
  /// Статус обработки данных
  /// </summary>
  public enum ExecutionStatus
  {
    /// <summary>
    /// Все впорядке
    /// </summary>
    OK = 0,
    /// <summary>
    /// Ошибка при работе с БД
    /// </summary>
    ERROR_DB = 1,
    /// <summary>
    /// присутсвуют ошибочные значения в полях
    /// или др. ошибки
    /// </summary>
    ERROR_DATA = 2
  }
}