﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;
//using TeletrackTuning.TDBCommand;

namespace TeletrackTuning.SrvDBCommand
{
  class TransferBufferDBCommand : SrvDbCommand  
  {
    private const string PROC_NAME = "OnTransferBuffer";

    public TransferBufferDBCommand(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.StoredProcedure;
      MyDbCommand.CommandText = PROC_NAME;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
