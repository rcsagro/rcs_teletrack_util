namespace TeletrackTuning.Entity
{
  /// <summary>
  /// ���� ����� ������ �������� ����������� ���
  /// </summary>
  public enum ZoneCfgPacketType
  {
    /// <summary>
    /// ��������� ��� (���-�� ���, ���-�� ����� � ����� � �.�.) = 1
    /// </summary>
    Config = 1,
    /// <summary>
    /// �������� ���. ��������������� = 2
    /// </summary>
    Name = 2,
    /// <summary>
    /// ���������� ����� = 3
    /// </summary>
    Point = 3,
    /// <summary>
    /// ����� ������-������ ��� ������ ���� = 4
    /// </summary>
    Mask = 4
  }
}
