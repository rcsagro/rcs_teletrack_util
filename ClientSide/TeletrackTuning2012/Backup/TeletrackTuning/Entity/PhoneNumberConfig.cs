using System;
using TeletrackTuning.Error;

namespace TeletrackTuning.Entity
{
  /// <summary>
  /// ������������ ���������� �������. 
  /// �������������� ������: 12, 22, 42
  /// </summary>
  public class PhoneNumberConfig : CommonDescription
  {
    /// <summary>
    /// ���������� ����� �1, � �������� �������� ����� ��������� �������� ������.
    /// �������� 11 ��������.
    /// </summary>
    public string NumberAccept1
    {
      get { return numberAccept1; }
      set { numberAccept1 = value; }
    }
    private string numberAccept1;

    /// <summary>
    /// ���������� ����� �2, � �������� �������� ����� ��������� �������� ������.
    /// �������� 11 ��������.
    /// </summary>
    public string NumberAccept2
    {
      get { return numberAccept2; }
      set { numberAccept2 = value; }
    }
    private string numberAccept2;

    /// <summary>
    /// ���������� ����� �3, � �������� �������� ����� ��������� �������� ������.
    /// �������� 11 ��������.
    /// </summary>
    public string NumberAccept3
    {
      get { return numberAccept3; }
      set { numberAccept3 = value; }
    }
    private string numberAccept3;

    /// <summary>
    /// ����� �������� ����������, �� ������� ����� ���������� ������ ��� ������� �� ������ Dispatcher.
    /// �������� 11 ��������.
    /// </summary>
    public string NumberDspt
    {
      get { return numberDspt; }
      set { numberDspt = value; }
    }
    private string numberDspt;

    /// <summary>
    /// ����� �������� ����������, �� ������� ����� ���������� ������ ��� ������� �� ������ SOS.
    /// �������� 11 ��������.
    /// </summary>
    public string NumberSOS
    {
      get { return numberSOS; }
      set { numberSOS = value; }
    }
    private string numberSOS;

    /// <summary>
    /// ���������������. ������ �� ������������.
    /// �������� 1 ��������.
    /// </summary>
    public string NumberMask
    {
      get { return numberMask; }
      set { numberMask = value; }
    }
    private string numberMask;

    /// <summary>
    /// ��� ��������, ������� ����� ��������� �� �����, ��� ������ � ������ 1.
    /// �������� 8 ��������.
    /// </summary>
    public string Name1
    {
      get { return name1; }
      set { name1 = value; }
    }
    private string name1;

    /// <summary>
    /// ��� ��������, ������� ����� ��������� �� �����, ��� ������ � ������ 2.
    /// �������� 8 ��������.
    /// </summary>
    public string Name2
    {
      get { return name2; }
      set { name2 = value; }
    }
    private string name2;

    /// <summary>
    /// ��� ��������, ������� ����� ��������� �� �����, ��� ������ � ������ 3.
    /// �������� 8 ��������.
    /// </summary>
    public string Name3
    {
      get { return name3; }
      set { name3 = value; }
    }
    private string name3;

    /// <summary>
    /// �����������
    /// </summary>
    public PhoneNumberConfig()
    {
      numberAccept1 = "";
      numberAccept2 = "";
      numberAccept3 = "";
      numberDspt = "";
      numberSOS = "";
      name1 = "";
      name2 = "";
      name3 = "";
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;

      if (NumberAccept1.Length > 11)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "NumberAccept1(���������� ����� �1)",
          "PhoneNumberConfig(������������ ���������� �������)",
          11);
        return false;
      } // if (DsptEmailGprs.Length)
      if (NumberAccept2.Length > 11)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "NumberAccept2(���������� ����� �2)",
          "PhoneNumberConfig(������������ ���������� �������)",
          11);
        return false;
      }
      if (NumberAccept3.Length > 11)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "NumberAccept3(���������� ����� �3)",
          "PhoneNumberConfig(������������ ���������� �������)",
          11);
        return false;
      }
      if (NumberDspt.Length > 11)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "NumberDspt(����� �������� ���������� ��� ������ Dispatcher)",
          "PhoneNumberConfig(������������ ���������� �������)",
          11);
        return false;
      }
      if (NumberSOS.Length > 11)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "NumberSOS(����� �������� ���������� ��� ������ SOS)",
          "PhoneNumberConfig(������������ ���������� �������)",
          11);
        return false;
      }

      if (Name1.Length > 8)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "Name1(��� �������� 1)",
          "PhoneNumberConfig(������������ ���������� �������)",
          8);
        return false;
      }
      if (Name2.Length > 8)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "Name2(��� �������� 2)",
          "PhoneNumberConfig(������������ ���������� �������)",
          8);
        return false;
      }
      if (Name3.Length > 8)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "Name3(��� �������� 3)",
          "PhoneNumberConfig(������������ ���������� �������)",
          8);
        return false;
      }
      return true;
    }
  }
}
