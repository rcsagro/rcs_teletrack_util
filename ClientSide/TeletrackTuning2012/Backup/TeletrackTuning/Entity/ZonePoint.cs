#region Using directives
using System;
using TeletrackTuning.Error;
#endregion

namespace TeletrackTuning.Entity
{
  /// <summary>
  /// �������� ����� - ������, �������
  /// </summary>
  public class ZonePoint
  {
    /// <summary>
    /// ������, ������� * 600000
    /// </summary>
    public readonly int Latitude;
    /// <summary>
    /// �������,  ������� * 600000
    /// </summary>
    public readonly int Longitude;

    /// <summary>
    ///  �������� ==
    /// </summary>
    /// <param name="zonePoint1">Zone point 1</param>
    /// <param name="zonePoint2">Zone point 2</param>
    /// <returns>Bool</returns>
    public static bool operator ==(ZonePoint zonePoint1, ZonePoint zonePoint2)
    {
      return ((zonePoint1.Latitude == zonePoint2.Latitude) &&
        (zonePoint1.Longitude == zonePoint2.Longitude));
    }

    /// <summary>
    ///  �������� !=
    /// </summary>
    /// <param name="zonePoint1">Zone point 1</param>
    /// <param name="zonePoint2">Zone point 2</param>
    /// <returns>Bool</returns>
    public static bool operator !=(ZonePoint zonePoint1, ZonePoint zonePoint2)
    {
      return ((zonePoint1.Latitude != zonePoint2.Latitude) ||
        (zonePoint1.Longitude != zonePoint2.Longitude));
    }

    /// <summary>
    /// �������� Equals
    /// </summary>
    /// <param name="obj">object</param>
    /// <returns>True ���� ������� ����������</returns>
    public override bool Equals(object obj)
    {
      return ((obj != null) && (obj.GetType() == this.GetType()) &&
        ((this.Latitude == ((ZonePoint)obj).Latitude) &&
        (this.Longitude == ((ZonePoint)obj).Longitude)));
    }

    /// <summary>
    /// Hash Code
    /// </summary>
    /// <returns>HashCode</returns>
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    /// <summary>
    /// �����������
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    /// <param name="latitude">������, ������� * 600000</param>
    /// <param name="longitude">�������,  ������� * 600000</param>
    public ZonePoint(int latitude, int longitude)
    {
      if (Math.Abs(longitude) > Const.MAX_LONGITUDE)
      {
        throw new ArgumentOutOfRangeException("longitude", String.Format(
          ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "longitude(�������)",
          "ZonePoint( �������� �����)",
          Const.MAX_LONGITUDE * -1, Const.MAX_LONGITUDE));
      }
      if (Math.Abs(latitude) > Const.MAX_LATITUDE)
      {
        throw new ArgumentOutOfRangeException("latitude", String.Format(
          ErrorText.ATTRIBUTE_OUT_OF_RANGE,
          "latitude(������)",
          "ZonePoint( �������� �����)",
          Const.MAX_LATITUDE * -1, Const.MAX_LATITUDE));
      }
      Latitude = latitude;
      Longitude = longitude;
    }
  }
}
