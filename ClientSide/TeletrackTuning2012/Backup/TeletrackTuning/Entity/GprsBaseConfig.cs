using System;
using TeletrackTuning.Error;

namespace TeletrackTuning.Entity
{
  /// <summary>
  /// ������������ �������� GPRS. �������� ���������.
  /// �������������� ������: 50, 60, 80
  /// </summary>
  public class GprsBaseConfig : CommonDescription
  {
    /// <summary>
    /// APN ������, �������� 25 ��������.
    /// ����� ������� ��������� ������� ����.
    /// </summary>
    public string ApnServer
    {
      get { return apnServer; }
      set { apnServer = value; }
    }
    private string apnServer;

    /// <summary>
    /// APN ����� (��� ����� �������), �������� 10 ��������.
    /// </summary>
    public string ApnLogin
    {
      get { return apnLogin; }
      set { apnLogin = value; }
    }
    private string apnLogin;

    /// <summary>
    /// APN ������ (��� ����� �������), �������� 10 ��������.
    /// </summary>
    public string ApnPassword
    {
      get { return apnPassword; }
      set { apnPassword = value; }
    }
    private string apnPassword;

    /// <summary>
    /// Dns ������, �������� 16 ��������.
    /// DNS ������ �������� ������ � ���� IP ������ 
    /// (�������� DNS ������ ��������� ������� �����) -
    /// ���������� ��������� ���� ������������ ���������� �������� �������
    /// (Pop3Server) � ������� 51. ���� � ���� Pop3Server ������� 51 
    /// ����������� IP �����, �� � ���� ������ ����������� DNS ������ �������������.
    /// </summary>
    public string DnsServer
    {
      get { return dnsServer; }
      set { dnsServer = value; }
    }
    private string dnsServer;

    /// <summary>
    /// ����� ������� CSD, �������� 11 ��������.
    /// </summary>
    public string DialNumber
    {
      get { return dialNumber; }
      set { dialNumber = value; }
    }
    private string dialNumber;

    /// <summary>
    /// ����� ��� ����� � GPRS ���� ��� CSD. �������� 10 ��������.
    /// </summary>
    public string GprsLogin
    {
      get { return gprsLogin; }
      set { gprsLogin = value; }
    }
    private string gprsLogin;

    /// <summary>
    /// ������ ��� ����� � GPRS ���� ��� CSD. �������� 10 ��������.
    /// </summary>
    public string GprsPassword
    {
      get { return gprsPassword; }
      set { gprsPassword = value; }
    }
    private string gprsPassword;

    /// <summary>
    /// ����� ������ GPRS:
    /// <para>10 � Sockets off-line GPRS. �������� �������������� ���
    /// ����������� ������� ��� �������� ���������� ��������
    /// "�������� GPRS �� Email" - ������������� ��� � ����� �������.
    /// ��� ����, ����� �������� ��������� ������ ������ ���� ��������
    /// ���� "�������� GPRS �� Email" � ����� ���� �� ������ ������� �
    /// ���������� �������� ����� �������.
    /// �������� �� �����, ����� ������ 100 ������ ������������� ����� �
    /// ������ 1 ������ ��� �������� ������. ��� �����
    /// <list type="number">
    ///   <item><description>����������� �������</description></item>
    ///   <item><description>����������� Distance1 = 100</description></item>
    ///   <item><description>����������� Timer1 = 60</description></item>
    ///   <item><description>���������� ����� �������(����� �������� ���� ������� ��� �����������
    ///   ����� �������) ��� Distance1 (��� ����� ����� EventMask5)-
    ///   ��������� ������ �������� ���� "������ � ���" � �������.</description></item>
    ///   <item><description>���������� ����� ������� ��� Timer1 (��� ������ ����� EventMask2) -
    ///    ��������� �������� ���� "�������� GPRS �� Email" � �������.</description></item>
    /// </list>
    /// <para>20 � Sockets on-line GPRS. ������ ������������ ����� �����
    /// ������������. ����� ������ ������������� ���������� ���������� ��������
    /// "������ � ���" ��� ������������ ��� �������.
    /// ������������ �������, � �������� ��� ����� - ��������� ��������  (SpeedChange).
    /// ���� �������� SpeedChange ������� �� ����, �� ����� ��������� �������,
    /// � � ���������� ��������� �������������� "�����" �  ���� ���������.
    /// </para></para>
    /// </summary>
    public ushort Mode
    {
      get { return mode; }
      set { mode = value; }
    }
    private ushort mode = 10;

    /// <summary>
    /// �����������
    /// </summary>
    public GprsBaseConfig()
    {
      apnServer = "";
      apnLogin = "";
      apnPassword = "";
      dnsServer = "";
      dialNumber = "";
      gprsLogin = "";
      gprsPassword = "";
    }

    /// <summary>
    /// �������� ������������ ���������� ����� ������� ������. 
    /// ���������� true � ������ �������� ��������, ��� ������������ ���������� ���������� false � ������ � �����������.
    /// </summary>
    /// <param name="errorMessage">������ � ����������� ��� ������������ ����������.</param>
    /// <returns>���������� true � ������ �������� ��������, ��� ������������ ���������� 
    /// ���������� false � ������ � �����������.</returns>
    public override bool Validate(out string errorMessage)
    {
      errorMessage = string.Empty;

      if (apnServer.Length > 25)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "apnServer(APN ������)",
          "GprsBaseConfig(�������� ��������� GPRS)",
          25);
        return false;
      }
      if (apnLogin.Length > 10)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "apnLogin(APN �����)",
          "GprsBaseConfig(�������� ��������� GPRS)",
          10);
        return false;
      }
      if (apnPassword.Length > 10)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "apnPassword(APN ������)",
          "GprsBaseConfig(�������� ��������� GPRS)",
          10);
        return false;
      }
      if (dnsServer.Length > 16)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "dnsServer(Dns ������)",
          "GprsBaseConfig(�������� ��������� GPRS)",
          16);
        return false;
      }
      if (dialNumber.Length > 11)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "dialNumber(����� �������)",
          "GprsBaseConfig(�������� ��������� GPRS)",
          11);
        return false;
      }
      if (gprsLogin.Length > 10)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "gprsLogin(����� ��� ����� � GPRS ����)",
          "GprsBaseConfig(�������� ��������� GPRS)",
          10);
        return false;
      }
      if (gprsPassword.Length > 10)
      {
        errorMessage = String.Format(ErrorText.ATTRIBUTE_TO_BIG,
          "gprsPassword(������ ��� ����� � GPRS ����)",
          "GprsBaseConfig(�������� ��������� GPRS)",
          10);
        return false;
      }
      return true;
    }
  }
}
