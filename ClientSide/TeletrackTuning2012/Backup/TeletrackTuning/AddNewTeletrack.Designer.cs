﻿namespace TeletrackTuning
{
  partial class AddNewTeletrack
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.buttonAddTtOk = new System.Windows.Forms.Button();
      this.buttonAddTtCancel = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.textBoxDesctiption = new System.Windows.Forms.TextBox();
      this.textBoxName = new System.Windows.Forms.TextBox();
      this.textBoxShortID = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      // 
      // buttonAddTtOk
      // 
      this.buttonAddTtOk.Location = new System.Drawing.Point(12, 253);
      this.buttonAddTtOk.Name = "buttonAddTtOk";
      this.buttonAddTtOk.Size = new System.Drawing.Size(75, 23);
      this.buttonAddTtOk.TabIndex = 0;
      this.buttonAddTtOk.Text = "Ok";
      this.buttonAddTtOk.UseVisualStyleBackColor = true;
      this.buttonAddTtOk.Click += new System.EventHandler(this.buttonAddTtOk_Click);
      // 
      // buttonAddTtCancel
      // 
      this.buttonAddTtCancel.Location = new System.Drawing.Point(171, 252);
      this.buttonAddTtCancel.Name = "buttonAddTtCancel";
      this.buttonAddTtCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonAddTtCancel.TabIndex = 1;
      this.buttonAddTtCancel.Text = "Cancel";
      this.buttonAddTtCancel.UseVisualStyleBackColor = true;
      this.buttonAddTtCancel.Click += new System.EventHandler(this.buttonAddTtCancel_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.groupBox2);
      this.groupBox1.Controls.Add(this.buttonAddTtOk);
      this.groupBox1.Controls.Add(this.buttonAddTtCancel);
      this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox1.Location = new System.Drawing.Point(0, 0);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(258, 281);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Add New Teletrack";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.textBoxDesctiption);
      this.groupBox2.Controls.Add(this.textBoxName);
      this.groupBox2.Controls.Add(this.textBoxShortID);
      this.groupBox2.Controls.Add(this.label3);
      this.groupBox2.Controls.Add(this.label1);
      this.groupBox2.Controls.Add(this.label2);
      this.groupBox2.Location = new System.Drawing.Point(6, 28);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(246, 219);
      this.groupBox2.TabIndex = 3;
      this.groupBox2.TabStop = false;
      // 
      // textBoxDesctiption
      // 
      this.textBoxDesctiption.Location = new System.Drawing.Point(80, 113);
      this.textBoxDesctiption.Multiline = true;
      this.textBoxDesctiption.Name = "textBoxDesctiption";
      this.textBoxDesctiption.Size = new System.Drawing.Size(161, 79);
      this.textBoxDesctiption.TabIndex = 3;
      // 
      // textBoxName
      // 
      this.textBoxName.Location = new System.Drawing.Point(80, 50);
      this.textBoxName.Multiline = true;
      this.textBoxName.Name = "textBoxName";
      this.textBoxName.Size = new System.Drawing.Size(161, 56);
      this.textBoxName.TabIndex = 3;
      // 
      // textBoxShortID
      // 
      this.textBoxShortID.Location = new System.Drawing.Point(80, 21);
      this.textBoxShortID.Name = "textBoxShortID";
      this.textBoxShortID.Size = new System.Drawing.Size(161, 20);
      this.textBoxShortID.TabIndex = 3;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(10, 116);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(60, 13);
      this.label3.TabIndex = 2;
      this.label3.Text = "Description";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(10, 24);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(46, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Short ID";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(10, 53);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(35, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Name";
      // 
      // AddNewTeletrack
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(258, 281);
      this.Controls.Add(this.groupBox1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "AddNewTeletrack";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Load += new System.EventHandler(this.AddNewTeletrack_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button buttonAddTtOk;
    private System.Windows.Forms.Button buttonAddTtCancel;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.TextBox textBoxDesctiption;
    private System.Windows.Forms.TextBox textBoxName;
    private System.Windows.Forms.TextBox textBoxShortID;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
  }
}
