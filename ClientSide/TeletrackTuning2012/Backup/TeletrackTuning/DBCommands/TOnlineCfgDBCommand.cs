using System.Data;
using MySql.Data.MySqlClient;

namespace TeletrackTuning.TDBCommand
{
  class TOnlineCfgDBCommand : TDbCommand
  {
    private const string QUERY =
      "SELECT * FROM ServiceInit WHERE ServiceType_ID = 3";

    public TOnlineCfgDBCommand(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
