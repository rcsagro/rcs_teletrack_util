﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  class InsertNewInterMobitelConfig : SrvDbCommand
  {
    private const string updateID = "UPDATE InternalMobitelConfig SET ID=?LastID WHERE InternalMobitelConfig_ID = ?LastID";

    private const string QUERY =
      "INSERT INTO InternalMobitelConfig (Name, Descr, Message_ID, " +
      " devIdShort, devIdLong, verProtocolShort, verProtocolLong, " +
      " moduleIdGps, moduleIdGsm, moduleIdRf, moduleIdSs, moduleIdMm, ID) " +
      "VALUES (?Name, ?Descr, ?Message_ID, " +
      " ?devIdShort, ?devIdLong, ?verProtocolShort, ?verProtocolLong, " +
      " ?moduleIdGps, ?moduleIdGsm, ?moduleIdRf, ?moduleIdSs, ?moduleIdMm, 0)";

    private int newId;
    public int NewId
    {
      get { return newId; }
    }

    public InsertNewInterMobitelConfig(string connectionString)
      : base(connectionString)
    {
      InitCommandQUERY();
    }

    private void InitCommandQUERY()
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?devIdShort", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?devIdLong", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?verProtocolShort", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?verProtocolLong", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdGps", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdGsm", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdRf", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdSs", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdMm", MySqlDbType.String));
    }

    /// <summary>
    /// Порядок передачи параметров:
    /// 1. shortID
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "InsertNewInterMobitelConfig", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertNewInterMobitelConfig",
            initObjects.Length, 1), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = "";
      MyDbCommand.Parameters["?Descr"].Value = "";
      MyDbCommand.Parameters["?Message_ID"].Value = 0;

      MyDbCommand.Parameters["?devIdShort"].Value = (string)initObjects[0];
      MyDbCommand.Parameters["?devIdLong"].Value = 0;
      MyDbCommand.Parameters["?verProtocolShort"].Value = 0;
      MyDbCommand.Parameters["?verProtocolLong"].Value = 0;
      MyDbCommand.Parameters["?moduleIdGps"].Value = 0;
      MyDbCommand.Parameters["?moduleIdGsm"].Value = 0;
      MyDbCommand.Parameters["?moduleIdRf"].Value = 0;
      MyDbCommand.Parameters["?moduleIdSs"].Value = 0;
      MyDbCommand.Parameters["?moduleIdMm"].Value = 0;

      newId = 0;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();
      newId = (int)MyDbCommand.LastInsertedId;

      if (newId > 0)
      {
        if (MyDbCommand.CommandText != "")
          MyDbCommand.Parameters.Clear();
        MyDbCommand.CommandText = updateID;
        MyDbCommand.Parameters.Add(new MySqlParameter("?LastID", MySqlDbType.Int32));
        MyDbCommand.Parameters["?LastID"].Value = newId;
        MyDbCommand.ExecuteNonQuery();

        MyDbCommand.Parameters.Clear();
        InitCommandQUERY();
        return ExecutionStatus.OK;
      }
      else
        return ExecutionStatus.ERROR_DB;
    }
  }
}

