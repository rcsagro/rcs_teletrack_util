using MySql.Data.MySqlClient;

namespace TeletrackTuning.SrvDBCommand
{
  public interface ISrvDbCommand
  {
    /// <summary>
    /// ������� ����������, ���� ���������.
    /// ����������� ��������� ����� ������������� reader
    /// </summary>
    void CloseConnection();
    /// <summary>
    /// ��������� �������.
    /// <para>
    /// ��� ������������� ���������� ���� ��� ������������,
    /// ������������
    /// </para>
    /// </summary>
    ExecutionStatus Execute();
    /// <summary>
    /// ��������� ������� � ���������� DataReader � ResponseStatus
    /// </summary>
    /// <param name="status">ResponseStatus</param>
    /// <returns>MySqlDataReader</returns>
    MySqlDataReader ExecuteReader(out ExecutionStatus status);
    /// <summary>
    /// ������������� ������� ����� �����������
    /// </summary>
    /// <param name="initObjects">������ �������� �������������</param>
    void Init(params object[] initObjects);
    /// <summary>
    /// ���������� ��������� �������
    /// </summary>
    bool IsBusy { get; }
  }
}
