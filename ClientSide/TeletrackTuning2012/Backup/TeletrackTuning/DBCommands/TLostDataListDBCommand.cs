using System.Data;
using MySql.Data.MySqlClient;

namespace TeletrackTuning.TDBCommand
{
  /// <summary>
  /// Загрузка пропущенных данных
  /// </summary>
  class TLostDataListDBCommand : TDbCommand
  {
    private const string SQL_QUERY =
      "SELECT Mobitel_ID, Begin_SrvPacketID, End_SrvPacketID " +
      "FROM datagpslost_on " +
      "ORDER BY Mobitel_ID, Begin_SrvPacketID";

    public TLostDataListDBCommand(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = SQL_QUERY;
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
