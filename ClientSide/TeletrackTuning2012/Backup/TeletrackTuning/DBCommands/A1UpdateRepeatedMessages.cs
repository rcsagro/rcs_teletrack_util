using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ��������� �������� IsNew = 0 ��� ������������� ������ ���������,
  /// ��������������� ��� ��������. 
  /// �������������� ������ ����� ��������� ���������. ����������
  /// ������� ���� ������, ������� ������ ���������� ����������� �
  /// ���������� ��������� IsNew = 0 � ������ �� ��������������.
  /// </summary>
  class A1UpdateRepeatedMessages : SrvDbCommand
  {
    private const string QUERY =
      "UPDATE Messages " +
      "SET IsNew = 0, Source_ID = 0  " +
      "WHERE (Mobitel_ID = ?MobitelId) AND (Command_id = ?CommandId) AND " +
      "  (IsNew = 1) AND (Direction = 1) AND (IsSend = 0) AND (MessageCounter < ?MessageCounter)";

    public A1UpdateRepeatedMessages(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?CommandId", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MessageCounter", MySqlDbType.Int32));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. MobitelId
    /// 2. CommandId
    /// 3. MessageCounter
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1UpdateRepeatedMessages", "object[] initObjects"));
      if (initObjects.Length != 3)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1UpdateRepeatedMessages",
            initObjects.Length, 3), "object[] initObjects");

      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?CommandId"].Value = (int)initObjects[1];
      MyDbCommand.Parameters["?MessageCounter"].Value = (int)initObjects[2];
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
