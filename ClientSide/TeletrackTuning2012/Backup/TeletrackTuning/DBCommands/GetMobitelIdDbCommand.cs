using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ����� Mobitel_ID �� �������� DevIdShort
  /// </summary>
  class GetMobitelIdDbCommand : SrvDbCommand
  {
    private const string QUERY =
      "SELECT m.Mobitel_ID AS MobitelID " +
      "FROM mobitels m JOIN internalmobitelconfig c  " +
      "  ON (c.ID = m.InternalMobitelConfig_ID) " +
      "WHERE (c.InternalMobitelConfig_ID = ( " +
      "  SELECT intConf.InternalMobitelConfig_ID " +
      "  FROM internalmobitelconfig intConf " +
      "  WHERE (intConf.ID = c.ID)  " +
      "  ORDER BY intConf.InternalMobitelConfig_ID DESC " +
      "  LIMIT 1)) AND (c.devIdShort = ?devIdShort) ";


    private int mobitelId;
    /// <summary>
    /// MobitelId
    /// </summary>
    public int MobitelId
    {
      get { return mobitelId; }
    }

    public GetMobitelIdDbCommand(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?devIdShort", MySqlDbType.String));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. devIdShort
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "GetMobitelIdDbCommand", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "GetMobitelIdDbCommand",
            initObjects.Length, 1), "object[] initObjects");

      mobitelId = -1;
      MyDbCommand.Parameters["?devIdShort"].Value = (string)initObjects[0];
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      object mobitelIdValue = MyDbCommand.ExecuteScalar();

      if (mobitelIdValue == DBNull.Value)
      {
        return ExecutionStatus.ERROR_DATA;
      }
      else
      {
        mobitelId = Convert.ToInt32(mobitelIdValue);
        return ExecutionStatus.OK;
      }
    }
  }
}
