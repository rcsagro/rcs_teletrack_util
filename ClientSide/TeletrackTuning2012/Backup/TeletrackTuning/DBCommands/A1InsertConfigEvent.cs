using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ���������� �������� �������
  /// </summary>
  class A1InsertConfigEvent : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO ConfigEvent (Name, Descr, Message_ID, " +
      " tmr1Log, tmr2Send, tmr3Zone, dist1Log, dist2Send, dist3Zone,  " +
      " gprsEmail, gprsFtp, gprsSocket,  " +
      " maskEvent1, maskEvent2, maskEvent3, maskEvent4, maskEvent5, maskEvent6, " +
      " maskEvent7, maskEvent8, maskEvent9, maskEvent10, maskEvent11, maskEvent12, " +
      " maskEvent13, maskEvent14, maskEvent15, maskEvent16, maskEvent17, maskEvent18, " +
      " maskEvent19, maskEvent20, maskEvent21, maskEvent22, maskEvent23, maskEvent24, " +
      " maskEvent25, maskEvent26, maskEvent27, maskEvent28, maskEvent29, maskEvent30, " +
      " maskEvent31, deltaTimeZone, maskSensor, ID) " +
      "VALUES (?Name, ?Descr, ?Message_ID, " +
      " ?tmr1Log, ?tmr2Send, ?tmr3Zone, ?dist1Log, ?dist2Send, ?dist3Zone, " +
      " ?gprsEmail, ?gprsFtp, ?gprsSocket, " +
      " ?maskEvent1, ?maskEvent2, ?maskEvent3, ?maskEvent4, ?maskEvent5, ?maskEvent6, " +
      " ?maskEvent7, ?maskEvent8, ?maskEvent9, ?maskEvent10, ?maskEvent11, ?maskEvent12, " +
      " ?maskEvent13, ?maskEvent14, ?maskEvent15, ?maskEvent16, ?maskEvent17, ?maskEvent18, " +
      " ?maskEvent19, ?maskEvent20, ?maskEvent21, ?maskEvent22, ?maskEvent23, ?maskEvent24, " +
      " ?maskEvent25, ?maskEvent26, ?maskEvent27, ?maskEvent28, ?maskEvent29, ?maskEvent30, " +
      " ?maskEvent31, ?deltaTimeZone, ?maskSensor,  " +
      " (COALESCE((SELECT ConfigEvent_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))) ";

    public A1InsertConfigEvent(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?tmr1Log", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?tmr2Send", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?tmr3Zone", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?dist1Log", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?dist2Send", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?dist3Zone", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?gprsEmail", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?gprsFtp", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?gprsSocket", MySqlDbType.Int32));

      for (int i = 1; i < 32; i++)
        MyDbCommand.Parameters.Add(new MySqlParameter(
          "?maskEvent" + Convert.ToString(i), MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?deltaTimeZone", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?maskSensor", MySqlDbType.String));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. Message_ID
    /// 2. MobitelId (����� � ConfigMain ���� ConfigEvent_ID)
    /// 3. EventConfig
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1InsertConfigEvent", "object[] initObjects"));
      if (initObjects.Length != 3)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigEvent",
            initObjects.Length, 3), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = "";
      MyDbCommand.Parameters["?Descr"].Value = "";
      MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];

      EventConfig config = (EventConfig)initObjects[2];
      MyDbCommand.Parameters["?tmr1Log"].Value = config.MinSpeed;
      MyDbCommand.Parameters["?tmr2Send"].Value = config.Timer1;
      MyDbCommand.Parameters["?tmr3Zone"].Value = config.Timer2;
      MyDbCommand.Parameters["?dist1Log"].Value = config.CourseBend;
      MyDbCommand.Parameters["?dist2Send"].Value = config.Distance1;
      MyDbCommand.Parameters["?dist3Zone"].Value = config.Distance2;
      MyDbCommand.Parameters["?gprsEmail"].Value = config.GprsEmail;
      MyDbCommand.Parameters["?gprsFtp"].Value = 10;
      MyDbCommand.Parameters["?gprsSocket"].Value = 10;

      for (int i = 1; i < 32; i++)
        MyDbCommand.Parameters["?maskEvent" + Convert.ToString(i)].Value =
          config.EventMask[i - 1];

      MyDbCommand.Parameters["?deltaTimeZone"].Value = config.SpeedChange;
      MyDbCommand.Parameters["?maskSensor"].Value = "----";
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
