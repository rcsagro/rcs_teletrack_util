using System;

namespace TeletrackTuning.SrvDBCommand.Factory
{
  /// <summary>
  /// �������� �������� ������ �� ��������� ����.
  /// </summary>
  public class DbCommandFactory : IDbCommandFactory
  {
    /// <summary>
    /// ������ �����������.
    /// </summary>
    private readonly string _connectionString;

    private DbCommandFactory() { }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="�onnectionString">������ �����������.</param>
    public DbCommandFactory(string �onnectionString)
    {
      _connectionString = �onnectionString;
    }

    #region IDbCommandFactory Members

    /// <summary>
    /// �������� ��������� �������.
    /// </summary>
    /// <typeparam name="T">��� �������.</typeparam>
    /// <returns>ISrvDbCommand.</returns>
    public ISrvDbCommand GetDbCommand<T>() where T : SrvDbCommand
    {
      return (SrvDbCommand)Activator.CreateInstance(
        typeof(T), new object[1] { _connectionString });
    }

    #endregion
  }
}
