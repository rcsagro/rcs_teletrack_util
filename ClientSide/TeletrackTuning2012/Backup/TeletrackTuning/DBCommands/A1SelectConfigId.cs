using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������� �� �� �������� ����������������� ����������
  /// </summary>
  class A1SelectConfigId : SrvDbCommand
  {
    private const string QUERY =
      "SELECT * FROM InternalMobitelConfig WHERE Message_ID = ?MessageId";

    public A1SelectConfigId(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. MessageId
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1SelectConfigId", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigId",
            initObjects.Length, 1), "object[] initObjects");

      MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
