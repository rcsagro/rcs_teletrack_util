using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;
using MySql.Data;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ���������� �������� �������� GPRS 
  /// </summary>
  class A1InsertConfigGprsBase : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO ConfigGprsMain (Name, Descr, Message_ID, " +
      " Mode, Apnserv, Apnun, Apnpw, Dnsserv1, Dialn1, Ispun, Isppw, ID)  " +
      "VALUES (?Name, ?Descr, ?Message_ID,  " +
      " ?Mode, ?Apnserv, ?Apnun, ?Apnpw, ?Dnsserv1, ?Dialn1, ?Ispun, ?Isppw, " +
      " COALESCE((SELECT ConfigGprsMain_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

    public A1InsertConfigGprsBase(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?Mode", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Apnserv", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Apnun", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Apnpw", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Dnsserv1", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Dialn1", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Ispun", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Isppw", MySqlDbType.String));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. Message_ID
    /// 2. MobitelId (����� � ConfigMain ���� ConfigGprsMain_ID)
    /// 3. GprsBaseConfig
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1InsertConfigGprsBase", "object[] initObjects"));
      if (initObjects.Length != 3)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigGprsBase",
            initObjects.Length, 3), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = "";
      MyDbCommand.Parameters["?Descr"].Value = "";
      MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];

      GprsBaseConfig config = (GprsBaseConfig)initObjects[2];
      MyDbCommand.Parameters["?Mode"].Value = config.Mode;
      MyDbCommand.Parameters["?Apnserv"].Value = config.ApnServer;
      MyDbCommand.Parameters["?Apnun"].Value = config.ApnLogin;
      MyDbCommand.Parameters["?Apnpw"].Value = config.ApnPassword;
      MyDbCommand.Parameters["?Dnsserv1"].Value = config.DnsServer;
      MyDbCommand.Parameters["?Dialn1"].Value = config.DialNumber;
      MyDbCommand.Parameters["?Ispun"].Value = config.GprsLogin;
      MyDbCommand.Parameters["?Isppw"].Value = config.GprsPassword;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }

}
