using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������� �� �� �������� GPRS Email
  /// </summary>
  class A1SelectConfigGprsEmail : SrvDbCommand
  {
    private const string QUERY =
      "SELECT Smtpserv, Smtpun, Smtppw, " +
      " Pop3serv, Pop3un, Pop3pw " +
      "FROM ConfigGprsEmail " +
      "WHERE ID = COALESCE((SELECT ConfigGprsEmail_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1) " +
      "ORDER by ConfigGprsEmail_ID DESC LIMIT 1";
      //"WHERE Message_ID = ?MessageId";

    public A1SelectConfigGprsEmail(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. MessageId
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1SelectConfigGprsEmail", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigGprsEmail",
            initObjects.Length, 1), "object[] initObjects");

      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[0];
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
