﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  class InsertNewConfigSms : SrvDbCommand
  {
    private int newId;
    public int NewId
    {
      get { return newId; }
    }

    private const string QUERY = "INSERT Into configsms (Name, Descr, ID) " +
      "VALUES (?Name, ?Descr, COALESCE((SELECT max(ConfigSms_ID) + 1 from configsms c), 0)) ";

    public InsertNewConfigSms(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
    }

    /// <summary>
    /// Порядок передачи параметров:
    /// 1. Name
    /// 2. Description
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "InsertNewConfigSms", "object[] initObjects"));
      if (initObjects.Length != 2)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertNewConfigSms",
            initObjects.Length, 2), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = (string)initObjects[0];
      MyDbCommand.Parameters["?Descr"].Value = (string)initObjects[1];
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();
      newId = (int)MyDbCommand.LastInsertedId;

      return ExecutionStatus.OK;
    }
  }
}
