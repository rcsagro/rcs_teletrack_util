using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ���������� �������� GPRS Provider
  /// </summary>
  class A1InsertConfigGprsProvider : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO ConfigGprsInit (Name, Descr, Message_ID, Domain, InitString, ID) " +
      "VALUES (?Name, ?Descr, ?Message_ID, ?Domain, ?InitString, " +
      " COALESCE((SELECT ConfigGprsInit_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

    public A1InsertConfigGprsProvider(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?Domain", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?InitString", MySqlDbType.String));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. Message_ID
    /// 2. MobitelId (����� � ConfigMain ���� ConfigGprsInit_ID)
    /// 3. GprsProviderConfig
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1InsertConfigGprsProvider", "object[] initObjects"));
      if (initObjects.Length != 3)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigGprsProvider",
            initObjects.Length, 3), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = "";
      MyDbCommand.Parameters["?Descr"].Value = "";
      MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];

      GprsProviderConfig config = (GprsProviderConfig)initObjects[2];
      MyDbCommand.Parameters["?Domain"].Value = config.Domain;
      MyDbCommand.Parameters["?InitString"].Value = config.Message;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
