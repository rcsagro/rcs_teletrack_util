using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ���������� �������� GPRS Email
  /// </summary>
  class A1InsertConfigGprsEmail : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO ConfigGprsEmail (Name, Descr, Message_ID, " +
      " Smtpserv, Smtpun, Smtppw, Pop3serv, Pop3un, Pop3pw, ID) " +
      "VALUES (?Name, ?Descr, ?Message_ID,  " +
      " ?Smtpserv, ?Smtpun, ?Smtppw, ?Pop3serv, ?Pop3un, ?Pop3pw,  " +
      " COALESCE((SELECT ConfigGprsEmail_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

    public A1InsertConfigGprsEmail(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?Smtpserv", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Smtpun", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Smtppw", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Pop3serv", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Pop3un", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Pop3pw", MySqlDbType.String));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. Message_ID
    /// 2. MobitelId (����� � ConfigMain ���� ConfigGprsEmail_ID)
    /// 3. GprsEmailConfig
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1InsertConfigGprsEmail", "object[] initObjects"));
      if (initObjects.Length != 3)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigGprsEmail",
            initObjects.Length, 3), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = "";
      MyDbCommand.Parameters["?Descr"].Value = "";
      MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];

      GprsEmailConfig config = (GprsEmailConfig)initObjects[2];
      MyDbCommand.Parameters["?Smtpserv"].Value = config.SmtpServer;
      MyDbCommand.Parameters["?Smtpun"].Value = config.SmtpLogin;
      MyDbCommand.Parameters["?Smtppw"].Value = config.SmtpPassword;
      MyDbCommand.Parameters["?Pop3serv"].Value = config.Pop3Server;
      MyDbCommand.Parameters["?Pop3un"].Value = config.Pop3Login;
      MyDbCommand.Parameters["?Pop3pw"].Value = config.Pop3Password;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
