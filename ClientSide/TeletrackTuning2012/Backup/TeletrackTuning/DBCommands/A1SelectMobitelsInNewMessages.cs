using System.Data;
using MySql.Data.MySqlClient;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������� ��������������� ���������� � ��������� �����
  /// ���������-������ ��������������� ��� ��������� ����������
  /// </summary>
  class A1SelectMobitelsInNewMessages : SrvDbCommand
  {
    private const string QUERY =
      "SELECT DISTINCT msg.Mobitel_ID AS MobitelId " +
      "FROM Messages msg JOIN Mobitels m ON m.Mobitel_id = msg.Mobitel_id " +
      " JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID) " +
      "WHERE (msg.isNew = 1) AND (msg.Direction = 1) AND " +
      " (c.InternalMobitelConfig_ID = ( " +
      "   SELECT intConf.InternalMobitelConfig_ID " +
      "   FROM internalmobitelconfig intConf " +
      "   WHERE (intConf.ID = c.ID)  " +
      "   ORDER BY intConf.InternalMobitelConfig_ID DESC " +
      "   LIMIT 1)) AND " +
      " (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')";

    public A1SelectMobitelsInNewMessages(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
