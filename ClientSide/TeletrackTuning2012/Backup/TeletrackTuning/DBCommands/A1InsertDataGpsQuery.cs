﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;
using MySql.Data;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// Сохранение основных настроек GPRS 
  /// </summary>
  class A1InsertDataGpsQuery : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO gpsmasks (Name, Descr, Mobitel_ID, Message_ID, LastRecords, WhatSend, MaxSmsNum ) " +
      "VALUES (?Name, ?Descr, ?Mobitel_ID, ?Message_ID, ?LastRecords, ?WhatSend, ?MaxSmsNum )";

    public A1InsertDataGpsQuery(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Mobitel_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?LastRecords", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?WhatSend", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MaxSmsNum", MySqlDbType.Int32));
    }

    /// <summary>
    /// Порядок передачи параметров:
    /// 1. Message_ID
    /// 2. Mobitel_Id 
    /// 3. Description
    /// 4. LastRecords
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1InsertDataGpsQuery", "object[] initObjects"));
      if (initObjects.Length != 4)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertDataGpsQuery",
            initObjects.Length, 4), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = "";
      MyDbCommand.Parameters["?Descr"].Value = (string)initObjects[2];
      MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?Mobitel_ID"].Value = (int)initObjects[1];

      MyDbCommand.Parameters["?LastRecords"].Value = (int)initObjects[3];
      MyDbCommand.Parameters["?WhatSend"].Value = 0x3FF;//1023=all
      MyDbCommand.Parameters["?MaxSmsNum"].Value = 0;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }

}
