﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// Вставка в БД телефонного номера
  /// </summary>
  class InsertNewTelnumbers : SrvDbCommand
  {
    private const string QUERY =
    "INSERT INTO TelNumbers (Name, TelNumber) " +
      "VALUES (?Name, ?TelNumber)";

    private int id;
    public int ID
    {
      get { return id; }
    }

    public InsertNewTelnumbers(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?TelNumber", MySqlDbType.String));
    }

    /// Порядок передачи параметров:
    /// 1. Name
    /// 2. Phone
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "InsertNewTelnumbers", "object[] initObjects"));
      if (initObjects.Length != 2)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertNewTelnumbers",
            initObjects.Length, 2), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = (string)initObjects[0];
      MyDbCommand.Parameters["?TelNumber"].Value = (string)initObjects[1];

      id = 0;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      id = (int)MyDbCommand.LastInsertedId;
      return ExecutionStatus.OK;
    }
  }
}