using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ���������� ����� ������� Messages �������� ������ 
  /// ����� �������� ������ �� ���������
  /// </summary>
  class A1UpdateDeliveredMessage : SrvDbCommand
  {
    private const string QUERY =
      "UPDATE Messages " +
      "SET isDelivered = 1, Time4 = Unix_Timestamp(?Time) " +
      "WHERE Message_ID = ?MessageId";

    public A1UpdateDeliveredMessage(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Time", MySqlDbType.DateTime));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. MessageId
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1UpdateDeliveredMessage", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1UpdateDeliveredMessage",
            initObjects.Length, 1), "object[] initObjects");

      MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?Time"].Value = DateTime.Now;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
