using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������� �� �� �������� SMS
  /// </summary>
  class A1SelectConfigSms : SrvDbCommand
  {
    private const string QUERY =
      "SELECT COALESCE(cfg.DsptEmail, '') AS DsptEmailSMS, " +
      " COALESCE(cfg.DsptGprsEmail, '') AS DsptEmailGprs, " +
      " COALESCE(num1.SmsNumber, '') AS SmsCentre, " +
      " COALESCE(num2.SmsNumber, '') AS SmsDspt, " +
      " COALESCE(num3.SmsNumber, '') AS SmsEmailGate " +
      "FROM ConfigSms cfg LEFT JOIN SmsNumbers num1 ON num1.SmsNumber_ID = cfg.SmsCenter_ID  " +
      " LEFT JOIN SmsNumbers num2 ON num2.SmsNumber_ID = cfg.SmsDspt_ID  " +
      " LEFT JOIN SmsNumbers num3 ON num3.SmsNumber_ID = cfg.SmsEmailGate_ID " +
      "WHERE cfg.Message_ID = ?MessageId " +
      "LIMIT 1";

    public A1SelectConfigSms(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. MessageId
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1SelectConfigSms", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigSms",
            initObjects.Length, 1), "object[] initObjects");

      MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
