﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{

  class UpdateMobitelInfo : SrvDbCommand
  {
    private const string UpdateMobInf = "UPDATE mobitels SET Name = ?NewName, Descr = ?NewDescr WHERE Mobitel_ID = ?MobitelID";

    private int mobitelId = -1;

    public UpdateMobitelInfo(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = UpdateMobInf;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?NewName", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?NewDescr", MySqlDbType.String));
    }

    /// <summary>
    /// Порядок передачи параметров:
    /// 1. mobitelID
    /// 2. Name
    /// 3. Description
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "UpdateMobitelInfo", "object[] initObjects"));
      if (initObjects.Length != 3)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "UpdateMobitelInfo",
            initObjects.Length, 3), "object[] initObjects");

      MyDbCommand.Parameters["?MobitelID"].Value = (Int32)initObjects[0];
      MyDbCommand.Parameters["?NewName"].Value = (string)initObjects[1];
      MyDbCommand.Parameters["?NewDescr"].Value = (string)initObjects[2];
    }

    protected override ExecutionStatus InternalExecute()
    {
      ExecutionStatus result = ExecutionStatus.ERROR_DB;
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();
      result = ExecutionStatus.OK;
      return result;
    }
  }
}

