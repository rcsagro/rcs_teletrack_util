﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  class GetTeletrackPhone : SrvDbCommand
  {
    private const string QUERY =
      "SELECT t.TelNumber FROM telnumbers t " +
      "WHERE t.TelNumber_ID = COALESCE(("+
          "SELECT s.telMobitel FROM servicesend s WHERE s.ID = COALESCE(("+
            "SELECT m.ServiceSend_ID FROM mobitels m WHERE m.Mobitel_ID = ?MobitelId LIMIT 1), 0) ORDER by s.ServiceSend_ID DESC LIMIT 1), 0) ";

    private string phone;
    public string Phone
    {
      get { return phone; }
    }

    public GetTeletrackPhone(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
    }

    /// <summary>
    /// Порядок передачи параметров:
    /// 1. mobitelID
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "GetTeletrackPhone", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "GetTeletrackPhone",
            initObjects.Length, 1), "object[] initObjects");

      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[0];
      phone = "";
    }

    protected override ExecutionStatus InternalExecute()
    {
      ExecutionStatus result = ExecutionStatus.ERROR_DB;
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MySqlDataReader reader = MyDbCommand.ExecuteReader();
      if ((reader != null))//(result == ExecutionStatus.OK) && 
      {
        if (reader.Read())
        {
          phone = reader.GetString("TelNumber");
          result = ExecutionStatus.OK;
        }
      }
      return result;
    }
  }
}

