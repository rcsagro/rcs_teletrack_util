﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  class GetServiceinit : SrvDbCommand
  {
    private const string QUERY =
      "SELECT A1, A2, A3, A4 FROM serviceinit s WHERE ServiceType_ID = 3";

    public GetServiceinit(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}

