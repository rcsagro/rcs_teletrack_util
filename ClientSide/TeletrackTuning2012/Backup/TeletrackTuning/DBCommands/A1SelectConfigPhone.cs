using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������� �� �� �������� ���������� �������
  /// </summary>
  class A1SelectConfigPhone : SrvDbCommand
  {
    private const string QUERY =
      "SELECT COALESCE(num1.TelNumber, '') AS NumberSOS, " +
      " COALESCE(num2.TelNumber, '') AS NumberDspt, " +
      " COALESCE(num3.TelNumber, '') AS NumberAccept1, " +
      " COALESCE(num3.Name, '') AS Name1, " +
      " COALESCE(num4.TelNumber, '') AS NumberAccept2, " +
      " COALESCE(num4.Name, '') AS Name2, " +
      " COALESCE(num5.TelNumber, '') AS NumberAccept3, " +
      " COALESCE(num5.Name, '') AS Name3 " +
      "FROM ConfigTel cfg LEFT JOIN TelNumbers num1 ON num1.TelNumber_ID = cfg.TelSOS  " +
      " LEFT JOIN TelNumbers num2 ON num2.TelNumber_ID = cfg.TelDspt " +
      " LEFT JOIN TelNumbers num3 ON num3.TelNumber_ID = cfg.telAccept1 " +
      " LEFT JOIN TelNumbers num4 ON num4.TelNumber_ID = cfg.telAccept2 " +
      " LEFT JOIN TelNumbers num5 ON num5.TelNumber_ID = cfg.telAccept3 " +
      "WHERE cfg.ID = COALESCE((SELECT ConfigTel_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1) " +
      "ORDER by ConfigTel_ID DESC LIMIT 1";
      //"WHERE cfg.Message_ID = ?MessageId " +
      //"LIMIT 1";

    public A1SelectConfigPhone(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. MessageId
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1SelectConfigPhone", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigPhone",
            initObjects.Length, 1), "object[] initObjects");

      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[0];
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
