using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ���������� ����������������� ����������
  /// </summary>
  class A1InsertConfigId : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO InternalMobitelConfig (Name, Descr, Message_ID, " +
      " devIdShort, devIdLong, verProtocolShort, verProtocolLong, " +
      " moduleIdGps, moduleIdGsm, moduleIdRf, moduleIdSs, moduleIdMm, ID) " +
      "VALUES (?Name, ?Descr, ?Message_ID, " +
      " ?devIdShort, ?devIdLong, ?verProtocolShort, ?verProtocolLong, " +
      " ?moduleIdGps, ?moduleIdGsm, ?moduleIdRf, ?moduleIdSs, ?moduleIdMm, " +
      " COALESCE((SELECT InternalMobitelConfig_ID FROM Mobitels WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))";

    public A1InsertConfigId(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?devIdShort", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?devIdLong", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?verProtocolShort", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?verProtocolLong", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdGps", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdGsm", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdRf", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdSs", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?moduleIdMm", MySqlDbType.String));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. Message_ID
    /// 2. MobitelId (����� � ConfigMain ���� ConfigTel_ID)
    /// 3. IdConfig
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1InsertConfigId", "object[] initObjects"));
      if (initObjects.Length != 3)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigId",
            initObjects.Length, 3), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = "";
      MyDbCommand.Parameters["?Descr"].Value = "";
      MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];

      IdConfig config = (IdConfig)initObjects[2];
      MyDbCommand.Parameters["?devIdShort"].Value = config.DevIdShort;
      MyDbCommand.Parameters["?devIdLong"].Value = config.DevIdLong;
      MyDbCommand.Parameters["?verProtocolShort"].Value = config.VerProtocolShort;
      MyDbCommand.Parameters["?verProtocolLong"].Value = config.VerProtocolLong;
      MyDbCommand.Parameters["?moduleIdGps"].Value = config.ModuleIdGps;
      MyDbCommand.Parameters["?moduleIdGsm"].Value = config.ModuleIdGsm;
      MyDbCommand.Parameters["?moduleIdRf"].Value = config.ModuleIdRf;
      MyDbCommand.Parameters["?moduleIdSs"].Value = config.ModuleIdSs;
      MyDbCommand.Parameters["?moduleIdMm"].Value = config.ModuleIdMm;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
