using System.Data;

namespace TeletrackTuning.TDBCommand
{
  class TTransferBufferDBCommand : TDbCommand
  {
    private const string PROC_NAME = "OnTransferBuffer";

    public TTransferBufferDBCommand(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.StoredProcedure;
      MyDbCommand.CommandText = PROC_NAME;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
