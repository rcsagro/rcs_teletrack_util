using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������� ������ � ������� Messages
  /// </summary>
  class A1InsertMessages : SrvDbCommand
  {
    private const string QUERY = "INSERT INTO Messages " +
      "(Time1, Time2, isNew, Direction, Command_ID, Source_ID, " +
      "Address, DataFromService, Mobitel_ID, crc, MessageCounter) " +
      "VALUES (?Time1, ?Time2, ?isNew, ?Direction, ?Command_ID, ?Source_ID, " +
      "?Address, ?DataFromService, ?Mobitel_ID, ?crc, ?MessageCounter)";

    /// <summary>
    /// �������� ���������� ����� Message_ID ����� ������,
    /// ������� ������ ��� ���������
    /// </summary>
    private int newMessageId;
    /// <summary>
    /// �������� ���������� ����� Message_ID ����� ������,
    /// ������� ������ ��� ���������
    /// </summary>
    public int NewMessageId
    {
      get { return newMessageId; }
    }

    public A1InsertMessages(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Time1", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Time2", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?isNew", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Direction", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Command_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Source_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Address", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?DataFromService", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Mobitel_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?crc", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MessageCounter", MySqlDbType.Int32));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. Time1 
    /// 2. Command_ID
    /// 3. Source_ID
    /// 4. Address(Email)
    /// 5. Mobitel_ID
    /// 6. MessageCounter
    /// 7. Direction 1-to TT, 0-from TT
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1InsertMessages", "object[] initObjects"));
      if (initObjects.Length != 7)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertMessages",
            initObjects.Length, 7), "object[] initObjects");

      newMessageId = 0;
      MyDbCommand.Parameters["?Time1"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?Time2"].Value = 0;
      MyDbCommand.Parameters["?isNew"].Value = 1;
      MyDbCommand.Parameters["?Direction"].Value = (int)initObjects[6];
      MyDbCommand.Parameters["?Command_ID"].Value = (int)initObjects[1];
      MyDbCommand.Parameters["?Source_ID"].Value = (int)initObjects[2];
      MyDbCommand.Parameters["?Address"].Value = (string)initObjects[3];
      MyDbCommand.Parameters["?DataFromService"].Value = "Data";
      MyDbCommand.Parameters["?Mobitel_ID"].Value = (int)initObjects[4];
      MyDbCommand.Parameters["?crc"].Value = 1;
      MyDbCommand.Parameters["?MessageCounter"].Value = (int)initObjects[5];
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      newMessageId = (int)MyDbCommand.LastInsertedId;

      return ExecutionStatus.OK;
    }
  }
}
