using System;
using System.Text;
using TeletrackTuning.Error;

namespace TeletrackTuning.TDBCommand
{
  class TInsertDataInBufferDBCommand : TDbCommand
  {
    private const string SQL_QUERY = @"INSERT INTO datagpsbuffer_on 
		(Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Speed, Direction, Altitude,Valid,
		Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,  
		`Events`, whatIs, SrvPacketID) VALUES ";


    public TInsertDataInBufferDBCommand(string connectionString)
      : base(connectionString) { }

    /// <summary>
    /// Init
    /// </summary>
    /// <param name="string">������ ����� ������</param>
    public override void Init(params object[] initObjects)
    {
      string[] rows = (string[])initObjects[0];
      if ((rows == null) || (rows.Length == 0))
        throw new ArgumentNullException("string[] Rows",
          ErrorText.DB_NULL_DATA_INSERT_COMMAND);

      StringBuilder insertRows = new StringBuilder(SQL_QUERY);
      insertRows.Append(GetRowsFowsRorInsert(rows));

      MyDbCommand.CommandText = insertRows.ToString();
    }

    protected override bool NeedExecuteAfterFailure()
    {
      return false;
    }

    /// <summary>
    /// ��������� ������ ������� ����
    /// INSERT INTO datagpsbuffer (...)
    /// VALUES (..., ..., ...), (..., ..., ...)
    /// </summary>
    /// <param name="rows"></param>
    /// <returns>(..., ..., ...), (..., ..., ...)</returns>
    private string GetRowsFowsRorInsert(params string[] rows)
    {
      StringBuilder insertRows = new StringBuilder("");
      foreach (string row in rows)
      {
        if (insertRows.Length != 0)
          insertRows.Append(",");

        insertRows.Append("(").Append(row).Append(")");
      }
      return insertRows.ToString();
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
