﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// Выборка из БД телефонных номеров
  /// </summary>
  class SelectTelnumbers : SrvDbCommand
  {
    private const string QUERY =
      "SELECT Name, TelNumber " +
      "FROM TelNumbers";

    public SelectTelnumbers(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
