﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// Выборка описания мобителов
  /// </summary>
  class SelectMobitelsDescription : SrvDbCommand
  {
    private const string QUERY =
      "SELECT Name, Descr, Mobitel_ID " +
      "    FROM mobitels ORDER BY Mobitel_ID ";

    /// <param name="connectionString">Connection string</param>
    public SelectMobitelsDescription(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
