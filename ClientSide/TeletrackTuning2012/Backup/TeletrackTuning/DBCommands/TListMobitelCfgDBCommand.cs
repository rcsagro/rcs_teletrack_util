using System.Data;
using MySql.Data.MySqlClient;

namespace TeletrackTuning.TDBCommand
{
  /// <summary>
  /// Список телетреков
  /// </summary>
  class TListMobitelCfgDBCommand : TDbCommand
  {
    private const string PROC_NAME = "OnListMobitelConfig";

    public TListMobitelCfgDBCommand(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.StoredProcedure;
      MyDbCommand.CommandText = PROC_NAME;
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }

  }
}
