using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.TDBCommand
{
  /// <summary>
  /// �������� ��������� ��������� �� ������� datagpslost_on 
  /// </summary>
  class TLostDataDelDBCommand : TDbCommand
  {
    private const string PROC_NAME = "OnDeleteLostRange";
    private const string PARAM_MOBITEL_ID = "MobitelID";
    private const string PARAM_BEGIN_SRVPACKET_ID = "BeginSrvPacketID";

    public TLostDataDelDBCommand(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.StoredProcedure;
      MyDbCommand.CommandText = PROC_NAME;
      MyDbCommand.Parameters.Add(
        new MySqlParameter(PARAM_MOBITEL_ID, MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(
        new MySqlParameter(PARAM_BEGIN_SRVPACKET_ID, MySqlDbType.Int64));
    }

    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "TLostDataDelDBCommand", "object[] initObjects"));
      if (initObjects.Length != 2)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "TLostDataDelDBCommand",
            initObjects.Length, 2), "object[] initObjects");

      MyDbCommand.Parameters[PARAM_MOBITEL_ID].Value = (int)initObjects[0];
      MyDbCommand.Parameters[PARAM_BEGIN_SRVPACKET_ID].Value = (long)initObjects[1];
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
