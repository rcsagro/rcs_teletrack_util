﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  class UpdateServiceinit : SrvDbCommand
  {
    private const string QUERY =
      "UPDATE serviceinit SET A1 = ?A1, A2 = ?A2, A3 = ?A3, A4 = ?A4 " +
      "WHERE ServiceType_ID = 3";

    public UpdateServiceinit(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?A1", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?A2", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?A3", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?A4", MySqlDbType.String));
    }

    /// <summary>
    /// Порядок передачи параметров:
    /// 1. ip
    /// 2. port
    /// 3. login
    /// 4. password
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "UpdateServiceinit", "object[] initObjects"));
      if (initObjects.Length != 4)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "UpdateServiceinit",
            initObjects.Length, 4), "object[] initObjects");

      MyDbCommand.Parameters["?A1"].Value = (string)initObjects[0]; 
      MyDbCommand.Parameters["?A2"].Value = (string)initObjects[1];
      MyDbCommand.Parameters["?A3"].Value = (string)initObjects[2];
      MyDbCommand.Parameters["?A4"].Value = (string)initObjects[3];
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}