using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ���������� �������� SMS �������
  /// </summary>
  class A1InsertConfigSms : SrvDbCommand
  {
    private const string QUERY = "INSERT Into configsms (Name, Descr, Message_ID, " +
      "smsCenter_ID, smsDspt_ID, smsEmailGate_ID, dsptEmail, dsptGprsEmail, ID) " +
      "VALUES (?Name, ?Descr, ?Message_ID, " +
      "(COALESCE((SELECT SmsNumber_ID FROM SmsNumbers WHERE smsNumber = ?SmsCenterNumber LIMIT 1), -1)), " +
      "(COALESCE((SELECT SmsNumber_ID FROM SmsNumbers WHERE smsNumber = ?SmsDsptNumber LIMIT 1), -1)), " +
      "(COALESCE((SELECT SmsNumber_ID FROM SmsNumbers WHERE smsNumber = ?SmsEmailGateNumber LIMIT 1), -1)), " +
      "?dsptEmail, ?dsptGprsEmail, " +
      "(COALESCE((SELECT ConfigSms_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1)))";

    public A1InsertConfigSms(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?SmsCenterNumber", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?SmsDsptNumber", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?SmsEmailGateNumber", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?dsptEmail", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?dsptGprsEmail", MySqlDbType.String));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. Message_ID
    /// 2. MobitelId (����� � ConfigMain ���� ConfigSms_ID)
    /// 3. SmsAddrConfig
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1InsertConfigSms", "object[] initObjects"));
      if (initObjects.Length != 3)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigSms",
            initObjects.Length, 3), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = "";
      MyDbCommand.Parameters["?Descr"].Value = "";
      MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];

      SmsAddrConfig config = (SmsAddrConfig)initObjects[2];
      MyDbCommand.Parameters["?SmsCenterNumber"].Value = config.SmsCentre;
      MyDbCommand.Parameters["?SmsDsptNumber"].Value = config.SmsDspt;
      MyDbCommand.Parameters["?SmsEmailGateNumber"].Value = config.SmsEmailGate;
      MyDbCommand.Parameters["?dsptEmail"].Value = config.DsptEmailSMS;
      MyDbCommand.Parameters["?dsptGprsEmail"].Value = config.DsptEmailGprs;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
