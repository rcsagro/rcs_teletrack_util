﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  class InsertNewServiceSend : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO servicesend (Name, Descr, ID) " +
      "VALUES (?Name, ?Descr, COALESCE((SELECT max(ServiceSend_ID) + 1 from servicesend c), 0))";

    private int newId;
    public int NewId
    {
      get { return newId; }
    }

    public InsertNewServiceSend(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
    }

    /// <summary>
    /// Порядок передачи параметров:
    /// 1. name
    /// 2. description
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "InsertNewServiceSend", "object[] initObjects"));
      if (initObjects.Length != 2)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertNewServiceSend",
            initObjects.Length, 2), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = (string)initObjects[0];
      MyDbCommand.Parameters["?Descr"].Value = (string)initObjects[1];
      newId = 0;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();
      newId = (int)MyDbCommand.LastInsertedId;

      return ExecutionStatus.OK;
    }
  }
}

