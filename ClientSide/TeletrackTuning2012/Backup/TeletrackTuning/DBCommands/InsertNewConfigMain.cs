﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// Выборка из БД настроек телефонных номеров
  /// </summary>
  class InsertNewConfigMain : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO configmain (Name, Descr, Mobitel_ID, ConfigEvent_ID, " +
      " ConfigTel_ID, ConfigSms_ID, ConfigGprsMain_ID, ConfigGprsEmail_ID, " +
      " ConfigOther_ID, ConfigDriverMessage_ID, ConfigUnique_ID, ConfigZoneSet_ID, " +
      " ConfigSensorSet_ID, ConfigGprsInit_ID) " +
      "VALUES (?Name, ?Descr, ?Mobitel_ID, ?ConfigEvent_ID, " +
      " ?ConfigTel_ID, ?ConfigSms_ID, ?ConfigGprsMain_ID, ?ConfigGprsEmail_ID, " +
      " ?ConfigOther_ID, ?ConfigDriverMessage_ID, ?ConfigUnique_ID, ?ConfigZoneSet_ID, " +
      " ?ConfigSensorSet_ID, ?ConfigGprsInit_ID) ";
      //"  COALESCE((SELECT max(InternalMobitelConfig_ID) + 1 from InternalMobitelConfig c), 0))";

    public InsertNewConfigMain(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Mobitel_ID", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?ConfigEvent_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?ConfigTel_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?ConfigSms_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?ConfigGprsMain_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?ConfigGprsEmail_ID", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?ConfigOther_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?ConfigDriverMessage_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?ConfigUnique_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?ConfigZoneSet_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?ConfigSensorSet_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?ConfigGprsInit_ID", MySqlDbType.Int32));
    }

    /// <summary>
    /// Порядок передачи параметров:
    /// 1. Name
    /// 2. Descr
    /// 3. Mobitel_ID
    /// 4. ConfigEvent_ID
    /// 5. ConfigTel_ID
    /// 6. ConfigSms_ID
    /// 7. ConfigGprsMain_ID
    /// 8. ConfigGprsEmail_ID
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "InsertNewConfigMain", "object[] initObjects"));
      if (initObjects.Length != 8)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertNewConfigMain",
            initObjects.Length, 8), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = (string)initObjects[0];
      MyDbCommand.Parameters["?Descr"].Value = (string)initObjects[1];
      MyDbCommand.Parameters["?Mobitel_ID"].Value = (int)initObjects[2];

      MyDbCommand.Parameters["?ConfigEvent_ID"].Value = (int)initObjects[3];
      MyDbCommand.Parameters["?ConfigTel_ID"].Value = (int)initObjects[4];
      MyDbCommand.Parameters["?ConfigSms_ID"].Value = (int)initObjects[5];
      MyDbCommand.Parameters["?ConfigGprsMain_ID"].Value = (int)initObjects[6];
      MyDbCommand.Parameters["?ConfigGprsEmail_ID"].Value = (int)initObjects[7];

      MyDbCommand.Parameters["?ConfigOther_ID"].Value = 50;
      MyDbCommand.Parameters["?ConfigDriverMessage_ID"].Value = 50;
      MyDbCommand.Parameters["?ConfigUnique_ID"].Value = 50;
      MyDbCommand.Parameters["?ConfigZoneSet_ID"].Value = 50;
      MyDbCommand.Parameters["?ConfigSensorSet_ID"].Value = 50;
      MyDbCommand.Parameters["?ConfigGprsInit_ID"].Value = 50;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
