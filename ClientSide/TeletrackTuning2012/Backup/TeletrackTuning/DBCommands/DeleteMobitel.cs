﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{

  class DeleteMobitel : SrvDbCommand
  {
    private const string  DeleteInterMobConf = 
      "DELETE c FROM internalmobitelconfig c JOIN mobitels m ON m.InternalMobitelConfig_ID = c.ID "+
      "WHERE m.Mobitel_ID = ?MobitelID";

    private const string DeleteMob = "DELETE FROM mobitels WHERE Mobitel_ID = ?MobitelID";

    private int mobitelId = -1;

    public DeleteMobitel(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      //MyDbCommand.CommandText = QUERY;
      //MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));

    }

    /// <summary>
    /// Порядок передачи параметров:
    /// 1. mobitelID
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "DeleteMobitel", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "DeleteMobitel",
            initObjects.Length, 1), "object[] initObjects");

      mobitelId = (Int32)initObjects[0];
    }

    protected override ExecutionStatus InternalExecute()
    {
      ExecutionStatus result = ExecutionStatus.ERROR_DB;
      //delete internalmobitelconfig
      if(MyDbCommand.CommandText != "")
        MyDbCommand.Parameters.Clear();
      MyDbCommand.CommandText = DeleteInterMobConf;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelID", MySqlDbType.Int32));
      MyDbCommand.Parameters["?MobitelID"].Value = mobitelId;

      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();
      
      //delete mobitel
      MyDbCommand.CommandText = DeleteMob;
      MyDbCommand.ExecuteNonQuery();
      result = ExecutionStatus.OK;
      return result;
    }
  }
}

