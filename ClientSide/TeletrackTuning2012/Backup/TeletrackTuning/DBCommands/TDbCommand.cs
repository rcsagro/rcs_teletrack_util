using TeletrackTuning.SrvDBCommand;

namespace TeletrackTuning.TDBCommand
{
  class TDbCommand : SrvDbCommand
  {
    protected TDbCommand() { }
    public TDbCommand(string connectionString) : base(connectionString) { }
  }
}
