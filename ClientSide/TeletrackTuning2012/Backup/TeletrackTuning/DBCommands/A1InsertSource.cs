using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������� ������ � ������� Source
  /// </summary>
  class A1InsertSource : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO Sources (SourceText, SourceError) VALUES (?SourceText, ?SourceError)";

    /// <summary>
    /// �������� ���������� ����� Source_ID ����� ������,
    /// ������� ������ ��� ���������
    /// </summary>
    private int newSourceId;
    /// <summary>
    /// �������� ���������� ����� Source_ID ����� ������,
    /// ������� ������ ��� ���������
    /// </summary>
    public int NewSourceId
    {
      get { return newSourceId; }
    }

    public A1InsertSource(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?SourceText", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?SourceError", MySqlDbType.String));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. SourceText
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1InsertSource", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertSource",
            initObjects.Length, 1), "object[] initObjects");

      newSourceId = 0;
      MyDbCommand.Parameters["?SourceText"].Value = (string)initObjects[0];
      MyDbCommand.Parameters["?SourceError"].Value = "No error";
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      newSourceId = (int)MyDbCommand.LastInsertedId;

      return ExecutionStatus.OK;
    }
  }
}
