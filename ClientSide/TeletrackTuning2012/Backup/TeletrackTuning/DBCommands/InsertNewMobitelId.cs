﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{

  class InsertNewMobitelId : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO mobitels (Name, Descr, InternalMobitelConfig_ID, " +
      " MapStyleLine_ID, MapStyleLastPoint_ID, MapStylePoint_ID, ServiceSend_ID) " +
      "VALUES (?Name, ?Descr, ?InternalMobitelConfig_ID, " +
      " ?MapStyleLine_ID, ?MapStyleLastPoint_ID, ?MapStylePoint_ID, ?ServiceSend_ID)";

    private int newMobitelId;
    public int NewMobitelId
    {
      get { return newMobitelId; }
    }

    public InsertNewMobitelId(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?InternalMobitelConfig_ID", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?MapStyleLine_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MapStyleLastPoint_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MapStylePoint_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?ServiceSend_ID", MySqlDbType.Int32));
    }

    /// <summary>
    /// Порядок передачи параметров:
    /// 1. internalMobitelConfig_ID
    /// 2. Name
    /// 3. Description
    /// 4. serviceSend_ID
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "InsertNewMobitelId", "object[] initObjects"));
      if (initObjects.Length != 4)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertNewMobitelId",
            initObjects.Length, 4), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = (string)initObjects[1];
      MyDbCommand.Parameters["?Descr"].Value = (string)initObjects[2];
      MyDbCommand.Parameters["?InternalMobitelConfig_ID"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?MapStyleLine_ID"].Value = 6;
      MyDbCommand.Parameters["?MapStyleLastPoint_ID"].Value = 5;
      MyDbCommand.Parameters["?MapStylePoint_ID"].Value = 5;
      MyDbCommand.Parameters["?ServiceSend_ID"].Value = (int)initObjects[3];

      newMobitelId = 0;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();
      newMobitelId = (int)MyDbCommand.LastInsertedId;

      return ExecutionStatus.OK;
    }
  }
}

