using System;
using System.Data;
using System.Text;
using System.Threading;
using MySql.Data.MySqlClient;
//using TeletrackTuning.Properties;
using TeletrackTuning.Error;
//using RCS.TDataMngr.OnlineClient.Logging;
using System.Windows.Forms;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������� ����c ������ ���������� � ����� ������  
  /// </summary>
  public class SrvDbCommand : ISrvDbCommand
  {
    /// <summary>
    /// ����� �������� ����� ��������� ����������� �������: 3 �������
    /// </summary>
    private const int SLEEPING_TIME = 3000;
    /// <summary>
    /// ������ �����������
    /// </summary>
    protected string ConnectionString;

    private MySqlConnection connection;
    /// <summary>
    /// ��������� ��������������� = null 
    /// � ������ CloseConnection()
    /// </summary>
    protected MySqlConnection Connection
    {
      get
      {
        if (connection == null)
        {
          connection = new MySqlConnection(ConnectionString);
        }
        return connection;
      }
    }

    private static object syncObject = new object();

    protected MySqlCommand MyDbCommand;

    protected SrvDbCommand() { }

    public SrvDbCommand(string connectionString)
    {
      if ((connectionString == null) || (connectionString.Length == 0))
      {
        throw new ArgumentNullException(
          "connectionString", ErrorText.DB_NULL_DATAMANAGER_CTOR);
      }
      ConnectionString = connectionString;

      MyDbCommand = new MySqlCommand();
      MyDbCommand.CommandTimeout = TeletrackTuning.Properties.Settings.Default.CommandTimeout;
    }

    /// <summary>
    /// ������������� ������� ����� �����������
    /// </summary>
    /// <param name="initObjects">������ �������� �������������</param>
    public virtual void Init(params object[] initObjects) { }

    /// <summary>
    /// ������� ����������, ���� ���������.
    /// ����������� ��������� ����� ������������� reader
    /// </summary>
    public void CloseConnection()
    {
      if (Connection.State != ConnectionState.Closed)
      {
        try
        {
          Connection.Close();
        }
        catch (Exception Ex)
        {
          MessageBox.Show(Ex.Message, "Close connection Exception");//ErrorHandler.Handle(Ex);
        }

        connection = null;

        //if (LogCfg.GetConfig().Debug)
        //{
        //  TraceCloseConnection();
        //}
      }
    }

    private void InitExecuting()
    {
      isBusy = true;
      //if (LogCfg.GetConfig().Debug)
      //{
      //  TraceStartCommand();
      //}
    }

    /// <summary>
    /// ��������� �� ��������� ���������� �������
    /// ����� ������� "�������"
    /// </summary>
    /// <returns>Bool</returns>
    protected virtual bool NeedExecuteAfterFailure()
    {
      return true;
    }

    /// <summary>
    /// ��������� �������.
    /// <para>
    /// ��� ������������� ���������� ���� ��� ������������,
    /// ������������
    /// </para>
    /// </summary>
    public ExecutionStatus Execute()
    {
      lock (syncObject)
      {
        InitExecuting();
        ExecutionStatus result = ExecutionStatus.OK;

        try
        {
          result = InternalExecute();
        }
        catch (Exception Ex)
        {
          HandleExecutingException(Ex);
          result = ExecutionStatus.ERROR_DB;

          if (NeedExecuteAfterFailure())
          {
            // ������� �������� ��������� 
            // ����� ��������� �������� �������
            try
            {
              Thread.Sleep(SLEEPING_TIME);
              result = TryExecute();
            }
            catch (Exception ExInner)
            {
              HandleExecutingException(ExInner);
            }
          }
        }
        finally
        {
          CloseConnection();
          isBusy = false;
        }

        return result;
      }
    }

    private ExecutionStatus TryExecute()
    {
      InitExecuting();
      return InternalExecute();
    }

    /// <summary>
    /// ��������� ������� � ���������� DataReader � ExecutionStatus
    /// </summary>
    /// <param name="status">ResponseStatus</param>
    /// <returns>MySqlDataReader</returns>
    public MySqlDataReader ExecuteReader(out ExecutionStatus status)
    {
      lock (syncObject)
      {
        status = ExecutionStatus.OK;
        MySqlDataReader result = null;
        try
        {
          result = TryExecuteReader();
        }
        catch (Exception Ex)
        {
          HandleExecutingException(Ex);
          status = ExecutionStatus.ERROR_DB;

          if (NeedExecuteAfterFailure())
          {
            // ������� �������� ���������
            // ����� ��������� �������� �������
            try
            {
              Thread.Sleep(SLEEPING_TIME);
              result = TryExecuteReader();
              status = ExecutionStatus.OK;
            }
            catch (Exception ExInner)
            {
              HandleExecutingException(ExInner);
              status = ExecutionStatus.ERROR_DB;
              throw ExInner;
            }
          }
        }
        finally
        {
          isBusy = false;
        }

        return result;
      }
    }

    /// <summary>
    /// ������ � ��� ���������� �� ������� � �������� ����������
    /// </summary>
    /// <param name="ExInner">Exception</param>
    private void HandleExecutingException(Exception ExInner)
    {
      string keyMassage = String.Format(ErrorText.DB_COMMAND_EXECUTING_FAILED,
        this.GetType().Name, ConnectionString, ExInner.Message);
      //ErrorHandler.Handle(new ApplicationException(keyMassage));
      //ErrorHandler.Handle(ExInner);
      CloseConnection();
    }

    /// <summary>
    /// Try execute reader
    /// </summary>
    /// <returns>My sql data reader</returns>
    private MySqlDataReader TryExecuteReader()
    {
      InitExecuting();
      return InternalExecuteReader();
    }

    /// <summary>
    /// ����� ������ ��������� ���������� 
    /// ���������� ��������������� �������
    /// </summary>
    protected virtual ExecutionStatus InternalExecute()
    {
      throw new NotImplementedException(String.Format(
        ErrorText.NOT_IMPLEMENTED, "InternalExecute", "DbCommand"));
    }

    protected virtual MySqlDataReader InternalExecuteReader()
    {
      throw new NotImplementedException(String.Format(
        ErrorText.NOT_IMPLEMENTED, "InternalExecuteReader", "DbCommand"));
    }

    private bool isBusy;
    public bool IsBusy
    {
      get { return isBusy; }
    }

    /// <summary>
    /// ���������� � ������� ������� ����� ���������
    /// </summary>
    protected virtual void TraceStartCommand()
    {
      const string strParamsInfo = "\r\n  ��������: {0}, ��������: {1}";

      StringBuilder paramsInfo = new StringBuilder("");
      foreach (MySqlParameter parameter in MyDbCommand.Parameters)
      {
        paramsInfo.Append(String.Format(strParamsInfo,
          parameter.ParameterName,
          Convert.ToString(parameter.Value)));
      }

      WriteDebug(String.Format(
        "�������: {0}, ������ �����������: {1}.\r\n ����� �������: {2}\r\n ��� �������: {3}\r\n ���������:{4}",
        this.GetType().Name,
        ConnectionString,
        MyDbCommand.CommandText,
        MyDbCommand.CommandType.ToString(),
        paramsInfo.ToString()));
    }

    /// <summary>
    /// ���������� � �������� ���������� ��������
    /// </summary>
    private void TraceCloseConnection()
    {
      WriteDebug(String.Format("������� {0} ������� ���������� � ����� {1} \r\n",
        this.GetType().Name, ConnectionString));
    }

    protected void WriteDebug(string message)
    {
      //Logger.WriteDebug("$  ", message);
    }
  }
}
