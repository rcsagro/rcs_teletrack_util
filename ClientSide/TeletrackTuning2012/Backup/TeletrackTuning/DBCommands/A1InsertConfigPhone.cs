using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ���������� �������� ���������� �������
  /// </summary>
  class A1InsertConfigPhone : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO ConfigTel (Name, Descr, Message_ID, " +
      " telSOS, telDspt, telAccept1, telAccept2, telAccept3, ID) " +
      "VALUES (?Name, ?Descr, ?Message_ID,  " +
      " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telSOS LIMIT 1), -1)), " +
      " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telDspt LIMIT 1), -1)), " +
      " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telAccept1 LIMIT 1), -1)), " +
      " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telAccept2 LIMIT 1), -1)), " +
      " (COALESCE((SELECT TelNumber_ID FROM Telnumbers WHERE TelNumber = ?telAccept3 LIMIT 1), -1)), " +
      " (COALESCE((SELECT ConfigTel_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1))) ";

    public A1InsertConfigPhone(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Message_ID", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));

      MyDbCommand.Parameters.Add(new MySqlParameter("?telSOS", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?telDspt", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?telAccept1", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?telAccept2", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?telAccept3", MySqlDbType.String));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. Message_ID
    /// 2. MobitelId (����� � ConfigMain ���� ConfigTel_ID)
    /// 3. PhoneNumberConfig
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1InsertConfigPhoneDbCommand", "object[] initObjects"));
      if (initObjects.Length != 3)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1InsertConfigPhoneDbCommand",
            initObjects.Length, 3), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = "";
      MyDbCommand.Parameters["?Descr"].Value = "";
      MyDbCommand.Parameters["?Message_ID"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[1];

      PhoneNumberConfig config = (PhoneNumberConfig)initObjects[2];
      MyDbCommand.Parameters["?telSOS"].Value = config.NumberSOS;
      MyDbCommand.Parameters["?telDspt"].Value = config.NumberDspt;
      MyDbCommand.Parameters["?telAccept1"].Value = config.NumberAccept1;
      MyDbCommand.Parameters["?telAccept2"].Value = config.NumberAccept2;
      MyDbCommand.Parameters["?telAccept3"].Value = config.NumberAccept3;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
