﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  class InsertServiceSend : SrvDbCommand
  {
    private const string QUERY =
      "INSERT INTO servicesend (Name, Descr, ID, telMobitel) " +
      "VALUES (?Name, ?Descr, COALESCE((SELECT m.ServiceSend_ID FROM mobitels m WHERE m.Mobitel_ID = ?Mobitel), 0), " +
      "COALESCE((SELECT TelNumber_ID from telnumbers t WHERE TelNumber = ?telMobitel), 0))";

    private int newId;
    public int NewId
    {
      get { return newId; }
    }

    public InsertServiceSend(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?Name", eMySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Descr", MySqlDbType.String));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Mobitel", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?telMobitel", MySqlDbType.String));
    }

    /// <summary>
    /// Порядок передачи параметров:
    /// 1. name
    /// 2. description
    /// 3. mobitelID
    /// 4. telMobitels
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "InsertServiceSend", "object[] initObjects"));
      if (initObjects.Length != 4)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "InsertServiceSend",
            initObjects.Length, 4), "object[] initObjects");

      MyDbCommand.Parameters["?Name"].Value = (string)initObjects[0];
      MyDbCommand.Parameters["?Descr"].Value = (string)initObjects[1];
      MyDbCommand.Parameters["?Mobitel"].Value = (int)initObjects[2];
      MyDbCommand.Parameters["?telMobitel"].Value = (string)initObjects[3];
      newId = 0;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();
      newId = (int)MyDbCommand.LastInsertedId;

      return ExecutionStatus.OK;
    }
  }
}

