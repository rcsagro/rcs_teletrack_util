using System;
using System.Data;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������� ���-�� ����� �������������� ��������� ������ ��� ����������� ����������
  /// </summary>
  class A1SelectNewMessageCount : SrvDbCommand
  {
    private const string QUERY =
      "SELECT COUNT(msg.Message_ID) " +
      "FROM Messages msg JOIN Mobitels m ON m.Mobitel_id = msg.Mobitel_id " +
      " JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID) " +
      "WHERE (msg.isNew = 1) AND (msg.Direction = 1) AND " +
      " (c.InternalMobitelConfig_ID = ( " +
      "   SELECT intConf.InternalMobitelConfig_ID " +
      "   FROM internalmobitelconfig intConf " +
      "   WHERE (intConf.ID = c.ID)  " +
      "   ORDER BY intConf.InternalMobitelConfig_ID DESC " +
      "   LIMIT 1)) AND " +
      " (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')";

    /// <summary>
    /// ���-�� ���������
    /// </summary>
    private int count;
    /// <summary>
    /// ���-�� ���������
    /// </summary>
    public int Count
    {
      get { return count; }
    }

    /// <summary>
    /// Create A1 select new messages
    /// </summary>
    /// <param name="connectionString">Connection string</param>
    public A1SelectNewMessageCount(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      count = Convert.ToInt32(MyDbCommand.ExecuteScalar());
      return ExecutionStatus.OK;
    }
  }
}
