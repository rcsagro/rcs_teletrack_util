using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ���������� ����� ������� Messages �������� ������ ����� �������� ���������
  /// </summary>
  class A1UpdateSentMessage : SrvDbCommand
  {
    private const string QUERY =
      "UPDATE Messages " +
      "SET IsNew = 0, IsSend = 1, Time3 = Unix_Timestamp(?Time), Source_ID = ?SourceId " +
      "WHERE Message_ID = ?MessageId";

    public A1UpdateSentMessage(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?SourceId", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?Time", MySqlDbType.DateTime));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. MessageId
    /// 2. SourceId
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1UpdateSentMessage", "object[] initObjects"));
      if (initObjects.Length != 2)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1UpdateSentMessage",
            initObjects.Length, 2), "object[] initObjects");

      MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
      MyDbCommand.Parameters["?SourceId"].Value = (int)initObjects[1];
      MyDbCommand.Parameters["?Time"].Value = DateTime.Now;
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
