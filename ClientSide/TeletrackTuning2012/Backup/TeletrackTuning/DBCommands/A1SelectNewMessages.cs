using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������� ����� ���������.
  /// �������� ������ ��������� ��������� ��� ������ �� ������ ������
  /// ����������, � ��� ���� ���������, ����� �������� ��� ��������� ������� 
  /// </summary>
  class A1SelectNewMessages : SrvDbCommand
  {
    private const string QUERY =
      "SELECT m1.Message_ID AS MessageID, m1.Command_ID AS CommandID, " +
      "  m1.MessageCounter AS MessageCounter " +
      "FROM Messages m1 JOIN ( " +
      "    SELECT Command_ID, MAX(Message_ID) AS MessageId " +
      "    FROM Messages " +
      "    WHERE (Mobitel_ID = ?MobitelId) AND (isNew = 1) AND (Direction = 1) " +
      "    GROUP BY Command_ID) m2 " +
      "  ON m1.Message_ID = m2.MessageId ";

    /// <summary>
    /// Create A1 select new messages
    /// </summary>
    /// <param name="connectionString">Connection string</param>
    public A1SelectNewMessages(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. MobitelId
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1SelectNewMessages", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectNewMessages",
            initObjects.Length, 1), "object[] initObjects");

      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[0];
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
