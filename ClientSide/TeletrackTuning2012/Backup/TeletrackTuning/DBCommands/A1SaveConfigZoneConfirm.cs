using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ���������� ������ �� ��������� ���
  /// </summary>
  class A1SaveConfigZoneConfirm : SrvDbCommand
  {
    private const string QUERY =
      "UPDATE Messages SET AdvCounter = ?ZoneResult " +
      "WHERE Message_ID = ?ZoneMsgId";

    public A1SaveConfigZoneConfirm(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?ZoneResult", MySqlDbType.Int32));
      MyDbCommand.Parameters.Add(new MySqlParameter("?ZoneMsgId", MySqlDbType.Int32));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. ZoneMsgId
    /// 2. ZoneResult
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1SaveConfigZoneConfirm", "object[] initObjects"));
      if (initObjects.Length != 2)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SaveConfigZoneConfirm",
            initObjects.Length, 2), "object[] initObjects");

      MyDbCommand.Parameters["?ZoneMsgId"].Value = (int)initObjects[0]; ;
      MyDbCommand.Parameters["?ZoneResult"].Value = (int)initObjects[1];
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }

  }
}
