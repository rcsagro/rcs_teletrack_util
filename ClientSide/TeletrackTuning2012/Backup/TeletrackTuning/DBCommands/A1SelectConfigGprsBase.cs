using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������� �� �� �������� �������� GPRS
  /// </summary>
  class A1SelectConfigGprsBase : SrvDbCommand
  {
    private const string QUERY =
      "SELECT Mode, Apnserv, Apnun, Apnpw, " +
      " Dnsserv1, Dialn1, Ispun, Isppw " +
      "FROM ConfigGprsMain " + 
      "WHERE ID = COALESCE((SELECT ConfigGprsMain_ID FROM ConfigMain WHERE Mobitel_ID = ?MobitelId LIMIT 1), -1) "+
      "ORDER by ConfigGprsMain_ID DESC LIMIT 1";
      //"WHERE Message_ID = ?MessageId";

    public A1SelectConfigGprsBase(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MobitelId", MySqlDbType.Int32));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. MobitelId
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1SelectConfigGprsBase", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1SelectConfigGprsBase",
            initObjects.Length, 1), "object[] initObjects");

      MyDbCommand.Parameters["?MobitelId"].Value = (int)initObjects[0];
    }

    protected override MySqlDataReader InternalExecuteReader()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      return MyDbCommand.ExecuteReader();
    }
  }
}
