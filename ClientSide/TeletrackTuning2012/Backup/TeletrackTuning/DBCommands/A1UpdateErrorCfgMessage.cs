using System;
using System.Data;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;

namespace TeletrackTuning.SrvDBCommand
{
  /// <summary>
  /// ������ ����� isnew ��������� � ������ �������������
  /// ���������� ��������
  /// </summary>
  class A1UpdateErrorCfgMessage : SrvDbCommand
  {
    private const string QUERY =
      "UPDATE messages " +
      "SET isNew = 0, DataFromService = 'Error Settings' " +
      "WHERE Message_ID = ?MessageId";

    public A1UpdateErrorCfgMessage(string connectionString)
      : base(connectionString)
    {
      MyDbCommand.CommandType = CommandType.Text;
      MyDbCommand.CommandText = QUERY;
      MyDbCommand.Parameters.Add(new MySqlParameter("?MessageId", MySqlDbType.Int32));
    }

    /// <summary>
    /// ������� �������� ����������:
    /// 1. MessageId
    /// </summary>
    /// <param name="initObjects"></param>
    public override void Init(params object[] initObjects)
    {
      if (initObjects == null)
        throw new ArgumentNullException("object[] initObjects",
          String.Format(ErrorText.ARGUMENT_NULL,
            "Init", "A1UpdateErrorCfgMessage", "object[] initObjects"));
      if (initObjects.Length != 1)
        throw new ArgumentException(String.Format(
          ErrorText.DB_WRONG_INIT_PARAMS_LENGTH, "A1UpdateErrorCfgMessage",
            initObjects.Length, 1), "object[] initObjects");

      MyDbCommand.Parameters["?MessageId"].Value = (int)initObjects[0];
    }

    protected override ExecutionStatus InternalExecute()
    {
      MyDbCommand.Connection = Connection;
      Connection.Open();
      MyDbCommand.ExecuteNonQuery();

      return ExecutionStatus.OK;
    }
  }
}
