﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;
using System.IO.Ports;
using System.Threading;
using MySql.Data.MySqlClient;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;
using TeletrackTuning.Properties;

namespace TeletrackTuning
{
    public partial class FormBase : Form
    {
        private const string DcOnly = "Dicect Cable Only";
        private const string DcDataBase = "Dicect Cable + DataBase";
        private const string Online = "Online";
        private const string SmsDataBase = "SMS + DataBase";
        private ushort cmdMsgCnt = 0;
        private ushort waitCmdMsgID = 0;
        private int waitCommandID = 0;
        private int counterWaitMsgID = 0;
        private string connectionString = "";
        private StructureSaveSetting structureSaveSett = null;
        private GsmOperatorSetting gsmGprsSetting = null;

        static object locker = new object();

        private SerialPort mySerialPort;
        private List<TxPacket> needTx = new List<TxPacket>();
        private DbDataManager dbDataMngr;
        private Dictionary<string, string> dicPhones = new Dictionary<string, string>();

        public FormBase()
        {
            InitializeComponent();

            ComboBoxInterfaceSettings.Items.Clear();

            ComboBoxInterfaceSettings.Items.Add( DcOnly );
            ComboBoxInterfaceSettings.Items.Add( DcDataBase );
            ComboBoxInterfaceSettings.Items.Add( Online );
            ComboBoxInterfaceSettings.Items.Add( SmsDataBase );
            ComboBoxInterfaceSettings.Text = ( string )ComboBoxInterfaceSettings.Items[0];

            mySerialPort = new SerialPort();
            mySerialPort.BaudRate = 115200;
            //mySerialPort.RtsEnable = Enabled;
            //mySerialPort.DtrEnable = Enabled;
            mySerialPort.StopBits = StopBits.One;//.OnePointFive;Two;//.
            mySerialPort.Handshake = Handshake.RequestToSend;
            mySerialPort.WriteTimeout = 500;

            ComboBoxFillComPort();

            textBoxID_TT.ReadOnly = false;
            ButtonDisconnect.Enabled = false;
            #region DataGridCellSetting
            //tabPageGPRS.dataGridViewGprsSetting;
            dataGridViewGprsSetting.RowCount = 8;
            dataGridViewGprsSetting.Rows[0].Cells[0].Value = "Режим/Regim";
            dataGridViewGprsSetting.Rows[1].Cells[0].Value = "Точка доступа GPRS/Access point name";
            dataGridViewGprsSetting.Rows[2].Cells[0].Value = "Логин для GPRS/Login GPRS";
            dataGridViewGprsSetting.Rows[3].Cells[0].Value = "Пароль для GPRS/Password GPRS";
            dataGridViewGprsSetting.Rows[4].Cells[0].Value = "DNS конфигурация/DNS server";
            dataGridViewGprsSetting.Rows[5].Cells[0].Value = "Номер дозвона/ISP phone number";
            dataGridViewGprsSetting.Rows[6].Cells[0].Value = "ISP логин/ISP login";
            dataGridViewGprsSetting.Rows[7].Cells[0].Value = "ISP пароль/ISP password";

            dataGridViewServer.RowCount = 3;
            dataGridViewServer.Rows[0].Cells[0].Value = "Сервер/Server";
            dataGridViewServer.Rows[1].Cells[0].Value = "Логин/Login";
            dataGridViewServer.Rows[2].Cells[0].Value = "Пароль/Password";

            dataGridViewPhones.RowCount = 6;
            dataGridViewPhones.Rows[0].Cells[0].Value = "Номер SOS/Phone SOS";
            dataGridViewPhones.Rows[1].Cells[0].Value = "Номер телефона диспетчера/Phone dispetcher";
            dataGridViewPhones.Rows[2].Cells[0].Value = "Разрешенный номер 1/Legal phone 1";
            dataGridViewPhones.Rows[3].Cells[0].Value = "Разрешенный номер 2/Legal phone 2";
            dataGridViewPhones.Rows[4].Cells[0].Value = "Разрешенный номер 3/Legal phone 3";
            dataGridViewPhones.Rows[5].Cells[0].Value = "Телефон обратной связи/Telephone feedback";

            dataGridViewEvent.RowCount = 21;
            DataGridViewRow dgv_row = dataGridViewEvent.Rows[0];
            dgv_row.Cells["ColumnEventsName"].Value = "Минимальная скорость/Min speed";
            dgv_row.Cells["ColumnEventsValue"].Value = 2;
            dgv_row.Cells["ColumnEventsValue"].ReadOnly = false;
            dgv_row.Cells["ColumnEventsUnit"].Value = "km/h";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;
            dgv_row.Cells["ColumnEventsRecomended"].Value = "0-5";
            dgv_row.Cells["ColumnEventsMaxValue"].Value = "0-250";

            dgv_row = dataGridViewEvent.Rows[1];
            dgv_row.Cells["ColumnEventsName"].Value = "Таймер 1/Timer 1";
            dgv_row.Cells["ColumnEventsValue"].Value = 60;
            dgv_row.Cells["ColumnEventsValue"].ReadOnly = false;
            dgv_row.Cells["ColumnEventsUnit"].Value = "sec";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;
            dgv_row.Cells["ColumnEventsRecomended"].Value = "30-3600";
            dgv_row.Cells["ColumnEventsMaxValue"].Value = "1-65535";

            dgv_row = dataGridViewEvent.Rows[2];
            dgv_row.Cells["ColumnEventsName"].Value = "Таймер 2/Timer 2";
            dgv_row.Cells["ColumnEventsValue"].Value = 600;
            dgv_row.Cells["ColumnEventsValue"].ReadOnly = false;
            dgv_row.Cells["ColumnEventsUnit"].Value = "sec";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;
            dgv_row.Cells["ColumnEventsRecomended"].Value = "30-3600";
            dgv_row.Cells["ColumnEventsMaxValue"].Value = "1-65535";

            dgv_row = dataGridViewEvent.Rows[3];
            dgv_row.Cells["ColumnEventsName"].Value = "Отклонение курса/Deviation of the course";
            dgv_row.Cells["ColumnEventsValue"].Value = 15;
            dgv_row.Cells["ColumnEventsValue"].ReadOnly = false;
            dgv_row.Cells["ColumnEventsUnit"].Value = "metr";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;
            dgv_row.Cells["ColumnEventsRecomended"].Value = "1-200";
            dgv_row.Cells["ColumnEventsMaxValue"].Value = "1-1000";

            dgv_row = dataGridViewEvent.Rows[4];
            dgv_row.Cells["ColumnEventsName"].Value = "Дистанция 1/Distance 1";
            dgv_row.Cells["ColumnEventsValue"].Value = 200;
            dgv_row.Cells["ColumnEventsValue"].ReadOnly = false;
            dgv_row.Cells["ColumnEventsUnit"].Value = "metr";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;
            dgv_row.Cells["ColumnEventsRecomended"].Value = "100-10000";
            dgv_row.Cells["ColumnEventsMaxValue"].Value = "100-65535";

            dgv_row = dataGridViewEvent.Rows[5];
            dgv_row.Cells["ColumnEventsName"].Value = "Дистанция 2/Distance 2";
            dgv_row.Cells["ColumnEventsValue"].Value = 2000;
            dgv_row.Cells["ColumnEventsValue"].ReadOnly = false;
            dgv_row.Cells["ColumnEventsUnit"].Value = "metr";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;
            dgv_row.Cells["ColumnEventsRecomended"].Value = "100-10000";
            dgv_row.Cells["ColumnEventsMaxValue"].Value = "100-65535";

            dgv_row = dataGridViewEvent.Rows[6];
            dgv_row.Cells["ColumnEventsName"].Value = "Ускорение/Acceleration";
            dgv_row.Cells["ColumnEventsValue"].Value = 10;
            dgv_row.Cells["ColumnEventsValue"].ReadOnly = false;
            dgv_row.Cells["ColumnEventsUnit"].Value = "km/h";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;
            dgv_row.Cells["ColumnEventsRecomended"].Value = "10";
            dgv_row.Cells["ColumnEventsMaxValue"].Value = "0-12";

            dgv_row = dataGridViewEvent.Rows[7];
            dgv_row.Cells["ColumnEventsName"].Value = "Питание включено/Power ON";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;

            dgv_row = dataGridViewEvent.Rows[8];
            dgv_row.Cells["ColumnEventsName"].Value = "Питание выключено/Power OFF";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;

            dgv_row = dataGridViewEvent.Rows[9];
            dgv_row.Cells["ColumnEventsName"].Value = "GSM появилось/GSM appeared";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;


            dgv_row = dataGridViewEvent.Rows[10];
            dgv_row.Cells["ColumnEventsName"].Value = "GSM пропало/GSM lost";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;

            dgv_row = dataGridViewEvent.Rows[11];
            dgv_row.Cells["ColumnEventsName"].Value = "Лог заполнен/Log full";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;

            dgv_row = dataGridViewEvent.Rows[12];
            dgv_row.Cells["ColumnEventsName"].Value = "Датчики/Sensors";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;

            dgv_row = dataGridViewEvent.Rows[13];
            dgv_row.Cells["ColumnEventsName"].Value = "Включение/Device ON";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;

            dgv_row = dataGridViewEvent.Rows[14];
            dgv_row.Cells["ColumnEventsName"].Value = "Сброс/Device restart";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;

            dgv_row = dataGridViewEvent.Rows[15];
            dgv_row.Cells["ColumnEventsName"].Value = "Звонок с тел 1/Call from a phone 1";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;

            dgv_row = dataGridViewEvent.Rows[16];
            dgv_row.Cells["ColumnEventsName"].Value = "Звонок с тел 2/Call from a phone 2";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;

            dgv_row = dataGridViewEvent.Rows[17];
            dgv_row.Cells["ColumnEventsName"].Value = "Звонок с тел 3/Call from a phone 3";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;

            dgv_row = dataGridViewEvent.Rows[18];
            dgv_row.Cells["ColumnEventsName"].Value = "Звонок диспетчеру/Call the dispetcher";
            dgv_row.Cells["ColumnEventsCheckWritePoint"].Value = true;
            dgv_row.Cells["ColumnEventsCheckSendToServer"].Value = false;
            dgv_row.Cells["ColumnEventsCheckSendSMS"].Value = false;

            dataGVServiceSetting.RowCount = 4;
            dataGVServiceSetting.Rows[0].Cells[0].Value = "IP";
            dataGVServiceSetting.Rows[1].Cells[0].Value = "Port";
            dataGVServiceSetting.Rows[2].Cells[0].Value = "Login";
            dataGVServiceSetting.Rows[3].Cells[0].Value = "Password";

            #endregion

            gsmGprsSetting = new GsmOperatorSetting();
            gsmGprsSetting.LoadDictionary();
            LoadComboBoxGsmOperators();
        }

        private void ComboBoxFillComPort()
        {
            comboBoxComPort.Items.Clear();
            string[] ports = SerialPort.GetPortNames();
            string tmp_port = null;
            foreach( string port in ports )
            {
                if( tmp_port != port )
                {
                    this.comboBoxComPort.Items.Add( port );
                }
                tmp_port = port;
            }
            if( comboBoxComPort.Items.Count > 0 )
                this.comboBoxComPort.SelectedIndex = 0;
        }

        private void ComboBoxInterfaceSettings_SelectedIndexChanged( object sender, EventArgs e )
        {
            ButtonDisconnect_Click( this, e );
            switch( ComboBoxInterfaceSettings.Text )
            {
                case DcOnly:
                    break;
                case DcDataBase:
                    break;
                case Online:
                    break;
                case SmsDataBase:
                //break;
                default:
                    MessageBox.Show( msg.NowOnlyOther );
                    ComboBoxInterfaceSettings.Text = DcOnly;
                    break;
            }
        }

        private void ButtonConnect_Click( object sender, EventArgs e )
        {
            bool enableBtn = true;
            if( ComboBoxInterfaceSettings.Text == DcDataBase || ComboBoxInterfaceSettings.Text == Online )
            {//tst connection to DB
                try
                {
                    connectionString = string.Format( "server={0};user id={1}; Password={2}; database={3}",
                                              Settings.Default.Server, Settings.Default.User, Settings.Default.Password, Settings.Default.DataBase );
                    //TO DO: загрувить с базы список всех ТТ и их настройки
                    dbDataMngr = new DbDataManager( connectionString );
                    if( dbDataMngr.FillMobitelCfg() == ExecutionStatus.OK )
                    {
                        LoadMobitelsInfo( dbDataMngr );
                        dicPhones.Clear();
                        if( dbDataMngr.FillPhones( ref dicPhones ) == ExecutionStatus.OK )
                        {
                            //Fill dataGVInDataBase.
                            int lenColection = dicPhones.Count;
                            dataGVPhonesInDataBase.Rows.Clear();
                            dataGVPhonesInDataBase.Rows.Add( lenColection );
                            string[] names = new string[lenColection];
                            string[] phone = new string[lenColection];
                            dicPhones.Keys.CopyTo( phone, 0 );
                            dicPhones.Values.CopyTo( names, 0 );
                            for( int i = 0; i < lenColection; i++ )
                            {
                                dataGVPhonesInDataBase["ColumnName", i].Value = names[i];
                                dataGVPhonesInDataBase["ColumnNumber", i].Value = phone[i];
                            }
                        }
                    }
                    //Load Service Setting
                    string serviceIP = "";
                    string servicePort = "";
                    string serviceLogin = "";
                    string servicePassword = "";
                    if( dbDataMngr.LoadServiceSetting( ref serviceIP, ref servicePort, ref serviceLogin, ref servicePassword ) == ExecutionStatus.OK )
                    {
                        dataGVServiceSetting.Rows[0].Cells[1].Value = serviceIP;
                        dataGVServiceSetting.Rows[1].Cells[1].Value = servicePort;
                        dataGVServiceSetting.Rows[2].Cells[1].Value = serviceLogin;
                        dataGVServiceSetting.Rows[3].Cells[1].Value = servicePassword;
                    }

                    buttonSetTtPhone.Enabled = true;
                    buttonAddNewPhone.Enabled = true;
                    buttonAddNewTt.Enabled = true;
                    buttonSaveServiceSetting.Enabled = true;
                    buttonEditTt.Enabled = true;
                    buttonDeleteTt.Enabled = true;
                    if( ComboBoxInterfaceSettings.Text == Online )
                        enableBtn = true;
                }
                catch( Exception ex )
                {
                    MessageBox.Show( ex.Message, "Connect DB Exception" );
                    enableBtn = false;//connectionString = "";
                }
            }

            if( ComboBoxInterfaceSettings.Text == DcOnly || ComboBoxInterfaceSettings.Text == DcDataBase )
            {
                if( comboBoxComPort.Text != "" )
                {
                    try
                    {
                        if( mySerialPort.IsOpen )
                            mySerialPort.Close();
                        mySerialPort.PortName = comboBoxComPort.Text;
                        mySerialPort.BaudRate = Convert.ToInt32( comboBoxBaudrate.Text );
                        mySerialPort.Open();

                        timerThreadRx.Start();

                        TxPacket newTx = new TxPacket();
                        newTx.isCyclic = true;
                        newTx.timeOutSet = 3000;
                        newTx.timeOutCurrent = newTx.timeOutSet;
                        newTx.txString = msg.PingStr;
                        needTx.Add( newTx );
                        //char[] sendMsg = msg.PingStr.ToCharArray();
                        //for (int i = 0; i < sendMsg.Length; i++)
                        //{
                        //  mySerialPort.Write(sendMsg, i, 1);
                        //  Thread.Sleep(2);
                        //}
                        //mySerialPort.Write("\n".ToCharArray(), 0, 1);

                        enableBtn = true;
                    }
                    catch( Exception ex )
                    {
                        MessageBox.Show( ex.Message, "Connect DC Exception" );
                    }
                }
                else
                    MessageBox.Show( msg.NeedSelectComPort );
            }

            if( enableBtn )
            {
                ButtonDisconnect.Enabled = true;
                ButtonConnect.Enabled = false;
                buttonReadParameter.Enabled = true;
                buttonWriteParameter.Enabled = true;
            }
        }

        private void ButtonDisconnect_Click( object sender, EventArgs e )
        {
            ButtonDisconnect.Enabled = false;
            ButtonConnect.Enabled = true;
            connectionString = "";

            if( dbDataMngr != null )
                dbDataMngr.dictMobitelID.Clear();

            dataGridViewTeletracks.Rows.Clear();

            timerThreadRx.Stop();
            if( mySerialPort != null )
                mySerialPort.Close();
            needTx.Clear();

            buttonSetTtPhone.Enabled = false;
            buttonAddNewPhone.Enabled = false;
            buttonAddNewTt.Enabled = false;
            buttonEditTt.Enabled = false;
            buttonDeleteTt.Enabled = false;
            buttonSaveServiceSetting.Enabled = false;
            buttonReadParameter.Enabled = false;
            buttonWriteParameter.Enabled = false;
            textBoxID_TT.BackColor = Color.LightCoral;
        }

        private void buttonReadParameter_Click( object sender, EventArgs e )
        {
            try
            {
                if( counterWaitMsgID > 0 )
                {
                    MessageBox.Show( Resources.waitRequestOnCommand );
                    return;
                }
                if( textBoxID_TT.Text == "" )
                {
                    MessageBox.Show( Resources.unknownID );
                    return;
                }

                SimpleQuery simpleQ = new SimpleQuery();
                simpleQ.ShortID = textBoxID_TT.Text;
                simpleQ.MessageID = ++cmdMsgCnt;

                TxPacket txPack = new TxPacket();
                txPack.timeOutCurrent = 10;
                string logMessage = "";
                int messageId = -1;
                int commandID = 0;

                switch( tabControlSettings.SelectedTab.Text )
                {
                    case "Events":
                        txPack.txString = EncodeA1.EncodeEventConfigQuery( simpleQ );
                        commandID = ( int )CommandDescriptor.EventConfigQuery;
                        logMessage = Resources.requestEvents;
                        break;
                    case "GPRS":
                        txPack.txString = EncodeA1.EncodeGprsBaseConfigQuery( simpleQ );
                        commandID = ( int )CommandDescriptor.GprsBaseConfigQuery;
                        logMessage = Resources.requestGprsSetting;
                        break;
                    case "Server":
                        txPack.txString = EncodeA1.EncodeGprsEmailConfigQuery( simpleQ );
                        commandID = ( int )CommandDescriptor.GprsEmailConfigQuery;
                        logMessage = Resources.requestServerSetting;
                        break;
                    case "Phones":
                        txPack.txString = EncodeA1.EncodePhoneNumberConfigQuery( simpleQ );
                        commandID = ( int )CommandDescriptor.PhoneConfigQuery;
                        logMessage = Resources.requestPhonesSetting;
                        break;
                    case "TextCmd":
                        commandID = ( int )CommandDescriptor.TextCommand;//Вроде сообщения водителю
                        txPack.txString = textBoxSendText.Text;
                        textBoxReadText.Text = "";
                        logMessage = Resources.runTextCommand;
                        break;
                    case "GPS":
                        DataGpsQuery query = CreateDataGpsQuery();
                        txPack.txString = EncodeA1.EncodeDataGpsQuery( query );
                        commandID = ( int )CommandDescriptor.DataGpsQuery;
                        logMessage = Resources.requestGpsData;
                        break;
                    default:
                        MessageBox.Show( Resources.unknownCommand );
                        break;
                }

                if( txPack.txString != "" )//Есть сформированная комманда
                {
                    logMessage = RunCmdForSelectedInterface( logMessage, txPack, commandID, cmdMsgCnt, out messageId );
                    AddToListBox( logMessage );
                }
                else
                {
                    cmdMsgCnt--;
                }
            }
            catch( Exception ex )
            {
                MessageBox.Show( ex.Message, "Exception Read" );
            }
        }

        private void buttonWriteParameter_Click( object sender, EventArgs e )
        {
            try
            {
                if( counterWaitMsgID > 0 )
                {
                    MessageBox.Show( Resources.waitRequestOnCommand );
                    return;
                }
                if( textBoxID_TT.Text == "" )
                {
                    MessageBox.Show( Resources.unknownID );
                    return;
                }

                TxPacket txPack = new TxPacket();
                txPack.timeOutCurrent = 10;
                string logMessage = "";
                int messageId = -1;
                int commandID = 0;

                switch( tabControlSettings.SelectedTab.Text )
                {
                    case "Events":
                        EventConfig eventConf = DataGridViewGetEvent();
                        txPack.txString = EncodeA1.EncodeEventConfigSet( eventConf );
                        commandID = ( int )CommandDescriptor.EventConfigSet;
                        logMessage = Resources.setEvents;
                        break;
                    case "GPRS":
                        GprsBaseConfig gprsBaseConf = DataGridViewGetGprsBase();
                        txPack.txString = EncodeA1.EncodeGprsBaseConfigSet( gprsBaseConf );
                        commandID = ( int )CommandDescriptor.GprsBaseConfigSet;
                        logMessage = Resources.setGprsSetting;
                        break;
                    case "Server":
                        GprsEmailConfig gprsEmailConf = DataGridViewGetGprsEmail();
                        txPack.txString = EncodeA1.EncodeGprsEmailConfigSet( gprsEmailConf );
                        commandID = ( int )CommandDescriptor.GprsEmailConfigSet;
                        logMessage = Resources.setServerSetting;
                        break;
                    case "Phones":
                        PhoneNumberConfig phoneNumberConf = DataGridViewGetPhones();
                        txPack.txString = EncodeA1.EncodePhoneNumberConfigSet( phoneNumberConf );
                        commandID = ( int )CommandDescriptor.PhoneConfigSet;
                        logMessage = Resources.setPhonesSetting;
                        break;
                    case "TextCmd": //Аналогично выполняется WriteParameter
                        commandID = ( int )CommandDescriptor.TextCommand;//Вроде сообщения водителю
                        txPack.txString = textBoxSendText.Text;
                        textBoxReadText.Text = "";
                        logMessage = Resources.runTextCommand;
                        break;
                    case "GPS":
                        DataGpsQuery query = CreateDataGpsQuery();
                        txPack.txString = EncodeA1.EncodeDataGpsQuery( query );
                        commandID = ( int )CommandDescriptor.DataGpsQuery;
                        logMessage = Resources.requestGpsData;
                        break;
                    default:
                        cmdMsgCnt--;
                        MessageBox.Show( Resources.unknownCommand );
                        break;
                }

                if( txPack.txString != "" )//Есть сформированная комманда
                {
                    logMessage = RunCmdForSelectedInterface( logMessage, txPack, commandID, cmdMsgCnt, out messageId );
                    AddToListBox( logMessage );
                }
                else
                {
                    cmdMsgCnt--;
                }
            }
            catch( Exception ex )
            {
                MessageBox.Show( ex.Message, "Exception Write" );
            }
        }

        private DataGpsQuery CreateDataGpsQuery()
        {
            DataGpsQuery query = new DataGpsQuery();
            query.ShortID = textBoxID_TT.Text;
            query.MessageID = ++cmdMsgCnt;
            query.CheckMask = ( 1 << 0 );//10);
            query.CommandID = CommandDescriptor.DataGpsQuery;
            query.WhatSend = ( ( 1 << 10 ) - 1 );
            query.MaxSmsNumber = 1;

            query.StartTime = 0;// (int)((TimeZone.CurrentTimeZone.ToUniversalTime(dateTimePickerGpsBegin.Value) - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds);
            query.EndTime = 0;// (int)((TimeZone.CurrentTimeZone.ToUniversalTime(dateTimePickerGpsEnd.Value) - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds);
            query.LastRecords = 101;
            return query;
        }

        private static List<string> bufferRx = new List<string>();
        private void timerThreadRx_Tick( object sender, EventArgs e )
        {
            try
            {
                if( mySerialPort.IsOpen )
                {
                    const string clearStr = "";
                    while( mySerialPort.BytesToRead > 0 )
                    {
                        char tmpRead = ( char )mySerialPort.ReadChar();
                        if( bufferRx.Count == 0 )
                            bufferRx.Add( clearStr );

                        if( tmpRead == '\r' || tmpRead == '\n' )
                        {
                            if( bufferRx[bufferRx.Count - 1] != "" )
                                bufferRx.Add( clearStr );
                        }
                        else
                            bufferRx[bufferRx.Count - 1] += tmpRead;
                    }

                    while( bufferRx.Count > 1 )//может быть А1
                    {
                        string curentParse = bufferRx[0];
                        bufferRx.RemoveAt( 0 );
                        int startDataPacket = curentParse.IndexOf( "%%" );

                        if( startDataPacket >= Const.LEVEL0_START_INDICATOR_POSITION )
                        {//Есть строка в буффере
                            curentParse = curentParse.Substring( startDataPacket - Const.LEVEL0_START_INDICATOR_POSITION );//уберем лишний кусок в начале
                            if( curentParse.Length >= Const.LEVEL0_PACKET_LENGTH )
                            {
                                byte[] rxPacketBytes = CodeDecodeA1Levels.StringToByteArray( curentParse );
                                string rxEmail = CodeDecodeA1Levels.GetA1Email( curentParse );
                                PacketBlock rxPackBlock = CodeDecodeA1Levels.ConvertSymbolToPacketBlock( rxPacketBytes );
                                if( rxPackBlock.isCrcOk == true )
                                {
                                    ParserA1Cmd( rxPackBlock, curentParse );
                                }
                            }
                        }
                        else
                        {//байты - но не А1
                            AddToListBox( "rx string: " + curentParse );
                            if( waitCommandID == ( int )CommandDescriptor.TextCommand && counterWaitMsgID > 0 )
                                textBoxReadText.Text += " " + curentParse;
                        }
                    }

                    //TX if need //С индексом 0 всегда будет пакет Ping!!!!! (Он первый добавляется при создании нового Connect)
                    if( needTx.Count > 0 )
                    {
                        for( int i = needTx.Count - 1; i >= 0; i-- )
                        {
                            if( needTx[i].timeOutCurrent > 0 )
                            {
                                needTx[i].timeOutCurrent -= timerThreadRx.Interval;
                                if( needTx[i].timeOutCurrent <= 0 )
                                {
                                    if( checkBoxPing.Checked || i != 0 )
                                    {
                                        char[] sendMsg = needTx[i].txString.ToCharArray();
                                        //string tstString = needTx[i].txString.Substring(0,28);
                                        byte[] cBuf = new byte[160];
                                        //sendMsg.CopyTo(cBuf, 0);
                                        for( int f = 0; f < 28 && f < sendMsg.Length; f++ )
                                            cBuf[f] = ( byte )sendMsg[f];

                                        for( int f = 28; f < 160 && f < sendMsg.Length; f++ )
                                        {
                                            cBuf[f] = ( byte )( 0x30 + f - 28 );
                                            //sendMsg[f] = (char)(cBuf[f-28]);
                                        }
                                        try
                                        {
                                            mySerialPort.WriteLine( needTx[i].txString + "\r" );
                                            //for (int f = 0; f < cBuf.Length; f++)
                                            //{
                                            //  mySerialPort.Write(cBuf, f, 1); //Thread.Sleep(1);
                                            //}
                                            //mySerialPort.Write("\r".ToCharArray(), 0, 1);
                                            //mySerialPort.Write("\n".ToCharArray(), 0, 1);
                                        }
                                        catch( System.TimeoutException exsep )
                                        {
                                            //none timeout write
                                        }
                                        if( needTx[i].isCyclic == true )
                                        {
                                            needTx[i].timeOutCurrent = needTx[i].timeOutSet;
                                            AddToListBox( "Send Cmd Ping" );
                                        }
                                        else//Delete Unused
                                        {//Если запрос был на команду (Не циклический) - проверим и отстрочим следующий Ping
                                            AddToListBox( "Send Cmd №" + waitCmdMsgID.ToString() );
                                            needTx.RemoveAt( i );
                                            if( needTx.Count > 0 && needTx[0].isCyclic )
                                                needTx[0].timeOutCurrent = needTx[0].timeOutSet;
                                            break;//Если уже один запрос отправлен, следующий отправим в следующий раз
                                        }
                                    }
                                    else
                                    {
                                        if( needTx[i].isCyclic == true )//Перезапускаем таймер, для циклического процесса
                                            needTx[i].timeOutCurrent = needTx[i].timeOutSet;
                                    }
                                }
                            }
                        }
                    }

                }

                lock( locker )
                {
                    if( counterWaitMsgID > 0 )//Таймер ожидания ответа на комманду
                    {
                        counterWaitMsgID -= timerThreadRx.Interval;
                        if( counterWaitMsgID <= 0 )
                        {
                            buttonReadParameter.Enabled = true;
                            buttonWriteParameter.Enabled = true;
                            textBoxID_TT.BackColor = Color.LightCoral;
                            AddToListBox( Resources.timeoutWaidRequestOnCmd + waitCmdMsgID.ToString() );
                            waitCmdMsgID = 0;
                            counterWaitMsgID = 0;
                        }
                    }
                }
            }
            catch( Exception ex )
            {
                MessageBox.Show( ex.Message + "\nException on timerRx\n" + ex.Source );
            }
        }

        public void ParserA1Cmd( PacketBlock rxPackBlock, string currentA1 )
        {
            DataBlock8bit rxDataBl8 = CodeDecodeA1Levels.ConverPacket6bitTo8bit( rxPackBlock );
            CommonDescription result = CodeDecodeA1Levels.DecodeLevel3Message( rxDataBl8.data );
            result.ShortID = CodeDecodeA1Levels.GetA1IDTT( rxPackBlock );
            result.Source = rxDataBl8.data;
            string logString = "Принят пакет А1: ";
            lock( locker )
            {
                if( result.MessageID != 0 && result.MessageID == waitCmdMsgID )
                {//Контроль выполнения комманды
                    buttonReadParameter.Enabled = true;
                    buttonWriteParameter.Enabled = true;
                    waitCmdMsgID = 0;
                    counterWaitMsgID = 0;
                    logString = string.Format( "Принят ответ №{0} на комманду А1: ", result.MessageID );
                }
            }

            int messageId = -1;
            int mobitelID = -1;
            if( ( ComboBoxInterfaceSettings.Text == DcDataBase || ComboBoxInterfaceSettings.Text == Online ) && result.ShortID != "\0\0\0\0" )//Проверка на наличие ТТ в базе
            {
                if( ( dbDataMngr == null || dbDataMngr.dictMobitelID.Count == 0 ||
                            dbDataMngr.dictMobitelID.ContainsKey( result.ShortID ) == false ) && result.CommandID != CommandDescriptor.Ping )
                    throw new Exception( "Устройство с таким номером в базе данных не найдено" );

                if( dbDataMngr.dictMobitelID.ContainsKey( result.ShortID ) )
                    mobitelID = dbDataMngr.dictMobitelID[result.ShortID];
                result.MobitelID = mobitelID;

                if( dbDataMngr.SaveInMessages( currentA1, ( int )result.CommandID,
                  mobitelID, result.MessageID, out messageId ) != ExecutionStatus.OK )
                {
                    throw new Exception( "Не удалось сохранить команду в базу" );
                }
            }

            switch( result.CommandID )//Разобрать пакет согласно типу - и отобразить на соотведствующей вкладке
            {
                case CommandDescriptor.Ping:
                    logString += "Ping";
                    if( textBoxID_TT.Text == "" || ( result.ShortID != "0000" && textBoxID_TT.Text == "0000" ) )
                    {
                        //Если подключены к базе то проверить есть ли такое устройство и выбрать его из списка
                        if( dbDataMngr == null || !dbDataMngr.dictMobitelID.ContainsKey( result.ShortID ) )
                            textBoxID_TT.Text = result.ShortID;
                        else
                        {
                            for( int i = 0; i < dataGridViewTeletracks.Rows.Count - 1; i++ )
                            {
                                if( dataGridViewTeletracks.Rows[i].Cells["ColumnShortIDTT"].Value.ToString() == result.ShortID )
                                {
                                    dataGridViewTeletracks.CurrentCell = dataGridViewTeletracks.Rows[i].Cells["ColumnShortIDTT"];
                                    dataGridViewTeletracks_SelectionChanged( this, null );
                                }
                            }
                        }
                    }
                    if( textBoxID_TT.Text == result.ShortID )
                        textBoxID_TT.BackColor = Color.GreenYellow;
                    else
                        textBoxID_TT.BackColor = Color.LightCoral;
                    break;
                case CommandDescriptor.GprsBaseConfigConfirm:
                case CommandDescriptor.GprsBaseConfigAnswer:
                    logString += "GPRS Base";
                    {
                        GprsBaseConfig configGprsBase = ( GprsBaseConfig )result;
                        DataGridViewSetGprsBase( configGprsBase );
                        if( messageId >= 0 )
                            dbDataMngr.SaveConfigGprsBase( messageId, configGprsBase );
                    }
                    break;
                case CommandDescriptor.GprsEmailConfigConfirm:
                case CommandDescriptor.GprsEmailConfigAnswer:
                    logString += "Server";
                    {
                        GprsEmailConfig configGprsEmail = ( GprsEmailConfig )result;
                        DataGridViewSetGprsEmail( configGprsEmail );

                        if( messageId >= 0 )
                            dbDataMngr.SaveConfigGprsEmail( messageId, configGprsEmail );
                    }
                    break;
                case CommandDescriptor.EventConfigConfirm:
                case CommandDescriptor.EventConfigAnswer:
                    logString += "Events";
                    {
                        EventConfig configEvent = ( EventConfig )result;
                        DataGridViewSetEvent( configEvent );

                        if( messageId >= 0 )
                            dbDataMngr.SaveConfigEvents( messageId, configEvent );
                    }
                    break;
                case CommandDescriptor.PhoneConfigConfirm:
                case CommandDescriptor.PhoneConfigAnswer:
                    logString += "Phones";
                    {
                        PhoneNumberConfig configPhone = ( PhoneNumberConfig )result;
                        DataGridViewSetPhones( configPhone );

                        if( messageId >= 0 )
                            dbDataMngr.SaveConfigPhoneNumbers( messageId, configPhone );
                    }
                    break;
                case CommandDescriptor.DataGpsAuto:
                case CommandDescriptor.DataGpsAnswer:
                    logString += "DataGpsAnswer.";
                    {
                        DataGpsAnswer dataGps = ( DataGpsAnswer )result;
                        logString += " Data Count = " + dataGps.DataGpsList.Count.ToString();
                        if( dataGps.DataGpsList.Count > 0 && dbDataMngr != null )
                        {
                            if( dbDataMngr.SaveDataGps( mobitelID, dataGps.DataGpsList, new string[] { "100" } ) != ExecutionStatus.OK )//Save DataGps 3320000000
                                logString += " ERROR INSERT DATA!!!";
                        }
                    }
                    break;
                default:
                    logString += "Unknown";
                    break;
            }

            AddToListBox( logString );//MessageBox.Show(result.Message, "Принят пакет А1");
        }

        /// <summary>
        /// Добавляем строку информации о событии
        /// </summary>
        /// <param name="text"></param>
        public void AddToListBox( string text )
        {
            listBoxLog.Items.Add( text );
            listBoxLog.SelectedIndex = listBoxLog.Items.Count - 1;
        }

        private void ButtonDBSetting_Click( object sender, EventArgs e )
        {
            ButtonDisconnect_Click( sender, e );
            //
            DbSettingForm formDbSet = new DbSettingForm();
            formDbSet.Show();
        }

        private void toolStripButtonSave_Click( object sender, EventArgs e )
        {
            //Copy from DataGridView to StructureSaveSEtting
            structureSaveSett = new StructureSaveSetting();
            structureSaveSett.baseConfigSetting = DataGridViewGetGprsBase();
            structureSaveSett.emailConfigSetting = DataGridViewGetGprsEmail();
            structureSaveSett.eventConfigSetting = DataGridViewGetEvent();
            structureSaveSett.phoneConfigSetting = DataGridViewGetPhones();

            //Save Structure to file
            try
            {
                if( saveFileDialog1.ShowDialog() == DialogResult.OK )
                    if( saveFileDialog1.FileName != null )
                    {
                        TextWriter writer = new StreamWriter( saveFileDialog1.FileName, false );
                        XmlSerializer serializer = new XmlSerializer( ( typeof( StructureSaveSetting ) ) );
                        serializer.Serialize( writer, structureSaveSett );
                        writer.Close();
                    }
                    else
                        MessageBox.Show( "недопустимое имя файла" );
            }
            catch( Exception ex )
            {
                MessageBox.Show( ex.Message, "error save file" );
            }
        }

        private void toolStripButtonOpen_Click( object sender, EventArgs e )
        {
            try
            {
                if( openFileDialog1.ShowDialog() == DialogResult.OK )
                {
                    TextReader reader = new StreamReader( openFileDialog1.FileName );
                    XmlSerializer serializer = new XmlSerializer( ( typeof( StructureSaveSetting ) ) );
                    structureSaveSett = ( StructureSaveSetting )serializer.Deserialize( reader );
                    reader.Close();

                    buttonSetFromFile.Enabled = true;
                    buttonSetFromFile_Click( this, e );
                }
            }
            catch( Exception ex )
            {
                MessageBox.Show( ex.Message, "error read file" );
            }
        }

        private string RunCmdForSelectedInterface( string log, TxPacket tx_pack, int comman_id, int msg_cnt, out int msg_ID )
        {
            int mobitel_id = -1;
            msg_ID = -1;
            if( ComboBoxInterfaceSettings.Text == DcDataBase || ComboBoxInterfaceSettings.Text == Online )//Проверка на наличие ТТ в базе
            {
                if( dbDataMngr == null || dbDataMngr.dictMobitelID.Count == 0 ||
                            dbDataMngr.dictMobitelID.ContainsKey( textBoxID_TT.Text ) == false )
                    throw new Exception( "Устройство с таким номером в базе данных не найдено" );
                mobitel_id = dbDataMngr.dictMobitelID[textBoxID_TT.Text];
            }
            //Выполнять комманду в зависимости от выбранного интерфейса
            int timeoutMsg = 3000;
            switch( ComboBoxInterfaceSettings.Text )
            {
                case DcOnly:
                    log = "DC:: " + log;
                    needTx.Add( tx_pack );
                    buttonReadParameter.Enabled = false;
                    buttonWriteParameter.Enabled = false;
                    break;
                case DcDataBase:
                    log = "DC DB:: " + log;
                    if( dbDataMngr.SaveInMessages( tx_pack.txString, comman_id, mobitel_id, msg_cnt, out msg_ID ) != ExecutionStatus.OK )
                    {
                        string errTxt = " Error Save to DataBase";
                        MessageBox.Show( errTxt );
                        log += errTxt;
                    }
                    needTx.Add( tx_pack );
                    buttonReadParameter.Enabled = false;
                    buttonWriteParameter.Enabled = false;
                    break;
                case Online:
                    log = "Online:: " + log;
                    if( dbDataMngr.SaveA1MessagesForOnline( tx_pack.txString, comman_id, mobitel_id, msg_cnt, out msg_ID ) != ExecutionStatus.OK )
                    {
                        string errTxt = " Error Insert command to DataBase for Online";
                        MessageBox.Show( errTxt );
                        log += errTxt;
                    }
                    else
                    {
                        ExecutionStatus cmdStatus = ExecutionStatus.OK;
                        switch( ( CommandDescriptor )comman_id )
                        {
                            case CommandDescriptor.GprsBaseConfigSet:
                                GprsBaseConfig configGprsBase = DataGridViewGetGprsBase();
                                configGprsBase.MobitelID = mobitel_id;
                                if( msg_ID >= 0 )
                                    cmdStatus = dbDataMngr.SaveConfigGprsBase( msg_ID, configGprsBase );
                                break;
                            case CommandDescriptor.GprsEmailConfigSet:
                                GprsEmailConfig configGprsEmail = DataGridViewGetGprsEmail();
                                configGprsEmail.MobitelID = mobitel_id;
                                if( msg_ID >= 0 )
                                    cmdStatus = dbDataMngr.SaveConfigGprsEmail( msg_ID, configGprsEmail );
                                break;
                            case CommandDescriptor.EventConfigSet:
                                EventConfig configEvent = DataGridViewGetEvent();
                                configEvent.MobitelID = mobitel_id;
                                if( msg_ID >= 0 )
                                    cmdStatus = dbDataMngr.SaveConfigEvents( msg_ID, configEvent );
                                break;
                            case CommandDescriptor.PhoneConfigSet:
                                PhoneNumberConfig configPhone = DataGridViewGetPhones();
                                configPhone.MobitelID = mobitel_id;
                                if( msg_ID >= 0 )
                                    cmdStatus = dbDataMngr.SaveConfigPhoneNumbers( msg_ID, configPhone );
                                break;
                            case CommandDescriptor.DataGpsQuery:
                                DataGpsQuery gpsQuery = CreateDataGpsQuery();
                                gpsQuery.MobitelID = mobitel_id;
                                if( msg_ID >= 0 )
                                    cmdStatus = dbDataMngr.SaveDataGpsQuery( msg_ID, gpsQuery );
                                break;
                            default:
                                break;
                        }
                        if( cmdStatus != ExecutionStatus.OK )
                        {
                            string errTxt = " Error Insert parameters command's to DataBase for Online";
                            MessageBox.Show( errTxt );
                            log += errTxt;
                        }
                    }
                    timeoutMsg = 0;
                    break;
                case SmsDataBase:
                    log = "SMS DB:: " + log;
                    timeoutMsg = 0;
                    break;
            }

            lock( locker )
            {
                waitCmdMsgID = cmdMsgCnt;
                waitCommandID = comman_id;
                counterWaitMsgID = timeoutMsg;
            }
            return log;
        }

        private void dataGridViewTeletracks_SelectionChanged( object sender, EventArgs e )
        {
            if( dataGridViewTeletracks.CurrentRow.Index >= 0 && dbDataMngr.dictMobitelID.Count > 0 && ( string )dataGridViewTeletracks.CurrentRow.Cells["ColumnShortIDTT"].Value != null )//.SelectedCells.Count > 0)
            {
                textBoxID_TT.Text = ( string )dataGridViewTeletracks.CurrentRow.Cells["ColumnShortIDTT"].Value;
                //Вычитываем настройки из Базы и заполняем его настройками форму.
                //SELECT ConfigGprsMain_ID, ConfigGprsEmail_ID, ConfigEvent_ID, ConfigTel_ID FROM configmain c where Mobitel_ID = 141 limit 1;
                if( dbDataMngr == null || dbDataMngr.dictMobitelID.Count == 0 || dbDataMngr.dictMobitelID.ContainsKey( textBoxID_TT.Text ) == false )
                {
                    MessageBox.Show( "Устройство с таким номером в базе данных не найдено или нет соединения с базой данных" );
                    return;
                }
                int mobitel_id = dbDataMngr.dictMobitelID[textBoxID_TT.Text];
                GprsBaseConfig gprsBaseConf = new GprsBaseConfig();
                GprsEmailConfig gprsEmailConf = new GprsEmailConfig();
                EventConfig eventConf = new EventConfig();
                PhoneNumberConfig phoneConf = new PhoneNumberConfig();
                bool isLoad = false;
                if( dbDataMngr.GetConfigGprsBase( mobitel_id, ref gprsBaseConf ) == ExecutionStatus.OK )
                {
                    if( dbDataMngr.GetConfigGprsEmail( mobitel_id, ref gprsEmailConf ) == ExecutionStatus.OK )
                    {
                        if( dbDataMngr.GetConfigEvents( mobitel_id, ref eventConf ) == ExecutionStatus.OK )
                        {
                            if( dbDataMngr.GetConfigPhoneNumbers( mobitel_id, ref phoneConf ) == ExecutionStatus.OK )
                            {
                                isLoad = true;
                                //Load to datagrid
                                DataGridViewSetGprsBase( gprsBaseConf );
                                DataGridViewSetGprsEmail( gprsEmailConf );
                                DataGridViewSetEvent( eventConf );
                                DataGridViewSetPhones( phoneConf );

                                textBoxTtPhone.Text = dbDataMngr.GetPhoneTt( textBoxID_TT.Text );
                            }
                        }
                    }
                }
                if( !isLoad )
                {
                    //Error;
                    MessageBox.Show( Resources.errorLoadSettingTT + textBoxID_TT.Text );
                }
            }
        }

        #region DataGridUse
        private void DataGridViewSetGprsBase( GprsBaseConfig configGprsBase )
        {
            dataGridViewGprsSetting.Rows[0].Cells[1].Value = configGprsBase.Mode;//"Режим"
            dataGridViewGprsSetting.Rows[1].Cells[1].Value = configGprsBase.ApnServer;//"Точка доступа GPRS";
            dataGridViewGprsSetting.Rows[2].Cells[1].Value = configGprsBase.ApnLogin;//"Логин для GPRS";
            dataGridViewGprsSetting.Rows[3].Cells[1].Value = configGprsBase.ApnPassword;//"Пароль для GPRS";
            dataGridViewGprsSetting.Rows[4].Cells[1].Value = configGprsBase.DnsServer;//"DNS конфигурация";
            dataGridViewGprsSetting.Rows[5].Cells[1].Value = configGprsBase.DialNumber;//"Номер дозвона";
            dataGridViewGprsSetting.Rows[6].Cells[1].Value = configGprsBase.GprsLogin;//"ISP логин";
            dataGridViewGprsSetting.Rows[7].Cells[1].Value = configGprsBase.GprsPassword;//"ISP пароль";
        }

        private void DataGridViewSetGprsEmail( GprsEmailConfig configGprsEmail )
        {
            dataGridViewServer.Rows[0].Cells[1].Value = configGprsEmail.Pop3Server;//"Сервер";
            dataGridViewServer.Rows[1].Cells[1].Value = configGprsEmail.Pop3Login;//"Логин";
            dataGridViewServer.Rows[2].Cells[1].Value = configGprsEmail.Pop3Password;//"Пароль";
        }

        private void DataGridViewSetEvent( EventConfig configEvent )
        {
            dataGridViewEvent.Rows[0].Cells["ColumnEventsValue"].Value = configEvent.MinSpeed;//"Минимальная скорость";
            dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_MIN_SPEED] & EventConfig.BIT_WRITE_LOG ) > 0;
            dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_MIN_SPEED] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_MIN_SPEED] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[1].Cells["ColumnEventsValue"].Value = configEvent.Timer1;//"Таймер 1";
            dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_TIMER1] & EventConfig.BIT_WRITE_LOG ) > 0;
            dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_TIMER1] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_TIMER1] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[2].Cells["ColumnEventsValue"].Value = configEvent.Timer2;//"Таймер 2";
            dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_TIMER2] & EventConfig.BIT_WRITE_LOG ) > 0;
            dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_TIMER2] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_TIMER2] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[3].Cells["ColumnEventsValue"].Value = configEvent.CourseBend;//"Отклонение курса";
            dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_COURSE] & EventConfig.BIT_WRITE_LOG ) > 0;
            dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_COURSE] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_COURSE] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[4].Cells["ColumnEventsValue"].Value = configEvent.Distance1;//"Дистанция 1";
            dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_DISTANCE1] & EventConfig.BIT_WRITE_LOG ) > 0;
            dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_DISTANCE1] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_DISTANCE1] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[5].Cells["ColumnEventsValue"].Value = configEvent.Distance2;//"Дистанция 2";
            dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_DISTANCE2] & EventConfig.BIT_WRITE_LOG ) > 0;
            dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_DISTANCE2] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_DISTANCE2] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[6].Cells["ColumnEventsValue"].Value = configEvent.SpeedChange;//"Ускорение";
            dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_ACCELERATION] & EventConfig.BIT_WRITE_LOG ) > 0;
            dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_ACCELERATION] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_ACCELERATION] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_POWER_ON] & EventConfig.BIT_WRITE_LOG ) > 0;//"Питание включено";
            dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_POWER_ON] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_POWER_ON] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_POWER_OFF] & EventConfig.BIT_WRITE_LOG ) > 0;//"Питание выключено";
            dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_POWER_OFF] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_POWER_OFF] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_GSM_FIND] & EventConfig.BIT_WRITE_LOG ) > 0;//"GSM появилось";
            dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_GSM_FIND] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_GSM_FIND] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_GSM_LOST] & EventConfig.BIT_WRITE_LOG ) > 0;//"GSM пропало";
            dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_GSM_LOST] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_GSM_LOST] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_LOG_FULL] & EventConfig.BIT_WRITE_LOG ) > 0;//"Лог заполнен";
            dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_LOG_FULL] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_LOG_FULL] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_SENSORS] & EventConfig.BIT_WRITE_LOG ) > 0;//"Датчики";
            dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_SENSORS] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_SENSORS] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_ON] & EventConfig.BIT_WRITE_LOG ) > 0;//"Включение";
            dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_ON] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_ON] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_REBOOT] & EventConfig.BIT_WRITE_LOG ) > 0;//"Сброс";
            dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_REBOOT] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_REBOOT] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_PHONE1] & EventConfig.BIT_WRITE_LOG ) > 0;//"Звонок с тел 1";
            dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_PHONE1] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_PHONE1] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_PHONE2] & EventConfig.BIT_WRITE_LOG ) > 0;//"Звонок с тел 2";
            dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_PHONE2] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_PHONE2] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_PHONE3] & EventConfig.BIT_WRITE_LOG ) > 0;//"Звонок с тел 3";
            dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_PHONE3] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_PHONE3] & EventConfig.BIT_SEND_SMS ) > 0;

            dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckWritePoint"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_DISPETCHER] & EventConfig.BIT_WRITE_LOG ) > 0;//"Звонок диспетчеру";
            dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckSendToServer"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_DISPETCHER] & EventConfig.BIT_SEND_TO_SERVER ) > 0;
            dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckSendSMS"].Value = ( configEvent.EventMask[EventConfig.INDEX_RING_DISPETCHER] & EventConfig.BIT_SEND_SMS ) > 0;
        }

        private void DataGridViewSetPhones( PhoneNumberConfig configPhone )
        {
            dataGridViewPhones.Rows[0].Cells[1].Value = configPhone.NumberSOS;// "Номер SOS";
            dataGridViewPhones.Rows[1].Cells[1].Value = configPhone.NumberDspt;//"Номер телефона диспетчера";
            dataGridViewPhones.Rows[2].Cells[1].Value = configPhone.NumberAccept1;//"Разрешенный номер 1";
            dataGridViewPhones.Rows[3].Cells[1].Value = configPhone.NumberAccept2;//"Разрешенный номер 2";
            dataGridViewPhones.Rows[4].Cells[1].Value = configPhone.NumberAccept3;//"Разрешенный номер 3";
            dataGridViewPhones.Rows[5].Cells[1].Value = "Not USE";//"Телефон обратной связи";
        }

        private GprsBaseConfig DataGridViewGetGprsBase()
        {
            GprsBaseConfig gprsBaseCfg = new GprsBaseConfig();
            gprsBaseCfg.ShortID = textBoxID_TT.Text;
            gprsBaseCfg.MessageID = ++cmdMsgCnt;
            gprsBaseCfg.Mode = Convert.ToUInt16( dataGridViewGprsSetting.Rows[0].Cells[1].Value );
            gprsBaseCfg.ApnServer = ( string )dataGridViewGprsSetting.Rows[1].Cells[1].Value;
            gprsBaseCfg.ApnLogin = ( string )dataGridViewGprsSetting.Rows[2].Cells[1].Value;
            gprsBaseCfg.ApnPassword = ( string )dataGridViewGprsSetting.Rows[3].Cells[1].Value;
            gprsBaseCfg.DnsServer = ( string )dataGridViewGprsSetting.Rows[4].Cells[1].Value;
            gprsBaseCfg.DialNumber = ( string )dataGridViewGprsSetting.Rows[5].Cells[1].Value;
            gprsBaseCfg.GprsLogin = ( string )dataGridViewGprsSetting.Rows[6].Cells[1].Value;
            gprsBaseCfg.GprsPassword = ( string )dataGridViewGprsSetting.Rows[7].Cells[1].Value;
            return gprsBaseCfg;
        }

        private GprsEmailConfig DataGridViewGetGprsEmail()
        {
            GprsEmailConfig gprsEmailCfg = new GprsEmailConfig();
            gprsEmailCfg.ShortID = textBoxID_TT.Text;
            gprsEmailCfg.MessageID = ++cmdMsgCnt;
            //gprsEmailCfg.SmtpServer = ;
            //gprsEmailCfg.SmtpPassword;
            //gprsEmailCfg.SmtpLogin;
            gprsEmailCfg.Pop3Server = ( string )dataGridViewServer.Rows[0].Cells[1].Value;
            gprsEmailCfg.Pop3Login = ( string )dataGridViewServer.Rows[1].Cells[1].Value;
            gprsEmailCfg.Pop3Password = ( string )dataGridViewServer.Rows[2].Cells[1].Value;
            return gprsEmailCfg;
        }

        private EventConfig DataGridViewGetEvent()
        {
            EventConfig eventCfg = new EventConfig();
            eventCfg.ShortID = textBoxID_TT.Text;
            eventCfg.MessageID = ++cmdMsgCnt;
            //Заполняем с DataView
            eventCfg.CourseBend = Convert.ToUInt16( dataGridViewEvent.Rows[3].Cells["ColumnEventsValue"].Value );
            eventCfg.Distance1 = Convert.ToUInt16( dataGridViewEvent.Rows[4].Cells["ColumnEventsValue"].Value );
            eventCfg.Distance2 = Convert.ToUInt16( dataGridViewEvent.Rows[5].Cells["ColumnEventsValue"].Value );
            eventCfg.MinSpeed = Convert.ToUInt16( dataGridViewEvent.Rows[0].Cells["ColumnEventsValue"].Value );
            eventCfg.SpeedChange = Convert.ToSByte( dataGridViewEvent.Rows[6].Cells["ColumnEventsValue"].Value );
            eventCfg.Timer1 = Convert.ToUInt16( dataGridViewEvent.Rows[1].Cells["ColumnEventsValue"].Value );
            eventCfg.Timer2 = Convert.ToUInt16( dataGridViewEvent.Rows[2].Cells["ColumnEventsValue"].Value );
            //"Минимальная скорость";
            if( ( bool )dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_MIN_SPEED] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_MIN_SPEED] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_MIN_SPEED] |= EventConfig.BIT_SEND_SMS;
            //"Таймер 1";
            if( ( bool )dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_TIMER1] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_TIMER1] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_TIMER1] |= EventConfig.BIT_SEND_SMS;
            //"Таймер 2";
            if( ( bool )dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_TIMER2] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_TIMER2] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_TIMER2] |= EventConfig.BIT_SEND_SMS;
            //"Отклонение курса";
            if( ( bool )dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_COURSE] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_COURSE] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_COURSE] |= EventConfig.BIT_SEND_SMS;
            //"Дистанция 1";
            if( ( bool )dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_DISTANCE1] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_DISTANCE1] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_DISTANCE1] |= EventConfig.BIT_SEND_SMS;
            //"Дистанция 2";
            if( ( bool )dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_DISTANCE2] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_DISTANCE2] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_DISTANCE2] |= EventConfig.BIT_SEND_SMS;
            //"Ускорение";
            if( ( bool )dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_ACCELERATION] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_ACCELERATION] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_ACCELERATION] |= EventConfig.BIT_SEND_SMS;
            //"Питание включено";
            if( ( bool )dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_POWER_ON] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_POWER_ON] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_POWER_ON] |= EventConfig.BIT_SEND_SMS;
            //"Питание выключено"
            if( ( bool )dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_POWER_OFF] |= EventConfig.BIT_WRITE_LOG; ;
            if( ( bool )dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_POWER_OFF] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_POWER_OFF] |= EventConfig.BIT_SEND_SMS;
            //"GSM появилось";
            if( ( bool )dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_GSM_FIND] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_GSM_FIND] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_GSM_FIND] |= EventConfig.BIT_SEND_SMS;
            //"GSM пропало";
            if( ( bool )dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_GSM_LOST] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_GSM_LOST] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_GSM_LOST] |= EventConfig.BIT_SEND_SMS;
            //"Лог заполнен"
            if( ( bool )dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_LOG_FULL] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_LOG_FULL] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_LOG_FULL] |= EventConfig.BIT_SEND_SMS;
            //"Датчики";
            if( ( bool )dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_SENSORS] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_SENSORS] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_SENSORS] |= EventConfig.BIT_SEND_SMS;
            //"Включение";
            if( ( bool )dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_ON] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_ON] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_ON] |= EventConfig.BIT_SEND_SMS;
            //"Сброс";
            if( ( bool )dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_REBOOT] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_REBOOT] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_REBOOT] |= EventConfig.BIT_SEND_SMS;
            //"Звонок с тел 1";
            if( ( bool )dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_PHONE1] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_PHONE1] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_PHONE1] |= EventConfig.BIT_SEND_SMS;
            //"Звонок с тел 2";
            if( ( bool )dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_PHONE2] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_PHONE2] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_PHONE2] |= EventConfig.BIT_SEND_SMS;
            //"Звонок с тел 3";
            if( ( bool )dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_PHONE3] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_PHONE3] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_PHONE3] |= EventConfig.BIT_SEND_SMS;
            //"Звонок диспетчеру";
            if( ( bool )dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckWritePoint"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_DISPETCHER] |= EventConfig.BIT_WRITE_LOG;
            if( ( bool )dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckSendToServer"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_DISPETCHER] |= EventConfig.BIT_SEND_TO_SERVER;
            if( ( bool )dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckSendSMS"].Value == true )
                eventCfg.EventMask[EventConfig.INDEX_RING_DISPETCHER] |= EventConfig.BIT_SEND_SMS;
            return eventCfg;
        }

        private PhoneNumberConfig DataGridViewGetPhones()
        {
            PhoneNumberConfig phoneNumCfg = new PhoneNumberConfig();
            phoneNumCfg.ShortID = textBoxID_TT.Text;
            phoneNumCfg.MessageID = ++cmdMsgCnt;
            phoneNumCfg.NumberSOS = ( string )dataGridViewPhones.Rows[0].Cells[1].Value;
            //phoneNumCfg.NumberMask = ;
            phoneNumCfg.NumberDspt = ( string )dataGridViewPhones.Rows[1].Cells[1].Value;
            phoneNumCfg.NumberAccept3 = ( string )dataGridViewPhones.Rows[4].Cells[1].Value;
            phoneNumCfg.NumberAccept2 = ( string )dataGridViewPhones.Rows[3].Cells[1].Value;
            phoneNumCfg.NumberAccept1 = ( string )dataGridViewPhones.Rows[2].Cells[1].Value;
            //dataGridViewPhones.Rows[5].Cells[1].Value = "Not USE";//"Телефон обратной связи";
            return phoneNumCfg;
        }
        #endregion//DAtaGridUse

        private void buttonSetFromFile_Click( object sender, EventArgs e )
        {
            try
            {
                if( structureSaveSett != null )
                {
                    DataGridViewSetGprsBase( structureSaveSett.baseConfigSetting );
                    DataGridViewSetGprsEmail( structureSaveSett.emailConfigSetting );
                    DataGridViewSetEvent( structureSaveSett.eventConfigSetting );
                    DataGridViewSetPhones( structureSaveSett.phoneConfigSetting );
                }
            }
            catch( Exception ex )
            {
                MessageBox.Show( ex.Message, "error set data from file to form" );
            }
        }

        private void buttonAddGsmOperator_Click( object sender, EventArgs e )
        {
            StructureSaveOperator saveGsmSetting = new StructureSaveOperator();
            GprsBaseConfig tmpBaseConf = DataGridViewGetGprsBase();
            saveGsmSetting.AccessPointName = tmpBaseConf.ApnServer;
            saveGsmSetting.ApnLogin = tmpBaseConf.ApnLogin;
            saveGsmSetting.ApnPass = tmpBaseConf.ApnPassword;
            saveGsmSetting.dnsServ = tmpBaseConf.DnsServer;

            gsmGprsSetting.AddNewOperator( saveGsmSetting );

            LoadComboBoxGsmOperators();
        }

        private void LoadComboBoxGsmOperators()
        {
            comboBoxGsmOperator.Items.Clear();
            string[] gsmEnum = gsmGprsSetting.GetEnumOperators();
            if( gsmEnum != null && gsmEnum.Length > 0 )
            {
                comboBoxGsmOperator.Items.AddRange( gsmEnum );
                comboBoxGsmOperator.SelectedIndex = comboBoxGsmOperator.Items.Count - 1;
            }
        }

        private void LoadMobitelsInfo( DbDataManager dbDataMngr )
        {
            dbDataMngr.dictMobitelID.Clear();
            dataGridViewTeletracks.Rows.Clear();
            if( dbDataMngr.FillMobitelCfg() == ExecutionStatus.OK )
            {
                DescriptionMobID[] descrMobitel = null;
                if( dbDataMngr.LoadMobitelDescriptions( out descrMobitel ) == ExecutionStatus.OK )
                {
                    foreach( DescriptionMobID descr in descrMobitel )
                    {
                        if( dbDataMngr.dictMobitelID.ContainsValue( descr.mobitelID ) )
                        {
                            foreach( KeyValuePair<string, int> kvpMobitels in dbDataMngr.dictMobitelID )
                            {
                                if( kvpMobitels.Value == descr.mobitelID )
                                {
                                    string shortID = kvpMobitels.Key;
                                    dataGridViewTeletracks.Rows.Add( descr.name, descr.descr, shortID );
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                    MessageBox.Show( "Error load teletracks" );
            }
        }

        private void buttonApplyGsmOperator_Click( object sender, EventArgs e )
        {
            if( comboBoxGsmOperator.Items.Count > 0 )
            {
                GprsBaseConfig tmpBaseConf = new GprsBaseConfig();
                StructureSaveOperator saveGsmSetting = gsmGprsSetting.GetSetting( comboBoxGsmOperator.Items[comboBoxGsmOperator.SelectedIndex].ToString() );

                tmpBaseConf.ApnServer = saveGsmSetting.AccessPointName;
                tmpBaseConf.ApnLogin = saveGsmSetting.ApnLogin;
                tmpBaseConf.ApnPassword = saveGsmSetting.ApnPass;
                tmpBaseConf.DnsServer = saveGsmSetting.dnsServ;

                DataGridViewSetGprsBase( tmpBaseConf );
            }
        }

        private void buttonAddNewTt_Click( object sender, EventArgs e )
        {
            AddNewTeletrack formAdd = new AddNewTeletrack();
            formAdd.ShowDialog();

            if( formAdd.shortId != "" )
            {
                if( dbDataMngr != null )
                {
                    if( dbDataMngr.GetMobitelId( formAdd.shortId ) > 0 )
                    {
                        MessageBox.Show( "such a shortID is already in the database" );
                    }
                    else
                    {
                        //Add new TT to database
                        if( dbDataMngr.AddNewMobitel( formAdd.shortId, formAdd.name, formAdd.description ) == ExecutionStatus.OK )
                        {
                            LoadMobitelsInfo( dbDataMngr );
                        }
                        else
                            MessageBox.Show( "Create new Teletrack FAILED" );
                    }
                }
                else
                    MessageBox.Show( "Database not connected" );
            }
        }

        private void buttonEditTt_Click( object sender, EventArgs e )
        {
            AddNewTeletrack formAdd = new AddNewTeletrack( "Edit Teletrack Info" );
            formAdd.name = ( string )dataGridViewTeletracks.CurrentRow.Cells["ColumnNameTT"].Value;
            formAdd.description = ( string )dataGridViewTeletracks.CurrentRow.Cells["ColumnDescriptionTT"].Value;
            formAdd.shortId = textBoxID_TT.Text;
            if( formAdd.ShowDialog() == DialogResult.OK )
            {
                if( formAdd.shortId != "" )
                {
                    if( dbDataMngr != null )
                    {
                        if( dbDataMngr.UpdateMobitel( formAdd.shortId, formAdd.name, formAdd.description ) == ExecutionStatus.OK )
                        {
                            LoadMobitelsInfo( dbDataMngr );
                        }
                        else
                            MessageBox.Show( "Update Teletrack info FAILED" );
                    }
                    else
                        MessageBox.Show( "Database not connected" );
                }
                else
                    MessageBox.Show( "Error ShortID" );
            }
        }

        private void buttonAddNewPhone_Click( object sender, EventArgs e )
        {
            AddNewPhone newPhoneAdd = new AddNewPhone();
            newPhoneAdd.ShowDialog();
            if( newPhoneAdd.getName != "" && newPhoneAdd.getPhone != "" )
            {
                if( !dicPhones.ContainsKey( newPhoneAdd.getPhone ) )
                {
                    AddNewPhoneInDb( newPhoneAdd.getName, newPhoneAdd.getPhone );
                }
                else
                    MessageBox.Show( "Number was already in the database", "attention" );
            }
        }

        private int AddNewPhoneInDb( string name, string phone )
        {
            int addID = -1;
            if( dbDataMngr.AddNewPhone( name, phone, ref addID ) == ExecutionStatus.OK )
            {
                dicPhones.Add( phone, name );
                dataGVPhonesInDataBase.Rows.Add( new object[] { name, phone } );
            }
            return addID;
        }

        char[] passKode = { 'p', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 's', 'n', 'v', 'f', 'r', 'g' };//0123456789ABCDEF
        private void buttonGeneratePass_Click( object sender, EventArgs e )
        {
            if( textBoxID_TT.Text != "" )
            {
                string textID = textBoxID_TT.Text;
                dataGridViewServer.Rows[1].Cells[1].Value = textID + "@"; //Login
                char[] charID = textID.ToCharArray();
                string pass = "";
                for( int i = 0; i < charID.Length && i < 4; i++ )
                {
                    //char tmpChar = 0;
                    int num = Convert.ToInt32( charID[i].ToString(), 16 );
                    if( num < passKode.Length )
                    {
                        if( i == 0 || i == 3 )
                            pass += ( char )( passKode[num] + ( 'A' - 'a' ) );
                        else
                            pass += passKode[num];
                    }
                }
                pass += textID;
                dataGridViewServer.Rows[2].Cells[1].Value = pass; //Password
            }
            else
                MessageBox.Show( "Need Enter TT_ID" );
        }

        private void buttonSetTtPhone_Click( object sender, EventArgs e )
        {
            if( textBoxTtPhone.Text != "" )
            {
                int phoneIndex = -1;
                if( !dicPhones.ContainsKey( textBoxTtPhone.Text ) )
                {
                    phoneIndex = AddNewPhoneInDb( textBoxID_TT.Text, textBoxTtPhone.Text );
                }
                if( dbDataMngr.SavePhoneTt( textBoxID_TT.Text, textBoxTtPhone.Text ) == ExecutionStatus.OK )
                    MessageBox.Show( "Set OK" );
                else
                    MessageBox.Show( "Set ERROR" );
            }
        }

        private void buttonSaveServiceSetting_Click( object sender, EventArgs e )
        {
            string serviceIP = ( string )dataGVServiceSetting.Rows[0].Cells[1].Value;
            string servicePort = ( string )dataGVServiceSetting.Rows[1].Cells[1].Value;
            string serviceLogin = ( string )dataGVServiceSetting.Rows[2].Cells[1].Value;
            string servicePassword = ( string )dataGVServiceSetting.Rows[3].Cells[1].Value;
            if( dbDataMngr.SaveServiceSetting( serviceIP, servicePort, serviceLogin, servicePassword ) == ExecutionStatus.OK )
                MessageBox.Show( "Save OK" );
            else
                MessageBox.Show( "Save ERROR" );
        }

        private void buttonDeleteTt_Click( object sender, EventArgs e )
        {
            DeleteTeletrack formDel = new DeleteTeletrack( textBoxID_TT.Text );
            formDel.ShowDialog();

            if( formDel.DialogResult == DialogResult.OK )
            {
                if( dbDataMngr == null )
                {
                    MessageBox.Show( "Database not connected" );
                }
                else
                {
                    int mobitel_ID = dbDataMngr.GetMobitelId( textBoxID_TT.Text );
                    if( mobitel_ID <= 0 )
                    {
                        MessageBox.Show( "such a shortID is already delete in the database" );
                    }
                    else
                    {
                        //Add new TT to database
                        if( dbDataMngr.DeleteMobitel( mobitel_ID ) == ExecutionStatus.OK )
                        {
                            LoadMobitelsInfo( dbDataMngr );
                        }
                        else
                            MessageBox.Show( "Delete Teletrack FAILED" );
                    }
                }
            }
        }

        private void buttonCopyPhone_Click( object sender, EventArgs e )
        {
            if( dataGVPhonesInDataBase.CurrentRow.Index >= 0 )//.SelectedRows.Count == 1 && dataGVPhonesInDataBase.SelectedRows[0].Cells["ColumnNumber"] != dataGVPhonesInDataBase[1,0].Value)
            {
                if( dataGridViewPhones.CurrentRow.Index >= 0 )
                {
                    string dbPhone = ( string )dataGVPhonesInDataBase.CurrentRow.Cells["ColumnNumber"].Value;
                    dataGridViewPhones.CurrentRow.Cells["ColumnPhoneValue"].Value = dbPhone;
                }
                else
                    MessageBox.Show( "Need select setting phone" );
            }
            else
                MessageBox.Show( "Need select one Phone" );
        }

    }

    /// <summary>
    /// Клас с параметрами сообщения, для передачи: строка, цикличность, таймер.
    /// </summary>
    public class TxPacket
    {
        public string txString = "";
        public bool isCyclic = false;
        public int timeOutSet = 0;
        public int timeOutCurrent = 0;
    }

    public struct DescriptionMobID
    {
        public string name;
        public string descr;
        public int mobitelID;
    }
}
