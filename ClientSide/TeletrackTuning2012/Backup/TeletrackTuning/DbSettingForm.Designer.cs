﻿namespace TeletrackTuning
{
  partial class DbSettingForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.textBoxServer = new System.Windows.Forms.TextBox();
      this.textBoxDataBase = new System.Windows.Forms.TextBox();
      this.textBoxUser = new System.Windows.Forms.TextBox();
      this.textBoxPassword = new System.Windows.Forms.TextBox();
      this.buttonApply = new System.Windows.Forms.Button();
      this.buttonCansel = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(23, 28);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(41, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Server:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(23, 54);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(57, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "DataBase:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(23, 80);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(32, 13);
      this.label3.TabIndex = 0;
      this.label3.Text = "User:";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(23, 108);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(56, 13);
      this.label4.TabIndex = 0;
      this.label4.Text = "Password:";
      // 
      // textBoxServer
      // 
      this.textBoxServer.Location = new System.Drawing.Point(105, 25);
      this.textBoxServer.Name = "textBoxServer";
      this.textBoxServer.Size = new System.Drawing.Size(100, 20);
      this.textBoxServer.TabIndex = 1;
      // 
      // textBoxDataBase
      // 
      this.textBoxDataBase.Location = new System.Drawing.Point(105, 51);
      this.textBoxDataBase.Name = "textBoxDataBase";
      this.textBoxDataBase.Size = new System.Drawing.Size(100, 20);
      this.textBoxDataBase.TabIndex = 1;
      // 
      // textBoxUser
      // 
      this.textBoxUser.Location = new System.Drawing.Point(105, 77);
      this.textBoxUser.Name = "textBoxUser";
      this.textBoxUser.Size = new System.Drawing.Size(100, 20);
      this.textBoxUser.TabIndex = 1;
      // 
      // textBoxPassword
      // 
      this.textBoxPassword.Location = new System.Drawing.Point(105, 105);
      this.textBoxPassword.Name = "textBoxPassword";
      this.textBoxPassword.Size = new System.Drawing.Size(100, 20);
      this.textBoxPassword.TabIndex = 1;
      // 
      // buttonApply
      // 
      this.buttonApply.Location = new System.Drawing.Point(225, 37);
      this.buttonApply.Name = "buttonApply";
      this.buttonApply.Size = new System.Drawing.Size(75, 23);
      this.buttonApply.TabIndex = 2;
      this.buttonApply.Text = "Apply";
      this.buttonApply.UseVisualStyleBackColor = true;
      this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
      // 
      // buttonCansel
      // 
      this.buttonCansel.Location = new System.Drawing.Point(225, 89);
      this.buttonCansel.Name = "buttonCansel";
      this.buttonCansel.Size = new System.Drawing.Size(75, 23);
      this.buttonCansel.TabIndex = 2;
      this.buttonCansel.Text = "Cansel";
      this.buttonCansel.UseVisualStyleBackColor = true;
      this.buttonCansel.Click += new System.EventHandler(this.buttonCansel_Click);
      // 
      // DbSettingForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(314, 149);
      this.Controls.Add(this.buttonCansel);
      this.Controls.Add(this.buttonApply);
      this.Controls.Add(this.textBoxPassword);
      this.Controls.Add(this.textBoxUser);
      this.Controls.Add(this.textBoxDataBase);
      this.Controls.Add(this.textBoxServer);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "DbSettingForm";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.Text = "DbSettingForm";
      this.TopMost = true;
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox textBoxServer;
    private System.Windows.Forms.TextBox textBoxDataBase;
    private System.Windows.Forms.TextBox textBoxUser;
    private System.Windows.Forms.TextBox textBoxPassword;
    private System.Windows.Forms.Button buttonApply;
    private System.Windows.Forms.Button buttonCansel;
  }
}