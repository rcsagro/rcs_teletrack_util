﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using TeletrackTuning.Error;
using TeletrackTuning.SrvDBCommand;
using TeletrackTuning.TDBCommand;
using TeletrackTuning.Cache;
using TeletrackTuning.SrvDBCommand.Factory;
using TeletrackTuning.Entity;
using TeletrackTuning;

namespace TeletrackTuning
{
  class DbDataManager
  {
    public Dictionary<string, int> dictMobitelID = new Dictionary<string, int>();
    
    protected readonly ICommandCache _commandCache;

    public DbDataManager(string connectStr)
    {
      _commandCache = new CommandCache(new DbCommandFactory(connectStr));
    }

    /// <summary>
    /// Заполнение локального кэша настроек телетреков.
    /// </summary>
    /// <returns>FetchDataStatus</returns>
    public ExecutionStatus FillMobitelCfg()
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<TListMobitelCfgDBCommand>();
      dictMobitelID.Clear();
      ExecutionStatus result = ExecutionStatus.OK;
      try
      {
        using (MySqlDataReader reader = command.ExecuteReader(out result))
        {
          if ((result == ExecutionStatus.OK) && (reader != null))
          {
            while (reader.Read())
            {
              string shorId = reader.GetString("DevIdShort");
              int mobID = reader.GetInt32("MobitelID");
              Int64 lastPackID = reader.GetInt64("LastPacketID");

              if (dictMobitelID.ContainsKey(shorId))
                dictMobitelID.Remove(shorId);
              dictMobitelID.Add(shorId, mobID);
            }
          }
          else
          {}//not mobitels 
        }
      }
      catch(Exception ex)
      {
      //  //result = ExecutionStatus.ERROR_DB;
        if (ex.Message.Contains("Access") || ex.Message.Contains("Unknown") || ex.Message.Contains("Unable to connect"))
          throw ex;
      }
      finally
      {
        command.CloseConnection();
      }
      return result;
    }

    public ExecutionStatus LoadMobitelDescriptions(out DescriptionMobID[] descrMobID)
    {
      List<DescriptionMobID> listDescr = new List<DescriptionMobID>();
      ISrvDbCommand command = _commandCache.GetDbCommand<SelectMobitelsDescription>();
      ExecutionStatus result = ExecutionStatus.ERROR_DATA;
      descrMobID = null;
      if (dictMobitelID != null && dictMobitelID.Count > 0)
      {
        try
        {
          using (MySqlDataReader reader = command.ExecuteReader(out result))
          {
            if ((result == ExecutionStatus.OK) && (reader != null))
            {
              while (reader.Read())
              {
                string strName = reader.GetString("Name");
                string strDescr = reader.GetString("Descr");
                int mobID = reader.GetInt32("Mobitel_ID");
                listDescr.Add(new DescriptionMobID() { name = strName, descr = strDescr, mobitelID = mobID });
              }
            }
          }
        }
        catch
        {
          result = ExecutionStatus.ERROR_DB;
        }
        finally
        {
          command.CloseConnection();
          if (listDescr.Count > 0)
            descrMobID = listDescr.ToArray();
        }
      }
      return result;
    }

    /// <summary>
    /// Сохранение сообщения принятого от телетрека в таблицах source и messages
    /// </summary>
    /// <param name="source">Исходный текст сообщения</param>
    /// <param name="commandId">Идентификатор команды</param>
    /// <param name="mobitelId">Mobitel_ID</param>
    /// <param name="messageCounter">Порядковый номер обрабатываемой команды для данного телетрека</param>
    /// <param name="messageId">MessageID для дальнейшей работы</param>
    /// <returns>FetchDataStatus</returns>
    internal ExecutionStatus SaveInMessages(string source, int commandId,
      int mobitelId, int messageCounter, out int messageId)
    {
      ExecutionStatus result = ExecutionStatus.OK;
      int sourceId = 0;
      messageId = -1;

      // 1. Вначале сохраним в сырцах кроме 46 и 49 &&Ping
      if ((commandId != (int)CommandDescriptor.DataGpsAnswer) &&
        (commandId != (int)CommandDescriptor.DataGpsAuto) && commandId != (int)CommandDescriptor.Ping)
      {

        ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertSource>();
        command.Init(new object[] { source });
        result = command.Execute();
        if (result == ExecutionStatus.OK)
        {
          sourceId = ((A1InsertSource)command).NewSourceId;
        }
        // 2. Теперь можно и в messages заинсертить
        if (result == ExecutionStatus.OK)
        {
          ISrvDbCommand command2 = _commandCache.GetDbCommand<A1InsertMessages>();
          /// Порядок передачи параметров:
          /// 1. Time1 
          /// 2. Command_ID
          /// 3. Source_ID
          /// 4. Address(email)
          /// 5. Mobitel_ID
          /// 6. MessageCounter
          /// 7. Direction
          command2.Init(new object[] { Util.ToUnixTime(DateTime.UtcNow), 
                    commandId, sourceId, "", mobitelId, messageCounter, 0 });

          result = command2.Execute();
          if (result == ExecutionStatus.OK)
          {
            messageId = ((A1InsertMessages)command2).NewMessageId;
          }
        }
      }
      return result;
    }

    /// <summary>
    /// Сохранение настроек GPRS Email;
    /// </summary>
    /// <param name="messageId">Message id</param>
    /// <param name="config">GprsEmailConfig</param>
    /// <returns>Fetch data status</returns>
    internal ExecutionStatus SaveConfigGprsEmail(int messageId, GprsEmailConfig config)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigGprsEmail>();
      // Порядок передачи параметров:
      // 1. Message_ID
      // 2. MobitelId (связь с ConfigMain поле ConfigGprsEmail_ID)
      // 3. GprsEmailConfig
      command.Init(new object[] { messageId, config.MobitelID, config });
      return command.Execute();
    }

    /// <summary>
    /// Запрос настроек для данного мобитела
    /// </summary>
    /// <param name="mobitelId"></param>
    /// <param name="config"></param>
    /// <returns></returns>
    internal ExecutionStatus GetConfigGprsEmail(int mobitelId, ref GprsEmailConfig config)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigGprsEmail>();
      ExecutionStatus result;
      try
      {
        command.Init(new object[] { mobitelId });
        using (MySqlDataReader reader = command.ExecuteReader(out result))
        {
          if ((result == ExecutionStatus.OK) && (reader != null) && reader.Read())
          {
            config.Pop3Login = reader.GetString("Pop3un");
            config.Pop3Password = reader.GetString("Pop3pw");
            config.Pop3Server = reader.GetString("Pop3serv");
            config.SmtpLogin = reader.GetString("Smtpun");
            config.SmtpPassword = reader.GetString("Smtppw");
            config.SmtpServer = reader.GetString("Smtpserv");
          }
        }
      }
      catch
      {
        result = ExecutionStatus.ERROR_DB;
      }
      finally
      {
        command.CloseConnection();
      }
      return result;
    }

    /// <summary>
    /// Сохранение основных настроек GPRS 
    /// </summary>
    /// <param name="messageId">Message id</param>
    /// <param name="config">GprsBaseConfig</param>
    /// <returns>Fetch data status</returns>
    internal ExecutionStatus SaveConfigGprsBase(int messageId, GprsBaseConfig config)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigGprsBase>();
      // Порядок передачи параметров:
      // 1. Message_ID
      // 2. MobitelId (связь с ConfigMain поле ConfigGprsMain_ID)
      // 3. GprsBaseConfig
      command.Init(new object[] { messageId, config.MobitelID, config });
      return command.Execute();
    }

    /// <summary>
    /// Запрос настроек для данного мобитела
    /// </summary>
    /// <param name="mobitelId"></param>
    /// <param name="config"></param>
    /// <returns></returns>
    internal ExecutionStatus GetConfigGprsBase(int mobitelId, ref GprsBaseConfig config)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigGprsBase>();
      ExecutionStatus result;
      try
      {
        command.Init(new object[] { mobitelId });
        using (MySqlDataReader reader = command.ExecuteReader(out result))
        {
          if ((result == ExecutionStatus.OK) && (reader != null) && reader.Read())
          {
            config.Mode = reader.GetUInt16("Mode");
            config.ApnServer = reader.GetString("Apnserv");
            config.ApnLogin = reader.GetString("Apnun");
            config.ApnPassword = reader.GetString("Apnpw");
            config.DnsServer = reader.GetString("Dnsserv1");
            config.DialNumber = reader.GetString("Dialn1");
            config.GprsLogin = reader.GetString("Ispun");
            config.GprsPassword = reader.GetString("Isppw");
          }
        }
      }
      catch
      {
        result = ExecutionStatus.ERROR_DB;
      }
      finally
      {
        command.CloseConnection();
      }
      return result;
    }

    /// <summary>
    /// Сохранение настроек событий
    /// </summary>
    /// <param name="messageId">Message id</param>
    /// <param name="config">EventConfig</param>
    /// <returns>Fetch data status</returns>
    internal ExecutionStatus SaveConfigEvents(int messageId, EventConfig config)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigEvent>();
      // Порядок передачи параметров:
      // 1. Message_ID
      // 2. MobitelId (связь с ConfigMain поле ConfigEvent_ID)
      // 3. EventConfig
      command.Init(new object[] { messageId, config.MobitelID, config });
      return command.Execute();
    }

    /// <summary>
    /// Запрос настроек для данного мобитела
    /// </summary>
    /// <param name="mobitelId"></param>
    /// <param name="config"></param>
    /// <returns></returns>
    internal ExecutionStatus GetConfigEvents(int mobitelId, ref EventConfig config)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigEvent>();
      ExecutionStatus result;
      try
      {
        command.Init(new object[] { mobitelId });
        using (MySqlDataReader reader = command.ExecuteReader(out result))
        {
          if ((result == ExecutionStatus.OK) && (reader != null) && reader.Read())
          {
            config.CourseBend = reader.GetUInt16("dist1Log");//Вот такое анархоичное несоотведствие
            config.Distance1 = reader.GetUInt16("dist2Send");
            config.Distance2 = reader.GetUInt16("dist3Zone");
            config.MinSpeed = reader.GetUInt16("tmr1Log");
            config.SpeedChange = (sbyte)reader.GetInt32("deltaTimeZone");
            config.Timer1 = reader.GetUInt16("tmr2Send");
            config.Timer2 = reader.GetUInt16("tmr3Zone");;

            for (int i = 1; i < 32; i++)
              config.EventMask[i-1] = reader.GetUInt16("maskEvent" + Convert.ToString(i));

          }
        }
      }
      catch 
      {
        result = ExecutionStatus.ERROR_DB;
      }
      finally
      {
        command.CloseConnection();
      }
      return result;
    }

    /// <summary>
    /// Сохранение конфигурации телефонных номеров 
    /// </summary>
    /// <param name="messageId">Messageid</param>
    /// <param name="config">PhoneNumberConfig</param>
    /// <returns>Fetch data status</returns>
    internal ExecutionStatus SaveConfigPhoneNumbers(int messageId, PhoneNumberConfig config)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertConfigPhone>();
      // Порядок передачи параметров:
      // 1. Message_ID
      // 2. MobitelId (связь с ConfigMain поле ConfigTel_ID)
      // 3. PhoneNumberConfig
      command.Init(new object[] { messageId, config.MobitelID, config });
      return command.Execute();
    }

    /// <summary>
    /// Запрос настроек для данного мобитела
    /// </summary>
    /// <param name="mobitelId"></param>
    /// <param name="config"></param>
    /// <returns></returns>
    internal ExecutionStatus GetConfigPhoneNumbers(int mobitelId, ref PhoneNumberConfig config)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<A1SelectConfigPhone>();
      ExecutionStatus result;
      try
      {
        command.Init(new object[] { mobitelId });
        using (MySqlDataReader reader = command.ExecuteReader(out result))
        {
          if ((result == ExecutionStatus.OK) && (reader != null) && reader.Read())
          {
            config.Name1 = reader.GetString("Name1");
            config.Name2 = reader.GetString("Name2");
            config.Name3 = reader.GetString("Name3");
            config.NumberSOS = reader.GetString("NumberSOS");
            config.NumberDspt = reader.GetString("NumberDspt");
            config.NumberAccept1 = reader.GetString("NumberAccept1");
            config.NumberAccept2 = reader.GetString("NumberAccept2");
            config.NumberAccept3 = reader.GetString("NumberAccept3");
          }
        }
      }
      catch
      {
        result = ExecutionStatus.ERROR_DB;
      }
      finally
      {
        command.CloseConnection();
      }
      return result;
    }

    internal ExecutionStatus SaveDataGpsQuery(int messageId, DataGpsQuery query)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertDataGpsQuery>();
      // Порядок передачи параметров:
      /// 1. Message_ID
      /// 2. MobitelId 
      /// 3. Description
      /// 4. LastRecords
      command.Init(new object[] { messageId, query.MobitelID, "TunerQuery", (int)query.LastRecords });
      return command.Execute();
    }

    /// <summary>
    /// Сохранение сообщения для отправки телетрека в таблицах source и messages
    /// </summary>
    /// <param name="source">Исходный текст сообщения</param>
    /// <param name="commandId">Идентификатор команды</param>
    /// <param name="mobitelId">Mobitel_ID</param>
    /// <param name="messageCounter">Порядковый номер обрабатываемой команды для данного телетрека</param>
    /// <param name="messageId">MessageID для дальнейшей работы</param>
    /// <returns>FetchDataStatus</returns>
    internal ExecutionStatus SaveA1MessagesForOnline(string source, int commandId,
      int mobitelId, int messageCounter, out int messageId)
    {
      ExecutionStatus result = ExecutionStatus.OK;
      int sourceId = 0;
      messageId = -1;

      // 1. Вначале сохраним в сырцах кроме
      ISrvDbCommand command = _commandCache.GetDbCommand<A1InsertSource>();
      command.Init(new object[] { source });
      result = command.Execute();
      if (result == ExecutionStatus.OK)
      {// 2. Теперь можно и в messages заинсертить
        sourceId = 0;// ((A1InsertSource)command).NewSourceId;

        ISrvDbCommand command2 = _commandCache.GetDbCommand<A1InsertMessages>();
        /// Порядок передачи параметров:
        /// 1. Time1 
        /// 2. Command_ID
        /// 3. Source_ID
        /// 4. Address(email)
        /// 5. Mobitel_ID
        /// 6. MessageCounter
        /// 7. Direction
        command2.Init(new object[] { Util.ToUnixTime(DateTime.UtcNow), 
          commandId, sourceId, "", mobitelId, messageCounter, 1});

        result = command2.Execute();
        if (result == ExecutionStatus.OK)
        {
          messageId = ((A1InsertMessages)command2).NewMessageId;
        }
      }
      return result;
    }

    internal ExecutionStatus AddNewMobitel(string short_id, string name, string description)
    {
      ExecutionStatus result = ExecutionStatus.OK;
      ISrvDbCommand commandInsertIMC = _commandCache.GetDbCommand<InsertNewInterMobitelConfig>();
      /// Порядок передачи параметров:
      /// 1. Message_ID
      /// 2. MobitelId (связь с ConfigMain поле ConfigTel_ID)
      /// 3. IdConfig
      commandInsertIMC.Init(new object[] { short_id });
      result = commandInsertIMC.Execute();
      if (result == ExecutionStatus.OK)
      {//Insert in Mobitels
        int internalMobConfID = ((InsertNewInterMobitelConfig)commandInsertIMC).NewId;

        ISrvDbCommand commandInsertServiceSend = _commandCache.GetDbCommand<InsertNewServiceSend>();
        /// Порядок передачи параметров:
        /// 1. name
        /// 2. description
        commandInsertServiceSend.Init(new object[] { name, description });
        result = commandInsertServiceSend.Execute();
        if (result == ExecutionStatus.OK)
        {
          int servideSendID = ((InsertNewServiceSend)commandInsertServiceSend).NewId;

          ISrvDbCommand commandInsertMobitel = _commandCache.GetDbCommand<InsertNewMobitelId>();
          /// Порядок передачи параметров:
          /// 1. internalMobitelConfig_ID
          /// 2. Name
          /// 3. Description
          /// 4. serviceSend_ID
          commandInsertMobitel.Init(new object[] { internalMobConfID, name, description, servideSendID });
          result = commandInsertMobitel.Execute();
          if (result == ExecutionStatus.OK)
          {
            int mobitelID = ((InsertNewMobitelId)commandInsertMobitel).NewMobitelId;

            int configeventID = 0;
            int configtelID = 0;
            int configsmsID = 0;
            int configgprsmainID = 0;
            int configgprsemailID = 0;

            #region configIdInit
            ISrvDbCommand commandInsertConfEvent = _commandCache.GetDbCommand<InsertNewConfigEvent>();
            /// Порядок передачи параметров:
            /// 1. Name
            /// 2. Descr
            commandInsertConfEvent.Init(new object[] { name, description });
            result = commandInsertConfEvent.Execute();
            if (result != ExecutionStatus.OK)
              return result;
            configeventID = ((InsertNewConfigEvent)commandInsertConfEvent).NewId;

            ISrvDbCommand commandInsertConfTel = _commandCache.GetDbCommand<InsertNewConfigPhone>();
            /// Порядок передачи параметров:
            /// 1. Name
            /// 2. Descr
            commandInsertConfTel.Init(new object[] { name, description });
            result = commandInsertConfTel.Execute();
            if (result != ExecutionStatus.OK)
              return result;
            configtelID = ((InsertNewConfigPhone)commandInsertConfTel).NewId;

            ISrvDbCommand commandInsertConfSms = _commandCache.GetDbCommand<InsertNewConfigSms>();
            /// Порядок передачи параметров:
            /// 1. Name
            /// 2. Descr
            commandInsertConfSms.Init(new object[] { name, description });
            result = commandInsertConfSms.Execute();
            if (result != ExecutionStatus.OK)
              return result;
            configsmsID = ((InsertNewConfigSms)commandInsertConfSms).NewId;

            ISrvDbCommand commandInsertConfGprsMain = _commandCache.GetDbCommand<InsertNewConfigGprsMain>();
            /// Порядок передачи параметров:
            /// 1. Name
            /// 2. Descr
            commandInsertConfGprsMain.Init(new object[] { name, description });
            result = commandInsertConfGprsMain.Execute();
            if (result != ExecutionStatus.OK)
              return result;
            configgprsmainID = ((InsertNewConfigGprsMain)commandInsertConfGprsMain).NewId;

            ISrvDbCommand commandInsertConfGprsEmail = _commandCache.GetDbCommand<InsertNewConfigGprsEmail>();
            /// Порядок передачи параметров:
            /// 1. Name
            /// 2. Descr
            commandInsertConfGprsEmail.Init(new object[] { name, description });
            result = commandInsertConfGprsEmail.Execute();
            if (result != ExecutionStatus.OK)
              return result;
            configgprsemailID = ((InsertNewConfigGprsEmail)commandInsertConfGprsEmail).NewId;
            #endregion //configIdInit

            //create configmain
            ISrvDbCommand commandInsercConfMain = _commandCache.GetDbCommand<InsertNewConfigMain>();
            /// Порядок передачи параметров:
            /// 1. Name
            /// 2. Descr
            /// 3. Mobitel_ID
            /// 4. ConfigEvent_ID
            /// 5. ConfigTel_ID
            /// 6. ConfigSms_ID
            /// 7. ConfigGprsMain_ID
            /// 8. ConfigGprsEmail_ID
            commandInsercConfMain.Init(new object[] { "mobitel", "mca4", mobitelID, configeventID, configtelID, configsmsID, configgprsmainID, configgprsemailID });
            result = commandInsercConfMain.Execute();
            if (result == ExecutionStatus.OK)
            {
            }
          }
        }
      }
      return result;
    }

    internal ExecutionStatus UpdateMobitel(string short_id, string name, string description)
    {
      ExecutionStatus result = ExecutionStatus.OK;
      ISrvDbCommand commandUpdateMobitel = _commandCache.GetDbCommand<UpdateMobitelInfo>();
      /// Порядок передачи параметров:
      /// 1. mobitel_id
      /// 2. name
      /// 3. description
      commandUpdateMobitel.Init(new object[] {dictMobitelID[short_id], name, description });
      result = commandUpdateMobitel.Execute();
      return result;
    }

    internal ExecutionStatus DeleteMobitel(int mobitel_id)
    {
      ExecutionStatus result = ExecutionStatus.ERROR_DATA;
      ISrvDbCommand command = _commandCache.GetDbCommand<DeleteMobitel>();
      /// Порядок передачи параметров:
      /// 1. mobitelID
      command.Init(new object[] { mobitel_id });
      result = command.Execute();
      return result;
    }

    internal int GetMobitelId(string short_id)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<GetMobitelIdDbCommand>();
      /// Порядок передачи параметров:
      /// 1. devIdShort
      command.Init(new object[] { short_id });
      if (command.Execute() == ExecutionStatus.OK)
        return ((GetMobitelIdDbCommand)command).MobitelId;
      else
        return -1;
    }

    internal ExecutionStatus FillPhones(ref Dictionary<string,string> phonesDic)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<SelectTelnumbers>();
      ExecutionStatus result;
      try
      {
        command.Init();
        using (MySqlDataReader reader = command.ExecuteReader(out result))
        {
          if ((result == ExecutionStatus.OK) && (reader != null))
          {
            while (reader.Read())
            {
              if(!phonesDic.ContainsKey(reader.GetString("TelNumber")))
                phonesDic.Add(reader.GetString("TelNumber"), reader.GetString("Name"));
            }
          }
        }
      }
      catch
      {
        result = ExecutionStatus.ERROR_DB;
      }
      finally
      {
        command.CloseConnection();
      }
      return result;
    }

    internal ExecutionStatus AddNewPhone(string name, string phone, ref int id)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<InsertNewTelnumbers>();
      ExecutionStatus result;
       /// Порядок передачи параметров:
      /// 1. Name
      /// 2. Phone
      command.Init(new object[] { name, phone });
      result = command.Execute();
      id = ((InsertNewTelnumbers)command).ID;
      return result;
    }

    internal ExecutionStatus SavePhoneTt(string mobitel, string phone)
    {
      int mobitelID = dictMobitelID[mobitel];
      ISrvDbCommand command = _commandCache.GetDbCommand<InsertServiceSend>();
      ExecutionStatus result;
      /// 1. name
      /// 2. description
      /// 3. mobitelID
      /// 4. telMobitels
      command.Init(new object[] { "set", "phone", mobitelID, phone });
      result = command.Execute();
      return result;
    }

    internal string GetPhoneTt(string mobitel)
    {
      string value = "";
      int mobitelID = dictMobitelID[mobitel];
      ISrvDbCommand command = _commandCache.GetDbCommand<GetTeletrackPhone>();
      /// 1. name
      command.Init(new object[] {mobitelID});
      if (command.Execute() == ExecutionStatus.OK)
      {
        value = ((GetTeletrackPhone)command).Phone;
      }
      return value;
    }

    internal ExecutionStatus LoadServiceSetting(ref string serviceIP, ref string servicePort, ref string serviceLogin, ref string servicePassword)
    {
      ExecutionStatus result;
      ISrvDbCommand command = _commandCache.GetDbCommand<GetServiceinit>();
      try
      {
        command.Init();
        using (MySqlDataReader reader = command.ExecuteReader(out result))
        {
          if ((result == ExecutionStatus.OK) && (reader != null) && reader.Read())
          {
            serviceIP = reader.GetString("A1");
            servicePort = reader.GetString("A2");
            serviceLogin = reader.GetString("A3");
            servicePassword = reader.GetString("A4");
          }
        }
      }
      catch
      {
        result = ExecutionStatus.ERROR_DB;
      }
      return result;
    }

    internal ExecutionStatus SaveServiceSetting(string serviceIP, string servicePort, string serviceLogin, string servicePassword)
    {
      ISrvDbCommand command = _commandCache.GetDbCommand<UpdateServiceinit>(); 
      /// 1. ip
      /// 2. port
      /// 3. login
      /// 4. password
      command.Init(new object[] { serviceIP, servicePort, serviceLogin, servicePassword });
      return command.Execute();
    }

    internal ExecutionStatus SaveDataGps(int mobitelID, List<DataGps> listDataGps, string[] extFields)
    {
      ExecutionStatus result = ExecutionStatus.ERROR_DATA;
      List<string> strDataGpsInsert = new List<string>();
      if (mobitelID > 0)
      {
        foreach (DataGps data in listDataGps)
        {
          DataGpsInfo dataGps = DataGpsParser.GetDataGpsRow(mobitelID, data);
          string parsedRow = dataGps.GetAssembledRow();
          // Добавление дополнительных полей
          if ((extFields != null) && (extFields.Length > 0))
          {
            foreach (string field in extFields)
            {
              parsedRow = String.Concat(parsedRow, DataGpsInfo.DELIMITER, field);
            }
          }
          strDataGpsInsert.Add(parsedRow);
        }

        ISrvDbCommand command = _commandCache.GetDbCommand<TInsertDataInBufferDBCommand>();
        // Пробуем выполнить вставку одним пакетом
        command.Init(new object[] { strDataGpsInsert.ToArray() });
        result = command.Execute();
        if (result == ExecutionStatus.OK)
        {
          //Перемещаем данные из буфера в основную таблицу
          result = _commandCache.GetDbCommand<TTransferBufferDBCommand>().Execute();
        }
      }
      return result;
    }
  }
}
