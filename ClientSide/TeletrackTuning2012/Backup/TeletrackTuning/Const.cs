﻿namespace TeletrackTuning
{
  /// <summary>
  /// Константы встречающиеся в работе с протоколом А1
  /// </summary>
  public static class Const
  {
    /// <summary>
    /// Размер пакета на уровне LEVEL0 = 160 байт
    /// </summary>
    public const byte LEVEL0_PACKET_LENGTH = 160;
    /// <summary>
    /// Длина  Email = 13 байт
    /// </summary>
    public const byte LEVEL0_EMAIL_LENGTH = 13;
    /// <summary>
    /// Позиция начала ID_TT в пакете А1
    /// </summary>
    public const byte LEVEL0_IDTT_POSITION = 19;
    /// <summary>
    /// Длина ID_TT в пакете А1
    /// </summary>
    public const byte LEVEL0_IDTT_LENGHT = 4;
    /// <summary>
    /// Позиция начала признака пакета на уровне LEVEL0 - 14ый байт.
    /// Признак %% занимает 14 и 15 байты.
    /// </summary>
    public const byte LEVEL0_START_INDICATOR_POSITION = 14;
    /// <summary>
    /// Позиция байта хранящего длину блока данных на уровне LEVEL1 = 2-ой байт
    /// </summary>
    public const byte LEVEL1_LENGTH_POSITION = 2;
    /// <summary>
    /// Позиция CRC на уровне LEVEL1 = 7-ой байт
    /// </summary>
    public const byte LEVEL1_CRC_POSITION = 7;
    /// <summary>
    /// Длина пакета на уровне LEVEL1 = 144 байт
    /// </summary>
    public const byte LEVEL1_PACKET_LENGHT = 144;
    /// <summary>
    /// Длина блока данных на уровне LEVEL1 = 136 байт
    /// </summary>
    public const byte LEVEL1_DATA_BLOCK_LENGHT = 136;
    /// <summary>
    /// Длина пакета на уровне LEVEL3 = 102 байта
    /// </summary>
    public const byte LEVEL3_PACKET_LENGHT = 102;
    /// <summary>
    /// Позиция начала MessageID на уровне LEVEL3 = 1-ый байт
    /// </summary>
    public const byte LEVEL3_MESSAGEID_POSITION = 1;
    /// <summary>
    /// Позиция начала блока данных на уровне LEVEL3 = 3-ий байт
    /// </summary>
    public const byte LEVEL3_DATA_BLOCK_POSITION = 3;
    /// <summary>
    /// Длина блока данных на уровне LEVEL3 = 99 байт
    /// </summary>
    public const byte LEVEL3_DATA_BLOCK_LENGTH = 99;
    /// <summary>
    /// Символ с кодом 0
    /// </summary>
    public const char ZERO_SYMBOL = (char)0;
    /// <summary>
    /// Стартовый символ. 
    /// Признак начала пакета состоит из двух стартовых символов %%
    /// </summary>
    public const char START_INDICATOR_SYMBOL = '%';


    /// <summary>
    /// Максимальное абсолютное значение долготы = 108000000 (180)
    /// </summary>
    public const int MAX_LONGITUDE = 108000000;
    /// <summary>
    /// Максимальное абсолютное значение широты = 54000000 (90)
    /// </summary>
    public const int MAX_LATITUDE = 54000000;
  }
}
