﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3053
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TeletrackTuning.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TeletrackTuning.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        internal static System.Drawing.Bitmap database_gear_icon {
            get {
                object obj = ResourceManager.GetObject("database-gear-icon", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error load setting for TT.
        /// </summary>
        internal static string errorLoadSettingTT {
            get {
                return ResourceManager.GetString("errorLoadSettingTT", resourceCulture);
            }
        }
        
        internal static System.Drawing.Bitmap Folder_Open_icon {
            get {
                object obj = ResourceManager.GetObject("Folder-Open-icon", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap IconX {
            get {
                object obj = ResourceManager.GetObject("IconX", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Запрос настроек событий.
        /// </summary>
        internal static string requestEvents {
            get {
                return ResourceManager.GetString("requestEvents", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Запрос настроек GPRS.
        /// </summary>
        internal static string requestGprsSetting {
            get {
                return ResourceManager.GetString("requestGprsSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Запрос GPS данных с устройства.
        /// </summary>
        internal static string requestGpsData {
            get {
                return ResourceManager.GetString("requestGpsData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Запрос настроек телефонных номеров.
        /// </summary>
        internal static string requestPhonesSetting {
            get {
                return ResourceManager.GetString("requestPhonesSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Запрос настроек сервера.
        /// </summary>
        internal static string requestServerSetting {
            get {
                return ResourceManager.GetString("requestServerSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выполнение текстовой комманды.
        /// </summary>
        internal static string runTextCommand {
            get {
                return ResourceManager.GetString("runTextCommand", resourceCulture);
            }
        }
        
        internal static System.Drawing.Bitmap Save_icon {
            get {
                object obj = ResourceManager.GetObject("Save-icon", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Установка настроек событий.
        /// </summary>
        internal static string setEvents {
            get {
                return ResourceManager.GetString("setEvents", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Установка настроек GPRS.
        /// </summary>
        internal static string setGprsSetting {
            get {
                return ResourceManager.GetString("setGprsSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Установка настроек телефонных номеров.
        /// </summary>
        internal static string setPhonesSetting {
            get {
                return ResourceManager.GetString("setPhonesSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Установка настроек сервера.
        /// </summary>
        internal static string setServerSetting {
            get {
                return ResourceManager.GetString("setServerSetting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Timeout ожидания ответа на команду №.
        /// </summary>
        internal static string timeoutWaidRequestOnCmd {
            get {
                return ResourceManager.GetString("timeoutWaidRequestOnCmd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Не выбрана вкладка.
        /// </summary>
        internal static string unknownCommand {
            get {
                return ResourceManager.GetString("unknownCommand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Незадано ID устройства.
        /// </summary>
        internal static string unknownID {
            get {
                return ResourceManager.GetString("unknownID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ожидается ответ на предыдущую команду. Попробуйте позже.
        /// </summary>
        internal static string waitRequestOnCommand {
            get {
                return ResourceManager.GetString("waitRequestOnCommand", resourceCulture);
            }
        }
    }
}
