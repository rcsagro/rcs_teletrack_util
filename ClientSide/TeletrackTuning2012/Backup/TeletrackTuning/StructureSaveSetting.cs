﻿using System;
using System.Collections.Generic;
using System.Text;
using TeletrackTuning.Entity;

namespace TeletrackTuning
{
  public class StructureSaveSetting
  {
    private GprsBaseConfig baseConfig;
    public GprsBaseConfig baseConfigSetting 
    {
      get { return baseConfig; }
      set { baseConfig = value; }
    }

    private GprsEmailConfig emailConfig;
    public GprsEmailConfig emailConfigSetting
    {
      get { return emailConfig; }
      set { emailConfig = value; }
    }

    private EventConfig eventConfig;
    public EventConfig eventConfigSetting
    {
      get { return eventConfig; }
      set { eventConfig = value; }
    }

    private PhoneNumberConfig phoneConfig;
    public PhoneNumberConfig phoneConfigSetting
    {
      get { return phoneConfig; }
      set { phoneConfig = value; }
    }
  }

}
