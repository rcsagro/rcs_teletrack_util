﻿using System;
using System.Collections.Generic;
using System.Text;
using TeletrackTuning.Entity;
using TeletrackTuning.Error;

namespace TeletrackTuning
{
  /// <summary>
  /// Кодирование сообщений отправляемых телетреку по протоколу A1
  /// </summary>
  public static class EncodeA1
  {
    /// <summary>
    /// Символ _ устанавливается на незаполненных местах
    /// </summary>
    private const byte CLEAR_MESSAGE_SYMBOL = (byte)'_';

    /// <summary>
    /// Формирование заготовки для команды. 
    /// Заполняются первые 3 байта: CommandID + MessageID
    /// </summary>
    /// <param name="commandId">Command id</param>
    /// <param name="messageId">Message id</param>
    /// <returns>byte[102]</returns>
    private static byte[] GetCommandLevel3Template(byte commandId, ushort messageId)
    {
      // формирование массива
      byte[] result = new byte[Const.LEVEL3_PACKET_LENGHT];
      // заполнение идентификатора команды
      result[0] = commandId;
      // заполнение идентификатора сообщения
      byte[] tmp = Level4Converter.UShortToBytes(messageId);
      result[1] = tmp[0];
      result[2] = tmp[1];
      // Все остальное забиваем символом _ 
      for (int i = 3; i < Const.LEVEL3_PACKET_LENGHT; i++)
        result[i] = CLEAR_MESSAGE_SYMBOL;
      return result;
    }

    /// <summary>
    /// Формирование пакета уровня LEVEL1
    /// </summary>
    /// <exception cref="ArgumentNullException">Не переданы параметры</exception> 
    /// <exception cref="A1Exception">Ошибочная длина параметра level3Command</exception> 
    /// <param name="level3Command">массив байт byte[102] с командой</param>
    /// <param name="devShortId">Строка короткого идентификатора телетрека</param>
    /// <returns>byte[144]</returns>
    private static byte[] GetMessageLevel1(byte[] level3Command, string devShortId)
    {
      if (level3Command == null)
        throw new ArgumentNullException("level3Command", "Не передан массив байт level3Command");
      if (level3Command.Length != Const.LEVEL3_PACKET_LENGHT)
        throw new A1Exception(String.Format(ErrorText.LENGTH_PACKET,
          "LEVEL3", Const.LEVEL3_PACKET_LENGHT, level3Command.Length));
      if (String.IsNullOrEmpty(devShortId))
        throw new ArgumentNullException("devShortId", "Не передан параметр devShortId");

      byte[] result = new byte[Const.LEVEL1_PACKET_LENGHT];
      // Версия
      result[0] = CodeDecodeA1Levels.ValueToSymbol(0);
      result[1] = CodeDecodeA1Levels.ValueToSymbol(0);

      // Длина
      result[Const.LEVEL1_LENGTH_POSITION] = CodeDecodeA1Levels.ValueToSymbol(
        Const.LEVEL1_DATA_BLOCK_LENGHT >> 2);

      // Короткий идентификатор телетрека
      result[Const.LEVEL1_LENGTH_POSITION + 1] =
        CodeDecodeA1Levels.ValueToSymbol(Convert.ToByte(devShortId[0]));
      result[Const.LEVEL1_LENGTH_POSITION + 2] =
        CodeDecodeA1Levels.ValueToSymbol(Convert.ToByte(devShortId[1]));
      result[Const.LEVEL1_LENGTH_POSITION + 3] =
        CodeDecodeA1Levels.ValueToSymbol(Convert.ToByte(devShortId[2]));
      result[Const.LEVEL1_LENGTH_POSITION + 4] =
        CodeDecodeA1Levels.ValueToSymbol(Convert.ToByte(devShortId[3]));

      // Блок Данных
      Array.Copy(CodeDecodeA1Levels.Encode8BitTo6(level3Command), 0,
        result, Const.LEVEL1_CRC_POSITION + 1, Const.LEVEL1_DATA_BLOCK_LENGHT);

      // CRC
      result[Const.LEVEL1_CRC_POSITION] =
        CodeDecodeA1Levels.ValueToSymbol(Convert.ToByte(
           Util.CalculateLevel1CRC(result, 0, Const.LEVEL1_DATA_BLOCK_LENGHT)));

      return result;
    }

    /// <summary>
    /// Формирование пакета уровня LEVEL0
    /// </summary>
    /// <exception cref="ArgumentNullException">Не переданы параметры</exception> 
    /// <exception cref="A1Exception">Ошибочная длина параметра level1Message</exception> 
    /// <param name="level1Message">Закодированное сообшение Level1</param>
    /// <param name="email">Email не более 13 символов</param>
    /// <returns>byte[160]</returns>
    private static byte[] GetMessageLevel0(byte[] level1Message, string email)
    {
      if (level1Message == null)
        throw new ArgumentNullException("level1Message", "Не передан массив байт level1Message");
      if (level1Message.Length != Const.LEVEL1_PACKET_LENGHT)
        throw new A1Exception(String.Format(ErrorText.LENGTH_PACKET,
          "LEVEL3", Const.LEVEL1_PACKET_LENGHT, level1Message.Length));
      if (email == null)
        throw new ArgumentNullException("email", "Не передан параметр email");
      if (email.Length > Const.LEVEL0_EMAIL_LENGTH)
        throw new A1Exception(String.Format(ErrorText.EMAIL_TO_BIG, Const.LEVEL0_EMAIL_LENGTH));


      byte[] result = new byte[Const.LEVEL0_PACKET_LENGTH];
      // Email
      for (int i = 0; i <= Const.LEVEL0_EMAIL_LENGTH; i++)
      {
        if (i < email.Length)
        {
          byte value = Convert.ToByte(email[i]);
          if ((value == 0) || (value == 255))
            result[i] = Convert.ToByte(' ');
          else
            result[i] = value;
        }
        else
        {
          result[i] = Convert.ToByte(' ');
        }
      }
      // Пробел
      result[Const.LEVEL0_EMAIL_LENGTH] = Convert.ToByte(' ');
      // Признак начала пакета
      const byte start_indicator_symbol = (byte)Const.START_INDICATOR_SYMBOL;
      result[Const.LEVEL0_START_INDICATOR_POSITION] = start_indicator_symbol;
      result[Const.LEVEL0_START_INDICATOR_POSITION + 1] = start_indicator_symbol;
      // Сам пакет
      Array.Copy(level1Message, 0,
        result,
        Const.LEVEL0_START_INDICATOR_POSITION + 2,
        Const.LEVEL1_PACKET_LENGHT);

      return result;
    }

    /// <summary>
    /// Кодирование команды кофигурации SMS адресов.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода)</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана конфигурация SMS адресов smsAddrConfig</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта smsAddrConfig</exception> 
    /// <param name="smsAddrConfig">Конфигурация SMS адресов</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeSmsAddrConfigSet(SmsAddrConfig smsAddrConfig)
    {
      if (smsAddrConfig == null)
        throw new ArgumentNullException("smsAddrConfig",
          "Не передана конфигурация SMS адресов smsAddrConfig");

      string vaidateErrorMessage;
      if (!smsAddrConfig.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          smsAddrConfig.GetType().Name, vaidateErrorMessage));

      smsAddrConfig.CommandID = CommandDescriptor.SMSConfigSet;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)smsAddrConfig.CommandID, smsAddrConfig.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(smsAddrConfig.DsptEmailGprs, 30),
        command, index, 30);

      index += 30;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(smsAddrConfig.DsptEmailSMS, 14),
        command, index, 14);

      index += 14;
      Util.FillCommandAttribute(
        Level4Converter.TelNumberToBytes(smsAddrConfig.SmsCentre, 11),
        command, index, 11);

      index += 11;
      Util.FillCommandAttribute(
        Level4Converter.TelNumberToBytes(smsAddrConfig.SmsDspt, 11),
        command, index, 11);

      index += 11;
      Util.FillCommandAttribute(
        Level4Converter.TelNumberToBytes(smsAddrConfig.SmsEmailGate, 11),
         command, index, 11);

      //2. Кодирование на уровне Level1 + Level0
      smsAddrConfig.Source = GetMessageLevel0(
        GetMessageLevel1(command, smsAddrConfig.ShortID), "");
      smsAddrConfig.Message = Util.GetStringFromByteArray(smsAddrConfig.Source);
      return smsAddrConfig.Message;
    }

    /// <summary>
    /// Кодирование сообщения-запроса кофигурации SMS адресов.
    /// В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан запрос кофигурации SMS адресов</exception>  
    /// <param name="query">Простой запрос</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeSmsAddrConfigQuery(SimpleQuery query)
    {
      if (query == null)
        throw new ArgumentNullException("query",
          "Не передан запрос кофигурации SMS адресов");

      query.CommandID = CommandDescriptor.SMSConfigQuery;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)query.CommandID, query.MessageID);
      //2. Кодирование на уровне Level1 + Level0
      query.Source = GetMessageLevel0(
        GetMessageLevel1(command, query.ShortID), "");
      query.Message = Util.GetStringFromByteArray(query.Source);
      return query.Message;
    }

    /// <summary>
    /// Кодирование сообщения-кофигурации телефонных номеров.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана конфигурация телефонных номеров phoneNumberConfig</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта phoneNumberConfig</exception> 
    /// <param name="phoneNumberConfig">Конфигурация номеров телефонов</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodePhoneNumberConfigSet(PhoneNumberConfig phoneNumberConfig)
    {
      if (phoneNumberConfig == null)
        throw new ArgumentNullException("phoneNumberConfig",
          "Не передана конфигурация телефонных номеров phoneNumberConfig");

      string vaidateErrorMessage;
      if (!phoneNumberConfig.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          phoneNumberConfig.GetType().Name, vaidateErrorMessage));

      phoneNumberConfig.CommandID = CommandDescriptor.PhoneConfigSet;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)phoneNumberConfig.CommandID, phoneNumberConfig.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      Util.FillCommandAttribute(
        Level4Converter.TelNumberToBytes(phoneNumberConfig.NumberAccept1, 11),
        command, index, 11);

      index += 11;
      Util.FillCommandAttribute(
        Level4Converter.TelNumberToBytes(phoneNumberConfig.NumberAccept2, 11),
        command, index, 11);

      index += 11;
      Util.FillCommandAttribute(
        Level4Converter.TelNumberToBytes(phoneNumberConfig.NumberAccept3, 11),
        command, index, 11);

      index += 11;
      Util.FillCommandAttribute(
        Level4Converter.TelNumberToBytes(phoneNumberConfig.NumberDspt, 11),
        command, index, 11);

      index += 11;
      Util.FillCommandAttribute(new byte[1], command, index, 1);

      index += 1;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(phoneNumberConfig.Name1, 8),
        command, index, 8);

      index += 8;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(phoneNumberConfig.Name2, 8),
        command, index, 8);

      index += 8;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(phoneNumberConfig.Name3, 8),
        command, index, 8);

      index += 8;
      Util.FillCommandAttribute(
        Level4Converter.TelNumberToBytes(phoneNumberConfig.NumberSOS, 11),
        command, index, 11);

      //2. Кодирование на уровне Level1 + Level0 
      phoneNumberConfig.Source = GetMessageLevel0(
        GetMessageLevel1(command, phoneNumberConfig.ShortID), "");
      phoneNumberConfig.Message = Util.GetStringFromByteArray(phoneNumberConfig.Source);
      return phoneNumberConfig.Message;
    }

    /// <summary>
    /// Кодирование сообщения-запроса конфигурации телефонных номеров.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан запрос кофигурации телефонных номеров</exception>  
    /// <param name="query">Простой запрос</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodePhoneNumberConfigQuery(SimpleQuery query)
    {
      if (query == null)
        throw new ArgumentNullException("query",
          "Не передан запрос кофигурации телефонных номеров");

      query.CommandID = CommandDescriptor.PhoneConfigQuery;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)query.CommandID, query.MessageID);
      //2. Кодирование на уровне Level1 + Level0
      query.Source = GetMessageLevel0(
        GetMessageLevel1(command, query.ShortID), "");
      query.Message = Util.GetStringFromByteArray(query.Source);
      return query.Message;
    }

    /// <summary>
    /// Кодирование сообщения-кофигурации событий.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана конфигурация событий eventConfig</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта eventConfig</exception> 
    /// <param name="eventConfig">Конфигурация событий</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeEventConfigSet(EventConfig eventConfig)
    {
      if (eventConfig == null)
        throw new ArgumentNullException("phoneNumberConfig",
          "Не передана конфигурация событий eventConfig");

      string vaidateErrorMessage;
      if (!eventConfig.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          eventConfig.GetType().Name, vaidateErrorMessage));

      eventConfig.CommandID = CommandDescriptor.EventConfigSet;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)eventConfig.CommandID, eventConfig.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      Util.FillCommandAttribute(
        new byte[1] { Convert.ToByte(eventConfig.SpeedChange) },
        command, index, 1);

      index++;
      Util.FillCommandAttribute(
        Level4Converter.UShortToBytes(eventConfig.CourseBend),
        command, index, 2);

      index += 2;
      Util.FillCommandAttribute(
        Level4Converter.UShortToBytes(eventConfig.Distance1),
        command, index, 2);

      index += 2;
      Util.FillCommandAttribute(
        Level4Converter.UShortToBytes(eventConfig.Distance2),
        command, index, 2);

      index += 2;
      for (int i = 0; i < 32; i++)
      {
        Util.FillCommandAttribute(
          Level4Converter.UShortToBytes(eventConfig.EventMask[i]),
          command, index, 2);
        index += 2;
      } // for (int)

      Util.FillCommandAttribute(
        Level4Converter.UShortToBytes(eventConfig.MinSpeed),
        command, index, 2);

      index += 2;
      Util.FillCommandAttribute(
        Level4Converter.UShortToBytes(eventConfig.Timer1),
        command, index, 2);

      index += 2;
      Util.FillCommandAttribute(
        Level4Converter.UShortToBytes(eventConfig.Timer2),
        command, index, 2);

      index += 2;
      // Зарезервировано
      Util.FillCommandAttribute(new byte[2], command, index, 2);

      //2. Кодирование на уровне Level1 + Level0 
      eventConfig.Source = GetMessageLevel0(
        GetMessageLevel1(command, eventConfig.ShortID), "");
      eventConfig.Message = Util.GetStringFromByteArray(eventConfig.Source);
      return eventConfig.Message;
    }

    /// <summary>
    /// Кодирование сообщения-запроса конфигурации событий.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан запрос кофигурации событий</exception>  
    /// <param name="query">Простой запрос</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeEventConfigQuery(SimpleQuery query)
    {
      if (query == null)
        throw new ArgumentNullException("query",
          "Не передан запрос кофигурации телефонных номеров");

      query.CommandID = CommandDescriptor.EventConfigQuery;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)query.CommandID, query.MessageID);
      //2. Кодирование на уровне Level1 + Level0
      query.Source = GetMessageLevel0(
        GetMessageLevel1(command, query.ShortID), "");
      query.Message = Util.GetStringFromByteArray(query.Source);
      return query.Message;
    }

    /// <summary>
    /// Кодирование сообщения-кофигурации уникальных параметров
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана конфигурация уникальных параметров uniqueConfig</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта uniqueConfig</exception> 
    /// <param name="uniqueConfig">Конфигурация уникальных параметров</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeUniqueConfigSet(UniqueConfig uniqueConfig)
    {
      if (uniqueConfig == null)
        throw new ArgumentNullException("uniqueConfig",
          "Не передана конфигурация уникальных параметров uniqueConfig");

      string vaidateErrorMessage;
      if (!uniqueConfig.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          uniqueConfig.GetType().Name, vaidateErrorMessage));

      uniqueConfig.CommandID = CommandDescriptor.UniqueConfigSet;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)uniqueConfig.CommandID, uniqueConfig.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(uniqueConfig.DispatcherID, 4),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(uniqueConfig.Password, 8),
        command, index, 8);

      index += 8;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(uniqueConfig.TmpPassword, 8),
        command, index, 8);

      //2. Кодирование на уровне Level1 + Level0 
      uniqueConfig.Source = GetMessageLevel0(
        GetMessageLevel1(command, uniqueConfig.ShortID), "");
      uniqueConfig.Message = Util.GetStringFromByteArray(uniqueConfig.Source);
      return uniqueConfig.Message;
    }

    /// <summary>
    /// Кодирование сообщения-запроса конфигурации уникальных параметров.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан запрос кофигурации уникальных параметров</exception>  
    /// <param name="query">Простой запрос</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeUniqueConfigQuery(SimpleQuery query)
    {
      if (query == null)
        throw new ArgumentNullException("query",
          "Не передан запрос кофигурации уникальных параметров");

      query.CommandID = CommandDescriptor.UniqueConfigQuery;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)query.CommandID, query.MessageID);
      //2. Кодирование на уровне Level1 + Level0
      query.Source = GetMessageLevel0(
        GetMessageLevel1(command, query.ShortID), "");
      query.Message = Util.GetStringFromByteArray(query.Source);
      return query.Message;
    }

    /// <summary>
    /// Кодирование сообщения-кофигурации идентификационных параметров.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана конфигурация идентификационных параметров idConfig</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта idConfig</exception> 
    /// <param name="idConfig">Конфигурация идентификационных параметров</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeIdConfigSet(IdConfig idConfig)
    {
      if (idConfig == null)
        throw new ArgumentNullException("idConfig",
          "Не передана конфигурация идентификационных параметров idConfig");

      string vaidateErrorMessage;
      if (!idConfig.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          idConfig.GetType().Name, vaidateErrorMessage));

      idConfig.CommandID = CommandDescriptor.IdConfigSet;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)idConfig.CommandID, idConfig.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(idConfig.DevIdShort, 4),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(idConfig.DevIdLong, 16),
        command, index, 16);

      index += 16;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(idConfig.ModuleIdGps, 4),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(idConfig.ModuleIdGsm, 4),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(idConfig.ModuleIdMm, 4),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(idConfig.ModuleIdRf, 4),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(idConfig.ModuleIdSs, 4),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(idConfig.VerProtocolLong, 16),
        command, index, 16);

      index += 16;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(idConfig.VerProtocolShort, 4),
        command, index, 4);

      //2. Кодирование на уровне Level1 + Level0 
      idConfig.Source = GetMessageLevel0(
        GetMessageLevel1(command, idConfig.ShortID), "");
      idConfig.Message = Util.GetStringFromByteArray(idConfig.Source);
      return idConfig.Message;
    }

    /// <summary>
    /// Кодирование сообщения-запроса конфигурации идентификационных параметров.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан запрос кофигурации идентификационных параметров</exception>  
    /// <param name="query">Простой запрос</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeIdConfigQuery(SimpleQuery query)
    {
      if (query == null)
        throw new ArgumentNullException("query",
          "Не передан запрос кофигурации идентификационных параметров");

      query.CommandID = CommandDescriptor.IdConfigQuery;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)query.CommandID, query.MessageID);
      //2. Кодирование на уровне Level1 + Level0
      query.Source = GetMessageLevel0(
        GetMessageLevel1(command, query.ShortID), "");
      query.Message = Util.GetStringFromByteArray(query.Source);
      return query.Message;
    }

    /// <summary>
    /// Кодирование сообщения-кофигурации контрольных зон.
    /// <para>В результате получаем 25 пакетов, которые отправляются
    /// телетреку строго в порядке их расположения в результирующем массиве.</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана конфигурация контрольных зон zoneConfig</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта zoneConfig</exception> 
    /// <param name="zoneConfig">Кофигурация контрольных зон</param>
    /// <returns>Конфигурация контрольных зон - 25 сообщений</returns>
    public static string[] EncodeZoneConfigSet(ZoneConfig zoneConfig)
    {
      if (zoneConfig == null)
        throw new ArgumentNullException("zoneConfig",
          "Не передана конфигурация контрольных зон zoneConfig");

      string vaidateErrorMessage;
      if (!zoneConfig.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          zoneConfig.GetType().Name, vaidateErrorMessage));

      // полный список точек
      List<ZonePoint> pointsList = new List<ZonePoint>();
      // полный список масок
      List<byte> maskList = new List<byte>();

      zoneConfig.CommandID = CommandDescriptor.ZoneConfigSet;

      #region 1-ое сообщение - 64 описания зон

      //формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)zoneConfig.CommandID, zoneConfig.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      // тип пакета
      command[index] = (byte)ZoneCfgPacketType.Config;
      // номер под-пакета
      index++;
      command[index] = 0;
      // кол-во зон
      index++;
      command[index] = (byte)zoneConfig.ZoneCount;
      // Описание зон
      index++;
      for (byte i = 0; i < 64; i++)
      {
        if (zoneConfig.ZoneCount > i)
        {
          foreach (ZonePoint point in zoneConfig.ZoneList[i].Points)
            pointsList.Add(point);

          maskList.Add(Util.GetMaskForZone(
            zoneConfig.ZoneList[i].Mask.EntryFlag,
            zoneConfig.ZoneList[i].Mask.ExitFlag,
            zoneConfig.ZoneList[i].Mask.InFlag,
            zoneConfig.ZoneList[i].Mask.OutFlag));

          // Индекс на котором заканчивается описание зоны в общем списке точек
          if (pointsList.Count > 0)
            command[index] = (byte)(pointsList.Count - 1);
        }
        else
        {
          maskList.Add(0);
        }
        index++;
      }

      // Кодирование на уровне Level1 + Level0 
      zoneConfig.AddSource(GetMessageLevel0(
        GetMessageLevel1(command, zoneConfig.ShortID), ""));

      zoneConfig.AddMessage(Util.GetStringFromByteArray(zoneConfig.Source[0]));

      #endregion

      // Дозаполним 0-выми значениями оставшиеся точки
      for (int i = pointsList.Count; i < 256; i++)
      {
        pointsList.Add(new ZonePoint(0, 0));
      }

      #region Формирование 23-ех сообщений, которые в конечном итоге будут содержать 253 точки

      for (byte i = 1; i <= 23; i++)
      {
        //формирование 102 байт  - данные  + MessageID + CommandID 
        //   LEVEL3 + LEVEL4
        command = GetCommandLevel3Template(
          (byte)zoneConfig.CommandID, zoneConfig.MessageID);

        index = Const.LEVEL3_DATA_BLOCK_POSITION;
        // тип пакета
        command[index] = (byte)ZoneCfgPacketType.Point;
        // номер под-пакета
        index++;
        command[index] = i;
        // точки - по 11 штук в пакете
        index++;
        for (byte j = 0; j < 11; j++)
        {
          byte pointIndex = (byte)(((i - 1) * 11) + j);
          Util.FillCommandAttribute(
            Level4Converter.IntToBytes(pointsList[pointIndex].Longitude / 10),
            command, index, 4);
          index += 4;
          Util.FillCommandAttribute(
            Level4Converter.IntToBytes(pointsList[pointIndex].Latitude / 10),
            command, index, 4);
          index += 4;
        }

        // Кодирование на уровне Level1 + Level0 
        zoneConfig.AddSource(GetMessageLevel0(
          GetMessageLevel1(command, zoneConfig.ShortID), ""));

        zoneConfig.AddMessage(Util.GetStringFromByteArray(zoneConfig.Source[i]));
      }

      #endregion

      #region Последнее 25-ое сообщение - 3 последние точки + 64 маски

      //формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      command = GetCommandLevel3Template(
        (byte)zoneConfig.CommandID, zoneConfig.MessageID);

      index = Const.LEVEL3_DATA_BLOCK_POSITION;
      // тип пакета
      command[index] = (byte)ZoneCfgPacketType.Mask;
      // номер под-пакета
      index++;
      command[index] = 0;
      // Последние 3 точки
      index++;
      for (byte i = 0; i < 3; i++)
      {
        byte pointIndex = (byte)(253 + i);
        Util.FillCommandAttribute(
          Level4Converter.IntToBytes(pointsList[pointIndex].Longitude / 10),
          command, index, 4);
        index += 4;
        Util.FillCommandAttribute(
          Level4Converter.IntToBytes(pointsList[pointIndex].Latitude / 10),
          command, index, 4);
        index += 4;
      }
      // Записываем маски
      for (byte i = 0; i < 64; i++)
      {
        Util.FillCommandAttribute(
          new byte[] { maskList[i] }, command, index, 1);
        index++;
      } // for (byte)
      // ZoneMsgID
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(zoneConfig.ZoneMsgID),
        command, index, 4);
      // Контрольная сумма
      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(Util.CalculateZoneConfigCRC(zoneConfig)),
        command, index, 4);

      // Кодирование на уровне Level1 + Level0 
      zoneConfig.AddSource(GetMessageLevel0(
        GetMessageLevel1(command, zoneConfig.ShortID), ""));

      zoneConfig.AddMessage(Util.GetStringFromByteArray(zoneConfig.Source[24]));

      #endregion

      return zoneConfig.Message.ToArray();
    }

    /// <summary>
    /// Кодирование сообщения-запроса GPS данных.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан запрос GPS данных query</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта query</exception>  
    /// <param name="query">Запрос GPS данных</param>
    /// <returns>Запрос GPS данных в строковом виде</returns>
    public static string EncodeDataGpsQuery(DataGpsQuery query)
    {
      if (query == null)
        throw new ArgumentNullException("query",
          "Не передан запрос GPS данных query");

      string vaidateErrorMessage;
      if (!query.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          query.GetType().Name, vaidateErrorMessage));

      query.CommandID = CommandDescriptor.DataGpsQuery;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)query.CommandID, query.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      Util.FillCommandAttribute(
        Level4Converter.UIntToBytes(query.CheckMask),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.UIntToBytes(query.Events),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.UIntToBytes(query.LastRecords),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.UIntToBytes(query.LastTimes),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.UIntToBytes(query.LastMeters),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.UIntToBytes(query.MaxSmsNumber),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.UIntToBytes(query.WhatSend),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.StartTime),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(new byte[] { query.StartSpeed },
        command, index, 1);

      index++;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.StartLatitude / 10),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.StartLongitude / 10),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(new byte[] { query.StartAltitude },
        command, index, 1);

      index++;
      Util.FillCommandAttribute(new byte[] { query.StartDirection },
        command, index, 1);

      index++;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.StartLogId),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(new byte[] { query.StartFlags },
        command, index, 1);

      index++;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.EndTime),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(new byte[] { query.EndSpeed },
        command, index, 1);

      index++;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.EndLatitude / 10),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.EndLongitude / 10),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(new byte[] { query.EndAltitude },
        command, index, 1);

      index++;
      Util.FillCommandAttribute(new byte[] { query.EndDirection },
        command, index, 1);

      index++;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.EndLogId),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(new byte[] { query.EndFlags },
        command, index, 1);

      index++;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.LogId1),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.LogId2),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.LogId3),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.LogId4),
        command, index, 4);

      index += 4;
      Util.FillCommandAttribute(
        Level4Converter.IntToBytes(query.LogId5),
        command, index, 4);

      //2. Кодирование на уровне Level1 + Level0 
      query.Source = GetMessageLevel0(
        GetMessageLevel1(command, query.ShortID), "");
      query.Message = Util.GetStringFromByteArray(query.Source);
      return query.Message;
    }

    /// <summary>
    /// Кодирование сообщения водителю.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передано сообщение водителю messageToDriver</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта messageToDriver</exception> 
    /// <param name="messageToDriver">сообщение водителю</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeMessageToDriver(MessageToDriver messageToDriver)
    {
      if (messageToDriver == null)
        throw new ArgumentNullException("messageToDriver",
          "Не передано сообщение водителю messageToDriver");

      string vaidateErrorMessage;
      if (!messageToDriver.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          messageToDriver.GetType().Name, vaidateErrorMessage));

      messageToDriver.CommandID = CommandDescriptor.MessageToDriver;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)messageToDriver.CommandID, messageToDriver.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      // Последний символ 0 на всякий случай
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(messageToDriver.Text, 79),
        command, index, 80);

      //2. Кодирование на уровне Level1 + Level0 
      messageToDriver.Source = GetMessageLevel0(
        GetMessageLevel1(command, messageToDriver.ShortID), "");
      messageToDriver.Message = Util.GetStringFromByteArray(messageToDriver.Source);
      return messageToDriver.Message;
    }

    /// <summary>
    /// Кодирование сообщения-кофигурации  основных настроек GPRS.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не переданы Основные настройки GPRS gprsBaseConfig</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта gprsBaseConfig</exception> 
    /// <param name="gprsBaseConfig">Основные настройки GPRS</param>
    /// <returns>string Основные настройки GPRS</returns>
    public static string EncodeGprsBaseConfigSet(GprsBaseConfig gprsBaseConfig)
    {
      if (gprsBaseConfig == null)
        throw new ArgumentNullException("gprsBaseConfig",
          "Не переданы основные настройки GPRS gprsBaseConfig");

      string vaidateErrorMessage;
      if (!gprsBaseConfig.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          gprsBaseConfig.GetType().Name, vaidateErrorMessage));

      gprsBaseConfig.CommandID = CommandDescriptor.GprsBaseConfigSet;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)gprsBaseConfig.CommandID, gprsBaseConfig.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      Util.FillCommandAttribute(
        Level4Converter.UShortToBytes(gprsBaseConfig.Mode),
        command, index, 2);

      index += 2;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsBaseConfig.ApnServer, 25),
        command, index, 25);

      index += 25;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsBaseConfig.ApnLogin, 10),
        command, index, 10);

      index += 10;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsBaseConfig.ApnPassword, 10),
        command, index, 10);

      index += 10;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsBaseConfig.DnsServer, 16),
        command, index, 16);

      index += 16;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsBaseConfig.DialNumber, 11),
        command, index, 11);

      index += 11;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsBaseConfig.GprsLogin, 10),
        command, index, 10);

      index += 10;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsBaseConfig.GprsPassword, 10),
        command, index, 10);

      //2. Кодирование на уровне Level1 + Level0 
      gprsBaseConfig.Source = GetMessageLevel0(
        GetMessageLevel1(command, gprsBaseConfig.ShortID), "");
      gprsBaseConfig.Message = Util.GetStringFromByteArray(gprsBaseConfig.Source);
      return gprsBaseConfig.Message;
    }

    /// <summary>
    /// Кодирование сообщения-запроса базовых настроек GPRS.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан запрос кофигурации базовых настроек GPRS</exception>  
    /// <param name="query">Простой запрос</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeGprsBaseConfigQuery(SimpleQuery query)
    {
      if (query == null)
        throw new ArgumentNullException("query",
          "Не передан запрос кофигурации базовых настроек GPRS");

      query.CommandID = CommandDescriptor.GprsBaseConfigQuery;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)query.CommandID, query.MessageID);
      //2. Кодирование на уровне Level1 + Level0
      query.Source = GetMessageLevel0(
        GetMessageLevel1(command, query.ShortID), "");
      query.Message = Util.GetStringFromByteArray(query.Source);
      return query.Message;
    }

    /// <summary>
    /// Кодирование сообщения-кофигурации настроек Email GPRS.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана кофигурации настроек Email GPRS gprsEmailConfig</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта gprsEmailConfig</exception> 
    /// <param name="gprsEmailConfig">Настройки Email GPRS</param>
    /// <returns>string  Настройки Email GPRS</returns>
    public static string EncodeGprsEmailConfigSet(GprsEmailConfig gprsEmailConfig)
    {
      if (gprsEmailConfig == null)
        throw new ArgumentNullException("gprsEmailConfig",
          "Не переданы настройки Email GPRS gprsEmailConfig");

      string vaidateErrorMessage;
      if (!gprsEmailConfig.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          gprsEmailConfig.GetType().Name, vaidateErrorMessage));

      gprsEmailConfig.CommandID = CommandDescriptor.GprsEmailConfigSet;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)gprsEmailConfig.CommandID, gprsEmailConfig.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsEmailConfig.SmtpServer, 25),
        command, index, 25);

      index += 25;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsEmailConfig.SmtpLogin, 10),
        command, index, 10);

      index += 10;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsEmailConfig.SmtpPassword, 10),
        command, index, 10);

      index += 10;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsEmailConfig.Pop3Server, 25),
        command, index, 25);

      index += 25;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsEmailConfig.Pop3Login, 10),
        command, index, 10);

      index += 10;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsEmailConfig.Pop3Password, 10),
        command, index, 10);

      //2. Кодирование на уровне Level1 + Level0 
      gprsEmailConfig.Source = GetMessageLevel0(
        GetMessageLevel1(command, gprsEmailConfig.ShortID), "");
      gprsEmailConfig.Message = Util.GetStringFromByteArray(gprsEmailConfig.Source);
      return gprsEmailConfig.Message;
    }

    /// <summary>
    /// Кодирование сообщения-запроса настроек Email GPRS.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан запрос кофигурации настроек Email GPRS</exception>  
    /// <param name="query">Простой запрос</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeGprsEmailConfigQuery(SimpleQuery query)
    {
      if (query == null)
        throw new ArgumentNullException("query",
          "Не передан запрос кофигурации настроек Email GPRS");

      query.CommandID = CommandDescriptor.GprsEmailConfigQuery;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)query.CommandID, query.MessageID);
      //2. Кодирование на уровне Level1 + Level0
      query.Source = GetMessageLevel0(
        GetMessageLevel1(command, query.ShortID), "");
      query.Message = Util.GetStringFromByteArray(query.Source);
      return query.Message;
    }

    /// <summary>
    /// Кодирование сообщения-кофигурации настроек FTP GPRS.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана кофигурации настроек FTP GPRS gprsFtpConfig</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта gprsFtpConfig</exception> 
    /// <param name="gprsFtpConfig">Настройки FTP GPRS</param>
    /// <returns>string  Настройки FTP GPRS</returns>
    public static string EncodeGprsFtpConfigSet(GprsFtpConfig gprsFtpConfig)
    {
      if (gprsFtpConfig == null)
        throw new ArgumentNullException("gprsFtpConfig",
          "Не переданы настройки FTP GPRS gprsFtpConfig");

      string vaidateErrorMessage;
      if (!gprsFtpConfig.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          gprsFtpConfig.GetType().Name, vaidateErrorMessage));

      gprsFtpConfig.CommandID = CommandDescriptor.GprsFtpConfigSet;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)gprsFtpConfig.CommandID, gprsFtpConfig.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsFtpConfig.Server, 25),
        command, index, 25);

      index += 25;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsFtpConfig.Login, 10),
        command, index, 10);

      index += 10;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsFtpConfig.Password, 10),
        command, index, 10);

      index += 10;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsFtpConfig.ConfigPath, 20),
        command, index, 20);

      index += 20;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsFtpConfig.PutPath, 20),
        command, index, 20);

      //2. Кодирование на уровне Level1 + Level0 
      gprsFtpConfig.Source = GetMessageLevel0(
        GetMessageLevel1(command, gprsFtpConfig.ShortID), "");
      gprsFtpConfig.Message = Util.GetStringFromByteArray(gprsFtpConfig.Source);
      return gprsFtpConfig.Message;
    }

    /// <summary>
    /// Кодирование сообщения-запроса настроек FTP GPRS.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан запрос кофигурации настроек Email GPRS</exception>  
    /// <param name="query">Простой запрос</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeGprsFtpConfigQuery(SimpleQuery query)
    {
      if (query == null)
        throw new ArgumentNullException("query",
          "Не передан запрос кофигурации настроек FTP GPRS");

      query.CommandID = CommandDescriptor.GprsFtpConfigQuery;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)query.CommandID, query.MessageID);
      //2. Кодирование на уровне Level1 + Level0
      query.Source = GetMessageLevel0(
        GetMessageLevel1(command, query.ShortID), "");
      query.Message = Util.GetStringFromByteArray(query.Source);
      return query.Message;
    }

    /// <summary>
    /// Кодирование сообщения-кофигурации настроек Socket GPRS.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана кофигурации настроек Socket GPRS gprsFtpConfig</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта gprsFtpConfig</exception> 
    /// <param name="gprsSocketConfig">Настройки Socket GPRS</param>
    /// <returns>string  Настройки Socket GPRS</returns>
    public static string EncodeGprsSocketConfigSet(GprsSocketConfig gprsSocketConfig)
    {
      if (gprsSocketConfig == null)
        throw new ArgumentNullException("gprsSocketConfig",
          "Не переданы настройки Socket GPRS gprsSocketConfig");

      string vaidateErrorMessage;
      if (!gprsSocketConfig.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          gprsSocketConfig.GetType().Name, vaidateErrorMessage));

      gprsSocketConfig.CommandID = CommandDescriptor.GprsSocketConfigSet;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)gprsSocketConfig.CommandID, gprsSocketConfig.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsSocketConfig.Server, 20),
        command, index, 20);

      index += 20;
      Util.FillCommandAttribute(
        Level4Converter.UShortToBytes(gprsSocketConfig.Port),
        command, index, 2);

      //2. Кодирование на уровне Level1 + Level0 
      gprsSocketConfig.Source = GetMessageLevel0(
        GetMessageLevel1(command, gprsSocketConfig.ShortID), "");
      gprsSocketConfig.Message = Util.GetStringFromByteArray(gprsSocketConfig.Source);
      return gprsSocketConfig.Message;
    }

    /// <summary>
    /// Кодирование сообщения-запроса настроек Socket GPRS.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан запрос кофигурации настроек Socket GPRS</exception>  
    /// <param name="query">Простой запрос</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeGprsSocketConfigQuery(SimpleQuery query)
    {
      if (query == null)
        throw new ArgumentNullException("query",
          "Не передан запрос кофигурации настроек Socket GPRS");

      query.CommandID = CommandDescriptor.GprsSocketConfigQuery;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)query.CommandID, query.MessageID);
      //2. Кодирование на уровне Level1 + Level0
      query.Source = GetMessageLevel0(
        GetMessageLevel1(command, query.ShortID), "");
      query.Message = Util.GetStringFromByteArray(query.Source);
      return query.Message;
    }

    /// <summary>
    /// Кодирование сообщения-кофигурации подключения к провайдеру GPRS.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передана кофигурации подключения к провайдеру GPRS gprsProviderConfig</exception> 
    /// <exception cref="A1Exception">Не пройдена проверка заполнения атрибутов объекта gprsProviderConfig</exception> 
    /// <param name="gprsProviderConfig">Настройки подключения к провайдеру GPRS</param>
    /// <returns>string Конфигурация подключения к провайдеру GPRS</returns>
    public static string EncodeGprsProviderConfigSet(GprsProviderConfig gprsProviderConfig)
    {
      if (gprsProviderConfig == null)
        throw new ArgumentNullException("gprsProviderConfig",
          "Не переданы настройки подключения к провайдеру GPRS gprsProviderConfig");

      string vaidateErrorMessage;
      if (!gprsProviderConfig.Validate(out vaidateErrorMessage))
        throw new A1Exception(String.Format(
          ErrorText.VALIDATE_ENTITY_ATTRIBUTES,
          gprsProviderConfig.GetType().Name, vaidateErrorMessage));

      gprsProviderConfig.CommandID = CommandDescriptor.GprsProviderConfigSet;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)gprsProviderConfig.CommandID, gprsProviderConfig.MessageID);

      int index = Const.LEVEL3_DATA_BLOCK_POSITION;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsProviderConfig.InitString, 50),
        command, index, 50);

      index += 50;
      Util.FillCommandAttribute(
        Level4Converter.StringToBytes(gprsProviderConfig.Domain, 25),
        command, index, 25);

      //2. Кодирование на уровне Level1 + Level0 
      gprsProviderConfig.Source = GetMessageLevel0(
        GetMessageLevel1(command, gprsProviderConfig.ShortID), "");
      gprsProviderConfig.Message = Util.GetStringFromByteArray(gprsProviderConfig.Source);
      return gprsProviderConfig.Message;
    }

    /// <summary>
    /// Кодирование сообщения-запроса настроек подключения к провайдеру GPRS.
    /// <para>В процессе выполнения заполняются поля Source и Message 
    /// переданного объекта (Message = результату выполнения метода).</para>
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан запрос настроек настроек подключения к провайдеру GPRS</exception>  
    /// <param name="query">Простой запрос</param>
    /// <returns>string сообщение телетреку</returns>
    public static string EncodeGprsProviderConfigQuery(SimpleQuery query)
    {
      if (query == null)
        throw new ArgumentNullException("query",
          "Не передан запрос настроек подключения к провайдеру GPRS");

      query.CommandID = CommandDescriptor.GprsProviderConfigQuery;
      //1. формирование 102 байт  - данные  + MessageID + CommandID 
      //   LEVEL3 + LEVEL4
      byte[] command = GetCommandLevel3Template(
        (byte)query.CommandID, query.MessageID);
      //2. Кодирование на уровне Level1 + Level0
      query.Source = GetMessageLevel0(
        GetMessageLevel1(command, query.ShortID), "");
      query.Message = Util.GetStringFromByteArray(query.Source);
      return query.Message;
    }
  }
}
