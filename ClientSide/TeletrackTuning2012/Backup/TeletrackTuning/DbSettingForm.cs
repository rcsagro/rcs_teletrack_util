﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TeletrackTuning.Properties;

namespace TeletrackTuning
{
  public partial class DbSettingForm : Form
  {
    public DbSettingForm()
    {
      InitializeComponent();

      textBoxDataBase.Text = Settings.Default.DataBase;
      textBoxServer.Text = Settings.Default.Server;
      textBoxUser.Text = Settings.Default.User;
      textBoxPassword.Text = Settings.Default.Password;
    }

    private void buttonCansel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void buttonApply_Click(object sender, EventArgs e)
    {
      Settings.Default.DataBase = textBoxDataBase.Text;
      Settings.Default.Server = textBoxServer.Text;
      Settings.Default.User = textBoxUser.Text;
      Settings.Default.Password = textBoxPassword.Text;
      Settings.Default.Save();
      Close();
    }
  }
}
