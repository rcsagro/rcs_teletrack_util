﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TeletrackTuning
{
  public partial class DeleteTeletrack : Form
  {
    public DeleteTeletrack(string tt_id)
    {
      InitializeComponent();
      textBoxMessage.Text = "You want delete ID = " + tt_id;
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void buttonDelete_Click(object sender, EventArgs e)
    {
      this.DialogResult = MessageBox.Show("Do You want to remove the teletrack from the database?", "ATTENTION", MessageBoxButtons.OKCancel);
      Close();
    }
  }
}
