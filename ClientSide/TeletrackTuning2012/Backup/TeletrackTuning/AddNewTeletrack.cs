﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace TeletrackTuning
{
  public partial class AddNewTeletrack : Form
  {
    public string shortId = "";
    public string name = "";
    public string description = "";

    public AddNewTeletrack()
    {
      InitializeComponent();
    }

    public AddNewTeletrack(string nameGropu)
    {
      InitializeComponent();
      groupBox1.Text = nameGropu;
    }

    private void AddNewTeletrack_Load(object sender, EventArgs e)
    {
      if (shortId != "")
      {
        textBoxShortID.Text = shortId;
        textBoxShortID.ReadOnly = true;
      }
      if (name != "")
        textBoxName.Text = name;
      else
        textBoxName.Text = "Teletrack "+ DateTime.Now.ToString();
      if (description != "")
        textBoxDesctiption.Text = description;
      else
        textBoxDesctiption.Text = "Desctiption for Teletrack registered at " + DateTime.Now.ToString(); 
    }

    private void buttonAddTtCancel_Click(object sender, EventArgs e)
    {
      Close();
      this.DialogResult = DialogResult.Cancel;
    }

    private void buttonAddTtOk_Click(object sender, EventArgs e)
    {
      shortId = textBoxShortID.Text;
      name = textBoxName.Text;
      description = textBoxDesctiption.Text;
      Close();
      this.DialogResult = DialogResult.OK;
    }
  }
}
