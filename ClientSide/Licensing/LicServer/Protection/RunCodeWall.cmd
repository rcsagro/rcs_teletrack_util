@ECHO OFF

echo Protecting ...
echo ---------------------------------------------------

call %1 CodeWall.cwx

echo ---------------------------------------------------
echo Copying additional files ...

xcopy /Y ..\..\..\Build\Licensing\Teletrack.LicServer\Init\*.* ..\..\..\Build\Licensing\Teletrack.LicServer\Protected\Init\
xcopy /Y ..\..\..\Build\Licensing\Teletrack.LicServer\x64\*.* ..\..\..\Build\Licensing\Teletrack.LicServer\Protected\x64\
xcopy /Y ..\..\..\Build\Licensing\Teletrack.LicServer\x86\*.* ..\..\..\Build\Licensing\Teletrack.LicServer\Protected\x86\
xcopy /Y ..\..\..\Build\Licensing\Teletrack.LicServer\Protected\Teletrack.LicServer.exe ..\..\..\Build\Licensing\Teletrack.LicServer\Protected\Srv\
