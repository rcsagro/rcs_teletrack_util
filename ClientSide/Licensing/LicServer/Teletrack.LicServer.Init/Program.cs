﻿using System.ServiceProcess;

namespace Teletrack.LicServer
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    static void Main()
    {
      ServiceBase[] ServicesToRun;
      ServicesToRun = new ServiceBase[] 
			{ 
				new TeletrackLicServer() 
			};
      ServiceBase.Run(ServicesToRun);
    }
  }
}
