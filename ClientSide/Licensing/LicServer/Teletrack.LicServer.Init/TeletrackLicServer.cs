﻿using System.ServiceProcess;

namespace Teletrack.LicServer
{
  public partial class TeletrackLicServer : ServiceBase
  {
    public TeletrackLicServer()
    {
      InitializeComponent();
    }

    protected override void OnStart(string[] args)
    {
    }

    protected override void OnStop()
    {
    }
  }
}
