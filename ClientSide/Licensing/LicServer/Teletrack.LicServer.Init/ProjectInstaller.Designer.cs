﻿namespace Teletrack.LicServer
{
  partial class ProjectInstaller
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.licServerProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
      this.serviceInstaller = new System.ServiceProcess.ServiceInstaller();
      // 
      // licServerProcessInstaller
      // 
      this.licServerProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
      this.licServerProcessInstaller.Password = null;
      this.licServerProcessInstaller.Username = null;
      // 
      // serviceInstaller
      // 
      this.serviceInstaller.Description = "Лицензирование системы Teletrack";
      this.serviceInstaller.ServiceName = "TeletrackLicServer";
      this.serviceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
      // 
      // ProjectInstaller
      // 
      this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.licServerProcessInstaller,
            this.serviceInstaller});

    }

    #endregion

    private System.ServiceProcess.ServiceProcessInstaller licServerProcessInstaller;
    private System.ServiceProcess.ServiceInstaller serviceInstaller;
  }
}