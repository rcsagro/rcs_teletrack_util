﻿using System;
using System.ServiceProcess;

namespace Teletrack.LicServer
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    static void Main()
    {
#if DEBUG
      if (Environment.UserInteractive)
      {
        Console.Title = "Teletrack License Service";

        using (TeletrackLicServer service = new TeletrackLicServer())
        {
          Console.WriteLine("Starting service...");
          service.Start();
          Console.Write("Started. Press any key to exit.");
          Console.ReadKey(true);
          service.Break();
        }
        Console.WriteLine("Service stoped");
      }
      else
      {
        ServiceBase.Run(new TeletrackLicServer());
      }
#else
      ServiceBase.Run(new TeletrackLicServer());
#endif
    }
  }
}