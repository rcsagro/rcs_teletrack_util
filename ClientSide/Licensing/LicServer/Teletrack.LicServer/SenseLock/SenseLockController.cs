﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using Robosoft.LicenseServer;
using Teletrack.Licensing;
using Teletrack.Licensing.Exceptions;
using Teletrack.LicServer.Config;
using Teletrack.LicServer.RoboLicServer;
using Teletrack.Logging;

namespace Teletrack.LicServer.SenseLock
{
  enum KeyState
  {
    NotFound, Searching, PresenceRcs, PresenceRob
  }

  /// <summary>
  /// SenseLock контроллер.
  /// </summary>
  internal class SenseLockController : ILicense
  {
    private KeyState StatusKey;
    private RoboController _roboController;
    private SenseLockHelper.SENSE4_CONTEXT _s4Handler;

    private ManualResetEvent _shutdown;
    private TimeSpan _delay;

    internal SenseLockController()
    {
      _delay = new TimeSpan(0, 0, 10);
      StatusKey = KeyState.Searching;
      _roboController = new RoboController();
    }

    internal event Action<bool> ConnectionStateChangedEvent;

    #region ILicense Members

    /// <summary>
    /// Получить лицензию.
    /// </summary>
    /// <param name="licNumber">Номер лицензии.</param>
    /// <returns>Лицензия в виде масива байт.</returns>
    public byte[] GetLicense(byte[] licNumber)
    {
      if (licNumber == null || licNumber.Length == 0)
      {
        throw new ArgumentNullException("licNumber");
      }

      byte[] result = null;

      switch (StatusKey)
      {
        case KeyState.PresenceRcs:
          result = GettingLicenseRcs(licNumber);
          break;
        case KeyState.PresenceRob:
          result = _roboController.ExecuteFunction(SenseLockHelper.FileLicGet, licNumber);
          break;
        default:
          throw new SenselockNotFoundException("Senselock key not found.");
      }

      if (result == null)
      {
        throw new SenselockException("Unable to obtain a license.");
      }

      return result;
    }

    #endregion

    internal void Start()
    {
      _shutdown = new ManualResetEvent(false);
      ConnectToSenselock();
    }

    internal void Stop()
    {
      StopRoboLicensing();
      _shutdown.Set();
    }

    private void HardwareKeyConnected(IAsyncResult ar)
    {
      AsyncResult result = (AsyncResult)ar;
      SenselockConnector connector = (SenselockConnector)result.AsyncDelegate;
      bool connected = connector.EndInvoke(ar);
      if (connected)
      {
        LogHelper.Info("Соединение с ключом SenseLock установлено");
        StatusKey = KeyState.PresenceRob;
        StartRoboLicensing();
      }
      else
      {
        StatusKey = KeyState.NotFound;
        LogHelper.Info("Нет соединения с ключом SenseLock");
        if (!_shutdown.WaitOne(_delay, true))
        {
          ConnectToSenselock();
        }
      }
    }

    private void StartRoboLicensing()
    {
      if (!_shutdown.WaitOne(1, true))
      {
        _roboController.StatusChangedEvent += OnRoboControllerStatusChanged;
        _roboController.StartLicenseService();
      }
    }

    private void StopRoboLicensing()
    {
      _roboController.Stop();
      _roboController.StatusChangedEvent -= OnRoboControllerStatusChanged;
    }

    private void OnRoboControllerStatusChanged(object sender, ServerEventArgs e)
    {
      if (e.ServerStarted)
      {
        LogHelper.InfoFormat("Сервер Robosoft запущен, используется порт {0}", ServerCfg.GetConfig().RobosoftPort);
      }
      else
      {
        LogHelper.FatalFormat("Сервер Robosoft отключен. {0}", e.Info);
      }
    }

    private void ConnectToSenselock()
    {
      do
      {
        if (StatusKey != KeyState.PresenceRob && StatusKey != KeyState.PresenceRcs)
        {
          try
          {
            SenseLockHelper.SENSE4_CONTEXT[] si = SenseLockHelper.GetS4Keys();
            LogHelper.InfoFormat("Найдено {0} ключей SenseLock.", si.Length);

            if (si.Length > 0)
            {
              StatusKey = KeyState.Searching;
              for (int i = 0; i < si.Length; i++)
              {
                string toLog = "Id key:";
                for (int j = 0; j < si[i].bID.Length; j++)
                  toLog += string.Format("{0,2:X2}", si[i].bID[j]);
                LogHelper.Info(toLog);
              }
              int indexRcsKey = SenseLockHelper.GetRcsKey(si);
              if (indexRcsKey >= 0)
              {
                LogHelper.Info("Ключ RCS найден");

                _s4Handler = si[indexRcsKey];
                if (InitializeRcsKey(ref _s4Handler) == 0)
                {//init Ok
                  StatusKey = KeyState.PresenceRcs;
                }
              }
              else
              {
                if (!_roboController.IsActive)
                {
                  _roboController.Start();
                }
                SenselockConnector connector = _roboController.ConnectToHardwareKey();
                connector.BeginInvoke(HardwareKeyConnected, null);
              }
            }
            else
            {
              StatusKey = KeyState.NotFound;
            }
          }
          catch (SenselockNotFoundException ex)
          {
            LogHelper.Error(ex.Message);
          }
          catch (Exception ex)
          {
            LogHelper.Error("SenseLock connection exception", ex);
          }
          finally
          {
            SenseLockHelper.S4Close(ref _s4Handler);
          }
        }
      }
      while (!_shutdown.WaitOne(_delay, true));
    }

    /// <summary>
    /// Открывае ключ и выполняем подготовительные операции для работы
    /// </summary>
    /// <param name="s4_hdl"></param>
    /// <returns>0-Ok, !0 - kod error</returns>
    private uint InitializeRcsKey(ref SenseLockHelper.SENSE4_CONTEXT s4_hdl)
    {
      uint ret = 0;
      //open the device
      ret = SenseLockHelper.S4Open(ref s4_hdl);
      if (0x00 != ret)
      {
        LogHelper.Error("Не удалось открыть ключ");
      }
      else
      {//переход в корневой каталог
        ret = SenseLockHelper.S4ChangeDir(ref s4_hdl, SenseLockHelper.WorkFolderName);
        if (0x00 != ret)
        {
          LogHelper.InfoFormat("Не удалось перейти в рабочий каталог. Ключ неотформатирован. {0}", ret);
        }
        else
        {
          ret = SenseLockHelper.S4VerifyPin(ref s4_hdl, SenseLockHelper.RcsUserPin, 8, SenseLockHelper.S4_USER_PIN);
          if (ret != 0)
          {
            LogHelper.ErrorFormat("Не верный ПИН пользователя. {0}", ret);
          }
        }
      }
      return ret;
    }

    /// <summary>
    /// Кодирует запрашиваемый номер лицензии
    /// </summary>
    /// <param name="numLic">запрашиваемый номер лицензии</param>
    /// <param name="buffer">буфер с случайным числом, для запроса лицензии: [1]...[5], 4Бт</param>
    /// <returns>первый байт масива [0], для запроса лицензии</returns>
    private byte KodeNumLic(int numLic, byte[] buffer)
    {
      byte numberLic = 0;
      numberLic = (byte)(((buffer[1] ^ buffer[4]) + (buffer[2] ^ buffer[3])) ^ numLic);
      return numberLic;
    }

    public int GetLicense(int licNum)
    {
      int ret = -1;
      switch (StatusKey)
      {
        case KeyState.PresenceRcs:
          ret = int.Parse(GetLicenseRcs(UInt32.Parse("0"), licNum));
          break;
        case KeyState.PresenceRob:
          ret = int.Parse(GetLicenseRobosoft(UInt32.Parse("0"), licNum));
          break;
        default:
          throw new SenselockNotFoundException("Senselock key not found.");
      }
      return ret;
    }

    private byte[] GettingLicenseRcs(byte[] licNumber)
    {
      byte[] retValue = null;
      uint BytesReturned = 0;
      uint lenCmd_getLic = 5;
      uint ret = 0;
      if (InitializeRcsKey(ref _s4Handler) == 0)
      {//init Ok
        ret = SenseLockHelper.S4Execute(ref _s4Handler, SenseLockHelper.FileLicGet, SenseLockHelper.inBuffer,
          lenCmd_getLic, SenseLockHelper.outBuffer, (uint)256, ref BytesReturned);
        if (ret == 0)
        {
          if (BytesReturned >= SenseLockHelper.LEN_LIC)
          {
            retValue = new byte[SenseLockHelper.LEN_LIC];
            Array.Copy(SenseLockHelper.outBuffer, retValue, retValue.Length);
          }
          else
          {
            LogHelper.ErrorFormat("Получена ошибка поиска лицензии. {0} ",
              Auxiliary.BytesToString(SenseLockHelper.outBuffer, 0, BytesReturned));
          }
        }
        else
        {
          LogHelper.ErrorFormat("Ошибка выполнения LicGet: {0}", ret);
        }
      }
      else
      {
        StatusKey = KeyState.Searching;
      }
      SenseLockHelper.S4Close(ref _s4Handler);
      return retValue;
    }

    private string GetLicenseRcs(UInt32 kriptRND, int numLic)
    {
      string textLic = "";
      uint BytesReturned = 0;
      uint lenCmd_getLic = 5;
      uint ret = 0;
      BitConverter.GetBytes(kriptRND).CopyTo(SenseLockHelper.inBuffer, 1);
      SenseLockHelper.inBuffer[0] = KodeNumLic(numLic, SenseLockHelper.inBuffer);
      if (InitializeRcsKey(ref _s4Handler) == 0)
      {//init Ok
        ret = SenseLockHelper.S4Execute(ref _s4Handler, SenseLockHelper.FileLicGet, SenseLockHelper.inBuffer,
          lenCmd_getLic, SenseLockHelper.outBuffer, (uint)256, ref BytesReturned);
        if (ret == 0)
        {
          if (BytesReturned >= SenseLockHelper.LEN_LIC)
          {
            for (int i = 1; i < SenseLockHelper.LEN_LIC; i++)
            {
              if (SenseLockHelper.outBuffer[i] != 0)
              {
                textLic += (char)SenseLockHelper.outBuffer[i];//string.Format("{0,2:X2}", Senselock.outBuffer[i]);
              }
              else
              {
                break;
              }
            }
            LogHelper.InfoFormat("Read Lic № {0}, value={1:s}", SenseLockHelper.outBuffer[0], textLic);
          }
          else
          {
            LogHelper.ErrorFormat("Получена ошибка поиска лицензии. :: " +
            Auxiliary.BytesToString(SenseLockHelper.outBuffer, 0, BytesReturned));
          }
        }
        else
        {
          SenseLockHelper.S4Close(ref _s4Handler);
          throw new SenselockException(string.Format("Ошибка выполнения LicGet(RCS): {0}", ret));
        }
      }
      else
      {
        StatusKey = KeyState.Searching;
      }
      SenseLockHelper.S4Close(ref _s4Handler);
      return textLic;
    }

    private string GetLicenseRobosoft(UInt32 kriptRND, int numLic)
    {
      string textLic = "";
      byte[] buffer = new byte[5];
      byte[] outBuffer;
      BitConverter.GetBytes(kriptRND).CopyTo(buffer, 1);
      buffer[0] = KodeNumLic(numLic, buffer);

      outBuffer = _roboController.ExecuteFunction(SenseLockHelper.FileLicGet, buffer);
      if ((outBuffer != null) && (outBuffer.Length >= SenseLockHelper.LEN_LIC))
      {
        for (int i = 1; i < SenseLockHelper.LEN_LIC; i++)
        {
          if (outBuffer[i] != 0)
          {
            textLic += (char)outBuffer[i];
          }
          else
          {
            break;
          }
        }
        LogHelper.DebugFormat("Read Lic № {0}, value={1:s}", outBuffer[0], textLic);
      }
      else if (_roboController.IsActive)
      {
        StatusKey = KeyState.NotFound;
        StopRoboLicensing();
        throw new SenselockNotFoundException("Не удалось прочить лицензию(Robosoft).");
      }
      else
      {
        throw new SenselockException("Не удалось прочить лицензию(Robosoft).");
      }
      return textLic;
    }
  }
}
