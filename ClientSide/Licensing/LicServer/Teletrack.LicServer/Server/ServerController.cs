﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using Teletrack.Licensing;
using Teletrack.LicServer.Config;
using Teletrack.LicServer.Server.Services;
using Teletrack.Logging;

namespace Teletrack.LicServer.Server
{
  /// <summary>
  /// Контроллер серверных сервисов.
  /// </summary>
  internal class ServerController
  {
    /// <summary>
    /// Хост сервиса трек-контролов.
    /// </summary>
    private ServiceHost _tcServiceHost;
    /// <summary>
    /// Хост сервиса дата-манагеров.
    /// </summary>
    private ServiceHost _tdmServiceHost;
    /// <summary>
    /// Объект, работающий с лицензиями.
    /// </summary>
    private ILicense _licenser;

    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <exception cref="ArgumentNullException">licenser = null.</exception>
    /// <param name="licenser">Объект, работающий с лицензиями.</param>
    public ServerController(ILicense licenser)
    {
      if (licenser == null)
      {
        throw new ArgumentNullException("licenser");
      }
      _licenser = licenser;
    }

    /// <summary>
    /// Запуск контроллера.
    /// </summary>
    internal void Start()
    {
      CreateTcServiceHost();
      CreateTdmServiceHost();
      TimeSpan timeout = TimeSpan.FromSeconds(10);
      _tcServiceHost.Open(timeout);
      _tdmServiceHost.Open(timeout);
      LogHelper.InfoFormat("Сервер лицензий запущен, используется порт {0}",
        ServerCfg.GetConfig().ServerPort);
    }

    /// <summary>
    /// Остановка контроллера. Обнуляем сервисы, т.к. после закрытия 
    /// пользоваться ими в дальнейшем не удасться, единственный путь -
    /// создать заново.
    /// </summary>
    internal void Stop()
    {
      if (_tcServiceHost != null)
      {
        if (_tcServiceHost.State == CommunicationState.Opened)
        {
          _tcServiceHost.Close();
        }
        else
        {
          _tcServiceHost.Abort();
        }
        _tcServiceHost = null;
      }

      if (_tdmServiceHost != null)
      {
        if (_tdmServiceHost.State == CommunicationState.Opened)
        {
          _tdmServiceHost.Close();
        }
        else
        {
          _tdmServiceHost.Abort();
        }
        _tdmServiceHost = null;
      }
    }

    private void OnFaulted(object sender, EventArgs e)
    {
      Stop();
      Start();
    }

    private void CreateTcServiceHost()
    {
      _tcServiceHost = new ServiceHost(new TcService(_licenser), GetTcUri());
      _tcServiceHost.AddServiceEndpoint(typeof(ILicenseService), CreateNetTcpBinding(), "");

      if (ServiceThrottlingBehaviorMissed(_tcServiceHost))
      {
        _tcServiceHost.Description.Behaviors.Add(CreateThrottlingBehavior());
      }
      _tcServiceHost.Faulted += new EventHandler(OnFaulted);
    }

    private void CreateTdmServiceHost()
    {
      _tdmServiceHost = new ServiceHost(new TdmService(_licenser), GetTdmUri());
      _tdmServiceHost.AddServiceEndpoint(typeof(ILicenseService), CreateNetTcpBinding(), "");

      if (ServiceThrottlingBehaviorMissed(_tdmServiceHost))
      {
        _tdmServiceHost.Description.Behaviors.Add(CreateThrottlingBehavior());
      }
      _tdmServiceHost.Faulted += new EventHandler(OnFaulted);
    }

    private Uri GetTcUri()
    {
      return LicServerUrlBuilder.GetSrvUriForTc(
        ServerCfg.GetConfig().Address, ServerCfg.GetConfig().ServerPort); ;
    }

    private Uri GetTdmUri()
    {
      return LicServerUrlBuilder.GetUriForTdm(
        ServerCfg.GetConfig().Address, ServerCfg.GetConfig().ServerPort);
    }

    private NetTcpContextBinding CreateNetTcpBinding()
    {
      NetTcpContextBinding binding = new NetTcpContextBinding();
      // Максимальное кол-во соединений оставляемое в пуле для дальнейшего использования.
      binding.MaxConnections = 2;
      // Максимальное кол-во соединений ожидающих в очереди.
      binding.ListenBacklog = 5;
      return binding;
    }
    /// <summary>
    /// Максимальное число одновременно обслуживаемых запросов = 200.
    /// </summary>
    private const int MAX_CONNECTIONS = 200;

    /// <summary>
    /// Создания настроек транспортного уровня. 
    /// </summary>
    /// <returns>IServiceBehavior.</returns>
    private IServiceBehavior CreateThrottlingBehavior()
    {
      ServiceThrottlingBehavior result = new ServiceThrottlingBehavior();
      result.MaxConcurrentCalls = MAX_CONNECTIONS;
      result.MaxConcurrentSessions = MAX_CONNECTIONS;
      return result;
    }

    /// <summary>
    /// Проверка того, что настройки ThrottlingBehavior не определнены,
    /// например, в конфигурационном файле.
    /// </summary>
    /// <param name="host">ServiceHost.</param>
    /// <returns>True - настройки ServiceThrottlingBehavior не определены.</returns>
    private bool ServiceThrottlingBehaviorMissed(ServiceHost host)
    {
      return host.Description.Behaviors.Find<ServiceThrottlingBehavior>() == null;
    }
  }
}
