using System;
using System.Collections.Generic;
using System.Linq;

namespace Teletrack.LicServer.Server.Sessions
{
  /// <summary>
  /// ����������� ������ - ������ ���������� � �������.
  /// </summary>
  internal class SessionRepository : ISessionRepository
  {
    /// <summary>
    /// �������� ������. ������������ ����� ������-�������� ������.
    /// </summary>
    private Dictionary<string, LicSession> _sessions;

    private object _syncObject;

    /// <summary>
    /// ���-�� ��������� � ���������.
    /// </summary>
    public int Count
    {
      get
      {
        lock (_syncObject)
        {
          return _sessions.Count;
        }
      }
    }

    internal SessionRepository()
    {
      _syncObject = new object();
      _sessions = new Dictionary<string, LicSession>();
    }

    /// <summary>
    /// ���������� ������ � �����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">Session is null.</exception>
    /// <param name="session">������.</param>
    public void Add(LicSession session)
    {
      if (session == null)
      {
        throw new ArgumentNullException("session");
      }
      lock (_syncObject)
      {
        _sessions.Add(session.SessionId, session);
      }
    }

    /// <summary>
    /// �������� ������.
    /// </summary>
    /// <exception cref="ArgumentNullException">Session is null.</exception>
    /// <exception cref="KeyNotFoundException">Can't update session,
    /// id was not found in the repository.</exception>
    /// <param name="session">������.</param>
    public void Update(LicSession session)
    {
      if (session == null)
      {
        throw new ArgumentNullException("session");
      }
      lock (_syncObject)
      {
        if (_sessions.ContainsKey(session.SessionId))
        {
          _sessions[session.SessionId] = session;
        }
        else
        {
          throw new KeyNotFoundException(string.Format(
            "Can't update session, id {0} was not found in the repository.", session.SessionId));
        }
      }
    }

    /// <summary>
    /// ������� ������.
    /// </summary>
    /// <exception cref="ArgumentNullException">SessionId is null or emprty.</exception>
    /// <param name="sessionId">������������� ������.</param>
    public void Delete(string sessionId)
    {
      if (string.IsNullOrEmpty(sessionId))
      {
        throw new ArgumentNullException("sessionId");
      }
      lock (_syncObject)
      {
        if (_sessions.ContainsKey(sessionId))
        {
          _sessions.Remove(sessionId);
        }
      }
    }

    /// <summary>
    /// ����� ������ �� ������.
    /// </summary>
    /// <exception cref="ArgumentNullException">Login is null or emprty.</exception>
    /// <param name="login">�����.</param>
    /// <returns>������ LicSession.</returns>
    public IList<LicSession> GetByLogin(string login)
    {
      if (string.IsNullOrEmpty(login))
      {
        throw new ArgumentNullException("login");
      }
      lock (_syncObject)
      {
        var result =
          from entry in _sessions
          where entry.Value.Login == login
          select entry.Value;

        return result.ToList<LicSession>();
      }
    }

    /// <summary>
    /// ����� ������ �� ��������������, ���������� ���� ��� null � ������ �������.
    /// </summary>
    /// <exception cref="ArgumentNullException">SessionId is null or emprty.</exception>
    /// <param name="sessionId">������������� ������.</param>
    /// <returns>LicSession ��� null.</returns>
    public LicSession GetById(string sessionId)
    {
      if (string.IsNullOrEmpty(sessionId))
      {
        throw new ArgumentNullException("sessionId");
      }
      lock (_syncObject)
      {
        return _sessions.ContainsKey(sessionId) ? _sessions[sessionId].Clone() : null;
      }
    }

    /// <summary>
    /// ������ ���� ������.
    /// </summary>
    /// <returns>������ LicSession.</returns>
    public IEnumerable<LicSession> GetAll()
    {
      lock (_syncObject)
      {
        foreach (var item in _sessions)
        {
          yield return item.Value;
        }
      }
    }

    /// <summary>
    /// ������ ���� ������, ����� �������� ���������.
    /// </summary>
    /// <returns>������ LicSession.</returns>
    public IList<LicSession> GetSnapshot()
    {
      lock (_syncObject)
      {
        return _sessions.Values.ToList<LicSession>();
      }
    }

    /// <summary>
    /// �������� ������� ����� � �������� ���������������.
    /// </summary>
    /// <exception cref="ArgumentNullException">SessionId is null or emprty.</exception>
    /// <param name="sessionId">������������� ������.</param>
    /// <returns>True - ������ �������.</returns>
    public bool ContainsSession(string sessionId)
    {
      if (string.IsNullOrEmpty(sessionId))
      {
        throw new ArgumentNullException("sessionId");
      }
      lock (_syncObject)
      {
        return _sessions.ContainsKey(sessionId);
      }
    }
  }
}