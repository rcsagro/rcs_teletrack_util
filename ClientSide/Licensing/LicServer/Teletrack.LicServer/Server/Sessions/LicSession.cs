﻿using System;
using System.Net;

namespace Teletrack.LicServer.Server.Sessions
{
  /// <summary>
  /// Информация о сессии.
  /// </summary>
  internal class LicSession
  {
    /// <summary>
    /// Идентификатор сессии.
    /// </summary>
    internal readonly string SessionId;
    /// <summary>
    /// Адрес клиента.
    /// </summary>
    internal readonly IPEndPoint ClientAddress;
    /// <summary>
    /// Логин (диспетчера).
    /// </summary>
    internal readonly string Login;
    /// <summary>
    /// Время последнего подключения.
    /// </summary>
    internal DateTime LastConnectionTime { get; set; }

    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="sessionId">Идентификатор сессии.</param>
    /// <param name="clientAddress">Адрес клиента.</param>
    /// <param name="login">Логин (диспетчера).</param>
    /// <param name="lastConnectionTime">Время последнего подключения.</param>
    internal LicSession(string sessionId, IPEndPoint clientAddress,
      string login, DateTime lastConnectionTime)
    {
      SessionId = sessionId;
      ClientAddress = clientAddress;
      Login = login;
      LastConnectionTime = lastConnectionTime;
    }

    internal LicSession Clone()
    {
      return new LicSession(SessionId, ClientAddress, Login, LastConnectionTime);
    }
  }
}
