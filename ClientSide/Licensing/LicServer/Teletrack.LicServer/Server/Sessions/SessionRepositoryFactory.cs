
namespace Teletrack.LicServer.Server.Sessions
{
  /// <summary>
  /// ������� ���������� ������������.
  /// </summary>
  internal class SessionRepositoryFactory
  {
    private static SessionRepository _tcSessionRepository = new SessionRepository();
    private static SessionRepository _tdmSessionRepository = new SessionRepository();

    /// <summary>
    /// ������ ����������� ����������� TDataMngr'��.
    /// </summary>
    /// <returns>SessionRepository TDataMngr'��.</returns>
    internal static SessionRepository GetTdmSessionRepository()
    {
      return _tdmSessionRepository;
    }

    /// <summary>
    /// ������ ����������� ����������� TrackControl'��.
    /// </summary>
    /// <returns>SessionRepository TrackControl'��.</returns>
    internal static SessionRepository GetTcSessionRepository()
    {
      return _tcSessionRepository;
    }
  }
}