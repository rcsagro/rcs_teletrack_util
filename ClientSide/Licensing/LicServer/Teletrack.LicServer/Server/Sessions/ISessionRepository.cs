using System.Collections.Generic;

namespace Teletrack.LicServer.Server.Sessions
{
  /// <summary>
  /// ��������� ������� � ����������� ������.
  /// </summary>
  internal interface ISessionRepository
  {
    /// <summary>
    /// ���-�� ��������� ���������.
    /// </summary>
    int Count { get; }

    /// <summary>
    /// ���������� ������ � �����������.
    /// </summary>
    /// <exception cref="ArgumentNullException">Session is null.</exception>
    /// <param name="session">������.</param>
    void Add(LicSession session);

    /// <summary>
    /// �������� ������.
    /// </summary>
    /// <exception cref="ArgumentNullException">Session is null.</exception>
    /// <exception cref="KeyNotFoundException">Can't update session,
    /// id was not found in the repository.</exception>
    /// <param name="session">������.</param>
    void Update(LicSession session);

    /// <summary>
    /// ������� ������.
    /// </summary>
    /// <exception cref="ArgumentNullException">SessionId is null or emprty.</exception>
    /// <param name="sessionId">������������� ������.</param>
    void Delete(string sessionId);

    /// <summary>
    /// ����� ������ �� ������.
    /// </summary>
    /// <exception cref="ArgumentNullException">Login is null or emprty.</exception>
    /// <param name="login">�����.</param>
    /// <returns>������ LicSession.</returns>
    IList<LicSession> GetByLogin(string login);

    /// <summary>
    /// ����� ������ �� ��������������.
    /// </summary>
    /// <exception cref="ArgumentNullException">SessionId is null or emprty.</exception>
    /// <param name="sessionId">������������� ������.</param>
    /// <returns>LicSession.</returns>
    LicSession GetById(string sessionId);

    /// <summary>
    /// ������ ���� ������.
    /// </summary>
    /// <returns>������ LicSession.</returns>
    IEnumerable<LicSession> GetAll();

    /// <summary>
    /// ������ ���� ������, ����� �������� ���������.
    /// </summary>
    /// <returns>������ LicSession</returns>
    IList<LicSession> GetSnapshot();

    /// <summary>
    /// �������� ������� ����� � �������� ���������������.
    /// </summary>
    /// <exception cref="ArgumentNullException">SessionId is null or emprty.</exception>
    /// <param name="sessionId">������������� ������.</param>
    /// <returns>True - ������ �������.</returns>
    bool ContainsSession(string sessionId);
  }
}