﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.Timers;
using Teletrack.Licensing;
using Teletrack.Licensing.Exceptions;
using Teletrack.LicServer.Properties;
using Teletrack.LicServer.Server.Sessions;
using Teletrack.Logging;
using Teletrack.WcfServices;

namespace Teletrack.LicServer.Server.Services
{
  internal class LicServiceBase : ILicensing
  {
    protected readonly ILicense KeyController;

    /// <summary>
    /// Время последнего успешного соединение с ключом.
    /// </summary>
    protected static DateTime LastTimeConnectionToKey;

    /// <summary>
    /// Таймер контроля "живых" сессий.
    /// </summary>
    private Timer _sessionTimer;

    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="senseLockController">Контроллер SenseLock.</param>
    public LicServiceBase(ILicense senseLockController)
    {
      KeyController = senseLockController;
      CreateTimer();
    }

    /// <summary>
    /// Интервал таймера: 1 мин.
    /// </summary>
    private const int TIMER_INTERVAL = 1 * 60 * 1000;
    /// <summary>
    /// Максимальный временной интервал сессии с момента последнего конекта, когда
    /// она еще считается "живой": 3 мин.
    /// </summary>
    private const int LIVE_SESSION_INTERVAL = 3 * 60 * 1000;

    private void CreateTimer()
    {
      _sessionTimer = new Timer(TIMER_INTERVAL);
      _sessionTimer.Elapsed += OnTimerElapsed;
      _sessionTimer.Start();
    }

    private void OnTimerElapsed(object sender, ElapsedEventArgs e)
    {
      List<string> deadSessionsId = new List<string>();
      // удаляем в два этапа: вначале собираем идентификаторы, затем удаляем.
      foreach (LicSession session in GetRepository().GetAll())
      {
        if (session.LastConnectionTime.AddMilliseconds(LIVE_SESSION_INTERVAL) < DateTime.Now)
        {
          deadSessionsId.Add(session.SessionId);
        }
      }

      deadSessionsId.ForEach(delegate(String id)
      {
        DeleteSession(id);
        LogHelper.DebugFormat("Session of {0} {1} was marked as unused and then deleted.",
          GetClientShortName(), id.GetHashCode());
      });
    }

    #region ILicense Members

    /// <summary>
    /// Получить лицензию.
    /// </summary>
    /// <param name="licNumber">Номер лицензии.</param>
    /// <returns>Лицензия в виде масива байт.</returns>
    public byte[] GetLicense(byte[] licNumber)
    {
      UpdateCurrentSessionTime();
      byte[] result = KeyController.GetLicense(licNumber);
      SetLastConnectionTimeToKey();
      return result;
    }

    #endregion

    /// <summary>
    /// Сессионный репозиторий. Должен быть перекрыт в классах-наследниках.
    /// </summary>
    /// <exception cref="NotImplementedException"></exception>
    /// <returns>SessionRepository</returns>
    protected virtual SessionRepository GetRepository()
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Создание новой сессии и сохранение в репозитарии.
    /// </summary>
    /// <param name="login"></param>
    protected void CreateNewSession(string login)
    {
      string sessionId = Extractor.GetSessionIdFromContext(OperationContext.Current);
      IPEndPoint ipAddress = Extractor.GetClientIpFromContext(OperationContext.Current);

      GetRepository().Add(new LicSession(sessionId, ipAddress, login, DateTime.Now));
    }

    /// <summary>
    /// Удалить из репозитария информацию о текущей сессии.
    /// </summary>
    protected void DeleteCurrentSession()
    {
      GetRepository().Delete(Extractor.GetSessionIdFromContext(OperationContext.Current));
    }

    /// <summary>
    /// Удалить заданную сессию из репозитария.
    /// </summary>
    /// <exception cref="ArgumentNullException">SessionId is null or emprty.</exception>
    /// <param name="sessionId">Идентификатор сессии.</param>
    internal void DeleteSession(string sessionId)
    {
      GetRepository().Delete(sessionId);
    }

    /// <summary>
    /// Обновление времени последнего обращения по текущей сессии.
    /// </summary>
    protected void UpdateCurrentSessionTime()
    {
      try
      {
        SessionRepository repository = GetRepository();
        LicSession session = repository.GetById(
          Extractor.GetSessionIdFromContext(OperationContext.Current));
        if (session == null)
        {
          throw new SessionNotFoundException(Resources.SessionNotFoundClosed);
        }

        session.LastConnectionTime = DateTime.Now;
        repository.Update(session);
      }
      catch (KeyNotFoundException ex)
      {
        throw new SessionNotFoundException(string.Format(
          "Update session of {0}: {1}. {2}",
          GetClientShortName(), Resources.SessionNotFoundClosed, ex.Message));
      }
    }

    /// <summary>
    /// Получить из ключа значение Max разрешенного кол-ва Trackcontrol'ов.
    /// </summary>
    /// <returns>Max разрешенное кол-во Trackcontrol'ов.</returns>
    protected int GetMaxAllowedConnections()
    {
      int result = KeyController.GetLicense(LicenseNumbers.MAX_TRACK_CTR);
      SetLastConnectionTimeToKey();
      return result;
    }

    private void SetLastConnectionTimeToKey()
    {
      LastTimeConnectionToKey = DateTime.Now;
    }

    /// <summary>
    /// Временной интервал, по истечению которого необходимо
    /// проверить наличие ключа: 60 сек.
    /// </summary>
    private const int KEY_CHECK_INTERVAL = 60;

    /// <summary>
    /// Проверка следует ли выполнить обращение к ключу.
    /// </summary>
    /// <returns>True - следует обратиться к ключу.</returns>
    protected bool MustCheckKey()
    {
      return LastTimeConnectionToKey.AddSeconds(KEY_CHECK_INTERVAL) <= DateTime.Now;
    }

    /// <summary>
    /// Короткое наименование класса обслуживаемого клиента (для нужд логирования).
    /// </summary>
    /// <returns></returns>
    protected virtual string GetClientShortName()
    {
      throw new NotImplementedException();
    }
  }
}
