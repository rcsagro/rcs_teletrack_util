﻿using System;
using System.ServiceModel;
using Teletrack.Licensing;
using Teletrack.Licensing.Exceptions;
using Teletrack.LicServer.Properties;
using Teletrack.LicServer.Server.Services.Extensions;
using Teletrack.LicServer.Server.Sessions;

namespace Teletrack.LicServer.Server.Services
{
  /// <summary>
  /// Сервис обслуживания TrackControl.
  /// </summary>
  [ServiceBehavior(
    InstanceContextMode = InstanceContextMode.Single,
    ConcurrencyMode = ConcurrencyMode.Multiple,
    IncludeExceptionDetailInFaults = false)]
  [DispatchBehaviorTc]
  internal class TcService : LicServiceBase, ILicenseService
  {
    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="senseLockController">Контроллер SenseLock.</param>
    public TcService(ILicense senseLockController)
      : base(senseLockController)
    {
    }

    #region ISessionControl Members

    /// <summary>
    /// Открытие сессии.
    /// </summary>
    public void OpenSession(string login)
    {
      if (string.IsNullOrEmpty(login))
      {
        throw new ArgumentNullException(string.Format(
          "Login of {0} must be sent.", Resources.TcShortName));
      }

      CheckMaxConnectionLimit();
      CreateNewSession(login);
    }

    /// <summary>
    /// Закрытие сессии.
    /// </summary>
    public void CloseSession()
    {
      DeleteCurrentSession();
    }

    /// <summary>
    /// Уведомление о том что сессия еще нужна клиенту.
    /// </summary>
    public void NotifySessionAlive()
    {
      if (MustCheckKey())
      {
        // Просто проверка наличия ключа.
        GetMaxAllowedConnections();
      }
      UpdateCurrentSessionTime();
    }

    #endregion

    /// <summary>
    /// Проверка не достигли ли мы максимального разрешенного кол-ва коннектов.
    /// </summary>
    private void CheckMaxConnectionLimit()
    {
      int maxAllowedConnections = GetMaxAllowedConnections();

      if (maxAllowedConnections <= 0)
      {
        throw new SenselockException(
          "The value оf max allowed connections not defined. Please check you have the right SenseLock key.");
      }

      if (GetRepository().Count >= maxAllowedConnections)
      {
        throw new TooManyConnectionsException(
          "Maximum allowed connections was established on the license server. Please try again later.");
      }
    }

    protected override SessionRepository GetRepository()
    {
      return SessionRepositoryFactory.GetTcSessionRepository();
    }

    protected override string GetClientShortName()
    {
      return Resources.TcShortName;
    }
  }
}
