﻿using System;
using System.ServiceModel;
using Teletrack.Licensing;
using Teletrack.Licensing.Exceptions;
using Teletrack.LicServer.Properties;
using Teletrack.LicServer.Server.Services.Extensions;
using Teletrack.LicServer.Server.Sessions;

namespace Teletrack.LicServer.Server.Services
{
  /// <summary>
  /// Сервис обслуживания TDataManager.
  /// </summary>
  [ServiceBehavior(
    InstanceContextMode = InstanceContextMode.Single,
    ConcurrencyMode = ConcurrencyMode.Multiple,
    IncludeExceptionDetailInFaults = true)]
  [DispatchBehaviorTdm]
  internal class TdmService : LicServiceBase, ILicenseService
  {
    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="senseLockController">Контроллер SenseLock.</param>
    public TdmService(ILicense senseLockController)
      : base(senseLockController)
    {
    }

    #region ISessionControl Members

    public void OpenSession(string login)
    {
      if (string.IsNullOrEmpty(login))
      {
        throw new ArgumentNullException(string.Format(
          "Login of {0} must be sent.", Resources.TdmShortName));
      }

      CheckUniqueLogin(login);
      CreateNewSession(login);
    }

    public void CloseSession()
    {
      DeleteCurrentSession();
    }

    public void NotifySessionAlive()
    {
      UpdateCurrentSessionTime();
    }

    #endregion

    /// <summary>
    /// Проверка на уникальность login. Не может быть запущено несколько
    /// дата-менеджеров с одинаковым логином.
    /// </summary>
    /// <param name="login">Логин.</param>
    private void CheckUniqueLogin(string login)
    {
      if (GetRepository().GetByLogin(login).Count > 0)
      {
        throw new UniqueLoginException(string.Format(
          "Session with {0} login '{1}' alredy exists.Check running services in your network.",
          Resources.TdmShortName, login));
      }
    }

    protected override SessionRepository GetRepository()
    {
      return SessionRepositoryFactory.GetTdmSessionRepository();
    }

    protected override string GetClientShortName()
    {
      return Resources.TdmShortName;
    }
  }
}
