﻿using Teletrack.LicServer.Properties;
using Teletrack.LicServer.Server.Sessions;

namespace Teletrack.LicServer.Server.Services.Extensions
{
  /// <summary>
  /// Инспектор сообщений приходящих от TrackControl.
  /// </summary>
  internal class TcMessageInspector : BaseMessageInspector
  {
    protected override SessionRepository GetRepository()
    {
      return SessionRepositoryFactory.GetTcSessionRepository();
    }

    protected override string GetClientShortName()
    {
      return Resources.TcShortName;
    }
  }
}
