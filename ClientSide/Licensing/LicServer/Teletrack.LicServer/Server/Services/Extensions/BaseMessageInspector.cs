﻿using System;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Teletrack.LicServer.Server.Sessions;
using Teletrack.Logging;
using Teletrack.WcfServices;

namespace Teletrack.LicServer.Server.Services.Extensions
{
  /// <summary>
  /// Базовый класс инспекторов сообщений.
  /// </summary>
  internal class BaseMessageInspector : IDispatchMessageInspector
  {
    #region IDispatchMessageInspector Members

    /// <summary>
    /// Действия, выполняемые сразу после получения сообщения от трэкконтрола.
    /// </summary>
    /// <param name="request">Message</param>
    /// <param name="channel">IClientChannel</param>
    /// <param name="instanceContext">InstanceContext</param>
    /// <returns></returns>
    public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
    {
      HandleRequest(ref request);
      return instanceContext;
    }

    /// <summary>
    /// Действия, выполняемые перед отправкой ответа трэкконтролу.
    /// На данный момент делать нечего.
    /// </summary>
    /// <param name="reply">Message</param>
    /// <param name="correlationState">object</param>
    public void BeforeSendReply(ref Message reply, object correlationState)
    {
    }

    #endregion

    /// <summary>
    /// Обработка клинтского запроса.
    /// </summary>
    /// <param name="request">Message.</param>
    private void HandleRequest(ref Message request)
    {
      ServiceEndpoint endpoint = Extractor.GetServiseEndpoint(OperationContext.Current);
      if (endpoint == null)
      {
        throw new Exception("Endpoint wasn't been found. Please check connection configuration.");
      }

      string sessionId = Extractor.GetSessionIdFromContext(OperationContext.Current);
      string action = Extractor.GetMethodNameFromActionHeader(request.Headers);
      IPEndPoint ipAddress = Extractor.GetClientIpFromContext(OperationContext.Current);

      LogHelper.InfoFormat(String.Format("{0} client {1} calls action {2}, session: {3}.",
        GetClientShortName(), ipAddress, action, sessionId.GetHashCode()));

      MessageInspectorHelper.CheckProtocolVersion(Extractor.GetProtocolVersionFromContext(OperationContext.Current));

      MethodInfo mi = Extractor.FindMethodByAction(endpoint.Contract, action);
      bool miInitSession = MessageInspectorHelper.IsInitSessionMethod(mi);
      bool miCloseSession = MessageInspectorHelper.IsCloseSessionMethod(mi);

      // Проверка сессии осуществляется во всех методах, кроме открытия сессии.
      if (!miInitSession)
      {
        MessageInspectorHelper.CheckSessionExists(GetRepository(), sessionId, ipAddress);
      }
    }

    /// <summary>
    /// Сессионный репозиторий. Должен быть перекрыт в классах-наследниках.
    /// </summary>
    /// <returns>SessionRepository</returns>
    protected virtual SessionRepository GetRepository()
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Короткое наименование типа обслуживаемого клиента (для нужд логирования).
    /// </summary>
    /// <returns></returns>
    protected virtual string GetClientShortName()
    {
      throw new NotImplementedException();
    }
  }
}
