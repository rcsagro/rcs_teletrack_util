﻿using Teletrack.LicServer.Properties;
using Teletrack.LicServer.Server.Sessions;

namespace Teletrack.LicServer.Server.Services.Extensions
{
  /// <summary>
  /// Инспектор сообщений приходящих от TDataManager.
  /// </summary>
  internal class TdmMessageInspector : BaseMessageInspector
  {
    protected override SessionRepository GetRepository()
    {
      return SessionRepositoryFactory.GetTdmSessionRepository();
    }

    protected override string GetClientShortName()
    {
      return Resources.TdmShortName;
    }
  }
}
