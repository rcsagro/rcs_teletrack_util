﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Teletrack.WcfServices;

namespace Teletrack.LicServer.Server.Services.Extensions
{
  /// <summary>
  /// Атрибут, предназначенный для назначения сетевым сервисам 
  /// инспектора сообщений TcMessageInspector и обработчика событий.
  /// (<see cref="Teletrack.LicServer.Server.Services.Extensions.TcMessageInspector"/>).
  /// </summary>
  [AttributeUsage(AttributeTargets.Class)]
  public class DispatchBehaviorTcAttribute : Attribute, IServiceBehavior
  {
    #region IServiceBehavior Members

    /// <summary>
    /// Provides the ability to change run-time property values or insert custom
    /// extension objects such as error handlers, message or parameter interceptors,
    /// security extensions, and other custom extension objects.
    /// </summary>
    /// <param name="serviceDescription">ServiceDescription</param>
    /// <param name="serviceHostBase">ServiceHostBase</param>
    public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
      IDispatchMessageInspector inspector = new TcMessageInspector();
      IErrorHandler errorHandler = new ErrorHandler();

      foreach (ChannelDispatcherBase dispatcherBase in serviceHostBase.ChannelDispatchers)
      {
        ChannelDispatcher dispatcher = dispatcherBase as ChannelDispatcher;
        foreach (EndpointDispatcher endpointDispatcher in dispatcher.Endpoints)
        {
          endpointDispatcher.DispatchRuntime.MessageInspectors.Add(inspector);
        }
        dispatcher.ErrorHandlers.Add(errorHandler);
      }
    }

    /// <summary>
    /// Действия не определены.
    /// </summary>
    /// <param name="serviceDescription">ServiceDescription</param>
    /// <param name="serviceHostBase">ServiceHostBase</param>
    /// <param name="endpoints">Collection of ServiceEndpoints</param>
    /// <param name="bindingParameters">BindingParameterCollection</param>
    public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase,
      Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
    {
    }

    /// <summary>
    /// Действия не определены.
    /// </summary>
    /// <param name="serviceDescription">ServiceDescription</param>
    /// <param name="serviceHostBase">ServiceHostBase</param>
    public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
    }

    #endregion
  }
}
