﻿using System;
using System.Linq;
using System.Net;
using System.Reflection;
using Teletrack.Licensing;
using Teletrack.Licensing.Exceptions;
using Teletrack.LicServer.Properties;
using Teletrack.LicServer.Server.Sessions;
using Teletrack.WcfServices;

namespace Teletrack.LicServer.Server.Services.Extensions
{
  /// <summary>
  /// Проверяет различные аспекты, связанные с анализом клиентских сообщений.
  /// </summary>
  internal static class MessageInspectorHelper
  {
    /// <summary>
    /// Проверка соответствия версии протокола клиента.
    /// </summary>
    /// <exception cref="ArgumentNullException">Не пердана версия.</exception>
    /// <exception cref="ProtocolVersionException">Wrong protocol version.</exception>
    /// <param name="ver">Версия протокола клиента.</param>
    internal static void CheckProtocolVersion(Version ver)
    {
      if (ver == null)
      {
        throw new ArgumentNullException("ver");
      }
      if (ver != ContextParams.ProtocolVersion)
      {
        throw new ProtocolVersionException(Resources.ProtocolVersionMismatch);
      }
    }

    /// <summary>
    /// Проверка зарегистрирована ли сессия с заданным id для клиента с интересующим нас адресом.
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передан репозитарий или идентификатор сессии.</exception>
    /// <exception cref="SessionNotFoundException">Session not found or closed or 
    /// registered client address and session address are diferent.</exception>
    /// <param name="repository">SessionRepository.</param>
    /// <param name="sessionId">Идентификатор сессии.</param>
    /// <param name="clntIpAddress">Клиентский IP адрес.</param>
    internal static void CheckSessionExists(SessionRepository repository, string sessionId, IPEndPoint clntIpAddress)
    {
      if (repository == null)
      {
        throw new ArgumentNullException("repository");
      }
      if (string.IsNullOrEmpty(sessionId))
      {
        throw new ArgumentNullException("sessionId");
      }

      LicSession session = repository.GetById(sessionId);
      if (session == null)
      {
        throw new SessionNotFoundException(Resources.SessionNotFoundClosed);
      }

      if (!session.ClientAddress.Address.Equals(clntIpAddress.Address))
      {
        throw new SessionNotFoundException(Resources.IpAddressesMismatch); ;
      }
    }

    /// <summary>
    /// Определение является ли метод инициализатором сессии.
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передано описание метода.</exception>
    /// <param name="mi">MethodInfo</param>
    /// <returns>True - метод инициализирует сессию.</returns>
    internal static bool IsInitSessionMethod(MethodInfo mi)
    {
      if (mi == null)
      {
        throw new ArgumentNullException("mi");
      }
      return mi.GetCustomAttributes(typeof(InitSessionAttribute), false).Count() != 0;
    }

    /// <summary>
    /// Определение является ли метод закрывающим сессию.
    /// </summary>
    /// <exception cref="ArgumentNullException">Не передано описание метода.</exception>
    /// <param name="mi">MethodInfo</param>
    /// <returns>True - метод закрывает сессию.</returns>
    internal static bool IsCloseSessionMethod(MethodInfo mi)
    {
      if (mi == null)
      {
        throw new ArgumentNullException("mi");
      }
      return mi.GetCustomAttributes(typeof(CloseSessionAttribute), false).Count() != 0;
    }
  }
}
