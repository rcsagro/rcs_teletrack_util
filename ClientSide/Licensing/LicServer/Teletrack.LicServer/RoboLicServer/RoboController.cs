﻿using System;
using Robosoft.LicenseServer;
using Teletrack.Licensing;
using Teletrack.LicServer.Config;
using Teletrack.Logging;

namespace Teletrack.LicServer.RoboLicServer
{
  internal class RoboController
  {
    private LicenseService _roboService;

    internal event EventHandler<ServerEventArgs> StatusChangedEvent;

    internal bool IsActive
    {
      get;
      private set;
    }

    internal RoboController()
    {
    }

    internal void Start()
    {
      _roboService = LicenseService.Instance;
      _roboService.ConnectionAdded += OnServerConnectionAdded;
      _roboService.ConnectionRemoved += OnServiceConnectionRemoved;
      IsActive = true;
    }

    internal void Stop()
    {
      if (!IsActive)
      {
        return;
      }

      IsActive = false;
      _roboService.DisconnectAllConnections("Остановка сервера лицензирования");
      _roboService.DisconnectFromHardwareKey();
      _roboService.StopLicenseService();
      _roboService.ConnectionAdded -= OnServerConnectionAdded;
      _roboService.ConnectionRemoved -= OnServiceConnectionRemoved;
    }

    internal byte[] ExecuteFunction(string fileName, byte[] data)
    {
      return _roboService.ExecuteFunction(fileName, data);
    }

    internal SenselockConnector ConnectToHardwareKey()
    {
      return new SenselockConnector(_roboService.ConnectToHardwareKey);
    }

    internal void StartLicenseService()
    {
      _roboService.StartLicenseService(
        LicServerUrlBuilder.RS_VIRT_CATALOG,
        ServerCfg.GetConfig().RobosoftPort,
        OnStatusChanged);
    }

    private void OnStatusChanged(object sender, ServerEventArgs e)
    {
      if (StatusChangedEvent != null)
      {
        StatusChangedEvent(sender, e);
      }
    }

    private void OnServiceConnectionRemoved(object sender, ConnectionEventArgs e)
    {
      IConnection conn = e.Connection;
      LogHelper.InfoFormat("Отключен клиент: {0}, IP:{1}, Session:{2}; Reason:{3}; Info: \"{4}\"",
        conn.Name, conn.Address, conn.SessionId, e.Reason, e.Info);
    }

    private void OnServerConnectionAdded(object sender, ConnectionEventArgs e)
    {
      IConnection conn = e.Connection;
      LogHelper.InfoFormat("Подключился клиент: {0}, IP:{1}, Session:{2}; Reason:{3}; Info: \"{4}\"",
        conn.Name, conn.Address, conn.SessionId, e.Reason, e.Info);
    }
  }
}
