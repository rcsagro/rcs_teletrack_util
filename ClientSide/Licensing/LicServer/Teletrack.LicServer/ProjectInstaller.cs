﻿using System.ComponentModel;
using System.Configuration.Install;

namespace Teletrack.LicServer
{
  [RunInstaller(true)]
  public partial class ProjectInstaller : Installer
  {
    public ProjectInstaller()
    {
      InitializeComponent();
    }
  }
}
