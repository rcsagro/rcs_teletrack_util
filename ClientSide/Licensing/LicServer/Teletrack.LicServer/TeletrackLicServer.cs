﻿using System;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;
using NLog.Config;
using Teletrack.LicServer.Config;
using Teletrack.LicServer.SenseLock;
using Teletrack.LicServer.Server;
using Teletrack.Logging;

namespace Teletrack.LicServer
{
  class TeletrackLicServer : ServiceBase
  {
    /// <summary>
    /// Дополнительное время необходимое для
    /// старта и завершения работы - 3 минуты
    /// </summary>
    private const int ADDITIONAL_TIME = 3 * 60 * 1000;

    private ServerCfg _config;
    /// <summary>
    /// Контроллер для SenseLock.
    /// </summary>
    private SenseLockController _senseLockController;
    /// <summary>
    /// Поток в котором работает контроллер для SenseLockа.
    /// </summary>
    private Thread _senseLockControllerThread;
    /// <summary>
    /// Контроллер серверных сервисов.
    /// </summary>
    private ServerController _serverController;

    public TeletrackLicServer()
    {
      AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
      _config = ServerCfg.GetConfig();

      // Регистрация нашего логера
      ConfigurationItemFactory.Default.Targets.RegisterDefinition(
        "RollingDayFile", typeof(Teletrack.Logging.RollingDayFileTarget));

      _senseLockController = new SenseLockController();
      _senseLockController.ConnectionStateChangedEvent += OnSenseLockControllerStateChanged;
      _senseLockControllerThread = new Thread(new ThreadStart(_senseLockController.Start));

      _serverController = new ServerController(_senseLockController);
    }

    private void InitializeComponent()
    {
      this.CanShutdown = true;
      this.ServiceName = "TeletrackLicServer";
    }

    protected override void OnStart(string[] args)
    {
      RequestAdditionalTime(ADDITIONAL_TIME);
      base.OnStart(args);
      Start();
    }

    internal void Start()
    {
      LogHelper.Info("-----------------------------------------------------");
      LogHelper.Info(System.Reflection.Assembly.GetCallingAssembly().FullName);
      LogHelper.Info("Служба запущена.");

      _senseLockControllerThread.Start();
      _serverController.Start();
    }

    protected override void OnStop()
    {
      RequestAdditionalTime(ADDITIONAL_TIME);
      Break();
      base.OnStop();
    }

    /// <summary>
    /// Выход из системы
    /// </summary>
    protected override void OnShutdown()
    {
      LogHelper.Info("OS SHUTDOWN");
      Break();
      base.OnShutdown();
    }

    internal void Break()
    {
      _serverController.Stop();
      _senseLockController.Stop();
      LogHelper.Info("Служба остановлена.\r\n");
    }

    private void OnSenseLockControllerStateChanged(bool conState)
    {
      LogHelper.DebugFormat("State {0}", conState);
    }

    /// <summary>
    /// Код завершения работы 1066, сообщающий о специфической для
    /// данного приложения ошибке
    /// </summary>
    private const int ERROR_SERVICE_SPECIFIC_ERROR = 1066;

    void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
    {
      // Установим код выхода "специфическая ошибка"
      // 1066L ERROR_SERVICE_SPECIFIC_ERROR - 
      // The service has returned a service-specific error code.
      // Установить же serviceSpecificExitCode не удается, т.к.
      // это свойство скрыто.
      ExitCode = ERROR_SERVICE_SPECIFIC_ERROR;

      Exception ex = (Exception)e.ExceptionObject;
      string errorMessage = String.Format("Fatal Error! {0}", ex);

      try
      {
        LogHelper.Fatal("Необработанное исключение", ex);
      }
      finally
      {
        ShowException(errorMessage);
        Break();
      }
    }

    private delegate DialogResult ShowMsgDelegate(
     string text,
     string caption,
     MessageBoxButtons buttons,
     MessageBoxIcon icon,
     MessageBoxDefaultButton defaultButton,
     MessageBoxOptions options);

    /// <summary>
    /// Показываем асинхронный диалог с критической ошибкой.
    /// </summary>
    /// <param name="errorMessage">сообщение</param>
    private void ShowException(string errorMessage)
    {
      new ShowMsgDelegate(MessageBox.Show).BeginInvoke(
        errorMessage, String.Format("Служба {0}", ServiceName),
        MessageBoxButtons.OK,
        MessageBoxIcon.Error,
        MessageBoxDefaultButton.Button1,
        MessageBoxOptions.ServiceNotification,
        null, null);
    }
  }
}
