@ECHO OFF

REM The following directory is for .NET 2.0
set DOTNETFX2=%SystemRoot%\Microsoft.NET\Framework\v2.0.50727
set PATH=%PATH%;%DOTNETFX2%

echo Installing Teletrack.Licensing.exe
echo ---------------------------------------------------
InstallUtil /i Teletrack.LicServer.exe
echo ---------------------------------------------------
echo Done.