@ECHO OFF

REM The following directory is for .NET 2.0
SET DOTNETFX2=%SystemRoot%\Microsoft.NET\Framework\v2.0.50727
SET PATH=%PATH%;%DOTNETFX2%

echo Copying initializer...

copy /Y Init\Teletrack.LicServer.exe

echo ---------------------------------------------------

IF ERRORLEVEL 1 (
  echo Deinstallation error. Unable to replace the Teletrack.Licensing.exe.
  echo Please check service is stopped.
) ELSE (
  echo Deinstalling Teletrack.Licensing.exe...

  InstallUtil /u Teletrack.LicServer.exe

  echo ---------------------------------------------------
  echo Done.  
)
pause