@ECHO OFF

REM The following directory is for .NET 2.0
SET DOTNETFX2=%SystemRoot%\Microsoft.NET\Framework\v2.0.50727
SET PATH=%PATH%;%DOTNETFX2%

echo Copying initializer...

copy /Y Init\Teletrack.LicServer.exe

echo ---------------------------------------------------

IF ERRORLEVEL 1 (
  echo Installation error. Unable to copy the initializer Teletrack.Licensing.exe
) ELSE (
  echo Installing Teletrack.Licensing.exe...

  InstallUtil /i Teletrack.LicServer.exe

  echo ---------------------------------------------------
  echo Copying service...

  copy /Y Srv\Teletrack.LicServer.exe

  echo ---------------------------------------------------

  IF ERRORLEVEL 1 (
    echo Installation error. Unable to copy the service Teletrack.Licensing.exe
    echo Please replace Teletrack.Licensing.exe with the similar file from the Srv folder.
  ) ELSE (
    echo Copying sense4.dll ...

    IF EXIST "%programfiles(x86)%" (
      copy /Y x64\sense4.dll
    ) else (
      copy /Y x86\sense4.dll
    )

    echo ---------------------------------------------------

    IF ERRORLEVEL 1 (
      echo Installation error. Unable to copy the sense4.dll
      echo Please replace sense4.dll with the similar file from the x64 or x86 folder.
    ) else (
      echo Done.
    )
  )
)
pause