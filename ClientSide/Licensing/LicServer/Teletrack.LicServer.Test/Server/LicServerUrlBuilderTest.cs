﻿using System;
using NUnit.Framework;
using Teletrack.Licensing;

namespace Teletrack.LicServer.Test
{
  [TestFixture, Description("Тестирование построителя строк подключения к серверу лицензий.")]
  public class LicServerUrlBuilderTest
  {
    [Test, Description("Проверка формирования строки подключения к серверу лицензий для TrackControl'ов.")]
    public void CheckSrvUrlForTc()
    {
      Assert.AreEqual("net.tcp://localhost:9000/Lic/TC",
        LicServerUrlBuilder.GetSrvUriForTc("localhost", 9000).ToString());
    }

    [Test, Description("Проверка формирования строки подключения к серверу Robosoft для TrackControl'ов.")]
    public void CheckRoboUrlForTc()
    {
      Assert.AreEqual("http://localhost:100/Lic/RS",
        LicServerUrlBuilder.GetRoboUriForTc("localhost", 100).ToString());
    }

    [Test, Description("Проверка формирования строки подключения к серверу лицензий для TDataManager'ов.")]
    public void CheckGetUrlForTdm()
    {
      Assert.AreEqual("net.tcp://localhost:9001/Lic/TDM",
        LicServerUrlBuilder.GetUriForTdm("localhost", 9001).ToString());
    }
  }
}
