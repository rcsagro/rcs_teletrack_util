﻿using System.Configuration;
using Teletrack.Config;

namespace Teletrack.LicServer.Config
{
  /// <summary>
  /// Настройки сервера.
  /// </summary>
  public sealed class ServerCfg : TtConfigurationSection
  {
    [ConfigurationProperty("address")]
    public string Address
    {
      get { return (string)this["address"]; }
      set { this["address"] = value; }
    }

    [ConfigurationProperty("serverPort")]
    public ushort ServerPort
    {
      get { return (ushort)this["serverPort"]; }
      set { this["serverPort"] = value; }
    }

    [ConfigurationProperty("robosoftPort")]
    public ushort RobosoftPort
    {
      get { return (ushort)this["robosoftPort"]; }
      set { this["robosoftPort"] = value; }
    }

    /// <summary>
    /// Валидация настроек.
    /// </summary>
    protected override void Validate()
    {
      if (ServerPort == 0)
      {
        ServerPort = 9005;
      }
      if (RobosoftPort == 0)
      {
        ServerPort = 8080;
      }
    }

    /// <summary>
    /// Создает экземпляр ProxyConnectionCfg.
    /// </summary>
    /// <exception cref="ConfigurationErrorsException">Не задан адрес в секции server конфигурационного файла.</exception>
    /// <returns>ProxyConnectionCfg.</returns>
    public static ServerCfg GetConfig()
    {
      return (ServerCfg)TtConfigurationSection.GetConfigSection("server");
    }
  }
}
