﻿namespace SenselockStartPrograming
{
  partial class FormSenslockSetting
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.buttonCreateRCSKey = new System.Windows.Forms.Button();
      this.comboBoxSenselockKeys = new System.Windows.Forms.ComboBox();
      this.buttonRefresh = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.buttonUpdateLicense = new System.Windows.Forms.Button();
      this.openFileD = new System.Windows.Forms.OpenFileDialog();
      this.statusStripForm = new System.Windows.Forms.StatusStrip();
      this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
      this.label1 = new System.Windows.Forms.Label();
      this.groupBox1.SuspendLayout();
      this.statusStripForm.SuspendLayout();
      this.SuspendLayout();
      // 
      // buttonCreateRCSKey
      // 
      this.buttonCreateRCSKey.Enabled = false;
      this.buttonCreateRCSKey.Location = new System.Drawing.Point(191, 70);
      this.buttonCreateRCSKey.Name = "buttonCreateRCSKey";
      this.buttonCreateRCSKey.Size = new System.Drawing.Size(110, 23);
      this.buttonCreateRCSKey.TabIndex = 0;
      this.buttonCreateRCSKey.Text = "Создать ключ";
      this.buttonCreateRCSKey.UseVisualStyleBackColor = true;
      this.buttonCreateRCSKey.Visible = false;
      this.buttonCreateRCSKey.Click += new System.EventHandler(this.buttonCreateRCSKey_Click);
      // 
      // comboBoxSenselockKeys
      // 
      this.comboBoxSenselockKeys.FormattingEnabled = true;
      this.comboBoxSenselockKeys.Location = new System.Drawing.Point(10, 19);
      this.comboBoxSenselockKeys.Name = "comboBoxSenselockKeys";
      this.comboBoxSenselockKeys.Size = new System.Drawing.Size(325, 21);
      this.comboBoxSenselockKeys.TabIndex = 2;
      // 
      // buttonRefresh
      // 
      this.buttonRefresh.Location = new System.Drawing.Point(351, 17);
      this.buttonRefresh.Name = "buttonRefresh";
      this.buttonRefresh.Size = new System.Drawing.Size(75, 23);
      this.buttonRefresh.TabIndex = 4;
      this.buttonRefresh.Text = "Обновить список";
      this.buttonRefresh.UseVisualStyleBackColor = true;
      this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.buttonRefresh);
      this.groupBox1.Controls.Add(this.comboBoxSenselockKeys);
      this.groupBox1.Location = new System.Drawing.Point(5, 10);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(437, 54);
      this.groupBox1.TabIndex = 5;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Доступны ключи";
      // 
      // buttonUpdateLicense
      // 
      this.buttonUpdateLicense.Location = new System.Drawing.Point(319, 70);
      this.buttonUpdateLicense.Name = "buttonUpdateLicense";
      this.buttonUpdateLicense.Size = new System.Drawing.Size(123, 23);
      this.buttonUpdateLicense.TabIndex = 6;
      this.buttonUpdateLicense.Text = "Обновить лицензию";
      this.buttonUpdateLicense.UseVisualStyleBackColor = true;
      this.buttonUpdateLicense.Click += new System.EventHandler(this.buttonUpdateLicense_Click);
      // 
      // openFileD
      // 
      this.openFileD.Filter = "(*.urs)|*.urs";
      // 
      // statusStripForm
      // 
      this.statusStripForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar});
      this.statusStripForm.Location = new System.Drawing.Point(0, 102);
      this.statusStripForm.Name = "statusStripForm";
      this.statusStripForm.Size = new System.Drawing.Size(449, 22);
      this.statusStripForm.TabIndex = 7;
      // 
      // toolStripProgressBar
      // 
      this.toolStripProgressBar.Name = "toolStripProgressBar";
      this.toolStripProgressBar.Size = new System.Drawing.Size(100, 16);
      this.toolStripProgressBar.Visible = false;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(2, 75);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(0, 13);
      this.label1.TabIndex = 8;
      // 
      // FormSenslockSetting
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(449, 124);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.statusStripForm);
      this.Controls.Add(this.buttonUpdateLicense);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.buttonCreateRCSKey);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Name = "FormSenslockSetting";
      this.Text = "SenselockSetting";
      this.groupBox1.ResumeLayout(false);
      this.statusStripForm.ResumeLayout(false);
      this.statusStripForm.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button buttonCreateRCSKey;
    private System.Windows.Forms.ComboBox comboBoxSenselockKeys;
    private System.Windows.Forms.Button buttonRefresh;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button buttonUpdateLicense;
    private System.Windows.Forms.OpenFileDialog openFileD;
    private System.Windows.Forms.StatusStrip statusStripForm;
    private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
    private System.Windows.Forms.Label label1;
  }
}

