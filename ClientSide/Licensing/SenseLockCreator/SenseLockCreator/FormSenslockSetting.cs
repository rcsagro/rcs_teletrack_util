﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace SenselockStartPrograming
{
  public partial class FormSenslockSetting : Form
  {
    Senselock.SENSE4_CONTEXT[] si;
    //Dictionary<int, string> keysDictionary = new Dictionary<int, string>();

    public FormSenslockSetting()
    {
      InitializeComponent();

      string version = "2.1";
      Text = "SenselockMaster " + version;

#if !CLIENT
      //!!!!Кнопка создать новый ключ, активна, только при наличии папки с файлами, для создания ключа!!!!!
      buttonCreateRCSKey.Visible = true;
      if(Directory.Exists("SenselockFile"))
        buttonCreateRCSKey.Enabled = true;
      Text = "SenselockMaster Pro " + version;
#endif
    }

    const string FileLicUpdate = "5204";
#if !CLIENT
    const string FileLicGet = "5201";
    const string FileLicFile = "5202";
#endif

    private void buttonCreateRCSKey_Click(object sender, EventArgs e)
    {
#if !CLIENT
      uint ret = 0;
      try
      {
        uint BytesReturned = 0;
        int numKey = comboBoxSenselockKeys.SelectedIndex;
          //open the device
        ret = Senselock.S4Open(ref si[numKey]);
        if (0x00 != ret)
          throw new Exception("Не удалось открыть ключ");

        ret = Senselock.S4ChangeDir(ref si[numKey], Senselock.WorkFolderName);//ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.RcsUserPin, 8, Senselock.S4_USER_PIN);
        if (0x00 != ret)//ключ уже проинициализирован РКС или Робософт и долна быть папка 
        {//"Не удалось перейти в рабочую дерикторию"
          ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.DefUserPin, 8, Senselock.S4_USER_PIN);
          if (0x00 != ret)
          {//DefUserPin не подошел - пробуем код RCS
            ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.RcsUserPin, 8, Senselock.S4_USER_PIN);
            if (0x00 != ret)
            {
              throw new Exception("Не подходит корневой UserPin по умолчанию и RCS");
            }
            //verify dev PIN
            ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.RcsDevelopPin, 24, Senselock.S4_DEV_PIN);
            if (0x00 != ret)
              throw new Exception("Не подходит корневой DevelopPin по умолчанию. Чужой ключ?");
          }
          else //Если ПИН по умолчанию подошел, пробуем код разработчика по умолчанию
          {
            ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.DefDevelopPin, 24, Senselock.S4_DEV_PIN);
            if (0x00 != ret)
              throw new Exception("Не подходит корневой DevelopPin по умолчанию. Чужой ключ?");
          }
          #region work_root_dir
          //erase root directory 
          ret = Senselock.S4EraseDir(ref si[numKey], null);
          if (0x00 != ret)
            throw new Exception("Неудачная попытка очисткы корневой деректории");
          //reate root directory
          ret = Senselock.S4CreateDir(ref si[numKey], "\\", 0, Senselock.S4_CREATE_ROOT_DIR);
          if (0x00 != ret)
            throw new Exception("Неудачная попытка создания корневой деректории");
          //change current directory to root directory
          ret = Senselock.S4ChangeDir(ref si[numKey], "\\");
          if (0x00 != ret)
            throw new Exception("Неудалось перейти в корневую деректорию");
          //verify dev PIN
          ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.DefDevelopPin, 24, Senselock.S4_DEV_PIN);
          if (0x00 != ret)
            throw new Exception("Ошибка ввода DefDevelopPin");
          //change user PIN
          ret = Senselock.S4ChangePin(ref si[numKey], Senselock.DefUserPin, 8, Senselock.RcsUserPin, 8, Senselock.S4_USER_PIN);
          if (0x00 != ret)
            throw new Exception("Ошибка при изменении DefUserPin в корневой деректории");
          //change dev PIN
          ret = Senselock.S4ChangePin(ref si[numKey], Senselock.DefDevelopPin, 24, Senselock.RcsDevelopPin, 24, Senselock.S4_DEV_PIN);
          if (0x00 != ret)
            throw new Exception("Ошибка при изменении DefDevelopPin в корневой деректории");
          //set HID mode
          ret = Senselock.S4Control(ref si[numKey], Senselock.S4_SET_HID_MODE, null, 0, Senselock.outBuffer, 256, ref BytesReturned);
          if (0x00 != ret)
            throw new Exception("Ошибка перехода в HID_MODE");
          //set DeviceID
          Senselock.MarkKeysRCS.CopyTo(Senselock.inBuffer, 0);
          ret = Senselock.S4Control(ref si[numKey], Senselock.S4_SET_DEVICE_ID, Senselock.inBuffer, 8, Senselock.outBuffer, 256, ref BytesReturned);
          if (0x00 != ret)
            throw new Exception("Ошибка установки своего DEVICE_ID");
          //on Led
          ret = Senselock.S4Control(ref si[numKey], Senselock.S4_LED_UP, null, 0, null, 0, ref BytesReturned);
          if (0x00 != ret)
            throw new Exception("Ошибка включения светодиода");
          #endregion //work_root_dir

          #region create_work_dir
          //verify dev PIN
          ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.RcsDevelopPin, 24, Senselock.S4_DEV_PIN);
          if (0x00 != ret)
            throw new Exception("Ошибка ввода DefDevelopPin");
          //create work dir
          ret = Senselock.S4CreateDir(ref si[numKey], Senselock.WorkFolderName, 16384, Senselock.S4_CREATE_SUB_DIR);
          if (0x00 != ret)
            throw new Exception("Неудачная попытка создания рабочей деректории");
          //change current directory to root directory
          ret = Senselock.S4ChangeDir(ref si[numKey], Senselock.WorkFolderName);
          if (0x00 != ret)
            throw new Exception("Неудалось перейти в рабочую деректорию");
          #endregion //create_work_dir

        }

        //verify dev PIN
        ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.DefDevelopPin, 24, Senselock.S4_DEV_PIN);
        if (0x00 != ret)
        {
          string firstErr = "Ошибка ввода DefDevelopPin в рабочую дерикторию";
          ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.RcsDevelopPin, 24, Senselock.S4_DEV_PIN);
          if (0x00 != ret)
            throw new Exception(firstErr + "&& Ошибка ввода RcsDevelopPin в рабочей деректории.");
        }
        //Авторизация прошла успешно, очищаем директорию
        //!!!!!! При очистке пин стирается!!!!!! и теряется авторизация
        ret = Senselock.S4EraseDir(ref si[numKey], null); //Senselock.WorkFolderName);
        if (0x00 != ret)
          throw new Exception("Ошибка очистки рабочей деректории");
        //change user PIN
        ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.DefUserPin, 8, Senselock.S4_USER_PIN);
        if (0x00 != ret)
          throw new Exception("Не подходит рабочий DefUserPin");
        ret = Senselock.S4ChangePin(ref si[numKey], Senselock.DefUserPin, 8, Senselock.RcsUserPin, 8, Senselock.S4_USER_PIN);
        if (0x00 != ret)
          throw new Exception("Ошибка при изменении DefUserPin в рабочей деректории");
        //change dev pin
        ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.DefDevelopPin, 24, Senselock.S4_DEV_PIN);
        if (0x00 != ret)
          throw new Exception("Не подходит рабочий DefDevelopPin");
        ret = Senselock.S4ChangePin(ref si[numKey], Senselock.DefDevelopPin, 24, Senselock.RcsDevelopPin, 24, Senselock.S4_DEV_PIN);
        if (0x00 != ret)
          throw new Exception("Ошибка при изменении DefUserPin в рабочей деректории");

        //PINs поменяны, деректория очищенна... Пишем файлы
        BinaryReader srLicFile = null;
        BinaryReader srLicGet = null;
        BinaryReader srLicUpd = null;
        try
        {
          FileStream fsLicFile = new FileStream(@"SenselockFile\BinLic.dat", FileMode.Open);
          FileStream fsLicGet = new FileStream(@"SenselockFile\LiecenseGetter.bin", FileMode.Open);
          FileStream fsLicUpd = new FileStream(@"SenselockFile\LicenseWriter.bin", FileMode.Open);
          srLicFile = new BinaryReader(fsLicFile);
          srLicGet = new BinaryReader(fsLicGet);
          srLicUpd = new BinaryReader(fsLicUpd);
          uint BytesWritten = 0;

          byte[] inBufFile = new byte[5400];
          //create License file
          srLicFile.Read(inBufFile, 0, (int)fsLicFile.Length);
          ret = Senselock.S4WriteFile(ref si[numKey], FileLicFile, 0, inBufFile, (uint)fsLicFile.Length,
            4000, ref BytesWritten, Senselock.S4_CREATE_NEW, Senselock.S4_DATA_FILE);
          if (0x00 != ret || fsLicFile.Length != BytesWritten)
            throw new Exception("Не удалось создать файл лицензии и др.");

          //create LicenseGetting file
          BytesWritten = 0;
          srLicGet.Read(inBufFile, 0, (int)fsLicGet.Length);
          ret = Senselock.S4WriteFile(ref si[numKey], FileLicGet, 0, inBufFile, (uint)fsLicGet.Length,
            5400, ref BytesWritten, Senselock.S4_CREATE_NEW | Senselock.S4_FILE_EXECUTE_ONLY, Senselock.S4_EXE_FILE);
          if (0x00 != ret || fsLicGet.Length != BytesWritten)
            throw new Exception("Не удалось создать файл FileLicGet и FileLicUpdate.");
          //create LicenseWriter file
          BytesWritten = 0;
          srLicUpd.Read(inBufFile, 0, (int)fsLicUpd.Length);
          ret = Senselock.S4WriteFile(ref si[numKey], FileLicUpdate, 0, inBufFile, (uint)fsLicUpd.Length,
            5400, ref BytesWritten, Senselock.S4_CREATE_NEW | Senselock.S4_FILE_EXECUTE_ONLY, Senselock.S4_EXE_FILE);
          if (0x00 != ret || fsLicUpd.Length != BytesWritten)
            throw new Exception("Не удалось создать файл FileLicUpdate.");
          
          MessageBox.Show("Ключ успешно создан");
        }
        catch (Exception _ex)
        {
          Console.WriteLine("Exception in ReadFile: {0}", _ex);
          Console.ReadLine();
          if (ret != 0)
            MessageBox.Show(string.Format("Exception Caught! {0} \n cod: {1:X2}", _ex, ret), "Exception");
          else
            MessageBox.Show(string.Format("Файл записался не полностью. \n {0}", _ex), "Exception");
        }
        finally
        {
          srLicFile.Close();
          srLicGet.Close();
          srLicUpd.Close();
        }

        ret = Senselock.S4Close(ref si[numKey]);
      }

      catch (Exception ex)
      {
        MessageBox.Show(string.Format("Exception Caught! {0} \n cod: {1:X2}", ex, ret),"Exception");
      }
#endif
    }

    private void buttonRefresh_Click(object sender, EventArgs e)
    {
      try
      {
        uint ret = 0;
        si = Senselock.GetS4Keys();
        label1.Text = string.Format("Find keys: {0}", si.Length);

        comboBoxSenselockKeys.Items.Clear();
        for (int i = 0; i < si.Length; i++)
        {
          ret = Senselock.S4Open(ref si[i]);
          if (0x00 != ret)
            label1.Text = "Не удалось открыть ключ";
          else
          {
            uint BytesReturned = 0;
            ret = Senselock.S4Control(ref si[i], Senselock.S4_GET_SERIAL_NUMBER, null, 0, Senselock.outBuffer, 256, ref BytesReturned);
            if (0x00 != ret)
              label1.Text = "Не удалось получить серийный номер";
            else
            {
              string keyID = "";
              for (int j = 0; j < BytesReturned; j++)
                keyID += string.Format("{0,2:X2}", Senselock.outBuffer[j]);
              keyID += "  CustomerID(";
              for (int j = 0; j < si[i].bID.Length; j++)
                keyID += string.Format("{0,2:X2}", si[i].bID[j]);
              keyID += ")";
              comboBoxSenselockKeys.Items.Add(keyID);
            }
          }
          //keysDictionary.Add(i, factoryID);
          Senselock.S4Close(ref si[i]);
        }
        comboBoxSenselockKeys.SelectedIndex = 0;
        comboBoxSenselockKeys.Text = comboBoxSenselockKeys.Items[0].ToString();
      }
      catch(Exception ex)
      {
        MessageBox.Show(ex.Message);
      }
    }

    private void buttonUpdateLicense_Click(object sender, EventArgs e)
    {
      int numKey = comboBoxSenselockKeys.SelectedIndex;
      uint ret = 0;
      if (openFileD.ShowDialog() == DialogResult.OK)
      {
        try
        {
          //open the device
          ret = Senselock.S4Open(ref si[numKey]);
          if (0x00 != ret)
            throw new Exception("Не удалось открыть ключ");
          //переход в корневой каталог
          ret = Senselock.S4ChangeDir(ref si[numKey], Senselock.WorkFolderName); //"\\");
          if (0x00 != ret)
            throw new Exception(string.Format("Не удалось перейти в корневой каталог. Ключ неинициализирован или чужой? {0:X2}", ret));

          ret = Senselock.S4VerifyPin(ref si[numKey], Senselock.RcsUserPin, 8, Senselock.S4_USER_PIN);
          if (ret != 0)
            throw new Exception(string.Format("Не верний ПИН пользователя. {0:X2}", ret));

          try
          {
            int lenOneCmd = Senselock.LEN_LIC + 8;//32+8 - всегда кратно 8. 32-сама лицензия 8-команда и срс. остальное нули
            FileStream licFileStream = File.OpenRead(openFileD.FileName);
            int lenRead = 0;
            int countUpdateRecords = 0;
            string resultatOk = "Успешно выполненные обновления:\n";
            string resultatError = "Получены ошибки при обновлении:\n";
            toolStripProgressBar.Maximum = (int)(licFileStream.Length / lenOneCmd);
            toolStripProgressBar.Value = 0;
            toolStripProgressBar.Visible = true;
            do
            {
              lenRead = licFileStream.Read(Senselock.inBuffer, 0, lenOneCmd);
              if (lenRead >= lenOneCmd)
              {
                uint BytesReturned = 0;
                ret = Senselock.S4Execute(ref si[numKey], FileLicUpdate, Senselock.inBuffer, (uint)lenOneCmd, Senselock.outBuffer, 256, ref BytesReturned);
                if (ret == 0)
                {
                  if (BytesReturned == lenOneCmd)
                  {
                    resultatOk += string.Format("№ {0}\n", countUpdateRecords);
                  }
                  else
                  {
                    string textError = "";
                    for (int i = 0; i < BytesReturned-1; i++)
                      textError += (char)Senselock.outBuffer[i];
                    resultatError += string.Format("№ {0} :: " + textError + "\n", countUpdateRecords);//Auxiliary.BytesToString(outBuffer, 0, BytesReturned));
                  }
                }
                else
                  resultatError += string.Format("№ {0} :: Ошибка выполнения LicUpdate: {1}", countUpdateRecords, ret);
                countUpdateRecords++;
                toolStripProgressBar.Increment(1);
                this.Update(); //System.Threading.Thread.
              }
              else
                MessageBox.Show(resultatOk + resultatError, "Конец файла");
            }
            while (lenRead >= lenOneCmd);
            licFileStream.Close();
            toolStripProgressBar.Visible = false;
          }
          catch (FileNotFoundException)
          {
            throw new Exception("File not found");
          }

          //close device
          ret = Senselock.S4Close(ref si[numKey]);
          if (0x00 != ret)
            throw new Exception("Ошибка закрытия ключа, так и быть...");
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message, "Exception Caught!");
        }
      }
    }
  }
}
