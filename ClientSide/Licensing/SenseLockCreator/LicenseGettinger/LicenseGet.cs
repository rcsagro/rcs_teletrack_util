﻿using System;
//using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using SenselockStartPrograming;

namespace LicenseGetting
{
  class LicenseGet : Senselock
  {
    static void Main(string[] args)
    {
      try
      {
        //       CriptTest();
        uint ret;
        bool cmdFind;

        do
        {
          cmdFind = false;
          #region FindAndOpenSenselock
          SENSE4_CONTEXT[] si = GetS4Keys();
          Console.WriteLine("Find keys: {0}", si.Length);

          int numKeyRcs = GetRcsKey(si);
          SENSE4_CONTEXT s4_handler;
          if (numKeyRcs >= 0)
            Console.WriteLine("Найден ключ RCS.");
          else
          {
            Console.WriteLine("Ключ RCS не найден!");
            numKeyRcs = 0;
          }
          s4_handler = si[numKeyRcs];

          //open the device
          ret = S4Open(ref s4_handler);
          if (0x00 != ret)
            Console.WriteLine("Не удалось открыть ключ");
          //переход в корневой каталог
          ret = S4ChangeDir(ref s4_handler, Senselock.WorkFolderName);//"\\");
          if (0x00 != ret)
            Console.WriteLine("Не удалось перейти в рабочий каталог. Ключ неинициализирован. {0:X2}", ret);

          ret = S4VerifyPin(ref s4_handler, RcsUserPin, 8, S4_USER_PIN);
          if (ret != 0)
            Console.WriteLine("Не верний ПИН пользователя. {0}", ret);
          #endregion //FindAndOpenSenselock

          bool cmdExit = false;
          while (!cmdExit)
          {
            Console.WriteLine(
              "Введине номер требуемой лицензии (0..{0}) или: exit - для выхода, find - для поиска другого ключа",
              MAX_CNT_LIC - 1);
            string readCmd = Console.ReadLine();
            if (readCmd == "Exit" || readCmd == "exit")
              cmdExit = true;
            else
            {
              if (readCmd == "Find" || readCmd == "find")
              {
                cmdExit = true;
                cmdFind = true;
              }
              else
              {
                try
                {
                  byte numLic = byte.Parse(readCmd);//Convert.ToByte(readCmd);
                  if (numLic < MAX_CNT_LIC)
                  {
                    uint lenCmd_getLic = 5;
                    uint BytesReturned = 0;
                    //получаем данные с ключа
                    inBuffer[0] = numLic;
                    BitConverter.GetBytes(UInt32.Parse("0")).CopyTo(inBuffer, 1);
                    ret = S4Execute(ref s4_handler, FileLicGet, inBuffer, lenCmd_getLic, outBuffer, 256, ref BytesReturned);
                    if (ret == 0)
                    {
                      if (BytesReturned >= LEN_LIC)
                      {
                        string textLic = "";
                        for (int i = 1; i < LEN_LIC; i++)
                          textLic += string.Format("{0,2:X2}", outBuffer[i]);//outBuffer[i];//
                        Console.WriteLine("Read Lic № {0}, value={1:s}", outBuffer[0], textLic);
                      }
                      else
                      {
                        Console.WriteLine("Получена ошибка поиска лицензии. :: " + Auxiliary.BytesToString(outBuffer, 0, BytesReturned));
                      }
                    }
                    else
                      Console.WriteLine("Ошибка выполнения LicGet: {0}", ret);
                  }
                  else
                    Console.WriteLine("Недопустимый номер лицензии: {0}", numLic);
                }
                catch
                {
                  Console.WriteLine("Введено недопустимое значение");
                }
              }
            }
          }

          //close device
          ret = S4Close(ref s4_handler);
          if (0x00 != ret)
            Console.WriteLine("Ошибка закрытия ключа, так и быть...");

        }
        while (cmdFind);
      }

      catch (Exception e)
      {
        Console.WriteLine("Exception Caught! {0}", e);
        Console.ReadLine();
      }
    }
   }
}
