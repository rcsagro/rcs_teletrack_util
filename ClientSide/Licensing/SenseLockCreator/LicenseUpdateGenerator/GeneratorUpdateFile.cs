﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using SenselockStartPrograming;

namespace LicenseUpdateGenerator
{
  public  class GeneratorUpdateFile:Senselock
  {
    public const byte CMD_WRITE_LIC_REC = 0x21;
    public const byte LEN_SERIAL_NUMBER_BYTE = 8;
    static void Main(string[] args)
    {
      try
      {
        byte[] serialNumberSL = new byte[LEN_SERIAL_NUMBER_BYTE];

        Console.WriteLine("Введине название txt файла лицензиями. (файл д.б. в каталоге с программой)");
        string read = Console.ReadLine();

        StreamReader txtFileStream = File.OpenText(read);
        string nameFileWrite = read.Split('.')[0] + ".urs";
        FileStream writeStream = File.Create(nameFileWrite);

        int lenOneCmd = LEN_LIC + 8;//32+8 - всегда кратно 8. 32-сама лицензия 8-команда и срс. остальное нули
        
        Console.WriteLine("Введите уникальный номер ключа, для которого будет создан файл обновления.\n В шестнадцатиричном виде, длиной в 32 символа.");
        bool keyIdOk = false;
        do
        {
          string sKeyID = Console.ReadLine();
          if (sKeyID.Length == LEN_SERIAL_NUMBER_BYTE*2)
          {
            try
            {
              for (int i = 0; i < LEN_SERIAL_NUMBER_BYTE; i++)
              {
                serialNumberSL[i] = Convert.ToByte(sKeyID.Substring(i * 2, 2),16);
              }
              keyIdOk = true;
            }
            catch (FormatException)
            {
              Console.WriteLine("Не формат введенных чисел");
            }
          }
          else
            Console.WriteLine("Неверная длина");
        }
        while (!keyIdOk);

        int cnt = 0;
        while (!txtFileStream.EndOfStream)
        {
          string licLine = txtFileStream.ReadLine();//.Replace("  ", " ");
          cnt++;
          string[] licPart = licLine.Split(' ');
          if (licPart.Length >= 2)
          {
            uint numLic = uint.Parse(licPart[0]);
            if (numLic < MAX_CNT_LIC && licPart[1].Length <= 30)
            {
                byte[] cmdBuf = LicenceCoding(serialNumberSL, lenOneCmd, licPart, numLic);

              writeStream.Write(cmdBuf, 0, lenOneCmd);
              Console.WriteLine("Добавлена запись: {0}", cnt);
            }
            else
              Console.WriteLine("Недопустимый номер лицензии или длина, в строке: {0}", cnt);
          }
          else
            Console.WriteLine("Неверный формат строки: {0}", cnt);
        }
        txtFileStream.Close();
        writeStream.Close();
      }
      catch (FileNotFoundException)
      {
        Console.WriteLine("File not found. Exit");
        Console.ReadLine();
      }
      catch (Exception e)
      {
        Console.WriteLine("Exception Caught! {0}. Exit", e);
        Console.ReadLine();
      }
    }

    public static byte[] LicenceCoding(byte[] serialNumberSL, int lenOneCmd, string[] licPart, uint numLic)
    {
        byte[] cmdBuf = new byte[lenOneCmd];
        cmdBuf[0] = CMD_WRITE_LIC_REC;
        cmdBuf[1] = (byte)numLic;
        for (int i = 0; i < licPart[1].Length; i++)
            cmdBuf[i + 2] = (byte)licPart[1].ToCharArray()[i];

        byte crc = 0; //CRC licRecord
        for (int i = 0; i < LEN_LIC - 1; i++)
            crc ^= cmdBuf[i + 1];
        cmdBuf[LEN_LIC] = (byte)(0xFF - crc);

        crc = 0; //CRC cmd
        for (int i = 0; i < lenOneCmd - 1; i++)
            crc ^= cmdBuf[i];
        cmdBuf[lenOneCmd - 1] = (byte)(0xFF - crc);

        //Code cmd
        cmdBuf = CodeCmd(serialNumberSL, cmdBuf, lenOneCmd);
        return cmdBuf;
    }

    private static byte[] CodeCmd(byte[] serialID, byte[] buf, int len)
    {
      byte[] KeyDecodeCmd = new byte[]{0x5E, 0x24, 0xF1, 0xD0, 0x23, 0x3E, 0x4A, 0x44, 0x38, 0x16, 0x32, 0x33, 0x39, 0x10, 0xCC, 0xE6 };

      DESCryptoServiceProvider desProvider = new DESCryptoServiceProvider();
      desProvider.Mode = CipherMode.ECB;
      desProvider.Key = serialID;
      desProvider.IV = new byte[8];
      desProvider.Padding = PaddingMode.Zeros;
      ICryptoTransform desCrTransform = desProvider.CreateEncryptor();
      byte[] newEncriptKey = desCrTransform.TransformFinalBlock(KeyDecodeCmd, 0, 16);

      TripleDESCryptoServiceProvider tDesProvider = new TripleDESCryptoServiceProvider();
      tDesProvider.Mode = CipherMode.ECB;
      tDesProvider.Key = newEncriptKey;
      tDesProvider.IV = new byte[8];
      tDesProvider.Padding = PaddingMode.Zeros;

      ICryptoTransform tCrTransform = tDesProvider.CreateEncryptor();
      return tCrTransform.TransformFinalBlock(buf, 0, len);
    }
  }
}
