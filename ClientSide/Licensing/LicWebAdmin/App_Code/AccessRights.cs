﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Права доступа пользователя к модулям программы
/// </summary>
public class AccessRights
{
    private bool _is_admin = false;

    public bool Is_admin
    {
        get { return _is_admin; }
        set { _is_admin = value; }
    }

    private bool _is_edit_dictionary = false;

    public bool Is_edit_dictionary
    {
        get { return _is_edit_dictionary; }
        set { _is_edit_dictionary = value; }
    }

    private bool _is_access_dictionary = false;

    public bool Is_access_dictionary
    {
        get { return _is_access_dictionary; }
        set { _is_access_dictionary = value; }
    }

    private bool _is_edit_license = false;

    public bool Is_edit_license
    {
        get { return _is_edit_license; }
        set { _is_edit_license = value; }
    }

    private int _user_id = 0;
    public int User_id
    {
        get { return _user_id; }
        set { _user_id = value; }
    }
    private string _user_name;

    public string User_name
    {
        get { return _user_name; }
        set { _user_name = value; }
    }
    public AccessRights()
	{
        if (HttpContext.Current.Request.Cookies["License"] != null)
        {
            if (Int32.TryParse(HttpContext.Current.Request.Cookies["License"]["UserId"].ToString(), out _user_id))
            {
                using (DataClassesDataContext dc = new DataClassesDataContext())
                {
                    User us = (from usr in dc.Users where usr.Id == _user_id select usr).Single();
                    _user_name = us.UserName;
                    _is_admin = us.Admin;
                    _is_edit_dictionary = us.UserRole.Dictionary_edit;
                    _is_access_dictionary = us.UserRole.Dictionary_view;
                    _is_edit_license = us.UserRole.License_edit; 
                }
            }
        }

	}
}
