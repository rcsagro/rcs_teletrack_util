<%@ Page Title="���������� ���������� ������" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Keys.aspx.cs" Inherits="Dictionary_Keys" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%;">
    <tr>
        <td style="width: 30%;" align="left" valign="top">
            <asp:LinqDataSource ID="ldsKeyTypes" runat="server" 
                ContextTypeName="DataClassesDataContext" TableName="KeyTypes">
            </asp:LinqDataSource>
            <dxwgv:ASPxGridView ID="agvKeyTypes" runat="server" 
                AutoGenerateColumns="False" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                CssPostfix="Aqua" DataSourceID="ldsKeyTypes" KeyFieldName="Id" 
                Width="90%">
                <Styles CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua">
                </Styles>
                <SettingsLoadingPanel Text="" />
                <SettingsPager>
                    <AllButton>
                        <Image Height="19px" Width="27px" />
                    </AllButton>
                    <FirstPageButton>
                        <Image Height="19px" Width="23px" />
                    </FirstPageButton>
                    <LastPageButton>
                        <Image Height="19px" Width="23px" />
                    </LastPageButton>
                    <NextPageButton>
                        <Image Height="19px" Width="19px" />
                    </NextPageButton>
                    <PrevPageButton>
                        <Image Height="19px" Width="19px" />
                    </PrevPageButton>
                </SettingsPager>
                <Images ImageFolder="~/App_Themes/Aqua/{0}/">
                    <CollapsedButton Height="15px" 
                        Url="~/App_Themes/Aqua/GridView/gvCollapsedButton.png" Width="15px" />
                    <ExpandedButton Height="15px" 
                        Url="~/App_Themes/Aqua/GridView/gvExpandedButton.png" Width="15px" />
                    <DetailCollapsedButton Height="15px" 
                        Url="~/App_Themes/Aqua/GridView/gvDetailCollapsedButton.png" Width="15px" />
                    <DetailExpandedButton Height="15px" 
                        Url="~/App_Themes/Aqua/GridView/gvDetailExpandedButton.png" Width="15px" />
                    <HeaderFilter Height="19px" Url="~/App_Themes/Aqua/GridView/gvHeaderFilter.png" 
                        Width="19px" />
                    <HeaderActiveFilter Height="19px" 
                        Url="~/App_Themes/Aqua/GridView/gvHeaderFilterActive.png" Width="19px" />
                    <HeaderSortDown Height="5px" 
                        Url="~/App_Themes/Aqua/GridView/gvHeaderSortDown.png" Width="7px" />
                    <HeaderSortUp Height="5px" Url="~/App_Themes/Aqua/GridView/gvHeaderSortUp.png" 
                        Width="7px" />
                    <FilterRowButton Height="13px" Width="13px" />
                    <WindowResizer Height="13px" Url="~/App_Themes/Aqua/GridView/WindowResizer.png" 
                        Width="13px" />
                </Images>
                <Columns>
                    <dxwgv:GridViewDataTextColumn FieldName="Id" ReadOnly="True" Visible="False" 
                        VisibleIndex="0">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="��� ����������� �����" 
                        FieldName="TypeName" VisibleIndex="0">
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
                <ImagesEditors>
                    <CalendarFastNavPrevYear Height="19px" 
                        Url="~/App_Themes/Aqua/Editors/edtCalendarFNPrevYear.png" Width="19px" />
                    <CalendarFastNavNextYear Height="19px" 
                        Url="~/App_Themes/Aqua/Editors/edtCalendarFNNextYear.png" Width="19px" />
                    <DropDownEditDropDown Height="7px" 
                        Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                        Width="9px" />
                    <SpinEditIncrement Height="7px" 
                        Url="~/App_Themes/Aqua/Editors/edtSpinEditIncrementImage.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditIncrementDisabledImage.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" 
                        Width="7px" />
                    <SpinEditDecrement Height="7px" 
                        Url="~/App_Themes/Aqua/Editors/edtSpinEditDecrementImage.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditDecrementDisabledImage.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" 
                        Width="7px" />
                    <SpinEditLargeIncrement Height="9px" 
                        Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncImage.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncDisabledImage.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" 
                        Width="7px" />
                    <SpinEditLargeDecrement Height="9px" 
                        Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecImage.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecDisabledImage.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" 
                        Width="7px" />
                </ImagesEditors>
            </dxwgv:ASPxGridView>
        </td>
        <td style="width: 70%;" align="left" valign="top">
            <asp:LinqDataSource ID="ldsKeys" runat="server" 
                ContextTypeName="DataClassesDataContext" TableName="Keys" OrderBy="Number">
            </asp:LinqDataSource>
            <dxwgv:ASPxGridView ID="agvKeys" runat="server" AutoGenerateColumns="False" 
                CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                DataSourceID="ldsKeys" KeyFieldName="Id" Width="100%">
                <SettingsBehavior ConfirmDelete="True" />
                <Styles CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua">
                </Styles>
                <SettingsLoadingPanel Text="" />
                <SettingsPager PageSize="50">
                    <AllButton>
                        <Image Height="19px" Width="27px" />
                    </AllButton>
                    <FirstPageButton>
                        <Image Height="19px" Width="23px" />
                    </FirstPageButton>
                    <LastPageButton>
                        <Image Height="19px" Width="23px" />
                    </LastPageButton>
                    <NextPageButton>
                        <Image Height="19px" Width="19px" />
                    </NextPageButton>
                    <PrevPageButton>
                        <Image Height="19px" Width="19px" />
                    </PrevPageButton>
                </SettingsPager>
                <Images ImageFolder="~/App_Themes/Aqua/{0}/">
                    <CollapsedButton Height="15px" 
                        Url="~/App_Themes/Aqua/GridView/gvCollapsedButton.png" Width="15px" />
                    <ExpandedButton Height="15px" 
                        Url="~/App_Themes/Aqua/GridView/gvExpandedButton.png" Width="15px" />
                    <DetailCollapsedButton Height="15px" 
                        Url="~/App_Themes/Aqua/GridView/gvDetailCollapsedButton.png" Width="15px" />
                    <DetailExpandedButton Height="15px" 
                        Url="~/App_Themes/Aqua/GridView/gvDetailExpandedButton.png" Width="15px" />
                    <HeaderFilter Height="19px" Url="~/App_Themes/Aqua/GridView/gvHeaderFilter.png" 
                        Width="19px" />
                    <HeaderActiveFilter Height="19px" 
                        Url="~/App_Themes/Aqua/GridView/gvHeaderFilterActive.png" Width="19px" />
                    <HeaderSortDown Height="5px" 
                        Url="~/App_Themes/Aqua/GridView/gvHeaderSortDown.png" Width="7px" />
                    <HeaderSortUp Height="5px" Url="~/App_Themes/Aqua/GridView/gvHeaderSortUp.png" 
                        Width="7px" />
                    <FilterRowButton Height="13px" Width="13px" />
                    <WindowResizer Height="13px" Url="~/App_Themes/Aqua/GridView/WindowResizer.png" 
                        Width="13px" />
                </Images>
                <Columns>
                    <dxwgv:GridViewCommandColumn VisibleIndex="0">
                        <ClearFilterButton Visible="True">
                        </ClearFilterButton>
                    </dxwgv:GridViewCommandColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Id" ReadOnly="True" 
                        VisibleIndex="1" Caption="���">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="�����" FieldName="Number" 
                        VisibleIndex="2" ReadOnly="True">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="���" FieldName="Id_type" 
                        VisibleIndex="3" ReadOnly="True">
                        <PropertiesComboBox DataSourceID="ldsKeyTypes" TextField="TypeName" 
                            ValueField="Id" ValueType="System.Int32">
                        </PropertiesComboBox>
                        <DataItemTemplate>
                            <dxe:ASPxComboBox ID="ASPxComboBox1" runat="server" 
                                CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                                DataSourceID="ldsKeyTypes" ImageFolder="~/App_Themes/Aqua/{0}/" 
                                LoadingPanelText="" ShowShadow="False" Value='<%# Eval("Id_type") %>' 
                                       ValueType="System.Int32" TextField="TypeName" ValueField="Id" 
                                Enabled="False">
                                <ButtonEditEllipsisImage Height="3px" 
                                    Url="~/App_Themes/Aqua/Editors/edtEllipsis.png" 
                                    UrlDisabled="~/App_Themes/Aqua/Editors/edtEllipsisDisabled.png" 
                                    UrlHottracked="~/App_Themes/Aqua/Editors/edtEllipsisHottracked.png" 
                                    UrlPressed="~/App_Themes/Aqua/Editors/edtEllipsisPressed.png" Width="12px" />
                                <DropDownButton>
                                    <Image Height="7px" Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                                        UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                                        UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                                        UrlPressed="~/App_Themes/Aqua/Editors/edtDropDownPressed.png" Width="9px" />
                                </DropDownButton>
                                <ValidationSettings>
                                    <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" 
                                        Width="14px" />
                                    <ErrorFrameStyle ImageSpacing="4px">
                                        <ErrorTextPaddings PaddingLeft="4px" />
                                    </ErrorFrameStyle>
                                </ValidationSettings>
                            </dxe:ASPxComboBox>
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Left">
                        </CellStyle>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataTextColumn Caption="���������� ���.���� Robosoft" 
                        FieldName="LicenseQTY" VisibleIndex="4" ReadOnly="True">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataDateColumn Caption="���� �����" FieldName="DateInit" 
                        VisibleIndex="5" ReadOnly="True">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dxwgv:GridViewDataDateColumn>
                </Columns>
                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
                <ImagesEditors>
                    <CalendarFastNavPrevYear Height="19px" 
                        Url="~/App_Themes/Aqua/Editors/edtCalendarFNPrevYear.png" Width="19px" />
                    <CalendarFastNavNextYear Height="19px" 
                        Url="~/App_Themes/Aqua/Editors/edtCalendarFNNextYear.png" Width="19px" />
                    <DropDownEditDropDown Height="7px" 
                        Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                        Width="9px" />
                    <SpinEditIncrement Height="7px" 
                        Url="~/App_Themes/Aqua/Editors/edtSpinEditIncrementImage.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditIncrementDisabledImage.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" 
                        Width="7px" />
                    <SpinEditDecrement Height="7px" 
                        Url="~/App_Themes/Aqua/Editors/edtSpinEditDecrementImage.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditDecrementDisabledImage.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" 
                        Width="7px" />
                    <SpinEditLargeIncrement Height="9px" 
                        Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncImage.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncDisabledImage.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" 
                        Width="7px" />
                    <SpinEditLargeDecrement Height="9px" 
                        Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecImage.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecDisabledImage.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" 
                        Width="7px" />
                </ImagesEditors>
            </dxwgv:ASPxGridView>
        </td>
    </tr>
</table>
</asp:Content>

