﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dictionary_Roles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void gvRoles_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        DataClassesDataContext dc = new DataClassesDataContext();
        UserRole ur = (from url in dc.UserRoles where url.Id == (int)e.Keys["Id"] select url).Single();
        int cnt = ur.Users.Count;  
        if (cnt > 0)
        {
            e.Cancel = true;
            throw new Exception("Удаление запрещено.Роль пользователя имеет ссылок: " + cnt);

        }
    }
}
