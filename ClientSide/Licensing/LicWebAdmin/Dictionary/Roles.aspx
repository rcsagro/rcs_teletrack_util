<%@ Page Title="���������� ����� �������������" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Roles.aspx.cs" Inherits="Dictionary_Roles" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
.dxgvControl_Aqua,
.dxgvDisabled_Aqua
{
	border: Solid 1px #A3C0E8;
	font: 9pt Tahoma;
	background-color: #FFFFFF;
	color: #000000;
	cursor: default;
}

.dxgvTable_Aqua
{
	background-color: #FFFFFF;
	border: 0;
	border-collapse: separate!important;
	overflow: hidden;
	font: 9pt Tahoma;
	color: #4F4F4F;
}

.dxgvHeader_Aqua {
	cursor: pointer;
	white-space: nowrap;
	padding: 5px 5px 5px 5px;
	border: Solid 1px #A3C0E8;
	background-image: url('mvwres://DevExpress.Web.ASPxThemes.v9.2,%20Version=9.2.6.0,%20Culture=neutral,%20PublicKeyToken=b88d1754d700e49a/gvHeaderBackground.gif');
	background-position: 50% top;
	background-repeat: repeat-x;
	background-color: #E2F0FF;
	overflow: hidden;
    font-weight: normal;
    text-align: left;
}

.dxgvCommandColumn_Aqua
{
	padding: 2px 0px 2px 2px;
}

.dxgvPagerBottomPanel_Aqua, .dxgvPagerTopPanel_Aqua
{
    padding-top: 4px;
    padding-bottom: 4px;
    background-image: url('mvwres://DevExpress.Web.ASPxThemes.v9.2,%20Version=9.2.6.0,%20Culture=neutral,%20PublicKeyToken=b88d1754d700e49a/gvPagerBottomPanelBackground.gif');
    background-repeat: repeat-x;
    background-color: #ffffff;
}

    .style1
    {
        color: #283B56;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        font-size: 9pt;
        line-height: normal;
        font-family: Tahoma;
        border-left-width: 0px;
        border-top-width: 0px;
    }
    .style2
    {
        color: #283B56;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        font-size: 9pt;
        line-height: normal;
        font-family: Tahoma;
        border-left-width: 0px;
        border-right-width: 0px;
        border-top-width: 0px;
    }
    .style3
    {
        overflow: hidden;
        border-left-style: none;
        border-left-color: inherit;
        border-left-width: 0;
        border-right: 1px Solid #BED6F6;
        border-top-style: none;
        border-top-color: inherit;
        border-top-width: 0;
        border-bottom: 1px Solid #BED6F6;
        padding-left: 5px;
        padding-right: 5px;
        padding-top: 4px;
        padding-bottom: 5px;
    }
    .style4
    {
        color: #3F66A0;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:LinqDataSource ID="ldsRoles" runat="server" 
                        AutoGenerateOrderByClause="True" ContextTypeName="DataClassesDataContext" 
                        EnableDelete="True" EnableInsert="True" EnableUpdate="True" 
                        TableName="UserRoles" 
        StoreOriginalValuesInViewState="False">
</asp:LinqDataSource>
<br />
                    <dxwgv:ASPxGridView ID="gvRoles" runat="server" 
                        AutoGenerateColumns="False" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                        CssPostfix="Aqua" DataSourceID="ldsRoles" 
    KeyFieldName="Id" Width="100%" onrowdeleting="gvRoles_RowDeleting">
                        <SettingsBehavior ConfirmDelete="True" />
                        <styles cssfilepath="~/App_Themes/Aqua/{0}/styles.css" csspostfix="Aqua">
                        </styles>
                        <SettingsLoadingPanel Text="" />
                        <settingspager PageSize="50">
                            <allbutton>
                                <Image Height="19px" Width="27px" />
                            </allbutton>
                            <firstpagebutton>
                                <Image Height="19px" Width="23px" />
                            </firstpagebutton>
                            <lastpagebutton>
                                <Image Height="19px" Width="23px" />
                            </lastpagebutton>
                            <nextpagebutton>
                                <Image Height="19px" Width="19px" />
                            </nextpagebutton>
                            <prevpagebutton>
                                <Image Height="19px" Width="19px" />
                            </prevpagebutton>
                        </settingspager>
                        <images imagefolder="~/App_Themes/Aqua/{0}/">
                            <CollapsedButton Height="15px" 
                                Url="~/App_Themes/Aqua/GridView/gvCollapsedButton.png" Width="15px" />
                            <ExpandedButton Height="15px" 
                                Url="~/App_Themes/Aqua/GridView/gvExpandedButton.png" Width="15px" />
                            <DetailCollapsedButton Height="15px" 
                                Url="~/App_Themes/Aqua/GridView/gvDetailCollapsedButton.png" Width="15px" />
                            <DetailExpandedButton Height="15px" 
                                Url="~/App_Themes/Aqua/GridView/gvDetailExpandedButton.png" Width="15px" />
                            <HeaderFilter Height="19px" Url="~/App_Themes/Aqua/GridView/gvHeaderFilter.png" 
                                Width="19px" />
                            <HeaderActiveFilter Height="19px" 
                                Url="~/App_Themes/Aqua/GridView/gvHeaderFilterActive.png" Width="19px" />
                            <HeaderSortDown Height="5px" 
                                Url="~/App_Themes/Aqua/GridView/gvHeaderSortDown.png" Width="7px" />
                            <HeaderSortUp Height="5px" Url="~/App_Themes/Aqua/GridView/gvHeaderSortUp.png" 
                                Width="7px" />
                            <FilterRowButton Height="13px" Width="13px" />
                            <WindowResizer Height="13px" Url="~/App_Themes/Aqua/GridView/WindowResizer.png" 
                                Width="13px" />
                        </images>
                        <SettingsText ConfirmDelete="������������� �������� ����?" />
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="Id" ReadOnly="True" VisibleIndex="0" 
                                Visible="False">
                                <EditFormSettings Visible="False" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="RoleName" VisibleIndex="0" 
                                Caption="�������� ����">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Left">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataCheckColumn FieldName="License_edit" VisibleIndex="1" 
                                Caption="�������������� ��������">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataCheckColumn>
                            <dxwgv:GridViewDataCheckColumn FieldName="Dictionary_view" VisibleIndex="2" 
                                Caption="�������� ������������">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataCheckColumn>
                            <dxwgv:GridViewDataCheckColumn FieldName="Dictionary_edit" VisibleIndex="3" 
                                Caption="�������������� ������������">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataCheckColumn>
                            <dxwgv:GridViewCommandColumn VisibleIndex="4">
                                <EditButton Visible="True">
                                </EditButton>
                                <NewButton Visible="True">
                                </NewButton>
                                <DeleteButton Visible="True">
                                </DeleteButton>
                                <ClearFilterButton Visible="True">
                                </ClearFilterButton>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewCommandColumn>
                        </Columns>
                        <Settings ShowGroupPanel="True" ShowFilterRow="True" />
                        <styleseditors>
                            <progressbar height="25px">
                            </progressbar>
                        </styleseditors>
                        <imageseditors>
                            <CalendarFastNavPrevYear Height="19px" 
                                Url="~/App_Themes/Aqua/Editors/edtCalendarFNPrevYear.png" Width="19px" />
                            <CalendarFastNavNextYear Height="19px" 
                                Url="~/App_Themes/Aqua/Editors/edtCalendarFNNextYear.png" Width="19px" />
                            <DropDownEditDropDown Height="7px" 
                                Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                                UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                                UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                                Width="9px" />
                            <SpinEditIncrement Height="7px" 
                                Url="~/App_Themes/Aqua/Editors/edtSpinEditIncrementImage.png" 
                                UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditIncrementDisabledImage.png" 
                                UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" 
                                UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" 
                                Width="7px" />
                            <SpinEditDecrement Height="7px" 
                                Url="~/App_Themes/Aqua/Editors/edtSpinEditDecrementImage.png" 
                                UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditDecrementDisabledImage.png" 
                                UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" 
                                UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" 
                                Width="7px" />
                            <SpinEditLargeIncrement Height="9px" 
                                Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncImage.png" 
                                UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncDisabledImage.png" 
                                UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" 
                                UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" 
                                Width="7px" />
                            <SpinEditLargeDecrement Height="9px" 
                                Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecImage.png" 
                                UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecDisabledImage.png" 
                                UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" 
                                UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" 
                                Width="7px" />
                        </imageseditors>
                    </dxwgv:ASPxGridView>
                </asp:Content>

