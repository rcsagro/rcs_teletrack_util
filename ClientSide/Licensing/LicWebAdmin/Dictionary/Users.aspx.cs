﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dictionary_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void gvUsers_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        DataClassesDataContext dc = new DataClassesDataContext();
        User us = (from users in dc.Users where users.Id == (int)e.Keys["Id"] select users).SingleOrDefault();
        int cnt = us.LicCards.Count ;
        if (cnt > 0)
        {
            e.Cancel = true;
            throw new Exception("Удаление запрещено.Пользователь создал лицензий : " + cnt);

        }
    }
}
