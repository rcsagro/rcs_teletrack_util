﻿<%@ Page Title="Справочник пользователей" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Users.aspx.cs" Inherits="Dictionary_Default" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <table style="width: 100%; height: 100%;">
            <tr >
                <td  valign="top"  >
                    <asp:LinqDataSource ID="ldsUsers" runat="server" AutoPage="False" 
                        ContextTypeName="DataClassesDataContext" EnableDelete="True" 
                        EnableInsert="True" EnableUpdate="True" TableName="Users" 
                        StoreOriginalValuesInViewState="False">
                    </asp:LinqDataSource>
                    <asp:LinqDataSource ID="ldsRoleIdName" runat="server" 
                        ContextTypeName="DataClassesDataContext" OrderBy="RoleName" 
                        Select="new (Id, RoleName)" TableName="UserRoles">
                    </asp:LinqDataSource>
                </td>
            </tr>
            <tr >
                <td valign="top" width="100%">
                    <dxwgv:ASPxGridView ID="gvUsers" runat="server" 
                        AutoGenerateColumns="False" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                        CssPostfix="Aqua" DataSourceID="ldsUsers" KeyFieldName="Id" Width="100%" 
                        onrowdeleting="gvUsers_RowDeleting">
                        <SettingsBehavior ConfirmDelete="True" />
                        <Styles CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua">
                        </Styles>
                        <SettingsLoadingPanel Text="" />
                        <SettingsPager PageSize="50">
                            <AllButton>
                                <Image Height="19px" Width="27px" />
                            </AllButton>
                            <FirstPageButton>
                                <Image Height="19px" Width="23px" />
                            </FirstPageButton>
                            <LastPageButton>
                                <Image Height="19px" Width="23px" />
                            </LastPageButton>
                            <NextPageButton>
                                <Image Height="19px" Width="19px" />
                            </NextPageButton>
                            <PrevPageButton>
                                <Image Height="19px" Width="19px" />
                            </PrevPageButton>
                        </SettingsPager>
                        <Images ImageFolder="~/App_Themes/Aqua/{0}/">
                            <CollapsedButton Height="15px" 
                                Url="~/App_Themes/Aqua/GridView/gvCollapsedButton.png" Width="15px" />
                            <ExpandedButton Height="15px" 
                                Url="~/App_Themes/Aqua/GridView/gvExpandedButton.png" Width="15px" />
                            <DetailCollapsedButton Height="15px" 
                                Url="~/App_Themes/Aqua/GridView/gvDetailCollapsedButton.png" Width="15px" />
                            <DetailExpandedButton Height="15px" 
                                Url="~/App_Themes/Aqua/GridView/gvDetailExpandedButton.png" Width="15px" />
                            <HeaderFilter Height="19px" Url="~/App_Themes/Aqua/GridView/gvHeaderFilter.png" 
                                Width="19px" />
                            <HeaderActiveFilter Height="19px" 
                                Url="~/App_Themes/Aqua/GridView/gvHeaderFilterActive.png" Width="19px" />
                            <HeaderSortDown Height="5px" 
                                Url="~/App_Themes/Aqua/GridView/gvHeaderSortDown.png" Width="7px" />
                            <HeaderSortUp Height="5px" Url="~/App_Themes/Aqua/GridView/gvHeaderSortUp.png" 
                                Width="7px" />
                            <FilterRowButton Height="13px" Width="13px" />
                            <WindowResizer Height="13px" Url="~/App_Themes/Aqua/GridView/WindowResizer.png" 
                                Width="13px" />
                        </Images>
                        <SettingsText ConfirmDelete="Подтверждаете удаление пользователя?" />
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="Id" ReadOnly="True" VisibleIndex="0">
                                <EditFormSettings Visible="False" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="UserName" VisibleIndex="1" 
                                Caption="Имя пользователя">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Left">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="Password" VisibleIndex="2" 
                                Caption="Пароль" Visible="False">
                                <PropertiesTextEdit Password="True">
                                </PropertiesTextEdit>
                                <EditFormSettings Visible="True" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="Id_role" VisibleIndex="2" 
                                Caption="Роль">
                                <PropertiesTextEdit Width="100%">
                                </PropertiesTextEdit>
                                <DataItemTemplate>
                                    <dxe:ASPxComboBox ID="ASPxComboBox1" runat="server" 
                                        DataSourceID="ldsRoleIdName" TextField="RoleName" 
                                        Value='<%# Eval("Id_role") %>' ValueField="Id" ValueType="System.Int32" 
                                        Enabled="False">
                                        <Columns>
                                            <dxe:ListBoxColumn Caption="Роль" FieldName="RoleName" />
                                        </Columns>
                                    </dxe:ASPxComboBox>
                                </DataItemTemplate>
                                <EditItemTemplate>
                                    <dxe:ASPxComboBox ID="ASPxComboBox1" runat="server" 
                                        DataSourceID="ldsRoleIdName" TextField="RoleName" 
                                        Value='<%# Bind("Id_role") %>' ValueField="Id" ValueType="System.Int32">
                                        <Columns>
                                            <dxe:ListBoxColumn Caption="Роль" FieldName="RoleName" />
                                        </Columns>
                                    </dxe:ASPxComboBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Left">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataCheckColumn FieldName="Admin" VisibleIndex="4" 
                                Caption="Администратор">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataCheckColumn>
                            <dxwgv:GridViewDataDateColumn FieldName="LastVisit" VisibleIndex="3" 
                                Caption="Последний визит" ReadOnly="True">
                                <PropertiesDateEdit ConvertEmptyStringToNull="False" DisplayFormatString="g">
                                </PropertiesDateEdit>
                                <EditFormSettings Visible="True" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataDateColumn>
                            <dxwgv:GridViewCommandColumn VisibleIndex="5">
                                <EditButton Visible="True">
                                </EditButton>
                                <NewButton Visible="True">
                                </NewButton>
                                <DeleteButton Visible="True">
                                </DeleteButton>
                                <ClearFilterButton Visible="True">
                                </ClearFilterButton>
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewCommandColumn>
                        </Columns>
                        <Settings ShowGroupPanel="True" ShowFilterRow="True" />
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
                        <ImagesEditors>
                            <CalendarFastNavPrevYear Height="19px" 
                                Url="~/App_Themes/Aqua/Editors/edtCalendarFNPrevYear.png" Width="19px" />
                            <CalendarFastNavNextYear Height="19px" 
                                Url="~/App_Themes/Aqua/Editors/edtCalendarFNNextYear.png" Width="19px" />
                            <DropDownEditDropDown Height="7px" 
                                Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                                UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                                UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                                Width="9px" />
                            <SpinEditIncrement Height="7px" 
                                Url="~/App_Themes/Aqua/Editors/edtSpinEditIncrementImage.png" 
                                UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditIncrementDisabledImage.png" 
                                UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" 
                                UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" 
                                Width="7px" />
                            <SpinEditDecrement Height="7px" 
                                Url="~/App_Themes/Aqua/Editors/edtSpinEditDecrementImage.png" 
                                UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditDecrementDisabledImage.png" 
                                UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" 
                                UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" 
                                Width="7px" />
                            <SpinEditLargeIncrement Height="9px" 
                                Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncImage.png" 
                                UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncDisabledImage.png" 
                                UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" 
                                UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" 
                                Width="7px" />
                            <SpinEditLargeDecrement Height="9px" 
                                Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecImage.png" 
                                UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecDisabledImage.png" 
                                UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" 
                                UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" 
                                Width="7px" />
                        </ImagesEditors>
                    </dxwgv:ASPxGridView>
                    </td>
            </tr>
        </table>
</asp:Content>

