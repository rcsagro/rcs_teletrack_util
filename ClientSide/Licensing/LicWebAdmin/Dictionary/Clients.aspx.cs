﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxGridView;


public partial class Dictionary_ClientGruops : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        AccessRights ar = new AccessRights();
        agvClientTypes.Columns[2].Visible = ar.Is_edit_dictionary || ar.Is_admin;
        agvClients.Columns[5].Visible = agvClientTypes.Columns[2].Visible;
    }
     protected void agvClientTypes_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        DataClassesDataContext dc = new DataClassesDataContext();
        ClientType ct = (from clt in dc.ClientTypes where clt.Id == (int)e.Keys["Id"] select clt).Single();
        int cnt = ct.Clients.Count;
        if (cnt > 0)
        {
            e.Cancel = true;
            throw new Exception("Удаление запрещено.Категория контрагентов имеет ссылок: " + cnt);
            
        }
    }
    protected void agvClients_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        DataClassesDataContext dc = new DataClassesDataContext();
        Client ct = (from cl in dc.Clients  where cl.Id == (int)e.Keys["Id"] select cl).SingleOrDefault();
        int cnt = ct.LicCards.Count;  
        if (cnt > 0)
        {
            e.Cancel = true;
            throw new Exception("Удаление запрещено.Контрагент имеет лицензий: " + cnt);

        }
    }
}


