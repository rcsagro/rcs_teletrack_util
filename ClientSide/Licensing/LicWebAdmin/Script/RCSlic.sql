USE [RCSlic]
GO
/****** Object:  ForeignKey [FK_Clients_ClientTypes]    Script Date: 06/15/2011 16:11:18 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Clients_ClientTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Clients]'))
ALTER TABLE [dbo].[Clients] DROP CONSTRAINT [FK_Clients_ClientTypes]
GO
/****** Object:  ForeignKey [FK_Keys_KeyTypes]    Script Date: 06/15/2011 16:11:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Keys_KeyTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Keys]'))
ALTER TABLE [dbo].[Keys] DROP CONSTRAINT [FK_Keys_KeyTypes]
GO
/****** Object:  ForeignKey [FK_LicCard_Keys]    Script Date: 06/15/2011 16:11:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCard_Keys]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards] DROP CONSTRAINT [FK_LicCard_Keys]
GO
/****** Object:  ForeignKey [FK_LicCard_Users]    Script Date: 06/15/2011 16:11:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCard_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards] DROP CONSTRAINT [FK_LicCard_Users]
GO
/****** Object:  ForeignKey [FK_LicCards_Clients]    Script Date: 06/15/2011 16:11:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCards_Clients]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards] DROP CONSTRAINT [FK_LicCards_Clients]
GO
/****** Object:  ForeignKey [FK_Users_UserRoles]    Script Date: 06/15/2011 16:11:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_UserRoles]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_UserRoles]
GO
/****** Object:  StoredProcedure [dbo].[Licences_Delete]    Script Date: 06/15/2011 16:11:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Licences_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Licences_Delete]
GO
/****** Object:  StoredProcedure [dbo].[Licences_Select]    Script Date: 06/15/2011 16:11:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Licences_Select]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Licences_Select]
GO
/****** Object:  StoredProcedure [dbo].[Licences_Select_Card]    Script Date: 06/15/2011 16:11:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Licences_Select_Card]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Licences_Select_Card]
GO
/****** Object:  View [dbo].[vClients]    Script Date: 06/15/2011 16:11:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vClients]'))
DROP VIEW [dbo].[vClients]
GO
/****** Object:  Table [dbo].[LicCards]    Script Date: 06/15/2011 16:11:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCard_Keys]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards] DROP CONSTRAINT [FK_LicCard_Keys]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCard_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards] DROP CONSTRAINT [FK_LicCard_Users]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCards_Clients]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards] DROP CONSTRAINT [FK_LicCards_Clients]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Table_1_PlaceQTY]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LicCards] DROP CONSTRAINT [DF_Table_1_PlaceQTY]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_LicCardT_TT_qty]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LicCards] DROP CONSTRAINT [DF_LicCardT_TT_qty]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_LicCardT_Module_agro]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LicCards] DROP CONSTRAINT [DF_LicCardT_Module_agro]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_LicCardT_Module_route]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LicCards] DROP CONSTRAINT [DF_LicCardT_Module_route]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_LicCards_DateInit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LicCards] DROP CONSTRAINT [DF_LicCards_DateInit]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LicCards]') AND type in (N'U'))
DROP TABLE [dbo].[LicCards]
GO
/****** Object:  Table [dbo].[Keys]    Script Date: 06/15/2011 16:11:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Keys_KeyTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Keys]'))
ALTER TABLE [dbo].[Keys] DROP CONSTRAINT [FK_Keys_KeyTypes]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Keys_LicenseQTY]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Keys] DROP CONSTRAINT [DF_Keys_LicenseQTY]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Keys_DateInit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Keys] DROP CONSTRAINT [DF_Keys_DateInit]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Keys]') AND type in (N'U'))
DROP TABLE [dbo].[Keys]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 06/15/2011 16:11:18 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Clients_ClientTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Clients]'))
ALTER TABLE [dbo].[Clients] DROP CONSTRAINT [FK_Clients_ClientTypes]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Clients_Id_type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Clients] DROP CONSTRAINT [DF_Clients_Id_type]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Clients]') AND type in (N'U'))
DROP TABLE [dbo].[Clients]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 06/15/2011 16:11:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_UserRoles]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_UserRoles]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Users_Id_role]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [DF_Users_Id_role]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Users_Admin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [DF_Users_Admin]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
DROP TABLE [dbo].[Users]
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 06/15/2011 16:11:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserRoles]') AND type in (N'U'))
DROP TABLE [dbo].[UserRoles]
GO
/****** Object:  Table [dbo].[ClientTypes]    Script Date: 06/15/2011 16:11:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClientTypes]') AND type in (N'U'))
DROP TABLE [dbo].[ClientTypes]
GO
/****** Object:  Table [dbo].[KeyTypes]    Script Date: 06/15/2011 16:11:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KeyTypes]') AND type in (N'U'))
DROP TABLE [dbo].[KeyTypes]
GO
/****** Object:  Table [dbo].[KeyTypes]    Script Date: 06/15/2011 16:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KeyTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KeyTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_KeyTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ClientTypes]    Script Date: 06/15/2011 16:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClientTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ClientTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ClientTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 06/15/2011 16:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](50) NOT NULL,
	[License_edit] [bit] NOT NULL,
	[Dictionary_view] [bit] NOT NULL,
	[Dictionary_edit] [bit] NOT NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Users]    Script Date: 06/15/2011 16:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Id_role] [int] NOT NULL CONSTRAINT [DF_Users_Id_role]  DEFAULT ((0)),
	[LastVisit] [smalldatetime] NULL,
	[Admin] [bit] NOT NULL CONSTRAINT [DF_Users_Admin]  DEFAULT ((0)),
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 06/15/2011 16:11:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Clients]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Clients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientName] [nvarchar](50) NOT NULL,
	[Id_type] [int] NOT NULL CONSTRAINT [DF_Clients_Id_type]  DEFAULT ((0)),
	[Id_parrent_client] [int] NULL,
	[Comments] [text] NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Keys]    Script Date: 06/15/2011 16:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Keys]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Keys](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [nvarchar](50) NOT NULL,
	[Id_type] [int] NOT NULL,
	[LicenseQTY] [int] NOT NULL CONSTRAINT [DF_Keys_LicenseQTY]  DEFAULT ((0)),
	[DateInit] [smalldatetime] NOT NULL CONSTRAINT [DF_Keys_DateInit]  DEFAULT (getdate()),
 CONSTRAINT [PK_Keys] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[LicCards]    Script Date: 06/15/2011 16:11:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LicCards]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LicCards](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_client] [int] NOT NULL,
	[Id_user] [int] NOT NULL,
	[Id_key] [int] NULL,
	[LoginDisp] [nvarchar](50) NULL,
	[Place_qty] [int] NULL CONSTRAINT [DF_Table_1_PlaceQTY]  DEFAULT ((0)),
	[TT_qty] [int] NULL CONSTRAINT [DF_LicCardT_TT_qty]  DEFAULT ((0)),
	[Module_agro] [bit] NULL CONSTRAINT [DF_LicCardT_Module_agro]  DEFAULT ((0)),
	[Module_route] [bit] NULL CONSTRAINT [DF_LicCardT_Module_route]  DEFAULT ((0)),
	[Comments] [text] NULL,
	[DateInit] [smalldatetime] NOT NULL CONSTRAINT [DF_LicCards_DateInit]  DEFAULT (getdate()),
 CONSTRAINT [PK_LicCard] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  View [dbo].[vClients]    Script Date: 06/15/2011 16:11:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vClients]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[vClients]
AS
SELECT     dbo.Clients.Id, dbo.ClientTypes.Id AS TypeId, dbo.ClientTypes.TypeName, dbo.Clients.ClientName, dbo.Clients.Id_parrent_client AS ClientParentId, 
                      Clients_1.ClientName AS ClientParentName, dbo.Clients.Comments
FROM         dbo.Clients INNER JOIN
                      dbo.ClientTypes ON dbo.Clients.Id_type = dbo.ClientTypes.Id LEFT OUTER JOIN
                      dbo.Clients AS Clients_1 ON dbo.Clients.Id_parrent_client = Clients_1.Id
'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'vClients', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[32] 4[30] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Clients"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 220
               Right = 201
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ClientTypes"
            Begin Extent = 
               Top = 6
               Left = 239
               Bottom = 99
               Right = 390
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Clients_1"
            Begin Extent = 
               Top = 119
               Left = 391
               Bottom = 227
               Right = 554
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 2310
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vClients'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'vClients', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vClients'
GO
/****** Object:  StoredProcedure [dbo].[Licences_Select_Card]    Script Date: 06/15/2011 16:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Licences_Select_Card]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Create date: 12.04.2011
-- Description:	:Журнал лицензий
-- =============================================
CREATE PROCEDURE [dbo].[Licences_Select_Card]
@CL int = 0
AS
BEGIN

	SET NOCOUNT ON;
SELECT     LicCards.Id, Keys.Number, LicCards.LoginDisp, LicCards.Place_qty, LicCards.TT_qty, LicCards.Module_agro, LicCards.Module_route, 
                      Users.UserName, LicCards.Comments, LicCards.DateInit
FROM         LicCards INNER JOIN
                      Users ON LicCards.Id_user = Users.Id LEFT OUTER JOIN
                      Keys ON LicCards.Id_key = Keys.Id
WHERE     (LicCards.Id_client = @CL)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[Licences_Select]    Script Date: 06/15/2011 16:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Licences_Select]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Create date: 11.04.2011
-- Description:	:Журнал лицензий
-- =============================================
CREATE PROCEDURE [dbo].[Licences_Select]
@DB smalldatetime,
@DE smalldatetime,
@TYPE_CL int =0,
@CL int = 0,
@KEY_NUM nvarchar(50)=''0''
AS
BEGIN

	SET NOCOUNT ON;

SELECT     LicCards.Id, ClientTypes.TypeName, Clients.ClientName, Clients_1.ClientName AS Parent, Keys.Number, LicCards.LoginDisp, LicCards.Place_qty, 
                      LicCards.TT_qty, LicCards.Module_agro, LicCards.Module_route, Users.UserName, LicCards.Comments, LicCards.DateInit
FROM         LicCards INNER JOIN
                      Clients ON LicCards.Id_client = Clients.Id INNER JOIN
                      ClientTypes ON Clients.Id_type = ClientTypes.Id INNER JOIN
                      Users ON LicCards.Id_user = Users.Id LEFT OUTER JOIN
                      Keys ON LicCards.Id_key = Keys.Id LEFT OUTER JOIN
                      Clients AS Clients_1 ON Clients.Id_parrent_client = Clients_1.Id
                      WHERE     (LicCards.DateInit BETWEEN @DB AND @DE)
                      AND (ClientTypes.Id = @TYPE_CL OR ISNULL(@TYPE_CL,0)=0)
                      AND (Clients.Id = @CL OR ISNULL(@CL,0) = 0)
                      AND ((Keys.Number LIKE N''%'' + @KEY_NUM + ''%'') OR 
                      (@KEY_NUM =''0'') OR (LEN(RTRIM(@KEY_NUM)) = 0)) 

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[Licences_Delete]    Script Date: 06/15/2011 16:11:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Licences_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Create date: 14.06.2011
-- Description:	Удаление лицензии
-- =============================================
CREATE PROCEDURE [dbo].[Licences_Delete]
@ID int
AS
BEGIN
DELETE FROM LicCards
WHERE     (Id = @ID)
END
' 
END
GO
/****** Object:  ForeignKey [FK_Clients_ClientTypes]    Script Date: 06/15/2011 16:11:18 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Clients_ClientTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Clients]'))
ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_Clients_ClientTypes] FOREIGN KEY([Id_type])
REFERENCES [dbo].[ClientTypes] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Clients_ClientTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Clients]'))
ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [FK_Clients_ClientTypes]
GO
/****** Object:  ForeignKey [FK_Keys_KeyTypes]    Script Date: 06/15/2011 16:11:19 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Keys_KeyTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Keys]'))
ALTER TABLE [dbo].[Keys]  WITH CHECK ADD  CONSTRAINT [FK_Keys_KeyTypes] FOREIGN KEY([Id_type])
REFERENCES [dbo].[KeyTypes] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Keys_KeyTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Keys]'))
ALTER TABLE [dbo].[Keys] CHECK CONSTRAINT [FK_Keys_KeyTypes]
GO
/****** Object:  ForeignKey [FK_LicCard_Keys]    Script Date: 06/15/2011 16:11:19 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCard_Keys]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards]  WITH CHECK ADD  CONSTRAINT [FK_LicCard_Keys] FOREIGN KEY([Id_key])
REFERENCES [dbo].[Keys] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCard_Keys]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards] CHECK CONSTRAINT [FK_LicCard_Keys]
GO
/****** Object:  ForeignKey [FK_LicCard_Users]    Script Date: 06/15/2011 16:11:19 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCard_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards]  WITH CHECK ADD  CONSTRAINT [FK_LicCard_Users] FOREIGN KEY([Id_user])
REFERENCES [dbo].[Users] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCard_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards] CHECK CONSTRAINT [FK_LicCard_Users]
GO
/****** Object:  ForeignKey [FK_LicCards_Clients]    Script Date: 06/15/2011 16:11:19 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCards_Clients]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards]  WITH CHECK ADD  CONSTRAINT [FK_LicCards_Clients] FOREIGN KEY([Id_client])
REFERENCES [dbo].[Clients] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LicCards_Clients]') AND parent_object_id = OBJECT_ID(N'[dbo].[LicCards]'))
ALTER TABLE [dbo].[LicCards] CHECK CONSTRAINT [FK_LicCards_Clients]
GO
/****** Object:  ForeignKey [FK_Users_UserRoles]    Script Date: 06/15/2011 16:11:19 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_UserRoles]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_UserRoles] FOREIGN KEY([Id_role])
REFERENCES [dbo].[UserRoles] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_UserRoles]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_UserRoles]
GO
