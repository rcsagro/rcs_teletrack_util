﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>

<%@ Register assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" namespace="System.Web.UI.WebControls" tagprefix="asp" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>База учета лицензий</title>
    <style type="text/css">
        .style1
        {
            text-align: center;
            width: 368px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <table style="width: 100%; height: 568px;">
        <tr style="height: 33%;">
            <td style="width: 33%;">
                &nbsp;</td>
            <td class="style1">
                <asp:LinqDataSource ID="ldsUsers" runat="server" 
                    ContextTypeName="DataClassesDataContext" Select="new (Id, UserName)" 
                    TableName="Users">
                </asp:LinqDataSource>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr style="height: 34%;">
            <td style="width: 33%;">
                </td>
            <td class="style1">
                <table style="width: 100%; height: 100%;" bgcolor="#DBEBFF">
                    <tr style="height: 20%;">
                        <td style="width: 40%; ">
                            <dxe:ASPxLabel ID="ASPxLabel1" runat="server" 
                                CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                                EnableClientSideAPI="True" Text="Выберите пользователя" EncodeHtml="False" 
                                Width="100%">
                            </dxe:ASPxLabel>
                        </td>
                    </tr>
                    <tr style="height: 20%;">
                        <td>
                            <dxe:ASPxComboBox ID="cbUsers" runat="server" DataSourceID="ldsUsers" 
                                ValueType="System.Int32" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                                CssPostfix="Aqua" Height="25px" ImageFolder="~/App_Themes/Aqua/{0}/" 
                                LoadingPanelText="" ShowShadow="False" Width="100%" TextField="UserName" 
                                ValueField="Id">
                                <ButtonEditEllipsisImage Height="3px" 
                                    Url="~/App_Themes/Aqua/Editors/edtEllipsis.png" 
                                    UrlDisabled="~/App_Themes/Aqua/Editors/edtEllipsisDisabled.png" 
                                    UrlHottracked="~/App_Themes/Aqua/Editors/edtEllipsisHottracked.png" 
                                    UrlPressed="~/App_Themes/Aqua/Editors/edtEllipsisPressed.png" Width="12px" />
                                <DropDownButton>
                                    <Image Height="7px" Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                                        UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                                        UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                                        UrlPressed="~/App_Themes/Aqua/Editors/edtDropDownPressed.png" Width="9px" />
                                </DropDownButton>
                                <Columns>
                                    <dxe:ListBoxColumn FieldName="Id" Visible="False" />
                                    <dxe:ListBoxColumn Caption="Пользователь" FieldName="UserName" />
                                </Columns>
                                <ValidationSettings>
                                    <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" 
                                        Width="14px" />
                                    <ErrorFrameStyle ImageSpacing="4px">
                                        <ErrorTextPaddings PaddingLeft="4px" />
                                    </ErrorFrameStyle>
                                </ValidationSettings>
                            </dxe:ASPxComboBox>
                        </td>
                    </tr>
                    <tr style="height: 20%;">
                        <td>
                            <dxe:ASPxLabel ID="ASPxLabel2" runat="server" 
                                CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                                EnableClientSideAPI="True" Text="Введите пароль" EncodeHtml="False" 
                                Width="100%">
                            </dxe:ASPxLabel>
                        </td>
                    </tr>
                    <tr style="height: 20%;">
                        <td>
                            <dxe:ASPxTextBox ID="txPassword" runat="server" AutoCompleteType="Disabled" 
                                CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" Height="25px" 
                                HorizontalAlign="Center" Password="True" Width="100%" AutoPostBack="True" 
                                ontextchanged="txPassword_TextChanged">
                                <ValidationSettings>
                                    <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" />
                                    <ErrorFrameStyle ImageSpacing="4px">
                                        <ErrorTextPaddings PaddingLeft="4px" />
                                    </ErrorFrameStyle>
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                    </tr>
                    <tr style="height: 20%;">
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td style="width:33%;">
                                        </td>
                                    <td style="width:34%;" align="center" >
                                        <dxe:ASPxButton ID="btInput" runat="server"  Width=100%
                                            CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                                            Text="Вход" onclick="btInput_Click">
                                        </dxe:ASPxButton>
                                    </td>
                                    <td style="width:33%;">
                                        </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr style="height: 33%;">
            <td style="width: 33%;">
                &nbsp;</td>
            <td class="style1" align="center" valign="top">
                <dxe:ASPxLabel ID="albError" runat="server" ForeColor="#FF3300" Visible="False">
                </dxe:ASPxLabel>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    </form>
    </body>
</html>
