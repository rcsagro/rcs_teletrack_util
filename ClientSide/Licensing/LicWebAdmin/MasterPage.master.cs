﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Page_Init(object sender, EventArgs e)
    {
        AccessRights ar = new AccessRights();
        ///доступ к справочникам 
        bool _Is_access_dictionary = false;
        ///управление доступом к справочнику пользователей и ролей 
        bool _Is_access_control = false;
        if (ar.User_id == 0)
        {
            albUser.Text = "Пользователь не активирован";
        }
        else
        {
            albUser.Text = ar.User_name; 
            if (ar.Is_admin)
            {
                _Is_access_dictionary = true;
                _Is_access_control = true;
            }
            else
            {
                _Is_access_dictionary = ar.Is_access_dictionary;
            }
        }
        amnNavigate.Items[1].ClientVisible = _Is_access_control;
        amnNavigate.Items[2].ClientVisible = _Is_access_dictionary;
    }
}
