﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;  

public partial class Licence_LicenceGrn : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        AccessRights ar = new AccessRights();
        agvGrnLicenses.Columns[12].Visible = ar.Is_edit_license || ar.Is_admin;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            adeStart.Date = DateTime.Today.AddDays(-365) ;
            adeEnd.Date = DateTime.Today.AddDays(1)  ;
        }
    }

    protected void acbClientsTypes_SelectedIndexChanged(object sender, EventArgs e)
    {
        acbClients.Items.Clear();
        acbClients.Text = "";
        ldsClients.DataBind();

    }
    protected void ldsClientsType_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {

    }
    protected void abtNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/License/LicenseCard.aspx?CardId=0");
    }

    protected void agvGrnLicenses_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        //e.Cancel = true;
    }
    protected void agvGrnLicenses_RowCommand(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewRowCommandEventArgs e)
    {

    }
    protected void agvGrnLicenses_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        Response.Redirect("~/License/LicenseCard.aspx?CardId=" + e.EditingKeyValue);
        e.Cancel = true; 
    }
    protected void abtRefresh_Click(object sender, EventArgs e)
    {
        //Console.WriteLine(atxNumber.Text);  
        agvGrnLicenses.DataBind(); 
    }
    protected void abtFilterClear_Click(object sender, EventArgs e)
    {
        acbClientsTypes.Value = null;
        acbClients.Value = null;
        atxNumber.Text = null;
        agvGrnLicenses.DataBind();
    }
}
