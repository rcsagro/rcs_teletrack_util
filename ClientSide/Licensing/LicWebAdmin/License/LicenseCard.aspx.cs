﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors; 
using LicenseUpdateGenerator;

public partial class License_LicenseCard : System.Web.UI.Page
{
    AccessRights ar ;
    bool BanUpdateCombo;
 
    protected void Page_Init(object sender, EventArgs e)
    {
        ar = new AccessRights();
        albId.Text = Request.QueryString.Get("CardId");
        abtUpdate.Visible =  ar.Is_edit_license || ar.Is_admin;
        abtKeySave.Visible = abtUpdate.Visible;
        abtLicGener.Visible = abtUpdate.Visible;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) LoadCard();
        //LoadCard();
    }

    #region Интерфейс
    protected void acbClientsTypes_ValueChanged(object sender, EventArgs e)
    {
        acbId_client.Items.Clear();
        acbId_client.Text = "";
        ldsClients.DataBind();
    }

    protected void abtLicGener_Click(object sender, EventArgs e)
    {
        GetLicenceFile();
    }

    protected void abtUpdate_Click(object sender, EventArgs e)
    {
        SaveLicence();
    }

    protected void abtKeySave_Click(object sender, EventArgs e)
    {
        SaveKey();
    } 
    #endregion

    private void LoadCard()
    {
        int _id;
        if (Int32.TryParse(albId.Text, out _id) && _id > 0)
        {
            DataClassesDataContext dc = new DataClassesDataContext();
            LicCard lc = (from lcard in dc.LicCards where lcard.Id == _id select lcard).Single();
            albDateInit.Text = lc.DateInit.ToString();
            acbClientsTypes.Value = lc.Client.ClientType.Id;
            ldsClients.DataBind();
            acbId_client.Value = lc.Id_client;
            albWorker.Text = lc.User.UserName;
            albDateInit.Value = lc.DateInit;
            atxLogin.Text = lc.LoginDisp;  
            atxComments.Text = lc.Comments;
            achAGRO.Value = lc.Module_agro;
            achRouter.Value = lc.Module_route;
            atxWorkPlaces.Text = lc.Place_qty.ToString();  
 
            if (lc.Id_key != null)
            {
                atxKeyCode.Text = lc.Id_key.ToString();
                acbKeyTypes.Value = lc.Key.Id_type;
                atxKeyNumber.Text = lc.Key.Number;
                atxRobosoftLic.Text = lc.Key.LicenseQTY.ToString() ;
            }
        }
        else
        {
            albWorker.Text = ar.User_name;
            albDateInit.Value = DateTime.Now; 
        }
    }

    private bool SaveKey()
    {
        albError.Text = "";
        if (!SaveLicence()) return false;
        if (acbKeyTypes.Value == null)
        {
            albError.Text = "Укажите тип аппаратного ключа!";
            return false;
        }
        if (LicenseUpdateGenerator.GeneratorUpdateFile.LEN_SERIAL_NUMBER_BYTE * 2 != atxKeyNumber.Text.Length)
        {
            albError.Text = "Неверная длина номера аппаратного ключа!";
            return false;
        }
        byte[] serialNumberSL = new byte[LicenseUpdateGenerator.GeneratorUpdateFile.LEN_SERIAL_NUMBER_BYTE];
        try
        {
            for (int i = 0; i < LicenseUpdateGenerator.GeneratorUpdateFile.LEN_SERIAL_NUMBER_BYTE; i++)
            {
                serialNumberSL[i] = Convert.ToByte(atxKeyNumber.Text.Substring(i * 2, 2), 16);
            }
        }
        catch (FormatException)
        {
            albError.Text = "Номер аппаратного ключа должен иметь только 0...9 и A...F!";
            return false;
        }
        int RoboLicQty;
        if (!Int32.TryParse(atxRobosoftLic.Text, out RoboLicQty))
        {
            albError.Text = "Количество лицензий Robosoft должно быть цифровым!";
            return false;
        }
        int _id = IsKeyExist();
        if (_id!=0)
        {
            albError.Text = "Ключ с номером " + atxKeyNumber.Text  + " уже существует с кодом " + _id + "!";
            return false;
        }
        DataClassesDataContext dc = new DataClassesDataContext();
        if (atxKeyCode.Text == "0")
        {
            Key k_insert = new Key();
            k_insert.Number = atxKeyNumber.Text;
            k_insert.Id_type = (int)acbKeyTypes.Value;
            k_insert.LicenseQTY = RoboLicQty;
            k_insert.DateInit = DateTime.Today;
            dc.Keys.InsertOnSubmit(k_insert);
            dc.SubmitChanges();
            atxKeyCode.Text = k_insert.Id.ToString();
            if (Int32.TryParse(albId.Text, out _id))
            {
                LicCard lc = (from lcard in dc.LicCards where lcard.Id == _id select lcard).Single();
                lc.Id_key = k_insert.Id;
                dc.SubmitChanges();
            }
        }
        else
        {
            if (Int32.TryParse(atxKeyCode.Text, out _id))
            {
                if (Int32.TryParse(atxRobosoftLic.Text, out RoboLicQty))
                {
                    Key k_update = (from ks in dc.Keys where ks.Id == _id select ks).Single();
                    k_update.LicenseQTY = RoboLicQty;
                    k_update.Number  = atxKeyNumber.Text;
                    dc.SubmitChanges();
                }
                
            }
        }
        return true;
    }

    private bool SaveLicence()
    {
        albError.Text = "";
        if (atxLogin.Text.Length==0)
        {
            albError.Text = "Укажите логин диспетчера!";
            return false;
        }
        if (acbId_client.Value   == null)
        {
            albError.Text = "Укажите контрагента!";
            return false;
        }
        try
        {
            DataClassesDataContext dc = new DataClassesDataContext();
            if (albId.Text == "0")
            {
                LicCard lc = new LicCard();
                lc.Comments = atxComments.Text;
                lc.Id_client = (int)acbId_client.Value;
                lc.Id_user = ar.User_id;
                lc.DateInit = DateTime.Now;
                SetLicenseValues(lc); 
                lc.LoginDisp = atxLogin.Text;
                dc.LicCards.InsertOnSubmit(lc);
                dc.SubmitChanges();
                albId.Text = lc.Id.ToString();
                albDateInit.Value = DateTime.Now;
            }
            else
            {
                int _id;
                if (Int32.TryParse(albId.Text, out _id))
                {
                    LicCard lc = (from lcard in dc.LicCards where lcard.Id == _id select lcard).Single();
                    lc.Comments = atxComments.Text;
                    lc.Id_client = (int)acbId_client.Value;
                    lc.LoginDisp = atxLogin.Text;
                    SetLicenseValues(lc);
                    dc.SubmitChanges();
                    
                }
            }
            agvLicClient.DataBind();
            return true;
        }
        catch (Exception ex)
        {
            albError.Text = ex.Message;
            return false;
        }
        
    }

    private void GetLicenceFile()
    {
        albError.Text = "";
        if (!SaveLicence()) return;
        if (LicenseUpdateGenerator.GeneratorUpdateFile.LEN_SERIAL_NUMBER_BYTE * 2 != atxKeyNumber.Text.Length)
        {
            albError.Text = "Неверная длина номера аппратного ключа!";
            return;
        }
        if (atxKeyCode.Text == "0" && !SaveKey()) return;

        int work_paces = 0;
        if (!Int32.TryParse(atxWorkPlaces.Text, out work_paces) || work_paces == 0)
        {
            albError.Text = "Количество рабочих мест должно быть больше нуля!";
            return;
        }
        int _id;
        if (Int32.TryParse(albId.Text, out _id))
        {
            DataClassesDataContext dc = new DataClassesDataContext();
            LicCard lc = (from lcard in dc.LicCards where lcard.Id == _id select lcard).Single();
             SetLicenseValues(lc);
            dc.SubmitChanges();
        }
        else
        {
            albError.Text = "Лицензия не сохранена!";
            return;
        }


        byte[] serialNumberSL = new byte[LicenseUpdateGenerator.GeneratorUpdateFile.LEN_SERIAL_NUMBER_BYTE];
        for (int i = 0; i < LicenseUpdateGenerator.GeneratorUpdateFile.LEN_SERIAL_NUMBER_BYTE; i++)
        {
            serialNumberSL[i] = Convert.ToByte(atxKeyNumber.Text.Substring(i * 2, 2), 16);
        }
        int lenOneCmd = LicenseUpdateGenerator.GeneratorUpdateFile.LEN_LIC + 8;
        string[] licPart = new string[2] ;
        MemoryStream ms = new MemoryStream();
        // количество рабочих мест
        licPart[1] = atxWorkPlaces.Text;
        byte[] cmdBuf = LicenseUpdateGenerator.GeneratorUpdateFile.LicenceCoding(serialNumberSL, lenOneCmd, licPart, LicenseNumbers.MAX_TRACK_CTR);
        ms.Write(cmdBuf, 0, lenOneCmd);
        // агро
        licPart[1] = (achAGRO.Value ?? false).ToString().ToLower()  == "true" ? "1" : "0" ;
        cmdBuf = LicenseUpdateGenerator.GeneratorUpdateFile.LicenceCoding(serialNumberSL, lenOneCmd, licPart, LicenseNumbers.AGRO);
        ms.Write(cmdBuf, 0, lenOneCmd);
        // маршруты
        licPart[1] = (achRouter.Value ?? false).ToString().ToLower() == "true" ? "1" : "0";
        cmdBuf = LicenseUpdateGenerator.GeneratorUpdateFile.LicenceCoding(serialNumberSL, lenOneCmd, licPart, LicenseNumbers.ROUTE);
        ms.Write(cmdBuf, 0, lenOneCmd);
         Response.Clear();
         Response.ClearContent();
         Response.ClearHeaders();
         Response.AddHeader("Content-Disposition", "attachment; filename=" + atxKeyNumber.Text  + ".urs");
         Response.ContentType = "text/html";
         Response.AppendHeader("Connection", "keep-alive");
         Response.BinaryWrite(ms.ToArray());
         Response.End();
    }

    private void SetLicenseValues(LicCard lc)
    {
            lc.Module_agro = (bool)(achAGRO.Value ?? false);
            lc.Module_route = (bool)(achRouter.Value ?? false);
            int work_paces = 0;
            if (Int32.TryParse(atxWorkPlaces.Text, out work_paces)) lc.Place_qty = work_paces;
    }

    private int IsKeyExist()
    {
        DataClassesDataContext dc = new DataClassesDataContext();
        Key k = (from kk in dc.Keys where kk.Number == atxKeyNumber.Text select kk).FirstOrDefault()  ;
        if (k == null)
            return 0;
        else
            return k.Id;
    }

}
