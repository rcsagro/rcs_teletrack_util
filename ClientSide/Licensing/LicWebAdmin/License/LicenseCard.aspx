<%@ Page Title="�������� ��������" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LicenseCard.aspx.cs" Inherits="License_LicenseCard" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>

<%@ Register assembly="DevExpress.Web.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dxp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">


.dxeButtonEdit_Aqua
{
    background-color: White;
    border: Solid 1px #AECAF0;
    width: 170px;
}
.dxeButtonEdit_Aqua .dxeEditArea_Aqua
{
    background-color: White;
}
.dxeEditArea_Aqua 
{
	font-family: Tahoma;
	font-size: 9pt;
	border: 1px solid #A0A0A0;
}
.dxeButtonEditButton_Aqua
{
	padding: 0px 2px;
}
.dxeButtonEditButton_Aqua,
.dxeSpinIncButton_Aqua, .dxeSpinDecButton_Aqua, .dxeSpinLargeIncButton_Aqua, .dxeSpinLargeDecButton_Aqua
{
    padding: 1px 2px 3px 2px;
	background-image: url('mvwres://DevExpress.Web.ASPxThemes.v9.2,%20Version=9.2.6.0,%20Culture=neutral,%20PublicKeyToken=b88d1754d700e49a/edtDropDownBack.gif');
    background-repeat: repeat-x;
    background-color: #DBEBFF;
}
.dxeButtonEditButton_Aqua, .dxeCalendarButton_Aqua,
.dxeSpinIncButton_Aqua, .dxeSpinDecButton_Aqua,
.dxeSpinLargeIncButton_Aqua, .dxeSpinLargeDecButton_Aqua
{	
	vertical-align: Middle;
	border: Solid 1px #A3C0E8;
	cursor: pointer;
} 
        .style3
        {
            height: 30px;
            text-align: right;
            background-color: #99CCFF;
        }
        .style1
        {
            height: 30px;
            text-align:center;
            background-color: #DEEDFE;
        }
        .style5
        {
            background-color: #99CCFF;
        }
        .style6
        {
            width: 250px;
            background-color: #99CCFF;
            text-align: left ; 
        }
        .style9
        {
            width: 150px;
            background-color: #99CCFF;
        }
        .style10
        {
            width: 10px;
        }
        .style11
        {
            width: 200px;
            background-color: #99CCFF;
            text-align: right;
        }
        .style12
        {
            width: 50px;
            background-color: #99CCFF;
            text-align: left;
        }
        .style13
        {
            width: 69px;
            background-color: #99CCFF;
            text-align: left;
        }
        .style14
        {
            height: 30px;
            text-align: left;
            background-color: #99CCFF;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%; height: 700px;">
        <tr>
            <td  align="left"  colspan="6" valign="middle">
                <dxe:ASPxLabel ID="albError" runat="server" ForeColor="#FF3300">
                </dxe:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td class="style3">
                <dxe:ASPxLabel ID="ASPxLabel5" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="����� ��������">
                </dxe:ASPxLabel>
                </td>
            <td class="style14" align="left" valign="middle">
                <dxe:ASPxLabel ID="albId" runat="server" Height="22px"
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Width="30%">
                    <Border BorderColor="#DBEBFF" BorderStyle="Solid" BorderWidth="1px" />
                </dxe:ASPxLabel>
            </td>
            <td class="style3" align="center" valign="middle">
                <dxe:ASPxLabel ID="ASPxLabel7" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="���� ��������" Font-Names="Arial">
                </dxe:ASPxLabel>
                </td>
            <td class="style14" align="left" valign="middle">
                <dxe:ASPxLabel ID="albDateInit" runat="server" Width="50%" Height="22px">
                    <Border BorderColor="#DBEBFF" BorderStyle="Solid" BorderWidth="1px" />
                </dxe:ASPxLabel>
            </td>
            <td class="style3" align="center" valign="middle">
                <dxe:ASPxLabel ID="ASPxLabel8" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="������" Font-Names="Arial">
                </dxe:ASPxLabel>
                </td>
            <td class="style14" align="left" valign="middle">
                <dxe:ASPxLabel ID="albWorker" runat="server" Width="100%" Height="22px">
                    <Border BorderColor="#DBEBFF" BorderStyle="Solid" BorderWidth="1px" />
                </dxe:ASPxLabel>
                </td>
        </tr>
        <tr>
            <td class="style3" valign="middle" align="left">
                <dxe:ASPxLabel ID="ASPxLabel3" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="��������� ������������">
                </dxe:ASPxLabel>
                </td>
            <td align="center" class="style14" valign="Center">
                <dxe:ASPxComboBox ID="acbClientsTypes" runat="server" AutoPostBack="True" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    DataSourceID="ldsClientsType" ImageFolder="~/App_Themes/Aqua/{0}/" 
                    LoadingPanelText="" 
                    ShowShadow="False" TextField="TypeName" ValueField="Id" 
                    ValueType="System.Int32" Width="100%" 
                    onvaluechanged="acbClientsTypes_ValueChanged">
                    <ButtonEditEllipsisImage Height="3px" 
                        Url="~/App_Themes/Aqua/Editors/edtEllipsis.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtEllipsisDisabled.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtEllipsisHottracked.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtEllipsisPressed.png" Width="12px" />
                    <DropDownButton>
                        <Image Height="7px" Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtDropDownPressed.png" Width="9px" />
                    </DropDownButton>
                    <ValidationSettings>
                        <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" 
                            Width="14px" />
                        <ErrorFrameStyle ImageSpacing="4px">
                            <ErrorTextPaddings PaddingLeft="4px" />
                        </ErrorFrameStyle>
                    </ValidationSettings>
                </dxe:ASPxComboBox>
                </td>
            <td align="center" class="style3" valign="middle">
                <dxe:ASPxLabel ID="ASPxLabel4" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="����������" Font-Names="Arial">
                </dxe:ASPxLabel>
                </td>
            <td class="style14" valign="Center">
                <dxe:ASPxComboBox ID="acbId_client" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    DataSourceID="ldsClients" ImageFolder="~/App_Themes/Aqua/{0}/" 
                    LoadingPanelText="" ShowShadow="False" TextField="ClientName" ValueField="Id" 
                    ValueType="System.Int32" Width="100%">
                    <ButtonEditEllipsisImage Height="3px" 
                        Url="~/App_Themes/Aqua/Editors/edtEllipsis.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtEllipsisDisabled.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtEllipsisHottracked.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtEllipsisPressed.png" Width="12px" />
                    <DropDownButton>
                        <Image Height="7px" Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtDropDownPressed.png" Width="9px" />
                    </DropDownButton>
                    <ValidationSettings>
                        <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" 
                            Width="14px" />
                        <ErrorFrameStyle ImageSpacing="4px">
                            <ErrorTextPaddings PaddingLeft="4px" />
                        </ErrorFrameStyle>
                    </ValidationSettings>
                </dxe:ASPxComboBox>
            </td>
            <td class="style3" valign="middle" align="center">
                &nbsp;</td>
            <td class="style14" valign="middle" align="left">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3" valign="middle" align="left">
                <dxe:ASPxLabel ID="ASPxLabel14" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="����� ����������">
                </dxe:ASPxLabel>
                </td>
            <td align="center" class="style14" valign="Center">
                            <dxe:ASPxTextBox ID="atxLogin" runat="server" Width="80px">
                            </dxe:ASPxTextBox>
                </td>
            <td align="center" class="style3" valign="middle">
                <dxe:ASPxLabel ID="ASPxLabel9" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="�����������">
                </dxe:ASPxLabel>
                </td>
            <td class="style14" valign="Center">
                <dxe:ASPxTextBox ID="atxComments" runat="server" 
                    Width="100%" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                    CssPostfix="Aqua">
                    <ValidationSettings>
                        <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" />
                        <ErrorFrameStyle ImageSpacing="4px">
                            <ErrorTextPaddings PaddingLeft="4px" />
                        </ErrorFrameStyle>
                    </ValidationSettings>
                </dxe:ASPxTextBox>
            </td>
            <td class="style3" valign="middle" align="center">
                &nbsp;</td>
            <td class="style14" valign="middle" align="left">
                <dxe:ASPxButton ID="abtUpdate" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="��������� ��������" Width="70%" onclick="abtUpdate_Click">
                </dxe:ASPxButton>
            </td>
        </tr>
        <tr>
            <td class="style1" valign="middle" align="left" colspan="6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style1" valign="middle" align="center" bgcolor="#E2F0FF" colspan="6">
                <table bgcolor="#E2F0FF" style="width:100%;">
                    <caption>
                    </caption>
                    <tr>
                        <td class="style5" colspan="3">
                <dxe:ASPxLabel ID="ASPxLabel16" runat="server" EncodeHtml="False" 
                    Font-Bold="True" Text="���� ��������">
                </dxe:ASPxLabel>
                        </td>
                        <td class="style10">
                            &nbsp;</td>
                        <td class="style5" colspan="3">
                <dxe:ASPxLabel ID="ASPxLabel10" runat="server" EncodeHtml="False" 
                    Font-Bold="True" Text="��������� ��������">
                </dxe:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td class="style11">
                <dxe:ASPxLabel ID="ASPxLabel13" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="��� �����">
                </dxe:ASPxLabel>
                        </td>
                        <td class="style6" colspan="2" >
                            <dxe:ASPxTextBox ID="atxKeyCode" runat="server" Width="80px" 
                                CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" Height="20px" 
                                HorizontalAlign="Center" Text="0" Enabled="False">
                                <ValidationSettings>
                                    <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" />
                                    <ErrorFrameStyle ImageSpacing="4px">
                                        <ErrorTextPaddings PaddingLeft="4px" />
                                    </ErrorFrameStyle>
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                        <td class="style10">
                            &nbsp;</td>
                        <td class="style11">
                <dxe:ASPxLabel ID="ASPxLabel15" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="���-�� ������� ����">
                </dxe:ASPxLabel>
                            </td>
                        <td class="style12">
                            <dxe:ASPxTextBox ID="atxWorkPlaces" runat="server" Width="100%" 
                                HorizontalAlign="Center" Text="0">
                            </dxe:ASPxTextBox>
                        </td>
                        <td class="style9">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style11">
                <dxe:ASPxLabel ID="ASPxLabel12" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="��� ����������� �����">
                </dxe:ASPxLabel>
                        </td>
                        <td class="style6" colspan="2">
                <dxe:ASPxComboBox ID="acbKeyTypes" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    DataSourceID="ldsKeyTypes" ImageFolder="~/App_Themes/Aqua/{0}/" 
                    LoadingPanelText="" 
                    ShowShadow="False" TextField="TypeName" ValueField="Id" 
                    ValueType="System.Int32" Width="100%" Height="20px">
                    <ButtonEditEllipsisImage Height="3px" 
                        Url="~/App_Themes/Aqua/Editors/edtEllipsis.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtEllipsisDisabled.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtEllipsisHottracked.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtEllipsisPressed.png" Width="12px" />
                    <DropDownButton>
                        <Image Height="7px" Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtDropDownPressed.png" Width="9px" />
                    </DropDownButton>
                    <ValidationSettings>
                        <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" 
                            Width="14px" />
                        <ErrorFrameStyle ImageSpacing="4px">
                            <ErrorTextPaddings PaddingLeft="4px" />
                        </ErrorFrameStyle>
                    </ValidationSettings>
                </dxe:ASPxComboBox>
                        </td>
                        <td class="style10">
                            &nbsp;</td>
                        <td class="style11" >
                <dxe:ASPxLabel ID="ASPxLabel18" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="������� ������ AGRO">
                </dxe:ASPxLabel>
                            </td>
                        <td align="center" class="style12" valign="middle">
                            <dxe:ASPxCheckBox ID="achAGRO" runat="server" 
                                CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                                TextSpacing="2px">
                            </dxe:ASPxCheckBox>
                        </td>
                        <td align="center" class="style9" valign="middle">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style11">
                <dxe:ASPxLabel ID="ASPxLabel11" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="����� ����������� �����">
                </dxe:ASPxLabel>
                        </td>
                        <td class="style6" align="left" colspan="2">
                            <dxe:ASPxTextBox ID="atxKeyNumber" runat="server" Width="100%" Height="20px">
                            </dxe:ASPxTextBox>
                        </td>
                        <td align="center" class="style10">
                            &nbsp;</td>
                        <td class="style11">
                <dxe:ASPxLabel ID="ASPxLabel19" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="������� ������ ��������">
                </dxe:ASPxLabel>
                            </td>
                        <td align="center" class="style12">
                            <dxe:ASPxCheckBox ID="achRouter" runat="server" 
                                CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                                TextSpacing="2px">
                            </dxe:ASPxCheckBox>
                        </td>
                        <td align="center" class="style9">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style11">
                <dxe:ASPxLabel ID="ASPxLabel17" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="���-�� ������� ���� Robosoft">
                </dxe:ASPxLabel>
                        </td>
                        <td align="left" class="style12">
                            <dxe:ASPxTextBox ID="atxRobosoftLic" runat="server" Width="80px" 
                                CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" Height="20px" 
                                HorizontalAlign="Center" Text="0">
                                <ValidationSettings>
                                    <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" />
                                    <ErrorFrameStyle ImageSpacing="4px">
                                        <ErrorTextPaddings PaddingLeft="4px" />
                                    </ErrorFrameStyle>
                                </ValidationSettings>
                            </dxe:ASPxTextBox>
                        </td>
                        <td align="left" class="style13">
                <dxe:ASPxButton ID="abtKeySave" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="��������� ����" Width="160px" onclick="abtKeySave_Click">
                </dxe:ASPxButton>
                        </td>
                        <td class="style10">
                            &nbsp;</td>
                        <td class="style11">
                            &nbsp;</td>
                        <td class="style12" align="center" valign="middle">
                            &nbsp;</td>
                        <td class="style14" align="center" valign="middle">
                <dxe:ASPxButton ID="abtLicGener" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    onclick="abtLicGener_Click" Text="������������� ">
                </dxe:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" class="style1" colspan="6" valign="middle">
                <dxe:ASPxLabel ID="ASPxLabel6" runat="server" EncodeHtml="False" 
                    Font-Bold="True" Text="������  �������� �����������">
                </dxe:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="6" valign="top">
                <asp:LinqDataSource ID="ldsClients" runat="server" 
                    ContextTypeName="DataClassesDataContext" TableName="Clients" 
                    Where="Id_type == @Id_type" OrderBy="ClientName">
                    <WhereParameters>
                        <asp:ControlParameter ControlID="acbClientsTypes" DefaultValue="0" 
                            Name="Id_type" PropertyName="Value" Type="Int32" />
                    </WhereParameters>
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="ldsClientsType" runat="server" AutoPage="False" 
                    ContextTypeName="DataClassesDataContext" TableName="ClientTypes" 
                    OrderBy="TypeName" >
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="ldsCard" runat="server" 
                    ContextTypeName="DataClassesDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="LicCards">
                </asp:LinqDataSource>
                <asp:SqlDataSource ID="sdsClientCards" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:RCSlicConnectionString %>" 
                    SelectCommand="Licences_Select_Card" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="acbId_client" DefaultValue="0" Name="CL" 
                            PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:LinqDataSource ID="ldsKeyTypes" runat="server" 
                    ContextTypeName="DataClassesDataContext" OrderBy="TypeName" 
                    Select="new (Id, TypeName)" TableName="KeyTypes">
                </asp:LinqDataSource>
                <br />
                <dxwgv:ASPxGridView ID="agvLicClient" runat="server" 
                    AutoGenerateColumns="False" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                    CssPostfix="Aqua" DataSourceID="sdsClientCards" KeyFieldName="Id" 
                    Width="100%" AccessibilityCompliant="True">
                    <Styles CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua">
                    </Styles>
                    <SettingsLoadingPanel Text="" />
                    <SettingsPager PageSize="50">
                        <AllButton>
                            <Image Height="19px" />
                        </AllButton>
                        <FirstPageButton>
                            <Image Height="19px" />
                        </FirstPageButton>
                        <LastPageButton>
                            <Image Height="19px" />
                        </LastPageButton>
                        <NextPageButton>
                            <Image Height="19px" />
                        </NextPageButton>
                        <PrevPageButton>
                            <Image Height="19px" />
                        </PrevPageButton>
                    </SettingsPager>
                    <Images ImageFolder="~/App_Themes/Aqua/{0}/">
                        <CollapsedButton Height="15px" 
                            Url="~/App_Themes/Aqua/GridView/gvCollapsedButton.png" />
                        <ExpandedButton Height="15px" 
                            Url="~/App_Themes/Aqua/GridView/gvExpandedButton.png" />
                        <DetailCollapsedButton Height="15px" 
                            Url="~/App_Themes/Aqua/GridView/gvDetailCollapsedButton.png" />
                        <DetailExpandedButton Height="15px" 
                            Url="~/App_Themes/Aqua/GridView/gvDetailExpandedButton.png" />
                        <HeaderFilter Height="19px" 
                            Url="~/App_Themes/Aqua/GridView/gvHeaderFilter.png" />
                        <HeaderActiveFilter Height="19px" 
                            Url="~/App_Themes/Aqua/GridView/gvHeaderFilterActive.png" />
                        <HeaderSortDown Height="5px" 
                            Url="~/App_Themes/Aqua/GridView/gvHeaderSortDown.png" />
                        <HeaderSortUp Height="5px" 
                            Url="~/App_Themes/Aqua/GridView/gvHeaderSortUp.png" />
                        <FilterRowButton Height="13px" />
                        <WindowResizer Height="13px" 
                            Url="~/App_Themes/Aqua/GridView/WindowResizer.png" />
                    </Images>
                    <SettingsText EmptyDataRow="���������� �� ����� ��������" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="����� �����" FieldName="Id" 
                            ReadOnly="True" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="����� �����" FieldName="Number" 
                            VisibleIndex="1">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="����� ����������" FieldName="LoginDisp" 
                            VisibleIndex="2">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="���-�� ���.����" FieldName="Place_qty" 
                            VisibleIndex="3">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataCheckColumn Caption="AGRO" FieldName="Module_agro" 
                            VisibleIndex="4">
                        </dxwgv:GridViewDataCheckColumn>
                        <dxwgv:GridViewDataCheckColumn Caption="��������" FieldName="Module_route" 
                            VisibleIndex="5">
                        </dxwgv:GridViewDataCheckColumn>
                        <dxwgv:GridViewDataTextColumn Caption="������" FieldName="UserName" 
                            VisibleIndex="6">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="���� ��������" FieldName="DateInit" 
                            VisibleIndex="7">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="�����������" FieldName="Comments" 
                            VisibleIndex="8">
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <ProgressBar Height="25px">
                        </ProgressBar>
                    </StylesEditors>
                    <ImagesEditors>
                        <CalendarFastNavPrevYear Height="19px" 
                            Url="~/App_Themes/Aqua/Editors/edtCalendarFNPrevYear.png" />
                        <CalendarFastNavNextYear Height="19px" 
                            Url="~/App_Themes/Aqua/Editors/edtCalendarFNNextYear.png" />
                        <DropDownEditDropDown Height="7px" 
                            Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" />
                        <SpinEditIncrement Height="7px" 
                            Url="~/App_Themes/Aqua/Editors/edtSpinEditIncrementImage.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditIncrementDisabledImage.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" />
                        <SpinEditDecrement Height="7px" 
                            Url="~/App_Themes/Aqua/Editors/edtSpinEditDecrementImage.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditDecrementDisabledImage.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" />
                        <SpinEditLargeIncrement Height="9px" 
                            Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncImage.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncDisabledImage.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" />
                        <SpinEditLargeDecrement Height="9px" 
                            Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecImage.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecDisabledImage.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" />
                    </ImagesEditors>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
    </table>
</asp:Content>

