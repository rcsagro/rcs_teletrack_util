﻿<%@ Page Title="Журнал лицензий" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LicenseGrn.aspx.cs" Inherits="Licence_LicenceGrn" %>

<%@ Register assembly="DevExpress.Web.ASPxScheduler.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxScheduler.Controls" tagprefix="dxwschsc" %>
<%@ Register assembly="DevExpress.Web.ASPxScheduler.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxScheduler" tagprefix="dxwschs" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v9.2, Version=9.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        .style1
        {
            height: 37px;
        }
        .style2
        {
            height: 37px;
            width: 200px;
        }
    </style>

 </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server" >
    <p>
    <table style="width: 100%; height: 80%;">
        <tr>
            <td class="style2" align="center" valign ="top" 
                style="border: thin solid #E2F0FF"  >
                <dxe:ASPxLabel ID="ASPxLabel2" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" Text="Дата с">
                </dxe:ASPxLabel>
                <dxe:ASPxDateEdit ID="adeStart" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" Height="19px" 
                    ImageFolder="~/App_Themes/Aqua/{0}/" ShowShadow="False" Width="103px" 
                    AutoPostBack="True">
                    <ValidationSettings>
                        <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" 
                            Width="14px" />
                        <ErrorFrameStyle ImageSpacing="4px">
                            <ErrorTextPaddings PaddingLeft="4px" />
                        </ErrorFrameStyle>
                    </ValidationSettings>
                    <DropDownButton>
                        <Image Height="7px" Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" Width="9px" />
                    </DropDownButton>
                    <CalendarProperties>
                        <HeaderStyle Spacing="1px" />
                        <FooterStyle Spacing="17px" />
                    </CalendarProperties>
                </dxe:ASPxDateEdit>
            </td>
            <td class="style2" align="center" valign="top" 
                style="border: thin solid #E2F0FF"  >
                <dxe:ASPxLabel ID="ASPxLabel1" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" Text="по">
                </dxe:ASPxLabel>
                <dxe:ASPxDateEdit ID="adeEnd" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" Height="19px" 
                    ImageFolder="~/App_Themes/Aqua/{0}/" ShowShadow="False" Width="103px" 
                    AutoPostBack="True">
                    <ValidationSettings>
                        <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" 
                            Width="14px" />
                        <ErrorFrameStyle ImageSpacing="4px">
                            <ErrorTextPaddings PaddingLeft="4px" />
                        </ErrorFrameStyle>
                    </ValidationSettings>
                    <DropDownButton>
                        <Image Height="7px" Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" Width="9px" />
                    </DropDownButton>
                    <CalendarProperties>
                        <HeaderStyle Spacing="1px" />
                        <FooterStyle Spacing="17px" />
                    </CalendarProperties>
                </dxe:ASPxDateEdit>
            </td>
            <td class="style2" align="center" valign="top" 
                style="border: thin solid #E2F0FF" >
                <dxe:ASPxLabel ID="ASPxLabel3" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="Категория контрагентов">
                </dxe:ASPxLabel>
                <dxe:ASPxComboBox ID="acbClientsTypes" runat="server" AutoPostBack="True" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    DataSourceID="ldsClientsType" ImageFolder="~/App_Themes/Aqua/{0}/" 
                    LoadingPanelText="" 
                    ShowShadow="False" TextField="TypeName" ValueField="Id" 
                    ValueType="System.Int32">
                    <ButtonEditEllipsisImage Height="3px" 
                        Url="~/App_Themes/Aqua/Editors/edtEllipsis.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtEllipsisDisabled.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtEllipsisHottracked.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtEllipsisPressed.png" Width="12px" />
                    <DropDownButton>
                        <Image Height="7px" Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtDropDownPressed.png" Width="9px" />
                    </DropDownButton>
                    <ValidationSettings>
                        <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" 
                            Width="14px" />
                        <ErrorFrameStyle ImageSpacing="4px">
                            <ErrorTextPaddings PaddingLeft="4px" />
                        </ErrorFrameStyle>
                    </ValidationSettings>
                </dxe:ASPxComboBox>
                </td>
            <td class="style2" align="center" valign="top" 
                style="border: thin solid #E2F0FF" >
                <dxe:ASPxLabel ID="ASPxLabel4" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="Контрагент">
                </dxe:ASPxLabel>
                <dxe:ASPxComboBox ID="acbClients" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    DataSourceID="ldsClients" ImageFolder="~/App_Themes/Aqua/{0}/" 
                    LoadingPanelText="" ShowShadow="False" TextField="ClientName" ValueField="Id" 
                    ValueType="System.Int32" AutoPostBack="True">
                    <ButtonEditEllipsisImage Height="3px" 
                        Url="~/App_Themes/Aqua/Editors/edtEllipsis.png" 
                        UrlDisabled="~/App_Themes/Aqua/Editors/edtEllipsisDisabled.png" 
                        UrlHottracked="~/App_Themes/Aqua/Editors/edtEllipsisHottracked.png" 
                        UrlPressed="~/App_Themes/Aqua/Editors/edtEllipsisPressed.png" Width="12px" />
                    <DropDownButton>
                        <Image Height="7px" Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtDropDownPressed.png" Width="9px" />
                    </DropDownButton>
                    <ValidationSettings>
                        <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" 
                            Width="14px" />
                        <ErrorFrameStyle ImageSpacing="4px">
                            <ErrorTextPaddings PaddingLeft="4px" />
                        </ErrorFrameStyle>
                    </ValidationSettings>
                </dxe:ASPxComboBox>
            </td>
            <td class="style2" align="center" valign="top" 
                style="border: thin solid #E2F0FF" >
                <dxe:ASPxLabel ID="ASPxLabel5" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Text="Номер аппаратного ключа">
                </dxe:ASPxLabel>
                <dxe:ASPxTextBox ID="atxNumber" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    Width="170px" AutoPostBack="True">
                    <ValidationSettings>
                        <ErrorImage Height="14px" Url="~/App_Themes/Aqua/Editors/edtError.png" />
                        <ErrorFrameStyle ImageSpacing="4px">
                            <ErrorTextPaddings PaddingLeft="4px" />
                        </ErrorFrameStyle>
                    </ValidationSettings>
                </dxe:ASPxTextBox>
                </td>
            <td class="style1" align="center" valign="middle" style="table-layout: auto" >
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2" align="center" valign ="middle" 
                style="border: thin solid #E2F0FF"  >
                <dxe:ASPxButton ID="abtRefresh" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    onclick="abtRefresh_Click" Text="Обновить журнал">
                </dxe:ASPxButton>
            </td>
            <td class="style2" align="center" valign="middle" 
                style="border: thin solid #E2F0FF"  >
                <dxe:ASPxButton ID="abtNew" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    onclick="abtNew_Click" Text="Добавить лицензию">
                </dxe:ASPxButton>
            </td>
            <td class="style2" align="center" valign="middle" 
                style="border: thin solid #E2F0FF" >
                <dxe:ASPxButton ID="abtFilterClear" runat="server" 
                    CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                    onclick="abtFilterClear_Click" Text="Очистить фильтр" >
                </dxe:ASPxButton>
            </td>
            <td class="style2" align="center" valign="top" 
                style="border: thin solid #E2F0FF" >
                &nbsp;</td>
            <td class="style2" align="center" valign="top" 
                style="border: thin solid #E2F0FF" >
                &nbsp;</td>
            <td class="style1" align="center" valign="middle" style="table-layout: auto" >
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="6" align="left" valign="top" height ="700px">
                <asp:SqlDataSource ID="sdsGrnLicense" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:RCSlicConnectionString %>" 
                    SelectCommand="Licences_Select" SelectCommandType="StoredProcedure" 
                    DeleteCommand="Licences_Delete" DeleteCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="adeStart" DefaultValue="" Name="DB" 
                            PropertyName="Value" Type="DateTime" />
                        <asp:ControlParameter ControlID="adeEnd" DefaultValue="" Name="DE" 
                            PropertyName="Value" Type="DateTime" />
                        <asp:ControlParameter ControlID="acbClientsTypes" DefaultValue="0" 
                            Name="TYPE_CL" PropertyName="Value" Type="Int32" />
                        <asp:ControlParameter ControlID="acbClients" DefaultValue="0" Name="CL" 
                            PropertyName="Value" Type="Int32" />
                        <asp:ControlParameter ControlID="atxNumber" DefaultValue="0" Name="KEY_NUM" 
                            PropertyName="Text" Type="String" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="ID" Type="Int32" />
                    </DeleteParameters>
                </asp:SqlDataSource>
                <asp:LinqDataSource ID="ldsClientsType" runat="server" AutoPage="False" 
                    ContextTypeName="DataClassesDataContext" TableName="ClientTypes" 
                    onselecting="ldsClientsType_Selecting">
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="ldsClients" runat="server" 
                    ContextTypeName="DataClassesDataContext" TableName="Clients" 
                    Where="Id_type == @Id_type">
                    <WhereParameters>
                        <asp:ControlParameter ControlID="acbClientsTypes" DefaultValue="0" 
                            Name="Id_type" PropertyName="Value" Type="Int32" />
                    </WhereParameters>
                </asp:LinqDataSource>
                <dxwgv:ASPxGridView ID="agvGrnLicenses" runat="server" 
                    AutoGenerateColumns="False" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                    CssPostfix="Aqua" DataSourceID="sdsGrnLicense" KeyFieldName="Id" 
                    Width="100%" EnableDefaultAppearance="False">
                    <SettingsBehavior ConfirmDelete="True" />
                    <Styles CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                        EnableDefaultAppearance="False">
                    </Styles>
                    <SettingsLoadingPanel Text="" />
                    <SettingsPager PageSize="50">
                        <AllButton>
                            <Image Height="19px" />
                        </AllButton>
                        <FirstPageButton>
                            <Image Height="19px" />
                        </FirstPageButton>
                        <LastPageButton>
                            <Image Height="19px" />
                        </LastPageButton>
                        <NextPageButton>
                            <Image Height="19px" />
                        </NextPageButton>
                        <PrevPageButton>
                            <Image Height="19px" />
                        </PrevPageButton>
                    </SettingsPager>
                    <Images ImageFolder="~/App_Themes/Aqua/{0}/">
                        <CollapsedButton Height="15px" 
                            Url="~/App_Themes/Aqua/GridView/gvCollapsedButton.png" />
                        <ExpandedButton Height="15px" 
                            Url="~/App_Themes/Aqua/GridView/gvExpandedButton.png" />
                        <DetailCollapsedButton Height="15px" 
                            Url="~/App_Themes/Aqua/GridView/gvDetailCollapsedButton.png" />
                        <DetailExpandedButton Height="15px" 
                            Url="~/App_Themes/Aqua/GridView/gvDetailExpandedButton.png" />
                        <HeaderFilter Height="19px" 
                            Url="~/App_Themes/Aqua/GridView/gvHeaderFilter.png" />
                        <HeaderActiveFilter Height="19px" 
                            Url="~/App_Themes/Aqua/GridView/gvHeaderFilterActive.png" />
                        <HeaderSortDown Height="5px" 
                            Url="~/App_Themes/Aqua/GridView/gvHeaderSortDown.png" />
                        <HeaderSortUp Height="5px" 
                            Url="~/App_Themes/Aqua/GridView/gvHeaderSortUp.png" />
                        <FilterRowButton Height="13px" />
                        <WindowResizer Height="13px" 
                            Url="~/App_Themes/Aqua/GridView/WindowResizer.png" />
                    </Images>
                    <SettingsText EmptyDataRow="Отсутствие лицензий " 
                        ConfirmDelete="Подтверждаете удаление лицензии?" />
                    <Columns>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Номер карты" FieldName="Id" 
                            ReadOnly="True" VisibleIndex="0">
                            <PropertiesHyperLinkEdit NavigateUrlFormatString="~//License//LicenseCard.aspx?CardId={0}">
                            </PropertiesHyperLinkEdit>
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataHyperLinkColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="TypeName" VisibleIndex="1" 
                            Caption="Категория контр." ReadOnly="True">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="ClientName" VisibleIndex="2" 
                            Caption="Контрагент" ReadOnly="True">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="Parent" VisibleIndex="3" 
                            Caption="Основной контр." ReadOnly="True">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="Number" VisibleIndex="4" 
                            Caption="Номер ключа" ReadOnly="True">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="LoginDisp" VisibleIndex="5" 
                            Caption="Логин диспетчера" ReadOnly="True">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="Place_qty" VisibleIndex="6" 
                            Caption="Кол-во раб.мест" ReadOnly="True">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataCheckColumn FieldName="Module_agro" VisibleIndex="7" 
                            Caption="AGRO" ReadOnly="True">
                        </dxwgv:GridViewDataCheckColumn>
                        <dxwgv:GridViewDataCheckColumn FieldName="Module_route" VisibleIndex="8" 
                            Caption="Маршруты" ReadOnly="True">
                        </dxwgv:GridViewDataCheckColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="UserName" VisibleIndex="9" 
                            Caption="Создал" ReadOnly="True">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn FieldName="DateInit" VisibleIndex="10" 
                            Caption="Дата создания" ReadOnly="True">
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="Comments" VisibleIndex="11" 
                            Caption="Комментарий" ReadOnly="True">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewCommandColumn VisibleIndex="12">
                            <DeleteButton Visible="True">
                            </DeleteButton>
                            <ClearFilterButton Visible="True">
                            </ClearFilterButton>
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                    <StylesEditors>
                        <ProgressBar Height="25px">
                        </ProgressBar>
                    </StylesEditors>
                    <ImagesEditors>
                        <CalendarFastNavPrevYear Height="19px" 
                            Url="~/App_Themes/Aqua/Editors/edtCalendarFNPrevYear.png" />
                        <CalendarFastNavNextYear Height="19px" 
                            Url="~/App_Themes/Aqua/Editors/edtCalendarFNNextYear.png" />
                        <DropDownEditDropDown Height="7px" 
                            Url="~/App_Themes/Aqua/Editors/edtDropDown.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtDropDownDisabled.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtDropDownHottracked.png" />
                        <SpinEditIncrement Height="7px" 
                            Url="~/App_Themes/Aqua/Editors/edtSpinEditIncrementImage.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditIncrementDisabledImage.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditIncrementHottrackedImage.png" />
                        <SpinEditDecrement Height="7px" 
                            Url="~/App_Themes/Aqua/Editors/edtSpinEditDecrementImage.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditDecrementDisabledImage.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditDecrementHottrackedImage.png" />
                        <SpinEditLargeIncrement Height="9px" 
                            Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncImage.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncDisabledImage.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeIncHottrackedImage.png" />
                        <SpinEditLargeDecrement Height="9px" 
                            Url="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecImage.png" 
                            UrlDisabled="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecDisabledImage.png" 
                            UrlHottracked="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" 
                            UrlPressed="~/App_Themes/Aqua/Editors/edtSpinEditLargeDecHottrackedImage.png" />
                    </ImagesEditors>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
    </table>
</p>
</asp:Content>

