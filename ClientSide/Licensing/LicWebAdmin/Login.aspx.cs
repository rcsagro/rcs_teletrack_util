﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btInput_Click(object sender, EventArgs e)
    {
        Entry();
    }
    protected void txPassword_TextChanged(object sender, EventArgs e)
    {
        Entry();
    }
    private void Entry()
    {
        albError.Visible = false;
        if (cbUsers.Value == null || txPassword.Text.Length == 0) return;
        using (DataClassesDataContext dc = new DataClassesDataContext())
        {
            int user_id = (int)cbUsers.Value;
            User us = (from usr in dc.Users where usr.Id == user_id select usr).Single();
            if (txPassword.Text == us.Password)
            {
                us.LastVisit = DateTime.Now;
                dc.SubmitChanges();
                Response.Cookies["License"]["UserId"] = user_id.ToString();
                Response.Redirect("~/License/LicenseGrn.aspx");
            }
            else
            {
                albError.Visible = true;
                albError.Text = "Неверный пароль";
            }
        }
    }
}
