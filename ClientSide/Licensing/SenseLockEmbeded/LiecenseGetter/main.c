#include "ses_v3.h"
//#include "string.h"

#define MAX_CNT_LIC 64
#define LEN_LIC     32
#define LEN_OUTBUF  256
#define LEN_KEYOUT  8 //for DES code
#define RET_SUCCESS 0
#define FILE_LIC    0x5202

#define ERROR_CRC   -10

int Error(unsigned char *buffer, int length, unsigned char nErr)
{
    int i = 0;
    if(length >= 10)
    {
        buffer[0] = 'E';
        buffer[1] = 'r';
        buffer[2] = 'r';
        buffer[3] = 'o';
        buffer[4] = 'r';
        buffer[5] = ' ';
        for(i=0; i < 3; i++)
        {
            buffer[8 - i] = (nErr%10) + 0x30;
            nErr = nErr/10;
        }
        buffer[9] = 0;
        return 10;
    }
    return -1;
}
unsigned char DecodeNumberLic(unsigned char codeReq[5])
{
    unsigned char numberLic = 0;
    numberLic = ((codeReq[0]^codeReq[1])+(codeReq[2]^codeReq[3]))^codeReq[4];
    return numberLic;
}
int ReadAndDecodeLicRec(unsigned char numb, unsigned char *buffer, int length)
{
    HANDLE hFile = 0;
    unsigned short offset = numb*LEN_LIC;
    unsigned char pbKey[] = {0x63, 0x38, 0x39, 0x6B, 0x64, 0x73, 0x77, 0x6B, 0x08, 0x61, 0x3F, 0x6C, 0x04, 0x01, 0x0E, 0x6E };
    int ret = -1;
    int i = 0;
    if(length >= LEN_LIC)
    {
        ret = _open(FILE_LIC, &hFile);
        if(ret == 0)
        {
            ret = _read(hFile, offset, LEN_LIC, buffer);
            if(ret == RET_SUCCESS)
            {
                ret = _tdes_dec(pbKey, LEN_LIC, buffer);
                if(ret == RET_SUCCESS)
                {//test crc
                    unsigned char crc = 0; //CRC licRecord
                    for (i = 0; i < LEN_LIC - 1; i++)
                        crc ^= buffer[i];
                    if(buffer[LEN_LIC-1] != (unsigned char)(0xFF - crc))
                        ret = ERROR_CRC;
                }
            }
        }
        _close(hFile);
    }
    return ret;
}
int CodeLicForReply(unsigned char *input, int lenInput, unsigned char *outBuffer, int len)
{
    int ret = 0;
    int i = 0;
    int needCript = 0;
    BYTE PrivatKey[8] = {0xD8, 0x20, 0xFD, 0x98, 0x43, 0x8F, 0x90, 0xDF};
    BYTE seanceKey[8] = {0,0,0,0,0,0,0,0};

    //count CRC
    BYTE crc = 0;
    for(i=0;i<len;i++)
        crc += outBuffer[i];
    outBuffer[len] = crc;

    for(i=1; i<lenInput; i++) //���� ��� ���������� ��� ���� (4��) �� �� �������
    {
        if(input[i] != 0)
        {
            needCript = 1;
            break;
        }
    }

    if(needCript)
    {    //generate+key
        _mem_copy(seanceKey, input, lenInput);
        ret = _des_enc(PrivatKey, 8, seanceKey);
        if(ret == RET_SUCCESS)
        {//Code Data
            ret = _des_enc(seanceKey, len, outBuffer);
        }
    }
    return ret;
}
/* SenseLock EL main procedure */
void main()
{
	unsigned char *input = pbInBuff;
	int len = bInLen;
	int ret = -1;

    unsigned char numberLic = 0;
    //unsigned char keyOut[LEN_KEYOUT];
    unsigned char outBuffer[LEN_OUTBUF];
    int i=0;

    //numberLic = input[0];
    numberLic = DecodeNumberLic(input); //5Bt = 1Bt(reqNumLic) + 4Bt(randorNum); (��� ������, ����� ������ ��� FF-val).
	/* code */
	if(numberLic < MAX_CNT_LIC)
    {
        //_mem_copy(keyOut, input+1, LEN_KEYOUT);
        ret = ReadAndDecodeLicRec(numberLic, outBuffer, LEN_OUTBUF);
        if(ret == RET_SUCCESS)
        {
            ret = CodeLicForReply(input, 5, outBuffer, LEN_LIC);
            if(ret == RET_SUCCESS)
                len = LEN_LIC+1;
        }
    }
    if(ret)//Error
    {
        len = 6;
        Error(outBuffer, len, ret);
    }

	_set_response(len, outBuffer);
	_exit();
}
