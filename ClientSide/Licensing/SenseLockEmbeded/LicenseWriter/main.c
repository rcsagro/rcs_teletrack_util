#include "ses_v3.h"
//#include "string.h"

#define MAX_CNT_LIC 64
#define LEN_LIC     32
#define LEN_OUTBUF  256
#define LEN_KEYOUT  8 //for DES code
#define RET_SUCCESS 0

#define CMD_WRITE_LIC_REC   0x21
#define FILE_LIC    0x5202

int Error(unsigned char *buffer, int length, unsigned char nErr)
{
    int i = 0;
    if(length >= 10)
    {
        buffer[0] = 'E';
        buffer[1] = 'r';
        buffer[2] = 'r';
        buffer[3] = 'o';
        buffer[4] = 'r';
        buffer[5] = ' ';
        for(i=0; i < 3; i++)
        {
            buffer[8 - i] = (nErr%10) + 0x30;
            nErr = nErr/10;
        }
        buffer[9] = 0;
        return 10;
    }
    return -1;
}
int DecodeCmd(BYTE *buffer, int len)
{
    BYTE KeyDecodeCmd[] = {0x5E, 0x24, 0xF1, 0xD0, 0x23, 0x3E, 0x4A, 0x44, 0x38, 0x16, 0x32, 0x33, 0x39, 0x10, 0xCC, 0xE6 };
    BYTE serialID[8];
    BYTE ret = -1;
    ret = _get_gbdata(GLOBAL_SERIAL_NUMBER, serialID, 8);
    if(ret == RET_SUCCESS)
    {
        ret = _des_enc(serialID, 16, KeyDecodeCmd);
        if(ret == RET_SUCCESS)
            return _tdes_dec(KeyDecodeCmd, len, buffer);
    }
    return ret;
}
int TstCmd(BYTE *buffer, int len)
{
    int ret = -1;
    BYTE crc = 0;
    int i = 0;
    for(i=0; i<len-1; i++)
        crc ^= buffer[i];
    if((0xFF - crc) == buffer[len-1])
    {//�������� �������
        ret = -2;
        if(buffer[0] == CMD_WRITE_LIC_REC) //���� ��� ������ ������
        {
            ret = -3;
            if(buffer[1] < MAX_CNT_LIC) //�������� ��� ����� ������������ ���. ��������
                ret = 0;
        }
    }
    return ret;
}

int CodeAndWriteLicRec(unsigned char numb, unsigned char *buffer, int length)
{
    HANDLE hFile = 0;
    unsigned short offset = numb*LEN_LIC;
    unsigned char pbKey[] = {0x63, 0x38, 0x39, 0x6B, 0x64, 0x73, 0x77, 0x6B, 0x08, 0x61, 0x3F, 0x6C, 0x04, 0x01, 0x0E, 0x6E };
    int ret = -1;
    if(length >= LEN_LIC)
    {
        ret = _open(FILE_LIC, &hFile);
        if(ret == 0)
        {
            ret = _tdes_enc(pbKey, LEN_LIC, buffer);
            if(ret == RET_SUCCESS)
            {//write to lic_file
                ret = _write(hFile, offset, LEN_LIC, buffer);
            }
        }
        _close(hFile);
    }
    return ret;
}
/* SenseLock EL main procedure */
void main()
{
	unsigned char *input = pbInBuff;
	int len = bInLen;
	unsigned char outBuffer[LEN_OUTBUF];
	int ret = -1;

    if(!(len%8)) //������� ����������� � ������ ���� ������� ������� ����������
    {
        ret = DecodeCmd(input, len);
        if(ret == RET_SUCCESS)
        {
            //tst crc + CMD + Num License
            ret = TstCmd(input, len);
            if(ret == RET_SUCCESS)
            {//Code License and Write
                ret = CodeAndWriteLicRec(input[1], input+1, LEN_LIC); // input+1 - �� � ������ ������� ���� �������
                _mem_copy(outBuffer,input, len);
            }
        }
    }
    if(ret)
    {
        len = 10;
        len = Error(outBuffer, len, ret);
    }

	_set_response(len, outBuffer);
	_exit();
}
