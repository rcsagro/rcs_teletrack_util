﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TDataMngrView")]
[assembly: AssemblyDescription("TDataMngr control and state monitoring tool.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("RCS")]
[assembly: AssemblyProduct("TDataMngr")]
[assembly: AssemblyCopyright("Copyright © 2008-2020 RCS")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1b2576a3-dada-44f0-8329-e1fbc69257e5")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("3.5.2.0")]
[assembly: AssemblyFileVersion("3.5.2.0")]
