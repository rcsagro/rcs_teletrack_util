namespace TDataMngrView
{
  /// <summary>
  /// �����(�����) ��������
  /// </summary>
  partial class SettingsForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnSave = new System.Windows.Forms.Button();
      this.tabPageCommon = new System.Windows.Forms.TabPage();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.label7 = new System.Windows.Forms.Label();
      this.lblRowCount = new System.Windows.Forms.Label();
      this.numRowCount = new System.Windows.Forms.NumericUpDown();
      this.checkAutoScrol = new System.Windows.Forms.CheckBox();
      this.lblInterval = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.numInterval = new System.Windows.Forms.NumericUpDown();
      this.tabControl = new System.Windows.Forms.TabControl();
      this.tabPageService = new System.Windows.Forms.TabPage();
      this.lblSrvName = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.lblIpcAgntName = new System.Windows.Forms.Label();
      this.txtIpcAgntName = new System.Windows.Forms.TextBox();
      this.txtIpcPortName = new System.Windows.Forms.TextBox();
      this.lblIpcPortName = new System.Windows.Forms.Label();
      this.txtSrvName = new System.Windows.Forms.TextBox();
      this.btnCancel = new System.Windows.Forms.Button();
      this.tabPageCommon.SuspendLayout();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numRowCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numInterval)).BeginInit();
      this.tabControl.SuspendLayout();
      this.tabPageService.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnSave
      // 
      this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnSave.Location = new System.Drawing.Point(165, 233);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(75, 23);
      this.btnSave.TabIndex = 0;
      this.btnSave.Text = "���������";
      this.btnSave.UseVisualStyleBackColor = true;
      // 
      // tabPageCommon
      // 
      this.tabPageCommon.Controls.Add(this.groupBox2);
      this.tabPageCommon.Controls.Add(this.lblInterval);
      this.tabPageCommon.Controls.Add(this.label3);
      this.tabPageCommon.Controls.Add(this.numInterval);
      this.tabPageCommon.Location = new System.Drawing.Point(4, 22);
      this.tabPageCommon.Name = "tabPageCommon";
      this.tabPageCommon.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageCommon.Size = new System.Drawing.Size(334, 199);
      this.tabPageCommon.TabIndex = 0;
      this.tabPageCommon.Text = "�����";
      this.tabPageCommon.UseVisualStyleBackColor = true;
      // 
      // groupBox2
      // 
      this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox2.Controls.Add(this.label7);
      this.groupBox2.Controls.Add(this.lblRowCount);
      this.groupBox2.Controls.Add(this.numRowCount);
      this.groupBox2.Controls.Add(this.checkAutoScrol);
      this.groupBox2.Location = new System.Drawing.Point(6, 44);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(324, 119);
      this.groupBox2.TabIndex = 9;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "���������";
      // 
      // label7
      // 
      this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(285, 29);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(33, 13);
      this.label7.TabIndex = 12;
      this.label7.Text = "(���.)";
      // 
      // lblRowCount
      // 
      this.lblRowCount.AutoSize = true;
      this.lblRowCount.Location = new System.Drawing.Point(6, 29);
      this.lblRowCount.Name = "lblRowCount";
      this.lblRowCount.Size = new System.Drawing.Size(186, 13);
      this.lblRowCount.TabIndex = 11;
      this.lblRowCount.Text = "���������� ��������� ����� � UI:";
      // 
      // numRowCount
      // 
      this.numRowCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.numRowCount.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::TDataMngrView.Properties.Settings.Default, "UIRowsCount", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
      this.numRowCount.Location = new System.Drawing.Point(199, 27);
      this.numRowCount.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.numRowCount.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.numRowCount.Name = "numRowCount";
      this.numRowCount.Size = new System.Drawing.Size(80, 20);
      this.numRowCount.TabIndex = 10;
      this.numRowCount.Value = global::TDataMngrView.Properties.Settings.Default.UIRowsCount;
      // 
      // checkAutoScrol
      // 
      this.checkAutoScrol.AutoSize = true;
      this.checkAutoScrol.Checked = true;
      this.checkAutoScrol.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkAutoScrol.Location = new System.Drawing.Point(9, 71);
      this.checkAutoScrol.Name = "checkAutoScrol";
      this.checkAutoScrol.Size = new System.Drawing.Size(222, 17);
      this.checkAutoScrol.TabIndex = 9;
      this.checkAutoScrol.Text = "������������� ������������ ������";
      this.checkAutoScrol.UseVisualStyleBackColor = true;
      // 
      // lblInterval
      // 
      this.lblInterval.AutoSize = true;
      this.lblInterval.Location = new System.Drawing.Point(12, 20);
      this.lblInterval.Name = "lblInterval";
      this.lblInterval.Size = new System.Drawing.Size(172, 13);
      this.lblInterval.TabIndex = 3;
      this.lblInterval.Text = "������������� ������ �������:";
      // 
      // label3
      // 
      this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(290, 20);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(34, 13);
      this.label3.TabIndex = 2;
      this.label3.Text = "(���.)";
      // 
      // numInterval
      // 
      this.numInterval.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.numInterval.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::TDataMngrView.Properties.Settings.Default, "PeriodOfControl", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
      this.numInterval.Location = new System.Drawing.Point(205, 18);
      this.numInterval.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.numInterval.Name = "numInterval";
      this.numInterval.Size = new System.Drawing.Size(80, 20);
      this.numInterval.TabIndex = 1;
      this.numInterval.Value = global::TDataMngrView.Properties.Settings.Default.PeriodOfControl;
      // 
      // tabControl
      // 
      this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tabControl.Controls.Add(this.tabPageCommon);
      this.tabControl.Controls.Add(this.tabPageService);
      this.tabControl.Location = new System.Drawing.Point(0, 0);
      this.tabControl.Name = "tabControl";
      this.tabControl.SelectedIndex = 0;
      this.tabControl.Size = new System.Drawing.Size(342, 225);
      this.tabControl.TabIndex = 1;
      // 
      // tabPageService
      // 
      this.tabPageService.Controls.Add(this.lblSrvName);
      this.tabPageService.Controls.Add(this.groupBox1);
      this.tabPageService.Controls.Add(this.txtSrvName);
      this.tabPageService.Location = new System.Drawing.Point(4, 22);
      this.tabPageService.Name = "tabPageService";
      this.tabPageService.Size = new System.Drawing.Size(334, 199);
      this.tabPageService.TabIndex = 1;
      this.tabPageService.Text = "������";
      this.tabPageService.UseVisualStyleBackColor = true;
      // 
      // lblSrvName
      // 
      this.lblSrvName.AutoSize = true;
      this.lblSrvName.Location = new System.Drawing.Point(13, 15);
      this.lblSrvName.Name = "lblSrvName";
      this.lblSrvName.Size = new System.Drawing.Size(74, 13);
      this.lblSrvName.TabIndex = 9;
      this.lblSrvName.Text = "��� ������:";
      // 
      // groupBox1
      // 
      this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox1.Controls.Add(this.lblIpcAgntName);
      this.groupBox1.Controls.Add(this.txtIpcAgntName);
      this.groupBox1.Controls.Add(this.txtIpcPortName);
      this.groupBox1.Controls.Add(this.lblIpcPortName);
      this.groupBox1.Location = new System.Drawing.Point(7, 63);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(322, 110);
      this.groupBox1.TabIndex = 10;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "��������� IPC ����������";
      // 
      // lblIpcAgntName
      // 
      this.lblIpcAgntName.AutoSize = true;
      this.lblIpcAgntName.Location = new System.Drawing.Point(6, 70);
      this.lblIpcAgntName.Name = "lblIpcAgntName";
      this.lblIpcAgntName.Size = new System.Drawing.Size(89, 13);
      this.lblIpcAgntName.TabIndex = 12;
      this.lblIpcAgntName.Text = "��� IPC-������:";
      // 
      // txtIpcAgntName
      // 
      this.txtIpcAgntName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.txtIpcAgntName.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::TDataMngrView.Properties.Settings.Default, "agentName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
      this.txtIpcAgntName.Enabled = false;
      this.txtIpcAgntName.Location = new System.Drawing.Point(141, 67);
      this.txtIpcAgntName.Name = "txtIpcAgntName";
      this.txtIpcAgntName.ReadOnly = true;
      this.txtIpcAgntName.Size = new System.Drawing.Size(173, 20);
      this.txtIpcAgntName.TabIndex = 13;
      this.txtIpcAgntName.Text = global::TDataMngrView.Properties.Settings.Default.agentName;
      // 
      // txtIpcPortName
      // 
      this.txtIpcPortName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.txtIpcPortName.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::TDataMngrView.Properties.Settings.Default, "IPC_PortName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
      this.txtIpcPortName.Enabled = false;
      this.txtIpcPortName.Location = new System.Drawing.Point(141, 30);
      this.txtIpcPortName.Name = "txtIpcPortName";
      this.txtIpcPortName.ReadOnly = true;
      this.txtIpcPortName.Size = new System.Drawing.Size(173, 20);
      this.txtIpcPortName.TabIndex = 11;
      this.txtIpcPortName.Text = global::TDataMngrView.Properties.Settings.Default.IPC_PortName;
      // 
      // lblIpcPortName
      // 
      this.lblIpcPortName.AutoSize = true;
      this.lblIpcPortName.Location = new System.Drawing.Point(6, 33);
      this.lblIpcPortName.Name = "lblIpcPortName";
      this.lblIpcPortName.Size = new System.Drawing.Size(98, 13);
      this.lblIpcPortName.TabIndex = 10;
      this.lblIpcPortName.Text = "IPC-���� �������:";
      // 
      // txtSrvName
      // 
      this.txtSrvName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.txtSrvName.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::TDataMngrView.Properties.Settings.Default, "ServiceName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
      this.txtSrvName.Enabled = false;
      this.txtSrvName.Location = new System.Drawing.Point(148, 12);
      this.txtSrvName.Name = "txtSrvName";
      this.txtSrvName.ReadOnly = true;
      this.txtSrvName.Size = new System.Drawing.Size(173, 20);
      this.txtSrvName.TabIndex = 7;
      this.txtSrvName.Text = global::TDataMngrView.Properties.Settings.Default.ServiceName;
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(258, 233);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 2;
      this.btnCancel.Text = "������";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // SettingsForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(342, 266);
      this.ControlBox = false;
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnSave);
      this.Controls.Add(this.tabControl);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(350, 300);
      this.Name = "SettingsForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "���������";
      this.tabPageCommon.ResumeLayout(false);
      this.tabPageCommon.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numRowCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numInterval)).EndInit();
      this.tabControl.ResumeLayout(false);
      this.tabPageService.ResumeLayout(false);
      this.tabPageService.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabPage tabPageCommon;
    private System.Windows.Forms.TabControl tabControl;
    public System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Label lblInterval;
    private System.Windows.Forms.Label label3;
    protected System.Windows.Forms.NumericUpDown numInterval;
    private System.Windows.Forms.TabPage tabPageService;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.TextBox txtIpcAgntName;
    private System.Windows.Forms.Label lblIpcAgntName;
    private System.Windows.Forms.TextBox txtIpcPortName;
    private System.Windows.Forms.Label lblIpcPortName;
    private System.Windows.Forms.Label lblSrvName;
    internal System.Windows.Forms.TextBox txtSrvName;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label lblRowCount;
    internal System.Windows.Forms.NumericUpDown numRowCount;
    private System.Windows.Forms.CheckBox checkAutoScrol;
    private System.Windows.Forms.Button btnCancel;
  }
}