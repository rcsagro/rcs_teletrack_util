﻿namespace TDataMngrView
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      this.outputText = new System.Windows.Forms.TextBox();
      this.tabError = new System.Windows.Forms.TabPage();
      this.errorsInfo = new System.Windows.Forms.TextBox();
      this.tabState = new System.Windows.Forms.TabPage();
      this.tabControl = new System.Windows.Forms.TabControl();
      this.tabData = new System.Windows.Forms.TabPage();
      this.dataListView = new System.Windows.Forms.ListView();
      this.colID = new System.Windows.Forms.ColumnHeader();
      this.colTTLogin = new System.Windows.Forms.ColumnHeader();
      this.colType = new System.Windows.Forms.ColumnHeader();
      this.colPacketLength = new System.Windows.Forms.ColumnHeader();
      this.colPacketSize = new System.Windows.Forms.ColumnHeader();
      this.colDateTime = new System.Windows.Forms.ColumnHeader();
      this.tabStatistics = new System.Windows.Forms.TabPage();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.errorsCountTextBox = new System.Windows.Forms.TextBox();
      this.connCountTextBox = new System.Windows.Forms.TextBox();
      this.deltaTimeOfStartTextBox = new System.Windows.Forms.TextBox();
      this.startTimeTextBox = new System.Windows.Forms.TextBox();
      this.label9 = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.lastTimeIncomDataTextBox = new System.Windows.Forms.TextBox();
      this.label13 = new System.Windows.Forms.Label();
      this.incomDataSizeTextBox = new System.Windows.Forms.TextBox();
      this.label14 = new System.Windows.Forms.Label();
      this.incomingPacketCount = new System.Windows.Forms.TextBox();
      this.label15 = new System.Windows.Forms.Label();
      this.sendQueryCount = new System.Windows.Forms.TextBox();
      this.label16 = new System.Windows.Forms.Label();
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.btnStart = new System.Windows.Forms.ToolStripButton();
      this.btnStop = new System.Windows.Forms.ToolStripButton();
      this.btnPause = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.btnSettings = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
      this.btnAbout = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
      this.btnExit = new System.Windows.Forms.ToolStripButton();
      this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
      this.Main = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.serviceStartContextMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.serviceStopContextMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.statistic = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.label8 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.label6 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.tabError.SuspendLayout();
      this.tabState.SuspendLayout();
      this.tabControl.SuspendLayout();
      this.tabData.SuspendLayout();
      this.tabStatistics.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.toolStrip1.SuspendLayout();
      this.Main.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // outputText
      // 
      this.outputText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.outputText.Location = new System.Drawing.Point(4, 4);
      this.outputText.Margin = new System.Windows.Forms.Padding(4);
      this.outputText.MaxLength = 229;
      this.outputText.Multiline = true;
      this.outputText.Name = "outputText";
      this.outputText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.outputText.Size = new System.Drawing.Size(743, 406);
      this.outputText.TabIndex = 39;
      // 
      // tabError
      // 
      this.tabError.Controls.Add(this.errorsInfo);
      this.tabError.Location = new System.Drawing.Point(4, 25);
      this.tabError.Margin = new System.Windows.Forms.Padding(4);
      this.tabError.Name = "tabError";
      this.tabError.Padding = new System.Windows.Forms.Padding(4);
      this.tabError.Size = new System.Drawing.Size(751, 414);
      this.tabError.TabIndex = 1;
      this.tabError.Text = "Ошибки";
      this.tabError.UseVisualStyleBackColor = true;
      // 
      // errorsInfo
      // 
      this.errorsInfo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.errorsInfo.Location = new System.Drawing.Point(4, 4);
      this.errorsInfo.Margin = new System.Windows.Forms.Padding(4);
      this.errorsInfo.Multiline = true;
      this.errorsInfo.Name = "errorsInfo";
      this.errorsInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.errorsInfo.Size = new System.Drawing.Size(743, 406);
      this.errorsInfo.TabIndex = 0;
      // 
      // tabState
      // 
      this.tabState.Controls.Add(this.outputText);
      this.tabState.Location = new System.Drawing.Point(4, 25);
      this.tabState.Margin = new System.Windows.Forms.Padding(4);
      this.tabState.Name = "tabState";
      this.tabState.Padding = new System.Windows.Forms.Padding(4);
      this.tabState.Size = new System.Drawing.Size(751, 414);
      this.tabState.TabIndex = 0;
      this.tabState.Text = "Состояние службы";
      this.tabState.UseVisualStyleBackColor = true;
      // 
      // tabControl
      // 
      this.tabControl.Controls.Add(this.tabData);
      this.tabControl.Controls.Add(this.tabState);
      this.tabControl.Controls.Add(this.tabError);
      this.tabControl.Controls.Add(this.tabStatistics);
      this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControl.Location = new System.Drawing.Point(0, 25);
      this.tabControl.Margin = new System.Windows.Forms.Padding(4);
      this.tabControl.Name = "tabControl";
      this.tabControl.SelectedIndex = 0;
      this.tabControl.Size = new System.Drawing.Size(759, 443);
      this.tabControl.TabIndex = 8;
      // 
      // tabData
      // 
      this.tabData.Controls.Add(this.dataListView);
      this.tabData.Location = new System.Drawing.Point(4, 25);
      this.tabData.Margin = new System.Windows.Forms.Padding(4);
      this.tabData.Name = "tabData";
      this.tabData.Padding = new System.Windows.Forms.Padding(4);
      this.tabData.Size = new System.Drawing.Size(751, 414);
      this.tabData.TabIndex = 2;
      this.tabData.Text = "Данные";
      this.tabData.UseVisualStyleBackColor = true;
      // 
      // dataListView
      // 
      this.dataListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colID,
            this.colTTLogin,
            this.colType,
            this.colPacketLength,
            this.colPacketSize,
            this.colDateTime});
      this.dataListView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dataListView.FullRowSelect = true;
      this.dataListView.GridLines = true;
      this.dataListView.HoverSelection = true;
      this.dataListView.Location = new System.Drawing.Point(4, 4);
      this.dataListView.MultiSelect = false;
      this.dataListView.Name = "dataListView";
      this.dataListView.ShowGroups = false;
      this.dataListView.Size = new System.Drawing.Size(743, 406);
      this.dataListView.TabIndex = 0;
      this.dataListView.UseCompatibleStateImageBehavior = false;
      this.dataListView.View = System.Windows.Forms.View.Details;
      // 
      // colID
      // 
      this.colID.Text = "№";
      // 
      // colTTLogin
      // 
      this.colTTLogin.Text = "Телетрек";
      this.colTTLogin.Width = 110;
      // 
      // colType
      // 
      this.colType.Text = "Тип пакета";
      this.colType.Width = 97;
      // 
      // colPacketLength
      // 
      this.colPacketLength.Text = "Кол-во пакетов";
      this.colPacketLength.Width = 151;
      // 
      // colPacketSize
      // 
      this.colPacketSize.Text = "Размер (byte)";
      this.colPacketSize.Width = 133;
      // 
      // colDateTime
      // 
      this.colDateTime.Text = "Время/дата получения";
      this.colDateTime.Width = 180;
      // 
      // tabStatistics
      // 
      this.tabStatistics.BackColor = System.Drawing.SystemColors.Control;
      this.tabStatistics.Controls.Add(this.tableLayoutPanel1);
      this.tabStatistics.Location = new System.Drawing.Point(4, 25);
      this.tabStatistics.Margin = new System.Windows.Forms.Padding(4);
      this.tabStatistics.Name = "tabStatistics";
      this.tabStatistics.Padding = new System.Windows.Forms.Padding(4);
      this.tabStatistics.Size = new System.Drawing.Size(751, 414);
      this.tabStatistics.TabIndex = 3;
      this.tabStatistics.Text = "Статистика";
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 1;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.groupBox4, 0, 0);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 4);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.82609F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.17391F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(743, 406);
      this.tableLayoutPanel1.TabIndex = 1;
      // 
      // groupBox3
      // 
      this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
      this.groupBox3.Controls.Add(this.errorsCountTextBox);
      this.groupBox3.Controls.Add(this.connCountTextBox);
      this.groupBox3.Controls.Add(this.deltaTimeOfStartTextBox);
      this.groupBox3.Controls.Add(this.startTimeTextBox);
      this.groupBox3.Controls.Add(this.label9);
      this.groupBox3.Controls.Add(this.label10);
      this.groupBox3.Controls.Add(this.label11);
      this.groupBox3.Controls.Add(this.label12);
      this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox3.Location = new System.Drawing.Point(3, 197);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(737, 206);
      this.groupBox3.TabIndex = 58;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Сервис";
      // 
      // errorsCountTextBox
      // 
      this.errorsCountTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.errorsCountTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.errorsCountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.errorsCountTextBox.Location = new System.Drawing.Point(558, 143);
      this.errorsCountTextBox.Margin = new System.Windows.Forms.Padding(1, 4, 4, 4);
      this.errorsCountTextBox.Name = "errorsCountTextBox";
      this.errorsCountTextBox.ReadOnly = true;
      this.errorsCountTextBox.Size = new System.Drawing.Size(169, 16);
      this.errorsCountTextBox.TabIndex = 65;
      this.errorsCountTextBox.Text = "0";
      // 
      // connCountTextBox
      // 
      this.connCountTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.connCountTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.connCountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.connCountTextBox.Location = new System.Drawing.Point(558, 105);
      this.connCountTextBox.Margin = new System.Windows.Forms.Padding(1, 4, 4, 4);
      this.connCountTextBox.Name = "connCountTextBox";
      this.connCountTextBox.ReadOnly = true;
      this.connCountTextBox.Size = new System.Drawing.Size(169, 16);
      this.connCountTextBox.TabIndex = 64;
      this.connCountTextBox.Text = "0";
      // 
      // deltaTimeOfStartTextBox
      // 
      this.deltaTimeOfStartTextBox.AcceptsReturn = true;
      this.deltaTimeOfStartTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.deltaTimeOfStartTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.deltaTimeOfStartTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.deltaTimeOfStartTextBox.Location = new System.Drawing.Point(558, 68);
      this.deltaTimeOfStartTextBox.Margin = new System.Windows.Forms.Padding(1, 4, 4, 4);
      this.deltaTimeOfStartTextBox.Name = "deltaTimeOfStartTextBox";
      this.deltaTimeOfStartTextBox.ReadOnly = true;
      this.deltaTimeOfStartTextBox.Size = new System.Drawing.Size(169, 16);
      this.deltaTimeOfStartTextBox.TabIndex = 63;
      this.deltaTimeOfStartTextBox.Text = "Нет данных";
      // 
      // startTimeTextBox
      // 
      this.startTimeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.startTimeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.startTimeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.startTimeTextBox.Location = new System.Drawing.Point(558, 32);
      this.startTimeTextBox.Margin = new System.Windows.Forms.Padding(1, 4, 4, 4);
      this.startTimeTextBox.Name = "startTimeTextBox";
      this.startTimeTextBox.ReadOnly = true;
      this.startTimeTextBox.Size = new System.Drawing.Size(169, 16);
      this.startTimeTextBox.TabIndex = 62;
      this.startTimeTextBox.Text = "Нет данных";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label9.ForeColor = System.Drawing.Color.Black;
      this.label9.Location = new System.Drawing.Point(7, 142);
      this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(162, 17);
      this.label9.TabIndex = 61;
      this.label9.Text = "Количество ошибок:";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label10.ForeColor = System.Drawing.Color.Black;
      this.label10.Location = new System.Drawing.Point(7, 104);
      this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(284, 17);
      this.label10.TabIndex = 60;
      this.label10.Text = "Количество соединений с сервером:";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label11.ForeColor = System.Drawing.Color.Black;
      this.label11.Location = new System.Drawing.Point(7, 67);
      this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(234, 17);
      this.label11.TabIndex = 59;
      this.label11.Text = "Общее время работы службы:";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label12.ForeColor = System.Drawing.Color.Black;
      this.label12.Location = new System.Drawing.Point(7, 31);
      this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(293, 17);
      this.label12.TabIndex = 58;
      this.label12.Text = "Время и дата начала работы службы:";
      // 
      // groupBox4
      // 
      this.groupBox4.BackColor = System.Drawing.SystemColors.Control;
      this.groupBox4.Controls.Add(this.lastTimeIncomDataTextBox);
      this.groupBox4.Controls.Add(this.label13);
      this.groupBox4.Controls.Add(this.incomDataSizeTextBox);
      this.groupBox4.Controls.Add(this.label14);
      this.groupBox4.Controls.Add(this.incomingPacketCount);
      this.groupBox4.Controls.Add(this.label15);
      this.groupBox4.Controls.Add(this.sendQueryCount);
      this.groupBox4.Controls.Add(this.label16);
      this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox4.Location = new System.Drawing.Point(3, 3);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(737, 188);
      this.groupBox4.TabIndex = 57;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "Данные";
      // 
      // lastTimeIncomDataTextBox
      // 
      this.lastTimeIncomDataTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lastTimeIncomDataTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.lastTimeIncomDataTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lastTimeIncomDataTextBox.Location = new System.Drawing.Point(558, 102);
      this.lastTimeIncomDataTextBox.Margin = new System.Windows.Forms.Padding(1, 4, 4, 4);
      this.lastTimeIncomDataTextBox.Name = "lastTimeIncomDataTextBox";
      this.lastTimeIncomDataTextBox.ReadOnly = true;
      this.lastTimeIncomDataTextBox.Size = new System.Drawing.Size(169, 16);
      this.lastTimeIncomDataTextBox.TabIndex = 63;
      this.lastTimeIncomDataTextBox.Text = "Нет данных";
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label13.ForeColor = System.Drawing.Color.Black;
      this.label13.Location = new System.Drawing.Point(7, 101);
      this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(346, 17);
      this.label13.TabIndex = 62;
      this.label13.Text = "Время и дата получения последних данных: ";
      // 
      // incomDataSizeTextBox
      // 
      this.incomDataSizeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.incomDataSizeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.incomDataSizeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.incomDataSizeTextBox.Location = new System.Drawing.Point(558, 69);
      this.incomDataSizeTextBox.Margin = new System.Windows.Forms.Padding(1, 4, 4, 4);
      this.incomDataSizeTextBox.Name = "incomDataSizeTextBox";
      this.incomDataSizeTextBox.ReadOnly = true;
      this.incomDataSizeTextBox.Size = new System.Drawing.Size(169, 16);
      this.incomDataSizeTextBox.TabIndex = 61;
      this.incomDataSizeTextBox.Text = "0";
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label14.ForeColor = System.Drawing.Color.Black;
      this.label14.Location = new System.Drawing.Point(7, 68);
      this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(258, 17);
      this.label14.TabIndex = 60;
      this.label14.Text = "Объем входящего трафика (Mb): ";
      // 
      // incomingPacketCount
      // 
      this.incomingPacketCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.incomingPacketCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.incomingPacketCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.incomingPacketCount.Location = new System.Drawing.Point(558, 36);
      this.incomingPacketCount.Margin = new System.Windows.Forms.Padding(1, 4, 4, 4);
      this.incomingPacketCount.Name = "incomingPacketCount";
      this.incomingPacketCount.ReadOnly = true;
      this.incomingPacketCount.Size = new System.Drawing.Size(169, 16);
      this.incomingPacketCount.TabIndex = 58;
      this.incomingPacketCount.Text = "0";
      // 
      // label15
      // 
      this.label15.AutoSize = true;
      this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label15.ForeColor = System.Drawing.Color.Black;
      this.label15.Location = new System.Drawing.Point(7, 35);
      this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(247, 17);
      this.label15.TabIndex = 56;
      this.label15.Text = "Количество принятых пакетов: ";
      // 
      // sendQueryCount
      // 
      this.sendQueryCount.AcceptsReturn = true;
      this.sendQueryCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.sendQueryCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.sendQueryCount.Enabled = false;
      this.sendQueryCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.sendQueryCount.Location = new System.Drawing.Point(558, 138);
      this.sendQueryCount.Margin = new System.Windows.Forms.Padding(1, 4, 4, 4);
      this.sendQueryCount.Name = "sendQueryCount";
      this.sendQueryCount.ReadOnly = true;
      this.sendQueryCount.Size = new System.Drawing.Size(169, 16);
      this.sendQueryCount.TabIndex = 59;
      this.sendQueryCount.Text = "0";
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Enabled = false;
      this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label16.ForeColor = System.Drawing.Color.Black;
      this.label16.Location = new System.Drawing.Point(7, 137);
      this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(372, 17);
      this.label16.TabIndex = 57;
      this.label16.Text = "Количество запросов на пропущенные данные: ";
      // 
      // toolStrip1
      // 
      this.toolStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnStart,
            this.btnStop,
            this.btnPause,
            this.toolStripSeparator2,
            this.btnSettings,
            this.toolStripSeparator3,
            this.btnAbout,
            this.toolStripSeparator4,
            this.btnExit});
      this.toolStrip1.Location = new System.Drawing.Point(0, 0);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(759, 25);
      this.toolStrip1.TabIndex = 10;
      this.toolStrip1.Text = "toolStrip1";
      // 
      // btnStart
      // 
      this.btnStart.Enabled = false;
      this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
      this.btnStart.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.btnStart.Name = "btnStart";
      this.btnStart.Size = new System.Drawing.Size(67, 22);
      this.btnStart.Text = "Старт";
      this.btnStart.Click += new System.EventHandler(this.ServiceStart_Click);
      // 
      // btnStop
      // 
      this.btnStop.Enabled = false;
      this.btnStop.Image = ((System.Drawing.Image)(resources.GetObject("btnStop.Image")));
      this.btnStop.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.btnStop.Name = "btnStop";
      this.btnStop.Size = new System.Drawing.Size(60, 22);
      this.btnStop.Text = "Стоп";
      this.btnStop.Click += new System.EventHandler(this.ServiceStop_Click);
      // 
      // btnPause
      // 
      this.btnPause.Enabled = false;
      this.btnPause.Image = ((System.Drawing.Image)(resources.GetObject("btnPause.Image")));
      this.btnPause.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.btnPause.Name = "btnPause";
      this.btnPause.Size = new System.Drawing.Size(70, 22);
      this.btnPause.Text = "Пауза";
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
      // 
      // btnSettings
      // 
      this.btnSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnSettings.Image")));
      this.btnSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.btnSettings.Name = "btnSettings";
      this.btnSettings.Size = new System.Drawing.Size(99, 22);
      this.btnSettings.Text = "Настройки";
      this.btnSettings.Click += new System.EventHandler(this.CallSettingsDlg_Click);
      // 
      // toolStripSeparator3
      // 
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
      // 
      // btnAbout
      // 
      this.btnAbout.Image = ((System.Drawing.Image)(resources.GetObject("btnAbout.Image")));
      this.btnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.btnAbout.Name = "btnAbout";
      this.btnAbout.Size = new System.Drawing.Size(113, 22);
      this.btnAbout.Text = "О программе";
      this.btnAbout.Click += new System.EventHandler(this.BtnAbout_Click);
      // 
      // toolStripSeparator4
      // 
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
      // 
      // btnExit
      // 
      this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
      this.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.btnExit.Margin = new System.Windows.Forms.Padding(25, 1, 0, 2);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(68, 22);
      this.btnExit.Text = "Выход";
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // notifyIcon
      // 
      this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
      this.notifyIcon.BalloonTipText = "MailServiceController";
      this.notifyIcon.BalloonTipTitle = "TDataMngrView";
      this.notifyIcon.ContextMenuStrip = this.Main;
      this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
      this.notifyIcon.Text = "OnLineServiceControl";
      this.notifyIcon.Visible = true;
      this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
      // 
      // Main
      // 
      this.Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serviceStartContextMenu,
            this.serviceStopContextMenu,
            this.statistic,
            this.toolStripSeparator1});
      this.Main.Name = "Main";
      this.Main.Size = new System.Drawing.Size(195, 76);
      this.Main.Text = "Запуск сервиса";
      // 
      // serviceStartContextMenu
      // 
      this.serviceStartContextMenu.Enabled = false;
      this.serviceStartContextMenu.Image = ((System.Drawing.Image)(resources.GetObject("serviceStartContextMenu.Image")));
      this.serviceStartContextMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.serviceStartContextMenu.Name = "serviceStartContextMenu";
      this.serviceStartContextMenu.Size = new System.Drawing.Size(194, 22);
      this.serviceStartContextMenu.Text = "Запуск службы";
      this.serviceStartContextMenu.Click += new System.EventHandler(this.ServiceStart_Click);
      // 
      // serviceStopContextMenu
      // 
      this.serviceStopContextMenu.Enabled = false;
      this.serviceStopContextMenu.Image = ((System.Drawing.Image)(resources.GetObject("serviceStopContextMenu.Image")));
      this.serviceStopContextMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.serviceStopContextMenu.Name = "serviceStopContextMenu";
      this.serviceStopContextMenu.Size = new System.Drawing.Size(194, 22);
      this.serviceStopContextMenu.Text = "Остановка службы";
      this.serviceStopContextMenu.Click += new System.EventHandler(this.ServiceStop_Click);
      // 
      // statistic
      // 
      this.statistic.Name = "statistic";
      this.statistic.Size = new System.Drawing.Size(194, 22);
      this.statistic.Text = "Просмотр статистики";
      this.statistic.Click += new System.EventHandler(this.statistic_Click);
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(191, 6);
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.label8);
      this.groupBox2.Controls.Add(this.label7);
      this.groupBox2.Controls.Add(this.label5);
      this.groupBox2.Controls.Add(this.label3);
      this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox2.Location = new System.Drawing.Point(3, 201);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(673, 210);
      this.groupBox2.TabIndex = 58;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Сервис";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label8.ForeColor = System.Drawing.Color.Black;
      this.label8.Location = new System.Drawing.Point(7, 142);
      this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(162, 17);
      this.label8.TabIndex = 61;
      this.label8.Text = "Количество ошибок:";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label7.ForeColor = System.Drawing.Color.Black;
      this.label7.Location = new System.Drawing.Point(7, 104);
      this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(195, 17);
      this.label7.TabIndex = 60;
      this.label7.Text = "Количество соединений:";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.ForeColor = System.Drawing.Color.Black;
      this.label5.Location = new System.Drawing.Point(7, 67);
      this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(239, 17);
      this.label5.TabIndex = 59;
      this.label5.Text = "Общее время работы сервиса:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.ForeColor = System.Drawing.Color.Black;
      this.label3.Location = new System.Drawing.Point(7, 31);
      this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(285, 17);
      this.label3.TabIndex = 58;
      this.label3.Text = "Дата/время начала работы сервиса:";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.label6);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox1.Location = new System.Drawing.Point(3, 3);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(673, 192);
      this.groupBox1.TabIndex = 57;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Данные";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.ForeColor = System.Drawing.Color.Black;
      this.label6.Location = new System.Drawing.Point(7, 124);
      this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(292, 17);
      this.label6.TabIndex = 62;
      this.label6.Text = "Время получения последних данных: ";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.ForeColor = System.Drawing.Color.Black;
      this.label1.Location = new System.Drawing.Point(7, 91);
      this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(249, 17);
      this.label1.TabIndex = 60;
      this.label1.Text = "Объм входящего трафика (Mb): ";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.ForeColor = System.Drawing.Color.Black;
      this.label2.Location = new System.Drawing.Point(7, 58);
      this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(247, 17);
      this.label2.TabIndex = 56;
      this.label2.Text = "Количество принятых пакетов: ";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.ForeColor = System.Drawing.Color.Black;
      this.label4.Location = new System.Drawing.Point(7, 28);
      this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(232, 17);
      this.label4.TabIndex = 57;
      this.label4.Text = "Количество принятых писем: ";
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(759, 468);
      this.Controls.Add(this.tabControl);
      this.Controls.Add(this.toolStrip1);
      this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Margin = new System.Windows.Forms.Padding(4);
      this.MinimumSize = new System.Drawing.Size(600, 300);
      this.Name = "MainForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Контроль работы службы:  ";
      this.Load += new System.EventHandler(this.ServiceControlForm_Load);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ServiceControlForm_FormClosed);
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
      this.Resize += new System.EventHandler(this.ServiceControlForm_Resize);
      this.tabError.ResumeLayout(false);
      this.tabError.PerformLayout();
      this.tabState.ResumeLayout(false);
      this.tabState.PerformLayout();
      this.tabControl.ResumeLayout(false);
      this.tabData.ResumeLayout(false);
      this.tabStatistics.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.Main.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    public System.Windows.Forms.TextBox outputText;
    public System.Windows.Forms.TabPage tabError;
    public System.Windows.Forms.TextBox errorsInfo;
    public System.Windows.Forms.TabPage tabState;
    public System.Windows.Forms.TabControl tabControl;
    public System.Windows.Forms.TabPage tabData;
    public System.Windows.Forms.ToolStripButton btnStop;
    public System.Windows.Forms.ToolStripButton btnPause;
    public System.Windows.Forms.ToolStripButton btnSettings;
    public System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.NotifyIcon notifyIcon;
    private System.Windows.Forms.ContextMenuStrip Main;
    private System.Windows.Forms.ToolStripMenuItem serviceStartContextMenu;
    private System.Windows.Forms.ToolStripMenuItem serviceStopContextMenu;
    private System.Windows.Forms.ToolStripMenuItem statistic;
    private System.Windows.Forms.ListView dataListView;
    private System.Windows.Forms.ColumnHeader colID;
    private System.Windows.Forms.ColumnHeader colPacketSize;
    private System.Windows.Forms.ColumnHeader colPacketLength;
    private System.Windows.Forms.ColumnHeader colDateTime;
    public System.Windows.Forms.TabPage tabStatistics;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.GroupBox groupBox3;
    public System.Windows.Forms.TextBox errorsCountTextBox;
    public System.Windows.Forms.TextBox connCountTextBox;
    public System.Windows.Forms.TextBox deltaTimeOfStartTextBox;
    public System.Windows.Forms.TextBox startTimeTextBox;
    public System.Windows.Forms.Label label9;
    public System.Windows.Forms.Label label10;
    public System.Windows.Forms.Label label11;
    public System.Windows.Forms.Label label12;
    private System.Windows.Forms.GroupBox groupBox4;
    public System.Windows.Forms.TextBox lastTimeIncomDataTextBox;
    public System.Windows.Forms.Label label13;
    public System.Windows.Forms.TextBox incomDataSizeTextBox;
    public System.Windows.Forms.Label label14;
    public System.Windows.Forms.TextBox incomingPacketCount;
    public System.Windows.Forms.Label label15;
    public System.Windows.Forms.TextBox sendQueryCount;
    public System.Windows.Forms.Label label16;
    private System.Windows.Forms.GroupBox groupBox2;
    public System.Windows.Forms.Label label8;
    public System.Windows.Forms.Label label7;
    public System.Windows.Forms.Label label5;
    public System.Windows.Forms.Label label3;
    private System.Windows.Forms.GroupBox groupBox1;
    public System.Windows.Forms.Label label6;
    public System.Windows.Forms.Label label1;
    public System.Windows.Forms.Label label2;
    public System.Windows.Forms.Label label4;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    public System.Windows.Forms.ToolStripButton btnStart;
    private System.Windows.Forms.ColumnHeader colType;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripButton btnAbout;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    private System.Windows.Forms.ToolStripButton btnExit;
    private System.Windows.Forms.ColumnHeader colTTLogin;
  }
}

