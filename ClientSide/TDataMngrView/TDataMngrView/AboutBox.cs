using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Win32;
using TDataMngrView.Properties;

namespace TDataMngrView
{
  partial class AboutBox : Form
  {
    /// <summary>
    /// �����������
    /// </summary>
    /// <param name="serviceName">������������ ������</param>
    public AboutBox(string serviceName)
    {
      InitializeComponent();
      Localize();

      //  Initialize the AboutBox to display the product information from the assembly information.
      //  Change assembly information settings for your application through either:
      //  - Project->Properties->Application->Assembly Information
      //  - AssemblyInfo.cs
      this.Text = String.Format("About {0}", AssemblyTitle);
      labelProductName.Text = AssemblyTitle;
      labelVersion.Text = String.Format("Version: {0}", AssemblyVersion);
      labelCopyright.Text = AssemblyCopyright;
      labelCompanyName.Text = AssemblyCompany;

      txtSrvName.Text = serviceName;

      string srvPath = GetServiceImagePath(serviceName);

      if ((String.IsNullOrEmpty(srvPath)) || (!File.Exists(srvPath)))
      {
        srvPath = Resources.ExeFileNotFound;
      }
      else
      {
        txtSrvVersion.Text = FileVersionInfo.GetVersionInfo(srvPath).FileVersion;
      }
      txtAssemblyPath.Text = srvPath;
    }

    private void Localize()
    {
      Text = Resources.AboutApp;
      textBoxDescription.Text = Resources.AssemblyDescription;
      gboxServiceInfo.Text = Resources.AboutService;
      lblSrvName.Text = Resources.Name;
      lblSrvVersion.Text = Resources.Version;
      lblAssemblyPath.Text = Resources.ExeAssembly;
    }

    /// <summary>
    /// ���������� ���� � ������������ ����� ������ �� ��� �����
    /// </summary>
    /// <param name="serviceName">��� ������</param>
    /// <returns>���� � ������</returns>
    private string GetServiceImagePath(string serviceName)
    {
      const string srv_path = @"SYSTEM\CurrentControlSet\Services\{0}";
      string registryPath = String.Format(srv_path, serviceName);
      RegistryKey keyHKLM = Registry.LocalMachine;
      RegistryKey key = keyHKLM.OpenSubKey(registryPath);

      if (key == null)
      {
        return String.Empty;
      }

      object tmpValue = key.GetValue("ImagePath");
      key.Close();
      if (tmpValue == null)
      {
        return String.Empty;
      }

      string value = tmpValue.ToString();
      if (value == null)
      {
        return String.Empty;
      }

      value = Environment.ExpandEnvironmentVariables(value);

      // �������� ����������� �������
      const string double_quotes = "\"";
      int index = value.IndexOf(double_quotes);
      while (index >= 0)
      {
        value = value.Remove(index, 1);
        index = value.IndexOf(double_quotes);
      }

      return value;
    }

    #region Assembly Attribute Accessors

    public string AssemblyTitle
    {
      get
      {
        // Get all Title attributes on this assembly
        object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(
          typeof(AssemblyTitleAttribute), false);
        // If there is at least one Title attribute
        if (attributes.Length > 0)
        {
          // Select the first one
          AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
          // If it is not an empty string, return it
          if (titleAttribute.Title != "")
          {
            return titleAttribute.Title;
          }
        }
        // If there was no Title attribute, or if the Title attribute was the empty string, return the .exe name
        return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
      }
    }

    public string AssemblyVersion
    {
      get
      {
        return Assembly.GetExecutingAssembly().GetName().Version.ToString();
      }
    }

    public string AssemblyDescription
    {
      get
      {
        // Get all Description attributes on this assembly
        object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
        // If there aren't any Description attributes, return an empty string
        if (attributes.Length == 0)
          return "";
        // If there is a Description attribute, return its value
        return ((AssemblyDescriptionAttribute)attributes[0]).Description;
      }
    }

    public string AssemblyProduct
    {
      get
      {
        // Get all Product attributes on this assembly
        object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
        // If there aren't any Product attributes, return an empty string
        if (attributes.Length == 0)
        {
          return "";
        }
        // If there is a Product attribute, return its value
        return ((AssemblyProductAttribute)attributes[0]).Product;
      }
    }

    public string AssemblyCopyright
    {
      get
      {
        // Get all Copyright attributes on this assembly
        object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
        // If there aren't any Copyright attributes, return an empty string
        if (attributes.Length == 0)
        {
          return "";
        }
        // If there is a Copyright attribute, return its value
        return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
      }
    }

    public string AssemblyCompany
    {
      get
      {
        // Get all Company attributes on this assembly
        object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
        // If there aren't any Company attributes, return an empty string
        if (attributes.Length == 0)
        {
          return "";
        }
        // If there is a Company attribute, return its value
        return ((AssemblyCompanyAttribute)attributes[0]).Company;
      }
    }
    #endregion
  }
}
