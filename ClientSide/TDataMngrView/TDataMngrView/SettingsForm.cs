using System;
using System.Windows.Forms;
using TDataMngrView.Properties;

namespace TDataMngrView
{

  public partial class SettingsForm : Form
  {
    /// <summary>
    /// �����������
    /// </summary>
    public SettingsForm()
    {
      InitializeComponent();
      Localize();
    }

    private void Localize()
    {
      Text = Resources.Settings;
      tabPageCommon.Text = Resources.Common;
      lblInterval.Text = Resources.ServicePollPeriodicity;
      groupBox2.Text = Resources.Intarface;
      lblRowCount.Text = Resources.UiRowsNumber;
      checkAutoScrol.Text = Resources.AutoScrollingFlag;
      label3.Text = Resources.SecShortForm;
      label7.Text = Resources.RowShortForm;

      btnSave.Text = Resources.Save;
      btnCancel.Text = Resources.Cancel;

      tabPageService.Text = Resources.Service;
      lblSrvName.Text = Resources.ServiceName;
      groupBox1.Text = Resources.IpcSettings;
      lblIpcPortName.Text = Resources.IpcPort;
      lblIpcAgntName.Text = Resources.IpcAgent;
    }

    /// <summary>
    /// ����� ������
    /// </summary>
    /// <param name="sender">object sender</param>
    /// <param name="e">EventArgs e</param>
    private void radioBtCl_CheckedChanged(object sender, EventArgs e)
    {
    }

    public int Period
    {
      get { return period; }
      set { period = value; }
    }
    private int period = 10;

    private void numericUpDown1_ValueChanged(object sender, EventArgs e)
    {
      period = (int)(numInterval.Value * 1000);
    }

    public bool EditSettings()
    {
      return this.ShowDialog() == DialogResult.OK ? true : false;
    }
  }
}