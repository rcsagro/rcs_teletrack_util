﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.Remoting;
using System.Threading;
using System.Windows.Forms;
using RemoteInteractionLib;
using TDataMngrView.Properties;

namespace TDataMngrView
{

  public partial class MainForm : Form
  {
    #region Формирует дату в заданном формате

    /// <summary>
    /// {0} {1}
    /// </summary>
    private const string LOG_FORMAT = "{0} {1}";
    /// <summary>
    /// dd.MM.yy   HH:mm:ss:fff
    /// </summary>
    private const string LONG_TIME_FORMAT = "dd.MM.yy   HH:mm:ss:fff ";
    /// <summary>
    /// HH:mm:ss:fff
    /// </summary>
    private const string SHORT_TIME_FORMAT = "HH:mm:ss:fff ";
    /// <summary>
    /// HH:mm:ss:fff       dd.MM.yy
    /// </summary>
    private const string REV_LONG_TIME_FORMAT = "HH:mm:ss:fff       dd.MM.yy";
    /// <summary>
    ///  dd_MM_yyyy
    /// </summary>
    private const string DATE_FORMAT = " dd_MM_yyyy";
    
    string LnDTime()
    {
      return DateTime.Now.ToString(LONG_TIME_FORMAT);
    }
    string ShDTime()
    {
      return DateTime.Now.ToString(SHORT_TIME_FORMAT);
    }

    string RevDTime()
    {
      return DateTime.Now.ToString(REV_LONG_TIME_FORMAT);
    }

    #endregion

    MyServiceState oldServiceState = 0;
    MyServiceState curServiceState = 0;

    /// <summary>
    /// Итератор
    /// </summary>
    protected int Number
    {
      get { return Interlocked.Increment(ref number); }
    }
    private static int number;

    /// <summary>
    /// Имя сервиса (задаваемое пользователем при старте)
    /// </summary>
    protected string serviceName;

    public int PeriodOfControll
    {
      get { return periodOfControll; }
      set
      {
        periodOfControll = value * 1000;
        if (timerControllOfService != null)
        {
          timerControllOfService.Change(periodOfControll, periodOfControll);
        }
      }
    }
    int periodOfControll = 10000;

    /// <summary>
    /// Кол-во выводимых строк
    /// </summary>
    public int UILineCount
    {
      get { return uiLineCount; }
      set { uiLineCount = value; }
    }
    int uiLineCount = 1000;

    //private int outpacket;
    //private int inpacket;

    private Icon icon1;
    private Icon icon2;
    private Icon icon3;
    private Icon icon4;
    private Icon icon5;

    ClientIpc ipcClient;

    private System.Threading.Timer timerControllOfService;

    Queue<Icon> iconQueue = new Queue<Icon>();
    List<Icon> ls = new List<Icon>();

    public MainForm()
    {
      InitializeComponent();
      Localize();

      SettingsForm st = new SettingsForm();
      serviceName = st.txtSrvName.Text;
      periodOfControll = st.Period * 1000;
      ipcClient = new ClientIpc(serviceName);

      try
      {
        icon1 = new Icon("Icon\\DataFromToServer.ico");
        icon2 = new Icon("Icon\\DataToServer.ico");
        icon3 = new Icon("Icon\\DataFromServer.ico");
        icon4 = new Icon("Icon\\NoData.ico");
        icon5 = new Icon("Icon\\StopOrError.ico");
        timerControllOfService = new System.Threading.Timer(
          new TimerCallback(MainControl), null, 2000, periodOfControll);
      }
      catch (Exception e)
      {
        MessageBox.Show(String.Format("Error {0}", e.Message),
          Resources.IconsNotFound, MessageBoxButtons.AbortRetryIgnore);
      }
    }

    private void Localize()
    {
      Text = Resources.MainFormCaption;
      btnStart.Text = Resources.Start;
      btnStart.ToolTipText = Resources.Start;

      btnStop.Text = Resources.Stop;
      btnStop.ToolTipText = Resources.Stop;

      btnPause.Text = Resources.Pause;
      btnPause.ToolTipText = Resources.Pause;

      btnSettings.Text = Resources.Settings;
      btnSettings.ToolTipText = Resources.Settings;

      btnAbout.Text = Resources.AboutApp;
      btnAbout.ToolTipText = Resources.AboutApp;

      btnExit.Text = Resources.Exit;
      btnExit.ToolTipText = Resources.Exit;

      tabData.Text = Resources.Data;
      tabState.Text = Resources.ServiceState;
      tabError.Text = Resources.Errors;
      tabStatistics.Text = Resources.Statistics;

      groupBox4.Text = Resources.Data;
      label15.Text = Resources.ReceivedPacketsNumber;
      label14.Text = Resources.IncomingTraffic;
      label13.Text = Resources.LatestDataTime;
      label16.Text = Resources.MissingDataRequestsNumber;

      groupBox3.Text = Resources.Service;
      label12.Text = Resources.StartServiceTime;
      label11.Text = Resources.TotalWorkingTime;
      label10.Text = Resources.ServerConnectionsNumber;
      label9.Text = Resources.ErrorsNumber;

      colTTLogin.Text = Resources.Teletrack;
      colType.Text = Resources.PacketType;
      colPacketLength.Text = Resources.PacketsNumber;
      colPacketSize.Text = Resources.PacketSize;
      colDateTime.Text = Resources.ReceiptTime;
    }

    private volatile string headTextSrvState = "";

    void MainControl(object obj)
    {
      oldServiceState = curServiceState;
      try
      {
        curServiceState = ipcClient.GetServiceState(serviceName);

        switch (curServiceState)
        {
          case MyServiceState.Running:
            if (oldServiceState != curServiceState)
            {
              headTextSrvState = String.Format(
                Resources.ServiceWorking, serviceName);
              UIServiceRunning(headTextSrvState);
            }
            if (ChanelInteraction.remoutService == null)
            {
              ipcClient.remoutService =
                ChanelInteraction.OpenClientChanel(serviceName);
            }
            else
            {
              ContainerDataFromService ss =
                ChanelInteraction.remoutService.GetDataFromService();
              OutDataFromService(ss);
            }
            break;

          case MyServiceState.StartPending:
            if (oldServiceState != curServiceState)
            {
              headTextSrvState = String.Format(Resources.ServiceStarting, serviceName);
              UIServiceRunning(headTextSrvState);
            }
            break;

          case MyServiceState.Stopped:
            if (oldServiceState != curServiceState)
            {
              headTextSrvState = String.Format(Resources.ServiceStopped, serviceName);
              UIServiceStoped(headTextSrvState);
            }
            break;

          case MyServiceState.StopPending:
            if (oldServiceState != curServiceState)
            {
              headTextSrvState = String.Format(Resources.ServiceStoppig, serviceName);
              UIServiceStoped(headTextSrvState);
            }
            break;

          case MyServiceState.NotFound:
            if (oldServiceState != curServiceState)
            {
              headTextSrvState = String.Format(Resources.ServiceNotFound, serviceName);
              UIServiceNotFound(headTextSrvState);
            }
            break;
        }
      }
      catch (RemotingException ex)
      {
        WriteErros(String.Format("{0} {1}", ex.Message, ex.TargetSite));
        SetStopOrErrIcon();
      }
      catch (Exception ex)
      {
        WriteErros(String.Format("{0} {1}", ex.Message, ex.TargetSite));
        SetStopOrErrIcon();
      }
    }

    delegate void UIDelegate(string msg);

    void UILoginOfConnection(string msg)
    {
      if (this.InvokeRequired)
      {
        UIDelegate delg = new UIDelegate(UILoginOfConnection);
        this.Invoke(delg, new object[] { msg });
      }
      else
      {
        this.Text = String.Format(
          Resources.HeaderWithLogin, headTextSrvState, msg);
      }
    }

    /// <summary>
    /// Вывод сообщения в трее
    /// </summary>
    /// <param name="msg"></param>
    private void ShowBallonTip(string msg)
    {
      if (!String.IsNullOrEmpty(msg))
      {
        notifyIcon.BalloonTipText = msg;
        notifyIcon.ShowBalloonTip(3000);
      }
    }

    void UIServiceRunning(string msg)
    {
      if (this.InvokeRequired)
      {
        UIDelegate delg = new UIDelegate(UIServiceRunning);
        this.Invoke(delg, new object[] { msg });
      }
      else
      {
        btnStart.Enabled = false;
        btnStop.Enabled = true;
        serviceStartContextMenu.Enabled = false;
        serviceStopContextMenu.Enabled = true;

        WriteCurInfo(msg);
        SetAckPackChanges();
        //SetEnvelopIcon();
        this.Text = msg;
        ShowBallonTip(msg);
      }
    }

    void UIServiceStoped(string msg)
    {
      if (this.InvokeRequired)
      {
        UIDelegate delg = new UIDelegate(UIServiceStoped);
        this.Invoke(delg, new object[] { msg });
      }
      else
      {
        btnStart.Enabled = true;
        btnStop.Enabled = false;
        serviceStartContextMenu.Enabled = true;
        serviceStopContextMenu.Enabled = false;

        WriteCurInfo(msg);
        SetStopOrErrIcon();
        this.Text = msg;
        ShowBallonTip(msg);
      }
    }

    void UIServiceNotFound(string msg)
    {
      if (this.InvokeRequired)
      {
        UIDelegate delg = new UIDelegate(UIServiceNotFound);
        this.Invoke(delg, new object[] { msg });
      }
      else
      {
        btnStart.Enabled = false;
        btnStop.Enabled = false;
        serviceStartContextMenu.Enabled = false;
        serviceStopContextMenu.Enabled = false;

        WriteCurInfo(msg);
        SetStopOrErrIcon();
        this.Text = msg;
        ShowBallonTip(msg);
      }
    }

    delegate void ListDelegate(ListViewItem lv);

    void ListWrite(ListViewItem lv)
    {
      if (this.InvokeRequired)
      {
        ListDelegate listDelg = new ListDelegate(ListWrite);
        this.Invoke(listDelg, new object[] { lv });
      }
      else
      {
        if (dataListView.Items.Count > UILineCount)
        {
          dataListView.Items.Clear();
        }
        dataListView.Items.Add(lv);
        lv.EnsureVisible();
      }
    }

    delegate void OutListDataDelegate(ContainerDataFromService ss);

    /// <summary>
    /// Выводит данные в ListView
    /// </summary>
    /// <param name="ss"></param>
    void OutDataFromService(ContainerDataFromService ss)
    {
      UILoginOfConnection(ss.boxName);
      TimeOfServiceStart(ss.dtServiceStart);
      ConnectionCount(ss.connectionCount);

      if (ss.lstLett.Count > 0)
      {
        foreach (ILetterInfo lett in ss.lstLett)
        {
          ListViewItem lv = new ListViewItem(Number.ToString());

          lv.SubItems.Add(lett.Teletrack);
          lv.SubItems.Add(lett.PacketType);
          lv.SubItems.Add(lett.PackCountInLetter.ToString());
          lv.SubItems.Add(lett.LettSize.ToString());
          lv.SubItems.Add(lett.DateOfIncom.ToString(REV_LONG_TIME_FORMAT));
          ListWrite(lv);
        }

        SetDataFromToServerIcon();

        //Вывод данных на закладку статистики
        //LettersCount(ss.lstLett.Count);
        PacketsCount(ss.incBinCount);
        IncomDataSize(ss.incTrafic);
        TimeOfLastIncomData(ss.lstLett[ss.lstLett.Count - 1].DateOfIncom);
      }

      if (ss.lstErrors.Count > 0)
      {
        SetStopOrErrIcon();
        //SetErrorInLetter();
        foreach (string str in ss.lstErrors)
          WriteErros(str);
      }

      //Обмен служебными пакетами
      if (ss.lstLett.Count == 0 && ss.lstErrors.Count == 0)
        SetAckPackChanges();
    }

    delegate void WriteOutputDelegate(string output);

    public void WriteCurInfo(string msg)
    {
      string str = LnDTime() + msg;
      if (this.InvokeRequired)
      {
        WriteOutputDelegate writeOutputDelegate = new WriteOutputDelegate(WriteCurInfo);
        this.Invoke(writeOutputDelegate, new object[] { str });
      }
      else
      {
        if (outputText.Lines.Length > UILineCount)
        {
          outputText.Clear();
        }
        outputText.AppendText(str + Environment.NewLine);
      }
    }

    public void WriteErros(string err)
    {
      if (this.InvokeRequired)
      {
        WriteOutputDelegate writeOutputDelegate = new WriteOutputDelegate(WriteErros);
        this.Invoke(writeOutputDelegate, new object[] { err });
      }
      else
      {
        ErrorsCount();
        if (errorsInfo.Lines.Length > UILineCount)
        {
          errorsInfo.Clear();
        }
        errorsInfo.AppendText(String.Format("{0}{1}", err, Environment.NewLine));
      }
    }

    delegate void IncomLettAndPacketCountDelegate(long incount);

    public void LettersCount(long lettCount)
    {
      if (this.InvokeRequired)
      {
        IncomLettAndPacketCountDelegate writeOutputDelegate =
          new IncomLettAndPacketCountDelegate(LettersCount);
        this.Invoke(writeOutputDelegate, new object[] { lettCount });
      }
      else
      {
        //Количество полученных писем
        sendQueryCount.Text = lettCount.ToString();
      }
    }

    public void PacketsCount(long pkCount)
    {
      if (this.InvokeRequired)
      {
        IncomLettAndPacketCountDelegate writeOutputDelegate =
          new IncomLettAndPacketCountDelegate(PacketsCount);
        this.Invoke(writeOutputDelegate, new object[] { pkCount });
      }
      else
      {
        //Количество полученных пакетов
        incomingPacketCount.Text = pkCount.ToString();
      }
    }

    public void ConnectionCount(long cnCount)
    {
      if (this.InvokeRequired)
      {
        IncomLettAndPacketCountDelegate cnCountDelegate =
          new IncomLettAndPacketCountDelegate(ConnectionCount);
        this.Invoke(cnCountDelegate, new object[] { cnCount });
      }
      else
      {
        //Количество полученных пакетов
        connCountTextBox.Text = cnCount.ToString();
      }
    }

    int errCount = 0;
    delegate void ErrorsCountDelegate();
    public void ErrorsCount()
    {
      if (this.InvokeRequired)
      {
        ErrorsCountDelegate cnCountDelegate = new ErrorsCountDelegate(ErrorsCount);
        this.Invoke(cnCountDelegate);
      }
      else
      {
        //Количество ошибок
        errCount++;
        errorsCountTextBox.Text = errCount.ToString();
      }
    }

    delegate void IncomDataSizeDelegate(long incount);

    public void IncomDataSize(long dataSize)
    {
      if (this.InvokeRequired)
      {
        IncomDataSizeDelegate delg = new IncomDataSizeDelegate(IncomDataSize);
        this.Invoke(delg, new object[] { dataSize });
      }
      else
      {
        //Объем трафика
        incomDataSizeTextBox.Text =
          string.Format("{0:F3}", (((float)(long)dataSize) / 1048576));
      }
    }

    delegate void LastTimeDelegate(DateTime date);

    public void TimeOfLastIncomData(DateTime date)
    {
      if (this.InvokeRequired)
      {
        LastTimeDelegate delg = new LastTimeDelegate(TimeOfLastIncomData);
        this.Invoke(delg, new object[] { date });
      }
      else
      {
        lastTimeIncomDataTextBox.Text = date.ToString(REV_LONG_TIME_FORMAT);
      }
    }

    delegate void ServiceStartTimeDelegate(DateTime dt);

    void TimeOfServiceStart(DateTime dt)
    {
      if (this.InvokeRequired)
      {
        ServiceStartTimeDelegate delg =
          new ServiceStartTimeDelegate(TimeOfServiceStart);
        this.Invoke(delg, new object[] { dt });
      }
      else
      {
        DateTime curDt = DateTime.Now;
        startTimeTextBox.Text = dt.ToString(REV_LONG_TIME_FORMAT);
        TimeSpan tsp = curDt - dt;
        deltaTimeOfStartTextBox.Text = string.Format(Resources.DateTimeFormat,
          tsp.Days, tsp.Hours, tsp.Minutes, tsp.Seconds);
      }
    }

    private void ServiceControlForm_Resize(object sender, EventArgs e)
    {
      if (FormWindowState.Minimized == WindowState)
        Hide();
    }

    private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      Show();
      WindowState = FormWindowState.Normal;
    }

    #region Запуск/Остановка сервиса

    private void ServiceStart_Click(object sender, EventArgs e)
    {
      btnStart.Enabled = false;
      serviceStartContextMenu.Enabled = false;
      ClearStatistics();
      new Thread(new ThreadStart(ServiceStart)).Start();
    }

    /// <summary>
    /// Сброс всей статистики
    /// </summary>
    private void ClearStatistics()
    {
      dataListView.Items.Clear();
      incomingPacketCount.Text = "0";
      incomDataSizeTextBox.Text = "0";
      lastTimeIncomDataTextBox.Text = Resources.NoData;
      sendQueryCount.Text = "0";

      startTimeTextBox.Text = Resources.NoData;
      deltaTimeOfStartTextBox.Text = Resources.NoData;
      connCountTextBox.Text = "0";

      errorsCountTextBox.Text = "0";
      errorsInfo.Clear();
      errCount = 0;
    }

    private void ServiceStart()
    {
      try
      {
        ipcClient.MailServiceStart();
      }
      catch (System.ServiceProcess.TimeoutException ex)
      {
        WriteErros(String.Format(Resources.StartTimeoutError, ex.Message));
      }
      catch (Exception ex)
      {
        WriteErros(ex.Message + " " + ex.TargetSite);
        SetStopOrErrIcon();
        //UIServiceStoped("  " + ex.Message);
      }
    }

    private void ServiceStop_Click(object sender, EventArgs e)
    {
      btnStop.Enabled = false;
      serviceStopContextMenu.Enabled = false;
      new Thread(new ThreadStart(ServiceStop)).Start();
    }

    private void ServiceStop()
    {
      try
      {
        ipcClient.MailServiceStop();
      }
      catch (System.ServiceProcess.TimeoutException ex)
      {
        WriteErros(String.Format(Resources.StopTimeoutError, ex.Message));
      }
      catch (Exception ex)
      {
        WriteErros(ex.Message + " " + ex.TargetSite);
        SetStopOrErrIcon();
        //UIServiceStoped("  " + ex.Message);
      }
    }

    #endregion

    private void ServiceControlForm_Load(object sender, EventArgs e)
    {
    }

    private void ServiceControlForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      ipcClient.Dispose();
    }

    private void statistic_Click(object sender, EventArgs e)
    {
      Show();
      WindowState = FormWindowState.Normal;
      tabControl.SelectedIndex = 3;
    }

    private void CallSettingsDlg_Click(object sender, EventArgs e)
    {
      using (SettingsForm st = new SettingsForm())
      {
        if (st.EditSettings())
        {
          PeriodOfControll = st.Period;
          UILineCount = (int)st.numRowCount.Value;
        }
      }
    }

    #region Иконки

    delegate void IconSetDelegate();

    public void SetEnvelopIcon()
    {
      if (this.InvokeRequired)
      {
        IconSetDelegate delg = new IconSetDelegate(SetEnvelopIcon);
        Invoke(delg);
      }
      else
      {
        notifyIcon.Icon = icon4;
        this.Icon = icon4;
      }
    }

    public void SetGetMailIcon()
    {
      if (this.InvokeRequired)
      {
        IconSetDelegate delg = new IconSetDelegate(SetGetMailIcon);
        Invoke(delg);
      }
      else
      {
        notifyIcon.Icon = icon1;
        this.Icon = icon1;
      }
    }

    public void SetSendLetIcon()
    {
      if (this.InvokeRequired)
      {
        IconSetDelegate delg = new IconSetDelegate(SetSendLetIcon);
        Invoke(delg);
      }
      else
      {
        notifyIcon.Icon = icon3;
        this.Icon = icon3;
      }
    }

    public void SetStopOrErrIcon()
    {
      if (this.InvokeRequired)
      {
        IconSetDelegate delg = new IconSetDelegate(SetStopOrErrIcon);
        Invoke(delg);
      }
      else
      {
        notifyIcon.Icon = icon5;
        this.Icon = icon5;
      }
    }

    public void SetErrorInLetter()
    {
      if (this.InvokeRequired)
      {
        IconSetDelegate delg = new IconSetDelegate(SetErrorInLetter);
        Invoke(delg);
      }
      else
      {
        notifyIcon.Icon = icon5;
        this.Icon = icon5;
      }
    }

    /// <summary>
    /// Устанавливает иконку - прием данных
    /// </summary>
    void SetDataFromToServerIcon()
    {
      if (this.InvokeRequired)
      {
        IconSetDelegate delg = new IconSetDelegate(SetDataFromToServerIcon);
        Invoke(delg);
      }
      else
      {
        notifyIcon.Icon = icon1;
        this.Icon = icon1;
      }
    }

    void SetDataToServerIcon()
    {
      if (this.InvokeRequired)
      {
        IconSetDelegate delg = new IconSetDelegate(SetDataToServerIcon);
        Invoke(delg);
      }
      else
      {
        notifyIcon.Icon = icon2;
        this.Icon = icon2;
      }
    }

    void SetDataFromServerIcon()
    {
      if (this.InvokeRequired)
      {
        IconSetDelegate delg = new IconSetDelegate(SetDataToServerIcon);
        Invoke(delg);
      }
      else
      {
        notifyIcon.Icon = icon3;
        this.Icon = icon3;
      }
    }

    void SetNoDataIcon()
    {
      if (this.InvokeRequired)
      {
        IconSetDelegate delg = new IconSetDelegate(SetNoDataIcon);
        Invoke(delg);
      }
      else
      {
        notifyIcon.Icon = icon4;
        this.Icon = icon4;
      }
    }

    private void SetAckPackChanges()
    {
      SetDataToServerIcon();
      Thread.Sleep(800);
      SetNoDataIcon();
      Thread.Sleep(600);
      SetDataFromServerIcon();
      Thread.Sleep(600);
      SetNoDataIcon();
    }

    #endregion

    private void testToolStripMenuItem_Click(object sender, EventArgs e)
    {
      MainControl(this);
    }

    private void toolStripButton2_Click(object sender, EventArgs e)
    {
      SetAckPackChanges();
    }

    private void toolStripButton2_Click_1(object sender, EventArgs e)
    {
      IncomDataSize(320);
    }

    private bool isCloseButtonPushed;

    /// <summary>
    /// Отслеживаем закрытие формы. 
    /// юзеру разрешено закрывать только используя кнопку "Выход" 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      if ((e.CloseReason == CloseReason.UserClosing) && (!isCloseButtonPushed))
      {
        e.Cancel = true;
        this.WindowState = FormWindowState.Minimized;
      }
    }

    /// <summary>
    /// Нажатие на кнопку выход
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnExit_Click(object sender, EventArgs e)
    {
      isCloseButtonPushed = true;
      Close();
    }

    private void BtnAbout_Click(object sender, EventArgs e)
    {
      new AboutBox(serviceName).ShowDialog();
    }
  }
}