namespace TDataMngrView
{
	partial class AboutBox
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutBox));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.logoPictureBox = new System.Windows.Forms.PictureBox();
            this.labelProductName = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelCopyright = new System.Windows.Forms.Label();
            this.labelCompanyName = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.gboxServiceInfo = new System.Windows.Forms.GroupBox();
            this.txtAssemblyPath = new System.Windows.Forms.TextBox();
            this.txtSrvVersion = new System.Windows.Forms.TextBox();
            this.txtSrvName = new System.Windows.Forms.TextBox();
            this.lblAssemblyPath = new System.Windows.Forms.Label();
            this.lblSrvVersion = new System.Windows.Forms.Label();
            this.lblSrvName = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).BeginInit();
            this.gboxServiceInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67F));
            this.tableLayoutPanel.Controls.Add(this.logoPictureBox, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.labelProductName, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.labelVersion, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.labelCopyright, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.labelCompanyName, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.textBoxDescription, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.okButton, 1, 6);
            this.tableLayoutPanel.Controls.Add(this.gboxServiceInfo, 1, 5);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(9, 9);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 7;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.808379F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.808379F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.808379F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.808379F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.4359F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.55128F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.25641F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(455, 312);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // logoPictureBox
            // 
            this.logoPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("logoPictureBox.Image")));
            this.logoPictureBox.Location = new System.Drawing.Point(3, 3);
            this.logoPictureBox.Name = "logoPictureBox";
            this.tableLayoutPanel.SetRowSpan(this.logoPictureBox, 7);
            this.logoPictureBox.Size = new System.Drawing.Size(144, 306);
            this.logoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logoPictureBox.TabIndex = 12;
            this.logoPictureBox.TabStop = false;
            // 
            // labelProductName
            // 
            this.labelProductName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProductName.Location = new System.Drawing.Point(156, 0);
            this.labelProductName.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelProductName.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelProductName.Name = "labelProductName";
            this.labelProductName.Size = new System.Drawing.Size(296, 17);
            this.labelProductName.TabIndex = 19;
            this.labelProductName.Text = "GPS ��������";
            this.labelProductName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelVersion
            // 
            this.labelVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelVersion.Location = new System.Drawing.Point(156, 18);
            this.labelVersion.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelVersion.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(296, 17);
            this.labelVersion.TabIndex = 0;
            this.labelVersion.Text = "������";
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCopyright
            // 
            this.labelCopyright.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCopyright.Location = new System.Drawing.Point(156, 36);
            this.labelCopyright.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelCopyright.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelCopyright.Name = "labelCopyright";
            this.labelCopyright.Size = new System.Drawing.Size(296, 17);
            this.labelCopyright.TabIndex = 21;
            this.labelCopyright.Text = "Copyright @ RCS Ltd";
            this.labelCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCompanyName.Location = new System.Drawing.Point(156, 54);
            this.labelCompanyName.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelCompanyName.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Size = new System.Drawing.Size(296, 17);
            this.labelCompanyName.TabIndex = 22;
            this.labelCompanyName.Text = "RCS Ltd";
            this.labelCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDescription.Location = new System.Drawing.Point(156, 75);
            this.textBoxDescription.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ReadOnly = true;
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxDescription.Size = new System.Drawing.Size(296, 63);
            this.textBoxDescription.TabIndex = 23;
            this.textBoxDescription.TabStop = false;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.okButton.Location = new System.Drawing.Point(377, 286);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 24;
            this.okButton.Text = "&OK";
            // 
            // gboxServiceInfo
            // 
            this.gboxServiceInfo.Controls.Add(this.txtAssemblyPath);
            this.gboxServiceInfo.Controls.Add(this.txtSrvVersion);
            this.gboxServiceInfo.Controls.Add(this.txtSrvName);
            this.gboxServiceInfo.Controls.Add(this.lblAssemblyPath);
            this.gboxServiceInfo.Controls.Add(this.lblSrvVersion);
            this.gboxServiceInfo.Controls.Add(this.lblSrvName);
            this.gboxServiceInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxServiceInfo.Location = new System.Drawing.Point(153, 144);
            this.gboxServiceInfo.Name = "gboxServiceInfo";
            this.gboxServiceInfo.Size = new System.Drawing.Size(299, 132);
            this.gboxServiceInfo.TabIndex = 25;
            this.gboxServiceInfo.TabStop = false;
            this.gboxServiceInfo.Text = "� ������";
            // 
            // txtAssemblyPath
            // 
            this.txtAssemblyPath.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtAssemblyPath.Location = new System.Drawing.Point(3, 76);
            this.txtAssemblyPath.Multiline = true;
            this.txtAssemblyPath.Name = "txtAssemblyPath";
            this.txtAssemblyPath.ReadOnly = true;
            this.txtAssemblyPath.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAssemblyPath.Size = new System.Drawing.Size(293, 53);
            this.txtAssemblyPath.TabIndex = 5;
            // 
            // txtSrvVersion
            // 
            this.txtSrvVersion.Location = new System.Drawing.Point(59, 37);
            this.txtSrvVersion.Name = "txtSrvVersion";
            this.txtSrvVersion.ReadOnly = true;
            this.txtSrvVersion.Size = new System.Drawing.Size(234, 20);
            this.txtSrvVersion.TabIndex = 4;
            // 
            // txtSrvName
            // 
            this.txtSrvName.Location = new System.Drawing.Point(59, 13);
            this.txtSrvName.Name = "txtSrvName";
            this.txtSrvName.ReadOnly = true;
            this.txtSrvName.Size = new System.Drawing.Size(234, 20);
            this.txtSrvName.TabIndex = 3;
            // 
            // lblAssemblyPath
            // 
            this.lblAssemblyPath.AutoSize = true;
            this.lblAssemblyPath.Location = new System.Drawing.Point(6, 60);
            this.lblAssemblyPath.Name = "lblAssemblyPath";
            this.lblAssemblyPath.Size = new System.Drawing.Size(108, 13);
            this.lblAssemblyPath.TabIndex = 2;
            this.lblAssemblyPath.Text = "����������� ����";
            // 
            // lblSrvVersion
            // 
            this.lblSrvVersion.AutoSize = true;
            this.lblSrvVersion.Location = new System.Drawing.Point(6, 40);
            this.lblSrvVersion.Name = "lblSrvVersion";
            this.lblSrvVersion.Size = new System.Drawing.Size(44, 13);
            this.lblSrvVersion.TabIndex = 1;
            this.lblSrvVersion.Text = "������";
            // 
            // lblSrvName
            // 
            this.lblSrvName.AutoSize = true;
            this.lblSrvName.Location = new System.Drawing.Point(6, 16);
            this.lblSrvName.Name = "lblSrvName";
            this.lblSrvName.Size = new System.Drawing.Size(29, 13);
            this.lblSrvName.TabIndex = 0;
            this.lblSrvName.Text = "���";
            // 
            // AboutBox
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 330);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutBox";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "� ���������";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).EndInit();
            this.gboxServiceInfo.ResumeLayout(false);
            this.gboxServiceInfo.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
		private System.Windows.Forms.PictureBox logoPictureBox;
		private System.Windows.Forms.Label labelProductName;
		private System.Windows.Forms.Label labelVersion;
		private System.Windows.Forms.Label labelCopyright;
		private System.Windows.Forms.Label labelCompanyName;
		private System.Windows.Forms.TextBox textBoxDescription;
		private System.Windows.Forms.Button okButton;
    private System.Windows.Forms.GroupBox gboxServiceInfo;
    private System.Windows.Forms.Label lblSrvName;
    private System.Windows.Forms.Label lblAssemblyPath;
    private System.Windows.Forms.Label lblSrvVersion;
    private System.Windows.Forms.TextBox txtAssemblyPath;
    private System.Windows.Forms.TextBox txtSrvVersion;
    private System.Windows.Forms.TextBox txtSrvName;
	}
}
