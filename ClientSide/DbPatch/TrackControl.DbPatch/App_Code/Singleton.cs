using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.DbPatch
{
  /// <summary>
  /// ������� �������� ��������.
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public sealed class Singleton<T> where T : new()
  {
    Singleton() { }

    public static T Instance
    {
      get { return SingletonCreator.instance; }
    }

    class SingletonCreator
    {
      static SingletonCreator() { }

      internal static readonly T instance = new T();
    }
  }
}
