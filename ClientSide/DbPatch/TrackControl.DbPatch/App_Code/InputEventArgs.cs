using System;

namespace TrackControl.DbPatch
{
  /// <summary>
  /// ���������� ��� �������� ������� ������ � ���������� ��� �����
  /// �� ������ "�����".
  /// </summary>
  public class InputEventArgs : EventArgs
  {
    #region Fields
    /// <summary>
    /// ��� �������.
    /// </summary>
    private string _hostName;
    /// <summary>
    /// �������� ���� ������.
    /// </summary>
    private string _dbName;
    /// <summary>
    /// ��� �������������� ��.
    /// </summary>
    private string _login;
    /// <summary>
    /// ������ �������������� ��.
    /// </summary>
    private string _password;
    #endregion

    #region .ctor
    /// <summary>
    /// �����������
    /// </summary>
    /// <param name="hostName">��� �������</param>
    /// <param name="dbName">�������� ���� ������</param>
    /// <param name="login">��� �������������� ��</param>
    /// <param name="password">������ �������������� ��</param>
    public InputEventArgs(string hostName, string dbName, string login, string password)
    {
      _hostName = hostName;
      _dbName = dbName;
      _login = login;
      _password = password;
    }
    #endregion

    #region Properties
    /// <summary>
    /// ��� �������
    /// </summary>
    public string HostName
    {
      get { return _hostName; }
    }
    /// <summary>
    /// �������� ���� ������
    /// </summary>
    public string DbName
    {
      get { return _dbName; }
    }
    /// <summary>
    /// ��� �������������� ��
    /// </summary>
    public string Login
    {
      get { return _login; }
    }
    /// <summary>
    /// ������ �������������� ��
    /// </summary>
    public string Password
    {
      get { return _password; }
    }
    #endregion
  }
}
