using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using MySql.Data.MySqlClient;

namespace TrackControl.DbPatch
{
  public class PatchManager
  {
    #region Fields
    /// <summary>
    /// ������� ����� ����������
    /// </summary>
    private MainForm _mainForm;
    /// <summary>
    /// ������ �������� �������� ��� �����.
    /// </summary>
    private Dictionary<string, string> _scriptInfo;

    Controls.Progress _progress;
    #endregion

    private delegate void startPatchDelegate(string serverName, string dbName, string login, string password);

    #region .ctor
    public PatchManager(MainForm mainForm)
    {
      _scriptInfo = new Dictionary<string, string>();
      fetchSriptInfo();
      _progress = new Controls.Progress(_scriptInfo.Count);
      _mainForm = mainForm;

      if (GetScriptRegime() == "AGRO")
      {
        _mainForm.Text = "AGRO PATCH";
      }

      initView();
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// ��������� ������� ����������� � ��������.
    /// </summary>
    private void fetchSriptInfo()
    {
      string sStream = "";
      sStream = GetScriptRegime() == "AGRO" ?
        "TrackControl.DbPatch.ScriptsAgro.xml" :
        "TrackControl.DbPatch.Scripts.xml";
      using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(sStream))
      {
        XmlDocument doc = new XmlDocument();
        doc.Load(stream);
        XmlNodeList nodes = doc.SelectNodes("/patch/script");
        foreach (XmlNode node in nodes)
        {
          _scriptInfo.Add(node["name"].InnerText, node["description"].InnerText);
        }
      }
    }

    /// <summary>
    /// ��������� ���������� � ��������� �� ��� ���������������
    /// ��������������� �����������.
    /// </summary>
    /// <param name="serverName">��� ������� MySQL</param>
    /// <param name="dbName">�������� ���� ������</param>
    /// <param name="login">����� �������������� ��</param>
    /// <param name="password">������ �������������� ��</param>
    /// <returns></returns>
    private bool checkConnection(string serverName, string dbName, string login, string password)
    {
      string connectionString = String.Format(
        "server={0};database={1};user id={2};password={3}",
        serverName, dbName, login, password);

      bool result = true;
      MySqlConnection conn = new MySqlConnection(connectionString);
      try
      {
        conn.Open();
        conn.Close();
      }
      catch (Exception)
      {
        result = false;
      }
      return result;
    }

    /// <summary>
    /// ���������, ���������� �� � ������������ ���� ��� ������������� �����.
    /// </summary>
    /// <param name="serverName">��� ������� MySQL</param>
    /// <param name="dbName">�������� ���� ������</param>
    /// <param name="login">����� �������������� ��</param>
    /// <param name="password">������ �������������� ��</param>
    /// <returns></returns>
    private bool checkPrivileges(string serverName, string dbName, string login, string password)
    {
      string connectionString = String.Format(
        "server={0};database={1};user id={2};password={3}",
        serverName, dbName, login, password);
      string host = ("localhost" == serverName) ? serverName : "%";
      string script = String.Format(@"
SELECT 1 FROM `mysql`.`user` u WHERE u.`User`='{0}' AND u.`Host`='{1}'
  AND u.`Select_priv`='Y' AND u.`Insert_priv`='Y' AND u.`Update_priv`='Y'
  AND u.`Delete_priv`='Y' AND u.`Create_priv`='Y' AND u.`Drop_priv`='Y'
  AND u.`Grant_priv`='Y' AND u.`References_priv`='Y' AND u.`Index_priv`='Y'
  AND u.`Alter_priv`='Y' AND u.`Show_db_priv`='Y' AND u.`Super_priv`='Y'
  AND u.`Create_tmp_table_priv`='Y' AND u.`Lock_tables_priv`='Y' AND u.`Process_priv`='Y'
  AND u.`Execute_priv`='Y' AND u.`Create_view_priv`='Y' AND u.`Show_view_priv`='Y'
  AND u.`Create_routine_priv`='Y' AND u.`Alter_routine_priv`='Y' AND u.`File_priv`='Y';",
          login, host);

      bool result = false;
      MySqlConnection connection = new MySqlConnection(connectionString);
      try
      {
        MySqlCommand command = new MySqlCommand(script);
        command.Connection = connection;
        command.CommandType = CommandType.Text;

        connection.Open();
        result = Convert.ToBoolean(command.ExecuteScalar());
      }
      finally
      {
        connection.Close();
      }

      return result;
    }

    /// <summary>
    /// ������������� ������� ����� ���������� � ��������� ���������.
    /// ���������� �������� �����������.
    /// </summary>
    private void initView()
    {
      Controls.Welcome welcome = new Controls.Welcome();
      welcome.Next += new EventHandler<InputEventArgs>(welcome_Next);
      _mainForm.SetWelcomePage(welcome);

      Application.Run(_mainForm);
    }

    /// <summary>
    /// ����������� ������� ������ ���������� �����.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void welcome_Next(object sender, InputEventArgs e)
    {
      Controls.Welcome welcome = (Controls.Welcome)sender;

      //��������� ����������
      if (!checkConnection(e.HostName, e.DbName, e.Login, e.Password))
      {
        welcome.ShowNoConnectionWarning();
        return;
      }

      //��������� ����� ������������
      if (!checkPrivileges(e.HostName, e.DbName, e.Login, e.Password))
      {
        welcome.ShowNoPrivilegesWarning();
        return;
      }

      _mainForm.SetProgressPage(_progress);
      startPatchDelegate del = new startPatchDelegate(startPatch);
      del.BeginInvoke(e.HostName, e.DbName, e.Login, e.Password, finishPatch, null);
    }

    /// <summary>
    /// �������� ���������� ��������� ���� ������.
    /// </summary>
    /// <param name="serverName">��� ������� MySQL</param>
    /// <param name="dbName">�������� ���� ������</param>
    /// <param name="login">��� �������������� ��</param>
    /// <param name="password">������ �������������� ��</param>
    private void startPatch(string serverName, string dbName, string login, string password)
    {
      string connectionString = String.Format(
        "server={0};database={1};user id={2};password={3};allow user variables=yes",
        serverName, dbName, login, password);

      foreach (string key in _scriptInfo.Keys)
      {
        _progress.ShowScriptBegin(_scriptInfo[key]);
        string scriptName = "";
        scriptName = GetScriptRegime() == "AGRO" ?
          String.Format("TrackControl.DbPatch.ScriptsAgro.{0}", key) :
          String.Format("TrackControl.DbPatch.Scripts.{0}", key);
        execScript(scriptName, connectionString);

        _progress.ShowScriptEnd();
      }
    }

    /// <summary>
    /// ��������� ������ �� ����� Scripts.
    /// ��� ������� ��� ����� ������ ������ ���� Embedded Resource.
    /// </summary>
    /// <param name="scriptName">��� ����� �������</param>
    /// <param name="connectionString">������ ���������� � ��</param>
    private void execScript(string scriptName, string connectionString)
    {
      try
      {
        using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(scriptName))
        {
          using (StreamReader reader = new StreamReader(stream, Encoding.GetEncoding(1251)))
          {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
              MySqlCommand command = new MySqlCommand(reader.ReadToEnd());
              command.Connection = connection;
              command.CommandTimeout = Convert.ToInt32(Int32.MaxValue / 1000);
              command.CommandType = CommandType.Text;

              connection.Open();
              command.ExecuteNonQuery();
            }
          }
        }
      }
      catch (Exception ex)
      {
        string text = String.Format(
          "{0}\r\n��������� ������ ��� ���������� ������� {1}.\r\n������ ����������� � ��: {2}.\r\n{3}",
          Assembly.GetExecutingAssembly().FullName, scriptName, connectionString, ex);

        MessageBox.Show(text, "DB Patch", MessageBoxButtons.OK, MessageBoxIcon.Error);
        LogException(text);

        throw ex;
      }
    }

    /// <summary>
    /// ����������� ���������� � ���-�����.
    /// </summary>
    /// <param name="text">����� ����������.</param>
    private static void LogException(string text)
    {
      string path = Path.Combine(
        Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
        String.Format("Error_{0}.log", DateTime.Now.ToString("yyyyMMdd")));

      using (StreamWriter wr = new StreamWriter(path, true, Encoding.Default))
      {
        wr.WriteLine(DateTime.Now);
        wr.WriteLine(text);
        wr.WriteLine(String.Empty);
      }
    }

    /// <summary>
    /// ���������� �� �������� ��������� ���������� ����� ��.
    /// </summary>
    private void finishPatch(IAsyncResult ar)
    {
      Controls.Finish finish = new Controls.Finish();
      finish.Exit += new EventHandler(finish_Exit);
      _mainForm.SetFinishPage(finish);
    }

    /// <summary>
    /// ��������� ����������.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void finish_Exit(object sender, EventArgs e)
    {
      Application.Exit();
    }

    #endregion

    private string GetScriptRegime()
    {
      System.Resources.ResourceManager rm = new System.Resources.ResourceManager(
        "TrackControl.DbPatch.Res", Assembly.GetExecutingAssembly());

      return rm.GetString("REGIME") == "1" ? "AGRO" : "MAIN";
    }
  }
}
