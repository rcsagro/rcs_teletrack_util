namespace TrackControl.DbPatch
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.mainTable = new System.Windows.Forms.TableLayoutPanel();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.copyrightLbl = new System.Windows.Forms.Label();
      this.mainPanel = new System.Windows.Forms.Panel();
      this.mainTable.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // mainTable
      // 
      this.mainTable.ColumnCount = 1;
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.Controls.Add(this.tableLayoutPanel1, 0, 1);
      this.mainTable.Controls.Add(this.mainPanel, 0, 0);
      this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainTable.Location = new System.Drawing.Point(0, 0);
      this.mainTable.Margin = new System.Windows.Forms.Padding(0);
      this.mainTable.Name = "mainTable";
      this.mainTable.RowCount = 2;
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.mainTable.Size = new System.Drawing.Size(492, 366);
      this.mainTable.TabIndex = 0;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.BackgroundImage = global::TrackControl.DbPatch.Res.Status;
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Controls.Add(this.copyrightLbl, 1, 0);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 346);
      this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 1;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(492, 20);
      this.tableLayoutPanel1.TabIndex = 1;
      // 
      // copyrightLbl
      // 
      this.copyrightLbl.AutoSize = true;
      this.copyrightLbl.BackColor = System.Drawing.Color.Transparent;
      this.copyrightLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.copyrightLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.copyrightLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(38)))), ((int)(((byte)(72)))));
      this.copyrightLbl.Location = new System.Drawing.Point(261, 3);
      this.copyrightLbl.Margin = new System.Windows.Forms.Padding(15, 3, 15, 0);
      this.copyrightLbl.Name = "copyrightLbl";
      this.copyrightLbl.Size = new System.Drawing.Size(216, 17);
      this.copyrightLbl.TabIndex = 0;
      this.copyrightLbl.Text = "Copyright";
      this.copyrightLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // mainPanel
      // 
      this.mainPanel.BackgroundImage = global::TrackControl.DbPatch.Res.MainBg;
      this.mainPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainPanel.Location = new System.Drawing.Point(0, 0);
      this.mainPanel.Margin = new System.Windows.Forms.Padding(0);
      this.mainPanel.Name = "mainPanel";
      this.mainPanel.Size = new System.Drawing.Size(492, 346);
      this.mainPanel.TabIndex = 2;
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(492, 366);
      this.Controls.Add(this.mainTable);
      this.DoubleBuffered = true;
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(500, 400);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(500, 400);
      this.Name = "MainForm";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "TrackControl DB Patch";
      this.mainTable.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private System.Windows.Forms.Label copyrightLbl;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Panel mainPanel;
  }
}

