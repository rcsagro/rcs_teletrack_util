DROP PROCEDURE IF EXISTS CreateAgro;
CREATE PROCEDURE CreateAgro ()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Создание базы AGRO'
BEGIN
  IF NOT EXISTS (
    SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "agro_order")
  THEN

	CREATE TABLE IF NOT EXISTS agro_Unit(
	  `Id` INT (11) NOT NULL AUTO_INCREMENT,
	  `Name` CHAR (100) NOT NULL COMMENT 'Название',
	  `NameShort` CHAR (50) NOT NULL COMMENT 'Название краткое',
	  `Factor` DOUBLE DEFAULT 0 COMMENT 'Коэфициент пересчета к базовой',
	  `Comment` TEXT DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id)
	)
	ENGINE = INNODB DEFAULT CHARSET = cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Единицы измерения площади';

	INSERT
	INTO agro_Unit (`Name`, `NameShort`, `Factor`, `Comment`)
	VALUES ('Квадратный метр (м2, m2 ), square meter.', 'м2', 1000000, 'По определению, принятому во Франции в 1791, метр был равен 1*10-7 части четверти длины парижского меридиана и в 1799 году Ленуаром был изготовлен платиновый «архивный метр».
	C 1983 года метр равен расстоянию, которое проходит в вакууме свет за 1/299792458 доли секунды.');
	INSERT
	INTO agro_Unit (`Name`, `NameShort`, `Factor`, `Comment`)
	VALUES ('Квадратный фут (ft2, sq ft ), square foot.', 'фт2', 10763910.4, 'Равен 0,09290304 квадратных метра (точно).
	Имперские (английские, американские) единицы площади:
	1 кв. миля = 640 акров
	1 акр = 160 кв. род = 43560 кв. футов
	1 кв. род = 272,25 кв. футов
	1 кв. ярд = 9 кв. футов = 1296 кв. дюймов
	1 кв. фут = 144 кв. дюймов 
	');
	INSERT
	INTO agro_Unit (`Name`, `NameShort`, `Factor`, `Comment`)
	VALUES ('Акр, acre', 'акр', 247.1, 'Равен 4046,8564224 квадратных метра (точно).
	Земельная мера, применяемая в ряде стран, использующих английскую систему мер (Великобритания, США, Канада, Австралия и др.)
	');
	INSERT
	INTO agro_Unit (`Name`, `NameShort`, `Factor`, `Comment`)
	VALUES ('Гектар (га, ha), hectare.', 'га', 100, '1 гектар равен площади квадрата со стороной 100 м. Наименование «гектар» образовано добавлением приставки «гекто» к наименованию единицы площади «ар». Равен 10000 м2 
	');
	INSERT
	INTO agro_Unit (`Name`, `NameShort`, `Factor`, `Comment`)
	VALUES ('ар, are, сотка', 'ар', 10000, 'Происходит от латинского area - площадь, равен площади квадрата со стороной в 10 м то есть 100 м2 (точно).
	Сотка - тот же ар, применяется обычно к дачным участкам и международного обозначения не имеет.
	');
	INSERT
	INTO agro_Unit (`Name`, `NameShort`, `Factor`, `Comment`)
	VALUES ('Квадратный километр (км2, km2 ), square kilometer.', 'км2', 1, 'Равен 1000000 квадратных метров 
	');
	INSERT
	INTO agro_Unit (`Name`, `NameShort`, `Factor`, `Comment`)
	VALUES ('Квадратная миля (mi2, sq mi), square mile.', 'мл2', 0.4, 'Равна 2,589988110336 квадратных километров (точно)
	Имеется в виду имперская (английская, американская) миля равная 1609,344 метра
	');
	CREATE TABLE IF NOT EXISTS agro_Agregat(
	  `Id` INT (11) NOT NULL AUTO_INCREMENT,
	  `Name` CHAR (50) NOT NULL COMMENT 'Название',
	  `Width` DOUBLE DEFAULT 0 COMMENT 'Ширина навесного оборудования для подсчета обработанной площади ',
	  Identifier SMALLINT (5) UNSIGNED DEFAULT NULL COMMENT '10 битный идентификатор агрегата',
	  Def TINYINT (1) DEFAULT 0 COMMENT 'Агрегат по умолчанию для нарядов',
	  `Comment` TEXT DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id)
	) ENGINE = INNODB DEFAULT CHARSET = cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Навесное оборудование';

	INSERT
	INTO agro_Agregat (`Name`, `Width`,`Def`)
	VALUES ('Неизвестное оборудование', 10,1);

	CREATE TABLE IF NOT EXISTS agro_Work(
	  `Id` INT (11) NOT NULL AUTO_INCREMENT,
	  Name CHAR (50) NOT NULL COMMENT 'Название',
	  SpeedBottom DOUBLE DEFAULT 0 COMMENT 'Нижний предел скорости',
	  SpeedTop DOUBLE DEFAULT 0 COMMENT 'Верхний предел скорости',
	  `Comment` TEXT DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id)
	) ENGINE = INNODB DEFAULT CHARSET = cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Виды работ';

	INSERT
	INTO agro_Work (`Name`, `Comment`)
	VALUES ('Внесення добрив', '');

	INSERT
	INTO agro_Work (`Name`, `Comment`)
	VALUES ('Подрібнення пожнивних решток', '');

	INSERT
	INTO agro_Work (`Name`, `Comment`)
	VALUES ('Боронування', '');

	INSERT
	INTO agro_Work (`Name`, `Comment`)
	VALUES ('Соняшник - обмолот ', '');
	INSERT
	INTO agro_Work (`Name`, `Comment`)
	VALUES ('Соя - обмолот', '');
	INSERT
	INTO agro_Work (`Name`, `Comment`)
	VALUES ('Цукровий буряк - збирання гички', '');
	INSERT
	INTO agro_Work (`Name`, `Comment`)
	VALUES ('Цукровий буряк - копання', '');
	INSERT
	INTO agro_Work (`Name`, `Comment`)
	VALUES ('Цукровий буряк - навантаження', '');
	INSERT
	INTO agro_Work (`Name`, `Comment`)
	VALUES ('Силос - трамбування', '');
	INSERT
	INTO agro_Work (`Name`, `Comment`)
	VALUES ('Силос - підгортання бульдозером', '');
	INSERT
	INTO agro_Work (`Name`, `Comment`)
	VALUES ('Силос - перевезення', '');

	CREATE TABLE IF NOT EXISTS agro_Price(
	  `Id` INT (11) NOT NULL AUTO_INCREMENT,
	  `DateInit` DATE COMMENT 'Дата приказа ',
	  `DateComand` DATE COMMENT 'Дата ,с котрой действуют цены ',
	  `Name` CHAR (50) NOT NULL COMMENT 'Название',
	  `Comment` TEXT DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id)
	) ENGINE = INNODB DEFAULT CHARSET = cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Приказы о введении цен';
	INSERT
	INTO agro_Price (`DateComand`, `DateInit`, `Name`, `Comment`)
	VALUES ('09.07.01', '09.09.2001', 'Додаток 3 до Наказу № 15 від 05.07.09 р.', 'Типові норми та розцінки на польові роботи в ТОВ ІПК "Полтавазернопродукт", з 01.09.2009 року');


	CREATE TABLE IF NOT EXISTS agro_PriceT(
	  `Id` INT (11) NOT NULL AUTO_INCREMENT,
	  `Id_main` INT (11) NOT NULL COMMENT 'Ссылка на приказ',
	  `Id_work` INT (11) NOT NULL COMMENT 'Ссылка на работу',
	  `Price` DOUBLE (10, 2) DEFAULT 0 COMMENT 'Расценка',
	  `Id_unit` INT (11) NOT NULL COMMENT 'Ссылка на единицу измерения',
	  `Id_mobitel` INT (11) NOT NULL COMMENT 'Транспортное средство',
	  `Id_agregat` INT (11) DEFAULT 0 COMMENT 'Навесное оборудование',
	  `Comment` TEXT DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id),
	  INDEX Id_main_FK1 USING BTREE (Id_main),
	  CONSTRAINT Id_main_FK1 FOREIGN KEY (Id_main) REFERENCES agro_Price (ID),
	  INDEX Id_work_FK2 USING BTREE (Id_work),
	  CONSTRAINT Id_work_FK2 FOREIGN KEY (Id_work) REFERENCES agro_Work (ID),
	  INDEX Id_mobitel_FK3 USING BTREE (Id_mobitel),
	  CONSTRAINT Id_mobitel_FK3 FOREIGN KEY (Id_mobitel) REFERENCES mobitels (Mobitel_ID),
	  INDEX Id_unit_FK4 USING BTREE (Id_unit),
	  CONSTRAINT Id_unit_FK4 FOREIGN KEY (Id_unit) REFERENCES agro_unit (ID),
	  INDEX Id_agregat_Price_FK5 USING BTREE (Id_agregat),
	  CONSTRAINT Id_agregat_Price_FK5 FOREIGN KEY (Id_agregat) REFERENCES agro_agregat (ID)
	) ENGINE = INNODB DEFAULT CHARSET = cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Расценки';



	CREATE TABLE IF NOT EXISTS agro_Culture(
	  `Id` INT (11) NOT NULL AUTO_INCREMENT,
	  `Name` CHAR (50) NOT NULL COMMENT 'Название',
	  `Icon` BLOB NULL COMMENT 'Иконка культуры',
	  `Comment` TEXT DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id)
	)
	ENGINE = INNODB DEFAULT CHARSET = cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Культура, выращиваемая на поле';

	INSERT
	INTO agro_Culture (`Name`, `Comment`)
	VALUES ('Сахарный буряк', 'Урожайность Х центнеров с гектара');
	INSERT
	INTO agro_Culture (`Name`, `Comment`)
	VALUES ('Кукуруза', 'Урожайность Х  центнеров с гектара');
	INSERT
	INTO agro_Culture (`Name`, `Comment`)
	VALUES ('Соя', 'Урожайность Х  центнеров с гектара');
	INSERT
	INTO agro_Culture (`Name`, `Comment`)
	VALUES ('Пшеница озимая ', 'Урожайность Х  центнеров с гектара');
	INSERT
	INTO agro_Culture (`Name`, `Comment`)
	VALUES ('Ячмень', 'Урожайность Х  центнеров с гектара');
	INSERT
	INTO agro_Culture (`Name`, `Comment`)
	VALUES ('Травы многолетние ', 'Урожайност Х  центнеров с гектара');
	INSERT
	INTO agro_Culture (`Name`, `Comment`)
	VALUES ('Травы однолетние', 'Урожайность Х  центнеров с гектара');

	INSERT
	INTO agro_Culture (`Name`, `Comment`)
	VALUES ('Подсолнечник', 'Урожайность Х  центнеров с гектара');
	INSERT
	INTO agro_Culture (`Name`, `Comment`)
	VALUES ('Овес', 'Урожайность Х  центнеров с гектара');

	INSERT
	INTO agro_Culture (`Name`, `Comment`)
	VALUES ('Гречка', 'Урожайность центнеров с гектара');

	INSERT
	INTO agro_Culture (`Name`, `Comment`)
	VALUES ('Пшеница яровая', 'Урожайность центнеров с гектара');

	CREATE TABLE IF NOT EXISTS agro_FieldGroupe(
	  `Id` INT (11) NOT NULL AUTO_INCREMENT,
	  `Name` CHAR (50) NOT NULL COMMENT 'Название',
	  `Comment` TEXT DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id)
	)
	ENGINE = INNODB DEFAULT CHARSET = cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Группа полей';

	INSERT
	INTO agro_FieldGroupe (`Name`)
	VALUES ('Семенівська');
	INSERT
	INTO agro_FieldGroupe (`Name`)
	VALUES ('Троїцька');
	INSERT
	INTO agro_FieldGroupe (`Name`)
	VALUES ('Мусіївська');
	INSERT
	INTO agro_FieldGroupe (`Name`)
	VALUES ('Гриньки');

	CREATE TABLE IF NOT EXISTS agro_field(
	  Id INT (11) NOT NULL AUTO_INCREMENT,
	  Id_main INT (11) DEFAULT NULL COMMENT 'Ссылка на группу полей',
	  Id_zone INT (11) NOT NULL COMMENT 'Ссылка на контрольную зону',
	  Name CHAR (50) NOT NULL COMMENT 'Название',
	  `Comment` TEXT DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id),
	  INDEX Id_main_Field_FK1 USING BTREE (Id_main)
	)
	ENGINE = INNODB
	AUTO_INCREMENT = 14
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Поле';



	CREATE TABLE IF NOT EXISTS agro_FieldCulture(
	  `Id` INT (11) NOT NULL AUTO_INCREMENT,
	  `Id_main` INT (11) COMMENT 'Ссылка на поле',
	  `Id_culture` INT (11) NOT NULL COMMENT 'Ссылка на культуру',
	  `Year` SMALLINT NOT NULL COMMENT 'Год обработки',
	  PRIMARY KEY (Id),
	  INDEX Id_main_FieldCulture_FK1 USING BTREE (Id_main),
	  CONSTRAINT Id_main_FieldCulture_FK1 FOREIGN KEY (Id_main) REFERENCES agro_Field (ID),
	  INDEX Id_culture_FK2 USING BTREE (Id_culture),
	  CONSTRAINT Id_culture_FK2 FOREIGN KEY (Id_culture) REFERENCES agro_Culture (ID)
	)
	ENGINE = INNODB DEFAULT CHARSET = cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Культура на поле в промежуток времени';

	CREATE TABLE IF NOT EXISTS agro_Order(
	  Id INT (11) NOT NULL AUTO_INCREMENT,
	  `Date` DATE DEFAULT NULL COMMENT 'Дата наряда на работы',
	  Id_mobitel INT (11) NOT NULL DEFAULT 0 COMMENT 'Транспортное средство',
	  LocationStart VARCHAR (255) DEFAULT NULL COMMENT 'Место начала движения',
	  LocationEnd VARCHAR (255) DEFAULT NULL COMMENT 'Место окончания движения',
	  TimeStart DATETIME DEFAULT NULL COMMENT 'Время начала движения',
	  TimeEnd DATETIME DEFAULT NULL COMMENT 'Время окончания движения',
	  Regime TINYINT (4) DEFAULT 0 COMMENT 'Режим наряд -ручной,автоматический, закрыт',
	  TimeWork CHAR (10) DEFAULT NULL COMMENT 'Продолжительность смены, ч',
	  TimeMove CHAR (10) DEFAULT NULL COMMENT 'Общее время движения, ч',
	  Distance DOUBLE DEFAULT 0 COMMENT 'Пройденный путь, км',
	  SpeedAvg DOUBLE DEFAULT NULL COMMENT 'Средняя скорость, км/ч',
	  `Comment` TEXT DEFAULT NULL COMMENT 'Комментарий',
	  PRIMARY KEY (Id),
	  INDEX Id_mobitel_Order_FK1 USING BTREE (Id_mobitel),
	  CONSTRAINT Id_mobitel_Order_FK1 FOREIGN KEY (Id_mobitel)
	  REFERENCES mobitels (Mobitel_ID)
	)
	ENGINE = INNODB
	AUTO_INCREMENT = 1
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Заголовок наряда';

	CREATE TABLE IF NOT EXISTS agro_ordert(
	  Id INT (11) NOT NULL AUTO_INCREMENT,
	  Id_main INT (11)  NOT NULL DEFAULT 0 COMMENT 'Ссылка на наряд',
	  Id_field INT (11) NOT NULL DEFAULT 0 COMMENT 'Ссылка на поле',
	  Id_zone INT (11) NOT NULL DEFAULT 0 COMMENT 'Ссылка на зону',
	  Id_driver INT (11) NOT NULL DEFAULT 0 COMMENT 'Водитель',
	  TimeStart DATETIME DEFAULT NULL COMMENT 'Факт время начала работы в зоне',
	  TimeEnd DATETIME DEFAULT NULL COMMENT 'Факт время окончания работ в зоне',
	  TimeMove TIME DEFAULT NULL COMMENT 'Время движения, ч',
	  TimeStop TIME DEFAULT NULL COMMENT 'Время стоянок, ч',
	  TimeRate TIME DEFAULT NULL COMMENT 'Моточасы, ч',
	  Id_work INT (11) NOT NULL DEFAULT 0 COMMENT 'Ссылка на работу',
	  FactTime TIME DEFAULT NULL COMMENT 'Факт время работ',
	  Distance DOUBLE NOT NULL DEFAULT 0 COMMENT 'Пройденный путь, км',
	  FactSquare DOUBLE (10, 5) NOT NULL DEFAULT 0.00000 COMMENT 'Факт обработанная площадь',
	  FactSquareCalc DOUBLE (10, 5) NOT NULL DEFAULT 0.00000 COMMENT 'Факт обработанная площадь полученная расчетом',
	  Price DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Расценка',
	  Sum DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Сумма за площадь',
	  SpeedAvg DOUBLE NOT NULL DEFAULT 0 COMMENT 'Средняя скорость выполнения работ',
	  `Comment` TEXT DEFAULT NULL COMMENT 'Комментарий',
	  FuelStart DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Топливо в начале',
	  FuelAdd DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Заправлено',
	  FuelSub DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Слито',
	  FuelEnd DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Топливо в конце',
	  FuelExpens DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Общий расход топлива',
	  FuelExpensAvg DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Общий расход топлива на гектар',
	  FuelExpensAvgRate DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Средний расход, л/моточас',
	  Fuel_ExpensAvgRate DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДРТ/CAN Средний расход, л/моточас',
	  Fuel_ExpensMove DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДРТ/CAN Расход топлива в движении, л',
	  Fuel_ExpensStop DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДРТ/CAN Расход топлива на стоянках, л',
	  Fuel_ExpensAvg DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДРТ/CAN Средний расход, л/га',
	  Fuel_ExpensTotal DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДРТ/CAN Общий расход, л',
	  Id_agregat INT (11) NOT NULL DEFAULT 0 COMMENT 'Навесное оборудование',
	  PRIMARY KEY (Id),
	  INDEX Id_main_Order_FK1 USING BTREE (Id_main),
	  CONSTRAINT Id_main_Order_FK1 FOREIGN KEY (Id_main)
	  REFERENCES  agro_order (Id) ON DELETE CASCADE ON UPDATE CASCADE
	)
	ENGINE = INNODB
	AUTO_INCREMENT = 1
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Содержимое наряда';
	END IF;

	CREATE TABLE IF NOT EXISTS  agro_ordert_control(
	  Id INT (11) NOT NULL AUTO_INCREMENT,
	  Id_main INT (11) NOT NULL DEFAULT 0 COMMENT 'Ссылка на наряд',
	  Id_object INT (11) NOT NULL DEFAULT 0 COMMENT 'Код объекта управления',
	  Type_object TINYINT (4) NOT NULL DEFAULT 0 COMMENT 'Тип объекта управления',
	  TimeStart DATETIME NOT NULL COMMENT 'Факт время начала работы',
	  TimeEnd DATETIME NOT NULL COMMENT 'Факт время окончания работ',
	  PRIMARY KEY (Id),
	  INDEX FK_agro_ordert_control_agro_order_Id (Id_main),
	  CONSTRAINT FK_agro_ordert_control_agro_order_Id FOREIGN KEY (Id_main)
	  REFERENCES agro_order (Id) ON DELETE CASCADE ON UPDATE CASCADE
	)
	ENGINE = INNODB
	AUTO_INCREMENT = 223
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Управляющие данные для наряда';

	CREATE TABLE IF NOT EXISTS  agro_params(
	  Id INT (11) NOT NULL AUTO_INCREMENT,
	  Number INT (11) NOT NULL COMMENT 'Номер параметра',
	  Value VARCHAR (100) DEFAULT NULL COMMENT 'Значение',
	  Description VARCHAR (50) DEFAULT NULL COMMENT 'Описание',
	  PRIMARY KEY (Id)
	)
	ENGINE = INNODB
	AUTO_INCREMENT = 1
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Таблица параметров';
	
		
	
	CREATE TABLE IF NOT EXISTS agro_datagps(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Id_main INT(11) NOT NULL,
  Lon_base DOUBLE NOT NULL,
  Lat_base DOUBLE NOT NULL,
  Lon_dev VARCHAR(255) NOT NULL,
  Lat_dev VARCHAR(255) NOT NULL,
  Speed_base DOUBLE NOT NULL,
  Speed_dev VARCHAR(255) NOT NULL,
  PRIMARY KEY (Id),
  INDEX FK_agro_datagps_agro_ordert_Id (Id_main),
  CONSTRAINT FK_agro_datagps_agro_ordert_Id FOREIGN KEY (Id_main)
  REFERENCES agro_ordert (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 0
CHARACTER SET cp1251
COLLATE cp1251_general_ci;
	
CREATE TABLE IF NOT EXISTS   agro_agregat_vehicle(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Id_agregat INT(11) NOT NULL DEFAULT 0,
  Id_vehicle INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (Id),
  INDEX FK_agro_agregat_vehicle_agro_agregat_Id (Id_agregat),
  INDEX FK_agro_agregat_vehicle_vehicle_id (Id_vehicle),
  CONSTRAINT FK_agro_agregat_vehicle_agro_agregat_Id FOREIGN KEY (Id_agregat)
  REFERENCES  agro_agregat (Id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_agro_agregat_vehicle_vehicle_id FOREIGN KEY (Id_vehicle)
  REFERENCES  vehicle (id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = 'Агрегаты по умолчанию для транспортных средств';		
	
	
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_ordert" AND COLUMN_NAME = "Confirm")
    THEN
      ALTER TABLE `agro_ordert` ADD COLUMN `Confirm` BOOL NOT NULL DEFAULT 1 COMMENT 'Подтверждение обработки площади';
    END IF;

    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "Id_main")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `Id_main` INT (11) NOT NULL DEFAULT 0 COMMENT 'Ссылка на элемент верхнего уровня в иерархии';
    END IF;

    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "IsGroupe")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `IsGroupe` TINYINT (1) NOT NULL DEFAULT 0 COMMENT 'Запись - группа';
    END IF;      
     
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "InvNumber")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `InvNumber` CHAR (50) DEFAULT NULL COMMENT 'Инвентарный номер агрегата';
    END IF;      
    
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "Id_work")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `Id_work` INT (11) NOT NULL DEFAULT 0 COMMENT 'Вид работ по умолчанию';
    END IF; 

    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "Identifier")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `Identifier` SMALLINT (5) UNSIGNED DEFAULT NULL COMMENT '10 битный идентификатор агрегата';
    END IF;
    
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_agregat" AND COLUMN_NAME = "Def")
    THEN
      ALTER TABLE `agro_agregat` ADD COLUMN `Def` TINYINT (1) DEFAULT 0 COMMENT 'Агрегат по умолчанию для нарядов';
    END IF;
    
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "agro_order" AND COLUMN_NAME = "PathWithoutWork")
    THEN
      ALTER TABLE `agro_order` ADD COLUMN `PathWithoutWork` DOUBLE DEFAULT 0 COMMENT 'Переезды, км';
      ALTER TABLE `agro_order` ADD COLUMN `SquareWorkDescript` TEXT DEFAULT NULL COMMENT 'Обработанная площадь';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDUTExpensSquare` DOUBLE DEFAULT 0 COMMENT 'Расход в полях,  всего';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDRTExpensSquare` DOUBLE DEFAULT 0 COMMENT 'Расход в полях,  всего';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDUTExpensAvgSquare` DOUBLE DEFAULT 0 COMMENT 'Расход в полях, л/га:';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDRTExpensAvgSquare` DOUBLE DEFAULT 0 COMMENT 'Расход в полях, л/га:';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDUTExpensWithoutWork` DOUBLE DEFAULT 0 COMMENT 'Расход на переездах, всего';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDRTExpensWithoutWork` DOUBLE DEFAULT 0 COMMENT 'Расход на переездах, всего';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDUTExpensAvgWithoutWork` DOUBLE DEFAULT 0 COMMENT 'Переезды, км';
      ALTER TABLE `agro_order` ADD COLUMN `FuelDRTExpensAvgWithoutWork` DOUBLE DEFAULT 0 COMMENT 'Переезды, км';
    END IF;    
    
        IF NOT EXISTS (
		SELECT *
		FROM
		  INFORMATION_SCHEMA.COLUMNS
		WHERE
		  TABLE_SCHEMA = (SELECT DATABASE())
		  AND TABLE_NAME = 'agro_order'
		  AND Column_Name = "Date" 
		  AND Data_type ="DATETIME")
    THEN
      ALTER TABLE `agro_order` MODIFY `Date` DATETIME COMMENT 'Дата наряда на работы(new type - DATE->DATETIME)';
    END IF;
END;
CALL CreateAgro();
DROP PROCEDURE IF EXISTS CreateAgro;
	/*CREATE TABLE IF NOT EXISTS agro_calc(
	  Id INT (11) NOT NULL AUTO_INCREMENT,
	  Id_orderT INT (11) NOT NULL DEFAULT 0 COMMENT 'ссылка на запись наряда',
	  StepLon DOUBLE NOT NULL DEFAULT 0.0001 COMMENT 'шаг квадрата по оси Х',
	  StepLat DOUBLE NOT NULL DEFAULT 0.0001 COMMENT 'шаг квадрата по оси У',
	  Time_First DATETIME DEFAULT NULL COMMENT 'Время начала трека в площади',
	  Time_Last DATETIME DEFAULT NULL COMMENT 'Время окончания трека в площади',
	  PRIMARY KEY (Id),
	  INDEX agro_calc_FK1 USING BTREE (Id_orderT),
	  CONSTRAINT agro_calc_FK1 FOREIGN KEY (Id_orderT)
	  REFERENCES agro_ordert (Id) ON DELETE CASCADE ON UPDATE CASCADE
	)
	ENGINE = INNODB
	AUTO_INCREMENT = 1
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Расчет обработанной площади  - шапка';
	CREATE TABLE agro_calct(
	  Id INT (11) NOT NULL AUTO_INCREMENT,
	  Id_main INT (11) NOT NULL DEFAULT 0,
	  Lon DOUBLE NOT NULL DEFAULT 0,
	  Lat DOUBLE NOT NULL DEFAULT 0,
	  Selected TINYINT (1) NOT NULL DEFAULT 0 COMMENT 'квадрат обработан',
	  Border TINYINT (1) NOT NULL DEFAULT 0 COMMENT 'квадрат граничный',
	  BorderNode TINYINT (1) NOT NULL DEFAULT 0 COMMENT 'квадрат  - узловая точка',
	  Number INT (11) NOT NULL DEFAULT 0 COMMENT 'посл-ть узловых точек для прорисовки контура',
	  PRIMARY KEY (Id),
	  INDEX agro_calct_FK1 USING BTREE (Id_main),
	  CONSTRAINT agro_calct_FK1 FOREIGN KEY (Id_main)
	  REFERENCES  agro_calc (Id) ON DELETE CASCADE ON UPDATE CASCADE
	)
	ENGINE = INNODB
	AUTO_INCREMENT = 1
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Расчет обработанной площади  - содержание';
*/