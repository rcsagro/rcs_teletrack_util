using System;
using System.Reflection;
using System.Windows.Forms;
using TrackControl.DbPatch.Controls;

namespace TrackControl.DbPatch
{
  /// <summary>
  /// ������� ����� ����������.
  /// </summary>
  public partial class MainForm : Form
  {
    private delegate void setFinishPageDelegate(Finish finish);

    #region .ctor
    /// <summary>
    /// �����������.
    /// </summary>
    public MainForm()
    {
      InitializeComponent();
      initComponents();
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// ���������� �������� �����������.
    /// </summary>
    /// <param name="welcome">�������� �����������</param>
    public void SetWelcomePage(Welcome welcome)
    {
      welcome.Dock = DockStyle.Fill;
      this.mainPanel.Controls.Add(welcome);
    }

    /// <summary>
    /// ���������� �������� � ����������� � ���� ���������� �����.
    /// </summary>
    /// <param name="progress">��������, �� ������� ������������ ��������� ���������� �����</param>
    public void SetProgressPage(Progress progress)
    {
      progress.Dock = DockStyle.Fill;

      this.mainPanel.Controls.Clear();
      this.mainPanel.Controls.Add(progress);
      this.ControlBox = false;
    }

    /// <summary>
    /// ���������� �������� � ������������ �� �������� ���������� �����.
    /// </summary>
    /// <param name="finish">�������� � ������������ �� �������� ���������� �����.</param>
    public void SetFinishPage(Finish finish)
    {
      if (this.InvokeRequired)
      {
        this.Invoke(new setFinishPageDelegate(SetFinishPage), finish);
      }
      else
      {
        finish.Dock = DockStyle.Fill;
        this.mainPanel.Controls.Clear();
        this.mainPanel.Controls.Add(finish);
        finish.Focus();
      }
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// �������������� ���������� � ������ ������.
    /// </summary>
    private void initComponents()
    {
      initStatus();
      this.Text += String.Format(" -- {0}", Application.ProductVersion);
    }

    /// <summary>
    /// ������������� ��������.
    /// </summary>
    private void initStatus()
    {
      object[] attrs = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
      if (attrs.Length > 0)
      {
        AssemblyCopyrightAttribute copyRight = attrs[0] as AssemblyCopyrightAttribute;
        if (copyRight != null)
        {
          this.copyrightLbl.Text = copyRight.Copyright;
        }
      }
    }
    #endregion
  }
}