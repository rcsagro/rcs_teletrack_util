-- �������� ����� �� ��� ���������� ������ TrackControl.
-- ������������ ��� ��� ���, � �������� TrackControl ��� �� �������.

CREATE TABLE IF NOT EXISTS ver(
  id INT (11) NOT NULL AUTO_INCREMENT,
  Num INT (11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '����� ������� ������ ��',
  `Desc` TEXT CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL COMMENT '�������� ������� ������ ��',
  `time` DATETIME DEFAULT NULL COMMENT '����� ����������',
  PRIMARY KEY (id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

--
-- ������� Driver
--
CREATE TABLE IF NOT EXISTS driver(
  `id` INT (11) NOT NULL AUTO_INCREMENT,
  `Family` CHAR (40) DEFAULT NULL COMMENT '�������',
  `Name` CHAR (40) DEFAULT NULL COMMENT '���',
  `ByrdDay` DATE DEFAULT NULL COMMENT '���� ��������',
  `Category` CHAR (5) DEFAULT NULL COMMENT '���������',
  `Permis` CHAR (20) DEFAULT NULL COMMENT '�����',
  `foto` BLOB DEFAULT NULL COMMENT '����',
  PRIMARY KEY (id),
  UNIQUE INDEX id USING BTREE (id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

DROP PROCEDURE IF EXISTS rcs__popup_driver;
CREATE PROCEDURE rcs__popup_driver()
BEGIN
  IF NOT EXISTS
  (
    SELECT NULL FROM driver WHERE id = 1
  )
  THEN
    INSERT INTO driver 
      (`id`,`Family`,`Name`,`ByrdDay`,`Category`,`Permis`,`foto`)
    VALUES
      (1, '', '', '2009-01-01', '�', '1111', NULL);
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM driver WHERE id = 2
  )
  THEN
    INSERT INTO driver
      (`id`,`Family`,`Name`,`ByrdDay`,`Category`,`Permis`,`foto`)
    VALUES
      (2, '��������', '', '2009-01-22', '�', '1111', NULL);
  END IF;
END;
CALL rcs__popup_driver();
DROP PROCEDURE IF EXISTS rcs__popup_driver;

--
-- ������� Setting
--
CREATE TABLE IF NOT EXISTS setting(
  `id` INT (11) NOT NULL AUTO_INCREMENT,
  `TimeBreak` TIME DEFAULT '00:01:00' COMMENT '����������� ����� ���������',
  `RotationMain` INT (11) DEFAULT 100 COMMENT '������� ��������� ���������',
  `RotationAdd` INT (11) DEFAULT 100 COMMENT '������� ���. ���������',
  `FuelingEdge` INT (11) DEFAULT 0 COMMENT '����������� ��������',
  `band` INT (11) DEFAULT 25 COMMENT '������ ����������',
  `FuelingDischarge` INT (11) DEFAULT 0 COMMENT '����������� ���� �������',
  `accelMax` DOUBLE DEFAULT 1 COMMENT '������������ ���������',
  `breakMax` DOUBLE DEFAULT 1 COMMENT '������������ ����������',
  `speedMax` DOUBLE DEFAULT 70 COMMENT '������������ ��������',
  `idleRunningMain` INT (11) DEFAULT 0 COMMENT '�������� ��� ��������� ���������',
  `idleRunningAdd` INT (11) DEFAULT 0 COMMENT '�������� ��� ���. ���������',
  `Name` CHAR (20) DEFAULT '���������' COMMENT '��������',
  `Desc` CHAR (80) DEFAULT '�������� ���������' COMMENT '�������� ',
  `AvgFuelRatePerHour` DOUBLE (10, 5) NOT NULL DEFAULT 30.00000 COMMENT '������� ������ ������� � ���',
  `FuelApproximationTime` TIME NOT NULL DEFAULT '00:02:00' COMMENT '��������� �������� ����� ��������, �� ����� �������� ���������� ������� ������� � ������ �������',
  `TimeGetFuelAfterStop` TIME NOT NULL DEFAULT '00:00:00' COMMENT '��������� �������� ����� ������� ������� � ������� ������ �������',
  `TimeGetFuelBeforeMotion` TIME NOT NULL DEFAULT '00:00:00' COMMENT '��������� �������� ����� ������� ������ ������� � ������� ��������',
  `CzMinCrossingPairTime` TIME NOT NULL DEFAULT '00:01:00' COMMENT 'Min ��������� �������� ����� ����� ������������� ��',
  PRIMARY KEY (id),
  UNIQUE INDEX id USING BTREE (id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

DROP PROCEDURE IF EXISTS rcs__popup_setting;
CREATE PROCEDURE rcs__popup_setting()
BEGIN
  IF NOT EXISTS
  (
    SELECT NULL FROM setting WHERE id = 1
  )
  THEN
    INSERT INTO setting
      (
        `id`,`TimeBreak`,`RotationMain`,`RotationAdd`,`FuelingEdge`,`band`,`FuelingDischarge`,`accelMax`,
        `breakMax`,`speedMax`,`idleRunningMain`,`idleRunningAdd`,`Name`,`Desc`
      )
     VALUES
     (
      1, '00:01:00', 100, 100, 20, 5, 10, 1, 1, 70, 0, 0, '��������� 1', '��������� �� ���������'
     );
  END IF;
END;
CALL rcs__popup_setting();
DROP PROCEDURE IF EXISTS rcs__popup_setting;


CREATE TABLE IF NOT EXISTS team(
  id INT (11) NOT NULL AUTO_INCREMENT,
  Name CHAR (20) DEFAULT NULL COMMENT '�������� ������',
  Descr TEXT DEFAULT NULL COMMENT '��������',
  Setting_id INT (11) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX id USING BTREE (id),
  INDEX Setting_id USING BTREE (Setting_id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

DROP PROCEDURE IF EXISTS rcs__popup_team;
CREATE PROCEDURE rcs__popup_team()
BEGIN
  IF NOT EXISTS
  (
    SELECT NULL FROM team WHERE id = 1
  )
  THEN
    INSERT INTO team
      (
        `id`,`Name`,`Descr`,`Setting_id`
      )
    VALUES
      (
        1, '����������', NULL, 1
      );
  END IF;
END;
CALL rcs__popup_team();
DROP PROCEDURE IF EXISTS rcs__popup_team;

CREATE TABLE IF NOT EXISTS vehicle(
  id INT (11) NOT NULL AUTO_INCREMENT,
  MakeCar TEXT DEFAULT NULL COMMENT '����� ����������',
  NumberPlate TEXT DEFAULT NULL COMMENT '�������� ����',
  Team_id INT (11) DEFAULT NULL,
  Mobitel_id INT (11) DEFAULT NULL,
  Odometr INT (11) DEFAULT 0 COMMENT '��������� ��������',
  CarModel TEXT DEFAULT NULL COMMENT '������',
  setting_id INT (11) DEFAULT 1,
  odoTime DATETIME DEFAULT NULL COMMENT '���� � ����� ������ ��������',
  driver_id INT (11) DEFAULT -1,
  PRIMARY KEY (id),
  UNIQUE INDEX id USING BTREE (id),
  INDEX Mobitel_id USING BTREE (Mobitel_id),
  INDEX Team_id USING BTREE (Team_id),
  INDEX setting_id USING BTREE (setting_id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

CREATE TABLE IF NOT EXISTS sensors(
  id INT (11) NOT NULL AUTO_INCREMENT,
  mobitel_id INT (11) NOT NULL,
  Name VARCHAR (45) NOT NULL,
  Description VARCHAR (200) NOT NULL,
  StartBit INT (10) UNSIGNED NOT NULL,
  Length INT (10) UNSIGNED NOT NULL,
  NameUnit VARCHAR (45) NOT NULL,
  K DOUBLE (10, 5) NOT NULL DEFAULT 0.00000,
  PRIMARY KEY (id),
  INDEX Index_2 USING BTREE (mobitel_id)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

CREATE TABLE IF NOT EXISTS relationalgorithms(
  ID INT (11) NOT NULL AUTO_INCREMENT,
  AlgorithmID INT (11) NOT NULL DEFAULT 0,
  SensorID INT (11) NOT NULL DEFAULT 0,
  PRIMARY KEY (ID),
  UNIQUE INDEX ID USING BTREE (ID)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

CREATE TABLE IF NOT EXISTS sensorcoefficient(
  id INT (10) UNSIGNED NOT NULL AUTO_INCREMENT,
  Sensor_id INT (10) UNSIGNED NOT NULL,
  UserValue DOUBLE (10, 5) NOT NULL,
  SensorValue DOUBLE (10, 5) NOT NULL,
  K DOUBLE (10, 5) NOT NULL DEFAULT 1.00000,
  b DOUBLE (10, 5) NOT NULL DEFAULT 0.00000,
  PRIMARY KEY (id),
  UNIQUE INDEX Index_2 USING BTREE (Sensor_id, UserValue)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

-- ������� SensorAlgorithms
-- �������� � ����������

CREATE TABLE IF NOT EXISTS sensoralgorithms(
  `ID` INT (11) NOT NULL AUTO_INCREMENT,
  `Name` CHAR (45) NOT NULL,
  `Description` CHAR (45) DEFAULT NULL,
  `AlgorithmID` INT (11) NOT NULL DEFAULT 0,
  PRIMARY KEY (ID),
  UNIQUE INDEX ID USING BTREE (ID)
) ENGINE = INNODB DEFAULT CHARSET=cp1251;

DROP PROCEDURE IF EXISTS rcs__populate_sensoralgorithms;
CREATE PROCEDURE rcs__populate_sensoralgorithms()
BEGIN
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 1
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (1, '������ ������������', '��� ������� ������������ �������� ���. ����.', 1);
  ELSE
    UPDATE sensoralgorithms SET `Name`='���������� ������' WHERE `ID`=1;
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 2
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (2, '������ ��������� ���������', '������ ������������ ��� ����������� � ���.', 2);
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 3
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (3, '������� ������������', '��� ������� ������������ �-�� ��������', 3);
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 4
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (4, '����������', '�������������� ���� ����', 4);
  ELSE
    UPDATE sensoralgorithms SET `Description` = '�������������� ���� ����' WHERE `ID` = 4;
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 5
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (5, '������� ��������� ���������', '��� ������� ������������ �-�� ��������', 5);
  ELSE
    UPDATE sensoralgorithms SET `Name` = '������� ��������� ���������' WHERE `ID` = 5;
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 6
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (6, '�����������', '������ �����������', 6);
  ELSE
    UPDATE sensoralgorithms SET `Description` = '������ �����������' WHERE `ID` = 6;
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 7
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (7, '������� �������', '������� ������� � ����1', 7);
  ELSE
    UPDATE sensoralgorithms SET `Name` = '������� �������' WHERE `ID` = 7;
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 8
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (8, '������ �������', '������� ������� � ����2', 8);
  ELSE
    UPDATE sensoralgorithms SET `Name` = '������ �������' WHERE `ID` = 8;
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 9
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (9, '���������1', '��� �������� ���������� ����������1', 9);
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 10
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (10, '���������2', '��� �������� ���������� ����������2', 10);
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 11
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (11, '�����1', '��� ����������1', 11);
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 12
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (12, '�����2', '��� ����������2', 12);
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 13
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (13, '������������� ��������', '��� ������������� ���������', 13);
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 14
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (14, '������������� ���������� ����������', '��� ������������� ��������� ���������', 14);
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 15
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (15, '����������������', '��� �������� �������� � ���������', 15);
  END IF;
  IF NOT EXISTS
  (
    SELECT NULL FROM sensoralgorithms WHERE ID = 16
  )
  THEN
    INSERT INTO sensoralgorithms (`ID`,`Name`,`Description`,`AlgorithmID`)
      VALUES (16, '�������� - ���������� ������', '��� ����������� � ������ ��������', 16);
  END IF;
END;
CALL rcs__populate_sensoralgorithms();
DROP PROCEDURE IF EXISTS rcs__populate_sensoralgorithms;

-- ������� Lines

CREATE TABLE IF NOT EXISTS `lines`(
  id INT (11) NOT NULL AUTO_INCREMENT,
  Color INT (11) DEFAULT 0 COMMENT '����',
  Width INT (11) DEFAULT 3 COMMENT '������� �����',
  Mobitel_ID INT (11) DEFAULT NULL,
  Icon BLOB DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX lines_FK1 USING BTREE (Mobitel_ID),
  CONSTRAINT lines_FK1 FOREIGN KEY (Mobitel_ID) REFERENCES mobitels (Mobitel_ID)
) ENGINE = INNODB DEFAULT CHARSET=cp1251 COMMENT = '�������� �����';

DELETE FROM `lines` WHERE (`Mobitel_ID` IS NULL) OR (`Icon` IS NULL);

-- �������� ���������

DROP PROCEDURE IF EXISTS CheckOdo;
CREATE PROCEDURE CheckOdo(IN m_id INTEGER(11), IN end_time DATETIME)
  SQL SECURITY INVOKER
BEGIN
  SELECT 
   FROM_UNIXTIME(datagps.UnixTime) AS `time` ,
   (datagps.Latitude / 600000.00000) AS Lat,
   (datagps.Longitude / 600000.00000) AS Lon
  FROM
    datagps
  WHERE
    datagps.Mobitel_ID = m_id AND 
    datagps.UnixTime BETWEEN (SELECT UNIX_TIMESTAMP(odoTime)  FROM vehicle WHERE Mobitel_id = m_id) AND 
    UNIX_TIMESTAMP(end_time) AND 
    Valid = 1;
END;


DROP PROCEDURE IF EXISTS dataview;
CREATE PROCEDURE dataview(
  IN m_id INTEGER(11), 
  IN val BOOL, 
  IN begin_time DATETIME, 
  IN end_time DATETIME)
  SQL SECURITY INVOKER
BEGIN
  SELECT DataGps_ID AS DataGps_ID,
    Mobitel_ID AS Mobitel_ID,
    ((Latitude * 1.0000) / 600000) AS Lat,
    ((Longitude * 1.0000) / 600000) AS Lon,
    Altitude AS Altitude,
    FROM_UNIXTIME(UnixTime) AS `time`,
    (Speed * 1.852) AS speed,
    ((Direction * 360) / 255) AS direction,
    Valid AS Valid,
    (Sensor8 + (Sensor7 << 8) + (Sensor6 << 16) + 
      (Sensor5 << 24) + (Sensor4 << 32) + (Sensor3 << 40) + 
      (Sensor2 << 48) + (Sensor1 << 56)) AS sensor,
    Events AS Events,
    LogID AS LogID 
  FROM datagps 
  WHERE Mobitel_ID = m_id AND  
    (UnixTime BETWEEN UNIX_TIMESTAMP(begin_time) AND UNIX_TIMESTAMP(end_time)) AND                                                                                                                                
    (Valid = val)
  ORDER BY UnixTime, LogId;
END;


DROP PROCEDURE IF EXISTS hystoryOnline;
CREATE PROCEDURE hystoryOnline(IN id INTEGER(11))
  SQL SECURITY INVOKER
BEGIN
  SELECT LogID, Mobitel_Id, 
    FROM_UNIXTIME(unixtime)  AS `time`,
    ROUND((Latitude / 600000.00000), 6) AS Lat,
    ROUND((Longitude / 600000.00000), 6)  AS Lon,
    Altitude  AS Altitude,
    (Speed * 1.852) AS speed,
    ((Direction * 360) / 255) AS direction,
    Valid AS Valid,
    (Sensor8 + (Sensor7 << 8) + (Sensor6 << 16) + 
      (Sensor5 << 24) + (Sensor4 << 32) + (Sensor3 << 40) + 
      (Sensor2 << 48) + (Sensor1 << 56)) AS sensor,
    Events AS Events,
    DataGPS_ID  AS 'DataGPS_ID'
  FROM online
  WHERE  datagps_id > id
  ORDER BY datagps_id DESC;
END;


DROP PROCEDURE IF EXISTS online;
CREATE PROCEDURE online()
  SQL SECURITY INVOKER
BEGIN
  SELECT Log_ID, MobitelId, 
    (SELECT DataGPS_ID
    FROM `online` 
    WHERE mobitel_id = MobitelId AND LogId = Log_ID Limit 1) AS 'DataGPS_ID',
      (SELECT from_unixtime(unixtime) 
      FROM `online` 
      WHERE mobitel_id = MobitelId AND LogId = Log_ID Limit 1) AS 'time',
        (SELECT ROUND((`Latitude` / 600000.00000),6)
        FROM `online` 
        WHERE mobitel_id = MobitelId AND LogId = Log_ID Limit 1) AS `Lat`,
          (SELECT ROUND((`Longitude` / 600000.00000),6) 
          FROM `online` 
          WHERE mobitel_id = MobitelId AND LogId = Log_ID Limit 1) AS `Lon`,
            (SELECT `Altitude` 
            FROM `online` 
            WHERE mobitel_id = MobitelId AND LogId = Log_ID Limit 1) AS `Altitude`,
              (SELECT (`Speed` * 1.852)
              FROM `online` 
              WHERE mobitel_id = MobitelId AND LogId = Log_ID Limit 1) AS `speed`,
                (SELECT ((`Direction` * 360) / 255)
                FROM `online` 
                WHERE mobitel_id = MobitelId AND LogId = Log_ID Limit 1) AS `direction`,
                  (SELECT `Valid`
                  FROM `online` 
                  WHERE mobitel_id = MobitelId AND LogId = Log_ID Limit 1) AS `Valid`,
                    (SELECT (((((((`Sensor8` + 
                    (`Sensor7` << 8)) + 
                    (`Sensor6` << 16)) + 
                    (`Sensor5` << 24)) + 
                    (`Sensor4` << 32)) + 
                    (`Sensor3` << 40)) + 
                    (`Sensor2` << 48)) + 
                    (`Sensor1` << 56)) 
                    FROM `online` 
                    WHERE mobitel_id = MobitelId AND LogId = Log_ID Limit 1) AS `sensor`,
                      (SELECT `Events`
                      FROM `online` 
                      WHERE mobitel_id = MobitelId AND LogId = Log_ID Limit 1) AS `Events`
   
  FROM (SELECT MAX(`LogID`) AS `Log_ID`, mobitel_id AS MobitelId
        FROM `online` 
        GROUP BY `Mobitel_ID`) T1;
END;

CREATE TABLE IF NOT EXISTS mobitels_rotate_bands(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Mobitel_ID INT(11) NOT NULL DEFAULT 0,
  Bound INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (Id),
  INDEX FK_mobitels_rotate_bands_mobitels_Mobitel_ID (Mobitel_ID),
  CONSTRAINT FK_mobitels_rotate_bands_mobitels_Mobitel_ID FOREIGN KEY (Mobitel_ID)
  REFERENCES mobitels (Mobitel_ID) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;