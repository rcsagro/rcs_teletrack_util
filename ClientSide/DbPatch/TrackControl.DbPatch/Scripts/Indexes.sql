DROP PROCEDURE IF EXISTS tmpAddIndex;
CREATE PROCEDURE tmpAddIndex()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '��������� ��������� ���������� ��������'
BEGIN
  DECLARE DbName VARCHAR(64);
  SELECT DATABASE() INTO DbName;
  
  IF NOT EXISTS (
    SELECT NULL 
    FROM information_schema.STATISTICS
    WHERE (TABLE_SCHEMA = DbName) AND 
      (TABLE_NAME = "internalmobitelconfig") AND 
      (INDEX_NAME = "Index_3"))
  THEN
    ALTER TABLE internalmobitelconfig ADD INDEX Index_3 (Message_ID);
  END IF;
  
  IF NOT EXISTS (
    SELECT NULL 
    FROM information_schema.STATISTICS
    WHERE (TABLE_SCHEMA = DbName) AND 
      (TABLE_NAME = "internalmobitelconfig") AND 
      (INDEX_NAME = "Index_4"))
  THEN
    ALTER TABLE internalmobitelconfig ADD INDEX Index_4 (devIdShort);
  END IF;
  
  IF NOT EXISTS (
    SELECT NULL 
    FROM information_schema.STATISTICS
    WHERE (TABLE_SCHEMA = DbName) AND 
      (TABLE_NAME = "internalmobitelconfig") AND 
      (INDEX_NAME = "IDX_InternalmobitelconfigID"))
  THEN
    ALTER TABLE internalmobitelconfig ADD INDEX IDX_InternalmobitelconfigID (ID);
  END IF;
  
  IF NOT EXISTS (
    SELECT NULL 
    FROM information_schema.STATISTICS
    WHERE (TABLE_SCHEMA = DbName) AND 
      (TABLE_NAME = "servicesend") AND 
      (INDEX_NAME = "Index_2"))
  THEN
    ALTER TABLE servicesend ADD INDEX Index_2 (ID);
  END IF;
END;

CALL tmpAddIndex;
DROP PROCEDURE IF EXISTS tmpAddIndex;