-- ��� ���, � ���� � ������� �������� (setting) �����������
-- ������� AvgFuelRatePerHour, FuelApproximationTime, TimeGetFuelAfterStop, TimeGetFuelBeforeMotion 

DROP PROCEDURE IF EXISTS rcs__add_some_new__Columns_to__SettingTbl;
CREATE PROCEDURE rcs__add_some_new__Columns_to__SettingTbl()
BEGIN
	IF EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "setting"
	)
	THEN
	
		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND TABLE_NAME = "setting"
				AND	COLUMN_NAME = "AvgFuelRatePerHour"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `AvgFuelRatePerHour` DOUBLE (10, 5) NOT NULL DEFAULT 15.00000 COMMENT '������� ������ ������� � ���';
		END IF;

		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND	TABLE_NAME = "setting"
				AND	COLUMN_NAME = "FuelApproximationTime"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `FuelApproximationTime` TIME NOT NULL DEFAULT '00:02:00' COMMENT '��������� �������� ����� ��������, �� ����� �������� ���������� ������� ������� � ������ �������';
		END IF;
		
		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND	TABLE_NAME = "setting"
				AND	COLUMN_NAME = "TimeGetFuelAfterStop"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `TimeGetFuelAfterStop` TIME NOT NULL DEFAULT '00:00:00' COMMENT '��������� �������� ����� ������� ������� � ������� ������ �������';
		END IF;
		
		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND	TABLE_NAME = "setting"
				AND	COLUMN_NAME = "TimeGetFuelBeforeMotion"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `TimeGetFuelBeforeMotion` TIME NOT NULL DEFAULT '00:00:00' COMMENT '��������� �������� ����� ������� ������ ������� � ������� ��������';
		END IF;
		
		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND	TABLE_NAME = "setting"
				AND	COLUMN_NAME = "CzMinCrossingPairTime"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `CzMinCrossingPairTime` TIME NOT NULL DEFAULT '00:01:00' COMMENT 'Min ��������� �������� ����� ����� ������������� ��';
		END IF;
		
		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND	TABLE_NAME = "setting"
				AND	COLUMN_NAME = "FuelerRadiusFinds"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `FuelerRadiusFinds` INT NOT NULL DEFAULT 10 COMMENT '������ ������ ������������ �� ������ �����������������';
		END IF;
		
		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND	TABLE_NAME = "setting"
				AND	COLUMN_NAME = "FuelerMaxTimeStop"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `FuelerMaxTimeStop` TIME NOT NULL DEFAULT '00:02:00' COMMENT '����������� ����� �� �������� ���������� ��';
		END IF;
		
		IF NOT EXISTS
		(
			SELECT NULL FROM information_schema.columns
				WHERE TABLE_SCHEMA = (SELECT DATABASE())
				AND	TABLE_NAME = "setting"
				AND	COLUMN_NAME = "FuelerMinFuelrate"
		)
		THEN
			ALTER TABLE `setting` ADD COLUMN `FuelerMinFuelrate` INT NOT NULL DEFAULT 1 COMMENT '����������� �������� ��������, ��� ����� �������� � �����';
		END IF;
		
	END IF;
END;
CALL rcs__add_some_new__Columns_to__SettingTbl();
DROP PROCEDURE IF EXISTS rcs__add_some_new__Columns_to__SettingTbl;