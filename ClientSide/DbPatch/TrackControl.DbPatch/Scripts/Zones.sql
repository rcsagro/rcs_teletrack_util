﻿DROP PROCEDURE IF EXISTS ZonesFields;
CREATE PROCEDURE ZonesFields ()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Новые поля в таблице контрольных зон'
BEGIN
  IF EXISTS (
    SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "zones")
  THEN
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "zones" AND COLUMN_NAME = "Square")
    THEN
      ALTER TABLE `zones` ADD COLUMN `Square` DOUBLE(14, 5) NULL DEFAULT 0 COMMENT 'Площадь контрольной зоны';
    END IF;
    
    /*--------------------------------------------------------------------------*/
    
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "zones" AND COLUMN_NAME = "ZoneColor")
    THEN
      ALTER TABLE `zones` ADD COLUMN `ZoneColor` INT(11) NULL DEFAULT 0 COMMENT 'Цвет контрольной зоны';
      ALTER TABLE `zones` ADD COLUMN `TextColor` INT(11) NULL DEFAULT 0 COMMENT 'Цвет подписи контрольной зоны';
      ALTER TABLE `zones` ADD COLUMN `TextFontName` CHAR(50) NULL DEFAULT 'Microsoft Sans Serif' COMMENT 'Шрирфт подписи контрольной зоны';			
      ALTER TABLE `zones` ADD COLUMN `TextFontSize` DOUBLE(5, 2) NULL DEFAULT 25 COMMENT 'Размер шрирфта подписи контрольной зоны';		
      ALTER TABLE `zones` ADD COLUMN `TextFontParams` CHAR(8)  NULL DEFAULT '0;0;0;0;' COMMENT 'Параметры шрирфта подписи контрольной зоны';	
      ALTER TABLE `zones` ADD COLUMN `Icon` BLOB NULL DEFAULT NULL COMMENT 'Иконка контрольной зоны';
      ALTER TABLE `zones` ADD COLUMN `HatchStyle` INT(11) NULL DEFAULT 0 COMMENT 'Текстура контрольной зоны';
      ALTER TABLE `zones` ADD COLUMN `DateCreate` DATETIME NULL DEFAULT NULL COMMENT 'Дата создания';
      ALTER TABLE `zones` ADD COLUMN `Id_main` INT(11) NULL DEFAULT 0 COMMENT 'Ссылка на элемент верхнего уровня в иерархии';
      ALTER TABLE `zones` ADD COLUMN `Level` TINYINT(4) NULL DEFAULT 1 COMMENT 'Уровень в иерархии';
      
      UPDATE `zones`
      SET `ZoneColor` = -16776961, `TextColor` = -16777216, `TextFontName` = 'Microsoft Sans Serif', `TextFontSize` = 48, `TextFontParams` = '0;0;0;0;'
      WHERE (`ZoneColor` = 0) OR (`ZoneColor` IS NULL);
    END IF;
    
  END IF;
END;
CALL ZonesFields();
DROP PROCEDURE IF EXISTS ZonesFields;

CREATE TABLE IF NOT EXISTS `zonesgroup` (
	`Id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `Title` VARCHAR(50) NOT NULL,
  `Description` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE = InnoDB CHARACTER SET cp1251 COLLATE cp1251_general_ci;

DROP PROCEDURE IF EXISTS rcs__SetZonesGroup_4Zones;
CREATE PROCEDURE rcs__SetZonesGroup_4Zones()
BEGIN
	IF NOT EXISTS
	(
		SELECT NULL FROM `zonesgroup`	WHERE `Id` = 1
	)
	THEN
		SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;	
		INSERT INTO `zonesgroup` (`Id`, `Title`, `Description`) VALUES (1, 'Все зоны', 'НЕ УДАЛЯТЬ! Корневой элемент дерева контрольных зон');
		SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
	END IF;
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zones") AND
        (COLUMN_NAME = "StyleId")
	)
	THEN
		ALTER TABLE `zones` ADD COLUMN `StyleId` INTEGER UNSIGNED NOT NULL DEFAULT 0 COMMENT 'ID стиля зоны';
	END IF;
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zones") AND
        (COLUMN_NAME = "ZonesGroupId")
	)
	THEN
		ALTER TABLE `zones`
			ADD COLUMN `ZonesGroupId` INTEGER UNSIGNED NOT NULL DEFAULT 1 COMMENT 'ID Группы зон' AFTER `StyleId`,
			ADD CONSTRAINT `FK_zones_zonesgroup` FOREIGN KEY `FK_zones_zonesgroup` (`ZonesGroupId`)
				REFERENCES `zonesgroup` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;
	END IF;
END;
CALL rcs__SetZonesGroup_4Zones();
DROP PROCEDURE IF EXISTS rcs__SetZonesGroup_4Zones;

DROP PROCEDURE IF EXISTS rcs_Create_PointsFK_4Zones;
CREATE PROCEDURE rcs_Create_PointsFK_4Zones()
BEGIN
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.KEY_COLUMN_USAGE
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "points") AND (CONSTRAINT_NAME = "FK_points_zones")
	)
	THEN
		DELETE FROM `points` WHERE `Zone_ID` NOT IN (SELECT `Zone_ID` FROM `zones`);
		ALTER TABLE `points`
			ADD CONSTRAINT `FK_points_zones` FOREIGN KEY `FK_points_zones` (`Zone_ID`)
				REFERENCES `zones` (`Zone_ID`) ON DELETE CASCADE ON UPDATE CASCADE;
	END IF;
END;
CALL rcs_Create_PointsFK_4Zones();
DROP PROCEDURE IF EXISTS rcs_Create_PointsFK_4Zones;