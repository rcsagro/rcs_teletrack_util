DROP PROCEDURE IF EXISTS rcs__change_engine_for_T_ables;
CREATE PROCEDURE rcs__change_engine_for_T_ables()
BEGIN
	DECLARE tableName VARCHAR(64);
	DECLARE done TINYINT DEFAULT 1;
	DECLARE cursorTables CURSOR FOR SELECT TABLE_NAME
																	FROM information_schema.tables
																	WHERE TABLE_SCHEMA = (SELECT DATABASE())
																	AND ENGINE = "MyISAM";
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 0;
	
	OPEN cursorTables;
	FETCH cursorTables INTO tableName;
	WHILE done = 1 DO
		SET @sql = CONCAT("ALTER TABLE ", tableName, " ENGINE = InnoDB");
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
    
		FETCH cursorTables INTO tableName;
	END WHILE;
	CLOSE cursorTables;
END;
CALL rcs__change_engine_for_T_ables();
DROP PROCEDURE IF EXISTS rcs__change_engine_for_T_ables;