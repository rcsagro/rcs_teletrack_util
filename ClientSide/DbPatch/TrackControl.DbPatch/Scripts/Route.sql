﻿CREATE TABLE IF NOT EXISTS  rt_events(
  Id INT (11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR (50) NOT NULL,
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 0
CHARACTER SET cp1251
COLLATE cp1251_general_ci;


DROP PROCEDURE IF EXISTS rcs__rt_events;
CREATE PROCEDURE  rcs__rt_events()
BEGIN
  IF NOT EXISTS
  (
  SELECT
    *
  FROM
    rt_events
  WHERE
    id = 1
  ) THEN
    INSERT
    INTO rt_events (`Name`)
    VALUES ('Въезд');
    INSERT
    INTO rt_events (`Name`)
    VALUES ('Выезд');
  END IF;
END;
CALL rcs__rt_events();
DROP PROCEDURE IF EXISTS rcs__rt_events;


CREATE TABLE IF NOT EXISTS  rt_route_type(
  Id INT (11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR (50) NOT NULL COMMENT 'Тип маршрута',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 0
CHARACTER SET cp1251
COLLATE cp1251_general_ci;


DROP PROCEDURE IF EXISTS rcs__route_type;
CREATE PROCEDURE  rcs__route_type()
BEGIN
  IF NOT EXISTS
  (
  SELECT
    *
  FROM
    rt_route_type
  WHERE
    id = 1
  ) THEN
    INSERT
    INTO rt_route_type (`Name`)
    VALUES ('Контрольные зоны');
    INSERT
    INTO rt_route_type (`Name`)
    VALUES ('Населенные пункты');
  END IF;
END;
CALL rcs__route_type();
DROP PROCEDURE IF EXISTS rcs__route_type;

CREATE TABLE IF NOT EXISTS  rt_sample(
  Id INT (11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR (100) NOT NULL,
  Id_mobitel INT (11) DEFAULT 0 COMMENT 'Транспортное средство (опц)',
  TimeStart TIME DEFAULT NULL COMMENT 'Время начала маршрута (опц)',
  Distance DOUBLE DEFAULT 0 COMMENT 'Общий путь, км',
  Remark TEXT DEFAULT NULL COMMENT 'Примечание',
  Id_main INT (11) NOT NULL DEFAULT 0 COMMENT 'Ссылка на элемент верхнего уровня в иерархии',
  IsGroupe TINYINT (1) NOT NULL DEFAULT 0 COMMENT 'Запись - группа',
  AutoCreate TINYINT (1) NOT NULL DEFAULT 0 COMMENT 'Маршрут может автосоздаваться ',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 0
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

CREATE TABLE IF NOT EXISTS  rt_samplet(
  Id INT (11) NOT NULL AUTO_INCREMENT,
  Id_main INT (11) NOT NULL,
  Position TINYINT (4) NOT NULL COMMENT 'Порядковый номер записи шаблона маршрута',
  Id_event INT (11) NOT NULL COMMENT 'Событие - въезд / выезд ',
  Id_zone INT (11) DEFAULT NULL COMMENT 'Контрольная зона',
  TimeRel CHAR (5) DEFAULT '00:00' COMMENT 'Относительное время от начала маршрута',
  TimeRelP CHAR (5) DEFAULT '00:00' COMMENT 'Допустимое отклонение (+)',
  TimeRelM CHAR (5) DEFAULT '00:00' COMMENT 'Допустимое отклонение (-)',
  PRIMARY KEY (Id),
  INDEX rt_samplet_FK1 USING BTREE (Id_main),
  CONSTRAINT rt_samplet_FK1 FOREIGN KEY (Id_main)
  REFERENCES rt_sample (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 0
CHARACTER SET cp1251
COLLATE cp1251_general_ci;


CREATE TABLE IF NOT EXISTS  rt_route(
  Id INT (11) NOT NULL AUTO_INCREMENT,
  Id_sample INT (11) NOT NULL COMMENT 'Ссылка на шаблон маршрута ',
  Id_mobitel INT (11) NOT NULL DEFAULT 0 COMMENT 'Транспортное средство ',
  Id_driver INT (11) NOT NULL DEFAULT 0 COMMENT 'Водитель',
  TimeStartPlan DATETIME DEFAULT NULL COMMENT 'План время начала маршрута',
  Distance DOUBLE NOT NULL DEFAULT 0 COMMENT 'Общий путь, км',
  TimePlanTotal  VARCHAR (20) DEFAULT NULL COMMENT 'Общее время на маршруте (план)',
  TimeFactTotal  VARCHAR (20) DEFAULT NULL COMMENT 'Общее время на маршруте (факт)',
  Deviation VARCHAR (20) DEFAULT NULL COMMENT 'Отклонение от графика',
  DeviationAr VARCHAR (20) DEFAULT NULL COMMENT 'Отклонение по времени прибытия',
  Remark TEXT DEFAULT NULL COMMENT 'Примечание',
  FuelStart DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Топливо в начале',
  FuelAdd DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Заправлено',
  FuelSub DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Слито',
  FuelEnd DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Топливо в конце',
  FuelExpens DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Общий расход топлива',
  FuelExpensAvg DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДУТ Средний расход,  л/100 км',
  Fuel_ExpensMove DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДРТ/CAN Расход топлива в движении, л',
  Fuel_ExpensStop DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДРТ/CAN Расход топлива на стоянках, л',
  Fuel_ExpensTotal DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДРТ/CAN Общий расход, л',
  Fuel_ExpensAvg DOUBLE (10, 2) NOT NULL DEFAULT 0.00 COMMENT 'ДРТ/CAN Средний расход, л/100 км',
  TimeMove VARCHAR (20) DEFAULT NULL COMMENT 'Время движения',
  TimeStop VARCHAR (20) DEFAULT NULL COMMENT 'Время остановок',
  SpeedAvg DOUBLE (10, 2) DEFAULT 0.00 COMMENT 'Средняя скорость',
  FuelAddQty INT (11) DEFAULT 0 COMMENT 'ДУТ Заправлено - кол-во',
  FuelSubQty INT (11) DEFAULT 0 COMMENT 'ДУТ Слито - кол-во',
  PointsValidity INT (11) DEFAULT 0 COMMENT 'Валидность данных',
  PointsCalc INT (11) DEFAULT 0 COMMENT 'Количество точек (по Log_ID) расчетное',
  PointsFact INT (11) DEFAULT 0 COMMENT 'Количество точек (по Log_ID) фактическое',
  PointsIntervalMax VARCHAR (20) DEFAULT NULL COMMENT 'Максимальный интервал между точками',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 0
CHARACTER SET cp1251
COLLATE cp1251_general_ci;


CREATE TABLE IF NOT EXISTS  rt_routet(
  Id INT (11) NOT NULL AUTO_INCREMENT,
  Id_main INT (11) NOT NULL,
  Id_zone INT (11) NOT NULL DEFAULT 0 COMMENT 'Контрольная зона',
  Id_event INT (11) NOT NULL DEFAULT 1 COMMENT 'Событие - въезд / выезд ',
  DatePlan DATETIME DEFAULT NULL COMMENT 'План время события ',
  DateFact DATETIME DEFAULT NULL COMMENT 'Факт время события ',
  Deviation VARCHAR (10) DEFAULT NULL COMMENT 'Отклонение от графика',
  Remark TEXT DEFAULT NULL COMMENT 'Примечание',
  Distance DOUBLE DEFAULT 0 COMMENT 'Путь между точками маршрута',
  PRIMARY KEY (Id),
  INDEX rt_routet_FK1 USING BTREE (Id_main),
  CONSTRAINT rt_routet_FK1 FOREIGN KEY (Id_main)
  REFERENCES rt_route (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 0
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

CREATE TABLE IF NOT EXISTS  rt_route_stops(
  Id INT (11) NOT NULL AUTO_INCREMENT,
  Id_main INT (11) NOT NULL,
  Location VARCHAR (255) DEFAULT NULL,
  InitialTime DATETIME DEFAULT NULL,
  FinalTime DATETIME DEFAULT NULL,
  `Interval` TIME DEFAULT NULL,
  CheckZone TINYINT (1) NOT NULL DEFAULT 0 COMMENT 'Остановка в контрольной зоне',
  PRIMARY KEY (Id),
  INDEX rt_route_stops_FK1 USING BTREE (Id_main),
  CONSTRAINT rt_route_stops_FK1 FOREIGN KEY (Id_main)
  REFERENCES rt_route (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 0
CHARACTER SET cp1251
COLLATE cp1251_general_ci;



CREATE TABLE  IF NOT EXISTS  rt_route_sensors(
  Id INT (11) NOT NULL AUTO_INCREMENT,
  Id_main INT (11) NOT NULL,
  SensorName VARCHAR (127) NOT NULL,
  Location VARCHAR (255) NOT NULL,
  EventTime DATETIME NOT NULL,
  Duration TIME NOT NULL,
  Description VARCHAR (255) DEFAULT NULL,
  Speed DOUBLE DEFAULT 0,
  PRIMARY KEY (Id),
  INDEX rt_route_sensors_FK1 USING BTREE (Id_main),
  CONSTRAINT rt_route_sensors_FK1 FOREIGN KEY (Id_main)
  REFERENCES  rt_route (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 0
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

CREATE TABLE  IF NOT EXISTS  rt_route_fuel(
  Id INT (11) NOT NULL AUTO_INCREMENT,
  Id_main INT (11) NOT NULL,
  Location VARCHAR (255) NOT NULL,
  time_ DATETIME NOT NULL,
  dValueCalc DOUBLE NOT NULL DEFAULT 0 COMMENT 'Расчетная величина',
  dValueHandle DOUBLE DEFAULT 0 COMMENT 'Ручная правка',
  PRIMARY KEY (Id),
  UNIQUE INDEX Id USING BTREE (Id),
  CONSTRAINT rt_route_fuel_FK1 FOREIGN KEY (Id_main)
  REFERENCES  rt_route (Id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 0
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

	CREATE TABLE IF NOT EXISTS  agro_params(
	  Id INT (11) NOT NULL AUTO_INCREMENT,
	  Number INT (11) NOT NULL COMMENT 'Номер параметра',
	  Value VARCHAR (100) DEFAULT NULL COMMENT 'Значение',
	  Description VARCHAR (50) DEFAULT NULL COMMENT 'Описание',
	  PRIMARY KEY (Id)
	)
	ENGINE = INNODB
	AUTO_INCREMENT = 1
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci
	COMMENT = 'Таблица параметров';

DROP PROCEDURE IF EXISTS rcs_NewRouteFields;
CREATE PROCEDURE rcs_NewRouteFields ()
BEGIN
  IF EXISTS (
    SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "rt_routet")
  THEN
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "rt_routet" AND COLUMN_NAME = "Location")
    THEN
      ALTER TABLE `rt_routet` ADD COLUMN `Location` VARCHAR(255) DEFAULT NULL COMMENT 'Населенный пункт';
    END IF;
  END IF;
  IF EXISTS (
    SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "rt_route")
  THEN
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "rt_route" AND COLUMN_NAME = "Type")
    THEN
      ALTER TABLE `rt_route` ADD COLUMN `Type` TINYINT(4) NOT NULL DEFAULT 1 COMMENT 'Тип маршрутного листа - КЗ или населенные пункты';
    END IF;
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "rt_route" AND COLUMN_NAME = "TimeEndFact")
    THEN
      ALTER TABLE `rt_route` ADD COLUMN `TimeEndFact` DATETIME DEFAULT NULL COMMENT 'Фактическое время окончания маршрута';
    END IF;
    
        IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "rt_route" AND COLUMN_NAME = "DistLogicSensor")
    THEN
      ALTER TABLE `rt_route` ADD COLUMN `DistLogicSensor` DOUBLE (10, 2) NULL DEFAULT 0.00 COMMENT 'Путь с включенным логическим датчиком, км';
    END IF;
    
         IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "rt_route_sensors" AND COLUMN_NAME = "Distance")
    THEN
      ALTER TABLE `rt_route_sensors` ADD COLUMN `Distance` DOUBLE (10, 2) NULL DEFAULT 0.00 COMMENT 'Путь, км';
    END IF;

         IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "rt_samplet" AND COLUMN_NAME = "TimeAbs")
    THEN
      ALTER TABLE `rt_samplet` ADD COLUMN `TimeAbs` CHAR (5) DEFAULT '00:00' COMMENT 'Абсолютное время  маршрута';
    END IF;    
    
  END IF;
END;
CALL rcs_NewRouteFields();
DROP PROCEDURE IF EXISTS rcs_NewRouteFields;