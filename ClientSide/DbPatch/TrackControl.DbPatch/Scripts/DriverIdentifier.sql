﻿DROP PROCEDURE IF EXISTS DriverIdentifier;
CREATE PROCEDURE DriverIdentifier()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Новое поля в таблице водителя'
BEGIN
  IF EXISTS (
    SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "driver")
  THEN
    IF NOT EXISTS (
      SELECT NULL FROM information_schema.columns
      WHERE TABLE_SCHEMA = (SELECT DATABASE())
        AND TABLE_NAME = "driver" AND COLUMN_NAME = "Identifier")
    THEN
      ALTER TABLE driver ADD COLUMN Identifier SMALLINT UNSIGNED NULL COMMENT '10 битный идентификатор водителя';
      ALTER TABLE driver ADD UNIQUE UQ_Identifier (Identifier);
    END IF;
   END IF;
END;
CALL DriverIdentifier();
DROP PROCEDURE IF EXISTS DriverIdentifier;
