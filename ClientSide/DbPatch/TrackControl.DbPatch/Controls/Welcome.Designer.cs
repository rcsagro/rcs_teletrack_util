namespace TrackControl.DbPatch.Controls
{
  partial class Welcome
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Welcome));
        this.mainTable = new System.Windows.Forms.TableLayoutPanel();
        this.mySqlIcon = new System.Windows.Forms.Label();
        this.textTable = new System.Windows.Forms.TableLayoutPanel();
        this.headLbl = new System.Windows.Forms.Label();
        this.textLbl = new System.Windows.Forms.Label();
        this.loginGroupBox = new System.Windows.Forms.GroupBox();
        this.loginTable = new System.Windows.Forms.TableLayoutPanel();
        this.hostLbl = new System.Windows.Forms.Label();
        this.dbNameLbl = new System.Windows.Forms.Label();
        this.loginLbl = new System.Windows.Forms.Label();
        this.passLbl = new System.Windows.Forms.Label();
        this.hostTextBox = new System.Windows.Forms.TextBox();
        this.dbNameTextBox = new System.Windows.Forms.TextBox();
        this.loginTextBox = new System.Windows.Forms.TextBox();
        this.passwordTextBox = new System.Windows.Forms.TextBox();
        this.nextBtn = new System.Windows.Forms.Button();
        this.warningTable = new System.Windows.Forms.TableLayoutPanel();
        this.warningIcon = new System.Windows.Forms.Label();
        this.warningText = new System.Windows.Forms.Label();
        this.mainTable.SuspendLayout();
        this.textTable.SuspendLayout();
        this.loginGroupBox.SuspendLayout();
        this.loginTable.SuspendLayout();
        this.warningTable.SuspendLayout();
        this.SuspendLayout();
        // 
        // mainTable
        // 
        this.mainTable.BackColor = System.Drawing.Color.Transparent;
        this.mainTable.ColumnCount = 5;
        this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
        this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
        this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
        this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
        this.mainTable.Controls.Add(this.mySqlIcon, 1, 1);
        this.mainTable.Controls.Add(this.textTable, 3, 1);
        this.mainTable.Controls.Add(this.loginGroupBox, 1, 3);
        this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
        this.mainTable.Location = new System.Drawing.Point(0, 0);
        this.mainTable.Margin = new System.Windows.Forms.Padding(0);
        this.mainTable.Name = "mainTable";
        this.mainTable.RowCount = 5;
        this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
        this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
        this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
        this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
        this.mainTable.Size = new System.Drawing.Size(492, 346);
        this.mainTable.TabIndex = 0;
        // 
        // mySqlIcon
        // 
        this.mySqlIcon.AutoSize = true;
        this.mySqlIcon.Dock = System.Windows.Forms.DockStyle.Fill;
        this.mySqlIcon.Image = global::TrackControl.DbPatch.Res.MySQL;
        this.mySqlIcon.Location = new System.Drawing.Point(15, 15);
        this.mySqlIcon.Margin = new System.Windows.Forms.Padding(0);
        this.mySqlIcon.Name = "mySqlIcon";
        this.mySqlIcon.Size = new System.Drawing.Size(115, 120);
        this.mySqlIcon.TabIndex = 0;
        // 
        // textTable
        // 
        this.textTable.ColumnCount = 1;
        this.textTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.textTable.Controls.Add(this.headLbl, 0, 0);
        this.textTable.Controls.Add(this.textLbl, 0, 1);
        this.textTable.Dock = System.Windows.Forms.DockStyle.Fill;
        this.textTable.Location = new System.Drawing.Point(145, 15);
        this.textTable.Margin = new System.Windows.Forms.Padding(0);
        this.textTable.Name = "textTable";
        this.textTable.RowCount = 2;
        this.textTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
        this.textTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.textTable.Size = new System.Drawing.Size(332, 120);
        this.textTable.TabIndex = 1;
        // 
        // headLbl
        // 
        this.headLbl.AutoSize = true;
        this.headLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.headLbl.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.headLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(58)))), ((int)(((byte)(57)))));
        this.headLbl.Location = new System.Drawing.Point(0, 0);
        this.headLbl.Margin = new System.Windows.Forms.Padding(0);
        this.headLbl.Name = "headLbl";
        this.headLbl.Size = new System.Drawing.Size(332, 25);
        this.headLbl.TabIndex = 0;
        this.headLbl.Text = "���������� ��������� ��";
        // 
        // textLbl
        // 
        this.textLbl.AutoSize = true;
        this.textLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.textLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.textLbl.Location = new System.Drawing.Point(0, 25);
        this.textLbl.Margin = new System.Windows.Forms.Padding(0);
        this.textLbl.Name = "textLbl";
        this.textLbl.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
        this.textLbl.Size = new System.Drawing.Size(332, 95);
        this.textLbl.TabIndex = 1;
        this.textLbl.Text = resources.GetString("textLbl.Text");
        // 
        // loginGroupBox
        // 
        this.mainTable.SetColumnSpan(this.loginGroupBox, 3);
        this.loginGroupBox.Controls.Add(this.loginTable);
        this.loginGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
        this.loginGroupBox.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.loginGroupBox.ForeColor = System.Drawing.Color.Navy;
        this.loginGroupBox.Location = new System.Drawing.Point(18, 153);
        this.loginGroupBox.Name = "loginGroupBox";
        this.loginGroupBox.Padding = new System.Windows.Forms.Padding(20, 10, 20, 10);
        this.loginGroupBox.Size = new System.Drawing.Size(456, 175);
        this.loginGroupBox.TabIndex = 2;
        this.loginGroupBox.TabStop = false;
        this.loginGroupBox.Text = "����������� � ���� MySQL";
        // 
        // loginTable
        // 
        this.loginTable.ColumnCount = 6;
        this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
        this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
        this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
        this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95F));
        this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.loginTable.Controls.Add(this.hostLbl, 0, 0);
        this.loginTable.Controls.Add(this.dbNameLbl, 0, 2);
        this.loginTable.Controls.Add(this.loginLbl, 0, 4);
        this.loginTable.Controls.Add(this.passLbl, 0, 6);
        this.loginTable.Controls.Add(this.hostTextBox, 2, 0);
        this.loginTable.Controls.Add(this.dbNameTextBox, 2, 2);
        this.loginTable.Controls.Add(this.loginTextBox, 2, 4);
        this.loginTable.Controls.Add(this.passwordTextBox, 2, 6);
        this.loginTable.Controls.Add(this.nextBtn, 4, 0);
        this.loginTable.Controls.Add(this.warningTable, 0, 8);
        this.loginTable.Dock = System.Windows.Forms.DockStyle.Fill;
        this.loginTable.Location = new System.Drawing.Point(20, 25);
        this.loginTable.Name = "loginTable";
        this.loginTable.RowCount = 9;
        this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
        this.loginTable.Size = new System.Drawing.Size(416, 140);
        this.loginTable.TabIndex = 0;
        // 
        // hostLbl
        // 
        this.hostLbl.AutoSize = true;
        this.hostLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.hostLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.hostLbl.ForeColor = System.Drawing.Color.DarkGreen;
        this.hostLbl.Location = new System.Drawing.Point(0, 0);
        this.hostLbl.Margin = new System.Windows.Forms.Padding(0);
        this.hostLbl.Name = "hostLbl";
        this.hostLbl.Size = new System.Drawing.Size(70, 20);
        this.hostLbl.TabIndex = 0;
        this.hostLbl.Text = "������";
        this.hostLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // dbNameLbl
        // 
        this.dbNameLbl.AutoSize = true;
        this.dbNameLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.dbNameLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.dbNameLbl.ForeColor = System.Drawing.Color.Peru;
        this.dbNameLbl.Location = new System.Drawing.Point(0, 25);
        this.dbNameLbl.Margin = new System.Windows.Forms.Padding(0);
        this.dbNameLbl.Name = "dbNameLbl";
        this.dbNameLbl.Size = new System.Drawing.Size(70, 20);
        this.dbNameLbl.TabIndex = 1;
        this.dbNameLbl.Text = "����";
        this.dbNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // loginLbl
        // 
        this.loginLbl.AutoSize = true;
        this.loginLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.loginLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.loginLbl.ForeColor = System.Drawing.Color.Black;
        this.loginLbl.Location = new System.Drawing.Point(0, 50);
        this.loginLbl.Margin = new System.Windows.Forms.Padding(0);
        this.loginLbl.Name = "loginLbl";
        this.loginLbl.Size = new System.Drawing.Size(70, 20);
        this.loginLbl.TabIndex = 2;
        this.loginLbl.Text = "�����";
        this.loginLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // passLbl
        // 
        this.passLbl.AutoSize = true;
        this.passLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.passLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.passLbl.ForeColor = System.Drawing.Color.Black;
        this.passLbl.Location = new System.Drawing.Point(0, 75);
        this.passLbl.Margin = new System.Windows.Forms.Padding(0);
        this.passLbl.Name = "passLbl";
        this.passLbl.Size = new System.Drawing.Size(70, 20);
        this.passLbl.TabIndex = 3;
        this.passLbl.Text = "������";
        this.passLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // hostTextBox
        // 
        this.hostTextBox.BackColor = System.Drawing.Color.White;
        this.hostTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
        this.hostTextBox.Location = new System.Drawing.Point(75, 0);
        this.hostTextBox.Margin = new System.Windows.Forms.Padding(0);
        this.hostTextBox.MaxLength = 60;
        this.hostTextBox.Name = "hostTextBox";
        this.hostTextBox.Size = new System.Drawing.Size(180, 22);
        this.hostTextBox.TabIndex = 4;
        this.hostTextBox.Enter += new System.EventHandler(this.textBox_Enter);
        // 
        // dbNameTextBox
        // 
        this.dbNameTextBox.BackColor = System.Drawing.Color.White;
        this.dbNameTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
        this.dbNameTextBox.Location = new System.Drawing.Point(75, 25);
        this.dbNameTextBox.Margin = new System.Windows.Forms.Padding(0);
        this.dbNameTextBox.MaxLength = 64;
        this.dbNameTextBox.Name = "dbNameTextBox";
        this.dbNameTextBox.Size = new System.Drawing.Size(180, 22);
        this.dbNameTextBox.TabIndex = 5;
        this.dbNameTextBox.Enter += new System.EventHandler(this.textBox_Enter);
        // 
        // loginTextBox
        // 
        this.loginTextBox.BackColor = System.Drawing.Color.White;
        this.loginTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
        this.loginTextBox.Location = new System.Drawing.Point(75, 50);
        this.loginTextBox.Margin = new System.Windows.Forms.Padding(0);
        this.loginTextBox.MaxLength = 16;
        this.loginTextBox.Name = "loginTextBox";
        this.loginTextBox.Size = new System.Drawing.Size(180, 22);
        this.loginTextBox.TabIndex = 6;
        this.loginTextBox.Enter += new System.EventHandler(this.textBox_Enter);
        // 
        // passwordTextBox
        // 
        this.passwordTextBox.BackColor = System.Drawing.Color.White;
        this.passwordTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
        this.passwordTextBox.Location = new System.Drawing.Point(75, 75);
        this.passwordTextBox.Margin = new System.Windows.Forms.Padding(0);
        this.passwordTextBox.MaxLength = 40;
        this.passwordTextBox.Name = "passwordTextBox";
        this.passwordTextBox.Size = new System.Drawing.Size(180, 22);
        this.passwordTextBox.TabIndex = 7;
        this.passwordTextBox.UseSystemPasswordChar = true;
        // 
        // nextBtn
        // 
        this.nextBtn.BackgroundImage = global::TrackControl.DbPatch.Res.Next;
        this.nextBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
        this.nextBtn.Dock = System.Windows.Forms.DockStyle.Fill;
        this.nextBtn.FlatAppearance.BorderSize = 3;
        this.nextBtn.Location = new System.Drawing.Point(280, 0);
        this.nextBtn.Margin = new System.Windows.Forms.Padding(0);
        this.nextBtn.Name = "nextBtn";
        this.loginTable.SetRowSpan(this.nextBtn, 7);
        this.nextBtn.Size = new System.Drawing.Size(95, 95);
        this.nextBtn.TabIndex = 8;
        this.nextBtn.UseVisualStyleBackColor = true;
        this.nextBtn.Click += new System.EventHandler(this.nextBtn_Click);
        // 
        // warningTable
        // 
        this.warningTable.ColumnCount = 3;
        this.loginTable.SetColumnSpan(this.warningTable, 6);
        this.warningTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this.warningTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
        this.warningTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.warningTable.Controls.Add(this.warningIcon, 1, 0);
        this.warningTable.Controls.Add(this.warningText, 2, 0);
        this.warningTable.Dock = System.Windows.Forms.DockStyle.Fill;
        this.warningTable.Location = new System.Drawing.Point(0, 100);
        this.warningTable.Margin = new System.Windows.Forms.Padding(0);
        this.warningTable.Name = "warningTable";
        this.warningTable.RowCount = 1;
        this.warningTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.warningTable.Size = new System.Drawing.Size(416, 40);
        this.warningTable.TabIndex = 9;
        // 
        // warningIcon
        // 
        this.warningIcon.AutoSize = true;
        this.warningIcon.Dock = System.Windows.Forms.DockStyle.Fill;
        this.warningIcon.Location = new System.Drawing.Point(5, 0);
        this.warningIcon.Margin = new System.Windows.Forms.Padding(0);
        this.warningIcon.Name = "warningIcon";
        this.warningIcon.Size = new System.Drawing.Size(50, 40);
        this.warningIcon.TabIndex = 0;
        // 
        // warningText
        // 
        this.warningText.AutoSize = true;
        this.warningText.Dock = System.Windows.Forms.DockStyle.Fill;
        this.warningText.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.warningText.ForeColor = System.Drawing.Color.DarkRed;
        this.warningText.Location = new System.Drawing.Point(55, 0);
        this.warningText.Margin = new System.Windows.Forms.Padding(0);
        this.warningText.Name = "warningText";
        this.warningText.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
        this.warningText.Size = new System.Drawing.Size(361, 40);
        this.warningText.TabIndex = 1;
        this.warningText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // Welcome
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.BackColor = System.Drawing.Color.Transparent;
        this.BackgroundImage = global::TrackControl.DbPatch.Res.WelcomeBg;
        this.Controls.Add(this.mainTable);
        this.Name = "Welcome";
        this.Size = new System.Drawing.Size(492, 346);
        this.mainTable.ResumeLayout(false);
        this.mainTable.PerformLayout();
        this.textTable.ResumeLayout(false);
        this.textTable.PerformLayout();
        this.loginGroupBox.ResumeLayout(false);
        this.loginTable.ResumeLayout(false);
        this.loginTable.PerformLayout();
        this.warningTable.ResumeLayout(false);
        this.warningTable.PerformLayout();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private System.Windows.Forms.Label mySqlIcon;
    private System.Windows.Forms.TableLayoutPanel textTable;
    private System.Windows.Forms.Label headLbl;
    private System.Windows.Forms.Label textLbl;
    private System.Windows.Forms.GroupBox loginGroupBox;
    private System.Windows.Forms.TableLayoutPanel loginTable;
    private System.Windows.Forms.Label hostLbl;
    private System.Windows.Forms.Label dbNameLbl;
    private System.Windows.Forms.Label loginLbl;
    private System.Windows.Forms.Label passLbl;
    private System.Windows.Forms.TextBox hostTextBox;
    private System.Windows.Forms.TextBox dbNameTextBox;
    private System.Windows.Forms.TextBox loginTextBox;
    private System.Windows.Forms.TextBox passwordTextBox;
    private System.Windows.Forms.Button nextBtn;
    private System.Windows.Forms.TableLayoutPanel warningTable;
    private System.Windows.Forms.Label warningIcon;
    private System.Windows.Forms.Label warningText;
  }
}
