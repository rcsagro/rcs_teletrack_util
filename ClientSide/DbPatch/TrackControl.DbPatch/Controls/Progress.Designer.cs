namespace TrackControl.DbPatch.Controls
{
  partial class Progress
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.mainTable = new System.Windows.Forms.TableLayoutPanel();
      this.progressBar = new System.Windows.Forms.ProgressBar();
      this.headLbl = new System.Windows.Forms.Label();
      this.patchDescriptionLbl = new System.Windows.Forms.Label();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.label1 = new System.Windows.Forms.Label();
      this.mainTable.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // mainTable
      // 
      this.mainTable.BackColor = System.Drawing.Color.Transparent;
      this.mainTable.ColumnCount = 3;
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
      this.mainTable.Controls.Add(this.progressBar, 0, 5);
      this.mainTable.Controls.Add(this.headLbl, 0, 0);
      this.mainTable.Controls.Add(this.patchDescriptionLbl, 0, 1);
      this.mainTable.Controls.Add(this.pictureBox1, 2, 1);
      this.mainTable.Controls.Add(this.label1, 0, 3);
      this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainTable.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.mainTable.Location = new System.Drawing.Point(20, 20);
      this.mainTable.Margin = new System.Windows.Forms.Padding(0);
      this.mainTable.Name = "mainTable";
      this.mainTable.RowCount = 6;
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
      this.mainTable.Size = new System.Drawing.Size(452, 306);
      this.mainTable.TabIndex = 0;
      // 
      // progressBar
      // 
      this.mainTable.SetColumnSpan(this.progressBar, 3);
      this.progressBar.Dock = System.Windows.Forms.DockStyle.Fill;
      this.progressBar.Location = new System.Drawing.Point(0, 256);
      this.progressBar.Margin = new System.Windows.Forms.Padding(0);
      this.progressBar.Name = "progressBar";
      this.progressBar.Size = new System.Drawing.Size(452, 50);
      this.progressBar.Step = 1;
      this.progressBar.TabIndex = 0;
      // 
      // headLbl
      // 
      this.headLbl.AutoSize = true;
      this.mainTable.SetColumnSpan(this.headLbl, 3);
      this.headLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.headLbl.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.headLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(58)))), ((int)(((byte)(57)))));
      this.headLbl.Location = new System.Drawing.Point(0, 0);
      this.headLbl.Margin = new System.Windows.Forms.Padding(0);
      this.headLbl.Name = "headLbl";
      this.headLbl.Size = new System.Drawing.Size(452, 30);
      this.headLbl.TabIndex = 1;
      this.headLbl.Text = "����������� ����������...";
      // 
      // patchDescriptionLbl
      // 
      this.patchDescriptionLbl.AutoSize = true;
      this.patchDescriptionLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.patchDescriptionLbl.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.patchDescriptionLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(47)))), ((int)(((byte)(61)))));
      this.patchDescriptionLbl.Location = new System.Drawing.Point(0, 30);
      this.patchDescriptionLbl.Margin = new System.Windows.Forms.Padding(0);
      this.patchDescriptionLbl.Name = "patchDescriptionLbl";
      this.patchDescriptionLbl.Size = new System.Drawing.Size(332, 186);
      this.patchDescriptionLbl.TabIndex = 2;
      this.patchDescriptionLbl.Text = "�������������� ���������� ��������� �� MyISAM � InnoDB.";
      this.patchDescriptionLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pictureBox1
      // 
      this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pictureBox1.Image = global::TrackControl.DbPatch.Res.Animation;
      this.pictureBox1.Location = new System.Drawing.Point(352, 30);
      this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(100, 186);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pictureBox1.TabIndex = 3;
      this.pictureBox1.TabStop = false;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label1.ForeColor = System.Drawing.Color.Black;
      this.label1.Location = new System.Drawing.Point(0, 231);
      this.label1.Margin = new System.Windows.Forms.Padding(0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(332, 20);
      this.label1.TabIndex = 4;
      this.label1.Text = "��� ����������";
      this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // Progress
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImage = global::TrackControl.DbPatch.Res.ProgressBg;
      this.Controls.Add(this.mainTable);
      this.Margin = new System.Windows.Forms.Padding(0);
      this.Name = "Progress";
      this.Padding = new System.Windows.Forms.Padding(20);
      this.Size = new System.Drawing.Size(492, 346);
      this.mainTable.ResumeLayout(false);
      this.mainTable.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private System.Windows.Forms.ProgressBar progressBar;
    private System.Windows.Forms.Label headLbl;
    private System.Windows.Forms.Label patchDescriptionLbl;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Label label1;
  }
}
