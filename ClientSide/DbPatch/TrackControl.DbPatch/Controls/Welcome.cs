using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace TrackControl.DbPatch.Controls
{
  [ToolboxItem(false)]
  public partial class Welcome : UserControl
  {
    #region Events
    /// <summary>
    /// �������, ����������� ����� ��������� ����� �� ������ "�����".
    /// </summary>
    public event EventHandler<InputEventArgs> Next;
    #endregion

    #region .ctor
    /// <summary>
    /// �����������.
    /// </summary>
    public Welcome()
    {
      InitializeComponent();
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// ���������� ������������ � ������������� ����������� � ��������������
    /// ��������������� ��� ������� ������.
    /// </summary>
    public void ShowNoConnectionWarning()
    {
      this.warningIcon.Image = Res.NoConnection;
      this.warningText.Text = "�������� ������. ��� ����������.";
      this.warningTable.Visible = true;
    }
    /// <summary>
    /// ���������� ������������ �� ���������� ����������� ���� ��� ���������� �����.
    /// </summary>
    public void ShowNoPrivilegesWarning()
    {
      this.warningIcon.Image = Res.NoPrivileges;
      this.warningText.Text = "������������ ���� ��� ���������� �����.";
      this.warningTable.Visible = true;
    }
    #endregion

    #region GUI Event Handlers
    /// <summary>
    /// ������������ ���� �� ������ "�����".
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void nextBtn_Click(object sender, EventArgs e)
    {
      if (!validate()) return;

      onNext();
    }
    /// <summary>
    /// ������������ ��������� ������ ����� ��� �����.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void textBox_Enter(object sender, EventArgs e)
    {
      clearWarningStatus((TextBox)sender);
      hideErrorMessage();
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// ���������� ������� Next.
    /// </summary>
    private void onNext()
    {
      InputEventArgs inputEventArgs = new InputEventArgs(
        this.hostTextBox.Text.Trim(),
        this.dbNameTextBox.Text.Trim(),
        this.loginTextBox.Text.Trim(),
        this.passwordTextBox.Text);

      if (Next != null)
        Next(this, inputEventArgs);
    }
    /// <summary>
    /// ��������� ������������ ��������� ������������� ������.
    /// </summary>
    private bool validate()
    {
      bool result = true;
      if (this.hostTextBox.Text.Trim().Length == 0 || this.hostTextBox.TextAlign != HorizontalAlignment.Left)
      {
        setWarningStatus(this.hostTextBox);
        result = false;
      }
      if (this.dbNameTextBox.Text.Trim().Length == 0 || this.dbNameTextBox.TextAlign != HorizontalAlignment.Left)
      {
        setWarningStatus(this.dbNameTextBox);
        result = false;
      }
      if (this.loginTextBox.Text.Trim().Length == 0 || this.loginTextBox.TextAlign != HorizontalAlignment.Left)
      {
        setWarningStatus(this.loginTextBox);
        result = false;
      }
      return result;
    }
    /// <summary>
    /// ������������� ���� ��� ����� ����� ������.
    /// </summary>
    /// <param name="textBox">���� ��� �����</param>
    private void setWarningStatus(TextBox textBox)
    {
      textBox.BackColor = Color.FromArgb(255, 242, 232);
      textBox.TextAlign = HorizontalAlignment.Center;
      textBox.ForeColor = Color.DarkRed;
      textBox.Text = "���������� ���������";
    }
    /// <summary>
    /// ������������� ���� ��� ����� ���������� �����.
    /// </summary>
    /// <param name="textBox">���� ��� �����</param>
    private void clearWarningStatus(TextBox textBox)
    {
      textBox.BackColor = System.Drawing.Color.White;
      textBox.TextAlign = HorizontalAlignment.Left;
      textBox.ForeColor = Color.Black;
      textBox.Text = "";
    }
    /// <summary>
    /// ������ ��������� �� ������.
    /// </summary>
    private void hideErrorMessage()
    {      
      this.warningTable.Visible = false;
    }
    #endregion
  }
}
