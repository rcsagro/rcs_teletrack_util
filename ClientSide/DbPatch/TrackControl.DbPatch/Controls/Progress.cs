using System.ComponentModel;
using System.Windows.Forms;

namespace TrackControl.DbPatch.Controls
{
  [ToolboxItem(false)]
  public partial class Progress : UserControl
  {
    delegate void ShowScriptBeginDelegate(string description);
    delegate void ShowScriptEndDelegate();

    #region .ctor
    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="scriptsCount"></param>
    public Progress(int scriptsCount)
    {
      InitializeComponent();

      this.progressBar.Maximum = scriptsCount;
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// ���������� �������� ����������, ������� ����������
    /// � ������� ������ �������.
    /// </summary>
    /// <param name="description">�������� ����������</param>
    public void ShowScriptBegin(string description)
    {
      if (this.InvokeRequired)
        this.Invoke(new ShowScriptBeginDelegate(ShowScriptBegin), description);
      else
        this.patchDescriptionLbl.Text = description;
      Application.DoEvents();
    }

    public void ShowScriptEnd()
    {
      if (this.InvokeRequired)
        this.Invoke(new ShowScriptEndDelegate(ShowScriptEnd));
      else
        this.progressBar.Increment(1);
    }
    #endregion
  }
}
