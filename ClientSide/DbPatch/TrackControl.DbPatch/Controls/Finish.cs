using System;
using System.Windows.Forms;

namespace TrackControl.DbPatch.Controls
{
  public partial class Finish : UserControl
  {
    #region .ctor
    /// <summary>
    /// �����������.
    /// </summary>
    public Finish()
    {
      InitializeComponent();
    }
    #endregion

    #region Events
    /// <summary>
    /// �������, ����������� ����� ��������� ����� �� ������ "�����".
    /// </summary>
    public event EventHandler Exit;
    #endregion

    #region Private Methods
    /// <summary>
    /// ������������ ���� �� ������ "�����".
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void exitBtn_Click(object sender, EventArgs e)
    {
      if (Exit != null)
        Exit(this, EventArgs.Empty);
    }
    #endregion
  }
}
