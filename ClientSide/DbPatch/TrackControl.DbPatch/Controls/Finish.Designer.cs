namespace TrackControl.DbPatch.Controls
{
  partial class Finish
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.mainTable = new System.Windows.Forms.TableLayoutPanel();
      this.finishTextLbl = new System.Windows.Forms.Label();
      this.exitTable = new System.Windows.Forms.TableLayoutPanel();
      this.exitBtn = new System.Windows.Forms.Button();
      this.mainTable.SuspendLayout();
      this.exitTable.SuspendLayout();
      this.SuspendLayout();
      // 
      // mainTable
      // 
      this.mainTable.BackColor = System.Drawing.Color.Transparent;
      this.mainTable.ColumnCount = 3;
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 351F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.mainTable.Controls.Add(this.finishTextLbl, 1, 1);
      this.mainTable.Controls.Add(this.exitTable, 0, 2);
      this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainTable.Location = new System.Drawing.Point(0, 0);
      this.mainTable.Margin = new System.Windows.Forms.Padding(0);
      this.mainTable.Name = "mainTable";
      this.mainTable.RowCount = 4;
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
      this.mainTable.Size = new System.Drawing.Size(492, 346);
      this.mainTable.TabIndex = 0;
      // 
      // finishTextLbl
      // 
      this.finishTextLbl.AutoSize = true;
      this.finishTextLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.finishTextLbl.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.finishTextLbl.ForeColor = System.Drawing.Color.Black;
      this.finishTextLbl.Location = new System.Drawing.Point(70, 241);
      this.finishTextLbl.Margin = new System.Windows.Forms.Padding(0);
      this.finishTextLbl.Name = "finishTextLbl";
      this.finishTextLbl.Size = new System.Drawing.Size(351, 50);
      this.finishTextLbl.TabIndex = 0;
      this.finishTextLbl.Text = "���� ������ ������� ������������ ��� ������ � ����������� TrackControl";
      this.finishTextLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // exitTable
      // 
      this.exitTable.ColumnCount = 3;
      this.mainTable.SetColumnSpan(this.exitTable, 3);
      this.exitTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.exitTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
      this.exitTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
      this.exitTable.Controls.Add(this.exitBtn, 1, 0);
      this.exitTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.exitTable.Location = new System.Drawing.Point(0, 291);
      this.exitTable.Margin = new System.Windows.Forms.Padding(0);
      this.exitTable.Name = "exitTable";
      this.exitTable.RowCount = 1;
      this.exitTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.exitTable.Size = new System.Drawing.Size(492, 40);
      this.exitTable.TabIndex = 1;
      // 
      // exitBtn
      // 
      this.exitBtn.BackgroundImage = global::TrackControl.DbPatch.Res.BtnBg;
      this.exitBtn.Cursor = System.Windows.Forms.Cursors.Hand;
      this.exitBtn.Dock = System.Windows.Forms.DockStyle.Fill;
      this.exitBtn.Font = new System.Drawing.Font("Arial", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.exitBtn.ForeColor = System.Drawing.Color.Navy;
      this.exitBtn.Location = new System.Drawing.Point(299, 0);
      this.exitBtn.Margin = new System.Windows.Forms.Padding(0);
      this.exitBtn.Name = "exitBtn";
      this.exitBtn.Size = new System.Drawing.Size(180, 40);
      this.exitBtn.TabIndex = 0;
      this.exitBtn.Text = "�����";
      this.exitBtn.UseVisualStyleBackColor = true;
      this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
      // 
      // Finish
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImage = global::TrackControl.DbPatch.Res.FinishBg;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.Controls.Add(this.mainTable);
      this.DoubleBuffered = true;
      this.Margin = new System.Windows.Forms.Padding(0);
      this.Name = "Finish";
      this.Size = new System.Drawing.Size(492, 346);
      this.mainTable.ResumeLayout(false);
      this.mainTable.PerformLayout();
      this.exitTable.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private System.Windows.Forms.Label finishTextLbl;
    private System.Windows.Forms.TableLayoutPanel exitTable;
    private System.Windows.Forms.Button exitBtn;
  }
}
