﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DbPatch")]
[assembly: AssemblyDescription("Обновление базы данных для корректной работы системы TrackControl")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyCopyright("Copyright © RCS 2008-2011")]
[assembly: AssemblyTrademark("AVS")]
[assembly: AssemblyCompany("RCS Ltd")]
[assembly: AssemblyProduct("Система Teletrack")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dafd415f-155a-493c-b5ad-b6b9d55cb75b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyInformationalVersion("2.0")]
