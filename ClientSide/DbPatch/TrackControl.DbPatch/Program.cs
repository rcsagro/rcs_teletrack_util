using System;
using System.Windows.Forms;

namespace TrackControl.DbPatch
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      PatchManager manager = new PatchManager(new MainForm());
    }
  }
}