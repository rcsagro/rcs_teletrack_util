#!/usr/local/bin/python
# -*- coding: utf-8 -*-

class ViewerTab(object):
    def __init__(self):
        self._sensor = ["AIN 1", "AIN 2", "CAN 1 (Ch0,Addr:01234567,St:0,Len:16)", "CAN 2 (Ch0,Addr:01234567,St:0,Len:16)",
                        "CAN 3 (Ch0,Addr:01234567,St:0,Len:16)", "COUNT 1 (Count)", "COUNT 2 (Freq.)", "COUNT 3 (Din)", "DIN",
                        "RS485 1 (LLS.St:0,Len:16)", "RS485 2 (LLS.St:0,Len:16)", "RS485 3 (LLS.St:0,Len:16)",
                        "RS485 4 (LLS.St:0,Len:16)", "RS485 5 (LLS.St:0,Len:16)", "RS485 6 (LLS.St:0,Len:16)", "RS485 7 (LLS.St:0,Len:16)"]
        self._sensor_value = ["","","","","","","","","","","","","","","",""]
        self.linerssi = ""
        self.linesatt = ""
        self.linevbort = ""
        self.checkrequest = False
        return

    @property
    def SensorTable(self): return self._sensor

    @property
    def SensorValue(self): return  self._sensor_value

    @SensorValue.setter
    def SensorValue(self, value): self._sensor_value = value

    @property
    def LineRssi(self): return self.linerssi

    @LineRssi.setter
    def LineRssi(self, value): self.linerssi = value

    @property
    def LineSatt(self): return self.linesatt

    @LineSatt.setter
    def LineSatt(self, value): self.linesatt = value

    @property
    def LineVBort(self): return self.linevbort

    @LineVBort.setter
    def LineVBort(self, value): self.linevbort = value

    @property
    def CheckRequest(self): return self.checkrequest

    @CheckRequest.setter
    def CheckRequest(self, value): self.checkrequest = value