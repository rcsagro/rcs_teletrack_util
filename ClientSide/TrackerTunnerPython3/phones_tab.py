#!/usr/local/bin/python
# -*- coding: utf-8 -*-

class PhonesParam(object):
    def __init__(self):
        self.phones_param = ["Номер SOS", "Номер телефона диспетчера", "Разрешенный номер 1", "Разрешенный номер 2",
                             "Разрешенный номер 2", "Телефон обратной связи"]
        self.parameter_value = ["", "", "", "", "", ""]
        return

    @property
    def PhonesParam(self): return self.phones_param

    @property
    def ParameterValue(self): return self.parameter_value

    def setParameterValue(self, value, index):
        self.parameter_value[index] = value
        return

class PhonesValue(object):
    def __init__(self):
        self.phones_param = ["RCSB", "Телефон", "Телефон", "Телефон", "Телефон", "Телефон", "Телефон", "Телефон", "Телефон",
                             "RCS3", "RCS2", "RCS1", "RCSB2", "Телефон", "RCS"]
        self.parameter_value = ["80672452724", "22222222222", "33333333333", "44444444444", "55555555555", "66666666666",
                                "77777777777", "88888888888", "99999999999", "80637562678", "80933222277", "80637562677",
                                "80672452734", "1", "No Data"]
        return

    @property
    def PhonesParam(self): return self.phones_param

    @property
    def ParameterValue(self): return self.parameter_value

    def setParameterValue(self, value, index):
        if index < len(self.parameter_value):
            self.parameter_value[index] = value
        else:
            self.parameter_value.append(value)
        self.position = index
        return

    def setNameValue(self, value, index):
        if index < len(self.parameter_value):
            self.phones_param[index] = value
        else:
            self.phones_param.append(value)
        self.position = index
        return

    def truncatePhones(self):
        indx = self.position + 1
        if indx < len(self.parameter_value):
            del self.phones_param[indx:]
            del self.parameter_value[indx:]
        return