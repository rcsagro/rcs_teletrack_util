# -*- coding: utf-8 -*-

import time
import os
import copy
import sys
import traceback
import xlwt
import datetime
import sqlite3

from os.path import abspath
from datetime import datetime
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import QIcon, QTextCharFormat
from PyQt5.QtCore import QSize, QDate, Qt
from PyQt5.QtCore import pyqtSlot, pyqtSignal, QTime, QTimer, QDate, QDateTime
from PyQt5.QtWidgets import QDateTimeEdit, QStackedWidget, QListWidget, QSpinBox, QFileDialog, QTextEdit, QCalendarWidget, QMainWindow, QComboBox, QDialog, QCheckBox, \
    QTableView, QTableWidget, QTableWidgetItem, QHBoxLayout, QPushButton, QLineEdit, QLabel, QGridLayout, QTabWidget, \
    QWidget, qApp, QAction, QDesktopWidget, QMessageBox, QVBoxLayout, QProgressBar, QTimeEdit, QCompleter, QFrame, \
    QSplitter, QDateEdit

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.OK_MSG = 1024
        self.setMinimumSize(QSize(1000, 700))
        self.showMaximized()
        self.setWindowTitle(self.tr("Report Generator"))
        self.setWindowIcon(QIcon('web.png'))
        self.statusBar().showMessage('Ready')
        opdbAction = QAction(self.tr('&Open database...'), self)
        opdbAction.setShortcut('Ctrl+O')
        opdbAction.setStatusTip('Open sqlite database')
        opdbAction.triggered.connect(self.OnOpenDb)
        exitAction = QAction(self.tr('&Exit'), self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Close this application')
        exitAction.triggered.connect(self.closed)
        prefAction = QAction(self.tr('&Parameters'), self)
        prefAction.setShortcut('Ctrl+P')
        prefAction.setStatusTip('Parameters this application')
        prefAction.triggered.connect(self.showParameters)
        aboutAction = QAction(self.tr('&About'), self)
        aboutAction.setShortcut('Ctrl+A')
        aboutAction.setStatusTip(self.tr('About this application'))
        aboutAction.triggered.connect(self.showAbout)
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(opdbAction)
        fileMenu.addAction(exitAction)
        fileMenu = menubar.addMenu('&Edit')
        fileMenu.addAction(prefAction)
        fileMenu = menubar.addMenu('&Help')
        fileMenu.addAction(aboutAction)

        self.centralWidget = QWidget(self)
        self.setCentralWidget(self.centralWidget)

        self.path2Database = ""
        self.connSqlite3 = ""
        self.codeTickets = []
        self.pawerTable = []
        self.oplatyTable = []
        self.operations = []
        self.codeMoney = []
        self.persones = []
        self.casher = []
        self.counters = []
        self.codeReport = []
        self.searchColumn = 0
        self.searchColumnOplaty = 0
        self.searchRow = 0
        self.searchRowOplaty = 0
        self.listNewRecords = []
        self.IndexRow = 0
        self.listUpdateRecords = []
        self.listUpdateRecordsPerson = []
        self.listNewRecords = []
        self.listNewRecordsPerson = []
        self.maxOplatyID = -1
        self.searchRowPersone1 = 0
        self.searchColumnPersone1 = 0
        self.searchRowPersone = 0
        self.searchColumnPersone = 0
        self.listNewRecordsOperat = []
        self.listUpdateRecordsOperat = []
        self.maxOperationID = -1
        self.maxPowersID = -1
        self.IndexRowPow = 0
        self.listNewRecordsPower = []
        self.listUpdateRecordsPower = []
        self.listUpdateViewPerson = []
        self.listUpdateViewLister = []
        self.listUpdateViewPower = []
        self.listUpdateViewOperat = []
        self.listUpdateViewRecords = []
        self.searchRowPower = 0
        self.searchColumnPower = 0
        self.maxListerID = -1
        self.IndexRowLister = 0
        self.listNewRecordsLister = []
        self.listUpdateRecordsLister = []
        self.searchRowLister = 0
        self.searchColumnLister = 0
        self.currentName = "Неизвестный"
        self.lastPowerItem = ''
        self.summaTickets = ''

        self.labelDataFrom = QLabel("Дата от: ")
        self.labelDataTo = QLabel(" - до - ")
        self.dateFrom = QDateTimeEdit(self)
        self.dateFrom.setDisplayFormat("yyyy-MM-dd")
        self.dateFrom.setDate(QDate.currentDate())
        self.dateFrom.setCalendarPopup(True)
        self.dateTo = QDateTimeEdit(self)
        self.dateTo.setDisplayFormat("yyyy-MM-dd")
        self.dateTo.setDate(QDate.currentDate())
        self.dateTo.setCalendarPopup(True)
        self.labelTypeTick = QLabel("Тип квитанции: ")
        self.typeTickCombo = QComboBox(self)
        self.typeTickCombo.setEditable(False)
        self.typeTickCombo.currentIndexChanged.connect(self.OnClickTicket)
        self.typeTickCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.btnExport = QPushButton("Експорт")
        self.btnExport.setToolTip("Експортировать данные квитанций")
        self.btnExport.clicked.connect(self.OnExportTick)
        self.filtCombo = QComboBox(self)
        self.filtCombo.setEditable(False)
        self.filtCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.filtCombo.setToolTip("Выберите клонку фильтрации")
        self.lineFilt = QLineEdit()
        self.lineFilt.setToolTip("Наберите выражение фильтрации")
        self.lineFilt.textChanged.connect(self.setFilterIdentifier)
        self.labelFiltLine = QLabel("Фильтр:")
        self.labelSummaTick = QLabel("Сумма:")
        self.lineSummaTick = QLineEdit()
        self.lineSummaTick.setReadOnly(True)
        self.lineSummaTick.setToolTip("Общая сумма")
        self.labelNumberRow = QLabel("Всего строк:")
        self.lineNumRow = QLineEdit()
        self.lineNumRow.setReadOnly(True)
        self.lineNumRow.setToolTip("Число строк в таблице")
        self.labelSearch = QLabel("Поиск:")
        self.lineSearch = QLineEdit()
        self.lineSearch.setToolTip("Наберите выражение для поиска")
        self.btnSearch = QPushButton("Поиск")
        self.btnSearch.setToolTip("Искать данные квитанций")
        self.btnSearch.clicked.connect(self.OnSearchTick)

        widgetup = QWidget(self)
        hlayUp = QHBoxLayout(self)
        hlayUp.addWidget(self.labelDataFrom)
        hlayUp.addWidget(self.dateFrom)
        hlayUp.addWidget(self.labelDataTo)
        hlayUp.addWidget(self.dateTo)
        hlayUp.addWidget(self.labelTypeTick)
        hlayUp.addWidget(self.typeTickCombo)
        hlayUp.addWidget(self.btnExport)
        widgetup.setLayout(hlayUp)

        widgetline = QWidget(self)
        hlayline = QHBoxLayout(self)
        hlayline.addWidget(self.labelSummaTick)
        hlayline.addWidget(self.lineSummaTick)
        hlayline.addWidget(self.labelNumberRow)
        hlayline.addWidget(self.lineNumRow)
        hlayline.addWidget(self.labelFiltLine)
        hlayline.addWidget(self.filtCombo)
        hlayline.addWidget(self.lineFilt)
        hlayline.addWidget(self.labelSearch)
        hlayline.addWidget(self.lineSearch)
        hlayline.addWidget(self.btnSearch)
        widgetline.setLayout(hlayline)

        self.tableTickets = QTableWidget(self)
        self.tableTickets.setColumnCount(8)
        self.columnTickets = ["ID", "Код квитанции", "Название операции", "Сумма", "Валюта", "Отчетность", "Дата", "Примечание"]
        self.filtCombo.addItems(["Код квитанции", "Название операции", "Сумма", "Валюта", "Отчетность", "Дата", "Примечание"])

        self.tableTickets.setHorizontalHeaderLabels(self.columnTickets)
        self.tableTickets.horizontalHeaderItem(0).setToolTip("ID")
        self.tableTickets.horizontalHeaderItem(1).setToolTip("Код квитанции")
        self.tableTickets.horizontalHeaderItem(2).setToolTip("Название операции")
        self.tableTickets.horizontalHeaderItem(3).setToolTip("Значение суммы")
        self.tableTickets.horizontalHeaderItem(4).setToolTip("Обозначение валюты")
        self.tableTickets.horizontalHeaderItem(5).setToolTip("Вид отчетности")
        self.tableTickets.horizontalHeaderItem(6).setToolTip("Дата операции")
        self.tableTickets.horizontalHeaderItem(7).setToolTip("Примечание к операции")
        self.tableTickets.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTickets.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTickets.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTickets.horizontalHeaderItem(3).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTickets.horizontalHeaderItem(4).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTickets.horizontalHeaderItem(5).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTickets.horizontalHeaderItem(6).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTickets.horizontalHeaderItem(7).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTickets.resizeColumnsToContents()
        self.tableTickets.resizeRowsToContents()
        self.tableTickets.setSortingEnabled(True)
        self.tableTickets.setSelectionBehavior(QTableView.SelectRows)

        self.labelDataFromOplaty = QLabel("Дата от: ")
        self.labelDataToOplaty = QLabel(" - до - ")
        self.dateFromOplaty = QDateTimeEdit(self)
        self.dateFromOplaty.setDisplayFormat("yyyy-MM-dd")
        self.dateFromOplaty.setDate(QDate.currentDate())
        self.dateFromOplaty.setCalendarPopup(True)
        self.dateToOplaty = QDateTimeEdit(self)
        self.dateToOplaty.setDisplayFormat("yyyy-MM-dd")
        self.dateToOplaty.setDate(QDate.currentDate())
        self.dateToOplaty.setCalendarPopup(True)
        self.labelFiltOplaty = QLabel("Фильтр:")
        self.labelSearchOplaty = QLabel("Поиск:")
        self.btnReadOplaty = QPushButton("Получить")
        self.btnReadOplaty.setToolTip("Прочитать квитанции с базы")
        self.btnReadOplaty.clicked.connect(self.OnReadOplaty)
        self.lineSearchOplaty = QLineEdit()
        self.lineSearchOplaty.setToolTip("Наберите выражение для поиска")
        self.btnSearchOplaty = QPushButton("Поиск")
        self.btnSearchOplaty.setToolTip("Искать данные квитанций")
        self.btnSearchOplaty.clicked.connect(self.OnSearchOplaty)
        self.lineFiltOplaty = QLineEdit()
        self.lineFiltOplaty.setToolTip("Наберите выражение фильтрации")
        self.lineFiltOplaty.textChanged.connect(self.setFilterOplaty)
        self.filtComboOplaty = QComboBox(self)
        self.filtComboOplaty.setEditable(False)
        self.filtComboOplaty.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.filtComboOplaty.setToolTip("Выберите клонку фильтрации")
        self.btnExportOplaty = QPushButton("Експорт")
        self.btnExportOplaty.setToolTip("Експортировать данные квитанций")
        self.btnExportOplaty.clicked.connect(self.OnExportOplaty)
        self.labelSummaOplaty = QLabel("Сумма:")
        self.lineSummaOplaty = QLineEdit()
        self.lineSummaOplaty.setReadOnly(True)
        self.lineSummaOplaty.setToolTip("Общая сумма")
        self.tableOplaty = QTableWidget(self)
        self.tableComplex = QTableWidget(self)
        self.tableOplaty.setColumnCount(6)
        self.columnOplaty = ["ID", "Название операции", "Сумма", "Валюта", "Дата", "Примечание"]
        self.filtComboOplaty.addItems(["Название операции", "Сумма", "Валюта", "Дата", "Примечание"])
        self.tableOplaty.setHorizontalHeaderLabels(self.columnOplaty)
        self.tableOplaty.horizontalHeaderItem(0).setToolTip("ID")
        self.tableOplaty.horizontalHeaderItem(1).setToolTip("Название операции")
        self.tableOplaty.horizontalHeaderItem(2).setToolTip("Сумма в заданной валюте")
        self.tableOplaty.horizontalHeaderItem(3).setToolTip("Название валюты")
        self.tableOplaty.horizontalHeaderItem(4).setToolTip("Дата операции")
        self.tableOplaty.horizontalHeaderItem(5).setToolTip("Примечание к операции")
        self.tableOplaty.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOplaty.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOplaty.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOplaty.horizontalHeaderItem(3).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOplaty.horizontalHeaderItem(4).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOplaty.horizontalHeaderItem(5).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOplaty.resizeColumnsToContents()
        self.tableOplaty.resizeRowsToContents()
        self.tableOplaty.setSortingEnabled(True)
        self.tableOplaty.setSelectionBehavior(QTableView.SelectRows)
        self.tableOplaty.clicked.connect(self.viewClicked)

        self.labelDataFromComplex = QLabel("Дата от: ")
        self.labelDataToComplex = QLabel(" - до - ")
        self.dateFromComplex = QDateTimeEdit(self)
        self.dateFromComplex.setDisplayFormat("yyyy-MM-dd")
        self.dateFromComplex.setDate(QDate.currentDate())
        self.dateFromComplex.setCalendarPopup(True)
        self.dateToComplex = QDateTimeEdit(self)
        self.dateToComplex.setDisplayFormat("yyyy-MM-dd")
        self.dateToComplex.setDate(QDate.currentDate())
        self.dateToComplex.setCalendarPopup(True)
        self.labelBeginerSumma = QLabel("Начальная сумма бюджета: ")
        self.lineBeginerComplex = QLineEdit()
        self.lineBeginerComplex.setToolTip("Комплексная строка начального значения бюджета")
        self.btnGenerComplex = QPushButton("Сгенерировать")
        self.btnGenerComplex.setToolTip("Сгенерировать комплексный отчет")
        self.btnGenerComplex.clicked.connect(self.OnGenerateComplex)
        self.btnExportComplex = QPushButton("Експорт")
        self.btnExportComplex.setToolTip("Експортировать комплексный отчет в XLS")
        self.btnExportComplex.clicked.connect(self.OnExporterComplex)

        self.btnOplatyAdd = QPushButton("Добавить")
        self.btnOplatyAdd.setToolTip("Добавить новую строку данных")
        self.btnOplatyAdd.clicked.connect(self.OnOplatyAdd)
        self.btnOplatyAdd.setEnabled(False)
        self.btnOplatyCancel = QPushButton("Отклонить")
        self.btnOplatyCancel.setToolTip("Отклонить изменения")
        self.btnOplatyCancel.clicked.connect(self.OnOplatyCancel)
        self.btnOplatyCancel.setEnabled(False)
        self.btnOplatySave = QPushButton("Сохранить")
        self.btnOplatySave.setToolTip("Сохранить изменения")
        self.btnOplatySave.clicked.connect(self.OnOplatySave)
        self.btnOplatySave.setEnabled(False)
        self.btnOplatyDelete = QPushButton("Удалить")
        self.btnOplatyDelete.setToolTip("Удалить данные")
        self.btnOplatyDelete.clicked.connect(self.OnOplatyDelete)
        self.btnOplatyDelete.setEnabled(False)

        self.labelFiltPersone = QLabel("Фильтр:")
        self.labelSearchPersone = QLabel("Поиск:")
        self.lineSearchPersone = QLineEdit()
        self.lineSearchPersone.setToolTip("Наберите выражение для поиска")
        self.btnSearchPersone = QPushButton("Поиск")
        self.btnSearchPersone.setToolTip("Искать данные пользователей")
        self.btnSearchPersone.clicked.connect(self.OnSearchPersone)
        self.lineFiltPersone = QLineEdit()
        self.lineFiltPersone.setToolTip("Наберите выражение фильтрации")
        self.lineFiltPersone.textChanged.connect(self.setFilterPersone)
        self.filtComboPersone = QComboBox(self)
        self.filtComboPersone.setEditable(False)
        self.filtComboPersone.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.filtComboPersone.setToolTip("Выберите клонку фильтрации")
        self.btnExportPersone = QPushButton("Експорт")
        self.btnExportPersone.setToolTip("Експортировать данные таблицы")
        self.btnExportPersone.clicked.connect(self.OnExportPersone)
        self.tablePerson = QTableWidget(self)
        self.tablePerson.setColumnCount(3)
        self.columnPersones = ["ID", "ФИО", "Примечание"]
        self.filtComboPersone.addItems(["ФИО", "Примечание"])
        self.tablePerson.setHorizontalHeaderLabels(self.columnPersones)
        self.tablePerson.horizontalHeaderItem(0).setToolTip("ID")
        self.tablePerson.horizontalHeaderItem(1).setToolTip("ФИО")
        self.tablePerson.horizontalHeaderItem(2).setToolTip("Примечание к операции")
        self.tablePerson.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePerson.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePerson.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePerson.resizeColumnsToContents()
        self.tablePerson.resizeRowsToContents()
        self.tablePerson.setSortingEnabled(True)
        self.tablePerson.setSelectionBehavior(QTableView.SelectRows)
        self.tablePerson.clicked.connect(self.viewClickedPerson)
        self.btnPersonRead = QPushButton("Прочитать")
        self.btnPersonRead.setToolTip("Прочитать данные из базы")
        self.btnPersonRead.clicked.connect(self.OnPersonRead)
        self.btnPersonAdd = QPushButton("Добавить")
        self.btnPersonAdd.setToolTip("Добавить новую строку данных")
        self.btnPersonAdd.clicked.connect(self.OnPersonAdd)
        self.btnPersonAdd.setEnabled(False)
        self.btnPersonCancel = QPushButton("Отклонить")
        self.btnPersonCancel.setToolTip("Отклонить изменения")
        self.btnPersonCancel.clicked.connect(self.OnPersonCancel)
        self.btnPersonCancel.setEnabled(False)
        self.btnPersonSave = QPushButton("Сохранить")
        self.btnPersonSave.setToolTip("Сохранить изменения")
        self.btnPersonSave.clicked.connect(self.OnPersonSave)
        self.btnPersonSave.setEnabled(False)
        self.btnPersonDelete = QPushButton("Удалить")
        self.btnPersonDelete.setToolTip("Удалить данные")
        self.btnPersonDelete.clicked.connect(self.OnPersonDelete)
        self.btnPersonDelete.setEnabled(False)

        self.labelFiltPersone1 = QLabel("Фильтр:")
        self.labelSearchPersone1 = QLabel("Поиск:")
        self.lineSearchPersone1 = QLineEdit()
        self.lineSearchPersone1.setToolTip("Наберите выражение для поиска")
        self.btnSearchPersone1 = QPushButton("Поиск")
        self.btnSearchPersone1.setToolTip("Искать данные пользователей")
        self.btnSearchPersone1.clicked.connect(self.OnSearchPersone1)
        self.lineFiltPersone1 = QLineEdit()
        self.lineFiltPersone1.setToolTip("Наберите выражение фильтрации")
        self.lineFiltPersone1.textChanged.connect(self.setFilterPersone1)
        self.filtComboPersone1 = QComboBox(self)
        self.filtComboPersone1.setEditable(False)
        self.filtComboPersone1.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.filtComboPersone1.setToolTip("Выберите клонку фильтрации")
        self.btnExportPersone1 = QPushButton("Експорт")
        self.btnExportPersone1.setToolTip("Експортировать данные таблицы")
        self.btnExportPersone1.clicked.connect(self.OnExportPersone1)
        self.tableOperat = QTableWidget(self)
        self.tableOperat.setColumnCount(9)
        self.columnOperat = ["ID", "Операция", "Раздел", "Квитанция", "Назначение", "Отчетность", "Тематика", "Дата", "Комментарий"]
        self.filtComboPersone1.addItems(["Операция", "Раздел", "Квитанция", "Назначение", "Отчетность", "Тематика", "Дата", "Комментарий"])
        self.tableOperat.setHorizontalHeaderLabels(self.columnOperat)
        self.tableOperat.horizontalHeaderItem(0).setToolTip("ID")
        self.tableOperat.horizontalHeaderItem(1).setToolTip("Операция")
        self.tableOperat.horizontalHeaderItem(2).setToolTip("Раздел")
        self.tableOperat.horizontalHeaderItem(3).setToolTip("Квитанция")
        self.tableOperat.horizontalHeaderItem(4).setToolTip("Назначение")
        self.tableOperat.horizontalHeaderItem(5).setToolTip("Отчетность")
        self.tableOperat.horizontalHeaderItem(6).setToolTip("Тематика для отчета")
        self.tableOperat.horizontalHeaderItem(7).setToolTip("Дата")
        self.tableOperat.horizontalHeaderItem(8).setToolTip("Комментарий")
        self.tableOperat.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOperat.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOperat.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOperat.horizontalHeaderItem(3).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOperat.horizontalHeaderItem(4).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOperat.horizontalHeaderItem(5).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOperat.horizontalHeaderItem(6).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOperat.horizontalHeaderItem(7).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOperat.horizontalHeaderItem(8).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableOperat.resizeColumnsToContents()
        self.tableOperat.resizeRowsToContents()
        self.tableOperat.setSortingEnabled(True)
        self.tableOperat.setSelectionBehavior(QTableView.SelectRows)
        self.tableOperat.clicked.connect(self.viewClickedOperat)
        self.btnOperatRead = QPushButton("Прочитать")
        self.btnOperatRead.setToolTip("Прочитать данные из базы")
        self.btnOperatRead.clicked.connect(self.OnOperatRead)
        self.btnOperatAdd = QPushButton("Добавить")
        self.btnOperatAdd.setToolTip("Добавить новую строку данных")
        self.btnOperatAdd.clicked.connect(self.OnOperatAdd)
        self.btnOperatAdd.setEnabled(False)
        self.btnOperatCancel = QPushButton("Отклонить")
        self.btnOperatCancel.setToolTip("Отклонить изменения")
        self.btnOperatCancel.clicked.connect(self.OnOperatCancel)
        self.btnOperatCancel.setEnabled(False)
        self.btnOperatSave = QPushButton("Сохранить")
        self.btnOperatSave.setToolTip("Сохранить изменения")
        self.btnOperatSave.clicked.connect(self.OnOperatSave)
        self.btnOperatSave.setEnabled(False)
        self.btnOperatDelete = QPushButton("Удалить")
        self.btnOperatDelete.setToolTip("Удалить данные")
        self.btnOperatDelete.clicked.connect(self.OnOperatDelete)
        self.btnOperatDelete.setEnabled(False)

        self.labelDataFromPower = QLabel("Дата от: ")
        self.labelDataToPower = QLabel(" - до - ")
        self.dateFromPower = QDateTimeEdit(self)
        self.dateFromPower.setDisplayFormat("yyyy-MM-dd")
        self.dateFromPower.setDate(QDate.currentDate())
        self.dateFromPower.setCalendarPopup(True)
        self.dateToPower = QDateTimeEdit(self)
        self.dateToPower.setDisplayFormat("yyyy-MM-dd")
        self.dateToPower.setDate(QDate.currentDate())
        self.dateToPower.setCalendarPopup(True)
        self.labelTick = QLabel("Квитанция: ")
        self.tickCombo = QComboBox(self)
        self.tickCombo.setEditable(False)
        self.tickCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.tickCombo.setToolTip("Выберите тип квитанции")
        self.labelCash = QLabel("Кассир: ")
        self.cashCombo = QComboBox(self)
        self.cashCombo.setEditable(False)
        self.cashCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.cashCombo.setToolTip("Выберите кассира")
        self.labelCount1 = QLabel("Счетчик 1: ")
        self.count1Combo = QComboBox(self)
        self.count1Combo.setEditable(True)
        self.count1Combo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.count1Combo.setToolTip("Выберите первого счетчика")
        self.labelCount2 = QLabel("Счетчик 2: ")
        self.count2Combo = QComboBox(self)
        self.count2Combo.setEditable(True)
        self.count2Combo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.count2Combo.setToolTip("Выберите второго счетчика")
        self.labelData = QLabel("Дата: ")
        self.datePower = QDateTimeEdit(self)
        self.datePower.setDisplayFormat("yyyy-MM-dd")
        self.datePower.setDate(QDate.currentDate())
        self.datePower.setCalendarPopup(True)
        self.labelFiltPower = QLabel("Фильтр:")
        self.filtComboPower = QComboBox(self)
        self.filtComboPower.setEditable(False)
        self.filtComboPower.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.filtComboPower.setToolTip("Выберите клонку фильтрации")
        self.labelSummaPower = QLabel("Сумма:")
        self.lineSummaPower = QLineEdit()
        self.lineSummaPower.setToolTip("Общая сумма")
        self.lineSummaPower.setReadOnly(True)
        self.labelNumPower = QLabel("Всего строк:")
        self.lineNumPower = QLineEdit()
        self.lineNumPower.setReadOnly(True)
        self.lineNumPower.setToolTip("Число строк в таблице")
        self.lineFiltPower = QLineEdit()
        self.lineFiltPower.setToolTip("Наберите выражение фильтрации")
        self.lineFiltPower.textChanged.connect(self.OnFilterPower)
        self.labelSearchPower = QLabel("Поиск:")
        self.lineSearchPower = QLineEdit()
        self.lineSearchPower.setToolTip("Наберите выражение для поиска")
        self.btnSearchPower = QPushButton("Поиск")
        self.btnSearchPower.setToolTip("Искать данные пользователей")
        self.btnSearchPower.clicked.connect(self.OnSearchPower)
        self.btnExportPower = QPushButton("Експорт")
        self.btnExportPower.setToolTip("Експортировать данные в формат excel")
        self.btnExportPower.clicked.connect(self.OnExportPower)
        self.tablePowers = QTableWidget(self)
        self.tablePowers.setColumnCount(12)
        self.columnPowers = ["ID", "Квитанция", "Тип квитанции", "Операция", "Сумма", "Валюта", "Получатель", "Кассир", "Счетчик 1",
                             "Счетчик 2", "Дата", "Примечание"]
        self.filtComboPower.addItems(["Квитанция", "Тип квитанции", "Операция", "Сумма", "Валюта", "Получатель", "Кассир", "Счетчик 1",
                             "Счетчик 2", "Дата", "Примечание"])
        self.tablePowers.setHorizontalHeaderLabels(self.columnPowers)
        self.tablePowers.horizontalHeaderItem(0).setToolTip("ID")
        self.tablePowers.horizontalHeaderItem(1).setToolTip("Номер квитанции")
        self.tablePowers.horizontalHeaderItem(2).setToolTip("Тип квитанции")
        self.tablePowers.horizontalHeaderItem(3).setToolTip("Название операции")
        self.tablePowers.horizontalHeaderItem(4).setToolTip("Значение суммы")
        self.tablePowers.horizontalHeaderItem(5).setToolTip("Код валюты")
        self.tablePowers.horizontalHeaderItem(6).setToolTip("ФИО кассира")
        self.tablePowers.horizontalHeaderItem(7).setToolTip("ФИО получателя")
        self.tablePowers.horizontalHeaderItem(8).setToolTip("ФИО счетчика 1")
        self.tablePowers.horizontalHeaderItem(9).setToolTip("ФИО счетчика 2")
        self.tablePowers.horizontalHeaderItem(10).setToolTip("Дата")
        self.tablePowers.horizontalHeaderItem(11).setToolTip("Примечание")
        self.tablePowers.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.horizontalHeaderItem(3).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.horizontalHeaderItem(4).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.horizontalHeaderItem(5).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.horizontalHeaderItem(6).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.horizontalHeaderItem(7).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.horizontalHeaderItem(8).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.horizontalHeaderItem(9).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.horizontalHeaderItem(10).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.horizontalHeaderItem(11).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePowers.resizeColumnsToContents()
        self.tablePowers.resizeRowsToContents()
        self.tablePowers.setSortingEnabled(True)
        self.tablePowers.setSelectionBehavior(QTableView.SelectRows)
        self.tablePowers.clicked.connect(self.viewClickedPowers)
        self.btnPowersRead = QPushButton("Прочитать")
        self.btnPowersRead.setToolTip("Прочитать данные из базы")
        self.btnPowersRead.clicked.connect(self.OnPowersRead)
        self.btnPowersAdd = QPushButton("Добавить")
        self.btnPowersAdd.setToolTip("Добавить новую строку данных")
        self.btnPowersAdd.clicked.connect(self.OnPowersAdd)
        self.btnPowersAdd.setEnabled(False)
        self.btnPowersCancel = QPushButton("Отклонить")
        self.btnPowersCancel.setToolTip("Отклонить изменения")
        self.btnPowersCancel.clicked.connect(self.OnPowersCancel)
        self.btnPowersCancel.setEnabled(False)
        self.btnPowersSave = QPushButton("Сохранить")
        self.btnPowersSave.setToolTip("Сохранить изменения")
        self.btnPowersSave.clicked.connect(self.OnPowersSave)
        self.btnPowersSave.setEnabled(False)
        self.btnPowersDelete = QPushButton("Удалить")
        self.btnPowersDelete.setToolTip("Удалить данные")
        self.btnPowersDelete.clicked.connect(self.OnPowersDelete)
        self.btnPowersDelete.setEnabled(False)

        self.labelDataFromLister = QLabel("Дата от: ")
        self.labelDataToLister = QLabel(" - до - ")
        self.dateFromLister = QDateTimeEdit(self)
        self.dateFromLister.setDisplayFormat("yyyy-MM-dd")
        self.dateFromLister.setDate(QDate.currentDate())
        self.dateFromLister.setCalendarPopup(True)
        self.dateToLister = QDateTimeEdit(self)
        self.dateToLister.setDisplayFormat("yyyy-MM-dd")
        self.dateToLister.setDate(QDate.currentDate())
        self.dateToLister.setCalendarPopup(True)
        self.labelFiltLister = QLabel("Фильтр:")
        self.labelSearchLister = QLabel("Поиск:")
        self.lineSearchLister = QLineEdit()
        self.lineSearchLister.setToolTip("Наберите выражение для поиска")
        self.btnSearchLister = QPushButton("Поиск")
        self.btnSearchLister.setToolTip("Искать данные пользователей")
        self.btnSearchLister.clicked.connect(self.OnSearchLister)
        self.lineFiltLister = QLineEdit()
        self.lineFiltLister.setToolTip("Наберите выражение фильтрации")
        self.lineFiltLister.textChanged.connect(self.setFilterLister)
        self.lineFiltLister1 = QLineEdit()
        self.lineFiltLister1.setToolTip("Наберите выражение фильтрации")
        self.lineFiltLister1.textChanged.connect(self.setFilterLister)
        self.filtComboLister = QComboBox(self)
        self.filtComboLister.setEditable(False)
        self.filtComboLister.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.filtComboLister.setToolTip("Выберите клонку фильтрации")
        self.filtComboLister1 = QComboBox(self)
        self.filtComboLister1.setEditable(False)
        self.filtComboLister1.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.filtComboLister1.setToolTip("Выберите клонку фильтрации")
        self.labelSummaLister = QLabel("Сумма:")
        self.lineSummaLister = QLineEdit()
        self.lineSummaLister.setToolTip("Общая сумма:")
        self.lineSummaLister.setReadOnly(True)
        self.btnExportLister = QPushButton("Експорт")
        self.btnExportLister.setToolTip("Експортировать данные таблицы")
        self.btnExportLister.clicked.connect(self.OnExportLister)
        self.tableLister = QTableWidget(self)
        self.tableLister.setColumnCount(7)
        self.columnLister = ["ID", "ФИО", "Операция", "Сумма", "Валюта", "Дата", "Примечание"]
        self.filtComboLister.addItems(["ФИО", "Операция", "Сумма", "Валюта", "Дата", "Примечание"])
        self.filtComboLister1.addItems(["ФИО", "Операция", "Сумма", "Валюта", "Дата", "Примечание"])
        self.tableLister.setHorizontalHeaderLabels(self.columnLister)
        self.tableLister.horizontalHeaderItem(0).setToolTip("ID")
        self.tableLister.horizontalHeaderItem(1).setToolTip("ФИО")
        self.tableLister.horizontalHeaderItem(2).setToolTip("Тип операции")
        self.tableLister.horizontalHeaderItem(3).setToolTip("Значение суммы")
        self.tableLister.horizontalHeaderItem(4).setToolTip("Код валюты")
        self.tableLister.horizontalHeaderItem(5).setToolTip("Дата")
        self.tableLister.horizontalHeaderItem(6).setToolTip("Примечание")
        self.tableLister.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableLister.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableLister.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableLister.horizontalHeaderItem(3).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableLister.horizontalHeaderItem(4).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableLister.horizontalHeaderItem(5).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableLister.horizontalHeaderItem(6).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableLister.resizeColumnsToContents()
        self.tableLister.resizeRowsToContents()
        self.tableLister.setSortingEnabled(True)
        self.tableLister.setSelectionBehavior(QTableView.SelectRows)
        self.tableLister.clicked.connect(self.viewClickedLister)
        self.btnListerRead = QPushButton("Прочитать")
        self.btnListerRead.setToolTip("Прочитать данные из базы")
        self.btnListerRead.clicked.connect(self.OnListerRead)
        self.btnListerAdd = QPushButton("Добавить")
        self.btnListerAdd.setToolTip("Добавить новую строку данных")
        self.btnListerAdd.clicked.connect(self.OnListerAdd)
        self.btnListerAdd.setEnabled(False)
        self.btnListerCancel = QPushButton("Отклонить")
        self.btnListerCancel.setToolTip("Отклонить изменения")
        self.btnListerCancel.clicked.connect(self.OnListerCancel)
        self.btnListerCancel.setEnabled(False)
        self.btnListerSave = QPushButton("Сохранить")
        self.btnListerSave.setToolTip("Сохранить изменения")
        self.btnListerSave.clicked.connect(self.OnListerSave)
        self.btnListerSave.setEnabled(False)
        self.btnListerDelete = QPushButton("Удалить")
        self.btnListerDelete.setToolTip("Удалить данные")
        self.btnListerDelete.clicked.connect(self.OnListerDelete)
        self.btnListerDelete.setEnabled(False)
        self.labelDataLister = QLabel("Дата:")
        self.dateLister = QDateTimeEdit(self)
        self.dateLister.setDisplayFormat("yyyy-MM-dd")
        self.dateLister.setDate(QDate.currentDate())
        self.dateLister.setCalendarPopup(True)

        self.labelDataFromKonfer = QLabel("Дата от: ")
        self.labelDataToKonfer = QLabel(" - до - ")
        self.dateFromKonfer = QDateTimeEdit(self)
        self.dateFromKonfer.setDisplayFormat("yyyy-MM-dd")
        self.dateFromKonfer.setDate(QDate.currentDate())
        self.dateFromKonfer.setCalendarPopup(True)
        self.dateToKonfer = QDateTimeEdit(self)
        self.dateToKonfer.setDisplayFormat("yyyy-MM-dd")
        self.dateToKonfer.setDate(QDate.currentDate())
        self.dateToKonfer.setCalendarPopup(True)
        self.btnGenerKonfer = QPushButton("Генерация")
        self.btnGenerKonfer.setToolTip("Сгенерировать отчет")
        self.btnGenerKonfer.clicked.connect(self.OnGenerKonfer)
        self.labelCheck = QLabel("Полный экспорт:")
        self.checkBoxGenerate = QCheckBox(self)
        self.checkBoxGenerate.setChecked(True)
        self.checkBoxGenerate.clicked.connect(self.OnTypeGenerate)
        self.btnExportKonfer = QPushButton("Експорт")
        self.btnExportKonfer.setToolTip("Експортировать данные таблицы в excel")
        self.btnExportKonfer.clicked.connect(self.OnExportKonfer)
        self.labelKonfer = QLabel("Ежемесячный отчет кассира общины в ЦК")
        self.tableKonfer = QTableWidget(self)
        self.tableKonfer.setColumnCount(2)
        self.columnKonfer = ["Название", "Сумма в валюте"]
        self.tableKonfer.setHorizontalHeaderLabels(self.columnKonfer)
        self.tableKonfer.horizontalHeaderItem(0).setToolTip("Название темы")
        self.tableKonfer.horizontalHeaderItem(1).setToolTip("Общая сумма в валюте")
        self.tableKonfer.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableKonfer.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.labelPublic = QLabel("Отчет о приношениях местной церкви")
        self.tablePublic = QTableWidget(self)
        self.tablePublic.setColumnCount(2)
        self.columnPublic = ["Название", "Сумма в валюте"]
        self.tablePublic.setHorizontalHeaderLabels(self.columnPublic)
        self.tablePublic.horizontalHeaderItem(0).setToolTip("Название темы")
        self.tablePublic.horizontalHeaderItem(1).setToolTip("Общая сумма в валюте")
        self.tablePublic.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePublic.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.labelSummaKonfer = QLabel("Общая сумма:")
        self.lineSummKonfer = QLineEdit()
        self.lineSummKonfer.setToolTip("Отображение общей суммы")
        self.lineSummKonfer.setReadOnly(True)
        self.labelSummaPublic = QLabel("Общая сумма:")
        self.lineSummPublic = QLineEdit()
        self.lineSummPublic.setToolTip("Отображение общей суммы")
        self.lineSummPublic.setReadOnly(True)

        self.labelDataAudit = QLabel("Дата от: ")
        self.labelDataToAudit = QLabel(" - до - ")
        self.dateFromAudit = QDateTimeEdit(self)
        self.dateFromAudit.setDisplayFormat("yyyy-MM-dd")
        self.dateFromAudit.setDate(QDate.currentDate())
        self.dateFromAudit.setCalendarPopup(True)
        self.dateToAudit = QDateTimeEdit(self)
        self.dateToAudit.setDisplayFormat("yyyy-MM-dd")
        self.dateToAudit.setDate(QDate.currentDate())
        self.dateToAudit.setCalendarPopup(True)
        self.btnGenerAudit = QPushButton("Генерировать")
        self.btnGenerAudit.setToolTip("Сгенерировать отчет")
        self.btnGenerAudit.clicked.connect(self.OnGenerAudit)
        self.btnExportAuditL = QPushButton("Експорт")
        self.btnExportAuditL.setToolTip("Експортировать данные из левых таблиц в excel")
        self.btnExportAuditL.clicked.connect(self.OnExportAuditL)
        self.labelTable1 = QLabel("Приношения в ЦК")
        self.tableAudit1 = QTableWidget(self)
        self.columnTable1 = []
        self.tableAudit1.resizeColumnsToContents()
        self.tableAudit1.resizeRowsToContents()
        self.tableAudit1.setSortingEnabled(False)
        self.tableAudit1.setSelectionBehavior(QTableView.SelectRows)
        self.tableAudit1.clicked.connect(self.viewClickedAudit1)
        self.labelTable2 = QLabel("Приношения в местную общину")
        self.tableAudit2 = QTableWidget(self)
        self.columnTable2 = []
        self.tableAudit2.resizeColumnsToContents()
        self.tableAudit2.resizeRowsToContents()
        self.tableAudit2.setSortingEnabled(False)
        self.tableAudit2.setSelectionBehavior(QTableView.SelectRows)
        self.tableAudit2.clicked.connect(self.viewClickedAudit2)
        self.labelTable3 = QLabel("Ежемесячный отчет в ЦК")
        self.tableAudit3 = QTableWidget(self)
        self.columnTable3 = []
        self.tableAudit3.resizeColumnsToContents()
        self.tableAudit3.resizeRowsToContents()
        self.tableAudit3.setSortingEnabled(False)
        self.tableAudit3.setSelectionBehavior(QTableView.SelectRows)
        self.tableAudit3.clicked.connect(self.viewClickedAudit3)
        self.labelTable4 = QLabel("Расходы в местной общине")
        self.tableAudit4 = QTableWidget(self)
        self.columnTable4 = []
        self.tableAudit4.resizeColumnsToContents()
        self.tableAudit4.resizeRowsToContents()
        self.tableAudit4.setSortingEnabled(False)
        self.tableAudit4.setSelectionBehavior(QTableView.SelectRows)
        self.tableAudit4.clicked.connect(self.viewClickedAudit4)
        self.labelTable5 = QLabel("Возвраты в местной общине")
        self.tableAudit5 = QTableWidget(self)
        self.columnTable5 = []
        self.tableAudit5.resizeColumnsToContents()
        self.tableAudit5.resizeRowsToContents()
        self.tableAudit5.setSortingEnabled(False)
        self.tableAudit5.setSelectionBehavior(QTableView.SelectRows)
        self.tableAudit5.clicked.connect(self.viewClickedAudit5)
        self.labelTable6 = QLabel("Итого средств в местной общине")
        self.tableAudit6 = QTableWidget(self)
        self.tableAudit6.setColumnCount(4)
        self.columnTable6 = ["Название", "Начало периода", "Окончание периода", "Итоговые суммы"]
        self.tableAudit6.setHorizontalHeaderLabels(self.columnTable6)
        self.tableAudit6.horizontalHeaderItem(0).setToolTip("Название темы")
        self.tableAudit6.horizontalHeaderItem(1).setToolTip("Начало периода отчета")
        self.tableAudit6.horizontalHeaderItem(2).setToolTip("Конец периода отчета")
        self.tableAudit6.horizontalHeaderItem(3).setToolTip("Итоговые отчетные суммы")
        self.tableAudit6.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableAudit6.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableAudit6.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableAudit6.horizontalHeaderItem(3).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableAudit6.resizeColumnsToContents()
        self.tableAudit6.resizeRowsToContents()
        self.tableAudit6.setSortingEnabled(False)
        self.tableAudit6.setSelectionBehavior(QTableView.SelectRows)
        self.tableAudit6.clicked.connect(self.viewClickedAudit6)

        self.labelDataTeam = QLabel("Дата от: ")
        self.labelDataToTeam = QLabel(" - до - ")
        self.dateFromTeam = QDateTimeEdit(self)
        self.dateFromTeam.setDisplayFormat("yyyy-MM-dd")
        self.dateFromTeam.setDate(QDate.currentDate())
        self.dateFromTeam.setCalendarPopup(True)
        self.dateToTeam = QDateTimeEdit(self)
        self.dateToTeam.setDisplayFormat("yyyy-MM-dd")
        self.dateToTeam.setDate(QDate.currentDate())
        self.dateToTeam.setCalendarPopup(True)
        self.btnGenerTeam = QPushButton("Генерировать")
        self.btnGenerTeam.setToolTip("Сгенерировать отчет")
        self.btnGenerTeam.clicked.connect(self.OnGenerTeam)
        self.btnExportTeamL = QPushButton("Експорт")
        self.btnExportTeamL.setToolTip("Експортировать данные из таблиц в excel")
        self.btnExportTeamL.clicked.connect(self.OnExportTeamL)
        self.labelTeam1 = QLabel("Отчет кассира в общине подробный (операционный) по приношениям")
        self.labelTeam3 = QLabel("Отчет кассира в общине подробный (операционный) по расходам")
        self.tableTeam1 = QTableWidget(self)
        self.tableTeam1.setColumnCount(5)
        self.columnTeam1 = ["Название", "Сумма", "Начальная дата", "Конечная дата", "Общая сумма"]
        self.tableTeam1.setHorizontalHeaderLabels(self.columnTeam1)
        self.tableTeam1.horizontalHeaderItem(0).setToolTip("Название операции")
        self.tableTeam1.horizontalHeaderItem(1).setToolTip("Сумма")
        self.tableTeam1.horizontalHeaderItem(2).setToolTip("Начальная дата общей суммы")
        self.tableTeam1.horizontalHeaderItem(3).setToolTip("Конечная дата общей суммы")
        self.tableTeam1.horizontalHeaderItem(4).setToolTip("Общая сумма")
        self.tableTeam1.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam1.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam1.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam1.horizontalHeaderItem(3).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam1.horizontalHeaderItem(4).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.labelTeam2 = QLabel("Отчет кассира в общине сжатый (тематический) по приношениям")
        self.labelTeam4 = QLabel("Отчет кассира в общине сжатый (тематический) по расходам")
        self.tableTeam2 = QTableWidget(self)
        self.tableTeam2.setColumnCount(5)
        self.columnTeam2 = ["Название", "Сумма", "Начальная дата", "Конечная дата", "Общая сумма"]
        self.tableTeam2.setHorizontalHeaderLabels(self.columnTeam2)
        self.tableTeam2.horizontalHeaderItem(0).setToolTip("Название операции")
        self.tableTeam2.horizontalHeaderItem(1).setToolTip("Сумма")
        self.tableTeam2.horizontalHeaderItem(2).setToolTip("Начальная дата общей суммы")
        self.tableTeam2.horizontalHeaderItem(3).setToolTip("Конечная дата общей суммы")
        self.tableTeam2.horizontalHeaderItem(4).setToolTip("Общая сумма")
        self.tableTeam2.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam2.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam2.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam2.horizontalHeaderItem(3).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam2.horizontalHeaderItem(4).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam3 = QTableWidget(self)
        self.tableTeam3.setColumnCount(2)
        self.columnTeam3 = ["Название", "Сумма"]
        self.tableTeam3.setHorizontalHeaderLabels(self.columnTeam3)
        self.tableTeam3.horizontalHeaderItem(0).setToolTip("Название операции")
        self.tableTeam3.horizontalHeaderItem(1).setToolTip("Сумма")
        self.tableTeam3.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam3.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam4 = QTableWidget(self)
        self.tableTeam4.setColumnCount(2)
        self.columnTeam4 = ["Название", "Сумма"]
        self.tableTeam4.setHorizontalHeaderLabels(self.columnTeam4)
        self.tableTeam4.horizontalHeaderItem(0).setToolTip("Название операции")
        self.tableTeam4.horizontalHeaderItem(1).setToolTip("Сумма")
        self.tableTeam4.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTeam4.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        hlayTeamUp = QHBoxLayout(self)
        hlayTeamUp.addWidget(self.labelDataTeam)
        hlayTeamUp.addWidget(self.dateFromTeam)
        hlayTeamUp.addWidget(self.labelDataToTeam)
        hlayTeamUp.addWidget(self.dateToTeam)
        hlayTeamUp.addWidget(self.btnGenerTeam)
        hlayTeamUp.addWidget(self.btnExportTeamL)

        vlayAuditL = QVBoxLayout(self)
        vlayAuditL.addWidget(self.labelTable1)
        vlayAuditL.addWidget(self.tableAudit1)
        vlayAuditL.addWidget(self.labelTable2)
        vlayAuditL.addWidget(self.tableAudit2)
        vlayAuditL.addWidget(self.labelTable3)
        vlayAuditL.addWidget(self.tableAudit3)
        vlayAuditR = QVBoxLayout(self)
        vlayAuditR.addWidget(self.labelTable4)
        vlayAuditR.addWidget(self.tableAudit4)
        vlayAuditR.addWidget(self.labelTable5)
        vlayAuditR.addWidget(self.tableAudit5)
        vlayAuditR.addWidget(self.labelTable6)
        vlayAuditR.addWidget(self.tableAudit6)

        widgetAuditUp = QWidget(self)
        widgetAuditUp.setMaximumHeight(60)
        widgetAuditUp.setMinimumHeight(60)
        hlayAuditUp = QHBoxLayout(self)
        hlayAuditUp.addWidget(self.labelDataAudit)
        hlayAuditUp.addWidget(self.dateFromAudit)
        hlayAuditUp.addWidget(self.labelDataToAudit)
        hlayAuditUp.addWidget(self.dateToAudit)
        hlayAuditUp.addWidget(self.btnGenerAudit)
        hlayAuditUp.addWidget(self.btnExportAuditL)
        widgetAuditUp.setLayout(hlayAuditUp)

        widgetSummKonfer = QWidget(self)
        hlaySummKonfer = QHBoxLayout(self)
        hlaySummKonfer.addWidget(self.labelSummaKonfer)
        hlaySummKonfer.addWidget(self.lineSummKonfer)
        widgetSummKonfer.setLayout(hlaySummKonfer)
        widgetSummPublic = QWidget(self)
        hlaySummPublic = QHBoxLayout(self)
        hlaySummPublic.addWidget(self.labelSummaPublic)
        hlaySummPublic.addWidget(self.lineSummPublic)
        widgetSummPublic.setLayout(hlaySummPublic)
        vlay_Left = QVBoxLayout(self)
        vlay_Left.addWidget(self.labelKonfer)
        vlay_Left.addWidget(self.tableKonfer)
        vlay_Left.addWidget(widgetSummKonfer)
        vlay_Right = QVBoxLayout(self)
        vlay_Right.addWidget(self.labelPublic)
        vlay_Right.addWidget(self.tablePublic)
        vlay_Right.addWidget(widgetSummPublic)
        leftwidget = QWidget(self)
        rightwidget = QWidget(self)
        leftwidgetAud = QWidget(self)
        leftwidgetAud.setLayout(vlayAuditL)
        rightwidgetAud = QWidget(self)
        rightwidgetAud.setLayout(vlayAuditR)
        leftwidget.setLayout(vlay_Left)
        rightwidget.setLayout(vlay_Right)
        splitterKonfer = QSplitter(Qt.Horizontal)
        splitterKonfer.setHandleWidth(3)
        splitterKonfer.addWidget(leftwidget)
        splitterKonfer.addWidget(rightwidget)
        splitterAudit = QSplitter(Qt.Horizontal)
        splitterAudit.setHandleWidth(3)
        splitterAudit.addWidget(leftwidgetAud)
        splitterAudit.addWidget(rightwidgetAud)

        widgetUpTeam = QWidget(self)
        widgetUpTeam.setMinimumHeight(60)
        widgetUpTeam.setMaximumHeight(60)
        widgetUpTeam.setLayout(hlayTeamUp)
        widgetKonferUp = QWidget(self)
        widgetKonferUp.setMinimumHeight(60)
        widgetKonferUp.setMaximumHeight(60)
        hlayKonferUp = QHBoxLayout(self)
        hlayKonferUp.addWidget(self.labelDataFromKonfer)
        hlayKonferUp.addWidget(self.dateFromKonfer)
        hlayKonferUp.addWidget(self.labelDataToKonfer)
        hlayKonferUp.addWidget(self.dateToKonfer)
        hlayKonferUp.addWidget(self.btnGenerKonfer)
        hlayKonferUp.addWidget(self.labelCheck)
        hlayKonferUp.addWidget(self.checkBoxGenerate)
        hlayKonferUp.addWidget(self.btnExportKonfer)
        widgetKonferUp.setLayout(hlayKonferUp)

        widgetListerDown = QWidget(self)
        hlayListerDown = QHBoxLayout(self)
        hlayListerDown.addWidget(self.btnListerRead)
        hlayListerDown.addWidget(self.btnListerAdd)
        hlayListerDown.addWidget(self.btnListerCancel)
        hlayListerDown.addWidget(self.btnListerSave)
        hlayListerDown.addWidget(self.btnListerDelete)
        widgetListerDown.setLayout(hlayListerDown)

        widgetLister = QWidget(self)
        hlayLister = QHBoxLayout(self)
        hlayLister.addWidget(self.tableLister)
        widgetLister.setLayout(hlayLister)

        widgetListerUp = QWidget(self)
        hlayListerUp = QHBoxLayout(self)
        hlayListerUp.addWidget(self.labelDataFromLister)
        hlayListerUp.addWidget(self.dateFromLister)
        hlayListerUp.addWidget(self.labelDataToLister)
        hlayListerUp.addWidget(self.dateToLister)
        hlayListerUp.addWidget(self.labelSummaLister)
        hlayListerUp.addWidget(self.lineSummaLister)
        hlayListerUp.addWidget(self.labelSearchLister)
        hlayListerUp.addWidget(self.lineSearchLister)
        hlayListerUp.addWidget(self.btnSearchLister)
        hlayListerUp.addWidget(self.labelDataLister)
        hlayListerUp.addWidget(self.dateLister)
        hlayListerUp.addWidget(self.btnExportLister)
        widgetListerUp.setLayout(hlayListerUp)

        widgetListerUp2 = QWidget(self)
        hlayListerUp2 = QHBoxLayout(self)
        hlayListerUp2.addWidget(self.labelFiltLister)
        hlayListerUp2.addWidget(self.filtComboLister)
        hlayListerUp2.addWidget(self.lineFiltLister)
        hlayListerUp2.addWidget(self.filtComboLister1)
        hlayListerUp2.addWidget(self.lineFiltLister1)
        widgetListerUp2.setLayout(hlayListerUp2)

        widgetPowerDown = QWidget(self)
        hlayPowerDown = QHBoxLayout(self)
        hlayPowerDown.addWidget(self.btnPowersRead)
        hlayPowerDown.addWidget(self.btnPowersAdd)
        hlayPowerDown.addWidget(self.btnPowersCancel)
        hlayPowerDown.addWidget(self.btnPowersSave)
        hlayPowerDown.addWidget(self.btnPowersDelete)
        widgetPowerDown.setLayout(hlayPowerDown)

        widgetPower = QWidget(self)
        hlayPower = QHBoxLayout(self)
        hlayPower.addWidget(self.tablePowers)
        widgetPower.setLayout(hlayPower)

        widgetPowerUp2 = QWidget(self)
        hlayPowerUp2 = QHBoxLayout(self)
        hlayPowerUp2.addWidget(self.labelData)
        hlayPowerUp2.addWidget(self.datePower)
        hlayPowerUp2.addWidget(self.labelSummaPower)
        hlayPowerUp2.addWidget(self.lineSummaPower)
        hlayPowerUp2.addWidget(self.labelNumPower)
        hlayPowerUp2.addWidget(self.lineNumPower)
        hlayPowerUp2.addWidget(self.labelFiltPower)
        hlayPowerUp2.addWidget(self.filtComboPower)
        hlayPowerUp2.addWidget(self.lineFiltPower)
        hlayPowerUp2.addWidget(self.labelSearchPower)
        hlayPowerUp2.addWidget(self.lineSearchPower)
        hlayPowerUp2.addWidget(self.btnSearchPower)
        hlayPowerUp2.addWidget(self.btnExportPower)
        widgetPowerUp2.setLayout(hlayPowerUp2)

        widgetPowerUp = QWidget(self)
        hlayPowerUp = QHBoxLayout(self)
        hlayPowerUp.addWidget(self.labelDataFromPower)
        hlayPowerUp.addWidget(self.dateFromPower)
        hlayPowerUp.addWidget(self.labelDataToPower)
        hlayPowerUp.addWidget(self.dateToPower)
        hlayPowerUp.addWidget(self.labelTick)
        hlayPowerUp.addWidget(self.tickCombo)
        hlayPowerUp.addWidget(self.labelCash)
        hlayPowerUp.addWidget(self.cashCombo)
        hlayPowerUp.addWidget(self.labelCount1)
        hlayPowerUp.addWidget(self.count1Combo)
        hlayPowerUp.addWidget(self.labelCount2)
        hlayPowerUp.addWidget(self.count2Combo)
        widgetPowerUp.setLayout(hlayPowerUp)

        self.tabs = QTabWidget(self)
        vlayTickets = QVBoxLayout()
        vlayTickets.addWidget(widgetup)
        vlayTickets.addWidget(widgetline)
        vlayTickets.addWidget(self.tableTickets)

        self.tabTickets = QWidget(self)
        self.tabTickets.setLayout(vlayTickets)
        hlays = QHBoxLayout(self)
        hlays.addWidget(self.tabs)

        hlayUper = QHBoxLayout(self)
        hlayUper.addWidget(self.labelFiltPersone)
        hlayUper.addWidget(self.filtComboPersone)
        hlayUper.addWidget(self.lineFiltPersone)
        hlayUper.addWidget(self.labelSearchPersone)
        hlayUper.addWidget(self.lineSearchPersone)
        hlayUper.addWidget(self.btnSearchPersone)
        hlayUper.addWidget(self.btnExportPersone)
        widgetPersoneUp = QWidget(self)
        widgetPersoneUp.setLayout(hlayUper)

        hlayUper1 = QHBoxLayout(self)
        hlayUper1.addWidget(self.labelFiltPersone1)
        hlayUper1.addWidget(self.filtComboPersone1)
        hlayUper1.addWidget(self.lineFiltPersone1)
        hlayUper1.addWidget(self.labelSearchPersone1)
        hlayUper1.addWidget(self.lineSearchPersone1)
        hlayUper1.addWidget(self.btnSearchPersone1)
        hlayUper1.addWidget(self.btnExportPersone1)
        widgetPersoneUp1 = QWidget(self)
        widgetPersoneUp1.setLayout(hlayUper1)

        widgetPersonButton = QWidget(self)
        hlayPersonButton = QHBoxLayout(self)
        hlayPersonButton.addWidget(self.btnPersonRead)
        hlayPersonButton.addWidget(self.btnPersonAdd)
        hlayPersonButton.addWidget(self.btnPersonCancel)
        hlayPersonButton.addWidget(self.btnPersonSave)
        hlayPersonButton.addWidget(self.btnPersonDelete)
        widgetPersonButton.setLayout(hlayPersonButton)

        widgetOperatButton = QWidget(self)
        hlayOperatButton = QHBoxLayout(self)
        hlayOperatButton.addWidget(self.btnOperatRead)
        hlayOperatButton.addWidget(self.btnOperatAdd)
        hlayOperatButton.addWidget(self.btnOperatCancel)
        hlayOperatButton.addWidget(self.btnOperatSave)
        hlayOperatButton.addWidget(self.btnOperatDelete)
        widgetOperatButton.setLayout(hlayOperatButton)

        vlayLeftTeam = QVBoxLayout(self)
        vlayLeftTeam.addWidget(self.labelTeam1)
        vlayLeftTeam.addWidget(self.tableTeam1)
        vlayLeftTeam.addWidget(self.labelTeam3)
        vlayLeftTeam.addWidget(self.tableTeam3)
        vlayRightTeam = QVBoxLayout(self)
        vlayRightTeam.addWidget(self.labelTeam2)
        vlayRightTeam.addWidget(self.tableTeam2)
        vlayRightTeam.addWidget(self.labelTeam4)
        vlayRightTeam.addWidget(self.tableTeam4)
        vlayLeft = QVBoxLayout(self)
        vlayLeft.addWidget(widgetPersoneUp)
        vlayLeft.addWidget(self.tablePerson)
        vlayLeft.addWidget(widgetPersonButton)
        vlayRight = QVBoxLayout(self)
        vlayRight.addWidget(widgetPersoneUp1)
        vlayRight.addWidget(self.tableOperat)
        vlayRight.addWidget(widgetOperatButton)
        left_widget = QWidget(self)
        right_widget = QWidget(self)
        left_widget.setLayout(vlayLeft)
        right_widget.setLayout(vlayRight)
        left_widget_team = QWidget(self)
        right_widget_team = QWidget(self)
        left_widget_team.setLayout(vlayLeftTeam)
        right_widget_team.setLayout(vlayRightTeam)
        splitterPerson = QSplitter(Qt.Horizontal)
        splitterPerson.setHandleWidth(3)
        splitterPerson.addWidget(left_widget)
        splitterPerson.addWidget(right_widget)
        splitterTeam = QSplitter(Qt.Horizontal)
        splitterTeam.setHandleWidth(3)
        splitterTeam.addWidget(left_widget_team)
        splitterTeam.addWidget(right_widget_team)

        widgetOplatyButton = QWidget(self)
        hlayOplatyButton = QHBoxLayout(self)
        hlayOplatyButton.addWidget(self.btnOplatyAdd)
        hlayOplatyButton.addWidget(self.btnOplatyCancel)
        hlayOplatyButton.addWidget(self.btnOplatySave)
        hlayOplatyButton.addWidget(self.btnOplatyDelete)
        widgetOplatyButton.setLayout(hlayOplatyButton)

        widgetComplexUp = QWidget(self)
        hlayComplex = QHBoxLayout(self)
        hlayComplex.addWidget(self.labelDataFromComplex)
        hlayComplex.addWidget(self.dateFromComplex)
        hlayComplex.addWidget(self.labelDataToComplex)
        hlayComplex.addWidget(self.dateToComplex)
        hlayComplex.addWidget(self.labelBeginerSumma)
        hlayComplex.addWidget(self.lineBeginerComplex)
        hlayComplex.addWidget(self.btnGenerComplex)
        hlayComplex.addWidget(self.btnExportComplex)
        widgetComplexUp.setLayout(hlayComplex)

        widgetUpers = QWidget(self)
        hlayOplaty = QHBoxLayout(self)
        hlayOplaty.addWidget(self.labelDataFromOplaty)
        hlayOplaty.addWidget(self.dateFromOplaty)
        hlayOplaty.addWidget(self.labelDataToOplaty)
        hlayOplaty.addWidget(self.dateToOplaty)
        hlayOplaty.addWidget(self.btnReadOplaty)
        hlayOplaty.addWidget(self.labelSummaOplaty)
        hlayOplaty.addWidget(self.lineSummaOplaty)
        hlayOplaty.addWidget(self.labelFiltOplaty)
        hlayOplaty.addWidget(self.filtComboOplaty)
        hlayOplaty.addWidget(self.lineFiltOplaty)
        hlayOplaty.addWidget(self.labelSearchOplaty)
        hlayOplaty.addWidget(self.lineSearchOplaty)
        hlayOplaty.addWidget(self.btnSearchOplaty)
        hlayOplaty.addWidget(self.btnExportOplaty)
        widgetUpers.setLayout(hlayOplaty)
        self.tabOplaty = QWidget(self)
        self.tabPersone = QWidget(self)
        self.tabTeam = QWidget(self)
        self.tabComplex = QWidget(self)
        self.tabPower = QWidget(self)
        self.tabLister = QWidget(self)
        self.tabKonfer = QWidget(self)
        self.tabAudit = QWidget(self)
        vlayOplaty = QVBoxLayout(self)
        vlayOplaty.addWidget(widgetUpers)
        vlayOplaty.addWidget(self.tableOplaty)
        vlayOplaty.addWidget(widgetOplatyButton)
        vlayPerson = QVBoxLayout(self)
        vlayPerson.addWidget(splitterPerson)
        vlayTeam = QVBoxLayout(self)
        vlayComplex = QVBoxLayout(self)
        vlayTeam.addWidget(widgetUpTeam)
        vlayTeam.addWidget(splitterTeam)
        vlayComplex.addWidget(widgetComplexUp)
        vlayComplex.addWidget(self.tableComplex)
        vlayPower = QVBoxLayout(self)
        vlayLister = QVBoxLayout(self)
        vlayKonfer = QVBoxLayout(self)
        vlayAudit = QVBoxLayout(self)
        vlayKonfer.addWidget(widgetKonferUp)
        vlayKonfer.addWidget(splitterKonfer)
        vlayAudit.addWidget(widgetAuditUp)
        vlayAudit.addWidget(splitterAudit)
        self.tabOplaty.setLayout(vlayOplaty)
        self.tabPersone.setLayout(vlayPerson)
        self.tabTeam.setLayout(vlayTeam)
        self.tabComplex.setLayout(vlayComplex)
        vlayPower.addWidget(widgetPowerUp)
        vlayPower.addWidget(widgetPowerUp2)
        vlayPower.addWidget(widgetPower)
        vlayPower.addWidget(widgetPowerDown)
        vlayLister.addWidget(widgetListerUp)
        vlayLister.addWidget(widgetListerUp2)
        vlayLister.addWidget(widgetLister)
        vlayLister.addWidget(widgetListerDown)
        self.tabPower.setLayout(vlayPower)
        self.tabLister.setLayout(vlayLister)
        self.tabKonfer.setLayout(vlayKonfer)
        self.tabAudit.setLayout(vlayAudit)
        self.tabs.addTab(self.tabTickets, "Квитанции")
        self.tabs.addTab(self.tabOplaty, "Оплаты")
        self.tabs.addTab(self.tabPersone, "Персонализация")
        self.tabs.addTab(self.tabPower, "Редактирование")
        self.tabs.addTab(self.tabLister, "Списки")
        self.tabs.addTab(self.tabKonfer, "Отчет Конференции")
        self.tabs.addTab(self.tabAudit, "Аудит")
        self.tabs.addTab(self.tabTeam, "Отчет Общины")
        self.tabs.addTab(self.tabComplex, "Отчет Комплексный")
        hlays = QHBoxLayout(self)
        hlays.addWidget(self.tabs)
        self.centralWidget.setLayout(hlays)
        self.path = abspath(__file__)
        self.indx = self.path.rfind('/')
        self.inidir = self.path[0:self.indx + 1] + 'boyarka_asd_sqlitedb.db3'
        self.OpeningDataBase(self.inidir)
        return

    @pyqtSlot()
    def OnGenerTeam(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow. OnGenerKonfer", "Data base is clousing!", str(self.connSqlite3))
                return
            dateFrom = self.dateFromTeam.date()
            strDateFrom = dateFrom.toString(self.dateFromTeam.displayFormat())
            dateTo = self.dateToTeam.date()
            strDateTo = dateTo.toString(self.dateToTeam.displayFormat())

            # получить резальтаты по общим темам приход
            sql = "SELECT PowerTable.CodeOperation,PowerTable.Summa,PowerTable.CodeMoney, Thematic.Id, Operations.Date FROM PowerTable, Operations, Thematic WHERE (PowerTable.Date >= '" + strDateFrom + "' AND PowerTable.Date <= '" + strDateTo + "') AND Operations.CodeReport=1 AND (Operations.CodeOperation=1 OR Operations.CodeOperation=3) AND PowerTable.CodeOperation=Operations.Id AND Operations.Thema=Thematic.Id ORDER BY Operations.Date"
            teamTable = self.getFromDataBase(sql)
            sql = "SELECT * FROM Money"
            result = self.getFromDataBase(sql)
            moneyCode = []
            moneyId = []
            for ls in result:
                moneyCode.append(ls[1])
                moneyId.append(ls[0])
            sql = "SELECT Id,Name FROM Thematic"
            result = self.getFromDataBase(sql)
            teamCodeOffer = []
            teamIDOffer = []
            for ls in result:
                teamCodeOffer.append(ls[1])
                teamIDOffer.append(ls[0])
            dictSumma = {}
            for i,lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                dictSumma[name] = []
                for j,lt in enumerate(teamTable):
                    if lt[3] == lk:
                        dictSumma[name].append(lt)
            dictTeam = {}
            countThema = 0
            for i,lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                data = dictSumma[name]
                if len(data) == 0:
                    continue
                countThema += 1
                dictTeam[name] = []
                for lt in moneyId:
                    money = [0,lt]
                    dictTeam[name].append(money)
                for ls in data:
                    if lk == ls[3]:
                        for lt in moneyId:
                            if lt == ls[2]:
                                dictTeam[name][lt - 1][0] += ls[1]
                                break
            self.tableTeam2.clearContents()
            self.tableTeam2.setRowCount(countThema + 1)
            row = 0
            summaAll = {}
            for lt in moneyCode:
                summaAll[lt] = 0
            for i,lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                summaCurrency = ''
                if name not in dictTeam:
                    continue
                for lt in dictTeam[name]:
                    if lt[0] != 0:
                        summa = str(round(lt[0], 2))
                        currency = moneyCode[moneyId.index(lt[1])]
                        summaCurrency += summa + currency.lower() + ";"
                        summaAll[currency] += round(lt[0], 2)
                self.tableTeam2.setItem(row, 0, QTableWidgetItem(name))
                self.tableTeam2.setItem(row, 1, QTableWidgetItem(summaCurrency))
                row += 1
            summaCurrency = ''
            for lt in moneyCode:
                if summaAll[lt] != 0:
                    summaCurrency += str(round(summaAll[lt],2)) + lt.lower() + ";"
            self.tableTeam2.setItem(row, 0, QTableWidgetItem('Итого:'))
            self.tableTeam2.setItem(row, 1, QTableWidgetItem(summaCurrency))
            self.tableTeam2.resizeColumnsToContents()
            self.tableTeam2.resizeRowsToContents()

            # получить общие резальтаты по общим темам приход
            idOffer = []
            nameOffer = []
            idteams = '('
            for i, lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                data = dictSumma[name]
                if len(data) == 0:
                    continue
                idOffer.append(lk)
                nameOffer.append(name)
                idteams += str(lk) + ","
            strteam = idteams[0:len(idteams) - 1] + ")"
            nowDate = datetime.now()
            beginDate = nowDate.strftime("%Y-%m-%d")
            for i, lk in enumerate(idOffer):
                name = nameOffer[i]
                data = dictSumma[name]
                for lp in data:
                    if beginDate > lp[4]:
                        beginDate = lp[4]
            sql = "SELECT PowerTable.CodeOperation,PowerTable.Summa,PowerTable.CodeMoney, Thematic.Id, Operations.Date FROM PowerTable, Operations, Thematic WHERE (PowerTable.Date >= '" + beginDate + "' AND PowerTable.Date <= '" + strDateTo + "') AND Operations.CodeReport=1 AND (Operations.CodeOperation=1 OR Operations.CodeOperation=3) AND PowerTable.CodeOperation=Operations.Id AND Operations.Thema=Thematic.Id AND Thematic.Id in " + strteam + " ORDER BY Operations.Date"
            teamTable = self.getFromDataBase(sql)
            dictSumma = {}
            for i, lk in enumerate(idOffer):
                name = nameOffer[i]
                dictSumma[name] = []
                for j, lt in enumerate(teamTable):
                    if lt[3] == lk:
                        dictSumma[name].append(lt)
            dictTeam = {}
            for i, lk in enumerate(idOffer):
                name = nameOffer[i]
                data = dictSumma[name]
                dictTeam[name] = []
                for lt in moneyId:
                    money = [0, lt]
                    dictTeam[name].append(money)
                for ls in data:
                    if lk == ls[3]:
                        for lt in moneyId:
                            if lt == ls[2]:
                                dictTeam[name][lt - 1][0] += ls[1]
                                break
            row = 0
            summaAll = {}
            for lt in moneyCode:
                summaAll[lt] = 0
            for i, lk in enumerate(idOffer):
                name = nameOffer[i]
                nowDate = datetime.now()
                beginDate = nowDate.strftime("%Y-%m-%d")
                data = dictSumma[name]
                for lp in data:
                    if beginDate > lp[4]:
                        beginDate = lp[4]
                summaCurrency = ''
                if name not in dictTeam:
                    continue
                for lt in dictTeam[name]:
                    if lt[0] != 0:
                        summa = str(round(lt[0], 2))
                        currency = moneyCode[moneyId.index(lt[1])]
                        summaCurrency += summa + currency.lower() + ";"
                        summaAll[currency] += round(lt[0], 2)
                self.tableTeam2.setItem(row, 2, QTableWidgetItem(beginDate))
                self.tableTeam2.setItem(row, 3, QTableWidgetItem(strDateTo))
                self.tableTeam2.setItem(row, 4, QTableWidgetItem(summaCurrency))
                row += 1
            summaCurrency = ''
            for lt in moneyCode:
                if summaAll[lt] != 0:
                    summaCurrency += str(round(summaAll[lt], 2)) + lt.lower() + ";"
            self.tableTeam2.setItem(row, 4, QTableWidgetItem(summaCurrency))
            self.tableTeam2.resizeColumnsToContents()
            self.tableTeam2.resizeRowsToContents()

            # получить значения по операциям приход
            sql = "SELECT PowerTable.CodeOperation,PowerTable.Summa,PowerTable.CodeMoney, Operations.Date FROM PowerTable, Operations WHERE (PowerTable.Date >= '" + strDateFrom + "' AND PowerTable.Date <= '" + strDateTo + "') AND Operations.CodeReport=1 AND (Operations.CodeOperation=1 OR Operations.CodeOperation=3) AND PowerTable.CodeOperation=Operations.Id"
            teamTable = self.getFromDataBase(sql)
            sql = "SELECT Id,Name FROM Operations WHERE Operations.CodeReport=1 AND (Operations.CodeOperation=1 OR Operations.CodeOperation=3)"
            result = self.getFromDataBase(sql)
            teamCodeOffer = []
            teamIDOffer = []
            for ls in result:
                teamCodeOffer.append(ls[1])
                teamIDOffer.append(ls[0])
            dictSumma = {}
            for i, lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                dictSumma[name] = []
                for j, lt in enumerate(teamTable):
                    if lt[0] == lk:
                        dictSumma[name].append(lt)
            dictTeam = {}
            countThema = 0
            for i, lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                data = dictSumma[name]
                if len(data) == 0:
                    continue
                countThema += 1
                dictTeam[name] = []
                for lt in moneyId:
                    money = [0, lt]
                    dictTeam[name].append(money)
                for ls in data:
                    if lk == ls[0]:
                        for lt in moneyId:
                            if lt == ls[2]:
                                dictTeam[name][lt - 1][0] += ls[1]
                                break
            self.tableTeam1.clearContents()
            self.tableTeam1.setRowCount(countThema + 1)
            row = 0
            summaAll = {}
            for lt in moneyCode:
                summaAll[lt] = 0
            for i, lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                summaCurrency = ''
                if name not in dictTeam:
                    continue
                for lt in dictTeam[name]:
                    if lt[0] != 0:
                        summa = str(round(lt[0], 2))
                        currency = moneyCode[moneyId.index(lt[1])]
                        summaCurrency += summa + currency.lower() + ";"
                        summaAll[currency] += round(lt[0], 2)
                self.tableTeam1.setItem(row, 0, QTableWidgetItem(name))
                self.tableTeam1.setItem(row, 1, QTableWidgetItem(summaCurrency))
                row += 1
            summaCurrency = ''
            for lt in moneyCode:
                if summaAll[lt] != 0:
                    summaCurrency += str(round(summaAll[lt], 2)) + lt.lower() + ";"
            self.tableTeam1.setItem(row, 0, QTableWidgetItem("Итого:"))
            self.tableTeam1.setItem(row, 1, QTableWidgetItem(summaCurrency))
            self.tableTeam1.resizeColumnsToContents()
            self.tableTeam1.resizeRowsToContents()

            # получить общее значения по операциям приход
            idOffer = []
            nameOffer = []
            for i, lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                data = dictSumma[name]
                if len(data) == 0:
                    continue
                idOffer.append(lk)
                nameOffer.append(name)

            for i, lk in enumerate(idOffer):
                name = nameOffer[i]
                data = dictSumma[name]
                if len(data) == 0:
                    continue
                beginDate = data[0][3]
                sql = "SELECT PowerTable.CodeOperation,PowerTable.Summa,PowerTable.CodeMoney,Operations.Date FROM PowerTable, Operations WHERE (PowerTable.Date >= '" + beginDate + "' AND PowerTable.Date <= '" + strDateTo + "') AND Operations.CodeReport=1 AND (Operations.CodeOperation=1 OR Operations.CodeOperation=3) AND PowerTable.CodeOperation=" + str(data[0][0]) + " AND PowerTable.CodeOperation=Operations.Id"
                teamTable = self.getFromDataBase(sql)
                dictSumma[name] = []
                for lp in teamTable:
                    dictSumma[name].append(lp)

            dictTeam = {}
            for i, lk in enumerate(idOffer):
                name = nameOffer[i]
                data = dictSumma[name]
                if len(data) == 0:
                    continue
                dictTeam[name] = []
                for lt in moneyId:
                    money = [0, lt]
                    dictTeam[name].append(money)
                for ls in data:
                    if lk == ls[0]:
                        for lt in moneyId:
                            if lt == ls[2]:
                                dictTeam[name][lt - 1][0] += ls[1]
                                break
            row = 0
            summaAll = {}
            for lt in moneyCode:
                summaAll[lt] = 0
            for i, lk in enumerate(idOffer):
                name = nameOffer[i]
                data = dictSumma[name]
                beginDate = data[0][3]
                summaCurrency = ''
                for lt in dictTeam[name]:
                    if lt[0] != 0:
                        summa = str(round(lt[0], 2))
                        currency = moneyCode[moneyId.index(lt[1])]
                        summaCurrency += summa + currency.lower() + ";"
                        summaAll[currency] += round(lt[0], 2)
                self.tableTeam1.setItem(row, 2, QTableWidgetItem(beginDate))
                self.tableTeam1.setItem(row, 3, QTableWidgetItem(strDateTo))
                self.tableTeam1.setItem(row, 4, QTableWidgetItem(summaCurrency))
                row += 1
            summaCurrency = ''
            for lt in moneyCode:
                if summaAll[lt] != 0:
                    summaCurrency += str(round(summaAll[lt], 2)) + lt.lower() + ";"
            self.tableTeam1.setItem(row, 4, QTableWidgetItem(summaCurrency))
            self.tableTeam1.resizeColumnsToContents()
            self.tableTeam1.resizeRowsToContents()

            # получить значения по операциям расход
            sql = "SELECT PowerTable.CodeOperation,PowerTable.Summa,PowerTable.CodeMoney, Operations.Date FROM PowerTable, Operations WHERE (PowerTable.Date >= '" + strDateFrom + "' AND PowerTable.Date <= '" + strDateTo + "') AND Operations.CodeReport=3 AND Operations.CodeOperation=2 AND PowerTable.CodeOperation=Operations.Id"
            teamTable = self.getFromDataBase(sql)
            sql = "SELECT Id,Name FROM Operations WHERE Operations.CodeReport=3 AND Operations.CodeOperation=2"
            result = self.getFromDataBase(sql)
            teamCodeOffer = []
            teamIDOffer = []
            for ls in result:
                teamCodeOffer.append(ls[1])
                teamIDOffer.append(ls[0])
            dictSumma = {}
            for i, lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                dictSumma[name] = []
                for j, lt in enumerate(teamTable):
                    if lt[0] == lk:
                        dictSumma[name].append(lt)
            dictTeam = {}
            countThema = 0
            for i, lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                data = dictSumma[name]
                if len(data) == 0:
                    continue
                countThema += 1
                dictTeam[name] = []
                for lt in moneyId:
                    money = [0, lt]
                    dictTeam[name].append(money)
                for ls in data:
                    if lk == ls[0]:
                        for lt in moneyId:
                            if lt == ls[2]:
                                dictTeam[name][lt - 1][0] += ls[1]
                                break
            self.tableTeam3.clearContents()
            self.tableTeam3.setRowCount(countThema + 2)
            row = 0
            summaAll = {}
            for lt in moneyCode:
                summaAll[lt] = 0
            for i, lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                summaCurrency = ''
                if name not in dictTeam:
                    continue
                for lt in dictTeam[name]:
                    if lt[0] != 0:
                        summa = str(round(lt[0], 2))
                        currency = moneyCode[moneyId.index(lt[1])]
                        summaCurrency += summa + currency.lower() + ";"
                        summaAll[currency] += round(lt[0], 2)
                self.tableTeam3.setItem(row, 0, QTableWidgetItem(name))
                self.tableTeam3.setItem(row, 1, QTableWidgetItem(summaCurrency))
                row += 1
            sql = 'SELECT Summa,CodeMoney FROM Periodica WHERE ' + "(Date >= '" + strDateFrom + "' AND " + "Date <= '" + strDateTo + "')"
            result = self.getFromDataBase(sql)
            summaPeriodica = [0 for i in range(len(moneyId))]
            summaMoney = ['' for i in range(len(moneyId))]
            for lt in moneyId:
                for ls in result:
                    if lt == ls[1]:
                        summaPeriodica[lt - 1] += ls[0]
                        summaMoney[lt - 1] = moneyCode[moneyId.index(lt)]
            periodicaStr = ''
            for i, lt in enumerate(summaPeriodica):
                if lt != 0:
                    periodicaStr += str(lt) + summaMoney[i].lower() + ";"
                    summaAll[summaMoney[i]] += round(lt, 2)
            summaCurrency = ''
            for lt in moneyCode:
                if summaAll[lt] != 0:
                    summaCurrency += str(round(summaAll[lt], 2)) + lt.lower() + ";"
            self.tableTeam3.setItem(row, 0, QTableWidgetItem("Периодика:"))
            self.tableTeam3.setItem(row, 1, QTableWidgetItem(periodicaStr))
            row += 1
            self.tableTeam3.setItem(row, 0, QTableWidgetItem("Итого:"))
            self.tableTeam3.setItem(row, 1, QTableWidgetItem(summaCurrency))
            self.tableTeam3.resizeColumnsToContents()
            self.tableTeam3.resizeRowsToContents()

            # получить значения по общим темам расход
            sql = "SELECT PowerTable.CodeOperation,PowerTable.Summa,PowerTable.CodeMoney, Thematic.Id, Operations.Date FROM PowerTable, Operations, Thematic WHERE (PowerTable.Date >= '" + strDateFrom + "' AND PowerTable.Date <= '" + strDateTo + "') AND Operations.CodeReport=3 AND Operations.CodeOperation=2 AND PowerTable.CodeOperation=Operations.Id AND Operations.Thema=Thematic.Id ORDER BY Operations.Date"
            teamTable = self.getFromDataBase(sql)
            sql = "SELECT Id,Name FROM Thematic"
            result = self.getFromDataBase(sql)
            teamCodeOffer = []
            teamIDOffer = []
            for ls in result:
                teamCodeOffer.append(ls[1])
                teamIDOffer.append(ls[0])
            dictSumma = {}
            for i, lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                dictSumma[name] = []
                for j, lt in enumerate(teamTable):
                    if lt[3] == lk:
                        dictSumma[name].append(lt)
            dictTeam = {}
            countThema = 0
            for i, lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                data = dictSumma[name]
                if len(data) == 0:
                    continue
                countThema += 1
                dictTeam[name] = []
                for lt in moneyId:
                    money = [0, lt]
                    dictTeam[name].append(money)
                for ls in data:
                    if lk == ls[3]:
                        for lt in moneyId:
                            if lt == ls[2]:
                                dictTeam[name][lt - 1][0] += ls[1]
                                break
            self.tableTeam4.clearContents()
            self.tableTeam4.setRowCount(countThema + 2)
            row = 0
            summaAll = {}
            for lt in moneyCode:
                summaAll[lt] = 0
            for i, lk in enumerate(teamIDOffer):
                name = teamCodeOffer[i]
                summaCurrency = ''
                if name not in dictTeam:
                    continue
                for lt in dictTeam[name]:
                    if lt[0] != 0:
                        summa = str(round(lt[0], 2))
                        currency = moneyCode[moneyId.index(lt[1])]
                        summaCurrency += summa + currency.lower() + ";"
                        summaAll[currency] += round(lt[0], 2)
                self.tableTeam4.setItem(row, 0, QTableWidgetItem(name))
                self.tableTeam4.setItem(row, 1, QTableWidgetItem(summaCurrency))
                row += 1
            periodicaStr = ''
            for i, lt in enumerate(summaPeriodica):
                if lt != 0:
                    periodicaStr += str(lt) + summaMoney[i].lower() + ";"
                    summaAll[summaMoney[i]] += round(lt, 2)
            summaCurrency = ''
            for lt in moneyCode:
                if summaAll[lt] != 0:
                    summaCurrency += str(round(summaAll[lt], 2)) + lt.lower() + ";"
            self.tableTeam4.setItem(row, 0, QTableWidgetItem("Периодика:"))
            self.tableTeam4.setItem(row, 1, QTableWidgetItem(periodicaStr))
            row += 1
            self.tableTeam4.setItem(row, 0, QTableWidgetItem('Итого:'))
            self.tableTeam4.setItem(row, 1, QTableWidgetItem(summaCurrency))
            self.tableTeam4.resizeColumnsToContents()
            self.tableTeam4.resizeRowsToContents()
        except Exception as ex:
            showError("Error get records", "OnGenerTeam", str(ex) + traceback.format_exc())
        return

    def generateReport(self, n, ws, table, label, column):
        ws.write(n, 0, label.text())
        n += 1
        for i, ls in enumerate(column):
            ws.write(n, i, column[i])
        n += 1
        for k in range(table.rowCount()):
            for i, ls in enumerate(column):
                try:
                    cel0 = table.item(k, i).text()
                except Exception as e:
                    cel0 = " "
                ws.write(n, i, cel0)
            n += 1
        return ws

    @pyqtSlot()
    def OnExportTeamL(self):
        try:
            if self.tableTeam1.rowCount() == 0:
                return
            if self.tableTeam2.rowCount() == 0:
                return
            if self.tableTeam3.rowCount() == 0:
                return
            if self.tableTeam4.rowCount() == 0:
                return
            dateFrom = self.dateFromTeam.date()
            strDateFrom = dateFrom.toString(self.dateFromTeam.displayFormat())
            dateTo = self.dateToTeam.date()
            strDateTo = dateTo.toString(self.dateToTeam.displayFormat())
            wb = xlwt.Workbook()
            ws = wb.add_sheet("Отчет_общины")
            k = 0
            ws = self.generateReport(k, ws, self.tableTeam1, self.labelTeam1, self.columnTeam1)
            k += self.tableTeam1.rowCount() + 3
            ws = self.generateReport(k, ws, self.tableTeam2, self.labelTeam2, self.columnTeam2)
            k += self.tableTeam2.rowCount() + 3
            ws = self.generateReport(k, ws, self.tableTeam3, self.labelTeam3, self.columnTeam3)
            k += self.tableTeam3.rowCount() + 3
            self.generateReport(k, ws, self.tableTeam4, self.labelTeam4, self.columnTeam4)
            namefile = 'Отчет_для_общины_' + strDateFrom + '_' + strDateTo + '.xls'
            wb.save(namefile)
            showInfo("Info export", "Save exporting data is successfull!", namefile)
        except Exception as ex:
            showError("Error generate report audit!", "OnExportTeamL", str(ex) + "\n" + traceback.format_exc())
        return

    @pyqtSlot()
    def viewClickedAudit1(self):
        return

    @pyqtSlot()
    def viewClickedAudit2(self):
        return

    @pyqtSlot()
    def viewClickedAudit3(self):
        return

    @pyqtSlot()
    def viewClickedAudit4(self):
        return

    @pyqtSlot()
    def viewClickedAudit5(self):
        return

    @pyqtSlot()
    def viewClickedAudit6(self):
        return

    @pyqtSlot()
    def OnExportAuditL(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow.OnGenerAudit", "Data Base is clousing!", str(self.connSqlite3))
                return
            dateFrom = self.dateFromAudit.date()
            strDateFrom = dateFrom.toString(self.dateFromAudit.displayFormat())
            dateTo = self.dateToAudit.date()
            strDateTo = dateTo.toString(self.dateToAudit.displayFormat())
            monthName = strDateFrom + "_" + strDateTo
            wb = xlwt.Workbook()
            ws = wb.add_sheet('Приношения')
            row = 2
            for j,lt in enumerate(self.columnTable1):
                if j == 0:
                    lt = self.labelTable1.text()
                ws.write(row, j, lt)
            row += 1
            for j in range(self.tableAudit1.columnCount()):
                for k in range(self.tableAudit1.rowCount()):
                    try:
                        cell0 = self.tableAudit1.item(k, j).text()
                    except Exception as err:
                        cell0 = ""
                    ws.write(row + k, j, cell0)
            row += self.tableAudit1.rowCount() + 1
            for j,lt in enumerate(self.columnTable2):
                if j == 0:
                    lt = self.labelTable2.text()
                ws.write(row, j, lt)
            row += 1
            for j in range(self.tableAudit2.columnCount()):
                for k in range(self.tableAudit2.rowCount()):
                    try:
                        cell0 = self.tableAudit2.item(k, j).text()
                    except Exception as err:
                        cell0 = ""
                    ws.write(row + k, j, cell0)
            row += self.tableAudit2.rowCount() + 1
            ws = wb.add_sheet('Конференция')
            for j, lt in enumerate(self.columnTable3):
                if j == 0:
                    lt = self.labelTable3.text()
                ws.write(row, j, lt)
            row += 1
            for j in range(self.tableAudit3.columnCount()):
                for k in range(self.tableAudit3.rowCount()):
                    try:
                        cell0 = self.tableAudit3.item(k, j).text()
                    except Exception as err:
                        cell0 = ""
                    ws.write(row + k, j, cell0)
            ws = wb.add_sheet('Расходы')
            row += self.tableAudit3.rowCount()
            lt = self.labelTable4.text()
            ws.write(row - 1, 0, lt)
            for j, lt in enumerate(self.columnTable4):
                ws.write(row, j, lt)
            row += 1
            for j in range(self.tableAudit4.columnCount()):
                for k in range(self.tableAudit4.rowCount()):
                    try:
                        cell0 = self.tableAudit4.item(k, j).text()
                    except Exception as err:
                        cell0 = ""
                    ws.write(row + k, j, cell0)
            row += self.tableAudit4.rowCount() + 1
            ws = wb.add_sheet('Возвраты')
            for j, lt in enumerate(self.columnTable5):
                if j == 0:
                    lt = self.labelTable5.text()
                ws.write(row, j, lt)
            row += 1
            for j in range(self.tableAudit5.columnCount()):
                for k in range(self.tableAudit5.rowCount()):
                    try:
                        cell0 = self.tableAudit5.item(k, j).text()
                    except Exception as err:
                        cell0 = ""
                    ws.write(row + k, j, cell0)
            row += self.tableAudit5.rowCount() + 1
            ws = wb.add_sheet('Итоги')
            for j, lt in enumerate(self.columnTable6):
                if j == 0:
                    lt = self.labelTable6.text()
                ws.write(row, j, lt)
            row += 1
            for j in range(self.tableAudit6.columnCount()):
                for k in range(self.tableAudit6.rowCount()):
                    try:
                        cell0 = self.tableAudit6.item(k, j).text()
                    except Exception as err:
                        cell0 = ""
                    ws.write(row + k, j, cell0)
            namefile = 'Отчет_аудит_' + monthName + '.xls'
            wb.save(namefile)
            showInfo("Info export", "Save data is successfull!", namefile)
        except Exception as ex:
            showError("Error generate report audit!", "OnGenerAudit", str(ex) + "\n" + traceback.format_exc())
        return

    @pyqtSlot()
    def OnGenerAudit(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow.OnGenerAudit", "Data Base is clousing!", str(self.connSqlite3))
                return
            dateFrom = self.dateFromAudit.date()
            strDateFrom = dateFrom.toString(self.dateFromAudit.displayFormat())
            dateTo = self.dateToAudit.date()
            strDateTo = dateTo.toString(self.dateToAudit.displayFormat())
            sql = "SELECT PowerTable.NumberTicket,Operations.Name,PowerTable.Summa,Money.Code,PowerTable.Date FROM PowerTable,Operations,Money WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND PowerTable.CodeTicket=1 AND PowerTable.CodeMoney=Money.Id AND Operations.CodeReport=2 AND PowerTable.CodeOperation=Operations.Id"
            auditKonfTable = self.getFromDataBase(sql)
            self.CalculatingTable("Приношения", auditKonfTable, self.columnTable1, self.tableAudit1)
            sql = "SELECT PowerTable.NumberTicket,Operations.Name,PowerTable.Summa,Money.Code,PowerTable.Date FROM PowerTable,Operations,Money WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND PowerTable.CodeTicket=1 AND PowerTable.CodeMoney=Money.Id AND Operations.CodeReport=1 AND PowerTable.CodeOperation=Operations.Id"
            auditKonfTable = self.getFromDataBase(sql)
            self.CalculatingTable("Приношения", auditKonfTable, self.columnTable2, self.tableAudit2)
            sql = "SELECT PowerTable.NumberTicket,Operations.Name,PowerTable.Summa,Money.Code,PowerTable.Date FROM PowerTable,Operations,Money WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND PowerTable.CodeTicket=4 AND PowerTable.CodeMoney=Money.Id AND Operations.CodeReport=3 AND PowerTable.CodeOperation=Operations.Id"
            auditKonfTable = self.getFromDataBase(sql)
            self.CalculatingTable("Название", auditKonfTable, self.columnTable3, self.tableAudit3)
            sql = "SELECT PowerTable.NumberTicket,Operations.Name,PowerTable.Summa,Money.Code,PowerTable.Date FROM PowerTable,Operations,Money WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND PowerTable.CodeTicket=2 AND PowerTable.CodeMoney=Money.Id AND Operations.CodeReport=3 AND PowerTable.CodeOperation=Operations.Id"
            auditKonfTable = self.getFromDataBase(sql)
            self.CalculatingTableOut(auditKonfTable, self.columnTable4, self.tableAudit4)
            sql = "SELECT PowerTable.NumberTicket,Operations.Name,PowerTable.Summa,Money.Code,PowerTable.Date FROM PowerTable,Operations,Money WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND PowerTable.CodeTicket=3 AND PowerTable.CodeMoney=Money.Id AND Operations.CodeReport=3 AND PowerTable.CodeOperation=Operations.Id"
            auditKonfTable = self.getFromDataBase(sql)
            self.CalculatingTable("Возвраты", auditKonfTable, self.columnTable5, self.tableAudit5)

            sql = "SELECT * FROM Money"
            result = self.getFromDataBase(sql)
            moneyCode = []
            moneyId = []
            for ls in result:
                moneyCode.append(ls[1])
                moneyId.append(ls[0])

            summaIn = {}
            for money in moneyId:
                sql = "SELECT SUM(PowerTable.Summa) FROM PowerTable,Operations WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND PowerTable.CodeTicket=1 AND PowerTable.CodeMoney=" + str(money) + " AND Operations.CodeReport=1 AND PowerTable.CodeOperation=Operations.Id"
                res = self.getFromDataBase(sql)
                if res[0][0] != None:
                    summaIn[str(money)] = res[0][0]
                else:
                    summaIn[str(money)] = 0

            summaOut = {}
            for money in moneyId:
                sql = "SELECT SUM(PowerTable.Summa) FROM PowerTable,Operations WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND PowerTable.CodeTicket=1 AND PowerTable.CodeMoney=" + str(
                    money) + " AND Operations.CodeReport=2 AND PowerTable.CodeOperation=Operations.Id"
                res = self.getFromDataBase(sql)
                if res[0][0] != None:
                    summaOut[str(money)] = res[0][0]
                else:
                    summaOut[str(money)] = 0

            summaDoll = {}
            for money in moneyId:
                sql = "SELECT SUM(PowerTable.Summa) FROM PowerTable,Operations WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND PowerTable.CodeTicket=2 AND PowerTable.CodeMoney=" + str(
                    money) + " AND Operations.CodeReport=3 AND PowerTable.CodeOperation=Operations.Id"
                res = self.getFromDataBase(sql)
                if res[0][0] != None:
                    summaDoll[str(money)] = res[0][0]
                else:
                    summaDoll[str(money)] = 0

            summaUp = {}
            for money in moneyId:
                sql = "SELECT SUM(PowerTable.Summa) FROM PowerTable,Operations WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND PowerTable.CodeTicket=3 AND PowerTable.CodeMoney=" + str(
                    money) + " AND Operations.CodeReport=3 AND PowerTable.CodeOperation=Operations.Id"
                res = self.getFromDataBase(sql)
                if res[0][0] != None:
                    summaUp[str(money)] = res[0][0]
                else:
                    summaUp[str(money)] = 0

            summaFoc = {}
            for money in moneyId:
                sql = "SELECT SUM(Periodica.Summa) FROM Periodica WHERE (Periodica.Date >= '" + strDateFrom + "' AND " + "Periodica.Date <= '" + strDateTo + "') AND Periodica.CodeMoney=" + str(money)
                res = self.getFromDataBase(sql)
                if res[0][0] != None:
                    summaFoc[str(money)] = res[0][0]
                else:
                    summaFoc[str(money)] = 0

            dateBegin  = '2016-12-30'
            utctime = int(time.mktime(time.strptime(strDateFrom + ' 00:00:00', '%Y-%m-%d %H:%M:%S')))
            utctime = utctime - 86400
            dateEnd = time.strftime("%Y-%m-%d", time.localtime(utctime))

            beginSumma = {}
            for money in moneyId:
                sql = "SELECT Summa FROM BeginerSumma WHERE CodeMoney=" + str(money)
                res = self.getFromDataBase(sql)
                if len(res) > 0:
                    if res[0][0] != None:
                        beginSumma[str(money)] = res[0][0]
                    else:
                        beginSumma[str(money)] = 0
                else:
                    beginSumma[str(money)] = 0

            summaInD = {}
            for money in moneyId:
                sql = "SELECT SUM(PowerTable.Summa) FROM PowerTable,Operations WHERE (PowerTable.Date >= '" + dateBegin + "' AND " + "PowerTable.Date <= '" + dateEnd + "') AND PowerTable.CodeTicket=1 AND PowerTable.CodeMoney=" + str(
                    money) + " AND Operations.CodeReport=1 AND PowerTable.CodeOperation=Operations.Id"
                res = self.getFromDataBase(sql)
                if res[0][0] != None:
                    summaInD[str(money)] = res[0][0]
                else:
                    summaInD[str(money)] = 0

            summaOutD = {}
            for money in moneyId:
                sql = "SELECT SUM(PowerTable.Summa) FROM PowerTable,Operations WHERE (PowerTable.Date >= '" + dateBegin + "' AND " + "PowerTable.Date <= '" + dateEnd + "') AND PowerTable.CodeTicket=1 AND PowerTable.CodeMoney=" + str(
                    money) + " AND Operations.CodeReport=2 AND PowerTable.CodeOperation=Operations.Id"
                res = self.getFromDataBase(sql)
                if res[0][0] != None:
                    summaOutD[str(money)] = res[0][0]
                else:
                    summaOutD[str(money)] = 0

            summaDollD = {}
            for money in moneyId:
                sql = "SELECT SUM(PowerTable.Summa) FROM PowerTable,Operations WHERE (PowerTable.Date >= '" + dateBegin + "' AND " + "PowerTable.Date <= '" + dateEnd + "') AND PowerTable.CodeTicket=2 AND PowerTable.CodeMoney=" + str(
                    money) + " AND Operations.CodeReport=3 AND PowerTable.CodeOperation=Operations.Id"
                res = self.getFromDataBase(sql)
                if res[0][0] != None:
                    summaDollD[str(money)] = res[0][0]
                else:
                    summaDollD[str(money)] = 0

            summaUpD = {}
            for money in moneyId:
                sql = "SELECT SUM(PowerTable.Summa) FROM PowerTable,Operations WHERE (PowerTable.Date >= '" + dateBegin + "' AND " + "PowerTable.Date <= '" + dateEnd + "') AND PowerTable.CodeTicket=3 AND PowerTable.CodeMoney=" + str(
                    money) + " AND Operations.CodeReport=3 AND PowerTable.CodeOperation=Operations.Id"
                res = self.getFromDataBase(sql)
                if res[0][0] != None:
                    summaUpD[str(money)] = res[0][0]
                else:
                    summaUpD[str(money)] = 0

            summaFocD = {}
            for money in moneyId:
                sql = "SELECT SUM(Periodica.Summa) FROM Periodica WHERE (Periodica.Date >= '" + dateBegin + "' AND " + "Periodica.Date <= '" + dateEnd + "') AND Periodica.CodeMoney=" + str(
                    money)
                res = self.getFromDataBase(sql)
                if res[0][0] != None:
                    summaFocD[str(money)] = res[0][0]
                else:
                    summaFocD[str(money)] = 0

            utctime = int(time.mktime(time.strptime(strDateFrom + ' 00:00:00', '%Y-%m-%d %H:%M:%S')))
            utctime = utctime - 86400
            beginDate = time.strftime("%d %b %Y", time.localtime(utctime))
            utctime = int(time.mktime(time.strptime(strDateTo + ' 00:00:00', '%Y-%m-%d %H:%M:%S')))
            endDate = time.strftime("%d %b %Y", time.localtime(utctime))
            utctime = int(time.mktime(time.strptime(strDateFrom + ' 00:00:00', '%Y-%m-%d %H:%M:%S')))
            beginDatePeriod = time.strftime("%d %b %Y", time.localtime(utctime))

            summaBeginer = {}
            for money in moneyId:
                summaBeginer[str(money)] = summaInD[str(money)] + summaUpD[str(money)] - summaDollD[str(money)] - summaFocD[str(money)]

            for money in moneyId:
                summaBeginer[str(money)] += beginSumma[str(money)]

            strSummBegin = ''
            for i, lt in enumerate(moneyId):
                summ = summaBeginer[str(lt)]
                if summ != 0:
                    strSummBegin += str(round(summ, 2)) + moneyCode[i].lower() + ';'

            self.tableAudit6.clearContents()
            self.tableAudit6.setRowCount(6)
            self.tableAudit6.setItem(0, 0, QTableWidgetItem("Остаток средств местной общины на:"))
            self.tableAudit6.setItem(0, 1, QTableWidgetItem(beginDate))
            self.tableAudit6.setItem(0, 2, QTableWidgetItem(""))
            self.tableAudit6.setItem(0, 3, QTableWidgetItem(strSummBegin))

            self.tableAudit6.setItem(1, 0, QTableWidgetItem("Приход средств в местную община за:"))
            self.tableAudit6.setItem(1, 1, QTableWidgetItem(beginDatePeriod))
            self.tableAudit6.setItem(1, 2, QTableWidgetItem(endDate))

            strSummIn = ''
            for i,lt in enumerate(moneyId):
                summ = summaIn[str(lt)]
                if summ != 0:
                    strSummIn += str(round(summ,2)) + moneyCode[i].lower() + ';'
            self.tableAudit6.setItem(1, 3, QTableWidgetItem(strSummIn))

            self.tableAudit6.setItem(2, 0, QTableWidgetItem("Возврат авансовых средств в местную община за:"))
            self.tableAudit6.setItem(2, 1, QTableWidgetItem(beginDatePeriod))
            self.tableAudit6.setItem(2, 2, QTableWidgetItem(endDate))

            strSummUp = ''
            for i, lt in enumerate(moneyId):
                summ = summaUp[str(lt)]
                if summ != 0:
                    strSummUp += str(round(summ,2)) + moneyCode[i].lower() + ';'
            self.tableAudit6.setItem(2, 3, QTableWidgetItem(strSummUp))
            self.tableAudit6.setItem(3, 0, QTableWidgetItem("Расходы средств в местной общине за:"))
            self.tableAudit6.setItem(3, 1, QTableWidgetItem(beginDatePeriod))
            self.tableAudit6.setItem(3, 2, QTableWidgetItem(endDate))

            strSummDoll = ''
            for i, lt in enumerate(moneyId):
                summ = summaDoll[str(lt)]
                if summ != 0:
                    strSummDoll += str(round(summ,2)) + moneyCode[i].lower() + ';'
            self.tableAudit6.setItem(3, 3, QTableWidgetItem(strSummDoll))
            self.tableAudit6.setItem(4, 0, QTableWidgetItem("Отчисления в ЦК местной общиной за:"))
            self.tableAudit6.setItem(4, 1, QTableWidgetItem(beginDatePeriod))
            self.tableAudit6.setItem(4, 2, QTableWidgetItem(endDate))

            strSummFoc = ''
            for i, lt in enumerate(moneyId):
                summ = summaFoc[str(lt)]
                if summ != 0:
                    strSummFoc += str(round(summ,2)) + moneyCode[i].lower() + ';'
            self.tableAudit6.setItem(4, 3, QTableWidgetItem(strSummFoc))
            self.tableAudit6.setItem(5, 0, QTableWidgetItem("Остаток средств местной общины на:"))
            self.tableAudit6.setItem(5, 1, QTableWidgetItem(""))
            self.tableAudit6.setItem(5, 2, QTableWidgetItem(endDate))

            summaEnder = {}
            for money in moneyId:
                summaEnder[str(money)] = summaBeginer[str(money)] + summaIn[str(money)] + summaUp[str(money)] - summaDoll[str(money)] - summaFoc[str(money)]

            strSummSub = ''
            for i, lt in enumerate(moneyId):
                summ = summaEnder[str(lt)]
                if summ != 0:
                    strSummSub += str(round(summ,2)) + moneyCode[i].lower() + ';'

            self.tableAudit6.setItem(5, 3, QTableWidgetItem(strSummSub))
            self.tableAudit6.resizeColumnsToContents()
            self.tableAudit6.resizeRowsToContents()
        except Exception as ex:
            showError("Error generate report audit!", "OnGenerAudit", str(ex) + "\n" + traceback.format_exc())
        return

    def CalculatingTable(self, namecol, auditKonfTable, columnTable1, tableAudit1):
        checkList = []
        dateDict = {}
        for ls in auditKonfTable:
            checkList.append(ls[0])
            dateDict[ls[0]] = ls[4]
        checks = list(set(checkList))
        checks.sort(reverse=False)
        columnTable1.clear()
        tableAudit1.clearContents()
        tableAudit1.clear()
        columnTable1.append(namecol)
        for ls in checks:
            utc = int(time.mktime(time.strptime(dateDict[ls], '%Y-%m-%d')))
            headerDate = time.strftime("%d-%m-%Y", time.localtime(utc))
            columnTable1.append(ls + "\n\r" + headerDate)
        columnTable1.append("Итого")
        tableAudit1.setColumnCount(len(columnTable1))
        tableAudit1.setHorizontalHeaderLabels(columnTable1)
        for i, lt in enumerate(columnTable1):
            tableAudit1.horizontalHeaderItem(i).setToolTip(lt)
            tableAudit1.horizontalHeaderItem(i).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        ticket = {}
        for k, ls in enumerate(checks):
            ticket[ls] = {}
            for p, lt in enumerate(auditKonfTable):
                if ls == lt[0]:
                    if lt[1] not in ticket[ls]:
                        ticket[ls][lt[1]] = {}
                    ticket[ls][lt[1]][lt[3]] = lt[2]
        operateList = []
        for lr in auditKonfTable:
            operateList.append(lr[1])
        operate = list(set(operateList))
        operate.sort(reverse=False)
        tableAudit1.setRowCount(len(operate) + 1)
        for i, ls in enumerate(operate):
            tableAudit1.setItem(i, 0, QTableWidgetItem(ls))
        tableAudit1.resizeColumnsToContents()
        tableAudit1.resizeRowsToContents()
        moneyList = []
        for lr in auditKonfTable:
            moneyList.append(lr[3])
        money = list(set(moneyList))
        isumma = {}
        for lp in money:
            isumma[lp] = 0
        opsumma = {}
        for lk in operate:
            opsumma[lk] = {}
            for lp in money:
                opsumma[lk][lp] = 0
        opsumma["Всего"] = {}
        for lp in money:
            opsumma["Всего"][lp] = 0
        for i, ls in enumerate(checks):
            chck = ticket[ls]
            for lr in money:
                isumma[lr] = 0
            for j, lt in enumerate(operate):
                if lt in chck:
                    oprt = chck[lt]
                    summa = ''
                    for p, lr in enumerate(money):
                        if lr in oprt:
                            isumma[lr] += oprt[lr]
                            opsumma[lt][lr] += oprt[lr]
                            summa += str(round(oprt[lr], 2)) + lr.lower() + ';'
                    tableAudit1.setItem(j, i + 1, QTableWidgetItem(summa))
            strsumma = ''
            for p, lr in enumerate(money):
                if lr in isumma:
                    if isumma[lr] != 0:
                        opsumma["Всего"][lr] += isumma[lr]
                        strsumma += str(round(isumma[lr], 2)) + lr.lower() + ';'
            tableAudit1.setItem(len(operate), i + 1, QTableWidgetItem(strsumma))
        for j, lt in enumerate(operate):
            strsumma = ''
            for p, lr in enumerate(money):
                if opsumma[lt][lr] != 0:
                    strsumma += str(round(opsumma[lt][lr], 2)) + lr.lower() + ';'
            tableAudit1.setItem(j, len(columnTable1) - 1, QTableWidgetItem(strsumma))
        strsumma = ''
        for p, lr in enumerate(money):
            if opsumma["Всего"][lr] != 0:
                strsumma += str(round(opsumma["Всего"][lr], 2)) + lr.lower() + ';'
        tableAudit1.setItem(len(operate), len(columnTable1) - 1, QTableWidgetItem(strsumma))
        tableAudit1.setItem(len(operate), 0, QTableWidgetItem('Всего:'))
        tableAudit1.resizeColumnsToContents()
        tableAudit1.resizeRowsToContents()
        return

    def CalculatingTableOut(self, auditKonfTable, colTable, tableAudit1):
        checkList = ["Квитанция","Название расходов","Сумма","Дата"]
        tableAudit1.clearContents()
        tableAudit1.clear()
        tableAudit1.setColumnCount(len(checkList))
        tableAudit1.setHorizontalHeaderLabels(checkList)
        for i, lt in enumerate(checkList):
            colTable.append(lt)
            tableAudit1.horizontalHeaderItem(i).setToolTip(lt)
            tableAudit1.horizontalHeaderItem(i).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        tableAudit1.setRowCount(len(auditKonfTable) + 1)
        i = 0
        for i, ls in enumerate(auditKonfTable):
            tableAudit1.setItem(i, 0, QTableWidgetItem(ls[0]))
            tableAudit1.setItem(i, 1, QTableWidgetItem(ls[1]))
            tableAudit1.setItem(i, 2, QTableWidgetItem(str(ls[2]) + ls[3].lower()))
            tableAudit1.setItem(i, 3, QTableWidgetItem(ls[4]))
        tableAudit1.setItem(i + 1, 0, QTableWidgetItem("Итого:"))
        tableAudit1.setItem(i + 1, 1, QTableWidgetItem(""))
        moneyList = []
        for lk in auditKonfTable:
            moneyList.append(lk[3])
        moneyList = list(set(moneyList))
        money = {}
        for lk in moneyList:
            money[lk] = 0
        for ls in auditKonfTable:
            for lt in moneyList:
                if ls[3] == lt:
                    money[ls[3]] += ls[2]
        strsumma = ""
        for ls in moneyList:
            strsumma += (str(round(money[ls], 2)) + ls.lower() + ";")
        tableAudit1.setItem(i + 1, 2, QTableWidgetItem(strsumma))
        tableAudit1.setItem(i + 1, 3, QTableWidgetItem(""))
        tableAudit1.resizeColumnsToContents()
        tableAudit1.resizeRowsToContents()
        return

    @pyqtSlot()
    def OnTypeGenerate(self):
        if self.checkBoxGenerate.checkState() == False:
            self.labelCheck.setText("Минимальный экспорт:")
        else:
            self.labelCheck.setText("Полный экспорт:")
        return

    @pyqtSlot()
    def OnExportKonfer(self):
        try:
            if self.tableKonfer.rowCount() == 0:
                return
            old_format = self.dateFromKonfer.displayFormat()
            self.dateFromKonfer.setDisplayFormat("MMMM yyyy")
            dateFrom = self.dateFromKonfer.date()
            monthName = dateFrom.toString(self.dateFromKonfer.displayFormat())
            self.dateFromKonfer.setDisplayFormat(old_format)
            wb = xlwt.Workbook()
            ws = wb.add_sheet('Отчет в конференцию')
            ws.write(0, 0, "Ежемесячный отчет кассира общины")
            ws.write(0, 2, "Отчет по приношениям")
            ws.write(1, 0, "(передаются в конференцию)")
            ws.write(1, 2, "(остаются в местной общине)")
            for k in range(self.tableKonfer.rowCount()):
                cell0 = self.tableKonfer.item(k, 0).text()
                cell1 = self.tableKonfer.item(k, 1).text()
                ws.write(k + 2, 0, cell0)
                ws.write(k + 2, 1, cell1)
            ws.write(k + 3, 0, "Сдал кассир (ФИО):")
            sql = "SELECT NameShort FROM Casher"
            res = self.getFromDataBase(sql)
            ws.write(k + 3, 1, res[0][0])
            ws.write(k + 4, 0, "Подпись кассира:")
            ws.write(k + 4, 1, "________________")
            if self.checkBoxGenerate.isChecked() == True:
                for k in range(self.tablePublic.rowCount()):
                    cell0 = self.tablePublic.item(k, 0).text()
                    cell1 = self.tablePublic.item(k, 1).text()
                    ws.write(k + 2, 2, cell0)
                    ws.write(k + 2, 3, cell1)
                ws.write(k + 3, 2, "Принял (дата):")
                ws.write(k + 4, 2, "Подпись:")
                ws.write(k + 3, 3, "_____________")
                ws.write(k + 4, 3, "_____________")
            else:
                k = self.tablePublic.rowCount()
                t = self.tableKonfer.rowCount()
                cell00 = self.tablePublic.item(0, 0).text()
                cell01 = self.tablePublic.item(0, 1).text()
                cell11 = self.tablePublic.item(k - 2, 0).text()
                cell12 = self.tablePublic.item(k - 2, 1).text()
                cell22 = self.tablePublic.item(k - 1, 0).text()
                cell23 = self.tablePublic.item(k - 1, 1).text()
                ws.write(2, 2, cell00)
                ws.write(2, 3, cell01)
                ws.write(3, 2, cell11)
                ws.write(3, 3, cell12)
                ws.write(t + 1, 2, cell22)
                ws.write(t + 1, 3, cell23)
                ws.write(t + 2, 2, "Принял (дата):")
                ws.write(t + 3, 2, "Подпись:")
                ws.write(t + 2, 3, "_____________")
                ws.write(t + 3, 3, "_____________")
            namefile = 'Отчет_в_конференцию_' + monthName + '.xls'
            wb.save(namefile)
            showInfo("Info export", "Save data is successfull!", namefile)
        except Exception as ex:
            showError("Error delete records", " OnExportKonfer", str(ex))

    @pyqtSlot()
    def OnGenerKonfer(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow. OnGenerKonfer", "Data base is clousing!", str(self.connSqlite3))
                return
            dateFrom = self.dateFromKonfer.date()
            strDateFrom = dateFrom.toString(self.dateFromKonfer.displayFormat())
            dateTo = self.dateToKonfer.date()
            strDateTo = dateTo.toString(self.dateToKonfer.displayFormat())
            sql = "SELECT PowerTable.CodeOperation,PowerTable.Summa,PowerTable.CodeMoney FROM PowerTable, Operations WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND Operations.CodeReport=2 AND PowerTable.CodeOperation=Operations.Id"
            KonferTable = self.getFromDataBase(sql)
            sql = "SELECT DISTINCT PowerTable.CodeOperation FROM PowerTable, Operations WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND Operations.CodeReport=2 AND PowerTable.CodeOperation=Operations.Id"
            arrayOperation = self.getFromDataBase(sql)
            sql = "SELECT * FROM Money"
            result = self.getFromDataBase(sql)
            moneyCode = []
            moneyId = []
            for ls in result:
                moneyCode.append(ls[1])
                moneyId.append(ls[0])
            sql = "SELECT Id,Name FROM Operations WHERE CodeTicket=1"
            result = self.getFromDataBase(sql)
            operatCodeOffer = []
            operatIDOffer = []
            for ls in result:
                operatCodeOffer.append(ls[1])
                operatIDOffer.append(ls[0])
            sql = 'SELECT Summa,CodeMoney FROM Periodica WHERE ' + "(Date >= '" + strDateFrom + "' AND " + "Date <= '" + strDateTo + "')"
            result = self.getFromDataBase(sql)
            summaPeriodica = [0 for i in range(len(moneyId))]
            summaMoney = ['' for i in range(len(moneyId))]
            for lt in moneyId:
                for ls in result:
                    if lt == ls[1]:
                        summaPeriodica[lt - 1] += ls[0]
                        summaMoney[lt - 1] = moneyCode[moneyId.index(lt)]
            periodicaStr = ''
            for i, lt in enumerate(summaPeriodica):
                if lt != 0:
                    periodicaStr += str(lt) + summaMoney[i].lower() + ";"
            dictSumma = {}
            for lk in arrayOperation:
                name = str(lk[0])
                dictSumma[name] = []
                for lt in moneyId:
                    money = [0,lt]
                    dictSumma[name].append(money)
                for ls in KonferTable:
                    if lk[0] == ls[0]:
                        for lt in moneyId:
                            if lt == ls[2]:
                                dictSumma[name][lt - 1][0] += ls[1]
                                break
            self.tableKonfer.clearContents()
            self.tableKonfer.setRowCount(len(arrayOperation) + 4)
            self.tableKonfer.setItem(0, 0, QTableWidgetItem("Название общины:"))
            self.tableKonfer.setItem(0, 1, QTableWidgetItem("ЦК АСД Боярка"))
            old_format = self.dateFromKonfer.displayFormat()
            self.dateFromKonfer.setDisplayFormat("MMMM yyyy")
            monthName = dateFrom.toString(self.dateFromKonfer.displayFormat())
            self.dateFromKonfer.setDisplayFormat(old_format)
            self.tableKonfer.setItem(1, 0, QTableWidgetItem("Дата:"))
            self.tableKonfer.setItem(1, 1, QTableWidgetItem(monthName))
            row = 2
            for ls in arrayOperation:
                nameoper = operatCodeOffer[operatIDOffer.index(ls[0])]
                name = str(ls[0])
                summaCurrency = ''
                for lt in dictSumma[name]:
                    if lt[0] != 0:
                        summa = str(lt[0])
                        currency = moneyCode[moneyId.index(lt[1])]
                        summaCurrency += summa + currency.lower() + ";"
                    if row == 4:
                        self.tableKonfer.setItem(row, 0, QTableWidgetItem("Периодика"))
                        self.tableKonfer.setItem(row, 1, QTableWidgetItem(periodicaStr))
                        row += 1
                self.tableKonfer.setItem(row, 0, QTableWidgetItem(nameoper))
                self.tableKonfer.setItem(row, 1, QTableWidgetItem(summaCurrency))
                row += 1
            summaTemp = [0 for i in range(len(moneyId))]
            moneyTemp = [0 for i in range(len(moneyId))]
            for ls in arrayOperation:
                name = str(ls[0])
                k = 0
                for lt in dictSumma[name]:
                    summaTemp[k] += lt[0]
                    moneyTemp[k] = moneyCode[moneyId.index(lt[1])]
                    k += 1
            for i,ls in enumerate(summaPeriodica):
                summaTemp[i] += ls
            summaStr = ''
            for i,lt in enumerate(summaTemp):
                if lt != 0:
                    summaStr += str(lt) + moneyTemp[i].lower() + ";"
            self.tableKonfer.setItem(row, 0, QTableWidgetItem("Всего:"))
            self.tableKonfer.setItem(row, 1, QTableWidgetItem(summaStr))
            self.tableKonfer.resizeColumnsToContents()
            self.tableKonfer.resizeRowsToContents()
            self.lineSummKonfer.setText(summaStr)
            dateFrom = self.dateFromKonfer.date()
            strDateFrom = dateFrom.toString(self.dateFromKonfer.displayFormat())
            dateTo = self.dateToKonfer.date()
            strDateTo = dateTo.toString(self.dateToKonfer.displayFormat())
            sql = "SELECT PowerTable.CodeOperation,PowerTable.Summa,PowerTable.CodeMoney FROM PowerTable, Operations WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND Operations.CodeReport=1 AND PowerTable.CodeOperation=Operations.Id"
            PublicTable = self.getFromDataBase(sql)
            sql = "SELECT DISTINCT PowerTable.CodeOperation FROM PowerTable, Operations WHERE (PowerTable.Date >= '" + strDateFrom + "' AND " + "PowerTable.Date <= '" + strDateTo + "') AND Operations.CodeReport=1 AND PowerTable.CodeOperation=Operations.Id"
            arrayOperation = self.getFromDataBase(sql)
            dictSumma = {}
            for lk in arrayOperation:
                name = str(lk[0])
                dictSumma[name] = []
                for lt in moneyId:
                    money = [0, lt]
                    dictSumma[name].append(money)
                for ls in PublicTable:
                    if lk[0] == ls[0]:
                        for lt in moneyId:
                            if lt == ls[2]:
                                dictSumma[name][lt - 1][0] += ls[1]
                                break
            self.tablePublic.clearContents()
            self.tablePublic.setRowCount(len(arrayOperation) + 2)
            row = 0
            for ls in arrayOperation:
                nameoper = operatCodeOffer[operatIDOffer.index(ls[0])]
                name = str(ls[0])
                summaCurrency = ''
                for lt in dictSumma[name]:
                    if lt[0] != 0:
                        summa = str(lt[0])
                        currency = moneyCode[moneyId.index(lt[1])]
                        summaCurrency += summa + currency.lower() + ";"
                self.tablePublic.setItem(row, 0, QTableWidgetItem(nameoper))
                self.tablePublic.setItem(row, 1, QTableWidgetItem(summaCurrency))
                row += 1
            summaTemp = [0 for i in range(len(moneyId))]
            moneyTemp = [0 for i in range(len(moneyId))]
            for ls in arrayOperation:
                name = str(ls[0])
                k = 0
                for lt in dictSumma[name]:
                    summaTemp[k] += lt[0]
                    moneyTemp[k] = moneyCode[moneyId.index(lt[1])]
                    k += 1
            summaStr = ''
            for i, lt in enumerate(summaTemp):
                if lt != 0:
                    summaStr += str(lt) + moneyTemp[i].lower() + ";"
            summaTemp = [0 for i in range(len(moneyId))]
            moneyTemp = [0 for i in range(len(moneyId))]
            for k,ls in enumerate(arrayOperation):
                if ls[0] == 2:
                    del arrayOperation[k]
                    break
            if '2' in dictSumma:
                del dictSumma['2']
            for ls in arrayOperation:
                name = str(ls[0])
                k = 0
                for lt in dictSumma[name]:
                    summaTemp[k] += lt[0]
                    moneyTemp[k] = moneyCode[moneyId.index(lt[1])]
                    k += 1
            celesterStr = ''
            for i, lt in enumerate(summaTemp):
                if lt != 0:
                    celesterStr += str(lt) + moneyTemp[i].lower() + ";"
            self.tablePublic.setItem(row, 0, QTableWidgetItem("Целевых приношений:"))
            self.tablePublic.setItem(row, 1, QTableWidgetItem(celesterStr))
            self.tablePublic.setItem(row + 1, 0, QTableWidgetItem("Всего:"))
            self.tablePublic.setItem(row + 1, 1, QTableWidgetItem(summaStr))
            self.tablePublic.resizeColumnsToContents()
            self.tablePublic.resizeRowsToContents()
            self.lineSummPublic.setText(summaStr)
        except Exception as ex:
            showError("Error delete records", " OnGenerKonfer", str(ex))

    @pyqtSlot()
    def OnListerDelete(self):
        try:
            if len(self.listUpdateRecordsLister) > 0:
                if showInfo("OnListerDelete", "Удалить отмеченные строки?",
                            "Отмеченные для удаления строки: " + str(self.listUpdateViewLister)) == 1024:
                    for k in self.listUpdateRecordsLister:
                        id = int(self.tableLister.item(k, 0).text())
                        sqlu = 'DELETE FROM ListerOfferings WHERE ID=' + str(id)
                        self.writeToDataBase(sqlu)
                    self.listUpdateRecordsLister.clear()
                    self.listUpdateViewLister.clear()
                    self.OnListerRead()
        except Exception as ex:
            showError("Error delete records", "OnListerDelete", str(ex))
        self.listUpdateRecordsLister.clear()
        self.listUpdateViewLister.clear()
        self.btnListerSave.setText("Сохранить")
        self.btnListerSave.setToolTip("Сохранить")
        return

    @pyqtSlot()
    def OnListerSave(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow.OnPowersSave", "Data base is clousing!", str(self.connSqlite3))
                return
            sql = "SELECT * FROM Operations WHERE CodeTicket=1"
            result = self.getFromDataBase(sql)
            operatCode = []
            operatID = []
            for ls in result:
                operatCode.append(ls[1])
                operatID.append(ls[0])
            sql = "SELECT * FROM Money"
            result = self.getFromDataBase(sql)
            moneyCode = []
            moneyID = []
            for ls in result:
                moneyCode.append(ls[1])
                moneyID.append(ls[0])
            for k in self.listUpdateRecordsLister:
                id = int(self.tableLister.item(k, 0).text())
                name = self.tableLister.item(k, 1).text()
                mainWidget = self.tableLister.cellWidget(k, 2)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeOper = operatID[comboBox.currentIndex()]
                summa = self.tableLister.item(k, 3).text()
                mainWidget = self.tableLister.cellWidget(k, 4)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeMoney = moneyID[comboBox.currentIndex()]
                mainWidget = self.tableLister.cellWidget(k, 5)
                calendar = mainWidget.layout().itemAt(0).widget()
                strdate = calendar.date()
                str_Date = strdate.toString("yyyy-MM-dd")
                comment = self.tableLister.item(k, 6).text()
                sql = "UPDATE ListerOfferings SET ID=" + str(id) + ",Name='" + name + "',CodeOperation=" + str(codeOper) + \
                    ",Summa=" + str(summa) + ",Currency=" + str(codeMoney) + ",Date='" + str_Date + "',Comment='" + comment + "'" + 'WHERE ID=' + str(id)
                self.writeToDataBase(sql)
            self.listUpdateRecordsPower.clear()
            self.listUpdateViewPower.clear()

            for k in self.listNewRecordsLister:
                id = int(self.tableLister.item(k, 0).text())
                name = self.tableLister.item(k, 1).text()
                self.currentName = name
                mainWidget = self.tableLister.cellWidget(k, 2)
                comboBox = mainWidget.layout().itemAt(0).widget()
                comboBox.currentIndexChanged.connect(self.comboListOperatActivated)
                codeOper = operatID[comboBox.currentIndex()]
                summa = self.tableLister.item(k, 3).text()
                mainWidget = self.tableLister.cellWidget(k, 4)
                comboBox = mainWidget.layout().itemAt(0).widget()
                comboBox.currentIndexChanged.connect(self.comboListMoneyActivated)
                codeMoney = moneyID[comboBox.currentIndex()]
                mainWidget = self.tableLister.cellWidget(k, 5)
                calendar = mainWidget.layout().itemAt(0).widget()
                strdate = calendar.date()
                str_Date = strdate.toString("yyyy-MM-dd")
                comment = self.tableLister.item(k, 6).text()
                sql = "INSERT INTO ListerOfferings(ID,Name,CodeOperation,Summa,Currency,Date,Comment) VALUES (" + \
                    str(id) + ",'" + name + "'," + str(codeOper) + "," + str(summa) + "," + str(codeMoney) + ",'" + str_Date + "','" + comment + "')"
                self.writeToDataBase(sql)
            sqlm = "SELECT * FROM Money"
            codeMoney = self.getFromDataBase(sqlm)
            moneyCodes = []
            for lm in codeMoney:
                moneyCodes.append(lm[1])
            arraySumma = [0 for i in range(len(moneyCodes))]
            arrayMoney = ['' for i in range(len(moneyCodes))]
            for i in range(self.tableLister.rowCount()):
                summa = self.tableLister.item(i, 3).text()
                mainWidget = self.tableLister.cellWidget(i, 4)
                comboBox = mainWidget.layout().itemAt(0).widget()
                arraySumma[comboBox.currentIndex()] += round(float(summa), 2)
                arrayMoney[comboBox.currentIndex()] = comboBox.currentText().lower()
            summas = ''
            for i, ls in enumerate(arraySumma):
                if ls != 0:
                    summas += str(round(ls, 2)) + arrayMoney[i] + ";"
            self.lineSummaLister.setText(summas)
            self.listNewRecordsLister.clear()
            self.tableLister.resizeColumnsToContents()
            self.tableLister.resizeRowsToContents()
            self.btnListerSave.setText("Сохранить")
            self.btnListerSave.setToolTip("Сохранить")
        except Exception as ex:
            showError("Error read from data base", "OnListerSave", str(ex) + traceback.format_exc())

    @pyqtSlot()
    def OnListerCancel(self):
        self.OnListerRead()
        return

    @pyqtSlot()
    def OnListerAdd(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow. OnListerAdd", "Data base is clousing!", str(self.connSqlite3))
                return
            self.IndexRowLister = self.tableLister.rowCount()
            self.tableLister.insertRow(self.IndexRowLister)
            self.maxListerID += 1
            sql = "SELECT * FROM Operations WHERE CodeTicket=1"
            result = self.getFromDataBase(sql)
            operatCodeOffer = []
            operatIDOffer = []
            for ls in result:
                operatCodeOffer.append(ls[1])
                operatIDOffer.append(ls[0])
            sql = "SELECT * FROM Money"
            result = self.getFromDataBase(sql)
            moneyCode = []
            moneyId = []
            for ls in result:
                moneyCode.append(ls[1])
                moneyId.append(ls[0])
            self.tableLister.setItem(self.IndexRowLister, 0, QTableWidgetItem(str(self.maxListerID)))
            self.tableLister.setItem(self.IndexRowLister, 1, QTableWidgetItem(self.currentName))
            completer = QCompleter(operatCodeOffer)
            completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
            completer.popup().setStyleSheet("background-color: yellow")
            comboBoxWidget = QWidget()
            operatCombo = QComboBox()
            operatCombo.setEditable(True)
            operatCombo.setToolTip("Выберите вид операции")
            operatCombo.setCompleter(completer)
            operatCombo.addItems(operatCodeOffer)
            operatCombo.setCurrentIndex(operatIDOffer.index(555))
            operatCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            operatCombo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(operatCombo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tableLister.setCellWidget(self.IndexRowLister, 2, comboBoxWidget)
            self.tableLister.setItem(self.IndexRowLister, 3, QTableWidgetItem("0.00"))
            self.tableLister.setCellWidget(self.IndexRowLister, 4, self.getComboBox(moneyCode, 0, "Выберите код валюты"))
            calenadarWidget = QWidget()
            lineCalenader = QDateTimeEdit(self)
            lineCalenader.setDisplayFormat("dd-MM-yyyy")
            strdate = self.dateLister.date()
            _strDate = strdate.toString("dd-MM-yyyy")
            lineCalenader.setDate(QDate.fromString(_strDate, "dd-MM-yyyy"))
            lineCalenader.setCalendarPopup(True)
            layoutCalendBox = QHBoxLayout(calenadarWidget)
            layoutCalendBox.addWidget(lineCalenader)
            layoutCalendBox.setAlignment(Qt.AlignCenter)
            layoutCalendBox.setContentsMargins(0, 0, 0, 0)
            self.tableLister.setCellWidget(self.IndexRowLister, 5, calenadarWidget)
            self.tableLister.setItem(self.IndexRowLister, 6, QTableWidgetItem(str(" ")))
            self.tableLister.resizeColumnsToContents()
            self.tableLister.resizeRowsToContents()
            self.listNewRecordsLister.append(self.IndexRowLister)
            item = self.tableLister.item(self.IndexRowLister, 1)
            self.tableLister.selectRow(self.IndexRowLister)
            self.tableLister.scrollToItem(item)
            self.IndexRowLister += 1
            self.btnListerSave.setText("*Сохранить: новую строку")
            self.btnListerSave.setToolTip("*Сохранить: новую строку")
        except Exception as err:
            showError("Error read from data base", "OnListerAdd", str(err) + traceback.format_exc())

    @pyqtSlot()
    def comboListMoneyActivated(self):
        combo = self.sender()
        row = 0
        for row in range(self.tableLister.rowCount()):
            mainWidget = self.tableLister.cellWidget(row, 4)
            comboBoxMoney = mainWidget.layout().itemAt(0).widget()
            if comboBoxMoney == combo:
                break
        self.btnListerSave.setText("*Сохранить: изменено (" + str(row + 1) + ":" + str(4) + ")")
        self.btnListerSave.setToolTip("*Сохранить: изменено (" + str(row + 1) + ":" + str(4) + ")")
        return

    @pyqtSlot()
    def comboListOperatActivated(self):
        combo = self.sender()
        row = 0
        for row in range(self.tableLister.rowCount()):
            mainWidget = self.tableLister.cellWidget(row, 2)
            comboBoxOperat = mainWidget.layout().itemAt(0).widget()
            if comboBoxOperat == combo:
                break
        self.btnListerSave.setText("*Сохранить: изменено (" + str(row + 1) + ":" + str(2) + ")")
        self.btnListerSave.setToolTip("*Сохранить: изменено (" + str(row + 1) + ":" + str(2) + ")")
        return

    @pyqtSlot()
    def OnListerRead(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow.OnListerRead", "Data base is clousing!", str(self.connSqlite3))
                return
            sqlid = "SELECT max(ID) FROM ListerOfferings"
            id = self.getFromDataBase(sqlid)
            self.maxListerID = id[0][0]
            sql = "SELECT * FROM Operations WHERE CodeTicket=1"
            result = self.getFromDataBase(sql)
            operatCodeOffer = []
            operatIDOffer = []
            for ls in result:
                operatCodeOffer.append(ls[1])
                operatIDOffer.append(ls[0])
            sql = "SELECT * FROM Money"
            result = self.getFromDataBase(sql)
            moneyCode = []
            moneyId = []
            for ls in result:
                moneyCode.append(ls[1])
                moneyId.append(ls[0])
            dateFrom = self.dateFromLister.date()
            strDateFrom = dateFrom.toString(self.dateFromLister.displayFormat())
            dateTo = self.dateToLister.date()
            strDateTo = dateTo.toString(self.dateToLister.displayFormat())
            sql = "SELECT * FROM ListerOfferings WHERE (Date >= '" + strDateFrom + "' AND " + "Date <= '" + strDateTo + "')"
            listerTable = self.getFromDataBase(sql)
            row = 0
            self.tableLister.clearContents()
            self.tableLister.setRowCount(len(listerTable))
            arraySumma = [0 for i in range(len(moneyCode))]
            arrayMoney = ['' for i in range(len(moneyCode))]
            for ls in listerTable:
                id = ls[0]
                name = ls[1]
                id_offer = ls[2]
                try:
                    codeOper = operatIDOffer.index(id_offer)
                except Exception as ex:
                    showError("Error records '" + str(ls) + "' in operations", "OnListerRead", str(ex))
                    self.tableLister.removeRow(row)
                    continue
                summa = ls[3]
                codeMoney = moneyId.index(ls[4])
                dates = ls[5]
                comment = ls[6]
                self.tableLister.setItem(row, 0, QTableWidgetItem(str(id)))
                self.tableLister.setItem(row, 1, QTableWidgetItem(str(name)))
                completer = QCompleter(operatCodeOffer)
                completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
                completer.popup().setStyleSheet("background-color: yellow")
                comboBoxWidget = QWidget()
                operatCombo = QComboBox()
                operatCombo.setEditable(True)
                operatCombo.setToolTip("Выберите операцию")
                operatCombo.setCompleter(completer)
                operatCombo.addItems(operatCodeOffer)
                operatCombo.setCurrentIndex(codeOper)
                operatCombo.currentIndexChanged.connect(self.comboListOperatActivated)
                operatCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                operatCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(operatCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tableLister.setCellWidget(row, 2, comboBoxWidget)
                self.tableLister.setItem(row, 3, QTableWidgetItem(str(float(summa))))
                comboBoxWidget = QWidget()
                moneyCombo = QComboBox()
                moneyCombo.setEditable(False)
                moneyCombo.setToolTip("Выберите код валюты")
                moneyCombo.addItems(moneyCode)
                moneyCombo.setCurrentIndex(codeMoney)
                moneyCombo.currentIndexChanged.connect(self.comboListMoneyActivated)
                moneyCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                moneyCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(moneyCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tableLister.setCellWidget(row, 4, comboBoxWidget)
                calenadarWidget = QWidget()
                lineCalenader = QDateTimeEdit(self)
                lineCalenader.setDisplayFormat("dd-MM-yyyy")
                lineCalenader.setDate(QDate.fromString(dates, "yyyy-MM-dd"))
                lineCalenader.setCalendarPopup(True)
                layoutCalendBox = QHBoxLayout(calenadarWidget)
                layoutCalendBox.addWidget(lineCalenader)
                layoutCalendBox.setAlignment(Qt.AlignCenter)
                layoutCalendBox.setContentsMargins(0, 0, 0, 0)
                self.tableLister.setCellWidget(row, 5, calenadarWidget)
                self.tableLister.setItem(row, 6, QTableWidgetItem(str(comment)))
                arraySumma[moneyCombo.currentIndex()] += round(float(summa), 2)
                arrayMoney[moneyCombo.currentIndex()] = moneyCombo.currentText().lower()
                row += 1
            summas = ''
            for i, ls in enumerate(arraySumma):
                if ls != 0:
                    summas += str(round(ls, 2)) + arrayMoney[i] + ";"
            self.lineSummaLister.setText(summas)
            self.tableLister.resizeColumnsToContents()
            self.tableLister.resizeRowsToContents()
            self.tableLister.setColumnHidden(0, True)
            self.listUpdateRecordsLister.clear()
            self.listUpdateViewLister.clear()
            self.listNewRecordsLister.clear()
            self.btnListerSave.setText("Сохранить")
            self.btnListerSave.setToolTip("Сохранить")
            self.btnListerAdd.setEnabled(True)
            self.btnListerCancel.setEnabled(True)
            self.btnListerDelete.setEnabled(True)
            self.btnListerSave.setEnabled(True)
        except Exception as ex:
            showError("Error read from data base", "OnListerRead", str(ex) + traceback.format_exc())

    @pyqtSlot()
    def viewClickedLister(self):
        selectRow = self.tableLister.selectionModel().selectedRows()
        for index in sorted(selectRow):
            if index.row() in self.listNewRecordsLister:
                return
            if index.row() not in self.listUpdateRecordsLister:
                self.listUpdateRecordsLister.append(index.row())
                self.listUpdateViewLister.append(index.row() + 1)
                self.btnListerSave.setText("*Сохранить: update строки:" + str(self.listUpdateViewLister))
                self.btnListerSave.setToolTip("*Сохранить: update строки:" + str(self.listUpdateViewLister))
        return

    @pyqtSlot()
    def OnSearchLister(self):
        try:
            row = self.searchRowLister
            while row < self.tableLister.rowCount():
                column = self.searchColumnLister
                while column < self.tableLister.columnCount():
                    item = self.tableLister.item(row, column)
                    if item:
                        try:
                            value = self.lineSearchLister.text()
                            item.text().index(value)
                            self.tableLister.selectRow(row)
                            self.tableLister.scrollToItem(item)
                            self.searchColumnLister = column + 1
                            self.searchRowLister = row
                            return
                        except Exception as ex:
                            pass
                    else:
                        mainWidget = self.tableLister.cellWidget(row, column)
                        comboBox = mainWidget.layout().itemAt(0).widget()
                        if type(comboBox) is QComboBox:
                            texttype = comboBox.currentText()
                            try:
                                value = self.lineSearchLister.text()
                                texttype.index(value)
                                self.tableLister.selectRow(row)
                                self.tableLister.scrollToItem(item)
                                self.searchColumnLister = column + 1
                                self.searchRowLister = row
                                return
                            except Exception as ex:
                                pass
                    column += 1
                self.searchColumnLister = 0
                row += 1
            self.searchRowLister = 0
        except Exception as err:
            self.showError("MainWindow.OnSearchLister", str(err), traceback.format_exc())

    @pyqtSlot()
    def setFilterLister(self):
        textfilt = self.lineFiltLister.text()
        textfilt1 = self.lineFiltLister1.text()

        if textfilt == '' and textfilt1 == '':
            for i in range(self.tableLister.rowCount()):
                self.tableLister.setRowHidden(i, False)
            sqlm = "SELECT * FROM Money"
            codeMoney = self.getFromDataBase(sqlm)
            moneyCodes = []
            for lm in codeMoney:
                moneyCodes.append(lm[1])
            arraySumma = [0 for i in range(len(moneyCodes))]
            arrayMoney = ['' for i in range(len(moneyCodes))]
            for i in range(self.tableLister.rowCount()):
                if not self.tableLister.isRowHidden(i):
                    summa = round(float(self.tableLister.item(i, 3).text()), 2)
                    mainWidget = self.tableLister.cellWidget(i, 4)
                    comboBox = mainWidget.layout().itemAt(0).widget()
                    current = comboBox.currentText()
                    indx = moneyCodes.index(current)
                    arraySumma[indx] += summa
                    arrayMoney[indx] = current.lower()
            summas = ''
            for i, ls in enumerate(arraySumma):
                if ls != 0:
                    summas += str(round(ls, 2)) + arrayMoney[i] + ";"
            self.lineSummaLister.setText(summas)
            return

        for i in range(self.tableLister.rowCount()):
            indxCol = self.filtComboLister.currentIndex() + 1
            indxCol1 = self.filtComboLister1.currentIndex() + 1
            item = self.tableLister.item(i, indxCol)
            text = ''
            if item:
                text = item.text()
            else:
                mainWidget = self.tableLister.cellWidget(i, indxCol)
                objectBox = mainWidget.layout().itemAt(0).widget()
                if type(objectBox) is QComboBox:
                    text = objectBox.currentText()
                elif type(objectBox) is QDateTimeEdit:
                    strdate = objectBox.date()
                    text = strdate.toString("dd-MM-yyyy")
            item = self.tableLister.item(i, indxCol1)
            text1 = ''
            if item:
                text1 = item.text()
            else:
                mainWidget = self.tableLister.cellWidget(i, indxCol1)
                objectBox = mainWidget.layout().itemAt(0).widget()
                if type(objectBox) is QComboBox:
                    text1 = objectBox.currentText()
                elif type(objectBox) is QDateTimeEdit:
                    strdate = objectBox.date()
                    text1 = strdate.toString("dd-MM-yyyy")
            try:
                text.index(textfilt)
                text1.index(textfilt1)
                self.tableLister.setRowHidden(i, False)
            except Exception as ex:
                self.tableLister.setRowHidden(i, True)
        sqlm = "SELECT * FROM Money"
        codeMoney = self.getFromDataBase(sqlm)
        moneyCodes = []
        for lm in codeMoney:
            moneyCodes.append(lm[1])
        arraySumma = [0 for i in range(len(moneyCodes))]
        arrayMoney = ['' for i in range(len(moneyCodes))]
        for i in range(self.tableLister.rowCount()):
            if not self.tableLister.isRowHidden(i):
                summa = round(float(self.tableLister.item(i, 3).text()), 2)
                mainWidget = self.tableLister.cellWidget(i, 4)
                comboBox = mainWidget.layout().itemAt(0).widget()
                current = comboBox.currentText()
                indx = moneyCodes.index(current)
                arraySumma[indx] += summa
                arrayMoney[indx] = current.lower()
        summas = ''
        for i, ls in enumerate(arraySumma):
            if ls != 0:
                summas += str(round(ls, 2)) + arrayMoney[i] + ";"
        self.lineSummaLister.setText(summas)
        return # setFilterLister()

    @pyqtSlot()
    def OnExportLister(self):
        try:
            if self.tableLister.rowCount() == 0:
                return

            dateFrom = self.dateFromLister.date()
            dateBegin = dateFrom.toString(self.dateFromLister.displayFormat())
            dateTo = self.dateTo.date()
            dateEnd = dateTo.toString(self.dateToLister.displayFormat())
            currentDate = datetime.now().strftime('%d-%m-%Y %H:%M:%S')
            wb = xlwt.Workbook()
            ws = wb.add_sheet("Отчет по спискам")
            ws.write(0, 1, "Отчет 'Списки' за период с " + dateBegin + " по " + dateEnd)
            ws.write(1, 1, "Дата генерации: " + currentDate)
            ws.write(2, 0, self.tableLister.horizontalHeaderItem(1).text())
            ws.write(2, 1, self.tableLister.horizontalHeaderItem(2).text())
            ws.write(2, 2, self.tableLister.horizontalHeaderItem(3).text())
            ws.write(2, 3, self.tableLister.horizontalHeaderItem(4).text())
            ws.write(2, 4, self.tableLister.horizontalHeaderItem(5).text())
            ws.write(2, 5, self.tableLister.horizontalHeaderItem(6).text())
            m = 3
            j = 0
            for k in range(self.tableLister.rowCount()):
                if self.tableLister.isRowHidden(k):
                    continue
                name = self.tableLister.item(k, 1).text()
                mainWidget = self.tableLister.cellWidget(k, 2)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeOper = comboBox.currentText()
                summa = self.tableLister.item(k, 3).text()
                mainWidget = self.tableLister.cellWidget(k, 4)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeMoney = comboBox.currentText()
                mainWidget = self.tableLister.cellWidget(k, 5)
                calendar = mainWidget.layout().itemAt(0).widget()
                strdate = calendar.date()
                str_Date = strdate.toString("dd-MM-yyyy")
                comment = self.tableLister.item(k, 6).text()

                ws.write(j + m, 0, name)
                ws.write(j + m, 1, codeOper)
                ws.write(j + m, 2, float(summa))
                ws.write(j + m, 3, codeMoney.lower())
                ws.write(j + m, 4, str_Date)
                ws.write(j + m, 5, comment)
                j += 1

            currDate = datetime.now().strftime('%d-%m-%Y')
            namefile = 'Отчет_списки_' + currDate + '.xls'
            wb.save(namefile)
            showInfo("Info export", "Save data is successfull!", namefile)
        except Exception as ex:
            showError("Error read from data base", "OnExportLister", str(ex) + traceback.format_exc())

    @pyqtSlot()
    def OnExportPower(self):
        try:
            if self.tablePowers.rowCount() == 0:
                return

            dateFrom = self.dateFromPower.date()
            dateBegin = dateFrom.toString(self.dateFromPower.displayFormat())
            dateTo = self.dateTo.date()
            dateEnd = dateTo.toString(self.dateToPower.displayFormat())
            currentDate = datetime.now().strftime('%d-%m-%Y %H:%M:%S')
            wb = xlwt.Workbook()
            ws = wb.add_sheet("Отчет по записям")
            ws.write(0, 1, "Отчет 'Редактирование' за период с " + dateBegin + " по " + dateEnd)
            ws.write(1, 1, "Дата генерации: " + currentDate)
            ws.write(2, 0, self.tablePowers.horizontalHeaderItem(1).text())
            ws.write(2, 1, self.tablePowers.horizontalHeaderItem(2).text())
            ws.write(2, 2, self.tablePowers.horizontalHeaderItem(3).text())
            ws.write(2, 3, self.tablePowers.horizontalHeaderItem(4).text())
            ws.write(2, 4, self.tablePowers.horizontalHeaderItem(5).text())
            ws.write(2, 5, self.tablePowers.horizontalHeaderItem(6).text())
            ws.write(2, 6, self.tablePowers.horizontalHeaderItem(7).text())
            ws.write(2, 7, self.tablePowers.horizontalHeaderItem(8).text())
            ws.write(2, 8, self.tablePowers.horizontalHeaderItem(9).text())
            ws.write(2, 9, self.tablePowers.horizontalHeaderItem(10).text())
            ws.write(2, 10, self.tablePowers.horizontalHeaderItem(11).text())
            m = 3
            for k in range(self.tablePowers.rowCount()):
                numticket = self.tablePowers.item(k, 1).text()
                mainWidget = self.tablePowers.cellWidget(k, 2)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeTick = comboBox.currentText()
                mainWidget = self.tablePowers.cellWidget(k, 3)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeOperat = comboBox.currentText()
                summa = self.tablePowers.item(k, 4).text()
                mainWidget = self.tablePowers.cellWidget(k, 5)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeMoney = comboBox.currentText()
                mainWidget = self.tablePowers.cellWidget(k, 6)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeUser = comboBox.currentText()
                mainWidget = self.tablePowers.cellWidget(k, 7)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeCash = comboBox.currentText()
                mainWidget = self.tablePowers.cellWidget(k, 8)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeCount1 = comboBox.currentText()
                mainWidget = self.tablePowers.cellWidget(k, 9)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeCount2 = comboBox.currentText()
                mainWidget = self.tablePowers.cellWidget(k, 10)
                calendar = mainWidget.layout().itemAt(0).widget()
                strdate = calendar.date()
                str_Date = strdate.toString("yyyy-MM-dd")
                comment = self.tablePowers.item(k, 11).text()

                ws.write(k + m, 0, numticket)
                ws.write(k + m, 1, codeTick)
                ws.write(k + m, 2, codeOperat)
                ws.write(k + m, 3, summa)
                ws.write(k + m, 4, codeMoney)
                ws.write(k + m, 5, codeUser)
                ws.write(k + m, 6, codeCash)
                ws.write(k + m, 7, codeCount1)
                ws.write(k + m, 8, codeCount2)
                ws.write(k + m, 9, str_Date)
                ws.write(k + m, 10, comment)

            currDate = datetime.now().strftime('%d-%m-%Y')
            namefile = 'Отчет_редактирование_' + currDate + '.xls'
            wb.save(namefile)
            showInfo("Info export", "Save data is successfull!", namefile)
        except Exception as ex:
            showError("Error read from data base", "OnExportPower", str(ex) + traceback.format_exc())

    @pyqtSlot()
    def OnPowersDelete(self):
        try:
            if len(self.listUpdateRecordsPower) > 0:
                if showInfo("OnPowersDelete", "Удалить отмеченные строки?",
                            "Отмеченные для удаления строки: " + str(self.listUpdateRecordsPower)) == 1024:
                    for k in self.listUpdateRecordsPower:
                        id = int(self.tablePowers.item(k - 1, 0).text())
                        sqlu = 'DELETE FROM PowerTable WHERE Id=' + str(id)
                        self.writeToDataBase(sqlu)
                    self.listUpdateRecordsPower.clear()
                    self.listUpdateViewPower.clear()
                    self.OnPowersRead()
        except Exception as ex:
            showError("Error delete records", "OnPowersDelete", str(ex))
        self.listUpdateRecordsPower.clear()
        self.listUpdateViewPower.clear()
        self.btnPowersSave.setText("Сохранить")
        self.btnPowersSave.setToolTip("Сохранить")
        return

    @pyqtSlot()
    def OnPowersSave(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow.OnPowersSave", "Data base is clousing!", str(self.connSqlite3))
                return
            sql = "SELECT * FROM CodeTickets"
            result = self.getFromDataBase(sql)
            tickCode = []
            tickId = []
            for ls in result:
                tickCode.append(ls[1])
                tickId.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=1"
            result = self.getFromDataBase(sql)
            operatCodeOffer = []
            operatIDOffer = []
            for ls in result:
                operatCodeOffer.append(ls[1])
                operatIDOffer.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=2"
            result = self.getFromDataBase(sql)
            operatCodeOut = []
            operatIDOut = []
            for ls in result:
                operatCodeOut.append(ls[1])
                operatIDOut.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=3"
            result = self.getFromDataBase(sql)
            operatCodeIn = []
            operatIDIn = []
            for ls in result:
                operatCodeIn.append(ls[1])
                operatIDIn.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=4"
            result = self.getFromDataBase(sql)
            operatCodeRep = []
            operatIDRep = []
            for ls in result:
                operatCodeRep.append(ls[1])
                operatIDRep.append(ls[0])
            sql = "SELECT * FROM Money"
            result = self.getFromDataBase(sql)
            moneyCode = []
            moneyId = []
            for ls in result:
                moneyCode.append(ls[1])
                moneyId.append(ls[0])
            sql = "SELECT * FROM Persones"
            result = self.getFromDataBase(sql)
            personeCode = []
            personeID = []
            for ls in result:
                personeCode.append(ls[1])
                personeID.append(ls[0])
            sql = "SELECT * FROM Casher"
            result = self.getFromDataBase(sql)
            casherCode = []
            casherId = []
            for ls in result:
                casherCode.append(ls[1])
                casherId.append(ls[0])

            for k in self.listUpdateRecordsPower:
                id = int(self.tablePowers.item(k, 0).text())
                numticket = self.tablePowers.item(k, 1).text()
                mainWidget = self.tablePowers.cellWidget(k, 2)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeTick = tickId[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 3)
                comboBox = mainWidget.layout().itemAt(0).widget()

                if codeTick == 1:
                    codeOperat = operatIDOffer[comboBox.currentIndex()]
                elif codeTick == 2:
                    codeOperat = operatIDOut[comboBox.currentIndex()]
                elif codeTick == 3:
                    codeOperat = operatIDIn[comboBox.currentIndex()]
                elif codeTick == 4:
                    codeOperat = operatIDRep[comboBox.currentIndex()]
                else:
                    showError("Error ID in type tickets", "OnPowerSave", "")
                    return

                summa = self.tablePowers.item(k, 4).text()
                mainWidget = self.tablePowers.cellWidget(k, 5)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeMoney = moneyId[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 6)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeUser = personeID[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 7)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeCash = casherId[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 8)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeCount1 = personeID[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 9)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeCount2 = personeID[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 10)
                calendar = mainWidget.layout().itemAt(0).widget()
                strdate = calendar.date()
                str_Date = strdate.toString("yyyy-MM-dd")
                comment = self.tablePowers.item(k, 11).text()
                sql = "UPDATE PowerTable SET Id=" + str(id) + ",NumberTicket='" + numticket + "',CodeTicket=" + str(codeTick) + \
                ",CodeOperation=" + str(codeOperat) + ",Summa=" + str(summa) + ",CodeMoney=" + str(codeMoney) + ",CodeUser=" + \
                str(codeUser) + ",CodeCash=" + str(codeCash) + ",CodeCounter1=" + str(codeCount1) + ",CodeCounter2=" + str(codeCount2) + \
                ",Date='" + str_Date + "',Comment='" + comment + "'" + 'WHERE ID=' + str(id)
                self.writeToDataBase(sql)
            self.listUpdateRecordsPower.clear()
            self.listUpdateViewPower.clear()

            numticket = ""
            for k in self.listNewRecordsPower:
                id = int(self.tablePowers.item(k, 0).text())
                numticket = self.tablePowers.item(k, 1).text()
                mainWidget = self.tablePowers.cellWidget(k, 2)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeTick = tickId[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 3)
                comboBox = mainWidget.layout().itemAt(0).widget()
                comboBox.currentIndexChanged.connect(self.comboOperatActivated)

                if codeTick == 1:
                    codeOperat = operatIDOffer[comboBox.currentIndex()]
                elif codeTick == 2:
                    codeOperat = operatIDOut[comboBox.currentIndex()]
                elif codeTick == 3:
                    codeOperat = operatIDIn[comboBox.currentIndex()]
                elif codeTick == 4:
                    codeOperat = operatIDRep[comboBox.currentIndex()]
                else:
                    showError("Error ID in type tickets", "OnPowerSave", "")
                    return

                summa = self.tablePowers.item(k, 4).text()
                mainWidget = self.tablePowers.cellWidget(k, 5)
                comboBox = mainWidget.layout().itemAt(0).widget()
                comboBox.currentIndexChanged.connect(self.comboMoneyActivated)
                codeMoney = moneyId[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 6)
                comboBox = mainWidget.layout().itemAt(0).widget()
                comboBox.currentIndexChanged.connect(self.comboUserActivated)
                codeUser = personeID[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 7)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeCash = casherId[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 8)
                comboBox = mainWidget.layout().itemAt(0).widget()
                comboBox.currentIndexChanged.connect(self.comboCounter1Activated)
                codeCount1 = personeID[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 9)
                comboBox = mainWidget.layout().itemAt(0).widget()
                comboBox.currentIndexChanged.connect(self.comboCounter2Activated)
                codeCount2 = personeID[comboBox.currentIndex()]
                mainWidget = self.tablePowers.cellWidget(k, 10)
                calendar = mainWidget.layout().itemAt(0).widget()
                strdate = calendar.date()
                str_Date = strdate.toString("yyyy-MM-dd")
                comment = self.tablePowers.item(k, 11).text()
                sql = "INSERT INTO PowerTable(Id,NumberTicket,CodeTicket,CodeOperation,Summa,CodeMoney,CodeUser,CodeCash,CodeCounter1,CodeCounter2,Date,Comment) VALUES (" + \
                str(id) + ",'" + numticket + "'," + str(codeTick) + "," + str(codeOperat) + "," + summa + "," + str(codeMoney) + "," + str(codeUser) + "," + str(codeCash) + "," + \
                str(codeCount1) + "," + str(codeCount2) + ",'" + str_Date + "','" + comment + "')"
                self.writeToDataBase(sql)
            sqlm = "SELECT * FROM Money"
            codeMoney = self.getFromDataBase(sqlm)
            moneyCodes = []
            for lm in codeMoney:
                moneyCodes.append(lm[1])
            arraySumma = [0 for i in range(len(moneyCodes))]
            arrayMoney = ['' for i in range(len(moneyCodes))]
            row = 0
            for i in range(self.tablePowers.rowCount()):
                summa = self.tablePowers.item(i, 4).text()
                mainWidget = self.tablePowers.cellWidget(i, 5)
                comboBox = mainWidget.layout().itemAt(0).widget()
                arraySumma[comboBox.currentIndex()] += round(float(summa), 2)
                arrayMoney[comboBox.currentIndex()] = comboBox.currentText().lower()
                row += 1
            summas = ''
            for i, ls in enumerate(arraySumma):
                if ls != 0:
                    summas += str(round(ls, 2)) + arrayMoney[i] + ";"
            self.lineSummaPower.setText(summas)
            self.lineNumPower.setText(str(self.tablePowers.rowCount()))
            self.lastPowerItem = numticket
            self.listNewRecordsPower.clear()
            self.tablePowers.resizeColumnsToContents()
            self.tablePowers.resizeRowsToContents()
            self.btnPowersSave.setText("Сохранить")
            self.btnPowersSave.setToolTip("Сохранить")
        except Exception as ex:
            showError("Error read from data base", "OnPowersSave", str(ex) + traceback.format_exc())

    @pyqtSlot()
    def OnPowersCancel(self):
        self.OnPowersRead()
        return

    @pyqtSlot()
    def OnPowersAdd(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow. OnPowersAdd", "Data base is clousing!", str(self.connSqlite3))
                return
            self.IndexRowPow = self.tablePowers.rowCount()
            self.tablePowers.insertRow(self.IndexRowPow)
            self.maxPowersID += 1
            sql = "SELECT * FROM CodeTickets"
            result = self.getFromDataBase(sql)
            tickCode = []
            tickId = []
            for ls in result:
                tickCode.append(ls[1])
                tickId.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=1"
            result = self.getFromDataBase(sql)
            operatCodeOffer = []
            operatIDOffer = []
            for ls in result:
                operatCodeOffer.append(ls[1])
                operatIDOffer.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=2"
            result = self.getFromDataBase(sql)
            operatCodeOut = []
            operatIDOut = []
            for ls in result:
                operatCodeOut.append(ls[1])
                operatIDOut.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=3"
            result = self.getFromDataBase(sql)
            operatCodeIn = []
            operatIDIn = []
            for ls in result:
                operatCodeIn.append(ls[1])
                operatIDIn.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=4"
            result = self.getFromDataBase(sql)
            operatCodeRep = []
            operatIDRep = []
            for ls in result:
                operatCodeRep.append(ls[1])
                operatIDRep.append(ls[0])
            sql = "SELECT * FROM Money"
            result = self.getFromDataBase(sql)
            moneyCode = []
            moneyId = []
            for ls in result:
                moneyCode.append(ls[1])
                moneyId.append(ls[0])
            sql = "SELECT * FROM Persones"
            result = self.getFromDataBase(sql)
            personeCode = []
            personeID = []
            for ls in result:
                personeCode.append(ls[1])
                personeID.append(ls[0])
            sql = "SELECT * FROM Casher"
            result = self.getFromDataBase(sql)
            casherCode = []
            casherId = []
            for ls in result:
                casherCode.append(ls[1])
                casherId.append(ls[0])

            self.tablePowers.setItem(self.IndexRowPow, 0, QTableWidgetItem(str(self.maxPowersID)))
            self.tablePowers.setItem(self.IndexRowPow, 1, QTableWidgetItem(self.lastPowerItem))
            comboBoxWidget = QWidget()
            tickCombo = QComboBox()
            tickCombo.setEditable(False)
            tickCombo.setToolTip("Выберите тип квитанции")
            tickCombo.addItems(tickCode)
            tickCombo.setCurrentIndex(self.tickCombo.currentIndex())
            tickCombo.currentIndexChanged.connect(self.comboTicketActivated)
            tickCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            tickCombo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(tickCombo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tablePowers.setCellWidget(self.IndexRowPow, 2, comboBoxWidget)

            type = self.tickCombo.currentIndex() + 1
            if type == 1:
                operatCode = operatCodeOffer
            elif type == 2:
                operatCode = operatCodeOut
            elif type == 3:
                operatCode = operatCodeIn
            elif type == 4:
                operatCode = operatCodeRep
            else:
                showError("Error ID in type tickets", "OnPowerAdd", "")
                return

            completer = QCompleter(operatCode)
            completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
            completer.popup().setStyleSheet("background-color: yellow")
            comboBoxWidget = QWidget()
            operatCombo = QComboBox()
            operatCombo.setEditable(True)
            operatCombo.setToolTip("Выберите операцию")
            operatCombo.setCompleter(completer)
            operatCombo.addItems(operatCode)
            operatCombo.setCurrentIndex(0)
            operatCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            operatCombo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(operatCombo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tablePowers.setCellWidget(self.IndexRowPow, 3, comboBoxWidget)
            self.tablePowers.setItem(self.IndexRowPow, 4, QTableWidgetItem(str(float(0))))
            self.tablePowers.setCellWidget(self.IndexRowPow, 5, self.getComboBox(moneyCode, 0, "Выберите код валюты"))
            completer = QCompleter(personeCode)
            completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
            completer.popup().setStyleSheet("background-color: yellow")
            comboBoxWidget = QWidget()
            userCombo = QComboBox()
            userCombo.setEditable(True)
            userCombo.setToolTip("Выберите ФИО пользователя")
            userCombo.setCompleter(completer)
            userCombo.addItems(personeCode)

            if (tickCombo.currentIndex() + 1) == 1:
                userCombo.setCurrentIndex(personeID.index(69))
            else:
                userCombo.setCurrentIndex(0)

            userCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            userCombo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(userCombo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tablePowers.setCellWidget(self.IndexRowPow, 6, comboBoxWidget)
            self.tablePowers.setCellWidget(self.IndexRowPow, 7, self.getComboBox(casherCode, self.cashCombo.currentIndex(), "Выберите ФИО кассира"))
            completer = QCompleter(personeCode)
            completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
            completer.popup().setStyleSheet("background-color: yellow")
            comboBoxWidget = QWidget()
            counter1Combo = QComboBox()
            counter1Combo.setEditable(True)
            counter1Combo.setToolTip("Выберите ФИО счетчика 1")
            counter1Combo.setCompleter(completer)
            counter1Combo.addItems(personeCode)

            if (tickCombo.currentIndex() + 1) == 1:
                counter1Combo.setCurrentIndex(self.count1Combo.currentIndex())
            else:
                counter1Combo.setCurrentIndex(personeID.index(69))

            counter1Combo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            counter1Combo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(counter1Combo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tablePowers.setCellWidget(self.IndexRowPow, 8, comboBoxWidget)
            completer = QCompleter(personeCode)
            completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
            completer.popup().setStyleSheet("background-color: yellow")
            comboBoxWidget = QWidget()
            counter2Combo = QComboBox()
            counter2Combo.setEditable(True)
            counter2Combo.setToolTip("Выберите ФИО счетчика 2")
            counter2Combo.setCompleter(completer)
            counter2Combo.addItems(personeCode)

            if (tickCombo.currentIndex() + 1) == 1:
                counter2Combo.setCurrentIndex(self.count2Combo.currentIndex())
            else:
                counter2Combo.setCurrentIndex(personeID.index(69))

            counter2Combo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            counter2Combo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(counter2Combo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tablePowers.setCellWidget(self.IndexRowPow, 9, comboBoxWidget)
            calenadarWidget = QWidget()
            lineCalenader = QDateTimeEdit(self)
            lineCalenader.setDisplayFormat("dd-MM-yyyy")
            strdate = self.datePower.date()
            _strDate = strdate.toString("dd-MM-yyyy")
            lineCalenader.setDate(QDate.fromString(_strDate, "dd-MM-yyyy"))
            lineCalenader.setCalendarPopup(True)
            layoutCalendBox = QHBoxLayout(calenadarWidget)
            layoutCalendBox.addWidget(lineCalenader)
            layoutCalendBox.setAlignment(Qt.AlignCenter)
            layoutCalendBox.setContentsMargins(0, 0, 0, 0)
            self.tablePowers.setCellWidget(self.IndexRowPow, 10, calenadarWidget)
            self.tablePowers.setItem(self.IndexRowPow, 11, QTableWidgetItem(str(" ")))
            self.tablePowers.resizeColumnsToContents()
            self.tablePowers.resizeRowsToContents()
            self.listNewRecordsPower.append(self.IndexRowPow)
            item = self.tablePowers.item(self.IndexRowPow, 1)
            self.tablePowers.selectRow(self.IndexRowPow)
            self.tablePowers.scrollToItem(item)
            self.IndexRowPow += 1
            self.btnPowersSave.setText("*Сохранить: новую строку")
            self.btnPowersSave.setToolTip("*Сохранить: новую строку")
        except Exception as err:
            showError("Error read from data base", "OnPowersAdd", str(err) + traceback.format_exc())

    def getComboBox(self, items, code, tip):
        comboBoxWidget = QWidget()
        tickCombo = QComboBox()
        tickCombo.setEditable(False)
        tickCombo.setToolTip(tip)
        tickCombo.addItems(items)
        tickCombo.setCurrentIndex(code)
        tickCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        tickCombo.adjustSize()
        layoutComboBox = QHBoxLayout(comboBoxWidget)
        layoutComboBox.addWidget(tickCombo)
        layoutComboBox.setAlignment(Qt.AlignCenter)
        layoutComboBox.setContentsMargins(0, 0, 0, 0)
        return comboBoxWidget

    @pyqtSlot()
    def comboTicketActivated(self):
        row = 0
        comboBoxTick = ''
        combo = self.sender()
        for row in range(self.tablePowers.rowCount()):
            mainWidget = self.tablePowers.cellWidget(row, 2)
            comboBoxTick = mainWidget.layout().itemAt(0).widget()
            if comboBoxTick == combo:
                break
        indexticket = comboBoxTick.currentIndex() + 1
        mainWidget = self.tablePowers.cellWidget(row, 3)
        comboBoxOperat = mainWidget.layout().itemAt(0).widget()
        sql = "SELECT * FROM Operations WHERE CodeTicket=" + str(indexticket)
        dataoperat = self.getFromDataBase(sql)
        tickCode = []
        tickId = []
        for ls in dataoperat:
            tickCode.append(ls[1])
            tickId.append(ls[0])
        sql = "SELECT * FROM Persones"
        result = self.getFromDataBase(sql)
        personeCode = []
        personeID = []
        for ls in result:
            personeCode.append(ls[1])
            personeID.append(ls[0])
        comboBoxOperat.clear()
        completer = QCompleter(tickCode)
        completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
        completer.popup().setStyleSheet("background-color: yellow")
        comboBoxOperat.setCompleter(completer)
        comboBoxOperat.addItems(tickCode)
        comboBoxOperat.setCurrentIndex(0)
        comboBoxOperat.update()
        self.btnPowersSave.setText("*Сохранить: обновлено (" + str(row + 1) + ":" + str(2) + ")")
        self.btnPowersSave.setToolTip("*Сохранить: обновлено (" + str(row + 1) + ":" + str(2) + ")")
        mainWidget = self.tablePowers.cellWidget(row, 6)
        comboBoxUser = mainWidget.layout().itemAt(0).widget()
        if indexticket == 1:
            comboBoxUser.setCurrentIndex(personeID.index(69))
        else:
            comboBoxUser.setCurrentIndex(0)
        mainWidget = self.tablePowers.cellWidget(row, 8)
        comboBoxCount1 = mainWidget.layout().itemAt(0).widget()
        if indexticket != 1:
            comboBoxCount1.setCurrentIndex(personeID.index(69))
        else:
            comboBoxCount1.setCurrentIndex(0)
        mainWidget = self.tablePowers.cellWidget(row, 9)
        comboBoxCount2 = mainWidget.layout().itemAt(0).widget()
        if indexticket != 1:
            comboBoxCount2.setCurrentIndex(personeID.index(69))
        else:
            comboBoxCount2.setCurrentIndex(0)
        return

    @pyqtSlot()
    def comboCounter1Activated(self):
        combo = self.sender()
        row = 0
        for row in range(self.tablePowers.rowCount()):
            mainWidget = self.tablePowers.cellWidget(row, 8)
            comboBoxCount1 = mainWidget.layout().itemAt(0).widget()
            if comboBoxCount1 == combo:
                break
        self.btnPowersSave.setText("*Сохранить: изменено (" + str(row + 1) + ":" + str(8) + ")")
        self.btnPowersSave.setToolTip("*Сохранить: изменено (" + str(row + 1) + ":" + str(8) + ")")
        return

    @pyqtSlot()
    def comboCounter2Activated(self):
        combo = self.sender()
        row = 0
        for row in range(self.tablePowers.rowCount()):
            mainWidget = self.tablePowers.cellWidget(row, 8)
            comboBoxCount2 = mainWidget.layout().itemAt(0).widget()
            if comboBoxCount2 == combo:
                break
        self.btnPowersSave.setText("*Сохранить: изменено (" + str(row + 1) + ":" + str(9) + ")")
        self.btnPowersSave.setToolTip("*Сохранить: изменено (" + str(row + 1) + ":" + str(9) + ")")
        return

    @pyqtSlot()
    def comboUserActivated(self):
        combo = self.sender()
        row = 0
        for row in range(self.tablePowers.rowCount()):
            mainWidget = self.tablePowers.cellWidget(row, 6)
            comboBoxCount2 = mainWidget.layout().itemAt(0).widget()
            if comboBoxCount2 == combo:
                break
        self.btnPowersSave.setText("*Сохранить: изменено (" + str(row + 1) + ":" + str(6) + ")")
        self.btnPowersSave.setToolTip("*Сохранить: изменено (" + str(row + 1) + ":" + str(6) + ")")
        return

    @pyqtSlot()
    def comboOperatActivated(self):
        combo = self.sender()
        row = 0
        for row in range(self.tablePowers.rowCount()):
            mainWidget = self.tablePowers.cellWidget(row, 3)
            comboBoxOperat = mainWidget.layout().itemAt(0).widget()
            if comboBoxOperat == combo:
                break
        self.btnPowersSave.setText("*Сохранить: изменено (" + str(row + 1) + ":" + str(3) + ")")
        self.btnPowersSave.setToolTip("*Сохранить: изменено (" + str(row + 1) + ":" + str(3) + ")")

    @pyqtSlot()
    def comboMoneyActivated(self):
        combo = self.sender()
        row = 0
        for row in range(self.tablePowers.rowCount()):
            mainWidget = self.tablePowers.cellWidget(row, 5)
            comboBoxMoney = mainWidget.layout().itemAt(0).widget()
            if comboBoxMoney == combo:
                break
        self.btnPowersSave.setText("*Сохранить: изменено (" + str(row + 1) + ":" + str(5) + ")")
        self.btnPowersSave.setToolTip("*Сохранить: изменено (" + str(row + 1) + ":" + str(5) + ")")

    @pyqtSlot()
    def OnPowersRead(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow.OnPowersRead", "Data base is clousing!", str(self.connSqlite3))
                return
            sqlid = "SELECT max(ID) FROM PowerTable"
            id = self.getFromDataBase(sqlid)
            self.maxPowersID = id[0][0]
            sql = "SELECT * FROM CodeTickets"
            result = self.getFromDataBase(sql)
            tickCode = []
            tickId = []
            for ls in result:
                tickCode.append(ls[1])
                tickId.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=1"
            result = self.getFromDataBase(sql)
            operatCodeOffer = []
            operatIDOffer = []
            for ls in result:
                operatCodeOffer.append(ls[1])
                operatIDOffer.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=2"
            result = self.getFromDataBase(sql)
            operatCodeOut = []
            operatIDOut = []
            for ls in result:
                operatCodeOut.append(ls[1])
                operatIDOut.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=3"
            result = self.getFromDataBase(sql)
            operatCodeIn = []
            operatIDIn = []
            for ls in result:
                operatCodeIn.append(ls[1])
                operatIDIn.append(ls[0])
            sql = "SELECT * FROM Operations WHERE CodeTicket=4"
            result = self.getFromDataBase(sql)
            operatCodeRep = []
            operatIDRep = []
            for ls in result:
                operatCodeRep.append(ls[1])
                operatIDRep.append(ls[0])
            sql = "SELECT * FROM Money"
            result = self.getFromDataBase(sql)
            moneyCode = []
            moneyId = []
            for ls in result:
                moneyCode.append(ls[1])
                moneyId.append(ls[0])
            sql = "SELECT * FROM Persones"
            result = self.getFromDataBase(sql)
            personeCode = []
            personeID = []
            for ls in result:
                personeCode.append(ls[1])
                personeID.append(ls[0])
            sql = "SELECT * FROM Casher"
            result = self.getFromDataBase(sql)
            casherCode = []
            casherId = []
            for ls in result:
                casherCode.append(ls[1])
                casherId.append(ls[0])

            dateFrom = self.dateFromPower.date()
            strDateFrom = dateFrom.toString(self.dateFromPower.displayFormat())
            dateTo = self.dateToPower.date()
            strDateTo = dateTo.toString(self.dateToPower.displayFormat())
            sql = "SELECT * FROM PowerTable WHERE (Date >= '" + strDateFrom + "' AND " + "Date <= '" + strDateTo + "')"
            powerTable = self.getFromDataBase(sql)
            row = 0
            self.tablePowers.clearContents()
            self.tablePowers.setRowCount(len(powerTable))
            arraySumma = [0 for i in range(len(moneyCode))]
            arrayMoney = ['' for i in range(len(moneyCode))]
            for ls in powerTable:
                id = ls[0]
                numberTicket = ls[1]
                codeTicket = tickId.index(ls[2])
                summa = ls[4]
                codeMoney = moneyId.index(ls[5])
                if ls[6] == None:
                    codeUser = personeID.index(69)
                else:
                    codeUser = personeID.index(ls[6])
                codeCash = casherId.index(ls[7])
                if ls[8] == None:
                    codeCount1 = personeID.index(69)
                else:
                    codeCount1 = personeID.index(ls[8])
                if ls[9] == None:
                    codeCount2 = personeID.index(69)
                else:
                    codeCount2 = personeID.index(ls[9])
                dates = ls[10]
                comment = ls[11]
                self.tablePowers.setItem(row, 0, QTableWidgetItem(str(id)))
                self.tablePowers.setItem(row, 1, QTableWidgetItem(numberTicket))
                comboBoxWidget = QWidget()
                tickCombo = QComboBox()
                tickCombo.setEditable(False)
                tickCombo.setToolTip("Выберите тип квитанции")
                tickCombo.addItems(tickCode)
                tickCombo.setCurrentIndex(codeTicket)
                tickCombo.currentIndexChanged.connect(self.comboTicketActivated)
                tickCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                tickCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(tickCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tablePowers.setCellWidget(row, 2, comboBoxWidget)

                if ls[2] == 1:
                    operatCode = operatCodeOffer
                    codeOperat = operatIDOffer.index(ls[3])
                elif ls[2] == 2:
                    operatCode = operatCodeOut
                    codeOperat = operatIDOut.index(ls[3])
                elif ls[2] == 3:
                    operatCode = operatCodeIn
                    codeOperat = operatIDIn.index(ls[3])
                elif ls[2] == 4:
                    operatCode = operatCodeRep
                    codeOperat = operatIDRep.index(ls[3])
                else:
                    showError("Error ID in type tickets", "OnPowerRead", "")
                    return

                completer = QCompleter(operatCode)
                completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
                completer.popup().setStyleSheet("background-color: yellow")
                comboBoxWidget = QWidget()
                operatCombo = QComboBox()
                operatCombo.setEditable(True)
                operatCombo.setToolTip("Выберите операцию")
                operatCombo.setCompleter(completer)
                operatCombo.addItems(operatCode)
                operatCombo.setCurrentIndex(codeOperat)
                operatCombo.currentIndexChanged.connect(self.comboOperatActivated)
                operatCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                operatCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(operatCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tablePowers.setCellWidget(row, 3, comboBoxWidget)
                self.tablePowers.setItem(row, 4, QTableWidgetItem(str(float(summa))))
                comboBoxWidget = QWidget()
                moneyCombo = QComboBox()
                moneyCombo.setEditable(False)
                moneyCombo.setToolTip("Выберите код валюты")
                moneyCombo.addItems(moneyCode)
                moneyCombo.setCurrentIndex(codeMoney)
                moneyCombo.currentIndexChanged.connect(self.comboMoneyActivated)
                moneyCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                moneyCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(moneyCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tablePowers.setCellWidget(row, 5, comboBoxWidget)
                completer = QCompleter(personeCode)
                completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
                completer.popup().setStyleSheet("background-color: yellow")
                comboBoxWidget = QWidget()
                userCombo = QComboBox()
                userCombo.setEditable(True)
                userCombo.setToolTip("Выберите ФИО пользователя")
                userCombo.setCompleter(completer)
                userCombo.addItems(personeCode)
                userCombo.setCurrentIndex(codeUser)
                userCombo.currentIndexChanged.connect(self.comboUserActivated)
                userCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                userCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(userCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tablePowers.setCellWidget(row, 6, comboBoxWidget)
                self.tablePowers.setCellWidget(row, 7, self.getComboBox(casherCode, codeCash, "Выберите ФИО кассира"))
                completer = QCompleter(personeCode)
                completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
                completer.popup().setStyleSheet("background-color: yellow")
                comboBoxWidget = QWidget()
                counter1Combo = QComboBox()
                counter1Combo.setEditable(True)
                counter1Combo.setToolTip("Выберите ФИО счетчика 1")
                counter1Combo.setCompleter(completer)
                counter1Combo.addItems(personeCode)
                counter1Combo.setCurrentIndex(codeCount1)
                counter1Combo.currentIndexChanged.connect(self.comboCounter1Activated)
                counter1Combo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                counter1Combo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(counter1Combo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tablePowers.setCellWidget(row, 8, comboBoxWidget)
                completer = QCompleter(personeCode)
                completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
                completer.popup().setStyleSheet("background-color: yellow")
                comboBoxWidget = QWidget()
                counter2Combo = QComboBox()
                counter2Combo.setEditable(True)
                counter2Combo.setToolTip("Выберите ФИО счетчика 2")
                counter2Combo.setCompleter(completer)
                counter2Combo.addItems(personeCode)
                counter2Combo.setCurrentIndex(codeCount2)
                counter2Combo.currentIndexChanged.connect(self.comboCounter2Activated)
                counter2Combo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                counter2Combo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(counter2Combo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tablePowers.setCellWidget(row, 9, comboBoxWidget)
                calenadarWidget = QWidget()
                lineCalenader = QDateTimeEdit(self)
                lineCalenader.setDisplayFormat("dd-MM-yyyy")
                lineCalenader.setDate(QDate.fromString(dates, "yyyy-MM-dd"))
                lineCalenader.setCalendarPopup(True)
                layoutCalendBox = QHBoxLayout(calenadarWidget)
                layoutCalendBox.addWidget(lineCalenader)
                layoutCalendBox.setAlignment(Qt.AlignCenter)
                layoutCalendBox.setContentsMargins(0, 0, 0, 0)
                self.tablePowers.setCellWidget(row, 10, calenadarWidget)
                self.tablePowers.setItem(row, 11, QTableWidgetItem(str(comment)))
                arraySumma[moneyCombo.currentIndex()] += round(float(summa), 2)
                arrayMoney[moneyCombo.currentIndex()] = moneyCombo.currentText().lower()
                row += 1
            summas = ''
            for i, ls in enumerate(arraySumma):
                if ls != 0:
                    summas += str(round(ls, 2)) + arrayMoney[i] + ";"
            self.lineSummaPower.setText(summas)
            self.lineNumPower.setText(str(self.tablePowers.rowCount()))
            self.tablePowers.resizeColumnsToContents()
            self.tablePowers.resizeRowsToContents()
            self.tablePowers.setColumnHidden(0, True)
            self.listUpdateRecordsPower.clear()
            self.listUpdateViewPower.clear()
            self.listNewRecordsPower.clear()
            self.btnPowersSave.setText("Сохранить")
            self.btnPowersSave.setToolTip("Сохранить")
            self.btnPowersAdd.setEnabled(True)
            self.btnPowersCancel.setEnabled(True)
            self.btnPowersSave.setEnabled(True)
            self.btnPowersDelete.setEnabled(True)
        except Exception as ex:
            showError("Error read from data base", "OnPowerRead", str(ex) + traceback.format_exc())

    @pyqtSlot()
    def viewClickedPowers(self):
        selectRow = self.tablePowers.selectionModel().selectedRows()
        for index in sorted(selectRow):
            if index.row() in self.listNewRecordsPower:
                return
            if index.row() not in self.listUpdateRecordsPower:
                self.listUpdateRecordsPower.append(index.row())
                self.listUpdateViewPower.append(index.row() + 1)
                self.btnPowersSave.setText("*Сохранить: update строки:" + str(self.listUpdateViewPower))
                self.btnPowersSave.setToolTip("*Сохранить: update строки:" + str(self.listUpdateViewPower))
        return

    @pyqtSlot()
    def OnFilterPower(self):
        textfilt = self.lineFiltPower.text()

        if textfilt == '':
            for i in range(self.tablePowers.rowCount()):
                self.tablePowers.setRowHidden(i, False)
            rowCount = 0
            sqlm = "SELECT * FROM Money"
            codeMoney = self.getFromDataBase(sqlm)
            moneyCodes = []
            for lm in codeMoney:
                moneyCodes.append(lm[1])
            arraySumma = [0 for i in range(len(moneyCodes))]
            arrayMoney = ['' for i in range(len(moneyCodes))]
            for i in range(self.tablePowers.rowCount()):
                if not self.tablePowers.isRowHidden(i):
                    summa = round(float(self.tablePowers.item(i, 4).text()), 2)
                    mainWidget = self.tablePowers.cellWidget(i, 5)
                    comboBox = mainWidget.layout().itemAt(0).widget()
                    current = comboBox.currentText()
                    indx = moneyCodes.index(current)
                    arraySumma[indx] += summa
                    arrayMoney[indx] = current.lower()
                    rowCount += 1
            summas = ''
            for i, ls in enumerate(arraySumma):
                if ls != 0:
                    summas += str(round(ls, 2)) + arrayMoney[i] + ";"
            self.lineSummaPower.setText(summas)
            self.lineNumPower.setText(str(rowCount))
            return

        for i in range(self.tablePowers.rowCount()):
            indxCol = self.filtComboPower.currentIndex() + 1
            item = self.tablePowers.item(i, indxCol)
            if item:
                text = item.text()
                try:
                    text.index(textfilt)
                    self.tablePowers.setRowHidden(i, False)
                except Exception as ex:
                    self.tablePowers.setRowHidden(i, True)
            else:
                mainWidget = self.tablePowers.cellWidget(i, indxCol)
                objectBox = mainWidget.layout().itemAt(0).widget()
                if type(objectBox) is QComboBox:
                    texttype = objectBox.currentText()
                    try:
                        texttype.index(textfilt)
                        self.tablePowers.setRowHidden(i, False)
                    except Exception as ex:
                        self.tablePowers.setRowHidden(i, True)
                elif type(objectBox) is QDateTimeEdit:
                    strdate = objectBox.date()
                    try:
                        strdate.index(textfilt)
                        self.tablePowers.setRowHidden(i, False)
                    except Exception as ex:
                        self.tablePowers.setRowHidden(i, True)
        rowCount = 0
        sqlm = "SELECT * FROM Money"
        codeMoney = self.getFromDataBase(sqlm)
        moneyCodes = []
        for lm in codeMoney:
            moneyCodes.append(lm[1])
        arraySumma = [0 for i in range(len(moneyCodes))]
        arrayMoney = ['' for i in range(len(moneyCodes))]
        for i in range(self.tablePowers.rowCount()):
            if not self.tablePowers.isRowHidden(i):
                summa = round(float(self.tablePowers.item(i, 4).text()), 2)
                mainWidget = self.tablePowers.cellWidget(i, 5)
                comboBox = mainWidget.layout().itemAt(0).widget()
                current = comboBox.currentText()
                indx = moneyCodes.index(current)
                arraySumma[indx] += summa
                arrayMoney[indx] = current.lower()
                rowCount += 1
        summas = ''
        for i, ls in enumerate(arraySumma):
            if ls != 0:
                summas += str(round(ls, 2)) + arrayMoney[i] + ";"
        self.lineSummaPower.setText(summas)
        self.lineNumPower.setText(str(rowCount))
        return

    @pyqtSlot()
    def OnSearchPower(self):
        try:
            row = self.searchRowPower
            while row < self.tablePowers.rowCount():
                column = self.searchColumnPower
                while column < self.tablePowers.columnCount():
                    item = self.tablePowers.item(row, column)
                    if item:
                        try:
                            value = self.lineSearchPower.text()
                            item.text().index(value)
                            self.tablePowers.selectRow(row)
                            self.tablePowers.scrollToItem(item)
                            self.searchColumnPower = column + 1
                            self.searchRowPower = row
                            return
                        except Exception as ex:
                            pass
                    else:
                        mainWidget = self.tablePowers.cellWidget(row, column)
                        comboBox = mainWidget.layout().itemAt(0).widget()
                        if type(comboBox) is QComboBox:
                            texttype = comboBox.currentText()
                            try:
                                value = self.lineSearchPower.text()
                                texttype.index(value)
                                self.tablePowers.selectRow(row)
                                self.tablePowers.scrollToItem(item)
                                self.searchColumnPower = column + 1
                                self.searchRowPower = row
                                return
                            except Exception as ex:
                                pass
                    column += 1
                self.searchColumnPower = 0
                row += 1
            self.searchRowPower = 0
        except Exception as err:
            self.showError("MainWindow.OnSearchPowers", str(err), traceback.format_exc())

    @pyqtSlot()
    def OnPersonRead(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow.OnPersonRead", "Data base is clousing!", str(self.connSqlite3))
                return
            sqlid = "SELECT max(id) FROM Persones"
            id = self.getFromDataBase(sqlid)
            self.maxPersonesID = id[0][0]
            sqlo = "SELECT * FROM Persones"
            person = self.getFromDataBase(sqlo)
            self.tablePerson.clearContents()
            self.tablePerson.setRowCount(len(person))
            row = 0
            for ls in person:
                id = ls[0]
                name = ls[1]
                comment = ls[2]
                self.tablePerson.setItem(row, 0, QTableWidgetItem(str(id)))
                self.tablePerson.setItem(row, 1, QTableWidgetItem(name))
                self.tablePerson.setItem(row, 2, QTableWidgetItem(str(comment)))
                row += 1
            self.tablePerson.resizeColumnsToContents()
            self.tablePerson.resizeRowsToContents()
            self.tablePerson.setColumnHidden(0, True)
            self.listUpdateRecordsPerson.clear()
            self.listUpdateViewPerson.clear()
            self.listNewRecordsPerson.clear()
            self.btnPersonSave.setText("Сохранить")
            self.btnPersonSave.setToolTip("Сохранить")
            self.btnPersonCancel.setEnabled(True)
            self.btnPersonSave.setEnabled(True)
            self.btnPersonDelete.setEnabled(True)
            self.btnPersonAdd.setEnabled(True)
        except Exception as ex:
            showError("Error read from data base", "OnPersoneRead", str(ex))

    @pyqtSlot()
    def OnOperatRead(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow.OnOperatRead", "Data base is clousing!", str(self.connSqlite3))
                return
            sqlid = "SELECT max(id) FROM Operations"
            id = self.getFromDataBase(sqlid)
            self.maxOperationID = id[0][0]
            sqlo = "SELECT * FROM Operations"
            operationTable = self.getFromDataBase(sqlo)
            sqlm = "SELECT * FROM Department"
            codeDepartment = self.getFromDataBase(sqlm)
            departCodes = []
            departId = []
            for lm in codeDepartment:
                departCodes.append(lm[1])
                departId.append(lm[0])
            sqlm = "SELECT * FROM CodeTickets"
            codeTicket = self.getFromDataBase(sqlm)
            ticketCodes = []
            tickId = []
            for lm in codeTicket:
                ticketCodes.append(lm[1])
                tickId.append(lm[0])
            sqlm = "SELECT * FROM TypeOperation"
            codeOperation = self.getFromDataBase(sqlm)
            operCodes = []
            operId = []
            for lm in codeOperation:
                operCodes.append(lm[1])
                operId.append(lm[0])
            sqlm = "SELECT * FROM CodeReport"
            codeReport = self.getFromDataBase(sqlm)
            reportCodes = []
            reportId = []
            for lm in codeReport:
                reportCodes.append(lm[1])
                reportId.append(lm[0])
            sqlm = "SELECT * FROM Thematic"
            codeThema = self.getFromDataBase(sqlm)
            reportThema = []
            themaId = []
            for lm in codeThema:
                reportThema.append(lm[1])
                themaId.append(lm[0])

            row = 0
            self.tableOperat.clearContents()
            self.tableOperat.setRowCount(len(operationTable))
            for ls in operationTable:
                id = ls[0]
                name = ls[1]
                depart = departId.index(ls[2])
                ticket = tickId.index(ls[3])
                operat = operId.index(ls[4])
                report = reportId.index(ls[5])
                thema = themaId.index(ls[6])
                date = ls[7]
                comment = ls[8]
                self.tableOperat.setItem(row, 0, QTableWidgetItem(str(id)))
                self.tableOperat.setItem(row, 1, QTableWidgetItem(name))
                completer = QCompleter(departCodes)
                completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
                completer.popup().setStyleSheet("background-color: yellow")
                comboBoxWidget = QWidget()
                departCombo = QComboBox()
                departCombo.setEditable(True)
                departCombo.setToolTip("Chiose code money")
                departCombo.setCompleter(completer)
                departCombo.addItems(departCodes)
                departCombo.setCurrentIndex(depart)
                departCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                departCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(departCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tableOperat.setCellWidget(row, 2, comboBoxWidget)
                comboBoxWidget = QWidget()
                ticketCombo = QComboBox()
                ticketCombo.setEditable(False)
                ticketCombo.setToolTip("Chiose code money")
                ticketCombo.addItems(ticketCodes)
                ticketCombo.setCurrentIndex(ticket)
                ticketCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                ticketCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(ticketCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tableOperat.setCellWidget(row, 3, comboBoxWidget)
                comboBoxWidget = QWidget()
                operatCombo = QComboBox()
                operatCombo.setEditable(False)
                operatCombo.setToolTip("Chiose code money")
                operatCombo.addItems(operCodes)
                operatCombo.setCurrentIndex(operat)
                operatCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                operatCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(operatCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tableOperat.setCellWidget(row, 4, comboBoxWidget)
                comboBoxWidget = QWidget()
                reportCombo = QComboBox()
                reportCombo.setEditable(False)
                reportCombo.setToolTip("Chiose code money")
                reportCombo.addItems(reportCodes)
                reportCombo.setCurrentIndex(report)
                reportCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                reportCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(reportCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tableOperat.setCellWidget(row, 5, comboBoxWidget)
                comboBoxWidget = QWidget()
                reportCombo = QComboBox()
                reportCombo.setEditable(False)
                reportCombo.setToolTip("Chiose name themas")
                reportCombo.addItems(reportThema)
                reportCombo.setCurrentIndex(thema)
                reportCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                reportCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(reportCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tableOperat.setCellWidget(row, 6, comboBoxWidget)
                calenadarWidget = QWidget()
                lineCalenader = QDateTimeEdit(self)
                lineCalenader.setDisplayFormat("dd-MM-yyyy")
                lineCalenader.setDate(QDate.fromString(date, "yyyy-MM-dd"))
                lineCalenader.setCalendarPopup(True)
                layoutCalendBox = QHBoxLayout(calenadarWidget)
                layoutCalendBox.addWidget(lineCalenader)
                layoutCalendBox.setAlignment(Qt.AlignCenter)
                layoutCalendBox.setContentsMargins(0, 0, 0, 0)
                self.tableOperat.setCellWidget(row, 7, calenadarWidget)
                self.tableOperat.setItem(row, 8, QTableWidgetItem(comment))
                row += 1
            self.tableOperat.resizeColumnsToContents()
            self.tableOperat.resizeRowsToContents()
            self.tableOperat.setColumnHidden(0, True)
            self.listUpdateRecordsOperat.clear()
            self.listUpdateViewOperat.clear()
            self.listNewRecordsOperat.clear()
            self.btnOperatSave.setText("Сохранить")
            self.btnOperatSave.setToolTip("Сохранить")
            self.btnOperatCancel.setEnabled(True)
            self.btnOperatDelete.setEnabled(True)
            self.btnOperatSave.setEnabled(True)
            self.btnOperatAdd.setEnabled(True)
        except Exception as ex:
            showError("Error read from data base", "OnOperatRead", str(ex))

    @pyqtSlot()
    def OnOperatDelete(self):
        try:
            if showInfo("OnOperatSave", "Удалить отмеченные строки?",
                        "Отмеченные для удаления строки: " + str(self.listUpdateRecordsOperat)) == 1024:
                for k in self.listUpdateRecordsOperat:
                    id = int(self.tableOperat.item(k - 1, 0).text())
                    sqlu = 'DELETE FROM Operations WHERE Id=' + str(id)
                    self.writeToDataBase(sqlu)
                self.listUpdateRecordsOperat.clear()
                self.listUpdateViewOperat.clear()
                self.OnOperatRead()
        except Exception as ex:
            showError("Error delete records", "OnOperatDelete", str(ex))
        self.listUpdateRecordsOperat.clear()
        self.listUpdateViewOperat.clear()
        self.btnOperatSave.setText("Сохранить")
        self.btnOperatSave.setToolTip("Сохранить")
        return

    @pyqtSlot()
    def OnOperatSave(self):
        if len(self.listUpdateRecordsOperat) > 0:
            if showInfo("OnOperatSave", "Сохранить измененные строки?", "Измененные строки: " + str(self.listUpdateRecordsOperat)) == 1024:
                sqlm = "SELECT * FROM Department"
                codeDepartment = self.getFromDataBase(sqlm)
                departCodes = []
                departId = []
                for lm in codeDepartment:
                    departCodes.append(lm[1])
                    departId.append(lm[0])
                sqlm = "SELECT * FROM CodeTickets"
                codeTicket = self.getFromDataBase(sqlm)
                ticketCodes = []
                tickId = []
                for lm in codeTicket:
                    ticketCodes.append(lm[1])
                    tickId.append(lm[0])
                sqlm = "SELECT * FROM TypeOperation"
                codeOperation = self.getFromDataBase(sqlm)
                operCodes = []
                operId = []
                for lm in codeOperation:
                    operCodes.append(lm[1])
                    operId.append(lm[0])
                sqlm = "SELECT * FROM Thematic"
                codeThema = self.getFromDataBase(sqlm)
                reportThema = []
                themaId = []
                for lm in codeThema:
                    reportThema.append(lm[1])
                    themaId.append(lm[0])
                sqlm = "SELECT * FROM CodeReport"
                codeReport = self.getFromDataBase(sqlm)
                reportCodes = []
                reportId = []
                for lm in codeReport:
                    reportCodes.append(lm[1])
                    reportId.append(lm[0])
                for k in self.listUpdateRecordsOperat:
                    id = int(self.tableOperat.item(k, 0).text())
                    nameoper = self.tableOperat.item(k, 1).text()

                    mainWidget = self.tableOperat.cellWidget(k, 2)
                    comboBox = mainWidget.layout().itemAt(0).widget()
                    codeDepart = departId[comboBox.currentIndex()]

                    mainWidget = self.tableOperat.cellWidget(k, 3)
                    comboBox = mainWidget.layout().itemAt(0).widget()
                    codeTick = tickId[comboBox.currentIndex()]

                    mainWidget = self.tableOperat.cellWidget(k, 4)
                    comboBox = mainWidget.layout().itemAt(0).widget()
                    codeType = operId[comboBox.currentIndex()]

                    mainWidget = self.tableOperat.cellWidget(k, 5)
                    comboBox = mainWidget.layout().itemAt(0).widget()
                    codeReport = reportId[comboBox.currentIndex()]

                    mainWidget = self.tableOperat.cellWidget(k, 6)
                    comboBox = mainWidget.layout().itemAt(0).widget()
                    codeThema = themaId[comboBox.currentIndex()]

                    mainWidget = self.tableOperat.cellWidget(k, 7)
                    calendar = mainWidget.layout().itemAt(0).widget()
                    strdate = calendar.date()
                    strDate = strdate.toString("yyyy-MM-dd")
                    comment = self.tableOperat.item(k, 8).text()
                    sqlu = 'UPDATE Operations SET Id=' + str(id) + ',Name=' + "'" + nameoper + "'" + ',Department=' + str(codeDepart) + \
                           ',CodeTicket=' + str(codeTick) + ',CodeOperation=' + str(codeType) + ',CodeReport=' + str(codeReport) + ',Thema=' + str(codeThema) + \
                           ',Date=' + "'" + strDate + "'," + 'Comment=' + "'" + comment + "'" + ' WHERE Id=' + str(id)
                    self.writeToDataBase(sqlu)
        self.listUpdateRecordsOperat.clear()
        self.listUpdateViewOperat.clear()

        if len(self.listNewRecordsOperat) > 0:
            sqlm = "SELECT * FROM Department"
            codeDepartment = self.getFromDataBase(sqlm)
            departCodes = []
            departId = []
            for lm in codeDepartment:
                departCodes.append(lm[1])
                departId.append(lm[0])
            sqlm = "SELECT * FROM CodeTickets"
            codeTicket = self.getFromDataBase(sqlm)
            ticketCodes = []
            tickId = []
            for lm in codeTicket:
                ticketCodes.append(lm[1])
                tickId.append(lm[0])
            sqlm = "SELECT * FROM Thematic"
            codeThema = self.getFromDataBase(sqlm)
            reportThema = []
            themaId = []
            for lm in codeThema:
                reportThema.append(lm[1])
                themaId.append(lm[0])
            sqlm = "SELECT * FROM TypeOperation"
            codeOperation = self.getFromDataBase(sqlm)
            operCodes = []
            operId = []
            for lm in codeOperation:
                operCodes.append(lm[1])
                operId.append(lm[0])
            sqlm = "SELECT * FROM CodeReport"
            codeReport = self.getFromDataBase(sqlm)
            reportCodes = []
            reportId = []
            for lm in codeReport:
                reportCodes.append(lm[1])
                reportId.append(lm[0])
            for k in self.listNewRecordsOperat:
                id = int(self.tableOperat.item(k, 0).text())
                nameoper = self.tableOperat.item(k, 1).text()

                mainWidget = self.tableOperat.cellWidget(k, 2)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeDepart = departId[comboBox.currentIndex()]

                mainWidget = self.tableOperat.cellWidget(k, 3)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeTick = tickId[comboBox.currentIndex()]

                mainWidget = self.tableOperat.cellWidget(k, 4)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeType = operId[comboBox.currentIndex()]

                mainWidget = self.tableOperat.cellWidget(k, 5)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeReport = reportId[comboBox.currentIndex()]

                mainWidget = self.tableOperat.cellWidget(k, 6)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeThema = themaId[comboBox.currentIndex()]

                mainWidget = self.tableOperat.cellWidget(k, 7)
                calendar = mainWidget.layout().itemAt(0).widget()
                strdate = calendar.date()
                strDate = strdate.toString("yyyy-MM-dd")
                comment = self.tableOperat.item(k, 8).text()
                sqli = 'INSERT INTO Operations(Id, Name, Department, CodeTicket, CodeOperation, CodeReport, Thema, Date, Comment) VALUES(' + str(id) + ",'" + \
                      str(nameoper) + "'," + str(codeDepart) + "," + str(codeTick) + "," + str(codeType) + "," + str(codeReport) + "," + str(codeThema) + ",'" + strDate + "','" + str(comment) + "')"
                self.writeToDataBase(sqli)
        self.listNewRecordsOperat.clear()
        self.tableOperat.resizeColumnsToContents()
        self.tableOperat.resizeRowsToContents()
        self.btnOperatSave.setText("Сохранить")
        self.btnOperatSave.setToolTip("Сохранить")
        return

    @pyqtSlot()
    def OnOperatCancel(self):
        self.OnOperatRead()
        return

    @pyqtSlot()
    def OnOperatAdd(self):
        try:
            self.IndexRow = self.tableOperat.rowCount()
            self.tableOperat.insertRow(self.IndexRow)
            self.maxOperationID += 1
            sqlm = "SELECT * FROM Department"
            codeDepartment = self.getFromDataBase(sqlm)
            codeDepart = []
            for lm in codeDepartment:
                codeDepart.append(lm[1])
            sqlm = "SELECT * FROM CodeTickets"
            codeTicket = self.getFromDataBase(sqlm)
            codeTick = []
            for lm in codeTicket:
                codeTick.append(lm[1])
            sqlm = "SELECT * FROM TypeOperation"
            codeType = self.getFromDataBase(sqlm)
            codeTp = []
            for lm in codeType:
                codeTp.append(lm[1])
            sqlm = "SELECT * FROM CodeReport"
            codeReport = self.getFromDataBase(sqlm)
            codeRep = []
            for lm in codeReport:
                codeRep.append(lm[1])
            sqlm = "SELECT * FROM Thematic"
            codeThema = self.getFromDataBase(sqlm)
            reportThema = []
            for lm in codeThema:
                reportThema.append(lm[1])

            self.tableOperat.setItem(self.IndexRow, 0, QTableWidgetItem(str(self.maxOperationID)))
            self.tableOperat.setItem(self.IndexRow, 1, QTableWidgetItem(''))

            comboBoxWidget = QWidget()
            departCombo = QComboBox()
            departCombo.setEditable(True)
            departCombo.setToolTip("Chiose name of department")
            departCombo.addItems(codeDepart)
            departCombo.setCurrentIndex(0)
            departCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            departCombo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(departCombo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tableOperat.setCellWidget(self.IndexRow, 2, comboBoxWidget)

            comboBoxWidget = QWidget()
            ticketCombo = QComboBox()
            ticketCombo.setEditable(False)
            ticketCombo.setToolTip("Chiose name of ticket")
            ticketCombo.addItems(codeTick)
            ticketCombo.setCurrentIndex(0)
            ticketCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            ticketCombo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(ticketCombo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tableOperat.setCellWidget(self.IndexRow, 3, comboBoxWidget)

            comboBoxWidget = QWidget()
            typeCombo = QComboBox()
            typeCombo.setEditable(False)
            typeCombo.setToolTip("Chiose name of type operation")
            typeCombo.addItems(codeTp)
            typeCombo.setCurrentIndex(0)
            typeCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            typeCombo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(typeCombo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tableOperat.setCellWidget(self.IndexRow, 4, comboBoxWidget)

            comboBoxWidget = QWidget()
            reportCombo = QComboBox()
            reportCombo.setEditable(False)
            reportCombo.setToolTip("Chiose name of report")
            reportCombo.addItems(codeRep)
            reportCombo.setCurrentIndex(0)
            reportCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            reportCombo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(reportCombo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tableOperat.setCellWidget(self.IndexRow, 5, comboBoxWidget)

            comboBoxWidget = QWidget()
            reportCombo = QComboBox()
            reportCombo.setEditable(False)
            reportCombo.setToolTip("Chiose name of thematic")
            reportCombo.addItems(reportThema)
            reportCombo.setCurrentIndex(0)
            reportCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            reportCombo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(reportCombo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tableOperat.setCellWidget(self.IndexRow, 6, comboBoxWidget)

            calenadarWidget = QWidget(self)
            lineCalenader = QDateTimeEdit(self)
            lineCalenader.setDisplayFormat("dd-MM-yyyy")
            lineCalenader.setDate(QDate.currentDate())
            lineCalenader.setCalendarPopup(True)
            layoutCalendBox = QHBoxLayout(calenadarWidget)
            layoutCalendBox.addWidget(lineCalenader)
            layoutCalendBox.setAlignment(Qt.AlignCenter)
            layoutCalendBox.setContentsMargins(0, 0, 0, 0)
            self.tableOperat.setCellWidget(self.IndexRow, 7, calenadarWidget)
            self.tableOperat.setItem(self.IndexRow, 8, QTableWidgetItem(''))

            self.tableOperat.resizeColumnsToContents()
            self.tableOperat.resizeRowsToContents()
            self.listNewRecordsOperat.append(self.IndexRow)
            item = self.tableOperat.item(self.IndexRow, 1)
            self.tableOperat.selectRow(self.IndexRow)
            self.tableOperat.scrollToItem(item)
            self.IndexRow += 1
            self.btnOperatSave.setText("*Сохранить: новую строку")
            self.btnOperatSave.setToolTip("*Сохранить: новую строку")
        except Exception as err:
            showError("mainWindow.OnOperatAdd", str(err), traceback.format_exc())

    @pyqtSlot()
    def OnPersonDelete(self):
        try:
            if len(self.listUpdateRecordsPerson) > 0:
                if showInfo("OnPersonDelete", "Удалить отмеченные строки?",
                            "Отмеченные для удаления строки: " + str(self.listUpdateRecordsPerson)) == 1024:
                    for k in self.listUpdateRecordsPerson:
                        id = int(self.tablePerson.item(k - 1, 0).text())
                        sqlu = 'DELETE FROM Persones WHERE Id=' + str(id)
                        self.writeToDataBase(sqlu)
                    self.listUpdateRecordsPerson.clear()
                    self.listUpdateViewPerson.clear()
                    self.OnPersonRead()
        except Exception as ex:
            showError("Error delete records", "OnPersonDelete", str(ex))
        self.listUpdateRecordsPerson.clear()
        self.listUpdateViewPerson.clear()
        self.btnPersonSave.setText("Сохранить")
        self.btnPersonSave.setToolTip("Сохранить")
        return

    @pyqtSlot()
    def OnPersonSave(self):
        if len(self.listUpdateRecordsPerson) > 0:
            if showInfo("OnPersoneSave", "Сохранить измененные строки?", "Измененные строки: " + str(self.listUpdateRecordsPerson)) == 1024:
                for k in self.listUpdateRecordsPerson:
                    id = int(self.tablePerson.item(k, 0).text())
                    fio = self.tablePerson.item(k, 1).text()
                    comment = self.tablePerson.item(k, 2).text()
                    sqlu = 'UPDATE Persones SET ID=' + str(id) + "," + 'Name=' + "'" + fio + "'," + 'Comment=' + "'" + comment + "'" + ' WHERE Id=' + str(id)
                    self.writeToDataBase(sqlu)
        self.listUpdateRecordsPerson.clear()
        self.listUpdateViewPerson.clear()

        for k in self.listNewRecordsPerson:
            id = int(self.tablePerson.item(k, 0).text())
            fio = self.tablePerson.item(k, 1).text()
            comment = self.tablePerson.item(k, 2).text()
            sqli = 'INSERT INTO Persones(ID, Name, Comment) VALUES(' + str(id) + ",'" + str(fio) + "','" + str(comment) + "')"
            self.writeToDataBase(sqli)
        self.listNewRecordsPerson.clear()
        self.tablePerson.resizeColumnsToContents()
        self.tablePerson.resizeRowsToContents()
        self.btnPersonSave.setText("Сохранить")
        self.btnPersonSave.setToolTip("Сохранить")
        return

    @pyqtSlot()
    def OnPersonCancel(self):
        self.OnPersonRead()
        return

    @pyqtSlot()
    def OnPersonAdd(self):
        try:
            self.IndexRow = self.tablePerson.rowCount()
            self.tablePerson.insertRow(self.IndexRow)
            self.maxPersonesID += 1
            self.tablePerson.setItem(self.IndexRow, 0, QTableWidgetItem(str(self.maxPersonesID)))
            self.tablePerson.setItem(self.IndexRow, 1, QTableWidgetItem(''))
            self.tablePerson.setItem(self.IndexRow, 2, QTableWidgetItem(''))
            self.tablePerson.resizeColumnsToContents()
            self.tablePerson.resizeRowsToContents()
            self.listNewRecordsPerson.append(self.IndexRow)
            item = self.tablePerson.item(self.IndexRow, 1)
            self.tablePerson.selectRow(self.IndexRow)
            self.tablePerson.scrollToItem(item)
            self.IndexRow += 1
            self.btnPersonSave.setText("*Сохранить: новую строку")
            self.btnPersonSave.setToolTip("*Сохранить: новую строку")
        except Exception as err:
            showError("mainWindow.OnPersonAdd", str(err), traceback.format_exc())

    @pyqtSlot()
    def viewClickedOperat(self):
        selectRow = self.tableOperat.selectionModel().selectedRows()
        for index in sorted(selectRow):
            if index.row() in self.listNewRecordsOperat:
                return
            if index.row() not in self.listUpdateRecordsOperat:
                self.listUpdateRecordsOperat.append(index.row())
                self.listUpdateViewOperat.append(index.row() + 1)
                self.btnOperatSave.setText("*Сохранить: update строки:" + str(self.listUpdateViewOperat))
                self.btnOperatSave.setToolTip("*Сохранить: update строки:" + str(self.listUpdateViewOperat))
        return

    @pyqtSlot()
    def viewClickedPerson(self):
        selectRow = self.tablePerson.selectionModel().selectedRows()
        for index in sorted(selectRow):
            if index.row() in self.listNewRecordsPerson:
                return
            if index.row() not in self.listUpdateRecordsPerson:
                self.listUpdateRecordsPerson.append(index.row())
                self.listUpdateViewPerson.append(index.row() + 1)
                self.btnPowersSave.setText("*Сохранить: update строки:" + str(self.listUpdateViewPerson))
                self.btnPowersSave.setToolTip("*Сохранить: update строки:" + str(self.listUpdateViewPerson))
        return

    @pyqtSlot()
    def OnSearchPersone1(self):
        try:
            row = self.searchRowPersone1
            while row < self.tableOperat.rowCount():
                column = self.searchColumnPersone1
                while column < self.tableOperat.columnCount():
                    item = self.tableOperat.item(row, column)
                    if item:
                        try:
                            value = self.lineSearchPersone1.text()
                            item.text().index(value)
                            self.tableOperat.selectRow(row)
                            self.tableOperat.scrollToItem(item)
                            self.searchColumnPersone1 = column + 1
                            self.searchRowPersone1 = row
                            return
                        except Exception as ex:
                            pass
                    column += 1
                self.searchColumnPersone1 = 0
                row += 1
            self.searchRowPersone1 = 0
        except Exception as err:
            self.showError("MainWindow.OnSearchPersone1", str(err), traceback.format_exc())


    @pyqtSlot()
    def setFilterPersone1(self):
        textfilt = self.lineFiltPersone1.text()

        if textfilt == '':
            for i in range(self.tableOperat.rowCount()):
                self.tableOperat.setRowHidden(i, False)
            return

        for i in range(self.tableOperat.rowCount()):
            indxCol = self.filtComboPersone1.currentIndex() + 1
            item = self.tableOperat.item(i, indxCol)
            if item:
                text = item.text()
                try:
                    text.index(textfilt)
                    self.tableOperat.setRowHidden(i, False)
                except Exception as ex:
                    self.tableOperat.setRowHidden(i, True)
        return

    @pyqtSlot()
    def OnExportPersone1(self):
        try:
            if self.tableOperat.rowCount() == 0:
                return

            currentDate = datetime.now().strftime('%d-%m-%Y %H:%M:%S')
            wb = xlwt.Workbook()
            ws = wb.add_sheet("Отчет по операциям")
            ws.write(0, 0, "Отчет 'Операции'")
            ws.write(1, 0, "Дата генерации: " + currentDate)
            ws.write(2, 0, self.tableOperat.horizontalHeaderItem(1).text())
            ws.write(2, 1, self.tableOperat.horizontalHeaderItem(2).text())
            ws.write(2, 2, self.tableOperat.horizontalHeaderItem(3).text())
            ws.write(2, 3, self.tableOperat.horizontalHeaderItem(4).text())
            ws.write(2, 4, self.tableOperat.horizontalHeaderItem(5).text())
            ws.write(2, 5, self.tableOperat.horizontalHeaderItem(6).text())
            ws.write(2, 6, self.tableOperat.horizontalHeaderItem(7).text())
            m = 3
            for k in range(self.tableOperat.rowCount()):
                operation = self.tableOperat.item(k, 1).text()
                mainWidget = self.tableOperat.cellWidget(k, 2)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeDepart = comboBox.currentText()

                mainWidget = self.tableOperat.cellWidget(k, 3)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeTick = comboBox.currentText()

                mainWidget = self.tableOperat.cellWidget(k, 4)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeType = comboBox.currentText()

                mainWidget = self.tableOperat.cellWidget(k, 5)
                comboBox = mainWidget.layout().itemAt(0).widget()
                codeReport = comboBox.currentText()

                mainWidget = self.tableOperat.cellWidget(k, 6)
                calendar = mainWidget.layout().itemAt(0).widget()
                strdate = calendar.date()
                strDate = strdate.toString("dd-MM-yyyy")

                comment = self.tableOperat.item(k, 7).text()

                ws.write(k + m, 0, operation)
                ws.write(k + m, 1, codeDepart)
                ws.write(k + m, 2, codeTick)
                ws.write(k + m, 3, codeType)
                ws.write(k + m, 4, codeReport)
                ws.write(k + m, 5, strDate)
                ws.write(k + m, 6, comment)
            currDate = datetime.now().strftime('%d-%m-%Y')
            namefile = 'Отчет_операции_' + currDate + '.xls'
            wb.save(namefile)
            showInfo("Info export", "Save data is successfull!", namefile)
        except Exception as err:
            showError("MainWindow.OnExportPersone1.", str(err), traceback.format_exc())

    @pyqtSlot()
    def OnSearchPersone(self):
        try:
            row = self.searchRowPersone
            while row < self.tablePerson.rowCount():
                column = self.searchColumnPersone
                while column < self.tablePerson.columnCount():
                    item = self.tablePerson.item(row, column)
                    if item:
                        try:
                            value = self.lineSearchPersone.text()
                            item.text().index(value)
                            self.tablePerson.selectRow(row)
                            self.tablePerson.scrollToItem(item)
                            self.searchColumnPersone = column + 1
                            self.searchRowPersone = row
                            return
                        except Exception as ex:
                            pass
                    column += 1
                self.searchColumnPersone = 0
                row += 1
            self.searchRowPersone = 0
        except Exception as err:
            showError("MainWindow.OnSearchPersone", str(err), traceback.format_exc())

    @pyqtSlot()
    def setFilterPersone(self):
        textfilt = self.lineFiltPersone.text()

        if textfilt == '':
            for i in range(self.tablePerson.rowCount()):
                self.tablePerson.setRowHidden(i, False)
            return

        for i in range(self.tablePerson.rowCount()):
            indxCol = self.filtComboPersone.currentIndex() + 1
            item = self.tablePerson.item(i, indxCol)
            if item:
                text = item.text()
                try:
                    text.index(textfilt)
                    self.tablePerson.setRowHidden(i, False)
                except Exception as ex:
                    self.tablePerson.setRowHidden(i, True)
        return

    @pyqtSlot()
    def OnExportPersone(self):
        try:
            if self.tablePerson.rowCount() == 0:
                return

            currentDate = datetime.now().strftime('%d-%m-%Y %H:%M:%S')
            wb = xlwt.Workbook()
            ws = wb.add_sheet("Отчет по пользователям")
            ws.write(0, 0, "Отчет 'Пользователи'")
            ws.write(1, 0, "Дата генерации: " + currentDate)
            ws.write(2, 0, self.tablePerson.horizontalHeaderItem(1).text())
            ws.write(2, 1, self.tablePerson.horizontalHeaderItem(2).text())
            m = 3
            for k in range(self.tablePerson.rowCount()):
                fio = self.tablePerson.item(k, 1).text()
                comment = self.tablePerson.item(k, 2).text()
                ws.write(k + m, 0, fio)
                ws.write(k + m, 1, comment)
            currDate = datetime.now().strftime('%d-%m-%Y')
            namefile = 'Отчет_пользователи_' + currDate + '.xls'
            wb.save(namefile)
            showInfo("Info export", "Save data is successfull!", namefile)
        except Exception as err:
            self.showError("MainWindow.OnExportTick.", str(err), traceback.format_exc())

    @pyqtSlot()
    def OnOplatyCancel(self):
        self.OnReadOplaty()
        return

    @pyqtSlot()
    def viewClicked(self):
        selectRow = self.tableOplaty.selectionModel().selectedRows()
        for index in sorted(selectRow):
            if index.row() in self.listNewRecords:
                return
            if index.row() not in self.listUpdateRecords:
                self.listUpdateRecords.append(index.row())
                self.listUpdateViewRecords.append(index.row() + 1)
                self.btnPowersSave.setText("*Сохранить: update строки:" + str(self.listUpdateViewRecords))
                self.btnPowersSave.setToolTip("*Сохранить: update строки:" + str(self.listUpdateViewRecords))
        return

    @pyqtSlot()
    def OnOplatyDelete(self):
        try:
            if len(self.listUpdateRecords) > 0:
                if showInfo("OnOplatySave", "Удалить отмеченные строки?",
                            "Отмеченные для удаления строки: " + str(self.listUpdateRecords)) == 1024:
                    for k in self.listUpdateRecords:
                        id = int(self.tableOplaty.item(k - 1, 0).text())
                        sqlu = 'DELETE FROM Periodica WHERE Id=' + str(id)
                        self.writeToDataBase(sqlu)
                    self.listUpdateRecords.clear()
                    self.listUpdateViewRecords.clear()
                    self.OnReadOplaty()
        except Exception as ex:
            showError("Error delete records", "OnOplatyDelete", str(ex))
        self.listUpdateRecords.clear()
        self.listUpdateViewRecords.clear()
        self.btnOplatySave.setText("Сохранить")
        self.btnOplatySave.setToolTip("Сохранить")
        return

    def writeToDataBase(self, sql):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow.writeToDataBase", "Data base is clousing!", str(self.connSqlite3))
                return
            curr = self.connSqlite3.cursor()
            curr.execute(sql)
            self.connSqlite3.commit()
            curr.close()
        except Exception as err:
            showError("mainWindow.writeToDataBase", str(err), traceback.format_exc())

    @pyqtSlot()
    def OnReadOplaty(self):
        try:
            if self.connSqlite3 == '':
                showError("mainWindow.OnReadOplaty", "Data base is clousing!", str(self.connSqlite3))
                return
            sqlid = "SELECT max(id) FROM Periodica"
            id = self.getFromDataBase(sqlid)
            self.maxOplatyID = id[0][0]
            dateFrom = self.dateFromOplaty.date()
            strDateFrom = dateFrom.toString(self.dateFromOplaty.displayFormat())
            dateTo = self.dateToOplaty.date()
            strDateTo = dateTo.toString(self.dateToOplaty.displayFormat())
            sqlo = "SELECT * FROM Periodica WHERE (Date >= '" + strDateFrom + "' AND " + "Date <= '" + strDateTo + "')"
            self.oplatyTable = self.getFromDataBase(sqlo)
            sqlm = "SELECT * FROM Money"
            codeMoney = self.getFromDataBase(sqlm)
            self.tableOplaty.clearContents()
            self.tableOplaty.setRowCount(len(self.oplatyTable))
            moneyCodes = []
            moneyId = []
            for lm in codeMoney:
                moneyCodes.append(lm[1])
                moneyId.append(lm[0])
            row = 0
            arraySumma = [0 for i in range(len(moneyCodes))]
            arrayMoney = ['' for i in range(len(moneyCodes))]
            for ls in self.oplatyTable:
                id = ls[0]
                name = ls[1]
                summa = ls[2]
                codemoney = moneyId.index(ls[3])
                dates = ls[4]
                comment = ls[5]
                self.tableOplaty.setItem(row, 0, QTableWidgetItem(str(id)))
                self.tableOplaty.setItem(row, 1, QTableWidgetItem(name))
                self.tableOplaty.setItem(row, 2, QTableWidgetItem(str(summa)))
                comboBoxWidget = QWidget()
                moneyCombo = QComboBox()
                moneyCombo.setEditable(False)
                moneyCombo.setToolTip("Chiose code money")
                moneyCombo.addItems(moneyCodes)
                moneyCombo.setCurrentIndex(codemoney)
                moneyCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                moneyCombo.adjustSize()
                layoutComboBox = QHBoxLayout(comboBoxWidget)
                layoutComboBox.addWidget(moneyCombo)
                layoutComboBox.setAlignment(Qt.AlignCenter)
                layoutComboBox.setContentsMargins(0, 0, 0, 0)
                self.tableOplaty.setCellWidget(row, 3, comboBoxWidget)
                calenadarWidget = QWidget()
                lineCalenader = QDateTimeEdit(self)
                lineCalenader.setDisplayFormat("dd-MM-yyyy")
                lineCalenader.setDate(QDate.fromString(dates, "yyyy-MM-dd"))
                lineCalenader.setCalendarPopup(True)
                layoutCalendBox = QHBoxLayout(calenadarWidget)
                layoutCalendBox.addWidget(lineCalenader)
                layoutCalendBox.setAlignment(Qt.AlignCenter)
                layoutCalendBox.setContentsMargins(0, 0, 0, 0)
                self.tableOplaty.setCellWidget(row, 4, calenadarWidget)
                self.tableOplaty.setItem(row, 5, QTableWidgetItem(comment))
                arraySumma[codemoney] += summa
                arrayMoney[codemoney] = moneyCombo.currentText().lower()
                row += 1
            summas = ''
            for i, ls in enumerate(arraySumma):
                if ls != 0:
                    summas += str(ls) + arrayMoney[i] + ";"
            self.lineSummaOplaty.setText(summas)
            self.tableOplaty.resizeColumnsToContents()
            self.tableOplaty.resizeRowsToContents()
            self.tableOplaty.setColumnHidden(0, True)
            self.listUpdateRecords.clear()
            self.listUpdateViewRecords.clear()
            self.listNewRecords.clear()
            self.btnOplatySave.setText("Сохранить")
            self.btnOplatySave.setToolTip("Сохранить")
            self.btnOplatyAdd.setEnabled(True)
            self.btnOplatyCancel.setEnabled(True)
            self.btnOplatySave.setEnabled(True)
            self.btnOplatyDelete.setEnabled(True)
        except Exception as ex:
            showError("Error read from data base", "OnReadOplaty", str(ex))

    @pyqtSlot()
    def OnOplatySave(self):
        if len(self.listUpdateRecords) > 0:
            if showInfo("OnOplatySave", "Сохранить измененные строки?", "Измененные строки: " + str(self.listUpdateRecords)) == 1024:
                for k in self.listUpdateRecords:
                    id = int(self.tableOplaty.item(k, 0).text())
                    nameoper = self.tableOplaty.item(k, 1).text()
                    summa = self.tableOplaty.item(k, 2).text()
                    mainWidget = self.tableOplaty.cellWidget(k, 3)
                    comboBox = mainWidget.layout().itemAt(0).widget()
                    codemoney = comboBox.currentIndex() + 1
                    mainWidget = self.tableOplaty.cellWidget(k, 4)
                    calendar = mainWidget.layout().itemAt(0).widget()
                    strdate = calendar.date()
                    strDate = strdate.toString("yyyy-MM-dd")
                    comment = self.tableOplaty.item(k, 5).text()
                    sqlu = 'UPDATE Periodica SET Id=' + str(id) + "," + 'Name=' + "'" + nameoper + "'," + 'Summa=' + str(float(str(summa))) + \
                          ',CodeMoney=' + str(codemoney) + ',Date=' + "'" + strDate +"'," + 'Comment=' + "'" + comment + "'" + ' WHERE Id=' + str(id)
                    self.writeToDataBase(sqlu)
        self.listUpdateRecords.clear()
        self.listUpdateViewRecords.clear()

        for k in self.listNewRecords:
            id = int(self.tableOplaty.item(k, 0).text())
            nameoper = self.tableOplaty.item(k, 1).text()
            summa = self.tableOplaty.item(k, 2).text()
            mainWidget = self.tableOplaty.cellWidget(k, 3)
            comboBox = mainWidget.layout().itemAt(0).widget()
            codemoney = comboBox.currentIndex() + 1
            mainWidget = self.tableOplaty.cellWidget(k, 4)
            calendar = mainWidget.layout().itemAt(0).widget()
            strdate = calendar.date()
            strDate = strdate.toString("yyyy-MM-dd")
            comment = self.tableOplaty.item(k, 5).text()
            sqli = 'INSERT INTO Periodica(Id, Name, Summa, CodeMoney, Date, Comment) VALUES(' + str(id) + ",'" + \
                  str(nameoper) + "'," + str(float(str(summa))) + "," + str(codemoney) + ",'" + strDate + "','" + str(comment) + "')"
            self.writeToDataBase(sqli)
        self.listNewRecords.clear()
        self.tableOplaty.resizeColumnsToContents()
        self.tableOplaty.resizeRowsToContents()
        sqlm = "SELECT * FROM Money"
        codeMoney = self.getFromDataBase(sqlm)
        moneyCodes = []
        for lm in codeMoney:
            moneyCodes.append(lm[1])
        arraySumma = [0 for i in range(len(moneyCodes))]
        arrayMoney = ['' for i in range(len(moneyCodes))]
        for i in range(self.tableOplaty.rowCount()):
            summa = self.tableOplaty.item(i, 2).text()
            mainWidget = self.tableOplaty.cellWidget(i, 3)
            comboBox = mainWidget.layout().itemAt(0).widget()
            arraySumma[comboBox.currentIndex()] += float(summa)
            arrayMoney[comboBox.currentIndex()] = comboBox.currentText().lower()
        summas = ''
        for i, ls in enumerate(arraySumma):
            if ls != 0:
                summas += str(ls) + arrayMoney[i] + ";"
        self.lineSummaOplaty.setText(summas)
        self.btnOplatySave.setText("Сохранить")
        self.btnOplatySave.setToolTip("Сохранить")
        return

    @pyqtSlot()
    def OnOplatyAdd(self):
        try:
            self.IndexRow = self.tableOplaty.rowCount()
            self.tableOplaty.insertRow(self.IndexRow)
            self.maxOplatyID += 1
            sqlm = "SELECT * FROM Money"
            codeMoney = self.getFromDataBase(sqlm)
            self.tableOplaty.setItem(self.IndexRow, 0, QTableWidgetItem(str(self.maxOplatyID)))
            self.tableOplaty.setItem(self.IndexRow, 1, QTableWidgetItem(''))
            self.tableOplaty.setItem(self.IndexRow, 2, QTableWidgetItem(''))
            comboBoxWidget = QWidget()
            moneyCombo = QComboBox()
            moneyCombo.setEditable(False)
            moneyCombo.setToolTip("Chiose code money")
            for lm in codeMoney:
                moneyCombo.addItem(lm[1])
            moneyCombo.setCurrentIndex(0)
            moneyCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            moneyCombo.adjustSize()
            layoutComboBox = QHBoxLayout(comboBoxWidget)
            layoutComboBox.addWidget(moneyCombo)
            layoutComboBox.setAlignment(Qt.AlignCenter)
            layoutComboBox.setContentsMargins(0, 0, 0, 0)
            self.tableOplaty.setCellWidget(self.IndexRow, 3, comboBoxWidget)
            calenadarWidget = QWidget(self)
            lineCalenader = QDateTimeEdit(self)
            lineCalenader.setDisplayFormat("dd-MM-yyyy")
            lineCalenader.setDate(QDate.currentDate())
            lineCalenader.setCalendarPopup(True)
            layoutCalendBox = QHBoxLayout(calenadarWidget)
            layoutCalendBox.addWidget(lineCalenader)
            layoutCalendBox.setAlignment(Qt.AlignCenter)
            layoutCalendBox.setContentsMargins(0, 0, 0, 0)
            self.tableOplaty.setCellWidget(self.IndexRow, 4, calenadarWidget)
            self.tableOplaty.setItem(self.IndexRow, 5, QTableWidgetItem(''))
            self.tableOplaty.resizeColumnsToContents()
            self.tableOplaty.resizeRowsToContents()
            self.listNewRecords.append(self.IndexRow)
            item = self.tableOplaty.item(self.IndexRow, 1)
            self.tableOplaty.selectRow(self.IndexRow)
            self.tableOplaty.scrollToItem(item)
            self.IndexRow += 1
            self.btnOplatySave.setText("*Сохранить: новую строку")
            self.btnOplatySave.setToolTip("*Сохранить: новую строку")
        except Exception as err:
            showError("mainWindow.OnOplatyAdd", str(err), traceback.format_exc())

    @pyqtSlot()
    def OnExportOplaty(self):
        try:
            if self.tableOplaty.rowCount() == 0:
                return

            dateFrom = self.dateFromOplaty.date()
            dateBegin = dateFrom.toString(self.dateFromOplaty.displayFormat())
            dateTo = self.dateTo.date()
            dateEnd = dateTo.toString(self.dateToOplaty.displayFormat())
            currentDate = datetime.now().strftime('%d-%m-%Y %H:%M:%S')
            wb = xlwt.Workbook()
            ws = wb.add_sheet("Отчет по оплатам")
            ws.write(0, 1, "Отчет 'Оплаты' за период с " + dateBegin + " по " + dateEnd)
            ws.write(1, 1, "Дата генерации: " + currentDate)
            ws.write(2, 0, self.tableOplaty.horizontalHeaderItem(1).text())
            ws.write(2, 1, self.tableOplaty.horizontalHeaderItem(2).text())
            ws.write(2, 2, self.tableOplaty.horizontalHeaderItem(3).text())
            ws.write(2, 3, self.tableOplaty.horizontalHeaderItem(4).text())
            ws.write(2, 4, self.tableOplaty.horizontalHeaderItem(5).text())
            m = 3
            for k in range(self.tableOplaty.rowCount()):
                operation = self.tableOplaty.item(k, 1).text()
                summa = self.tableOplaty.item(k, 2).text()
                mainWidget = self.tableOplaty.cellWidget(k, 3)
                comboBox = mainWidget.layout().itemAt(0).widget()
                namemoney = comboBox.currentText()
                mainWidget = self.tableOplaty.cellWidget(k, 4)
                calendar = mainWidget.layout().itemAt(0).widget()
                strdate = calendar.date()
                data = strdate.toString("yyyy-MM-dd")
                comment = self.tableOplaty.item(k, 5).text()
                ws.write(k + m, 0, operation)
                ws.write(k + m, 1, summa)
                ws.write(k + m, 2, namemoney)
                ws.write(k + m, 3, data)
                ws.write(k + m, 4, comment)
            currDate = datetime.now().strftime('%d-%m-%Y')
            namefile = 'Отчет_по_оплатам_' + currDate + '.xls'
            wb.save(namefile)
            showInfo("Info export", "Save data is successfull!", namefile)
        except Exception as err:
            self.showError("MainWindow.OnExportTick.", str(err), traceback.format_exc())

    @pyqtSlot()
    def setFilterOplaty(self):
        textfilt = self.lineFiltOplaty.text()

        if textfilt == '':
            for i in range(self.tableOplaty.rowCount()):
                self.tableOplaty.setRowHidden(i, False)
            sqlm = "SELECT * FROM Money"
            codeMoney = self.getFromDataBase(sqlm)
            moneyCodes = []
            for lm in codeMoney:
                moneyCodes.append(lm[1])
            arraySumma = [0 for i in range(len(moneyCodes))]
            arrayMoney = ['' for i in range(len(moneyCodes))]
            for i in range(self.tableOplaty.rowCount()):
                if not self.tableOplaty.isRowHidden(i):
                    summa = float(self.tableOplaty.item(i, 2).text())
                    mainWidget = self.tableOplaty.cellWidget(i, 3)
                    comboBox = mainWidget.layout().itemAt(0).widget()
                    current = comboBox.currentText()
                    indx = moneyCodes.index(current)
                    arraySumma[indx] += summa
                    arrayMoney[indx] = current.lower()
            summas = ''
            for i, ls in enumerate(arraySumma):
                if ls != 0:
                    summas += str(ls) + arrayMoney[i] + ";"
            self.lineSummaOplaty.setText(summas)
            return

        for i in range(self.tableOplaty.rowCount()):
            indxCol = self.filtComboOplaty.currentIndex() + 1
            item = self.tableOplaty.item(i, indxCol)
            if item:
                text = item.text()
                try:
                    text.index(textfilt)
                    self.tableOplaty.setRowHidden(i, False)
                except Exception as ex:
                    self.tableOplaty.setRowHidden(i, True)
            else:
                mainWidget = self.tableOplaty.cellWidget(i, indxCol)
                objectBox = mainWidget.layout().itemAt(0).widget()
                if type(objectBox) is QComboBox:
                    texttype = objectBox.currentText()
                    try:
                        texttype.index(textfilt)
                        self.tableOplaty.setRowHidden(i, False)
                    except Exception as ex:
                        self.tableOplaty.setRowHidden(i, True)
                elif type(objectBox) is QDateTimeEdit:
                    strdate = objectBox.date()
                    texttype = strdate.toString("dd-MM-yyyy")
                    try:
                        texttype.index(textfilt)
                        self.tableOplaty.setRowHidden(i, False)
                    except Exception as ex:
                        self.tableOplaty.setRowHidden(i, True)
        sqlm = "SELECT * FROM Money"
        codeMoney = self.getFromDataBase(sqlm)
        moneyCodes = []
        for lm in codeMoney:
            moneyCodes.append(lm[1])
        arraySumma = [0 for i in range(len(moneyCodes))]
        arrayMoney = ['' for i in range(len(moneyCodes))]
        for i in range(self.tableOplaty.rowCount()):
            if not self.tableOplaty.isRowHidden(i):
                summa = float(self.tableOplaty.item(i, 2).text())
                mainWidget = self.tableOplaty.cellWidget(i, 3)
                comboBox = mainWidget.layout().itemAt(0).widget()
                current = comboBox.currentText()
                indx = moneyCodes.index(current)
                arraySumma[indx] += summa
                arrayMoney[indx] = current.lower()
        summas = ''
        for i, ls in enumerate(arraySumma):
            if ls != 0:
                summas += str(ls) + arrayMoney[i] + ";"
        self.lineSummaOplaty.setText(summas)
        return

    @pyqtSlot()
    def OnSearchOplaty(self):
        try:
            row = self.searchRowOplaty
            while row < self.tableOplaty.rowCount():
                column = self.searchColumnOplaty
                while column < self.tableOplaty.columnCount():
                    item = self.tableOplaty.item(row, column)
                    if item:
                        try:
                            value = self.lineSearchOplaty.text()
                            item.text().index(value)
                            self.tableOplaty.selectRow(row)
                            self.tableOplaty.scrollToItem(item)
                            self.searchColumnOplaty = column + 1
                            self.searchRowOplaty = row
                            return
                        except Exception as ex:
                            pass
                    column += 1
                self.searchColumnOplaty = 0
                row += 1
            self.searchRowOplaty = 0
        except Exception as err:
            self.showError("MainWindow.OnSearchOplaty", str(err), traceback.format_exc())

    @pyqtSlot()
    def setFilterIdentifier(self):
        textfilt = self.lineFilt.text()

        if textfilt == '':
            for i in range(self.tableTickets.rowCount()):
                self.tableTickets.setRowHidden(i, False)
            rowCount = 0
            arraySumma = [0 for i in range(len(self.codeMoney))]
            arrayMoney = ['' for i in range(len(self.codeMoney))]
            sqlm = "SELECT * FROM Money"
            codeMoney = self.getFromDataBase(sqlm)
            moneyCodes = []
            for lm in codeMoney:
                moneyCodes.append(lm[1])
            for i in range(self.tableTickets.rowCount()):
                if not self.tableTickets.isRowHidden(i):
                    summa = float(self.tableTickets.item(i, 3).text())
                    current = self.tableTickets.item(i, 4).text()
                    indx = moneyCodes.index(current)
                    arraySumma[indx] += summa
                    arrayMoney[indx] = current.lower()
                    rowCount += 1
            summas = ''
            for i, ls in enumerate(arraySumma):
                if ls != 0:
                    summas += str(ls) + arrayMoney[i] + ";"
            self.lineSummaTick.setText(summas)
            self.lineNumRow.setText(str(rowCount))
            return

        for i in range(self.tableTickets.rowCount()):
            indxCol = self.filtCombo.currentIndex() + 1
            item = self.tableTickets.item(i, indxCol)
            if item:
                text = item.text()
                try:
                    text.index(textfilt)
                    self.tableTickets.setRowHidden(i, False)
                except Exception as ex:
                    self.tableTickets.setRowHidden(i, True)
        rowCount = 0
        arraySumma = [0 for i in range(len(self.codeMoney))]
        arrayMoney = ['' for i in range(len(self.codeMoney))]
        sqlm = "SELECT * FROM Money"
        codeMoney = self.getFromDataBase(sqlm)
        moneyCodes = []
        for lm in codeMoney:
            moneyCodes.append(lm[1])
        for i in range(self.tableTickets.rowCount()):
            if not self.tableTickets.isRowHidden(i):
                summa = float(self.tableTickets.item(i, 3).text())
                current = self.tableTickets.item(i, 4).text()
                indx = moneyCodes.index(current)
                arraySumma[indx] += summa
                arrayMoney[indx] = current.lower()
                rowCount += 1
        summas = ''
        for i, ls in enumerate(arraySumma):
            if ls != 0:
                summas += str(ls) + arrayMoney[i] + ";"
        self.lineSummaTick.setText(summas)
        self.lineNumRow.setText(str(rowCount))
        return

    def removeRows(self, table):
        k = 0
        while k < table.rowCount():
            table.removeRow(k)
        return

    @pyqtSlot()
    def OnClickTicket(self):
        dateFrom = self.dateFrom.date()
        strDateFrom = dateFrom.toString(self.dateFrom.displayFormat())
        dateTo = self.dateTo.date()
        strDateTo = dateTo.toString(self.dateTo.displayFormat())
        idTickets = self.typeTickCombo.currentIndex() + 1
        sql = "SELECT * FROM PowerTable WHERE (Date >= '" + strDateFrom + "' AND " + "Date <= '" + strDateTo + "') AND CodeTicket = " + str(idTickets)
        self.pawerTable = self.getFromDataBase(sql)
        sql = "SELECT * FROM Operations"
        self.operations = self.getFromDataBase(sql)
        sql = "SELECT * FROM Money"
        self.codeMoney = self.getFromDataBase(sql)
        sql = "SELECT * FROM CodeReport"
        self.codeReport = self.getFromDataBase(sql)
        irow = 0
        self.tableTickets.clearContents()
        self.tableTickets.setRowCount(len(self.pawerTable))
        arraySumma = [0 for i in range(len(self.codeMoney))]
        arrayMoney = ['' for i in range(len(self.codeMoney))]
        for ls in self.pawerTable:
            id = ls[0]
            numbertick = ls[1]
            codeoper = ""
            report = ""
            for lt in self.operations:
                if lt[0] == ls[3]:
                    codeoper = lt[1]
                    for lk in self.codeReport:
                        if lk[0] == lt[5]:
                            report = lk[1]
                            break
                    break
            summa = ls[4]
            codemoney = ""
            idMoney = 0
            for lt in self.codeMoney:
                if lt[0] == ls[5]:
                    idMoney = lt[0]
                    codemoney = lt[1]
                    break
            date = ls[10]
            comment = ls[11]
            self.tableTickets.setItem(irow, 0, QTableWidgetItem(str(id)))
            self.tableTickets.setItem(irow, 1, QTableWidgetItem(numbertick))
            self.tableTickets.setItem(irow, 2, QTableWidgetItem(codeoper))
            self.tableTickets.setItem(irow, 3, QTableWidgetItem(str(summa)))
            self.tableTickets.setItem(irow, 4, QTableWidgetItem(codemoney))
            self.tableTickets.setItem(irow, 5, QTableWidgetItem(report))
            self.tableTickets.setItem(irow, 6, QTableWidgetItem(date))
            self.tableTickets.setItem(irow, 7, QTableWidgetItem(comment))
            arraySumma[idMoney - 1] += summa
            arrayMoney[idMoney - 1] = codemoney.lower()
            irow += 1

        self.tableTickets.resizeColumnsToContents()
        self.tableTickets.resizeRowsToContents()
        self.tableTickets.setColumnHidden(0, True)
        self.lineNumRow.setText(str(self.tableTickets.rowCount()))
        summas = ''
        for i,ls in enumerate(arraySumma):
            if ls != 0:
                summas += str(ls) + arrayMoney[i] + ";"
        self.lineSummaTick.setText(summas)
        return

    def OpeningDataBase(self, fileName):
        if fileName:
            self.path2Database = fileName
            self.openDatabase(self.path2Database)
            sql = "SELECT * FROM Casher"
            res = self.getFromDataBase(sql)
            self.cashCombo.addItem(res[0][1])
            sql = "SELECT * FROM Persones"
            res = self.getFromDataBase(sql)
            counters1 = []
            counters2 = []
            for ls in res:
                counters1.append(ls[1])
                counters2.append(ls[1])
            completer1 = QCompleter(counters1)
            completer1.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
            completer1.popup().setStyleSheet("background-color: yellow")
            self.count1Combo.addItems(counters1)
            self.count1Combo.setCompleter(completer1)
            completer2 = QCompleter(counters2)
            completer2.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
            completer2.popup().setStyleSheet("background-color: yellow")
            self.count2Combo.addItems(counters2)
            self.count2Combo.setCompleter(completer2)
            sql = "SELECT * FROM CodeTickets"
            res = self.getFromDataBase(sql)
            for ls in res:
                self.tickCombo.addItem(ls[1])
        return

    @pyqtSlot()
    def OnOpenDb(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "All Files (*);;Python Files (*.db3)", options=options)
        if fileName:
            self.path2Database = fileName
            self.openDatabase(self.path2Database)
            sql = "SELECT * FROM Casher"
            res = self.getFromDataBase(sql)
            self.cashCombo.addItem(res[0][1])
            sql = "SELECT * FROM Persones"
            res = self.getFromDataBase(sql)
            counters1 = []
            counters2 = []
            for ls in res:
                counters1.append(ls[1])
                counters2.append(ls[1])
            completer1 = QCompleter(counters1)
            completer1.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
            completer1.popup().setStyleSheet("background-color: yellow")
            self.count1Combo.addItems(counters1)
            self.count1Combo.setCompleter(completer1)
            completer2 = QCompleter(counters2)
            completer2.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
            completer2.popup().setStyleSheet("background-color: yellow")
            self.count2Combo.addItems(counters2)
            self.count2Combo.setCompleter(completer2)
            sql = "SELECT * FROM CodeTickets"
            res = self.getFromDataBase(sql)
            for ls in res:
                self.tickCombo.addItem(ls[1])
        return

    def openDatabase(self, path):
        try:
            if self.connSqlite3 == "":
                self.connSqlite3 = sqlite3.connect(path)
                self.fillComboTicket()
        except Exception as err:
            self.connSqlite3 = ""
            showError(self, "Error", "Can not open database. " + str(err), path)
        return

    def getFromDataBase(self, sql):
        curr = self.connSqlite3.cursor()
        curr.execute(sql)
        data = list(curr.fetchall())
        curr.close()
        return data

    def fillComboTicket(self):
        if self.path2Database == "":
            return
        sql = "SELECT * FROM CodeTickets"
        self.codeTickets = self.getFromDataBase(sql)

        for ls in self.codeTickets:
            self.typeTickCombo.addItem(ls[1])
        return

    @pyqtSlot()
    def OnSearchTick(self):
        try:
            row = self.searchRow
            while row < self.tableTickets.rowCount():
                column = self.searchColumn
                while column < self.tableTickets.columnCount():
                    item = self.tableTickets.item(row, column)
                    if item:
                        try:
                            value = self.lineSearch.text()
                            item.text().index(value)
                            self.tableTickets.selectRow(row)
                            self.tableTickets.scrollToItem(item)
                            self.searchColumn = column + 1
                            self.searchRow = row
                            return
                        except Exception as err:
                            pass
                    column += 1
                self.searchColumn = 0
                row += 1
            self.searchRow = 0
        except Exception as err:
            self.showError("RetranslatorTableWidget.OnTextEdit", str(err), traceback.format_exc())

    @pyqtSlot()
    def OnExportTick(self):
        try:
            if self.tableTickets.rowCount() == 0:
                return

            dateFrom = self.dateFrom.date()
            dateBegin = dateFrom.toString(self.dateFrom.displayFormat())
            dateTo = self.dateTo.date()
            dateEnd = dateTo.toString(self.dateTo.displayFormat())
            currentDate = datetime.now().strftime('%d-%m-%Y %H:%M:%S')
            wb = xlwt.Workbook()
            ws = wb.add_sheet("Отчет по квитанциям")
            ws.write(0, 1, "Отчет '" + self.typeTickCombo.currentText() + "' за период с " + dateBegin + " по " + dateEnd)
            ws.write(1, 1, "Дата генерации: " + currentDate)
            ws.write(2, 0, self.tableTickets.horizontalHeaderItem(1).text())
            ws.write(2, 1, self.tableTickets.horizontalHeaderItem(2).text())
            ws.write(2, 2, self.tableTickets.horizontalHeaderItem(3).text())
            ws.write(2, 3, self.tableTickets.horizontalHeaderItem(4).text())
            ws.write(2, 4, self.tableTickets.horizontalHeaderItem(5).text())
            ws.write(2, 5, self.tableTickets.horizontalHeaderItem(6).text())
            ws.write(2, 6, self.tableTickets.horizontalHeaderItem(7).text())
            m = 3
            for k in range(self.tableTickets.rowCount()):
                numtick = self.tableTickets.item(k, 1).text()
                codeoper = self.tableTickets.item(k, 2).text()
                summa = self.tableTickets.item(k, 3).text()
                codemoney = self.tableTickets.item(k, 4).text()
                report = self.tableTickets.item(k, 5).text()
                data = self.tableTickets.item(k, 6).text()
                comment = self.tableTickets.item(k, 7).text()
                ws.write(k + m, 0, numtick)
                ws.write(k + m, 1, codeoper)
                ws.write(k + m, 2, summa)
                ws.write(k + m, 3, codemoney)
                ws.write(k + m, 4, report)
                ws.write(k + m, 5, data)
                ws.write(k + m, 6, comment)
            currDate = datetime.now().strftime('%d-%m-%Y')
            namefile = 'Отчет_по_квитанциям_' + currDate + '.xls'
            wb.save(namefile)
            showInfo("Info export", "Save data is successfull!", namefile)
        except Exception as err:
            self.showError("MainWindow.OnExportTick.", str(err), traceback.format_exc())


    def OnSaveStateParam(self):
        pass

    @pyqtSlot()
    def showAbout(self):
        pass

    @pyqtSlot()
    def showParameters(self):
        pass

    @pyqtSlot()
    def closed(self):
        if showInfo("Are you realy exit?") == self.OK_MSG:
            self.OnSaveStateParam()
            self.close()
        return

    def msgbtnInfo(self, i):
        if i.text() == 'Cancel':
            pass
        elif i.text() == 'OK':
            pass

    def msgbtnError(self, i):
        if i.text() == 'Cancel':
            pass
        elif i.text() == 'OK':
            pass

    def generMoneySumma(self, defer, txt):
        result = ''
        temp = txt.split(';')
        for ls in temp:
            for lk in defer:
                buff = ls.split(lk)
                if len(buff) == 2:
                    defer[lk] += float(buff[0])
                    break
        for ls in defer:
            if defer[ls] != 0:
                result += str(round(defer[ls], 2)) + ls + ";"
        return result

    def generDictSumma(self, defer, txt):
        temp = txt.split(';')
        for ls in temp:
            for lk in defer:
                buff = ls.split(lk)
                if len(buff) == 2:
                    defer[lk] += float(buff[0])
                    break
        return defer

    @pyqtSlot()
    def OnGenerateComplex(self):
        if self.connSqlite3 == '':
            showError("mainWindow.OnGenerateComplex", "Data base is clousing!", str(self.connSqlite3))
            return

        if self.lineBeginerComplex.text() == '':
            showError("mainWindow.OnGenerateComplex", "Is empty  lineBeginerComplex!", self.lineBeginerComplex.text())
            return

        self.tableComplex.clearContents()
        self.tableComplex.clear()
        self.tableComplex.setRowCount(0)
        self.tableComplex.setColumnCount(0)
        dateFrom = self.dateFromComplex.date()
        strDateFrom = dateFrom.toString(self.dateFromComplex.displayFormat())
        dateTo = self.dateToComplex.date()
        strDateTo = dateTo.toString(self.dateToComplex.displayFormat())
        sql = "SELECT * FROM Money"
        codeMoney = self.getFromDataBase(sql)
        dictMoney = {}
        dictValuta = {}
        for lt in codeMoney:
            dictMoney[lt[0]] = lt
            dictValuta[lt[1].lower()] = 0

        sql = "SELECT * FROM Operations WHERE CodeTicket IN (1,2,3) AND CodeReport IN (1,3)"
        codeOperations = self.getFromDataBase(sql)
        dictOperations = {}
        for k,element in enumerate(codeOperations):
            dictOperations[element[0]] = element

        sql = "SELECT * FROM Thematic WHERE Id <> 1"
        codeThematic = self.getFromDataBase(sql)
        dictThematic = {}
        for k, element in enumerate(codeThematic):
            dictThematic[element[0]] = element
        sql = "SELECT * FROM TypeOperation WHERE Id <> 4"
        codeOperat = self.getFromDataBase(sql)

        sql = "SELECT * FROM Periodica WHERE (Date >= '" + strDateFrom + "' AND " + "Date <= '" + strDateTo + "')"
        periodica = self.getFromDataBase(sql)

        dateBegin = self.dateFromComplex.date()
        dateB = dateBegin.addMonths(-1)
        strDateBegin = dateB.toString(self.dateFromComplex.displayFormat())
        dateEnd = self.dateToComplex.date()
        dateE = dateEnd.addMonths(-1)
        strDateEnd = dateE.toString(self.dateFromComplex.displayFormat())

        sql = "SELECT * FROM Balances WHERE (Date >= '" + strDateBegin + "' AND " + "Date <= '" + strDateEnd + "')"
        balances = self.getFromDataBase(sql)
        dictBalance = {}
        for k, element in enumerate(balances):
            dictBalance[element[1]] = element[2]

        dictThemat = {}
        existCol = {}
        for lt in codeThematic:
            sql = "SELECT * FROM Operations WHERE CodeTicket IN (1,2,3) AND CodeReport IN (1,3) AND Thema = " + str(lt[0])
            codeOperations = self.getFromDataBase(sql)
            codeOper = []
            for ls in codeOperations:
                codeOper.append(ls[0])
            strOper = str(codeOper)
            strOperTmp = strOper.replace('[', '(')
            strOper = strOperTmp.replace(']', ')')

            sql = "SELECT * FROM PowerTable WHERE (Date >= '" + strDateFrom + "' AND " + "Date <= '" + strDateTo + "') AND CodeOperation IN " + strOper
            powerData = self.getFromDataBase(sql)
            dictPower = {}
            for lk in codeOperat:
                for ls in powerData:
                    if ls[2] == lk[0]:
                        try:
                            existCol[lk[0]] = True
                            dictPower[lk[0]].append(ls)
                        except Exception as ex:
                            dictPower[lk[0]] = []
                            dictPower[lk[0]].append(ls)
            dictThemat[lt[0]] = dictPower

        id = 100000
        for lt in periodica:
            new_record = (id, 'Periodica', 2, 667, lt[2], lt[3], 67, 1, 69, 69, lt[4])
            id += 1
            try:
                dictThemat[4][2].append(new_record)
            except Exception as er:
                dictThemat[4][2] = []
                dictThemat[4][2].append(new_record)

        columnAll = 11
        self.tableComplex.setColumnCount(columnAll)
        columnComplex = []
        columnComplex.append("Название группы")
        columnComplex.append("Процент")
        columnComplex.append("Сумма нецелевых приношений")
        columnComplex.append("Название приношений")
        columnComplex.append("Сумма приношений")
        columnComplex.append("Название расходов")
        columnComplex.append("Сумма расходов")
        columnComplex.append("Название возвратов")
        columnComplex.append("Сумма возвратов")
        columnComplex.append("Сумма с прошлого периода")
        columnComplex.append("Сумма баланса")
        self.tableComplex.setHorizontalHeaderLabels(columnComplex)
        self.tableComplex.horizontalHeaderItem(0).setToolTip("Название группы средств")
        self.tableComplex.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableComplex.horizontalHeaderItem(1).setToolTip("Процент от нецелевых и целевых раходов")
        self.tableComplex.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableComplex.horizontalHeaderItem(2).setToolTip("Сумма от нецелевых и целевых раходов")
        self.tableComplex.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableComplex.horizontalHeaderItem(3).setToolTip("Название статьи приношения")
        self.tableComplex.horizontalHeaderItem(3).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableComplex.horizontalHeaderItem(4).setToolTip("Сумма статьи приношения")
        self.tableComplex.horizontalHeaderItem(4).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableComplex.horizontalHeaderItem(5).setToolTip("Название статьи расхода")
        self.tableComplex.horizontalHeaderItem(5).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableComplex.horizontalHeaderItem(6).setToolTip("Сумма статьи расхода")
        self.tableComplex.horizontalHeaderItem(6).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableComplex.horizontalHeaderItem(7).setToolTip("Название статьи возврата")
        self.tableComplex.horizontalHeaderItem(7).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableComplex.horizontalHeaderItem(8).setToolTip("Сумма статьи возврата")
        self.tableComplex.horizontalHeaderItem(8).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableComplex.horizontalHeaderItem(9).setToolTip("Сумма с прошлого периода")
        self.tableComplex.horizontalHeaderItem(9).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableComplex.horizontalHeaderItem(10).setToolTip("Сумма баланса")
        self.tableComplex.horizontalHeaderItem(10).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)

        for k,skey in enumerate(dictThemat):
            beginIndex = self.tableComplex.rowCount()
            for j,pkey in enumerate(dictThemat[skey]):
                countRow = beginIndex
                for l,tkey in enumerate(dictThemat[skey][pkey]):
                    rowPosition = self.tableComplex.rowCount()
                    if countRow >= rowPosition:
                        self.tableComplex.insertRow(rowPosition)
                        name = dictOperations[tkey[3]][1]
                        summa = str(tkey[4]) + dictMoney[tkey[5]][1].lower()
                        typeState = tkey[2]
                        if 1 == typeState:
                            numColumn = 3
                        elif 2 == typeState:
                            numColumn = 5
                        elif 3 == typeState:
                            numColumn = 7
                        else:
                            raise ValueError("Error type state, unknown type!")
                        self.tableComplex.setItem(countRow, numColumn, QTableWidgetItem(name))
                        self.tableComplex.setItem(countRow, numColumn + 1, QTableWidgetItem(summa))
                    else:
                        name = dictOperations[tkey[3]][1]
                        summa = str(tkey[4]) + dictMoney[tkey[5]][1].lower()
                        typeState = tkey[2]
                        if 1 == typeState:
                            numColumn = 3
                        elif 2 == typeState:
                            numColumn = 5
                        elif 3 == typeState:
                            numColumn = 7
                        else:
                            raise ValueError("Error type state, unknown type!")
                        self.tableComplex.setItem(countRow, numColumn, QTableWidgetItem(name))
                        self.tableComplex.setItem(countRow, numColumn + 1, QTableWidgetItem(summa))
                    countRow += 1
            rowPosition = self.tableComplex.rowCount()
            self.tableComplex.insertRow(rowPosition)
            nameGroup = dictThematic[skey][1]
            self.tableComplex.setItem(beginIndex, 0, QTableWidgetItem(nameGroup))
            percentGroup = str(dictThematic[skey][3])
            if percentGroup != '0.0':
                self.tableComplex.setItem(beginIndex, 1, QTableWidgetItem(percentGroup))
            if rowPosition == beginIndex:
                rowPosition = self.tableComplex.rowCount()
                self.tableComplex.insertRow(rowPosition)
            self.tableComplex.setItem(rowPosition, 0, QTableWidgetItem("Всего:"))

        indx = 0
        count = 0
        begin = 0
        while count < self.tableComplex.rowCount():
            summaServ = ''
            for k in range(begin, self.tableComplex.rowCount(), 1):
                count += 1
                try:
                    vsego = self.tableComplex.item(k, 0).text()
                except Exception as ex:
                    vsego = ''
                if vsego == "Всего:":
                    indx = k
                    break
                try:
                    summaServ = summaServ + self.tableComplex.item(k, 4).text() + ";"
                except Exception as ex:
                    pass

            if summaServ != '':
                valuta = copy.deepcopy(dictValuta)
                summsrv = self.generMoneySumma(valuta, summaServ)
                self.tableComplex.setItem(indx, 4, QTableWidgetItem(summsrv))
            begin = indx + 1

        indx = 0
        count = 0
        begin = 0
        while count < self.tableComplex.rowCount():
            summaSubs = ''
            for k in range(begin, self.tableComplex.rowCount(), 1):
                count += 1
                try:
                    vsego = self.tableComplex.item(k, 0).text()
                except Exception as ex:
                    vsego = ''
                if vsego == "Всего:":
                    indx = k
                    break
                try:
                    summaSubs = summaSubs + self.tableComplex.item(k, 6).text() + ";"
                except Exception as ex:
                    pass

            if summaSubs != '':
                valuta = copy.deepcopy(dictValuta)
                summsubs = self.generMoneySumma(valuta, summaSubs)
                self.tableComplex.setItem(indx, 6, QTableWidgetItem(summsubs))
            begin = indx + 1

        indx = 0
        count = 0
        begin = 0
        while count < self.tableComplex.rowCount():
            summaAdd = ''
            for k in range(begin, self.tableComplex.rowCount(), 1):
                count += 1
                try:
                    vsego = self.tableComplex.item(k, 0).text()
                except Exception as ex:
                    vsego = ''
                if vsego == "Всего:":
                    indx = k
                    break
                try:
                    summaAdd = summaAdd + self.tableComplex.item(k, 8).text() + ";"
                except Exception as ex:
                    pass

            if summaAdd != '':
                valuta = copy.deepcopy(dictValuta)
                summadd = self.generMoneySumma(valuta, summaAdd)
                self.tableComplex.setItem(indx, 8, QTableWidgetItem(summadd))
            begin = indx + 1

        begin = 0
        summaServ = ''
        dictsumma = copy.deepcopy(dictValuta)
        valuta = copy.deepcopy(dictValuta)
        for k in range(begin, self.tableComplex.rowCount(), 1):
            try:
                vsego = self.tableComplex.item(k, 0).text()
            except Exception as ex:
                vsego = ''
            if vsego == "Всего:":
                break
            try:
                summaServ = self.tableComplex.item(k, 4).text()
            except Exception as ex:
                pass
            if summaServ != '':
                dictsumma = self.generDictSumma(valuta, summaServ)
            else:
                dictsumma = copy.deepcopy(dictValuta)

        indx = 0
        begin = 0
        while indx < self.tableComplex.rowCount():
            for k in range(begin, self.tableComplex.rowCount(), 1):
                try:
                    vsego = self.tableComplex.item(k, 0).text()
                except Exception as ex:
                    vsego = ''
                if vsego == "Всего:":
                    indx = k + 1
                    break
            try:
                summa = float(self.tableComplex.item(indx, 1).text()) / 100.0
            except Exception as ex:
                summa = ''

            if summaServ != '':
                if summa != '':
                    txtsumma = ''
                    for key in dictsumma:
                        txtsumma += str(dictsumma[key] * summa) + key + ";"
                    valuta = copy.deepcopy(dictValuta)
                    strsumma = self.generMoneySumma(valuta, txtsumma)
                    if strsumma != '':
                        point = 0
                        for j in range(indx, self.tableComplex.rowCount(), 1):
                            try:
                                vsego = self.tableComplex.item(j, 0).text()
                            except Exception as ex:
                                vsego = ''
                            if vsego == "Всего:":
                                point = j
                                break
                        if point != 0:
                            self.tableComplex.setItem(point, 2, QTableWidgetItem(strsumma))
            begin = indx

        indx = 0
        begin = 0
        count = 0
        m = 0
        allSummaServ = ''
        while m < self.tableComplex.rowCount():
            for k in range(begin, self.tableComplex.rowCount(), 1):
                m += 1
                try:
                    vsego = self.tableComplex.item(k, 0).text()
                except Exception as ex:
                    vsego = ''
                if vsego == "Всего:":
                    indx = k
                    if count != 0:
                        break
                    count += 1
            try:
                summa = self.tableComplex.item(indx, 4).text()
            except Exception as ex:
                summa = ''

            if summa != '':
                allSummaServ += summa + ';'
            begin = indx + 1
        valuta = copy.deepcopy(dictValuta)
        allDictSummaServ = self.generMoneySumma(valuta, allSummaServ)

        indx = 0
        begin = 0
        count = 0
        m = 0
        allSummaSubs = ''
        while m < self.tableComplex.rowCount():
            for k in range(begin, self.tableComplex.rowCount(), 1):
                m += 1
                try:
                    vsego = self.tableComplex.item(k, 0).text()
                except Exception as ex:
                    vsego = ''
                if vsego == "Всего:":
                    indx = k
                    if count != 0:
                        break
                    count += 1
            try:
                summa = self.tableComplex.item(indx, 6).text()
            except Exception as ex:
                summa = ''

            if summa != '':
                allSummaSubs += summa + ";"
            begin = indx + 1
        valuta = copy.deepcopy(dictValuta)
        allDictSummaSubs = self.generMoneySumma(valuta, allSummaSubs)

        indx = 0
        begin = 0
        count = 0
        m = 0
        allSummaAdd = ''
        while m < self.tableComplex.rowCount():
            for k in range(begin, self.tableComplex.rowCount(), 1):
                m += 1
                try:
                    vsego = self.tableComplex.item(k, 0).text()
                except Exception as ex:
                    vsego = ''
                if vsego == "Всего:":
                    indx = k
                    if count != 0:
                        break
                    count += 1
            try:
                summa = self.tableComplex.item(indx, 8).text()
            except Exception as ex:
                summa = ''

            if summa != '':
                allSummaAdd += summa + ";"
            begin = indx + 1
        valuta = copy.deepcopy(dictValuta)
        allDictSummaAdd = self.generMoneySumma(valuta, allSummaAdd)

        rowPosition = self.tableComplex.rowCount()
        self.tableComplex.insertRow(rowPosition)
        self.tableComplex.setItem(rowPosition, 0, QTableWidgetItem('Итого:'))
        self.tableComplex.setItem(rowPosition, 1, QTableWidgetItem('100'))
        self.tableComplex.setItem(rowPosition, 2, QTableWidgetItem(self.generStringSumma(dictsumma)))
        self.tableComplex.setItem(rowPosition, 4, QTableWidgetItem(allDictSummaServ))
        self.tableComplex.setItem(rowPosition, 6, QTableWidgetItem(allDictSummaSubs))
        self.tableComplex.setItem(rowPosition, 8, QTableWidgetItem(allDictSummaAdd))

        getrow = []
        for k in range(0, self.tableComplex.rowCount(), 1):
            try:
                vsego = self.tableComplex.item(k, 0).text()
            except Exception as ex:
                vsego = ''
            if vsego == "Всего:":
                getrow.append(k)
        del getrow[0]

        buffIndxGroup = []
        for key in dictThemat:
            buffIndxGroup.append(key)
        del buffIndxGroup[0]

        for k, key in enumerate(buffIndxGroup):
            balans = dictBalance[key]
            rown = getrow[k]
            self.tableComplex.setItem(rown, columnAll - 2, QTableWidgetItem(balans))

        for row in getrow:
            dictsumma = copy.deepcopy(dictValuta)

            try:
                summa = self.tableComplex.item(row, 2).text()
            except Exception as tir:
                summa = ''
            if summa != '':
                valuta = copy.deepcopy(dictValuta)
                tempSumma = self.generDictSumma(valuta, summa)
                for key in tempSumma:
                    dictsumma[key] = dictsumma[key] + tempSumma[key]
            try:
                summa = self.tableComplex.item(row, 4).text()
            except Exception as tir:
                summa = ''
            if summa != '':
                valuta = copy.deepcopy(dictValuta)
                tempSumma = self.generDictSumma(valuta, summa)
                for key in tempSumma:
                    dictsumma[key] = dictsumma[key] + tempSumma[key]

            try:
                summa = self.tableComplex.item(row, 6).text()
            except Exception as tir:
                summa = ''
            if summa != '':
                valuta = copy.deepcopy(dictValuta)
                tempSumma = self.generDictSumma(valuta, summa)
                for key in tempSumma:
                    dictsumma[key] = dictsumma[key] - tempSumma[key]

            try:
                summa = self.tableComplex.item(row, 8).text()
            except Exception as tir:
                summa = ''
            if summa != '':
                valuta = copy.deepcopy(dictValuta)
                tempSumma = self.generDictSumma(valuta, summa)
                for key in tempSumma:
                    dictsumma[key] = dictsumma[key] + tempSumma[key]
            try:
                summa = self.tableComplex.item(row, self.tableComplex.columnCount() - 2).text()
            except Exception as tir:
                summa = ''
            if summa != '':
                valuta = copy.deepcopy(dictValuta)
                tempSumma = self.generDictSumma(valuta, summa)
                for key in tempSumma:
                    dictsumma[key] = dictsumma[key] + tempSumma[key]
            strsumm = ''
            for key in dictsumma:
                if dictsumma[key] != 0:
                    strsumm += str(round(dictsumma[key], 2)) + key + ";"
            self.tableComplex.setItem(row, self.tableComplex.columnCount() - 1, QTableWidgetItem(strsumm))

        rowPosition = self.tableComplex.rowCount()
        self.tableComplex.insertRow(rowPosition)
        try:
            old_budget = self.lineBeginerComplex.text()
        except Exception as er:
            old_budget = "0uah;"
        self.tableComplex.setItem(rowPosition, 0, QTableWidgetItem('Начальное значение бюджета на:' + strDateFrom))
        self.tableComplex.setItem(rowPosition, 2, QTableWidgetItem(old_budget))
        column = self.tableComplex.columnCount()
        self.tableComplex.setItem(rowPosition, column - 2, QTableWidgetItem('Конечное значение бюджета на:' + strDateTo))
        dictsumma = copy.deepcopy(dictValuta)
        index = 0
        for k in range(0, self.tableComplex.rowCount(), 1):
            try:
                vsego = self.tableComplex.item(k, 0).text()
            except Exception as ex:
                vsego = ''
            if vsego == "Итого:":
                index = k
        try:
            summa60 = self.tableComplex.item(index, 2).text()
        except Exception as er:
            summa60 = "0uah;"
        try:
            summaserver = self.tableComplex.item(index, 4).text()
        except Exception as er:
            summaserver = "0uah;"
        try:
            summasubb = self.tableComplex.item(index, 6).text()
        except Exception as er:
            summasubb = "0uah;"
        try:
            summaadd = self.tableComplex.item(index, 8).text()
        except Exception as er:
            summaadd = "0uah;"

        valuta = copy.deepcopy(dictValuta)
        dictOldBudg = self.generDictSumma(valuta, old_budget)
        dictsumma = self.summaDictSumm(dictsumma, dictOldBudg)
        valuta = copy.deepcopy(dictValuta)
        dict60 = self.generDictSumma(valuta, summa60)
        dictsumma = self.summaDictSumm(dictsumma, dict60)
        valuta = copy.deepcopy(dictValuta)
        dictserv = self.generDictSumma(valuta, summaserver)
        dictsumma = self.summaDictSumm(dictsumma, dictserv)
        valuta = copy.deepcopy(dictValuta)
        dictsubs = self.generDictSumma(valuta, summasubb)
        dictsumma = self.subDictSumm(dictsumma, dictsubs)
        valuta = copy.deepcopy(dictValuta)
        dictadd = self.generDictSumma(valuta, summaadd)
        dictsumma = self.summaDictSumm(dictsumma, dictadd)
        dictsumma['uah'] = round(dictsumma['uah'], 2)
        new_budget = self.generStringSumma(dictsumma)
        self.tableComplex.setItem(rowPosition, column - 1, QTableWidgetItem(new_budget))

        row = 0
        for k in range(0, self.tableComplex.rowCount(), 1):
            try:
                vsego = self.tableComplex.item(k, 0).text()
            except Exception as ex:
                vsego = ''
            if vsego == "Итого:":
                row = k
                break

        try:
            summa60 = self.tableComplex.item(row, 2).text()
            valuta = copy.deepcopy(dictValuta)
            dictsumma60 = self.generDictSumma(valuta, summa60)
        except Exception as ex:
            dictsumma60 = copy.deepcopy(dictValuta)
        try:
            summaserv = self.tableComplex.item(row, 4).text()
            valuta = copy.deepcopy(dictValuta)
            dictsummaserv = self.generDictSumma(valuta, summaserv)
        except Exception as ex:
            dictsummaserv = copy.deepcopy(dictValuta)
        try:
            summasub = self.tableComplex.item(row, 6).text()
            valuta = copy.deepcopy(dictValuta)
            dictsummasub = self.generDictSumma(valuta, summasub)
        except Exception as ex:
            dictsummasub = copy.deepcopy(dictValuta)
        try:
            summaadd = self.tableComplex.item(row, 8).text()
            valuta = copy.deepcopy(dictValuta)
            dictsummaadd = self.generDictSumma(valuta, summaadd)
        except Exception as ex:
            dictsummaadd = copy.deepcopy(dictValuta)

        resultBalance = self.summaDictSumm(dictsumma60, dictsummaserv)
        resultBalance = self.subDictSumm(resultBalance, dictsummasub)
        resultBalance = self.summaDictSumm(resultBalance, dictsummaadd)
        result = self.generStringSumma(resultBalance)
        self.tableComplex.setItem(row, column - 1, QTableWidgetItem(result))

        buf_current_balans = []
        for row in getrow:
            try:
                vsego = self.tableComplex.item(row, column - 1).text()
                buf_current_balans.append(vsego)
            except Exception as ex:
                buf_current_balans.append('0uah;')
        sql = "SELECT * FROM Balances WHERE (Date >= '" + strDateFrom + "' AND " + "Date <= '" + strDateTo + "')"
        current_balances = self.getFromDataBase(sql)
        sql = "SELECT max(Id) FROM Balances"
        idx = self.getFromDataBase(sql)
        id = idx[0][0]
        dateBegin = self.dateFromComplex.date()
        dateB = dateBegin.addDays(10)
        currDate = dateB.toString(self.dateFromComplex.displayFormat())
        if len(current_balances) > 0:
            dctBlns = {}
            for ls in current_balances:
                dctBlns[ls[1]] = ls[0]
            for k, idgrp in enumerate(buffIndxGroup):
                curbalans = buf_current_balans[k]
                if curbalans == '':
                    curbalans = '0uah;'
                id = dctBlns[idgrp]
                sql = "UPDATE Balances SET " + "SummaStr=" + "'" + curbalans + "'" + " WHERE Id=" + str(id) + " AND FondId=" + str(idgrp)
                self.writeToDataBase(sql)
        else:
            for k, idgrp in enumerate(buffIndxGroup):
                curbalans = buf_current_balans[k]
                if curbalans == '':
                    curbalans = '0uah;'
                id += 1
                sql = "INSERT INTO Balances(Id,FondId,SummaStr,Date) VALUES (" + str(id) + "," + str(idgrp) + ",'" + curbalans + "','" + currDate + "')"
                self.writeToDataBase(sql)

        self.tableComplex.resizeColumnsToContents()
        self.tableComplex.resizeRowsToContents()
        return

    def generStringSumma(self, dicts):
        result = ''
        for lk in dicts:
            if dicts[lk] != 0:
                result += str(dicts[lk]) + lk + ';'
        return result

    def summaDictSumm(self, dict1, dict2):
        for key in dict2:
            dict1[key] = dict1[key] + dict2[key]
        return dict1

    def subDictSumm(self, dict1, dict2):
        for key in dict2:
            dict1[key] = dict1[key] - dict2[key]
        return dict1

    @pyqtSlot()
    def OnExporterComplex(self):
        try:
            if self.tableComplex.rowCount() == 0:
                return

            dateFrom = self.dateFromComplex.date()
            strDateFrom = dateFrom.toString(self.dateFromComplex.displayFormat())
            dateTo = self.dateToComplex.date()
            strDateTo = dateTo.toString(self.dateToComplex.displayFormat())
            currentDate = datetime.now().strftime('%d-%m-%Y %H:%M:%S')
            wb = xlwt.Workbook()
            ws = wb.add_sheet("Отчет комплексный")
            ws.write(0, 0, "Отчет комплексный за период с " + strDateFrom + " по " + strDateTo)
            ws.write(1, 0, "Дата генерации отчета: " + currentDate)
            ws.write(1, 1, "ЦК АСД Боярка")
            ws.write(2, 0, self.tableComplex.horizontalHeaderItem(0).text())
            ws.write(2, 1, self.tableComplex.horizontalHeaderItem(1).text())
            ws.write(2, 2, self.tableComplex.horizontalHeaderItem(2).text())
            ws.write(2, 3, self.tableComplex.horizontalHeaderItem(3).text())
            ws.write(2, 4, self.tableComplex.horizontalHeaderItem(4).text())
            ws.write(2, 5, self.tableComplex.horizontalHeaderItem(5).text())
            ws.write(2, 6, self.tableComplex.horizontalHeaderItem(6).text())
            ws.write(2, 7, self.tableComplex.horizontalHeaderItem(7).text())
            ws.write(2, 8, self.tableComplex.horizontalHeaderItem(8).text())
            ws.write(2, 9, self.tableComplex.horizontalHeaderItem(9).text())
            ws.write(2, 10, self.tableComplex.horizontalHeaderItem(10).text())
            m = 3
            for k in range(self.tableComplex.rowCount()):
                try:
                    numtick = self.tableComplex.item(k, 0).text()
                except Exception as er:
                    numtick = ' '
                try:
                    codeoper = self.tableComplex.item(k, 1).text()
                except Exception as er:
                    codeoper = ' '
                try:
                    summa = self.tableComplex.item(k, 2).text()
                except Exception as er:
                    summa = ' '
                try:
                    codemoney = self.tableComplex.item(k, 3).text()
                except Exception as er:
                    codemoney = ' '
                try:
                    report = self.tableComplex.item(k, 4).text()
                except Exception as er:
                    report = ' '
                try:
                    data = self.tableComplex.item(k, 5).text()
                except Exception as er:
                    data = ' '
                try:
                    comment = self.tableComplex.item(k, 6).text()
                except Exception as er:
                    comment = ' '
                try:
                    comment1 = self.tableComplex.item(k, 7).text()
                except Exception as er:
                    comment1 = ' '
                try:
                    comment2 = self.tableComplex.item(k, 8).text()
                except Exception as er:
                    comment2 = ' '
                try:
                    comment3 = self.tableComplex.item(k, 9).text()
                except Exception as er:
                    comment3 = ' '
                try:
                    comment4 = self.tableComplex.item(k, 10).text()
                except Exception as er:
                    comment4 = ' '

                ws.write(k + m, 0, numtick)
                ws.write(k + m, 1, codeoper)
                ws.write(k + m, 2, summa)
                ws.write(k + m, 3, codemoney)
                ws.write(k + m, 4, report)
                ws.write(k + m, 5, data)
                ws.write(k + m, 6, comment)
                ws.write(k + m, 7, comment1)
                ws.write(k + m, 8, comment2)
                ws.write(k + m, 9, comment3)
                ws.write(k + m, 10, comment4)

            dateFrom = self.dateFromComplex.date()
            strDateFrom = dateFrom.toString(self.dateFromComplex.displayFormat())
            dateTo = self.dateToComplex.date()
            strDateTo = dateTo.toString(self.dateToComplex.displayFormat())
            namefile = 'Комплексный_отчет_по_отделам_' + strDateFrom + '__' + strDateTo + '_' + '.xls'
            wb.save(namefile)
            showInfo("Info export", "Save data complex is successfull!", namefile)
        except Exception as err:
            self.showError("MainWindow.OnExportTick.", str(err), traceback.format_exc())

def showInfo(inform, additional='', details=''):
    try:
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(inform)
        msg.setInformativeText(additional)
        msg.setWindowTitle("Information")
        msg.setDetailedText(details)
        msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
        retval = msg.exec_()
        return retval
    except Exception as ex:
        pass

def showError(inform, additional='', details=''):
    try:
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText(inform)
        msg.setInformativeText(additional)
        msg.setWindowTitle("Error")
        msg.setDetailedText(details)
        msg.setStandardButtons(QMessageBox.Ok)
        retval = msg.exec_()
        return retval
    except Exception as ex:
        pass

if __name__ == "__main__":
    start = time.time()
    _PID = os.getpid()
    print("Starting application, wait please...")
    app = QtWidgets.QApplication(sys.argv)
    mw = MainWindow()
    mw.show()
    ret = app.exec_()
    end = time.time()
    print("Application Working with ", round(end - start, 2), " seconds")
    sys.exit(ret)
