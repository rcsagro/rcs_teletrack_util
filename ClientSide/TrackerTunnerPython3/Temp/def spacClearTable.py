	def spacClearTable(self, qwtable):
		try:
			qwtable.clearContents()
			number = qwtable.rowCount()
			while number > 0:
				qwtable.removeRow(0)
				number = qwtable.rowCount()
		except Exception as err:
			self.showError("RouterTableWidget.spacClearTable", str(err), traceback.format_exc())
		return


		self.spacClearTable(self.tableDevices)

		