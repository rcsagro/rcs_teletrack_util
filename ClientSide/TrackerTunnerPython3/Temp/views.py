# -*- encoding: utf-8 -*-
from flask import Blueprint
from flask import request,session
from app.db_provider.redis_dbprovider import RedisDataProvider
from flask import jsonify
from app.entities.user import User
from app.helpers.Json_helper import support_jsonp
import app.helpers.consts as consts
from app import app
import datetime
import app.helpers.service as Service
import app.helpers.decorators as decorators
import app.postgresql.azs.users as AzsUsersDb
import app.helpers.consts as Consts
from app.helpers.Json_helper import support_jsonp_par

apiAuth = Blueprint('apiAuthPart',__name__,url_prefix='/api/auth/')
redis = RedisDataProvider()

@apiAuth.route('/',methods=['POST'])
#@support_jsonp
def index():
    try:
         res = Service.getError(consts.not_user_identified)
         #app.logger.critical('%s  start'%(datetime.datetime.now().strftime('%H:%M:%S')))
         if request.json:
             req_json = request.get_json()
             if req_json.has_key('login') and req_json.has_key('password'):
                 if len(req_json['login'])>0 and len(req_json['password'])>0:
                    login = req_json['login']
                    password = req_json['password']
                    # app.logger.critical('%s  login:%s password:%s'%(datetime.datetime.now().strftime('%H:%M:%S'),login,password))
                    user = User()
                    #app.logger.critical('%s  initByLoginPassword'%(datetime.datetime.now().strftime('%H:%M:%S')))
                    res,isExist = user.initByLoginPassword(login,password)
                    if isExist:
                        if user.is_authenticated():
                            if req_json.has_key('token') and req_json['token'] and len(req_json['token'])>10:
                                #app.logger.critical('%s  activateToken '%(datetime.datetime.now().strftime('%H:%M:%S')))
                                if req_json.has_key('not_expire') :
                                    isExpire = False
                                else:
                                    isExpire = True
                                redis.activateToken(user,req_json['token'],isExpire)
                                token = 'success'
                            else:
                                #app.logger.critical('%s  getTokenNew '%(datetime.datetime.now().strftime('%H:%M:%S')))
                                token = redis.getTokenNew()
                            res = Service.getSelectData(token)
                            _setAzsInfo(res, user)
                    else:
                        res = Service.getError(res)
         #app.logger.critical('%s  return res %s'%(datetime.datetime.now().strftime('%H:%M:%S'),res))
         return jsonify(res)
    except Exception as ex:
         app.logger.critical('%s Error authentication : %s res: %s'%(datetime.datetime.now().strftime('%H:%M:%S'),ex.message,res))
         ret = Service.getError(ex.message)
    return jsonify(ret)

@apiAuth.route('mobile/',methods=['POST'])
#@support_jsonp
def mobile():
    try:
         res = Service.getError(consts.not_user_identified)
         #app.logger.critical('%s  start'%(datetime.datetime.now().strftime('%H:%M:%S')))
         if request.json:
             req_json = request.get_json()
             if req_json.has_key('login') and req_json.has_key('password'):
                 if len(req_json['login'])>0 and len(req_json['password'])>0:
                    login = req_json['login']
                    password = req_json['password']
                    user = User()
                    res,isExist = user.initByLoginPassword(login,password)
                    if isExist:
                        if user.is_authenticated():
                            token = redis.getTokenNew()
                            redis.activateToken(user, token, False)
                            redis.setIsTokenForMobile(token)
                            res = Service.getSelectData(token)
                            _setAzsInfo(res, user)
                    else:
                        res = Service.getError(res)
         #app.logger.critical('%s  return res %s'%(datetime.datetime.now().strftime('%H:%M:%S'),res))
         return jsonify(res)
    except Exception as ex:
         app.logger.critical('%s Error mobile authentication: %s res: %s'%(datetime.datetime.now().strftime('%H:%M:%S'),ex.message,res))
         ret = Service.getError(ex.message)
    return jsonify(ret)

def _setAzsInfo(res, user):
    # ------------------A Z S------------------------------
    if user.is_system():
        res['azsAdmin'] = 1
    else:
        isAccesAzs, isAdmin, idAzs, nameAzs = getAzsRole(user.id)
        if isAccesAzs:
            res['azsAdmin'] = isAdmin
            res['azsId'] = idAzs
            res['azsName'] = nameAzs
            res['userName'] = user.name
            # else:
            #   res['azsAdmin'] = 'Not access %s'%user.id
            # ------------------A Z S------------------------------


@apiAuth.route('getToken/',methods=['GET'])
@support_jsonp
def getToken():
    res = consts.private_method
    if 'userId' in session:
         res = consts.not_all_parameters
         if 'parentType' in request.args and 'parent' in request.args:
            userId = session['userId']
            user = User()
            user.createFromDb(userId)
            token = redis.getTokenNew()
            redis.activateTokenAnotherUser(user,token, request.args.get('parentType'), request.args.get('parent'))
            res = Service.getSelectData(token)
    return jsonify(res)

@apiAuth.route('getUserInfo/',methods=['GET'])
@support_jsonp
def getUserInfo():
    res = Service.getError(Consts.not_all_parameters)
    if 'token' in request.args:
        token = request.args.get('token')
        res = Service.getError(Consts.token_not_valid)
        if redis.isTokenValid(token):
            res = {}
            user = User()
            dictParams = redis.getUserParams(token)
            # app.logger.critical('%s getUserInfo: dictParams parent %s  parentType %s idUser %s nameUser %s ' % (datetime.datetime.now().strftime('%H:%M:%S')
            #                                                                  , dictParams['parent'], dictParams['parentType']
            #                                                                  , dictParams['idUser'] , dictParams['nameUser']))
            # app.logger.critical('getIsTokenForMobile:%s %s'%(token,redis.getIsTokenForMobile(token)))

            user.createFromDb(dictParams['idUser'],False)
            _setAzsInfo(res,user)
            res = Service.getSelectData(res)
    return jsonify(res)


@apiAuth.route('getTest/',methods=['GET'])
@support_jsonp
def getTest():
    token = redis.getTokenNew()
    redis.setUserId(token,1)
    res = redis.getUserId(token)
    res = Service.getSelectData(res)
    return jsonify(res)
@apiAuth.route('getAccessToSysobject/',methods=['GET'])
@support_jsonp
@decorators.checkArgsAndGetSelect(['token','sysobjectname'])
def getAccessToSysobject():
    return redis.getAccessUserParam(request.args.get('token'),request.args.get('sysobjectname'))

@apiAuth.route('getLimitSysobject/',methods=['GET'])
@support_jsonp
@decorators.checkArgsAndGetSelect(['token','sysobjectname'])
def getLimitSysobject():
    access = redis.getAccessUserParam(request.args.get('token'),request.args.get('sysobjectname'))
    if (access==1):
        return redis.getLimitUserParam(request.args.get('token'),request.args.get('sysobjectname'))
    return Service.getEmptyData()


def getAzsRole(idUser):
    data = AzsUsersDb.SelectUser(idUser)
    if data:
        role = data[0]['RoleId']
        if role == 1:
            return True, 0,data[0]['AzsId'],data[0]['stname']
        elif role==4:
            return True, 3, data[0]['AzsId'], data[0]['stname']
        elif role==5:
            return True, 4, 0, ''
        elif role == 6:
            return True, 6, data[0]['AzsId'], data[0]['stname']
        else:
            return True, 1,0,''
    else:
        return False,-1,0,''

@app.route('/api/log_out/<token>/',methods=['DELETE'])
@support_jsonp
@decorators.checkArgsAndDeleteToken([])
def delete(token):
    app.logger.critical('%s auth.log_out %s' % (datetime.datetime.now().strftime('%H:%M:%S'),token))
    redis.deleteTokenKeys(token)
    return {'Ok': 1}


