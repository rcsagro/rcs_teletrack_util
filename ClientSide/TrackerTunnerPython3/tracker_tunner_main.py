#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import pickle
import datetime
import time
import os
import json
import sys
import sshtunel
import traceback
import pymongo
import bson
from werkzeug.security import check_password_hash
from cryptography.fernet import Fernet
from gprs_tab import GprsTab
import coder_command_A1 as comA1
from service_tab import ServPhone, ServPhoneP
from text_cmd import TextCmd, TextRqst
from phones_tab import PhonesParam, PhonesValue
from server_tab import TelematServer, ServiceSettings
from events_tab import EventsTabTta, EventsTabTtl, EventsTabTtu, EventsTabTtlU
from transport_tab import GprsSetts, GprsSettsP, ServerSettings, OperatorSetting
from labels_tab import LabelsTab

from sensors_tab import SensorsTab, MemoAin1, MemoAin2, MemoCan1, MemoCan2, MemoCan3, memoCount1, \
    memoCount2, memoCount3, MemoDin, MemoRs485_1, MemoRs485_2, MemoRs485_3, MemoRs485_4, MemoRs485_5, \
    MemoRs485_6, MemoRs485_7, Fuel1, Fuel2, Fuel3, Fuel4, Fuel5, Rfid1, Rfid2

from terminal_tab import TerminalTab
from viewer_tab import ViewerTab
from programm_data import ProgrammData

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import QIcon, QFont, QPixmap
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QStackedWidget, QListWidget, QSpinBox, QFileDialog, QMainWindow, \
    QComboBox, QCheckBox, QTableView, QTableWidget, QTableWidgetItem, QHBoxLayout, QPushButton, QLineEdit, \
    QLabel, QTabWidget, QWidget, QAction, QDesktopWidget, QMessageBox, QVBoxLayout, \
    QCompleter, QSplitter, QPlainTextEdit, QListWidgetItem, QScrollArea

class ViewerRequest:
    def __init__(self):
        pass

    def Start(self):
        pass

    def Stop(self):
        pass

class Dilers:
    def __init__(self):
        self.dilersId = []
        self.dilersName = []

    def getDilerId(self, index):
        return self.dilersId[index]

    def getDilerName(self, index):
        return self.dilersName[index]

    def getDilerLen(self):
        return len(self.dilersId)

    def getIdFromName(self, nameCli):
        try:
            index = self.dilersName.index(nameCli)
            return self.dilersId[index]
        except Exception:
            return ''


class Clients:
    def __init__(self):
        self.clientsId = []
        self.clientsName = []
        self.clientsDilerId = []
        return

    def getClientId(self, index):
        return self.clientsId[index]

    def getClientName(self, index):
        return self.clientsName[index]

    def getIdFromName(self, nameCli):
        try:
            index = self.clientsName.index(nameCli)
            return self.clientsId[index]
        except Exception:
            return ''

    def getClientLen(self):
        return len(self.clientsId)

    def getClientDiler(self, index):
        return self.clientsDilerId[index]


class GprsBaseConfig:
    def __init__(self):
        self.name = ''
        self.regim = 0
        self.AccessPointName = ''
        self.ApnLogin = ''
        self.ApnPass = ''
        self.dnsServ = ''
        self.ispPhone = ''
        self.ispLogin = ''
        self.ipsPassword = ''
        return


class GprsOperSett:
    def __init__(self):
        self.name = ''
        self.GprsLogin = ''
        self.GprsPass = ''
        self.dnsServer = ''
        return

class SensorsParamTtu:
    def __init__(self):
        self.items = []
        self.textBoxSensCode = 1
        self.comboBoxSpeedRs485 = 19600
        self.numericUpDownAccSensetivity = 8
        self.canSpeed1 = 0
        self.canSpeed2 = 0
        self.ainUpDownLvlUp1 = 0
        self.ainUpDownLvlDown1 = 0
        self.ainUpDownLvlUp2 = 0
        self.ainUpDownLvlDown2 = 0
        self.workCan1 = ''
        self.addresCan1 = ''
        self.maskCan1 = ''
        self.startbitCan1 = 0
        self.lengthCan1 = 0
        self.timeoutCan1 = 0
        self.workCan2 = ''
        self.addresCan2 = ''
        self.maskCan2 = ''
        self.startbitCan2 = 0
        self.lengthCan2 = 0
        self.timeoutCan2 = 0
        self.workCan3 = ''
        self.addresCan3 = ''
        self.maskCan3 = ''
        self.startbitCan3 = 0
        self.lengthCan3 = 0
        self.timeoutCan3 = 0
        self.countRegimSelectedIndex1 = 0
        self.countUpDownEvent1Value1 = 0
        self.countUpDownEvent2Value1 = 0
        self.countSourseDin1 = ''
        self.countSourceValue1 = 0
        self.countRegimSelectedIndex2 = ''
        self.countUpDownEvent1Value2 = 0
        self.countUpDownEvent2Value2 = 0
        self.countSourseDin2 = ''
        self.countSourceValue2 = 0
        self.countRegimSelectedIndex3 = ''
        self.countUpDownEvent1Value3 = 0
        self.countUpDownEvent2Value3 = 0
        self.countSourseDin3 = ''
        self.countSourceValue3 = 0
        self.Din1SelectedIndex = 0
        self.Din2SelectedIndex = 0
        self.Din3SelectedIndex = 0
        self.Din4SelectedIndex = 0
        self.checkSaveDin1 = False
        self.checkSaveDin2 = False
        self.checkSaveDin3 = False
        self.checkSaveDin4 = False
        self.DinUsedIn = ''
        self.DinLvlUp = 0
        self.DinLvlLow = 0
        self.DinCmdIn = ''
        self.DinCmdOut = ''
        self.rs485TypeSens1 = ''
        self.rs485ReqData1 = False
        self.rs485Adress1 = 0
        self.rs485StartBit1 = 0
        self.rs485Length1 = 0
        self.rs485Average1 = 0
        self.rs485Event1 = 0
        self.rs485TypeSens2 = ''
        self.rs485ReqData2 = False
        self.rs485Adress2 = 0
        self.rs485StartBit2 = 0
        self.rs485Length2 = 0
        self.rs485Average2 = 0
        self.rs485Event2 = 0
        self.rs485TypeSens3 = ''
        self.rs485ReqData3 = False
        self.rs485Adress3 = 0
        self.rs485StartBit3 = 0
        self.rs485Length3 = 0
        self.rs485Average3 = 0
        self.rs485Event3 = 0
        self.rs485TypeSens4 = ''
        self.rs485ReqData4 = False
        self.rs485Adress4 = 0
        self.rs485StartBit4 = 0
        self.rs485Length4 = 0
        self.rs485Average4 = 0
        self.rs485Event4 = 0
        self.rs485TypeSens5 = ''
        self.rs485ReqData5 = False
        self.rs485Adress5 = 0
        self.rs485StartBit5 = 0
        self.rs485Length5 = 0
        self.rs485Average5 = 0
        self.rs485Event5 = 0
        self.rs485TypeSens6 = ''
        self.rs485ReqData6 = False
        self.rs485Adress6 = 0
        self.rs485StartBit6 = 0
        self.rs485Length6 = 0
        self.rs485Average6 = 0
        self.rs485Event6 = 0
        self.rs485TypeSens7 = ''
        self.rs485ReqData7 = False
        self.rs485Adress7 = 0
        self.rs485StartBit7 = 0
        self.rs485Length7 = 0
        self.rs485Average7 = 0
        self.rs485Event7 = 0
        self.checkedListBoxSensors = []
        return

class SensorsParamTtl:
    def __init__(self):
        self.textBoxSensCode = '1'
        self.comboBoxSpeedRs485 = 19200
        self.numericUpDownAccSensetivity = 8
        self.checkedListBoxSensors = []
        self.ainUpDownLvlUp1 = 0
        self.ainUpDownLvlDown1 = 0
        self.ainUpDownLvlUp2 = 0
        self.ainUpDownLvlDown2 = 0
        self.countRegimSelectedIndex1 = 0
        self.countUpDownEvent1Value1 = 0
        self.countUpDownEvent2Value1 = 0
        self.countRegimSelectedIndex2 = 0
        self.countUpDownEvent1Value2 = 0
        self.countUpDownEvent2Value2 = 0
        self.countRegimSelectedIndex3 = 0
        self.countUpDownEvent1Value3 = 0
        self.countUpDownEvent2Value3 = 0
        self.Din1SelectedIndex = 0
        self.Din2SelectedIndex = 0
        self.checkSaveDin1 = 0
        self.checkSaveDin2 = 0
        self.fuelUpDownAddrValue1 = 0
        self.fuelUpDownStartBitValue1 = 0
        self.fuelUpDownLengthBitValue1 = 0
        self.fuelUpDownAverageValue1 = 0
        self.fuelUpDownAddrValue2 = 0
        self.fuelUpDownStartBitValue2 = 0
        self.fuelUpDownLengthBitValue2 = 0
        self.fuelUpDownAverageValue2 = 0
        self.fuelUpDownAddrValue3 = 0
        self.fuelUpDownStartBitValue3 = 0
        self.fuelUpDownLengthBitValue3 = 0
        self.fuelUpDownAverageValue3 = 0
        self.fuelUpDownAddrValue4 = 0
        self.fuelUpDownStartBitValue4 = 0
        self.fuelUpDownLengthBitValue4 = 0
        self.fuelUpDownAverageValue4 = 0
        self.fuelUpDownAddrValue5 = 0
        self.fuelUpDownStartBitValue5 = 0
        self.fuelUpDownLengthBitValue5 = 0
        self.fuelUpDownAverageValue5 = 0
        self.rfidUpDownAddrValue1 = 0
        self.rfidUpDownLengthBitValue1 = 0
        self.rfidUpDownStartBitValue1 = 0
        self.rfidUpDownAddrValue2 = 0
        self.rfidUpDownLengthBitValue2 = 0
        self.rfidUpDownStartBitValue2 = 0
        return

class MyQCheckBox(QCheckBox):
    def __init__(self, *args, **kwargs):
        QCheckBox.__init__(self, *args, **kwargs)
        self.is_modifiable = True
        self.clicked.connect( self.value_change_slot )

    def value_change_slot(self):
        if self.isChecked():
            self.setChecked(self.is_modifiable)
        else:
            self.setChecked(not self.is_modifiable)

    def setModifiable(self, flag):
        self.is_modifiable = flag

    def isModifiable(self):
        return self.is_modifiable

class TObjectSensors:
    def __init__(self, this):
        self.checkedListBoxSensors = []
        self.lineSnsCode = this.lineSnsCode
        self.comboRs485 = this.comboRs485
        self.spinAcc = this.spinAcc
        self.spinLimitUp1 = this.spinLimitUp1
        self.spinLimitDown1 = this.spinLimitDown1
        self.spinLimitUp2 = this.spinLimitUp2
        self.spinLimitDown2 = this.spinLimitDown2
        self.comboCnt1 = this.comboCnt1
        self.spinEvent11 = this.spinEvent11
        self.spinEvent21 = this.spinEvent21
        self.comboCnt2 = this.comboCnt2
        self.spinEvent12 = this.spinEvent12
        self.spinEvent22 = this.spinEvent22
        self.comboCnt3 = this.comboCnt3
        self.spinEvent13 = this.spinEvent13
        self.spinEvent23 = this.spinEvent23
        self.comboBounceDin1 = this.comboBounceDin1
        self.comboBounceDin2 = this.comboBounceDin2
        self.comboBounceDin3 = this.comboBounceDin3
        self.comboBounceDin4 = this.comboBounceDin4
        self.checkDin1 = this.checkDin1
        self.checkDin2 = this.checkDin2
        self.checkDin3 = this.checkDin3
        self.checkDin4 = this.checkDin4
        self.classFuel1 = this.classFuel1
        self.classFuel2 = this.classFuel2
        self.classFuel3 = this.classFuel3
        self.classFuel4 = this.classFuel4
        self.classFuel5 = this.classFuel5
        self.classRfid1 = this.classRfid1
        self.classRfid2 = this.classRfid2
        self.canSpeed1 = this.comboCan1
        self.canSpeed2 = this.comboCan2
        self.comboSourseDin1 = this.comboSource1
        self.spinSourceValue1 = this.spinSource1
        self.comboSourseDin2 = this.comboSource2
        self.spinSourceValue2 = this.spinSource2
        self.comboSourseDin3 = this.comboSource3
        self.spinSourceValue3 = this.spinSource3
        self.combors485TypeSens1 = this.comboTpSens1
        self.checkrs485ReqData1 = this.checkRqstData1
        self.spinrs485Adress1 = this.spinAddrs1
        self.spinrs485StartBit1 = this.spinStartBit1
        self.spinrs485Length1 = this.spinLen1
        self.spinrs485Average1 = this.spinAver1
        self.spinrs485Event1 = this.spinEvnt1
        self.combors485TypeSens2 = this.comboTpSens2
        self.checkrs485ReqData2 = this.checkRqstData2
        self.spinrs485Adress2 = this.spinAddrs2
        self.spinrs485StartBit2 = this.spinStartBit2
        self.spinrs485Length2 = this.spinLen2
        self.spinrs485Average2 = this.spinAver2
        self.spinrs485Event2 = this.spinEvnt2
        self.combors485TypeSens3 = this.comboTpSens3
        self.checkrs485ReqData3 = this.checkRqstData3
        self.spinrs485Adress3 = this.spinAddrs3
        self.spinrs485StartBit3 = this.spinStartBit3
        self.spinrs485Length3 = this.spinLen3
        self.spinrs485Average3 = this.spinAver3
        self.spinrs485Event3 = this.spinEvnt3
        self.combors485TypeSens4 = this.comboTpSens4
        self.checkrs485ReqData4 = this.checkRqstData4
        self.spinrs485Adress4 = this.spinAddrs4
        self.spinrs485StartBit4 = this.spinStartBit4
        self.spinrs485Length4 = this.spinLen4
        self.spinrs485Average4 = this.spinAver4
        self.spinrs485Event4 = this.spinEvnt4
        self.combors485TypeSens5 = this.comboTpSens5
        self.checkrs485ReqData5 = this.checkRqstData5
        self.spinrs485Adress5 = this.spinAddrs5
        self.spinrs485StartBit5 = this.spinStartBit5
        self.spinrs485Length5 = this.spinLen5
        self.spinrs485Average5 = this.spinAver5
        self.spinrs485Event5 = this.spinEvnt5
        self.combors485TypeSens6 = this.comboTpSens6
        self.checkrs485ReqData6 = this.checkRqstData6
        self.spinrs485Adress6 = this.spinAddrs6
        self.spinrs485StartBit6 = this.spinStartBit6
        self.spinrs485Length6 = this.spinLen6
        self.spinrs485Average6 = this.spinAver6
        self.spinrs485Event6 = this.spinEvnt6
        self.combors485TypeSens7 = this.comboTpSens7
        self.checkrs485ReqData7 = this.checkRqstData7
        self.spinrs485Adress7 = this.spinAddrs7
        self.spinrs485StartBit7 = this.spinStartBit7
        self.spinrs485Length7 = this.spinLen7
        self.spinrs485Average7 = this.spinAver7
        self.spinrs485Event7 = this.spinEvnt7
        self.comboworkCan1 = this.comboWorkCan1
        self.lineaddresCan1 = this.lineCanAddrs1
        self.linemaskCan1 = this.lineMask1
        self.spinstartbitCan1 = this.spinStBit1
        self.spinlengthCan1 = this.spinLngth1
        self.spintimeoutCan1 = this.spinTmout1
        self.comboworkCan2 = this.comboWorkCan2
        self.lineaddresCan2 = this.lineCanAddrs2
        self.linemaskCan2 = this.lineMask2
        self.spinstartbitCan2 = this.spinStBit2
        self.spinlengthCan2 = this.spinLngth2
        self.spintimeoutCan2 = this.spinTmout2
        self.comboworkCan3 = this.comboWorkCan3
        self.lineaddresCan3 = this.lineCanAddrs3
        self.linemaskCan3 = this.lineMask3
        self.spinstartbitCan3 = this.spinStBit3
        self.spinlengthCan3 = this.spinLngth3
        self.spintimeoutCan3 = this.spinTmout3
        self.DinUsedIn = this.comboUsedIn
        self.DinLvlUp = this.spinLvlUp
        self.DinLvlLow = this.spinLvlLow
        self.DinCmdIn = this.lineCmdIn
        self.DinCmdOut = this.lineCmdOut
        return

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        if not os.path.exists('src'):
            os.mkdir("src")
        self.OK_MSG = 1024
        self.nameProgram = "Teletrack's tracker Tunning"
        self.setMinimumSize(QSize(1000, 700))
        self.showMaximized()
        self.setWindowTitle(self.tr(self.nameProgram))
        self.setWindowIcon(QIcon('src/photo_2020-12-03_16-48-06.jpg'))
        self.statusBar().showMessage('Ready')
        self.mongoAction = QAction(self.tr('&Login'), self)
        self.mongoAction.setShortcut('Ctrl+L')
        self.mongoAction.setStatusTip('Login and get trackers data')
        self.mongoAction.setIcon(QIcon("src/drawer.png"))
        self.mongoAction.triggered.connect(self.OnGetTrackersData)
        self.logoutAction = QAction(self.tr('&Logout'), self)
        self.logoutAction.setShortcut('Ctrl+E')
        self.logoutAction.setStatusTip('Logout from current session')
        self.logoutAction.setIcon(QIcon("src/Exit.png"))
        self.logoutAction.triggered.connect(self.logout)
        self.logoutAction.setEnabled(False)
        exitAction = QAction(self.tr('&Close'), self)
        exitAction.setShortcut('Ctrl+X')
        exitAction.setStatusTip('Close this application')
        exitAction.setIcon(QIcon("src/cancel.png"))
        exitAction.triggered.connect(self.closed)
        prefAction = QAction(self.tr('&Parameters'), self)
        prefAction.setShortcut('Ctrl+P')
        prefAction.setStatusTip('Parameters this application')
        prefAction.setIcon(QIcon("src/hammer_screwdriver.png"))
        prefAction.triggered.connect(self.showParameters)
        aboutAction = QAction(self.tr('&About'), self)
        aboutAction.setShortcut('Ctrl+A')
        aboutAction.setStatusTip(self.tr('About this application'))
        aboutAction.setIcon(QIcon("src/Info.png"))
        aboutAction.triggered.connect(self.showAbout)
        self.readAllAction = QAction(self.tr('&ReadAll'), self)
        self.readAllAction.setShortcut('Ctrl+R')
        self.readAllAction.setStatusTip('Read all data from current TTL/TTU tracker')
        self.readAllAction.setIcon(QIcon("src/kisspng-john-deereA.png"))
        self.readAllAction.triggered.connect(self.ReadAll)
        self.readAllAction.setEnabled(False)
        self.writeAllAction = QAction(self.tr('&WriteAll'), self)
        self.writeAllAction.setShortcut('Ctrl+W')
        self.writeAllAction.setStatusTip('Write all data into current TTL/TTU tracker')
        self.writeAllAction.setIcon(QIcon("src/kisspng-tractorA.png"))
        self.writeAllAction.triggered.connect(self.WriteAll)
        self.writeAllAction.setEnabled(False)
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(self.mongoAction)
        fileMenu.addAction(self.logoutAction)
        fileMenu.addAction(exitAction)
        fileMenu = menubar.addMenu('&Edit')
        fileMenu.addAction(prefAction)
        fileMenu = menubar.addMenu('&Tools')
        fileMenu.addAction(self.readAllAction)
        fileMenu.addAction(self.writeAllAction)
        fileMenu = menubar.addMenu('&Help')
        fileMenu.addAction(aboutAction)

        self.arrayFirst = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'q', 'a', 'z', 'w', 's', 'x', 'e', 'd',
                           'c',
                           'r', 'f', 'v', 't', 'g', 'b', 'y', 'h', 'n', 'u', 'j', 'i', 'k', 'o']
        self.arraySecond = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'w', 's', 'x', 'e', 'd', 'c', 'r', 'f',
                            'v',
                            't', 'g', 'b', 'y', 'h', 'n', 'u', 'j', 'm', 'i', 'k', 'o', 'l', 'p']
        self.isAuthorization = 'ON' # 'ON'/'OFF'
        self.searchRow = ''
        self.searchColumn = ''
        self.programmData = ''
        self.eventsTta = ''
        self.eventsTtl = ''
        self.eventsTtlu = ''
        self.eventsTtu = ''
        self.previouseTypeTracker = ''
        self.classRegim = ''
        self.teleServer = ''
        self.serviceSett = ''
        self.classPhoneParam = ''
        self.classPhones = ''
        self.classGprs = ''
        self.classGprsP = ''
        self.classTextCmd = ''
        self.classTextRqst = ''
        self.classServSett = ''
        self.classOperSett = ''
        self.srvPhone = ''
        self.srvPhoneP = ''
        self.srvSensViewer = ''
        self.classAin1 = ''
        self.classAin2 = ''
        self.classCan1 = ''
        self.classCan2 = ''
        self.classCan3 = ''
        self.classCount1 = ''
        self.classCount2 = ''
        self.classCount3 = ''
        self.classDin = ''
        self.classRs4851 = ''
        self.classRs4852 = ''
        self.classRs4853 = ''
        self.classRs4854 = ''
        self.classRs4855 = ''
        self.classRs4856 = ''
        self.classRs4857 = ''
        self.classFuel1 = ''
        self.classFuel2 = ''
        self.classFuel3 = ''
        self.classFuel4 = ''
        self.classFuel5 = ''
        self.classRfid1 = ''
        self.classRfid2 = ''
        self.sensors_Tab = ''
        self.parentId = ''
        self.pcbversionId = []
        self.protocolId = []
        self.protocolName = []
        self.dilersId = []
        self.dilersName = []
        self.selectRowTracker = ''
        self.searchRow = 0
        self.searchColumn = 0
        self.commandA1 = comA1.GenerateCommandA1('', '', '')
        self.HEIGHT_WIDGET = 100
        self.prefixCmd = "$RCSTT,"
        self.pinCode = "1234"
        self.simUse = 0
        self.gsmGprsSetting = []
        self.gprsSett = []
        self.currentSensor = ''
        self.classLabels = ''
        self.lastLabel = '0'
        self.currentIndex = 0
        self.currentIDChain = 0
        self.cipher_key = b'Mwyc-QMV_atVkj3TmnPW-cH0gIGoloOy8keGBe65ASQ='
        self.cipher = Fernet(self.cipher_key)

        self.btnClearTableView = QPushButton("Clear this table")
        self.btnClearTableView.setToolTip("Delete all Labels from current table")
        self.btnClearTableView.clicked.connect(self.OnClearViewLabel)
        self.btnClearTableView.setEnabled(False)
        self.btnClearTableNew = QPushButton("Clear this table")
        self.btnClearTableNew.setToolTip("Delete all Labels from current table")
        self.btnClearTableNew.clicked.connect(self.OnClearNewLabel)
        self.btnClearTableNew.setEnabled(False)
        self.btnViewLabel = QPushButton("View Labels")
        self.btnViewLabel.setToolTip("View Labels from current tracker")
        self.btnViewLabel.clicked.connect(self.OnViewLabel)
        self.btnViewLabel.setEnabled(True)
        self.tableLabel = QTableWidget(self)
        self.tableLabel.setColumnCount(2)
        self.tableLabel.setHorizontalHeaderLabels(["Name Label", "Value Label"])
        self.tableLabel.horizontalHeaderItem(0).setToolTip("Name of Label")
        self.tableLabel.horizontalHeaderItem(1).setToolTip("Value of Label")
        self.tableLabel.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableLabel.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableLabel.resizeColumnsToContents()
        self.tableLabel.resizeRowsToContents()
        self.tableLabel.setSortingEnabled(True)
        self.tableLabel.setSelectionBehavior(QTableView.SelectRows)
        self.tableLabel.clicked.connect(self.viewClickedLabel)
        self.labelLastDateResponse = QLabel("Date of last request:")
        self.labelLastDateResponse.setFont(QFont("Times", weight=QFont.Bold))
        self.labelLastDateResponse.setStyleSheet("QLabel { color : green; }")
        self.labelViewLabel = QLabel("Таблица просмотра прочитанных меток")
        self.labelDateLabel = QLabel("Date of Labels:")
        self.labelDateLabel.setFont(QFont("Times", weight=QFont.Bold))
        self.labelDateLabel.setStyleSheet("QLabel { color : blue; }")
        self.labelBeginIndex = QLabel("Beginer index:")
        self.labelBeginIndex.setFont(QFont("Times", weight=QFont.Bold))
        self.labelBeginIndex.setStyleSheet("QLabel { color : blue; }")
        self.labelEndMarker = QLabel("End marker of Labels:")
        self.labelEndMarker.setFont(QFont("Times", weight=QFont.Bold))
        self.labelEndMarker.setStyleSheet("QLabel { color : blue; }")
        self.labelMaskLabel = QLabel("Mask of Labels:")
        self.labelMaskLabel.setFont(QFont("Times", weight=QFont.Bold))
        self.labelMaskLabel.setStyleSheet("QLabel { color : blue; }")
        self.btnGetLabel = QPushButton("Get more Labels")
        self.btnGetLabel.setToolTip("Get more Labels ")
        self.btnGetLabel.clicked.connect(self.OnGetMoreLabels)
        self.btnGetLabel.setEnabled(False)
        hlayVLeftLabel = QVBoxLayout(self)
        hlayVLeftLabel.addWidget(self.btnViewLabel)
        hlayVLeftLabel.addWidget(self.labelLastDateResponse)
        hlayVLeftLabel.addWidget(self.labelDateLabel)
        hlayVLeftLabel.addWidget(self.labelBeginIndex)
        hlayVLeftLabel.addWidget(self.labelMaskLabel)
        hlayVLeftLabel.addWidget(self.labelViewLabel)
        hlayVLeftLabel.addWidget(self.tableLabel)
        hlayVLeftLabel.addWidget(self.labelEndMarker)
        hlayHLay = QHBoxLayout(self)
        hlayHLay.addWidget(self.btnGetLabel)
        hlayHLay.addWidget(self.btnClearTableView)
        widgetBtnGroup = QWidget(self)
        widgetBtnGroup.setLayout(hlayHLay)
        hlayVLeftLabel.addWidget(widgetBtnGroup)
        widgetLabelLeft = QWidget(self)
        widgetLabelLeft.setLayout(hlayVLeftLabel)
        self.labelNameNewTable = QLabel("Таблица добавленных текущих меток")
        self.tableNewLabel = QTableWidget(self)
        self.tableNewLabel.setColumnCount(3)
        self.tableNewLabel.setHorizontalHeaderLabels(["Name Label", "Number Cell", "Number Label"])
        self.tableNewLabel.horizontalHeaderItem(0).setToolTip("Name of Label")
        self.tableNewLabel.horizontalHeaderItem(1).setToolTip("Number of Cell")
        self.tableNewLabel.horizontalHeaderItem(2).setToolTip("Number of Label")
        self.tableNewLabel.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableNewLabel.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableNewLabel.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableNewLabel.resizeColumnsToContents()
        self.tableNewLabel.resizeRowsToContents()
        self.tableNewLabel.setSortingEnabled(True)
        self.tableNewLabel.setSelectionBehavior(QTableView.SelectRows)
        self.tableNewLabel.clicked.connect(self.viewClickedNewLabel)
        self.labelNumberCell = QLabel("Choise number cell:")
        self.comboAddCell = QComboBox()
        self.comboAddCell.setEditable(True)
        self.comboAddCell.setEnabled(False)
        self.comboAddCell.setToolTip("Choise new number of cell for new Label")
        self.comboAddCell.adjustSize()
        self.comboAddCell.currentIndexChanged.connect(self.OnChoiseNumberCell)
        self.labelNumberLabel = QLabel("Choise number Label:")
        self.comboAddLabel = QComboBox()
        self.comboAddLabel.setEditable(True)
        self.comboAddLabel.setToolTip("Choise new number of Label")
        self.comboAddLabel.adjustSize()
        self.comboAddLabel.currentIndexChanged.connect(self.OnChoiseNumberLabel)
        self.comboAddLabel.setEnabled(False)
        self.comboMarkerEnd = QComboBox()
        self.comboMarkerEnd.setEditable(False)
        self.comboMarkerEnd.setToolTip("Choise end marker")
        self.comboMarkerEnd.adjustSize()
        self.comboMarkerEnd.addItems(['0', '65535'])
        self.comboMarkerEnd.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.comboMarkerEnd.currentIndexChanged.connect(self.OnChoiseMarkerEnd)
        self.comboMarkerEnd.setEnabled(False)
        self.labelMarkerEnd = QLabel("Choise end marker:")
        self.btnAddLabel = QPushButton("Add new Label")
        self.btnAddLabel.setToolTip("Add new Label into current tracker")
        self.btnAddLabel.clicked.connect(self.OnAddLabel)
        self.btnAddLabel.setEnabled(False)
        hlayHLabel1 = QHBoxLayout(self)
        hlayHLabel1.addWidget(self.labelNumberCell)
        hlayHLabel1.addWidget(self.comboAddCell)
        widgetRight1 = QWidget(self)
        widgetRight1.setLayout(hlayHLabel1)
        hlayHLabel2 = QHBoxLayout(self)
        hlayHLabel2.addWidget(self.labelNumberLabel)
        hlayHLabel2.addWidget(self.comboAddLabel)
        widgetRight2 = QWidget(self)
        widgetRight2.setLayout(hlayHLabel2)
        hlayHLabel3 = QHBoxLayout(self)
        hlayHLabel3.addWidget(self.labelMarkerEnd)
        hlayHLabel3.addWidget(self.comboMarkerEnd)
        widgetRight3 = QWidget(self)
        widgetRight3.setLayout(hlayHLabel3)
        hlayVRight = QVBoxLayout(self)
        hlayVRight.addWidget(self.labelNameNewTable)
        hlayVRight.addWidget(self.tableNewLabel)
        hlayVRight.addWidget(widgetRight1)
        hlayVRight.addWidget(widgetRight2)
        hlayVRight.addWidget(widgetRight3)
        hlayHRght = QHBoxLayout(self)
        hlayHRght.addWidget(self.btnAddLabel)
        hlayHRght.addWidget(self.btnClearTableNew)
        widgetBtnGroupR = QWidget(self)
        widgetBtnGroupR.setLayout(hlayHRght)
        hlayVRight.addWidget(widgetBtnGroupR)
        widgetLabelRight = QWidget(self)
        widgetLabelRight.setLayout(hlayVRight)
        hlayHLabels = QHBoxLayout(self)
        hlayHLabels.addWidget(widgetLabelLeft)
        hlayHLabels.addWidget(widgetLabelRight)

        self.timerViewerRequest = ViewerRequest()
        self.scrollDin = QScrollArea()
        # Scroll Area Properties
        self.scrollDin.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollDin.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollDin.setWidgetResizable(True)
        self.btnSetDef = QPushButton("Default Parameter")
        self.btnSetDef.setToolTip("Set default parameter from file")
        self.btnSetDef.clicked.connect(self.OnSetDefault)
        self.btnSetDef.setEnabled(False)
        self.btnDeleteCom = QPushButton("Delete Command")
        self.btnDeleteCom.setToolTip("Delete selected command from DB")
        self.btnDeleteCom.clicked.connect(self.OnDeleteCommand)
        self.btnDeleteCom.setEnabled(False)
        self.btnDeleteAllCom = QPushButton("Clear Command")
        self.btnDeleteAllCom.setToolTip("Delete all command for selected tracker")
        self.btnDeleteAllCom.clicked.connect(self.OnDeleteAllCommand)
        self.btnDeleteAllCom.setEnabled(False)
        self.btnSavingInfo = QPushButton("Save to DB")
        self.btnSavingInfo.setToolTip("Save parameters into DB for tracker")
        self.btnSavingInfo.clicked.connect(self.OnSaveParametersToDb)
        self.btnSavingInfo.setEnabled(False)
        self.btnGetInfo = QPushButton("Refresh Trackers")
        self.btnGetInfo.setToolTip("Refresh table trackers")
        self.btnGetInfo.clicked.connect(self.OnGetInfoTrackers)
        self.btnGetInfo.setEnabled(False)
        self.btnSaveState = QPushButton("Save Parameters")
        self.btnSaveState.setToolTip("Save parameters into file for default")
        self.btnSaveState.clicked.connect(self.OnSaveStateParam)
        self.btnSaveState.setEnabled(False)
        self.labelDiler = QLabel("Diler:")
        self.comboDiler = QComboBox()
        self.comboDiler.setEditable(False)
        if self.isAuthorization == 'ON':
            self.comboDiler.setEnabled(False)
        self.comboDiler.setToolTip("Choise diler")
        self.comboDiler.adjustSize()
        self.comboDiler.currentIndexChanged.connect(self.OnChangeNameDiler)
        self.labelClient = QLabel("Client:")
        self.comboClient = QComboBox()
        self.comboClient.setEditable(False)
        if self.isAuthorization == 'ON':
            self.comboClient.setEnabled(False)
        self.comboClient.setToolTip("Choise client")
        self.comboClient.adjustSize()
        self.comboClient.currentIndexChanged.connect(self.OnChangeNameClient)
        self.labelType = QLabel("Type tracker:")
        self.comboType = QComboBox()
        self.comboType.setToolTip("Choise type tracker")
        self.comboType.adjustSize()
        self.comboType.currentIndexChanged.connect(self.OnChangeTypeTracker)
        self.labelModel = QLabel("Model trackers:")
        self.comboTypeSens = QComboBox()
        self.comboTypeSens.setToolTip("Choise model trackers for sensors")
        self.comboTypeSens.addItems(["TTL", "TTU"])
        self.comboTypeSens.setCurrentIndex(0)
        self.comboTypeSens.setEnabled(False)
        self.comboTypeSens.adjustSize()
        self.comboTypeSens.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.comboTypeSens.currentIndexChanged.connect(self.OnChangeTypeTrackSens)
        self.btnReadParam = QPushButton("Read Parameters")
        self.btnReadParam.setToolTip("Reading parameters from tracker")
        self.btnReadParam.clicked.connect(self.OnReadParam)
        self.btnReadParam.setEnabled(False)
        self.btnWriteParam = QPushButton("Write Parameters")
        self.btnWriteParam.setToolTip("Writing parameters into tracker")
        self.btnWriteParam.clicked.connect(self.OnWriteParam)
        self.btnWriteParam.setEnabled(False)
        self.lineParam = QPlainTextEdit(self)
        self.lineParam.setToolTip("Showing loging")
        self.lineParam.setReadOnly(True)
        self.lineParam.setMaximumHeight(self.HEIGHT_WIDGET - 10)
        self.lineParam.setMinimumHeight(self.HEIGHT_WIDGET - 10)
        self.lineParam.appendPlainText("[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + \
                                       "] " + self.nameProgram + " starting and ready for use")
        centralWidget = QWidget(self)
        self.setCentralWidget(centralWidget)
        vlayout = QVBoxLayout(self)
        hlayout1 = QHBoxLayout(self)
        hlayout1.addWidget(self.btnGetInfo)
        hlayout1.addWidget(self.btnSaveState)
        hlayout1.addWidget(self.btnSetDef)
        hlayout1.addWidget(self.btnSavingInfo)
        hlayout1.addWidget(self.labelDiler)
        hlayout1.addWidget(self.comboDiler)
        hlayout1.addWidget(self.labelClient)
        hlayout1.addWidget(self.comboClient)
        hlayout1.addWidget(self.labelType)
        hlayout1.addWidget(self.comboType)
        hWidget1 = QWidget(self)
        hWidget1.setLayout(hlayout1)
        hlayout2 = QHBoxLayout(self)
        hWidget2 = QWidget(self)
        hWidget2.setLayout(hlayout2)
        splitter1 = QSplitter(Qt.Vertical)
        splitter1.setHandleWidth(3)
        self.labelFilt = QLabel("Filter:")
        self.lineFilter = QLineEdit()
        self.lineFilter.setToolTip("Filtering Identifier")
        self.lineFilter.textChanged.connect(self.setFilterTracker)
        self.pinLabel = QLabel(self)
        self.pinLabel.setText("PinCode:")
        self.pinCode = QLineEdit(self)
        self.pinCode.setToolTip("Set Pincode to tracker TTL/TTU")
        self.pinCode.setEnabled(False)
        self.switchBtn = QPushButton('SIM1', self)
        self.switchBtn.setCheckable(True)
        self.switchBtn.clicked[bool].connect(self.setSim1Sim2)
        self.switchBtn.setEnabled(False)
        self.switchBtn.setToolTip("Switcher SIM1/SIM2")
        self.btnReset = QPushButton("Reset")
        self.btnReset.setToolTip("Reset tracker")
        self.btnReset.clicked.connect(self.OnResetTracker)
        self.btnReset.setEnabled(False)
        self.btnSearch = QPushButton("Search")
        self.btnSearch.setToolTip("Search tracker")
        self.btnSearch.clicked.connect(self.OnSearchTracker)
        self.labelSearch = QLabel("Search:")
        self.lineSearch = QLineEdit()
        self.lineSearch.setToolTip("Search in table")
        hlayUpFilt0 = QHBoxLayout(self)
        hlayUpFilt0.addWidget(self.labelFilt)
        hlayUpFilt0.addWidget(self.lineFilter)
        hlayUpFilt0.addWidget(self.labelSearch)
        hlayUpFilt0.addWidget(self.lineSearch)
        hlayUpFilt0.addWidget(self.btnSearch)
        hlayUpFilt1 = QHBoxLayout(self)
        hlayUpFilt1.addWidget(self.pinLabel)
        hlayUpFilt1.addWidget(self.pinCode)
        hlayUpFilt1.addWidget(self.comboTypeSens)
        hlayUpFilt1.addWidget(self.switchBtn)
        hlayUpFilt1.addWidget(self.btnReset)
        hlayUpFilt1.addWidget(self.btnDeleteCom)
        hlayUpFilt1.addWidget(self.btnDeleteAllCom)
        widgetUp0 = QWidget(self)
        widgetUp0.setLayout(hlayUpFilt0)
        widgetUp1 = QWidget(self)
        widgetUp1.setLayout(hlayUpFilt1)
        hlayUp = QVBoxLayout(self)
        hlayUp.addWidget(widgetUp1)
        hlayUp.addWidget(widgetUp0)
        downUp = QHBoxLayout(self)
        up_widget = QWidget(self)
        up_widget.setLayout(hlayUp)
        up_widget.setMinimumHeight(105)
        up_widget.setMaximumHeight(105)
        down_widget = QWidget(self)
        down_widget.setLayout(downUp)
        self.tableTrack = QTableWidget(self)
        self.tableTrack.setColumnCount(5)
        self.tableTrack.setHorizontalHeaderLabels(["Identifier", "Name object", "List commands", "Comments", "Type"])
        self.tableTrack.horizontalHeaderItem(0).setToolTip("Identifier of tracker")
        self.tableTrack.horizontalHeaderItem(1).setToolTip("Name of object")
        self.tableTrack.horizontalHeaderItem(2).setToolTip("List commands of tracker")
        self.tableTrack.horizontalHeaderItem(3).setToolTip("Comments")
        self.tableTrack.horizontalHeaderItem(4).setToolTip("Type")
        self.tableTrack.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTrack.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTrack.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTrack.horizontalHeaderItem(3).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTrack.horizontalHeaderItem(4).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTrack.resizeColumnsToContents()
        self.tableTrack.resizeRowsToContents()
        self.tableTrack.setSortingEnabled(True)
        self.tableTrack.setSelectionBehavior(QTableView.SelectRows)
        self.tableTrack.clicked.connect(self.viewClickedTrack)
        self.tableEvents = QTableWidget(self)
        self.tableEvents.setColumnCount(8)
        self.tableEvents.setHorizontalHeaderLabels(["Parameter Name", "Parameter Value", "Unit Name", "Fixing Point",
                                                    "Sent to Server", "SMS on Phone", "Recomended Values",
                                                    "Valid Values"])
        self.tableEvents.horizontalHeaderItem(0).setToolTip("Parameter name")
        self.tableEvents.horizontalHeaderItem(1).setToolTip("Parameter value")
        self.tableEvents.horizontalHeaderItem(2).setToolTip("Unit name")
        self.tableEvents.horizontalHeaderItem(3).setToolTip("Fixing point")
        self.tableEvents.horizontalHeaderItem(4).setToolTip("Sent to the server")
        self.tableEvents.horizontalHeaderItem(5).setToolTip("SMS on phone")
        self.tableEvents.horizontalHeaderItem(6).setToolTip("The recomended values")
        self.tableEvents.horizontalHeaderItem(7).setToolTip("Valid values")
        self.tableEvents.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableEvents.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableEvents.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableEvents.horizontalHeaderItem(3).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableEvents.horizontalHeaderItem(4).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableEvents.horizontalHeaderItem(5).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableEvents.horizontalHeaderItem(6).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableEvents.horizontalHeaderItem(7).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableEvents.resizeColumnsToContents()
        self.tableEvents.resizeRowsToContents()
        self.tableEvents.setSelectionBehavior(QTableView.SelectRows)
        self.tableRegim = QTableWidget(self)
        self.tableRegim.setColumnCount(2)
        self.tableRegim.setHorizontalHeaderLabels(["Regim Name", "Regim Value"])
        self.tableRegim.horizontalHeaderItem(0).setToolTip("Regim name")
        self.tableRegim.horizontalHeaderItem(1).setToolTip("Regim value")
        self.tableRegim.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableRegim.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableRegim.resizeColumnsToContents()
        self.tableRegim.resizeRowsToContents()
        self.tableRegim.setSelectionBehavior(QTableView.SelectRows)
        self.tableTelematicServer = QTableWidget(self)
        self.tableTelematicServer.setColumnCount(2)
        self.tableTelematicServer.setHorizontalHeaderLabels(["Parameter Name", "Parameter Value"])
        self.tableTelematicServer.horizontalHeaderItem(0).setToolTip("Parameter name")
        self.tableTelematicServer.horizontalHeaderItem(1).setToolTip("Parameter value")
        self.tableTelematicServer.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTelematicServer.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTelematicServer.resizeColumnsToContents()
        self.tableTelematicServer.resizeRowsToContents()
        self.tableTelematicServer.setSelectionBehavior(QTableView.SelectRows)
        self.btnGenAccess = QPushButton("Generate access")
        self.btnGenAccess.setToolTip("Generate login and password")
        self.btnGenAccess.clicked.connect(self.OnGenerateAccess)
        self.btnGenAccess.setEnabled(False)
        self.labelTelematicServer = QLabel("Telematic Server")
        self.tableServiceSettings = QTableWidget(self)
        self.tableServiceSettings.setColumnCount(2)
        self.tableServiceSettings.setHorizontalHeaderLabels(["Parameter Name", "Parameter Value"])
        self.tableServiceSettings.horizontalHeaderItem(0).setToolTip("Parameter name")
        self.tableServiceSettings.horizontalHeaderItem(1).setToolTip("Parameter value")
        self.tableServiceSettings.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableServiceSettings.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableServiceSettings.resizeColumnsToContents()
        self.tableServiceSettings.resizeRowsToContents()
        self.tableServiceSettings.setSelectionBehavior(QTableView.SelectRows)
        self.labelServiceSettings = QLabel("Service settings")
        self.btnSaveSetting = QPushButton("Save Settings")
        self.btnSaveSetting.setToolTip("Save service settings")
        self.btnSaveSetting.clicked.connect(self.OnSaveServiceSetting)
        self.btnSaveSetting.setEnabled(False)
        self.comboRegim = QComboBox()
        self.comboRegim.setEditable(False)
        self.comboRegim.setToolTip("Choise GSM operator")
        self.comboRegim.adjustSize()
        self.btnSetLeft = QPushButton("Set GSM operator")
        self.btnSetLeft.setToolTip("Set GSM parameter")
        self.btnSetLeft.clicked.connect(self.OnSetRegim)
        self.btnReadRegim = QPushButton("Save GSM operator")
        self.btnReadRegim.setToolTip("Save GSM parameter")
        self.btnReadRegim.clicked.connect(self.OnReadRegim)
        self.labelRegim = QLabel("Operator settings")
        self.labelPhonesParam = QLabel("Phones Parameters")
        self.tablePhonesParam = QTableWidget(self)
        self.tablePhonesParam.setColumnCount(2)
        self.tablePhonesParam.setHorizontalHeaderLabels(["Parameter Name", "Parameter Value"])
        self.tablePhonesParam.horizontalHeaderItem(0).setToolTip("Parametr phone name")
        self.tablePhonesParam.horizontalHeaderItem(1).setToolTip("Parametr phone value")
        self.tablePhonesParam.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePhonesParam.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePhonesParam.resizeColumnsToContents()
        self.tablePhonesParam.resizeRowsToContents()
        self.tablePhonesParam.setSelectionBehavior(QTableView.SelectRows)
        self.labelSIMCard = QLabel("SIM Card Phone")
        self.btnSetPhone = QPushButton("Set Phone")
        self.btnSetPhone.setToolTip("Setting phone")
        self.btnSetPhone.clicked.connect(self.OnSetPhone)
        self.btnSetPhone.setEnabled(False)
        self.btnGoPhone = QPushButton("Moving number")
        self.btnGoPhone.setToolTip("Moving setting phone")
        self.btnGoPhone.clicked.connect(self.OnGoPhone)
        self.btnGoPhone.setEnabled(False)
        self.lineSIMCard = QLineEdit()
        self.lineSIMCard.setToolTip("SIM Card Phone")
        self.tablePhones = QTableWidget(self)
        self.tablePhones.setColumnCount(2)
        self.tablePhones.setHorizontalHeaderLabels(["Name Client", "Phone Value"])
        self.tablePhones.horizontalHeaderItem(0).setToolTip("Parameter phone name")
        self.tablePhones.horizontalHeaderItem(1).setToolTip("Parameter phone value")
        self.tablePhones.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePhones.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tablePhones.resizeColumnsToContents()
        self.tablePhones.resizeRowsToContents()
        self.tablePhones.setSelectionBehavior(QTableView.SelectRows)
        self.btnAddPhone = QPushButton("Add Phone")
        self.btnAddPhone.setToolTip("Adding phone number")
        self.btnAddPhone.clicked.connect(self.OnAddPhone)
        self.btnAddPhone.setEnabled(False)
        self.btnDelPhone = QPushButton("Delete Phone")
        self.btnDelPhone.setToolTip("Delete phone number")
        self.btnDelPhone.clicked.connect(self.OnDeletePhone)
        self.btnDelPhone.setEnabled(False)
        self.labelPhone = QLabel("Phones from Data Base")
        self.labelTxtCmd = QLabel("Text Command:")
        self.lineTxtCmd = QPlainTextEdit(self)
        self.lineTxtCmd.setToolTip("Type text command")
        self.labelTxtRequest = QLabel("Text Request:")
        self.lineTxtRequest = QPlainTextEdit(self)
        self.lineTxtRequest.setToolTip("Result text from command")
        self.text_edit = QPlainTextEdit(self)
        self.text_edit.setReadOnly(True)
        self.text_edit.setToolTip("'Request all data from device!'")
        self.font = QFont()
        self.font.setPointSize(16)
        self.text_edit.setFont(self.font)
        self.text_edit.appendPlainText('Request all data from device!')
        self.labelGprs = QLabel("GPRS Settings")
        self.tableGprs = QTableWidget(self)
        self.tableGprs.setColumnCount(2)
        self.tableGprs.setHorizontalHeaderLabels(["Parameter", "Value"])
        self.tableGprs.horizontalHeaderItem(0).setToolTip("Parameter name")
        self.tableGprs.horizontalHeaderItem(1).setToolTip("Parameter value")
        self.tableGprs.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableGprs.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableGprs.resizeColumnsToContents()
        self.tableGprs.resizeRowsToContents()
        self.tableGprs.setSelectionBehavior(QTableView.SelectRows)
        self.labelServSett = QLabel("Server Settings")
        self.tableServSett = QTableWidget(self)
        self.tableServSett.setColumnCount(2)
        self.tableServSett.setHorizontalHeaderLabels(["Parameter", "Value"])
        self.tableServSett.horizontalHeaderItem(0).setToolTip("Parameter name")
        self.tableServSett.horizontalHeaderItem(1).setToolTip("Parameter value")
        self.tableServSett.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableServSett.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableServSett.resizeColumnsToContents()
        self.tableServSett.resizeRowsToContents()
        self.tableServSett.setSelectionBehavior(QTableView.SelectRows)
        self.btnGprsAccess = QPushButton("Generate Access")
        self.btnGprsAccess.setToolTip("Generate for server access")
        self.btnGprsAccess.clicked.connect(self.OnGprsAccess)
        self.labelOperSett = QLabel("")
        self.comboOperSett = QComboBox()
        self.comboOperSett.setEditable(False)
        self.comboOperSett.setToolTip("Choise operator setting")
        self.comboOperSett.adjustSize()
        self.btnOperUp = QPushButton("Set GPRS settings")
        self.btnOperUp.setToolTip("Move settings from list to table")
        self.btnOperUp.clicked.connect(self.OnUpOperSett)
        self.btnOperDown = QPushButton("Save GPRS settings")
        self.btnOperDown.setToolTip("Move settings from table to list")
        self.btnOperDown.clicked.connect(self.OnDownOperSett)
        self.labelServPhone = QLabel("Phones")
        self.tableServPhone = QTableWidget(self)
        self.tableServPhone.setColumnCount(2)
        self.tableServPhone.setHorizontalHeaderLabels(["Phone Name", "Value Phone"])
        self.tableServPhone.horizontalHeaderItem(0).setToolTip("Phone name")
        self.tableServPhone.horizontalHeaderItem(1).setToolTip("Phone value")
        self.tableServPhone.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableServPhone.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableServPhone.resizeColumnsToContents()
        self.tableServPhone.resizeRowsToContents()
        self.tableServPhone.setSelectionBehavior(QTableView.SelectRows)
        self.labelConnectReg = QLabel("Connect Regim:")
        self.lineReg = QLineEdit()
        self.lineReg.setToolTip("Timeout connect (sec)")
        self.lineReg.setEnabled(False)
        self.labelSnsCode = QLabel("Sens_Code:")
        self.lineSnsCode = QLineEdit()
        hlaySns = QHBoxLayout(self)
        hlaySns.addWidget(self.labelModel)
        hlaySns.addWidget(self.labelSnsCode)
        hlaySns.addWidget(self.lineSnsCode)
        widgetSns = QWidget(self)
        widgetSns.setLayout(hlaySns)
        self.labelRs485 = QLabel("Speed RS485:")
        self.comboRs485 = QComboBox()
        self.comboRs485.setToolTip("Choise speed of Rs485")
        self.comboRs485.adjustSize()
        hlayRs = QHBoxLayout(self)
        hlayRs.addWidget(self.labelRs485)
        hlayRs.addWidget(self.comboRs485)
        widgetRs = QWidget(self)
        widgetRs.setLayout(hlayRs)
        self.labelCan1 = QLabel("Speed CAN1:")
        self.comboCan1 = QComboBox()
        self.comboCan1.setEditable(False)
        self.comboCan1.setToolTip("Choise speed of CAN1 speed")
        self.comboCan1.adjustSize()
        hlaycan1 = QHBoxLayout(self)
        hlaycan1.addWidget(self.labelCan1)
        hlaycan1.addWidget(self.comboCan1)
        widgetcan1 = QWidget(self)
        widgetcan1.setLayout(hlaycan1)
        self.labelCan2 = QLabel("Speed CAN2:")
        self.comboCan2 = QComboBox()
        self.comboCan2.setEditable(False)
        self.comboCan2.setToolTip("Choise speed of CAN2 speed")
        self.comboCan2.adjustSize()
        hlaycan2 = QHBoxLayout(self)
        hlaycan2.addWidget(self.labelCan2)
        hlaycan2.addWidget(self.comboCan2)
        widgetcan2 = QWidget(self)
        widgetcan2.setLayout(hlaycan2)
        self.labelAcc = QLabel("Acc sensetivity:")
        self.spinAcc = QSpinBox(self)
        self.spinAcc.setMinimum(0)
        self.spinAcc.setMaximum(20)
        hlayacc = QHBoxLayout(self)
        hlayacc.addWidget(self.labelAcc)
        hlayacc.addWidget(self.spinAcc)
        widgetacc = QWidget(self)
        widgetacc.setLayout(hlayacc)
        vlayRsAcc = QVBoxLayout(self)
        vlayRsAcc.addWidget(widgetRs)
        vlayRsAcc.addWidget(widgetacc)
        widgetRsAcc = QWidget(self)
        widgetRsAcc.setLayout(vlayRsAcc)
        vlayCan12 = QVBoxLayout(self)
        vlayCan12.addWidget(widgetcan1)
        vlayCan12.addWidget(widgetcan2)
        widgetCan12 = QWidget(self)
        widgetCan12.setLayout(vlayCan12)
        hlaySensorsUp = QHBoxLayout(self)
        hlaySensorsUp.addWidget(widgetSns)
        hlaySensorsUp.addWidget(widgetRsAcc)
        hlaySensorsUp.addWidget(widgetCan12)
        widgetSensorsUp = QWidget(self)
        widgetSensorsUp.setLayout(hlaySensorsUp)
        self.listMemo = QListWidget(self)
        self.listMemo.itemClicked.connect(self.OnListMemoClick)
        self.listItemsTtl = ["AIN 1", "AIN 2", "COUNT 1", "COUNT 2", "COUNT 3", "DIN", "FUEL 1",
                             "FUEL 2", "FUEL 3", "FUEL 4", "FUEL 5", "RESERVED 1", "RESERVED 2", "RESERVED 3", "RFID 1", "RFID 2"]
        self.listItemsTtu = ["AIN 1", "AIN 2", "CAN 1", "CAN 2", "CAN 3", "COUNT 1", "COUNT 2", "COUNT 3", "DIN",
                          "RS485 1", "RS485 2", "RS485 3", "RS485 4", "RS485 5", "RS485 6", "RS485 7"]
        self.labelSmOut = QLabel("SmOut")
        self.labelUsedIn = QLabel("Used In:")
        self.comboUsedIn = QComboBox()
        self.comboUsedIn.setEditable(False)
        self.comboUsedIn.setToolTip("Set value used in")
        self.comboUsedIn.adjustSize()
        hlaySm1 = QHBoxLayout(self)
        hlaySm1.addWidget(self.labelSmOut)
        widgetSm1 = QWidget(self)
        widgetSm1.setLayout(hlaySm1)
        hlaySm2 = QHBoxLayout(self)
        hlaySm2.addWidget(self.labelUsedIn)
        hlaySm2.addWidget(self.comboUsedIn)
        widgetSm2 = QWidget(self)
        widgetSm2.setLayout(hlaySm2)
        self.labelLvlUp = QLabel("Lvl Up:")
        self.spinLvlUp = QSpinBox()
        self.spinLvlUp.setMinimum(0)
        self.spinLvlUp.setMaximum(65535)
        hlaySm3 = QHBoxLayout(self)
        hlaySm3.addWidget(self.labelLvlUp)
        hlaySm3.addWidget(self.spinLvlUp)
        widgetSm3 = QWidget(self)
        widgetSm3.setLayout(hlaySm3)
        self.labelLvlLow = QLabel("Lvl Low:")
        self.spinLvlLow = QSpinBox()
        self.spinLvlLow.setMinimum(0)
        self.spinLvlLow.setMaximum(65535)
        hlaySm4 = QHBoxLayout(self)
        hlaySm4.addWidget(self.labelLvlLow)
        hlaySm4.addWidget(self.spinLvlLow)
        widgetSm4 = QWidget(self)
        widgetSm4.setLayout(hlaySm4)
        self.labelCmdIn = QLabel("Cmd IN:")
        self.lineCmdIn = QLineEdit()
        self.lineCmdIn.setToolTip("Set value Cmd IN")
        hlaySm5 = QHBoxLayout(self)
        hlaySm5.addWidget(self.labelCmdIn)
        hlaySm5.addWidget(self.lineCmdIn)
        widgetSm5 = QWidget(self)
        widgetSm5.setLayout(hlaySm5)
        self.labelCmdOut = QLabel("Cmd OUT:")
        self.lineCmdOut = QLineEdit()
        self.lineCmdOut.setToolTip("Set value Cmd OUT")
        hlaySm6 = QHBoxLayout(self)
        hlaySm6.addWidget(self.labelCmdOut)
        hlaySm6.addWidget(self.lineCmdOut)
        widgetSm6 = QWidget(self)
        widgetSm6.setLayout(hlaySm6)
        hlayVSim = QVBoxLayout(self)
        hlayVSim.addWidget(widgetSm1)
        hlayVSim.addWidget(widgetSm2)
        hlayVSim.addWidget(widgetSm3)
        hlayVSim.addWidget(widgetSm4)
        hlayVSim.addWidget(widgetSm5)
        hlayVSim.addWidget(widgetSm6)
        self.widgetSmOut = QWidget(self)
        self.widgetSmOut.setLayout(hlayVSim)
        self.labelDin1 = QLabel("DIN 1")
        self.checkDin1 = QCheckBox()
        self.labelChDin1 = QLabel("Do have change state")
        self.labelBounceDin1 = QLabel("Bounce, ms:")
        self.comboBounceDin1 = QComboBox()
        self.comboBounceDin1.setEditable(False)
        self.comboBounceDin1.setToolTip("Choice bounce")
        self.comboBounceDin1.adjustSize()
        hlayDin1_1 = QHBoxLayout(self)
        hlayDin1_1.addWidget(self.checkDin1)
        hlayDin1_1.addWidget(self.labelChDin1)
        widgetDin1_1 = QWidget(self)
        widgetDin1_1.setLayout(hlayDin1_1)
        hlayDin1_2 = QHBoxLayout(self)
        hlayDin1_2.addWidget(self.labelBounceDin1)
        hlayDin1_2.addWidget(self.comboBounceDin1)
        widgetDin1_2 = QWidget(self)
        widgetDin1_2.setLayout(hlayDin1_2)
        hlayVDin1 = QVBoxLayout(self)
        hlayVDin1.addWidget(self.labelDin1)
        hlayVDin1.addWidget(widgetDin1_1)
        hlayVDin1.addWidget(widgetDin1_2)
        widgetDin1 = QWidget(self)
        widgetDin1.setLayout(hlayVDin1)
        self.labelDin2 = QLabel("DIN 2")
        self.checkDin2 = QCheckBox()
        self.labelChDin2 = QLabel("Do have change state")
        self.labelBounceDin2 = QLabel("Bounce, ms:")
        self.comboBounceDin2 = QComboBox()
        self.comboBounceDin2.setEditable(False)
        self.comboBounceDin2.setToolTip("Choice bounce")
        self.comboBounceDin2.adjustSize()
        hlayDin2_1 = QHBoxLayout(self)
        hlayDin2_1.addWidget(self.checkDin2)
        hlayDin2_1.addWidget(self.labelChDin2)
        widgetDin2_1 = QWidget(self)
        widgetDin2_1.setLayout(hlayDin2_1)
        hlayDin2_2 = QHBoxLayout(self)
        hlayDin2_2.addWidget(self.labelBounceDin2)
        hlayDin2_2.addWidget(self.comboBounceDin2)
        widgetDin2_2 = QWidget(self)
        widgetDin2_2.setLayout(hlayDin2_2)
        hlayVDin2 = QVBoxLayout(self)
        hlayVDin2.addWidget(self.labelDin2)
        hlayVDin2.addWidget(widgetDin2_1)
        hlayVDin2.addWidget(widgetDin2_2)
        widgetDin2 = QWidget(self)
        widgetDin2.setLayout(hlayVDin2)
        self.labelDin3 = QLabel("DIN 3")
        self.checkDin3 = QCheckBox()
        self.labelChDin3 = QLabel("Do have change state")
        self.labelBounceDin3 = QLabel("Bounce, ms:")
        self.comboBounceDin3 = QComboBox()
        self.comboBounceDin3.setEditable(False)
        self.comboBounceDin3.setToolTip("Choice bounce")
        self.comboBounceDin3.adjustSize()
        hlayDin3_1 = QHBoxLayout(self)
        hlayDin3_1.addWidget(self.checkDin3)
        hlayDin3_1.addWidget(self.labelChDin3)
        widgetDin3_1 = QWidget(self)
        widgetDin3_1.setLayout(hlayDin3_1)
        hlayDin3_2 = QHBoxLayout(self)
        hlayDin3_2.addWidget(self.labelBounceDin3)
        hlayDin3_2.addWidget(self.comboBounceDin3)
        widgetDin3_2 = QWidget(self)
        widgetDin3_2.setLayout(hlayDin3_2)
        hlayVDin3 = QVBoxLayout(self)
        hlayVDin3.addWidget(self.labelDin3)
        hlayVDin3.addWidget(widgetDin3_1)
        hlayVDin3.addWidget(widgetDin3_2)
        widgetDin3 = QWidget(self)
        widgetDin3.setLayout(hlayVDin3)
        self.labelDin4 = QLabel("DIN 4")
        self.checkDin4 = QCheckBox()
        self.labelChDin4 = QLabel("Do have change state")
        self.labelBounceDin4 = QLabel("Bounce, ms:")
        self.comboBounceDin4 = QComboBox()
        self.comboBounceDin4.setEditable(False)
        self.comboBounceDin4.setToolTip("Choice bounce")
        self.comboBounceDin4.adjustSize()
        hlayDin4_1 = QHBoxLayout(self)
        hlayDin4_1.addWidget(self.checkDin4)
        hlayDin4_1.addWidget(self.labelChDin4)
        widgetDin4_1 = QWidget(self)
        widgetDin4_1.setLayout(hlayDin4_1)
        hlayDin4_2 = QHBoxLayout(self)
        hlayDin4_2.addWidget(self.labelBounceDin4)
        hlayDin4_2.addWidget(self.comboBounceDin4)
        widgetDin4_2 = QWidget(self)
        widgetDin4_2.setLayout(hlayDin4_2)
        hlayVDin4 = QVBoxLayout(self)
        hlayVDin4.addWidget(self.labelDin4)
        hlayVDin4.addWidget(widgetDin4_1)
        hlayVDin4.addWidget(widgetDin4_2)
        widgetDin4 = QWidget(self)
        widgetDin4.setLayout(hlayVDin4)
        hlayDinUp = QHBoxLayout(self)
        hlayDinUp.addWidget(widgetDin1)
        hlayDinUp.addWidget(widgetDin3)
        hlayDinDown = QHBoxLayout(self)
        hlayDinDown.addWidget(widgetDin2)
        hlayDinDown.addWidget(widgetDin4)
        widgetDin1Up = QWidget(self)
        widgetDin1Up.setLayout(hlayDinUp)
        widgetDin1Down = QWidget(self)
        widgetDin1Down.setLayout(hlayDinDown)
        self.hlayVDin1All = QVBoxLayout(self)
        self.hlayVDin1All.addWidget(widgetDin1Up)
        self.hlayVDin1All.addWidget(widgetDin1Down)
        self.hlayVDin1All.addWidget(self.widgetSmOut)
        self.labelFuelRec = QLabel("Fuel:")
        self.labelAddress = QLabel("Address:")
        self.pinAddress = QSpinBox(self)
        self.pinAddress.setMinimum(0)
        self.pinAddress.setMaximum(100)
        self.pinAddress.editingFinished.connect(self.on_address_change)
        self.labelStartBit = QLabel("Start bit:")
        self.spinStartBit = QSpinBox(self)
        self.spinStartBit.setMinimum(0)
        self.spinStartBit.setMaximum(100)
        self.spinStartBit.editingFinished.connect(self.on_startbit_change)
        self.labelLength = QLabel("Length:")
        self.spin_Length = QSpinBox(self)
        self.spin_Length.setMinimum(0)
        self.spin_Length.setMaximum(100)
        self.spin_Length.editingFinished.connect(self.on_length_change)
        self.labelAverage = QLabel("Average:")
        self.spinAverage = QSpinBox(self)
        self.spinAverage.setMinimum(0)
        self.spinAverage.setMaximum(100)
        self.spinAverage.editingFinished.connect(self.on_average_change)
        hlayFuel1 = QHBoxLayout()
        hlayFuel1.addWidget(self.labelFuelRec)
        hlayFuel2 = QHBoxLayout()
        hlayFuel2.addWidget(self.labelAddress)
        hlayFuel2.addWidget(self.pinAddress)
        hlayFuel3 = QHBoxLayout()
        hlayFuel3.addWidget(self.labelStartBit)
        hlayFuel3.addWidget(self.spinStartBit)
        hlayFuel4 = QHBoxLayout()
        hlayFuel4.addWidget(self.labelLength)
        hlayFuel4.addWidget(self.spin_Length)
        hlayFuel5 = QHBoxLayout()
        hlayFuel5.addWidget(self.labelAverage)
        hlayFuel5.addWidget(self.spinAverage)
        widgetFuel1 = QWidget(self)
        widgetFuel1.setLayout(hlayFuel1)
        widgetFuel2 = QWidget(self)
        widgetFuel2.setLayout(hlayFuel2)
        widgetFuel3 = QWidget(self)
        widgetFuel3.setLayout(hlayFuel3)
        widgetFuel4 = QWidget(self)
        widgetFuel4.setLayout(hlayFuel4)
        widgetFuel5 = QWidget(self)
        widgetFuel5.setLayout(hlayFuel5)
        self.hlayVFuel = QVBoxLayout()
        self.hlayVFuel.addWidget(widgetFuel1)
        self.hlayVFuel.addWidget(widgetFuel2)
        self.hlayVFuel.addWidget(widgetFuel3)
        self.hlayVFuel.addWidget(widgetFuel4)
        self.hlayVFuel.addWidget(widgetFuel5)
        self.labelRfidRec = QLabel("RFID:")
        self.labelAddrRfid = QLabel("Address:")
        self.spinAddrRfid = QSpinBox(self)
        self.spinAddrRfid.setMinimum(0)
        self.spinAddrRfid.setMaximum(100)
        self.spinAddrRfid.editingFinished.connect(self.on_addr_change)
        self.labelStartBitRfid = QLabel("Start bit:")
        self.spinStartBitRfid = QSpinBox(self)
        self.spinStartBitRfid.setMinimum(0)
        self.spinStartBitRfid.setMaximum(100)
        self.spinStartBitRfid.editingFinished.connect(self.on_stbit_change)
        self.labelLenRfid = QLabel("Length:")
        self.spinLenRfid = QSpinBox(self)
        self.spinLenRfid.setMinimum(0)
        self.spinLenRfid.setMaximum(100)
        self.spinLenRfid.editingFinished.connect(self.on_len_change)
        hlayRfid1 = QHBoxLayout()
        hlayRfid1.addWidget(self.labelRfidRec)
        hlayRfid2 = QHBoxLayout()
        hlayRfid2.addWidget(self.labelAddrRfid)
        hlayRfid2.addWidget(self.spinAddrRfid)
        hlayRfid3 = QHBoxLayout()
        hlayRfid3.addWidget(self.labelStartBitRfid)
        hlayRfid3.addWidget(self.spinStartBitRfid)
        hlayRfid4 = QHBoxLayout()
        hlayRfid4.addWidget(self.labelLenRfid)
        hlayRfid4.addWidget(self.spinLenRfid)
        widgetRfid1 = QWidget(self)
        widgetRfid1.setLayout(hlayRfid1)
        widgetRfid2 = QWidget(self)
        widgetRfid2.setLayout(hlayRfid2)
        widgetRfid3 = QWidget(self)
        widgetRfid3.setLayout(hlayRfid3)
        widgetRfid4 = QWidget(self)
        widgetRfid4.setLayout(hlayRfid4)
        self.hlayVRfid = QVBoxLayout()
        self.hlayVRfid.addWidget(widgetRfid1)
        self.hlayVRfid.addWidget(widgetRfid2)
        self.hlayVRfid.addWidget(widgetRfid3)
        self.hlayVRfid.addWidget(widgetRfid4)
        self.labelCount1 = QLabel("COUNT 1")
        self.labelCntRegim1 = QLabel("Regim:")
        self.comboCnt1 = QComboBox()
        self.comboCnt1.setEditable(False)
        self.comboCnt1.setToolTip("Choise regim")
        self.comboCnt1.adjustSize()
        hlayCnt1 = QHBoxLayout(self)
        hlayCnt1.addWidget(self.labelCntRegim1)
        hlayCnt1.addWidget(self.comboCnt1)
        widgetCnt1 = QWidget(self)
        widgetCnt1.setLayout(hlayCnt1)
        self.labelCnt1Event1 = QLabel("Event1:")
        self.spinEvent11 = QSpinBox()
        self.spinEvent11.setMinimum(0)
        self.spinEvent11.setMaximum(1024)
        hlayEvent11 = QHBoxLayout(self)
        hlayEvent11.addWidget(self.labelCnt1Event1)
        hlayEvent11.addWidget(self.spinEvent11)
        widgetEvent11 = QWidget(self)
        widgetEvent11.setLayout(hlayEvent11)
        self.labelCntEvent21 = QLabel("Event2:")
        self.spinEvent21 = QSpinBox()
        self.spinEvent21.setMinimum(0)
        self.spinEvent21.setMaximum(1024)
        hlayEvent21 = QHBoxLayout(self)
        hlayEvent21.addWidget(self.labelCntEvent21)
        hlayEvent21.addWidget(self.spinEvent21)
        widgetEvent21 = QWidget(self)
        widgetEvent21.setLayout(hlayEvent21)
        self.labelSource1 = QLabel("Source:")
        self.comboSource1 = QComboBox()
        self.comboSource1.setEditable(False)
        self.comboSource1.setToolTip("Choise chanale")
        self.comboSource1.adjustSize()
        self.spinSource1 = QSpinBox(self)
        self.spinSource1.setMinimum(1)
        self.spinSource1.setMaximum(4)
        hlaySource1 = QHBoxLayout(self)
        hlaySource1.addWidget(self.labelSource1)
        hlaySource1.addWidget(self.comboSource1)
        hlaySource1.addWidget(self.spinSource1)
        widgetSource1 = QWidget(self)
        widgetSource1.setLayout(hlaySource1)
        self.vlaySource1 = QVBoxLayout(self)
        self.vlaySource1.addWidget(self.labelCount1)
        self.vlaySource1.addWidget(widgetCnt1)
        self.vlaySource1.addWidget(widgetEvent11)
        self.vlaySource1.addWidget(widgetEvent21)
        self.vlaySource1.addWidget(widgetSource1)

        self.labelCount2 = QLabel("COUNT 2")
        self.labelCntRegim2 = QLabel("Regim:")
        self.comboCnt2 = QComboBox()
        self.comboCnt2.setEditable(False)
        self.comboCnt2.setToolTip("Choise regim")
        self.comboCnt2.adjustSize()
        hlayCnt2 = QHBoxLayout(self)
        hlayCnt2.addWidget(self.labelCntRegim2)
        hlayCnt2.addWidget(self.comboCnt2)
        widgetCnt2 = QWidget(self)
        widgetCnt2.setLayout(hlayCnt2)
        self.labelCnt2Event1 = QLabel("Event1:")
        self.spinEvent12 = QSpinBox()
        self.spinEvent12.setMinimum(0)
        self.spinEvent12.setMaximum(1024)
        hlayEvent12 = QHBoxLayout(self)
        hlayEvent12.addWidget(self.labelCnt2Event1)
        hlayEvent12.addWidget(self.spinEvent12)
        widgetEvent12 = QWidget(self)
        widgetEvent12.setLayout(hlayEvent12)
        self.labelCnt2Event2 = QLabel("Event2:")
        self.spinEvent22 = QSpinBox()
        self.spinEvent22.setMinimum(0)
        self.spinEvent22.setMaximum(1024)
        hlayEvent22 = QHBoxLayout(self)
        hlayEvent22.addWidget(self.labelCnt2Event2)
        hlayEvent22.addWidget(self.spinEvent22)
        widgetEvent22 = QWidget(self)
        widgetEvent22.setLayout(hlayEvent22)
        self.labelSource2 = QLabel("Source:")
        self.comboSource2 = QComboBox()
        self.comboSource2.setEditable(False)
        self.comboSource2.setToolTip("Choise chanale")
        self.comboSource2.adjustSize()
        self.spinSource2 = QSpinBox(self)
        self.spinSource2.setMinimum(1)
        self.spinSource2.setMaximum(4)
        hlaySource2 = QHBoxLayout(self)
        hlaySource2.addWidget(self.labelSource2)
        hlaySource2.addWidget(self.comboSource2)
        hlaySource2.addWidget(self.spinSource2)
        widgetSource2 = QWidget(self)
        widgetSource2.setLayout(hlaySource2)
        self.vlaySource2 = QVBoxLayout(self)
        self.vlaySource2.addWidget(self.labelCount2)
        self.vlaySource2.addWidget(widgetCnt2)
        self.vlaySource2.addWidget(widgetEvent12)
        self.vlaySource2.addWidget(widgetEvent22)
        self.vlaySource2.addWidget(widgetSource2)

        self.labelCount3 = QLabel("COUNT 3")
        self.labelCntRegim3 = QLabel("Regim:")
        self.comboCnt3 = QComboBox()
        self.comboCnt3.setEditable(False)
        self.comboCnt3.setToolTip("Choise regim")
        self.comboCnt3.adjustSize()
        hlayCnt3 = QHBoxLayout(self)
        hlayCnt3.addWidget(self.labelCntRegim3)
        hlayCnt3.addWidget(self.comboCnt3)
        widgetCnt3 = QWidget(self)
        widgetCnt3.setLayout(hlayCnt3)
        self.labelCnt3Event1 = QLabel("Event1:")
        self.spinEvent13 = QSpinBox()
        self.spinEvent13.setMinimum(0)
        self.spinEvent13.setMaximum(1024)
        hlayEvent13 = QHBoxLayout(self)
        hlayEvent13.addWidget(self.labelCnt3Event1)
        hlayEvent13.addWidget(self.spinEvent13)
        widgetEvent13 = QWidget(self)
        widgetEvent13.setLayout(hlayEvent13)
        self.labelCnt3Event3 = QLabel("Event2:")
        self.spinEvent23 = QSpinBox()
        self.spinEvent23.setMinimum(0)
        self.spinEvent23.setMaximum(1024)
        hlayEvent23 = QHBoxLayout(self)
        hlayEvent23.addWidget(self.labelCnt3Event3)
        hlayEvent23.addWidget(self.spinEvent23)
        widgetEvent23 = QWidget(self)
        widgetEvent23.setLayout(hlayEvent23)
        self.labelSource3 = QLabel("Source:")
        self.comboSource3 = QComboBox()
        self.comboSource3.setEditable(False)
        self.comboSource3.setToolTip("Choise chanale")
        self.comboSource3.adjustSize()
        self.spinSource3 = QSpinBox(self)
        self.spinSource3.setMinimum(1)
        self.spinSource3.setMaximum(4)
        hlaySource3 = QHBoxLayout(self)
        hlaySource3.addWidget(self.labelSource3)
        hlaySource3.addWidget(self.comboSource3)
        hlaySource3.addWidget(self.spinSource3)
        widgetSource3 = QWidget(self)
        widgetSource3.setLayout(hlaySource3)
        self.vlaySource3 = QVBoxLayout(self)
        self.vlaySource3.addWidget(self.labelCount3)
        self.vlaySource3.addWidget(widgetCnt3)
        self.vlaySource3.addWidget(widgetEvent13)
        self.vlaySource3.addWidget(widgetEvent23)
        self.vlaySource3.addWidget(widgetSource3)

        self.labelRs485Set1 = QLabel("RS485 Settings 1")
        self.labelTpSens1 = QLabel("Type sensor:")
        self.comboTpSens1 = QComboBox()
        self.comboTpSens1.setEditable(False)
        self.comboTpSens1.setToolTip("Choise sensor")
        self.comboTpSens1.adjustSize()
        hlayTpSens1 = QHBoxLayout(self)
        hlayTpSens1.addWidget(self.labelTpSens1)
        hlayTpSens1.addWidget(self.comboTpSens1)
        widgetTpSens1 = QWidget(self)
        widgetTpSens1.setLayout(hlayTpSens1)
        self.labelRqstData1 = QLabel("Request data:")
        self.checkRqstData1 = QCheckBox()
        hlayRqstData1 = QHBoxLayout(self)
        hlayRqstData1.addWidget(self.labelRqstData1)
        hlayRqstData1.addWidget(self.checkRqstData1)
        widgetRqstData1 = QWidget(self)
        widgetRqstData1.setLayout(hlayRqstData1)
        self.labelAddrs1 = QLabel("Address:")
        self.spinAddrs1 = QSpinBox(self)
        self.spinAddrs1.setMinimum(0)
        hlayAddrs1 = QHBoxLayout(self)
        hlayAddrs1.addWidget(self.labelAddrs1)
        hlayAddrs1.addWidget(self.spinAddrs1)
        widgetAddrs1 = QWidget(self)
        widgetAddrs1.setLayout(hlayAddrs1)
        self.labelStartBit1 = QLabel("Start bit:")
        self.spinStartBit1 = QSpinBox(self)
        self.spinStartBit1.setMinimum(0)
        hlayStartBit1 = QHBoxLayout(self)
        hlayStartBit1.addWidget(self.labelStartBit1)
        hlayStartBit1.addWidget(self.spinStartBit1)
        widgetStartBit1 = QWidget(self)
        widgetStartBit1.setLayout(hlayStartBit1)
        self.labelLen1 = QLabel("Length:")
        self.spinLen1 = QSpinBox(self)
        self.spinLen1.setMinimum(0)
        hlayLen1 = QHBoxLayout(self)
        hlayLen1.addWidget(self.labelLen1)
        hlayLen1.addWidget(self.spinLen1)
        widgetLen1 = QWidget(self)
        widgetLen1.setLayout(hlayLen1)
        self.labelAver1 = QLabel("Average:")
        self.spinAver1 = QSpinBox(self)
        self.spinAver1.setMinimum(0)
        hlayAver1 = QHBoxLayout(self)
        hlayAver1.addWidget(self.labelAver1)
        hlayAver1.addWidget(self.spinAver1)
        widgetAver1 = QWidget(self)
        widgetAver1.setLayout(hlayAver1)
        self.labelEvnt1 = QLabel("Event:")
        self.spinEvnt1 = QSpinBox(self)
        self.spinEvnt1.setMinimum(0)
        hlayEvnt1 = QHBoxLayout(self)
        hlayEvnt1.addWidget(self.labelEvnt1)
        hlayEvnt1.addWidget(self.spinEvnt1)
        widgetEvnt1 = QWidget(self)
        widgetEvnt1.setLayout(hlayEvnt1)
        self.vlayRs4851 = QVBoxLayout(self)
        self.vlayRs4851.addWidget(self.labelRs485Set1)
        self.vlayRs4851.addWidget(widgetTpSens1)
        self.vlayRs4851.addWidget(widgetRqstData1)
        self.vlayRs4851.addWidget(widgetAddrs1)
        self.vlayRs4851.addWidget(widgetStartBit1)
        self.vlayRs4851.addWidget(widgetLen1)
        self.vlayRs4851.addWidget(widgetAver1)
        self.vlayRs4851.addWidget(widgetEvnt1)
        self.labelRs485Set2 = QLabel("RS485 Settings 2")
        self.labelTpSens2 = QLabel("Type sensor:")
        self.comboTpSens2 = QComboBox()
        self.comboTpSens2.setEditable(False)
        self.comboTpSens2.setToolTip("Choise sensor")
        self.comboTpSens2.adjustSize()
        hlayTpSens2 = QHBoxLayout(self)
        hlayTpSens2.addWidget(self.labelTpSens2)
        hlayTpSens2.addWidget(self.comboTpSens2)
        widgetTpSens2 = QWidget(self)
        widgetTpSens2.setLayout(hlayTpSens2)
        self.labelRqstData2 = QLabel("Request data:")
        self.checkRqstData2 = QCheckBox()
        hlayRqstData2 = QHBoxLayout(self)
        hlayRqstData2.addWidget(self.labelRqstData2)
        hlayRqstData2.addWidget(self.checkRqstData2)
        widgetRqstData2 = QWidget(self)
        widgetRqstData2.setLayout(hlayRqstData2)
        self.labelAddrs2 = QLabel("Address:")
        self.spinAddrs2 = QSpinBox(self)
        self.spinAddrs2.setMinimum(0)
        hlayAddrs2 = QHBoxLayout(self)
        hlayAddrs2.addWidget(self.labelAddrs2)
        hlayAddrs2.addWidget(self.spinAddrs2)
        widgetAddrs2 = QWidget(self)
        widgetAddrs2.setLayout(hlayAddrs2)
        self.labelStartBit2 = QLabel("Start bit:")
        self.spinStartBit2 = QSpinBox(self)
        self.spinStartBit2.setMinimum(0)
        hlayStartBit2 = QHBoxLayout(self)
        hlayStartBit2.addWidget(self.labelStartBit2)
        hlayStartBit2.addWidget(self.spinStartBit2)
        widgetStartBit2 = QWidget(self)
        widgetStartBit2.setLayout(hlayStartBit2)
        self.labelLen2 = QLabel("Length:")
        self.spinLen2 = QSpinBox(self)
        self.spinLen2.setMinimum(0)
        hlayLen2 = QHBoxLayout(self)
        hlayLen2.addWidget(self.labelLen2)
        hlayLen2.addWidget(self.spinLen2)
        widgetLen2 = QWidget(self)
        widgetLen2.setLayout(hlayLen2)
        self.labelAver2 = QLabel("Average:")
        self.spinAver2 = QSpinBox(self)
        self.spinAver2.setMinimum(0)
        hlayAver2 = QHBoxLayout(self)
        hlayAver2.addWidget(self.labelAver2)
        hlayAver2.addWidget(self.spinAver2)
        widgetAver2 = QWidget(self)
        widgetAver2.setLayout(hlayAver2)
        self.labelEvnt2 = QLabel("Event:")
        self.spinEvnt2 = QSpinBox(self)
        self.spinEvnt2.setMinimum(0)
        hlayEvnt2 = QHBoxLayout(self)
        hlayEvnt2.addWidget(self.labelEvnt2)
        hlayEvnt2.addWidget(self.spinEvnt2)
        widgetEvnt2 = QWidget(self)
        widgetEvnt2.setLayout(hlayEvnt2)
        self.vlayRs4852 = QVBoxLayout(self)
        self.vlayRs4852.addWidget(self.labelRs485Set2)
        self.vlayRs4852.addWidget(widgetTpSens2)
        self.vlayRs4852.addWidget(widgetRqstData2)
        self.vlayRs4852.addWidget(widgetAddrs2)
        self.vlayRs4852.addWidget(widgetStartBit2)
        self.vlayRs4852.addWidget(widgetLen2)
        self.vlayRs4852.addWidget(widgetAver2)
        self.vlayRs4852.addWidget(widgetEvnt2)

        self.labelRs485Set3 = QLabel("RS485 Settings 3")
        self.labelTpSens3 = QLabel("Type sensor:")
        self.comboTpSens3 = QComboBox()
        self.comboTpSens3.setEditable(False)
        self.comboTpSens3.setToolTip("Choise sensor")
        self.comboTpSens3.adjustSize()
        hlayTpSens3 = QHBoxLayout(self)
        hlayTpSens3.addWidget(self.labelTpSens3)
        hlayTpSens3.addWidget(self.comboTpSens3)
        widgetTpSens3 = QWidget(self)
        widgetTpSens3.setLayout(hlayTpSens3)
        self.labelRqstData3 = QLabel("Request data:")
        self.checkRqstData3 = QCheckBox()
        hlayRqstData3 = QHBoxLayout(self)
        hlayRqstData3.addWidget(self.labelRqstData3)
        hlayRqstData3.addWidget(self.checkRqstData3)
        widgetRqstData3 = QWidget(self)
        widgetRqstData3.setLayout(hlayRqstData3)
        self.labelAddrs3 = QLabel("Address:")
        self.spinAddrs3 = QSpinBox(self)
        self.spinAddrs3.setMinimum(0)
        hlayAddrs3 = QHBoxLayout(self)
        hlayAddrs3.addWidget(self.labelAddrs3)
        hlayAddrs3.addWidget(self.spinAddrs3)
        widgetAddrs3 = QWidget(self)
        widgetAddrs3.setLayout(hlayAddrs3)
        self.labelStartBit3 = QLabel("Start bit:")
        self.spinStartBit3 = QSpinBox(self)
        self.spinStartBit3.setMinimum(0)
        hlayStartBit3 = QHBoxLayout(self)
        hlayStartBit3.addWidget(self.labelStartBit3)
        hlayStartBit3.addWidget(self.spinStartBit3)
        widgetStartBit3 = QWidget(self)
        widgetStartBit3.setLayout(hlayStartBit3)
        self.labelLen3 = QLabel("Length:")
        self.spinLen3 = QSpinBox(self)
        self.spinLen3.setMinimum(0)
        hlayLen3 = QHBoxLayout(self)
        hlayLen3.addWidget(self.labelLen3)
        hlayLen3.addWidget(self.spinLen3)
        widgetLen3 = QWidget(self)
        widgetLen3.setLayout(hlayLen3)
        self.labelAver3 = QLabel("Average:")
        self.spinAver3 = QSpinBox(self)
        self.spinAver3.setMinimum(0)
        hlayAver3 = QHBoxLayout(self)
        hlayAver3.addWidget(self.labelAver3)
        hlayAver3.addWidget(self.spinAver3)
        widgetAver3 = QWidget(self)
        widgetAver3.setLayout(hlayAver3)
        self.labelEvnt3 = QLabel("Event:")
        self.spinEvnt3 = QSpinBox(self)
        self.spinEvnt3.setMinimum(0)
        hlayEvnt3 = QHBoxLayout(self)
        hlayEvnt3.addWidget(self.labelEvnt3)
        hlayEvnt3.addWidget(self.spinEvnt3)
        widgetEvnt3 = QWidget(self)
        widgetEvnt3.setLayout(hlayEvnt3)
        self.vlayRs4853 = QVBoxLayout(self)
        self.vlayRs4853.addWidget(self.labelRs485Set3)
        self.vlayRs4853.addWidget(widgetTpSens3)
        self.vlayRs4853.addWidget(widgetRqstData3)
        self.vlayRs4853.addWidget(widgetAddrs3)
        self.vlayRs4853.addWidget(widgetStartBit3)
        self.vlayRs4853.addWidget(widgetLen3)
        self.vlayRs4853.addWidget(widgetAver3)
        self.vlayRs4853.addWidget(widgetEvnt3)

        self.labelRs485Set4 = QLabel("RS485 Settings 4")
        self.labelTpSens4 = QLabel("Type sensor:")
        self.comboTpSens4 = QComboBox()
        self.comboTpSens4.setEditable(False)
        self.comboTpSens4.setToolTip("Choise sensor")
        self.comboTpSens4.adjustSize()
        hlayTpSens4 = QHBoxLayout(self)
        hlayTpSens4.addWidget(self.labelTpSens4)
        hlayTpSens4.addWidget(self.comboTpSens4)
        widgetTpSens4 = QWidget(self)
        widgetTpSens4.setLayout(hlayTpSens4)
        self.labelRqstData4 = QLabel("Request data:")
        self.checkRqstData4 = QCheckBox()
        hlayRqstData4 = QHBoxLayout(self)
        hlayRqstData4.addWidget(self.labelRqstData4)
        hlayRqstData4.addWidget(self.checkRqstData4)
        widgetRqstData4 = QWidget(self)
        widgetRqstData4.setLayout(hlayRqstData4)
        self.labelAddrs4 = QLabel("Address:")
        self.spinAddrs4 = QSpinBox(self)
        self.spinAddrs4.setMinimum(0)
        hlayAddrs4 = QHBoxLayout(self)
        hlayAddrs4.addWidget(self.labelAddrs4)
        hlayAddrs4.addWidget(self.spinAddrs4)
        widgetAddrs4 = QWidget(self)
        widgetAddrs4.setLayout(hlayAddrs4)
        self.labelStartBit4 = QLabel("Start bit:")
        self.spinStartBit4 = QSpinBox(self)
        self.spinStartBit4.setMinimum(0)
        hlayStartBit4 = QHBoxLayout(self)
        hlayStartBit4.addWidget(self.labelStartBit4)
        hlayStartBit4.addWidget(self.spinStartBit4)
        widgetStartBit4 = QWidget(self)
        widgetStartBit4.setLayout(hlayStartBit4)
        self.labelLen4 = QLabel("Length:")
        self.spinLen4 = QSpinBox(self)
        self.spinLen4.setMinimum(0)
        hlayLen4 = QHBoxLayout(self)
        hlayLen4.addWidget(self.labelLen4)
        hlayLen4.addWidget(self.spinLen4)
        widgetLen4 = QWidget(self)
        widgetLen4.setLayout(hlayLen4)
        self.labelAver4 = QLabel("Average:")
        self.spinAver4 = QSpinBox(self)
        self.spinAver4.setMinimum(0)
        hlayAver4 = QHBoxLayout(self)
        hlayAver4.addWidget(self.labelAver4)
        hlayAver4.addWidget(self.spinAver4)
        widgetAver4 = QWidget(self)
        widgetAver4.setLayout(hlayAver4)
        self.labelEvnt4 = QLabel("Event:")
        self.spinEvnt4 = QSpinBox(self)
        self.spinEvnt4.setMinimum(0)
        hlayEvnt4 = QHBoxLayout(self)
        hlayEvnt4.addWidget(self.labelEvnt4)
        hlayEvnt4.addWidget(self.spinEvnt4)
        widgetEvnt4 = QWidget(self)
        widgetEvnt4.setLayout(hlayEvnt4)
        self.vlayRs4854 = QVBoxLayout(self)
        self.vlayRs4854.addWidget(self.labelRs485Set4)
        self.vlayRs4854.addWidget(widgetTpSens4)
        self.vlayRs4854.addWidget(widgetRqstData4)
        self.vlayRs4854.addWidget(widgetAddrs4)
        self.vlayRs4854.addWidget(widgetStartBit4)
        self.vlayRs4854.addWidget(widgetLen4)
        self.vlayRs4854.addWidget(widgetAver4)
        self.vlayRs4854.addWidget(widgetEvnt4)

        self.labelRs485Set5 = QLabel("RS485 Settings 5")
        self.labelTpSens5 = QLabel("Type sensor:")
        self.comboTpSens5 = QComboBox()
        self.comboTpSens5.setEditable(False)
        self.comboTpSens5.setToolTip("Choise sensor")
        self.comboTpSens5.adjustSize()
        hlayTpSens5 = QHBoxLayout(self)
        hlayTpSens5.addWidget(self.labelTpSens5)
        hlayTpSens5.addWidget(self.comboTpSens5)
        widgetTpSens5 = QWidget(self)
        widgetTpSens5.setLayout(hlayTpSens5)
        self.labelRqstData5 = QLabel("Request data:")
        self.checkRqstData5 = QCheckBox()
        hlayRqstData5 = QHBoxLayout(self)
        hlayRqstData5.addWidget(self.labelRqstData5)
        hlayRqstData5.addWidget(self.checkRqstData5)
        widgetRqstData5 = QWidget(self)
        widgetRqstData5.setLayout(hlayRqstData5)
        self.labelAddrs5 = QLabel("Address:")
        self.spinAddrs5 = QSpinBox(self)
        self.spinAddrs5.setMinimum(0)
        hlayAddrs5 = QHBoxLayout(self)
        hlayAddrs5.addWidget(self.labelAddrs5)
        hlayAddrs5.addWidget(self.spinAddrs5)
        widgetAddrs5 = QWidget(self)
        widgetAddrs5.setLayout(hlayAddrs5)
        self.labelStartBit5 = QLabel("Start bit:")
        self.spinStartBit5 = QSpinBox(self)
        self.spinStartBit5.setMinimum(0)
        hlayStartBit5 = QHBoxLayout(self)
        hlayStartBit5.addWidget(self.labelStartBit5)
        hlayStartBit5.addWidget(self.spinStartBit5)
        widgetStartBit5 = QWidget(self)
        widgetStartBit5.setLayout(hlayStartBit5)
        self.labelLen5 = QLabel("Length:")
        self.spinLen5 = QSpinBox(self)
        self.spinLen5.setMinimum(0)
        hlayLen5 = QHBoxLayout(self)
        hlayLen5.addWidget(self.labelLen5)
        hlayLen5.addWidget(self.spinLen5)
        widgetLen5 = QWidget(self)
        widgetLen5.setLayout(hlayLen5)
        self.labelAver5 = QLabel("Average:")
        self.spinAver5 = QSpinBox(self)
        self.spinAver5.setMinimum(0)
        hlayAver5 = QHBoxLayout(self)
        hlayAver5.addWidget(self.labelAver5)
        hlayAver5.addWidget(self.spinAver5)
        widgetAver5 = QWidget(self)
        widgetAver5.setLayout(hlayAver5)
        self.labelEvnt5 = QLabel("Event:")
        self.spinEvnt5 = QSpinBox(self)
        self.spinEvnt5.setMinimum(0)
        hlayEvnt5 = QHBoxLayout(self)
        hlayEvnt5.addWidget(self.labelEvnt5)
        hlayEvnt5.addWidget(self.spinEvnt5)
        widgetEvnt5 = QWidget(self)
        widgetEvnt5.setLayout(hlayEvnt5)
        self.vlayRs4855 = QVBoxLayout(self)
        self.vlayRs4855.addWidget(self.labelRs485Set5)
        self.vlayRs4855.addWidget(widgetTpSens5)
        self.vlayRs4855.addWidget(widgetRqstData5)
        self.vlayRs4855.addWidget(widgetAddrs5)
        self.vlayRs4855.addWidget(widgetStartBit5)
        self.vlayRs4855.addWidget(widgetLen5)
        self.vlayRs4855.addWidget(widgetAver5)
        self.vlayRs4855.addWidget(widgetEvnt5)

        self.labelRs485Set6 = QLabel("RS485 Settings 6")
        self.labelTpSens6 = QLabel("Type sensor:")
        self.comboTpSens6 = QComboBox()
        self.comboTpSens6.setEditable(False)
        self.comboTpSens6.setToolTip("Choise sensor")
        self.comboTpSens6.adjustSize()
        hlayTpSens6 = QHBoxLayout(self)
        hlayTpSens6.addWidget(self.labelTpSens6)
        hlayTpSens6.addWidget(self.comboTpSens6)
        widgetTpSens6 = QWidget(self)
        widgetTpSens6.setLayout(hlayTpSens6)
        self.labelRqstData6 = QLabel("Request data:")
        self.checkRqstData6 = QCheckBox()
        hlayRqstData6 = QHBoxLayout(self)
        hlayRqstData6.addWidget(self.labelRqstData6)
        hlayRqstData6.addWidget(self.checkRqstData6)
        widgetRqstData6 = QWidget(self)
        widgetRqstData6.setLayout(hlayRqstData6)
        self.labelAddrs6 = QLabel("Address:")
        self.spinAddrs6 = QSpinBox(self)
        self.spinAddrs6.setMinimum(0)
        hlayAddrs6 = QHBoxLayout(self)
        hlayAddrs6.addWidget(self.labelAddrs6)
        hlayAddrs6.addWidget(self.spinAddrs6)
        widgetAddrs6 = QWidget(self)
        widgetAddrs6.setLayout(hlayAddrs6)
        self.labelStartBit6 = QLabel("Start bit:")
        self.spinStartBit6 = QSpinBox(self)
        self.spinStartBit6.setMinimum(0)
        hlayStartBit6 = QHBoxLayout(self)
        hlayStartBit6.addWidget(self.labelStartBit6)
        hlayStartBit6.addWidget(self.spinStartBit6)
        widgetStartBit6 = QWidget(self)
        widgetStartBit6.setLayout(hlayStartBit6)
        self.labelLen6 = QLabel("Length:")
        self.spinLen6 = QSpinBox(self)
        self.spinLen6.setMinimum(0)
        hlayLen6 = QHBoxLayout(self)
        hlayLen6.addWidget(self.labelLen6)
        hlayLen6.addWidget(self.spinLen6)
        widgetLen6 = QWidget(self)
        widgetLen6.setLayout(hlayLen6)
        self.labelAver6 = QLabel("Average:")
        self.spinAver6 = QSpinBox(self)
        self.spinAver6.setMinimum(0)
        hlayAver6 = QHBoxLayout(self)
        hlayAver6.addWidget(self.labelAver6)
        hlayAver6.addWidget(self.spinAver6)
        widgetAver6 = QWidget(self)
        widgetAver6.setLayout(hlayAver6)
        self.labelEvnt6 = QLabel("Event:")
        self.spinEvnt6 = QSpinBox(self)
        self.spinEvnt6.setMinimum(0)
        hlayEvnt6 = QHBoxLayout(self)
        hlayEvnt6.addWidget(self.labelEvnt6)
        hlayEvnt6.addWidget(self.spinEvnt6)
        widgetEvnt6 = QWidget(self)
        widgetEvnt6.setLayout(hlayEvnt6)
        self.vlayRs4856 = QVBoxLayout(self)
        self.vlayRs4856.addWidget(self.labelRs485Set6)
        self.vlayRs4856.addWidget(widgetTpSens6)
        self.vlayRs4856.addWidget(widgetRqstData6)
        self.vlayRs4856.addWidget(widgetAddrs6)
        self.vlayRs4856.addWidget(widgetStartBit6)
        self.vlayRs4856.addWidget(widgetLen6)
        self.vlayRs4856.addWidget(widgetAver6)
        self.vlayRs4856.addWidget(widgetEvnt6)

        self.labelRs485Set7 = QLabel("RS485 Settings 7")
        self.labelTpSens7 = QLabel("Type sensor:")
        self.comboTpSens7 = QComboBox()
        self.comboTpSens7.setEditable(False)
        self.comboTpSens7.setToolTip("Choise sensor")
        self.comboTpSens7.adjustSize()
        hlayTpSens7 = QHBoxLayout(self)
        hlayTpSens7.addWidget(self.labelTpSens7)
        hlayTpSens7.addWidget(self.comboTpSens7)
        widgetTpSens7 = QWidget(self)
        widgetTpSens7.setLayout(hlayTpSens7)
        self.labelRqstData7 = QLabel("Request data:")
        self.checkRqstData7 = QCheckBox()
        hlayRqstData7 = QHBoxLayout(self)
        hlayRqstData7.addWidget(self.labelRqstData7)
        hlayRqstData7.addWidget(self.checkRqstData7)
        widgetRqstData7 = QWidget(self)
        widgetRqstData7.setLayout(hlayRqstData7)
        self.labelAddrs7 = QLabel("Address:")
        self.spinAddrs7 = QSpinBox(self)
        self.spinAddrs7.setMinimum(0)
        hlayAddrs7 = QHBoxLayout(self)
        hlayAddrs7.addWidget(self.labelAddrs7)
        hlayAddrs7.addWidget(self.spinAddrs7)
        widgetAddrs7 = QWidget(self)
        widgetAddrs7.setLayout(hlayAddrs7)
        self.labelStartBit7 = QLabel("Start bit:")
        self.spinStartBit7 = QSpinBox(self)
        self.spinStartBit7.setMinimum(0)
        hlayStartBit7 = QHBoxLayout(self)
        hlayStartBit7.addWidget(self.labelStartBit7)
        hlayStartBit7.addWidget(self.spinStartBit7)
        widgetStartBit7 = QWidget(self)
        widgetStartBit7.setLayout(hlayStartBit7)
        self.labelLen7 = QLabel("Length:")
        self.spinLen7 = QSpinBox(self)
        self.spinLen7.setMinimum(0)
        hlayLen7 = QHBoxLayout(self)
        hlayLen7.addWidget(self.labelLen7)
        hlayLen7.addWidget(self.spinLen7)
        widgetLen7 = QWidget(self)
        widgetLen7.setLayout(hlayLen7)
        self.labelAver7 = QLabel("Average:")
        self.spinAver7 = QSpinBox(self)
        self.spinAver7.setMinimum(0)
        hlayAver7 = QHBoxLayout(self)
        hlayAver7.addWidget(self.labelAver7)
        hlayAver7.addWidget(self.spinAver7)
        widgetAver7 = QWidget(self)
        widgetAver7.setLayout(hlayAver7)
        self.labelEvnt7 = QLabel("Event:")
        self.spinEvnt7 = QSpinBox(self)
        self.spinEvnt7.setMinimum(0)
        hlayEvnt7 = QHBoxLayout(self)
        hlayEvnt7.addWidget(self.labelEvnt7)
        hlayEvnt7.addWidget(self.spinEvnt7)
        widgetEvnt7 = QWidget(self)
        widgetEvnt7.setLayout(hlayEvnt7)
        self.vlayRs4857 = QVBoxLayout(self)
        self.vlayRs4857.addWidget(self.labelRs485Set7)
        self.vlayRs4857.addWidget(widgetTpSens7)
        self.vlayRs4857.addWidget(widgetRqstData7)
        self.vlayRs4857.addWidget(widgetAddrs7)
        self.vlayRs4857.addWidget(widgetStartBit7)
        self.vlayRs4857.addWidget(widgetLen7)
        self.vlayRs4857.addWidget(widgetAver7)
        self.vlayRs4857.addWidget(widgetEvnt7)

        self.labelCanSett1 = QLabel("CAN Settings 1")
        self.labelWorkCan1 = QLabel("Work CAN:")
        self.comboWorkCan1 = QComboBox()
        self.comboWorkCan1.setEditable(False)
        self.comboWorkCan1.setToolTip("Choise working CAN")
        self.comboWorkCan1.adjustSize()
        hlayWorkCan1 = QHBoxLayout(self)
        hlayWorkCan1.addWidget(self.labelWorkCan1)
        hlayWorkCan1.addWidget(self.comboWorkCan1)
        widgetWorkCan1 = QWidget(self)
        widgetWorkCan1.setLayout(hlayWorkCan1)
        self.labelCanAddrs1 = QLabel("Address:")
        self.lineCanAddrs1 = QLineEdit()
        hlayCanAddr1 = QHBoxLayout(self)
        hlayCanAddr1.addWidget(self.labelCanAddrs1)
        hlayCanAddr1.addWidget(self.lineCanAddrs1)
        widgetCanAddrs1 = QWidget(self)
        widgetCanAddrs1.setLayout(hlayCanAddr1)
        self.labelMask1 = QLabel("Mask:")
        self.lineMask1 = QLineEdit()
        hlayMask1 = QHBoxLayout(self)
        hlayMask1.addWidget(self.labelMask1)
        hlayMask1.addWidget(self.lineMask1)
        widgetMask1 = QWidget(self)
        widgetMask1.setLayout(hlayMask1)
        self.labelStBit1 = QLabel("Start bit:")
        self.spinStBit1 = QSpinBox(self)
        self.spinStBit1.setMinimum(0)
        hlayStBit1 = QHBoxLayout(self)
        hlayStBit1.addWidget(self.labelStBit1)
        hlayStBit1.addWidget(self.spinStBit1)
        widgetStBit1 = QWidget(self)
        widgetStBit1.setLayout(hlayStBit1)
        self.labelLngth1 = QLabel("Length:")
        self.spinLngth1 = QSpinBox(self)
        self.spinLngth1.setMinimum(0)
        hlayLngth1 = QHBoxLayout(self)
        hlayLngth1.addWidget(self.labelLngth1)
        hlayLngth1.addWidget(self.spinLngth1)
        widgetLngth1 = QWidget(self)
        widgetLngth1.setLayout(hlayLngth1)
        self.labelTmout1 = QLabel("Timeout (sec):")
        self.spinTmout1 = QSpinBox(self)
        self.spinTmout1.setMinimum(0)
        self.spinTmout1.setMaximum(1024)
        hlayTmout1 = QHBoxLayout(self)
        hlayTmout1.addWidget(self.labelTmout1)
        hlayTmout1.addWidget(self.spinTmout1)
        widgetTmout1 = QWidget(self)
        widgetTmout1.setLayout(hlayTmout1)
        self.vlayCan1 = QVBoxLayout(self)
        self.vlayCan1.addWidget(self.labelCanSett1)
        self.vlayCan1.addWidget(widgetWorkCan1)
        self.vlayCan1.addWidget(widgetCanAddrs1)
        self.vlayCan1.addWidget(widgetMask1)
        self.vlayCan1.addWidget(widgetStBit1)
        self.vlayCan1.addWidget(widgetLngth1)
        self.vlayCan1.addWidget(widgetTmout1)

        self.labelCanSett2 = QLabel("CAN Settings 2")
        self.labelWorkCan2 = QLabel("Work CAN:")
        self.comboWorkCan2 = QComboBox()
        self.comboWorkCan2.setEditable(False)
        self.comboWorkCan2.setToolTip("Choise working CAN")
        self.comboWorkCan2.adjustSize()
        hlayWorkCan2 = QHBoxLayout(self)
        hlayWorkCan2.addWidget(self.labelWorkCan2)
        hlayWorkCan2.addWidget(self.comboWorkCan2)
        widgetWorkCan2 = QWidget(self)
        widgetWorkCan2.setLayout(hlayWorkCan2)
        self.labelCanAddrs2 = QLabel("Address:")
        self.lineCanAddrs2 = QLineEdit()
        hlayCanAddr2 = QHBoxLayout(self)
        hlayCanAddr2.addWidget(self.labelCanAddrs2)
        hlayCanAddr2.addWidget(self.lineCanAddrs2)
        widgetCanAddrs2 = QWidget(self)
        widgetCanAddrs2.setLayout(hlayCanAddr2)
        self.labelMask2 = QLabel("Mask:")
        self.lineMask2 = QLineEdit()
        hlayMask2 = QHBoxLayout(self)
        hlayMask2.addWidget(self.labelMask2)
        hlayMask2.addWidget(self.lineMask2)
        widgetMask2 = QWidget(self)
        widgetMask2.setLayout(hlayMask2)
        self.labelStBit2 = QLabel("Start bit:")
        self.spinStBit2 = QSpinBox(self)
        self.spinStBit2.setMinimum(0)
        hlayStBit2 = QHBoxLayout(self)
        hlayStBit2.addWidget(self.labelStBit2)
        hlayStBit2.addWidget(self.spinStBit2)
        widgetStBit2 = QWidget(self)
        widgetStBit2.setLayout(hlayStBit2)
        self.labelLngth2 = QLabel("Length:")
        self.spinLngth2 = QSpinBox(self)
        self.spinLngth2.setMinimum(0)
        hlayLngth2 = QHBoxLayout(self)
        hlayLngth2.addWidget(self.labelLngth2)
        hlayLngth2.addWidget(self.spinLngth2)
        widgetLngth2 = QWidget(self)
        widgetLngth2.setLayout(hlayLngth2)
        self.labelTmout2 = QLabel("Timeout (sec):")
        self.spinTmout2 = QSpinBox(self)
        self.spinTmout2.setMinimum(0)
        self.spinTmout2.setMaximum(1024)
        hlayTmout2 = QHBoxLayout(self)
        hlayTmout2.addWidget(self.labelTmout2)
        hlayTmout2.addWidget(self.spinTmout2)
        widgetTmout2 = QWidget(self)
        widgetTmout2.setLayout(hlayTmout2)
        self.vlayCan2 = QVBoxLayout(self)
        self.vlayCan2.addWidget(self.labelCanSett2)
        self.vlayCan2.addWidget(widgetWorkCan2)
        self.vlayCan2.addWidget(widgetCanAddrs2)
        self.vlayCan2.addWidget(widgetMask2)
        self.vlayCan2.addWidget(widgetStBit2)
        self.vlayCan2.addWidget(widgetLngth2)
        self.vlayCan2.addWidget(widgetTmout2)

        self.labelCanSett3 = QLabel("CAN Settings 3")
        self.labelWorkCan3 = QLabel("Work CAN:")
        self.comboWorkCan3 = QComboBox()
        self.comboWorkCan3.setEditable(False)
        self.comboWorkCan3.setToolTip("Choise working CAN")
        self.comboWorkCan3.adjustSize()
        hlayWorkCan3 = QHBoxLayout(self)
        hlayWorkCan3.addWidget(self.labelWorkCan3)
        hlayWorkCan3.addWidget(self.comboWorkCan3)
        widgetWorkCan3 = QWidget(self)
        widgetWorkCan3.setLayout(hlayWorkCan3)
        self.labelCanAddrs3 = QLabel("Address:")
        self.lineCanAddrs3 = QLineEdit()
        hlayCanAddr3 = QHBoxLayout(self)
        hlayCanAddr3.addWidget(self.labelCanAddrs3)
        hlayCanAddr3.addWidget(self.lineCanAddrs3)
        widgetCanAddrs3 = QWidget(self)
        widgetCanAddrs3.setLayout(hlayCanAddr3)
        self.labelMask3 = QLabel("Mask:")
        self.lineMask3 = QLineEdit()
        hlayMask3 = QHBoxLayout(self)
        hlayMask3.addWidget(self.labelMask3)
        hlayMask3.addWidget(self.lineMask3)
        widgetMask3 = QWidget(self)
        widgetMask3.setLayout(hlayMask3)
        self.labelStBit3 = QLabel("Start bit:")
        self.spinStBit3 = QSpinBox(self)
        self.spinStBit3.setMinimum(0)
        hlayStBit3 = QHBoxLayout(self)
        hlayStBit3.addWidget(self.labelStBit3)
        hlayStBit3.addWidget(self.spinStBit3)
        widgetStBit3 = QWidget(self)
        widgetStBit3.setLayout(hlayStBit3)
        self.labelLngth3 = QLabel("Length:")
        self.spinLngth3 = QSpinBox(self)
        self.spinLngth3.setMinimum(0)
        hlayLngth3 = QHBoxLayout(self)
        hlayLngth3.addWidget(self.labelLngth3)
        hlayLngth3.addWidget(self.spinLngth3)
        widgetLngth3 = QWidget(self)
        widgetLngth3.setLayout(hlayLngth3)
        self.labelTmout3 = QLabel("Timeout (sec):")
        self.spinTmout3 = QSpinBox(self)
        self.spinTmout3.setMinimum(0)
        self.spinTmout3.setMaximum(1024)
        hlayTmout3 = QHBoxLayout(self)
        hlayTmout3.addWidget(self.labelTmout3)
        hlayTmout3.addWidget(self.spinTmout3)
        widgetTmout3 = QWidget(self)
        widgetTmout3.setLayout(hlayTmout3)
        self.vlayCan3 = QVBoxLayout(self)
        self.vlayCan3.addWidget(self.labelCanSett3)
        self.vlayCan3.addWidget(widgetWorkCan3)
        self.vlayCan3.addWidget(widgetCanAddrs3)
        self.vlayCan3.addWidget(widgetMask3)
        self.vlayCan3.addWidget(widgetStBit3)
        self.vlayCan3.addWidget(widgetLngth3)
        self.vlayCan3.addWidget(widgetTmout3)

        self.labelAin1 = QLabel("AIN 1")
        self.labelLimitUp1 = QLabel("Limit Up:")
        self.spinLimitUp1 = QSpinBox(self)
        self.spinLimitUp1.setMinimum(0)
        self.spinLimitUp1.setMaximum(4196)
        hlayLimitUp1 = QHBoxLayout(self)
        hlayLimitUp1.addWidget(self.labelLimitUp1)
        hlayLimitUp1.addWidget(self.spinLimitUp1)
        widgetLimitUp1 = QWidget(self)
        widgetLimitUp1.setLayout(hlayLimitUp1)
        self.labelLimitDown1 = QLabel("Limit Down:")
        self.spinLimitDown1 = QSpinBox(self)
        self.spinLimitDown1.setMinimum(0)
        self.spinLimitDown1.setMaximum(1048)
        hlayLimitDown1 = QHBoxLayout(self)
        hlayLimitDown1.addWidget(self.labelLimitDown1)
        hlayLimitDown1.addWidget(self.spinLimitDown1)
        widgetLimitDown1 = QWidget(self)
        widgetLimitDown1.setLayout(hlayLimitDown1)
        self.vlayAin1 = QVBoxLayout(self)
        self.vlayAin1.addWidget(self.labelAin1)
        self.vlayAin1.addWidget(widgetLimitUp1)
        self.vlayAin1.addWidget(widgetLimitDown1)

        self.labelAin2 = QLabel("AIN 2")
        self.labelLimitUp2 = QLabel("Limit Up:")
        self.spinLimitUp2 = QSpinBox(self)
        self.spinLimitUp2.setMinimum(0)
        self.spinLimitUp2.setMaximum(4196)
        hlayLimitUp2 = QHBoxLayout(self)
        hlayLimitUp2.addWidget(self.labelLimitUp2)
        hlayLimitUp2.addWidget(self.spinLimitUp2)
        widgetLimitUp2 = QWidget(self)
        widgetLimitUp2.setLayout(hlayLimitUp2)
        self.labelLimitDown2 = QLabel("Limit Down:")
        self.spinLimitDown2 = QSpinBox(self)
        self.spinLimitDown2.setMinimum(0)
        self.spinLimitDown2.setMaximum(1048)
        hlayLimitDown2 = QHBoxLayout(self)
        hlayLimitDown2.addWidget(self.labelLimitDown2)
        hlayLimitDown2.addWidget(self.spinLimitDown2)
        widgetLimitDown2 = QWidget(self)
        widgetLimitDown2.setLayout(hlayLimitDown2)
        self.vlayAin2 = QVBoxLayout(self)
        self.vlayAin2.addWidget(self.labelAin2)
        self.vlayAin2.addWidget(widgetLimitUp2)
        self.vlayAin2.addWidget(widgetLimitDown2)

        self.InitalStackWidgetSensors()
        self.labelTerm = QLabel("Type/Choise command:")
        self.comboTerminal = QComboBox(self)
        self.comboTerminal.setToolTip("Print command for sending to selected tracker")
        self.comboTerminal.setEditable(True)
        self.comboTerminal.setEnabled(False)
        self.comboTerminal.adjustSize()
        self.btnSendTerm = QPushButton("Send Command")
        self.btnSendTerm.setToolTip("Send command to selected tracker")
        self.btnSendTerm.setEnabled(False)
        self.btnSendTerm.clicked.connect(self.OnSendTerm)
        self.btnOpenFileTerm = QPushButton("Open File")
        self.btnOpenFileTerm.setToolTip("Открыть файл прошивки")
        self.btnOpenFileTerm.setEnabled(False)
        self.btnOpenFileTerm.clicked.connect(self.OnOpenUpdateFileTerm)
        self.lineTerm = QLineEdit(self)
        self.lineTerm.setEnabled(False)
        self.lineTerm.setToolTip("Путь к файлу прошивки трекера")
        self.btnBUpdateTerm = QPushButton("Begin Update")
        self.btnBUpdateTerm.setToolTip("Записать прошивку в трекер")
        self.btnBUpdateTerm.setEnabled(False)
        self.btnBUpdateTerm.clicked.connect(self.OnBeginUpdateTerm)
        self.checkHexModeTerm = QCheckBox()
        self.checkHexModeTerm.setEnabled(False)
        self.labelHexModeTerm = QLabel("Hex Mode")
        self.btnClearTerm = QPushButton("Clear")
        self.btnClearTerm.setToolTip("Clear")
        self.btnClearTerm.setEnabled(False)
        self.btnClearTerm.clicked.connect(self.OnClearTerm)
        self.text_term = QPlainTextEdit()
        self.text_term.setReadOnly(True)
        hlayTerminalUp = QHBoxLayout(self)
        hlayTerminalUp.addWidget(self.labelTerm)
        hlayTerminalUp.addWidget(self.comboTerminal)
        hlayTerminalUp.addWidget(self.btnSendTerm)
        hlayTerminalUp.addWidget(self.btnOpenFileTerm)
        hlayTerminalUp.addWidget(self.lineTerm)
        hlayTerminalUp.addWidget(self.btnBUpdateTerm)
        hlayTerminalUp.addWidget(self.checkHexModeTerm)
        hlayTerminalUp.addWidget(self.labelHexModeTerm)
        hlayTerminalUp.addWidget(self.btnClearTerm)
        widgetTermUp = QWidget(self)
        widgetTermUp.setLayout(hlayTerminalUp)
        vlayTermDown = QVBoxLayout(self)
        vlayTermDown.addWidget(widgetTermUp)
        vlayTermDown.addWidget(self.text_term)
        self.btnRequest = QPushButton("Request")
        self.btnRequest.setToolTip("Request")
        self.btnRequest.clicked.connect(self.OnRequest)
        self.checkRequest = QCheckBox()
        self.labelRequest = QLabel("Auto Requests")
        hlayrequest = QHBoxLayout(self)
        hlayrequest.addWidget(self.btnRequest)
        hlayrequest.addWidget(self.checkRequest)
        hlayrequest.addWidget(self.labelRequest)
        widgetrequest = QWidget(self)
        widgetrequest.setLayout(hlayrequest)
        self.labelViewer = QLabel("Sensors")
        self.tableSensViewer = QTableWidget(self)
        self.tableSensViewer.setColumnCount(2)
        self.tableSensViewer.setHorizontalHeaderLabels(["Sensors Name", "Sensors Value"])
        self.tableSensViewer.horizontalHeaderItem(0).setToolTip("Sensors name")
        self.tableSensViewer.horizontalHeaderItem(1).setToolTip("Sensors value")
        self.tableSensViewer.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableSensViewer.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableSensViewer.resizeColumnsToContents()
        self.tableSensViewer.resizeRowsToContents()
        self.tableSensViewer.setSelectionBehavior(QTableView.SelectRows)
        vlaySensView = QVBoxLayout(self)
        vlaySensView.addWidget(self.labelViewer)
        vlaySensView.addWidget(self.tableSensViewer)
        widgetSensView = QWidget(self)
        widgetSensView.setLayout(vlaySensView)
        self.labelOthers = QLabel("Others")
        self.labelRssi = QLabel("RSSI:")
        self.lineRssi = QLineEdit()
        self.lineRssi.setToolTip("Type RSSI")
        hlayrssi = QHBoxLayout(self)
        hlayrssi.addWidget(self.labelRssi)
        hlayrssi.addWidget(self.lineRssi)
        widgetrssi = QWidget(self)
        widgetrssi.setLayout(hlayrssi)
        self.labelSatt = QLabel("GPS Satellites:")
        self.lineSatt = QLineEdit()
        self.lineSatt.setToolTip("GPS satellites")
        hlaysatt = QHBoxLayout(self)
        hlaysatt.addWidget(self.labelSatt)
        hlaysatt.addWidget(self.lineSatt)
        widgetsatt = QWidget(self)
        widgetsatt.setLayout(hlaysatt)
        self.labelVbort = QLabel("Voltage in bort:")
        self.lineVbort = QLineEdit()
        self.lineVbort.setToolTip("Value voltage in bort")
        hlayVbort = QHBoxLayout(self)
        hlayVbort.addWidget(self.labelVbort)
        hlayVbort.addWidget(self.lineVbort)
        widgetVbort = QWidget(self)
        widgetVbort.setLayout(hlayVbort)
        vlayviewothers = QVBoxLayout(self)
        vlayviewothers.addWidget(self.labelOthers)
        vlayviewothers.addWidget(widgetrssi)
        vlayviewothers.addWidget(widgetsatt)
        vlayviewothers.addWidget(widgetVbort)
        self.btngetTypeLLS = QPushButton("Sensors RS485")
        self.btngetTypeLLS.setToolTip("Get type sensors (LLS) RS485")
        self.btngetTypeLLS.clicked.connect(self.OnGetTypeLls)
        self.tableTypeLls = QTableWidget(self)
        self.tableTypeLls.setColumnCount(3)
        self.tableTypeLls.setHorizontalHeaderLabels(["Address", "Type", "Info"])
        self.tableTypeLls.horizontalHeaderItem(0).setToolTip("Lls address")
        self.tableTypeLls.horizontalHeaderItem(1).setToolTip("Lls type")
        self.tableTypeLls.horizontalHeaderItem(2).setToolTip("Lls info")
        self.tableTypeLls.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTypeLls.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTypeLls.horizontalHeaderItem(2).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableTypeLls.resizeColumnsToContents()
        self.tableTypeLls.resizeRowsToContents()
        self.tableTypeLls.setSelectionBehavior(QTableView.SelectRows)
        self.labelWire = QLabel("One Wire")
        self.btnFuelRfid = QPushButton('FUEL', self)
        self.btnFuelRfid.setCheckable(True)
        self.btnFuelRfid.clicked[bool].connect(self.setFuelRfid)
        self.btnFuelRfid.setEnabled(True)
        self.btnFuelRfid.setToolTip("Switcher FUEL/RFID")
        self.btnFuelRfid.setMaximumWidth(80)
        self.btnFuelRfid.setMinimumWidth(80)
        self.btnFindWire = QPushButton("Find")
        self.btnFindWire.setToolTip("Find one fire")
        self.btnFindWire.clicked.connect(self.OnFindWire)
        self.tableSensWire = QTableWidget(self)
        self.tableSensWire.setColumnCount(2)
        self.tableSensWire.setHorizontalHeaderLabels(["Address", "Name"])
        self.tableSensWire.horizontalHeaderItem(0).setToolTip("Wire address")
        self.tableSensWire.horizontalHeaderItem(1).setToolTip("Wire name")
        self.tableSensWire.horizontalHeaderItem(0).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableSensWire.horizontalHeaderItem(1).setTextAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tableSensWire.resizeColumnsToContents()
        self.tableSensWire.resizeRowsToContents()
        self.tableSensWire.setSelectionBehavior(QTableView.SelectRows)
        hlaylls = QHBoxLayout(self)
        hlaylls.addWidget(self.btngetTypeLLS)
        hlaylls.addWidget(self.tableTypeLls)
        hlaywire = QHBoxLayout(self)
        hlaywire.addWidget(self.btnFindWire)
        hlaywire.addWidget(self.tableSensWire)
        widgetwirells = QWidget(self)
        widgetwirells.setLayout(hlaylls)
        widgetwire = QWidget(self)
        widgetwire.setLayout(hlaywire)
        vlaywiredown = QVBoxLayout(self)
        vlaywiredown.addWidget(self.labelWire)
        vlaywiredown.addWidget(self.btnFuelRfid)
        vlaywiredown.addWidget(widgetwirells)
        vlaywiredown.addWidget(widgetwire)
        widgetWiretake = QWidget(self)
        widgetWiretake.setLayout(vlaywiredown)
        vlayviewothers.addWidget(widgetWiretake)
        widgetviewothers = QWidget(self)
        widgetviewothers.setLayout(vlayviewothers)
        vlayrightview = QVBoxLayout(self)
        vlayrightview.addWidget(widgetrequest)
        vlayrightview.addWidget(widgetviewothers)
        widgetrightview = QWidget(self)
        widgetrightview.setLayout(vlayrightview)
        hlayViewer = QHBoxLayout(self)
        hlayViewer.addWidget(widgetSensView)
        hlayViewer.addWidget(widgetrightview)
        widgetSensorsDown = QWidget(self)
        widgetSensorsDown.setLayout(self.hlaySensorsDown)
        vlayoutSensorsUp = QVBoxLayout(self)
        vlayoutSensorsUp.addWidget(widgetSensorsUp)
        vlayoutSensorsUp.addWidget(widgetSensorsDown)
        hlayConnectReg = QVBoxLayout(self)
        hlayConnectReg.addWidget(self.tableServPhone)
        hlayConnectReg.addWidget(self.tableServPhone)
        hlayConnectReg.addWidget(self.labelConnectReg)
        hlayConnectReg.addWidget(self.lineReg)
        hlayTransport = QVBoxLayout(self)
        hlayTransport.addWidget(self.labelServSett)
        hlayTransport.addWidget(self.tableServSett)
        hlayTransport.addWidget(self.btnGprsAccess)
        hlayTransport.addWidget(self.labelGprs)
        hlayTransport.addWidget(self.tableGprs)
        widgetTransport = QWidget(self)
        widgetTransport.setLayout(hlayTransport)
        hlayOperSett = QVBoxLayout(self)
        hlayOperSett.addWidget(self.labelOperSett)
        hlayOperSett.addWidget(self.comboOperSett)
        hlayOperSett.addWidget(self.btnOperUp)
        hlayOperSett.addWidget(self.btnOperDown)
        widgetOperSett = QWidget(self)
        widgetOperSett.setLayout(hlayOperSett)
        hlayHTransport = QHBoxLayout(self)
        hlayHTransport.addWidget(widgetTransport)
        hlayHTransport.addWidget(widgetOperSett)
        layoutTextEdit = QVBoxLayout(self)
        layoutTextEdit.addWidget(self.text_edit)
        hlayTxtCmd = QHBoxLayout(self)
        hlayTxtCmd.addWidget(self.labelTxtCmd)
        hlayTxtCmd.addWidget(self.lineTxtCmd)
        widgetTxtCmd = QWidget(self)
        widgetTxtCmd.setLayout(hlayTxtCmd)
        hlayTxtRequest = QHBoxLayout(self)
        hlayTxtRequest.addWidget(self.labelTxtRequest)
        hlayTxtRequest.addWidget(self.lineTxtRequest)
        widgetTxtRequest = QWidget(self)
        widgetTxtRequest.setLayout(hlayTxtRequest)
        hlayCmd = QVBoxLayout(self)
        hlayCmd.addWidget(widgetTxtCmd)
        hlayCmd.addWidget(widgetTxtRequest)
        hlayParamPhone = QVBoxLayout(self)
        hlayParamPhone.addWidget(self.labelPhonesParam)
        hlayParamPhone.addWidget(self.tablePhonesParam)
        hlayParamPhone.addWidget(self.labelSIMCard)
        hlayParamPhone.addWidget(self.lineSIMCard)
        hlayParamPhone.addWidget(self.btnSetPhone)
        widgetParamPhone = QWidget(self)
        widgetParamPhone.setLayout(hlayParamPhone)
        hlaySetPhone = QVBoxLayout(self)
        hlaySetPhone.addWidget(self.labelPhone)
        hlaySetPhone.addWidget(self.tablePhones)
        hlaySetPhone.addWidget(self.btnAddPhone)
        hlaySetPhone.addWidget(self.btnDelPhone)
        widgetSetPhone = QWidget(self)
        widgetSetPhone.setLayout(hlaySetPhone)
        hlayPhone = QHBoxLayout(self)
        hlayPhone.addWidget(widgetParamPhone)
        hlayPhone.addWidget(self.btnGoPhone)
        hlayPhone.addWidget(widgetSetPhone)
        hlayServerTelematic = QVBoxLayout(self)
        hlayServerTelematic.addWidget(self.labelTelematicServer)
        hlayServerTelematic.addWidget(self.tableTelematicServer)
        hlayServerTelematic.addWidget(self.btnGenAccess)
        widgetTelematicServer = QWidget(self)
        widgetTelematicServer.setLayout(hlayServerTelematic)
        hlayServiceSetting = QVBoxLayout(self)
        hlayServiceSetting.addWidget(self.labelServiceSettings)
        hlayServiceSetting.addWidget(self.tableServiceSettings)
        hlayServiceSetting.addWidget(self.btnSaveSetting)
        widgetServiceSetting = QWidget(self)
        widgetServiceSetting.setLayout(hlayServiceSetting)
        hlayServer = QHBoxLayout(self)
        hlayServer.addWidget(widgetTelematicServer)
        hlayServer.addWidget(widgetServiceSetting)
        hlayParamRegim = QVBoxLayout(self)
        hlayParamRegim.addWidget(self.labelRegim)
        hlayParamRegim.addWidget(self.comboRegim)
        hlayParamRegim.addWidget(self.btnSetLeft)
        hlayParamRegim.addWidget(self.btnReadRegim)
        widgetParamRegim = QWidget(self)
        widgetParamRegim.setLayout(hlayParamRegim)
        hlayRegim = QHBoxLayout(self)
        hlayRegim.addWidget(self.tableRegim)
        hlayRegim.addWidget(widgetParamRegim)
        downUp.addWidget(self.tableTrack)
        splitter1.addWidget(up_widget)
        splitter1.addWidget(down_widget)
        splitter = QSplitter(Qt.Horizontal)
        splitter.setHandleWidth(3)
        hlay = QHBoxLayout(self)
        case_widget = QWidget(self)
        hlay.addWidget(splitter1)
        case_widget.setLayout(hlay)
        hlayEvents = QHBoxLayout()
        hlayEvents.addWidget(self.tableEvents)
        self.tabs = QTabWidget(self)
        self.tabs.currentChanged.connect(self.OnTabClicked)
        self.tabEvent = QWidget()
        self.tabEvent.setLayout(hlayEvents)
        self.tabs.addTab(self.tabEvent, "Events")
        self.tabGprs = QWidget()
        self.tabGprs.setLayout(hlayRegim)
        self.tabs.addTab(self.tabGprs, "GPRS")
        self.tabServer = QWidget()
        self.tabServer.setLayout(hlayServer)
        self.tabs.addTab(self.tabServer, "Server")
        self.tabPhones = QWidget()
        self.tabPhones.setLayout(hlayPhone)
        self.tabs.addTab(self.tabPhones, "Phones")
        self.tabCmd = QWidget()
        self.tabCmd.setLayout(hlayCmd)
        self.tabs.addTab(self.tabCmd, "TextCmd")
        self.tabGps = QWidget()
        self.tabGps.setLayout(layoutTextEdit)
        self.tabs.addTab(self.tabGps, "GPS")
        self.tabTransport = QWidget()
        self.tabTransport.setLayout(hlayHTransport)
        self.tabs.addTab(self.tabTransport, "Transport")
        self.tabService = QWidget()
        self.tabService.setLayout(hlayConnectReg)
        self.tabs.addTab(self.tabService, "Service")
        self.tabSensors = QWidget()
        self.tabSensors.setLayout(vlayoutSensorsUp)
        self.tabs.addTab(self.tabSensors, "Sensors")
        self.tabTerminal = QWidget()
        self.tabTerminal.setLayout(vlayTermDown)
        self.tabs.addTab(self.tabTerminal, "Terminal")
        self.tabViewer = QWidget()
        self.tabViewer.setLayout(hlayViewer)
        self.tabs.addTab(self.tabViewer, "Viewer")
        self.tabLabels = QWidget()
        self.tabLabels.setLayout(hlayHLabels)
        self.tabs.addTab(self.tabLabels, "Labels")
        hlays = QHBoxLayout(self)
        hlays.addWidget(self.tabs)
        log_widget = QWidget(self)
        log_widget.setLayout(hlays)
        splitter3 = QSplitter(Qt.Vertical)
        splitter3.setHandleWidth(3)
        splitter3.addWidget(hWidget1)
        splitter3.addWidget(log_widget)
        splitter.addWidget(case_widget)
        # case_widget.setMaximumWidth(450)
        # case_widget.setMinimumWidth(450)
        splitter.addWidget(splitter3)
        hlayout2.addWidget(splitter)
        hlayout3 = QHBoxLayout(self)
        hlayout3.addWidget(self.btnReadParam)
        hlayout3.addWidget(self.lineParam)
        hlayout3.addWidget(self.btnWriteParam)
        hWidget3 = QWidget(self)
        hWidget3.setLayout(hlayout3)
        vlayout.addWidget(hWidget2)
        vlayout.addWidget(hWidget3)
        centralWidget.setLayout(vlayout)
        hWidget1.setMinimumHeight(50)
        hWidget1.setMaximumHeight(50)
        hWidget3.setMinimumHeight(self.HEIGHT_WIDGET)
        hWidget3.setMaximumHeight(self.HEIGHT_WIDGET)
        self.ReadingParamTabAll()
        self.initComboBoxTypeTracker()
        self.initTableEvent(self.switchBtn.isChecked())
        self.initTableRegim()
        self.initComboRegim()
        self.initTableTelematicServer()
        self.initTableServiceSetting()
        self.initTablePhonesParam()
        self.initTablePhones()
        self.initTableRegim()
        self.initTableGprs(self.switchBtn.isChecked())
        self.initTableServSett()
        self.initComboOpperSetting()
        self.initServicePhone(self.switchBtn.isChecked())
        self.initSensorsTab()
        self.initTerminalTab()
        self.initSensViewerTab()
        self.initTextCmdTab()
        self.initTabGps()
        self.initTabSensors()
        self.initTabLabels()
        if self.OnGetTrackersData() == True:
            self.OnGetInfoTrackers()
        else:
            sys.exit(0)
        return

    @pyqtSlot()
    def OnClearViewLabel(self):
        if showInfo("OnClearViewLabel", "Хотите обнулить таблицу Просмотра меток?", "Эта операция удалит все метки из таблицы Просмотра меток") == self.OK_MSG:
            self.spacClearTable(self.tableLabel)
            self.btnClearTableView.setEnabled(False)
            self.currentIDChain = 0
            self.labelLastDateResponse.setText("Date of last request:")
            self.labelDateLabel.setText("Date of Labels:")
            self.labelBeginIndex.setText("Beginer index:")
            self.labelEndMarker.setText("End marker of Labels:")
            self.labelMaskLabel.setText("Mask of Labels:")
        return

    @pyqtSlot()
    def OnClearNewLabel(self):
        if showInfo("OnClearNewLabel", "Хотите обнулить таблицу Добавленных меток?", "Эта операция удалит все метки из таблицы Добавленных меток") == self.OK_MSG:
            self.spacClearTable(self.tableNewLabel)
            self.btnClearTableNew.setEnabled(False)
        return

    @pyqtSlot()
    def OnViewLabel(self):
        try:
            if self.comboType.currentText() == 'TTL/TTU':
                if self.comboTypeSens.currentText() == 'TTU':
                    if self.TestPin():
                        return
                    self.btnViewLabel.setEnabled(False)
                    self.lastLabel = '0'
                    self.currentIndex = 0
                    self.spacClearTable(self.tableLabel)
                    self.btnClearTableView.setEnabled(False)
                    self.currentIDChain = 0
                    self.labelLastDateResponse.setText("Date of last request:")
                    self.labelDateLabel.setText("Date of Labels:")
                    self.labelBeginIndex.setText("Beginer index:")
                    self.labelEndMarker.setText("End marker of Labels:")
                    self.labelMaskLabel.setText("Mask of Labels:")
                    start = self.prefixCmd + self.pinCode.text() + ","
                    selected = self.tableTrack.selectionModel().selectedRows()
                    if len(selected) == 0:
                        showError("OnViewLabel", "Нет трекера выделенного для указанной операции!", "Выделите трекер в таблице трекеров")
                        return
                    for ls in selected:
                        select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                        if len(select_identifier) == 4:
                            nameproto = self.protocolInfo[self.comboType.currentText()]
                            if self.comboClient.currentText() == 'diller':
                                idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                                nameclient = self.comboDiler.currentText()
                            else:
                                idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                                nameclient = self.comboClient.currentText()
                            self.lineParam.appendPlainText("OnViewLabel: Get all Labels from " + select_identifier)
                            idcomm = 215
                            id_chain = int(time.time())
                            self.SendStringToPort([start + "SLEGAL=" + self.lastLabel + ","], select_identifier, nameclient, idclient, nameproto, 1,
                                                  "read", "Labels", idcomm, id_chain, self.pinCode.text(), self.comboTypeSens.currentText())
                        else:
                            showError("OnViewLabel", "Размер ID трекера не соотвествует типу teletrack", "Длина должна быть 4 символа")
                    self.OnGetInfoTrackers()
        except Exception as er:
            showError("OnViewLabel", str(er), str(traceback.format_exc()))
        finally:
            self.btnViewLabel.setEnabled(True)

    @pyqtSlot()
    def viewClickedLabel(self):
        pass

    @pyqtSlot()
    def OnGetMoreLabels(self):
        try:
            if self.comboType.currentText() == 'TTL/TTU':
                if self.comboTypeSens.currentText() == 'TTU':
                    if self.TestPin():
                        return
                    self.btnViewLabel.setEnabled(False)
                    self.btnGetLabel.setEnabled(False)
                    start = self.prefixCmd + self.pinCode.text() + ","
                    selected = self.tableTrack.selectionModel().selectedRows()
                    if len(selected) == 0:
                        showError("OnGetMoreLabels", "Нет трекера выделенного для указанной операции!", "Выделите трекер в таблице трекеров")
                        return
                    for ls in selected:
                        select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                        if len(select_identifier) == 4:
                            nameproto = self.protocolInfo[self.comboType.currentText()]
                            if self.comboClient.currentText() == 'diller':
                                idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                                nameclient = self.comboDiler.currentText()
                            else:
                                idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                                nameclient = self.comboClient.currentText()
                            self.lineParam.appendPlainText("OnGetMoreLabels: Get more Labels from " + select_identifier)
                            idcomm = 217
                            id_chain = int(time.time())
                            self.SendStringToPort([start + "SLEGAL=" + self.lastLabel + ","], select_identifier, nameclient, idclient, nameproto, 1,
                                                  "read", "Labels", idcomm, id_chain, self.pinCode.text(), self.comboTypeSens.currentText())
                        else:
                            showError("OnGetMoreLabels", "Размер ID трекера не соотвествует типу teletrack",
                                      "Длина должна быть 4 символа")
                    self.OnGetInfoTrackers()
        except Exception as er:
            showError("OnGetMoreLabels", str(er), str(traceback.format_exc()))
        finally:
            self.btnViewLabel.setEnabled(True)

    @pyqtSlot()
    def OnAddLabel(self):
        try:
            if self.comboType.currentText() == 'TTL/TTU':
                if self.comboTypeSens.currentText() == 'TTU':
                    if self.TestPin():
                        return
                    if self.comboAddCell.currentText() == "" or int(self.comboAddCell.currentText()) < 0:
                        showInfo("OnAddLabel", "Неправильное значение ячейки метки", "Задайте правильное значение ячейки метки")
                        return
                    if self.comboAddLabel.currentText() == "" or int(self.comboAddLabel.currentText()) < 0:
                        showInfo("OnAddLabel", "Неправильное значение метки", "Задайте правильное значение метки")
                        return

                    self.btnAddLabel.setEnabled(False)
                    start = self.prefixCmd + self.pinCode.text() + ","
                    selected = self.tableTrack.selectionModel().selectedRows()
                    if len(selected) == 0:
                        showError("OnAddLabel", "Нет трекера выделенного для указанной операции!", "Выделите трекер в таблице трекеров")
                        return
                    for ls in selected:
                        select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                        if len(select_identifier) == 4:
                            nameproto = self.protocolInfo[self.comboType.currentText()]
                            if self.comboClient.currentText() == 'diller':
                                idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                                nameclient = self.comboDiler.currentText()
                            else:
                                idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                                nameclient = self.comboClient.currentText()
                            self.lineParam.appendPlainText("OnViewLabel: Get all Labels from " + select_identifier)
                            idcomm = 216
                            id_chain = int(time.time())

                            label_value = []
                            for k in range(self.tableLabel.rowCount()):
                                tval = self.tableLabel.item(k, 1).text()
                                label_value.append(tval)

                            cell = []
                            labels = []
                            for k in range(self.tableNewLabel.rowCount()):
                                tcell = self.tableNewLabel.item(k, 1).text()
                                tlabel = self.tableNewLabel.item(k, 2).text()
                                cell.append(tcell)
                                labels.append(tlabel)

                            comboCell = self.comboAddCell.currentText()
                            try:
                                indxc = cell.index(comboCell)
                                self.tableNewLabel.selectRow(indxc)
                            except Exception as er:
                                indxc = -1

                            if indxc != -1:
                                if showInfo("OnAddLabel", "Значение ячейки метки совпадает, продолжить?", "Значение ячейки метки совпадает с уже имеющимися значениями в таблицe Отправленных меток") != self.OK_MSG:
                                    return
                            try:
                                indxv = label_value.index(comboCell)
                                self.tableLabel.selectRow(indxv)
                            except Exception as er:
                                indxv = -1

                            if indxv != -1:
                                if showInfo("OnAddLabel", "Значение ячейки метки совпадает, продолжить?", "Значение ячейки метки совпадает с уже имеющимися значениями в таблицe Полученных меток") != self.OK_MSG:
                                    return

                            comboLabel = self.comboAddLabel.currentText()
                            try:
                                indxc = labels.index(comboLabel)
                                self.tableNewLabel.selectRow(indxc)
                            except Exception as er:
                                indxc = -1

                            if indxc != -1:
                                if showInfo("OnAddLabel", "Значение метки совпадает, продолжить?", "Значение метки совпадает с уже имеющимися значениями в таблице Отправленных меток") != self.OK_MSG:
                                    return

                            try:
                                indxv = label_value.index(comboLabel)
                                self.tableLabel.selectRow(indxv)
                            except Exception as er:
                                indxv = -1

                            if indxv != -1:
                                if showInfo("OnAddLabel", "Значение метки совпадает, продолжить?", "Значение метки совпадает с уже имеющимися значениями в таблице Полученных меток") != self.OK_MSG:
                                    return

                            commanda = [start + "SLEGAL=" + self.comboAddCell.currentText() + ":" +
                                        self.comboAddLabel.currentText() + ":" + self.comboMarkerEnd.currentText() + ","]
                            self.SendStringToPort(commanda, select_identifier, nameclient, idclient, nameproto, 2,
                                                  "write", "Labels", idcomm, id_chain, self.pinCode.text(), self.comboTypeSens.currentText())
                            rowPosition = self.tableNewLabel.rowCount()
                            self.tableNewLabel.insertRow(rowPosition)
                            self.tableNewLabel.setItem(rowPosition, 0, QTableWidgetItem("Metka_" + str(self.currentIndex)))
                            self.tableNewLabel.setItem(rowPosition, 1, QTableWidgetItem(self.comboAddCell.currentText()))
                            self.tableNewLabel.setItem(rowPosition, 2, QTableWidgetItem(self.comboAddLabel.currentText()))
                            self.tableNewLabel.resizeColumnsToContents()
                            self.tableNewLabel.resizeRowsToContents()
                            self.currentIndex += 1
                            value = self.comboAddLabel.currentText()
                            index = self.comboAddLabel.findText(value, QtCore.Qt.MatchFixedString)
                            if index == -1:
                                self.comboAddLabel.addItem(value)
                            value = self.comboAddCell.currentText()
                            index = self.comboAddCell.findText(value, QtCore.Qt.MatchFixedString)
                            if index == -1:
                                self.comboAddCell.addItem(value)
                        else:
                            showError("OnAddLabel", "Размер ID трекера не соотвествует типу teletrack",
                                    "Длина должна быть 4 символа")
                    self.btnClearTableNew.setEnabled(True)
                    self.OnGetInfoTrackers()
        except Exception as er:
            showError("OnAddLabel", str(er), str(traceback.format_exc()))
        finally:
            self.btnAddLabel.setEnabled(True)

    @pyqtSlot()
    def OnChoiseNumberCell(self):
        pass

    @pyqtSlot()
    def OnChoiseNumberLabel(self):
        pass

    @pyqtSlot()
    def OnChoiseMarkerEnd(self):
        pass

    @pyqtSlot()
    def viewClickedNewLabel(self):
        pass

    @pyqtSlot()
    def on_addr_change(self):
        if self.listMemo.currentItem().text() == 'RFID 1':
            self.classRfid1.Address = int(self.spinAddrRfid.value())
        elif self.listMemo.currentItem().text() == 'RFID 2':
            self.classRfid2.Address = int(self.spinAddrRfid.value())
        return

    @pyqtSlot()
    def on_stbit_change(self):
        if self.listMemo.currentItem().text() == 'RFID 1':
            self.classRfid1.StartBit = int(self.spinStartBitRfid.value())
        elif self.listMemo.currentItem().text() == 'RFID 2':
            self.classRfid2.StartBit = int(self.spinStartBitRfid.value())
        return

    @pyqtSlot()
    def on_len_change(self):
        if self.listMemo.currentItem().text() == 'RFID 1':
            self.classRfid1.Length = int(self.spinLenRfid.value())
        elif self.listMemo.currentItem().text() == 'RFID 2':
            self.classRfid2.Length = int(self.spinLenRfid.value())
        return

    @pyqtSlot()
    def on_address_change(self):
        if self.listMemo.currentItem().text() == 'FUEL 1':
            self.classFuel1.Address = int(self.pinAddress.value())
        elif self.listMemo.currentItem().text() == 'FUEL 2':
            self.classFuel2.Address = int(self.pinAddress.value())
        elif self.listMemo.currentItem().text() == 'FUEL 3':
            self.classFuel3.Address = int(self.pinAddress.value())
        elif self.listMemo.currentItem().text() == 'FUEL 4':
            self.classFuel4.Address = int(self.pinAddress.value())
        elif self.listMemo.currentItem().text() == 'FUEL 5':
            self.classFuel5.Address = int(self.pinAddress.value())
        return

    @pyqtSlot()
    def on_startbit_change(self):
        if self.listMemo.currentItem().text() == 'FUEL 1':
            self.classFuel1.StartBit = int(self.spinStartBit.value())
        elif self.listMemo.currentItem().text() == 'FUEL 2':
            self.classFuel2.StartBit = int(self.spinStartBit.value())
        elif self.listMemo.currentItem().text() == 'FUEL 3':
            self.classFuel3.StartBit = int(self.spinStartBit.value())
        elif self.listMemo.currentItem().text() == 'FUEL 4':
            self.classFuel4.StartBit = int(self.spinStartBit.value())
        elif self.listMemo.currentItem().text() == 'FUEL 5':
            self.classFuel5.StartBit = int(self.spinStartBit.value())
        return

    @pyqtSlot()
    def on_length_change(self):
        if self.listMemo.currentItem().text() == 'FUEL 1':
            self.classFuel1.Length = int(self.spin_Length.value())
        elif self.listMemo.currentItem().text() == 'FUEL 2':
            self.classFuel2.Length = int(self.spin_Length.value())
        elif self.listMemo.currentItem().text() == 'FUEL 3':
            self.classFuel3.Length = int(self.spin_Length.value())
        elif self.listMemo.currentItem().text() == 'FUEL 4':
            self.classFuel4.Length = int(self.spin_Length.value())
        elif self.listMemo.currentItem().text() == 'FUEL 5':
            self.classFuel5.Length = int(self.spin_Length.value())
        return

    @pyqtSlot()
    def on_average_change(self):
        if self.listMemo.currentItem().text() == 'FUEL 1':
            self.classFuel1.Average = int(self.spinAverage.value())
        elif self.listMemo.currentItem().text() == 'FUEL 2':
            self.classFuel2.Average = int(self.spinAverage.value())
        elif self.listMemo.currentItem().text() == 'FUEL 3':
            self.classFuel3.Average = int(self.spinAverage.value())
        elif self.listMemo.currentItem().text() == 'FUEL 4':
            self.classFuel4.Average = int(self.spinAverage.value())
        elif self.listMemo.currentItem().text() == 'FUEL 5':
            self.classFuel5.Average = int(self.spinAverage.value())
        return

    @pyqtSlot()
    def setFilterTracker(self):
        textfilt = self.lineFilter.text()

        if textfilt == '':
            for i in range(self.tableTrack.rowCount()):
                self.tableTrack.setRowHidden(i, False)
            return

        for i in range(self.tableTrack.rowCount()):
            item = self.tableTrack.item(i, 1)
            if item:
                text = item.text()
                try:
                    text.index(textfilt)
                    self.tableTrack.setRowHidden(i, False)
                except Exception as ex:
                    self.tableTrack.setRowHidden(i, True)
        rowCount = 0
        for i in range(self.tableTrack.rowCount()):
            if not self.tableTrack.isRowHidden(i):
                rowCount += 1
        return

    def writeDataToTableRegim(self, info, table):
        if info["Type"] == 'TTA':
            self.spacClearTable(table)
            table.setRowCount(len(self.classRegim.NameRegim))
            for k in range(len(self.classRegim.NameRegim)):
                name = self.classRegim.NameRegim[k]
                qtbl = QTableWidgetItem(name)
                table.setItem(k, 0, qtbl)
                name = info["regimparam"]["regimPoint"][str(k)]
                qtbl = QTableWidgetItem(name)
                table.setItem(k, 1, qtbl)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            namecli = self.comboClient.currentText()
            if namecli == 'diller':
                namecli = self.comboDiler.currentText()
            self.tabs.setTabText(1, "GPRS (" + info["Device"] + "," + namecli + ")")
        return

    def writeDataToTableServer(self, info, table):
        if info["Type"] == 'TTA' or info["Type"] == 'TTL/TTU':
            self.spacClearTable(table)
            table.setRowCount(len(info["serverparam"]["serverPoint"]))
            for key in info["serverparam"]["serverPoint"]:
                self.tableTelematicServer.setItem(int(key), 0,
                                                  QTableWidgetItem(self.teleServer.TelematicServer[int(key)]))
                self.tableTelematicServer.setItem(int(key), 1,
                                                  QTableWidgetItem(info["serverparam"]["serverPoint"][key]))
            self.tableEvents.resizeColumnsToContents()
            self.tableEvents.resizeRowsToContents()
            namecli = self.comboClient.currentText()
            if namecli == 'diller':
                namecli = self.comboDiler.currentText()
            self.tabs.setTabText(2, "Server (" + info["Device"] + "," + namecli + ")")
        return

    def writeDataToTabTextCmd(self, info, linecmd, linerqst):
        if info["Type"] == 'TTA' or info["Type"] == 'TTL/TTU':
            linecmd.clear()
            linecmd.appendPlainText(info["textcmdparam"]["paramCmd"]['0'])
            linerqst.clear()
            linerqst.appendPlainText(info["textcmdparam"]["paramRqst"]['0'])
            namecli = self.comboClient.currentText()
            if namecli == 'diller':
                namecli = self.comboDiler.currentText()
            self.tabs.setTabText(4, "TextCmd (" + info["Device"] + "," + namecli + ")")
        return

    def writeDataToTabGps(self, info, textedit):
        if info["Type"] == 'TTA' or info["Type"] == 'TTL/TTU':
            textedit.clear()
            textedit.appendPlainText(info["textcmdparam"]["paramCmd"]['0'])
            namecli = self.comboClient.currentText()
            if namecli == 'diller':
                namecli = self.comboDiler.currentText()
            self.tabs.setTabText(5, "GPS (" + info["Device"] + "," + namecli + ")")
        return

    def writeDataToTabSensors(self, info, tabSens):
        pass

    def writeDataToTabService(self, info, tServPhone):
        if info["Type"] == 'TTL/TTU':
            simType = info["service"]["simtype"]
            self.switchBtn.setChecked(simType)
            if simType:
                self.switchBtn.setText("SIM2")
            else:
                self.switchBtn.setText("SIM1")

            self.spacClearTable(tServPhone)
            tServPhone.setRowCount(len(info["service"]["phoneSet"]))
            if simType:
                for key in info["service"]["phoneSet"]:
                    tServPhone.setItem(int(key), 0, QTableWidgetItem(self.srvPhoneP.ServicePhone[int(key)]))
                    tServPhone.setItem(int(key), 1, QTableWidgetItem(info["service"]["phoneSet"][key]))
                self.srvPhoneP.LineReq = info["service"]["regimSet"]
                self.lineReg.setText(self.srvPhoneP.LineReq)
            else:
                for key in info["service"]["phoneSet"]:
                    tServPhone.setItem(int(key), 0, QTableWidgetItem(self.srvPhone.ServicePhone[int(key)]))
                    tServPhone.setItem(int(key), 1, QTableWidgetItem(info["service"]["phoneSet"][key]))
                self.srvPhone.LineReq = info["service"]["regimSet"]
                self.lineReg.setText(self.srvPhone.LineReq)
            tServPhone.resizeColumnsToContents()
            tServPhone.resizeRowsToContents()
        return

    def writeDataToTabTransport(self, info, tServSett, tGprs):
        if info["Type"] == 'TTL/TTU':
            simType = info["transport"]["simtype"]
            self.switchBtn.setChecked(simType)
            if simType:
                self.switchBtn.setText("SIM2")
            else:
                self.switchBtn.setText("SIM1")

            self.spacClearTable(tServSett)
            tServSett.setRowCount(len(info["transport"]["servSet"]))
            for key in info["transport"]["servSet"]:
                tServSett.setItem(int(key), 0, QTableWidgetItem(self.classServSett.ServerSettn[int(key)]))
                tServSett.setItem(int(key), 1, QTableWidgetItem(info["transport"]["servSet"][key]))
            tServSett.resizeColumnsToContents()
            tServSett.resizeRowsToContents()

            if simType:
                self.spacClearTable(tGprs)
                tGprs.setRowCount(len(info["transport"]["gprsSet"]))
                for key in info["transport"]["gprsSet"]:
                    tGprs.setItem(int(key), 0, QTableWidgetItem(self.classGprsP.GprsSetting[int(key)]))
                    tGprs.setItem(int(key), 1, QTableWidgetItem(info["transport"]["gprsSet"][key]))
            else:
                self.spacClearTable(tGprs)
                tGprs.setRowCount(len(info["transport"]["gprsSet"]))
                for key in info["transport"]["gprsSet"]:
                    tGprs.setItem(int(key), 0, QTableWidgetItem(self.classGprs.GprsSetting[int(key)]))
                    tGprs.setItem(int(key), 1, QTableWidgetItem(info["transport"]["gprsSet"][key]))
            tGprs.resizeColumnsToContents()
            tGprs.resizeRowsToContents()
            namecli = self.comboClient.currentText()
            if namecli == 'diller':
                namecli = self.comboDiler.currentText()
            self.tabs.setTabText(6, "Transport (" + info["Device"] + "," + namecli + ")")
        else:
            pass
        return

    def writeDataToTabPhones(self, info, tablephoneparam, tablephone):
        if info["Type"] == 'TTA' or info["Type"] == 'TTL/TTU':
            self.spacClearTable(tablephoneparam)
            tablephoneparam.setRowCount(len(info["phonesparam"]["paramPhones"]))
            for key in info["phonesparam"]["paramPhones"]:
                tablephoneparam.setItem(int(key), 0, QTableWidgetItem(self.classPhoneParam.PhonesParam[int(key)]))
                tablephoneparam.setItem(int(key), 1, QTableWidgetItem(info["phonesparam"]["paramPhones"][key]))
            tablephoneparam.resizeColumnsToContents()
            tablephoneparam.resizeRowsToContents()

            self.spacClearTable(tablephone)
            tablephone.setRowCount(len(info["phonesparam"]["phonePhones"]))
            for key in info["phonesparam"]["phonePhones"]:
                tablephone.setItem(int(key), 0, QTableWidgetItem(self.classPhones.PhonesParam[int(key)]))
                tablephone.setItem(int(key), 1, QTableWidgetItem(info["phonesparam"]["phonePhones"][key]))
            tablephone.resizeColumnsToContents()
            tablephone.resizeRowsToContents()
            namecli = self.comboClient.currentText()
            if namecli == 'diller':
                namecli = self.comboDiler.currentText()
            self.tabs.setTabText(3, "Phones (" + info["Device"] + "," + namecli + ")")
        return

    def writeDataToTableEvents(self, info, table):
        if info["Type"] == 'TTA':
            self.spacClearTable(table)
            table.setColumnHidden(5, False)
            table.setColumnHidden(7, False)
            table.setRowCount(len(self.eventsTta.ParamName))
            for k in range(len(self.eventsTta.ParamName)):
                name = self.eventsTta.ParamName[k]
                qtbl = QTableWidgetItem(name)
                self.tableEvents.setItem(k, 0, qtbl)
                param = self.eventsTta.ParamValue[k]
                qtbl = QTableWidgetItem(param)
                self.tableEvents.setItem(k, 1, qtbl)
                unit = self.eventsTta.Unit[k]
                qtbl = QTableWidgetItem(unit)
                self.tableEvents.setItem(k, 2, qtbl)
                self.tableEvents.setCellWidget(k, 3, self.initTableWidgetCall(QCheckBox(),
                                                                              info["parameters"]["fixingPoint"][
                                                                                  str(k)]))
                self.tableEvents.setCellWidget(k, 4, self.initTableWidgetCall(QCheckBox(),
                                                                              info["parameters"]["sentServer"][str(k)]))
                self.tableEvents.setCellWidget(k, 5, self.initTableWidgetCall(QCheckBox(),
                                                                              info["parameters"]["smsPhone"][str(k)]))
                name = self.eventsTta.RecommendedValue[k]
                qtbl = QTableWidgetItem(name)
                self.tableEvents.setItem(k, 6, qtbl)
                name = self.eventsTta.ValidValues[k]
                qtbl = QTableWidgetItem(name)
                self.tableEvents.setItem(k, 7, qtbl)
            self.tableEvents.resizeColumnsToContents()
            self.tableEvents.resizeRowsToContents()
            namecli = self.comboClient.currentText()
            if namecli == 'diller':
                namecli = self.comboDiler.currentText()
            self.tabs.setTabText(0, "Events (" + info["Device"] + "," + namecli + ")")
        elif self.previouseTypeTracker == 'TTL/TTU':
            simType = info["parameters"]["simtype"]
            self.switchBtn.setChecked(simType)
            if simType:
                self.switchBtn.setText("SIM2")
            else:
                self.switchBtn.setText("SIM1")
            if simType:
                self.spacClearTable(table)
                self.tableEvents.setColumnHidden(5, True)
                self.tableEvents.setColumnHidden(7, True)
                self.tableEvents.setRowCount(len(self.eventsTtlu.uParamName))
                for k in range(len(self.eventsTtlu.uParamName)):
                    name = self.eventsTtlu.uParamName[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableEvents.setItem(k, 0, qtbl)
                    param = self.eventsTtlu.uParamValue[k]
                    qtbl = QTableWidgetItem(param)
                    self.tableEvents.setItem(k, 1, qtbl)
                    unit = self.eventsTtlu.uUnit[k]
                    qtbl = QTableWidgetItem(unit)
                    self.tableEvents.setItem(k, 2, qtbl)
                    self.tableEvents.setCellWidget(k, 3, self.initTableWidgetCall(QCheckBox(),
                                                                                  info["parameters"]["fixingPoint"][
                                                                                      str(k)]))
                    self.tableEvents.setCellWidget(k, 4, self.initTableWidgetCall(QCheckBox(),
                                                                                  info["parameters"]["sentServer"][
                                                                                      str(k)]))
                    self.tableEvents.setCellWidget(k, 5, self.initTableWidgetCall(QCheckBox(), False))
                    name = self.eventsTtlu.uRecommendedValue[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableEvents.setItem(k, 6, qtbl)
                self.tableEvents.resizeColumnsToContents()
                self.tableEvents.resizeRowsToContents()
                namecli = self.comboClient.currentText()
                if namecli == 'diller':
                    namecli = self.comboDiler.currentText()
                self.tabs.setTabText(0, "Events (" + info["Device"] + "," + namecli + ")")
            else:
                self.spacClearTable(table)
                self.tableEvents.setColumnHidden(5, True)
                self.tableEvents.setColumnHidden(7, True)
                self.tableEvents.setRowCount(len(self.eventsTtl.ParamName))
                for k in range(len(self.eventsTtl.ParamName)):
                    name = self.eventsTtl.ParamName[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableEvents.setItem(k, 0, qtbl)
                    param = self.eventsTtl.ParamValue[k]
                    qtbl = QTableWidgetItem(param)
                    self.tableEvents.setItem(k, 1, qtbl)
                    unit = self.eventsTtl.Unit[k]
                    qtbl = QTableWidgetItem(unit)
                    self.tableEvents.setItem(k, 2, qtbl)
                    self.tableEvents.setCellWidget(k, 3, self.initTableWidgetCall(QCheckBox(),
                                                                                  info["parameters"]["fixingPoint"][
                                                                                      str(k)]))
                    self.tableEvents.setCellWidget(k, 4, self.initTableWidgetCall(QCheckBox(),
                                                                                  info["parameters"]["sentServer"][
                                                                                      str(k)]))
                    self.tableEvents.setCellWidget(k, 5, self.initTableWidgetCall(QCheckBox(), False))
                    name = self.eventsTtl.RecommendedValue[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableEvents.setItem(k, 6, qtbl)
                self.tableEvents.resizeColumnsToContents()
                self.tableEvents.resizeRowsToContents()
                namecli = self.comboClient.currentText()
                if namecli == 'diller':
                    namecli = self.comboDiler.currentText()
                self.tabs.setTabText(0, "Events (" + info["Device"] + "," + namecli + ")")
        return

    def unpackDataResponse(self, command, curdevice):
        start = time.time()
        if self.comboType.currentText() == 'TTA':
            result = self.commandA1.timerThreadRx_Tick(command["dataResponse"], curdevice)
            if result == False:
                showError("unpackDataResponse", str("Error unpacking responses command"),
                          "Not correcting data resource command")
                return False
            self.commandA1.setTableEvents(self.tableEvents, self.initTableWidgetCall, QCheckBox,
                                          QTableWidgetItem)  # unpack command Events
            strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                             time.localtime(int(time.time())))) + "] " + curdevice + \
                     ", current response command unpacking and Events table is filling"
            self.lineParam.appendPlainText(strcmd)
        elif self.comboType.currentText() == 'TTL/TTU':
            requestCmd = []
            mongodb = sshtunel.getMongoDb()
            collection = mongodb.get_collection('commandData')
            result = list(collection.find({"Device": curdevice, "IDChain": command["IDChain"]}))
            sshtunel.closeMongoDb()
            for cmd in result:
                requestCmd.append(cmd["dataResponse"])
            self.pinCode.setText(result[0]["PinCode"])
            if command["TypeSens"] == 'TTL':
                res, err = self.commandA1.SetEventsTtl(requestCmd, self.tableEvents, QTableWidgetItem)
                if type(result[0]["TypeSim"]) != str:
                    if result[0]["TypeSim"] == True:
                        self.switchBtn.setChecked(True)
                        self.switchBtn.setText("SIM2")
                    else:
                        self.switchBtn.setChecked(False)
                        self.switchBtn.setText("SIM1")
            elif command["TypeSens"] == 'TTU':
                res, err = self.commandA1.SetEventsTtu(requestCmd, self.tableEvents, QTableWidgetItem)
            if res == False:
                showError("unpackDataResponse", str("Error unpacking respons commands"), err)
                return False
            end = time.time()
            strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                             time.localtime(int(time.time())))) + "] " + curdevice + \
                     ", current response command(s) unpacking and Events table is filling, sec:" + str(
                round(end - start, 4))
            self.lineParam.appendPlainText(strcmd)
        return True

    def unpackDataResponseServer(self, command, curdevice):
        result = self.commandA1.timerThreadRx_Tick(command, curdevice)
        if result == False:
            showError("unpackDataResponseServer", str("Error unpacking responses command"),
                      "Not correcting data resource command")
            return False
        self.commandA1.setTableServer(self.tableTelematicServer, QTableWidgetItem)  # unpack command Server
        strcmd = "[" + str(
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + curdevice + \
                 ", current response command unpacking and Server table is filling"
        self.lineParam.appendPlainText(strcmd)
        return True

    def unpackDataResponsePhones(self, command, curdevice):
        result = self.commandA1.timerThreadRx_Tick(command, curdevice)
        if result == False:
            showError("unpackDataResponsePhone", str("Error unpacking responses command"),
                      "Not correcting data resource command")
            return False
        self.commandA1.setTablePhones(self.tablePhonesParam, QTableWidgetItem)  # unpack command Server
        strcmd = "[" + str(
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + curdevice + \
                 ", current response command unpacking and Phones Parameter table is filling"
        self.lineParam.appendPlainText(strcmd)
        return True

    def unpackDataService(self, command, curdevice):
        start = time.time()
        if self.comboType.currentText() == 'TTL/TTU':
            if self.switchBtn.isChecked():
                simUse = 1
            else:
                simUse = 0

            requestCmd = []
            mongodb = sshtunel.getMongoDb()
            collection = mongodb.get_collection('commandData')
            result = list(collection.find({"Device": curdevice, "IDChain": command["IDChain"]}))
            sshtunel.closeMongoDb()
            for cmd in result:
                requestCmd.append(cmd["dataResponse"])
            self.pinCode.setText(result[0]["PinCode"])
            if self.comboTypeSens.currentText() == 'TTL':
                res, err = self.commandA1.SetServiceTtl(requestCmd, self.tableServPhone, self.lineReg, simUse, QTableWidgetItem)
                if type(result[0]["TypeSim"]) != str:
                    if result[0]["TypeSim"] == True:
                        self.switchBtn.setChecked(True)
                        self.switchBtn.setText("SIM2")
                    else:
                        self.switchBtn.setChecked(False)
                        self.switchBtn.setText("SIM1")
            elif self.comboTypeSens.currentText() == 'TTU':
                res, err = self.commandA1.SetServiceTtu(requestCmd, self.tableServPhone, self.lineReg, QTableWidgetItem)
            if res == False:
                showError("unpackDataService", str("Error unpacking respons commands"), err)
                return False
            end = time.time()
            strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + curdevice + \
                     ", current response command(s) tracker " + str(curdevice) + " unpacking and Service table is filling, sec:" + \
                     str(round(end - start, 4))
            self.lineParam.appendPlainText(strcmd)
        return

    def unpackDataTransport(self, command, curdevice):
        start = time.time()
        if self.comboType.currentText() == 'TTL/TTU':
            if self.switchBtn.isChecked():
                simUse = 1
            else:
                simUse = 0

            requestCmd = []
            mongodb = sshtunel.getMongoDb()
            collection = mongodb.get_collection('commandData')
            result = list(collection.find({"Device": curdevice, "IDChain": command["IDChain"]}))
            sshtunel.closeMongoDb()
            for cmd in result:
                requestCmd.append(cmd["dataResponse"])
            self.pinCode.setText(result[0]["PinCode"])
            if self.comboTypeSens.currentText() == 'TTL':
                res, err = self.commandA1.SetTransportTtl(requestCmd, self.tableServSett, self.tableGprs, simUse, QTableWidgetItem)
                if type(result[0]["TypeSim"]) != str:
                    if result[0]["TypeSim"] == True:
                        self.switchBtn.setChecked(True)
                        self.switchBtn.setText("SIM2")
                    else:
                        self.switchBtn.setChecked(False)
                        self.switchBtn.setText("SIM1")
            elif self.comboTypeSens.currentText() == 'TTU':
                res, err = self.commandA1.SetTransportTtu(requestCmd, self.tableServSett, self.tableGprs, QTableWidgetItem)
            if res == False:
                showError("unpackDataTransport", str("Error unpacking respons commands"), err)
                return False
            end = time.time()
            strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                             time.localtime(int(time.time())))) + "] " + curdevice + \
                     ", current response command(s) tracker " + str(
                curdevice) + " unpacking and Tranports table is filling, sec:" + \
                     str(round(end - start, 4))
            self.lineParam.appendPlainText(strcmd)
        return

    def unpackDataTextCmd(self, command, curdevice):
        result = True
        if result == False:
            showError("unpackDataTextCmd", str("Error unpacking responses command"),
                      "Not correcting data resource command")
            return False
        self.commandA1.setTabTextCmd(self.lineTxtRequest)  # unpack command Server
        strcmd = "[" + str(
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + curdevice + \
                 ", current response command unpacking and TextCmd Parameter table is filling tracker " + curdevice
        self.lineParam.appendPlainText(strcmd)
        return True

    def unpackDataGPS(self, command, curdevice):
        result = True
        if result == False:
            showError("unpackDataGPS", str("Error unpacking responses command"),
                      "Not correcting data resource command")
            return False
        self.commandA1.setTabGps(self.text_edit)  # unpack command Server
        strcmd = "[" + str(
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + curdevice+ \
                 ", current response command unpacking and GPS Parameter table is filling tracker " + curdevice
        self.lineParam.appendPlainText(strcmd)
        return True

    def unpackDataResponseGprs(self, command, curdevice):
        result = self.commandA1.timerThreadRx_Tick(command, curdevice)
        if result == False:
            showError("unpackDataResponseGprs", str("Error unpacking responses command"),
                      "Not correcting data resource command")
            return False
        self.commandA1.setTableGprs(curdevice, self.tableRegim, QTableWidgetItem)  # unpack command GPRS
        strcmd = "[" + str(
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + curdevice + \
                 ", current response command unpacking and GPRS APN table is filling"
        self.lineParam.appendPlainText(strcmd)
        return True
    
    def unpackDataLabels(self, command, curdevice):
        starts = time.time()
        if self.comboType.currentText() == 'TTL/TTU':
            if self.comboTypeSens.currentText() == 'TTU':
                if self.currentIDChain == command["IDChain"]:
                    ends = time.time()
                    strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                                     time.localtime(
                                                         int(time.time())))) + "] " + curdevice + \
                             ", has already current response command(s) unpacking and Labels table is filling, last Label: " + self.lastLabel + \
                             ", current index:" + str(self.currentIndex)
                    self.lineParam.appendPlainText(strcmd)
                    return
                requestCmd = []
                dateResponse = []
                mongodb = sshtunel.getMongoDb()
                collection = mongodb.get_collection('commandData')
                result = list(collection.find({"Device": curdevice, "IDChain": command["IDChain"]}))
                sshtunel.closeMongoDb()
                for cmd in result:
                    requestCmd.append(cmd["dataResponse"])
                    dateResponse.append(cmd["dateResponse"])
                self.pinCode.setText(result[0]["PinCode"])
                res, err, self.currentIndex = self.commandA1.UnpackLabelsTtu(requestCmd, dateResponse, self.tableLabel, self.labelDateLabel, self.labelBeginIndex,
                                                          self.labelMaskLabel, self.labelEndMarker, self.btnGetLabel, self.labelLastDateResponse, QTableWidgetItem)
                if res == False:
                    showError("unpackDataLabels", str("Error unpacking respons commands"), err)
                    return False
                self.lastLabel = err
                self.comboAddLabel.setEnabled(True)
                self.comboAddCell.setEnabled(True)
                self.comboMarkerEnd.setEnabled(True)
                self.btnAddLabel.setEnabled(True)
                self.btnClearTableView.setEnabled(True)
                self.currentIDChain = command["IDChain"]
                ends = time.time()
                strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                                 time.localtime(int(time.time())))) + "] " + curdevice + \
                         ", current response command(s) unpacking and Labels table is filling, last Label: " + self.lastLabel + \
                         ", current index:" + str(self.currentIndex) + ", sec:" + str(round(ends - starts, 4))
                self.lineParam.appendPlainText(strcmd)
        return

    def unpackDataViewer(self, command, curdevice):
        start = time.time()
        if self.comboType.currentText() == 'TTL/TTU':
            if self.comboTypeSens.currentText() == 'TTU':
                requestCmd = []
                mongodb = sshtunel.getMongoDb()
                collection = mongodb.get_collection('commandData')
                result = list(collection.find({"Device": curdevice, "IDChain": command["IDChain"]}))
                sshtunel.closeMongoDb()
                for cmd in result:
                    requestCmd.append(cmd["dataResponse"])
                self.pinCode.setText(result[0]["PinCode"])
                res, err = self.commandA1.UnpackRequest(self.currentIndex, requestCmd, self.lineRssi, self.lineSatt, self.lineVbort,
                                                        self.tableSensViewer, self.tableTypeLls, QTableWidgetItem)
                if res == False:
                    showError("unpackDataViewer", str("Error unpacking respons commands"), err)
                    return False
                end = time.time()
                strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                                 time.localtime(int(time.time())))) + "] " + curdevice + \
                         ", current response command(s) unpacking and Viewer is filling, sec:" + str(round(end - start, 4))
                self.lineParam.appendPlainText(strcmd)
        return

    def unpackDataTerminal(self, command, curdevice):
        start = time.time()
        if self.comboType.currentText() == 'TTL/TTU':
            requestCmd = []
            mongodb = sshtunel.getMongoDb()
            collection = mongodb.get_collection('commandData')
            result = list(collection.find({"Device": curdevice, "IDChain": command["IDChain"]}))
            sshtunel.closeMongoDb()
            for cmd in result:
                requestCmd.append(cmd["dataResponse"])
            self.pinCode.setText(result[0]["PinCode"])
            res, err = self.commandA1.SetTerminal(requestCmd, self.text_term)
            if res == False:
                showError("unpackDataTerminal", str("Error unpacking respons commands"), err)
                return False
            end = time.time()
            strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                             time.localtime(int(time.time())))) + "] " + curdevice + \
                     ", current response command(s) unpacking and Terminal is filling, sec:" + str(
                round(end - start, 4))
            self.lineParam.appendPlainText(strcmd)
        return

    def unpackDataSensors(self, command, curdevice):
        start = time.time()
        if self.comboType.currentText() == 'TTL/TTU':
            requestCmd = []
            mongodb = sshtunel.getMongoDb()
            collection = mongodb.get_collection('commandData')
            result = list(collection.find({"Device": curdevice, "IDChain": command["IDChain"]}))
            sshtunel.closeMongoDb()
            notresponse = []
            for cmd in result:
                if len(cmd["dataResponse"]) > 0:
                    requestCmd.append(cmd["dataResponse"])
                else:
                    notresponse.append([str(cmd["_id"]), str(cmd["Command"])])
            self.pinCode.setText(result[0]["PinCode"])
            for commd in notresponse:
                strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(int(time.time())))) + "] " + curdevice + \
                         ", команда:" + str(commd[1]) + " ID: " + str(commd[0]) + " не содержит ответную информацию!"
                self.lineParam.appendPlainText(strcmd)

            if len(notresponse) > 0:
                msg_ok = showInfo("unpackDataSensors", "Некоторые команды не содержат ответ, ответные данные могут быть не полными", "Для деталей смотри окно ниже")
            else:
                msg_ok = self.OK_MSG

            if msg_ok == self.OK_MSG:
                sensParam = TObjectSensors(self)
                for index in range(self.listMemo.count()):
                    check_box = self.listMemo.itemWidget(self.listMemo.item(index))
                    sensParam.checkedListBoxSensors.append(check_box)
                if self.comboTypeSens.currentText() == 'TTL':
                    res, err = self.commandA1.SetSensorsTtl(requestCmd, sensParam)
                elif self.comboTypeSens.currentText() == 'TTU':
                    res, err = self.commandA1.SetSensorsTtu(requestCmd, sensParam)
                if res == False:
                    showError("unpackDataSensors", str("Error unpacking respons commands"), err)
                    return False
                end = time.time()
                strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                                 time.localtime(int(time.time())))) + "] " + curdevice + \
                         ", current response command(s) unpacking and Sensors tab is filling, sec:" + str(round(end - start, 4))
                self.lineParam.appendPlainText(strcmd)
        return

    @pyqtSlot()
    def viewClickedTrack(self):
        start = time.time()
        pp = None
        try:
            selected = self.tableTrack.selectionModel().selectedRows()
            selected_identifier = str(self.tableTrack.item(selected[len(selected) - 1].row(), 0).text())
            selected_type = str(self.tableTrack.item(selected[len(selected) - 1].row(), 4).text())

            if len(selected_identifier) != 4:
                showError("viewClickedTrack", "Размер ID трекера не соотвествует типу teletrack",
                          "Длина должна быть 4 символа")
                return

            if self.comboType.currentText() == 'TTL/TTU':
                if not selected_type in ['TTL', 'TTU']:
                    showError("viewClickedTrack", "Плата датчиков или тип трекера не соотвутсвует протоколу teletrack64",
                              "Разрешено только TTL или TTU")
                    return
            elif self.comboType.currentText() == 'TTA':
                if selected_type != 'TTA':
                    showError("viewClickedTrack",
                              "Плата датчиков или тип трекера не соотвутсвует протоколу teletrack32",
                              "Разрешено только TTA")
                    return

            self.comboTypeSens.setCurrentText(selected_type)

            showWarn("Start", "viewClickedTrack", "Внимание! Обрабатывается трекер " + selected_identifier, self.statusBar)

            self.btnSavingInfo.setEnabled(True)
            self.btnReadParam.setEnabled(True)
            self.btnWriteParam.setEnabled(True)
            self.btnDeleteCom.setEnabled(True)
            self.btnDeleteAllCom.setEnabled(True)

            self.initReadWriteMenu()

            if self.tabs.currentIndex() == 9 or self.tabs.currentIndex() == 10 or self.tabs.currentIndex() == 11: # Terminal or Viewer or Labels
                self.btnReadParam.setEnabled(False)
                self.btnWriteParam.setEnabled(False)

            if self.comboType.currentText() == 'TTL/TTU':
                self.btnReset.setEnabled(True)
                self.switchBtn.setEnabled(True)
                self.btnSendTerm.setEnabled(True)
                self.btnClearTerm.setEnabled(True)
                self.comboTerminal.setEnabled(True)

            self.lineParam.appendPlainText(
                "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + \
                "] " + "selected: " + selected_identifier)

            if self.tabs.currentIndex() == 2:  # Server
                identifier = self.tableTrack.item(selected[len(selected) - 1].row(), 0).text()
                self.tableTelematicServer.setItem(1, 1, QTableWidgetItem(str(identifier)))
                password = self.commandA1.generatePassw(identifier)
                self.tableTelematicServer.setItem(2, 1, QTableWidgetItem(password))
                self.tabs.setTabText(2, "Server (default)")

            if self.tabs.currentIndex() == 6:  # Transport
                if self.comboType.currentText() == 'TTL/TTU':
                    identifier = self.tableTrack.item(selected[len(selected) - 1].row(), 0).text()
                    self.tableServSett.setItem(2, 1, QTableWidgetItem(str(identifier)))
                    password = self.commandA1.generatePassw(identifier)
                    self.tableServSett.setItem(3, 1, QTableWidgetItem(password))
                    self.tabs.setTabText(6, "Transport (default)")

            if self.tabs.currentIndex() == 0 or self.tabs.currentIndex() == 1 or self.tabs.currentIndex() == 2 or \
                    self.tabs.currentIndex() == 3 or self.tabs.currentIndex() == 4 or self.tabs.currentIndex() == 5 or \
                    self.tabs.currentIndex() == 6 or self.tabs.currentIndex() == 7 or self.tabs.currentIndex() == 8 or \
                    self.tabs.currentIndex() == 9 or self.tabs.currentIndex() == 10 or self.tabs.currentIndex() == 11:
                mainWidget = self.tableTrack.cellWidget(selected[len(selected) - 1].row(), 2)
                comboBoxComm = mainWidget.layout().itemAt(0).widget()
                comboText = comboBoxComm.currentText()
                namecli = self.comboClient.currentText()
                if namecli == 'diller':
                    namecli = self.comboDiler.currentText()
                if len(comboText) > 0:
                    textcomm = json.loads(comboText)
                    mongodb = sshtunel.getMongoDb()
                    collection = mongodb.get_collection('commandData')
                    command = list(collection.find({"Device": selected_identifier, "_id": bson.ObjectId(textcomm["id"])}))
                    chains = list(collection.find({"Device": selected_identifier, "IDChain": command[0]["IDChain"]}))
                    sshtunel.closeMongoDb()
                    del command[0]["_id"]
                    del command[0]["id_client"]
                    del command[0]["Comment"]
                    self.comboTypeSens.setCurrentText(command[0]["TypeSens"])
                    strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + json.dumps(command[0])
                    self.lineParam.appendPlainText(strcmd)
                    success = textcomm["status"] == 'success'
                    dones = textcomm["execute"] == 'done'
                    reads = textcomm["type"] == 'read'
                    responses = "response" in textcomm
                    namegroup = textcomm["name"]
                    if len(command[0]["dataResponse"]) > 0:
                        datarespons = "ответная информация есть"
                    else:
                        datarespons = "ответной информации нет"
                    if responses:
                        qresponses = "может содержать ответ"
                    else:
                        qresponses = "может не содержать ответ"
                    strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                             "Отмеченная команда - группа: " + namegroup + ", статус: " + textcomm["status"] + ", результат выполнения: " + textcomm["execute"] + \
                        ", тип: " + textcomm["type"] + ", " + qresponses + ", " + datarespons + ", число команд в цепочке: " + str(len(chains))
                    self.lineParam.appendPlainText(strcmd)

                    if success and dones and reads and responses:
                        if len(command[0]["dataResponse"]) > 0:
                            if command[0]["Name"] == "Events":
                                ret = self.unpackDataResponse(command[0], selected_identifier)
                                self.tabs.setTabText(0, "Events (" + selected_identifier + "," + namecli + ")*")
                            elif command[0]["Name"] == "GPRS":
                                ret = self.unpackDataResponseGprs(command[0]["dataResponse"], selected_identifier)
                                self.tabs.setTabText(1, "GPRS (" + selected_identifier + "," + namecli + ")*")
                            elif command[0]["Name"] == "Server":
                                ret = self.unpackDataResponseServer(command[0]["dataResponse"],
                                                                    selected_identifier)
                                self.tabs.setTabText(2, "Server (" + selected_identifier + "," + namecli + ")*")
                            elif command[0]["Name"] == "Phones":
                                ret = self.unpackDataResponsePhones(command[0]["dataResponse"],
                                                                    selected_identifier)
                                self.tabs.setTabText(3, "Phones (" + selected_identifier + "," + namecli + ")*")
                            elif command[0]["Name"] == "TextCmd":
                                ret = self.unpackDataTextCmd(command[0]["dataResponse"], selected_identifier)
                                self.tabs.setTabText(4, "TextCmd (" + selected_identifier + "," + namecli + ")*")
                            elif command[0]["Name"] == "GPS":
                                ret = self.unpackDataGPS(command[0]["dataResponse"], selected_identifier)
                                self.tabs.setTabText(5, "GPS (" + selected_identifier + "," + namecli + ")*")
                            elif command[0]["Name"] == "Transport":
                                ret = self.unpackDataTransport(command[0], selected_identifier)
                                self.tabs.setTabText(6, "Transport (" + selected_identifier + "," + namecli + ")*")
                            elif command[0]["Name"] == "Service":
                                ret = self.unpackDataService(command[0], selected_identifier)
                                self.tabs.setTabText(7, "Service (" + selected_identifier + "," + namecli + ")*")
                            elif command[0]["Name"] == "Sensors":
                                ret = self.unpackDataSensors(command[0], selected_identifier)
                                self.tabs.setTabText(8, "Sensors (" + selected_identifier + "," + namecli + ")*")
                            elif command[0]["Name"] == "Terminal":
                                ret = self.unpackDataTerminal(command[0], selected_identifier)
                                self.tabs.setTabText(9, "Terminal (" + selected_identifier + "," + namecli + ")*")
                            elif command[0]["Name"] == "Viewer":
                                ret = self.unpackDataViewer(command[0], selected_identifier)
                                self.tabs.setTabText(10, "Viewer (" + selected_identifier + "," + namecli + ")*")
                            elif command[0]["Name"] == "Labels":
                                ret = self.unpackDataLabels(command[0], selected_identifier)
                                self.tabs.setTabText(11, "Labels (" + selected_identifier + "," + namecli + ")*")
                            return
                        else:
                            strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                                     "Выбранная команда не содержит ответную информацию и не может быть обработана, выберите другую команду"
                            self.lineParam.appendPlainText(strcmd)
                    else:
                        strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                                 "Выбранная команда не может быть распакованой так как не соответствует необходимым условиям, выберите другую команду"
                        self.lineParam.appendPlainText(strcmd)

            mongodb = sshtunel.getMongoDb()
            collection = mongodb.get_collection('parametersTrackData')
            result = list(collection.find({"Device": selected_identifier}))
            sshtunel.closeMongoDb()

            if len(result) > 0:
                if "parameters" in result[0]:
                    self.writeDataToTableEvents(result[0], self.tableEvents)
                else:
                    self.initTableEvent(self.switchBtn.isChecked())

                if "regimparam" in result[0]:
                    self.writeDataToTableRegim(result[0], self.tableRegim)
                else:
                    self.initTableRegim()

                if "serverparam" in result[0]:
                    self.writeDataToTableServer(result[0], self.tableTelematicServer)

                if "phonesparam" in result[0]:
                    self.writeDataToTabPhones(result[0], self.tablePhonesParam, self.tablePhones)
                else:
                    self.initTablePhonesParam()
                    self.initTablePhones()

                if "textcmdparam" in result[0]:
                    self.writeDataToTabTextCmd(result[0], self.lineTxtCmd, self.lineTxtRequest)
                else:
                    self.initTextCmdTab()

                if "gpsparam" in result[0]:
                    self.writeDataToTabGps(result[0], self.text_edit)
                else:
                    self.initTabGps()

                if "transport" in result[0]:
                    self.writeDataToTabTransport(result[0], self.tableServSett, self.tableGprs)
                else:
                    self.initTableServSett()
                    self.initTableGprs(self.switchBtn.isChecked())

                if "service" in result[0]:
                    self.writeDataToTabService(result[0], self.tableServPhone)
                else:
                    self.initServicePhone(self.switchBtn.isChecked())

                if "sensors" in result[0]:
                    self.writeDataToTabSensors(result[0], self.tabSensors)
                else:
                    self.initTabSensors()
            else:
                self.initTableEvent(self.switchBtn.isChecked())
                self.initTableRegim()
                self.initTablePhonesParam()
                self.initTablePhones()
                self.initTextCmdTab()
                self.initTabGps()
                self.initTableServSett()
                self.initTableGprs(self.switchBtn.isChecked())
                self.initServicePhone(self.switchBtn.isChecked())
                self.initTabSensors()
                self.initTabLabels()
        except Exception as err:
            sshtunel.closeMongoDb()
            showError("viewClickedTrack", str(err), str(traceback.format_exc()))
        finally:
            showWarn("Stop", "", "", self.statusBar)
            sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
            stop = time.time()
            self.lineParam.appendPlainText(sttime + "Show info for tracker(s) : " + str(round(stop - start, 3)) + "sec")
            return

    @pyqtSlot()
    def OnChangeNameClient(self):
        try:
            if len(self.clientsData.clientsId) == 0:
                return
            indexClient = self.comboClient.currentIndex()
            if indexClient == -1:
                return
            self.spacClearTable(self.tableTrack)
            self.btnReadParam.setEnabled(False)
            self.btnWriteParam.setEnabled(False)
            self.btnSavingInfo.setEnabled(False)
            self.btnDeleteCom.setEnabled(False)
            self.btnDeleteAllCom.setEnabled(False)
            self.btnReset.setEnabled(False)
            self.switchBtn.setEnabled(False)
            self.btnSendTerm.setEnabled(False)
            self.btnClearTerm.setEnabled(False)
            self.comboTerminal.setEnabled(False)
        except Exception as err:
            showError("OnChangeNameClient", str(err), str(traceback.format_exc()))

    @pyqtSlot()
    def setSim1Sim2(self):
        if self.switchBtn.isChecked():
            self.switchBtn.setText("SIM2")
        else:
            self.switchBtn.setText("SIM1")
        self.initTableEvent(not self.switchBtn.isChecked())
        self.initTableGprs(self.switchBtn.isChecked())
        self.initServicePhone(self.switchBtn.isChecked())
        return

    @pyqtSlot()
    def OnResetTracker(self):
        if self.comboType.currentText() == 'TTL/TTU':
            if self.TestPin():
                return
            selected = self.tableTrack.selectionModel().selectedRows()
            if len(selected) == 0:
                showError("OnResetTracker", "Нет трекера выделенного для указанной операции!",
                          "Выделите трекер в таблице трекеров")
                return
            for ls in selected:
                select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                if len(select_identifier) == 4:
                    nameproto = self.protocolInfo[self.comboType.currentText()]
                    if self.comboClient.currentText() == 'diller':
                        idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                        nameclient = self.comboDiler.currentText()
                    else:
                        idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                        nameclient = self.comboClient.currentText()
                    sendCmdList, idcmd = self.commandA1.ResetTracker(self.pinCode.text())
                    id_chain = int(time.time())
                    self.SendRebootToDB(sendCmdList, select_identifier, nameclient, idclient, nameproto, 1, "reboot",
                                        "Reset", idcmd, id_chain)
                else:
                    showError("OnResetTracker", "Размер ID трекера не соотвествует типу teletrack",
                              "Длина должна быть 4 символа")
            self.OnGetInfoTrackers()
            return

    @pyqtSlot()
    def OnSearchTracker(self):
        try:
            row = self.searchRow
            while row < self.tableTrack.rowCount():
                column = self.searchColumn
                while column < self.tableTrack.columnCount():
                    item = self.tableTrack.item(row, column)
                    if item:
                        try:
                            value = self.lineSearch.text()
                            item.text().index(value)
                            self.tableTrack.selectRow(row)
                            self.tableTrack.scrollToItem(item)
                            self.searchColumn = column + 1
                            self.searchRow = row
                            return
                        except Exception:
                            pass
                    column += 1
                self.searchColumn = 0
                row += 1
            self.searchRow = 0
        except Exception as err:
            showError("OnSearchTracker", str(err), str(traceback.format_exc()))

    @pyqtSlot()
    def OnTabClicked(self):
        selected = self.tableTrack.selectionModel().selectedRows()
        if len(selected) > 0:
            number = self.tabs.currentIndex()
            if number  == 9 or number == 10 or number == 11:  # Terminal or Viewer or Labels
                self.btnReadParam.setEnabled(False)
                self.btnWriteParam.setEnabled(False)
            else:
                 self.btnReadParam.setEnabled(True)
                 self.btnWriteParam.setEnabled(True)
        return

    @pyqtSlot()
    def OnChangeNameDiler(self):
        if len(self.dilersData.dilersId) == 0:
            return
        dilerIndex = self.comboDiler.currentIndex()
        if dilerIndex == -1:
            return
        diler_name = self.dilersData.dilersName[dilerIndex]
        self.comboClient.clear()
        self.comboClient.addItems(self.dilers_clients[diler_name])
        self.comboClient.addItem("diller")
        self.comboClient.adjustSize()
        self.comboClient.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.spacClearTable(self.tableTrack)
        self.btnWriteParam.setEnabled(False)
        self.btnReadParam.setEnabled(False)
        self.btnSavingInfo.setEnabled(False)
        self.btnDeleteCom.setEnabled(False)
        self.btnDeleteAllCom.setEnabled(False)
        self.btnReset.setEnabled(False)
        self.switchBtn.setEnabled(False)
        self.btnSendTerm.setEnabled(False)
        self.btnClearTerm.setEnabled(False)
        self.comboTerminal.setEnabled(False)
        return

    @pyqtSlot()
    def OnGetTrackersData(self):
        # =============== авторизация ==========================
        if self.isAuthorization == 'ON':
            self.parentId, self.user, self.nameClient = self.showAuthorization()
            if self.parentId == '':
                showInfo("Authorization.", "Пользователю " + self.user + " в доступе отказано", "Неправильный пароль или логин!")
                return False
            elif self.parentId == '*':
                self.closed_()
                return False
            self.mongoAction.setEnabled(False)
            self.logoutAction.setEnabled(True)
            self.setWindowTitle(self.nameProgram + " -- User: " + self.user + " - " + "Client: " + self.nameClient)
        #============++++++++++++++++++++++++++++++++++++++++++++

        start = time.time()
        try:
            showWarn("Start", "OnGetTrackersData", "Внимание! Вычитываются трекеры из базы данных!", self.statusBar)

            gMongoDb = sshtunel.getMongoDb()

            self.dilersData = Dilers()

            self.comboDiler.clear()
            self.comboType.clear()
            self.comboClient.clear()

            collection = gMongoDb.get_collection('dilers')
            dilers = list(collection.find().sort("short_name", pymongo.ASCENDING))
            for ls in dilers:
                self.dilersData.dilersId.append(str(ls["_id"]))
                self.dilersData.dilersName.append(ls["short_name"])

            self.clientsData = Clients()

            collection = gMongoDb.get_collection('clients')
            clients = list(collection.find().sort("short_name", pymongo.ASCENDING))

            for ls in clients:
                self.clientsData.clientsId.append(str(ls["_id"]))
                self.clientsData.clientsName.append(ls["short_name"])
                self.clientsData.clientsDilerId.append(str(ls["diler"]))

            self.dilers_clients = {}
            for k, id in enumerate(self.dilersData.dilersId):
                self.dilers_clients[self.dilersData.dilersName[k]] = []
                for ls in clients:
                    if id == str(ls["diler"]):
                        self.dilers_clients[self.dilersData.dilersName[k]].append(ls["short_name"])

            collection = gMongoDb.get_collection('protocol')
            protocols = list(collection.find())
            self.protocolId = []
            self.protocolName = []
            self.protocolInfo = {}
            for lm in protocols:
                if lm["protocol"]["protocolName"] == 'teletrack32':
                    self.protocolId.append(str(lm["_id"]))
                    self.protocolName.append('TTA')
                    self.protocolInfo['TTA'] = lm["protocol"]["protocolName"]
                if lm["protocol"]["protocolName"] == 'teletrack64':
                    self.protocolId.append(str(lm["_id"]))
                    self.protocolName.append('TTL/TTU')
                    self.protocolInfo['TTL/TTU'] = lm["protocol"]["protocolName"]

            self.comboDiler.addItems(self.dilersData.dilersName)
            self.comboDiler.adjustSize()
            self.comboDiler.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            self.comboType.addItems(self.protocolName)
            self.comboType.adjustSize()
            self.comboType.setSizeAdjustPolicy(QComboBox.AdjustToContents)

            if self.isAuthorization == 'ON':
                try:
                    index_diler = self.dilersData.dilersId.index(self.parentId)
                except Exception as ex:
                    index_diler = -1

                index_client = -1
                if index_diler == -1:
                    try:
                        index_client = self.clientsData.clientsId.index(self.parentId)
                    except Exception as er:
                        pass

                if index_diler != -1 and index_client != -1:
                    namedil = self.dilersData.dilersName[index_diler]
                    namecli = self.clientsData.clientsName[index_client]
                    self.comboDiler.setCurrentText(namedil)
                    self.comboClient.setCurrentText(namecli)
                elif index_diler != -1 and index_client == -1:
                    namedil = self.dilersData.dilersName[index_diler]
                    self.comboDiler.setCurrentText(namedil)
                    self.comboClient.setCurrentText('diller')
                elif index_diler == -1 and index_client != -1:
                    diler = self.clientsData.clientsDilerId[index_client]
                    index_diler = self.dilersData.dilersId.index(diler)
                    namedil = self.dilersData.dilersName[index_diler]
                    namecli = self.clientsData.clientsName[index_client]
                    self.comboDiler.setCurrentText(namedil)
                    self.comboClient.setCurrentText(namecli)
                else:
                    showError("OnGetTrackersData", "Error get data clients from data base", "Attention! Unknown state!")
                    return False

            sshtunel.closeMongoDb()
            self.btnSaveState.setEnabled(True)
            self.btnSetDef.setEnabled(True)
            self.btnGetInfo.setEnabled(True)
            self.initTerminalTab()
            self.initSensViewerTab()
            self.initTabLabels()
            return True
        except Exception as err:
            sshtunel.closeMongoDb()
            showError("OnGetTrackersData", str(err), str(traceback.format_exc()))
        finally:
            showWarn("Stop", "", "", self.statusBar)
            sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
            stop = time.time()
            self.lineParam.appendPlainText(
                sttime + "Getting Data Info from Data Base: " + str(round(stop - start, 3)) + "sec")

    def ReadingParamTabAll(self):
        try:
            with open("src/dataProgrammComboType.dat", "rb") as in_file:
                self.programmData = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.programmData = ProgrammData()
        try:
            with open("src/dataTabEventsTta.dat", "rb") as in_file:
                self.eventsTta = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.eventsTta = EventsTabTta()
        try:
            with open("src/dataTabEventsTtl.dat", "rb") as in_file:
                self.eventsTtl = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.eventsTtl = EventsTabTtl()
        try:
            with open("src/dataTabEventsTtlU.dat", "rb") as in_file:
                self.eventsTtlu = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.eventsTtlu = EventsTabTtlU()
        try:
            with open("src/dataTabEventsTtu.dat", "rb") as in_file:
                self.eventsTtu = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.eventsTtu = EventsTabTtu()
        try:
            with open("src/dataTabGprs.dat", "rb") as in_file:
                self.classRegim = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classRegim = GprsTab()
        try:
            with open("src/dataTabServerTelematic.dat", "rb") as in_file:
                self.teleServer = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.teleServer = TelematServer()
        try:
            with open("src/dataTabServerService.dat", "rb") as in_file:
                self.serviceSett = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.serviceSett = ServiceSettings()
        try:
            with open("src/dataTabPhones.dat", "rb") as in_file:
                self.classPhoneParam = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classPhoneParam = PhonesParam()
        try:
            with open("src/dataTabPhonesValue.dat", "rb") as in_file:
                self.classPhones = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classPhones = PhonesValue()
        try:
            with open("src/dataTabTransportGprs.dat", "rb") as in_file:
                self.classGprs = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classGprs = GprsSetts()
        try:
            with open("src/dataTabTransportGprsP.dat", "rb") as in_file:
                self.classGprsP = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classGprsP = GprsSettsP()
        try:
            with open("src/dataTabTextCmd.dat", "rb") as in_file:
                self.classTextCmd = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classTextCmd = TextCmd()
        try:
            with open("src/dataTabTextRqst.dat", "rb") as in_file:
                self.classTextRqst = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classTextRqst = TextRqst()
        try:
            with open("src/dataTabTransportServSett.dat", "rb") as in_file:
                self.classServSett = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classServSett = ServerSettings()
        try:
            with open("src/dataTabTransportCombo.dat", "rb") as in_file:
                self.classOperSett = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classOperSett = OperatorSetting()
        try:
            with open("src/dataTabService.dat", "rb") as in_file:
                self.srvPhone = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.srvPhone = ServPhone()
        try:
            with open("src/dataTabServiceP.dat", "rb") as in_file:
                self.srvPhoneP = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.srvPhoneP = ServPhoneP()
        try:
            with open("src/dataTabTerminal.dat", "rb") as in_file:
                self.terminal_tab = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.terminal_tab = TerminalTab()
        try:
            with open("src/dataTabViewer.dat", "rb") as in_file:
                self.srvSensViewer = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.srvSensViewer = ViewerTab()
        try:
            with open("src/dataTabLabels.dat", "rb") as in_file:
                self.classLabels = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classLabels = LabelsTab()
        try:
            with open("src/classAin1.dat", "rb") as in_file:
                self.classAin1 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classAin1 = MemoAin1()
        try:
            with open("src/classAin2.dat", "rb") as in_file:
                self.classAin2 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classAin2 = MemoAin1()
        try:
            with open("src/classCan1.dat", "rb") as in_file:
                self.classCan1 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classCan1 = MemoCan1()
        try:
            with open("src/classCan2.dat", "rb") as in_file:
                self.classCan2 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classCan2 = MemoCan2()
        try:
            with open("src/classCan3.dat", "rb") as in_file:
                self.classCan3 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classCan3 = MemoCan3()
        try:
            with open("src/classCount1.dat", "rb") as in_file:
                self.classCount1 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classCount1 = memoCount1()
        try:
            with open("src/classCount2.dat", "rb") as in_file:
                self.classCount2 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classCount2 = memoCount2()
        try:
            with open("src/classCount3.dat", "rb") as in_file:
                self.classCount3 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classCount3 = memoCount3()
        try:
            with open("src/classDin.dat", "rb") as in_file:
                self.classDin = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classDin = MemoDin()
        try:
            with open("src/classRs485_1.dat", "rb") as in_file:
                self.classRs4851 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classRs4851 = MemoRs485_1()
        try:
            with open("src/classRs485_2.dat", "rb") as in_file:
                self.classRs4852 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classRs4852 = MemoRs485_2()
        try:
            with open("src/classRs485_3.dat", "rb") as in_file:
                self.classRs4853 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classRs4853 = MemoRs485_3()
        try:
            with open("src/classRs485_4.dat", "rb") as in_file:
                self.classRs4854 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classRs4854 = MemoRs485_4()
        try:
            with open("src/classRs485_5.dat", "rb") as in_file:
                self.classRs4855 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classRs4855 = MemoRs485_5()
        try:
            with open("src/classRs485_6.dat", "rb") as in_file:
                self.classRs4856 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classRs4856 = MemoRs485_6()
        try:
            with open("src/classRs485_7.dat", "rb") as in_file:
                self.classRs4857 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classRs4857 = MemoRs485_7()
        try:
            with open("src/classFuel1.dat", "rb") as in_file:
                self.classFuel1 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classFuel1 = Fuel1()
        try:
            with open("src/classFuel2.dat", "rb") as in_file:
                self.classFuel2 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classFuel2 = Fuel2()
        try:
            with open("src/classFuel3.dat", "rb") as in_file:
                self.classFuel3 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classFuel3 = Fuel3()
        try:
            with open("src/classFuel4.dat", "rb") as in_file:
                self.classFuel4 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classFuel4 = Fuel4()
        try:
            with open("src/classFuel5.dat", "rb") as in_file:
                self.classFuel5 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classFuel5 = Fuel5()
        try:
            with open("src/classRfid1.dat", "rb") as in_file:
                self.classRfid1 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classRfid1 = Rfid1()
        try:
            with open("src/classRfid2.dat", "rb") as in_file:
                self.classRfid2 = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.classRfid2 = Rfid2()
        try:
            with open("src/sensors_Tab.dat", "rb") as in_file:
                self.sensors_Tab = pickle.loads(self.cipher.decrypt(in_file.read()))
        except Exception as err:
            self.sensors_Tab = SensorsTab()
        return

    @pyqtSlot()
    def OnSetDefault(self):
        try:
            if self.tabs.currentIndex() == 8: # Sensors
                self.initTabSensors()
                showInfo("OnSetDefault", "Эта функция не работает для вкладки Sensors", "All be right!")
                return
            elif self.tabs.currentIndex() == 10: # Viewer
                showInfo("OnSetDefault", "Эта функция не работает для вкладки Viewer", "All be right!")
                return
            elif self.tabs.currentIndex() == 11: # Labels
                showInfo("OnSetDefault", "Эта функция не работает для вкладки Labels", "All be right!")
                return
                
            if showInfo("OnSetDefault", "Realy set default for current parameters?", "All be right!") != 1024:
                return

            if self.tabs.currentIndex() == 0:  # Events
                self.initTableEvent(self.switchBtn.isChecked())
            elif self.tabs.currentIndex() == 1:  # GPRS
                self.initTableRegim()
            elif self.tabs.currentIndex() == 2:  # Server
                self.initTableTelematicServer()
            elif self.tabs.currentIndex() == 3:  # Phones
                self.initTablePhonesParam()
                self.initTablePhones()
            elif self.tabs.currentIndex() == 4:  # TextCmd
                self.initTextCmdTab()
            elif self.tabs.currentIndex() == 5:  # GPS
                self.initTabGps()
            elif self.tabs.currentIndex() == 6:  # Transportr
                self.initTableServSett()
                self.initTableGprs(self.switchBtn.isChecked())
                self.initComboOpperSetting()
            elif self.tabs.currentIndex() == 7: # Service
                self.initServicePhone(self.switchBtn.isChecked())
            elif self.tabs.currentIndex() == 9: # Terminal
                self.initTerminalTab()
            elif self.tabs.currentIndex() == 11: # Labels
                self.initTabLabels()
        except Exception as err:
            showError("OnSetDefault", str(err), str(traceback.format_exc()))

    @pyqtSlot()
    def OnDeleteAllCommand(self):
        start = time.time()
        pp = None
        try:
            if showInfo("OnDeleteAllCommand", "Удалить все комманды выделенного трекера?", "") == 1024:
                showWarn("Start", "OnDeleteAllCommand", "Внимание! Удаляются команды(а) выбранного трекера(ов)", self.statusBar)
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnDeleteAllCommand", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                mongodb = sshtunel.getMongoDb()
                collection = mongodb.get_collection('commandData')
                for sl in selected:
                    select_identifier = str(self.tableTrack.item(sl.row(), 0).text())
                    res = collection.remove({"Device": select_identifier})
                    slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + \
                           "] " + "deleted one command:" + str(res) + " for " + select_identifier
                    self.lineParam.appendPlainText(slog)
                sshtunel.closeMongoDb()
                self.OnGetInfoTrackers()
        except Exception as err:
            showError("OnDeleteCommand", str(err), str(traceback.format_exc()))
        finally:
            showWarn("Stop", "", "", self.statusBar)
            sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
            stop = time.time()
            self.lineParam.appendPlainText(
                sttime + "Delete trackers comand(s) from Data Base: " + str(round(stop - start, 3)) + "sec")
        return

    @pyqtSlot()
    def OnDeleteCommand(self):
        start = time.time()
        pp = None
        try:
            if showInfo("OnDeleteCommand", "Удалить комманду(ы) выделенного трекера(ов)?", "") == 1024:
                showWarn("Start", "OnDeleteCommand", "Внимание! Удаляется текущая(ие) команда(ы) трекера(ов)", self.statusBar)
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnDeleteCommand", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                mongodb = sshtunel.getMongoDb()
                collection = mongodb.get_collection('commandData')
                for sl in selected:
                    select_identifier = str(self.tableTrack.item(sl.row(), 0).text())
                    mainWidget = self.tableTrack.cellWidget(sl.row(), 2)
                    comboBoxComm = mainWidget.layout().itemAt(0).widget()
                    comboText = comboBoxComm.currentText()
                    if len(comboText) > 0:
                        textcomm = json.loads(comboText)
                        _id = bson.ObjectId(textcomm["id"])
                        res = collection.remove({"_id": _id})
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + \
                               "] " + "deleted one command:" + str(res) + " for " + select_identifier
                        self.lineParam.appendPlainText(slog)
                sshtunel.closeMongoDb()
                self.OnGetInfoTrackers()
        except Exception as err:
            showError("OnDeleteCommand", str(err), str(traceback.format_exc()))
        finally:
            showWarn("Stop", "", "", self.statusBar)
            sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
            stop = time.time()
            self.lineParam.appendPlainText(
                sttime + "Delete trackers comand(s) from Data Base: " + str(round(stop - start, 3)) + "sec")
        return

    @pyqtSlot()
    def OnSaveParametersToDb(self):
        start = time.time()
        pp = None
        try:
            selected = self.tableTrack.selectionModel().selectedRows()
            if len(selected) == 0:
                showError("OnReadParam", "Нет трекера выделенного для указанной операции!",
                          "Выделите трекер в таблице трекеров")
                return
            selected_identifier = str(self.tableTrack.item(selected[len(selected) - 1].row(), 0).text())

            namecli = self.comboClient.currentText()
            if namecli == 'diller':
                namecli = self.comboDiler.currentText()
            showWarn("Start", "OnSaveParametersToDb",
                          "Внимание! Сохраняются текущие данные в базу данных для трекера " + selected_identifier, self.statusBar)
            if self.tabs.currentIndex() == 0:  # Events
                structure = {}
                structure["Device"] = selected_identifier
                structure["Type"] = self.comboType.currentText()
                structure["Protocol"] = self.protocolInfo[self.comboType.currentText()]
                settings = {}
                fixingPoint = {}
                sentServer = {}
                smsPhone = {}
                if self.comboType.currentText() == 'TTA':
                    for k in range(len(self.eventsTta.ParamName)):
                        checkBoxWidget = self.tableEvents.cellWidget(k, 3)
                        checkBox = checkBoxWidget.layout().itemAt(0).widget()
                        key = str(k)
                        fixingPoint[key] = checkBox.isChecked()
                        checkBoxWidget = self.tableEvents.cellWidget(k, 4)
                        checkBox = checkBoxWidget.layout().itemAt(0).widget()
                        sentServer[key] = checkBox.isChecked()
                        checkBoxWidget = self.tableEvents.cellWidget(k, 5)
                        checkBox = checkBoxWidget.layout().itemAt(0).widget()
                        smsPhone[key] = checkBox.isChecked()
                elif self.comboType.currentText() == 'TTL/TTU':
                    for k in range(len(self.eventsTtl.ParamName)):
                        checkBoxWidget = self.tableEvents.cellWidget(k, 3)
                        checkBox = checkBoxWidget.layout().itemAt(0).widget()
                        key = str(k)
                        fixingPoint[key] = checkBox.isChecked()
                        checkBoxWidget = self.tableEvents.cellWidget(k, 4)
                        checkBox = checkBoxWidget.layout().itemAt(0).widget()
                        sentServer[key] = checkBox.isChecked()
                settings['simtype'] = self.switchBtn.isChecked()
                settings["fixingPoint"] = fixingPoint
                settings["sentServer"] = sentServer
                settings["smsPhone"] = smsPhone
                structure["parameters"] = settings
                mongodb = sshtunel.getMongoDb()
                collection = mongodb.get_collection('parametersTrackData')
                result = list(collection.find({"Device": selected_identifier}))
                if len(result) > 0:
                    res = collection.update({"Device": selected_identifier}, {"$set": {"parameters": settings}})
                else:
                    res = collection.insert(structure)
                sshtunel.closeMongoDb()
                self.tabs.setTabText(0, "Events (" + selected_identifier + "," + namecli + ")")
                sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
                self.lineParam.appendPlainText(sttime + "OnSaveParametersToDb. : Saving parameter to DB")
            elif self.tabs.currentIndex() == 1:  # GPRS
                if self.comboType.currentText() == 'TTA':
                    structure = {}
                    structure["Device"] = selected_identifier
                    structure["Type"] = self.comboType.currentText()
                    structure["Protocol"] = self.protocolInfo[self.comboType.currentText()]
                    settings = {}
                    regimPoint = {}
                    for k in range(len(self.classRegim.NameRegim)):
                        key = str(k)
                        regimPoint[key] = self.tableRegim.item(k, 1).text()
                    settings["regimPoint"] = regimPoint
                    structure["regimparam"] = settings
                    mongodb = sshtunel.getMongoDb()
                    collection = mongodb.get_collection('parametersTrackData')
                    result = list(collection.find({"Device": selected_identifier}))
                    if len(result) > 0:
                        res = collection.update({"Device": selected_identifier},
                                                {"$set": {"regimparam": settings}})
                    else:
                        res = collection.insert(structure)
                    sshtunel.closeMongoDb()
                    self.tabs.setTabText(1, "GPRS (" + selected_identifier + "," + namecli + ")")
                    sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
                    self.lineParam.appendPlainText(sttime + "OnSaveParametersToDb. : Saving parameter to DB")
            elif self.tabs.currentIndex() == 2:  # Server
                structure = {}
                structure["Device"] = selected_identifier
                structure["Type"] = self.comboType.currentText()
                structure["Protocol"] = self.protocolInfo[self.comboType.currentText()]
                settings = {}
                serverPoint = {}
                for row in range(self.tableTelematicServer.rowCount()):
                    serverPoint[str(row)] = self.tableTelematicServer.item(row, 1).text()
                settings["serverPoint"] = serverPoint
                structure["serverparam"] = settings
                mongodb = sshtunel.getMongoDb()
                collection = mongodb.get_collection('parametersTrackData')
                result = list(collection.find({"Device": selected_identifier}))
                if len(result) > 0:
                    res = collection.update({"Device": selected_identifier}, {"$set": {"serverparam": settings}})
                else:
                    res = collection.insert(structure)
                sshtunel.closeMongoDb()
                self.tabs.setTabText(2, "Server (" + selected_identifier + "," + namecli + ")")
                sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
                self.lineParam.appendPlainText(sttime + "OnSaveParametersToDb. : Saving parameter to DB")
            elif self.tabs.currentIndex() == 3:  # Phones
                structure = {}
                structure["Device"] = selected_identifier
                structure["Type"] = self.comboType.currentText()
                structure["Protocol"] = self.protocolInfo[self.comboType.currentText()]
                settings = {}
                paramPhones = {}
                phonePhones = {}
                for row in range(self.tablePhonesParam.rowCount()):
                    paramPhones[str(row)] = self.tablePhonesParam.item(row, 1).text()
                settings["paramPhones"] = paramPhones
                for row in range(self.tablePhones.rowCount()):
                    phonePhones[str(row)] = self.tablePhones.item(row, 1).text()
                settings["phonePhones"] = phonePhones
                structure["phonesparam"] = settings
                mongodb = sshtunel.getMongoDb()
                collection = mongodb.get_collection('parametersTrackData')
                result = list(collection.find({"Device": selected_identifier}))
                if len(result) > 0:
                    res = collection.update({"Device": selected_identifier}, {"$set": {"phonesparam": settings}})
                else:
                    res = collection.insert(structure)
                sshtunel.closeMongoDb()
                self.tabs.setTabText(3, "Phones (" + selected_identifier + "," + namecli + ")")
                sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
                self.lineParam.appendPlainText(sttime + "OnSaveParametersToDb. : Saving parameter to DB")
            elif self.tabs.currentIndex() == 4:  # TextCmd
                structure = {}
                structure["Device"] = selected_identifier
                structure["Type"] = self.comboType.currentText()
                structure["Protocol"] = self.protocolInfo[self.comboType.currentText()]
                settings = {}
                paramCmd = {}
                paramRqst = {}
                paramCmd['0'] = self.lineTxtCmd.toPlainText()
                settings["paramCmd"] = paramCmd
                paramRqst['0'] = self.lineTxtRequest.toPlainText()
                settings["paramRqst"] = paramRqst
                structure["textcmdparam"] = settings
                mongodb = sshtunel.getMongoDb()
                collection = mongodb.get_collection('parametersTrackData')
                result = list(collection.find({"Device": selected_identifier}))
                if len(result) > 0:
                    res = collection.update({"Device": selected_identifier}, {"$set": {"textcmdparam": settings}})
                else:
                    res = collection.insert(structure)
                sshtunel.closeMongoDb()
                self.tabs.setTabText(4, "TextCmd (" + selected_identifier + "," + namecli + ")")
            elif self.tabs.currentIndex() == 5:  # GPS
                showInfo("OnSaveParametersToDb", "Эта функция не работает для вкладки GPS", ":-(((")
            elif self.tabs.currentIndex() == 6:  # Transport
                structure = {}
                structure["Device"] = selected_identifier
                structure["Type"] = self.comboType.currentText()
                structure["Protocol"] = self.protocolInfo[self.comboType.currentText()]
                settings = {}
                servSet = {}
                gprsSet = {}
                settings['simtype'] = self.switchBtn.isChecked()
                for row in range(self.tableServSett.rowCount()):
                    servSet[str(row)] = self.tableServSett.item(row, 1).text()
                settings["servSet"] = servSet
                for row in range(self.tableGprs.rowCount()):
                    gprsSet[str(row)] = self.tableGprs.item(row, 1).text()
                settings["gprsSet"] = gprsSet
                structure["transport"] = settings
                mongodb = sshtunel.getMongoDb()
                collection = mongodb.get_collection('parametersTrackData')
                result = list(collection.find({"Device": selected_identifier}))
                if len(result) > 0:
                    res = collection.update({"Device": selected_identifier}, {"$set": {"transport": settings}})
                else:
                    res = collection.insert(structure)
                sshtunel.closeMongoDb()
                self.tabs.setTabText(6, "Transport (" + selected_identifier + "," + namecli + ")")
                sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
                self.lineParam.appendPlainText(sttime + "OnSaveParametersToDb. : Saving parameter to DB")
            elif self.tabs.currentIndex() == 7:  # Service
                structure = {}
                structure["Device"] = selected_identifier
                structure["Type"] = self.comboType.currentText()
                structure["Protocol"] = self.protocolInfo[self.comboType.currentText()]
                settings = {}
                phoneSet = {}
                settings['simtype'] = self.switchBtn.isChecked()
                for row in range(self.tableServPhone.rowCount()):
                    phoneSet[str(row)] = self.tableServPhone.item(row, 1).text()
                settings["phoneSet"] = phoneSet
                settings["regimSet"] = self.lineReg.text()
                structure["service"] = settings
                mongodb = sshtunel.getMongoDb()
                collection = mongodb.get_collection('parametersTrackData')
                result = list(collection.find({"Device": selected_identifier}))
                if len(result) > 0:
                    res = collection.update({"Device": selected_identifier}, {"$set": {"service": settings}})
                else:
                    res = collection.insert(structure)
                sshtunel.closeMongoDb()
                self.tabs.setTabText(7, "Service (" + selected_identifier + "," + namecli + ")")
                sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
                self.lineParam.appendPlainText(sttime + "OnSaveParametersToDb. : Saving parameter to DB")
            elif self.tabs.currentIndex() == 8:  # Sensors
                showInfo("OnSaveParametersToDb", "Эта функция не работает для вкладки Sensors", ":-)))")
            elif self.tabs.currentIndex() == 9:  # Terminal
                showInfo("OnSaveParametersToDb", "Эта функция не работает для вкладки Terminal", ":-)))")
            elif self.tabs.currentIndex() == 10:  # Viewer
                showInfo("OnSaveParametersToDb", "Эта функция не работает для вкладки Viewer", ";-))")
            elif self.tabs.currentIndex() == 11:  # Labels
                showInfo("OnSaveParametersToDb", "Эта функция не работает для вкладки Labels", ";-)")
        except Exception as err:
            showError("OnSaveParametersToDb", str(err), str(traceback.format_exc()))
        finally:
            showWarn("Stop", "", "", self.statusBar)
            sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
            stop = time.time()
            self.lineParam.appendPlainText(sttime + "OnSaveParametersToDb. Total working: " + str(round(stop - start, 4)) + "sec")
        return

    @pyqtSlot()
    def OnGetInfoTrackers(self):
        def sortedData(dcts):
            return dcts["UtcCreate"]

        start = time.time()
        pp = None
        try:
            showWarn("Start", "OnGetInfoTrackers",
                          "Внимание! Вычитываются из базы данных данные трекеров", self.statusBar)
            dilerIndex = self.comboDiler.currentIndex()
            if dilerIndex == -1:
                return

            self.spacClearTable(self.tableTrack)

            diler_name = self.dilersData.dilersName[dilerIndex]
            diler_id = self.dilersData.dilersId[dilerIndex]

            indexClient = self.comboClient.currentIndex()
            if indexClient == -1:
                return

            try:
                client_name = self.dilers_clients[diler_name][indexClient]
                index_client = self.clientsData.clientsName.index(client_name)
                client_id = self.clientsData.clientsId[index_client]
            except Exception:
                client_name = 'diller'

            qsql = 'SELECT * FROM "pcbVersion"'
            pcbVersion = sshtunel.queryFromPostgreSql(qsql)
            pcbTtl = []
            pcbTtu = []
            if len(pcbVersion["Id"]) == 0:
                showError("OnGetInfoTrackers.", "Can not get pcbVersion table", "pcbVersion table with error!")
                return
            for k in range(len(pcbVersion["Id"])):
                pcbvers = pcbVersion["Name"][k].lower()
                if 'ttl' in pcbvers:
                    pcbTtl.append(pcbVersion["Id"][k])
                elif 'ttu' in pcbvers:
                    pcbTtu.append(pcbVersion["Id"][k])
                else:
                    pass

            indexProtocol = self.comboType.currentIndex()
            protocol_id = self.protocolId[indexProtocol]

            if client_name == 'diller':
                qsql = 'SELECT "devices"."Identifier","objects"."Name","devices"."PcbVersion" FROM "devices","objects" WHERE "devices"."ProtocolId" = ' + \
                       "'" + protocol_id + "' AND " + '"devices"."Parent" = ' + "'" + diler_id + "' AND " + \
                       '"objects"."DeviceId" = "devices"."Id" ORDER BY "devices"."Identifier" ASC'
            else:
                qsql = 'SELECT "devices"."Identifier","objects"."Name","devices"."PcbVersion" FROM "devices","objects" WHERE "devices"."ProtocolId" = ' + \
                       "'" + protocol_id + "' AND " + '"devices"."Parent" = ' + "'" + client_id + "' AND " + \
                       '"objects"."DeviceId" = "devices"."Id" ORDER BY "devices"."Identifier" ASC'

            result = sshtunel.queryFromPostgreSql(qsql)

            if len(result) > 0:
                identifiers = []
                self.tableTrack.setRowCount(len(result["Identifier"]))
                for k in range(len(result["Identifier"])):
                    identifiers.append(str(result["Identifier"][k]))
                    qtbl = QTableWidgetItem(str(result["Identifier"][k]))
                    self.tableTrack.setItem(k, 0, qtbl)
                    qtbl = QTableWidgetItem(str(result["Name"][k]))
                    self.tableTrack.setItem(k, 1, qtbl)
                    if result["PcbVersion"][k] in pcbTtl:
                        qtbl = QTableWidgetItem('TTL')
                        self.tableTrack.setItem(k, 4, qtbl)
                    elif result["PcbVersion"][k] in pcbTtu:
                        qtbl = QTableWidgetItem('TTU')
                        self.tableTrack.setItem(k, 4, qtbl)
                    else:
                        qtbl = QTableWidgetItem('TTA')
                        self.tableTrack.setItem(k, 4, qtbl)

                mongodb = sshtunel.getMongoDb()
                collection = mongodb.get_collection('commandData')
                rst = list(collection.find({"Device": {"$in": identifiers}}))
                sshtunel.closeMongoDb()
                commands = {}
                for lt in rst:
                    try:
                        commands[lt["Device"]].append(lt)
                    except Exception:
                        commands[lt["Device"]] = []
                        commands[lt["Device"]].append(lt)

                for key in commands:
                    buff = commands[key]
                    buff.sort(key=sortedData, reverse=True)

                for k in range(len(result["Identifier"])):
                    operatCode = []
                    statecomm = {}
                    if result["Identifier"][k] in commands:
                        for ls in commands[result["Identifier"][k]]:
                            statecomm["status"] = ls["statusCommand"]
                            statecomm["execute"] = ls["Status"]
                            statecomm["name"] = ls["Name"]
                            statecomm["type"] = ls["Type"]
                            if ls["UtcResponse"] > 0:
                                statecomm["response"] = time.strftime("%Y-%m-%d %H:%M:%S",
                                                                      time.localtime(ls["UtcResponse"]))
                            elif ls["UtcRead"] > 0:
                                statecomm["read"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(ls["UtcRead"]))
                            else:
                                statecomm["create"] = time.strftime("%Y-%m-%d %H:%M:%S",
                                                                    time.localtime(ls["UtcCreate"]))
                            statecomm["chain"] = str(ls["IDChain"])
                            statecomm["id"] = str(ls["_id"])
                            operatCode.append(json.dumps(statecomm))

                    completer = QCompleter(operatCode)
                    completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
                    completer.popup().setStyleSheet("background-color: yellow")
                    comboBoxWidget = QWidget()
                    operatCombo = QComboBox()
                    operatCombo.setEditable(True)
                    operatCombo.setToolTip("Click for see command")
                    operatCombo.setCompleter(completer)
                    operatCombo.addItems(operatCode)
                    operatCombo.setCurrentIndex(0)
                    operatCombo.setSizeAdjustPolicy(QComboBox.AdjustToContents)
                    operatCombo.adjustSize()
                    layoutComboBox = QHBoxLayout(comboBoxWidget)
                    layoutComboBox.addWidget(operatCombo)
                    layoutComboBox.setAlignment(Qt.AlignLeft)
                    layoutComboBox.setContentsMargins(0, 0, 0, 0)
                    self.tableTrack.setCellWidget(k, 2, comboBoxWidget)
            self.tableTrack.resizeColumnsToContents()
            self.tableTrack.resizeRowsToContents()
        except Exception as err:
            showError("OnGetInfoTrackers", str(err), str(traceback.format_exc()))
        finally:
            showWarn("Stop", "", "", self.statusBar)
            sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
            stop = time.time()
            self.lineParam.appendPlainText(
                sttime + "Getting List trackers from Data Base: " + str(round(stop - start, 3)) + "sec")
            return

    def spacClearTable(self, qwtable):
        try:
            qwtable.clearContents()
            number = qwtable.rowCount()
            while number > 0:
                qwtable.removeRow(0)
                number = qwtable.rowCount()
        except Exception as err:
            showError("spacClearTable", str(err), str(traceback.format_exc()))

    def encrypt_data(self, data, outfl):
        encrypted_text = self.cipher.encrypt(data)
        outfl.write(encrypted_text)
        return

    @pyqtSlot()
    def OnSaveStateParam(self):
        self.programmData.setComboTypeTracker(self.comboType.currentText())
        if self.tabs.currentIndex() == 0:
            self.saveTabEvents(self.comboType.currentText(), self.switchBtn.isChecked())
            with open("src/dataTabEventsTta.dat", "wb") as out_file:
                eventstta = pickle.dumps(self.eventsTta)
                self.encrypt_data(eventstta, out_file)
            with open("src/dataTabEventsTtl.dat", "wb") as out_file:
                eventsttl = pickle.dumps(self.eventsTtl)
                self.encrypt_data(eventsttl, out_file)
            with open("src/dataTabEventsTtlU.dat", "wb") as out_file:
                eventsttlu = pickle.dumps(self.eventsTtlu)
                self.encrypt_data(eventsttlu, out_file)
            with open("src/dataTabEventsTtu.dat", "wb") as out_file:
                eventsttu = pickle.dumps(self.eventsTtu)
                self.encrypt_data(eventsttu, out_file)
            with open("src/dataProgrammComboType.dat", "wb") as out_file:
                programmdata = pickle.dumps(self.programmData)
                self.encrypt_data(programmdata, out_file)
        elif self.tabs.currentIndex() == 1:
            self.saveTabGprs(self.comboType.currentText())
            with open("src/dataTabGprs.dat", "wb") as out_file:
                classregim = pickle.dumps(self.classRegim)
                self.encrypt_data(classregim, out_file)
        elif self.tabs.currentIndex() == 2:
            self.saveTabServer(self.comboType.currentText())
            with open("src/dataTabServerTelematic.dat", "wb") as out_file:
                teleserver = pickle.dumps(self.teleServer)
                self.encrypt_data(teleserver, out_file)
            with open("src/dataTabServerService.dat", "wb") as out_file:
                servicesett = pickle.dumps(self.serviceSett)
                self.encrypt_data(servicesett, out_file)
        elif self.tabs.currentIndex() == 3:
            self.saveTabPhones(self.comboType.currentText())
            with open("src/dataTabPhones.dat", "wb") as out_file:
                classphoneparam = pickle.dumps(self.classPhoneParam)
                self.encrypt_data(classphoneparam, out_file)
            self.saveTabPhonesValue(self.comboType.currentText())
            with open("src/dataTabPhonesValue.dat", "wb") as out_file:
                classphones = pickle.dumps(self.classPhones)
                self.encrypt_data(classphones, out_file)
        elif self.tabs.currentIndex() == 4:
            self.saveTabTextCmd(self.comboType.currentText())
            with open("src/dataTabTextCmd.dat", "wb") as out_file:
                classtextcmd = pickle.dumps(self.classTextCmd)
                self.encrypt_data(classtextcmd, out_file)
            with open("src/dataTabTextRqst.dat", "wb") as out_file:
                classtextrqst = pickle.dumps(self.classTextRqst)
                self.encrypt_data(classtextrqst, out_file)
        elif self.tabs.currentIndex() == 5:
            self.saveTabGps(self.comboType.currentText())
        elif self.tabs.currentIndex() == 6:
            self.saveTabTransport(self.comboType.currentText(), not self.switchBtn.isChecked())
            with open("src/dataTabTransportGprs.dat", "wb") as out_file:
                classgprs = pickle.dumps(self.classGprs)
                self.encrypt_data(classgprs, out_file)
            with open("src/dataTabTransportGprsP.dat", "wb") as out_file:
                classgprsp = pickle.dumps(self.classGprsP)
                self.encrypt_data(classgprsp, out_file)
            with open("src/dataTabTransportServSett.dat", "wb") as out_file:
                classservsett = pickle.dumps(self.classServSett)
                self.encrypt_data(classservsett, out_file)
            with open("src/dataTabTransportCombo.dat", "wb") as out_file:
                classopersett = pickle.dumps(self.classOperSett)
                self.encrypt_data(classopersett, out_file)
        elif self.tabs.currentIndex() == 7:
            self.saveTabService(self.switchBtn.isChecked())
            with open("src/dataTabService.dat", "wb") as out_file:
                srvphone = pickle.dumps(self.srvPhone)
                self.encrypt_data(srvphone, out_file)
            with open("src/dataTabServiceP.dat", "wb") as out_file:
                srvphonep = pickle.dumps(self.srvPhoneP)
                self.encrypt_data(srvphonep, out_file)
        elif self.tabs.currentIndex() == 8:
            self.saveTabSensors()
            with open("src/classAin1.dat", "wb") as out_file:
                classain1 = pickle.dumps(self.classAin1)
                self.encrypt_data(classain1, out_file)
            with open("src/classAin2.dat", "wb") as out_file:
                classain2 = pickle.dumps(self.classAin2)
                self.encrypt_data(classain2, out_file)
            with open("src/classCan1.dat", "wb") as out_file:
                classcan1 = pickle.dumps(self.classCan1)
                self.encrypt_data(classcan1, out_file)
            with open("src/classCan2.dat", "wb") as out_file:
                classcan2 = pickle.dumps(self.classCan2)
                self.encrypt_data(classcan2, out_file)
            with open("src/classCan3.dat", "wb") as out_file:
                classcan3 = pickle.dumps(self.classCan3)
                self.encrypt_data(classcan3, out_file)
            with open("src/classCount1.dat", "wb") as out_file:
                classcount1 = pickle.dumps(self.classCount1)
                self.encrypt_data(classcount1, out_file)
            with open("src/classCount2.dat", "wb") as out_file:
                classcount2 = pickle.dumps(self.classCount2)
                self.encrypt_data(classcount2, out_file)
            with open("src/classCount3.dat", "wb") as out_file:
                classcount3 = pickle.dumps(self.classCount3)
                self.encrypt_data(classcount3, out_file)
            with open("src/classDin.dat", "wb") as out_file:
                classdin = pickle.dumps(self.classDin)
                self.encrypt_data(classdin, out_file)
            with open("src/classRs485_1.dat", "wb") as out_file:
                classrs4851 = pickle.dumps(self.classRs4851)
                self.encrypt_data(classrs4851, out_file)
            with open("src/classRs485_2.dat", "wb") as out_file:
                classrs4852 = pickle.dumps(self.classRs4852)
                self.encrypt_data(classrs4852, out_file)
            with open("src/classRs485_3.dat", "wb") as out_file:
                classrs4853 = pickle.dumps(self.classRs4853)
                self.encrypt_data(classrs4853, out_file)
            with open("src/classRs485_4.dat", "wb") as out_file:
                classrs4854 = pickle.dumps(self.classRs4854)
                self.encrypt_data(classrs4854, out_file)
            with open("src/classRs485_5.dat", "wb") as out_file:
                classrs4855 = pickle.dumps(self.classRs4855)
                self.encrypt_data(classrs4855, out_file)
            with open("src/classRs485_6.dat", "wb") as out_file:
                classrs4856 = pickle.dumps(self.classRs4856)
                self.encrypt_data(classrs4856, out_file)
            with open("src/classRs485_7.dat", "wb") as out_file:
                classrs4857 = pickle.dumps(self.classRs4857)
                self.encrypt_data(classrs4857, out_file)
            with open("src/classFuel1.dat", "wb") as out_file:
                classfuel1 = pickle.dumps(self.classFuel1)
                self.encrypt_data(classfuel1, out_file)
            with open("src/classFuel2.dat", "wb") as out_file:
                classfuel2 = pickle.dumps(self.classFuel2)
                self.encrypt_data(classfuel2, out_file)
            with open("src/classFuel3.dat", "wb") as out_file:
                classfuel3 = pickle.dumps(self.classFuel3)
                self.encrypt_data(classfuel3, out_file)
            with open("src/classFuel4.dat", "wb") as out_file:
                classfuel4 = pickle.dumps(self.classFuel4)
                self.encrypt_data(classfuel4, out_file)
            with open("src/classFuel5.dat", "wb") as out_file:
                classfuel5 = pickle.dumps(self.classFuel5)
                self.encrypt_data(classfuel5, out_file)
            with open("src/classRfid1.dat", "wb") as out_file:
                classrfid1 = pickle.dumps(self.classRfid1)
                self.encrypt_data(classrfid1, out_file)
            with open("src/classRfid2.dat", "wb") as out_file:
                classrfid2 = pickle.dumps(self.classRfid2)
                self.encrypt_data(classrfid2, out_file)
            with open("src/sensors_Tab.dat", "wb") as out_file:
                sensors_tab = pickle.dumps(self.sensors_Tab)
                self.encrypt_data(sensors_tab, out_file)
        elif self.tabs.currentIndex() == 9:
            self.saveTerminalTab()
            with open("src/dataTabTerminal.dat", "wb") as out_file:
                terminal_tab = pickle.dumps(self.terminal_tab)
                self.encrypt_data(terminal_tab, out_file)
        elif self.tabs.currentIndex() == 10:
            self.saveTabViewer()
            with open("src/dataTabViewer.dat", "wb") as out_file:
                srvsensviewer = pickle.dumps(self.srvSensViewer)
                self.encrypt_data(srvsensviewer, out_file)
        elif self.tabs.currentIndex() == 11:
            self.saveTabLabels()
            with open("src/dataTabLabels.dat", "wb") as out_file:
                classLabels = pickle.dumps(self.classLabels)
                self.encrypt_data(classLabels, out_file)
        return

    def initComboBoxTypeTracker(self):
        if self.programmData == '':
            self.programmData = ProgrammData()
        self.comboType.setCurrentText(self.programmData.ComboTypeTracker)
        return

    def initTerminalTab(self):
        if self.comboType.currentText() == 'TTA' or self.comboType.currentText() == '':
            self.tabs.setTabEnabled(9, False)
            self.tabs.setTabText(9, "Terminal")
        elif self.comboType.currentText() == 'TTL/TTU':
            if self.terminal_tab == '':
                self.terminal_tab = TerminalTab()
            self.comboTerminal.clear()
            self.comboTerminal.addItems(self.terminal_tab.CommTerm)
            self.lineTerm.clear()
            self.lineTerm.setText(self.terminal_tab.LineTermData)
            self.checkHexModeTerm.setChecked(self.terminal_tab.HexModeTerm)
            self.text_term.clear()
            lines = self.terminal_tab.TextTerm
            self.text_term.appendPlainText(lines)
            self.tabs.setTabEnabled(9, True)
            self.tabs.setTabText(9, "Terminal (default)")
        return

    def initSensorsTab(self):
        if self.sensors_Tab == '':
            self.sensors_Tab = SensorsTab()
        self.lineSnsCode.setText(self.sensors_Tab.SensCode)
        self.comboRs485.addItems(self.sensors_Tab.SpeedRs485)
        self.comboRs485.setCurrentText(self.sensors_Tab.SpeedValueRs485)
        self.comboCan1.addItems(self.sensors_Tab.CAN1)
        self.comboCan2.addItems(self.sensors_Tab.CAN2)
        self.spinAcc.setValue(self.sensors_Tab.AccSensitivity)
        if self.classAin1 == '':
            self.classAin1 = MemoAin1()
        self.spinLimitUp1.setValue(self.classAin1.AinLimitUp)
        self.spinLimitDown1.setValue(self.classAin1.AinLimitDown)
        if self.classAin2 == '':
            self.classAin2 = MemoAin2()
        self.spinLimitUp2.setValue(self.classAin2.AinLimitUp)
        self.spinLimitDown2.setValue(self.classAin2.AinLimitDown)
        if self.classCan1 == '':
            self.classCan1 = MemoCan1()
        self.comboWorkCan1.addItems(self.classCan1.WorkCan)
        self.lineCanAddrs1.setText(self.classCan1.CanAddress)
        self.lineMask1.setText(self.classCan1.CanMask)
        self.spinStBit1.setValue(self.classCan1.CanStartBit)
        self.spinLngth1.setValue(self.classCan1.CanLength)
        self.spinTmout1.setValue(self.classCan1.CanTimeout)
        if self.classCan2 == '':
            self.classCan2 = MemoCan2()
        self.comboWorkCan2.addItems(self.classCan2.WorkCan)
        self.lineCanAddrs2.setText(self.classCan2.CanAddress)
        self.lineMask2.setText(self.classCan2.CanMask)
        self.spinStBit2.setValue(self.classCan2.CanStartBit)
        self.spinLngth2.setValue(self.classCan2.CanLength)
        self.spinTmout2.setValue(self.classCan2.CanTimeout)
        if self.classCan3 == '':
            self.classCan3 = MemoCan3()
        self.comboWorkCan3.addItems(self.classCan3.WorkCan)
        self.lineCanAddrs3.setText(self.classCan3.CanAddress)
        self.lineMask3.setText(self.classCan3.CanMask)
        self.spinStBit3.setValue(self.classCan3.CanStartBit)
        self.spinLngth3.setValue(self.classCan3.CanLength)
        self.spinTmout3.setValue(self.classCan3.CanTimeout)
        if self.classCount1 == '':
            self.classCount1 = memoCount1()
        self.comboCnt1.addItems(self.classCount1.RegimD)
        self.comboCnt1.setCurrentText(self.classCount1.RegimValue)
        self.spinEvent11.setValue(self.classCount1.Event1)
        self.spinEvent21.setValue(self.classCount1.Event2)
        self.comboSource1.addItems(self.classCount1.Source)
        self.comboSource1.setCurrentText(self.classCount1.SourceValue)
        self.spinSource1.setValue(self.classCount1.Source_En)
        if self.classCount2 == '':
            self.classCount2 = memoCount2()
        self.comboCnt2.addItems(self.classCount2.RegimD)
        self.comboCnt2.setCurrentText(self.classCount2.RegimValue)
        self.spinEvent12.setValue(self.classCount2.Event1)
        self.spinEvent22.setValue(self.classCount2.Event2)
        self.comboSource2.addItems(self.classCount2.Source)
        self.comboSource2.setCurrentText(self.classCount2.SourceValue)
        self.spinSource2.setValue(self.classCount2.Source_En)
        if self.classCount3 == '':
            self.classCount3 = memoCount3()
        self.comboCnt3.addItems(self.classCount3.RegimD)
        self.comboCnt3.setCurrentText(self.classCount3.RegimValue)
        self.spinEvent13.setValue(self.classCount3.Event1)
        self.spinEvent23.setValue(self.classCount3.Event2)
        self.comboSource3.addItems(self.classCount3.Source)
        self.comboSource3.setCurrentText(self.classCount3.SourceValue)
        self.spinSource3.setValue(self.classCount3.Source_En)
        if self.classDin == '':
            self.classDin = MemoDin()
        self.comboBounceDin1.addItems(self.classDin.DinCode1)
        self.comboBounceDin1.setCurrentText(self.classDin.DinCode1Value)
        self.checkDin1.setChecked(self.classDin.CheckDin1)
        self.comboBounceDin2.addItems(self.classDin.DinCode2)
        self.comboBounceDin2.setCurrentText(self.classDin.DinCode2Value)
        self.checkDin2.setChecked(self.classDin.CheckDin2)
        self.comboBounceDin3.addItems(self.classDin.DinCode3)
        self.comboBounceDin3.setCurrentText(self.classDin.DinCode3Value)
        self.checkDin3.setChecked(self.classDin.CheckDin3)
        self.comboBounceDin4.addItems(self.classDin.DinCode4)
        self.comboBounceDin4.setCurrentText(self.classDin.DinCode4Value)
        self.checkDin4.setChecked(self.classDin.CheckDin4)
        self.comboUsedIn.addItems(self.classDin.SmUsedIn)
        self.comboUsedIn.setCurrentIndex(self.classDin.UsedInCurrentIndex)
        self.spinLvlUp.setValue(self.classDin.LvlUp)
        self.spinLvlLow.setValue(self.classDin.LvlLow)
        self.lineCmdIn.setText(self.classDin.CmdIN)
        self.lineCmdOut.setText(self.classDin.CmdOUT)
        if self.classRs4851 == '':
            self.classRs4851 = MemoRs485_1()
        self.comboTpSens1.addItems(self.classRs4851.TypeSensor)
        self.comboTpSens1.setCurrentText(self.classRs4851.TypeSensorValue)
        self.checkRqstData1.setChecked(self.classRs4851.Request_Data)
        self.spinAddrs1.setValue(self.classRs4851.Address)
        self.spinStartBit1.setValue(self.classRs4851.StartBit)
        self.spinLen1.setValue(self.classRs4851.Length)
        self.spinAver1.setValue(self.classRs4851.Average)
        self.spinEvnt1.setValue(self.classRs4851.Event)
        if self.classRs4852 == '':
            self.classRs4852 = MemoRs485_2()
        self.comboTpSens2.addItems(self.classRs4852.TypeSensor)
        self.comboTpSens2.setCurrentText(self.classRs4852.TypeSensorValue)
        self.checkRqstData2.setChecked(self.classRs4852.Request_Data)
        self.spinAddrs2.setValue(self.classRs4852.Address)
        self.spinStartBit2.setValue(self.classRs4852.StartBit)
        self.spinLen2.setValue(self.classRs4852.Length)
        self.spinAver2.setValue(self.classRs4852.Average)
        self.spinEvnt2.setValue(self.classRs4852.Event)
        if self.classRs4853 == '':
            self.classRs4853 = MemoRs485_3()
        self.comboTpSens3.addItems(self.classRs4853.TypeSensor)
        self.comboTpSens3.setCurrentText(self.classRs4853.TypeSensorValue)
        self.checkRqstData3.setChecked(self.classRs4853.Request_Data)
        self.spinAddrs3.setValue(self.classRs4853.Address)
        self.spinStartBit3.setValue(self.classRs4853.StartBit)
        self.spinLen3.setValue(self.classRs4853.Length)
        self.spinAver3.setValue(self.classRs4853.Average)
        self.spinEvnt3.setValue(self.classRs4853.Event)
        if self.classRs4854 == '':
            self.classRs4854 = MemoRs485_4()
        self.comboTpSens4.addItems(self.classRs4854.TypeSensor)
        self.comboTpSens4.setCurrentText(self.classRs4854.TypeSensorValue)
        self.checkRqstData4.setChecked(self.classRs4854.Request_Data)
        self.spinAddrs4.setValue(self.classRs4854.Address)
        self.spinStartBit4.setValue(self.classRs4854.StartBit)
        self.spinLen4.setValue(self.classRs4854.Length)
        self.spinAver4.setValue(self.classRs4854.Average)
        self.spinEvnt4.setValue(self.classRs4854.Event)
        if self.classRs4855 == '':
            self.classRs4855 = MemoRs485_5()
        self.comboTpSens5.addItems(self.classRs4855.TypeSensor)
        self.comboTpSens5.setCurrentText(self.classRs4855.TypeSensorValue)
        self.checkRqstData5.setChecked(self.classRs4855.Request_Data)
        self.spinAddrs5.setValue(self.classRs4855.Address)
        self.spinStartBit5.setValue(self.classRs4855.StartBit)
        self.spinLen5.setValue(self.classRs4855.Length)
        self.spinAver5.setValue(self.classRs4855.Average)
        self.spinEvnt5.setValue(self.classRs4855.Event)
        if self.classRs4856 == '':
            self.classRs4856 = MemoRs485_6()
        self.comboTpSens6.addItems(self.classRs4856.TypeSensor)
        self.comboTpSens6.setCurrentText(self.classRs4856.TypeSensorValue)
        self.checkRqstData6.setChecked(self.classRs4856.Request_Data)
        self.spinAddrs6.setValue(self.classRs4856.Address)
        self.spinStartBit6.setValue(self.classRs4856.StartBit)
        self.spinLen6.setValue(self.classRs4856.Length)
        self.spinAver6.setValue(self.classRs4856.Average)
        self.spinEvnt6.setValue(self.classRs4856.Event)
        if self.classRs4857 == '':
            self.classRs4857 = MemoRs485_7()
        self.comboTpSens7.addItems(self.classRs4857.TypeSensor)
        self.comboTpSens7.setCurrentText(self.classRs4857.TypeSensorValue)
        self.checkRqstData7.setChecked(self.classRs4857.Request_Data)
        self.spinAddrs7.setValue(self.classRs4857.Address)
        self.spinStartBit7.setValue(self.classRs4857.StartBit)
        self.spinLen7.setValue(self.classRs4857.Length)
        self.spinAver7.setValue(self.classRs4857.Average)
        self.spinEvnt7.setValue(self.classRs4857.Event)
        if self.classFuel1 == '':
            self.classFuel1 = Fuel1()
        if self.classFuel2 == '':
            self.classFuel2 = Fuel2()
        if self.classFuel3 == '':
            self.classFuel3 = Fuel3()
        if self.classFuel4 == '':
            self.classFuel4 = Fuel4()
        if self.classFuel5 == '':
            self.classFuel5 = Fuel5()
        if self.classRfid1 == '':
            self.classRfid1 = Rfid1()
        if self.classRfid2 == '':
            self.classRfid2 = Rfid2()
        return

    def initTableWidgetCall(self, checkBox, data):
        checkBoxWidget = QWidget()
        layoutCheckBox = QHBoxLayout(checkBoxWidget)
        layoutCheckBox.addWidget(checkBox)
        layoutCheckBox.setAlignment(Qt.AlignCenter)
        layoutCheckBox.setContentsMargins(0, 0, 0, 0)
        checkBox.setChecked(data)
        return checkBoxWidget

    def initReadWriteMenu(self):
        if self.comboType.currentText() == 'TTA' or self.comboType.currentText() == '':
            self.readAllAction.setEnabled(False)
            self.writeAllAction.setEnabled(False)
        elif self.comboType.currentText() == 'TTL/TTU':
            self.readAllAction.setEnabled(True)
            self.writeAllAction.setEnabled(True)
        return
    
    def initTabLabels(self):
        if self.comboType.currentText() == 'TTA' or self.comboType.currentText() == '':
            self.tabs.setTabEnabled(11, False)
            self.tabs.setTabText(11, "Labels")
        elif self.comboType.currentText() == 'TTL/TTU':
            if self.comboTypeSens.currentText() == 'TTU':
                if self.classLabels == '':
                    self.classLabels = LabelsTab()
                self.comboAddCell.clear()
                self.comboAddCell.addItems(self.classLabels.Cells)
                self.comboAddLabel.clear()
                self.comboAddLabel.addItems(self.classLabels.Labels)
                self.comboMarkerEnd.setCurrentText(self.classLabels.Marker)
                self.tabs.setTabEnabled(11, True)
                self.tabs.setTabText(11, "Labels (default)")
            elif self.comboTypeSens.currentText() == 'TTL':
                self.tabs.setTabEnabled(11, False)
                self.tabs.setTabText(11, "Labels")
        return
        
    def initSensViewerTab(self):
        if self.comboType.currentText() == 'TTA' or self.comboType.currentText() == '':
            self.tabs.setTabEnabled(10, False)
            self.tabs.setTabText(10, "Viewer")
        elif self.comboType.currentText() == 'TTL/TTU':
            if self.comboTypeSens.currentText() == 'TTU':
                if self.srvSensViewer == '':
                    self.srvSensViewer = ViewerTab()
                self.tableSensViewer.setRowCount(len(self.srvSensViewer.SensorTable))
                for k in range(len(self.srvSensViewer.SensorValue)):
                    name = self.srvSensViewer.SensorTable[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableSensViewer.setItem(k, 0, qtbl)
                    name = self.srvSensViewer.SensorValue[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableSensViewer.setItem(k, 1, qtbl)
                self.tableSensViewer.resizeColumnsToContents()
                self.tableSensViewer.resizeRowsToContents()
                self.lineRssi.setText(self.srvSensViewer.LineRssi)
                self.lineSatt.setText(self.srvSensViewer.LineSatt)
                self.lineVbort.setText(self.srvSensViewer.LineVBort)
                self.checkRequest.setChecked(self.srvSensViewer.CheckRequest)
                self.tabs.setTabEnabled(10, True)
                self.tabs.setTabText(10, "Viewer (default)")
        elif self.comboTypeSens.currentText() == 'TTL':
            self.tabs.setTabEnabled(10, False)
            self.tabs.setTabText(10, "Viewer")
        return

    def initServicePhone(self, sim_use):
        self.previouseTypeTracker = self.comboType.currentText()
        self.spacClearTable(self.tableServPhone)
        if self.previouseTypeTracker == 'TTL/TTU':
            if sim_use:
                if self.srvPhoneP == '':
                    self.srvPhoneP = ServPhoneP()
                self.tableServPhone.setRowCount(len(self.srvPhoneP.ServicePhone))
                for k in range(len(self.srvPhoneP.ServicePhone)):
                    name = self.srvPhoneP.ServicePhone[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableServPhone.setItem(k, 0, qtbl)
                    name = self.srvPhoneP.ParameterValue[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableServPhone.setItem(k, 1, qtbl)
                self.lineReg.setText(self.srvPhoneP.LineReq)
            else:
                if self.srvPhone == '':
                    self.srvPhone = ServPhone()
                self.tableServPhone.setRowCount(len(self.srvPhone.ServicePhone))
                for k in range(len(self.srvPhone.ServicePhone)):
                    name = self.srvPhone.ServicePhone[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableServPhone.setItem(k, 0, qtbl)
                    name = self.srvPhone.ParameterValue[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableServPhone.setItem(k, 1, qtbl)
                self.lineReg.setText(self.srvPhone.LineReq)

            self.tableServPhone.resizeColumnsToContents()
            self.tableServPhone.resizeRowsToContents()
            self.tabs.setTabText(7, "Service (default)")
            self.lineReg.setEnabled(True)
        else:
            self.tabs.setTabText(7, "Service")
            self.lineReg.setEnabled(False)
        return

    def setListItem(self, lstItems, lstMemo):
        for name in lstItems:
            item = QListWidgetItem(lstMemo)
            check = MyQCheckBox()
            check.setModifiable(False)
            check.setText(name)
            self.listMemo.setItemWidget(item, check)
        return

    def initTabSensors(self):
        if self.comboType.currentText() == 'TTA' or self.comboType.currentText() == '':
            self.tabs.setTabEnabled(8, False)
            self.tabs.setTabText(8, "Sensors")
        elif self.comboType.currentText() == 'TTL/TTU':
            self.listMemo.clear()
            if self.comboTypeSens.currentText() == 'TTL':
                self.setListItem(self.listItemsTtl, self.listMemo)
                self.setEnabledSensorObject(False)
            elif self.comboTypeSens.currentText() == 'TTU':
                self.setListItem(self.listItemsTtu, self.listMemo)
                self.setEnabledSensorObject(True)
            self.tabs.setTabEnabled(8, True)
            self.tabs.setTabText(8, "Sensors (default)")
        return

    @pyqtSlot()
    def OnChangeTypeTrackSens(self):
        if self.comboType.currentText() == 'TTL/TTU':
            self.listMemo.clear()
            if self.comboTypeSens.currentText() == 'TTL':
                self.setListItem(self.listItemsTtl, self.listMemo)
                self.setEnabledSensorObject(False)
                self.btnOpenFileTerm.setEnabled(False)
                self.btnBUpdateTerm.setEnabled(False)
                self.checkHexModeTerm.setEnabled(False)
                self.lineTerm.setEnabled(False)
                self.tabs.setTabEnabled(10, False)
                self.tabs.setTabText(10, "Viewer")
                self.tabs.setTabEnabled(11, False)
                self.tabs.setTabText(11, "Labels")
                self.switchBtn.setEnabled(True)
            elif self.comboTypeSens.currentText() == 'TTU':
                self.setListItem(self.listItemsTtu, self.listMemo)
                self.setEnabledSensorObject(True)
                self.setEnabledSensorObject(True)
                self.tabs.setTabEnabled(10, True)
                self.tabs.setTabText(10, "Viewer (default)")
                self.tabs.setTabEnabled(11, True)
                self.tabs.setTabText(11, "Labels (default)")
                self.initSensViewerTab()
                self.initTabLabels()
                self.switchBtn.setEnabled(False)
                #self.btnOpenFileTerm.setEnabled(True)
                #self.btnBUpdateTerm.setEnabled(True)
                #self.checkHexModeTerm.setEnabled(True)
                #self.lineTerm.setEnabled(True)
        return

    def setEnabledSensorObject(self, state):
        self.comboCan1.setEnabled(state)
        self.comboCan2.setEnabled(state)
        self.comboSource1.setEnabled(state)
        self.spinSource1.setEnabled(state)
        self.comboSource2.setEnabled(state)
        self.spinSource2.setEnabled(state)
        self.comboSource3.setEnabled(state)
        self.spinSource3.setEnabled(state)
        self.checkDin3.setEnabled(state)
        self.checkDin4.setEnabled(state)
        self.comboBounceDin3.setEnabled(state)
        self.comboBounceDin4.setEnabled(state)
        self.comboUsedIn.setEnabled(state)
        self.spinLvlUp.setEnabled(state)
        self.spinLvlLow.setEnabled(state)
        self.lineCmdIn.setEnabled(state)
        self.lineCmdOut.setEnabled(state)
        return

    @pyqtSlot()
    def OnChangeTypeTracker(self):
        self.initTableEvent(self.switchBtn.isChecked())
        self.initTableRegim()
        self.initTableTelematicServer()
        self.initTablePhonesParam()
        self.initTablePhones()
        self.initTextCmdTab()
        self.initTabGps()
        self.initRecordSwtBtn()
        self.spacClearTable(self.tableTrack)
        self.initRecordPinCode()
        self.initResetBtn()
        self.initTableServSett()
        self.initTableGprs(self.switchBtn.isChecked())
        self.initServicePhone(self.switchBtn.isChecked())
        self.initTabSensors()
        self.initSensViewerTab()
        self.initTabLabels()
        self.btnReadParam.setEnabled(False)
        self.btnWriteParam.setEnabled(False)
        self.btnSavingInfo.setEnabled(False)
        self.btnDeleteCom.setEnabled(False)
        self.btnDeleteAllCom.setEnabled(False)
        self.btnReset.setEnabled(False)
        self.switchBtn.setEnabled(False)
        self.btnSendTerm.setEnabled(False)
        self.btnClearTerm.setEnabled(False)
        self.comboTerminal.setEnabled(False)
        self.readAllAction.setEnabled(False)
        self.writeAllAction.setEnabled(False)
        if self.previouseTypeTracker == 'TTA':
            self.comboTypeSens.setVisible(False)
        elif self.previouseTypeTracker == 'TTL/TTU':
            self.comboTypeSens.setVisible(True)
        return

    def initRecordPinCode(self):
        if self.previouseTypeTracker == 'TTA':
            self.pinCode.setEnabled(False)
            self.switchBtn.setEnabled(False)
        elif self.previouseTypeTracker == 'TTL/TTU':
            self.pinCode.setEnabled(True)
        return

    def initResetBtn(self):
        if self.previouseTypeTracker == 'TTA':
            self.btnReset.setEnabled(False)
        return

    def initRecordSwtBtn(self):
        if self.previouseTypeTracker == 'TTA':
            self.switchBtn.setEnabled(False)
        return

    def initTabGps(self):
        self.previouseTypeTracker = self.comboType.currentText()
        self.text_edit.clear()
        if self.previouseTypeTracker == 'TTA':
            self.tabs.setTabText(5, "GPS (default)")
        else:
            self.tabs.setTabText(5, "GPS")
        return

    def initTextCmdTab(self):
        self.previouseTypeTracker = self.comboType.currentText()
        self.lineTxtCmd.clear()
        self.lineTxtRequest.clear()
        if self.previouseTypeTracker == 'TTA':
            if self.classTextCmd == '':
                self.classTextCmd = TextCmd()
            texts = self.classTextCmd.TextParam
            self.lineTxtCmd.appendPlainText(texts)
            if self.classTextRqst == '':
                self.classTextRqst = TextRqst()
            texts = self.classTextRqst.TextParam
            self.lineTxtRequest.appendPlainText(texts)
            self.tabs.setTabText(4, "TextCmd (default)")
        else:
            self.tabs.setTabText(4, "TextCmd")
        return

    def initTableGprs(self, switch):
        self.previouseTypeTracker = self.comboType.currentText()
        self.saveTabTransport(self.previouseTypeTracker, switch)
        self.spacClearTable(self.tableGprs)
        if self.previouseTypeTracker == 'TTL/TTU':
            if switch:
                if self.classGprsP == '':
                    self.classGprsP = GprsSettsP()
                self.tableGprs.setRowCount(len(self.classGprsP.GprsSetting))
                for k in range(len(self.classGprsP.GprsSetting)):
                    name = self.classGprsP.GprsSetting[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableGprs.setItem(k, 0, qtbl)
                    name = self.classGprsP.ParameterValue[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableGprs.setItem(k, 1, qtbl)
                self.tableGprs.resizeColumnsToContents()
                self.tableGprs.resizeRowsToContents()
                self.tabs.setTabText(6, "Transport (default)")
            else:
                if self.classGprs == '':
                    self.classGprs = GprsSetts()
                self.tableGprs.setRowCount(len(self.classGprs.GprsSetting))
                for k in range(len(self.classGprs.GprsSetting)):
                    name = self.classGprs.GprsSetting[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableGprs.setItem(k, 0, qtbl)
                    name = self.classGprs.ParameterValue[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableGprs.setItem(k, 1, qtbl)
                self.tableGprs.resizeColumnsToContents()
                self.tableGprs.resizeRowsToContents()
                self.tabs.setTabText(6, "Transport (default)")
            self.btnOperUp.setEnabled(True)
            self.btnOperDown.setEnabled(True)
            self.comboOperSett.setEditable(False)
        else:
            self.tabs.setTabText(6, "Transport")
            self.btnOperUp.setEnabled(False)
            self.btnOperDown.setEnabled(False)
            self.comboOperSett.setEditable(False)
        return

    def initTableServSett(self):
        self.previouseTypeTracker = self.comboType.currentText()
        self.spacClearTable(self.tableServSett)
        if self.previouseTypeTracker == 'TTL/TTU':
            if self.classServSett == '':
                self.classServSett = ServerSettings()
            self.tableServSett.setRowCount(len(self.classServSett.ServerSettn))
            for k in range(len(self.classServSett.ServerSettn)):
                name = self.classServSett.ServerSettn[k]
                qtbl = QTableWidgetItem(name)
                self.tableServSett.setItem(k, 0, qtbl)
                name = self.classServSett.ParameterValue[k]
                qtbl = QTableWidgetItem(name)
                self.tableServSett.setItem(k, 1, qtbl)
            self.tableServSett.resizeColumnsToContents()
            self.tableServSett.resizeRowsToContents()
            self.tabs.setTabText(6, "Transport (default)")
            self.btnOperUp.setEnabled(True)
            self.btnOperDown.setEnabled(True)
            self.comboOperSett.setEditable(False)
        else:
            self.tabs.setTabText(6, "Transport")
            self.btnOperUp.setEnabled(False)
            self.btnOperDown.setEnabled(False)
            self.comboOperSett.setEditable(False)
        return

    def initComboOpperSetting(self):
        self.comboOperSett.clear()
        if self.classOperSett == '':
            self.classOperSett = OperatorSetting()
        self.gprsSett.clear()
        for obj in self.classOperSett.listOper:
            tmpGprsSet = GprsOperSett()
            tmpGprsSet.name = obj.name
            tmpGprsSet.GprsLogin = obj.GprsLogin
            tmpGprsSet.GprsPass = obj.GprsPass
            tmpGprsSet.dnsServer = obj.dnsServer
            self.comboOperSett.addItem(tmpGprsSet.name)
            self.gprsSett.append(tmpGprsSet)
        if len(self.classOperSett.listOper) > 0:
            self.comboOperSett.setCurrentText(self.classOperSett.ParameterValue)
        return

    def initTablePhones(self):
        self.previouseTypeTracker = self.comboType.currentText()
        self.tablePhones.clearContents()
        self.spacClearTable(self.tablePhones)
        if self.previouseTypeTracker == 'TTA':
            if self.classPhones == '':
                self.classPhones = PhonesValue()
            self.tablePhones.setRowCount(len(self.classPhones.PhonesParam))
            for k in range(len(self.classPhones.PhonesParam)):
                name = self.classPhones.PhonesParam[k]
                qtbl = QTableWidgetItem(name)
                self.tablePhones.setItem(k, 0, qtbl)
                name = self.classPhones.ParameterValue[k]
                qtbl = QTableWidgetItem(name)
                self.tablePhones.setItem(k, 1, qtbl)
            self.tablePhones.resizeColumnsToContents()
            self.tablePhones.resizeRowsToContents()
            self.tabs.setTabText(3, "Phones (default)")
            self.btnAddPhone.setEnabled(True)
            self.btnDelPhone.setEnabled(True)
            self.btnGoPhone.setEnabled(True)
        else:
            self.tabs.setTabText(3, "Phones")
            self.btnAddPhone.setEnabled(False)
            self.btnDelPhone.setEnabled(False)
            self.btnGoPhone.setEnabled(False)
        return

    def initTablePhonesParam(self):
        self.previouseTypeTracker = self.comboType.currentText()
        self.tablePhonesParam.clearContents()
        self.spacClearTable(self.tablePhonesParam)
        if self.previouseTypeTracker == 'TTA':
            if self.classPhoneParam == '':
                self.classPhoneParam = PhonesParam()
            self.tablePhonesParam.setRowCount(len(self.classPhoneParam.PhonesParam))
            for k in range(len(self.classPhoneParam.PhonesParam)):
                name = self.classPhoneParam.PhonesParam[k]
                qtbl = QTableWidgetItem(name)
                self.tablePhonesParam.setItem(k, 0, qtbl)
                name = self.classPhoneParam.ParameterValue[k]
                qtbl = QTableWidgetItem(name)
                self.tablePhonesParam.setItem(k, 1, qtbl)
            self.tablePhonesParam.resizeColumnsToContents()
            self.tablePhonesParam.resizeRowsToContents()
            self.tabs.setTabText(3, "Phones (default)")
            self.btnSetPhone.setEnabled(True)
        else:
            self.tabs.setTabText(3, "Phones")
            self.btnSetPhone.setEnabled(False)
        return

    def initTableTelematicServer(self):
        self.previouseTypeTracker = self.comboType.currentText()
        self.tableTelematicServer.clearContents()
        self.spacClearTable(self.tableTelematicServer)
        if self.previouseTypeTracker == 'TTA':
            if self.teleServer == '':
                self.teleServer = TelematServer()
            self.tableTelematicServer.setRowCount(len(self.teleServer.TelematicServer))
            for k in range(len(self.teleServer.TelematicServer)):
                name = self.teleServer.TelematicServer[k]
                qtbl = QTableWidgetItem(name)
                self.tableTelematicServer.setItem(k, 0, qtbl)
                name = self.teleServer.ParameterValue[k]
                qtbl = QTableWidgetItem(name)
                self.tableTelematicServer.setItem(k, 1, qtbl)
            self.tableTelematicServer.resizeColumnsToContents()
            self.tableTelematicServer.resizeRowsToContents()
            self.tabs.setTabText(2, "Server (default)")
            self.btnGenAccess.setEnabled(True)
            self.btnSaveSetting.setEnabled(True)
        else:
            self.tabs.setTabText(2, "Server")
            self.btnGenAccess.setEnabled(False)
            self.btnSaveSetting.setEnabled(False)
        return

    def initTableServiceSetting(self):
        if self.serviceSett == '':
            self.serviceSett = ServiceSettings()
        self.tableServiceSettings.setRowCount(len(self.serviceSett.ServiceSett))
        for k in range(len(self.serviceSett.ServiceSett)):
            name = self.serviceSett.ServiceSett[k]
            qtbl = QTableWidgetItem(name)
            self.tableServiceSettings.setItem(k, 0, qtbl)
            name = self.serviceSett.ParameterValue[k]
            qtbl = QTableWidgetItem(name)
            self.tableServiceSettings.setItem(k, 1, qtbl)
        self.tableServiceSettings.resizeColumnsToContents()
        self.tableServiceSettings.resizeRowsToContents()
        return

    def initComboRegim(self):
        for obj in self.classRegim.list_apn:
            tmpBaseConf = GprsBaseConfig()
            tmpBaseConf.name = obj['name']
            tmpBaseConf.regim = obj['regim']
            tmpBaseConf.AccessPointName = obj['AccessPointName']
            tmpBaseConf.ApnLogin = obj['ApnLogin']
            tmpBaseConf.ApnPass = obj['ApnPass']
            tmpBaseConf.dnsServ = obj['dnsServ']
            tmpBaseConf.ispPhone = obj['ispPhone']
            tmpBaseConf.ispLogin = obj['ispLogin']
            tmpBaseConf.ipsPassword = obj['ipsPassword']
            self.comboRegim.addItem(tmpBaseConf.name)
            self.gsmGprsSetting.append(tmpBaseConf)
        return

    def initTableRegim(self):
        self.previouseTypeTracker = self.comboType.currentText()
        self.spacClearTable(self.tableRegim)
        if self.previouseTypeTracker == 'TTA':
            if self.classRegim == '':
                self.classRegim = GprsTab()
            self.tableRegim.setRowCount(len(self.classRegim.NameRegim))
            for k in range(len(self.classRegim.NameRegim)):
                name = self.classRegim.NameRegim[k]
                qtbl = QTableWidgetItem(name)
                self.tableRegim.setItem(k, 0, qtbl)
                name = self.classRegim.ValueRegim[k]
                qtbl = QTableWidgetItem(name)
                self.tableRegim.setItem(k, 1, qtbl)
            self.tableRegim.resizeColumnsToContents()
            self.tableRegim.resizeRowsToContents()
            self.tabs.setTabText(1, "GPRS (default)")
            self.btnSetLeft.setEnabled(True)
            self.btnReadRegim.setEnabled(True)
        else:
            self.tabs.setTabText(1, "GPRS")
            self.btnSetLeft.setEnabled(False)
            self.btnReadRegim.setEnabled(False)
        return

    def saveTabSensors(self):
        self.classAin1.AinLimitUp = self.spinLimitUp1.value()
        self.classAin1.AinLimitDown = self.spinLimitDown1.value()
        self.classAin2.AinLimitUp = self.spinLimitUp2.value()
        self.classAin2.AinLimitDown = self.spinLimitDown2.value()
        self.classCan1.WorkCanValue = self.comboWorkCan1.currentText()
        self.classCan1.CanAddress = self.lineCanAddrs1.text()
        self.classCan1.CanMask = self.lineMask1.text()
        self.classCan1.CanStartBit = self.spinStBit1.value()
        self.classCan1.CanLength = self.spinLngth1.value()
        self.classCan1.CanTimeout = self.spinTmout1.value()
        self.classCan2.WorkCanValue = self.comboWorkCan2.currentText()
        self.classCan2.CanAddress = self.lineCanAddrs2.text()
        self.classCan2.CanMask = self.lineMask2.text()
        self.classCan2.CanStartBit = self.spinStBit2.value()
        self.classCan2.CanLength = self.spinLngth2.value()
        self.classCan2.CanTimeout = self.spinTmout2.value()
        self.classCan3.WorkCanValue = self.comboWorkCan3.currentText()
        self.classCan3.CanAddress = self.lineCanAddrs3.text()
        self.classCan3.CanMask = self.lineMask3.text()
        self.classCan3.CanStartBit = self.spinStBit3.value()
        self.classCan3.CanLength = self.spinLngth3.value()
        self.classCan3.CanTimeout = self.spinTmout3.value()
        self.classCount1.RegimValue = self.comboCnt1.currentText()
        self.classCount1.Event1 = self.spinEvent11.value()
        self.classCount1.Event2 = self.spinEvent21.value()
        self.classCount1.SourceValue = self.comboSource1.currentText()
        self.classCount1.Source_En = self.spinSource1.value()
        self.classCount2.RegimValue = self.comboCnt2.currentText()
        self.classCount2.Event1 = self.spinEvent12.value()
        self.classCount2.Event2 = self.spinEvent22.value()
        self.classCount2.SourceValue = self.comboSource2.currentText()
        self.classCount2.Source_En = self.spinSource2.value()
        self.classCount3.RegimValue = self.comboCnt3.currentText()
        self.classCount3.Event1 = self.spinEvent13.value()
        self.classCount3.Event2 = self.spinEvent23.value()
        self.classCount3.SourceValue = self.comboSource3.currentText()
        self.classCount3.Source_En = self.spinSource3.value()
        self.classDin.CheckDin1 = self.checkDin1.checkState()
        self.classDin.CheckDin2 = self.checkDin2.checkState()
        self.classDin.CheckDin3 = self.checkDin3.checkState()
        self.classDin.CheckDin4 = self.checkDin4.checkState()
        self.classDin.DinCode1Value = self.comboBounceDin1.currentText()
        self.classDin.DinCode2Value = self.comboBounceDin2.currentText()
        self.classDin.DinCode3Value = self.comboBounceDin3.currentText()
        self.classDin.DinCode4Value = self.comboBounceDin4.currentText()
        self.classDin.UsedInCurrentIndex = self.comboUsedIn.currentIndex()
        self.classDin.LvlUp = self.spinLvlUp.value()
        self.classDin.LvlLow = self.spinLvlLow.value()
        self.classDin.CmdIN = self.lineCmdIn.text()
        self.classDin.CmdOUT = self.lineCmdOut.text()
        self.classRs4851.TypeSensorValue = self.comboTpSens1.currentText()
        self.classRs4851.Request_Data = self.checkRqstData1.checkState()
        self.classRs4851.Address = self.spinAddrs1.value()
        self.classRs4851.StartBit = self.spinStartBit1.value()
        self.classRs4851.Length = self.spinLen1.value()
        self.classRs4851.Average = self.spinAver1.value()
        self.classRs4851.Event = self.spinEvnt1.value()
        self.classRs4852.TypeSensorValue = self.comboTpSens2.currentText()
        self.classRs4852.Request_Data = self.checkRqstData2.checkState()
        self.classRs4852.Address = self.spinAddrs2.value()
        self.classRs4852.StartBit = self.spinStartBit2.value()
        self.classRs4852.Length = self.spinLen2.value()
        self.classRs4852.Average = self.spinAver2.value()
        self.classRs4852.Event = self.spinEvnt2.value()
        self.classRs4853.TypeSensorValue = self.comboTpSens3.currentText()
        self.classRs4853.Request_Data = self.checkRqstData3.checkState()
        self.classRs4853.Address = self.spinAddrs3.value()
        self.classRs4853.StartBit = self.spinStartBit3.value()
        self.classRs4853.Length = self.spinLen3.value()
        self.classRs4853.Average = self.spinAver3.value()
        self.classRs4853.Event = self.spinEvnt3.value()
        self.classRs4854.TypeSensorValue = self.comboTpSens4.currentText()
        self.classRs4854.Request_Data = self.checkRqstData4.checkState()
        self.classRs4854.Address = self.spinAddrs4.value()
        self.classRs4854.StartBit = self.spinStartBit4.value()
        self.classRs4854.Length = self.spinLen4.value()
        self.classRs4854.Average = self.spinAver4.value()
        self.classRs4854.Event = self.spinEvnt4.value()
        self.classRs4855.TypeSensorValue = self.comboTpSens5.currentText()
        self.classRs4855.Request_Data = self.checkRqstData5.checkState()
        self.classRs4855.Address = self.spinAddrs5.value()
        self.classRs4855.StartBit = self.spinStartBit5.value()
        self.classRs4855.Length = self.spinLen5.value()
        self.classRs4855.Average = self.spinAver5.value()
        self.classRs4855.Event = self.spinEvnt5.value()
        self.classRs4856.TypeSensorValue = self.comboTpSens6.currentText()
        self.classRs4856.Request_Data = self.checkRqstData6.checkState()
        self.classRs4856.Address = self.spinAddrs6.value()
        self.classRs4856.StartBit = self.spinStartBit6.value()
        self.classRs4856.Length = self.spinLen6.value()
        self.classRs4856.Average = self.spinAver6.value()
        self.classRs4856.Event = self.spinEvnt6.value()
        self.classRs4857.TypeSensorValue = self.comboTpSens7.currentText()
        self.classRs4857.Request_Data = self.checkRqstData7.checkState()
        self.classRs4857.Address = self.spinAddrs7.value()
        self.classRs4857.StartBit = self.spinStartBit7.value()
        self.classRs4857.Length = self.spinLen7.value()
        self.classRs4857.Average = self.spinAver7.value()
        self.classRs4857.Event = self.spinEvnt7.value()
        self.sensors_Tab.SensCode = self.lineSnsCode.text()
        self.sensors_Tab.SpeedValueRs485 = self.comboRs485.currentText()
        self.sensors_Tab.CAN1Value = self.comboCan1.currentText()
        self.sensors_Tab.CAN2Value = self.comboCan2.currentText()
        self.sensors_Tab.AccSensitivity = self.spinAcc.value()
        self.SaveCurrentSensors(self.currentSensor)
        return

    def saveTabLabels(self):
        lst_cells = [self.comboAddCell.itemText(i) for i in range(self.comboAddCell.count())]
        self.classLabels.setCells(lst_cells)
        lst_labels = [self.comboAddLabel.itemText(i) for i in range(self.comboAddLabel.count())]
        self.classLabels.setLabels(lst_labels)
        self.classLabels.setMarker(self.comboMarkerEnd.currentText())
        return

    def saveTabViewer(self):
        self.srvSensViewer.LineRssi = self.lineRssi.text()
        self.srvSensViewer.LineSatt = self.lineSatt.text()
        self.srvSensViewer.LineVBort = self.lineVbort.text()
        self.srvSensViewer.CheckRequest = self.checkRequest.checkState()
        listValue = []
        for row in range(self.tableSensViewer.rowCount()):
            itemName = self.tableSensViewer.item(row, 1)
            if itemName != None:
                name = itemName.text()
            else:
                name = ""
            listValue.append(name)
        self.srvSensViewer.SensorValue = listValue
        return

    def saveTerminalTab(self):
        self.terminal_tab.CommTerm = [self.comboTerminal.itemText(i) for i in range(self.comboTerminal.count())]
        self.terminal_tab.LineTermData = self.lineTerm.text()
        self.terminal_tab.HexModeTerm = self.checkHexModeTerm.isChecked()
        self.terminal_tab.TextTerm = self.text_term.toPlainText()
        return

    def saveTabTextCmd(self, type_tracker):
        if type_tracker == 'TTA' or type_tracker == 'TTL/TTU':
            texts = self.lineTxtCmd.toPlainText()
            self.classTextCmd.setTextValue(texts)
            texts = self.lineTxtRequest.toPlainText()
            self.classTextRqst.setTextValue(texts)
        return

    def saveTabTransport(self, type_tracker, switch):
        if type_tracker == 'TTL/TTU':
            if switch:
                for row in range(self.tableGprs.rowCount()):
                    itemName = self.tableGprs.item(row, 1)
                    name = itemName.text()
                    self.classGprs.setParameterValue(name, row)
            else:
                for row in range(self.tableGprs.rowCount()):
                    itemName = self.tableGprs.item(row, 1)
                    name = itemName.text()
                    self.classGprsP.setParameterValue(name, row)

            for row in range(self.tableServSett.rowCount()):
                itemName = self.tableServSett.item(row, 1)
                name = itemName.text()
                self.classServSett.setParameterValue(name, row)

            self.classOperSett.clearList()
            for obj in self.gprsSett:
                self.classOperSett.setValueOper(obj)

            self.classOperSett.ParameterValue = self.comboOperSett.currentText()
        return

    def saveTabPhones(self, type_tracker):
        if type_tracker == 'TTA' or type_tracker == 'TTL/TTU':
            for row in range(self.tablePhonesParam.rowCount()):
                itemName = self.tablePhonesParam.item(row, 1)
                name = itemName.text()
                self.classPhoneParam.setParameterValue(name, row)
        return

    def saveTabPhonesValue(self, type_tracker):
        if type_tracker == 'TTA' or type_tracker == 'TTL/TTU':
            if self.tablePhones.rowCount() > 0:
                for row in range(self.tablePhones.rowCount()):
                    itemName = self.tablePhones.item(row, 0)
                    name = itemName.text()
                    self.classPhones.setNameValue(name, row)
                    itemName = self.tablePhones.item(row, 1)
                    name = itemName.text()
                    self.classPhones.setParameterValue(name, row)
                self.classPhones.truncatePhones()
        return

    def saveTabServer(self, type_tracker):
        if type_tracker == 'TTA' or type_tracker == 'TTL/TTU':
            for row in range(self.tableTelematicServer.rowCount()):
                itemName = self.tableTelematicServer.item(row, 1)
                name = itemName.text()
                self.teleServer.setParameterValue(name, row)

            for row in range(self.tableServiceSettings.rowCount()):
                itemName = self.tableServiceSettings.item(row, 1)
                name = itemName.text()
                self.serviceSett.setParameterValue(name, row)
        return

    def saveTabGprs(self, type_tracker):
        if type_tracker == 'TTA':
            for row in range(self.tableRegim.rowCount()):
                itemName = self.tableRegim.item(row, 1)
                name = itemName.text()
                self.classRegim.setValueRegim(name, row)
            for obj in self.gsmGprsSetting:
                apn = {}
                apn['name'] = obj.name
                apn['regim'] = obj.regim
                apn['AccessPointName'] = obj.AccessPointName
                apn['ApnLogin'] = obj.ApnLogin
                apn['ApnPass'] = obj.ApnPass
                apn['dnsServ'] = obj.dnsServ
                apn['ispPhone'] = obj.ispPhone
                apn['ispLogin'] = obj.ispLogin
                apn['ipsPassword'] = obj.ipsPassword
                self.classRegim.setValueApn(apn)
        return

    def saveTabService(self, sim_use):
        if self.comboType.currentText() == 'TTL/TTU':
            if sim_use:
                for row in range(self.tableServPhone.rowCount()):
                    itemName = self.tableServPhone.item(row, 1)
                    name = itemName.text()
                    self.srvPhoneP.setParameterValue(name, row)
                self.srvPhoneP.LineReq = self.lineReg.text()
            else:
                for row in range(self.tableServPhone.rowCount()):
                    itemName = self.tableServPhone.item(row, 1)
                    name = itemName.text()
                    self.srvPhone.setParameterValue(name, row)
                self.srvPhone.LineReq = self.lineReg.text()
        return

    def saveTabEvents(self, type_tracker, switch=False):
        if type_tracker == 'TTA':
            for k in range(len(self.eventsTta.ParamName)):
                checkBoxWidget = self.tableEvents.cellWidget(k, 3)
                checkBox = checkBoxWidget.layout().itemAt(0).widget()
                self.eventsTta.setFixingPoint(checkBox.isChecked(), k)
                checkBoxWidget = self.tableEvents.cellWidget(k, 4)
                checkBox = checkBoxWidget.layout().itemAt(0).widget()
                self.eventsTta.setSentServer(checkBox.isChecked(), k)
                checkBoxWidget = self.tableEvents.cellWidget(k, 5)
                checkBox = checkBoxWidget.layout().itemAt(0).widget()
                self.eventsTta.setSmsPhone(checkBox.isChecked(), k)
        elif type_tracker == 'TTL/TTU':
            if switch:
                for k in range(len(self.eventsTtlu.uParamName)):
                    checkBoxWidget = self.tableEvents.cellWidget(k, 3)
                    checkBox = checkBoxWidget.layout().itemAt(0).widget()
                    self.eventsTtlu.usetFixingPoint(checkBox.isChecked(), k)
                    checkBoxWidget = self.tableEvents.cellWidget(k, 4)
                    checkBox = checkBoxWidget.layout().itemAt(0).widget()
                    self.eventsTtlu.usetSentServer(checkBox.isChecked(), k)
            else:
                for k in range(len(self.eventsTtl.ParamName)):
                    checkBoxWidget = self.tableEvents.cellWidget(k, 3)
                    checkBox = checkBoxWidget.layout().itemAt(0).widget()
                    self.eventsTtl.setFixingPoint(checkBox.isChecked(), k)
                    checkBoxWidget = self.tableEvents.cellWidget(k, 4)
                    checkBox = checkBoxWidget.layout().itemAt(0).widget()
                    self.eventsTtl.setSentServer(checkBox.isChecked(), k)
        return

    def initTableEvent(self, swtch):
        nameTab = self.tabs.tabText(0)
        if "default" in nameTab:
            self.saveTabEvents(self.previouseTypeTracker, swtch)
        self.previouseTypeTracker = self.comboType.currentText()
        self.spacClearTable(self.tableEvents)
        if self.previouseTypeTracker == 'TTA':
            if self.eventsTta == '':
                self.eventsTta = EventsTabTta()

            self.tableEvents.setColumnHidden(5, False)
            self.tableEvents.setColumnHidden(7, False)

            self.tableEvents.setRowCount(len(self.eventsTta.ParamName))
            for k in range(len(self.eventsTta.ParamName)):
                name = self.eventsTta.ParamName[k]
                qtbl = QTableWidgetItem(name)
                self.tableEvents.setItem(k, 0, qtbl)
                param = self.eventsTta.ParamValue[k]
                qtbl = QTableWidgetItem(param)
                self.tableEvents.setItem(k, 1, qtbl)
                unit = self.eventsTta.Unit[k]
                qtbl = QTableWidgetItem(unit)
                self.tableEvents.setItem(k, 2, qtbl)
                self.tableEvents.setCellWidget(k, 3,
                                               self.initTableWidgetCall(QCheckBox(), self.eventsTta.FixingPoint[k]))
                self.tableEvents.setCellWidget(k, 4,
                                               self.initTableWidgetCall(QCheckBox(), self.eventsTta.SentServer[k]))
                self.tableEvents.setCellWidget(k, 5, self.initTableWidgetCall(QCheckBox(), self.eventsTta.SmsPhone[k]))
                name = self.eventsTta.RecommendedValue[k]
                qtbl = QTableWidgetItem(name)
                self.tableEvents.setItem(k, 6, qtbl)
                name = self.eventsTta.ValidValues[k]
                qtbl = QTableWidgetItem(name)
                self.tableEvents.setItem(k, 7, qtbl)
            self.tableEvents.resizeColumnsToContents()
            self.tableEvents.resizeRowsToContents()
            self.tabs.setTabText(0, "Events (default)")
        elif self.previouseTypeTracker == 'TTL/TTU':
            if self.switchBtn.isChecked():
                if self.eventsTtlu == '':
                    self.eventsTtlu = EventsTabTtlU()
                self.tableEvents.setColumnHidden(5, True)
                self.tableEvents.setColumnHidden(7, True)
                self.tableEvents.setRowCount(len(self.eventsTtlu.uParamName))
                for k in range(len(self.eventsTtlu.uParamName)):
                    name = self.eventsTtlu.uParamName[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableEvents.setItem(k, 0, qtbl)
                    param = self.eventsTtlu.uParamValue[k]
                    qtbl = QTableWidgetItem(param)
                    self.tableEvents.setItem(k, 1, qtbl)
                    unit = self.eventsTtlu.uUnit[k]
                    qtbl = QTableWidgetItem(unit)
                    self.tableEvents.setItem(k, 2, qtbl)
                    self.tableEvents.setCellWidget(k, 3, self.initTableWidgetCall(QCheckBox(),
                                                                                  self.eventsTtlu.uFixingPoint[k]))
                    self.tableEvents.setCellWidget(k, 4, self.initTableWidgetCall(QCheckBox(),
                                                                                  self.eventsTtlu.uSentServer[k]))
                    self.tableEvents.setCellWidget(k, 5, self.initTableWidgetCall(QCheckBox(), False))
                    name = self.eventsTtlu.uRecommendedValue[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableEvents.setItem(k, 6, qtbl)
                self.tableEvents.resizeColumnsToContents()
                self.tableEvents.resizeRowsToContents()
                self.tabs.setTabText(0, "Events (default)")
            else:
                if self.eventsTtl == '':
                    self.eventsTtl = EventsTabTtl()
                self.tableEvents.setColumnHidden(5, True)
                self.tableEvents.setColumnHidden(7, True)
                self.tableEvents.setRowCount(len(self.eventsTtl.ParamName))
                for k in range(len(self.eventsTtl.ParamName)):
                    name = self.eventsTtl.ParamName[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableEvents.setItem(k, 0, qtbl)
                    param = self.eventsTtl.ParamValue[k]
                    qtbl = QTableWidgetItem(param)
                    self.tableEvents.setItem(k, 1, qtbl)
                    unit = self.eventsTtl.Unit[k]
                    qtbl = QTableWidgetItem(unit)
                    self.tableEvents.setItem(k, 2, qtbl)
                    self.tableEvents.setCellWidget(k, 3,
                                                   self.initTableWidgetCall(QCheckBox(), self.eventsTtl.FixingPoint[k]))
                    self.tableEvents.setCellWidget(k, 4,
                                                   self.initTableWidgetCall(QCheckBox(), self.eventsTtl.SentServer[k]))
                    self.tableEvents.setCellWidget(k, 5, self.initTableWidgetCall(QCheckBox(), False))
                    name = self.eventsTtl.RecommendedValue[k]
                    qtbl = QTableWidgetItem(name)
                    self.tableEvents.setItem(k, 6, qtbl)
                self.tableEvents.resizeColumnsToContents()
                self.tableEvents.resizeRowsToContents()
                self.tabs.setTabText(0, "Events (default)")
        else:
            self.tabs.setTabText(0, "Events")
        return

    def setFuelRfid(self):
        if self.btnFuelRfid.isChecked():
            self.btnFuelRfid.setText("RFID")
        else:
            self.btnFuelRfid.setText("FUEL")
        return

    def GetListSensors(self):
        fuel = []
        rfid = []
        rs485Set = comA1.TRs485Setting()
        for sens in rs485Set.llsTypeSens:
            if sens.address > 0 and sens.address < 255:
                if sens.type == comA1.TYPE_LLS.OMNICOMM_RES:
                    if not sens.address in fuel:
                        fuel.append(sens.address)
                elif sens.type == comA1.TYPE_LLS.SOVA_RES:
                    if not sens.address in rfid:
                        rfid.append(sens.address)
        ret = {}
        if self.btnFuelRfid.isChecked():
            for addr in rfid:
                ret[addr] = comA1.typeLssStr[1] # "RFID"
        else:
            for addr in fuel:
                ret[addr] = comA1.typeLssStr[0] # "FUEL"

        return ret

    @pyqtSlot()
    def OnGetTypeLls(self):
        if self.comboType.currentText() == 'TTL/TTU':
            if self.comboTypeSens.currentText() == 'TTU':
                if self.TestPin():
                    return
                sensList = self.GetListSensors()
                self.btngetTypeLLS.setEnabled(False)
                self.spacClearTable(self.tableTypeLls)
                start = self.prefixCmd + self.pinCode.text() + ","
                bufferCommand = []
                for key in sensList:
                    val = comA1.LlsSensor.GetReqToTypeSensor(key)
                    cmd = start + "CMD485=" + comA1.MyConverter.GetHexString(val)
                    bufferCommand.append(cmd)
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnGetTypeLls", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText(
                            "OnGetTypeLls: Get type Lls (RS485) sensors settings from teracker " + select_identifier)
                        idcomm = 211
                        id_chain = int(time.time())
                        self.SendStringToPort(bufferCommand, select_identifier, nameclient, idclient, nameproto, 1,
                                              "read", "Viewer", idcomm, id_chain, self.pinCode.text(), self.comboTypeSens.currentText())
                    else:
                        showError("OnGetTypeLls", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
                self.btngetTypeLLS.setEnabled(True)
        return

    @pyqtSlot()
    def OnFindWire(self):
        if self.comboType.currentText() == 'TTL/TTU':
            if self.comboTypeSens.currentText() == 'TTU':
                if self.TestPin():
                    return
                self.btnFindWire.setEnabled(False)
                self.spacClearTable(self.tableSensWire)
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnFindWire", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText(
                            "OnFindWire: Get type Wire sensors settings from teracker " + select_identifier)
                        idcomm = 213
                        id_chain = int(time.time())
                        self.SendStringToPort([self.prefixCmd + self.pinCode.text() + "," + "WIRE,"], select_identifier, nameclient, idclient, nameproto, 1,
                                              "read", "Viewer", idcomm, id_chain, self.pinCode.text(), self.comboTypeSens.currentText())
                    else:
                        showError("OnFindWire", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
                self.btnFindWire.setEnabled(True)
        return
        

    def checkBoxAutoRequest_CheckedChanged(self):
        if self.checkRequest.isChecked():
            self.timerViewerRequest.Start()
        else:
            self.timerViewerRequest.Stop()
        return

    @pyqtSlot()
    def OnRequest(self):
        if self.comboType.currentText() == 'TTL/TTU':
            if self.comboTypeSens.currentText() == 'TTU':
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnRequest", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText("OnRequest: Request sensors settings from teracker " + select_identifier)
                        if self.TestPin():
                            self.checkRequest.setChecked(False)
                            self.checkBoxAutoRequest_CheckedChanged()
                            return
                        self.baseCmd = self.prefixCmd + self.pinCode.text() + ","
                        cmd = [self.baseCmd + "SVIEW1,SVIEW0,"]
                        idcomm = 209
                        id_chain = int(time.time())
                        self.SendStringToPort(cmd, select_identifier, nameclient, idclient, nameproto, 1,
                                      "read", "Viewer", idcomm, id_chain, self.pinCode.text(), self.comboTypeSens.currentText())
                    else:
                        showError("OnRequest", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
        return

    @pyqtSlot()
    def OnClearTerm(self):
        if showInfo("OnClearTerm", "Хотите почистить Terminal?", "Реально?") == self.OK_MSG:
            self.text_term.clear()
        return

    @pyqtSlot()
    def OnBeginUpdateTerm(self):
        pass

    @pyqtSlot()
    def OnOpenUpdateFileTerm(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        self.fileTermName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                           "All Files (*);;Text Files (*.txt)", options=options)
        if self.fileTermName:
            self.lineTerm.setText(self.fileTermName)
            self.lineTerm.setToolTip(self.fileTermName)
        return

    @pyqtSlot()
    def OnSendTerm(self):
        command = self.comboTerminal.currentText()
        buff = command.split(",")
        if "$RCSTT" == buff[0] and len(buff[1]) > 0 and len(buff[2]) > 0:
            index = self.comboTerminal.findText(command, QtCore.Qt.MatchFixedString)
            if index == -1:
                self.comboTerminal.addItem(command)
            selected = self.tableTrack.selectionModel().selectedRows()
            if len(selected) > 0:
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        sendCmdList, idcomm = self.commandA1.SendTerminalCommand(command)
                        id_chain = int(time.time())
                        self.SendStringToPort([sendCmdList], select_identifier, nameclient, idclient, nameproto, 1,
                                          "read", "Terminal", idcomm, id_chain, self.pinCode.text(), self.comboTypeSens.currentText())
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                               "OnSendTerm: " + "send command " + command + " from terminal to tracker " + select_identifier + \
                               ", client: " + nameclient
                        self.lineParam.appendPlainText(slog)
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                                       time.localtime(int(time.time())))) + "] " + "Tx<<: " + command
                        self.text_term.appendPlainText(slog)
                    else:
                        showError("OnSendTerm", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
            else:
                showError("OnViewLabel", "Нет трекера выделенного для указанной операции!",
                          "Выделите трекер в таблице трекеров")
        return

    def InitalStackWidgetSensors(self):
        self.stackWidgetSensors = QStackedWidget(self)
        self.hlaySensorsDown = QHBoxLayout(self)
        self.hlaySensorsDown.addWidget(self.listMemo)
        self.hlaySensorsDown.addWidget(self.stackWidgetSensors)
        self.stackWidgetSensors.setMinimumWidth(500)
        self.widgetListWidgetDin = QWidget(self)
        self.widgetListWidgetDin.setLayout(self.hlayVDin1All)
        self.scrollDin.setWidget(self.widgetListWidgetDin)
        self.stackWidgetSensors.addWidget(self.scrollDin)
        self.widgetListWidgetCount1 = QWidget(self)
        self.widgetListWidgetCount1.setLayout(self.vlaySource1)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetCount1)
        self.widgetListWidgetCount2 = QWidget(self)
        self.widgetListWidgetCount2.setLayout(self.vlaySource2)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetCount2)
        self.widgetListWidgetCount3 = QWidget(self)
        self.widgetListWidgetCount3.setLayout(self.vlaySource3)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetCount3)
        self.widgetListWidgetCan1 = QWidget(self)
        self.widgetListWidgetCan1.setLayout(self.vlayCan1)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetCan1)
        self.widgetListWidgetCan2 = QWidget(self)
        self.widgetListWidgetCan2.setLayout(self.vlayCan2)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetCan2)
        self.widgetListWidgetCan3 = QWidget(self)
        self.widgetListWidgetCan3.setLayout(self.vlayCan3)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetCan3)
        self.widgetListWidgetAin1 = QWidget(self)
        self.widgetListWidgetAin1.setLayout(self.vlayAin1)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetAin1)
        self.widgetListWidgetAin2 = QWidget(self)
        self.widgetListWidgetAin2.setLayout(self.vlayAin2)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetAin2)
        self.widgetListWidgetRs4851 = QWidget(self)
        self.widgetListWidgetRs4851.setLayout(self.vlayRs4851)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetRs4851)
        self.widgetListWidgetRs4852 = QWidget(self)
        self.widgetListWidgetRs4852.setLayout(self.vlayRs4852)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetRs4852)
        self.widgetListWidgetRs4853 = QWidget(self)
        self.widgetListWidgetRs4853.setLayout(self.vlayRs4853)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetRs4853)
        self.widgetListWidgetRs4854 = QWidget(self)
        self.widgetListWidgetRs4854.setLayout(self.vlayRs4854)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetRs4854)
        self.widgetListWidgetRs4855 = QWidget(self)
        self.widgetListWidgetRs4855.setLayout(self.vlayRs4855)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetRs4855)
        self.widgetListWidgetRs4856 = QWidget(self)
        self.widgetListWidgetRs4856.setLayout(self.vlayRs4856)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetRs4856)
        self.widgetListWidgetRs4857 = QWidget(self)
        self.widgetListWidgetRs4857.setLayout(self.vlayRs4857)
        self.stackWidgetSensors.addWidget(self.widgetListWidgetRs4857)
        self.widgetFuelRoot = QWidget(self)
        self.widgetFuelRoot.setLayout(self.hlayVFuel)
        self.stackWidgetSensors.addWidget(self.widgetFuelRoot)
        self.widgetRfidRoot = QWidget(self)
        self.widgetRfidRoot.setLayout(self.hlayVRfid)
        self.stackWidgetSensors.addWidget(self.widgetRfidRoot)
        self.stackWidgetSensors.setCurrentIndex(0)
        return

    def SaveCurrentSensors(self, nameItem):
        if nameItem == 'FUEL 1':
            self.classFuel1.Address = self.pinAddress.value()
            self.classFuel1.StartBit = self.spinStartBit.value()
            self.classFuel1.Length = self.spin_Length.value()
            self.classFuel1.Average = self.spinAverage.value()
        elif nameItem == 'FUEL 2':
            self.classFuel2.Address = self.pinAddress.value()
            self.classFuel2.StartBit = self.spinStartBit.value()
            self.classFuel2.Length = self.spin_Length.value()
            self.classFuel2.Average = self.spinAverage.value()
        elif nameItem == 'FUEL 3':
            self.classFuel3.Address = self.pinAddress.value()
            self.classFuel3.StartBit = self.spinStartBit.value()
            self.classFuel3.Length = self.spin_Length.value()
            self.classFuel3.Average = self.spinAverage.value()
        elif nameItem == 'FUEL 4':
            self.classFuel4.Address = self.pinAddress.value()
            self.classFuel4.StartBit = self.spinStartBit.value()
            self.classFuel4.Length = self.spin_Length.value()
            self.classFuel4.Average = self.spinAverage.value()
        elif nameItem == 'FUEL 5':
            self.classFuel5.Address = self.pinAddress.value()
            self.classFuel5.StartBit = self.spinStartBit.value()
            self.classFuel5.Length = self.spin_Length.value()
            self.classFuel5.Average = self.spinAverage.value()
        elif nameItem == 'RFID 1':
            self.classRfid1.Address = self.spinAddrRfid.value()
            self.classRfid1.StartBit = self.spinStartBitRfid.value()
            self.classRfid1.Length = self.spinLenRfid.value()
        elif nameItem == 'RFID 2':
            self.classRfid2.Address = self.spinAddrRfid.value()
            self.classRfid2.StartBit = self.spinStartBitRfid.value()
            self.classRfid2.Length = self.spinLenRfid.value()
        return

    @pyqtSlot()
    def OnListMemoClick(self):
        if self.comboType.currentText() == 'TTL/TTU':
            if self.comboTypeSens.currentText() == 'TTL':
                objectItem = self.listMemo.itemWidget(self.listMemo.currentItem())
                nameItem = objectItem.text()
                if nameItem == 'DIN':
                    self.stackWidgetSensors.setCurrentIndex(0)
                elif nameItem == 'COUNT 1':
                    self.stackWidgetSensors.setCurrentIndex(1)
                elif nameItem == 'COUNT 2':
                    self.stackWidgetSensors.setCurrentIndex(2)
                elif nameItem == 'COUNT 3':
                    self.stackWidgetSensors.setCurrentIndex(3)
                elif nameItem == 'AIN 1':
                    self.stackWidgetSensors.setCurrentIndex(7)
                elif nameItem == 'AIN 2':
                    self.stackWidgetSensors.setCurrentIndex(8)
                elif nameItem == 'FUEL 1':
                    self.SaveCurrentSensors(self.currentSensor)
                    self.stackWidgetSensors.setCurrentIndex(16)
                    self.labelFuelRec.setText("FUEL1:")
                    self.pinAddress.setValue(self.classFuel1.Address)
                    self.spinStartBit.setValue(self.classFuel1.StartBit)
                    self.spin_Length.setValue(self.classFuel1.Length)
                    self.spinAverage.setValue(self.classFuel1.Average)
                    self.currentSensor = nameItem
                elif nameItem == 'FUEL 2':
                    self.SaveCurrentSensors(self.currentSensor)
                    self.stackWidgetSensors.setCurrentIndex(16)
                    self.labelFuelRec.setText("FUEL2:")
                    self.pinAddress.setValue(self.classFuel2.Address)
                    self.spinStartBit.setValue(self.classFuel2.StartBit)
                    self.spin_Length.setValue(self.classFuel2.Length)
                    self.spinAverage.setValue(self.classFuel2.Average)
                    self.currentSensor = nameItem
                elif nameItem == 'FUEL 3':
                    self.SaveCurrentSensors(self.currentSensor)
                    self.stackWidgetSensors.setCurrentIndex(16)
                    self.labelFuelRec.setText("FUEL3:")
                    self.pinAddress.setValue(self.classFuel3.Address)
                    self.spinStartBit.setValue(self.classFuel3.StartBit)
                    self.spin_Length.setValue(self.classFuel3.Length)
                    self.spinAverage.setValue(self.classFuel3.Average)
                    self.currentSensor = nameItem
                elif nameItem == 'FUEL 4':
                    self.SaveCurrentSensors(self.currentSensor)
                    self.stackWidgetSensors.setCurrentIndex(16)
                    self.labelFuelRec.setText("FUEL4:")
                    self.pinAddress.setValue(self.classFuel4.Address)
                    self.spinStartBit.setValue(self.classFuel4.StartBit)
                    self.spin_Length.setValue(self.classFuel4.Length)
                    self.spinAverage.setValue(self.classFuel4.Average)
                    self.currentSensor = nameItem
                elif nameItem == 'FUEL 5':
                    self.SaveCurrentSensors(self.currentSensor)
                    self.stackWidgetSensors.setCurrentIndex(16)
                    self.labelFuelRec.setText("FUEL5:")
                    self.pinAddress.setValue(self.classFuel5.Address)
                    self.spinStartBit.setValue(self.classFuel5.StartBit)
                    self.spin_Length.setValue(self.classFuel5.Length)
                    self.spinAverage.setValue(self.classFuel5.Average)
                    self.currentSensor = nameItem
                elif nameItem == 'RFID 1':
                    self.SaveCurrentSensors(self.currentSensor)
                    self.stackWidgetSensors.setCurrentIndex(17)
                    self.labelRfidRec.setText("RFID1:")
                    self.spinAddrRfid.setValue(self.classRfid1.Address)
                    self.spinStartBitRfid.setValue(self.classRfid1.StartBit)
                    self.spinLenRfid.setValue(self.classRfid1.Length)
                    self.currentSensor = nameItem
                elif nameItem == 'RFID 2':
                    self.SaveCurrentSensors(self.currentSensor)
                    self.stackWidgetSensors.setCurrentIndex(17)
                    self.labelRfidRec.setText("RFID2:")
                    self.spinAddrRfid.setValue(self.classRfid2.Address)
                    self.spinStartBitRfid.setValue(self.classRfid2.StartBit)
                    self.spinLenRfid.setValue(self.classRfid2.Length)
                    self.currentSensor = nameItem
            elif self.comboTypeSens.currentText() == 'TTU':
                objectItem = self.listMemo.itemWidget(self.listMemo.currentItem())
                nameItem = objectItem.text()
                if nameItem == 'DIN':
                    self.stackWidgetSensors.setCurrentIndex(0)
                elif nameItem == 'COUNT 1':
                    self.stackWidgetSensors.setCurrentIndex(1)
                elif nameItem == 'COUNT 2':
                    self.stackWidgetSensors.setCurrentIndex(2)
                elif nameItem == 'COUNT 3':
                    self.stackWidgetSensors.setCurrentIndex(3)
                elif nameItem == 'CAN 1':
                    self.stackWidgetSensors.setCurrentIndex(4)
                elif nameItem == 'CAN 2':
                    self.stackWidgetSensors.setCurrentIndex(5)
                elif nameItem == 'CAN 3':
                    self.stackWidgetSensors.setCurrentIndex(6)
                elif nameItem == 'AIN 1':
                    self.stackWidgetSensors.setCurrentIndex(7)
                elif nameItem == 'AIN 2':
                    self.stackWidgetSensors.setCurrentIndex(8)
                elif nameItem == 'RS485 1':
                    self.stackWidgetSensors.setCurrentIndex(9)
                elif nameItem == 'RS485 2':
                    self.stackWidgetSensors.setCurrentIndex(10)
                elif nameItem == 'RS485 3':
                    self.stackWidgetSensors.setCurrentIndex(11)
                elif nameItem == 'RS485 4':
                    self.stackWidgetSensors.setCurrentIndex(12)
                elif nameItem == 'RS485 5':
                    self.stackWidgetSensors.setCurrentIndex(13)
                elif nameItem == 'RS485 6':
                    self.stackWidgetSensors.setCurrentIndex(14)
                elif nameItem == 'RS485 7':
                    self.stackWidgetSensors.setCurrentIndex(15)
        return

    @pyqtSlot()
    def OnDownOperSett(self):
        tmpGprsConf = GprsOperSett()
        if len(self.tableGprs.item(0, 1).text()) == 0:
            showError("OnDownOperSett. Error: Имя доступа не может быть пустым!", "Установите имя доступа", "")
            return
        tmpGprsConf.name = self.tableGprs.item(0, 1).text()
        tmpGprsConf.GprsLogin = self.tableGprs.item(1, 1).text()
        tmpGprsConf.GprsPass = self.tableGprs.item(2, 1).text()
        tmpGprsConf.dnsServer = self.tableGprs.item(3, 1).text()

        index = self.comboOperSett.findText(tmpGprsConf.name)
        if index == -1:
            self.comboOperSett.addItem(tmpGprsConf.name)
            self.gprsSett.append(tmpGprsConf)
            self.comboOperSett.setCurrentText(tmpGprsConf.name)
        else:
            if showInfo("OnDownOperSett",
                        "Это имя доступа '" + tmpGprsConf.name + "' уже существует, хотите перезаписать?",
                        "Реально?") == self.OK_MSG:
                self.comboOperSett.removeItem(index)
                del self.gprsSett[index]
                self.comboOperSett.addItem(tmpGprsConf.name)
                self.gprsSett.append(tmpGprsConf)
        return

    @pyqtSlot()
    def OnUpOperSett(self):
        if len(self.gprsSett) == 0:
            return

        if self.switchBtn.isChecked():
            index = self.comboOperSett.currentIndex()
            tmpGprsConf = self.gprsSett[index]
            self.spacClearTable(self.tableGprs)
            self.tableGprs.setRowCount(len(self.classGprsP.GprsSetting))

            name = self.classGprsP.GprsSetting[0]
            qtbl = QTableWidgetItem(name)
            self.tableGprs.setItem(0, 0, qtbl)
            qtbl = QTableWidgetItem(tmpGprsConf.name)
            self.tableGprs.setItem(0, 1, qtbl)

            name = self.classGprsP.GprsSetting[1]
            qtbl = QTableWidgetItem(name)
            self.tableGprs.setItem(1, 0, qtbl)
            qtbl = QTableWidgetItem(tmpGprsConf.GprsLogin)
            self.tableGprs.setItem(1, 1, qtbl)

            name = self.classGprsP.GprsSetting[2]
            qtbl = QTableWidgetItem(name)
            self.tableGprs.setItem(2, 0, qtbl)
            qtbl = QTableWidgetItem(tmpGprsConf.GprsPass)
            self.tableGprs.setItem(2, 1, qtbl)

            name = self.classGprsP.GprsSetting[3]
            qtbl = QTableWidgetItem(name)
            self.tableGprs.setItem(3, 0, qtbl)
            qtbl = QTableWidgetItem(tmpGprsConf.dnsServer)
            self.tableGprs.setItem(3, 1, qtbl)
        else:
            index = self.comboOperSett.currentIndex()
            tmpGprsConf = self.gprsSett[index]
            self.spacClearTable(self.tableGprs)
            self.tableGprs.setRowCount(len(self.classGprs.GprsSetting))

            name = self.classGprs.GprsSetting[0]
            qtbl = QTableWidgetItem(name)
            self.tableGprs.setItem(0, 0, qtbl)
            qtbl = QTableWidgetItem(tmpGprsConf.name)
            self.tableGprs.setItem(0, 1, qtbl)

            name = self.classGprs.GprsSetting[1]
            qtbl = QTableWidgetItem(name)
            self.tableGprs.setItem(1, 0, qtbl)
            qtbl = QTableWidgetItem(tmpGprsConf.GprsLogin)
            self.tableGprs.setItem(1, 1, qtbl)

            name = self.classGprs.GprsSetting[2]
            qtbl = QTableWidgetItem(name)
            self.tableGprs.setItem(2, 0, qtbl)
            qtbl = QTableWidgetItem(tmpGprsConf.GprsPass)
            self.tableGprs.setItem(2, 1, qtbl)

            name = self.classGprs.GprsSetting[3]
            qtbl = QTableWidgetItem(name)
            self.tableGprs.setItem(3, 0, qtbl)
            qtbl = QTableWidgetItem(tmpGprsConf.dnsServer)
            self.tableGprs.setItem(3, 1, qtbl)

        self.tableGprs.resizeColumnsToContents()
        self.tableGprs.resizeRowsToContents()
        return

    @pyqtSlot()
    def OnGprsAccess(self):
        identifier = str(self.tableServSett.item(2, 1).text())
        if len(identifier) == 4:
            password = self.commandA1.generatePassw(identifier)
            self.tableServSett.setItem(3, 1, QTableWidgetItem(password))
            return
        else:
            showError('OnGprsAccess', 'Identifier must be len 4',
                      'Error with len identifier for ' + self.comboType.currentText())
        return

    @pyqtSlot()
    def OnGoPhone(self):
        row = self.tablePhones.currentRow()
        if row == -1: return
        curText = self.tablePhones.item(row, 1).text()
        row = self.tablePhonesParam.currentRow()
        if row == -1: return
        self.tablePhonesParam.setItem(row, 1, QTableWidgetItem(curText))
        return

    @pyqtSlot()
    def OnAddPhone(self):
        self.tablePhones.insertRow(self.tablePhones.rowCount())
        return

    @pyqtSlot()
    def OnSetPhone(self):
        showError("OnSetPhone", "This function is not realised!", "")
        return

    @pyqtSlot()
    def OnDeletePhone(self):
        row = self.tablePhones.currentRow()
        if showInfo("OnAddPhone", "Реально удалить номер на строке " + str(row + 1) + "?", "Вы готовы?") == self.OK_MSG:
            self.tablePhones.removeRow(row)
        return

    @pyqtSlot()
    def OnGenerateAccess(self):
        identifier = str(self.tableTelematicServer.item(1, 1).text())
        password = self.commandA1.generatePassw(identifier)
        self.tableTelematicServer.setItem(2, 1, QTableWidgetItem(password))
        return

    @pyqtSlot()
    def OnSaveServiceSetting(self):
        showInfo("OnSaveServiceSetting.", "This function is not realise!", "All right")
        return

    @pyqtSlot()
    def OnSetRegim(self):
        if len(self.gsmGprsSetting) == 0:
            return
        index = self.comboRegim.currentIndex()
        tmpBaseConf = self.gsmGprsSetting[index]
        self.spacClearTable(self.tableRegim)
        self.tableRegim.setRowCount(len(self.classRegim.NameRegim))
        name = self.classRegim.NameRegim[0]
        qtbl = QTableWidgetItem(name)
        self.tableRegim.setItem(0, 0, qtbl)
        qtbl = QTableWidgetItem(tmpBaseConf.name)
        self.tableRegim.setItem(0, 1, qtbl)

        name = self.classRegim.NameRegim[1]
        qtbl = QTableWidgetItem(name)
        self.tableRegim.setItem(1, 0, qtbl)
        qtbl = QTableWidgetItem(tmpBaseConf.regim)
        self.tableRegim.setItem(1, 1, qtbl)

        name = self.classRegim.NameRegim[2]
        qtbl = QTableWidgetItem(name)
        self.tableRegim.setItem(2, 0, qtbl)
        qtbl = QTableWidgetItem(tmpBaseConf.AccessPointName)
        self.tableRegim.setItem(2, 1, qtbl)

        name = self.classRegim.NameRegim[3]
        qtbl = QTableWidgetItem(name)
        self.tableRegim.setItem(3, 0, qtbl)
        qtbl = QTableWidgetItem(tmpBaseConf.ApnLogin)
        self.tableRegim.setItem(3, 1, qtbl)

        name = self.classRegim.NameRegim[4]
        qtbl = QTableWidgetItem(name)
        self.tableRegim.setItem(4, 0, qtbl)
        qtbl = QTableWidgetItem(tmpBaseConf.ApnPass)
        self.tableRegim.setItem(4, 1, qtbl)

        name = self.classRegim.NameRegim[5]
        qtbl = QTableWidgetItem(name)
        self.tableRegim.setItem(5, 0, qtbl)
        qtbl = QTableWidgetItem(tmpBaseConf.dnsServ)
        self.tableRegim.setItem(5, 1, qtbl)

        name = self.classRegim.NameRegim[6]
        qtbl = QTableWidgetItem(name)
        self.tableRegim.setItem(6, 0, qtbl)
        qtbl = QTableWidgetItem(tmpBaseConf.ispPhone)
        self.tableRegim.setItem(6, 1, qtbl)

        name = self.classRegim.NameRegim[7]
        qtbl = QTableWidgetItem(name)
        self.tableRegim.setItem(7, 0, qtbl)
        qtbl = QTableWidgetItem(tmpBaseConf.ispLogin)
        self.tableRegim.setItem(7, 1, qtbl)

        name = self.classRegim.NameRegim[8]
        qtbl = QTableWidgetItem(name)
        self.tableRegim.setItem(8, 0, qtbl)
        qtbl = QTableWidgetItem(tmpBaseConf.ipsPassword)
        self.tableRegim.setItem(8, 1, qtbl)

        self.tableRegim.resizeColumnsToContents()
        self.tableRegim.resizeRowsToContents()
        return

    @pyqtSlot()
    def OnReadRegim(self):
        tmpBaseConf = GprsBaseConfig()
        if len(self.tableRegim.item(0, 1).text()) == 0:
            showError("OnReadRegim. Error: Имя доступа не может быть пустым!", "Установите имя доступа", "")
            return
        tmpBaseConf.name = self.tableRegim.item(0, 1).text()
        tmpBaseConf.regim = self.tableRegim.item(1, 1).text()
        tmpBaseConf.AccessPointName = self.tableRegim.item(2, 1).text()
        tmpBaseConf.ApnLogin = self.tableRegim.item(3, 1).text()
        tmpBaseConf.ApnPass = self.tableRegim.item(4, 1).text()
        tmpBaseConf.dnsServ = self.tableRegim.item(5, 1).text()
        tmpBaseConf.ispPhone = self.tableRegim.item(6, 1).text()
        tmpBaseConf.ispLogin = self.tableRegim.item(7, 1).text()
        tmpBaseConf.ipsPassword = self.tableRegim.item(8, 1).text()
        index = self.comboRegim.findText(tmpBaseConf.name)
        if index == -1:
            self.comboRegim.addItem(tmpBaseConf.name)
            self.gsmGprsSetting.append(tmpBaseConf)
            self.comboRegim.setCurrentText(tmpBaseConf.name)
        else:
            if showInfo("OnReadRegim",
                        "Это имя доступа '" + tmpBaseConf.name + "' уже существует, хотите перезаписать?",
                        "Реально?") == self.OK_MSG:
                self.comboRegim.removeItem(index)
                del self.gsmGprsSetting[index]
                self.comboRegim.addItem(tmpBaseConf.name)
                self.gsmGprsSetting.append(tmpBaseConf)
        return

    def generatePassw(self, logid):
        try:
            code = ''
            logdev = logid.lower()
            for k in logdev:
                try:
                    indx = self.arrayFirst.index(k)
                    char = self.arraySecond[indx]
                except Exception as err:
                    showError("generatePassw. Not valid symbol in login for tracker " + str(logid), str(err),
                              str(traceback.format_exc()))
                    return ''
                code += char
            strcode = code.capitalize()
            last_symbol = strcode[3].title()
            password = strcode[0:3] + last_symbol + logid
            return password
        except Exception as err:
            showError('generatePassw.', str(err), str(traceback.format_exc()))
            return ''

    def createRecordCommand(self, device, command, clientName, idclient, nameproto, level, typecomm, nameTab, idcmd,
                            idchain, tpsens='TTA', simtype = 'none', codepin = ''):
        record = {}
        record["client"] = clientName
        record["id_client"] = idclient
        record["codeResponse"] = ''
        record["Command"] = command
        record["Comment"] = "Commanda for " + clientName
        record["countTry"] = int(1)
        record["dataResponse"] = ''
        now = datetime.datetime.now()
        record["dateCreate"] = str(now.strftime("%d-%m-%Y %H:%M:%S"))
        record["dateReade"] = ''
        record["dateResponse"] = ''
        record["levelCommand"] = level
        record["Device"] = device
        now = datetime.datetime.now()
        record["modifyDate"] = str(now.strftime("%d-%m-%Y %H:%M:%S"))
        record["numberTry"] = int(3)
        record["Protocol"] = nameproto
        record["Status"] = 'wait'
        record["statusCommand"] = 'new'
        record["UtcCreate"] = int(time.time())  # time create record
        record["UtcRead"] = 0  # time read command
        record["UtcResponse"] = 0  # time response tracker
        record["Type"] = typecomm
        record["Name"] = nameTab
        record["IDCmd"] = idcmd
        record["IDChain"] = idchain
        record["TypeSens"] = tpsens
        record["TypeSim"] = simtype
        record["PinCode"] = codepin
        time.sleep(1.0)
        return record

    def queryCommandConfigTracker(self, identifier, type):
        command = ''
        id = ''
        if type == 'TTA':
            command, id = self.commandA1.coderCommandA1Request(identifier)
        return (command, id)

    def queryCommandGPRSTracker(self, identifier, type):
        command = ''
        id = ''
        if type == 'TTA':
            command, id = self.commandA1.coderCommandA1GprsRequest(identifier)
        return (command, id)

    def queryCommandServerTracker(self, identifier, type):
        command = ''
        id = ''
        if type == 'TTA':
            command, id = self.commandA1.coderCommandA1ServerRequest(identifier)
        return (command, id)

    def queryCommandPhoneTracker(self, identifier, type):
        command = ''
        id = ''
        if type == 'TTA':
            command, id = self.commandA1.coderCommandA1PhonesRequest(identifier)
        return (command, id)

    def queryCommandGPSTracker(self, identifier, type):
        command = ''
        id = ''
        if type == 'TTA':
            command, id = self.commandA1.coderCommandA1GPSRequest(identifier)
        return (command, id)

    def queryCommandTextCmdTracker(self, identifier, type, msg):
        command = ''
        id = ''
        if type == 'TTA':
            command, id = self.commandA1.coderCommandA1TextCmdRequest(identifier, msg)
        return (command, id)

    def saveRecCommand(self, record, dev):
        mongodb = sshtunel.getMongoDb()
        collection = mongodb.get_collection('commandData')
        result = collection.insert(record)
        idlist = []
        if type(result) == list:
            for ls in result:
                idlist.append(str(ls))
        else:
            idlist.append(str(result))
        self.lineParam.appendPlainText("Succesfully save command with IDs: " + str(idlist) + " for tracker: " + dev)
        sshtunel.closeMongoDb()
        return

    def TestPin(self):
        if len(self.pinCode.text()) == 0:
            showInfo("TestPinCode", "Do not Enter PIN", "Do not be work!")
            return True
        return False

    def SendStringToPort(self, sendCmd, idlog, namecli, idclient, nameproto, level, typecomm, name, idcmd, idchn, typesens, pincode, typesim = 'none'):
        listCmdRecord = []
        while len(sendCmd) > 0:
            commandCmd = sendCmd[0]
            recordCommandA1 = self.createRecordCommand(idlog, commandCmd, namecli, idclient, nameproto, level, typecomm,
                                                       name, idcmd, idchn, typesens, typesim, pincode)
            listCmdRecord.append(recordCommandA1)
            slog = "[" + str(
                time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                   "saved reader command: " + recordCommandA1["client"] + "; " + idlog + "; " + \
                   recordCommandA1["Type"] + "; " + str(sendCmd[0])
            self.lineParam.appendPlainText(slog)
            del sendCmd[0]
        self.saveRecCommand(listCmdRecord, idlog)
        return

    def SendRebootToDB(self, sendCmd, idlog, namecli, idclient, nameproto, level, typecomm, name, idcom, idchn):
        listCmdRecord = []
        recordCommandA1 = self.createRecordCommand(idlog, sendCmd[0], namecli, idclient, nameproto, level, typecomm,
                                                   name, idcom, idchn)
        listCmdRecord.append(recordCommandA1)
        slog = "[" + str(
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
               "saved reset command: " + recordCommandA1["client"] + "; " + idlog + "; " + \
               recordCommandA1["Type"] + "; " + sendCmd[0]
        self.lineParam.appendPlainText(slog)
        self.saveRecCommand(listCmdRecord, idlog)
        return

    @pyqtSlot()
    def OnReadParam(self):
        start = time.time()
        pp = None
        try:
            showWarn("Start", "OnReadParam", "Внимание! Формируется команда на чтение параметров!", self.statusBar)
            if self.tabs.currentIndex() == 0:  # Events
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnReadParam", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText("OnReadParam: Request Events settings from " + select_identifier)
                        if self.comboType.currentText() == 'TTA':
                            commandaA1, idcomm = self.queryCommandConfigTracker(select_identifier,
                                                                                self.comboType.currentText())
                            id_chain = int(time.time())
                            recordCommandA1 = self.createRecordCommand(select_identifier, commandaA1, nameclient,
                                                                       idclient, nameproto, 1, 'read', "Events", idcomm,
                                                                       id_chain)
                            self.saveRecCommand(recordCommandA1, select_identifier)
                            slog = "[" + str(
                                time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                                   "saved reader command with Id=" + str(idcomm) + "; " + recordCommandA1[
                                       "client"] + "; " + select_identifier + "; " + \
                                   recordCommandA1["Type"] + "; " + recordCommandA1["Command"]
                            self.lineParam.appendPlainText(slog)
                        elif self.comboType.currentText() == 'TTL/TTU':
                            self.baseCmd = self.prefixCmd + self.pinCode.text() + ","
                            add = '1'
                            if self.switchBtn.isChecked():
                                add = '2'
                            if self.TestPin():
                                return
                            if self.comboTypeSens.currentText() == 'TTL':
                                sendCmdList, idcomm = self.commandA1.ReadEventsTtl(self.baseCmd, add)
                            elif self.comboTypeSens.currentText() == 'TTU':
                                sendCmdList, idcomm = self.commandA1.ReadEventsTtu(self.baseCmd)
                            id_chain = int(time.time())
                            self.SendStringToPort(sendCmdList, select_identifier, nameclient, idclient, nameproto, 1,
                                                  "read", "Events", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(),
                                                  self.switchBtn.isChecked())
                    else:
                        showError("OnReadParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 1:  # GPRS
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnReadParam", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText("OnReadParam: Request GPRS settings from " + select_identifier)
                        if self.comboType.currentText() == 'TTA':
                            commandaA1, idcomm = self.queryCommandGPRSTracker(select_identifier,
                                                                              self.comboType.currentText())
                            id_chain = int(time.time())
                            recordCommandA1 = self.createRecordCommand(select_identifier, commandaA1, nameclient,
                                                                       idclient, nameproto, 1, 'read', "GPRS", idcomm,
                                                                       id_chain)
                            self.saveRecCommand(recordCommandA1, select_identifier)
                            slog = "[" + str(
                                time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                                   "saved reader command with Id=" + str(idcomm) + "; " + recordCommandA1[
                                       "client"] + "; " + select_identifier + "; " + \
                                   recordCommandA1["Type"] + "; " + recordCommandA1["Command"]
                            self.lineParam.appendPlainText(slog)
                        else:
                            pass
                    else:
                        showError("OnReadParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 2:  # Server
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnReadParam", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText("OnReadParam: Request Server settings from " + select_identifier)
                        if self.comboType.currentText() == 'TTA':
                            commandaA1, idcomm = self.queryCommandServerTracker(select_identifier,
                                                                                self.comboType.currentText())
                            id_chain = int(time.time())
                            recordCommandA1 = self.createRecordCommand(select_identifier, commandaA1,
                                                                       nameclient, idclient, nameproto,
                                                                       1, 'read', "Server", idcomm, id_chain)
                            self.saveRecCommand(recordCommandA1, select_identifier)
                            slog = "[" + str(
                                time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                                   "saved reader command with Id=" + str(idcomm) + "; " + recordCommandA1[
                                       "client"] + "; " + select_identifier + "; " + \
                                   recordCommandA1["Type"] + "; " + recordCommandA1["Command"]
                            self.lineParam.appendPlainText(slog)
                        else:
                            pass
                    else:
                        showError("OnReadParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 3:  # Phones
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnReadParam", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText("OnReadParam: Request Phones settings from " + select_identifier)
                        if self.comboType.currentText() == 'TTA':
                            commandaA1, idcomm = self.queryCommandPhoneTracker(select_identifier,
                                                                               self.comboType.currentText())
                            id_chain = int(time.time())
                            recordCommandA1 = self.createRecordCommand(select_identifier, commandaA1,
                                                                       nameclient, idclient, nameproto,
                                                                       1, 'read', "Phones", idcomm, id_chain)
                            self.saveRecCommand(recordCommandA1, select_identifier)
                            slog = "[" + str(
                                time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                                   "saved reader command with Id=" + str(idcomm) + "; " + recordCommandA1[
                                       "client"] + "; " + select_identifier + "; " + \
                                   recordCommandA1["Type"] + "; " + recordCommandA1["Command"]
                            self.lineParam.appendPlainText(slog)
                        else:
                            pass
                    else:
                        showError("OnReadParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 4:  # TextCmd
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnReadParam", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText(
                            "OnReadParam: Request TextCmd settings from " + select_identifier)
                        if self.comboType.currentText() == 'TTA':
                            lines = self.lineTxtCmd.toPlainText().split("\n")
                            msgtxt = lines[len(lines) - 1]
                            commandaA1, idcomm = self.queryCommandTextCmdTracker(select_identifier,
                                                                                 self.comboType.currentText(), msgtxt)
                            self.lineTxtRequest.clear()
                            id_chain = int(time.time())
                            recordCommandA1 = self.createRecordCommand(select_identifier, commandaA1,
                                                                       nameclient, idclient, nameproto,
                                                                       1, 'read', "TextCmd", idcomm, id_chain)
                            self.saveRecCommand(recordCommandA1, select_identifier)
                            slog = "[" + str(
                                time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                                   "saved reader command with Id=" + str(idcomm) + "; " + recordCommandA1[
                                       "client"] + "; " + select_identifier + "; " + \
                                   recordCommandA1["Type"] + "; " + recordCommandA1["Command"]
                            self.lineParam.appendPlainText(slog)
                        else:
                            pass
                    else:
                        showError("OnReadParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 5:  # GPS
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnReadParam", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText(
                            "OnReadParam: Request GPS settings from " + select_identifier)
                        if self.comboType.currentText() == 'TTA':
                            commandaA1, idcomm = self.queryCommandGPSTracker(select_identifier,
                                                                             self.comboType.currentText())
                            id_chain = int(time.time())
                            recordCommandA1 = self.createRecordCommand(select_identifier, commandaA1,
                                                                       nameclient, idclient,
                                                                       nameproto,
                                                                       1, 'read', "GPS", idcomm, id_chain)
                            self.saveRecCommand(recordCommandA1, select_identifier)
                            slog = "[" + str(
                                time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                                   "saved reader command with Id=" + str(idcomm) + "; " + recordCommandA1[
                                       "client"] + "; " + select_identifier + "; " + \
                                   recordCommandA1["Type"] + "; " + recordCommandA1["Command"]
                            self.lineParam.appendPlainText(slog)
                        else:
                            pass
                    else:
                        showError("OnReadParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 6:  # Transport
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnReadParam", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText(
                            "OnReadParam: Request Transport settings from " + select_identifier)
                        if self.comboType.currentText() == 'TTL/TTU':
                            self.baseCmd = self.prefixCmd + self.pinCode.text() + ","
                            add = '1'
                            if self.switchBtn.isChecked():
                                add = '2'
                            if self.TestPin():
                                return
                            if self.comboTypeSens.currentText() == 'TTL':
                                sendCmdList, idcomm = self.commandA1.ReadTransportTtl(self.baseCmd, add)
                            elif self.comboTypeSens.currentText() == 'TTU':
                                sendCmdList, idcomm = self.commandA1.ReadTransportTtu(self.baseCmd)
                            id_chain = int(time.time())
                            self.SendStringToPort(sendCmdList, select_identifier, nameclient, idclient, nameproto, 1,
                                                  "read", "Transport", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(),
                                                  self.switchBtn.isChecked())
                        else:
                            pass
                    else:
                        showError("OnReadParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 7: # Servise
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnReadParam", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText(
                            "OnReadParam: Request Service settings from " + select_identifier)
                        if self.comboType.currentText() == 'TTL/TTU':
                            self.baseCmd = self.prefixCmd + self.pinCode.text() + ","
                            add = '1'
                            if self.switchBtn.isChecked():
                                add = '2'
                            if self.TestPin():
                                return
                            if self.comboTypeSens.currentText() == 'TTL':
                                sendCmdList, idcomm = self.commandA1.ReadServiceTtl(self.baseCmd, add)
                            elif self.comboTypeSens.currentText() == 'TTU':
                                sendCmdList, idcomm = self.commandA1.ReadServiceTtu(self.baseCmd)
                            id_chain = int(time.time())
                            self.SendStringToPort(sendCmdList, select_identifier, nameclient, idclient, nameproto, 1,
                                                  "read", "Service", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(),
                                                  self.switchBtn.isChecked())
                        else:
                            pass
                    else:
                        showError("OnReadParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 8: # Sensors
                selected = self.tableTrack.selectionModel().selectedRows()
                if len(selected) == 0:
                    showError("OnReadParam", "Нет трекера выделенного для указанной операции!",
                              "Выделите трекер в таблице трекеров")
                    return
                for ls in selected:
                    select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
                    if len(select_identifier) == 4:
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        self.lineParam.appendPlainText(
                            "OnReadParam: Request Sensors settings from " + select_identifier)
                        if self.comboType.currentText() == 'TTL/TTU':
                            self.baseCmd = self.prefixCmd + self.pinCode.text() + ","
                            if self.TestPin():
                                return
                            if self.comboTypeSens.currentText() == 'TTL':
                                sendCmdList, idcomm = self.commandA1.ReadSensorsTTL(self.baseCmd)
                            elif self.comboTypeSens.currentText() == 'TTU':
                                sendCmdList, idcomm = self.commandA1.ReadSensorsTTU(self.baseCmd)
                            id_chain = int(time.time())
                            self.SendStringToPort(sendCmdList, select_identifier, nameclient, idclient, nameproto, 1,
                                                  "read", "Sensors", idcomm, id_chain, self.pinCode.text(), self.comboTypeSens.currentText())
                        else:
                            pass
                    else:
                        showError("OnReadParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 9: # Terminal
                pass
            elif self.tabs.currentIndex() == 10: # Viewer
                pass
            elif self.tabs.currentIndex() == 11: # Labels
                pass
            else:
                pass
        except Exception as err:
            showError("OnReadParam", str(err), str(traceback.format_exc()))
        finally:
            showWarn("Stop", "", "", self.statusBar)
            sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
            stop = time.time()
            self.lineParam.appendPlainText(
                sttime + "Inserting read param command to Data Base: " + str(round(stop - start, 3)) + "sec")
            return

    def DataGridViewGetGprsBase(self, tableapn, iddevice):
        gprsBaseCfg = comA1.GprsBaseConfig()
        gprsBaseCfg.ShortID = iddevice
        self.commandA1.cmdMsgCnt += 1
        gprsBaseCfg.MessageID = self.commandA1.cmdMsgCnt
        gprsBaseCfg.Mode = int(tableapn.item(1, 1).text())
        gprsBaseCfg.ApnServer = tableapn.item(2, 1).text()
        gprsBaseCfg.ApnLogin = tableapn.item(3, 1).text()
        gprsBaseCfg.ApnPassword = tableapn.item(4, 1).text()
        gprsBaseCfg.DnsServer = tableapn.item(5, 1).text()
        gprsBaseCfg.DialNumber = tableapn.item(6, 1).text()
        gprsBaseCfg.GprsLogin = tableapn.item(7, 1).text()
        gprsBaseCfg.GprsPassword = tableapn.item(8, 1).text()
        return gprsBaseCfg

    def DataGridViewGetGprsEmail(self, tableTelServ, iddevice):
        gprsEmailCfg = comA1.GprsEmailConfig()
        gprsEmailCfg.ShortID = iddevice
        self.commandA1.cmdMsgCnt += 1
        gprsEmailCfg.MessageID = self.commandA1.cmdMsgCnt
        gprsEmailCfg.Pop3Server = tableTelServ.item(0, 1).text()
        gprsEmailCfg.Pop3Login = tableTelServ.item(1, 1).text() + "@"
        gprsEmailCfg.Pop3Password = tableTelServ.item(2, 1).text()
        return gprsEmailCfg

    def DataSetViewGetTextRqst(self, lines, iddevice):
        txtCmdRqst = comA1.CmdRqstConfig()
        txtCmdRqst.ShortID = iddevice
        self.commandA1.cmdMsgCnt += 1
        txtCmdRqst.MessageID = self.commandA1.cmdMsgCnt
        msgarray = lines.toPlainText().split("\n")
        txtCmdRqst.message = msgarray[len(msgarray) - 1]
        return txtCmdRqst

    def DataGridViewGetPhones(self, tablePhoneParam, iddevice):
        phoneNumCfg = comA1.PhoneNumberConfig()
        phoneNumCfg.ShortID = iddevice
        self.commandA1.cmdMsgCnt += 1
        phoneNumCfg.MessageID = self.commandA1.cmdMsgCnt
        phoneNumCfg.NumberSOS = str(tablePhoneParam.item(0, 1).text())
        phoneNumCfg.NumberDspt = str(tablePhoneParam.item(1, 1).text())
        phoneNumCfg.NumberAccept3 = str(tablePhoneParam.item(4, 1).text())
        phoneNumCfg.NumberAccept2 = str(tablePhoneParam.item(3, 1).text())
        phoneNumCfg.NumberAccept1 = str(tablePhoneParam.item(2, 1).text())
        return phoneNumCfg

    def coderCommandA1GPSWriter(self, idtt):
        query = self.commandA1.coderCommandA1GPSWriter(idtt)
        return query

    @pyqtSlot()
    def OnWriteParam(self):
        start = time.time()
        pp = None
        try:
            selected = self.tableTrack.selectionModel().selectedRows()
            if len(selected) == 0:
                showError("OnReadParam", "Нет трекера выделенного для указанной операции!",
                          "Выделите трекер в таблице трекеров")
                return
            selected_identifier = str(self.tableTrack.item(selected[len(selected) - 1].row(), 0).text())

            showWarn("Start", "OnWriteParam", "Внимание! Формируется команда на запись параметров для трекеров!", self.statusBar)

            if self.tabs.currentIndex() == 0:  # Events
                if self.comboType.currentText() == 'TTA':
                    if len(selected_identifier) == 4:
                        self.commandA1.readTableEvents(self.tableEvents)  # modified class this from event table
                        new_event_command, commandId = self.commandA1.coderCommandA1Event(
                            selected_identifier)  # get new command
                        strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                            int(time.time())))) + "] " + selected_identifier + \
                                 ", new event writer command from event table is generate, command ID:" + str(
                            commandId) + ", command:" + str(new_event_command)
                        self.lineParam.appendPlainText(strcmd)
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        id_chain = int(time.time())
                        recordCommandA1 = self.createRecordCommand(selected_identifier, new_event_command,
                                                                   nameclient,
                                                                   idclient, nameproto, 2, 'write', "Events", commandId,
                                                                   id_chain)
                        self.saveRecCommand(recordCommandA1, selected_identifier)
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                                       time.localtime(
                                                           int(time.time())))) + "] " + "saved writer command" + "; " + \
                               recordCommandA1["client"] + "; " + selected_identifier + "; " + recordCommandA1[
                                   "Type"] + "; " + \
                               recordCommandA1["Command"]
                        self.lineParam.appendPlainText(slog)
                    else:
                        showError("OnWriteParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                elif self.comboType.currentText() == 'TTL/TTU':
                    if len(selected_identifier) == 4:
                        self.baseCmd = self.prefixCmd + self.pinCode.text() + ","
                        add = '1'
                        simUse = 0
                        if self.switchBtn.isChecked():
                            add = '2'
                            simUse = 1
                        if self.TestPin():
                            return

                        if self.comboTypeSens.currentText() == 'TTL':
                            sendCmdList, idcomm = self.commandA1.coderCommandRcsttEventTtl(self.tableEvents, self.baseCmd, add, simUse)  # get new command
                        elif self.comboTypeSens.currentText() == 'TTU':
                            sendCmdList, idcomm = self.commandA1.coderCommandRcsttEventTtu(self.tableEvents, self.baseCmd)

                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        id_chain = int(time.time())
                        self.SendStringToPort(sendCmdList, selected_identifier, nameclient, idclient, nameproto, 2,
                                              "write", "Events", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(),
                                              self.switchBtn.isChecked())
                    else:
                        showError("OnWriteParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 1:  # GPRS
                if self.comboType.currentText() == 'TTA':
                    if len(selected_identifier) == 4:
                        gprsBaseConf = self.DataGridViewGetGprsBase(self.tableRegim, selected_identifier)
                        new_gprs_command, commandId = self.commandA1.EncodeGprsBaseConfigSet(gprsBaseConf)
                        strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                            int(time.time())))) + "] " + selected_identifier + \
                                 ", new event writer command from event table is generate, command ID:" + str(
                            commandId) + ", command:" + str(new_gprs_command)
                        self.lineParam.appendPlainText(strcmd)
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        id_chain = int(time.time())
                        recordCommandA1 = self.createRecordCommand(selected_identifier, new_gprs_command, nameclient,
                                                                   idclient, nameproto, 2, 'write', "GPRS", commandId,
                                                                   id_chain)
                        self.saveRecCommand(recordCommandA1, selected_identifier)
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                            int(time.time())))) + "] " + "saved writer command" + "; " + \
                               recordCommandA1["client"] + "; " + selected_identifier + "; " + recordCommandA1[
                                   "Type"] + "; " + \
                               recordCommandA1["Command"]
                        self.lineParam.appendPlainText(slog)
                    else:
                        showError("OnWriteParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                    self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 2:  # Server
                if self.comboType.currentText() == 'TTA':
                    if len(selected_identifier) == 4:
                        gprsEmailConf = self.DataGridViewGetGprsEmail(self.tableTelematicServer, selected_identifier)
                        new_gprs_command, commandId = self.commandA1.EncodeGprsEmailConfigSet(gprsEmailConf)
                        strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                                         time.localtime(
                                                             int(time.time())))) + "] " + selected_identifier + \
                                 ", new event writer command from event table is generate, command ID:" + str(
                            commandId) + ", command:" + str(new_gprs_command)
                        self.lineParam.appendPlainText(strcmd)
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        id_chain = int(time.time())
                        recordCommandA1 = self.createRecordCommand(selected_identifier, new_gprs_command,
                                                                   nameclient,
                                                                   idclient, nameproto, 2, 'write', "Server", commandId,
                                                                   id_chain)
                        self.saveRecCommand(recordCommandA1, selected_identifier)
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                            int(time.time())))) + "] " + "saved writer command" + "; " + \
                               recordCommandA1["client"] + "; " + selected_identifier + "; " + recordCommandA1[
                                   "Type"] + "; " + \
                               recordCommandA1["Command"]
                        self.lineParam.appendPlainText(slog)
                    else:
                        showError("OnWriteParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                    self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 3:  # Phones
                if self.comboType.currentText() == 'TTA':
                    if len(selected_identifier) == 4:
                        phoneNumberConf = self.DataGridViewGetPhones(self.tablePhonesParam, selected_identifier)
                        new_gprs_command, commandId = self.commandA1.EncodePhoneNumberConfigSet(phoneNumberConf)
                        strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                                         time.localtime(
                                                             int(time.time())))) + "] " + selected_identifier + \
                                 ", new event writer command from event table is generate, command ID:" + str(
                            commandId) + ", command:" + str(new_gprs_command)
                        self.lineParam.appendPlainText(strcmd)
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        id_chain = int(time.time())
                        recordCommandA1 = self.createRecordCommand(selected_identifier, new_gprs_command,
                                                                   nameclient,
                                                                   idclient, nameproto, 2, 'write', "Phones", commandId,
                                                                   id_chain)
                        self.saveRecCommand(recordCommandA1, selected_identifier)
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                            int(time.time())))) + "] " + "saved writer command" + "; " + \
                               recordCommandA1["client"] + "; " + selected_identifier + "; " + recordCommandA1[
                                   "Type"] + "; " + \
                               recordCommandA1["Command"]
                        self.lineParam.appendPlainText(slog)
                    else:
                        showError("OnWriteParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                    self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 4:  # TextCmd, Аналогично выполняется WriteParameter
                if self.comboType.currentText() == 'TTA':
                    if len(selected_identifier) == 4:
                        textCmdWriteConf = self.DataSetViewGetTextRqst(self.lineTxtCmd, selected_identifier)
                        new_gprs_command, commandId = self.commandA1.EncodeTextCmdConfigSet(textCmdWriteConf)
                        self.lineTxtRequest.clear()
                        strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                                         time.localtime(
                                                             int(time.time())))) + "] " + selected_identifier + \
                                 ", new event writer command from event table is generate, command ID:" + str(
                            commandId) + ", command:" + str(new_gprs_command)
                        self.lineParam.appendPlainText(strcmd)
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        id_chain = int(time.time())
                        recordCommandA1 = self.createRecordCommand(selected_identifier, new_gprs_command,
                                                                   nameclient,
                                                                   idclient, nameproto, 2, 'write', "TextCmd", commandId,
                                                                   id_chain)
                        self.saveRecCommand(recordCommandA1, selected_identifier)
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                            int(time.time())))) + "] " + "saved writer command" + "; " + \
                               recordCommandA1["client"] + "; " + selected_identifier + "; " + recordCommandA1[
                                   "Type"] + "; " + \
                               recordCommandA1["Command"]
                        self.lineParam.appendPlainText(slog)
                    else:
                        showError("OnWriteParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                    self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 5:  # GPS
                if self.comboType.currentText() == 'TTA':
                    if len(selected_identifier) == 4:
                        query = self.coderCommandA1GPSWriter(selected_identifier)
                        new_event_command, commandId = self.commandA1.EncodeDataGpsQueryWrite(query)  # get new command
                        strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                                         time.localtime(
                                                             int(time.time())))) + "] " + selected_identifier + \
                                 ", new event writer command from event table is generate, command ID:" + str(
                            commandId) + ", command:" + str(new_event_command)
                        self.lineParam.appendPlainText(strcmd)
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        id_chain = int(time.time())
                        recordCommandA1 = self.createRecordCommand(selected_identifier, new_event_command,
                                                                   nameclient,
                                                                   idclient, nameproto, 2, 'write', "GPS", commandId,
                                                                   id_chain)
                        self.saveRecCommand(recordCommandA1, selected_identifier)
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                                       time.localtime(
                                                           int(time.time())))) + "] " + "saved writer command" + "; " + \
                               recordCommandA1["client"] + "; " + selected_identifier + "; " + recordCommandA1[
                                   "Type"] + "; " + \
                               recordCommandA1["Command"]
                        self.lineParam.appendPlainText(slog)
                    else:
                        showError("OnWriteParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                    self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 6: # Transport
                if self.comboType.currentText() == 'TTL/TTU':
                    if len(selected_identifier) == 4:
                        self.baseCmd = self.prefixCmd + self.pinCode.text() + ","
                        add = '1'
                        simUse = 0
                        if self.switchBtn.isChecked():
                            add = '2'
                            simUse = 1
                        if self.TestPin():
                            return
                        if self.comboTypeSens.currentText() == 'TTL':
                            sendCmdList, idcomm = self.commandA1.WriteTransportTtl(self.tableServSett, self.tableGprs,
                                                                                   self.baseCmd, add, simUse)
                        elif self.comboTypeSens.currentText() == 'TTU':
                            sendCmdList, idcomm = self.commandA1.WriteTransportTtu(self.tableServSett, self.tableGprs, self.baseCmd)

                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        id_chain = int(time.time())
                        self.SendStringToPort(sendCmdList, selected_identifier, nameclient, idclient, nameproto, 2,
                                              "write", "Transport", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(),
                                              self.switchBtn.isChecked())
                    else:
                        showError("OnWriteParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                    self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 7: # Service
                if self.comboType.currentText() == 'TTL/TTU':
                    if len(selected_identifier) == 4:
                        self.baseCmd = self.prefixCmd + self.pinCode.text() + ","
                        add = '1'
                        simUse = 0
                        if self.switchBtn.isChecked():
                            add = '2'
                            simUse = 1
                        if self.TestPin():
                            return

                        if self.comboTypeSens.currentText() == 'TTL':
                            sendCmdList, idcomm = self.commandA1.WriteServiceTtl(self.tableServPhone, self.lineReg, self.baseCmd, add, simUse)
                        elif self.comboTypeSens.currentText() == 'TTU':
                            sendCmdList, idcomm = self.commandA1.WriteServiceTtu(self.tableServPhone, self.lineReg, self.baseCmd)
                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        id_chain = int(time.time())
                        self.SendStringToPort(sendCmdList, selected_identifier, nameclient, idclient, nameproto, 2,
                                              "write", "Service", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(),
                                              self.switchBtn.isChecked())
                    else:
                        showError("OnWriteParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                    self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 8: # Sensors
                if self.comboType.currentText() == 'TTL/TTU':
                    if len(selected_identifier) == 4:
                        self.baseCmd = self.prefixCmd + self.pinCode.text() + ","
                        if self.TestPin():
                            return

                        if self.comboTypeSens.currentText() == 'TTL':
                            sensParam = SensorsParamTtl()
                            sensParam.items = self.listItemsTtl
                            sensParam.textBoxSensCode = self.lineSnsCode.text()
                            sensParam.comboBoxSpeedRs485 = self.comboRs485.currentText()
                            sensParam.numericUpDownAccSensetivity = self.spinAcc.value()
                            sensParam.ainUpDownLvlUp1 = self.spinLimitUp1.value()
                            sensParam.ainUpDownLvlDown1 = self.spinLimitDown1.value()
                            sensParam.ainUpDownLvlUp2 = self.spinLimitUp2.value()
                            sensParam.ainUpDownLvlDown2 = self.spinLimitDown2.value()
                            sensParam.countRegimSelectedIndex1 = self.comboCnt1.currentIndex()
                            sensParam.countUpDownEvent1Value1 = self.spinEvent11.value()
                            sensParam.countUpDownEvent2Value1 = self.spinEvent21.value()
                            sensParam.countRegimSelectedIndex2 = self.comboCnt2.currentIndex()
                            sensParam.countUpDownEvent1Value2 = self.spinEvent12.value()
                            sensParam.countUpDownEvent2Value2 = self.spinEvent22.value()
                            sensParam.countRegimSelectedIndex3 = self.comboCnt3.currentIndex()
                            sensParam.countUpDownEvent1Value3 = self.spinEvent13.value()
                            sensParam.countUpDownEvent2Value3 = self.spinEvent23.value()
                            sensParam.Din1SelectedIndex = self.comboBounceDin1.currentIndex()
                            sensParam.Din2SelectedIndex = self.comboBounceDin2.currentIndex()
                            sensParam.Din3SelectedIndex = self.comboBounceDin3.currentIndex()
                            sensParam.Din4SelectedIndex = self.comboBounceDin4.currentIndex()
                            sensParam.checkSaveDin1 = self.checkDin1.isChecked()
                            sensParam.checkSaveDin2 = self.checkDin2.isChecked()
                            sensParam.fuelUpDownAddrValue1 = self.classFuel1.Address
                            sensParam.fuelUpDownStartBitValue1 = self.classFuel1.StartBit
                            sensParam.fuelUpDownLengthBitValue1 = self.classFuel1.Length
                            sensParam.fuelUpDownAverageValue1 = self.classFuel1.Average
                            sensParam.fuelUpDownAddrValue2 = self.classFuel2.Address
                            sensParam.fuelUpDownStartBitValue2 = self.classFuel2.StartBit
                            sensParam.fuelUpDownLengthBitValue2 = self.classFuel2.Length
                            sensParam.fuelUpDownAverageValue2 = self.classFuel2.Average
                            sensParam.fuelUpDownAddrValue3 = self.classFuel3.Address
                            sensParam.fuelUpDownStartBitValue3 = self.classFuel3.StartBit
                            sensParam.fuelUpDownLengthBitValue3 = self.classFuel3.Length
                            sensParam.fuelUpDownAverageValue3 = self.classFuel3.Average
                            sensParam.fuelUpDownAddrValue4 = self.classFuel4.Address
                            sensParam.fuelUpDownStartBitValue4 = self.classFuel4.StartBit
                            sensParam.fuelUpDownLengthBitValue4 = self.classFuel4.Length
                            sensParam.fuelUpDownAverageValue4 = self.classFuel4.Average
                            sensParam.fuelUpDownAddrValue5 = self.classFuel5.Address
                            sensParam.fuelUpDownStartBitValue5 = self.classFuel5.StartBit
                            sensParam.fuelUpDownLengthBitValue5 = self.classFuel5.Length
                            sensParam.fuelUpDownAverageValue5 = self.classFuel5.Average
                            sensParam.rfidUpDownAddrValue1 = self.classRfid1.Address
                            sensParam.rfidUpDownLengthBitValue1 = self.classRfid1.Length
                            sensParam.rfidUpDownStartBitValue1 = self.classRfid1.StartBit
                            sensParam.rfidUpDownAddrValue2 = self.classRfid2.Address
                            sensParam.rfidUpDownLengthBitValue2 = self.classRfid2.Length
                            sensParam.rfidUpDownStartBitValue2 = self.classRfid2.StartBit
                            for index in range(self.listMemo.count()):
                                check_box = self.listMemo.itemWidget(self.listMemo.item(index))
                                sensParam.checkedListBoxSensors.append(check_box)
                            sendCmdList, idcomm = self.commandA1.WriteSensorsTtl(sensParam, self.baseCmd)  # get new command
                        elif self.comboTypeSens.currentText() == 'TTU':
                            sensParam = SensorsParamTtu()
                            sensParam.items = self.listItemsTtu
                            sensParam.textBoxSensCode = self.lineSnsCode.text()
                            sensParam.comboBoxSpeedRs485 = self.comboRs485.currentText()
                            sensParam.numericUpDownAccSensetivity = self.spinAcc.value()
                            sensParam.canSpeed1 = self.comboCan1.currentText()
                            sensParam.canSpeed2 = self.comboCan2.currentText()
                            sensParam.ainUpDownLvlUp1 = self.spinLimitUp1.value()
                            sensParam.ainUpDownLvlDown1 = self.spinLimitDown1.value()
                            sensParam.ainUpDownLvlUp2 = self.spinLimitUp2.value()
                            sensParam.ainUpDownLvlDown2 = self.spinLimitDown2.value()
                            sensParam.workCan1 = self.comboWorkCan1.currentIndex()
                            sensParam.addresCan1 = self.lineCanAddrs1.text()
                            sensParam.maskCan1 = self.lineMask1.text()
                            sensParam.startbitCan1 = self.spinStBit1.value()
                            sensParam.lengthCan1 = self.spinLngth1.value()
                            sensParam.timeoutCan1 = self.spinTmout1.value()
                            sensParam.workCan2 = self.comboWorkCan2.currentIndex()
                            sensParam.addresCan2 = self.lineCanAddrs2.text()
                            sensParam.maskCan2 = self.lineMask2.text()
                            sensParam.startbitCan2 = self.spinStBit2.value()
                            sensParam.lengthCan2 = self.spinLngth2.value()
                            sensParam.timeoutCan2 = self.spinTmout2.value()
                            sensParam.workCan3 = self.comboWorkCan3.currentIndex()
                            sensParam.addresCan3 = self.lineCanAddrs3.text()
                            sensParam.maskCan3 = self.lineMask3.text()
                            sensParam.startbitCan3 = self.spinStBit3.value()
                            sensParam.lengthCan3 = self.spinLngth3.value()
                            sensParam.timeoutCan3 = self.spinTmout3.value()
                            sensParam.countRegimSelectedIndex1 = self.comboCnt1.currentIndex()
                            sensParam.countUpDownEvent1Value1 = self.spinEvent11.value()
                            sensParam.countUpDownEvent2Value1 = self.spinEvent21.value()
                            sensParam.countSourseDin1 = self.comboSource1.currentIndex()
                            sensParam.countSourceValue1 = self.spinSource1.value()
                            sensParam.countRegimSelectedIndex2 = self.comboCnt2.currentIndex()
                            sensParam.countUpDownEvent1Value2 = self.spinEvent12.value()
                            sensParam.countUpDownEvent2Value2 = self.spinEvent22.value()
                            sensParam.countSourseDin2 = self.comboSource2.currentIndex()
                            sensParam.countSourceValue2 = self.spinSource2.value()
                            sensParam.countRegimSelectedIndex3 = self.comboCnt3.currentIndex()
                            sensParam.countUpDownEvent1Value3 = self.spinEvent13.value()
                            sensParam.countUpDownEvent2Value3 = self.spinEvent23.value()
                            sensParam.countSourseDin3 = self.comboSource3.currentIndex()
                            sensParam.countSourceValue3 = self.spinSource3.value()
                            sensParam.Din1SelectedIndex = self.comboBounceDin1.currentIndex()
                            sensParam.Din2SelectedIndex = self.comboBounceDin2.currentIndex()
                            sensParam.Din3SelectedIndex = self.comboBounceDin3.currentIndex()
                            sensParam.Din4SelectedIndex = self.comboBounceDin4.currentIndex()
                            sensParam.checkSaveDin1 = self.checkDin1.isChecked()
                            sensParam.checkSaveDin2 = self.checkDin2.isChecked()
                            sensParam.checkSaveDin3 = self.checkDin3.isChecked()
                            sensParam.checkSaveDin4 = self.checkDin4.isChecked()
                            sensParam.DinUsedIn = self.comboUsedIn.currentIndex()
                            sensParam.DinLvlUp = self.spinLvlUp.value()
                            sensParam.DinLvlLow = self.spinLvlLow.value()
                            if int(self.lineCmdIn.text(), 16):
                                sensParam.DinCmdIn = self.lineCmdIn.text()
                            else:
                                showError("OnWriteParam", "Bad input value in Cmd IN", "There hex format string needing!")
                                return
                            if int(self.lineCmdOut.text(), 16):
                                sensParam.DinCmdOut = self.lineCmdOut.text()
                            else:
                                showError("OnWriteParam", "Bad input value in Command OUT", "There hex format string needing!")
                                return
                            sensParam.rs485TypeSens1 = self.comboTpSens1.currentText()
                            sensParam.rs485ReqData1 = self.checkRqstData1.isChecked()
                            sensParam.rs485Adress1 = self.spinAddrs1.value()
                            sensParam.rs485StartBit1 = self.spinStartBit1.value()
                            sensParam.rs485Length1 = self.spinLen1.value()
                            sensParam.rs485Average1 = self.spinAver1.value()
                            sensParam.rs485Event1 = self.spinEvnt1.value()
                            sensParam.rs485TypeSens2 = self.comboTpSens2.currentText()
                            sensParam.rs485ReqData2 = self.checkRqstData2.isChecked()
                            sensParam.rs485Adress2 = self.spinAddrs2.value()
                            sensParam.rs485StartBit2 = self.spinStartBit2.value()
                            sensParam.rs485Length2 = self.spinLen2.value()
                            sensParam.rs485Average2 = self.spinAver2.value()
                            sensParam.rs485Event2 = self.spinEvnt2.value()
                            sensParam.rs485TypeSens3 = self.comboTpSens3.currentText()
                            sensParam.rs485ReqData3 = self.checkRqstData3.isChecked()
                            sensParam.rs485Adress3 = self.spinAddrs3.value()
                            sensParam.rs485StartBit3 = self.spinStartBit3.value()
                            sensParam.rs485Length3 = self.spinLen3.value()
                            sensParam.rs485Average3 = self.spinAver3.value()
                            sensParam.rs485Event3 = self.spinEvnt3.value()
                            sensParam.rs485TypeSens4 = self.comboTpSens4.currentText()
                            sensParam.rs485ReqData4 = self.checkRqstData4.isChecked()
                            sensParam.rs485Adress4 = self.spinAddrs4.value()
                            sensParam.rs485StartBit4 = self.spinStartBit4.value()
                            sensParam.rs485Length4 = self.spinLen4.value()
                            sensParam.rs485Average4 = self.spinAver4.value()
                            sensParam.rs485Event4 = self.spinEvnt4.value()
                            sensParam.rs485TypeSens5 = self.comboTpSens5.currentText()
                            sensParam.rs485ReqData5 = self.checkRqstData5.isChecked()
                            sensParam.rs485Adress5 = self.spinAddrs5.value()
                            sensParam.rs485StartBit5 = self.spinStartBit5.value()
                            sensParam.rs485Length5 = self.spinLen5.value()
                            sensParam.rs485Average5 = self.spinAver5.value()
                            sensParam.rs485Event5 = self.spinEvnt5.value()
                            sensParam.rs485TypeSens6 = self.comboTpSens6.currentText()
                            sensParam.rs485ReqData6 = self.checkRqstData6.isChecked()
                            sensParam.rs485Adress6 = self.spinAddrs6.value()
                            sensParam.rs485StartBit6 = self.spinStartBit6.value()
                            sensParam.rs485Length6 = self.spinLen6.value()
                            sensParam.rs485Average6 = self.spinAver6.value()
                            sensParam.rs485Event6 = self.spinEvnt6.value()
                            sensParam.rs485TypeSens7 = self.comboTpSens7.currentText()
                            sensParam.rs485ReqData7 = self.checkRqstData7.isChecked()
                            sensParam.rs485Adress7 = self.spinAddrs7.value()
                            sensParam.rs485StartBit7 = self.spinStartBit7.value()
                            sensParam.rs485Length7 = self.spinLen7.value()
                            sensParam.rs485Average7 = self.spinAver7.value()
                            sensParam.rs485Event7 = self.spinEvnt7.value()
                            for index in range(self.listMemo.count()):
                                check_box = self.listMemo.itemWidget(self.listMemo.item(index))
                                sensParam.checkedListBoxSensors.append(check_box)

                            sendCmdList, idcomm = self.commandA1.WriteSensorsTtu(sensParam, self.baseCmd)  # get new command

                        nameproto = self.protocolInfo[self.comboType.currentText()]
                        if self.comboClient.currentText() == 'diller':
                            idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                            nameclient = self.comboDiler.currentText()
                        else:
                            idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                            nameclient = self.comboClient.currentText()
                        id_chain = int(time.time())
                        self.SendStringToPort(sendCmdList, selected_identifier, nameclient, idclient, nameproto, 2,
                                              "write", "Sensors", idcomm, id_chain, self.pinCode.text(), self.comboTypeSens.currentText())
                    else:
                        showError("OnWriteParam", "Размер ID трекера не соотвествует типу teletrack",
                                  "Длина должна быть 4 символа")
                    self.OnGetInfoTrackers()
            elif self.tabs.currentIndex() == 9: # Terminal
                pass
            elif self.tabs.currentIndex() == 10: # Viewer
                pass
            elif self.tabs.currentIndex() == 11: # Labels
                pass
            else:
                pass
        except Exception as err:
            showError("OnWriteParam", str(err), str(traceback.format_exc()))
        finally:
            showWarn("Stop", "", "", self.statusBar)
            sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
            stop = time.time()
            self.lineParam.appendPlainText(
                sttime + "Inserting writing param command to Data Base: " + str(round(stop - start, 3)) + "sec")
            return

    def msgbtnInfo(self, i):
        if i.text() == 'Cancel':
            pass
        elif i.text() == 'OK':
            pass

    def msgbtnError(self, i):
        if i.text() == 'Cancel':
            pass
        elif i.text() == 'OK':
            pass

    def showParameters(self):
        pass

    # Активация пункта главного меню 'About'
    # Показывает диалог. Основной интерфейс при этом блокируется
    def showAbout(self):
        class MyDialog(QtWidgets.QDialog):  # класс диалога
            def __init__(self, parent=None):
                super(MyDialog, self).__init__(parent)
                pixmap = QPixmap("src/photo_2020-12-03_16-48-06.jpg")
                lbl = QLabel(self)
                lbl.setPixmap(pixmap.scaled(150, 105, Qt.KeepAspectRatio))
                lbl.setAlignment(QtCore.Qt.AlignCenter)
                label = QtWidgets.QLabel('Radio Communications Systems (RCS) \n Tracker Tunner v1.2.0 \n Python 3.9.0 (x86_64) \n pyQt5.15.1 \n 2019-2020')
                label.setAlignment(QtCore.Qt.AlignCenter)
                label.setFont(QFont("Times", weight=QFont.Bold))
                label.setStyleSheet("QLabel { color : blue; }")
                mainLayout = QVBoxLayout()
                mainLayout.addWidget(lbl)
                mainLayout.addWidget(label)
                self.setLayout(mainLayout)
                self.setWindowTitle("About")
                return

        dialog = MyDialog()
        dialog.setFixedSize(300, 200)
        dialog.exec_()  # пока диалог не закроется интерфейс приложения будет заблокирован
        return

    def ReadEvents(self, bscmd, sim='none'):
        selected = self.tableTrack.selectionModel().selectedRows()
        if len(selected) == 0:
            showError("ReadEvents", "Нет трекера выделенного для указанной операции!",
                      "Выделите трекер в таблице трекеров")
            return
        for ls in selected:
            select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
            if len(select_identifier) == 4:
                nameproto = self.protocolInfo[self.comboType.currentText()]
                if self.comboClient.currentText() == 'diller':
                    idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                    nameclient = self.comboDiler.currentText()
                else:
                    idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                    nameclient = self.comboClient.currentText()
                self.lineParam.appendPlainText("[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + "ReadEvents: Request Events settings from tracker " + select_identifier)
                if self.comboType.currentText() == 'TTA':
                    commandaA1, idcomm = self.queryCommandConfigTracker(select_identifier,
                                                                        self.comboType.currentText())
                    id_chain = int(time.time())
                    recordCommandA1 = self.createRecordCommand(select_identifier, commandaA1, nameclient,
                                                               idclient, nameproto, 1, 'read', "Events", idcomm,
                                                               id_chain)
                    self.saveRecCommand(recordCommandA1, select_identifier)
                    slog = "[" + str(
                        time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                           "saved reader command with Id=" + str(idcomm) + "; " + recordCommandA1[
                               "client"] + "; " + select_identifier + "; " + \
                           recordCommandA1["Type"] + "; " + recordCommandA1["Command"]
                    self.lineParam.appendPlainText(slog)
                elif self.comboType.currentText() == 'TTL/TTU':
                    if self.comboTypeSens.currentText() == 'TTL':
                        sendCmdList, idcomm = self.commandA1.ReadEventsTtl(bscmd, sim)
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + "type SIM" + sim + " command generation"
                        self.lineParam.appendPlainText(slog)
                    elif self.comboTypeSens.currentText() == 'TTU':
                        sendCmdList, idcomm = self.commandA1.ReadEventsTtu(bscmd)
                    id_chain = int(time.time())

                    if sim == '1':
                        simtype = False
                    elif sim == '2':
                        simtype = True
                    else:
                        simtype = 'none'

                    self.SendStringToPort(sendCmdList, select_identifier, nameclient, idclient, nameproto, 1,
                                          "read", "Events", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(), simtype)
            else:
                showError("ReadEvents", "Размер ID трекера не соотвествует типу teletrack",
                          "Длина должна быть 4 символа")
        return

    # Активация авторизации
    # Показывает диалог авторизации. Основной интерфейс при этом блокируется
    def showAuthorization(self):
        class AuthDialog(QtWidgets.QDialog):  # класс диалога авторизации
            def __init__(self, parent=None):
                super(AuthDialog, self).__init__(parent)
                self.setWindowTitle("Авторизация")
                self.setWindowFlags(Qt.WindowTitleHint)
                lblСlient = QLabel("Пользователь:")
                self.lineClient = QLineEdit(self)
                self.lineClient.setToolTip("Укажите имя пользователя")
                lblPassw = QLabel("Пароль:")
                self.linePassw = QLineEdit(self)
                self.linePassw.setToolTip("Укажите пароль")
                self.linePassw.setEchoMode(QLineEdit.Password)
                labelMsg = QLabel(self)
                labelMsg.setText("To work with the program, you must \n have system administrator privileges")
                labelMsg.setAlignment(QtCore.Qt.AlignCenter)
                labelMsg.setFont(QFont("Times", weight=QFont.Bold))
                labelMsg.setStyleSheet("QLabel { color : blue; }")
                hlay1 = QHBoxLayout()
                hlay1.addWidget(lblСlient)
                hlay1.addWidget(self.lineClient)
                widget1 = QWidget(self)
                widget1.setLayout(hlay1)
                hlay2 = QHBoxLayout()
                hlay2.addWidget(lblPassw)
                hlay2.addWidget(self.linePassw)
                widget2 = QWidget(self)
                widget2.setLayout(hlay2)
                btnOk = QPushButton("Войти")
                btnOk.setToolTip("Нажмите чтобы войти")
                btnOk.clicked.connect(self.OnInput)
                btnCancel = QPushButton("Отказаться")
                btnCancel.setToolTip("Нажмите чтобы отказаться")
                btnCancel.clicked.connect(self.OnCancel)
                hlay3 = QHBoxLayout()
                hlay3.addWidget(btnOk)
                hlay3.addWidget(btnCancel)
                widget3 = QWidget(self)
                widget3.setLayout(hlay3)
                mainLayout = QVBoxLayout(self)
                mainLayout.addWidget(widget1)
                mainLayout.addWidget(widget2)
                mainLayout.addWidget(labelMsg)
                mainLayout.addWidget(widget3)
                self.setLayout(mainLayout)
                # пользовательские поля
                self.idparent = ''
                self.login = ''
                self.nameparent = ''
                return

            @pyqtSlot()
            def OnInput(self):
                self.login = self.lineClient.text()
                password = self.linePassw.text()
                self.idparent, self.nameparent = self.authorizathonProcedure(self.login, password)
                if self.idparent != '':
                    self.close()
                else:
                    showInfo("Authorization", "Неправильный логин и/или пароль", "Укажите правильный логин и/или пароль")
                return

            @pyqtSlot()
            def OnCancel(self):
                self.idparent = '*'
                self.close()
                return

            @property
            def getIdUser(self):
                return self.idparent

            @property
            def getLogin(self):
                return self.login

            @property
            def getParent(self):
                return self.nameparent

            def decodePass(self, codepass, realpass):
                decode = check_password_hash(codepass, realpass)
                return decode

            # здесь выполнение процедуры авторизации
            def authorizathonProcedure(self, user, passwd):
                try:
                    # сдесь сам процесс авторизации
                    mongodb = sshtunel.getMongoDb()
                    collection = mongodb.get_collection('users')
                    result = list(collection.find({"login": user}))
                    if len(result) == 0:
                        return '', ''
                    checkpasswd = self.decodePass(result[0]['password'], passwd)
                    if checkpasswd == False:
                        return '', ''
                    if result[0]['is_delete'] == True:
                        return '', ''
                    if 'is_locked' in result[0]:
                        if result[0]['is_locked'] == True:
                            return '', ''
                    if result[0]['status'] != 'system':
                        return '', ''
                    collection = mongodb.get_collection('dilers')
                    resultcli = list(collection.find({"_id": bson.ObjectId(result[0]['parent'])}))
                    if len(resultcli) == 0:
                        collection = mongodb.get_collection('clients')
                        resultcli = list(collection.find({"_id": bson.ObjectId(result[0]['parent'])}))
                        if len(resultcli) == 0:
                            return '', ''
                    if 'is_delete' in resultcli[0]:
                        if resultcli[0]['is_delete'] == True:
                            return '', ''
                    parent_user = str(result[0]['parent'])
                    name_parent = resultcli[0]["short_name"]
                    role_id = result[0]['role']
                    sshtunel.closeMongoDb()
                    self.input_status = True
                    return  parent_user, name_parent
                except Exception as er:
                    showError("Authorization", "Error for authorizathon", str(traceback.format_exc()))
                    self.input_status = False
                    return '', ''
                finally:
                    sshtunel.closeMongoDb()

        dialog = AuthDialog()
        dialog.setFixedSize(300, 200)
        dialog.exec_()  # пока диалог авторизации не закроется интерфейс приложения будет заблокирован
        return dialog.getIdUser, dialog.getLogin, dialog.getParent

    def ReadEvents(self, bscmd, sim='none'):
        selected = self.tableTrack.selectionModel().selectedRows()
        if len(selected) == 0:
            showError("ReadEvents", "Нет трекера выделенного для указанной операции!",
                      "Выделите трекер в таблице трекеров")
            return
        for ls in selected:
            select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
            if len(select_identifier) == 4:
                nameproto = self.protocolInfo[self.comboType.currentText()]
                if self.comboClient.currentText() == 'diller':
                    idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                    nameclient = self.comboDiler.currentText()
                else:
                    idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                    nameclient = self.comboClient.currentText()
                self.lineParam.appendPlainText("[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(
                    time.time())))) + "] " + "ReadEvents: Request Events settings from tracker " + select_identifier)
                if self.comboType.currentText() == 'TTA':
                    commandaA1, idcomm = self.queryCommandConfigTracker(select_identifier,
                                                                        self.comboType.currentText())
                    id_chain = int(time.time())
                    recordCommandA1 = self.createRecordCommand(select_identifier, commandaA1, nameclient,
                                                               idclient, nameproto, 1, 'read', "Events", idcomm,
                                                               id_chain)
                    self.saveRecCommand(recordCommandA1, select_identifier)
                    slog = "[" + str(
                        time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                           "saved reader command with Id=" + str(idcomm) + "; " + recordCommandA1[
                               "client"] + "; " + select_identifier + "; " + \
                           recordCommandA1["Type"] + "; " + recordCommandA1["Command"]
                    self.lineParam.appendPlainText(slog)
                elif self.comboType.currentText() == 'TTL/TTU':
                    if self.comboTypeSens.currentText() == 'TTL':
                        sendCmdList, idcomm = self.commandA1.ReadEventsTtl(bscmd, sim)
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                            int(time.time())))) + "] " + "type SIM" + sim + " command generation"
                        self.lineParam.appendPlainText(slog)
                    elif self.comboTypeSens.currentText() == 'TTU':
                        sendCmdList, idcomm = self.commandA1.ReadEventsTtu(bscmd)
                    id_chain = int(time.time())

                    if sim == '1':
                        simtype = False
                    elif sim == '2':
                        simtype = True
                    else:
                        simtype = 'none'

                    self.SendStringToPort(sendCmdList, select_identifier, nameclient, idclient, nameproto, 1,
                                          "read", "Events", idcomm, id_chain, self.comboTypeSens.currentText(),
                                          self.pinCode.text(), simtype)
            else:
                showError("ReadEvents", "Размер ID трекера не соотвествует типу teletrack",
                          "Длина должна быть 4 символа")
        return

    def ReadTransport(self, bscmd, sim='none'):
        selected = self.tableTrack.selectionModel().selectedRows()
        if len(selected) == 0:
            showError("ReadTransport", "Нет трекера выделенного для указанной операции!",
                      "Выделите трекер в таблице трекеров")
            return
        for ls in selected:
            select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
            if len(select_identifier) == 4:
                nameproto = self.protocolInfo[self.comboType.currentText()]
                if self.comboClient.currentText() == 'diller':
                    idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                    nameclient = self.comboDiler.currentText()
                else:
                    idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                    nameclient = self.comboClient.currentText()
                self.lineParam.appendPlainText("[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                    "ReadTransport: Request Transport settings from " + select_identifier)
                if self.comboType.currentText() == 'TTL/TTU':
                    if self.comboTypeSens.currentText() == 'TTL':
                        sendCmdList, idcomm = self.commandA1.ReadTransportTtl(bscmd, sim)
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                            int(time.time())))) + "] " + "type SIM" + sim + " command generation"
                        self.lineParam.appendPlainText(slog)
                    elif self.comboTypeSens.currentText() == 'TTU':
                        sendCmdList, idcomm = self.commandA1.ReadTransportTtu(bscmd)
                    id_chain = int(time.time())

                    if sim == '1':
                        simtype = False
                    elif sim == '2':
                        simtype = True
                    else:
                        simtype = 'none'

                    self.SendStringToPort(sendCmdList, select_identifier, nameclient, idclient, nameproto, 1,
                                          "read", "Transport", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(), simtype)
            else:
                showError("ReadTransport", "Размер ID трекера не соотвествует типу teletrack",
                          "Длина должна быть 4 символа")
        return

    def ReadService(self, bscmd, sim='none'):
        selected = self.tableTrack.selectionModel().selectedRows()
        if len(selected) == 0:
            showError("ReadService", "Нет трекера выделенного для указанной операции!",
                      "Выделите трекер в таблице трекеров")
            return
        for ls in selected:
            select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
            if len(select_identifier) == 4:
                nameproto = self.protocolInfo[self.comboType.currentText()]
                if self.comboClient.currentText() == 'diller':
                    idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                    nameclient = self.comboDiler.currentText()
                else:
                    idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                    nameclient = self.comboClient.currentText()
                self.lineParam.appendPlainText("[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " +
                    "ReadService: Request Service settings from " + select_identifier)
                if self.comboType.currentText() == 'TTL/TTU':
                    if self.comboTypeSens.currentText() == 'TTL':
                        sendCmdList, idcomm = self.commandA1.ReadServiceTtl(bscmd, sim)
                        slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                            int(time.time())))) + "] " + "type SIM" + sim + " command generation"
                        self.lineParam.appendPlainText(slog)
                    elif self.comboTypeSens.currentText() == 'TTU':
                        sendCmdList, idcomm = self.commandA1.ReadServiceTtu(bscmd)
                    id_chain = int(time.time())

                    if sim == '1':
                        simtype = False
                    elif sim == '2':
                        simtype = True
                    else:
                        simtype = 'none'

                    self.SendStringToPort(sendCmdList, select_identifier, nameclient, idclient, nameproto, 1,
                                          "read", "Service", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(), simtype)
            else:
                showError("ReadService", "Размер ID трекера не соотвествует типу teletrack",
                          "Длина должна быть 4 символа")
        return

    def ReadSensors(self, bscmd):
        selected = self.tableTrack.selectionModel().selectedRows()
        if len(selected) == 0:
            showError("ReadSensors", "Нет трекера выделенного для указанной операции!",
                      "Выделите трекер в таблице трекеров")
            return
        for ls in selected:
            select_identifier = str(self.tableTrack.item(ls.row(), 0).text())
            if len(select_identifier) == 4:
                nameproto = self.protocolInfo[self.comboType.currentText()]
                if self.comboClient.currentText() == 'diller':
                    idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                    nameclient = self.comboDiler.currentText()
                else:
                    idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                    nameclient = self.comboClient.currentText()
                self.lineParam.appendPlainText("[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + "ReadSensors: Request Sensors settings from " + select_identifier)
                if self.comboType.currentText() == 'TTL/TTU':
                    if self.comboTypeSens.currentText() == 'TTL':
                        sendCmdList, idcomm = self.commandA1.ReadSensorsTTL(bscmd)
                    elif self.comboTypeSens.currentText() == 'TTU':
                        sendCmdList, idcomm = self.commandA1.ReadSensorsTTU(bscmd)
                    id_chain = int(time.time())
                    self.SendStringToPort(sendCmdList, select_identifier, nameclient, idclient, nameproto, 1,
                                          "read", "Sensors", idcomm, id_chain, self.pinCode.text(), self.comboTypeSens.currentText())
            else:
                showError("ReadSensors", "Размер ID трекера не соотвествует типу teletrack",
                          "Длина должна быть 4 символа")
        return

    def WriteEvents(self, bscmd, selected_identifier, sim='none'):
        if self.comboType.currentText() == 'TTA':
            if len(selected_identifier) == 4:
                self.commandA1.readTableEvents(self.tableEvents)  # modified class this from event table
                new_event_command, commandId = self.commandA1.coderCommandA1Event(
                    selected_identifier)  # get new command
                strcmd = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                    int(time.time())))) + "] " + selected_identifier + \
                         ", new event writer command from event table is generate, command ID:" + str(
                    commandId) + ", command:" + str(new_event_command)
                self.lineParam.appendPlainText(strcmd)
                nameproto = self.protocolInfo[self.comboType.currentText()]
                if self.comboClient.currentText() == 'diller':
                    idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                    nameclient = self.comboDiler.currentText()
                else:
                    idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                    nameclient = self.comboClient.currentText()
                id_chain = int(time.time())
                recordCommandA1 = self.createRecordCommand(selected_identifier, new_event_command,
                                                           nameclient,
                                                           idclient, nameproto, 2, 'write', "Events", commandId,
                                                           id_chain)
                self.saveRecCommand(recordCommandA1, selected_identifier)
                slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S",
                                               time.localtime(
                                                   int(time.time())))) + "] " + "saved writer command" + "; " + \
                       recordCommandA1["client"] + "; " + selected_identifier + "; " + recordCommandA1[
                           "Type"] + "; " + \
                       recordCommandA1["Command"]
                self.lineParam.appendPlainText(slog)
            else:
                showError("WriteEvents", "Размер ID трекера не соотвествует типу teletrack",
                          "Длина должна быть 4 символа")
        elif self.comboType.currentText() == 'TTL/TTU':
            if len(selected_identifier) == 4:
                if sim == '1':
                    simUse = 0
                elif sim == '2':
                    simUse = 1
                else:
                    return

                if self.comboTypeSens.currentText() == 'TTL':
                    sendCmdList, idcomm = self.commandA1.coderCommandRcsttEventTtl(self.tableEvents, bscmd, sim, simUse)  # get new command
                    slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                        int(time.time())))) + "] " + "type SIM" + sim + " command generation"
                    self.lineParam.appendPlainText(slog)
                elif self.comboTypeSens.currentText() == 'TTU':
                    sendCmdList, idcomm = self.commandA1.coderCommandRcsttEventTtu(self.tableEvents, bscmd)

                nameproto = self.protocolInfo[self.comboType.currentText()]
                if self.comboClient.currentText() == 'diller':
                    idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                    nameclient = self.comboDiler.currentText()
                else:
                    idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                    nameclient = self.comboClient.currentText()
                id_chain = int(time.time())

                if sim == '1':
                    simtype = False
                elif sim == '2':
                    simtype = True
                else:
                    simtype = 'none'

                self.SendStringToPort(sendCmdList, selected_identifier, nameclient, idclient, nameproto, 2,
                                      "write", "Events", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(), simtype)
            else:
                showError("WriteEvents", "Размер ID трекера не соотвествует типу teletrack",
                          "Длина должна быть 4 символа")
        return

    def WriteTransport(self, bscmd, selected_identifier, sim='none'):
        if self.comboType.currentText() == 'TTL/TTU':
            if len(selected_identifier) == 4:
                if sim == '1':
                    simUse = 0
                elif sim == '2':
                    simUse = 1
                else:
                    return

                if self.comboTypeSens.currentText() == 'TTL':
                    sendCmdList, idcomm = self.commandA1.WriteTransportTtl(self.tableServSett, self.tableGprs,
                                                                           bscmd, sim, simUse)
                    slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(
                        int(time.time())))) + "] " + "type SIM" + sim + " command generation"
                    self.lineParam.appendPlainText(slog)
                elif self.comboTypeSens.currentText() == 'TTU':
                    sendCmdList, idcomm = self.commandA1.WriteTransportTtu(self.tableServSett, self.tableGprs, bscmd)

                nameproto = self.protocolInfo[self.comboType.currentText()]
                if self.comboClient.currentText() == 'diller':
                    idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                    nameclient = self.comboDiler.currentText()
                else:
                    idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                    nameclient = self.comboClient.currentText()
                id_chain = int(time.time())

                if sim == '1':
                    simtype = False
                elif sim == '2':
                    simtype = True
                else:
                    simtype = 'none'

                self.SendStringToPort(sendCmdList, selected_identifier, nameclient, idclient, nameproto, 2,
                                      "write", "Transport", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(), simtype)
            else:
                showError("WriteTransport", "Размер ID трекера не соотвествует типу teletrack",
                          "Длина должна быть 4 символа")
        return

    def WriteService(self, bscmd, selected_identifier, sim='none'):
        if self.comboType.currentText() == 'TTL/TTU':
            if len(selected_identifier) == 4:
                if sim == '1':
                    simUse = 0
                elif sim == '2':
                    simUse = 1
                else:
                    return

                if self.comboTypeSens.currentText() == 'TTL':
                    sendCmdList, idcomm = self.commandA1.WriteServiceTtl(self.tableServPhone, self.lineReg,
                                                                         bscmd, sim, simUse)
                elif self.comboTypeSens.currentText() == 'TTU':
                    sendCmdList, idcomm = self.commandA1.WriteServiceTtu(self.tableServPhone, self.lineReg, bscmd)
                nameproto = self.protocolInfo[self.comboType.currentText()]
                if self.comboClient.currentText() == 'diller':
                    idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                    nameclient = self.comboDiler.currentText()
                else:
                    idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                    nameclient = self.comboClient.currentText()
                id_chain = int(time.time())

                if sim == '1':
                    simtype = False
                elif sim == '2':
                    simtype = True
                else:
                    simtype = 'none'

                self.SendStringToPort(sendCmdList, selected_identifier, nameclient, idclient, nameproto, 2,
                                      "write", "Service", idcomm, id_chain, self.comboTypeSens.currentText(), self.pinCode.text(), simtype)
            else:
                showError("WriteService", "Размер ID трекера не соотвествует типу teletrack",
                          "Длина должна быть 4 символа")
        return

    def WriteSensors(self, bscmd, selected_identifier):
        if self.comboType.currentText() == 'TTL/TTU':
            if len(selected_identifier) == 4:
                if self.comboTypeSens.currentText() == 'TTL':
                    sensParam = SensorsParamTtl()
                    sensParam.items = self.listItemsTtl
                    sensParam.textBoxSensCode = self.lineSnsCode.text()
                    sensParam.comboBoxSpeedRs485 = self.comboRs485.currentText()
                    sensParam.numericUpDownAccSensetivity = self.spinAcc.value()
                    sensParam.ainUpDownLvlUp1 = self.spinLimitUp1.value()
                    sensParam.ainUpDownLvlDown1 = self.spinLimitDown1.value()
                    sensParam.ainUpDownLvlUp2 = self.spinLimitUp2.value()
                    sensParam.ainUpDownLvlDown2 = self.spinLimitDown2.value()
                    sensParam.countRegimSelectedIndex1 = self.comboCnt1.currentIndex()
                    sensParam.countUpDownEvent1Value1 = self.spinEvent11.value()
                    sensParam.countUpDownEvent2Value1 = self.spinEvent21.value()
                    sensParam.countRegimSelectedIndex2 = self.comboCnt2.currentIndex()
                    sensParam.countUpDownEvent1Value2 = self.spinEvent12.value()
                    sensParam.countUpDownEvent2Value2 = self.spinEvent22.value()
                    sensParam.countRegimSelectedIndex3 = self.comboCnt3.currentIndex()
                    sensParam.countUpDownEvent1Value3 = self.spinEvent13.value()
                    sensParam.countUpDownEvent2Value3 = self.spinEvent23.value()
                    sensParam.Din1SelectedIndex = self.comboBounceDin1.currentIndex()
                    sensParam.Din2SelectedIndex = self.comboBounceDin2.currentIndex()
                    sensParam.Din3SelectedIndex = self.comboBounceDin3.currentIndex()
                    sensParam.Din4SelectedIndex = self.comboBounceDin4.currentIndex()
                    sensParam.checkSaveDin1 = self.checkDin1.isChecked()
                    sensParam.checkSaveDin2 = self.checkDin2.isChecked()
                    sensParam.fuelUpDownAddrValue1 = self.classFuel1.Address
                    sensParam.fuelUpDownStartBitValue1 = self.classFuel1.StartBit
                    sensParam.fuelUpDownLengthBitValue1 = self.classFuel1.Length
                    sensParam.fuelUpDownAverageValue1 = self.classFuel1.Average
                    sensParam.fuelUpDownAddrValue2 = self.classFuel2.Address
                    sensParam.fuelUpDownStartBitValue2 = self.classFuel2.StartBit
                    sensParam.fuelUpDownLengthBitValue2 = self.classFuel2.Length
                    sensParam.fuelUpDownAverageValue2 = self.classFuel2.Average
                    sensParam.fuelUpDownAddrValue3 = self.classFuel3.Address
                    sensParam.fuelUpDownStartBitValue3 = self.classFuel3.StartBit
                    sensParam.fuelUpDownLengthBitValue3 = self.classFuel3.Length
                    sensParam.fuelUpDownAverageValue3 = self.classFuel3.Average
                    sensParam.fuelUpDownAddrValue4 = self.classFuel4.Address
                    sensParam.fuelUpDownStartBitValue4 = self.classFuel4.StartBit
                    sensParam.fuelUpDownLengthBitValue4 = self.classFuel4.Length
                    sensParam.fuelUpDownAverageValue4 = self.classFuel4.Average
                    sensParam.fuelUpDownAddrValue5 = self.classFuel5.Address
                    sensParam.fuelUpDownStartBitValue5 = self.classFuel5.StartBit
                    sensParam.fuelUpDownLengthBitValue5 = self.classFuel5.Length
                    sensParam.fuelUpDownAverageValue5 = self.classFuel5.Average
                    sensParam.rfidUpDownAddrValue1 = self.classRfid1.Address
                    sensParam.rfidUpDownLengthBitValue1 = self.classRfid1.Length
                    sensParam.rfidUpDownStartBitValue1 = self.classRfid1.StartBit
                    sensParam.rfidUpDownAddrValue2 = self.classRfid2.Address
                    sensParam.rfidUpDownLengthBitValue2 = self.classRfid2.Length
                    sensParam.rfidUpDownStartBitValue2 = self.classRfid2.StartBit
                    for index in range(self.listMemo.count()):
                        check_box = self.listMemo.itemWidget(self.listMemo.item(index))
                        sensParam.checkedListBoxSensors.append(check_box)
                    sendCmdList, idcomm = self.commandA1.WriteSensorsTtl(sensParam, bscmd)  # get new command
                elif self.comboTypeSens.currentText() == 'TTU':
                    sensParam = SensorsParamTtu()
                    sensParam.items = self.listItemsTtu
                    sensParam.textBoxSensCode = self.lineSnsCode.text()
                    sensParam.comboBoxSpeedRs485 = self.comboRs485.currentText()
                    sensParam.numericUpDownAccSensetivity = self.spinAcc.value()
                    sensParam.canSpeed1 = self.comboCan1.currentText()
                    sensParam.canSpeed2 = self.comboCan2.currentText()
                    sensParam.ainUpDownLvlUp1 = self.spinLimitUp1.value()
                    sensParam.ainUpDownLvlDown1 = self.spinLimitDown1.value()
                    sensParam.ainUpDownLvlUp2 = self.spinLimitUp2.value()
                    sensParam.ainUpDownLvlDown2 = self.spinLimitDown2.value()
                    sensParam.workCan1 = self.comboWorkCan1.currentIndex()
                    sensParam.addresCan1 = self.lineCanAddrs1.text()
                    sensParam.maskCan1 = self.lineMask1.text()
                    sensParam.startbitCan1 = self.spinStBit1.value()
                    sensParam.lengthCan1 = self.spinLngth1.value()
                    sensParam.timeoutCan1 = self.spinTmout1.value()
                    sensParam.workCan2 = self.comboWorkCan2.currentIndex()
                    sensParam.addresCan2 = self.lineCanAddrs2.text()
                    sensParam.maskCan2 = self.lineMask2.text()
                    sensParam.startbitCan2 = self.spinStBit2.value()
                    sensParam.lengthCan2 = self.spinLngth2.value()
                    sensParam.timeoutCan2 = self.spinTmout2.value()
                    sensParam.workCan3 = self.comboWorkCan3.currentIndex()
                    sensParam.addresCan3 = self.lineCanAddrs3.text()
                    sensParam.maskCan3 = self.lineMask3.text()
                    sensParam.startbitCan3 = self.spinStBit3.value()
                    sensParam.lengthCan3 = self.spinLngth3.value()
                    sensParam.timeoutCan3 = self.spinTmout3.value()
                    sensParam.countRegimSelectedIndex1 = self.comboCnt1.currentIndex()
                    sensParam.countUpDownEvent1Value1 = self.spinEvent11.value()
                    sensParam.countUpDownEvent2Value1 = self.spinEvent21.value()
                    sensParam.countSourseDin1 = self.comboSource1.currentIndex()
                    sensParam.countSourceValue1 = self.spinSource1.value()
                    sensParam.countRegimSelectedIndex2 = self.comboCnt2.currentIndex()
                    sensParam.countUpDownEvent1Value2 = self.spinEvent12.value()
                    sensParam.countUpDownEvent2Value2 = self.spinEvent22.value()
                    sensParam.countSourseDin2 = self.comboSource2.currentIndex()
                    sensParam.countSourceValue2 = self.spinSource2.value()
                    sensParam.countRegimSelectedIndex3 = self.comboCnt3.currentIndex()
                    sensParam.countUpDownEvent1Value3 = self.spinEvent13.value()
                    sensParam.countUpDownEvent2Value3 = self.spinEvent23.value()
                    sensParam.countSourseDin3 = self.comboSource3.currentIndex()
                    sensParam.countSourceValue3 = self.spinSource3.value()
                    sensParam.Din1SelectedIndex = self.comboBounceDin1.currentIndex()
                    sensParam.Din2SelectedIndex = self.comboBounceDin2.currentIndex()
                    sensParam.Din3SelectedIndex = self.comboBounceDin3.currentIndex()
                    sensParam.Din4SelectedIndex = self.comboBounceDin4.currentIndex()
                    sensParam.checkSaveDin1 = self.checkDin1.isChecked()
                    sensParam.checkSaveDin2 = self.checkDin2.isChecked()
                    sensParam.checkSaveDin3 = self.checkDin3.isChecked()
                    sensParam.checkSaveDin4 = self.checkDin4.isChecked()
                    sensParam.DinUsedIn = self.comboUsedIn.currentIndex()
                    sensParam.DinLvlUp = self.spinLvlUp.value()
                    sensParam.DinLvlLow = self.spinLvlLow.value()
                    stxt = self.lineCmdIn.text()
                    try:
                        res = int(stxt, 16)
                        sensParam.DinCmdIn = stxt
                    except Exception as err:
                        showError("WriteSensors", "Bad input value in Cmd IN", "There hex format string needing! " + str(err))
                        return
                    stxt = self.lineCmdOut.text()
                    try:
                        res = int(stxt, 16)
                        sensParam.DinCmdOut = self.lineCmdOut.text()
                    except Exception as err:
                        showError("WriteSensors", "Bad input value in Command OUT", "There hex format string needing! " + str(err))
                        return
                    sensParam.rs485TypeSens1 = self.comboTpSens1.currentText()
                    sensParam.rs485ReqData1 = self.checkRqstData1.isChecked()
                    sensParam.rs485Adress1 = self.spinAddrs1.value()
                    sensParam.rs485StartBit1 = self.spinStartBit1.value()
                    sensParam.rs485Length1 = self.spinLen1.value()
                    sensParam.rs485Average1 = self.spinAver1.value()
                    sensParam.rs485Event1 = self.spinEvnt1.value()
                    sensParam.rs485TypeSens2 = self.comboTpSens2.currentText()
                    sensParam.rs485ReqData2 = self.checkRqstData2.isChecked()
                    sensParam.rs485Adress2 = self.spinAddrs2.value()
                    sensParam.rs485StartBit2 = self.spinStartBit2.value()
                    sensParam.rs485Length2 = self.spinLen2.value()
                    sensParam.rs485Average2 = self.spinAver2.value()
                    sensParam.rs485Event2 = self.spinEvnt2.value()
                    sensParam.rs485TypeSens3 = self.comboTpSens3.currentText()
                    sensParam.rs485ReqData3 = self.checkRqstData3.isChecked()
                    sensParam.rs485Adress3 = self.spinAddrs3.value()
                    sensParam.rs485StartBit3 = self.spinStartBit3.value()
                    sensParam.rs485Length3 = self.spinLen3.value()
                    sensParam.rs485Average3 = self.spinAver3.value()
                    sensParam.rs485Event3 = self.spinEvnt3.value()
                    sensParam.rs485TypeSens4 = self.comboTpSens4.currentText()
                    sensParam.rs485ReqData4 = self.checkRqstData4.isChecked()
                    sensParam.rs485Adress4 = self.spinAddrs4.value()
                    sensParam.rs485StartBit4 = self.spinStartBit4.value()
                    sensParam.rs485Length4 = self.spinLen4.value()
                    sensParam.rs485Average4 = self.spinAver4.value()
                    sensParam.rs485Event4 = self.spinEvnt4.value()
                    sensParam.rs485TypeSens5 = self.comboTpSens5.currentText()
                    sensParam.rs485ReqData5 = self.checkRqstData5.isChecked()
                    sensParam.rs485Adress5 = self.spinAddrs5.value()
                    sensParam.rs485StartBit5 = self.spinStartBit5.value()
                    sensParam.rs485Length5 = self.spinLen5.value()
                    sensParam.rs485Average5 = self.spinAver5.value()
                    sensParam.rs485Event5 = self.spinEvnt5.value()
                    sensParam.rs485TypeSens6 = self.comboTpSens6.currentText()
                    sensParam.rs485ReqData6 = self.checkRqstData6.isChecked()
                    sensParam.rs485Adress6 = self.spinAddrs6.value()
                    sensParam.rs485StartBit6 = self.spinStartBit6.value()
                    sensParam.rs485Length6 = self.spinLen6.value()
                    sensParam.rs485Average6 = self.spinAver6.value()
                    sensParam.rs485Event6 = self.spinEvnt6.value()
                    sensParam.rs485TypeSens7 = self.comboTpSens7.currentText()
                    sensParam.rs485ReqData7 = self.checkRqstData7.isChecked()
                    sensParam.rs485Adress7 = self.spinAddrs7.value()
                    sensParam.rs485StartBit7 = self.spinStartBit7.value()
                    sensParam.rs485Length7 = self.spinLen7.value()
                    sensParam.rs485Average7 = self.spinAver7.value()
                    sensParam.rs485Event7 = self.spinEvnt7.value()
                    for index in range(self.listMemo.count()):
                        check_box = self.listMemo.itemWidget(self.listMemo.item(index))
                        sensParam.checkedListBoxSensors.append(check_box)

                    sendCmdList, idcomm = self.commandA1.WriteSensorsTtu(sensParam, bscmd)  # get new command

                nameproto = self.protocolInfo[self.comboType.currentText()]
                if self.comboClient.currentText() == 'diller':
                    idclient = self.dilersData.getIdFromName(self.comboDiler.currentText())
                    nameclient = self.comboDiler.currentText()
                else:
                    idclient = self.clientsData.getIdFromName(self.comboClient.currentText())
                    nameclient = self.comboClient.currentText()
                id_chain = int(time.time())
                self.SendStringToPort(sendCmdList, selected_identifier, nameclient, idclient, nameproto, 2,
                                      "write", "Sensors", idcomm, id_chain, self.pinCode.text(), self.comboTypeSens.currentText())
            else:
                showError("WriteSensors", "Размер ID трекера не соотвествует типу teletrack",
                          "Длина должна быть 4 символа")
        return

    def ReadAll(self):
        if self.comboType.currentText() == 'TTA':
            return

        startp = time.time()
        pp = None
        try:
            showWarn("Start", "ReadAll", "Внимание! Формируются команды на чтение параметров!", self.statusBar)
            baseCmd = self.prefixCmd + self.pinCode.text() + ","
            if self.TestPin():
                return

            if self.comboTypeSens.currentText() == 'TTL':
                self.ReadEvents(baseCmd, '1')
                self.ReadTransport(baseCmd, '1')
                self.ReadService(baseCmd, '1')

                self.ReadEvents(baseCmd, '2')
                self.ReadTransport(baseCmd, '2')
                self.ReadService(baseCmd, '2')
            elif self.comboTypeSens.currentText() == 'TTU':
                self.ReadEvents(baseCmd)
                self.ReadTransport(baseCmd)
                self.ReadService(baseCmd)

            if self.comboTypeSens.currentText() == 'TTL' or self.comboTypeSens.currentText() == 'TTU':
                self.ReadSensors(baseCmd)

            self.OnGetInfoTrackers()
        except Exception as err:
            showError("ReadAll", str(err), str(traceback.format_exc()))
        finally:
            showWarn("Stop", "", "", self.statusBar)
            sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
            pstop = time.time()
            self.lineParam.appendPlainText(sttime + "Inserting Event, Transport, Service, Sensors read parameters commands to Data Base: " + str(round(pstop - startp, 3)) + "sec")
            return

    def WriteAll(self):
        if self.comboType.currentText() == 'TTA':
            return

        selected = self.tableTrack.selectionModel().selectedRows()
        if len(selected) == 0:
            showError("WriteAll", "Нет трекера выделенного для указанной операции!",
                      "Выделите трекер в таблице трекеров")
            return
        selected_identifier = str(self.tableTrack.item(selected[len(selected) - 1].row(), 0).text())
        if len(selected_identifier) != 4:
            showError("WriteAll", "Размер ID трекера не соотвествует типу teletrack",
                      "Длина должна быть 4 символа")
            return

        startp = time.time()
        pp = None
        try:
            showWarn("Start", "WriteAll", "Внимание! Формируются команды на запись параметров!", self.statusBar)
            baseCmd = self.prefixCmd + self.pinCode.text() + ","
            if self.TestPin():
                return

            if self.comboTypeSens.currentText() == 'TTL':
                self.WriteEvents(baseCmd, selected_identifier, '1')
                self.WriteTransport(baseCmd, selected_identifier, '1')
                self.WriteService(baseCmd, selected_identifier, '1')

                self.WriteEvents(baseCmd, selected_identifier, '2')
                self.WriteTransport(baseCmd, selected_identifier, '2')
                self.WriteService(baseCmd, selected_identifier, '2')
            elif self.comboTypeSens.currentText() == 'TTU':
                self.WriteEvents(baseCmd, selected_identifier)
                self.WriteTransport(baseCmd, selected_identifier)
                self.WriteService(baseCmd, selected_identifier)

            if self.comboTypeSens.currentText() == 'TTL' or self.comboTypeSens.currentText() == 'TTU':
                self.WriteSensors(baseCmd, selected_identifier)

            self.OnGetInfoTrackers()
        except Exception as err:
            showError("WriteAll", str(err), str(traceback.format_exc()))
        finally:
            showWarn("Stop", "", "", self.statusBar)
            sttime = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] "
            pstop = time.time()
            self.lineParam.appendPlainText(sttime + "Inserting Event, Transport, Service, Sensors write parameters commands to Data Base: " + str(round(pstop - startp, 3)) + "sec")
            return

    def logout(self):
        if showInfo("Logout", "Хотите завершить сеанс?", "Реально хотите?") != self.OK_MSG:
            return
        self.OnSaveStateParam()
        self.spacClearTable(self.tableTrack)
        self.pinCode.clear()
        self.lineParam.clear()
        self.pinCode.setEnabled(False)
        self.comboTypeSens.setEnabled(False)
        self.switchBtn.setEnabled(False)
        self.btnReset.setEnabled(False)
        self.btnDeleteAllCom.setEnabled(False)
        self.btnDeleteCom.setEnabled(False)
        self.btnGetInfo.setEnabled(False)
        self.btnSavingInfo.setEnabled(False)
        self.btnSaveState.setEnabled(False)
        self.btnSetDef.setEnabled(False)
        #self.comboType.setEnabled(False)
        self.btnReadParam.setEnabled(False)
        self.btnWriteParam.setEnabled(False)
        self.logoutAction.setEnabled(False)
        self.mongoAction.setEnabled(True)
        self.readAllAction.setEnabled(False)
        self.writeAllAction.setEnabled(False)
        if self.OnGetTrackersData() == True:
            self.OnGetInfoTrackers()
        return

    def closed(self):
        if showInfo("closed", "Are you realy exit?", "") == self.OK_MSG:
            self.OnSaveStateParam()
            sshtunel.closeMongoDb()
            self.close()
        return

    def closed_(self):
        sshtunel.closeMongoDb()
        self.close()
        return

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())
        return
# The end MainWindows class

def showInfo(inform, additional='', details=''):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setInformativeText(additional)
    msg.setWindowTitle("Information")
    msg.setWindowTitle(inform)
    msg.setDetailedText(details)
    msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
    retval = msg.exec_()
    return retval


def showWarn(state, inf='', addt='', statusbar=None):
    if "Start" == state:
        statusbar().showMessage(inf + ": " + addt)
    elif "Stop" == state:
        if statusbar != None:
            statusbar().showMessage("")
    return

def showError(inform, additional='', details=''):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setInformativeText(additional)
    msg.setWindowTitle(inform)
    msg.setDetailedText(details)
    msg.setStandardButtons(QMessageBox.Ok)
    retval = msg.exec_()
    return retval

def qt_message_handler(mode, context, message):
    if mode == QtCore.QtInfoMsg:
        mode = 'INFO'
    elif mode == QtCore.QtWarningMsg:
        mode = 'WARNING'
    elif mode == QtCore.QtCriticalMsg:
        mode = 'CRITICAL'
    elif mode == QtCore.QtFatalMsg:
        mode = 'FATAL'
    else:
        mode = 'DEBUG'
    print('qt_message_handler: line: %d, func: %s(), file: %s' % (context.line, context.function, context.file))
    print('  %s: %s\n' % (mode, message))
    return

QtCore.qInstallMessageHandler(qt_message_handler)

if __name__ == "__main__":
    try:
        start = time.time()
        _PID = os.getpid()
        QtCore.qDebug('Initialization Qt Debug information')
        print("Starting application there, wait please...")
        sshtunel.RebootConfigIni()
        app = QtWidgets.QApplication(sys.argv)
        mw = MainWindow()
        mw.show()
        ret = app.exec_()
        end = time.time()
        print("Application exit success,  Worked with ", round(end - start, 2), " seconds")
        sys.exit(ret)
    except Exception as err:
        QtCore.qDebug("__main__. Application closed ubnormaly! ", str(err), str(traceback.format_exc()), " Worked with ",
              round(end - start, 2), " seconds")
        sys.exit(0)
