class Const:
    LEVEL0_PACKET_LENGTH = 160
    LEVEL0_EMAIL_LENGTH = 13
    LEVEL0_IDTT_POSITION = 19
    LEVEL0_IDTT_LENGHT = 4
    LEVEL0_START_INDICATOR_POSITION = 14
    LEVEL1_LENGTH_POSITION = 2
    LEVEL1_CRC_POSITION = 7
    LEVEL1_PACKET_LENGHT = 144
    LEVEL1_DATA_BLOCK_LENGHT = 136
    LEVEL3_PACKET_LENGHT = 102
    LEVEL3_MESSAGEID_POSITION = 1
    LEVEL3_DATA_BLOCK_POSITION = 3
    LEVEL3_DATA_BLOCK_LENGTH = 99
    ZERO_SYMBOL = 0
    START_INDICATOR_SYMBOL = '%'
    MAX_LONGITUDE = 108000000
    MAX_LATITUDE = 54000000

class CommandDescriptor:
    Unknown = 0
    TextCommand = 1
    Ping = 8
    SMSConfigSet = 11
    SMSConfigConfirm = 21
    SMSConfigQuery = 31
    SMSConfigAnswer = 41
    PhoneConfigSet = 12
    PhoneConfigConfirm = 22
    PhoneConfigQuery = 32
    PhoneConfigAnswer = 42
    EventConfigSet = 13
    EventConfigConfirm = 23
    EventConfigQuery = 33
    EventConfigAnswer = 43
    UniqueConfigSet = 14
    UniqueConfigConfirm = 24
    UniqueConfigQuery = 34
    UniqueConfigAnswer = 44
    IdConfigSet = 17
    IdConfigConfirm = 27
    IdConfigQuery = 37
    IdConfigAnswer = 47
    ZoneConfigSet = 15
    ZoneConfigConfirm = 25
    DataGpsQuery = 36
    DataGpsAnswer = 46
    DataGpsAuto = 49
    MessageToDriver = 10
    GprsBaseConfigSet = 50
    GprsBaseConfigConfirm = 60
    GprsBaseConfigQuery = 70
    GprsBaseConfigAnswer = 80
    GprsProviderConfigSet = 54
    GprsProviderConfigConfirm = 64
    GprsProviderConfigQuery = 74
    GprsProviderConfigAnswer = 84
    GprsEmailConfigSet = 51
    GprsEmailConfigConfirm = 61
    GprsEmailConfigQuery = 71
    GprsEmailConfigAnswer = 81
    GprsSocketConfigSet = 52
    GprsSocketConfigConfirm = 62
    GprsSocketConfigQuery = 72
    GprsSocketConfigAnswer = 82
    GprsFtpConfigSet = 53
    GprsFtpConfigConfirm = 63
    GprsFtpConfigQuery = 73
    GprsFtpConfigAnswer = 83

class SimpleQ:
    def __init__(self):
        self.commandId = CommandDescriptor.Unknown
        self.message = ""
        self.messageID = 1
        self.mobitelId = 0
        self.shortId = '9EE8'
        self.source = []
        return

class CommonDescription:
    def __init__(self):
        self.shortID = ""
        self.mobitelID = 0
        self.messageID = 0
        self.commandID = CommandDescriptor.Unknown
        self.source = []
        self.message = ''
        return

class EventConfig:
    def __init__(self):
        self.shortID = ""
        self.mobitelID = 0
        self.messageID = 0
        self.commandID = CommandDescriptor.Unknown
        self.source = []
        self.message = ''
        self.speedChange = 0
        self.courseBend = 1
        self.distance1 = 100
        self.distance2 = 100
        self.BITMASK_EVENT_COUNT = 32
        self.INDEX_MIN_SPEED = 0
        self.INDEX_TIMER1 = 1
        self.INDEX_TIMER2 = 2
        self.INDEX_COURSE = 3
        self.INDEX_DISTANCE1 = 4
        self.INDEX_DISTANCE2 = 5
        self.INDEX_ACCELERATION = 6
        self.INDEX_POWER_ON = 7
        self.INDEX_POWER_OFF = 8
        self.INDEX_GSM_FIND = 9
        self.INDEX_GSM_LOST = 10
        self.INDEX_LOG_FULL = 11
        self.INDEX_RING_PHONE1 = 13
        self.INDEX_RING_PHONE2 = 14
        self.INDEX_RING_PHONE3 = 15
        self.INDEX_RING_DISPETCHER = 17
        self.INDEX_SENSORS = 18
        self.INDEX_ON = 19
        self.INDEX_REBOOT = 20
        self.BIT_WRITE_LOG = 1 << 0
        self.BIT_SEND_SMS = 1 << 1
        self.BIT_SEND_TO_SERVER = 1 << 2
        self.eventMask = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.minSpeed = 0
        self.timer1 = 60
        self.timer2 = 60
        self.gprsEmail = 0
        return

class textBoxID_TT:
    Text = ''

class ValueData:
    def __init__(self):
        self.Value = 0

class CellsData:
    def __init__(self):
        self.Cells = {}

class dataGridViewEvent:
    Rows = []

    val = ValueData()
    val.Value = 0
    cell = CellsData()
    cell.Cells["ColumnEventsValue"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    val = ValueData()
    val.Value = 0
    cell = CellsData()
    cell.Cells["ColumnEventsValue"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    val = ValueData()
    val.Value = 0
    cell = CellsData()
    cell.Cells["ColumnEventsValue"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    val = ValueData()
    val.Value = 0
    cell = CellsData()
    cell.Cells["ColumnEventsValue"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    val = ValueData()
    val.Value = 0
    cell = CellsData()
    cell.Cells["ColumnEventsValue"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    val = ValueData()
    val.Value = 0
    cell = CellsData()
    cell.Cells["ColumnEventsValue"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    val = ValueData()
    val.Value = 0
    cell = CellsData()
    cell.Cells["ColumnEventsValue"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

    cell = CellsData()
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckWritePoint"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendToServer"] = val
    val = ValueData()
    val.Value = False
    cell.Cells["ColumnEventsCheckSendSMS"] = val
    Rows.append(cell)

class PacketBlock:
    def __init__(self):
        self.data = [0 for i in range(Const.LEVEL1_DATA_BLOCK_LENGHT)]
        self.crc = 0
        self.lenght = 0
        self.version = [0,0]
        self.id_tt = [0 for i in range(Const.LEVEL0_IDTT_LENGHT)]
        self.isCrcOk = False
        return

class DataBlock8bit:
    def __init__(self):
        self.data = [0 for i in range(Const.LEVEL3_PACKET_LENGHT)]

class ErrorText:
    LENGTH_PACKET = "A1_1. Длина пакета на уровне {0} не соответствует требованиям протокола. Ожидалось {1} байт, принято - {2}."
    START_INDICATOR_MISSING = "A1_2. Не найден признак начала пакета"
    START_INDICATOR_FLOW = "A1_3. Признак начала пакета расположен не на своем месте в позиции {0}, ожидалось в позиции 14."
    CRC = "A1_4. CRC не совпадает. Значение указанное в пакете {0}, расчитанное - {1}."
    COMMAND_UNKNOWN = "A1_5. От телетрека получена неизвестная команда с идентификатором: {0}."
    LENGTH_LEVEL1_UNEXPECTED = "A1_6. Указанная длина {0} в пакете уровня LEVEL1 не соответствует ожиданиям - {1}."
    VALIDATE_ENTITY_ATTRIBUTES = "A1_7. Не пройдена проверка правильности заполнения атрубутов сущности {0}: {1}."
    EMAIL_TO_BIG = "A1_8. Длина Email не может быть больше {0} символов."
    ATTRIBUTE_TO_BIG = "A1_9. Длина значения атрибута {0} сущности {1} не может быть больше {2}."
    ATTRIBUTE_TO_SMALL = "A1_10. Длина значения атрибута {0} сущности {1} не может быть менее {2}."
    ATTRIBUTE_OUT_OF_RANGE = "A1_11. Значение атрибута {0} сущности {1} должно быть в диапазоне от {2} до {3}."
    POINT_INDEX_OUT_OF_RANGE = "A1_14. Значение индекса ({0}) точки контрольной зоны {1} должно быть в диапазоне от 0 до 255."
    POINT_INDEX_ORDER = "A1_15. Нарушен порядок следования индексов в описании контрольных зон {0} и {1}. Индексы должны следовать в возрастающем порядке."
    ZONE_FULL = "A1_16. Невозможно добавить контрольную зону, т.к. достигнут предел кол-ва обслуживаемых зон: 64."
    ZONE_POINT_COUNT = "A1_17. Суммарное кол-во точек во всех контрольных зонах превысило 256."
    ZONE_MIN_POINT_COUNT = "A1_18. Минимальное кол-во точек описывающих контрольную зону равно трем."
    ZONE_BAD_DEFINED = "A1_19. Контрольная зона определена неправильно, слишком много повторяющихся точек."
    ZONE_SOURCE_MESSAGE_COUNT = "A1_20. Неправильный размер массива пакетов с настройками контрольных зон {0} = {1}. Должно быть {2}."
    NOT_IMPLEMENTED = "Srv_1. Метод {0} класса {1} должен быть перекрыт в потомках. Не следует вызывать его напрямую."
    ARGUMENT_NULL = "Srv_2. В метод {0} класса {1} не передан параметр {2}."
    BUFFER_OVERFlOW = "Srv_3. Произошло переполнение буфера(метод {0} класса {1})."
    DB_NUMBER_COLUMNS = "Srv_40. Количесво столбцов в таблице {0} и полученного набора данных после выполнения команды {1} различны."
    DB_NULL_DATAMANAGER_CTOR = "Srv_41. В конструктор объекта SrvDbCommand не передан параметр строка подключения."
    DB_NULL_DATA_INSERT_COMMAND = "Srv_42. В метод инициализации (Init) команды вставки новых данных не передан массив строк данных"
    DB_NULL_DATAGPS_PARAM = "Srv_43. В метод DataManager.InsertDataInBuffer не передан обязательный параматр DataGpsStruct[] DataGPS."
    DB_NUMBER_COLUMNS_LOST_DATA = "Srv_44. Количесво столбцов в наборе данных пропущенных записей: {0}, и ожидаемое количество: {1}, различны."
    DB_CONNECTION = "Srv_45. Невозможно подключиться к базе данных. Строка подключения: {0}"
    DB_CRITICAL_COMMAND = "Srv_47. Не выполнена важная команда: {0}. Дальнейшая работа обработка невозможна."
    DB_WRONG_INIT_PARAMS_LENGTH = "Srv_48. Команде {0} в метод Init передан параметр object[] initObjects длиной {1}, ожидалось принять массив длиной {2}."
    DB_COMMAND_EXECUTING_FAILED = "Srv_49. Ошибка выполнения команды {0}, строка подключения: {1}. \r\n Сообщение об ошибке: {2} \r\n Детализированную информацию см. ниже."
    PARSING_UNEXPECTED_LENGTH_ARRAY = "Srv_86. Длина параметра-массива метода {0} класса {1} не соответсвует ожиданиям. Ожидалось {2} байт, передано {3} байт."
    A1_TT_NOT_FOUND = "Srv_171. Телетрек с коротким идентификатором {0} не зарегистрирован в базе данных."
