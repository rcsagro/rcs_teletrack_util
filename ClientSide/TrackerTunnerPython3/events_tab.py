#!/usr/local/bin/python
# -*- coding: utf-8 -*-

class EventsTabTta(object):
    def __init__(self):
        self.param_name = ["Миниманльная скорость", "Taймер 1", "Таймер 2", "Отклонение курса", "Дистанция 1", "Дистанция 2",
                           "Ускорение", "Питание включено", "Питание выключено", "GSM появилось", "GSM пропало", "Лог заполнен",
                           "Датчики", "Включение", "Сброс", "Звонок с телефона 1", "Звонок с телефона 2", "Звонок с телефона 3",
                           "Звонок диспетчеру"]
        self.param_value = ["2", "60", "600", "15", "200", "2000", "10", "----", "----", "----", "----", "----", "----",
                            "----", "----", "----", "----", "----", "----"]
        self.unit = ["km/h", "sec", "sec", "meter", "meter", "meter", "km/h", "----", "----", "----", "----", "----",
                     "----", "----", "----", "----", "----", "----", "----"]
        self.fixing_point = [True, True, True, True, True, True, True, True, True, True, True, True, True, True, True,
                             True, True, True, True]
        self.sent_server = [False, False, True, False, False, True, False, True, True, False, False, False, False,
                            False, False, False, False, False, False]
        self.sms_phone = [False, False, False, False, False, False, False, False, False, False, False, False, False,
                          False, False, False, False, False, False]
        self.recommended_value = ["0-5", "30-3600", "30-3600", "1-200", "100-10000", "100-10000", "10", "----", "----",
                                  "----", "----", "----", "----", "----", "----", "----", "----", "----", "----"]
        self.valid_values = ["0-250", "1-65535", "1-65535", "1-1000", "100-65535", "100-65535", "0-12", "----", "----",
                             "----", "----", "----", "----", "----", "----", "----", "----", "----", "----", "----"]
        return

    def setFixingPoint(self, value, index):
        self.fixing_point[index] = value
        return

    def setSentServer(self, value, index):
        self.sent_server[index] = value
        return

    def setSmsPhone(self, value, index):
        self.sms_phone[index] = value
        return

    @property
    def ParamName(self): return self.param_name

    @property
    def ParamValue(self): return self.param_value

    @property
    def Unit(self): return self.unit

    @property
    def FixingPoint(self): return self.fixing_point

    @property
    def SentServer(self): return self.sent_server

    @property
    def SmsPhone(self): return self.sms_phone

    @property
    def RecommendedValue(self): return self.recommended_value

    @property
    def ValidValues(self): return self.valid_values


class EventsTabTtl(object):
    def __init__(self):
        self.param_name = ["Миниманльная скорость", "Taймер 1", "Таймер 2", "Отклонение курса", "Дистанция 1",
                           "Дистанция 2", "Ускорение", "Питание включено", "Питание выключено", "GSM появилось", "GSM пропало",
                           "Лог заполнен", "Датчики", "Устройство включено", "Рестарт утройства", "Звонок с телефона 1",
                           "Звонок с телефона 2", "Звонок с телефона 3"]
        self.param_value = ["2", "60", "600", "15", "200", "2000", "10", "----", "----", "----", "----", "----", "----",
                            "----", "----", "----", "----", "----"]
        self.unit = ["km/h", "sec", "sec", "meter", "meter", "meter", "km/h", "----", "----", "----", "----", "----",
                     "----", "----", "----", "----", "----", "----"]
        self.fixing_point = [True, True, True, True, True, True, True, True, True, True, True, True, True, True, True,
                             True, True, True]
        self.sent_server = [False, False, True, False, False, True, False, True, True, False, False, False, False,
                            False, False, False, False, False]
        self.recommended_value = ["0-5", "30-3600", "30-3600", "1-200", "100-10000", "100-10000", "10", "----", "----",
                                  "----", "----", "----", "----", "----", "----", "----", "----", "----"]
        return

    def setFixingPoint(self, value, index):
        self.fixing_point[index] = value
        return

    def setSentServer(self, value, index):
        self.sent_server[index] = value
        return

    @property
    def ParamName(self): return self.param_name

    @property
    def ParamValue(self): return self.param_value

    @property
    def Unit(self): return self.unit

    @property
    def FixingPoint(self): return self.fixing_point

    @property
    def SentServer(self): return self.sent_server

    @property
    def RecommendedValue(self): return self.recommended_value

class EventsTabTtlU(object):
    def __init__(self):
        self.uparam_name = ["Миниманльная скорость", "Taймер 1", "Таймер 2", "Отклонение курса", "Дистанция 1",
                           "Дистанция 2", "Ускорение", "Питание включено", "Питание выключено", "GSM появилось", "GSM пропало",
                           "Лог заполнен", "Датчики", "Устройство включено", "Рестарт утройства", "Звонок с телефона 1",
                           "Звонок с телефона 2", "Звонок с телефона 3"]
        self.uparam_value = ["3", "60", "600", "1", "100", "100", "8", "----", "----", "----", "----", "----", "----",
                            "----", "----", "----", "----", "----"]
        self.uunit = ["km/h", "sec", "sec", "meter", "meter", "meter", "km/h", "----", "----", "----", "----", "----",
                     "----", "----", "----", "----", "----", "----"]
        self.ufixing_point = [True, True, True, True, True, True, True, True, True, True, True, False, True, True, True,
                             True, True, False]
        self.usent_server = [False, True, False, False, True, False, False, False, False, False, False, False, False,
                            False, False, False, False, False]
        self.urecommended_value = ["0-5", "30-3600", "30-3600", "1-200", "100-10000", "100-10000", "10", "----", "----",
                                  "----", "----", "----", "----", "----", "----", "----", "----", "----"]
        return

    def usetFixingPoint(self, value, index):
        self.ufixing_point[index] = value
        return

    def usetSentServer(self, value, index):
        self.usent_server[index] = value
        return

    @property
    def uParamName(self): return self.uparam_name

    @property
    def uParamValue(self): return self.uparam_value

    @property
    def uUnit(self): return self.uunit

    @property
    def uFixingPoint(self): return self.ufixing_point

    @property
    def uSentServer(self): return self.usent_server

    @property
    def uRecommendedValue(self): return self.urecommended_value

class EventsTabTtu(object):
    def __init__(self):
        self.param_name = ["Миниманльная скорость", "Taймер 1", "Таймер 2", "Отклонение курса", "Дистанция 1",
                           "Дистанция 2", "Ускорение", "Питание включено", "Питание выключено", "GSM появилось", "GSM пропало",
                           "Лог заполнен", "Датчики", "Устройство включено", "Рестарт утройства", "Звонок с телефона 1",
                           "Звонок с телефона 2", "Звонок с телефона 3"]
        self.param_value = ["2", "60", "600", "15", "200", "2000", "10", "----", "----", "----", "----", "----", "----",
                            "----", "----", "----", "----", "----"]
        self.unit = ["km/h", "sec", "sec", "meter", "meter", "meter", "km/h", "----", "----", "----", "----", "----",
                     "----", "----", "----", "----", "----", "----"]
        self.fixing_point = [True, True, True, True, True, True, True, True, True, True, True, True, True, True, True,
                             True, True, True]
        self.sent_server = [False, False, True, False, False, True, False, True, True, False, False, False, False,
                            False, False, False, False, False]
        self.recommended_value = ["0-5", "30-3600", "30-3600", "1-200", "100-10000", "100-10000", "10", "----", "----",
                                  "----", "----", "----", "----", "----", "----", "----", "----", "----"]
        return

    def setFixingPoint(self, value, index):
        self.fixing_point[index] = value
        return

    def setSentServer(self, value, index):
        self.sent_server[index] = value
        return

    def setSmsPhone(self, value, index):
        self.sms_phone[index] = value
        return

    @property
    def ParamName(self): return self.param_name

    @property
    def ParamValue(self): return self.param_value

    @property
    def Unit(self): return self.unit

    @property
    def FixingPoint(self): return self.fixing_point

    @property
    def SentServer(self): return self.sent_server

    @property
    def RecommendedValue(self): return self.recommended_value



