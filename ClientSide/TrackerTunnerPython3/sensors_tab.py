#!/usr/local/bin/python
# -*- coding: utf-8 -*-

class MemoAin1(object):
    def __init__(self):
        self.ain_limit_up = 1200
        self.ain_limit_down = 200
        return

    @property
    def AinLimitUp(self): return self.ain_limit_up

    @AinLimitUp.setter
    def AinLimitUp(self, value): self.ain_limit_up = value

    @property
    def AinLimitDown(self): return self.ain_limit_down

    @AinLimitDown.setter
    def AinLimitDown(self, value): self.ain_limit_down = value

class MemoAin2(object):
    def __init__(self):
        self.ain_limit_up = 1200
        self.ain_limit_down = 200
        return

    @property
    def AinLimitUp(self): return self.ain_limit_up

    @AinLimitUp.setter
    def AinLimitUp(self, value): self.ain_limit_up = value

    @property
    def AinLimitDown(self): return self.ain_limit_down

    @AinLimitDown.setter
    def AinLimitDown(self, value): self.ain_limit_down = value

class MemoCan1(object):
    def __init__(self):
        self.work_can = ["----", "CAN1", "CAN2"]
        self.work_can_value = ""
        self.can_address = "01234567"
        self.can_mask = "00FFFF00"
        self.can_start_bit = 0
        self.can_length = 16
        self.can_timeout = 255
        return

    @property
    def WorkCanValue(self): return self.work_can_value

    @WorkCanValue.setter
    def WorkCanValue(self, value): self.work_can_value = value

    @property
    def CanStartBit(self): return self.can_start_bit

    @CanStartBit.setter
    def CanStartBit(self, value): self.can_start_bit = value

    @property
    def CanLength(self): return self.can_length

    @CanLength.setter
    def CanLength(self, value): self.can_length = value

    @property
    def CanTimeout(self): return self.can_timeout

    @CanTimeout.setter
    def CanTimeout(self, value): self.can_timeout = value

    @property
    def CanMask(self): return self.can_mask

    @CanMask.setter
    def CanMask(self, value): self.can_mask = value

    @property
    def WorkCan(self): return self.work_can

    @WorkCan.setter
    def WorkCan(self, value): self.work_can = value

    @property
    def CanAddress(self): return self.can_address

    @CanAddress.setter
    def CanAddress(self, value): self.can_address = value

class MemoCan2(object):
    def __init__(self):
        self.work_can = ["----", "CAN1", "CAN2"]
        self.work_can_value = ""
        self.can_address = "01234567"
        self.can_mask = "00FFFF00"
        self.can_start_bit = 0
        self.can_length = 16
        self.can_timeout = 255
        return

    @property
    def WorkCanValue(self): return self.work_can_value

    @WorkCanValue.setter
    def WorkCanValue(self, value): self.work_can_value = value

    @property
    def CanStartBit(self): return self.can_start_bit

    @CanStartBit.setter
    def CanStartBit(self, value): self.can_start_bit = value

    @property
    def CanLength(self): return self.can_length

    @CanLength.setter
    def CanLength(self, value): self.can_length = value

    @property
    def CanTimeout(self): return self.can_timeout

    @CanTimeout.setter
    def CanTimeout(self, value): self.can_timeout = value

    @property
    def CanMask(self): return self.can_mask

    @CanMask.setter
    def CanMask(self, value): self.can_mask = value

    @property
    def WorkCan(self): return self.work_can

    @WorkCan.setter
    def WorkCan(self, value): self.work_can = value

    @property
    def CanAddress(self): return self.can_address

    @CanAddress.setter
    def CanAddress(self, value): self.can_address = value

class MemoCan3(object):
    def __init__(self):
        self.work_can = ["----", "CAN1", "CAN2"]
        self.work_can_value = ""
        self.can_address = "01234567"
        self.can_mask = "00FFFF00"
        self.can_start_bit = 0
        self.can_length = 16
        self.can_timeout = 255
        return

    @property
    def WorkCanValue(self): return self.work_can_value

    @WorkCanValue.setter
    def WorkCanValue(self, value): self.work_can_value = value

    @property
    def CanStartBit(self): return self.can_start_bit

    @CanStartBit.setter
    def CanStartBit(self, value): self.can_start_bit = value

    @property
    def CanLength(self): return self.can_length

    @CanLength.setter
    def CanLength(self, value): self.can_length = value

    @property
    def CanTimeout(self): return self.can_timeout

    @CanTimeout.setter
    def CanTimeout(self, value): self.can_timeout = value

    @property
    def CanMask(self): return self.can_mask

    @CanMask.setter
    def CanMask(self, value): self.can_mask = value

    @property
    def WorkCan(self): return self.work_can

    @WorkCan.setter
    def WorkCan(self, value): self.work_can = value

    @property
    def CanAddress(self): return self.can_address

    @CanAddress.setter
    def CanAddress(self, value): self.can_address = value

class memoCount1(object):
    def __init__(self):
        self.regim = ["Din", "Impuls (Freq.)", "Count", "Ppo1", "Ppo2", "Ppo1_DIF", "Differ"]
        self.regim_value = "Count"
        self.source_value = "Din"
        self.event1_ = 508
        self.event2_ = 1016
        self.source_ = ["Pin", "Din"]
        self.source_en = 1
        return

    @property
    def RegimValue(self): return self.regim_value

    @RegimValue.setter
    def RegimValue(self, value): self.regim_value = value

    @property
    def SourceValue(self): return self.source_value

    @SourceValue.setter
    def SourceValue(self, value): self.source_value = value

    @property
    def Source_En(self): return self.source_en

    @Source_En.setter
    def Source_En(self, value): self.source_en = value

    @property
    def Source(self): return self.source_

    @Source.setter
    def Source(self, value): self.source_ = value

    @property
    def Event1(self): return self.event1_

    @Event1.setter
    def Event1(self, value): self.event1_ = value

    @property
    def Event2(self): return self.event2_

    @Event2.setter
    def Event2(self, value): self.event2_ = value

    @property
    def RegimD(self): return self.regim

    @RegimD.setter
    def RegimD(self, value): self.regim = value


class memoCount2(object):
    def __init__(self):
        self.regim = ["Din", "Impuls (Freq.)", "Count", "Ppo1", "Ppo2", "Ppo1_DIF", "Differ"]
        self.regim_value = "Impuls (Freq.)"
        self.source_value = "Din"
        self.event1_ = 508
        self.event2_ = 1016
        self.source_ = ["Pin", "Din"]
        self.source_en = 1
        return

    @property
    def RegimValue(self): return self.regim_value

    @RegimValue.setter
    def RegimValue(self, value): self.regim_value = value

    @property
    def SourceValue(self): return self.source_value

    @SourceValue.setter
    def SourceValue(self, value): self.source_value = value

    @property
    def Source_En(self): return self.source_en

    @Source_En.setter
    def Source_En(self, value): self.source_en = value

    @property
    def Source(self): return self.source_

    @Source.setter
    def Source(self, value): self.source_ = value

    @property
    def Event1(self): return self.event1_

    @Event1.setter
    def Event1(self, value): self.event1_ = value

    @property
    def Event2(self): return self.event2_

    @Event2.setter
    def Event2(self, value): self.event2_ = value

    @property
    def RegimD(self): return self.regim

    @RegimD.setter
    def RegimD(self, value): self.regim = value


class memoCount3(object):
    def __init__(self):
        self.regim = ["Din", "Impuls (Freq.)", "Count", "Ppo1", "Ppo2", "Ppo1_DIF", "Differ"]
        self.regim_value = "Din"
        self.source_value = "Din"
        self.event1_ = 508
        self.event2_ = 1016
        self.source_ = ["Pin", "Din"]
        self.source_en = 1
        return

    @property
    def RegimValue(self): return self.regim_value

    @RegimValue.setter
    def RegimValue(self, value): self.regim_value = value

    @property
    def SourceValue(self): return self.source_value

    @SourceValue.setter
    def SourceValue(self, value): self.source_value = value

    @property
    def Source_En(self): return self.source_en

    @Source_En.setter
    def Source_En(self, value): self.source_en = value

    @property
    def Source(self): return self.source_

    @Source.setter
    def Source(self, value): self.source_ = value

    @property
    def Event1(self): return self.event1_

    @Event1.setter
    def Event1(self, value): self.event1_ = value

    @property
    def Event2(self): return self.event2_

    @Event2.setter
    def Event2(self, value): self.event2_ = value

    @property
    def RegimD(self): return self.regim

    @RegimD.setter
    def RegimD(self, value): self.regim = value

class MemoDin(object):
    def __init__(self):
        self.din_code1 = ["0.05", "0.1", "0.25", "0.5", "1.0", "2.5", "5.0", "10.0", "25.0", "50.0", "75.0", "100.0"]
        self.din_code2 = ["0.05", "0.1", "0.25", "0.5", "1.0", "2.5", "5.0", "10.0", "25.0", "50.0", "75.0", "100.0"]
        self.din_code3 = ["0.05", "0.1", "0.25", "0.5", "1.0", "2.5", "5.0", "10.0", "25.0", "50.0", "75.0", "100.0"]
        self.din_code4 = ["0.05", "0.1", "0.25", "0.5", "1.0", "2.5", "5.0", "10.0", "25.0", "50.0", "75.0", "100.0"]
        self.din_code1_value = "0.05"
        self.din_code2_value = "0.05"
        self.din_code3_value = "0.05"
        self.din_code4_value = "0.05"
        self.checkDin1 = False
        self.checkDin2 = False
        self.checkDin3 = False
        self.checkDin4 = True
        self.comboUsedIn = ['none', 'Count_1', 'Count_2', 'Count_3', 'Count_4', 'AIN_1', 'AIN_2', 'AIN_3', 'AIN_4', 'RS485_1', 'RS485_2','RS485_3','RS485_4','RS485_5','RS485_6','RS485_7','RS485_8', 'CAN_1', 'CAN_2', 'CAN_3', 'CAN_4']
        self.usedInCurrentIndex = 0
        self.lvlUp = 0
        self.lvlLow = 0
        self.cmdIn = ""
        self.cmdOut = ""
        return

    @property
    def SmUsedIn(self): return self.comboUsedIn

    @property
    def UsedInCurrentIndex(self): return self.usedInCurrentIndex

    @UsedInCurrentIndex.setter
    def UsedInCurrentIndex(self, value): self.usedInCurrentIndex = value

    @property
    def LvlUp(self): return self.lvlUp

    @LvlUp.setter
    def LvlUp(self, value): self.lvlUp = value

    @property
    def LvlLow(self): return self.lvlLow

    @LvlLow.setter
    def LvlLow(self, value): self.lvlLow = value

    @property
    def CmdIN(self): return self.cmdIn

    @CmdIN.setter
    def CmdIN(self, value): self.cmdIn = value

    @property
    def CmdOUT(self): return self.cmdOut

    @CmdOUT.setter
    def CmdOUT(self, value): self.cmdOut = value

    @property
    def DinCode1Value(self): return self.din_code1_value

    @DinCode1Value.setter
    def DinCode1Value(self, value): self.din_code1_value = value

    @property
    def DinCode2Value(self): return self.din_code2_value

    @DinCode2Value.setter
    def DinCode2Value(self, value): self.din_code2_value = value

    @property
    def DinCode3Value(self): return self.din_code3_value

    @DinCode3Value.setter
    def DinCode3Value(self, value): self.din_code3_value = value

    @property
    def DinCode4Value(self): return self.din_code4_value

    @DinCode4Value.setter
    def DinCode4Value(self, value): self.din_code4_value = value

    @property
    def CheckDin1(self): return self.checkDin1

    @CheckDin1.setter
    def CheckDin1(self, value): self.checkDin1 = value

    @property
    def CheckDin2(self): return self.checkDin2

    @CheckDin2.setter
    def CheckDin2(self, value): self.checkDin2 = value

    @property
    def CheckDin3(self): return self.checkDin3

    @CheckDin3.setter
    def CheckDin3(self, value): self.checkDin3 = value

    @property
    def CheckDin4(self): return self.checkDin4

    @CheckDin4.setter
    def CheckDin4(self, value): self.checkDin4 = value

    @property
    def DinCode1(self): return self.din_code1

    @property
    def DinCode2(self): return self.din_code2

    @property
    def DinCode3(self): return self.din_code3

    @property
    def DinCode4(self): return self.din_code4

class MemoRs485_1(object):
    def __init__(self):
        self.type_sensor_ = ["FUEL", "RFID"]
        self.type_sensor_value = "RFID"
        self.address_ = 1
        self.startbit_ = 0
        self.length_ = 16
        self.average_ = 1
        self.event_ = 1
        self.request_data = True
        return

    @property
    def TypeSensorValue(self): return self.type_sensor_value

    @TypeSensorValue.setter
    def TypeSensorValue(self, value): self.type_sensor_value = value

    @property
    def Address(self): return self.address_

    @Address.setter
    def Address(self, value): self.address_ = value

    @property
    def Request_Data(self): return self.request_data

    @Request_Data.setter
    def Request_Data(self, value): self.request_data = value

    @property
    def Event(self): return self.event_

    @Event.setter
    def Event(self, value): self.event_ = value

    @property
    def Average(self): return self.average_

    @Average.setter
    def Average(self, value): self.average_ = value

    @property
    def StartBit(self): return self.startbit_

    @StartBit.setter
    def StartBit(self, value): self.startbit_ = value

    @property
    def Length(self): return self.length_

    @Length.setter
    def Length(self, value): self.length_ = value

    @property
    def TypeSensor(self): return self.type_sensor_


class MemoRs485_2(object):
    def __init__(self):
        self.type_sensor_ = ["FUEL", "RFID"]
        self.type_sensor_value = "RFID"
        self.address_ = 2
        self.startbit_ = 0
        self.length_ = 16
        self.average_ = 1
        self.event_ = 1
        self.request_data = True
        return

    @property
    def TypeSensorValue(self): return self.type_sensor_value

    @TypeSensorValue.setter
    def TypeSensorValue(self, value): self.type_sensor_value = value

    @property
    def Address(self): return self.address_

    @Address.setter
    def Address(self, value): self.address_ = value

    @property
    def Request_Data(self): return self.request_data

    @Request_Data.setter
    def Request_Data(self, value): self.request_data = value

    @property
    def Event(self): return self.event_

    @Event.setter
    def Event(self, value): self.event_ = value

    @property
    def Average(self): return self.average_

    @Average.setter
    def Average(self, value): self.average_ = value

    @property
    def StartBit(self): return self.startbit_

    @StartBit.setter
    def StartBit(self, value): self.startbit_ = value

    @property
    def Length(self): return self.length_

    @Length.setter
    def Length(self, value): self.length_ = value

    @property
    def TypeSensor(self): return self.type_sensor_

class MemoRs485_3(object):
    def __init__(self):
        self.type_sensor_ = ["FUEL", "RFID"]
        self.type_sensor_value = "RFID"
        self.address_ = 3
        self.startbit_ = 0
        self.length_ = 16
        self.average_ = 1
        self.event_ = 1
        self.request_data = True
        return

    @property
    def TypeSensorValue(self): return self.type_sensor_value

    @TypeSensorValue.setter
    def TypeSensorValue(self, value): self.type_sensor_value = value

    @property
    def Address(self): return self.address_

    @Address.setter
    def Address(self, value): self.address_ = value

    @property
    def Request_Data(self): return self.request_data

    @Request_Data.setter
    def Request_Data(self, value): self.request_data = value

    @property
    def Event(self): return self.event_

    @Event.setter
    def Event(self, value): self.event_ = value

    @property
    def Average(self): return self.average_

    @Average.setter
    def Average(self, value): self.average_ = value

    @property
    def StartBit(self): return self.startbit_

    @StartBit.setter
    def StartBit(self, value): self.startbit_ = value

    @property
    def Length(self): return self.length_

    @Length.setter
    def Length(self, value): self.length_ = value

    @property
    def TypeSensor(self): return self.type_sensor_


class MemoRs485_4(object):
    def __init__(self):
        self.type_sensor_ = ["FUEL", "RFID"]
        self.type_sensor_value = "RFID"
        self.address_ = 4
        self.startbit_ = 0
        self.length_ = 16
        self.average_ = 1
        self.event_ = 1
        self.request_data = True
        return

    @property
    def TypeSensorValue(self): return self.type_sensor_value

    @TypeSensorValue.setter
    def TypeSensorValue(self, value): self.type_sensor_value = value

    @property
    def Address(self): return self.address_

    @Address.setter
    def Address(self, value): self.address_ = value

    @property
    def Request_Data(self): return self.request_data

    @Request_Data.setter
    def Request_Data(self, value): self.request_data = value

    @property
    def Event(self): return self.event_

    @Event.setter
    def Event(self, value): self.event_ = value

    @property
    def Average(self): return self.average_

    @Average.setter
    def Average(self, value): self.average_ = value

    @property
    def StartBit(self): return self.startbit_

    @StartBit.setter
    def StartBit(self, value): self.startbit_ = value

    @property
    def Length(self): return self.length_

    @Length.setter
    def Length(self, value): self.length_ = value

    @property
    def TypeSensor(self): return self.type_sensor_


class MemoRs485_5(object):
    def __init__(self):
        self.type_sensor_ = ["FUEL", "RFID"]
        self.type_sensor_value = "RFID"
        self.address_ = 5
        self.startbit_ = 0
        self.length_ = 16
        self.average_ = 1
        self.event_ = 1
        self.request_data = True
        return

    @property
    def TypeSensorValue(self): return self.type_sensor_value

    @TypeSensorValue.setter
    def TypeSensorValue(self, value): self.type_sensor_value = value

    @property
    def Address(self): return self.address_

    @Address.setter
    def Address(self, value): self.address_ = value

    @property
    def Request_Data(self): return self.request_data

    @Request_Data.setter
    def Request_Data(self, value): self.request_data = value

    @property
    def Event(self): return self.event_

    @Event.setter
    def Event(self, value): self.event_ = value

    @property
    def Average(self): return self.average_

    @Average.setter
    def Average(self, value): self.average_ = value

    @property
    def StartBit(self): return self.startbit_

    @StartBit.setter
    def StartBit(self, value): self.startbit_ = value

    @property
    def Length(self): return self.length_

    @Length.setter
    def Length(self, value): self.length_ = value

    @property
    def TypeSensor(self): return self.type_sensor_

class MemoRs485_6(object):
    def __init__(self):
        self.type_sensor_ = ["FUEL", "RFID"]
        self.type_sensor_value = "RFID"
        self.address_ = 6
        self.startbit_ = 0
        self.length_ = 16
        self.average_ = 1
        self.event_ = 1
        self.request_data = True
        return

    @property
    def TypeSensorValue(self): return self.type_sensor_value

    @TypeSensorValue.setter
    def TypeSensorValue(self, value): self.type_sensor_value = value

    @property
    def Address(self): return self.address_

    @Address.setter
    def Address(self, value): self.address_ = value

    @property
    def Request_Data(self): return self.request_data

    @Request_Data.setter
    def Request_Data(self, value): self.request_data = value

    @property
    def Event(self): return self.event_

    @Event.setter
    def Event(self, value): self.event_ = value

    @property
    def Average(self): return self.average_

    @Average.setter
    def Average(self, value): self.average_ = value

    @property
    def StartBit(self): return self.startbit_

    @StartBit.setter
    def StartBit(self, value): self.startbit_ = value

    @property
    def Length(self): return self.length_

    @Length.setter
    def Length(self, value): self.length_ = value

    @property
    def TypeSensor(self): return self.type_sensor_

class MemoRs485_7(object):
    def __init__(self):
        self.type_sensor_ = ["FUEL", "RFID"]
        self.type_sensor_value = "RFID"
        self.address_ = 7
        self.startbit_ = 0
        self.length_ = 16
        self.average_ = 1
        self.event_ = 1
        self.request_data = True
        return

    @property
    def TypeSensorValue(self): return self.type_sensor_value

    @TypeSensorValue.setter
    def TypeSensorValue(self, value): self.type_sensor_value = value

    @property
    def Address(self): return self.address_

    @Address.setter
    def Address(self, value): self.address_ = value

    @property
    def Request_Data(self): return self.request_data

    @Request_Data.setter
    def Request_Data(self, value): self.request_data = value

    @property
    def Event(self): return self.event_

    @Event.setter
    def Event(self, value): self.event_ = value

    @property
    def Average(self): return self.average_

    @Average.setter
    def Average(self, value): self.average_ = value

    @property
    def StartBit(self): return self.startbit_

    @StartBit.setter
    def StartBit(self, value): self.startbit_ = value

    @property
    def Length(self): return self.length_

    @Length.setter
    def Length(self, value): self.length_ = value

    @property
    def TypeSensor(self): return self.type_sensor_


class SensorsTab(object):
    def __init__(self):
        self.speed_rs485 = ["9600", "19200", "56000", "115200"]
        self.speed_rs485_value = "19200"
        self.acc_sensitivity = 8
        self.can1 = ["----", "125000", "250000", "500000"]
        self.can1_value = ""
        self.can2 = ["----", "125000", "250000", "500000"]
        self.can2_value = ""
        self.sens_code = "1"
        return

    @property
    def SpeedRs485(self): return self.speed_rs485

    @property
    def SpeedValueRs485(self): return self.speed_rs485_value

    @SpeedValueRs485.setter
    def SpeedValueRs485(self, value): self.speed_rs485_value = value

    @property
    def AccSensitivity(self): return self.acc_sensitivity

    @AccSensitivity.setter
    def AccSensitivity(self, value): self.acc_sensitivity = value

    @property
    def CAN1(self): return self.can1

    @property
    def CAN1Value(self): return self.can1_value

    @CAN1Value.setter
    def CAN1Value(self, value): self.can1_value = value

    @property
    def CAN2(self): return self.can2

    @property
    def CAN2Value(self): return self.can2_value

    @CAN2Value.setter
    def CAN2Value(self, value): self.can2_value = value

    @property
    def SensCode(self): return self.sens_code

    @SensCode.setter
    def SensCode(self, value): self.sens_code = value

class Fuel1(object):
    def __init__(self):
        self.address = 1
        self.startbit = 16
        self.length = 16
        self.average = 20
        return

    @property
    def Address(self): return self.address

    @Address.setter
    def Address(self, value): self.address = value

    @property
    def StartBit(self): return self.startbit

    @StartBit.setter
    def StartBit(self, value): self.startbit = value

    @property
    def Length(self): return self.length

    @Length.setter
    def Length(self, value): self.length = value

    @property
    def Average(self): return self.average

    @Average.setter
    def Average(self, value): self.average = value

class Fuel2(object):
    def __init__(self):
        self.address = 2
        self.startbit = 16
        self.length = 16
        self.average = 20
        return

    @property
    def Address(self): return self.address

    @Address.setter
    def Address(self, value): self.address = value

    @property
    def StartBit(self): return self.startbit

    @StartBit.setter
    def StartBit(self, value): self.startbit = value

    @property
    def Length(self): return self.length

    @Length.setter
    def Length(self, value): self.length = value

    @property
    def Average(self): return self.average

    @Average.setter
    def Average(self, value): self.average = value

class Fuel3(object):
    def __init__(self):
        self.address = 3
        self.startbit = 16
        self.length = 16
        self.average = 20
        return

    @property
    def Address(self): return self.address

    @Address.setter
    def Address(self, value): self.address = value

    @property
    def StartBit(self): return self.startbit

    @StartBit.setter
    def StartBit(self, value): self.startbit = value

    @property
    def Length(self): return self.length

    @Length.setter
    def Length(self, value): self.length = value

    @property
    def Average(self): return self.average

    @Average.setter
    def Average(self, value): self.average = value

class Fuel4(object):
    def __init__(self):
        self.address = 4
        self.startbit = 16
        self.length = 16
        self.average = 20
        return

    @property
    def Address(self): return self.address

    @Address.setter
    def Address(self, value): self.address = value

    @property
    def StartBit(self): return self.startbit

    @StartBit.setter
    def StartBit(self, value): self.startbit = value

    @property
    def Length(self): return self.length

    @Length.setter
    def Length(self, value): self.length = value

    @property
    def Average(self): return self.average

    @Average.setter
    def Average(self, value): self.average = value

class Fuel5(object):
    def __init__(self):
        self.address = 5
        self.startbit = 16
        self.length = 16
        self.average = 20
        return

    @property
    def Address(self): return self.address

    @Address.setter
    def Address(self, value): self.address = value

    @property
    def StartBit(self): return self.startbit

    @StartBit.setter
    def StartBit(self, value): self.startbit = value

    @property
    def Length(self): return self.length

    @Length.setter
    def Length(self, value): self.length = value

    @property
    def Average(self): return self.average

    @Average.setter
    def Average(self, value): self.average = value

class Rfid1(object):
    def __init__(self):
        self.address = 1
        self.startbit = 0
        self.length = 16
        return

    @property
    def Address(self): return self.address

    @Address.setter
    def Address(self, value): self.address = value

    @property
    def StartBit(self): return self.startbit

    @StartBit.setter
    def StartBit(self, value): self.startbit = value

    @property
    def Length(self): return self.length

    @Length.setter
    def Length(self, value): self.length = value

class Rfid2(object):
    def __init__(self):
        self.address = 2
        self.startbit = 0
        self.length = 16
        return

    @property
    def Address(self): return self.address

    @Address.setter
    def Address(self, value): self.address = value

    @property
    def StartBit(self): return self.startbit

    @StartBit.setter
    def StartBit(self, value): self.startbit = value

    @property
    def Length(self): return self.length

    @Length.setter
    def Length(self, value): self.length = value



