import traceback
import copy
from datetime import datetime
import time
import event_config_set as ecs

typeLssStr = ["FUEL", "RFID"]

class LLS_PROTOCOL:
    OMNICOMM_RES = 0x3E
    OMNICOMM_REQ = 0x31
    SOVA_RES = 0x23
    SOVA_REQ = 0x21
    GET_DATA = 0x06
    GET_TYPE_DEV = 0x80
    LEN_GET_TYPE_DEV = 7
    IDX_ADDR = 1
    IDX_CMD = 2

class SENS_CONST:
    NUM_LSS_SENS = 20
    NUM_CAN_SENS = 10
    NUM_DIN_SENS = 4
    NUM_AIN_SENS = 4
    NUM_OWIRE_SENS = 4

class TYPE_LLS:
    OMNICOMM_RES = 0x3E
    SOVA_RES = 0x23

class LlsTypeSetting:
    type = TYPE_LLS.OMNICOMM_RES
    request = 1
    address = 0
    startBit = 0
    length = 16
    average = 1
    evtDelta = 1

class TRs485Setting:
    def __init__(self):
        self.rs485Speed = 19200
        self.llsTypeSens = []

        for i in range(0, SENS_CONST.NUM_LSS_SENS, 1):
            self.llsTypeSens.append(LlsTypeSetting())

        self.llsTypeSens[0].type = TYPE_LLS.OMNICOMM_RES
        self.llsTypeSens[0].address = 1
        self.llsTypeSens[1].type = TYPE_LLS.OMNICOMM_RES
        self.llsTypeSens[1].address = 2
        self.llsTypeSens[2].type = TYPE_LLS.OMNICOMM_RES
        self.llsTypeSens[2].address = 3
        self.llsTypeSens[3].type = TYPE_LLS.OMNICOMM_RES
        self.llsTypeSens[3].address = 4
        self.llsTypeSens[4].type = TYPE_LLS.OMNICOMM_RES
        self.llsTypeSens[4].address = 5
        self.llsTypeSens[5].type = TYPE_LLS.SOVA_RES
        self.llsTypeSens[5].address = 1
        self.llsTypeSens[6].type = TYPE_LLS.SOVA_RES
        self.llsTypeSens[6].address = 2
        return

class MyConverter:
    @staticmethod
    def GetHexString(input):
        outStr = ""
        for ch in input:
            outStr += format(ch, "02X") + " "
        return outStr[0:len(outStr) - 1]
    
    @staticmethod
    def GetByteFromHexString(input):
        if len(input) % 2 != 0:
            input = "0" + input
        ret = []
        size = int(len(input) / 2)
        for i in range(0, size, 1):
            strt = input[i * 2:i * 2 + 2]
            ret.append(int(strt, 16))
        return ret

class LlsSensor:
    @staticmethod
    def GetReqToTypeSensor(addr, fuel = True):
        ret = [LLS_PROTOCOL.OMNICOMM_REQ, addr, LLS_PROTOCOL.GET_TYPE_DEV, 0]
        ret[3] = LlsSensor.CalculateCRC(ret, 3)
        return ret
    
    @staticmethod
    def CalculateCRC(buffer, length_crc):
        ret = 0
        for i in range(0, length_crc, 1):
            ret = LlsSensor.CalkByteCRC(buffer[i], ret) # ret ^= mass[i]
        return ret

    @staticmethod
    def CalkByteCRC(dat, crc):
        i = dat ^ crc
        crc = 0
        if (i & 0x01) > 0: crc ^= 0x5e
        if (i & 0x02) > 0: crc ^= 0xbc
        if (i & 0x04) > 0: crc ^= 0x61
        if (i & 0x08) > 0: crc ^= 0xc2
        if (i & 0x10) > 0: crc ^= 0x9d
        if (i & 0x20) > 0: crc ^= 0x23
        if (i & 0x40) > 0: crc ^= 0x46
        if (i & 0x80) > 0: crc ^= 0x8c
        return crc

class EventSetting:
    BITMASK_EVENT_COUNT = 32
    INDEX_MIN_SPEED = 0
    INDEX_TIMER1 = 1
    INDEX_TIMER2 = 2
    INDEX_COURSE = 3
    INDEX_DISTANCE1 = 4
    INDEX_DISTANCE2 = 5
    INDEX_ACCELERATION = 6
    INDEX_POWER_ON = 7
    INDEX_POWER_OFF = 8
    INDEX_GSM_FIND = 9
    INDEX_GSM_LOST = 10
    INDEX_LOG_FULL = 11
    INDEX_RING_PHONE1 = 13
    INDEX_RING_PHONE2 = 14
    INDEX_RING_PHONE3 = 15
    INDEX_RING_DISPETCHER = 17
    INDEX_SENSORS = 18
    INDEX_ON = 19
    INDEX_REBOOT = 20
    BIT_WRITE_LOG = 1 << 0
    BIT_SEND_SMS = 1 << 1
    BIT_SEND_TO_SERVER = 1 << 2

    def __init__(self, usesim):
        self.useSim = usesim
        self.MinSpeed = 3
        self.Timer1 = 60
        self.Timer2 = 600
        self.CourseBend = 1
        self.Distance1 = 100
        self.Distance2 = 100
        self.SpeedChange = 8
        self.EventMask = [1, 5, 1, 1, 5, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        return

class TYPE_LLS:
    OMNICOMM_RES = 0x3E
    SOVA_RES = 0x23

class LlsTypeSetting:
    type = TYPE_LLS.OMNICOMM_RES
    request = 1
    address = 0
    startBit = 0
    length = 16
    average = 1
    evtDelta = 1

class CanSensors:
    pgn = 0x01234567
    mask = 0x00FFFF00
    canUse = 0
    startBit = 0
    lenBit = 16
    timeout = 255 # second

class CmdRqstConfig:
    def __init__(self):
        self.ShortID = ""
        self.MessageID = ""
        self.message = ""
        self.CommandID = ""
        return

class GprsBaseConfig:
    def __init__(self):
        self.CommandID = ""
        self.ShortID = ""
        self.MessageID = ""
        self.Mode = ""
        self.apnServer = ""
        self.apnLogin = ""
        self.apnPassword = ""
        self.dnsServer = ""
        self.dialNumber = ""
        self.gprsLogin = ""
        self.gprsPassword = ""
        self.Source = ""
        return

class DataGpsQuery:
    def __init__(self):
        self.ShortID = ""
        self.MessageID = 0
        self.Events = 0
        self.CheckMask = 0
        self.LastTimes = 0
        self.CommandID = 0
        self.WhatSend = 0
        self.MaxSmsNumber = 0
        self.StartTime = 0
        self.EndTime = 0
        self.LastRecords = 0
        self.startFlags = 0
        self.endFlags = 0
        self.LastMeters = 0
        self.StartSpeed = 0
        self.StartLongitude = 0
        self.StartLatitude = 0
        self.StartAltitude = 0
        self.StartDirection = 0
        self.StartLogId = 0
        self.StartFlags = 0
        self.EndTime = 0
        self.EndSpeed = 0
        self.EndLatitude = 0
        self.EndLongitude = 0
        self.EndAltitude = 0
        self.EndDirection = 0
        self.EndLogId = 0
        self.EndFlags = 0
        self.LogId1 = 0
        self.LogId2 = 0
        self.LogId3 = 0
        self.LogId4 = 0
        self.LogId5 = 0
        return

class GprsEmailConfig:
    def __init__(self):
        self.smtpServer = ""
        self.smtpLogin = ""
        self.smtpPassword = ""
        self.pop3Server = ""
        self.pop3Login = ""
        self.pop3Password = ""
        self.MessageID = 0
        self.ShortID = ""
        self.Source = ""
        self.Message = ""
        return

class PhoneNumberConfig:
    def __init__(self):
        self.MessageID = 0
        self.NumberSOS = ""
        self.NumberDspt = ""
        self.NumberAccept3 = ""
        self.NumberAccept2 = ""
        self.NumberAccept1 = ""
        self.Source = ""
        self.Message = ""
        self.ShortID = ""
        self.CommandID = ""
        self.Name1 = ""
        self.Name2 = ""
        self.Name3 = ""
        return

class TransportSetting:
    def __init__(self, simus):
        self.simUse = simus
        self.ApnServer = ""  # "Access point name"
        self.ApnLogin = ""   # "Login GPRS"
        self.ApnPassword = ""  # "Password GPRS"
        self.DnsServer = ""   # "DNS server"
        self.ServerAddr = ""  # "IP/Name"
        self.ServerPort = 0
        self.ServerLogin = ""  # "Login"
        self.ServerPassword = ""
        return

class ServiceSeting:
    def __init__(self, simuse):
        self.simuse = 0
        self.Phone1 = ""
        self.Phone2 = ""
        self.Phone3 = ""
        self.ConnectRegim = 0
        return

class CODE_SENS:
      CODE_DIN = 0x00
      CODE_AIN = 0x10
      CODE_CNT = 0x20
      CODE_FUEL = 0x30
      CODE_RFID = 0x40
      CODE_CAN = 0x50
      CODE_DGPS = 0x60

class CODE_SENSTtus:
    CODE_DIN = 0x00
    CODE_AIN = 0x10
    CODE_CNT = 0x20
    CODE_LLSTYPE = 0x30
    CODE_CAN = 0x50
    CODE_DGPS = 0x60
    CODE_OWIRE = 0x70

class SmoutValue:
    def __init__(self):
        self.baseSensCode = 0
        self.porogUp = 0
        self.porogDown = 0
        self.cmdIn = ""
        self.cmdOut = ""
        return

class paramStringsIDX:
    DIN = 49 #"DIN"
    AIN = 48 #"AIN"
    COUNT = 50 #"COUNT"
    FUEL = 51 #"FUEL"
    RFID = 52 #"RFID"

class SensorsSetting:
    def __init__(self):
        self.code = 1
        self.enable = 0x00103F
        self.rs485Speed = 19200
        self.SensitivityAcc = 8
        self.dinSet = ''
        self.ainSet = ['','']
        self.cntSet = ['','','']
        self.fuelSet = ['','','','','']
        self.rfidSet = ['','']
        self.canSet = ['','','']
        self.rs485Set = ['','','','','','','']
        self.canSpeed1 = ''
        self.canSpeed2 = ''
        return

class DinSetting:
    evtMask = 0
    bounce = [0,0,0,0,0,0]

class CountSetting:
    modeCnt = 0
    event1 = 508
    event2 = 1016

class AinSetting:
    AinPorogUp = 1200
    AinPorogDown = 200

class FuelSetting:
    addressFuel = 0
    startBitFuel = 16
    lengthFuel = 16
    averageFuel = 20

class RfidSetting:
    addressRfid = 0
    startBitRfid = 0
    lengthBitRfid = 16

class cmdStrIDX:
    PHLEGAL1 = 0
    PHLEGAL2 = 1
    PHLEGAL3 = 2
    GREG = 3
    GAPN = 4
    GLOGIN = 5
    GPASS = 6
    GDNS = 7
    SSERV = 8
    SPORT = 9
    SLOGIN = 10
    SPASS = 11
    BMINSP = 12
    BTIM1 = 13
    BTIM2 = 14
    BCOURS = 15
    BDIST1 = 16
    BDIST2 = 17
    BACC = 18
    EVMINSP = 19
    EVTIM1 = 20
    EVTIM2 = 21
    EVCOURS = 22
    EVDIST1 = 23
    EVDIST2 = 24
    EVACC = 25
    EVPWRON = 26
    EVPWROFF = 27
    EVGSMON = 28
    EVGSMOFF = 29
    EVLOGF = 30
    EVPHONE1 = 31
    EVPHONE2 = 32
    EVPHONE3 = 33
    EVSENS = 34
    EVON = 35
    EVRST = 36
    USER = 37
    NACC = 38
    DOUT = 39
    ACCLVL = 40
    LASTGPS = 41
    VIEW = 42
    DEBUG = 43
    SMOUT = 44
    RS485SPEED = 45
    CODE = 46
    AIN = 47
    DIN = 48
    COUNT = 49
    RS485 = 50
    CANSPEED = 51
    REBOOT = 52
    CAN = 53
    SVIEW = 54
    OWIRE = 55
    CMD485 = 56

class GenerateCommandA1:
    LEVEL3_PACKET_LENGHT = 102
    CLEAR_MESSAGE_SYMBOL = '_'
    GprsEmailConfigSet = 51
    LEVEL3_DATA_BLOCK_POSITION = 3
    LEVEL1_LENGTH_POSITION = 2
    LEVEL1_DATA_BLOCK_LENGHT = 136
    LEVEL1_CRC_POSITION = 7
    LEVEL0_EMAIL_LENGTH = 13
    LEVEL0_START_INDICATOR_POSITION = 14
    LEVEL1_PACKET_LENGHT = 144
    START_INDICATOR_SYMBOL = '%'
    LEVEL0_PACKET_LENGTH = 160
    ASCII_CODE_A = ord('A')
    cmdMsgCnt = 0
    arrayFirst = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'q', 'a', 'z', 'w', 's', 'x', 'e', 'd', 'c',
                       'r', 'f', 'v', 't', 'g', 'b', 'y', 'h', 'n', 'u', 'j', 'i', 'k', 'o']
    arraySecond = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'w', 's', 'x', 'e', 'd', 'c', 'r', 'f',
                        'v', 't', 'g', 'b', 'y', 'h', 'n', 'u', 'j', 'm', 'i', 'k', 'o', 'l', 'p']

    def __init__(self, logid, passwd, ip):
        try:
            self.MAX_SENSORS = 16
            self.LogId = logid
            self.password = passwd
            self.addr = ip
            self.cmdMsgCnt = 0
            self.commandA1 = ''#self.coderCommandA1(self.LogId, self.password, self.addr)
            self.commandA1Event = '' #self.coderCommandA1Event(self.LogId, self.password, self.addr)
            self.commandA1Request = '' #self.coderCommandA1Request(self.LogId, self.password, self.addr)
            self.cmdEventVal = ["BMINSP", "BTIM1", "BTIM2", "BCOURS", "BDIST1", "BDIST2", "BACC",
             "EVMINSP", "EVTIM1", "EVTIM2", "EVCOURS", "EVDIST1", "EVDIST2", "EVACC", "EVPWRON", "EVPWROFF", "EVGSMON",
             "EVGSMOFF", "EVLOGF", "EVPHONE1", "EVPHONE2", "EVPHONE3", "EVSENS", "EVON", "EVRST"]
            self.cmdTransportVal = ["GAPN", "GLOGIN", "GPASS", "GDNS", "SSERV", "SPORT", "SLOGIN", "SPASS"]
            self.cmdService = ["GREG", "PHLEGAL1", "PHLEGAL2", "PHLEGAL3"]
            self.cmdSensorsBaseTtl = ["RS485SPEED", "ACCLVL", "CODE", "ENABLE"]
            self.cmdSensorsBaseTtu = ["RS485SPEED", "CANSPEED1", "CANSPEED2", "ACCLVL", "CODE"]
            self.needTx = []
            self.orderItemsTtlSys = ["DIN", "COUNT1", "COUNT2", "FUEL1", "FUEL2", "RFID1", "FUEL3", "FUEL4", "RFID2", "RESERVED1",
                                     "RESERVED2", "RESERVED3", "AIN1", "FUEL5", "AIN2", "COUNT3"]
            self.orderItemsTtuSys = ["DIN", "COUNT1", "COUNT2", "RS4851", "RS4852", "RS4853", "RS4854", "RS4855",
                                        "RS4856", "CAN1", "CAN2", "CAN3", "AIN1", "RS4857", "AIN2", "COUNT3"]
            self.listCommandTtl = ["AIN1", "AIN2", "COUNT1", "COUNT2", "COUNT3", "DIN", "FUEL1", "FUEL2", "FUEL3", "FUEL4",
                                 "FUEL5", "RESERVED1", "RESERVED2", "RESERVED3", "RFID1", "RFID2"]
            self.listCommandTtu = ["AIN1", "AIN2", "CAN1", "CAN2", "CAN3", "COUNT1", "COUNT2", "COUNT3", "DIN",
                                   "RS4851", "RS4852", "RS4853", "RS4854", "RS4855", "RS4856", "RS4857"]
            self.orderItemsTtl = {"DIN":False, "COUNT 1":False, "COUNT 2":False, "FUEL 1":False, "FUEL 2":False, "RFID 1":False,
                                  "FUEL 3":False, "FUEL 4":False, "RFID 2":False, "RESERVED 1":False, "RESERVED 2":False, "RESERVED 3":False,
                                  "AIN 1":False, "FUEL 5":False, "AIN 2":False, "COUNT 3":False}
            self.orderItemsTtu = {"DIN": False, "COUNT 1": False, "COUNT 2": False, "RS485 1": False, "RS485 2": False,
                                  "RS485 3": False, "RS485 4": False, "RS485 5": False, "RS485 6": False, "CAN 1": False,
                                  "CAN 2": False, "CAN 3": False, "AIN 1": False, "RS485 7": False, "AIN 2": False, "COUNT 3": False}
            self.MAX_SENSORS_TTL = 16
            self.sensCells = [CODE_SENS.CODE_DIN, CODE_SENS.CODE_CNT, CODE_SENS.CODE_CNT+1, CODE_SENS.CODE_FUEL, CODE_SENS.CODE_FUEL+1,
                              CODE_SENS.CODE_RFID, CODE_SENS.CODE_FUEL+2, CODE_SENS.CODE_FUEL+3, CODE_SENS.CODE_RFID+1, 0xFF, 0xFF, 0xFF, CODE_SENS.CODE_AIN,
                              CODE_SENS.CODE_FUEL+4, CODE_SENS.CODE_AIN+1, CODE_SENS.CODE_CNT+2, CODE_SENS.CODE_DGPS+2, CODE_SENS.CODE_DGPS+1, CODE_SENS.CODE_DGPS]
            self.typeSensTtu = [CODE_SENSTtus.CODE_DIN, CODE_SENSTtus.CODE_CNT, CODE_SENSTtus.CODE_CNT, CODE_SENSTtus.CODE_LLSTYPE, CODE_SENSTtus.CODE_LLSTYPE,
                                CODE_SENSTtus.CODE_LLSTYPE, CODE_SENSTtus.CODE_LLSTYPE, CODE_SENSTtus.CODE_LLSTYPE, CODE_SENSTtus.CODE_LLSTYPE, CODE_SENSTtus.CODE_CAN,
                                CODE_SENSTtus.CODE_CAN, CODE_SENSTtus.CODE_CAN, CODE_SENSTtus.CODE_AIN, CODE_SENSTtus.CODE_LLSTYPE, CODE_SENSTtus.CODE_AIN,
                                CODE_SENSTtus.CODE_CNT, CODE_SENSTtus.CODE_DGPS, CODE_SENSTtus.CODE_DGPS, CODE_SENSTtus.CODE_DGPS]
            self.numTypeSens = [0,0,1,0,   1,2,3,4,   5,0,1,2,   0,6,1,2,   2,1,0]
            self.smOutVal = SmoutValue()
            self.paramStrings = [
            "PHLEGAL1", #0
            "PHLEGAL2", #1
            "PHLEGAL3", #2
            "GREG", #3 (0/1)
            "GAPN", #4	(0/1)
            "GLOGIN", #5	(0/1)
            "GPASS", #6	(0/1)
            "GDNS", #7	(0/1)
            "SSERV", #8
            "SPORT", #9
            "SLOGIN", #10
            "SPASS", #11
            "BMINSP", #12  (0/1)
            "BTIM1", #13  (0/1)
            "BTIM2", #14  (0/1)
            "BCOURS", #15  (0/1)
            "BDIST1", #16  (0/1)
            "BDIST2", #17  (0/1)
            "BACC", #18  (0/1)
            "EVMINSP", #19  (0/1)
            "EVTIM1", #20  (0/1) intdex=[1]
            "EVTIM2", #21  (0/1) index=[2]
            "EVCOURS", #22	(0/1)
            "EVDIST1", #23	(0/1)
            "EVDIST2", #24	(0/1)
            "EVACC", #25   (0/1)
            "EVPWRON", #26	(0/1)
            "EVPWROFF", #27	(0/1)
            "EVGSMON", #28	(0/1)
            "EVGSMOFF", #29   (0/1)
            "EVLOGF", #30   (0/1)
            "EVPHONE1", #31	(0/1) #none
            "EVPHONE2", #32	(0/1) #none
            "EVPHONE3", #33   (0/1) #none
            "EVSENS", #34	(0/1)
            "EVON", #35	(0/1)
            "EVRST", #36  (0/1)
            "USER", #37
            "NACC", #38
            "DOUT", #39
            "ACCLVL", #40
            "LASTGPS", #41
            "VIEW", #42
            "DEBUG", #43
            "SMOUT1", #44 “SMOUT1=SENSOR:TopLVL:LowLVL:CMDin:CMDout”
            "SMOUT2", #45
            "RS485SPEED", #46
            "CODE", #47
            "AIN", #48 #"AINx",
            "DIN", #49 #
            "COUNT", #50 #"COUNTx"
            "FUEL", #51 #"FUELx",
            "RFID", #52 #"RFIDx"
            "CAN", #53 #"CANx",
            "ENABLE", #54 #
            "REBOOT" #55
            ]
            self.cmdStrings = [
            "PHLEGAL1",
            "PHLEGAL2",
            "PHLEGAL3",
            "GREG",
            "GAPN",
            "GLOGIN",
            "GPASS",
            "GDNS",
            "SSERV",
            "SPORT",
            "SLOGIN",
            "SPASS",
            "BMINSP",
            "BTIM1",
            "BTIM2",
            "BCOURS",
            "BDIST1",
            "BDIST2",
            "BACC",
            "EVMINSP",
            "EVTIM1",
            "EVTIM2",
            "EVCOURS",
            "EVDIST1",
            "EVDIST2",
            "EVACC",
            "EVPWRON",
            "EVPWROFF",
            "EVGSMON",
            "EVGSMOFF",
            "EVLOGF",
            "EVPHONE1",
            "EVPHONE2",
            "EVPHONE3",
            "EVSENS",
            "EVON",
            "EVRST",
            "USER",
            "NACC",
            "DOUT",
            "ACCLVL",
            "LASTGPS",
            "VIEW",
            "DEBUG",
            "SMOUT",
            "RS485SPEED",
            "CODE",
            "AIN",
            "DIN",
            "COUNT",
            "RS485",
            "CANSPEED",
            "REBOOT",
            "CAN",
            "SVIEW",
            "OWIRE",
            "CMD485"
        ]
        except Exception as err:
            print(err)
            print(traceback.format_exc())
        return

    def generatePassw(self, logid):
        try:
            code = ''
            logdev = logid.lower()
            for k in logdev:
                try:
                    indx = GenerateCommandA1.arrayFirst.index(k)
                    char = GenerateCommandA1.arraySecond[indx]
                except Exception as err:
                    print("Not valid symbol in login for tracker. " + str(err), str(logid), str(traceback.extract_stack()))
                    return ''
                code += char
            strcode = code.capitalize()
            last_symbol = strcode[3].title()
            password = strcode[0:3] + last_symbol + logid
            return password
        except Exception as err:
            print(str(err), '', str(traceback.extract_stack()))
            return ''

    def saveCommandInFile(self, msg):
        self.file = open("command.txt", "w")
        self.file.write(msg)
        self.file.close()
        return

    def GetA1Request(self):
        return self.commandA1Request

    def GetA1Event(self):
        return self.commandA1Event

    def GetA1(self):
        return self.commandA1

    def DataGridViewGetGprsEmail(self, logid, passw, ip):
        gprsEmailCfg = GprsEmailConfig()
        gprsEmailCfg.ShortID = logid
        gprsEmailCfg.Pop3Server = ip
        gprsEmailCfg.Pop3Login = logid + "@"
        gprsEmailCfg.Pop3Password = passw
        gprsEmailCfg.MessageID = 1
        gprsEmailCfg.SmtpServer = ""
        gprsEmailCfg.SmtpLogin = ""
        gprsEmailCfg.SmtpPassword = ""
        return gprsEmailCfg

    def UShortToBytes(self, value):
        array = []
        array.append(value >> 8)
        array.append(value & 0x000000FF)
        return array

    def GetCommandLevel3Template(self, commandId, messageId):
        result = []
        for i in range(GenerateCommandA1.LEVEL3_PACKET_LENGHT):
            result.append(0)

        result[0] = commandId
        tmp = self.UShortToBytes(messageId)
        result[1] = tmp[0]
        result[2] = tmp[1]
        for i in range(GenerateCommandA1.LEVEL3_PACKET_LENGHT - 3):
            result[i+3] = ord(GenerateCommandA1.CLEAR_MESSAGE_SYMBOL)
        return result

    def StringToBytes(self, value, maxLength):
        result = []
        count = min(len(value), maxLength)
        for i in range(len(value)):
            result.append(0)
        for i in range(count):
            result[i] = self.ConvertCharToAsciiWin1251(value[i])
        return result

    def ConvertCharToAsciiWin1251(self, symbol):
        if (symbol == 'Ё'):
            symbol = 'Е'
        elif (symbol == 'ё'):
            symbol = 'е'

        result = 0
        if (ord(symbol) < 0x100):
            result = ord(symbol)
        else:
            if ((ord(symbol) >= 0x410) and (ord(symbol) <= 0x44f)):
                result = 0xc0 + ord(symbol) - 0x410
            else:
              result = ord('?')
        return result

    def FillCommandAttribute(self, sourceAttribute, destinationCommand, startIndex, alignLength):
        tmpArray = []
        count = min(alignLength, len(sourceAttribute))
        for i in range(alignLength):
            tmpArray.append(0)
        k = 0
        for ls in sourceAttribute:
            tmpArray[k] = ls
            k += 1
        k = startIndex
        for ls in tmpArray:
            destinationCommand[k] = ls
            k += 1
        return destinationCommand

    def EncodeGprsEmailConfigSet(self, gprsEmailConfig):
        gprsEmailConfig.CommandID = self.GprsEmailConfigSet
        command = self.GetCommandLevel3Template(gprsEmailConfig.CommandID, gprsEmailConfig.MessageID)
        index = GenerateCommandA1.LEVEL3_DATA_BLOCK_POSITION
        command = self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.SmtpServer, 25), command, index, 25)
        index += 25
        command = self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.SmtpLogin, 10), command, index, 10)
        index += 10
        command = self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.SmtpPassword, 10), command, index, 10)
        index += 10
        command = self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.Pop3Server, 25), command, index, 25)
        index += 25
        command = self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.Pop3Login, 10), command, index, 10)
        index += 10
        command = self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.Pop3Password, 10), command, index, 10)
        gprsEmailConfig.Source = self.GetMessageLevel0(self.GetMessageLevel1(command, gprsEmailConfig.ShortID), "")
        gprsEmailConfig.Message = self.GetStringFromByteArray(gprsEmailConfig.Source)
        return (gprsEmailConfig.Message, gprsEmailConfig.CommandID)

    def WriteTelNumberSymbol(self, symbol, result, index):
        res = 255
        if symbol == ecs.Const.ZERO_SYMBOL:
            res = 0x0F
        elif symbol == '+':
            res = 0x0A
        elif symbol == '*':
            res = 0x0B
        elif symbol == '#':
            res = 0x0C
        elif symbol == ';':
            res = 0x0D
        else:
            if ((ord(symbol) >= 48) and (ord(symbol) <= 57)):
                res = int(ord(symbol) - 48)

        if (res < 16):
            if ((index & 0x01) != 0):
                result[index >> 1] = int((result[index >> 1] & 0xF0) | (res & 0x0F))
            else:
                result[index >> 1] = int((result[index >> 1] & 0x0F) | ((res & 0x0F) << 4))
        return

    def TelNumberToBytes(self, phoneNumber, maxLength):
        result = [0 for i in range(maxLength)]
        i = 0
        tmpLength = (maxLength << 1) - 2
        while ((i < len(phoneNumber)) and (i < tmpLength)):
            self.WriteTelNumberSymbol(phoneNumber[i], result, i)
            i += 1
        self.WriteTelNumberSymbol(';', result, i)
        i += 1
        self.WriteTelNumberSymbol(ecs.Const.ZERO_SYMBOL, result, i)
        return result

    def EncodeTextCmdConfigSet(self, textCmdRqst):
        textCmdRqst.CommandID = ecs.CommandDescriptor.TextCommand
        return (textCmdRqst.message, textCmdRqst.CommandID)

    def EncodePhoneNumberConfigSet(self, phoneNumberConf):
        phoneNumberConf.CommandID = ecs.CommandDescriptor.PhoneConfigSet
        command = self.GetCommandLevel3Template(phoneNumberConf.CommandID, phoneNumberConf.MessageID)
        index = GenerateCommandA1.LEVEL3_DATA_BLOCK_POSITION
        self.FillCommandAttribute(self.TelNumberToBytes(phoneNumberConf.NumberAccept1, 11), command, index, 11)
        index += 11
        self.FillCommandAttribute(self.TelNumberToBytes(phoneNumberConf.NumberAccept2, 11), command, index, 11)
        index += 11
        self.FillCommandAttribute(self.TelNumberToBytes(phoneNumberConf.NumberAccept3, 11), command, index, 11)
        index += 11
        self.FillCommandAttribute(self.TelNumberToBytes(phoneNumberConf.NumberDspt, 11), command, index, 11)
        index += 11
        byte = []
        byte.append(0)
        self.FillCommandAttribute(byte, command, index, 1)
        index += 1
        self.FillCommandAttribute(self.StringToBytes(phoneNumberConf.Name1, 8), command, index, 8)
        index += 8
        self.FillCommandAttribute(self.StringToBytes(phoneNumberConf.Name2, 8), command, index, 8)
        index += 8
        self.FillCommandAttribute(self.StringToBytes(phoneNumberConf.Name3, 8), command, index, 8)
        index += 8
        self.FillCommandAttribute(self.TelNumberToBytes(phoneNumberConf.NumberSOS, 11), command, index, 11)
        phoneNumberConf.Source = self.GetMessageLevel0(self.GetMessageLevel1(command, phoneNumberConf.ShortID), "")
        phoneNumberConf.Message = self.GetStringFromByteArray(phoneNumberConf.Source)
        return (phoneNumberConf.Message, phoneNumberConf.CommandID)

    def EncodeEventConfigSet(self, eventConfig):
        eventConfig.CommandID = ecs.CommandDescriptor.EventConfigSet
        command = self.GetCommandLevel3Template(eventConfig.CommandID, eventConfig.messageID)
        index = ecs.Const.LEVEL3_DATA_BLOCK_POSITION
        bts = []
        bts.append(eventConfig.speedChange)
        self.FillCommandAttribute(bts, command, index, 1)
        index += 1
        self.FillCommandAttribute(self.UShortToBytes(eventConfig.courseBend), command, index, 2)
        index += 2
        self.FillCommandAttribute(self.UShortToBytes(eventConfig.distance1), command, index, 2)
        index += 2
        self.FillCommandAttribute(self.UShortToBytes(eventConfig.distance2), command, index, 2)
        index += 2
        for i in range(32):
            self.FillCommandAttribute(self.UShortToBytes(eventConfig.eventMask[i]), command, index, 2)
            index += 2
        self.FillCommandAttribute(self.UShortToBytes(eventConfig.minSpeed), command, index, 2)
        index += 2
        self.FillCommandAttribute(self.UShortToBytes(eventConfig.timer1), command, index, 2)
        index += 2
        self.FillCommandAttribute(self.UShortToBytes(eventConfig.timer2), command, index, 2)
        index += 2
        array = []
        array.append(0)
        array.append(0)
        self.FillCommandAttribute(array, command, index, 2)

        eventConfig.source = self.GetMessageLevel0(self.GetMessageLevel1(command, eventConfig.shortID), "")
        eventConfig.message = self.GetStringFromByteArray(eventConfig.source)
        return eventConfig.message

    def EncodeEventConfigQuery(self, query):
        query.CommandID = ecs.CommandDescriptor.EventConfigQuery
        command = self.GetCommandLevel3Template(query.CommandID, query.messageID)
        query.source = self.GetMessageLevel0(self.GetMessageLevel1(command, query.shortId), "")
        query.message = self.GetStringFromByteArray(query.source)
        return query.message

    def EncodeGprsBaseConfigQuery(self, query):
        query.CommandID = ecs.CommandDescriptor.GprsBaseConfigQuery
        command = self.GetCommandLevel3Template(query.CommandID, query.messageID)
        query.source = self.GetMessageLevel0(self.GetMessageLevel1(command, query.shortId), "")
        query.message = self.GetStringFromByteArray(query.source)
        return query.message

    def EncodeGprsEmailConfigQuery(self, query):
        query.CommandID = ecs.CommandDescriptor.GprsEmailConfigQuery
        command = self.GetCommandLevel3Template(query.CommandID, query.messageID)
        query.source = self.GetMessageLevel0(self.GetMessageLevel1(command, query.shortId), "")
        query.message = self.GetStringFromByteArray(query.source)
        return query.message

    def UIntToBytes(self, value):
        highArray = self.UShortToBytes(value >> 16)
        lowArray = self.UShortToBytes(value)
        return [highArray[0], highArray[1], lowArray[0], lowArray[1]]

    def IntToBytes(self, value):
        highArray = self.UShortToBytes(value >> 16)
        lowArray = self.UShortToBytes(value)
        return [highArray[0], highArray[1], lowArray[0], lowArray[1]]

    def EncodeDataGpsQueryWrite(self, query):
        res = self.EncodeDataGpsQuery(query)
        return (res, query.CommandID)

    def EncodeDataGpsQuery(self, query):
        query.CommandID = int(ecs.CommandDescriptor.DataGpsQuery)
        command = self.GetCommandLevel3Template(query.CommandID, query.MessageID)
        index = ecs.Const.LEVEL3_DATA_BLOCK_POSITION
        self.FillCommandAttribute(self.UIntToBytes(query.CheckMask), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.UIntToBytes(query.Events), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.UIntToBytes(query.LastRecords), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.UIntToBytes(query.LastTimes), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.UIntToBytes(query.LastMeters), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.UIntToBytes(query.MaxSmsNumber), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.UIntToBytes(query.WhatSend), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.IntToBytes(query.StartTime), command, index, 4)
        index += 4
        self.FillCommandAttribute([query.StartSpeed], command, index, 1)
        index += 1
        self.FillCommandAttribute(self.IntToBytes(int(query.StartLatitude / 10)), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.IntToBytes(int(query.StartLongitude / 10)), command, index, 4)
        index += 4
        self.FillCommandAttribute([query.StartAltitude], command, index, 1)
        index += 1
        self.FillCommandAttribute([query.StartDirection], command, index, 1)
        index += 1
        self.FillCommandAttribute(self.IntToBytes(query.StartLogId), command, index, 4)
        index += 4
        self.FillCommandAttribute([query.StartFlags], command, index, 1)
        index += 1
        self.FillCommandAttribute(self.IntToBytes(query.EndTime), command, index, 4)
        index += 4
        self.FillCommandAttribute([query.EndSpeed], command, index, 1)
        index += 1
        self.FillCommandAttribute(self.IntToBytes(int(query.EndLatitude / 10)), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.IntToBytes(int(query.EndLongitude / 10)), command, index, 4)
        index += 4
        self.FillCommandAttribute([query.EndAltitude], command, index, 1)
        index += 1
        self.FillCommandAttribute([query.EndDirection], command, index, 1)
        index += 1
        self.FillCommandAttribute(self.IntToBytes(query.EndLogId), command, index, 4)
        index += 4
        self.FillCommandAttribute([query.EndFlags], command, index, 1)
        index += 1
        self.FillCommandAttribute(self.IntToBytes(query.LogId1), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.IntToBytes(query.LogId2), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.IntToBytes(query.LogId3), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.IntToBytes(query.LogId4), command, index, 4)
        index += 4
        self.FillCommandAttribute(self.IntToBytes(query.LogId5), command, index, 4)
        query.Source = self.GetMessageLevel0(self.GetMessageLevel1(command, query.ShortID), "")
        query.Message = self.GetStringFromByteArray(query.Source)
        return query.Message

    def EncodePhoneNumberConfigQuery(self, query):
        query.CommandID = ecs.CommandDescriptor.PhoneConfigQuery
        command = self.GetCommandLevel3Template(query.CommandID, query.messageID)
        query.source = self.GetMessageLevel0(self.GetMessageLevel1(command, query.shortId), "")
        query.message = self.GetStringFromByteArray(query.source)
        return query.message

    def ValueToSymbol(self, value):
        symbol = ord('.')
        ASCII_CODE_A = ord('A')
        if (value <= 25):
            symbol = ASCII_CODE_A + value
        elif ((value >= 26) and (value <= 51)):
            symbol = ord('a') + value - 26
        elif ((value >= 52) and (value <= 61)):
            symbol = (ord('0') + value - 52)
        elif (value == 62):
            symbol = ord('+')
        elif (value == 63):
            symbol = ord('-')
        elif ((value >= ASCII_CODE_A) and (value <= ord('F'))):
            symbol = value - ASCII_CODE_A + 58
        return symbol

    def Encode8BitTo6(self, source):
        result = []
        for i in range(GenerateCommandA1.LEVEL1_DATA_BLOCK_LENGHT):
            result.append(0)

        blocks = GenerateCommandA1.LEVEL1_DATA_BLOCK_LENGHT >> 2

        for i in range(blocks):
            sA = 0
            sB = 0
            sC = 0
            sD = 0
            ind = 3 * i

            sX = source[ind]
            sY = source[ind + 1]
            sZ = source[ind + 2]

            sA = sX >> 2
            sB = sY >> 2
            sC = sZ >> 2

            sX = (sX << 6) & 0b11000000
            sX = sX >> 6
            sY = (sY << 6) & 0b11000000
            sY = sY >> 4
            sZ = (sZ << 6) & 0b11000000
            sZ = sZ >> 2
            sD = sX + sY + sZ

            result[i * 4] = self.ValueToSymbol(sA)
            result[i * 4 + 1] = self.ValueToSymbol(sB)
            result[i * 4 + 2] = self.ValueToSymbol(sC)
            result[i * 4 + 3] = self.ValueToSymbol(sD)
        return result

    def GetMessageLevel1(self, level3Command, devShortId):
        result = []
        for i in range(GenerateCommandA1.LEVEL1_PACKET_LENGHT):
            result.append(0)
        result[0] = self.ValueToSymbol(0)
        result[1] = self.ValueToSymbol(0)
        result[GenerateCommandA1.LEVEL1_LENGTH_POSITION] = self.ValueToSymbol(GenerateCommandA1.LEVEL1_DATA_BLOCK_LENGHT >> 2)
        result[GenerateCommandA1.LEVEL1_LENGTH_POSITION + 1] = self.ValueToSymbol(ord(devShortId[0]))
        result[GenerateCommandA1.LEVEL1_LENGTH_POSITION + 2] = self.ValueToSymbol(ord(devShortId[1]))
        result[GenerateCommandA1.LEVEL1_LENGTH_POSITION + 3] = self.ValueToSymbol(ord(devShortId[2]))
        result[GenerateCommandA1.LEVEL1_LENGTH_POSITION + 4] = self.ValueToSymbol(ord(devShortId[3]))
        temp = self.Encode8BitTo6(level3Command)
        k = GenerateCommandA1.LEVEL1_CRC_POSITION + 1
        for ls in temp:
            result[k] = ls
            k += 1
        #CRC
        result[GenerateCommandA1.LEVEL1_CRC_POSITION] = self.ValueToSymbol(self.CalculateLevel1CRC(result, 0, GenerateCommandA1.LEVEL1_DATA_BLOCK_LENGHT))
        return result

    def GetStringFromByteArray(self, source):
        result = ""
        for i in range(len(source)):
            result += chr(source[i])
        return result

    def GetMessageLevel0(self, level1Message, email):
        result = []
        for i in range(GenerateCommandA1.LEVEL0_PACKET_LENGTH):
            result.append(0)

        for i in range(GenerateCommandA1.LEVEL0_EMAIL_LENGTH + 1):
            if (i < len(email)):
                value = ord(email[i])
                if ((value == 0) or (value == 255)):
                    result[i] = ord(' ')
                else:
                    result[i] = value
            else:
                result[i] = ord(' ')
        result[GenerateCommandA1.LEVEL0_EMAIL_LENGTH] = ord(' ')
        start_indicator_symbol = ord(GenerateCommandA1.START_INDICATOR_SYMBOL)
        result[GenerateCommandA1.LEVEL0_START_INDICATOR_POSITION] = start_indicator_symbol
        result[GenerateCommandA1.LEVEL0_START_INDICATOR_POSITION + 1] = start_indicator_symbol
        k = GenerateCommandA1.LEVEL0_START_INDICATOR_POSITION + 2
        for l in range(GenerateCommandA1.LEVEL1_PACKET_LENGHT):
            result[k] = level1Message[l]
            k += 1
        return result

    def coderCommandA1(self, idlog, passwd, address):
        gprsemailconf = self.DataGridViewGetGprsEmail(idlog, passwd, address)
        txCommand = self.EncodeGprsEmailConfigSet(gprsemailconf)
        return txCommand

    def DataGridViewGetEvent(self, shortId):
        eventCfg = ecs.EventConfig()
        eventCfg.shortID = shortId
        self.cmdMsgCnt += 1
        eventCfg.messageID = self.cmdMsgCnt
        eventCfg.courseBend = ecs.dataGridViewEvent.Rows[3].Cells["ColumnEventsValue"].Value
        eventCfg.distance1 = self.dataGridViewEvent.Rows[4].Cells["ColumnEventsValue"].Value
        eventCfg.distance2 = self.dataGridViewEvent.Rows[5].Cells["ColumnEventsValue"].Value
        eventCfg.minSpeed = self.dataGridViewEvent.Rows[0].Cells["ColumnEventsValue"].Value
        eventCfg.speedChange = self.dataGridViewEvent.Rows[6].Cells["ColumnEventsValue"].Value
        eventCfg.timer1 = self.dataGridViewEvent.Rows[1].Cells["ColumnEventsValue"].Value
        eventCfg.timer2 = self.dataGridViewEvent.Rows[2].Cells["ColumnEventsValue"].Value

        if (self.dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_MIN_SPEED] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_MIN_SPEED] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_MIN_SPEED] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_TIMER1] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_TIMER1] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_TIMER1] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_TIMER2] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_TIMER2] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_TIMER2] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_COURSE] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_COURSE] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_COURSE] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_DISTANCE1] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_DISTANCE1] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_DISTANCE1] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_DISTANCE2] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_DISTANCE2] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_DISTANCE2] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_ACCELERATION] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_ACCELERATION] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_ACCELERATION] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_POWER_ON] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_POWER_ON] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_POWER_ON] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_POWER_OFF] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_POWER_OFF] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_POWER_OFF] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_GSM_FIND] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_GSM_FIND] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_GSM_FIND] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_GSM_LOST] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_GSM_LOST] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_GSM_LOST] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_LOG_FULL] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_LOG_FULL] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_LOG_FULL] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_SENSORS] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_SENSORS] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_SENSORS] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_ON] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_ON] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_ON] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_REBOOT] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_REBOOT] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_REBOOT] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_PHONE1] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_PHONE1] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_PHONE1] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_PHONE2] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_PHONE2] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_PHONE2] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_PHONE3] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_PHONE3] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_PHONE3] |= eventCfg.BIT_SEND_SMS

        if (self.dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckWritePoint"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_DISPETCHER] |= eventCfg.BIT_WRITE_LOG
        if (self.dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckSendToServer"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_DISPETCHER] |= eventCfg.BIT_SEND_TO_SERVER
        if (self.dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckSendSMS"].Value == True):
            eventCfg.eventMask[eventCfg.INDEX_RING_DISPETCHER] |= eventCfg.BIT_SEND_SMS
        return eventCfg

    def EncodeGprsEmailConfigSet(self, gprsEmailConfig):
        gprsEmailConfig.CommandID = ecs.CommandDescriptor.GprsEmailConfigSet
        command = self.GetCommandLevel3Template(gprsEmailConfig.CommandID, gprsEmailConfig.MessageID)
        index = ecs.Const.LEVEL3_DATA_BLOCK_POSITION
        self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.smtpServer, 25), command, index, 25)
        index += 25
        self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.smtpLogin, 10), command, index, 10)
        index += 10
        self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.smtpPassword, 10), command, index, 10)
        index += 10
        self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.Pop3Server, 25), command, index, 25)
        index += 25
        self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.Pop3Login, 10), command, index, 10)
        index += 10
        self.FillCommandAttribute(self.StringToBytes(gprsEmailConfig.Pop3Password, 10), command, index, 10)
        gprsEmailConfig.Source = self.GetMessageLevel0(self.GetMessageLevel1(command, gprsEmailConfig.ShortID), "")
        gprsEmailConfig.Message = self.GetStringFromByteArray(gprsEmailConfig.Source)
        return (gprsEmailConfig.Message, gprsEmailConfig.CommandID)

    def EncodeGprsBaseConfigSet(self, gprsBaseConfig):
        gprsBaseConfig.CommandID = ecs.CommandDescriptor.GprsBaseConfigSet
        command = self.GetCommandLevel3Template(gprsBaseConfig.CommandID, gprsBaseConfig.MessageID)
        index = ecs.Const.LEVEL3_DATA_BLOCK_POSITION
        self.FillCommandAttribute(self.UShortToBytes(gprsBaseConfig.Mode), command, index, 2)
        index += 2
        self.FillCommandAttribute(self.StringToBytes(gprsBaseConfig.ApnServer, 25), command, index, 25)
        index += 25
        self.FillCommandAttribute(self.StringToBytes(gprsBaseConfig.ApnLogin, 10), command, index, 10)
        index += 10
        self.FillCommandAttribute(self.StringToBytes(gprsBaseConfig.ApnPassword, 10), command, index, 10)
        index += 10
        self.FillCommandAttribute(self.StringToBytes(gprsBaseConfig.DnsServer, 16), command, index, 16)
        index += 16
        self.FillCommandAttribute(self.StringToBytes(gprsBaseConfig.DialNumber, 11), command, index, 11)
        index += 11
        self.FillCommandAttribute(self.StringToBytes(gprsBaseConfig.GprsLogin, 10), command, index, 10)
        index += 10
        self.FillCommandAttribute(self.StringToBytes(gprsBaseConfig.GprsPassword, 10), command, index, 10)
        gprsBaseConfig.Source = self.GetMessageLevel0(self.GetMessageLevel1(command, gprsBaseConfig.ShortID), "")
        gprsBaseConfig.Message = self.GetStringFromByteArray(gprsBaseConfig.Source)
        return (gprsBaseConfig.Message, gprsBaseConfig.CommandID)

    def coderCommandA1Event(self, idlog):
        eventConf = self.DataGridViewGetEvent(idlog)
        txString = self.EncodeEventConfigSet(eventConf)
        commandID = int(ecs.CommandDescriptor.EventConfigSet)
        return (txString, commandID)

    def GetFlagEvent(self, systable, index):
        ret = 0
        mainWidget = systable.cellWidget(index, 3)
        checkBox = mainWidget.layout().itemAt(0).widget()
        if checkBox.isChecked() == True:
            ret += EventSetting.BIT_WRITE_LOG

        mainWidget = systable.cellWidget(index, 4)
        checkBox = mainWidget.layout().itemAt(0).widget()
        if checkBox.isChecked():
            ret += EventSetting.BIT_SEND_TO_SERVER
        return ret

    def GetEvent(self, table, simUse=0):
        retEvt = EventSetting(simUse)
        retEvt.MinSpeed = int((table.item(0, 1).text()))
        retEvt.EventMask[EventSetting.INDEX_MIN_SPEED] = self.GetFlagEvent(table, 0)
        retEvt.Timer1 = int(table.item(1, 1).text())
        retEvt.EventMask[EventSetting.INDEX_TIMER1] = self.GetFlagEvent(table, 1)
        retEvt.Timer2 = int(table.item(2, 1).text())
        retEvt.EventMask[EventSetting.INDEX_TIMER2] = self.GetFlagEvent(table, 2)
        retEvt.CourseBend = int(table.item(3, 1).text())
        retEvt.EventMask[EventSetting.INDEX_COURSE] = self.GetFlagEvent(table, 3)
        retEvt.Distance1 = int(table.item(4, 1).text())
        retEvt.EventMask[EventSetting.INDEX_DISTANCE1] = self.GetFlagEvent(table, 4)
        retEvt.Distance2 = int(table.item(5, 1).text())
        retEvt.EventMask[EventSetting.INDEX_DISTANCE2] = self.GetFlagEvent(table, 5)
        retEvt.SpeedChange = int(table.item(6, 1).text())
        retEvt.EventMask[EventSetting.INDEX_ACCELERATION] = self.GetFlagEvent(table, 6)
        retEvt.EventMask[EventSetting.INDEX_POWER_ON] = self.GetFlagEvent(table, 7)
        retEvt.EventMask[EventSetting.INDEX_POWER_OFF] = self.GetFlagEvent(table, 8)
        retEvt.EventMask[EventSetting.INDEX_GSM_FIND] = self.GetFlagEvent(table, 9)
        retEvt.EventMask[EventSetting.INDEX_GSM_LOST] = self.GetFlagEvent(table, 10)
        retEvt.EventMask[EventSetting.INDEX_LOG_FULL] = self.GetFlagEvent(table, 11)
        retEvt.EventMask[EventSetting.INDEX_SENSORS] = self.GetFlagEvent(table, 12)
        retEvt.EventMask[EventSetting.INDEX_ON] = self.GetFlagEvent(table, 13)
        retEvt.EventMask[EventSetting.INDEX_REBOOT] = self.GetFlagEvent(table, 14)
        retEvt.EventMask[EventSetting.INDEX_RING_PHONE1] = self.GetFlagEvent(table, 15)
        retEvt.EventMask[EventSetting.INDEX_RING_PHONE2] = self.GetFlagEvent(table, 16)
        retEvt.EventMask[EventSetting.INDEX_RING_PHONE3] = self.GetFlagEvent(table, 17)
        return retEvt

    def GetTransport(self, tserv, tgprs, sim=-1):
        retTransp = TransportSetting(sim)
        retTransp.ApnServer = tgprs.item(0, 1).text() # "Access point name"
        retTransp.ApnLogin = tgprs.item(1, 1).text() # "Login GPRS"
        retTransp.ApnPassword = tgprs.item(2, 1).text() # "Password GPRS"
        retTransp.DnsServer = tgprs.item(3, 1).text() # "DNS server"

        retTransp.ServerAddr = tserv.item(0, 1).text() # "IP/Name"
        retTransp.ServerPort = tserv.item(1, 1).text()
        retTransp.ServerLogin = tserv.item(2, 1).text() # "Login"
        retTransp.ServerPassword = tserv.item(3, 1).text() # "Password"
        return retTransp

    def GetService(self, tsPhone, regim, simuse):
        retService =  ServiceSeting(simuse)
        retService.Phone1 = tsPhone.item(0, 1).text() # "Legal phone 1"
        retService.Phone2 = tsPhone.item(1, 1).text() # "Legal phone 2"
        retService.Phone3 = tsPhone.item(2, 1).text() # "Legal phone 3"
        retService.ConnectRegim = int(regim.text())
        return retService

    def GetSensorsTtl(self, this):
        listItemsTtl = this.items
        retSensSet = SensorsSetting()
        retSensSet.enable = self.GetSensorsEnableTtl()
        retSensSet.code = int(this.textBoxSensCode)
        retSensSet.rs485Speed = int(this.comboBoxSpeedRs485)
        retSensSet.SensitivityAcc = int(this.numericUpDownAccSensetivity)
        for box in this.checkedListBoxSensors:
            if box.text() == listItemsTtl[0]: #AIN1
                ret = AinSetting()
                ret.AinPorogUp = int(this.ainUpDownLvlUp1)
                ret.AinPorogDown = int(this.ainUpDownLvlDown1)
                retSensSet.ainSet[0] = ret
            elif box.text() == listItemsTtl[1]: #AIN2
                ret = AinSetting()
                ret.AinPorogUp = int(this.ainUpDownLvlUp2)
                ret.AinPorogDown = int(this.ainUpDownLvlDown2)
                retSensSet.ainSet[1] = ret
            elif box.text() == listItemsTtl[2]: #COUNT1
                cntSet = CountSetting()
                cntSet.modeCnt = int(this.countRegimSelectedIndex1)
                cntSet.event1 = int(this.countUpDownEvent1Value1)
                cntSet.event2 = int(this.countUpDownEvent2Value1)
                retSensSet.cntSet[0] = cntSet
            elif box.text() == listItemsTtl[3]: #COUNT2
                cntSet = CountSetting()
                cntSet.modeCnt = int(this.countRegimSelectedIndex2)
                cntSet.event1 = int(this.countUpDownEvent1Value2)
                cntSet.event2 = int(this.countUpDownEvent2Value2)
                retSensSet.cntSet[1] = cntSet
            elif box.text() == listItemsTtl[4]: #COUNT3
                cntSet = CountSetting()
                cntSet.modeCnt = int(this.countRegimSelectedIndex3)
                cntSet.event1 = int(this.countUpDownEvent1Value3)
                cntSet.event2 = int(this.countUpDownEvent2Value3)
                retSensSet.cntSet[2] = cntSet
            elif box.text() == listItemsTtl[5]: #DIN
                ret = DinSetting()
                ret.bounce[0] = this.Din1SelectedIndex
                ret.bounce[1] = this.Din2SelectedIndex
                ret.evtMask = 0
                if (this.checkSaveDin1): ret.evtMask |= 0x80
                if (this.checkSaveDin2): ret.evtMask |= 0x40
                retSensSet.dinSet = ret
            elif box.text() == listItemsTtl[6]: #FUEL1
                fuelSet = FuelSetting()
                fuelSet.addressFuel = int(this.fuelUpDownAddrValue1)
                fuelSet.startBitFuel = int(this.fuelUpDownStartBitValue1)
                fuelSet.lengthFuel = int(this.fuelUpDownLengthBitValue1)
                fuelSet.averageFuel = int(this.fuelUpDownAverageValue1)
                retSensSet.fuelSet[0] = fuelSet
            elif box.text() == listItemsTtl[7]: #FUEL2
                fuelSet = FuelSetting()
                fuelSet.addressFuel = int(this.fuelUpDownAddrValue2)
                fuelSet.startBitFuel = int(this.fuelUpDownStartBitValue2)
                fuelSet.lengthFuel = int(this.fuelUpDownLengthBitValue2)
                fuelSet.averageFuel = int(this.fuelUpDownAverageValue2)
                retSensSet.fuelSet[1] = fuelSet
            elif box.text() == listItemsTtl[8]: #FUEL3
                fuelSet = FuelSetting()
                fuelSet.addressFuel = int(this.fuelUpDownAddrValue3)
                fuelSet.startBitFuel = int(this.fuelUpDownStartBitValue3)
                fuelSet.lengthFuel = int(this.fuelUpDownLengthBitValue3)
                fuelSet.averageFuel = int(this.fuelUpDownAverageValue3)
                retSensSet.fuelSet[2] = fuelSet
            elif box.text() == listItemsTtl[9]: #FUEL4
                fuelSet = FuelSetting()
                fuelSet.addressFuel = int(this.fuelUpDownAddrValue4)
                fuelSet.startBitFuel = int(this.fuelUpDownStartBitValue4)
                fuelSet.lengthFuel = int(this.fuelUpDownLengthBitValue4)
                fuelSet.averageFuel = int(this.fuelUpDownAverageValue4)
                retSensSet.fuelSet[3] = fuelSet
            elif box.text() == listItemsTtl[10]: #FUEL5
                fuelSet = FuelSetting()
                fuelSet.addressFuel = int(this.fuelUpDownAddrValue5)
                fuelSet.startBitFuel = int(this.fuelUpDownStartBitValue5)
                fuelSet.lengthFuel = int(this.fuelUpDownLengthBitValue5)
                fuelSet.averageFuel = int(this.fuelUpDownAverageValue5)
                retSensSet.fuelSet[4] = fuelSet
            elif box.text() == listItemsTtl[11]: # RESERVED1
                pass
            elif box.text() == listItemsTtl[12]: # RESERVED2
                pass
            elif box.text() == listItemsTtl[13]: # RESERVED3
                pass
            elif box.text() == listItemsTtl[14]: #RFID1
                rfidSet = RfidSetting()
                rfidSet.addressRfid = int(this.rfidUpDownAddrValue1)
                rfidSet.lengthBitRfid = int(this.rfidUpDownLengthBitValue1)
                rfidSet.startBitRfid = int(this.rfidUpDownStartBitValue1)
                retSensSet.rfidSet[0] = rfidSet
            elif box.text() == listItemsTtl[15]: #RFID2
                rfidSet = RfidSetting()
                rfidSet.addressRfid = int(this.rfidUpDownAddrValue2)
                rfidSet.lengthBitRfid = int(this.rfidUpDownLengthBitValue2)
                rfidSet.startBitRfid = int(this.rfidUpDownStartBitValue2)
                retSensSet.rfidSet[1] = rfidSet
            else:
                raise Exception("GetSensorsTtl. Unknown sensors key:" + box.text())
        return retSensSet

    def SmoutGetValue(self, this):
        smOut = SmoutValue()
        smOut.baseSensCode = int(this.DinUsedIn)
        smOut.porogUp = int(this.DinLvlUp)
        smOut.porogDown = int(this.DinLvlLow)
        smOut.cmdIn = this.DinCmdIn
        smOut.cmdOut = this.DinCmdOut
        return smOut

    def GetSensorsTtu(self, this):
        listItemsTtu = this.items
        retSensSet = SensorsSetting()
        retSensSet.enable = self.GetSensorsEnableTtu()
        retSensSet.rs485Speed = int(this.comboBoxSpeedRs485)
        retSensSet.SensitivityAcc = int(this.numericUpDownAccSensetivity)
        if this.canSpeed1 == "" or this.canSpeed1 == "----":
            retSensSet.canSpeed1 = 0xFFFFFFFF
        else:
            retSensSet.canSpeed1 = int(this.canSpeed1)
        if this.canSpeed2 == "" or this.canSpeed2 == "----":
            retSensSet.canSpeed2 = 0xFFFFFFFF
        else:
            retSensSet.canSpeed2 = int(this.canSpeed2)

        for box in this.checkedListBoxSensors:
            if box.text() == listItemsTtu[0]: #AIN1 ok
                ret = AinSetting()
                ret.AinPorogUp = int(this.ainUpDownLvlUp1)
                ret.AinPorogDown = int(this.ainUpDownLvlDown1)
                retSensSet.ainSet[0] = ret
            elif box.text() == listItemsTtu[1]: #AIN2 ok
                ret = AinSetting()
                ret.AinPorogUp = int(this.ainUpDownLvlUp2)
                ret.AinPorogDown = int(this.ainUpDownLvlDown2)
                retSensSet.ainSet[1] = ret
            elif box.text() == listItemsTtu[2]: #CAN1 ok
                tmpCanSet = CanSensors()
                tmpCanSet.lenBit = this.lengthCan1
                tmpCanSet.startBit = this.startbitCan1
                tmpCanSet.timeout = this.timeoutCan1
                tmpCanSet.pgn = int(this.addresCan1, 16)
                tmpCanSet.mask = int(this.maskCan1, 16)
                tmpCanSet.canUse = this.workCan1
                retSensSet.canSet[0] = tmpCanSet
            elif box.text() == listItemsTtu[3]: #CAN2 ok
                tmpCanSet = CanSensors()
                tmpCanSet.lenBit = this.lengthCan2
                tmpCanSet.startBit = this.startbitCan2
                tmpCanSet.timeout = this.timeoutCan2
                tmpCanSet.pgn = int(this.addresCan2, 16)
                tmpCanSet.mask = int(this.maskCan2, 16)
                tmpCanSet.canUse = this.workCan2
                retSensSet.canSet[1] = tmpCanSet
            elif box.text() == listItemsTtu[4]: #CAN3 ok
                tmpCanSet = CanSensors()
                tmpCanSet.lenBit = this.lengthCan3
                tmpCanSet.startBit = this.startbitCan3
                tmpCanSet.timeout = this.timeoutCan3
                tmpCanSet.pgn = int(this.addresCan3, 16)
                tmpCanSet.mask = int(this.maskCan3, 16)
                tmpCanSet.canUse = this.workCan3
                retSensSet.canSet[2] = tmpCanSet
            elif box.text() == listItemsTtu[5]:  # COUNT1 ok
                cntSet = CountSetting()
                cntSet.modeCnt = int(this.countRegimSelectedIndex1)
                cntSet.event1 = int(this.countUpDownEvent1Value1)
                cntSet.event2 = int(this.countUpDownEvent2Value1)
                cntSet.source = int(this.countSourceValue1)
                if (this.countSourseDin1 > 0):
                    cntSet.source |= CODE_SENS.CODE_CAN
                retSensSet.cntSet[0] = cntSet
            elif box.text() == listItemsTtu[6]:  # COUNT2 ok
                cntSet = CountSetting()
                cntSet.modeCnt = int(this.countRegimSelectedIndex2)
                cntSet.event1 = int(this.countUpDownEvent1Value2)
                cntSet.event2 = int(this.countUpDownEvent2Value2)
                cntSet.source = int(this.countSourceValue2)
                if (this.countSourseDin2 > 0):
                    cntSet.source |= CODE_SENS.CODE_CAN
                retSensSet.cntSet[1] = cntSet
            elif box.text() == listItemsTtu[7]:  # COUNT3 ok
                cntSet = CountSetting()
                cntSet.modeCnt = int(this.countRegimSelectedIndex3)
                cntSet.event1 = int(this.countUpDownEvent1Value3)
                cntSet.event2 = int(this.countUpDownEvent2Value3)
                cntSet.source = int(this.countSourceValue3)
                if (this.countSourseDin3 > 0):
                    cntSet.source |= CODE_SENS.CODE_CAN
                retSensSet.cntSet[2] = cntSet
            elif box.text() == listItemsTtu[8]: #DIN ok
                ret = DinSetting()
                ret.bounce[0] = this.Din1SelectedIndex
                ret.bounce[1] = this.Din2SelectedIndex
                ret.bounce[2] = this.Din3SelectedIndex
                ret.bounce[3] = this.Din4SelectedIndex
                ret.evtMask = 0
                if (this.checkSaveDin1): ret.evtMask |= 0x80
                if (this.checkSaveDin2): ret.evtMask |= 0x40
                if (this.checkSaveDin3): ret.evtMask |= 0x20
                if (this.checkSaveDin4): ret.evtMask |= 0x10
                retSensSet.dinSet = ret
                self.smOutVal = self.SmoutGetValue(this)
            elif box.text() == listItemsTtu[9]: #RS4851 ok
                llsSet = LlsTypeSetting()
                llsSet.type = TYPE_LLS.OMNICOMM_RES
                if (this.rs485TypeSens1 == 'RFID'):
                    llsSet.type = TYPE_LLS.SOVA_RES
                llsSet.evtDelta = int(this.rs485Event1)
                llsSet.request = 1
                if (not this.rs485ReqData1):
                    llsSet.request = 0
                llsSet.address = int(this.rs485Adress1)
                llsSet.startBit = int(this.rs485StartBit1)
                llsSet.length = int(this.rs485Length1)
                llsSet.average = int(this.rs485Average1)
                retSensSet.rs485Set[0] = llsSet
            elif box.text() == listItemsTtu[10]: #RS4852 ok
                llsSet = LlsTypeSetting()
                llsSet.type = TYPE_LLS.OMNICOMM_RES
                if (this.rs485TypeSens2 == 'RFID'):
                    llsSet.type = TYPE_LLS.SOVA_RES
                llsSet.evtDelta = int(this.rs485Event2)
                llsSet.request = 1
                if (not this.rs485ReqData2):
                    llsSet.request = 0
                llsSet.address = int(this.rs485Adress2)
                llsSet.startBit = int(this.rs485StartBit2)
                llsSet.length = int(this.rs485Length2)
                llsSet.average = int(this.rs485Average2)
                retSensSet.rs485Set[1] = llsSet
            elif box.text() == listItemsTtu[11]: #RS4853 ok
                llsSet = LlsTypeSetting()
                llsSet.type = TYPE_LLS.OMNICOMM_RES
                if (this.rs485TypeSens3 == 'RFID'):
                    llsSet.type = TYPE_LLS.SOVA_RES
                llsSet.evtDelta = int(this.rs485Event3)
                llsSet.request = 1
                if (not this.rs485ReqData3):
                    llsSet.request = 0
                llsSet.address = int(this.rs485Adress3)
                llsSet.startBit = int(this.rs485StartBit3)
                llsSet.length = int(this.rs485Length3)
                llsSet.average = int(this.rs485Average3)
                retSensSet.rs485Set[2] = llsSet
            elif box.text() == listItemsTtu[12]: #RS4854 ok
                llsSet = LlsTypeSetting()
                llsSet.type = TYPE_LLS.OMNICOMM_RES
                if (this.rs485TypeSens4 == 'RFID'):
                    llsSet.type = TYPE_LLS.SOVA_RES
                llsSet.evtDelta = int(this.rs485Event4)
                llsSet.request = 1
                if (not this.rs485ReqData4):
                    llsSet.request = 0
                llsSet.address = int(this.rs485Adress4)
                llsSet.startBit = int(this.rs485StartBit4)
                llsSet.length = int(this.rs485Length4)
                llsSet.average = int(this.rs485Average4)
                retSensSet.rs485Set[3] = llsSet
            elif box.text() == listItemsTtu[13]: #RS4855 ok
                llsSet = LlsTypeSetting()
                llsSet.type = TYPE_LLS.OMNICOMM_RES
                if (this.rs485TypeSens5 == 'RFID'):
                    llsSet.type = TYPE_LLS.SOVA_RES
                llsSet.evtDelta = int(this.rs485Event5)
                llsSet.request = 1
                if (not this.rs485ReqData5):
                    llsSet.request = 0
                llsSet.address = int(this.rs485Adress5)
                llsSet.startBit = int(this.rs485StartBit5)
                llsSet.length = int(this.rs485Length5)
                llsSet.average = int(this.rs485Average5)
                retSensSet.rs485Set[4] = llsSet
            elif box.text() == listItemsTtu[14]: #RS4856 ok
                llsSet = LlsTypeSetting()
                llsSet.type = TYPE_LLS.OMNICOMM_RES
                if (this.rs485TypeSens6 == 'RFID'):
                    llsSet.type = TYPE_LLS.SOVA_RES
                llsSet.evtDelta = int(this.rs485Event6)
                llsSet.request = 1
                if (not this.rs485ReqData6):
                    llsSet.request = 0
                llsSet.address = int(this.rs485Adress6)
                llsSet.startBit = int(this.rs485StartBit6)
                llsSet.length = int(this.rs485Length6)
                llsSet.average = int(this.rs485Average6)
                retSensSet.rs485Set[5] = llsSet
            elif box.text() == listItemsTtu[15]: #RS4857 ok
                llsSet = LlsTypeSetting()
                llsSet.type = TYPE_LLS.OMNICOMM_RES
                if (this.rs485TypeSens7 == 'RFID'):
                    llsSet.type = TYPE_LLS.SOVA_RES
                llsSet.evtDelta = int(this.rs485Event7)
                llsSet.request = 1
                if (not this.rs485ReqData7):
                    llsSet.request = 0
                llsSet.address = int(this.rs485Adress7)
                llsSet.startBit = int(this.rs485StartBit7)
                llsSet.length = int(this.rs485Length7)
                llsSet.average = int(this.rs485Average7)
                retSensSet.rs485Set[6] = llsSet
            else:
                raise Exception("GetSensorsTtu. Unknown sensors key:" + box.text())
        return retSensSet

    def GetCmdSensorSettingTtu(self, name, sensSet, listItems):
        if name == listItems[0]:  # AIN1
            p = 0
            ret = "AIN1" + "=" + str(sensSet.ainSet[p].AinPorogUp) + ":" + str(sensSet.ainSet[p].AinPorogDown) + ","
        elif name == listItems[1]:  # AIN2
            p = 1
            ret = "AIN2" + "=" + str(sensSet.ainSet[p].AinPorogUp) + ":" + str(sensSet.ainSet[p].AinPorogDown) + ","
        elif name == listItems[2]: # CAN1
            p = 0
            ret = "CAN1" + "=" + str(sensSet.canSet[p].canUse) + ":" + format(sensSet.canSet[p].pgn, "08X") + ":" + format(sensSet.canSet[p].mask, "08X") + \
                ":" + str(sensSet.canSet[p].startBit) + ":" + str(sensSet.canSet[p].lenBit) + ":" + str(sensSet.canSet[p].timeout) + ","
        elif name == listItems[3]: # CAN2
            p = 1
            ret = "CAN2" + "=" + str(sensSet.canSet[p].canUse) + ":" + format(sensSet.canSet[p].pgn, "08X") + ":" + format(sensSet.canSet[p].mask, "08X") + \
                ":" + str(sensSet.canSet[p].startBit) + ":" + str(sensSet.canSet[p].lenBit) + ":" + str(sensSet.canSet[p].timeout) + ","
        elif name == listItems[4]: # CAN3
            p = 2
            ret = "CAN3" + "=" + str(sensSet.canSet[p].canUse) + ":" + format(sensSet.canSet[p].pgn, "08X") + ":" + format(sensSet.canSet[p].mask, "08X") + \
                ":" + str(sensSet.canSet[p].startBit) + ":" + str(sensSet.canSet[p].lenBit) + ":" + str(sensSet.canSet[p].timeout) + ","
        elif name == listItems[5]:  # COUNT1
            p = 0
            ret = "COUNT1" + "=" +  str(sensSet.cntSet[p].modeCnt) + ":" + str(sensSet.cntSet[p].event1) + ":" + str(sensSet.cntSet[p].event2) + \
                ":" + format(sensSet.cntSet[p].source, "02X") + ","
        elif name == listItems[6]:  # COUNT2
            p = 1
            ret = "COUNT2" + "=" +  str(sensSet.cntSet[p].modeCnt) + ":" + str(sensSet.cntSet[p].event1) + ":" + str(sensSet.cntSet[p].event2) + \
                ":" + format(sensSet.cntSet[p].source, "02X") + ","
        elif name == listItems[7]:  # COUNT3
            p = 2
            ret = "COUNT3" + "=" +  str(sensSet.cntSet[p].modeCnt) + ":" + str(sensSet.cntSet[p].event1) + ":" + str(sensSet.cntSet[p].event2) + \
                ":" + format(sensSet.cntSet[p].source, "02X") + ","
        elif name == listItems[8]:  # DIN
            p = 0
            ret = "DIN" + "=" + str(sensSet.dinSet.evtMask) + ":" + str(sensSet.dinSet.bounce[0]) + ":" + str(sensSet.dinSet.bounce[1]) + \
                  ":" + str(sensSet.dinSet.bounce[2]) + ":" + str(sensSet.dinSet.bounce[3]) + ","
        elif name == listItems[9]: # RS485_1
            p = 0 # RS485x=TYPE:ADDR:START:LEN:AVR:EVT:REQUEST
            ret = "RS4851" + "=" + str(sensSet.rs485Set[p].type) + ":" + str(sensSet.rs485Set[p].address) + ":" + str(sensSet.rs485Set[p].startBit) + \
                ":" + str(sensSet.rs485Set[p].length) + ":" + str(sensSet.rs485Set[p].average) + ":" + str(sensSet.rs485Set[p].evtDelta) + \
                  ":" + str(sensSet.rs485Set[p].request) + ","
        elif name == listItems[10]: # RS485_2
            p = 1 # RS485x=TYPE:ADDR:START:LEN:AVR:EVT:REQUEST
            ret = "RS4852" + "=" + str(sensSet.rs485Set[p].type) + ":" + str(sensSet.rs485Set[p].address) + ":" + str(sensSet.rs485Set[p].startBit) + \
                ":" + str(sensSet.rs485Set[p].length) + ":" + str(sensSet.rs485Set[p].average) + ":" + str(sensSet.rs485Set[p].evtDelta) + \
                  ":" + str(sensSet.rs485Set[p].request) + ","
        elif name == listItems[11]: # RS485_3
            p = 2 # RS485x=TYPE:ADDR:START:LEN:AVR:EVT:REQUEST
            ret = "RS4853" + "=" + str(sensSet.rs485Set[p].type) + ":" + str(sensSet.rs485Set[p].address) + ":" + str(sensSet.rs485Set[p].startBit) + \
                ":" + str(sensSet.rs485Set[p].length) + ":" + str(sensSet.rs485Set[p].average) + ":" + str(sensSet.rs485Set[p].evtDelta) + \
                  ":" + str(sensSet.rs485Set[p].request) + ","
        elif name == listItems[12]: # RS485_4
            p = 3 # RS485x=TYPE:ADDR:START:LEN:AVR:EVT:REQUEST
            ret = "RS4854" + "=" + str(sensSet.rs485Set[p].type) + ":" + str(sensSet.rs485Set[p].address) + ":" + str(sensSet.rs485Set[p].startBit) + \
                ":" + str(sensSet.rs485Set[p].length) + ":" + str(sensSet.rs485Set[p].average) + ":" + str(sensSet.rs485Set[p].evtDelta) + \
                  ":" + str(sensSet.rs485Set[p].request) + ","
        elif name == listItems[13]: # RS485_5
            p = 4 # RS485x=TYPE:ADDR:START:LEN:AVR:EVT:REQUEST
            ret = "RS4855" + "=" + str(sensSet.rs485Set[p].type) + ":" + str(sensSet.rs485Set[p].address) + ":" + str(sensSet.rs485Set[p].startBit) + \
                ":" + str(sensSet.rs485Set[p].length) + ":" + str(sensSet.rs485Set[p].average) + ":" + str(sensSet.rs485Set[p].evtDelta) + \
                  ":" + str(sensSet.rs485Set[p].request) + ","
        elif name == listItems[14]: # RS485_6
            p = 5 # RS485x=TYPE:ADDR:START:LEN:AVR:EVT:REQUEST
            ret = "RS4856" + "=" + str(sensSet.rs485Set[p].type) + ":" + str(sensSet.rs485Set[p].address) + ":" + str(sensSet.rs485Set[p].startBit) + \
                ":" + str(sensSet.rs485Set[p].length) + ":" + str(sensSet.rs485Set[p].average) + ":" + str(sensSet.rs485Set[p].evtDelta) + \
                  ":" + str(sensSet.rs485Set[p].request) + ","
        elif name == listItems[15]: # RS485_7
            p = 6 # RS485x=TYPE:ADDR:START:LEN:AVR:EVT:REQUEST
            ret = "RS4857" + "=" + str(sensSet.rs485Set[p].type) + ":" + str(sensSet.rs485Set[p].address) + ":" + str(sensSet.rs485Set[p].startBit) + \
                ":" + str(sensSet.rs485Set[p].length) + ":" + str(sensSet.rs485Set[p].average) + ":" + str(sensSet.rs485Set[p].evtDelta) + \
                  ":" + str(sensSet.rs485Set[p].request) + ","
        else:
            raise Exception("GetCmdSensorSettingTtu. Unknown sensors key:" + name)
        return ret

    def GetCmdSensorSettingTtl(self, name, sensSet, listItems):
        if name == listItems[0]:  # AIN1
            ret = "AIN1" + "=" + str(sensSet.ainSet[0].AinPorogUp) + ":" + str(sensSet.ainSet[0].AinPorogDown) + ","
        elif name == listItems[1]:  # AIN2
            ret = "AIN2" + "=" + str(sensSet.ainSet[1].AinPorogUp) + ":" + str(sensSet.ainSet[1].AinPorogDown) + ","
        elif name == listItems[2]:  # COUNT1
            ret = "COUNT1" + "=" +  str(sensSet.cntSet[0].modeCnt) + ":" + str(sensSet.cntSet[0].event1) + ":" + str(sensSet.cntSet[0].event2) + ","
        elif name == listItems[3]:  # COUNT2
            ret = "COUNT2" + "=" +  str(sensSet.cntSet[1].modeCnt) + ":" + str(sensSet.cntSet[1].event1) + ":" + str(sensSet.cntSet[1].event2) + ","
        elif name == listItems[4]:  # COUNT3
            ret = "COUNT3" + "=" +  str(sensSet.cntSet[2].modeCnt) + ":" + str(sensSet.cntSet[2].event1) + ":" + str(sensSet.cntSet[2].event2) + ","
        elif name == listItems[5]:  # DIN
            ret = "DIN" + "=" + str(sensSet.dinSet.evtMask) + ":" + str(sensSet.dinSet.bounce[0]) + ":" + str(sensSet.dinSet.bounce[1]) + \
                ":" + str(sensSet.dinSet.bounce[2]) + ":" + str(sensSet.dinSet.bounce[3]) + ","
        elif name == listItems[6]:  # FUEL1
            ret = "FUEL1" + "=" + str(sensSet.fuelSet[0].addressFuel) + ":" + str(sensSet.fuelSet[0].startBitFuel) + ":" + \
                str(sensSet.fuelSet[0].lengthFuel) + ":" + str(sensSet.fuelSet[0].averageFuel) + ","
        elif name == listItems[7]:  # FUEL2
            ret = "FUEL2" + "=" + str(sensSet.fuelSet[1].addressFuel) + ":" + str(sensSet.fuelSet[1].startBitFuel) + ":" + \
                  str(sensSet.fuelSet[1].lengthFuel) + ":" + str(sensSet.fuelSet[1].averageFuel) + ","
        elif name == listItems[8]:  # FUEL3
            ret = "FUEL3" + "=" + str(sensSet.fuelSet[2].addressFuel) + ":" + str(sensSet.fuelSet[2].startBitFuel) + ":" + \
                  str(sensSet.fuelSet[2].lengthFuel) + ":" + str(sensSet.fuelSet[2].averageFuel) + ","
        elif name == listItems[9]:  # FUEL4
            ret = "FUEL4" + "=" + str(sensSet.fuelSet[3].addressFuel) + ":" + str(sensSet.fuelSet[3].startBitFuel) + ":" + \
                  str(sensSet.fuelSet[3].lengthFuel) + ":" + str(sensSet.fuelSet[3].averageFuel) + ","
        elif name == listItems[10]:  # FUEL5
            ret = "FUEL5" + "=" + str(sensSet.fuelSet[4].addressFuel) + ":" + str(sensSet.fuelSet[4].startBitFuel) + ":" + \
                  str(sensSet.fuelSet[4].lengthFuel) + ":" + str(sensSet.fuelSet[4].averageFuel) + ","
        elif name == listItems[11]: # RESERVED1
            ret = ''
        elif name == listItems[12]: # RESERVED2
            ret = ''
        elif name == listItems[13]: # RESERVED3
            ret = ''
        elif name == listItems[14]:  # RFID1
            ret = "RFID1" + "=" + str(sensSet.rfidSet[0].addressRfid) + ":" + str(sensSet.rfidSet[0].startBitRfid) + \
                ":" + str(sensSet.rfidSet[0].lengthBitRfid) + ","
        elif name == listItems[15]:  # RFID2
            ret = "RFID2" + "=" + str(sensSet.rfidSet[1].addressRfid) + ":" + str(sensSet.rfidSet[1].startBitRfid) + \
                  ":" + str(sensSet.rfidSet[1].lengthBitRfid) + ","
        else:
            raise Exception("GetCmdSensorSettingTtl. Unknown sensors key:" + name)
        return ret

    def GetSensorsEnableTtl(self):
        enable = 0
        for i,lt in enumerate(self.orderItemsTtl):
            if self.orderItemsTtl[lt]:
                enable |= int(1 << i)
        return enable

    def GetSensorsEnableTtu(self):
        enable = 0
        for i,lt in enumerate(self.orderItemsTtu):
            if self.orderItemsTtu[lt]:
                enable |= int(1 << i)
        return enable

    def WriteSensorsTtu(self, this, baseCmd):
        for box in this.checkedListBoxSensors:
            self.orderItemsTtu[box.text()] = box.isChecked()

        keys = []
        for ls in self.orderItemsTtu:
            keys.append(ls)

        needTx = []
        lstItems = this.items
        sensSet = self.GetSensorsTtu(this)
        cmd = baseCmd
        cmd += self.cmdSensorsBaseTtu[0] + "=" + str(sensSet.rs485Speed) + ","
        cmd += self.cmdSensorsBaseTtu[1] + "1=" + str(sensSet.canSpeed1) + ","
        cmd += self.cmdSensorsBaseTtu[2] + "2=" + str(sensSet.canSpeed2) + ","
        cmd += self.cmdSensorsBaseTtu[3] + "=" + str(sensSet.SensitivityAcc) + ","
        needTx.append(cmd)

        cmd = baseCmd
        for i in range(0, 5, 1):
            key = keys[i]
            cmd += self.GetCmdSensorSettingTtu(key, sensSet, lstItems)
        needTx.append(cmd)

        cmd = baseCmd
        for i in range(5, 10, 1):
            key = keys[i]
            cmd += self.GetCmdSensorSettingTtu(key, sensSet, lstItems)
        needTx.append(cmd)

        cmd = baseCmd
        for i in range(10, 13, 1):
            key = keys[i]
            cmd += self.GetCmdSensorSettingTtu(key, sensSet, lstItems)
        needTx.append(cmd)

        cmd = baseCmd
        for i in range(13, 16, 1):
            key = keys[i]
            cmd += self.GetCmdSensorSettingTtu(key, sensSet, lstItems)
        needTx.append(cmd)

        # SMOUT1 = SENSOR:TopLVL: LowLVL:CMDin: CMDout
        cmd = baseCmd + "SMOUT1=" + str(self.smOutVal.baseSensCode) + ":" + str(self.smOutVal.porogUp) + ":" + str(self.smOutVal.porogDown) + \
            ":" + str(self.smOutVal.cmdIn) + ":" + str(self.smOutVal.cmdOut) +","
        needTx.append(cmd)

        return needTx, 207

    def WriteSensorsTtl(self, this, baseCmd):
        for box in this.checkedListBoxSensors:
            self.orderItemsTtl[box.text()] = box.isChecked()
        keys = []
        for ls in self.orderItemsTtl:
            keys.append(ls)

        needTx = []
        lstItems = this.items
        sensSet = self.GetSensorsTtl(this)
        cmd = baseCmd
        cmd += self.cmdSensorsBaseTtl[0] + "=" + str(sensSet.rs485Speed) + ","
        cmd += self.cmdSensorsBaseTtl[1] + "=" + str(sensSet.SensitivityAcc) + ","
        # self.cmdSensorsBaseTtl[2] is not using
        cmd += self.cmdSensorsBaseTtl[3] + "=" + format(sensSet.enable, "08X") + ","
        needTx.append(cmd)

        cmd = baseCmd
        for i in range(0, 5, 1):
            key = keys[i]
            cmd += self.GetCmdSensorSettingTtl(key, sensSet, lstItems)
        needTx.append(cmd)

        cmd = baseCmd
        for i in range(5, 10, 1):
            key = keys[i]
            cmd += self.GetCmdSensorSettingTtl(key, sensSet, lstItems)
        needTx.append(cmd)

        cmd = baseCmd
        for i in range(10, 16, 1):
            key = keys[i]
            cmd += self.GetCmdSensorSettingTtl(key, sensSet, lstItems)
        needTx.append(cmd)

        return needTx, 107

    def WriteServiceTtl(self, tPhone, tregim, baseCmd, add, simUse):
        needTx = []
        servSet = self.GetService(tPhone, tregim, simUse)
        cmd = baseCmd + self.cmdService[0] + add + "=" + str(servSet.ConnectRegim) + ","
        cmd += self.cmdService[1] + "=" + servSet.Phone1 + ","
        cmd += self.cmdService[2] + "=" + servSet.Phone2 + ","
        cmd += self.cmdService[3] + "=" + servSet.Phone3 + ","
        needTx.append(cmd)
        return needTx, 105

    def WriteServiceTtu(self, tPhone, tregim, baseCmd):
        needTx = []
        servSet = self.GetService(tPhone, tregim, -1)
        cmd = baseCmd + self.cmdService[0] + "=" + str(servSet.ConnectRegim) + ","
        cmd += self.cmdService[1] + "=" + servSet.Phone1 + ","
        cmd += self.cmdService[2] + "=" + servSet.Phone2 + ","
        cmd += self.cmdService[3] + "=" + servSet.Phone3 + ","
        needTx.append(cmd)
        return needTx, 205

    def WriteTransportTtu(self, tblServSett, tblGprs, baseCmd):
        needTx = []
        transSet = self.GetTransport(tblServSett, tblGprs)
        cmd = baseCmd
        cmd += self.cmdTransportVal[0] + "=" + transSet.ApnServer + ","
        cmd += self.cmdTransportVal[1] + "=" + transSet.ApnLogin + ","
        cmd += self.cmdTransportVal[2] + "=" + transSet.ApnPassword + ","
        cmd += self.cmdTransportVal[3] + "=" + transSet.DnsServer + ","
        needTx.append(cmd)

        cmd = baseCmd
        cmd += self.cmdTransportVal[4] + "=" + transSet.ServerAddr + ","
        cmd += self.cmdTransportVal[5] + "=" + str(transSet.ServerPort) + ","
        cmd += self.cmdTransportVal[6] + "=" + transSet.ServerLogin + ","
        cmd += self.cmdTransportVal[7] + "=" + transSet.ServerPassword + ","
        needTx.append(cmd)
        return needTx, 203

    def WriteTransportTtl(self, tblServSett, tblGprs, baseCmd, add, simUse):
        needTx = []
        transSet = self.GetTransport(tblServSett, tblGprs, simUse)
        cmd = baseCmd
        cmd += self.cmdTransportVal[0] + add + "=" + transSet.ApnServer + ","
        cmd += self.cmdTransportVal[1] + add + "=" + transSet.ApnLogin + ","
        cmd += self.cmdTransportVal[2] + add + "=" + transSet.ApnPassword + ","
        cmd += self.cmdTransportVal[3] + add + "=" + transSet.DnsServer + ","
        needTx.append(cmd)

        cmd = baseCmd
        cmd += self.cmdTransportVal[4] + "=" + transSet.ServerAddr + ","
        cmd += self.cmdTransportVal[5] + "=" + str(transSet.ServerPort) + ","
        cmd += self.cmdTransportVal[6] + "=" + transSet.ServerLogin + ","
        cmd += self.cmdTransportVal[7] + "=" + transSet.ServerPassword + ","
        needTx.append(cmd)
        return needTx, 103

    def coderCommandRcsttEventTtu(self, table, baseCmd):
        needTx = []
        evSet = self.GetEvent(table)
        cmd = baseCmd
        cmd += self.cmdEventVal[0] + "=" + str(evSet.MinSpeed) + ","
        cmd += self.cmdEventVal[1] + "=" + str(evSet.Timer1) + ","
        cmd += self.cmdEventVal[2] + "=" + str(evSet.Timer2) + ","
        cmd += self.cmdEventVal[3] + "=" + str(evSet.CourseBend) + ","
        cmd += self.cmdEventVal[4] + "=" + str(evSet.Distance1) + ","
        cmd += self.cmdEventVal[5] + "=" + str(evSet.Distance2) + ","
        cmd += self.cmdEventVal[6] + "=" + str(evSet.SpeedChange) + ","
        needTx.append(cmd)

        cmd = baseCmd
        cmd += self.cmdEventVal[7] + "=" + str(evSet.EventMask[EventSetting.INDEX_MIN_SPEED]) + ","
        cmd += self.cmdEventVal[8] + "=" + str(evSet.EventMask[EventSetting.INDEX_TIMER1]) + ","
        cmd += self.cmdEventVal[9] + "=" + str(evSet.EventMask[EventSetting.INDEX_TIMER2]) + ","
        cmd += self.cmdEventVal[10] + "=" + str(evSet.EventMask[EventSetting.INDEX_COURSE]) + ","
        cmd += self.cmdEventVal[11] + "=" + str(evSet.EventMask[EventSetting.INDEX_DISTANCE1]) + ","
        cmd += self.cmdEventVal[12] + "=" + str(evSet.EventMask[EventSetting.INDEX_DISTANCE2]) + ","
        cmd += self.cmdEventVal[13] + "=" + str(evSet.EventMask[EventSetting.INDEX_ACCELERATION]) + ","
        cmd += self.cmdEventVal[14] + "=" + str(evSet.EventMask[EventSetting.INDEX_POWER_ON]) + ","
        cmd += self.cmdEventVal[15] + "=" + str(evSet.EventMask[EventSetting.INDEX_POWER_OFF]) + ","
        needTx.append(cmd)

        cmd = baseCmd
        cmd += self.cmdEventVal[16] + "=" + str(evSet.EventMask[EventSetting.INDEX_GSM_FIND]) + ","
        cmd += self.cmdEventVal[17] + "=" + str(evSet.EventMask[EventSetting.INDEX_GSM_LOST]) + ","
        cmd += self.cmdEventVal[18] + "=" + str(evSet.EventMask[EventSetting.INDEX_LOG_FULL]) + ","
        cmd += self.cmdEventVal[19] + "=" + str(evSet.EventMask[EventSetting.INDEX_RING_PHONE1]) + ","
        cmd += self.cmdEventVal[20] + "=" + str(evSet.EventMask[EventSetting.INDEX_RING_PHONE2]) + ","
        cmd += self.cmdEventVal[21] + "=" + str(evSet.EventMask[EventSetting.INDEX_RING_PHONE3]) + ","
        cmd += self.cmdEventVal[22] + "=" + str(evSet.EventMask[EventSetting.INDEX_SENSORS]) + ","
        cmd += self.cmdEventVal[23] + "=" + str(evSet.EventMask[EventSetting.INDEX_ON]) + ","
        cmd += self.cmdEventVal[24] + "=" + str(evSet.EventMask[EventSetting.INDEX_REBOOT]) + ","
        needTx.append(cmd)
        return needTx, 201

    def coderCommandRcsttEventTtl(self, table, baseCmd, add, simUse):
        needTx = []
        evSet = self.GetEvent(table, simUse)
        cmd = baseCmd
        cmd += self.cmdEventVal[0] + add + "=" + str(evSet.MinSpeed) + ","
        cmd += self.cmdEventVal[1] + add + "=" + str(evSet.Timer1) + ","
        cmd += self.cmdEventVal[2] + add + "=" + str(evSet.Timer2) + ","
        cmd += self.cmdEventVal[3] + add + "=" + str(evSet.CourseBend) + ","
        cmd += self.cmdEventVal[4] + add + "=" + str(evSet.Distance1) + ","
        cmd += self.cmdEventVal[5] + add + "=" + str(evSet.Distance2) + ","
        cmd += self.cmdEventVal[6] + add + "=" + str(evSet.SpeedChange) + ","
        needTx.append(cmd)

        cmd = baseCmd
        cmd += self.cmdEventVal[7] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_MIN_SPEED]) + ","
        cmd += self.cmdEventVal[8] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_TIMER1]) + ","
        cmd += self.cmdEventVal[9] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_TIMER2]) + ","
        cmd += self.cmdEventVal[10] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_COURSE]) + ","
        cmd += self.cmdEventVal[11] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_DISTANCE1]) + ","
        cmd += self.cmdEventVal[12] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_DISTANCE2]) + ","
        cmd += self.cmdEventVal[13] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_ACCELERATION]) + ","
        cmd += self.cmdEventVal[14] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_POWER_ON]) + ","
        cmd += self.cmdEventVal[15] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_POWER_OFF]) + ","
        needTx.append(cmd)

        cmd = baseCmd
        cmd += self.cmdEventVal[16] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_GSM_FIND]) + ","
        cmd += self.cmdEventVal[17] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_GSM_LOST]) + ","
        cmd += self.cmdEventVal[18] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_LOG_FULL]) + ","
        cmd += self.cmdEventVal[19] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_RING_PHONE1]) + ","
        cmd += self.cmdEventVal[20] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_RING_PHONE2]) + ","
        cmd += self.cmdEventVal[21] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_RING_PHONE3])+ ","
        cmd += self.cmdEventVal[22] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_SENSORS]) + ","
        cmd += self.cmdEventVal[23] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_ON]) + ","
        cmd += self.cmdEventVal[24] + add + "=" + str(evSet.EventMask[EventSetting.INDEX_REBOOT]) + ","
        needTx.append(cmd)
        return needTx, 101

    def coderCommandA1Request(self, idlog):
        simleq = ecs.SimpleQ()
        simleq.shortId = idlog
        GenerateCommandA1.cmdMsgCnt += 1
        simleq.messageID = GenerateCommandA1.cmdMsgCnt
        txCommand = self.EncodeEventConfigQuery(simleq)
        commandID = int(ecs.CommandDescriptor.EventConfigQuery)
        return (txCommand, commandID)

    def coderCommandA1GprsRequest(self, idlog):
        simleq = ecs.SimpleQ()
        simleq.shortId = idlog
        GenerateCommandA1.cmdMsgCnt += 1
        simleq.messageID = GenerateCommandA1.cmdMsgCnt
        txCommand = self.EncodeGprsBaseConfigQuery(simleq)
        commandID = int(ecs.CommandDescriptor.GprsBaseConfigQuery)
        return (txCommand, commandID)

    def coderCommandA1ServerRequest(self, idlog):
        simleq = ecs.SimpleQ()
        simleq.shortId = idlog
        GenerateCommandA1.cmdMsgCnt += 1
        simleq.messageID = GenerateCommandA1.cmdMsgCnt
        txCommand = self.EncodeGprsEmailConfigQuery(simleq)
        commandID = int(ecs.CommandDescriptor.GprsEmailConfigQuery)
        return (txCommand, commandID)

    def coderCommandA1GPSRequest(self, idlog):
        simleq = ecs.SimpleQ()
        simleq.shortId = idlog
        GenerateCommandA1.cmdMsgCnt += 1
        simleq.messageID = GenerateCommandA1.cmdMsgCnt
        query = self.CreateDataGpsQuery(idlog, simleq.messageID)
        txCommand = self.EncodeDataGpsQuery(query)
        commandID = int(ecs.CommandDescriptor.DataGpsQuery)
        return (txCommand, commandID)

    def coderCommandA1GPSWriter(self, idlog):
        simleq = ecs.SimpleQ()
        simleq.shortId = idlog
        GenerateCommandA1.cmdMsgCnt += 1
        simleq.messageID = GenerateCommandA1.cmdMsgCnt
        query = self.CreateDataGpsQuery(idlog, simleq.messageID)
        return query

    def CreateDataGpsQuery(self, idtt, msgcnt):
        query = DataGpsQuery()
        query.ShortID = idtt
        query.MessageID = msgcnt
        query.CheckMask = (1 << 0)
        query.CommandID = int(ecs.CommandDescriptor.DataGpsQuery)
        query.WhatSend = ((1 << 10) - 1)
        query.MaxSmsNumber = 1
        query.StartTime = 0
        query.EndTime = 0
        query.LastRecords = 101
        return query

    def coderCommandA1PhonesRequest(self, idlog):
        simleq = ecs.SimpleQ()
        simleq.shortId = idlog
        GenerateCommandA1.cmdMsgCnt += 1
        simleq.messageID = GenerateCommandA1.cmdMsgCnt
        txCommand = self.EncodePhoneNumberConfigQuery(simleq)
        commandID = int(ecs.CommandDescriptor.PhoneConfigQuery)
        return (txCommand, commandID)

    def coderCommandA1TextCmdRequest(self, idlog, textmsg):
        simleq = ecs.SimpleQ()
        simleq.shortId = idlog
        GenerateCommandA1.cmdMsgCnt += 1
        simleq.messageID = GenerateCommandA1.cmdMsgCnt
        txCommand = textmsg
        commandID = int(ecs.CommandDescriptor.TextCommand) # Вроде сообщения водителю хотели сделать
        return (txCommand, commandID)

    def CalculateLevel1CRC(self, source, startIndex, length):
        result = 0
        k = startIndex
        finishIndex = startIndex + length
        for i in range(finishIndex + 1):
            if (k != GenerateCommandA1.LEVEL1_CRC_POSITION):
                result += source[k]
                result &= 0x00000000000000ff
            k += 1
        result >>= 2
        result &= 0xff
        return result

    def ConverPacket6bitTo8bit(self, packBlock):
        dBlock = ecs.DataBlock8bit()
        i = 0
        f = 0
        while i < len(dBlock.data):
            dBlock.data[i] = int((packBlock.data[f] << 2) + (packBlock.data[f + 3] & 0x03))
            dBlock.data[i + 1] = int((packBlock.data[f + 1] << 2) + ((packBlock.data[f + 3] >> 2) & 0x03))
            dBlock.data[i + 2] = int((packBlock.data[f + 2] << 2) + ((packBlock.data[f + 3] >> 4) & 0x03))
            i += 3
            f += 4
        return dBlock

    def BytesToUShort(self, source, startIndex):
        return abs(self.ToInt16(source, startIndex))

    def ToInt16(self, source, startIndex):
        if (source == None):
            raise ("source. Не передан массив байт source")
        if (len(source) == 0):
            raise ("source. Передан пустой массив байт source")
        if ((startIndex < 0) or (startIndex >= len(source))):
            raise ("startIndex. startIndex должен быть больше или равен нулю и меньше длины массива source")

        H = 0
        L = 0

        if (startIndex + 1 < len(source)):
            H = source[startIndex]
            L = source[startIndex + 1]
        else:
            L = source[startIndex]
        return ((H << 8) | L)

    def ToSByte(self, value):
        return int(value)

    def ConvertAsciiWin1251ToChar(self, code):
        if (code < 0xC0):
            if code == 0xA8:
                result = 'Ё'
            elif code == 0xB8:
                result = 'ё'
            else:
                result = chr(code)
        else:
            result = chr(code + 0x410 - 0xc0)
        return result

    def BytesToString(self, source, startIndex, length):
        result = []
        finishIndex = startIndex + length
        for i in range(startIndex, finishIndex, 1):
            ch = self.ConvertAsciiWin1251ToChar(source[i])
            if ord(ch) == 0:
                break
            result.append(ch)
        myString = ''.join(result)
        return myString

    def GetGprsEmailConfig(self, command):
        result = GprsEmailConfig()
        result.SmtpServer = self.BytesToString(command, 0, 25)
        result.SmtpLogin = self.BytesToString(command, 25, 10)
        result.SmtpPassword = self.BytesToString(command, 35, 10)
        result.Pop3Server = self.BytesToString(command, 45, 25)
        result.Pop3Login = self.BytesToString(command, 70, 10)
        result.Pop3Password = self.BytesToString(command, 80, 10)
        return result

    def GetPhoneNumberSymbol(self, source, index):
        if ((index & 0x01) != 0):
            return int(source[index >> 1] & 0x0F)
        else:
            return int(source[index >> 1] >> 4)

    def BytesToTelNumber(self, source, startIndex, length):
        finishIndex = startIndex + length
        result = ""
        for i in range(startIndex << 1, finishIndex << 1, 1):
            code = self.GetPhoneNumberSymbol(source, i)
            if (code < 0x0d):
                if code == 10:
                    result += "+"
                elif code == 11:
                    result += "*"
                elif code == 12:
                    result += "#"
                else:
                    result += str(code)
            else:
                break
        return result

    def GetPhoneNumberConfig(self, command):
        result = PhoneNumberConfig()
        result.NumberAccept1 = self.BytesToTelNumber(command, 0, 11)
        result.NumberAccept2 = self.BytesToTelNumber(command, 11, 11)
        result.NumberAccept3 = self.BytesToTelNumber(command, 22, 11)
        result.NumberDspt = self.BytesToTelNumber(command, 33, 11)
        result.Name1 = self.BytesToString(command, 45, 8)
        result.Name2 = self.BytesToString(command, 53, 8)
        result.Name3 = self.BytesToString(command, 61, 8)
        result.NumberSOS = self.BytesToTelNumber(command, 69, 11)
        return result

    def GetGprsBaseConfig(self, command):
        result = GprsBaseConfig()
        result.Mode = self.BytesToUShort(command, 0)
        result.ApnServer = self.BytesToString(command, 2, 25)
        result.ApnLogin = self.BytesToString(command, 27, 10)
        result.ApnPassword = self.BytesToString(command, 37, 10)
        result.DnsServer = self.BytesToString(command, 47, 16)
        result.DialNumber = self.BytesToString(command, 63, 11)
        result.GprsLogin = self.BytesToString(command, 74, 10)
        result.GprsPassword = self.BytesToString(command, 84, 10)
        return result

    def GetEventConfig(self, command):
        result = ecs.EventConfig()
        result.speedChange = self.ToSByte(command[0])
        result.courseBend = self.BytesToUShort(command, 1)
        result.distance1 = self.BytesToUShort(command, 3)
        result.distance2 = self.BytesToUShort(command, 5)
        for i in range(32):
            result.eventMask[i] = self.BytesToUShort(command, (i << 1) + 7)
        result.minSpeed = self.BytesToUShort(command, 71)
        result.timer1 = self.BytesToUShort(command, 73)
        result.timer2 = self.BytesToUShort(command, 75)
        return result

    def DecodeLevel3Message(self, command):
        if (len(command) != ecs.Const.LEVEL3_PACKET_LENGHT):
            raise (ecs.ErrorText.LENGTH_PACKET + "LEVEL3" + ecs.Const.LEVEL3_PACKET_LENGHT, len(command))
        commandId = command[0]
        messageId = self.BytesToUShort(command, ecs.Const.LEVEL3_MESSAGEID_POSITION)
        dataBlock = [0 for i in range(ecs.Const.LEVEL3_DATA_BLOCK_LENGTH)]
        self.Copy(command, ecs.Const.LEVEL3_DATA_BLOCK_POSITION, dataBlock, 0, ecs.Const.LEVEL3_DATA_BLOCK_LENGTH)
        result = ''
        if commandId == ecs.CommandDescriptor.EventConfigConfirm or commandId == ecs.CommandDescriptor.EventConfigAnswer:
            result = self.GetEventConfig(dataBlock)
        elif commandId == ecs.CommandDescriptor.GprsBaseConfigConfirm or commandId == ecs.CommandDescriptor.GprsBaseConfigAnswer:
            result = self.GetGprsBaseConfig(dataBlock)
        elif commandId == ecs.CommandDescriptor.GprsEmailConfigConfirm or commandId == ecs.CommandDescriptor.GprsEmailConfigAnswer:
            result = self.GetGprsEmailConfig(dataBlock)
        elif commandId == ecs.CommandDescriptor.PhoneConfigConfirm or commandId == ecs.CommandDescriptor.PhoneConfigAnswer:
            result = self.GetPhoneNumberConfig(dataBlock)
        result.CommandID = commandId
        result.MessageID = messageId
        return result

    def Copy(self, source, indxs, dist, indxd, length):
        dist[indxd:indxd+length] = source[indxs:indxs+length]
        return

    def ParserA1Cmd(self, rxPackBlock, curdev):
        rxDataBl8 = self.ConverPacket6bitTo8bit(rxPackBlock)
        result = self.DecodeLevel3Message(rxDataBl8.data)
        result.ShortID = self.GetA1IDTT(rxPackBlock)
        if result.ShortID != curdev:
            return False
        result.source = rxDataBl8.data
        if ecs.CommandDescriptor.EventConfigConfirm == result.CommandID or ecs.CommandDescriptor.EventConfigAnswer == result.CommandID:
            self.DataGridViewSetEvent(result)
        elif ecs.CommandDescriptor.GprsBaseConfigConfirm == result.CommandID or ecs.CommandDescriptor.GprsBaseConfigAnswer == result.CommandID:
            self.DataGridViewSetGprsBase(result)
        elif ecs.CommandDescriptor.GprsEmailConfigConfirm == result.CommandID or ecs.CommandDescriptor.GprsEmailConfigAnswer == result.CommandID:
            self.DataGridViewSetGprsEmail(result)
        elif ecs.CommandDescriptor.PhoneConfigConfirm == result.CommandID or ecs.CommandDescriptor.PhoneConfigAnswer == result.CommandID:
            self.DataGridViewSetPhones(result)
        return True

    def GetA1Email(self, source):
        if (len(source) == ecs.Const.LEVEL0_PACKET_LENGTH):
            ret = source[0:ecs.Const.LEVEL0_EMAIL_LENGTH]
        else:
            raise ("Неправильный размер строки с пакетом А1, для получения Email отправителя")
        return ret

    def GetA1IDTT(self, source):
        ret_idtt = ""
        for i in range(len(source.id_tt)):
            ret_idtt += chr(source.id_tt[i])
        return ret_idtt

    def StringToByteArray(self, source):
        if (source == None):
            raise ("source. Не передана строка source")
        result = [0 for i in range(len(source))]
        for i in range(len(source)- 1):
            result[i] = ord(source[i])
        return result

    def timerThreadRx_Tick(self, curentParse, current_device):
        res = False
        startDataPacket = curentParse.find("%%")
        if (startDataPacket >= ecs.Const.LEVEL0_START_INDICATOR_POSITION):
            curentParse = curentParse[startDataPacket - ecs.Const.LEVEL0_START_INDICATOR_POSITION:len(curentParse)]
            if (len(curentParse) >= ecs.Const.LEVEL0_PACKET_LENGTH):
                rxPacketBytes = self.StringToByteArray(curentParse)
                rxPackBlock = self.ConvertSymbolToPacketBlock(rxPacketBytes)
                if (rxPackBlock.isCrcOk == True):
                    res = self.ParserA1Cmd(rxPackBlock, current_device)
        return res

    def TestCRC6(self, source):
        isOk = False
        tmpCRC = 0
        massPacket = [0 for i in range(ecs.Const.LEVEL1_PACKET_LENGHT)]
        self.Copy(source, ecs.Const.LEVEL0_START_INDICATOR_POSITION + 2, massPacket, 0, ecs.Const.LEVEL1_PACKET_LENGHT)
        lenght = self.SymbolToValue(massPacket[ecs.Const.LEVEL1_LENGTH_POSITION])
        for i in range(ecs.Const.LEVEL1_DATA_BLOCK_LENGHT + 1):
            if (i != ecs.Const.LEVEL1_CRC_POSITION):
                tmpCRC += massPacket[i]
        n = int(tmpCRC / 256)
        tmpCRC -= n * 256
        tmpCRC = int(tmpCRC >> 2)
        if (self.SymbolToValue(massPacket[ecs.Const.LEVEL1_CRC_POSITION]) == tmpCRC):
            isOk = True
        return isOk

    def ConvertSymbolToPacketBlock(self, source):
        packBlock = ecs.PacketBlock()
        if (len(source)) != ecs.Const.LEVEL0_PACKET_LENGTH:
            raise ("Неправильный размер строки с пакетом А1, для конвертации в 6 бит")

        startPosition = ecs.Const.LEVEL0_PACKET_LENGTH - ecs.Const.LEVEL1_PACKET_LENGHT
        packBlock.version[0] = self.SymbolToValue(source[startPosition])
        startPosition += 1
        packBlock.version[1] = self.SymbolToValue(source[startPosition])
        startPosition += 1
        packBlock.lenght = self.SymbolToValue(source[startPosition])
        startPosition += 1
        for i in range(ecs.Const.LEVEL0_IDTT_LENGHT):
            packBlock.id_tt[i] = self.SymbolToValue(source[startPosition])
            startPosition += 1
        packBlock.crc = self.SymbolToValue(source[startPosition])
        startPosition += 1
        for k in range(ecs.Const.LEVEL1_DATA_BLOCK_LENGHT):
            packBlock.data[k] = self.SymbolToValue(source[k + startPosition])
        packBlock.isCrcOk = self.TestCRC6(source)
        return packBlock

    def SymbolToValue(self, symbolCode):
        value = 0
        symbol = chr(symbolCode)
        if ((symbol >= 'A') and (symbol <= 'Z')):
            value = int(symbolCode - GenerateCommandA1.ASCII_CODE_A)
        elif ((symbol >= 'a') and (symbol <= 'z')):
            value = int(symbolCode - ord('a') + 26)
        elif ((symbol >= '0') and (symbol <= '9')):
            value = int(symbolCode - ord('0') + 52)
        elif (symbol == '+'):
            value = 62
        elif (symbol == '-'):
            value = 63
        elif ((symbolCode >= 58) and (symbolCode <= 63)):
            value = int(symbolCode - 58 + GenerateCommandA1.ASCII_CODE_A)
        return value

    def DataGridViewSetGprsBase(self, result):
        self.configGprsBase = result
        return

    def DataGridViewSetGprsEmail(self, result):
        self.configGprsEmail = result
        return

    def DataGridViewSetPhones(self, result):
        self.configPhone = result
        return

    def DataGridViewSetEvent(self, configEvent):
        self.dataGridViewEvent = ecs.dataGridViewEvent()
        self.dataGridViewEvent.Rows[0].Cells["ColumnEventsValue"].Value = configEvent.minSpeed

        self.dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_MIN_SPEED] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_MIN_SPEED] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[0].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_MIN_SPEED] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[1].Cells["ColumnEventsValue"].Value = configEvent.timer1

        self.dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_TIMER1] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_TIMER1] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_TIMER1] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[2].Cells["ColumnEventsValue"].Value = configEvent.timer2

        self.dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_TIMER2] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_TIMER2] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_TIMER2] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[3].Cells["ColumnEventsValue"].Value = configEvent.courseBend

        self.dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_COURSE] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_COURSE] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[3].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_COURSE] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[4].Cells["ColumnEventsValue"].Value = configEvent.distance1

        self.dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_DISTANCE1] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_DISTANCE1] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[4].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_DISTANCE1] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[5].Cells["ColumnEventsValue"].Value = configEvent.distance2

        self.dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_DISTANCE2] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_DISTANCE2] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[5].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_DISTANCE2] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[6].Cells["ColumnEventsValue"].Value = configEvent.speedChange

        self.dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_ACCELERATION] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_ACCELERATION] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[6].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_ACCELERATION] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_POWER_ON] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_POWER_ON] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[7].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_POWER_ON] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_POWER_OFF] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_POWER_OFF] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[8].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_POWER_OFF] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_GSM_FIND] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_GSM_FIND] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[9].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_GSM_FIND] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_GSM_LOST] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_GSM_LOST] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[10].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_GSM_LOST] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_LOG_FULL] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_LOG_FULL] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[11].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_LOG_FULL] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_SENSORS] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_SENSORS] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_SENSORS] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_ON] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_ON] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[13].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_ON] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_REBOOT] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_REBOOT] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[14].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_REBOOT] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_PHONE1] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_PHONE1] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[15].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_PHONE1] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_PHONE2] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_PHONE2] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[16].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_PHONE2] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_PHONE3] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_PHONE3] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[17].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_PHONE3] & configEvent.BIT_SEND_SMS) > 0

        self.dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckWritePoint"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_DISPETCHER] & configEvent.BIT_WRITE_LOG) > 0
        self.dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckSendToServer"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_DISPETCHER] & configEvent.BIT_SEND_TO_SERVER) > 0
        self.dataGridViewEvent.Rows[18].Cells["ColumnEventsCheckSendSMS"].Value = \
            (configEvent.eventMask[configEvent.INDEX_RING_DISPETCHER] & configEvent.BIT_SEND_SMS) > 0
        return

    def setEventTimerPeriod(self, timer1, timer2, sendToServer1, sendToServer2):
        self.dataGridViewEvent = ecs.dataGridViewEvent()
        self.dataGridViewEvent.Rows[1].Cells["ColumnEventsValue"].Value = timer1
        self.dataGridViewEvent.Rows[2].Cells["ColumnEventsValue"].Value = timer2
        self.dataGridViewEvent.Rows[1].Cells["ColumnEventsCheckSendToServer"].Value = sendToServer1
        self.dataGridViewEvent.Rows[2].Cells["ColumnEventsCheckSendToServer"].Value = sendToServer2
        return

    def setEventSensorsToServer(self, eventsens):
        self.dataGridViewEvent = ecs.dataGridViewEvent()
        self.dataGridViewEvent.Rows[12].Cells["ColumnEventsCheckSendToServer"].Value = eventsens
        return

    def setValueSensorsTtu(self, commanda, this):
        listItemsTtu = self.orderItemsTtuSys
        retSensSet = SensorsSetting()
        for box in commanda:
            if box[0] == 'RS485SPEED': # ok
                retSensSet.rs485Speed = int(box[1])
                this.comboRs485.setCurrentText(box[1])
            elif box[0] == 'ACCLVL': # ok
                retSensSet.SensitivityAcc = int(box[1])
                this.spinAcc.setValue(int(box[1]))
            elif box[0] == 'CANSPEED1': # ok
                canspd = hex(int(box[1]))
                if canspd == '0xffffffff':
                    retSensSet.canSpeed1 = canspd
                    this.canSpeed1.setCurrentIndex(0)
                else:
                    retSensSet.canSpeed1 = box[1]
                    this.canSpeed1.setCurrentText(box[1])
            elif box[0] == 'CANSPEED2': # ok
                canspd = hex(int(box[1]))
                if canspd == '0xffffffff':
                    retSensSet.canSpeed2 = canspd
                    this.canSpeed2.setCurrentIndex(0)
                else:
                    retSensSet.canSpeed2 = box[1]
                    this.canSpeed2.setCurrentText(box[1])
            elif box[0] == 'CODE':
                retSensSet.code = box[1]
                # this.lineSnsCode.setText(box[1])
            elif box[0] == listItemsTtu[12]:  # AIN1
                temp = box[1].split(":")
                ret = AinSetting()
                ret.AinPorogUp = int(temp[0])
                this.spinLimitUp1.setValue(int(temp[0]))
                ret.AinPorogDown = int(temp[1])
                this.spinLimitDown1.setValue(int(temp[1]))
                retSensSet.ainSet[0] = ret
            elif box[0] == listItemsTtu[14]:  # AIN2
                temp = box[1].split(":")
                ret = AinSetting()
                ret.AinPorogUp = int(temp[0])
                this.spinLimitUp2.setValue(int(temp[0]))
                ret.AinPorogDown = int(temp[1])
                this.spinLimitDown2.setValue(int(temp[1]))
                retSensSet.ainSet[1] = ret
            elif box[0] == listItemsTtu[1]:  # COUNT1 ok
                temp = box[1].split(":")
                cntSet = CountSetting()
                cntSet.modeCnt = int(temp[0])
                this.comboCnt1.setCurrentIndex(int(temp[0]))
                cntSet.event1 = int(temp[1])
                this.spinEvent11.setValue(int(temp[1]))
                cntSet.event2 = int(temp[2])
                this.spinEvent21.setValue(int(temp[2]))
                cntSet.source = int(temp[3])
                this.spinSourceValue1.setValue(int(temp[3]))
                this.comboSourseDin1.setCurrentIndex(0)
                if cntSet.source & CODE_SENS.CODE_CAN != 0:
                    this.countSourseDin1.setCurrentIndex(1)
                retSensSet.cntSet[0] = cntSet
            elif box[0] == listItemsTtu[2]:  # COUNT2 ok
                temp = box[1].split(":")
                cntSet = CountSetting()
                cntSet.modeCnt = int(temp[0])
                this.comboCnt2.setCurrentIndex(int(temp[0]))
                cntSet.event1 = int(temp[1])
                this.spinEvent12.setValue(int(temp[1]))
                cntSet.event2 = int(temp[2])
                this.spinEvent22.setValue(int(temp[2]))
                cntSet.source = int(temp[3])
                this.spinSourceValue2.setValue(int(temp[3]))
                this.comboSourseDin2.setCurrentIndex(0)
                if cntSet.source & CODE_SENS.CODE_CAN != 0:
                    this.comboSourseDin2.setCurrentIndex(1)
                retSensSet.cntSet[1] = cntSet
            elif box[0] == listItemsTtu[15]:  # COUNT3 ok
                temp = box[1].split(":")
                cntSet = CountSetting()
                cntSet.modeCnt = int(temp[0])
                this.comboCnt3.setCurrentIndex(int(temp[0]))
                cntSet.event1 = int(temp[1])
                this.spinEvent13.setValue(int(temp[1]))
                cntSet.event2 = int(temp[2])
                this.spinEvent23.setValue(int(temp[2]))
                cntSet.source = int(temp[3])
                this.spinSourceValue3.setValue(int(temp[3]))
                this.comboSourseDin3.setCurrentIndex(0)
                if cntSet.source & CODE_SENS.CODE_CAN != 0:
                    this.comboSourseDin3.setCurrentIndex(1)
                retSensSet.cntSet[2] = cntSet
            elif box[0] == listItemsTtu[0]:  # DIN ok
                temp = box[1].split(":")
                ret = DinSetting()
                ret.bounce[0] = int(temp[1])
                this.comboBounceDin1.setCurrentIndex(int(temp[1]))
                ret.bounce[1] = int(temp[2])
                this.comboBounceDin2.setCurrentIndex(int(temp[2]))
                try:
                    ret.bounce[2] = int(temp[3])
                    this.comboBounceDin3.setCurrentIndex(int(temp[3]))
                except Exception:
                    ret.bounce[2] = 0
                    this.comboBounceDin3.setCurrentIndex(0)
                try:
                    ret.bounce[3] = int(temp[4])
                    this.comboBounceDin4.setCurrentIndex(int(temp[4]))
                except Exception:
                    ret.bounce[3] = 0
                    this.comboBounceDin4.setCurrentIndex(0)
                ret.evtMask = int(temp[0])
                this.checkDin1.setChecked(False)
                if ret.evtMask & 0x80 == 0x80:
                    this.checkDin1.setChecked(True)
                this.checkDin2.setChecked(False)
                if ret.evtMask & 0x40 == 0x40:
                    this.checkDin2.setChecked(True)
                this.checkDin3.setChecked(False)
                if ret.evtMask & 0x20 == 0x20:
                    this.checkDin3.setChecked(True)
                this.checkDin4.setChecked(False)
                if ret.evtMask & 0x10 == 0x10:
                    this.checkDin4.setChecked(True)
                retSensSet.dinSet = ret
            elif box[0] == listItemsTtu[3]:  # RS485_1 ok
                temp = box[1].split(":")
                llsSet = LlsTypeSetting()
                if int(temp[0]) == TYPE_LLS.OMNICOMM_RES:
                    llsSet.type = TYPE_LLS.OMNICOMM_RES
                    this.combors485TypeSens1.setCurrentText('FUEL')
                elif int(temp[0]) == TYPE_LLS.SOVA_RES:
                    llsSet.type = TYPE_LLS.SOVA_RES
                    this.combors485TypeSens1.setCurrentText('RFID')
                llsSet.evtDelta = int(temp[5])
                this.spinrs485Event1.setValue(int(temp[5]))
                llsSet.request = int(temp[6])
                if llsSet.request == 0:
                    this.checkrs485ReqData1.setChecked(False)
                else:
                    this.checkrs485ReqData1.setChecked(True)
                llsSet.address = int(temp[1])
                this.spinrs485Adress1.setValue(int(temp[1]))
                llsSet.startBit = int(temp[2])
                this.spinrs485StartBit1.setValue(int(temp[2]))
                llsSet.length = int(temp[3])
                this.spinrs485Length1.setValue(int(temp[3]))
                llsSet.average = int(temp[4])
                this.spinrs485Average1.setValue(int(temp[4]))
                retSensSet.rs485Set[0] = llsSet
            elif box[0] == listItemsTtu[4]:  # RS485_2 ok
                temp = box[1].split(":")
                llsSet = LlsTypeSetting()
                if int(temp[0]) == TYPE_LLS.OMNICOMM_RES:
                    llsSet.type = TYPE_LLS.OMNICOMM_RES
                    this.combors485TypeSens2.setCurrentText('FUEL')
                elif int(temp[0]) == TYPE_LLS.SOVA_RES:
                    llsSet.type = TYPE_LLS.SOVA_RES
                    this.combors485TypeSens2.setCurrentText('RFID')
                llsSet.evtDelta = int(temp[5])
                this.spinrs485Event2.setValue(int(temp[5]))
                llsSet.request = int(temp[6])
                if llsSet.request == 0:
                    this.checkrs485ReqData2.setChecked(False)
                else:
                    this.checkrs485ReqData2.setChecked(True)
                llsSet.address = int(temp[1])
                this.spinrs485Adress2.setValue(int(temp[1]))
                llsSet.startBit = int(temp[2])
                this.spinrs485StartBit2.setValue(int(temp[2]))
                llsSet.length = int(temp[3])
                this.spinrs485Length2.setValue(int(temp[3]))
                llsSet.average = int(temp[4])
                this.spinrs485Average2.setValue(int(temp[4]))
                retSensSet.rs485Set[1] = llsSet
            elif box[0] == listItemsTtu[5]:  # RS485_3 ok
                temp = box[1].split(":")
                llsSet = LlsTypeSetting()
                if int(temp[0]) == TYPE_LLS.OMNICOMM_RES:
                    llsSet.type = TYPE_LLS.OMNICOMM_RES
                    this.combors485TypeSens3.setCurrentText('FUEL')
                elif int(temp[0]) == TYPE_LLS.SOVA_RES:
                    llsSet.type = TYPE_LLS.SOVA_RES
                    this.combors485TypeSens3.setCurrentText('RFID')
                llsSet.evtDelta = int(temp[5])
                this.spinrs485Event3.setValue(int(temp[5]))
                llsSet.request = int(temp[6])
                if llsSet.request == 0:
                    this.checkrs485ReqData3.setChecked(False)
                else:
                    this.checkrs485ReqData3.setChecked(True)
                llsSet.address = int(temp[1])
                this.spinrs485Adress3.setValue(int(temp[1]))
                llsSet.startBit = int(temp[2])
                this.spinrs485StartBit3.setValue(int(temp[2]))
                llsSet.length = int(temp[3])
                this.spinrs485Length3.setValue(int(temp[3]))
                llsSet.average = int(temp[4])
                this.spinrs485Average3.setValue(int(temp[4]))
                retSensSet.rs485Set[2] = llsSet
            elif box[0] == listItemsTtu[6]:  # RS485_4 ok
                temp = box[1].split(":")
                llsSet = LlsTypeSetting()
                if int(temp[0]) == TYPE_LLS.OMNICOMM_RES:
                    llsSet.type = TYPE_LLS.OMNICOMM_RES
                    this.combors485TypeSens4.setCurrentText('FUEL')
                elif int(temp[0]) == TYPE_LLS.SOVA_RES:
                    llsSet.type = TYPE_LLS.SOVA_RES
                    this.combors485TypeSens4.setCurrentText('RFID')
                llsSet.evtDelta = int(temp[5])
                this.spinrs485Event4.setValue(int(temp[5]))
                llsSet.request = int(temp[6])
                if llsSet.request == 0:
                    this.checkrs485ReqData4.setChecked(False)
                else:
                    this.checkrs485ReqData4.setChecked(True)
                llsSet.address = int(temp[1])
                this.spinrs485Adress4.setValue(int(temp[1]))
                llsSet.startBit = int(temp[2])
                this.spinrs485StartBit4.setValue(int(temp[2]))
                llsSet.length = int(temp[3])
                this.spinrs485Length4.setValue(int(temp[3]))
                llsSet.average = int(temp[4])
                this.spinrs485Average4.setValue(int(temp[4]))
                retSensSet.rs485Set[3] = llsSet
            elif box[0] == listItemsTtu[7]:  # RS485_5 ok
                temp = box[1].split(":")
                llsSet = LlsTypeSetting()
                if int(temp[0]) == TYPE_LLS.OMNICOMM_RES:
                    llsSet.type = TYPE_LLS.OMNICOMM_RES
                    this.combors485TypeSens5.setCurrentText('FUEL')
                elif int(temp[0]) == TYPE_LLS.SOVA_RES:
                    llsSet.type = TYPE_LLS.SOVA_RES
                    this.combors485TypeSens5.setCurrentText('RFID')
                llsSet.evtDelta = int(temp[5])
                this.spinrs485Event5.setValue(int(temp[5]))
                llsSet.request = int(temp[6])
                if llsSet.request == 0:
                    this.checkrs485ReqData5.setChecked(False)
                else:
                    this.checkrs485ReqData5.setChecked(True)
                llsSet.address = int(temp[1])
                this.spinrs485Adress5.setValue(int(temp[1]))
                llsSet.startBit = int(temp[2])
                this.spinrs485StartBit5.setValue(int(temp[2]))
                llsSet.length = int(temp[3])
                this.spinrs485Length5.setValue(int(temp[3]))
                llsSet.average = int(temp[4])
                this.spinrs485Average5.setValue(int(temp[4]))
                retSensSet.rs485Set[4] = llsSet
            elif box[0] == listItemsTtu[8]:  # RS485_6 ok
                temp = box[1].split(":")
                llsSet = LlsTypeSetting()
                if int(temp[0]) == TYPE_LLS.OMNICOMM_RES:
                    llsSet.type = TYPE_LLS.OMNICOMM_RES
                    this.combors485TypeSens6.setCurrentText('FUEL')
                elif int(temp[0]) == TYPE_LLS.SOVA_RES:
                    llsSet.type = TYPE_LLS.SOVA_RES
                    this.combors485TypeSens6.setCurrentText('RFID')
                llsSet.evtDelta = int(temp[5])
                this.spinrs485Event6.setValue(int(temp[5]))
                llsSet.request = int(temp[6])
                if llsSet.request == 0:
                    this.checkrs485ReqData6.setChecked(False)
                else:
                    this.checkrs485ReqData6.setChecked(True)
                llsSet.address = int(temp[1])
                this.spinrs485Adress6.setValue(int(temp[1]))
                llsSet.startBit = int(temp[2])
                this.spinrs485StartBit6.setValue(int(temp[2]))
                llsSet.length = int(temp[3])
                this.spinrs485Length6.setValue(int(temp[3]))
                llsSet.average = int(temp[4])
                this.spinrs485Average6.setValue(int(temp[4]))
                retSensSet.rs485Set[5] = llsSet
            elif box[0] == listItemsTtu[9]:  # CAN1 ok
                temp = box[1].split(":")
                tmpCanSet = CanSensors()
                tmpCanSet.lenBit = int(temp[4])
                this.spinlengthCan1.setValue(int(temp[4]))
                tmpCanSet.startBit = int(temp[3])
                this.spinstartbitCan1.setValue(int(temp[3]))
                tmpCanSet.timeout = int(temp[5])
                this.spintimeoutCan1.setValue(int(temp[5]))
                tmpCanSet.pgn = int(temp[1], 16)
                this.lineaddresCan1.setText(hex(tmpCanSet.pgn))
                tmpCanSet.mask = int(temp[2], 16)
                this.linemaskCan1.setText(hex(tmpCanSet.mask))
                tmpCanSet.canUse = int(temp[0])
                this.comboworkCan1.setCurrentIndex(int(temp[0]))
                retSensSet.canSet[0] = tmpCanSet
            elif box[0] == listItemsTtu[10]:  # CAN2 ok
                temp = box[1].split(":")
                tmpCanSet = CanSensors()
                tmpCanSet.lenBit = int(temp[4])
                this.spinlengthCan2.setValue(int(temp[4]))
                tmpCanSet.startBit = int(temp[3])
                this.spinstartbitCan2.setValue(int(temp[3]))
                tmpCanSet.timeout = int(temp[5])
                this.spintimeoutCan2.setValue(int(temp[5]))
                tmpCanSet.pgn = int(temp[1], 16)
                this.lineaddresCan2.setText(hex(tmpCanSet.pgn))
                tmpCanSet.mask = int(temp[2], 16)
                this.linemaskCan2.setText(hex(tmpCanSet.mask))
                tmpCanSet.canUse = int(temp[0])
                this.comboworkCan2.setCurrentIndex(int(temp[0]))
                retSensSet.canSet[1] = tmpCanSet
            elif box[0] == listItemsTtu[11]:  # CAN3 ok
                temp = box[1].split(":")
                tmpCanSet = CanSensors()
                tmpCanSet.lenBit = int(temp[4])
                this.spinlengthCan3.setValue(int(temp[4]))
                tmpCanSet.startBit = int(temp[3])
                this.spinstartbitCan3.setValue(int(temp[3]))
                tmpCanSet.timeout = int(temp[5])
                this.spintimeoutCan3.setValue(int(temp[5]))
                tmpCanSet.pgn = int(temp[1], 16)
                this.lineaddresCan3.setText(hex(tmpCanSet.pgn))
                tmpCanSet.mask = int(temp[2], 16)
                this.linemaskCan3.setText(hex(tmpCanSet.mask))
                tmpCanSet.canUse = int(temp[0])
                this.comboworkCan3.setCurrentIndex(int(temp[0]))
                retSensSet.canSet[2] = tmpCanSet
            elif box[0] == listItemsTtu[13]:  # RS485_7 ok
                temp = box[1].split(":")
                llsSet = LlsTypeSetting()
                if int(temp[0]) == TYPE_LLS.OMNICOMM_RES:
                    llsSet.type = TYPE_LLS.OMNICOMM_RES
                    this.combors485TypeSens7.setCurrentText('FUEL')
                elif int(temp[0]) == TYPE_LLS.SOVA_RES:
                    llsSet.type = TYPE_LLS.SOVA_RES
                    this.combors485TypeSens7.setCurrentText('RFID')
                llsSet.evtDelta = int(temp[5])
                this.spinrs485Event7.setValue(int(temp[5]))
                llsSet.request = int(temp[6])
                if llsSet.request == 0:
                    this.checkrs485ReqData7.setChecked(False)
                else:
                    this.checkrs485ReqData7.setChecked(True)
                llsSet.address = int(temp[1])
                this.spinrs485Adress7.setValue(int(temp[1]))
                llsSet.startBit = int(temp[2])
                this.spinrs485StartBit7.setValue(int(temp[2]))
                llsSet.length = int(temp[3])
                this.spinrs485Length7.setValue(int(temp[3]))
                llsSet.average = int(temp[4])
                this.spinrs485Average7.setValue(int(temp[4]))
                retSensSet.rs485Set[6] = llsSet
            elif box[0] == 'SMOUT1':
                temp = box[1].split(":")
                smOut = SmoutValue()
                try:
                    smOut.baseSensCode = int(temp[0])
                    this.DinUsedIn.setCurrentIndex(int(temp[0]))
                except Exception as ex:
                    smOut.baseSensCode = 0
                    this.DinUsedIn.setCurrentIndex(0)
                try:
                    smOut.porogUp = int(temp[1])
                    this.DinLvlUp.setValue(int(temp[1]))
                except Exception as ex:
                    smOut.porogUp = 0
                    this.DinLvlUp.setValue(0)
                try:
                    smOut.porogDown = int(temp[2])
                    this.DinLvlLow.setValue(int(temp[2]))
                except Exception as ex:
                    smOut.porogDown = 0
                    this.DinLvlLow.setValue(0)
                try:
                    smOut.cmdIn = temp[3]
                    this.DinCmdIn.setText(temp[3])
                except Exception as ex:
                    smOut.cmdIn = ""
                    this.DinCmdIn.setText("")
                try:
                    smOut.cmdOut = temp[4]
                    this.DinCmdOut.setText(temp[4])
                except Exception as ex:
                    smOut.cmdOut = ""
                    this.DinCmdOut.setText("")
                self.smOutVal = smOut
            else:
                raise Exception("setValueSensorsTtu. Unknown sensors key:" + box[0])
        return

    def setValueSensorsTtl(self, commanda, this):
        listItemsTtl = self.orderItemsTtlSys
        retSensSet = SensorsSetting()
        for box in commanda:
            if box[0] == 'RS485SPEED':
                retSensSet.rs485Speed = int(box[1])
                this.comboRs485.setCurrentText(box[1])
            elif box[0] == 'ACCLVL':
                retSensSet.SensitivityAcc = int(box[1])
                this.spinAcc.setValue(int(box[1]))
            elif box[0] == 'CODE':
                retSensSet.code = box[1]
                #this.lineSnsCode.setText(box[1])
            elif box[0] == listItemsTtl[12]:  # AIN1
                temp = box[1].split(":")
                ret = AinSetting()
                ret.AinPorogUp = int(temp[0])
                this.spinLimitUp1.setValue(int(temp[0]))
                ret.AinPorogDown = int(temp[1])
                this.spinLimitDown1.setValue(int(temp[1]))
                retSensSet.ainSet[0] = ret
            elif box[0] == listItemsTtl[14]:  # AIN2
                temp = box[1].split(":")
                ret = AinSetting()
                ret.AinPorogUp = int(temp[0])
                this.spinLimitUp2.setValue(int(temp[0]))
                ret.AinPorogDown = int(temp[1])
                this.spinLimitDown2.setValue(int(temp[1]))
                retSensSet.ainSet[1] = ret
            elif box[0] == listItemsTtl[1]:  # COUNT1
                temp = box[1].split(":")
                cntSet = CountSetting()
                cntSet.modeCnt = int(temp[0])
                this.comboCnt1.setCurrentIndex(int(temp[0]))
                cntSet.event1 = int(temp[1])
                this.spinEvent11.setValue(int(temp[1]))
                cntSet.event2 = int(temp[2])
                this.spinEvent21.setValue(int(temp[2]))
                retSensSet.cntSet[0] = cntSet
            elif box[0] == listItemsTtl[2]:  # COUNT2
                temp = box[1].split(":")
                cntSet = CountSetting()
                cntSet.modeCnt = int(temp[0])
                this.comboCnt2.setCurrentIndex(int(temp[0]))
                cntSet.event1 = int(temp[1])
                this.spinEvent12.setValue(int(temp[1]))
                cntSet.event2 = int(temp[2])
                this.spinEvent22.setValue(int(temp[2]))
                retSensSet.cntSet[1] = cntSet
            elif box[0] == listItemsTtl[15]:  # COUNT3
                temp = box[1].split(":")
                cntSet = CountSetting()
                cntSet.modeCnt = int(temp[0])
                this.comboCnt3.setCurrentIndex(int(temp[0]))
                cntSet.event1 = int(temp[1])
                this.spinEvent13.setValue(int(temp[1]))
                cntSet.event2 = int(temp[2])
                this.spinEvent23.setValue(int(temp[2]))
                retSensSet.cntSet[2] = cntSet
            elif box[0] == listItemsTtl[0]:  # DIN
                temp = box[1].split(":")
                ret = DinSetting()
                ret.bounce[0] = int(temp[1])
                this.comboBounceDin1.setCurrentIndex(int(temp[1]))
                ret.bounce[1] = int(temp[2])
                this.comboBounceDin2.setCurrentIndex(int(temp[2]))
                try:
                    ret.bounce[2] = int(temp[3])
                    this.comboBounceDin3.setCurrentIndex(int(temp[3]))
                except Exception:
                    ret.bounce[2] = 0
                    this.comboBounceDin3.setCurrentIndex(0)
                try:
                    ret.bounce[3] = int(temp[4])
                    this.comboBounceDin4.setCurrentIndex(int(temp[4]))
                except Exception:
                    ret.bounce[3] = 0
                    this.comboBounceDin4.setCurrentIndex(0)
                ret.evtMask = int(temp[0])
                this.checkDin1.setChecked(False)
                if ret.evtMask & 0x80 == 0x80:
                    this.checkDin1.setChecked(True)
                this.checkDin2.setChecked(False)
                if ret.evtMask & 0x40 == 0x40:
                    this.checkDin2.setChecked(True)
                retSensSet.dinSet = ret
            elif box[0] == listItemsTtl[3]:  # FUEL1
                temp = box[1].split(":")
                fuelSet = FuelSetting()
                fuelSet.addressFuel = int(temp[0])
                this.classFuel1.Address = int(temp[0])
                fuelSet.startBitFuel = int(temp[1])
                this.classFuel1.StartBit = int(temp[1])
                fuelSet.lengthFuel = int(temp[2])
                this.classFuel1.Length = int(temp[2])
                fuelSet.averageFuel = int(temp[3])
                this.classFuel1.Average = int(temp[3])
                retSensSet.fuelSet[0] = fuelSet
            elif box[0] == listItemsTtl[4]:  # FUEL2
                temp = box[1].split(":")
                fuelSet = FuelSetting()
                fuelSet.addressFuel = int(temp[0])
                this.classFuel2.Address = int(temp[0])
                fuelSet.startBitFuel = int(temp[1])
                this.classFuel2.StartBit = int(temp[1])
                fuelSet.lengthFuel = int(temp[2])
                this.classFuel2.Length = int(temp[2])
                fuelSet.averageFuel = int(temp[3])
                this.classFuel2.Average = int(temp[3])
                retSensSet.fuelSet[1] = fuelSet
            elif box[0] == listItemsTtl[6]:  # FUEL3
                temp = box[1].split(":")
                fuelSet = FuelSetting()
                fuelSet.addressFuel = int(temp[0])
                this.classFuel3.Address = int(temp[0])
                fuelSet.startBitFuel = int(temp[1])
                this.classFuel3.StartBit(int(temp[1]))
                fuelSet.lengthFuel = int(temp[2])
                this.classFuel3.Length(int(temp[2]))
                fuelSet.averageFuel = int(temp[3])
                this.classFuel3.Average(int(temp[3]))
                retSensSet.fuelSet[2] = fuelSet
            elif box[0] == listItemsTtl[7]:  # FUEL4
                temp = box[1].split(":")
                fuelSet = FuelSetting()
                fuelSet.addressFuel = int(temp[0])
                this.classFuel4.Address = int(temp[0])
                fuelSet.startBitFuel = int(temp[1])
                this.classFuel4.StartBit = int(temp[1])
                fuelSet.lengthFuel = int(temp[2])
                this.classFuel4.Length = int(temp[2])
                fuelSet.averageFuel = int(temp[3])
                this.classFuel4.Average = int(temp[3])
                retSensSet.fuelSet[3] = fuelSet
            elif box[0] == listItemsTtl[13]:  # FUEL5
                temp = box[1].split(":")
                fuelSet = FuelSetting()
                fuelSet.addressFuel = int(temp[0])
                this.classFuel5.Address = int(temp[0])
                fuelSet.startBitFuel = int(temp[1])
                this.classFuel5.StartBit = int(temp[1])
                fuelSet.lengthFuel = int(temp[2])
                this.classFuel5.Length = int(temp[2])
                fuelSet.averageFuel = int(temp[3])
                this.classFuel5.Average = int(temp[3])
                retSensSet.fuelSet[4] = fuelSet
            elif box[0] == listItemsTtl[9]:  # RESERVED1
                pass
            elif box[0] == listItemsTtl[10]:  # RESERVED2
                pass
            elif box[0] == listItemsTtl[11]:  # RESERVED3
                pass
            elif box[0] == listItemsTtl[5]:  # RFID1
                temp = box[1].split(":")
                rfidSet = RfidSetting()
                rfidSet.addressRfid = int(temp[0])
                this.classRfid1.Address = int(temp[0])
                rfidSet.lengthBitRfid = int(temp[2])
                this.classRfid1.Length = int(temp[2])
                rfidSet.startBitRfid = int(temp[1])
                this.classRfid1.StartBit = int(temp[1])
                retSensSet.rfidSet[0] = rfidSet
            elif box[0] == listItemsTtl[8]:  # RFID2
                temp = box[1].split(":")
                rfidSet = RfidSetting()
                rfidSet.addressRfid = int(temp[0])
                this.classRfid2.Address = int(temp[0])
                rfidSet.lengthBitRfid = int(temp[2])
                this.classRfid2.Length = int(temp[2])
                rfidSet.startBitRfid = int(temp[1])
                this.classRfid2.StartBit = int(temp[1])
                retSensSet.rfidSet[1] = rfidSet
            else:
                raise Exception("setValueSensorsTtl. Unknown sensors key:" + box[0])
        return

    def SetSensorsTtl(self, reqCmd, sensParam):
        try:
            commanda = []
            listCommands = []
            for cmd in reqCmd:
                buff = cmd.split(',')
                del buff[0]
                data = buff[len(buff) - 1].split("\x00")
                buff[len(buff) - 1] = data[0]

                for rec in buff:
                    if rec == '':
                        continue
                    array = rec.split('=')
                    listCommands.append(array[0])
                    commanda.append([array[0], array[1]])

            self.setValueSensorsTtl(commanda, sensParam)

            for ls in listCommands:
                try:
                    indx = self.listCommandTtl.index(ls)
                    sensParam.checkedListBoxSensors[indx].setChecked(True)
                except Exception as er:
                    pass
            return True, ''
        except Exception as err:
            return False, str(err)

    def SetSensorsTtu(self, reqCmd, sensParam):
        try:
            commanda = []
            listCommands = []
            for cmd in reqCmd:
                buff = cmd.split(',')
                del buff[0]
                data = buff[len(buff) - 1].split("\x00")
                buff[len(buff) - 1] = data[0]

                for rec in buff:
                    if rec == '':
                        continue
                    array = rec.split('=')
                    listCommands.append(array[0])
                    commanda.append([array[0], array[1]])

            self.setValueSensorsTtu(commanda, sensParam)

            for ls in listCommands:
                try:
                    indx = self.listCommandTtu.index(ls)
                    sensParam.checkedListBoxSensors[indx].setChecked(True)
                except Exception as er:
                    pass
            return True, ''
        except Exception as err:
            return False, str(err)


    def datetime_from_utc_to_local(self, utc_datetime):
        def hms_to_seconds(t):
            h, m, s = [int(i) for i in t.split(':')]
            return 3600 * h + 60 * m + s

        now_timestamp = time.time()
        offset = datetime.fromtimestamp(now_timestamp) - datetime.utcfromtimestamp(now_timestamp)
        offsetsec = hms_to_seconds(str(offset))
        return utc_datetime + offsetsec

    def UnpackLabelsTtu(self, rqstCmd, dtRespons,  tblLabel, labelDtLbl, labelBgnIndex, labelMskLabel, labelEndMrkr, btnGetLbl, lblLastReq, wItem):
        try:
            if 'SLEGAL' in rqstCmd[0]:
                buff = []
                for cmd in rqstCmd:
                    buff = cmd.split(',')
                    del buff[0]
                    data = buff[len(buff) - 1].split("\x00")
                    buff[len(buff) - 1] = data[0]

                parameters = []
                for rec in buff:
                    array = rec.split('=')
                    parameters = array[1].split(":")

                if len(parameters) > 0:
                    resptime = int(time.mktime(time.strptime(dtRespons[0], '%d-%m-%Y %H:%M:%S')))
                    local = self.datetime_from_utc_to_local(resptime)
                    timeresp = time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime(local))
                    lblLastReq.setText("Date of last request: " + str(timeresp))
                    labelDtLbl.setText("Date of Labels: " + parameters[0])
                    labelBgnIndex.setText("Beginer index: " + parameters[1])
                    labelMskLabel.setText("Mask of Labels: " + parameters[2])
                    markerTheEnd = int(parameters[len(parameters) - 1])
                    if markerTheEnd == 0 or markerTheEnd == 65535:
                        labels = parameters[3:len(parameters) - 1]
                    else:
                        labels = parameters[3:len(parameters)]
                    metka = 'Метка_'
                    for el in labels:
                        rowPosition = tblLabel.rowCount()
                        tblLabel.insertRow(rowPosition)
                        tblLabel.setItem(rowPosition, 0, wItem(metka + str(rowPosition)))
                        tblLabel.setItem(rowPosition, 1, wItem(el))
                    tblLabel.resizeColumnsToContents()
                    tblLabel.resizeRowsToContents()
                    markerTheEnd = int(parameters[len(parameters) - 1])
                    if markerTheEnd == 0 or markerTheEnd == 65535:
                        btnGetLbl.setEnabled(False)
                        labelEndMrkr.setText("End marker of Labels: " + str(markerTheEnd))
                    else:
                        btnGetLbl.setEnabled(True)
                        labelEndMrkr.setText("End marker of Labels: " + ' ')
                    return True, labels[len(labels) - 1], tblLabel.rowCount()
                else:
                    return False, "Unknown parameters for commands SLEGAL", tblLabel.rowCount()
            return False, 'Unknown commands, need SLEGAL', tblLabel.rowCount()
        except Exception as err:
            return False, str(err), ''

    def UnpackRequest(self, reqCmd, lnrssi, lnsatt, lnvbort, tableSens, tableLls, widgetitem):
        try: #SVIEW1=1:sat=0:rssi=0:U=150,SVIEW0=E0:0:0:0:0:0:0:0:0:0:0:0:0:0:430:0,
            if 'SVIEW' in reqCmd[0]:
                commanda = []
                listCommands = []
                for cmd in reqCmd:
                    buff = cmd.split(',')
                    del buff[0]
                    data = buff[len(buff) - 1].split("\x00")
                    buff[len(buff) - 1] = data[0]

                    for rec in buff:
                        if 'SVIEW1' in rec:
                            if rec == '':
                                continue
                            array = rec.split(':')
                            for dmc in array:
                                liter = dmc.split("=")
                                listCommands.append(liter[0])
                                commanda.append([liter[0], liter[1]])
                        elif 'SVIEW0' in rec:
                            liter = rec.split('=')
                            listCommands.append(liter[0])
                            commanda.append([liter[0], liter[1]])
                for namecm in commanda:
                    if 'sat' == namecm[0]:
                        lnsatt.setText(namecm[1])
                    elif 'rssi' == namecm[0]:
                        lnrssi.setText(namecm[1])
                    elif 'U' == namecm[0]:
                        lnvbort.setText(namecm[1])
                    elif 'SVIEW0' == namecm[0]:
                        self.UpdateViewSensorsValue(namecm[1], tableSens, widgetitem)
                    elif 'SVIEW1' == namecm[0]:
                        pass
                    else:
                        pass
            elif "CMD485" in reqCmd[0]: #$RCSTT,1234,CMD485=3101066C Type=3101803D
                reqCmd[0] = "%%DR$RCSTT,1234,CMD485=3101066C Type=3101803D\x00"
                buffCmd = []
                for rcmd in reqCmd:
                    indx1 = rcmd.find("CMD485")
                    if indx1 > -1:
                        indx2 = rcmd.find(" ")
                        if indx2 > -1:
                            buffCmd.append(["CMD485", rcmd[indx1 + 7:indx2]])
                            indx3 = rcmd.find("Type")
                            if indx3 > -1:
                                indx4 = rcmd.find("\x00")
                                if indx4 > -1:
                                    buffCmd.append(["Type", rcmd[indx3 + 5:indx4]])
                for data in buffCmd:
                    if "CMD485" in data:
                        buf = MyConverter.GetByteFromHexString(data[1])
                        buf = self.DataGotTypeSensor(buf)
                        if len(buf) > 0:
                            value = str(buf[LLS_PROTOCOL.IDX_ADDR])
                            tableLls.setItem(0, 1, widgetitem(value))
                            value = ''
                            for p in buf[3: 3 + 4]:
                                value += str(p)
                            tableLls.setItem(1, 1, widgetitem(value))
                            value = MyConverter.GetHexString(buf)
                            tableLls.setItem(2, 1, widgetitem(value))    
            return True, ''
        except Exception as err:
            return False, str(err)
        
    def DataGotTypeSensor(self, buf):
        list = copy.deepcopy(buf)
        while (len(list) >= LLS_PROTOCOL.LEN_GET_TYPE_DEV):
            try:
                start = list.index(LLS_PROTOCOL.OMNICOMM_RES)
            except Exception as er:
                start = -1
            if start < 0:
                return []
            if start > 0:
                del list[0:start]
            else:
                if LlsSensor.CalculateCRC(list, LLS_PROTOCOL.LEN_GET_TYPE_DEV) == list[LLS_PROTOCOL.LEN_GET_TYPE_DEV]:
                    if list[LLS_PROTOCOL.IDX_CMD] == LLS_PROTOCOL.GET_TYPE_DEV:
                        return list
                    else:
                        del list[0]
                else:
                    del list[0]
        return []

    # SVIEW0 = E0:0:0:0:0:0:0:0:0:0:0:0:0:0:430:0,
    def UpdateViewSensorsValue(self, datacmd, sensTable, item):
        temp = datacmd.split(':')
        tform = "{0}/{1}"
        if len(temp) == self.MAX_SENSORS:
            value = tform.format(temp[12], format(int(temp[12]), "08X"))
            sensTable.setItem(0, 1, item(value)) # AIN1
            value = tform.format(temp[14], format(int(temp[14]), "08X"))
            sensTable.setItem(1, 1, item(value)) # AIN2
            value = tform.format(temp[9], format(int(temp[9]), "08X"))
            sensTable.setItem(2, 1, item(value)) # CAN1
            value = tform.format(temp[10], format(int(temp[10]), "08X"))
            sensTable.setItem(3, 1, item(value)) # CAN2
            value = tform.format(temp[11], format(int(temp[11]), "08X"))
            sensTable.setItem(4, 1, item(value)) # CAN3
            value = tform.format(temp[1], format(int(temp[1]), "08X"))
            sensTable.setItem(5, 1, item(value)) # COUNT1
            value = tform.format(temp[2], format(int(temp[2]), "08X"))
            sensTable.setItem(6, 1, item(value)) # COUNT2
            value = tform.format(temp[15], format(int(temp[15]), "08X"))
            sensTable.setItem(7, 1, item(value)) # COUNT3
            value = tform.format(str(int(temp[0], 16)), format(int(temp[0], 16), "08X"))
            sensTable.setItem(8, 1, item(value)) # DIN
            value = tform.format(temp[3], format(int(temp[3]), "08X"))
            sensTable.setItem(9, 1, item(value)) # RS485_1
            value = tform.format(temp[4], format(int(temp[4]), "08X"))
            sensTable.setItem(10, 1, item(value)) # RS485_2
            value = tform.format(temp[5], format(int(temp[5]), "08X"))
            sensTable.setItem(11, 1, item(value)) # RS485_3
            value = tform.format(temp[6], format(int(temp[6]), "08X"))
            sensTable.setItem(12, 1, item(value)) # RS485_4
            value = tform.format(temp[7], format(int(temp[7]), "08X"))
            sensTable.setItem(13, 1, item(value)) # RS485_5
            value = tform.format(temp[8], format(int(temp[8]), "08X"))
            sensTable.setItem(14, 1, item(value)) # RS485_6
            value = tform.format(temp[13], format(int(temp[13]), "08X"))
            sensTable.setItem(15, 1, item(value)) # RS485_7
        return

    def SetTerminal(self, requestCmd, textterm):
        try:
            for rcmd in requestCmd:
                slog = "[" + str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(time.time())))) + "] " + \
                   "RX>>: " + str(rcmd)
                textterm.appendPlainText(slog)
            return True, ''
        except Exception as err:
            return False, str(err)

    def setTableGprs(self, device, tableregim, QTableWidgetItem):
        tableregim.setItem(0, 1, QTableWidgetItem("NameApn_"+ device))
        tableregim.setItem(1, 1, QTableWidgetItem(str(self.configGprsBase.Mode))) # "Режим"
        tableregim.setItem(2, 1, QTableWidgetItem(self.configGprsBase.ApnServer)) # "Точка доступа GPRS"
        tableregim.setItem(3, 1, QTableWidgetItem(self.configGprsBase.ApnLogin)) # "Логин для GPRS"
        tableregim.setItem(4, 1, QTableWidgetItem(self.configGprsBase.ApnPassword)) # "Пароль для GPRS"
        tableregim.setItem(5, 1, QTableWidgetItem(self.configGprsBase.DnsServer)) # "DNS конфигурация"
        tableregim.setItem(6, 1, QTableWidgetItem(self.configGprsBase.DialNumber)) # "Номер дозвона"
        tableregim.setItem(7, 1, QTableWidgetItem(self.configGprsBase.GprsLogin)) # "ISP логин"
        tableregim.setItem(8, 1, QTableWidgetItem(self.configGprsBase.GprsPassword)) # "ISP пароль"
        tableregim.resizeColumnsToContents()
        tableregim.resizeRowsToContents()
        return

    def setTableServer(self, tableserv, QTableWidgetItem):
        tableserv.setItem(0, 1, QTableWidgetItem(self.configGprsEmail.Pop3Server)) # "Сервер"
        tableserv.setItem(1, 1, QTableWidgetItem(self.configGprsEmail.Pop3Login)) # "Логин"
        tableserv.setItem(2, 1, QTableWidgetItem(self.configGprsEmail.Pop3Password)) # "Пароль"
        tableserv.resizeColumnsToContents()
        tableserv.resizeRowsToContents()
        return

    def setTabTextCmd(self, lineTxtRequest):
        self.lineTextCmd = "*"
        lineTxtRequest.appendPlainText(self.lineTextCmd)
        return

    def setTabGps(self, text_edit):
        self.lineGps = "#"
        text_edit.appendPlainText(self.lineGps)
        return

    def setCmdRecordsService(self, cmdcode, value, tphone, lineRegim, item, evStt):
        if cmdcode == 'GREG':
            evStt.ConnectRegim = int(value)
            lineRegim.setText(value)
        elif cmdcode == 'PHLEGAL1':
            evStt.Phone1 = value
            tphone.setItem(0, 1, item(value))
        elif cmdcode == 'PHLEGAL2':
            evStt.Phone2 = value
            tphone.setItem(1, 1, item(value))
        elif cmdcode == 'PHLEGAL3':
            evStt.Phone3 = value
            tphone.setItem(2, 1, item(value))
        else:
            raise Exception("setCmdRecordsService. Unknown command key:" + cmdcode)
        return

    def SetServiceTtu(self, reqcmd, tPhone, tRegim, qtWidgetItem):
        try:
            evSetting = ServiceSeting(-1)
            for cmd in reqcmd:
                buff = cmd.split(',')
                del buff[0]
                data = buff[len(buff) - 1].split("\x00")
                buff[len(buff) - 1] = data[0]
                for rec in buff:
                    array = rec.split('=')
                    self.setCmdRecordsService(array[0], array[1], tPhone, tRegim, qtWidgetItem, evSetting)
            tPhone.resizeColumnsToContents()
            tPhone.resizeRowsToContents()
            return True, ''
        except Exception as err:
            return False, str(err)

    def SetServiceTtl(self, reqcmd, tPhone, tRegim, simUse, qtWidgetItem):
        try:
            evSetting = ServiceSeting(simUse)
            for cmd in reqcmd:
                buff = cmd.split(',')
                del buff[0]
                data = buff[len(buff) - 1].split("\x00")
                buff[len(buff) - 1] = data[0]
                for rec in buff:
                    array = rec.split('=')
                    self.setCmdRecordsService(array[0], array[1], tPhone, tRegim, qtWidgetItem, evSetting)
            tPhone.resizeColumnsToContents()
            tPhone.resizeRowsToContents()
            return True, ''
        except Exception as err:
            return False, str(err)        

    def setTablePhones(self, tablePhonesParam, QTableWidgetItem):
        tablePhonesParam.setItem(0, 1, QTableWidgetItem(self.configPhone.NumberSOS)) # "Номер SOS"
        tablePhonesParam.setItem(1, 1, QTableWidgetItem(self.configPhone.NumberDspt)) # "Номер телефона диспетчера"
        tablePhonesParam.setItem(2, 1, QTableWidgetItem(self.configPhone.NumberAccept1)) # "Разрешенный номер 1"
        tablePhonesParam.setItem(3, 1, QTableWidgetItem(self.configPhone.NumberAccept2)) # "Разрешенный номер 2"
        tablePhonesParam.setItem(4, 1, QTableWidgetItem(self.configPhone.NumberAccept3)) # "Разрешенный номер 3"
        tablePhonesParam.setItem(5, 1, QTableWidgetItem("Not USE")) # "Телефон обратной связи"
        tablePhonesParam.resizeColumnsToContents()
        tablePhonesParam.resizeRowsToContents()
        return

    def setEvntCheck(self, evnttable, value, row):
        result = int(value)
        mainWidget = evnttable.cellWidget(row, 3)
        checkBox = mainWidget.layout().itemAt(0).widget()
        checkBox.setChecked(False)
        if 1 & result == 1:
            checkBox.setChecked(True)
        mainWidget = evnttable.cellWidget(row, 4)
        checkBox = mainWidget.layout().itemAt(0).widget()
        checkBox.setChecked(False)
        if 4 & result == 4:
            checkBox.setChecked(True)
        return

    def setCmdRecordsTransport(self, namecmd, valuecmd, servSett, tgprs, item, transport):
        if namecmd == 'GAPN':
            transport.ApnServer = valuecmd
            tgprs.setItem(0, 1, item(valuecmd))
        elif namecmd == "GLOGIN":
            transport.ApnLogin = valuecmd
            tgprs.setItem(1, 1, item(valuecmd))
        elif namecmd == "GPASS":
            transport.ApnPassword = valuecmd
            tgprs.setItem(2, 1, item(valuecmd))
        elif namecmd == "GDNS":
            transport.DnsServer = valuecmd
            tgprs.setItem(3, 1, item(valuecmd))
        elif namecmd == "SSERV":
            transport.ServerAddr = valuecmd
            servSett.setItem(0, 1, item(valuecmd))
        elif namecmd == "SPORT":
            transport.ServerPort = valuecmd
            servSett.setItem(1, 1, item(valuecmd))
        elif namecmd == "SLOGIN":
            transport.ServerLogin = valuecmd
            servSett.setItem(2, 1, item(valuecmd))
        elif namecmd == "SPASS":
            transport.ServerPassword = valuecmd
            servSett.setItem(3, 1, item(valuecmd))
        else:
            raise Exception("setCmdRecordsTransport. Unknown command key:" + namecmd)
        return

    def setCmdRecordsEvents(self, namecmd, valuecmd, eventtable, item, evSet):
        if namecmd == 'BMINSP':
            evSet.MinSpeed = int(valuecmd)
            eventtable.setItem(0, 1, item(valuecmd))
        elif namecmd == 'BTIM1':
            evSet.Timer1 = int(valuecmd)
            eventtable.setItem(1, 1, item(valuecmd))
        elif namecmd == 'BTIM2':
            evSet.Timer2 = int(valuecmd)
            eventtable.setItem(2, 1, item(valuecmd))
        elif namecmd == 'BCOURS':
            evSet.CourseBend = int(valuecmd)
            eventtable.setItem(3, 1, item(valuecmd))
        elif namecmd == 'BDIST1':
            evSet.Distance1 = int(valuecmd)
            eventtable.setItem(4, 1, item(valuecmd))
        elif namecmd == 'BDIST2':
            evSet.Distance2 = int(valuecmd)
            eventtable.setItem(5, 1, item(valuecmd))
        elif namecmd == 'BACC':
            evSet.SpeedChange = int(valuecmd)
            eventtable.setItem(6, 1, item(valuecmd))
        elif namecmd == 'EVMINSP':
            evSet.EventMask[EventSetting.INDEX_MIN_SPEED] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 0)
        elif namecmd == 'EVTIM1':
            evSet.EventMask[EventSetting.INDEX_TIMER1] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 1)
        elif namecmd == 'EVTIM2':
            evSet.EventMask[EventSetting.INDEX_TIMER2] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 2)
        elif namecmd == 'EVCOURS':
            evSet.EventMask[EventSetting.INDEX_COURSE] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 3)
        elif namecmd == 'EVDIST1':
            evSet.EventMask[EventSetting.INDEX_DISTANCE1] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 4)
        elif namecmd == 'EVDIST2':
            evSet.EventMask[EventSetting.INDEX_DISTANCE2] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 5)
        elif namecmd == 'EVACC':
            evSet.EventMask[EventSetting.INDEX_ACCELERATION] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 6)
        elif namecmd == 'EVPWRON':
            evSet.EventMask[EventSetting.INDEX_POWER_ON] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 7)
        elif namecmd == 'EVPWROFF':
            evSet.EventMask[EventSetting.INDEX_POWER_OFF] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 8)
        elif namecmd == 'EVGSMON':
            evSet.EventMask[EventSetting.INDEX_GSM_FIND] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 9)
        elif namecmd == 'EVGSMOFF':
            evSet.EventMask[EventSetting.INDEX_GSM_LOST] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 10)
        elif namecmd == 'EVLOGF':
            evSet.EventMask[EventSetting.INDEX_LOG_FULL] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 11)
        elif namecmd == 'EVPHONE1':
            evSet.EventMask[EventSetting.INDEX_RING_PHONE1] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 12)
        elif namecmd == 'EVPHONE2':
            evSet.EventMask[EventSetting.INDEX_RING_PHONE2] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 13)
        elif namecmd == 'EVPHONE3':
            evSet.EventMask[EventSetting.INDEX_RING_PHONE3] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 14)
        elif namecmd == 'EVSENS':
            evSet.EventMask[EventSetting.INDEX_SENSORS] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 15)
        elif namecmd == 'EVON':
            evSet.EventMask[EventSetting.INDEX_ON] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 16)
        elif namecmd == 'EVRST':
            evSet.EventMask[EventSetting.INDEX_REBOOT] = int(valuecmd)
            self.setEvntCheck(eventtable, valuecmd, 17)
        else:
            raise Exception("setCmdRecordsEvents. Unknown command key:" + namecmd)
        return

    def SetTransportTtu(self, reqcmd, tServSett, tGprs, qtWidgetItem):
        try:
            evSetting = TransportSetting(-1)
            for cmd in reqcmd:
                buff = cmd.split(',')
                del buff[0]
                data = buff[len(buff) - 1].split("\x00")
                buff[len(buff) - 1] = data[0]
                for rec in buff:
                    array = rec.split('=')
                    self.setCmdRecordsTransport(array[0], array[1], tServSett, tGprs, qtWidgetItem, evSetting)
            tServSett.resizeColumnsToContents()
            tServSett.resizeRowsToContents()
            tGprs.resizeColumnsToContents()
            tGprs.resizeRowsToContents()
            return True, ''
        except Exception as err:
            return False, str(err)

    def SetTransportTtl(self, reqcmd, tServSett, tGprs, simUse, qtWidgetItem):
        try:
            evSetting = TransportSetting(simUse)
            for cmd in reqcmd:
                buff = cmd.split(',')
                del buff[0]
                data = buff[len(buff) - 1].split("\x00")
                buff[len(buff) - 1] = data[0]
                for rec in buff:
                    array = rec.split('=')
                    self.setCmdRecordsTransport(array[0], array[1], tServSett, tGprs, qtWidgetItem, evSetting)
            tServSett.resizeColumnsToContents()
            tServSett.resizeRowsToContents()
            tGprs.resizeColumnsToContents()
            tGprs.resizeRowsToContents()
            return True, ''
        except Exception as err:
            return False, str(err)

    def SetEventsTtl(self, reqcmd, eventtable, item):
        try:
            simUse = 0
            evSetting = EventSetting(simUse)
            for cmd in reqcmd:
                buff = cmd.split(',')
                del buff[0]
                data = buff[len(buff) - 1].split("\x00")
                buff[len(buff) - 1] = data[0]
                for rec in buff:
                    array = rec.split('=')
                    self.setCmdRecordsEvents(array[0], array[1], eventtable, item, evSetting)
            eventtable.resizeColumnsToContents()
            eventtable.resizeRowsToContents()
            return True, ''
        except Exception as err:
            return False, str(err)

    def SetEventsTtu(self, reqcmd, eventtable, item):
        try:
            evSetting = EventSetting(-1)
            for cmd in reqcmd:
                buff = cmd.split(',')
                del buff[0]
                data = buff[len(buff) - 1].split("\x00")
                buff[len(buff) - 1] = data[0]
                for rec in buff:
                    array = rec.split('=')
                    self.setCmdRecordsEvents(array[0], array[1], eventtable, item, evSetting)
            eventtable.resizeColumnsToContents()
            eventtable.resizeRowsToContents()
            return True, ''
        except Exception as err:
            return False, str(err)

    def setTableEvents(self, eventtable, initCall, checkbox, item):
        eventtable.setItem(0, 1, item(str(self.dataGridViewEvent.Rows[0].Cells["ColumnEventsValue"].Value)))
        eventtable.setItem(1, 1, item(str(self.dataGridViewEvent.Rows[1].Cells["ColumnEventsValue"].Value)))
        eventtable.setItem(2, 1, item(str(self.dataGridViewEvent.Rows[2].Cells["ColumnEventsValue"].Value)))
        eventtable.setItem(3, 1, item(str(self.dataGridViewEvent.Rows[3].Cells["ColumnEventsValue"].Value)))
        eventtable.setItem(4, 1, item(str(self.dataGridViewEvent.Rows[4].Cells["ColumnEventsValue"].Value)))
        eventtable.setItem(5, 1, item(str(self.dataGridViewEvent.Rows[5].Cells["ColumnEventsValue"].Value)))
        eventtable.setItem(6, 1, item(str(self.dataGridViewEvent.Rows[6].Cells["ColumnEventsValue"].Value)))

        for k in range(eventtable.rowCount()):
            eventtable.setCellWidget(k, 3, initCall(checkbox(), self.dataGridViewEvent.Rows[k].Cells["ColumnEventsCheckWritePoint"].Value))
            eventtable.setCellWidget(k, 4, initCall(checkbox(), self.dataGridViewEvent.Rows[k].Cells["ColumnEventsCheckSendToServer"].Value))
            eventtable.setCellWidget(k, 5, initCall(checkbox(), self.dataGridViewEvent.Rows[k].Cells["ColumnEventsCheckSendSMS"].Value))

        eventtable.resizeColumnsToContents()
        eventtable.resizeRowsToContents()
        return

    def readTableEvents(self, eventtable):
        self.dataGridViewEvent = ecs.dataGridViewEvent()
        self.dataGridViewEvent.Rows[0].Cells["ColumnEventsValue"].Value = int(eventtable.item(0, 1).text())
        self.dataGridViewEvent.Rows[1].Cells["ColumnEventsValue"].Value = int(eventtable.item(1, 1).text())
        self.dataGridViewEvent.Rows[2].Cells["ColumnEventsValue"].Value = int(eventtable.item(2, 1).text())
        self.dataGridViewEvent.Rows[3].Cells["ColumnEventsValue"].Value = int(eventtable.item(3, 1).text())
        self.dataGridViewEvent.Rows[4].Cells["ColumnEventsValue"].Value = int(eventtable.item(4, 1).text())
        self.dataGridViewEvent.Rows[5].Cells["ColumnEventsValue"].Value = int(eventtable.item(5, 1).text())
        self.dataGridViewEvent.Rows[6].Cells["ColumnEventsValue"].Value = int(eventtable.item(6, 1).text())

        for k in range(eventtable.rowCount()):
            mainWidget = eventtable.cellWidget(k, 3)
            checkBox = mainWidget.layout().itemAt(0).widget()
            self.dataGridViewEvent.Rows[k].Cells["ColumnEventsCheckWritePoint"].Value = checkBox.isChecked()
            mainWidget = eventtable.cellWidget(k, 4)
            checkBox = mainWidget.layout().itemAt(0).widget()
            self.dataGridViewEvent.Rows[k].Cells["ColumnEventsCheckSendToServer"].Value = checkBox.isChecked()
            mainWidget = eventtable.cellWidget(k, 5)
            checkBox = mainWidget.layout().itemAt(0).widget()
            self.dataGridViewEvent.Rows[k].Cells["ColumnEventsCheckSendSMS"].Value = checkBox.isChecked()
        return

    def GetRequesSensorSettingTtu(self, num):
        ret = ""
        sensor = self.typeSensTtu[num]
        number = self.numTypeSens[num]
        if sensor & 0xF0 == CODE_SENSTtus.CODE_DIN:
            ret = self.cmdStrings[cmdStrIDX.DIN] + ","
        elif sensor & 0xF0 == CODE_SENSTtus.CODE_AIN:
            ret = self.cmdStrings[cmdStrIDX.AIN] + str(number + 1) + ","
        elif sensor & 0xF0 == CODE_SENSTtus.CODE_CNT:
            ret = self.cmdStrings[cmdStrIDX.COUNT] + str(number + 1) + ","
        elif sensor & 0xF0 == CODE_SENSTtus.CODE_LLSTYPE:
            ret = self.cmdStrings[cmdStrIDX.RS485] + str(number + 1) + ","
        elif sensor & 0xF0 == CODE_SENSTtus.CODE_CAN:
            ret = self.cmdStrings[cmdStrIDX.CAN] + str(number + 1) + ","
        elif sensor & 0xF0 == CODE_SENSTtus.CODE_OWIRE:
            ret = self.cmdStrings[cmdStrIDX.OWIRE] + str(number + 1) + ","
        return ret

    def GetRequesSensorSettingTtl(self, num):
        ret = ""
        sensor = self.sensCells[num]
        number = int(sensor & 0x0F)
        value = int(sensor & 0xF0)
        if value == CODE_SENS.CODE_DIN:
            ret = self.paramStrings[int(paramStringsIDX.DIN)] + ","
        elif value == CODE_SENS.CODE_AIN:
            ret = self.paramStrings[int(paramStringsIDX.AIN)] + str(number + 1) + ","
        elif value == CODE_SENS.CODE_CNT:
            ret = self.paramStrings[int(paramStringsIDX.COUNT)] + str(number + 1) + ","
        elif value == CODE_SENS.CODE_FUEL:
            ret = self.paramStrings[int(paramStringsIDX.FUEL)] + str(number + 1) + ","
        elif value == CODE_SENS.CODE_RFID:
            ret = self.paramStrings[int(paramStringsIDX.RFID)] + str(number + 1) + ","
        return ret

    def ReadSensorsTTU(self, baseCmd):
        self.needTx = []
        cmd = baseCmd
        for i in range(0, 4, 1):
            cmd += self.cmdSensorsBaseTtu[i] + ","
        self.needTx.append(cmd)

        cmd = baseCmd
        cmd += self.cmdSensorsBaseTtu[4] + ","
        self.needTx.append(cmd)

        cmd = baseCmd
        for i in range(0, 5, 1):
            cmd += self.GetRequesSensorSettingTtu(i)
        self.needTx.append(cmd)

        cmd = baseCmd
        for i in range(5, 10, 1):
            cmd += self.GetRequesSensorSettingTtu(i)
        self.needTx.append(cmd)

        cmd = baseCmd
        for i in range(10, 13, 1):
            cmd += self.GetRequesSensorSettingTtu(i)
        self.needTx.append(cmd)

        cmd = baseCmd
        for i in range(13, 16, 1):
            cmd += self.GetRequesSensorSettingTtu(i)
        self.needTx.append(cmd)

        cmd = baseCmd + self.cmdStrings[cmdStrIDX.SMOUT] + "1,"
        self.needTx.append(cmd)
        return self.needTx, 206

    def ReadSensorsTTL(self, baseCmd):
        self.needTx = []
        cmd = baseCmd
        for i in range(0, 4, 1):
            cmd += self.cmdSensorsBaseTtl[i] + ","
        self.needTx.append(cmd)

        cmd = baseCmd
        for i in range(0, 5, 1):
            cmd += self.GetRequesSensorSettingTtl(i)
        self.needTx.append(cmd)

        cmd = baseCmd
        for i in range(5, 10, 1):
            cmd += self.GetRequesSensorSettingTtl(i)
        self.needTx.append(cmd)

        cmd = baseCmd
        for i in range(10, 16, 1):
            cmd += self.GetRequesSensorSettingTtl(i)
        self.needTx.append(cmd)
        return self.needTx, 106

    def SendTerminalCommand(self, command):
        return command, 108

    def ReadServiceTtu(self, baseCmd):
        self.needTx = []
        cmd = baseCmd + self.cmdService[0] + ","
        for i in range(1, 4, 1):
            cmd += self.cmdService[i] + ","
        self.needTx.append(cmd)
        return self.needTx, 204

    def ReadServiceTtl(self, baseCmd, add):
        self.needTx = []
        cmd = baseCmd + self.cmdService[0] + add + ","
        for i in range(1, 4, 1):
            cmd += self.cmdService[i] + ","
        self.needTx.append(cmd)
        return self.needTx, 204

    def ReadTransportTtu(self, baseCmd):
        self.needTx = []
        cmd = baseCmd
        for i in range(0, 4, 1):
            cmd += self.cmdTransportVal[i] + ","
        self.needTx.append(cmd)
        cmd = baseCmd
        for i in range(4, 8, 1):
            cmd += self.cmdTransportVal[i] + ","
        self.needTx.append(cmd)
        return self.needTx, 202

    def ReadTransportTtl(self, baseCmd, add):
        self.needTx = []
        cmd = baseCmd
        for i in range(0, 4, 1):
            cmd += self.cmdTransportVal[i] + add + ","
        self.needTx.append(cmd)
        cmd = baseCmd
        for i in range(4, 8, 1):
            cmd += self.cmdTransportVal[i] + ","
        self.needTx.append(cmd)
        return self.needTx, 102

    def ResetTracker(self, pincode):
        self.needTx = ["$RCSTT," + pincode + ",REBOOT,"]
        return self.needTx, 101

    def ReadEventsTtu(self, baseCmd):
        self.needTx = []
        cmd = baseCmd
        for i in range(0, 7, 1):
            cmd += self.cmdEventVal[i] + ","
        self.needTx.append(cmd)
        cmd = baseCmd
        for i in range(7, 16, 1):
            cmd += self.cmdEventVal[i] + ","
        self.needTx.append(cmd)
        cmd = baseCmd
        for i in range(16, 25, 1):
            cmd += self.cmdEventVal[i] + ","
        self.needTx.append(cmd)
        return self.needTx, 200

    def ReadEventsTtl(self, baseCmd, add):
        self.needTx = []
        cmd = baseCmd
        for i in range(0, 6, 1):
            cmd += self.cmdEventVal[i] + add + ","
        cmd += self.cmdEventVal[6] + ","
        self.needTx.append(cmd)
        cmd = baseCmd
        for i in range(7, 16, 1):
            cmd += self.cmdEventVal[i] + add + ","
        self.needTx.append(cmd)
        cmd = baseCmd
        for i in range(16, 25, 1):
            cmd += self.cmdEventVal[i] + add + ","
        self.needTx.append(cmd)
        return self.needTx, 100

def getingCommand(log, passw, ip):
    commandA1 = GenerateCommandA1(log, passw, ip)
    #return commandA1.GetA1Event()
    #return commandA1.GetA1()
    return commandA1.coderCommandA1Request(log)

def gettingRcstt(dev, pin, password, IP, port):
    commanda64 = "$RCSTT," + pin + ",SSERV=" + IP + ",SPORT=" + str(port) + ",SLOGIN=" + dev + ",SPASS=" + password
    return commanda64

def changeTimer1Timer2(pin, period1, period2):
    command64 = "$RCSTT," + pin + ",BTIM11=" + str(period1) + ",BTIM21=" + str(period2)
    return command64

def commandRebooting(pin):
    command64 = "$RCSTT," + pin + ",REBOOT"
    return command64
