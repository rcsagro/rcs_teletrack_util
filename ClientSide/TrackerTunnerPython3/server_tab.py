#!/usr/local/bin/python
# -*- coding: utf-8 -*-

class TelematServer(object):
    def __init__(self):
        self.telematic_server = ["Сервер", "Логин", "Пароль"]
        self.parameter_value = ["", "", ""]
        return

    @property
    def ParameterValue(self): return self.parameter_value

    @property
    def TelematicServer(self): return self.telematic_server

    def setParameterValue(self, value, index):
        self.parameter_value[index] = value
        return


class ServiceSettings(object):
    def __init__(self):
        self.setting_service = ["IP", "Порт", "Логин", "Пароль"]
        self.parameter_value = ["", "", "", "", ""]
        return

    @property
    def ServiceSett(self): return self.setting_service

    @property
    def ParameterValue(self): return self.parameter_value

    def setParameterValue(self, value, index):
        self.parameter_value[index] = value
        return