#!/usr/local/bin/python
# -*- coding: utf-8 -*-

class GprsTab(object):
    def __init__(self):
        self.name_regim = ["Имя доступа", "Режим", "Точка доступа GPRS", "Логин для GPRS", "Пароль для GPRS", "DNS сервер",
                           "Номер дозвона", "ISP логин", "ISP пароль"]
        self.value_regim = ["", "", "", "", "", "", "", "", ""]

        self.list_apn = []
        return

    @property
    def NameRegim(self): return self.name_regim

    @property
    def ValueRegim(self): return self.value_regim

    def setValueRegim(self, value, index):
        self.value_regim[index] = value
        return

    @property
    def listApn(self): return self.list_apn

    def setValueApn(self, object):
        name = object['name']
        k = 0
        while k < len(self.list_apn):
            if self.list_apn[k]['name'] == name:
                del self.list_apn[k]
                break
            k += 1
        self.list_apn.append(object)
        return