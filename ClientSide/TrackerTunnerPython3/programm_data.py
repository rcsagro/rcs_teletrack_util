#!/usr/local/bin/python3.8
# -*- coding: utf-8 -*-

class ProgrammData(object):
    def __init__(self):
        self.comboTypeTracker = "TTA"
        return

    @property
    def ComboTypeTracker(self): return self.comboTypeTracker

    def setComboTypeTracker(self, value):
        self.comboTypeTracker = value
        return