#!/usr/local/bin/python
# -*- coding: utf-8 -*-

class LabelsTab(object):
    def __init__(self):
        self.list_cell = []
        self.list_label = []
        self.marker = '0'
        self.table_new_label = []
        return

    @property
    def Cells(self): return self.list_cell

    def setCells(self, value): self.list_cell = value

    @property
    def Labels(self): return self.list_label

    def setLabels(self, value): self.list_label = value

    @property
    def Marker(self): return self.marker

    def setMarker(self, value): self.marker = value

    @property
    def TableNewLabel(self): return self.table_new_label

    def setTableNewLabel(self, value): self.table_new_label = value
