# coding: utf-8

from cx_Freeze import setup, Executable

executables = [Executable('tracker_tunner_main.py', targetName='Tracker_Tunner.exe', base='Win32GUI')]

excludes = ['unicodedata', 'logging', 'unittest', 'email', 'html', 'http', 'urllib',
            'xml', 'bz2']

options = {
    'build_exe': {
        'include_msvcr': True, 'build_exe': 'build_windows'
    }
}

setup(name='Tracker_tunner',
      version='1.0.0',
      description='TrackerTunner!',
      executables=executables)