from sshtunnel import SSHTunnelForwarder
from pymongo import MongoReplicaSetClient

import psycopg2
import pandas
import traceback
import pymongo
import socket

# ssh variables
ssh_host = ''
ssh_port = 0
ssh_username = ''
ssh_password = ''

# PostgreSql database variables
postgresql_conn = ''
postgresql_host = ''
postgresql_port = 0
postgre_db_name = ''
postgresql_user = ''
postgresql_password = ''
postgres_ssh = ''
postgres_local_host = ''
postgres_local_port = ''

socket_server_address = ''

# MongoDb database variable
mongo_conn = ''
mongo_host = ''
mongo_port = 0
mongo_replica_name = ''
is_using_replica = ''
mongo_db_name = ''
mongo_user = ''
mongo_password = ''
mongo_ssh = ''
mongo_local_host = ''
mongo_local_port = ''

host_redis_conn = ''
host_redis = ''
port_redis = 0
db_redis = 0

gmongo_client = ''
gserver = ''
pserver = ''
pconn = ''

def RebootConfigIni():
    global ssh_host
    global ssh_port
    global ssh_username
    global ssh_password

    global socket_server_address

    global postgresql_conn
    global postgresql_host
    global postgresql_port
    global postgre_db_name
    global postgresql_user
    global postgresql_password
    global postgres_ssh
    global postgres_ssh
    global postgres_local_host
    global postgres_local_port

    global mongo_conn
    global mongo_host
    global mongo_port
    global mongo_replica_name
    global is_using_replica
    global mongo_db_name
    global mongo_user
    global mongo_password
    global mongo_ssh
    global mongo_local_host
    global mongo_local_port

    # ssh variables
    ssh_host = '217.23.1.57'
    ssh_port = 223
    ssh_username = 'aleksandr'
    ssh_password = 'yak2ns8BK'

    socket_server_address = 'sock.teletrack.ua'

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    local_ip = s.getsockname()[0]
    s.close()

    # PostgreSql database variables
    postgresql_conn = 'postgresql://postgres:tk99tk99@10.21.0.31:5432/rcs_web_ent'
    postgresql_host = '10.21.0.31'
    postgresql_port = 5432
    postgre_db_name = 'rcs_web_ent'
    postgresql_user = 'postgres'
    postgresql_password = 'tk99tk99'
    postgres_ssh_ = 'yes'
    if postgres_ssh_ == 'no':
        postgres_ssh = False
    else:
        postgres_ssh = True
    postgres_local_host = local_ip
    postgres_local_port = 5433

    # MongoDb database variable
    mongo_conn = 'mongodb://dev:user123@127.0.0.1:27017/rcs_web_ent'
    mongo_host = '10.21.0.30'
    mongo_port = 27017
    mongo_replica_name = 'main'
    is_using_replika_ = 'no'
    if is_using_replika_ == 'no':
        is_using_replica = False
    else:
        is_using_replica = True
    mongo_db_name = 'rcs_web_ent'
    mongo_user = 'dev'
    mongo_password = 'user123'
    mongo_ssh_ = 'yes'

    if  mongo_ssh_ == 'no':
        mongo_ssh = False
    else:
        mongo_ssh = True
    mongo_local_host = local_ip
    mongo_local_port = 27018
    return

def sshPostgreSqlExecute(q):
    global postgres_local_port
    try:
        server = SSHTunnelForwarder((ssh_host, ssh_port), ssh_username=ssh_username, ssh_password=ssh_password,
                                    remote_bind_address=(postgresql_host, postgresql_port),
                                    local_bind_address=(postgres_local_host, postgres_local_port))
        server.start()
        conn = psycopg2.connect(database=postgre_db_name, user=postgresql_user, password=postgresql_password,
                                host=postgres_local_host, port=server.local_bind_port)
        cursor = conn.cursor()
        cursor.execute(q)
        conn.commit()
        cursor.close()
        conn.close()
        return True
    except Exception as err:
        print(str(err))
        print(traceback.format_exc())
        return False

def queryFromPostgreSql(q):
    if postgres_ssh == False:
        result = queryPostgreSql(q)
        return result
    elif postgres_ssh == True:
        result = sshQueryPostgreSql(q)
        return result
    else:
        return []

def sshQueryPostgreSql(q):
    global postgres_local_port
    try:
        server = SSHTunnelForwarder((ssh_host, ssh_port), ssh_username=ssh_username, ssh_password=ssh_password,
                                remote_bind_address=(postgresql_host, postgresql_port),
                                local_bind_address=(postgres_local_host, postgres_local_port))
        server.start()

        conn = psycopg2.connect(database=postgre_db_name, user=postgresql_user, password=postgresql_password,
                            host=postgres_local_host, port=server.local_bind_port)

        result = pandas.read_sql_query(q, conn)
        conn.close()
        server.stop()
        return result
    except Exception as err:
        print(str(err))
        print(traceback.format_exc())
        return []

def getPostgreSqlConn():
    global pserver
    global pconn
    pconn = psycopg2.connect(database=postgre_db_name, user=postgresql_user, password=postgresql_password,
                            host=postgresql_host, port=postgresql_port)
    return pconn

def getSshPostgreSqlConn():
    global pserver
    global pconn
    pserver = SSHTunnelForwarder((ssh_host, ssh_port), ssh_username=ssh_username, ssh_password=ssh_password,
                                remote_bind_address=(postgresql_host, postgresql_port),
                                local_bind_address=(postgres_local_host, postgres_local_port))
    pserver.start()
    pconn = psycopg2.connect(database=postgre_db_name, user=postgresql_user, password=postgresql_password,
                            host=postgres_local_host, port=pserver.local_bind_port)
    return pconn

def postgreSqlExecute(q):
    try:
        conn = psycopg2.connect(database=postgre_db_name, user=postgresql_user, password=postgresql_password,
                                host=postgres_local_host, port=postgres_local_port)
        cursor = conn.cursor()
        cursor.execute(q)
        conn.commit()
        cursor.close()
        conn.close()
        return True
    except Exception as err:
        print(str(err))
        print(traceback.format_exc())
        return False

def queryPostgreSql(q):
    try:
        conn = psycopg2.connect(database=postgre_db_name, user=postgresql_user, password=postgresql_password,
                                host=postgresql_host, port=postgresql_port)
        result = pandas.read_sql_query(q, conn)
        conn.close()
        return result
    except Exception as err:
        print( str(err) )
        print( traceback.format_exc() )
        return []

def sshQueryMongoDb(collection, query='{}'):
    global mongo_local_port
    try:
        with SSHTunnelForwarder(
            (ssh_host, ssh_port),
            ssh_username=ssh_username,
            ssh_password=ssh_password,
            remote_bind_address=(mongo_host, mongo_port),
            local_bind_address=(mongo_local_host, mongo_local_port)) as server:
            server.start()

            if is_using_replica:
                client = MongoReplicaSetClient(mongo_local_host, server.local_bind_port, replicaSet=mongo_replica_name)
            else:
                client = pymongo.MongoClient(mongo_local_host, server.local_bind_port)

            db = client[mongo_db_name]
            db.authenticate(name=mongo_user, password=mongo_password)
            collection = db.get_collection(collection)
            if query == '{}':
                curr = collection.find()
            else:
                curr = collection.find(query)
            listdata = []
            for doc in curr:
                listdata.append(doc)
            client.close()
            server.stop()
        return listdata
    except Exception as err:
        print( str(err) )
        print( traceback.format_exc() )
        return None

def sshGetMongoDb():
    global gmongo_client
    global gserver
    try:
        gserver = SSHTunnelForwarder(
            (ssh_host, ssh_port),
            ssh_username=ssh_username,
            ssh_password=ssh_password,
            remote_bind_address=(mongo_host, mongo_port),
            local_bind_address=(mongo_local_host, mongo_local_port))

        gserver.start()

        if is_using_replica:
            gmongo_client = MongoReplicaSetClient(mongo_local_host, gserver.local_bind_port, replicaSet=mongo_replica_name)
        else:
            gmongo_client = pymongo.MongoClient(mongo_local_host, gserver.local_bind_port)

        db = gmongo_client[mongo_db_name]
        db.authenticate(name=mongo_user, password=mongo_password)

        return db
    except Exception as err:
        print( str(err) )
        print( traceback.format_exc() )
        return []

def queryMongoDb(collection, query='{}'):
    try:
        if is_using_replica:
            client = MongoReplicaSetClient(mongo_conn, replicaSet=mongo_replica_name)
        else:
            client = MongoReplicaSetClient(mongo_conn)

        db = client[mongo_db_name]
        collection = db.get_collection(collection)
        if query == '{}':
            curr = collection.find()
        else:
            curr = collection.find({query})
        listdata = []
        for doc in curr:
            listdata.append(doc)
        client.close()
        return listdata
    except Exception as err:
        print( str(err) )
        print( traceback.format_exc() )
        return []

def getMongoDb():
    global gmongo_client
    try:
        if is_using_replica:
            gmongo_client = MongoReplicaSetClient(mongo_conn, replicaSet=mongo_replica_name)
        else:
            gmongo_client = MongoReplicaSetClient(mongo_conn)

        db = gmongo_client[mongo_db_name]
        db.authenticate(name=mongo_user, password=mongo_password)
        return db
    except Exception as err:
        print( str(err) )
        print( traceback.format_exc() )
        return []

def save_records_to_mongodb(dataStruct, mongodb, mongoNameDb):
    try:
        numRecords = len(dataStruct)
        if numRecords == 0:
            return -1
        mongodb[mongoNameDb].parseData.write_concern._WriteConcern__acknowledged = False
        bulkop = mongodb[mongoNameDb].parseData.initialize_unordered_bulk_op()
        for ls in dataStruct:
            bulkop.insert(ls)
        bulkop.execute()
        if bulkop.execute:
            return True
        else:
            return False
    except Exception as err:
        print( str(err) )
        print( traceback.format_exc() )
        return False

def getMongoDb():
    if mongo_ssh == 'no':
        gMongoDb = getMongoDb()
    else:
        gMongoDb = sshGetMongoDb()
    return gMongoDb

def closeMongoDb():
    global gmongo_client
    global gserver
    try:
        if mongo_ssh == 'no':
            gmongo_client.close()
        else:
            gmongo_client.close()
            gserver.close()
    except Exception:
        pass

# end this file


