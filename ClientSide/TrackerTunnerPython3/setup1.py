# coding: utf-8

from cx_Freeze import setup, Executable

executables = [Executable('tracker_tunner_main.py')]

setup(name='Tracker_tunner',
      version='1.0.0',
      description='TrackerTunner!',
      executables=executables)