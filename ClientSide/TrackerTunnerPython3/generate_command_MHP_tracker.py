import time
import datetime
import sys
import sshtunel
import traceback
import coder_command_A1 as comA1

clientName = 'Ширяев О.С.'
IP = 'sock.teletrack.ua'
PORT64 = 9008
PORT = 9009

arrayFirst = ['1','2','3','4','5','6','7','8','9','0','q','a','z','w','s','x','e','d','c','r','f','v','t','g','b','y','h','n','u','j','i','k','o']
arraySecond = ['q','w','e','r','t','y','u','i','o','p','w','s','x','e','d','c','r','f','v','t','g','b','y','h','n','u','j','m','i','k','o','l','p']

def showError(msg, msg1, msg2):
    print(msg + "\n" + msg1 + "\n" + msg2)
    return

def getDeviceClients(id_name):
    postgres_ssh = sshtunel.postgres_ssh
    qdev = 'SELECT "devices"."Id","devices"."Identifier","devices"."ProtocolId","objects"."Id" AS "ObjId" FROM "devices","objects" WHERE "devices"."Id" = "objects"."DeviceId" AND "devices"."Parent" = ' + "'" + id_name + "'"
    if postgres_ssh == 'no':
        packageDevices = sshtunel.queryPostgreSql(qdev)
    else:
        packageDevices = sshtunel.sshQueryPostgreSql(qdev)

    if len(packageDevices) <= 0:
        showError("getDeviceClients", "Not get data devices from PostgreSql", str(traceback.format_exc()))
        return ''
    return packageDevices

def getIdClients(name):
    mongo_ssh = sshtunel.mongo_ssh

    if mongo_ssh == 'no':
        clientos = sshtunel.queryMongoDb('clients', {'short_name':name})
    else:
        clientos = sshtunel.sshQueryMongoDb('clients', {'short_name':name})

    if len(clientos) == 0:
        showError("Can not gets data clients for MHP", '', '')
        return ''
    return str(clientos[0]["_id"])

def getDataProtocols():
    mongo_ssh = sshtunel.mongo_ssh

    if mongo_ssh == 'no':
        protocol = sshtunel.queryMongoDb('protocol', '')
    else:
        protocol = sshtunel.sshQueryMongoDb('protocol', '')

    if (protocol) == 0:
        showError("Can not get data for protocols!", "", "")
        return ''
    nameProtocol = []
    idProtocol = []
    for ls in protocol:
        nameProtocol.append(ls["protocol"]["protocolName"])
        idProtocol.append(str(ls["_id"]))
    return nameProtocol, idProtocol

def generatePassw(logid):
    try:
        code = ''
        logdev = logid.lower()
        for k in logdev:
            try:
                indx = arrayFirst.index(k)
                char = arraySecond[indx]
            except Exception as err:
                showError("Not valid symbol in login for tracker ", str(logid), str(traceback.extract_stack()))
                return ''
            code += char
        strcode = code.capitalize()
        last_symbol = strcode[3].title()
        password = strcode[0:3] + last_symbol + logid
        return password
    except Exception as err:
        showError(str(err), '', str(traceback.extract_stack()))
        return ''

if __name__ == "__main__":
    start = time.time()
    idclient = getIdClients(clientName)
    #devices = ['9D2B','9D8D','9D8C','E3D6','A23D','B7BD','E9AD','A7C5','A1D9','2CEF','A1DB','A1EA',
    #           'A20B','A26B','A82D','A24A','72B0','A1E1','E9B0','29F4','9D9A','E92A','A7C6','A1DE','A1E9','A20A','A26C',
    #           'E20D','A1D5','A23E','A1E2','A1D4','A1ED','14F8','B86D','E2AA','A83A','16AF','9D93','9D25','73D2',
    #           '9D27','29F0','29F7','168F','A43E','CDD6','72B3','A22D','A43C','73D5','A31D','4AFF','A22E','730E','A43D',
    #           '72B1','72AE','165F','9D91','A20C','E21B','E9B2','E2AC','A7D8','9D23','9D26','9D97','9D94','9D98',
    #           'A1DD','16DF','71EA','71D7','717A','72AB','10F7','72B4','73D7','717C','716E','2CDF','71EE','72AD','717B',
    #           '7DB3','76B1','A1D3','A1D7','E9AE','E9B1','166F','9D8E','9DA0','9D9C','E21B','E25C','2CF4','EA01',
    #           'A1D8','A23B','A23C','B8BE','72B5','73DA','73D5','A1EC','14F7','72AB','E9EA','716D','A1E0','E3DD','E20B',
    #           'E25E','E3DA','E9EE','29F1','10FC','E6D4','16CF','E9E7','16BF','EA05','EA00','E20A','E25D','E9E9',
    #           '14F9','2CF6','A1E8','2CAF','2C8F','EA07','E9E8','EA06','E9E5','EA04','E3D7','E91E','A23A','EA03','2CF0',
    #           'E3DE','29F3','164F','2C3F','E9EB','E9E6','2C9F','E20C','167F','EA02','E9ED','2CF3','E3DB','E9EC','E3E0',
    #           'E9E4','E3D8','2CCF','2CF5','2C2F','29F2','2CBF','E21A','E3D9']

    #devices = ['B7BD', 'A1EA', 'A7C6', 'A1E9', 'A26C', 'E20D', 'A1D5', 'A1E2', '14F8', 'B86D', '9D93', '9D25', '9D27', 'A43E',
    # '73D5', '730E', 'A43D', '9D91', '9D97', '9D94', 'A1DD', '71EA', '71D7', '10F7', '72B4', '73D7', '72AD', '7DB3',
    # '76B1', 'A1D3', '9D8E', '9DA0', '72B5', '73D5', 'E3DD', 'E20B', 'E25E', 'E3DA', '29F1', '16CF', 'E9E7', '16BF',
    # 'EA00', 'E20A', '14F9', '2CF6', 'A1E8', 'EA07', 'EA06', 'E9E5', 'EA04', 'E3D7', 'E91E', 'A23A', '29F3', '164F',
    # 'E9E6', '2C9F', 'E20C', '167F', 'E9ED', 'E3DB', 'E9EC', 'E3E0', 'E3D8', '2CCF', '29F2', '2CBF', 'E21A', 'E3D9']

    #devices = ['A26C', 'A1D5', 'B86D', '9D93', '9D25', '9D27', 'A43E', '730E', 'A43D', '9D91', '9D97', 'A1DD', '71EA', '10F7', '72AD', 'A1D3', '9D8E', '9DA0', 'E3DD', 'E20B', '29F1', 'E9E7', '16BF', 'EA00', '164F', 'E9ED', 'E9EC', 'E3E0', 'E3D8', '2CCF', '2CBF']

    #devices = ['A26C', 'A1D5', 'B86D', '9D93', '9D25', '9D27', 'A43E', '730E', 'A43D', '9D91', '9D97', '71EA', '10F7', '72AD', 'A1D3', '9D8E', '9DA0', 'E20B', 'E9E7', 'EA00', 'E9ED', 'E3E0', 'E3D8', '2CCF', '2CBF']

    #devices = ['4F5A','7CFC','7CF8','7CFD','7CFF','DE54','DE59','DE60'] 18.08.2020

    #devices = ['D3B5','D3B6', 'D38D']

    devices = ['6FDE']

    numberdev = len(devices)
    protocols = getDataProtocols()
    commandTable = []
    listdevi = []
    print("Begin generate records for trackers")
    i = 0
    commandA1 = comA1.GenerateCommandA1('', '', '')

    try:
        for ls in devices:
            record = {}
            try:
                try:
                    nameproto = 'teletrack64' # 'teletrack64'
                except Exception as err:
                    nameproto = ''
                password = generatePassw(ls)
                if password == '':
                    continue
                if nameproto == 'teletrack32':
                    command = commandA1.coderCommandA1Request(ls, password, IP)
                elif nameproto == 'teletrack64':
                    #command = comA1.gettingRcstt(ls, '1234', password, IP, PORT64)
                    #command = comA1.changeTimer1Timer2('1234', 60, 60)
                    command = comA1.commandRebooting('1234')
                else:
                    command = ''
                    continue
                if command == '':
                    continue

                record["client"] = clientName
                record["id_client"] = idclient
                record["codeResponse"] = ''
                record["Command"] = command
                record["Comment"] = "Command for " + clientName
                record["countTry"] = int(1)
                record["dataResponse"] = ''
                record["dateCreate"] = str(datetime.datetime.now())
                record["dateReade"] = ''
                record["dateResponse"] = ''
                record["levelCommand"] = 1
                record["Device"] = ls
                record["deviceId"] = 1
                record["modifyDate"] = str(datetime.datetime.now())
                record["numberTry"] = int(2)
                record["objectId"] = 2
                record["Protocol"] = nameproto
                record["Status"] = 'wait'
                record["statusCommand"] = 'new'
                record["UtcCreate"] = time.time()
                record["UtcRead"] = 0
                record["UtcResponse"] = 0
                record["Type"] = 'read'
                commandTable.append(record)

                listdevi.append(ls)
                print("Devices:" + ls + " Command:" + command)
            except Exception as err:
                showError("Have exception: " + str(err), str(ls), str(traceback.extract_stack()))
                continue
            i += 1
    except Exception as err:
        showError(str(err), '', str(traceback.extract_stack()))

    mongo_ssh = sshtunel.mongo_ssh

    if mongo_ssh == 'no':
        gMongo = sshtunel.getMongoDb()
    else:
        gMongo = sshtunel.sshGetMongoDb()

    if mongo_ssh == 'no':
        collection = gMongo['rcs_web_ent'].get_collection('commandData')
    else:
        collection = gMongo[2]['rcs_web_ent'].get_collection('commandData')

    command_array = list(collection.find({}))
    print("Number " + str(len(command_array)) + " commands reading")

    if len(commandTable) > 0:
        try:
            for ls in commandTable:
                collection.insert(ls)
        except Exception as err:
            showError("Have excaption: ", str(err), "")
            showError("", "", str(traceback.extract_stack()))
            showError("Have excaption. ", str(err), str(traceback.extract_stack()))

        file = open("list_devices.txt", 'w')
        for ls in listdevi:
            file.write(ls + "\n")
        file.close()

    #====== monitoring requst data ===================
    print("Start listen echo for trackers")
    eventcommand = ''
    while len(devices) > 0:
        result = list(collection.find({}))
        for i,ls in enumerate(result):
            try:
                if ls["Status"] == 'done' and ls["statusCommand"] == 'success' and ls["levelCommand"] == 1:
                    if ls["Device"] in devices:
                        if len(ls["dataResponse"]) > 0:
                            collection.update({'_id':ls["_id"]}, {"$set":{"Status":'processed end'}})

                            commanda = ls["dataResponse"] # read config command
                            commandA1.timerThreadRx_Tick(commanda) # unpack this
                            commandA1.setEventTimerPeriod(60, 60, False, True) # modified this
                            commandA1.setEventSensorsToServer(True) # modified this
                            eventcommand = commandA1.coderCommandA1Event(ls["Device"], '', '') # get new command
                            ls["codeResponse"] = ''
                            ls["Command"] = eventcommand
                            ls["countTry"] = int(0)
                            ls["dataResponse"] = ''
                            ls["dateCreate"] = str(datetime.datetime.now())
                            ls["dateReade"] = ''
                            ls["dateResponse"] = ''
                            ls["levelCommand"] = 2
                            ls["modifyDate"] = str(datetime.datetime.now())
                            ls["numberTry"] = int(1)
                            ls["Status"] = 'wait'
                            ls["statusCommand"] = 'new'
                            ls["UtcCreate"] = time.time()
                            del ls["_id"]
                            collection.insert(ls)
                            print("Processing is " + ls["Device"] + " command " + eventcommand)
                            index = devices.index(ls["Device"])
                            del devices[index]
                            print("Begin processing:" + str(devices))
                            print("Number all devices:" + str(numberdev))
                            print("Number begin processing devices:" + str(len(devices)))
                            print("Number close processing devices:" + str(numberdev - len(devices)))
            except Exception as er:
                print(str(er))
        time.sleep(60)
        print("Go to next step...")
    #=================================================

    if mongo_ssh == 'no':
        gMongo.close()
    else:
        gMongo[2].close()
        gMongo[1].stop()

    end = time.time()
    print("Working with ", round(end - start, 2), " seconds")
    sys.exit(0)