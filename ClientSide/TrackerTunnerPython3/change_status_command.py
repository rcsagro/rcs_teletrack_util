import sshtunel

name = "change_trackers8"

devices = []
with open ("list_devices.txt", "r") as myfile:
    devices = myfile.readlines()

if len(devices) == 0:
    exit(0)

mongo_ssh = sshtunel.mongo_ssh

if mongo_ssh == 'no':
    gMongo = sshtunel.getMongoDb()
else:
    gMongo = sshtunel.sshGetMongoDb()

collection = gMongo[0].get_collection('commandData')

NUMBER_DEVICE = 1
changedev = []
k = 0
while k < NUMBER_DEVICE:
    dev = devices[0].split('\n')
    collection.update({"Device":dev[0]}, {"$set":{"Status":'wait'}})
    changedev.append(dev[0])
    del devices[0]
    k += 1

gMongo[1].close()

file1 = open("list_devices.txt", 'w')
for ls in devices:
    sd = ls.split("\n")
    file1.write(sd[0] + "\n")
file1.close()

file2 = open(name + ".txt", 'w')
for lk in changedev:
    file2.write(lk + "\n")
file2.close()

print("Change dev:", str(changedev))
