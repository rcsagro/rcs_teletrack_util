#!/usr/local/bin/python
# -*- coding: utf-8 -*-

class TerminalTab(object):
    def __init__(self):
        self.comm_term = ["", "$RCSTT,1234, DEBUG=55"]
        self.line_term_data = ''
        self.check_hex_mode = False
        self.text_term = ""
        return

    @property
    def TextTerm(self): return self.text_term

    @TextTerm.setter
    def TextTerm(self, value): self.text_term = value

    @property
    def CommTerm(self): return self.comm_term

    @CommTerm.setter
    def CommTerm(self, value):
        self.comm_term = value

    @property
    def LineTermData(self): return self.line_term_data

    @LineTermData.setter
    def LineTermData(self, value): self.line_term_data = value

    @property
    def HexModeTerm(self): return self.check_hex_mode

    @HexModeTerm.setter
    def HexModeTerm(self, value): self.check_hex_mode = value

