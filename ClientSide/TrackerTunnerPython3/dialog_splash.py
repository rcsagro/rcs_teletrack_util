import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class ScreenArea(QWidget):
    def __init__(self, title, msg):
        app = QApplication(sys.argv)
        super().__init__()
        w = QWidget()
        w.setWindowTitle(title)
        w.setWindowIcon(QIcon('web.png'))
        w.setGeometry(300, 300, 450, 50)
        w.setWindowFlag(Qt.WindowStaysOnTopHint)
        w.setWindowFlags(Qt.WindowCloseButtonHint)
        self.screen = app.primaryScreen()
        self.size = self.screen.size()
        self.label = QLabel(msg, self)
        self.label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setStyleSheet("border: 1px solid black;")
        self.myFont = QFont()
        self.myFont.setBold(True)
        self.label.setFont(self.myFont)
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.label)
        w.setLayout(self.layout)
        width = w.frameGeometry().width()
        height = w.frameGeometry().height()
        w.setGeometry(self.size.width() / 2 - width / 2, self.size.height() / 2 - height / 2, width, height)
        w.show()
        sys.exit(app.exec_())
        return

def startDialogSplash(title, msg):
    wd = ScreenArea(title, msg)
    return