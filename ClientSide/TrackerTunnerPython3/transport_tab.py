#!/usr/local/bin/python
# -*- coding: utf-8 -*-

class OperatorSetting(object):
    def __init__(self):
        self.parameter_value = ''
        self.list_operator = []
        return

    def setValueOper(self, obj):
        self.list_operator.append(obj)
        return

    def clearList(self):
        self.list_operator.clear()

    @property
    def listOper(self): return self.list_operator

    @property
    def ParameterValue(self): return self.parameter_value

    @ParameterValue.setter
    def ParameterValue(self, value): self.parameter_value = value

class GprsSettsP(object):
    def __init__(self):
        self.gprs_setting = ["Имя точки доступа", "Логин GPRS", "Пароль GPRS", "DNS сервер"]
        self.parameter_value = ["", "", "", ""]
        return

    @property
    def GprsSetting(self): return self.gprs_setting

    @property
    def ParameterValue(self): return self.parameter_value

    def setParameterValue(self, value, index):
        self.parameter_value[index] = value
        return

class GprsSetts(object):
    def __init__(self):
        self.gprs_setting = ["Имя точки доступа", "Логин GPRS", "Пароль GPRS", "DNS сервер"]
        self.parameter_value = ["", "", "", ""]
        return

    @property
    def GprsSetting(self): return self.gprs_setting

    @property
    def ParameterValue(self): return self.parameter_value

    def setParameterValue(self, value, index):
        self.parameter_value[index] = value
        return

class ServerSettings(object):
    def __init__(self):
        self.setting_server = ["Сервер", "Порт", "Логин", "Пароль"]
        self.parameter_value = ["", "", "", ""]
        return

    @property
    def ServerSettn(self): return self.setting_server

    @property
    def ParameterValue(self): return self.parameter_value

    def setParameterValue(self, value, index):
        self.parameter_value[index] = value
        return