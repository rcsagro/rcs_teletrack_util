#!/usr/local/bin/python
# -*- coding: utf-8 -*-

class ServPhone(object):
    def __init__(self):
        self.service_phone = ["Legal phone 1", "Legal phone 2", "Legal phone 3"]
        self.line_req = "30"
        self.parameter_value = ["", "", ""]
        return

    @property
    def ServicePhone(self): return self.service_phone

    @property
    def ParameterValue(self): return self.parameter_value

    def setParameterValue(self, value, index):
        self.parameter_value[index] = value
        return

    @property
    def ConnectRegim(self): return self.connect_regim

    @property
    def LineReq(self): return self.line_req

    @LineReq.setter
    def LineReq(self, value): self.line_req = value

class ServPhoneP(object):
    def __init__(self):
        self.service_phone = ["Legal phone 4", "Legal phone 5", "Legal phone 6"]
        self.line_req = "60"
        self.parameter_value = ["", "", ""]
        return

    @property
    def ServicePhone(self): return self.service_phone

    @property
    def ParameterValue(self): return self.parameter_value

    def setParameterValue(self, value, index):
        self.parameter_value[index] = value
        return

    @property
    def ConnectRegim(self): return self.connect_regim

    @property
    def LineReq(self): return self.line_req

    @LineReq.setter
    def LineReq(self, value): self.line_req = value