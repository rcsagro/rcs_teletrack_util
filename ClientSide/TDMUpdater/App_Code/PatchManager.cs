using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using MySql.Data.MySqlClient;
using TDataManagerPath.Properties;

namespace TrackControl.DbPatch
{
    public class PatchManager
    {
        #region Fields

        /// <summary>
        /// ������� ����� ����������
        /// </summary>
        private MainForm _mainForm;

        /// <summary>
        /// ������ �������� �������� ��� �����.
        /// </summary>
        private Dictionary<string, string> _scriptInfo;

        /// <summary>
        /// ������ �������������
        /// </summary>
        private string _version;

        /// <summary>
        /// ������ �������� �������� ��� ����� ��� MS SQL
        /// </summary>
        private Dictionary<string, string> _scriptInfoMs;

        public const int MY_SQL_WORK = 0;
        public const int MS_SQL_WORK = 1;
        public static int TypeDataBase = MY_SQL_WORK;

        private int scriptCount = 0;
        private int scriptCountMs = 0;

        private Controls.Progress _progress;
        private Controls.Progress _progressMs;

        #endregion

        private delegate void startPatchDelegate(string serverName, string dbName, string login, string password, string versionTdm);

        #region .ctor

        public PatchManager(MainForm mainForm)
        {
            _scriptInfo = new Dictionary<string, string>();
            _scriptInfoMs = new Dictionary<string, string>(); // for MS SQL

            fetchSriptInfo();
            fetchSriptInfoMs(); // for MS SQL

            scriptCount = _scriptInfo.Count;
            scriptCountMs = _scriptInfoMs.Count; // for MS SQL

            _progress = new Controls.Progress(scriptCount);
            _progressMs = new Controls.Progress( scriptCountMs ); // for MS SQL

            _mainForm = mainForm;

            if (GetScriptRegime() == "AGRO")
            {
                _mainForm.Text = "AGRO PATCH";
            }

            initView();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// ��������� ������� ����������� � ��������.
        /// </summary>
        private void fetchSriptInfo()
        {
            string sStream = "TDataManagerPath.Scripts.xml";
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(sStream))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(stream);
                XmlNodeList nodes = doc.SelectNodes("/patch/script");
                foreach (XmlNode node in nodes)
                {
                    _scriptInfo.Add(node["name"].InnerText, node["description"].InnerText);
                }
            }
        }

        /// <summary>
        /// ��������� ������� ����������� � �������� ��� MS SQL
        /// </summary>
        private void fetchSriptInfoMs()
        {
            string strXml = Resources.ScriptsMS;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(strXml);
            XmlNodeList nodes = doc.SelectNodes("/patch/script");

            foreach (XmlNode node in nodes)
            {
                _scriptInfoMs.Add(node["name"].InnerText, node["description"].InnerText);
            }
        } // fetchSriptInfoMs

        /// <summary>
        /// ��������� ���������� � ��������� �� MS SQL ��� ���������������
        /// ��������������� �����������.
        /// </summary>
        /// <param name="serverName">��� ������� MS SQL</param>
        /// <param name="dbName">�������� ���� ������</param>
        /// <param name="login">����� �������������� ��</param>
        /// <param name="password">������ �������������� ��</param>
        /// <returns></returns>
        private bool checkConnectionMs(string serverName, string dbName, string login, string password)
        {
            string connectionString = "";

            if (login.Length == 0)
            {
                connectionString = String.Format("Server={0};database={1};trusted_connection=true;",
                                                 serverName, dbName);
            }
            else
            {
                connectionString = String.Format("Server={0};database={1};user id={2};password={3}",
                                                 serverName, dbName, login, password);
            }


            bool result = true;
            SqlConnection conn = new SqlConnection(connectionString);

            try
            {
                conn.Open();
                conn.Close();
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        } //  checkConnectionMs

        /// <summary>
        /// ��������� ���������� � ��������� �� ��� ���������������
        /// ��������������� �����������.
        /// </summary>
        /// <param name="serverName">��� ������� MySQL</param>
        /// <param name="dbName">�������� ���� ������</param>
        /// <param name="login">����� �������������� ��</param>
        /// <param name="password">������ �������������� ��</param>
        /// <returns></returns>
        private bool checkConnection(string serverName, string dbName, string login, string password)
        {
            string connectionString = String.Format(
                "server={0};database={1};user id={2};password={3}",
                serverName, dbName, login, password);

            bool result = true;
            MySqlConnection conn = new MySqlConnection(connectionString);

            try
            {
                conn.Open();
                conn.Close();
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// ���������, ���������� �� � ������������ ���� ��� ������������� ����� ��� MS SQL
        /// </summary>
        /// <param name="serverName">��� ������� MS SQL</param>
        /// <param name="dbName">�������� ���� ������</param>
        /// <param name="login">����� �������������� ��</param>
        /// <param name="password">������ �������������� ��</param>
        /// <returns></returns>
        private bool checkPrivilegesMs(string serverName, string dbName, string login, string password)
        {
            string connectionString = "";

            if (login.Length == 0)
            {
                connectionString = String.Format("Server={0};database={1};trusted_connection=true;",
                                                 serverName, dbName);
            }
            else
            {
                connectionString = String.Format("Server={0};database={1};user id={2};password={3}",
                                                 serverName, dbName, login, password);
            }

            //string host = ( "(local)" == serverName ) ? serverName : "%";
            string script = String.Format(@"SELECT has_perms_by_name(NULL, 'database', 'update'),
has_perms_by_name(NULL, 'database', 'select'),
has_perms_by_name(NULL, 'database', 'delete'),
has_perms_by_name(NULL, 'database', 'insert'),
has_perms_by_name(NULL, 'database', 'references'),
has_perms_by_name(NULL, 'database', 'alter'),
has_perms_by_name(NULL, 'database', 'execute'),
has_perms_by_name(NULL, 'database', 'view definition');");

            bool result = false;
            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.Text;
                connection.Open();
                command.CommandText = script;
                SqlDataAdapter sda = new SqlDataAdapter();
                DataSet sdsWork = new DataSet();
                sda.SelectCommand = command;
                command.CommandTimeout = 0;

                if (!Convert.ToBoolean(command.ExecuteScalar()))
                {
                    return result;
                }

                sda.Fill(sdsWork);
                DataTable table = sdsWork.Tables[0];

                int res = 1;
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (table.Rows[i][j] != DBNull.Value)
                        {
                            res = res & (int) table.Rows[i][j];
                        }
                        else
                        {
                            res = res & 0;
                            break;
                        }
                    } // for
                } // for

                if (res != 0)
                    result = true;
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        /// <summary>
        /// ���������, ���������� �� � ������������ ���� ��� ������������� �����.
        /// </summary>
        /// <param name="serverName">��� ������� MySQL</param>
        /// <param name="dbName">�������� ���� ������</param>
        /// <param name="login">����� �������������� ��</param>
        /// <param name="password">������ �������������� ��</param>
        /// <returns></returns>
        private bool checkPrivileges(string serverName, string dbName, string login, string password)
        {
            string connectionString = String.Format(
                "server={0};database={1};user id={2};password={3}",
                serverName, dbName, login, password);
            string host = ("localhost" == serverName) ? serverName : "%";
            string script = String.Format(@"
SELECT 1 FROM `mysql`.`user` u WHERE u.`User`='{0}' AND u.`Host`='{1}'
  AND u.`Select_priv`='Y' AND u.`Insert_priv`='Y' AND u.`Update_priv`='Y'
  AND u.`Delete_priv`='Y' AND u.`Create_priv`='Y' AND u.`Drop_priv`='Y'
  AND u.`Grant_priv`='Y' AND u.`References_priv`='Y' AND u.`Index_priv`='Y'
  AND u.`Alter_priv`='Y' AND u.`Show_db_priv`='Y' AND u.`Super_priv`='Y'
  AND u.`Create_tmp_table_priv`='Y' AND u.`Lock_tables_priv`='Y' AND u.`Process_priv`='Y'
  AND u.`Execute_priv`='Y' AND u.`Create_view_priv`='Y' AND u.`Show_view_priv`='Y'
  AND u.`Create_routine_priv`='Y' AND u.`Alter_routine_priv`='Y' AND u.`File_priv`='Y';", login, host);

            bool result = false;
            MySqlConnection connection = new MySqlConnection(connectionString);
            try
            {
                MySqlCommand command = new MySqlCommand(script);
                command.Connection = connection;
                command.CommandType = CommandType.Text;
                connection.Open();
                result = Convert.ToBoolean(command.ExecuteScalar());
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        /// <summary>
        /// ������������� ������� ����� ���������� � ��������� ���������.
        /// ���������� �������� �����������.
        /// </summary>
        private void initView()
        {
            Controls.Welcome welcome = new Controls.Welcome();
            TypeDataBase = Controls.Welcome.TypeDataBase;

            // ��� MY SQL
            welcome.Next += new EventHandler<InputEventArgs>(welcome_Next);

            // ��� MS SQL
            welcome.NextMs += new EventHandler<InputEventArgs>(welcome_NextMs);

            _mainForm.SetWelcomePage(welcome);
            Application.Run(_mainForm);
        }

        /// <summary>
        /// ����������� ������� ������ ���������� ����� ��� MS SQL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void welcome_NextMs(object sender, InputEventArgs e)
        {
            Controls.Welcome welcome = (Controls.Welcome) sender;

            TypeDataBase = Controls.Welcome.MS_SQL_WORK;

            //��������� ����������
            if (!checkConnectionMs(e.HostName, e.DbName, e.Login, e.Password))
            {
                welcome.ShowNoConnectionWarning();
                return;
            }

            //��������� ����� ������������
            if (!checkPrivilegesMs(e.HostName, e.DbName, e.Login, e.Password))
            {
                welcome.ShowNoPrivilegesWarning();
                return;
            }

            _mainForm.SetProgressPageMs(_progressMs);

            startPatchDelegate del = new startPatchDelegate(startPatchMs);
            del.BeginInvoke(e.HostName, e.DbName, e.Login, e.Password, e.versionTDM, finishPatch, null);
        } // welcome_NextMs

        /// <summary>
        /// ����������� ������� ������ ���������� �����.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void welcome_Next(object sender, InputEventArgs e)
        {
            Controls.Welcome welcome = (Controls.Welcome) sender;

            TypeDataBase = Controls.Welcome.MY_SQL_WORK;

            //��������� ����������
            if (!checkConnection(e.HostName, e.DbName, e.Login, e.Password))
            {
                welcome.ShowNoConnectionWarning();
                return;
            }

            //��������� ����� ������������
            if (!checkPrivileges(e.HostName, e.DbName, e.Login, e.Password))
            {
                welcome.ShowNoPrivilegesWarning();
                return;
            }

            _mainForm.SetProgressPage(_progress);
            startPatchDelegate del = new startPatchDelegate(startPatch);
            del.BeginInvoke(e.HostName, e.DbName, e.Login, e.Password, e.versionTDM, finishPatch, null);
        }

        /// <summary>
        /// �������� ���������� ��������� ���� ������.
        /// </summary>
        /// <param name="serverName">��� ������� MySQL</param>
        /// <param name="dbName">�������� ���� ������</param>
        /// <param name="login">��� �������������� ��</param>
        /// <param name="password">������ �������������� ��</param>
        private void startPatch(string serverName, string dbName, string login, string password, string versiontdm)
        {
            _version = versiontdm;
            string connectionString = String.Format(
                "server={0};database={1};user id={2};password={3};allow user variables=yes",
                serverName, dbName, login, password);

            if ("TDataManager v. 3.5.1" == versiontdm)
            {

               string key = "Update_New_To_351.sql";
                {
                    _progress.ShowScriptBegin(_scriptInfo[key]);
                    execScript(key, connectionString);
                    _progress.ShowScriptEnd();
                }
            }
            else if("TDataManager v. 3.0.5" == versiontdm)
            {
                string key = "Update_To_Back_TDM_3_0_5.sql";
                {
                    _progress.ShowScriptBegin(_scriptInfo[key]);
                    execScript(key, connectionString);
                    _progress.ShowScriptEnd();
                }
            }
            else if("TDataManager v. 3.5.0" == versiontdm)
            {
                string key = "Update_New_To_350.sql";
                {
                    _progress.ShowScriptBegin(_scriptInfo[key]);
                    execScript(key, connectionString);
                    _progress.ShowScriptEnd();
                }
            }
            else
            {
                // to do
            }
        }

        /// <summary>
        /// �������� ���������� ��������� ���� ������  ��� MS SQL
        /// </summary>
        /// <param name="serverName">��� ������� MS SQL</param>
        /// <param name="dbName">�������� ���� ������</param>
        /// <param name="login">��� �������������� ��</param>
        /// <param name="password">������ �������������� ��</param>
        private void startPatchMs(string serverName, string dbName, string login, string password, string version)
        {
            _version = version;

            string connectionString = "";

            if (login == "")
            {
                connectionString = String.Format("Server={0};database={1};trusted_connection=true;",
                                                 serverName, dbName);
            }
            else
            {
                connectionString = String.Format("Server={0};database={1};user id={2};password={3};",
                                                 serverName, dbName, login, password);
            }

            if ("TDataManager v. 3.5.1" == version)
            {
                string key = "MSUpdate_New_To_351.sql";
                {
                    _progressMs.ShowScriptBegin(_scriptInfoMs[key]);

                    string scriptNameMs = "";
                    string scriptSQL = "";

                    if (key.Contains("."))
                    {
                        using (StreamReader sr = new StreamReader("..\\ScriptsMS\\" + key))
                        {
                            scriptSQL = sr.ReadToEnd();
                        }
                    }
                    else
                    {
                        throw new Exception("������������ ������ ����� ����� SQL.\n����������� ����������.");
                    }

                    execScriptMs(scriptSQL, key, connectionString);

                    _progressMs.ShowScriptEnd();
                }
            } 
            else if("TDataManager v. 3.0.5" == version)
            {
                string key = "MSUpdate_To_Back_TDM_3_0_5.sql";
                _progressMs.ShowScriptBegin(_scriptInfoMs[key]);

                string scriptNameMs = "";
                string scriptSQL = "";

                if (key.Contains("."))
                {
                    using (StreamReader sr = new StreamReader("..\\ScriptsMS\\" + key))
                    {
                        scriptSQL = sr.ReadToEnd();
                    }
                }
                else
                {
                    throw new Exception("������������ ������ ����� ����� SQL.\n����������� ����������.");
                }

                execScriptMs(scriptSQL, key, connectionString);

                _progressMs.ShowScriptEnd();
            }
            else if("TDataManager v. 3.5.0" == version)
            {
                string key = "MSUpdate_New_To_350.sql";
                _progressMs.ShowScriptBegin(_scriptInfoMs[key]);

                string scriptNameMs = "";
                string scriptSQL = "";

                if (key.Contains("."))
                {
                    using (StreamReader sr = new StreamReader("..\\ScriptsMS\\" + key))
                    {
                        scriptSQL = sr.ReadToEnd();
                    }
                }
                else
                {
                    throw new Exception("������������ ������ ����� ����� SQL.\n����������� ����������.");
                }

                execScriptMs(scriptSQL, key, connectionString);

                _progressMs.ShowScriptEnd();
            }
            else
            {
                // to do
            }
        } // startPatchMs

        /// <summary>
        /// ��������� ������ �� ����� Scripts.
        /// ��� ������� ��� ����� ������ ������ ���� Embedded Resource.
        /// </summary>
        /// <param name="scriptName">��� ����� �������</param>
        /// <param name="connectionString">������ ���������� � ��</param>
        private void execScript(string scriptName, string connectionString)
        {
            try
            {
                {
                    using (StreamReader sr = new StreamReader("..\\Scripts\\" + scriptName))
                    {
                        using (MySqlConnection connection = new MySqlConnection(connectionString))
                        {
                            MySqlCommand command = new MySqlCommand(sr.ReadToEnd());
                            command.Connection = connection;
                            command.CommandTimeout = Convert.ToInt32(Int32.MaxValue/1000);
                            command.CommandType = CommandType.Text;
                            connection.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(Assembly.GetExecutingAssembly().FullName);
                sb.AppendFormat("{0}: {1}", Resources.ErrorOccured, scriptName);
                sb.AppendLine();
                sb.AppendFormat("{0}: {1}", Resources.ConnectionString, connectionString);
                sb.AppendLine();
                sb.Append(ex);

                string text = sb.ToString();
                MessageBox.Show(text, "DB Patch", MessageBoxButtons.OK, MessageBoxIcon.Error);
                LogException(text);

                throw ex;
            }
        }

        /// <summary>
        /// ��������� ������ �� ����� Scripts ��� MS SQL
        /// ��� ������� ��� ����� ������ ������ ���� Embedded Resource.
        /// </summary>
        /// <param name="strScript">����������� SQL c�����</param>
        /// <param name="scriptName">��� ����� �������</param>
        /// <param name="connectionString">������ ���������� � ��</param>
        private void execScriptMs(string strScript, string scriptName, string connectionString)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(strScript);
                    command.Connection = connection;
                    command.CommandTimeout = Convert.ToInt32(Int32.MaxValue / 1000);
                    command.CommandType = CommandType.Text;
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(Assembly.GetExecutingAssembly().FullName);
                sb.AppendFormat("{0}: {1}", Resources.ErrorOccured, scriptName);
                sb.AppendLine();
                sb.AppendFormat("{0}: {1}", Resources.ConnectionString, connectionString);
                sb.AppendLine();
                sb.Append(ex);

                string text = sb.ToString();
                MessageBox.Show(text, "DB Patch", MessageBoxButtons.OK, MessageBoxIcon.Error);
                LogException(text);

                throw new Exception(ex.Message + " File " + scriptName + ".");
            } // catch
        } // execScriptMs

        /// <summary>
        /// ����������� ���������� � ���-�����.
        /// </summary>
        /// <param name="text">����� ����������.</param>
        private static void LogException(string text)
        {
            string path = Path.Combine(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                String.Format("Error_{0}.log", DateTime.Now.ToString("yyyyMMdd")));

            using (StreamWriter wr = new StreamWriter(path, true, Encoding.Default))
            {
                wr.WriteLine(DateTime.Now);
                wr.WriteLine(text);
                wr.WriteLine(String.Empty);
            }
        }

        /// <summary>
        /// ���������� �� �������� ��������� ���������� ����� ��.
        /// </summary>
        private void finishPatch(IAsyncResult ar)
        {
            Controls.Finish finish = new Controls.Finish();
            finish.Exit += new EventHandler(finish_Exit);
            _mainForm.SetFinishPage(finish);
        }

        /// <summary>
        /// ��������� ����������.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void finish_Exit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion

        private string GetScriptRegime()
        {
            System.Resources.ResourceManager rm = new System.Resources.ResourceManager(
                "TDataManagerPath.Properties.Resources", Assembly.GetExecutingAssembly());

            return rm.GetString("REGIME") == "1" ? "AGRO" : "MAIN";
        }
    }
}
