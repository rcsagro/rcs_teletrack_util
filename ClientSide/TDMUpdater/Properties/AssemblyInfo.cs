﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TDMPath")]
[assembly: AssemblyDescription("Обновление базы данных для корректной работы системы TrackControl")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("RCS Ltd")]
[assembly: AssemblyProduct("TDMPath")]
[assembly: AssemblyCopyright("© RCS 2008-2017")]
[assembly: AssemblyTrademark("AVS")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in t his assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dafd415f-155a-493c-b5ad-b6b9d55cb75b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.48.15.2")]
[assembly: AssemblyFileVersion("1.48.15.2")]
