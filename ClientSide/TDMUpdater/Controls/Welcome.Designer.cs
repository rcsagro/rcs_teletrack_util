namespace TrackControl.DbPatch.Controls
{
  partial class Welcome
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.mainTable = new System.Windows.Forms.TableLayoutPanel();
            this.mySqlIcon = new System.Windows.Forms.Label();
            this.textTable = new System.Windows.Forms.TableLayoutPanel();
            this.headLbl = new System.Windows.Forms.Label();
            this.textLbl = new System.Windows.Forms.Label();
            this.loginGroupBox = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.loginTable = new System.Windows.Forms.TableLayoutPanel();
            this.hostLbl = new System.Windows.Forms.Label();
            this.dbNameLbl = new System.Windows.Forms.Label();
            this.loginLbl = new System.Windows.Forms.Label();
            this.passLbl = new System.Windows.Forms.Label();
            this.hostTextBox = new System.Windows.Forms.TextBox();
            this.dbNameTextBox = new System.Windows.Forms.TextBox();
            this.loginTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.nextBtn = new System.Windows.Forms.Button();
            this.warningTable = new System.Windows.Forms.TableLayoutPanel();
            this.warningIcon = new System.Windows.Forms.Label();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxVersion1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.comboBoxVersion2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxAuth = new System.Windows.Forms.ComboBox();
            this.labelAuth = new System.Windows.Forms.Label();
            this.buttonMS = new System.Windows.Forms.Button();
            this.textBoxPasswd = new System.Windows.Forms.TextBox();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.textBoxBD = new System.Windows.Forms.TextBox();
            this.textBoxServer = new System.Windows.Forms.TextBox();
            this.labelPasswd = new System.Windows.Forms.Label();
            this.labelLog = new System.Windows.Forms.Label();
            this.labelBD = new System.Windows.Forms.Label();
            this.labelServer = new System.Windows.Forms.Label();
            this.mainTable.SuspendLayout();
            this.textTable.SuspendLayout();
            this.loginGroupBox.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.loginTable.SuspendLayout();
            this.warningTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxVersion1.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxVersion2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // mainTable
            // 
            this.mainTable.BackColor = System.Drawing.Color.Transparent;
            this.mainTable.ColumnCount = 5;
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 122F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.mainTable.Controls.Add(this.mySqlIcon, 1, 1);
            this.mainTable.Controls.Add(this.textTable, 3, 1);
            this.mainTable.Controls.Add(this.loginGroupBox, 1, 3);
            this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTable.Location = new System.Drawing.Point(0, 0);
            this.mainTable.Margin = new System.Windows.Forms.Padding(0);
            this.mainTable.Name = "mainTable";
            this.mainTable.RowCount = 5;
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.mainTable.Size = new System.Drawing.Size(492, 346);
            this.mainTable.TabIndex = 0;
            // 
            // mySqlIcon
            // 
            this.mySqlIcon.AutoSize = true;
            this.mySqlIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mySqlIcon.Image = global::TDataManagerPath.Properties.Resources.MySQL;
            this.mySqlIcon.Location = new System.Drawing.Point(8, 15);
            this.mySqlIcon.Margin = new System.Windows.Forms.Padding(0);
            this.mySqlIcon.Name = "mySqlIcon";
            this.mySqlIcon.Size = new System.Drawing.Size(122, 123);
            this.mySqlIcon.TabIndex = 0;
            // 
            // textTable
            // 
            this.textTable.ColumnCount = 1;
            this.textTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.textTable.Controls.Add(this.headLbl, 0, 0);
            this.textTable.Controls.Add(this.textLbl, 0, 1);
            this.textTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textTable.Location = new System.Drawing.Point(145, 15);
            this.textTable.Margin = new System.Windows.Forms.Padding(0);
            this.textTable.Name = "textTable";
            this.textTable.RowCount = 2;
            this.textTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.textTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.textTable.Size = new System.Drawing.Size(339, 123);
            this.textTable.TabIndex = 1;
            // 
            // headLbl
            // 
            this.headLbl.AutoSize = true;
            this.headLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.headLbl.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.headLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(111)))), ((int)(((byte)(58)))), ((int)(((byte)(57)))));
            this.headLbl.Location = new System.Drawing.Point(0, 0);
            this.headLbl.Margin = new System.Windows.Forms.Padding(0);
            this.headLbl.Name = "headLbl";
            this.headLbl.Size = new System.Drawing.Size(339, 25);
            this.headLbl.TabIndex = 0;
            this.headLbl.Text = "���������� ��������� �� ���";
            // 
            // textLbl
            // 
            this.textLbl.AutoSize = true;
            this.textLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textLbl.Location = new System.Drawing.Point(0, 25);
            this.textLbl.Margin = new System.Windows.Forms.Padding(0);
            this.textLbl.Name = "textLbl";
            this.textLbl.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.textLbl.Size = new System.Drawing.Size(339, 98);
            this.textLbl.TabIndex = 1;
            // 
            // loginGroupBox
            // 
            this.mainTable.SetColumnSpan(this.loginGroupBox, 3);
            this.loginGroupBox.Controls.Add(this.tabControl1);
            this.loginGroupBox.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.loginGroupBox.ForeColor = System.Drawing.Color.Navy;
            this.loginGroupBox.Location = new System.Drawing.Point(11, 149);
            this.loginGroupBox.Name = "loginGroupBox";
            this.loginGroupBox.Padding = new System.Windows.Forms.Padding(20, 10, 20, 10);
            this.loginGroupBox.Size = new System.Drawing.Size(470, 186);
            this.loginGroupBox.TabIndex = 2;
            this.loginGroupBox.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(2, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(462, 182);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.loginTable);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(454, 153);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // loginTable
            // 
            this.loginTable.BackColor = System.Drawing.Color.AliceBlue;
            this.loginTable.ColumnCount = 6;
            this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.loginTable.Controls.Add(this.hostLbl, 0, 0);
            this.loginTable.Controls.Add(this.dbNameLbl, 0, 2);
            this.loginTable.Controls.Add(this.loginLbl, 0, 4);
            this.loginTable.Controls.Add(this.passLbl, 0, 6);
            this.loginTable.Controls.Add(this.hostTextBox, 2, 0);
            this.loginTable.Controls.Add(this.dbNameTextBox, 2, 2);
            this.loginTable.Controls.Add(this.loginTextBox, 2, 4);
            this.loginTable.Controls.Add(this.passwordTextBox, 2, 6);
            this.loginTable.Controls.Add(this.nextBtn, 4, 0);
            this.loginTable.Controls.Add(this.warningTable, 0, 8);
            this.loginTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginTable.Location = new System.Drawing.Point(3, 3);
            this.loginTable.Name = "loginTable";
            this.loginTable.RowCount = 9;
            this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.loginTable.Size = new System.Drawing.Size(448, 147);
            this.loginTable.TabIndex = 1;
            // 
            // hostLbl
            // 
            this.hostLbl.AutoSize = true;
            this.hostLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hostLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hostLbl.ForeColor = System.Drawing.Color.DarkGreen;
            this.hostLbl.Location = new System.Drawing.Point(0, 0);
            this.hostLbl.Margin = new System.Windows.Forms.Padding(0);
            this.hostLbl.Name = "hostLbl";
            this.hostLbl.Size = new System.Drawing.Size(70, 20);
            this.hostLbl.TabIndex = 0;
            this.hostLbl.Text = "������";
            this.hostLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dbNameLbl
            // 
            this.dbNameLbl.AutoSize = true;
            this.dbNameLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dbNameLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dbNameLbl.ForeColor = System.Drawing.Color.Peru;
            this.dbNameLbl.Location = new System.Drawing.Point(0, 25);
            this.dbNameLbl.Margin = new System.Windows.Forms.Padding(0);
            this.dbNameLbl.Name = "dbNameLbl";
            this.dbNameLbl.Size = new System.Drawing.Size(70, 20);
            this.dbNameLbl.TabIndex = 1;
            this.dbNameLbl.Text = "����";
            this.dbNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // loginLbl
            // 
            this.loginLbl.AutoSize = true;
            this.loginLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.loginLbl.ForeColor = System.Drawing.Color.Black;
            this.loginLbl.Location = new System.Drawing.Point(0, 50);
            this.loginLbl.Margin = new System.Windows.Forms.Padding(0);
            this.loginLbl.Name = "loginLbl";
            this.loginLbl.Size = new System.Drawing.Size(70, 20);
            this.loginLbl.TabIndex = 2;
            this.loginLbl.Text = "�����";
            this.loginLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // passLbl
            // 
            this.passLbl.AutoSize = true;
            this.passLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.passLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.passLbl.ForeColor = System.Drawing.Color.Black;
            this.passLbl.Location = new System.Drawing.Point(0, 75);
            this.passLbl.Margin = new System.Windows.Forms.Padding(0);
            this.passLbl.Name = "passLbl";
            this.passLbl.Size = new System.Drawing.Size(70, 20);
            this.passLbl.TabIndex = 3;
            this.passLbl.Text = "������";
            this.passLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // hostTextBox
            // 
            this.hostTextBox.BackColor = System.Drawing.Color.White;
            this.hostTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hostTextBox.Location = new System.Drawing.Point(75, 0);
            this.hostTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.hostTextBox.MaxLength = 60;
            this.hostTextBox.Name = "hostTextBox";
            this.hostTextBox.Size = new System.Drawing.Size(180, 22);
            this.hostTextBox.TabIndex = 4;
            // 
            // dbNameTextBox
            // 
            this.dbNameTextBox.BackColor = System.Drawing.Color.White;
            this.dbNameTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dbNameTextBox.Location = new System.Drawing.Point(75, 25);
            this.dbNameTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.dbNameTextBox.MaxLength = 64;
            this.dbNameTextBox.Name = "dbNameTextBox";
            this.dbNameTextBox.Size = new System.Drawing.Size(180, 22);
            this.dbNameTextBox.TabIndex = 5;
            // 
            // loginTextBox
            // 
            this.loginTextBox.BackColor = System.Drawing.Color.White;
            this.loginTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginTextBox.Location = new System.Drawing.Point(75, 50);
            this.loginTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.loginTextBox.MaxLength = 16;
            this.loginTextBox.Name = "loginTextBox";
            this.loginTextBox.Size = new System.Drawing.Size(180, 22);
            this.loginTextBox.TabIndex = 6;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.BackColor = System.Drawing.Color.White;
            this.passwordTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.passwordTextBox.Location = new System.Drawing.Point(75, 75);
            this.passwordTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.passwordTextBox.MaxLength = 40;
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(180, 22);
            this.passwordTextBox.TabIndex = 7;
            this.passwordTextBox.UseSystemPasswordChar = true;
            // 
            // nextBtn
            // 
            this.nextBtn.BackgroundImage = global::TDataManagerPath.Properties.Resources.Next;
            this.nextBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.nextBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nextBtn.FlatAppearance.BorderSize = 3;
            this.nextBtn.Location = new System.Drawing.Point(280, 0);
            this.nextBtn.Margin = new System.Windows.Forms.Padding(0);
            this.nextBtn.Name = "nextBtn";
            this.loginTable.SetRowSpan(this.nextBtn, 7);
            this.nextBtn.Size = new System.Drawing.Size(95, 95);
            this.nextBtn.TabIndex = 8;
            this.nextBtn.UseVisualStyleBackColor = true;
            // 
            // warningTable
            // 
            this.warningTable.ColumnCount = 3;
            this.loginTable.SetColumnSpan(this.warningTable, 6);
            this.warningTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.warningTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.warningTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.warningTable.Controls.Add(this.warningIcon, 1, 0);
            this.warningTable.Controls.Add(this.panelControl1, 2, 0);
            this.warningTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.warningTable.Location = new System.Drawing.Point(0, 107);
            this.warningTable.Margin = new System.Windows.Forms.Padding(0);
            this.warningTable.Name = "warningTable";
            this.warningTable.RowCount = 1;
            this.warningTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.warningTable.Size = new System.Drawing.Size(448, 40);
            this.warningTable.TabIndex = 9;
            // 
            // warningIcon
            // 
            this.warningIcon.AutoSize = true;
            this.warningIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.warningIcon.Location = new System.Drawing.Point(5, 0);
            this.warningIcon.Margin = new System.Windows.Forms.Padding(0);
            this.warningIcon.Name = "warningIcon";
            this.warningIcon.Size = new System.Drawing.Size(50, 40);
            this.warningIcon.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.comboBoxVersion1);
            this.panelControl1.Location = new System.Drawing.Point(58, 3);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(387, 34);
            this.panelControl1.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(64, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "������ ���:";
            // 
            // comboBoxVersion1
            // 
            this.comboBoxVersion1.EditValue = "TDataManager v. 3.0.5";
            this.comboBoxVersion1.Location = new System.Drawing.Point(176, 9);
            this.comboBoxVersion1.Name = "comboBoxVersion1";
            this.comboBoxVersion1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxVersion1.Properties.Items.AddRange(new object[] {
            "TDataManager v. 3.0.5",
            "TDataManager v. 3.5.0",
            "TDataManager v. 3.5.1"});
            this.comboBoxVersion1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxVersion1.Size = new System.Drawing.Size(204, 20);
            this.comboBoxVersion1.TabIndex = 0;
            this.comboBoxVersion1.ToolTip = "�������� ������ ���� ���������";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage2.Controls.Add(this.comboBoxVersion2);
            this.tabPage2.Controls.Add(this.labelControl2);
            this.tabPage2.Controls.Add(this.comboBoxAuth);
            this.tabPage2.Controls.Add(this.labelAuth);
            this.tabPage2.Controls.Add(this.buttonMS);
            this.tabPage2.Controls.Add(this.textBoxPasswd);
            this.tabPage2.Controls.Add(this.textBoxLog);
            this.tabPage2.Controls.Add(this.textBoxBD);
            this.tabPage2.Controls.Add(this.textBoxServer);
            this.tabPage2.Controls.Add(this.labelPasswd);
            this.tabPage2.Controls.Add(this.labelLog);
            this.tabPage2.Controls.Add(this.labelBD);
            this.tabPage2.Controls.Add(this.labelServer);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(454, 153);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            // 
            // comboBoxVersion2
            // 
            this.comboBoxVersion2.EditValue = "TDataManager v. 3.0.5";
            this.comboBoxVersion2.Location = new System.Drawing.Point(100, 118);
            this.comboBoxVersion2.Name = "comboBoxVersion2";
            this.comboBoxVersion2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxVersion2.Properties.Items.AddRange(new object[] {
            "TDataManager v. 3.0.5",
            "TDataManager v. 3.5.0",
            "TDataManager v. 3.5.1"});
            this.comboBoxVersion2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxVersion2.Size = new System.Drawing.Size(216, 20);
            this.comboBoxVersion2.TabIndex = 14;
            this.comboBoxVersion2.ToolTip = "�������� ������ ���� ���������";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 121);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "������ ���:";
            // 
            // comboBoxAuth
            // 
            this.comboBoxAuth.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxAuth.FormattingEnabled = true;
            this.comboBoxAuth.Location = new System.Drawing.Point(100, 31);
            this.comboBoxAuth.Name = "comboBoxAuth";
            this.comboBoxAuth.Size = new System.Drawing.Size(216, 23);
            this.comboBoxAuth.TabIndex = 12;
            // 
            // labelAuth
            // 
            this.labelAuth.AutoSize = true;
            this.labelAuth.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAuth.Location = new System.Drawing.Point(3, 31);
            this.labelAuth.Name = "labelAuth";
            this.labelAuth.Size = new System.Drawing.Size(94, 16);
            this.labelAuth.TabIndex = 11;
            this.labelAuth.Text = "�����������";
            // 
            // buttonMS
            // 
            this.buttonMS.BackgroundImage = global::TDataManagerPath.Properties.Resources.Next;
            this.buttonMS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonMS.FlatAppearance.BorderSize = 3;
            this.buttonMS.Location = new System.Drawing.Point(339, 16);
            this.buttonMS.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMS.Name = "buttonMS";
            this.buttonMS.Size = new System.Drawing.Size(95, 103);
            this.buttonMS.TabIndex = 9;
            this.buttonMS.UseVisualStyleBackColor = true;
            this.buttonMS.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxPasswd
            // 
            this.textBoxPasswd.Location = new System.Drawing.Point(241, 87);
            this.textBoxPasswd.Name = "textBoxPasswd";
            this.textBoxPasswd.Size = new System.Drawing.Size(75, 22);
            this.textBoxPasswd.TabIndex = 7;
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(100, 87);
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.Size = new System.Drawing.Size(72, 22);
            this.textBoxLog.TabIndex = 6;
            // 
            // textBoxBD
            // 
            this.textBoxBD.Location = new System.Drawing.Point(100, 59);
            this.textBoxBD.Name = "textBoxBD";
            this.textBoxBD.Size = new System.Drawing.Size(216, 22);
            this.textBoxBD.TabIndex = 5;
            // 
            // textBoxServer
            // 
            this.textBoxServer.Location = new System.Drawing.Point(100, 3);
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.Size = new System.Drawing.Size(216, 22);
            this.textBoxServer.TabIndex = 4;
            // 
            // labelPasswd
            // 
            this.labelPasswd.AutoSize = true;
            this.labelPasswd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPasswd.ForeColor = System.Drawing.Color.Black;
            this.labelPasswd.Location = new System.Drawing.Point(178, 90);
            this.labelPasswd.Name = "labelPasswd";
            this.labelPasswd.Size = new System.Drawing.Size(57, 16);
            this.labelPasswd.TabIndex = 3;
            this.labelPasswd.Text = "������";
            // 
            // labelLog
            // 
            this.labelLog.AutoSize = true;
            this.labelLog.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelLog.ForeColor = System.Drawing.Color.Black;
            this.labelLog.Location = new System.Drawing.Point(3, 87);
            this.labelLog.Name = "labelLog";
            this.labelLog.Size = new System.Drawing.Size(46, 16);
            this.labelLog.TabIndex = 2;
            this.labelLog.Text = "�����";
            // 
            // labelBD
            // 
            this.labelBD.AutoSize = true;
            this.labelBD.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBD.ForeColor = System.Drawing.Color.Chocolate;
            this.labelBD.Location = new System.Drawing.Point(3, 59);
            this.labelBD.Name = "labelBD";
            this.labelBD.Size = new System.Drawing.Size(40, 16);
            this.labelBD.TabIndex = 1;
            this.labelBD.Text = "����";
            // 
            // labelServer
            // 
            this.labelServer.AutoSize = true;
            this.labelServer.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelServer.ForeColor = System.Drawing.Color.DarkGreen;
            this.labelServer.Location = new System.Drawing.Point(3, 8);
            this.labelServer.Name = "labelServer";
            this.labelServer.Size = new System.Drawing.Size(57, 16);
            this.labelServer.TabIndex = 0;
            this.labelServer.Text = "������";
            // 
            // Welcome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::TDataManagerPath.Properties.Resources.WelcomeBg;
            this.Controls.Add(this.mainTable);
            this.Name = "Welcome";
            this.Size = new System.Drawing.Size(492, 346);
            this.mainTable.ResumeLayout(false);
            this.mainTable.PerformLayout();
            this.textTable.ResumeLayout(false);
            this.textTable.PerformLayout();
            this.loginGroupBox.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.loginTable.ResumeLayout(false);
            this.loginTable.PerformLayout();
            this.warningTable.ResumeLayout(false);
            this.warningTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxVersion1.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxVersion2.Properties)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private System.Windows.Forms.Label mySqlIcon;
    private System.Windows.Forms.TableLayoutPanel textTable;
    private System.Windows.Forms.Label headLbl;
    private System.Windows.Forms.Label textLbl;
    private System.Windows.Forms.GroupBox loginGroupBox;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.TableLayoutPanel loginTable;
    private System.Windows.Forms.Label hostLbl;
    private System.Windows.Forms.Label dbNameLbl;
    private System.Windows.Forms.Label loginLbl;
    private System.Windows.Forms.Label passLbl;
    private System.Windows.Forms.TextBox hostTextBox;
    private System.Windows.Forms.TextBox dbNameTextBox;
    private System.Windows.Forms.TextBox loginTextBox;
    private System.Windows.Forms.TextBox passwordTextBox;
    private System.Windows.Forms.Button nextBtn;
    private System.Windows.Forms.TableLayoutPanel warningTable;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.Button buttonMS;
    private System.Windows.Forms.TextBox textBoxPasswd;
    private System.Windows.Forms.TextBox textBoxLog;
    private System.Windows.Forms.TextBox textBoxBD;
    private System.Windows.Forms.TextBox textBoxServer;
    private System.Windows.Forms.Label labelPasswd;
    private System.Windows.Forms.Label labelLog;
    private System.Windows.Forms.Label labelBD;
    private System.Windows.Forms.Label labelServer;
    private System.Windows.Forms.Label labelAuth;
    private System.Windows.Forms.ComboBox comboBoxAuth;
    private DevExpress.XtraEditors.PanelControl panelControl1;
    private DevExpress.XtraEditors.LabelControl labelControl1;
    private DevExpress.XtraEditors.ComboBoxEdit comboBoxVersion1;
    public System.Windows.Forms.Label warningIcon;
    private DevExpress.XtraEditors.ComboBoxEdit comboBoxVersion2;
    private DevExpress.XtraEditors.LabelControl labelControl2;
  }
}
