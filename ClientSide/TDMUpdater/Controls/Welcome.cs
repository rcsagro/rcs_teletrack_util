using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TDataManagerPath.Properties;

namespace TrackControl.DbPatch.Controls
{
    [ToolboxItem(false)]
    public partial class Welcome : UserControl
    {
        /// <summary>
        /// �������, ����������� ����� ��������� ����� �� ������ "�����".
        /// </summary>
        public event EventHandler<InputEventArgs> Next;

        /// <summary>
        /// �������, ����������� ����� ��������� ����� �� ������ "�����" ��� MS SQL
        /// </summary>
        public event EventHandler<InputEventArgs> NextMs;

        public const int MY_SQL_WORK = 0;
        public const int MS_SQL_WORK = 1;
        public static int TypeDataBase = MY_SQL_WORK;

        /// <summary>
        /// �����������.
        /// </summary>
        public Welcome()
        {
            InitializeComponent();
            init();
        }

        #region Public Methods

        /// <summary>
        /// ���������� ������������ � ������������� ����������� � ��������������
        /// ��������������� ��� ������� ������.
        /// </summary>
        public void ShowNoConnectionWarning()
        {
            warningIcon.Image = Resources.NoConnection;
            //warningText.Text = Resources.WrongInputNoConnection;
            warningTable.Visible = true;
        }

        /// <summary>
        /// ���������� ������������ �� ���������� ����������� ���� ��� ���������� �����.
        /// </summary>
        public void ShowNoPrivilegesWarning()
        {
            warningIcon.Image = Resources.NoPrivileges;
            //warningText.Text = Resources.NoAuthority;
            warningTable.Visible = true;
        }

        private void init()
        {
            this.nextBtn.Click += new System.EventHandler(this.nextBtn_Click);
            this.hostTextBox.Enter += new System.EventHandler(this.textBox_Enter);
            this.dbNameTextBox.Enter += new System.EventHandler(this.textBox_Enter);
            this.loginTextBox.Enter += new System.EventHandler(this.textBox_Enter);
            headLbl.Text = Resources.DbSchemaUpdate;
            textLbl.Text = Resources.WelcomeMessage;
            //loginGroupBox.Text = Resources.DbConnect;
            tabPage1.Text = Resources.DbConnect;
            tabPage2.Text = Resources.DbConnectMS;
            hostLbl.Text = Resources.Host;
            dbNameLbl.Text = Resources.Db;
            loginLbl.Text = Resources.Login;
            passLbl.Text = Resources.Pass;

            this.textBoxServer.Enter += new System.EventHandler(this.textBox_EnterMs);
            this.textBoxBD.Enter += new System.EventHandler(this.textBox_EnterMs);
            this.textBoxLog.Enter += new System.EventHandler(this.textBox_EnterMs);
            comboBoxAuth.Items.Add(Resources.AuthWindows);
            comboBoxAuth.Items.Add(Resources.AuthSQL);
            comboBoxAuth.SelectedIndex = 0;
            comboBoxAuth.SelectedIndexChanged += ComboBoxAuthOnSelectedIndexChanged;

            if (comboBoxAuth.SelectedIndex == 0)
            {
                textBoxLog.Enabled = false;
                textBoxPasswd.Enabled = false;
            }
        }

        private void ComboBoxAuthOnSelectedIndexChanged(object sender, EventArgs eventArgs)
        {
            if (comboBoxAuth.SelectedIndex == 0)
            {
                textBoxLog.Enabled = false;
                textBoxPasswd.Enabled = false;
            }
            else if (comboBoxAuth.SelectedIndex == 1)
            {
                textBoxLog.Enabled = true;
                textBoxPasswd.Enabled = true;
            }
        }

        /// <summary>
        /// ������������ ���� �� ������ "�����".
        /// </summary>
        private void nextBtn_Click(object sender, EventArgs e)
        {
            TypeDataBase = MY_SQL_WORK;

            if (!validate()) return;

            onNext();
        }

        /// <summary>
        /// ������������ ��������� ������ ����� ��� �����.
        /// </summary>
        private void textBox_Enter(object sender, EventArgs e)
        {
            clearWarningStatus((TextBox) sender);
            //hideErrorMessage();
        }

        /// <summary>
        /// ������������ ��������� ������ ����� ��� ����� MS SQL
        /// </summary>
        private void textBox_EnterMs(object sender, EventArgs e)
        {
            clearWarningStatus((TextBox) sender);
            //hideErrorMessage();
        }

        /// <summary>
        /// ���������� ������� Next.
        /// </summary>
        private void onNext()
        {
            InputEventArgs inputEventArgs = new InputEventArgs(
                hostTextBox.Text.Trim(),
                dbNameTextBox.Text.Trim(),
                loginTextBox.Text.Trim(),
                passwordTextBox.Text,
                comboBoxVersion1.Text);

            if (Next != null)
                Next(this, inputEventArgs);
        }

        /// <summary>
        /// ��������� ������������ ��������� ������������� ������.
        /// </summary>
        private bool validate()
        {
            bool result = true;
            if (hostTextBox.Text.Trim().Length == 0 || hostTextBox.TextAlign != HorizontalAlignment.Left)
            {
                setWarningStatus(hostTextBox);
                result = false;
            }
            if (dbNameTextBox.Text.Trim().Length == 0 || dbNameTextBox.TextAlign != HorizontalAlignment.Left)
            {
                setWarningStatus(dbNameTextBox);
                result = false;
            }
            if (loginTextBox.Text.Trim().Length == 0 || loginTextBox.TextAlign != HorizontalAlignment.Left)
            {
                setWarningStatus(loginTextBox);
                result = false;
            }
            return result;
        }

        /// <summary>
        /// ������������� ���� ��� ����� ����� ������.
        /// </summary>
        /// <param name="textBox">���� ��� �����</param>
        private void setWarningStatus(TextBox textBox)
        {
            textBox.BackColor = Color.FromArgb(255, 242, 232);
            textBox.TextAlign = HorizontalAlignment.Center;
            textBox.ForeColor = Color.DarkRed;
            textBox.Text = Resources.MustBeFilled;
        }

        /// <summary>
        /// ������������� ���� ��� ����� ���������� �����.
        /// </summary>
        /// <param name="textBox">���� ��� �����</param>
        private void clearWarningStatus(TextBox textBox)
        {
            textBox.BackColor = Color.White;
            textBox.TextAlign = HorizontalAlignment.Left;
            textBox.ForeColor = Color.Black;
            textBox.Text = "";
        }

        /// <summary>
        /// ������ ��������� �� ������.
        /// </summary>
        private void hideErrorMessage()
        {
            warningTable.Visible = false;
        }

        #endregion

        /// <summary>
        /// ������������ ���� �� ������ "����� ��� MS SQL".
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            TypeDataBase = MS_SQL_WORK;

            if (!validateMs())
                return;

            onNextMs();
        }

        /// <summary>
        /// ���������� ������� Next ��� MS SQL
        /// </summary>
        private void onNextMs()
        {
            InputEventArgs inputEventArgs = new InputEventArgs(textBoxServer.Text.Trim(),
                                                               textBoxBD.Text.Trim(), textBoxLog.Text.Trim(),
                                                               textBoxPasswd.Text, comboBoxVersion2.Text);

            if (NextMs != null)
                NextMs(this, inputEventArgs);
        }

        /// <summary>
        /// ��������� ������������ ��������� ������������� ������ ��� MS SQL
        /// </summary>

        private bool validateMs()
        {
            bool result = true;

            if (textBoxServer.Text.Trim().Length == 0 || textBoxServer.TextAlign != HorizontalAlignment.Left)
            {
                setWarningStatus(textBoxServer);
                result = false;
            }

            if (textBoxBD.Text.Trim().Length == 0 || textBoxBD.TextAlign != HorizontalAlignment.Left)
            {
                setWarningStatus(textBoxBD);
                result = false;
            }

            if (comboBoxAuth.SelectedIndex == 1)
            {
                if (textBoxLog.Text.Trim().Length == 0 || textBoxLog.TextAlign != HorizontalAlignment.Left)
                {
                    setWarningStatus(textBoxLog);
                    result = false;
                }
            }
            else
            {
                textBoxLog.Text = "";
                textBoxPasswd.Text = "";
            }

            return result;
        }
    }
}
