using System;
using System.Windows.Forms;
using TDataManagerPath.Properties;

namespace TrackControl.DbPatch.Controls
{
    public partial class Finish : UserControl
    {
        /// <summary>
        /// �������, ����������� ����� ��������� ����� �� ������ "�����".
        /// </summary>
        public event EventHandler Exit;

        /// <summary>
        /// �����������.
        /// </summary>
        public Finish()
        {
            InitializeComponent();
            init();
        }

        private void init()
        {
            finishTextLbl.Text = Resources.PatchSuccessfullyDone;
            exitBtn.Text = Resources.Exit;
        }

        /// <summary>
        /// ������������ ���� �� ������ "�����".
        /// </summary>
        private void exitBtn_Click(object sender, EventArgs e)
        {
            if (Exit != null)
                Exit(this, EventArgs.Empty);
        }
    }
}
