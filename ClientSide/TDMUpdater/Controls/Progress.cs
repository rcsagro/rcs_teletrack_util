using System.ComponentModel;
using System.Windows.Forms;
using TDataManagerPath.Properties;

namespace TrackControl.DbPatch.Controls
{
    [ToolboxItem(false)]
    public partial class Progress : UserControl
    {
        private delegate void ShowScriptBeginDelegate(string description);

        private delegate void ShowScriptEndDelegate();

        public Progress(int scriptsCount)
        {
            InitializeComponent();
            init();

            progressBar.Maximum = scriptsCount;
        }

        private void init()
        {
            headLbl.Text = Resources.InProgress;
            label1.Text = Resources.Progress;
        }

        /// <summary>
        /// ���������� �������� ����������, ������� ����������
        /// � ������� ������ �������.
        /// </summary>
        /// <param name="description">�������� ����������</param>
        public void ShowScriptBegin(string description)
        {
            if (InvokeRequired)
                Invoke(new ShowScriptBeginDelegate(ShowScriptBegin), description);
            else
                patchDescriptionLbl.Text = description;
            Application.DoEvents();
        }

        public void ShowScriptEnd()
        {
            if (InvokeRequired)
                Invoke(new ShowScriptEndDelegate(ShowScriptEnd));
            else
                progressBar.Increment(1);
        }
    }
}
