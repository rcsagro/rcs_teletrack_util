﻿/* Begin this file */
DROP TABLE IF EXISTS datagpsbuffer_on64;
CREATE TABLE IF NOT EXISTS datagpsbuffer_on64 (
  Mobitel_ID int(11) NOT NULL DEFAULT 0 COMMENT 'Код Телетрека',
  Latitude int(11) NOT NULL DEFAULT 0 COMMENT 'Широта',
  Longitude int(11) NOT NULL DEFAULT 0 COMMENT 'Долгота',
  Direction tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'градусы/1.5',
  Acceleration int(11) NOT NULL DEFAULT 0 COMMENT 'Изменение скорости по GPS за последнюю секунду',
  UnixTime int(11) NOT NULL DEFAULT 0,
  Speed smallint(6) NOT NULL DEFAULT 0 COMMENT 'Сотен метров в час (0,1 км/ч)',
  Valid tinyint(1) NOT NULL DEFAULT 1,
  Satellites tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Количество спутников ',
  RssiGsm tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Уровень GSM сигнала',
  Events int(11) NOT NULL DEFAULT 0 COMMENT 'Флаги событий',
  SensorsSet tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Код набора датчиков',
  Sensors varbinary(50) NOT NULL DEFAULT '0' COMMENT 'Значения датчиков',
  Voltage tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Напряжение питания текущего источника/1.5 от 0 до 38,2',
  DGPS varchar(32) NOT NULL DEFAULT '0' COMMENT 'Данные дифференциальной системы GPS - высота,широта,долгота',
  LogID int(11) NOT NULL DEFAULT 0 COMMENT 'Идентификатор записи в журнале Телетрека',
  SrvPacketID bigint(20) NOT NULL DEFAULT 0 COMMENT 'ID серверного пакета в состав которого входит эта запись',
  INDEX IDX_MobitelidLogid_64 USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime_64 USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid_64 USING BTREE (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_SrvPacketID_64 USING BTREE (SrvPacketID),
  CONSTRAINT FK_datagps64_mobitels_Mobitel_ID_1 FOREIGN KEY (Mobitel_ID)
  REFERENCES mobitels (Mobitel_ID) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

DROP TABLE IF EXISTS datagpsbuffer_ontmp64;
CREATE TABLE IF NOT EXISTS datagpsbuffer_ontmp64 (
  Mobitel_ID int(11) NOT NULL DEFAULT 0 COMMENT 'Код Телетрека',
  LogID int(11) UNSIGNED DEFAULT 0 COMMENT 'Идентификатор записи в журнале Телетрека',
  SrvPacketID bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'ID серверного пакета в состав которого входит эта запись',
  INDEX IDX_Mobitelid64 (Mobitel_ID),
  INDEX IDX_MobitelidSrvpacketid64 (Mobitel_ID, SrvPacketID),
  CONSTRAINT FK_datagps64_mobitels_Mobitel_ID_2 FOREIGN KEY (Mobitel_ID)
  REFERENCES mobitels (Mobitel_ID) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

DROP TABLE IF EXISTS online64;
CREATE TABLE IF NOT EXISTS online64 (
  DataGps_ID int(11) NOT NULL AUTO_INCREMENT,
  Mobitel_ID int(11) NOT NULL DEFAULT 0 COMMENT 'Код Телетрека',
  Latitude int(11) NOT NULL DEFAULT 0 COMMENT 'Широта',
  Longitude int(11) NOT NULL DEFAULT 0 COMMENT 'Долгота',
  Direction tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'градусы/1.5',
  Acceleration int(11) DEFAULT 0 COMMENT 'Изменение скорости по GPS за последнюю секунду',
  UnixTime int(11) NOT NULL DEFAULT 0,
  Speed smallint(6) NOT NULL DEFAULT 0 COMMENT 'Сотен метров в час (0,1 км/ч)',
  Valid tinyint(1) NOT NULL DEFAULT 1,
  Satellites tinyint(1) UNSIGNED DEFAULT 0 COMMENT 'Количество спутников ',
  RssiGsm tinyint(1) UNSIGNED DEFAULT 0 COMMENT 'Уровень GSM сигнала',
  Events int(11) DEFAULT 0 COMMENT 'Флаги событий',
  SensorsSet tinyint(1) DEFAULT 0 COMMENT 'Код набора датчиков',
  Sensors varbinary(50) DEFAULT '0' COMMENT 'Значения датчиков',
  Voltage tinyint(4) UNSIGNED DEFAULT 0 COMMENT 'Напряжение питания текущего источника/1.5 от 0 до 38,2',
  DGPS varbinary(32) DEFAULT '0' COMMENT 'Данные дифференциальной системы GPS - высота,широта,долгота',
  LogID int(11) DEFAULT 0 COMMENT 'Идентификатор записи в журнале Телетрека',
  SrvPacketID bigint(20) NOT NULL DEFAULT 0 COMMENT 'ID серверного пакета в состав которого входит эта запись',
  PRIMARY KEY (DataGps_ID),
  INDEX IDX_Mobitelid64 (Mobitel_ID),
  INDEX IDX_MobitelidLogid64 (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime64 (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid64 (Mobitel_ID, UnixTime, Valid)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;


DROP PROCEDURE IF EXISTS OnCorrectInfotrackLogId64;
CREATE PROCEDURE OnCorrectInfotrackLogId64 ()
SQL SECURITY INVOKER
COMMENT 'Корректировка значений LogId и InMobitelID в данных инфотреков'
BEGIN
  DECLARE MobitelIDInCursor int; /* Текущее значение MobitelID */
  DECLARE DataExists tinyint DEFAULT 1; /* Флаг наличия данных после фетча */
  DECLARE MaxLogId int DEFAULT 0; /* Максимальный LogId для данного дивайса */
  DECLARE SrvPacketIDInCursor bigint DEFAULT 0; /* Текущее значение серверного пакета */

  /* Курсор по инфотрекам в множестве новых данных. 
     Все инфотреки начинаются с буквы I */
  DECLARE CursorInfoTrack CURSOR FOR
  SELECT DISTINCT
    buf.Mobitel_ID
  FROM datagpsbuffer_on64 buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE (c.InternalMobitelConfig_ID = (SELECT
      intConf.InternalMobitelConfig_ID
    FROM internalmobitelconfig intConf
    WHERE (intConf.ID = c.ID)
    ORDER BY intConf.InternalMobitelConfig_ID DESC
    LIMIT 1)) AND
  (c.devIdShort LIKE "I%") AND (buf.LogId = 0)
  ORDER BY 1;

  /* Курсор по данным телетрека */
  DECLARE CursorInfoTrackUpdateSet CURSOR FOR
  SELECT
    SrvPacketID
  FROM datagpsbuffer_on64
  WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId = 0)
  ORDER BY UnixTime;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* Находим максимальный LogId для данного инфотрека */
    SELECT
      COALESCE(MAX(LogID), 0) INTO MaxLogId
    FROM DataGps
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* Обновляем LogId и InMobitelID в наборе новых данных текущего инфотрека */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    WHILE DataExists = 1 DO
      SET MaxLogId = MaxLogId + 1;

      UPDATE datagpsbuffer_on64
      SET LogId = MaxLogId,
          InMobitelID = MaxLogId
      WHERE SrvPacketID = SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    END WHILE;
    CLOSE CursorInfoTrackUpdateSet;

    SET DataExists = 1;
    FETCH CursorInfoTrack INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorInfoTrack;
END;

DROP PROCEDURE IF EXISTS OnDeleteDuplicates64;
CREATE PROCEDURE OnDeleteDuplicates64 ()
SQL SECURITY INVOKER
COMMENT 'Удаление повторяющихся строк из datagpsbuffer_on64'
BEGIN
  /* Удаляет дубликаты строк из таблицы datagpsbuffer_on64. Критерием поиска
   являются одинаковые значения в полях Mobitel_Id + LogID. Из множества
   повторяющихся записей в таблице datagpsbuffer_on64 останется только одна - 
   последняя принятая телематическим сервером, т.е. с максимальным SrvPacketID. */

  /* Текущий MobitelId в курсоре CursorDuplicateGroups */
  DECLARE MobitelIdInCursor int DEFAULT 0;
  /* Текущий LogID в курсоре CursorDuplicateGroups */
  DECLARE LogIdInCursor int DEFAULT 0;
  /* Max SrvPacketId в множестве строк текущей группы дубликатов */
  DECLARE MaxSrvPacketId bigint DEFAULT 0;
  DECLARE DataExists tinyint DEFAULT 1;

  /* Курсор для прохода по всем группам дубликатов */
  DECLARE CursorDuplicateGroups CURSOR FOR
  SELECT
    Mobitel_Id,
    LogId
  FROM datagpsbuffer_on64
  GROUP BY Mobitel_Id,
           LogId
  HAVING COUNT(LogId) > 1;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  WHILE DataExists = 1 DO
    SELECT
      MAX(SrvPacketID) INTO MaxSrvPacketId
    FROM datagpsbuffer_on64
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor);

    DELETE
      FROM datagpsbuffer_on64
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor)
      AND (SrvPacketID < MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  END WHILE;
  CLOSE CursorDuplicateGroups;
END;

DROP PROCEDURE IF EXISTS OnLostDataGPS264;
CREATE PROCEDURE OnLostDataGPS264 (IN MobitelID integer(11), OUT NeedOptimize tinyint)
SQL SECURITY INVOKER
COMMENT 'Модификация диапазонов пропусков. Анализ дублирующих данных заданного телетрека.'
BEGIN
  /********** DECLARE **********/

  DECLARE NewConfirmedID int DEFAULT 0; /* Новое значение ConfirmedID */
  DECLARE BeginLogID int; /* Begin_LogID в курсоре CursorLostRanges */
  DECLARE EndLogID int; /* End_LogID в курсоре CursorLostRanges */
  DECLARE BeginSrvPacketID bigint; /* Begin_SrvPacketID в курсоре CursorLostRanges */
  DECLARE EndSrvPacketID bigint; /* End_SrvPacketID в курсоре CursorLostRanges */
  DECLARE RowCountForAnalyze int DEFAULT 0; /* Количество записей для анализа */
  DECLARE DataExists tinyint DEFAULT 1; /* Флаг наличия данных после фетча */

  /* Курсор для прохода по диапазонам пропусков телетрека */
  DECLARE CursorLostRanges CURSOR FOR
  SELECT
    Begin_LogID,
    End_LogID,
    Begin_SrvPacketID,
    End_SrvPacketID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;

  /* Курсор для прохода по новым LogID */
  DECLARE CursorNewLogID CURSOR FOR
  SELECT
    LogID,
    SrvPacketID
  FROM datagpslost_ontmp
  ORDER BY SrvPacketID;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* Перебираем диапазоны и сокращаем разрывы если это возможно */
  SET DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  WHILE DataExists = 1 DO
    /* Заполнение временной таблицы дублирующими данными телетрека
       для анализа разрывов */
    INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
      SELECT DISTINCT
        LogID,
        SrvPacketID
      FROM datagpsbuffer_ontmp64
      WHERE (Mobitel_ID = MobitelID) AND (SrvPacketID > BeginSrvPacketID)
      AND (SrvPacketID < EndSrvPacketID)
      ORDER BY SrvPacketID;

    /* Если есть данные модифицируем диапазон */
    SELECT
      COUNT(1) INTO RowCountForAnalyze
    FROM datagpslost_ontmp;

    IF RowCountForAnalyze > 0 THEN
      /* Добавляем первую и последнюю записи для анализа */
      INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
        VALUES (BeginLogID, BeginSrvPacketID), (EndLogID, EndSrvPacketID);

      /* Удаляем старый диапазон */
      DELETE
        FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        DECLARE NewLogID int; /* Значение LogID в курсоре CursorNewLogID */
        DECLARE NewSrvPacketID bigint; /* Значение SrvPacketID в курсоре CursorNewLogID */
        DECLARE FirstLogID int DEFAULT -9999999; /* Первое значение LogID для анализа */
        DECLARE SecondLogID int; /* Второе значение LogID для анализа */
        DECLARE FirstSrvPacketID bigint; /* Первое значение SrvPacketID */
        DECLARE SecondSrvPacketID bigint; /* Второе значение SrvPacketID */

        /* Обработчик отсутствующей записи при FETCH следующей записи */
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* Формирование команды-вставки в таблицу пропущенных диапазонов */
        SET DataExists = 1;
        OPEN CursorNewLogID;
        FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        WHILE DataExists = 1 DO
          IF FirstLogID = -9999999 THEN
            /* Первое значение в списке */
            SET FirstLogID = NewLogID;
            SET FirstSrvPacketID = NewSrvPacketID;
          ELSE
            /* Основная ветка расчета*/
            SET SecondLogID = NewLogID;
            SET SecondSrvPacketID = NewSrvPacketID;

            IF (SecondSrvPacketID - FirstSrvPacketID) > 1 THEN
              /* Найден разрыв */
              CALL OnInsertDatagpsLost(MobitelID, FirstLogID, SecondLogID,
              FirstSrvPacketID, SecondSrvPacketID);

              IF NeedOptimize = 0 THEN
                SET NeedOptimize = 1;
              END IF;
            END IF;
            /* Переместим текущее второе значение на первую позицию
               для участия в следующей итерации в качестве начального значения */
            SET FirstLogID = SecondLogID;
            SET FirstSrvPacketID = SecondSrvPacketID;
          END IF;
          FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        END WHILE;
        CLOSE CursorNewLogID;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END IF;

    FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  END WHILE;
  CLOSE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* Если пропущенных записей у данного телетрека нет - 
     тогда ConfirmedID будет равен максимальному LogID 
     данного телетрека. Иначе - минимальный Begin_LogID
     из таблицы пропусков */
  SELECT
    MIN(Begin_LogID) INTO NewConfirmedID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;

  IF NewConfirmedID IS NULL THEN
    SELECT
      MAX(LogID) INTO NewConfirmedID
    FROM datagps64
    WHERE Mobitel_ID = MobitelID;
  END IF;

  /* Обновим ConfirmedID если надо */
  UPDATE mobitels
  SET ConfirmedID = NewConfirmedID
  WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END;

DROP PROCEDURE IF EXISTS OnLostDataGPS64;
CREATE PROCEDURE OnLostDataGPS64 (IN MobitelID integer(11), OUT NeedOptimize tinyint)
SQL SECURITY INVOKER
COMMENT 'Поиск пропусков в online данных заданного телетрека и заполнение таблиц.'
BEGIN

  /********** DECLARE **********/

  DECLARE CurrentConfirmedID int DEFAULT 0; /* Текущее значение ConfirmedID */
  DECLARE NewConfirmedID int DEFAULT 0; /* Новое значение ConfirmedID */
  DECLARE StartLogIDSelect int DEFAULT 0; /* min LogID для выборки */
  DECLARE FinishLogIDSelect int DEFAULT 0; /* max LogID для выборки */
  DECLARE MaxEndLogID int DEFAULT 0; /* max End_LogID в пропущенных диапазонах данного телетрека */

  DECLARE MinLogID_n int DEFAULT 0; /* Минимальный LogID после ConfirmedID в множестве новых данных */
  DECLARE MaxLogID_n int DEFAULT 0; /* Максимальный LogID в множестве новых данных */

  DECLARE DataExists tinyint DEFAULT 1; /* Флаг наличия данных после фетча */

  DECLARE NewLogID int; /* Значение LogID в курсоре CursorNewLogID */
  DECLARE NewSrvPacketID bigint; /* Значение SrvPacketID в курсоре CursorNewLogID */
  DECLARE FirstLogID int DEFAULT -9999999; /* Первое значение LogID для анализа */
  DECLARE SecondLogID int; /* Второе значение LogID для анализа */
  DECLARE FirstSrvPacketID bigint; /* Первое значение SrvPacketID */
  DECLARE SecondSrvPacketID bigint; /* Второе значение SrvPacketID */

  DECLARE tmpSrvPacketID bigint DEFAULT 0; /* Значение SrvPacketID для первой искусственной записи в анализе новых данных, не пересекающихся с существующими */

  /* Курсор для прохода по новым LogID */
  DECLARE CursorNewLogID CURSOR FOR
  SELECT
    LogID,
    SrvPacketID
  FROM datagpslost_ontmp;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET NeedOptimize = 0;

  SELECT
    COALESCE(ConfirmedID, 0) INTO CurrentConfirmedID
  FROM mobitels
  WHERE Mobitel_ID = MobitelID;

  /* Минимальный LogID в новом наборе данных c которым будем работать */
  SELECT
    MIN(LogID) INTO MinLogID_n
  FROM datagpsbuffer_on64
  WHERE (Mobitel_ID = MobitelID) AND (LogID > CurrentConfirmedID);

  /* Максимальный LogID в новом наборе данных c которым будем работать */
  SELECT
    MAX(LogID) INTO MaxLogID_n
  FROM datagpsbuffer_on64
  WHERE Mobitel_ID = MobitelID;

  /* Максимальный End_LogID в пропущенных диапазонах данного телетрека */
  SELECT
    COALESCE(MAX(End_LogID), 0) INTO MaxEndLogID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;

  /* Проверим есть ли данные для анализа */
  IF MinLogID_n IS NOT NULL THEN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF MinLogID_n < MaxEndLogID THEN
      /* Если  MinLogID_n < MaxEndLogID значит пришли данные, 
       которые возможно закроют часть пропусков. */

      /* Находим стартовый LogID для пересчета. */
      SELECT
        COALESCE(Begin_LogID, MinLogID_n) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MinLogID_n > Begin_LogID)
      AND (MinLogID_n < End_LogID);

      /* Находим последний LogID для пересчета. */
      SELECT
        COALESCE(End_LogID, MaxLogID_n) INTO FinishLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MaxLogID_n > Begin_LogID)
      AND (MaxLogID_n < End_LogID);

      /* Удалим старые записи из реестра пропущенных записей 
         (стали неактуальными, надо пересчитать) */
      DELETE
        FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID >= StartLogIDSelect)
        AND (Begin_LogID <= FinishLogIDSelect);

      /* Если первая запись LogID = 0 и этой записи нет в DataGps, тогда
         надо добавить первую фиктивную запись для анализа. */
      IF (StartLogIDSelect = 0) AND (NOT EXISTS (SELECT
            1
          FROM datagps64
          WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect))) THEN

        INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
          VALUES (0, 0);
      END IF;
    ELSE
      /* Пришли новые данные. Надо добавить для анализа искусственную запись - 
         нижнюю границу */

      /* Находим стартовый LogID для пересчета. */
      SELECT
        COALESCE(MAX(End_LogID), CurrentConfirmedID) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE Mobitel_ID = MobitelID;

      /* Находим последний LogID для пересчета. */
      SELECT
        MAX(LogID) INTO FinishLogIDSelect
      FROM datagpsbuffer_on64
      WHERE Mobitel_ID = MobitelID;

      /* Выбираем из datagps соответствующее значение SrvPacketID  для первой
         искусственной записи */
      SELECT
        COALESCE(SrvPacketID, 0) INTO tmpSrvPacketID
      FROM datagps64
      WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect);

      /* Помещаем в таблицу datagpslost_ontmp64 для анализа пропусков 
         дополнительную первую запись */
      INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
        VALUES (StartLogIDSelect, tmpSrvPacketID);
    END IF; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

    /* Заполнение временной таблицы последними данными телетрека
       для анализа разрывов */
    INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
      SELECT
        LogID,
        SrvPacketID
      FROM datagps64
      WHERE (Mobitel_ID = MobitelID) AND (LogID >= StartLogIDSelect)
      AND (LogID <= FinishLogIDSelect)
      ORDER BY LogID;

    /* Формирование команды-вставки в таблицу пропущенных диапазонов */
    SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    WHILE DataExists = 1 DO
      IF FirstLogID = -9999999 THEN
        /* Первое значение в списке */
        SET FirstLogID = NewLogID;
        SET FirstSrvPacketID = NewSrvPacketID;
      ELSE
        /* Основная ветка расчета*/
        SET SecondLogID = NewLogID;
        SET SecondSrvPacketID = NewSrvPacketID;

        IF (SecondLogID - FirstLogID) > 1 THEN
          /* Найден разрыв */
          CALL OnInsertDatagpsLost(MobitelID, FirstLogID, SecondLogID,
          FirstSrvPacketID, SecondSrvPacketID);

          IF NeedOptimize = 0 THEN
            SET NeedOptimize = 1;
          END IF;
        END IF;
        /* Переместим текущее второе значение на первую позицию
           для участия в следующей итерации в качестве начального значения */
        SET FirstLogID = SecondLogID;
        SET FirstSrvPacketID = SecondSrvPacketID;
      END IF;
      FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    END WHILE;
    CLOSE CursorNewLogID;

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

    /* Если пропущенных записей у данного телетрека нет - 
       тогда ConfirmedID будет равен максимальному LogID 
       данного телетрека. Иначе - минимальный Begin_LogID
       из таблицы пропусков */
    SELECT
      MIN(Begin_LogID) INTO NewConfirmedID
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;

    IF NewConfirmedID IS NULL THEN
      SELECT
        MAX(LogID) INTO NewConfirmedID
      FROM datagps64
      WHERE Mobitel_ID = MobitelID;
    END IF;

    /* Обновим ConfirmedID если надо */
    UPDATE mobitels
    SET ConfirmedID = NewConfirmedID
    WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END IF; -- IF MaxEndLogID IS NOT NULL  
END;

DROP PROCEDURE IF EXISTS OnTransferBuffer64;
CREATE PROCEDURE OnTransferBuffer64()
  SQL SECURITY INVOKER
  COMMENT ''
BEGIN
  DECLARE MobitelIDInCursor int; /* ??????obitelID */
  DECLARE DataExists tinyint DEFAULT 1; /* */
  DECLARE NeedOptimize tinyint DEFAULT 0; /* ?? ????? ? */
  DECLARE NeedOptimizeParam tinyint;

  DECLARE TmpMaxUnixTime int; /* °??? ???? ? ??? Max UnixTime ??? online */

  /* */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
  SELECT DISTINCT
    (Mobitel_ID)
  FROM datagpsbuffer_on64;
  /* */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
  SELECT DISTINCT
    (Mobitel_ID)
  FROM datagpsbuffer_ontmp64;

  /* */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  /*DECLARE EXIT HANDLER FOR SQLEXCEPTION 
  BEGIN
    ROLLBACK;
    SELECT 'An error has occurred, operation rollbacked and the stored procedure was terminated';
  end;*/

  CALL delDupSrvPacket();
  CALL OnDeleteDuplicates64();
  /*CALL OnCorrectInfotrackLogId64();*/

  /* ??????, ?????? ?I ? ?? ?? ????? 
     ????????OnLostDataGPS264. */
  /*INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID)
    SELECT
      b.Mobitel_ID,
      b.LogID,
      b.SrvPacketID
    FROM datagpsbuffer_on64 b
      LEFT JOIN datagps64 d
        ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL GROUP BY d.SrvPacketID;*/
 DROP TEMPORARY TABLE IF EXISTS temp1;
  DROP TABLE IF EXISTS temp1;
  CREATE TEMPORARY TABLE temp1  SELECT
      b.Mobitel_ID,
      b.LogID,
      b.SrvPacketID
    FROM datagpsbuffer_on64 b
      LEFT JOIN datagps64 d
        ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL GROUP BY d.SrvPacketID;

INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID) SELECT * FROM temp1 GROUP BY SrvPacketID;

DROP TEMPORARY TABLE IF EXISTS temp1;
DROP TABLE IF EXISTS temp1;
  /* -------------------- ?????? Online ---------------------- */
  /* ??? ??? online ??????? datagpsbuffer_on64 */
  /* ????obitel_ID ?ogID */
  DELETE o
    FROM online64 o, datagpsbuffer_on64 dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);

  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* xxxx */
    SELECT
      COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online64 FORCE INDEX (IDX_Mobitelid64)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ±??????(???00)?????? online */
    INSERT INTO online64 (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, `Events`,
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS)
      SELECT
        Mobitel_ID,
        LogID,
        UnixTime,
        Latitude,
        Longitude,
        Acceleration,
        Direction,
        Speed,
        Valid,
        `Events`,
        Satellites,
        RssiGsm,
        SensorsSet,
        Sensors,
        Voltage,
        DGPS
      FROM (SELECT
          *
        FROM datagpsbuffer_on64
        WHERE (Mobitel_ID = MobitelIDInCursor) AND
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
        GROUP BY Mobitel_ID,
                 LogID
        ORDER BY UnixTime DESC
        LIMIT 100) AS T
      ORDER BY UnixTime ASC;

    /* */
    DELETE
      FROM online64
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < (SELECT
          COALESCE(MIN(UnixTime), 0)
        FROM (SELECT
            UnixTime
          FROM online64
          WHERE Mobitel_ID = MobitelIDInCursor
          ORDER BY UnixTime DESC
          LIMIT 100) T1));

    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */
  /* ???? ?? ... */
  UPDATE datagps64 d, datagpsbuffer_on64 b
  SET d.Latitude = b.Latitude,
      d.Longitude = b.Longitude,
      d.Acceleration = b.Acceleration,
      d.UnixTime = b.UnixTime,
      d.Speed = b.Speed,
      d.Direction = b.Direction,
      d.Valid = b.Valid,
      d.LogID = b.LogID,
      d.`Events` = b.`Events`,
      d.Satellites = b.Satellites,
      d.RssiGsm = b.RssiGsm,
      d.SensorsSet = b.SensorsSet,
      d.Sensors = b.Sensors,
      d.Voltage = b.Voltage,
      d.DGPS = b.DGPS,
      d.SrvPacketID = b.SrvPacketID
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  AND (d.SrvPacketID < b.SrvPacketID);

  /* ?????????? ??? */
  DELETE b
    FROM datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

    /* ±??????*/ 
  INSERT INTO datagps64 (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
  Direction, Speed, Valid, `Events`,
  Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID)
    SELECT
      Mobitel_ID,
      LogID,
      UnixTime,
      Latitude,
      Longitude,
      Acceleration,
      Direction,
      Speed,
      Valid,
      `Events`,
      Satellites,
      RssiGsm,
      SensorsSet,
      Sensors,
      Voltage,
      DGPS,
      SrvPacketID
    FROM datagpsbuffer_on64
    GROUP BY Mobitel_ID,
             LogID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ------ ???????????????------- */
    UPDATE mobitels
    SET LastInsertTime = UNIX_TIMESTAMP(NOW())
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ------ ?????????????-------------- */
    CALL OnLostDataGPS64(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;

  /* ????? */
  TRUNCATE TABLE datagpsbuffer_on64;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ---- ???????????????? ???--- */
    CALL OnLostDataGPS264(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsUpdateSet;

  /* ???????????/
  TRUNCATE TABLE datagpsbuffer_ontmp64;

  /* {???? - ?? ????*/
  IF NeedOptimize = 1 THEN
    OPTIMIZE TABLE datagpslost_on;
  END IF;
END;

DROP INDEX IDX_MobitelidSrvpacketid64 ON datagpsbuffer_ontmp64;
ALTER TABLE datagpsbuffer_ontmp64 ADD INDEX IDX_MobitelidSrvpacketid64(Mobitel_ID, SrvPacketID);

DROP PROCEDURE IF EXISTS OnListMobitelConfig;
CREATE PROCEDURE OnListMobitelConfig()
  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT 'Выборка настроек телетреков которые могут работать в online режиме'
BEGIN
  DECLARE MobitelIDInCursor INT;
  DECLARE DevIdShortInCursor CHAR(4);
  DECLARE DataExists TINYINT DEFAULT 1;

  /* Курсор в множестве настроенных телетреков */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS MobitelID, c.devIdShort AS DevIdShort
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID)
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND
      (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')
    ORDER BY m.Mobitel_ID;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* Временная таблица настроек телетрека */
  CREATE TEMPORARY TABLE tmpMobitelsConfig (
    MobitelID INT NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL) ENGINE = MEMORY;

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor, DevIdShortInCursor;
  WHILE DataExists = 1 DO

    INSERT INTO tmpMobitelsConfig SELECT MobitelIDInCursor, DevIdShortInCursor,
      (SELECT COALESCE(MAX(SrvPacketID), 0)
       FROM datagps
       WHERE Mobitel_ID = MobitelIDInCursor) AS LastPacketID;

    FETCH CursorMobitels INTO MobitelIDInCursor, DevIdShortInCursor;
  END WHILE;
  CLOSE CursorMobitels;

  SELECT MobitelID, DevIdShort, LastPacketID
  FROM tmpMobitelsConfig;

  DROP TEMPORARY TABLE tmpMobitelsConfig;  
END;


DROP PROCEDURE IF EXISTS OnInsertDatagpsLost;
CREATE PROCEDURE OnInsertDatagpsLost(
  IN MobitelID INT, 
  IN BeginLogID INT, 
  IN EndLogID INT,
  IN BeginSrvPacketID BIGINT, 
  IN EndSrvPacketID BIGINT)

  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT 'Вставка данных в таблицу datagpslost_on.'
BEGIN
  /* Флаг существования записи с заданными Mobitel_ID и Begin_LogID
     в таблице datagpslost_on */
  DECLARE MobitelId_BeginLogID_Exists TINYINT DEFAULT 0; 
  /* Флаг существования записи с заданными Mobitel_ID и Begin_SrvPacketID
     в таблице datagpslost_on  */
  DECLARE MobitelId_BeginSrvPacketID_Exists TINYINT DEFAULT 0;
  
  DECLARE RowBeginSrvPacketID BIGINT; 
  
  IF EXISTS(
    SELECT NULL
    FROM datagpslost_on
    WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID))
  THEN
    SET MobitelId_BeginLogID_Exists = 1;
  END IF;

  IF EXISTS(
    SELECT NULL
    FROM datagpslost_on
    WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID))
  THEN
    SET MobitelId_BeginSrvPacketID_Exists = 1;
  END IF;

  IF (MobitelId_BeginLogID_Exists = 0) AND (MobitelId_BeginSrvPacketID_Exists = 0)
  THEN
    /* Вставка новой записи */
    INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID)
    VALUES (MobitelID, BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID);
  ELSE
    IF ((MobitelId_BeginLogID_Exists = 1) AND (MobitelId_BeginSrvPacketID_Exists = 1))
    THEN
      /* Обновление существующей записи */
      UPDATE datagpslost_on
      SET End_LogID = EndLogID, End_SrvPacketID = EndSrvPacketID
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);
    ELSE
      /* Комбинацию MobitelId_BeginLogID_Exists = 0, MobitelId_BeginSrvPacketID_Exists = 1
         игнорируем - это исключительная ситуация, которая не должна возникать. */
      SELECT Begin_SrvPacketID
      INTO RowBeginSrvPacketID
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID)
      LIMIT 1;
      
      IF ((MobitelId_BeginLogID_Exists = 1) AND (MobitelId_BeginSrvPacketID_Exists = 0) AND
        (BeginSrvPacketID < RowBeginSrvPacketID))
      THEN
        /* В эту ветвь попадаем, если телетрек отдаст одну и ту же запись несколько раз 
           (LogID у записей один и тотже, а SrvPacketID различны) */
        UPDATE datagpslost_on
        SET End_LogID = EndLogID, Begin_SrvPacketID = BeginSrvPacketID, End_SrvPacketID = EndSrvPacketID
        WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);
      END IF;
    END IF;
  END IF;
END;


DROP PROCEDURE IF EXISTS OnLostDataGPS;
CREATE PROCEDURE OnLostDataGPS(IN MobitelID INTEGER(11), OUT NeedOptimize TINYINT)
  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT 'Поиск пропусков в online данных заданного телетрека и заполнение таблиц.'
BEGIN

  /********** DECLARE **********/
  
  DECLARE CurrentConfirmedID INT DEFAULT 0; /* Текущее значение ConfirmedID */
  DECLARE NewConfirmedID INT DEFAULT 0; /* Новое значение ConfirmedID */
  DECLARE StartLogIDSelect INT DEFAULT 0; /* min LogID для выборки */
  DECLARE FinishLogIDSelect INT DEFAULT 0; /* max LogID для выборки */
  DECLARE MaxEndLogID INT DEFAULT 0; /* max End_LogID в пропущенных диапазонах данного телетрека */
  
  DECLARE MinLogID_n INT DEFAULT 0; /* Минимальный LogID после ConfirmedID в множестве новых данных */
  DECLARE MaxLogID_n INT DEFAULT 0; /* Максимальный LogID в множестве новых данных */
  
  DECLARE DataExists TINYINT DEFAULT 1; /* Флаг наличия данных после фетча */
    
  DECLARE NewLogID INT; /* Значение LogID в курсоре CursorNewLogID */
  DECLARE NewSrvPacketID BIGINT; /* Значение SrvPacketID в курсоре CursorNewLogID */
  DECLARE FirstLogID INT DEFAULT -9999999; /* Первое значение LogID для анализа */
  DECLARE SecondLogID INT; /* Второе значение LogID для анализа */
  DECLARE FirstSrvPacketID BIGINT; /* Первое значение SrvPacketID */
  DECLARE SecondSrvPacketID BIGINT; /* Второе значение SrvPacketID */
  
  DECLARE tmpSrvPacketID BIGINT DEFAULT 0; /* Значение SrvPacketID для первой искусственной записи в анализе новых данных, не пересекающихся с существующими */  
  
  /* Курсор для прохода по новым LogID */
  DECLARE CursorNewLogID CURSOR FOR
    SELECT LogID, SrvPacketID 
    FROM datagpslost_ontmp;
    
  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  /********** END DECLARE **********/

  SET NeedOptimize = 0;
 
  SELECT COALESCE(ConfirmedID, 0) INTO CurrentConfirmedID
  FROM mobitels
  WHERE Mobitel_ID = MobitelID;

  /* Минимальный LogID в новом наборе данных c которым будем работать */
  SELECT MIN(LogID) INTO MinLogID_n
  FROM datagpsbuffer_on
  WHERE (Mobitel_ID = MobitelID) AND (LogID > CurrentConfirmedID);
  
  /* Максимальный LogID в новом наборе данных c которым будем работать */
  SELECT MAX(LogID) INTO MaxLogID_n
  FROM datagpsbuffer_on 
  WHERE Mobitel_ID = MobitelID;  
  
  /* Максимальный End_LogID в пропущенных диапазонах данного телетрека */
  SELECT COALESCE(MAX(End_LogID), 0) INTO MaxEndLogID
  FROM datagpslost_on 
  WHERE Mobitel_ID = MobitelID;  
  
  /* Проверим есть ли данные для анализа */
  IF MinLogID_n IS NOT NULL THEN
  
    /********** PREPARE **********/
    
    TRUNCATE TABLE datagpslost_ontmp;
  
    IF MinLogID_n < MaxEndLogID THEN
      /* Если  MinLogID_n < MaxEndLogID значит пришли данные, 
       которые возможно закроют часть пропусков. */
       
      /* Находим стартовый LogID для пересчета. */
      SELECT COALESCE(Begin_LogID, MinLogID_n) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MinLogID_n > Begin_LogID) 
        AND (MinLogID_n < End_LogID);
      
      /* Находим последний LogID для пересчета. */  
      SELECT COALESCE(End_LogID, MaxLogID_n) INTO FinishLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MaxLogID_n > Begin_LogID) 
        AND (MaxLogID_n < End_LogID);
       
      /* Удалим старые записи из реестра пропущенных записей 
         (стали неактуальными, надо пересчитать) */
      DELETE FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID >= StartLogIDSelect)
        AND (Begin_LogID <= FinishLogIDSelect);
      
      /* Если первая запись LogID = 0 и этой записи нет в DataGps, тогда
         надо добавить первую фиктивную запись для анализа. */
      IF (StartLogIDSelect = 0) AND (NOT EXISTS
        (Select 1 
         FROM datagps 
         WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect))) THEN
      
        INSERT INTO datagpslost_ontmp(LogID, SrvPacketID) VALUES (0, 0);  
      END IF;       
    ELSE
      /* Пришли новые данные. Надо добавить для анализа искусственную запись - 
         нижнюю границу */
      
      /* Находим стартовый LogID для пересчета. */
      SELECT COALESCE(MAX(End_LogID), CurrentConfirmedID) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE Mobitel_ID = MobitelID;   
         
      /* Находим последний LogID для пересчета. */  
      SELECT MAX(LogID)  INTO FinishLogIDSelect
      FROM datagpsbuffer_on
      WHERE Mobitel_ID = MobitelID;
      
      /* Выбираем из datagps соответствующее значение SrvPacketID  для первой
         искусственной записи */
      SELECT COALESCE(SrvPacketID, 0) INTO tmpSrvPacketID
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect);
      
      /* Помещаем в таблицу datagpslost_ontmp для анализа пропусков 
         дополнительную первую запись */
      INSERT INTO datagpslost_ontmp(LogID, SrvPacketID)
      VALUES (StartLogIDSelect, tmpSrvPacketID);
    END IF; -- IF MinLogID_n < MaxEndLogID   
    
    /********** END PREPARE **********/
    
    /**********  INSERT ROWS **********/
    
    /* Заполнение временной таблицы последними данными телетрека
       для анализа разрывов */
    INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      SELECT LogID, SrvPacketID
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND (LogID >= StartLogIDSelect)
        AND (LogID <= FinishLogIDSelect)
      ORDER BY LogID;
            
    /* Формирование команды-вставки в таблицу пропущенных диапазонов */
    SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    WHILE DataExists = 1 DO
      IF FirstLogID = -9999999 THEN
        /* Первое значение в списке */
        SET FirstLogID = NewLogID;
        SET FirstSrvPacketID = NewSrvPacketID;
      ELSE
        /* Основная ветка расчета*/
        SET SecondLogID = NewLogID;
        SET SecondSrvPacketID = NewSrvPacketID;
        
        IF (SecondLogID - FirstLogID) > 1 THEN
          /* Найден разрыв */
          CALL OnInsertDatagpsLost(MobitelID, FirstLogID, SecondLogID,  
            FirstSrvPacketID, SecondSrvPacketID);
          
          IF NeedOptimize = 0 THEN
            SET NeedOptimize = 1; 
          END IF;
        END IF;
        /* Переместим текущее второе значение на первую позицию
           для участия в следующей итерации в качестве начального значения */
        SET  FirstLogID = SecondLogID;
        SET FirstSrvPacketID = SecondSrvPacketID;
      END IF;  
      FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    END WHILE;
    CLOSE CursorNewLogID;
 
    /********** END INSERT ROWS **********/
               
    /********** UPDATE MOBITELS **********/
    
    /* Если пропущенных записей у данного телетрека нет - 
       тогда ConfirmedID будет равен максимальному LogID 
       данного телетрека. Иначе - минимальный Begin_LogID
       из таблицы пропусков */
    SELECT MIN(Begin_LogID) INTO NewConfirmedID
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;
       
    IF NewConfirmedID IS NULL THEN
      SELECT MAX(LogID) INTO NewConfirmedID
      FROM datagps
      WHERE Mobitel_ID = MobitelID;
    END IF;  
    
    /* Обновим ConfirmedID если надо */
    UPDATE mobitels
    SET ConfirmedID = NewConfirmedID
    WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);
    
    /********** END UPDATE MOBITELS **********/
        
    TRUNCATE TABLE datagpslost_ontmp;
  END IF; -- IF MaxEndLogID IS NOT NULL  
END;


DROP PROCEDURE IF EXISTS OnLostDataGPS2;
CREATE PROCEDURE OnLostDataGPS2(IN MobitelID INTEGER(11), OUT NeedOptimize TINYINT)
  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT 'Модификация диапазонов пропусков. Анализ дублирующих данных заданного телетрека.'
BEGIN
    /********** DECLARE **********/
    
  DECLARE NewConfirmedID INT DEFAULT 0; /* Новое значение ConfirmedID */
  DECLARE BeginLogID INT; /* Begin_LogID в курсоре CursorLostRanges */
  DECLARE EndLogID INT; /* End_LogID в курсоре CursorLostRanges */
  DECLARE BeginSrvPacketID BIGINT; /* Begin_SrvPacketID в курсоре CursorLostRanges */
  DECLARE EndSrvPacketID BIGINT; /* End_SrvPacketID в курсоре CursorLostRanges */
  DECLARE RowCountForAnalyze INT DEFAULT 0; /* Количество записей для анализа */
  DECLARE DataExists TINYINT DEFAULT 1; /* Флаг наличия данных после фетча */
    
  /* Курсор для прохода по диапазонам пропусков телетрека */
  DECLARE CursorLostRanges CURSOR FOR
    SELECT Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID 
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;    

  /* Курсор для прохода по новым LogID */
  DECLARE CursorNewLogID CURSOR FOR
    SELECT LogID, SrvPacketID 
    FROM datagpslost_ontmp
    ORDER BY SrvPacketID;    
    
  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  /********** END DECLARE **********/

  SET NeedOptimize = 0;
  
  TRUNCATE TABLE datagpslost_ontmp;
  
  /* Перебираем диапазоны и сокращаем разрывы если это возможно */
  SET DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  WHILE DataExists = 1 DO
    /* Заполнение временной таблицы дублирующими данными телетрека
       для анализа разрывов */
    INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      SELECT DISTINCT LogID, SrvPacketID
      FROM datagpsbuffer_ontmp
      WHERE (Mobitel_ID = MobitelID) AND (SrvPacketID > BeginSrvPacketID)
        AND (SrvPacketID < EndSrvPacketID)
      ORDER BY SrvPacketID;
      
    /* Если есть данные модифицируем диапазон */  
    SELECT COUNT(1) INTO RowCountForAnalyze
    FROM datagpslost_ontmp;  
    
    IF RowCountForAnalyze > 0 THEN  
      /* Добавляем первую и последнюю записи для анализа */  
      INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
      VALUES (BeginLogID, BeginSrvPacketID), (EndLogID, EndSrvPacketID);
      
      /* Удаляем старый диапазон */
      DELETE FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);
       
      /**********  INSERT ROWS **********/             
      BEGIN
        DECLARE NewLogID INT; /* Значение LogID в курсоре CursorNewLogID */
        DECLARE NewSrvPacketID BIGINT; /* Значение SrvPacketID в курсоре CursorNewLogID */
        DECLARE FirstLogID INT DEFAULT -9999999; /* Первое значение LogID для анализа */
        DECLARE SecondLogID INT; /* Второе значение LogID для анализа */
        DECLARE FirstSrvPacketID BIGINT; /* Первое значение SrvPacketID */
        DECLARE SecondSrvPacketID BIGINT; /* Второе значение SrvPacketID */
        
        /* Обработчик отсутствующей записи при FETCH следующей записи */
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
        
        /* Формирование команды-вставки в таблицу пропущенных диапазонов */
        SET DataExists = 1;
        OPEN CursorNewLogID;
        FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        WHILE DataExists = 1 DO
          IF FirstLogID = -9999999 THEN
            /* Первое значение в списке */
            SET FirstLogID = NewLogID;
            SET FirstSrvPacketID = NewSrvPacketID;
          ELSE
            /* Основная ветка расчета*/
            SET SecondLogID = NewLogID;
            SET SecondSrvPacketID = NewSrvPacketID;
            
            IF (SecondSrvPacketID - FirstSrvPacketID) > 1 THEN
              /* Найден разрыв */
              CALL OnInsertDatagpsLost(MobitelID, FirstLogID, SecondLogID,  
                FirstSrvPacketID, SecondSrvPacketID);

              IF NeedOptimize = 0 THEN
                SET NeedOptimize = 1; 
              END IF;
            END IF;
            /* Переместим текущее второе значение на первую позицию
               для участия в следующей итерации в качестве начального значения */
            SET FirstLogID = SecondLogID;
            SET FirstSrvPacketID = SecondSrvPacketID;
          END IF;  
          FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        END WHILE;
        CLOSE CursorNewLogID;
      END;
      /********** END INSERT ROWS **********/  

      TRUNCATE TABLE datagpslost_ontmp;
    END IF;
      
    FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  END WHILE;
  CLOSE CursorLostRanges;
  
  /********** UPDATE MOBITELS **********/
    
  /* Если пропущенных записей у данного телетрека нет - 
     тогда ConfirmedID будет равен максимальному LogID 
     данного телетрека. Иначе - минимальный Begin_LogID
     из таблицы пропусков */
  SELECT MIN(Begin_LogID) INTO NewConfirmedID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;
       
  IF NewConfirmedID IS NULL THEN
    SELECT MAX(LogID) INTO NewConfirmedID
    FROM datagps
    WHERE Mobitel_ID = MobitelID;
  END IF;  
    
  /* Обновим ConfirmedID если надо */
  UPDATE mobitels
  SET ConfirmedID = NewConfirmedID
  WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);
    
  /********** END UPDATE MOBITELS **********/
END;


DROP PROCEDURE IF EXISTS OnCorrectInfotrackLogId;
CREATE PROCEDURE OnCorrectInfotrackLogId()
  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT 'Корректировка значений LogId и InMobitelID в данных инфотреков'
BEGIN
  DECLARE MobitelIDInCursor INT; /* Текущее значение MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* Флаг наличия данных после фетча */
  DECLARE MaxLogId INT DEFAULT 0; /* Максимальный LogId для данного дивайса */
  DECLARE SrvPacketIDInCursor BIGINT DEFAULT 0; /* Текущее значение серверного пакета */
    
  /* Курсор по инфотрекам в множестве новых данных. 
     Все инфотреки начинаются с буквы I */
  DECLARE CursorInfoTrack CURSOR FOR
    SELECT DISTINCT buf.Mobitel_ID
    FROM datagpsbuffer_on buf 
      JOIN mobitels m ON (buf.mobitel_id = m.mobitel_id)
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID) 
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND 
      (c.devIdShort LIKE "I%") AND (buf.LogId = 0)
    ORDER BY 1;
    
  /* Курсор по данным телетрека */
  DECLARE CursorInfoTrackUpdateSet CURSOR FOR
    SELECT SrvPacketID 
    FROM datagpsbuffer_on
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId = 0)
    ORDER BY UnixTime;  

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* Находим максимальный LogId для данного инфотрека */
    SELECT COALESCE(MAX(LogID), 0) INTO	MaxLogId
    FROM DataGps
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* Обновляем LogId и InMobitelID в наборе новых данных текущего инфотрека */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    WHILE DataExists = 1 DO
      SET MaxLogId = MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET LogId = MaxLogId, InMobitelID = MaxLogId
      WHERE SrvPacketID = SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    END WHILE;
    CLOSE CursorInfoTrackUpdateSet;
 
    SET DataExists = 1;
    FETCH CursorInfoTrack INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorInfoTrack;
END;


DROP PROCEDURE IF EXISTS OnDeleteDuplicates;
CREATE PROCEDURE OnDeleteDuplicates()
  SQL SECURITY INVOKER
  COMMENT 'Удаление повторяющихся строк из datagpsbuffer_on'
BEGIN
  /* Удаляет дубликаты строк из таблицы datagpsbuffer_on. Критерием поиска
   являются одинаковые значения в полях Mobitel_Id + LogID. Из множества
   повторяющихся записей в таблице datagpsbuffer_on останется только одна - 
   последняя принятая телематическим сервером, т.е. с максимальным SrvPacketID. */
   
  /* Текущий MobitelId в курсоре CursorDuplicateGroups */
  DECLARE MobitelIdInCursor INT DEFAULT 0; 
  /* Текущий LogID в курсоре CursorDuplicateGroups */
  DECLARE LogIdInCursor INT DEFAULT 0; 
  /* Max SrvPacketId в множестве строк текущей группы дубликатов */
  DECLARE MaxSrvPacketId BIGINT DEFAULT 0; 
  DECLARE DataExists TINYINT DEFAULT 1;

  /* Курсор для прохода по всем группам дубликатов */  
  DECLARE CursorDuplicateGroups CURSOR FOR
    SELECT Mobitel_Id, LogId 
    FROM datagpsbuffer_on
    GROUP BY Mobitel_Id, LogId 
    HAVING COUNT(LogId) > 1;
    
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  WHILE DataExists = 1 DO
    SELECT MAX(SrvPacketID) INTO MaxSrvPacketId
    FROM datagpsbuffer_on 
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor);
         
    DELETE FROM datagpsbuffer_on  
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor) 
      AND (SrvPacketID < MaxSrvPacketId);
        
    FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  END WHILE;
  CLOSE CursorDuplicateGroups; 
END;


DROP PROCEDURE IF EXISTS OnTransferBuffer;
CREATE PROCEDURE OnTransferBuffer()
  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT 'Переносит GPS данные из буферной таблицы в datagps'
BEGIN
  DECLARE MobitelIDInCursor INT; /* Текущее значение MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* Флаг наличия данных после фетча */
  DECLARE NeedOptimize TINYINT DEFAULT 0; /* Нужна оптимизация или нет */
  DECLARE NeedOptimizeParam TINYINT;
  
  DECLARE TmpMaxUnixTime INT; /* Временная переменная для хранения Max UnixTime из таблицы online */

  /* Курсор по телетрекам в множестве новых данных */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_on;
  /* Курсор по телетрекам в множестве повторных данных */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_ontmp;  

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  CALL OnDeleteDuplicates();
  CALL OnCorrectInfotrackLogId();
  
  /* Сохраняем записи, которые уже есть в БД, для второго этапа анализа пропусков 
     Используется процедурой OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp (Mobitel_ID, LogID, SrvPacketID)
    SELECT b.Mobitel_ID, b.LogID, b.SrvPacketID
    FROM datagpsbuffer_on b LEFT JOIN datagps d ON
      (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL;
       
  /* -------------------- Заполнение таблицы Online ---------------------- */  
  /* Удаление из таблицы online дублирующих строк таблицы datagpsbuffer_on */
  /* по полям Mobitel_ID и LogID */
  DELETE o
  FROM `online` o, datagpsbuffer_on dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);
  
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* Нахождение Max значения UnixTime в таблице online для заданного
       телетрека. p.s. UnixTime в таблице online только валидный */
    SELECT COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online FORCE INDEX (IDX_Mobitelid)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* Вставка новых записей (максимум 100)каждого телетрека в online */
    INSERT INTO `online` (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs
    FROM (
      SELECT *
      FROM datagpsbuffer_on
      WHERE (Mobitel_ID = MobitelIDInCursor)  AND 
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
      GROUP BY Mobitel_ID, LogID
      ORDER BY UnixTime DESC
      LIMIT 100) AS T
    ORDER BY UnixTime ASC;

    /* Удаление строк из таблицы оперативных данных не удовлетворяющие
       условиям хранения - не являются последними 100 строками */
    DELETE FROM `online`
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < 
      (SELECT COALESCE(MIN(UnixTime), 0) 
       FROM (
         SELECT UnixTime
         FROM `online`
         WHERE Mobitel_ID = MobitelIDInCursor 
         ORDER BY UnixTime DESC
         LIMIT 100) T1)); 
  
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */ 
  
  /* Зачем-то нужен апдейт ... */
  UPDATE datagps d, datagpsbuffer_on b SET
    d.Latitude = b.Latitude,
    d.Longitude = b.Longitude,
    d.Altitude = b.Altitude,
    d.UnixTime = b.UnixTime,
    d.Speed = b.Speed,
    d.Direction = b.Direction,
    d.Valid = b.Valid,
    d.InMobitelID = b.LogID,
    d.Events = b.Events,
    d.Sensor1 = b.Sensor1,
    d.Sensor2 = b.Sensor2,
    d.Sensor3 = b.Sensor3,
    d.Sensor4 = b.Sensor4,
    d.Sensor5 = b.Sensor5,
    d.Sensor6 = b.Sensor6,
    d.Sensor7 = b.Sensor7,
    d.Sensor8 = b.Sensor8,
    d.whatIs = b.whatIs,
    d.Counter1 = b.Counter1,
    d.Counter2 = b.Counter2,
    d.Counter3 = b.Counter3,
    d.Counter4 = b.Counter4,
    d.SrvPacketID = b.SrvPacketID
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    AND (d.SrvPacketID < b.SrvPacketID);

  /* Удалим записи которые участвовали в апдейте */
  DELETE b
  FROM datagps d, datagpsbuffer_on b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  /* Вставка новых записей*/
  INSERT INTO datagps (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, InMobitelID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs, SrvPacketID)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, LogID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs, SrvPacketID
  FROM datagpsbuffer_on
  GROUP BY Mobitel_ID, LogID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ------ Обновление времени последней вставки записей ------- */
    UPDATE mobitels SET LastInsertTime = UNIX_TIMESTAMP(NOW())
    WHERE Mobitel_ID = MobitelIDInCursor;
       
    /* ------ Обновление списка пропущенных записей -------------- */
    CALL OnLostDataGPS(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;

  /* Очистка буфера */
  TRUNCATE TABLE datagpsbuffer_on;
    
  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ---- Обновление списка пропущенных записей по второму алгоритму --- */
    CALL OnLostDataGPS2(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsUpdateSet;
  
  /* Очистка вспомогательного буфера */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  /* Были изменения - сделаем оптимизацию */
  IF NeedOptimize = 1 THEN
    OPTIMIZE TABLE datagpslost_on;
  END IF;            
END;

DROP PROCEDURE IF EXISTS OnDeleteLostRange;
CREATE PROCEDURE OnDeleteLostRange(IN MobitelID INTEGER(11), IN BeginSrvPacketID BIGINT)
  NOT DETERMINISTIC
  CONTAINS SQL                          
  SQL SECURITY INVOKER
  COMMENT 'Удаление указанного диапазона пропусков из datagpslost_on.'
BEGIN
  /* Определяем является ли запись, предназначенная для удаления,
     первой в списке заданного телетрека. Если является - тогда
     внесем поправку в значение ConfirmedID */  
     
  DECLARE MinBeginSrvPacketID BIGINT; -- Минимальное значение Begin_SrvPacketID для данного телетрека
  DECLARE NewConfirmedID INT; -- Новое значение ConfirmedID  

  /* Нахождение минимального значения Begin_SrvPacketID */  
  SELECT MIN(Begin_SrvPacketID) INTO MinBeginSrvPacketID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;
  
  /* Если есть данные о пропусках у телетрека - продолжим */
  IF MinBeginSrvPacketID IS NOT NULL THEN
    /* Если удаляем первую запись - запомним новое значение ConfirmedID */
    IF MinBeginSrvPacketID = BeginSrvPacketID  THEN
      SELECT End_LogID INTO NewConfirmedID
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID);
    END IF;
  
    /* Удаление записи */
    DELETE FROM  datagpslost_on
    WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID);
    
    /* Если удалили первую запись - обновим ConfirmedID */  
    IF MinBeginSrvPacketID = BeginSrvPacketID  THEN
      UPDATE mobitels
      SET ConfirmedID = NewConfirmedID
      WHERE (Mobitel_ID = MobitelID) AND (NewConfirmedID > ConfirmedID);
    END IF;
  END IF;  
END;

DROP PROCEDURE IF EXISTS delDupSrvPacket;
CREATE PROCEDURE delDupSrvPacket()
  NOT DETERMINISTIC
  CONTAINS SQL                          
  SQL SECURITY INVOKER
  COMMENT 'Удаляет дубликаты SrvPacketID из таблицы datagpsbuffer_on64'
BEGIN
DROP TEMPORARY TABLE IF EXISTS tmp64;
DROP TABLE IF EXISTS tmp64;

CREATE TEMPORARY TABLE tmp64 SELECT Mobitel_ID,
  Latitude,
  Longitude,
  Direction,
  Acceleration,
  UnixTime,
  Speed,
  Valid,
  Satellites,
  RssiGsm,
  `Events`,
  SensorsSet,
  Sensors,
  Voltage,
  DGPS,
  LogID,
  SrvPacketID
FROM datagpsbuffer_on64 d
GROUP BY d.SrvPacketID;

  TRUNCATE TABLE datagpsbuffer_on64;

  INSERT INTO datagpsbuffer_on64 SELECT Mobitel_ID,
  Latitude,
  Longitude,
  Direction,
  Acceleration,
  UnixTime,
  Speed,
  Valid,
  Satellites,
  RssiGsm,
  `Events`,
  SensorsSet,
  Sensors,
  Voltage,
  DGPS,
  LogID,
  SrvPacketID
FROM tmp64 d;

DROP TEMPORARY TABLE IF EXISTS tmp64;
DROP TABLE IF EXISTS tmp64;
END;

DROP PROCEDURE IF EXISTS OnListMobitelConfig64;
CREATE PROCEDURE OnListMobitelConfig64()
  NOT DETERMINISTIC
  CONTAINS SQL                          
  SQL SECURITY INVOKER
  COMMENT 'Выборка настроек телетреков которые могут работать в online режиме для datagps и datagps64'
BEGIN
  DECLARE MobitelIDInCursor INT;
  DECLARE Is64Cursor tinyint;
  DECLARE DevIdShortInCursor CHAR(4);
  DECLARE DataExists TINYINT DEFAULT 1;

  /* Курсор в множестве настроенных телетреков */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS MobitelID, m.Is64bitPackets AS Is64, c.devIdShort AS DevIdShort
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID)
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND
      (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')
    ORDER BY m.Mobitel_ID;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* Временная таблица настроек телетрека */
  CREATE TEMPORARY TABLE tmpMobitelsConfig (
    MobitelID INT NOT NULL,
    Is64 tinyint NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL,
    LastPacketID64 BIGINT NOT NULL) ENGINE = MEMORY;

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor, Is64Cursor, DevIdShortInCursor;
  WHILE DataExists = 1 DO

    INSERT INTO tmpMobitelsConfig SELECT MobitelIDInCursor, Is64Cursor, DevIdShortInCursor,
      (SELECT COALESCE(MAX(d.SrvPacketID), 0)
       FROM datagps d
       WHERE d.Mobitel_ID = MobitelIDInCursor) AS LastPacketID,
      (SELECT COALESCE(MAX(t.SrvPacketID), 0)
       FROM datagps64 t
       WHERE t.Mobitel_ID = MobitelIDInCursor) AS LastPacketID64;

    FETCH CursorMobitels INTO MobitelIDInCursor, Is64Cursor, DevIdShortInCursor;
  END WHILE;
  CLOSE CursorMobitels;

  SELECT MobitelID, Is64, DevIdShort, LastPacketID, LastPacketID64
  FROM tmpMobitelsConfig;

  DROP TEMPORARY TABLE tmpMobitelsConfig;
END;

ALTER TABLE datagpsbuffer_on MODIFY COLUMN SrvPacketID BIGINT NOT NULL DEFAULT '0';

ALTER TABLE datagpsbuffer_ontmp MODIFY COLUMN SrvPacketID BIGINT NOT NULL DEFAULT '0';

ALTER TABLE datagpslost_on MODIFY COLUMN Begin_SrvPacketID BIGINT NOT NULL DEFAULT '0';

ALTER TABLE datagpslost_on MODIFY COLUMN End_SrvPacketID BIGINT NOT NULL DEFAULT '0';

DROP PROCEDURE IF EXISTS tmpAlterDmTables;
CREATE PROCEDURE tmpAlterDmTables()
BEGIN
DROP TABLE IF EXISTS datagpsbuffer_on;
CREATE TABLE IF NOT EXISTS datagpsbuffer_on (
  Mobitel_ID int(11) NOT NULL default 0,
  Message_ID int(11) default 0,
  Latitude int(11) NOT NULL default 0,
  Longitude int(11) NOT NULL default 0,
  Altitude int(11) default NULL,
  UnixTime int(11) NOT NULL default 0,
  Speed smallint(6) NOT NULL default 0,
  Direction int(11) NOT NULL default 0,
  Valid tinyint(4) NOT NULL default 1,
  InMobitelID int(11) default NULL,
  Events int(10) unsigned default 0,
  Sensor1 tinyint(4) unsigned default 0,
  Sensor2 tinyint(4) unsigned default 0,
  Sensor3 tinyint(4) unsigned default 0,
  Sensor4 tinyint(4) unsigned default 0,
  Sensor5 tinyint(4) unsigned default 0,
  Sensor6 tinyint(4) unsigned default 0,
  Sensor7 tinyint(4) unsigned default 0,
  Sensor8 tinyint(4) unsigned default 0,
  LogID int(11) NOT NULL default 0,
  isShow tinyint(4) default 0,
  whatIs smallint(6) NOT NULL default 0,
  Counter1 int(11) NOT NULL default -1,
  Counter2 int(11) NOT NULL default -1,
  Counter3 int(11) NOT NULL default -1,
  Counter4 int(11) NOT NULL default -1,
  SrvPacketID BIGINT NOT NULL DEFAULT 0 COMMENT 'ID серверного пакета в состав которого входит эта запись',
  INDEX IDX_MobitelidLogid USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid USING BTREE (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_SrvPacketID (SrvPacketID)
) ENGINE=MEMORY DEFAULT CHARSET=cp1251 COMMENT 'Буфер онлайн данных';
  END;


DROP TABLE IF EXISTS datagpsbuffer_ontmp;
CREATE TABLE IF NOT EXISTS datagpsbuffer_ontmp (
  Mobitel_ID INT(11) NOT NULL DEFAULT 0,
  LogID INT(11) NOT NULL DEFAULT 0,
  SrvPacketID BIGINT NOT NULL,
  INDEX IDX_MobitelidSrvpacketid USING BTREE (Mobitel_ID, SrvPacketID),
  INDEX IDX_Mobitelid USING BTREE (Mobitel_ID)
) ENGINE = MEMORY DEFAULT CHARSET=cp1251 COMMENT 'Временный вспомогательный буфер онлайн данных';

DROP TABLE IF EXISTS online;
CREATE TABLE IF NOT EXISTS online (
  DataGps_ID INT(11) AUTO_INCREMENT,
  Mobitel_ID INT(11) DEFAULT 0,
  Message_ID INT(11) DEFAULT 0,
  Latitude INT(11) NOT NULL DEFAULT 0,
  Longitude INT(11) NOT NULL DEFAULT 0,
  Altitude INT(11) DEFAULT 0,
  UnixTime INT(11) NOT NULL DEFAULT 0,
  Speed SMALLINT(6) NOT NULL DEFAULT 0,
  Direction INT(11) NOT NULL DEFAULT 0,
  Valid TINYINT(4) NOT NULL DEFAULT 1,
  InMobitelID INT(11) DEFAULT 0,
  Events INT(10) UNSIGNED DEFAULT 0,
  Sensor1 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor2 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor3 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor4 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor5 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor6 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor7 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor8 TINYINT(4) UNSIGNED DEFAULT 0,
  LogID INT(11) DEFAULT 0,
  isShow TINYINT(4) DEFAULT 0,
  whatIs SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  Counter1 INT(11) NOT NULL DEFAULT -1,
  Counter2 INT(11) NOT NULL DEFAULT -1,
  Counter3 INT(11) NOT NULL DEFAULT -1,
  Counter4 INT(11) NOT NULL DEFAULT -1,
  PRIMARY KEY (DataGps_ID),
  INDEX IDX_MobitelidLogid USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtimeValid USING BTREE (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_MobitelidUnixtime USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_Mobitelid USING BTREE (Mobitel_ID)
) ENGINE = INNODB COMMENT 'Оперативные данные';

DROP TABLE IF EXISTS datagpslost_on;
CREATE TABLE IF NOT EXISTS datagpslost_on (
  Mobitel_ID INT(11) NOT NULL DEFAULT 0,
  Begin_LogID INT(11) NOT NULL DEFAULT 0 COMMENT 'Значение LogID после которого начинается разрыв',
  End_LogID INT(11) NOT NULL DEFAULT 0 COMMENT 'Значение LogID перед которым завершается разрыв',
  Begin_SrvPacketID BIGINT NOT NULL COMMENT 'Идентификаор серверного пакета в который входит запись с Begin_LogID',
  End_SrvPacketID BIGINT NOT NULL COMMENT 'Идентификаор серверного пакета в который входит запись с End_LogID',
  PRIMARY KEY PK_DatagpslostOn(Mobitel_ID, Begin_LogID),
  INDEX IDX_MobitelID (Mobitel_ID),
  UNIQUE INDEX IDX_MobitelID_BeginSrvPacketID (Mobitel_ID, Begin_SrvPacketID)
) ENGINE=InnoDB COMMENT='Пропуски в данных DataGPS, приходящих от Online сервера';

DROP TABLE IF EXISTS datagpslost_ontmp;
CREATE TABLE IF NOT EXISTS datagpslost_ontmp (
  LogID INT(11) NOT NULL DEFAULT 0,
  SrvPacketID BIGINT NOT NULL
) ENGINE = MEMORY COMMENT = 'Используется исключительно процедурой OnLostDataGPS';


DROP PROCEDURE IF EXISTS tmpAlterDmTables;
CREATE PROCEDURE tmpAlterDmTables()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Временная процедура модификации таблиц'
BEGIN
  IF EXISTS (
    SELECT NULL 
    FROM information_schema.TABLE_CONSTRAINTS
    WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND (TABLE_NAME = "datagpslost_on") 
      AND (CONSTRAINT_NAME = "IDX_MobitelID_BeginLogID"))
  THEN
    ALTER TABLE datagpslost_on DROP INDEX IDX_MobitelID_BeginLogID;
  END IF;

	IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "datagps64")
	THEN
	ALTER TABLE datagps64 MODIFY DGPS varchar(32);
	END IF;

	IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "online64")
	THEN
	ALTER TABLE online64 MODIFY DGPS varchar(32);
	END IF;
END;

CALL tmpAlterDmTables;
DROP PROCEDURE IF EXISTS tmpAlterDmTables;
