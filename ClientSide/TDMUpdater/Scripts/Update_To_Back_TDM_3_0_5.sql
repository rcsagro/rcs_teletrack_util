/* Begin this file */
DROP TABLE IF EXISTS datagpsbuffer_on64;
CREATE TABLE IF NOT EXISTS datagpsbuffer_on64 (
  Mobitel_ID int(11) NOT NULL DEFAULT 0 COMMENT '��� ���������',
  Latitude int(11) NOT NULL DEFAULT 0 COMMENT '������',
  Longitude int(11) NOT NULL DEFAULT 0 COMMENT '�������',
  Direction tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������/1.5',
  Acceleration int(11) NOT NULL DEFAULT 0 COMMENT '��������� �������� �� GPS �� ��������� �������',
  UnixTime int(11) NOT NULL DEFAULT 0,
  Speed smallint(6) NOT NULL DEFAULT 0 COMMENT '����� ������ � ��� (0,1 ��/�)',
  Valid tinyint(1) NOT NULL DEFAULT 1,
  Satellites tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '���������� ��������� ',
  RssiGsm tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '������� GSM �������',
  Events int(11) NOT NULL DEFAULT 0 COMMENT '����� �������',
  SensorsSet tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '��� ������ ��������',
  Sensors varbinary(50) NOT NULL DEFAULT '0' COMMENT '�������� ��������',
  Voltage tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '���������� ������� �������� ���������/1.5 �� 0 �� 38,2',
  DGPS varchar(32) NOT NULL DEFAULT '0' COMMENT '������ ���������������� ������� GPS - ������,������,�������',
  LogID int(11) NOT NULL DEFAULT 0 COMMENT '������������� ������ � ������� ���������',
  SrvPacketID bigint(20) NOT NULL DEFAULT 0 COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������',
  INDEX IDX_MobitelidLogid_64 USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime_64 USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid_64 USING BTREE (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_SrvPacketID_64 USING BTREE (SrvPacketID),
  CONSTRAINT FK_datagps64_mobitels_Mobitel_ID_1 FOREIGN KEY (Mobitel_ID)
  REFERENCES mobitels (Mobitel_ID) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

DROP TABLE IF EXISTS datagpsbuffer_ontmp64;
CREATE TABLE IF NOT EXISTS datagpsbuffer_ontmp64 (
  Mobitel_ID int(11) NOT NULL DEFAULT 0 COMMENT '��� ���������',
  LogID int(11) UNSIGNED DEFAULT 0 COMMENT '������������� ������ � ������� ���������',
  SrvPacketID bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������',
  INDEX IDX_Mobitelid64 (Mobitel_ID),
  INDEX IDX_MobitelidSrvpacketid64 (Mobitel_ID, SrvPacketID),
  CONSTRAINT FK_datagps64_mobitels_Mobitel_ID_2 FOREIGN KEY (Mobitel_ID)
  REFERENCES mobitels (Mobitel_ID) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

DROP TABLE IF EXISTS online64;
CREATE TABLE IF NOT EXISTS online64 (
  DataGps_ID int(11) NOT NULL AUTO_INCREMENT,
  Mobitel_ID int(11) NOT NULL DEFAULT 0 COMMENT '��� ���������',
  Latitude int(11) NOT NULL DEFAULT 0 COMMENT '������',
  Longitude int(11) NOT NULL DEFAULT 0 COMMENT '�������',
  Direction tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������/1.5',
  Acceleration int(11) DEFAULT 0 COMMENT '��������� �������� �� GPS �� ��������� �������',
  UnixTime int(11) NOT NULL DEFAULT 0,
  Speed smallint(6) NOT NULL DEFAULT 0 COMMENT '����� ������ � ��� (0,1 ��/�)',
  Valid tinyint(1) NOT NULL DEFAULT 1,
  Satellites tinyint(1) UNSIGNED DEFAULT 0 COMMENT '���������� ��������� ',
  RssiGsm tinyint(1) UNSIGNED DEFAULT 0 COMMENT '������� GSM �������',
  Events int(11) DEFAULT 0 COMMENT '����� �������',
  SensorsSet tinyint(1) DEFAULT 0 COMMENT '��� ������ ��������',
  Sensors varbinary(50) DEFAULT '0' COMMENT '�������� ��������',
  Voltage tinyint(4) UNSIGNED DEFAULT 0 COMMENT '���������� ������� �������� ���������/1.5 �� 0 �� 38,2',
  DGPS varbinary(32) DEFAULT '0' COMMENT '������ ���������������� ������� GPS - ������,������,�������',
  LogID int(11) DEFAULT 0 COMMENT '������������� ������ � ������� ���������',
  SrvPacketID bigint(20) NOT NULL DEFAULT 0 COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������',
  PRIMARY KEY (DataGps_ID),
  INDEX IDX_Mobitelid64 (Mobitel_ID),
  INDEX IDX_MobitelidLogid64 (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime64 (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid64 (Mobitel_ID, UnixTime, Valid)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;


DROP PROCEDURE IF EXISTS OnCorrectInfotrackLogId64;
CREATE PROCEDURE OnCorrectInfotrackLogId64 ()
SQL SECURITY INVOKER
COMMENT '������������� �������� LogId � InMobitelID � ������ ����������'
BEGIN
  DECLARE MobitelIDInCursor int; /* ������� �������� MobitelID */
  DECLARE DataExists tinyint DEFAULT 1; /* ���� ������� ������ ����� ����� */
  DECLARE MaxLogId int DEFAULT 0; /* ������������ LogId ��� ������� ������� */
  DECLARE SrvPacketIDInCursor bigint DEFAULT 0; /* ������� �������� ���������� ������ */

  /* ������ �� ���������� � ��������� ����� ������. 
     ��� ��������� ���������� � ����� I */
  DECLARE CursorInfoTrack CURSOR FOR
  SELECT DISTINCT
    buf.Mobitel_ID
  FROM datagpsbuffer_on64 buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE (c.InternalMobitelConfig_ID = (SELECT
      intConf.InternalMobitelConfig_ID
    FROM internalmobitelconfig intConf
    WHERE (intConf.ID = c.ID)
    ORDER BY intConf.InternalMobitelConfig_ID DESC
    LIMIT 1)) AND
  (c.devIdShort LIKE "I%") AND (buf.LogId = 0)
  ORDER BY 1;

  /* ������ �� ������ ��������� */
  DECLARE CursorInfoTrackUpdateSet CURSOR FOR
  SELECT
    SrvPacketID
  FROM datagpsbuffer_on64
  WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId = 0)
  ORDER BY UnixTime;

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ������� ������������ LogId ��� ������� ��������� */
    SELECT
      COALESCE(MAX(LogID), 0) INTO MaxLogId
    FROM DataGps
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ��������� LogId � InMobitelID � ������ ����� ������ �������� ��������� */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    WHILE DataExists = 1 DO
      SET MaxLogId = MaxLogId + 1;

      UPDATE datagpsbuffer_on64
      SET LogId = MaxLogId,
          InMobitelID = MaxLogId
      WHERE SrvPacketID = SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    END WHILE;
    CLOSE CursorInfoTrackUpdateSet;

    SET DataExists = 1;
    FETCH CursorInfoTrack INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorInfoTrack;
END;

DROP PROCEDURE IF EXISTS OnDeleteDuplicates64;
CREATE PROCEDURE OnDeleteDuplicates64 ()
SQL SECURITY INVOKER
COMMENT '�������� ������������� ����� �� datagpsbuffer_on64'
BEGIN
  /* ������� ��������� ����� �� ������� datagpsbuffer_on64. ��������� ������
   �������� ���������� �������� � ����� Mobitel_Id + LogID. �� ���������
   ������������� ������� � ������� datagpsbuffer_on64 ��������� ������ ���� - 
   ��������� �������� �������������� ��������, �.�. � ������������ SrvPacketID. */

  /* ������� MobitelId � ������� CursorDuplicateGroups */
  DECLARE MobitelIdInCursor int DEFAULT 0;
  /* ������� LogID � ������� CursorDuplicateGroups */
  DECLARE LogIdInCursor int DEFAULT 0;
  /* Max SrvPacketId � ��������� ����� ������� ������ ���������� */
  DECLARE MaxSrvPacketId bigint DEFAULT 0;
  DECLARE DataExists tinyint DEFAULT 1;

  /* ������ ��� ������� �� ���� ������� ���������� */
  DECLARE CursorDuplicateGroups CURSOR FOR
  SELECT
    Mobitel_Id,
    LogId
  FROM datagpsbuffer_on64
  GROUP BY Mobitel_Id,
           LogId
  HAVING COUNT(LogId) > 1;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  WHILE DataExists = 1 DO
    SELECT
      MAX(SrvPacketID) INTO MaxSrvPacketId
    FROM datagpsbuffer_on64
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor);

    DELETE
      FROM datagpsbuffer_on64
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor)
      AND (SrvPacketID < MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  END WHILE;
  CLOSE CursorDuplicateGroups;
END;

DROP PROCEDURE IF EXISTS OnLostDataGPS264;
CREATE PROCEDURE OnLostDataGPS264 (IN MobitelID integer(11), OUT NeedOptimize tinyint)
SQL SECURITY INVOKER
COMMENT '����������� ���������� ���������. ������ ����������� ������ ��������� ���������.'
BEGIN
  /********** DECLARE **********/

  DECLARE NewConfirmedID int DEFAULT 0; /* ����� �������� ConfirmedID */
  DECLARE BeginLogID int; /* Begin_LogID � ������� CursorLostRanges */
  DECLARE EndLogID int; /* End_LogID � ������� CursorLostRanges */
  DECLARE BeginSrvPacketID bigint; /* Begin_SrvPacketID � ������� CursorLostRanges */
  DECLARE EndSrvPacketID bigint; /* End_SrvPacketID � ������� CursorLostRanges */
  DECLARE RowCountForAnalyze int DEFAULT 0; /* ���������� ������� ��� ������� */
  DECLARE DataExists tinyint DEFAULT 1; /* ���� ������� ������ ����� ����� */

  /* ������ ��� ������� �� ���������� ��������� ��������� */
  DECLARE CursorLostRanges CURSOR FOR
  SELECT
    Begin_LogID,
    End_LogID,
    Begin_SrvPacketID,
    End_SrvPacketID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;

  /* ������ ��� ������� �� ����� LogID */
  DECLARE CursorNewLogID CURSOR FOR
  SELECT
    LogID,
    SrvPacketID
  FROM datagpslost_ontmp
  ORDER BY SrvPacketID;

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* ���������� ��������� � ��������� ������� ���� ��� �������� */
  SET DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  WHILE DataExists = 1 DO
    /* ���������� ��������� ������� ������������ ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
      SELECT DISTINCT
        LogID,
        SrvPacketID
      FROM datagpsbuffer_ontmp64
      WHERE (Mobitel_ID = MobitelID) AND (SrvPacketID > BeginSrvPacketID)
      AND (SrvPacketID < EndSrvPacketID)
      ORDER BY SrvPacketID;

    /* ���� ���� ������ ������������ �������� */
    SELECT
      COUNT(1) INTO RowCountForAnalyze
    FROM datagpslost_ontmp;

    IF RowCountForAnalyze > 0 THEN
      /* ��������� ������ � ��������� ������ ��� ������� */
      INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
        VALUES (BeginLogID, BeginSrvPacketID), (EndLogID, EndSrvPacketID);

      /* ������� ������ �������� */
      DELETE
        FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        DECLARE NewLogID int; /* �������� LogID � ������� CursorNewLogID */
        DECLARE NewSrvPacketID bigint; /* �������� SrvPacketID � ������� CursorNewLogID */
        DECLARE FirstLogID int DEFAULT -9999999; /* ������ �������� LogID ��� ������� */
        DECLARE SecondLogID int; /* ������ �������� LogID ��� ������� */
        DECLARE FirstSrvPacketID bigint; /* ������ �������� SrvPacketID */
        DECLARE SecondSrvPacketID bigint; /* ������ �������� SrvPacketID */

        /* ���������� ������������� ������ ��� FETCH ��������� ������ */
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* ������������ �������-������� � ������� ����������� ���������� */
        SET DataExists = 1;
        OPEN CursorNewLogID;
        FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        WHILE DataExists = 1 DO
          IF FirstLogID = -9999999 THEN
            /* ������ �������� � ������ */
            SET FirstLogID = NewLogID;
            SET FirstSrvPacketID = NewSrvPacketID;
          ELSE
            /* �������� ����� �������*/
            SET SecondLogID = NewLogID;
            SET SecondSrvPacketID = NewSrvPacketID;

            IF (SecondSrvPacketID - FirstSrvPacketID) > 1 THEN
              /* ������ ������ */
              CALL OnInsertDatagpsLost(MobitelID, FirstLogID, SecondLogID,
              FirstSrvPacketID, SecondSrvPacketID);

              IF NeedOptimize = 0 THEN
                SET NeedOptimize = 1;
              END IF;
            END IF;
            /* ���������� ������� ������ �������� �� ������ �������
               ��� ������� � ��������� �������� � �������� ���������� �������� */
            SET FirstLogID = SecondLogID;
            SET FirstSrvPacketID = SecondSrvPacketID;
          END IF;
          FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        END WHILE;
        CLOSE CursorNewLogID;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END IF;

    FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  END WHILE;
  CLOSE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* ���� ����������� ������� � ������� ��������� ��� - 
     ����� ConfirmedID ����� ����� ������������� LogID 
     ������� ���������. ����� - ����������� Begin_LogID
     �� ������� ��������� */
  SELECT
    MIN(Begin_LogID) INTO NewConfirmedID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;

  IF NewConfirmedID IS NULL THEN
    SELECT
      MAX(LogID) INTO NewConfirmedID
    FROM datagps64
    WHERE Mobitel_ID = MobitelID;
  END IF;

  /* ������� ConfirmedID ���� ���� */
  UPDATE mobitels
  SET ConfirmedID = NewConfirmedID
  WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END;

DROP PROCEDURE IF EXISTS OnLostDataGPS64;
CREATE PROCEDURE OnLostDataGPS64 (IN MobitelID integer(11), OUT NeedOptimize tinyint)
SQL SECURITY INVOKER
COMMENT '����� ��������� � online ������ ��������� ��������� � ���������� ������.'
BEGIN

  /********** DECLARE **********/

  DECLARE CurrentConfirmedID int DEFAULT 0; /* ������� �������� ConfirmedID */
  DECLARE NewConfirmedID int DEFAULT 0; /* ����� �������� ConfirmedID */
  DECLARE StartLogIDSelect int DEFAULT 0; /* min LogID ��� ������� */
  DECLARE FinishLogIDSelect int DEFAULT 0; /* max LogID ��� ������� */
  DECLARE MaxEndLogID int DEFAULT 0; /* max End_LogID � ����������� ���������� ������� ��������� */

  DECLARE MinLogID_n int DEFAULT 0; /* ����������� LogID ����� ConfirmedID � ��������� ����� ������ */
  DECLARE MaxLogID_n int DEFAULT 0; /* ������������ LogID � ��������� ����� ������ */

  DECLARE DataExists tinyint DEFAULT 1; /* ���� ������� ������ ����� ����� */

  DECLARE NewLogID int; /* �������� LogID � ������� CursorNewLogID */
  DECLARE NewSrvPacketID bigint; /* �������� SrvPacketID � ������� CursorNewLogID */
  DECLARE FirstLogID int DEFAULT -9999999; /* ������ �������� LogID ��� ������� */
  DECLARE SecondLogID int; /* ������ �������� LogID ��� ������� */
  DECLARE FirstSrvPacketID bigint; /* ������ �������� SrvPacketID */
  DECLARE SecondSrvPacketID bigint; /* ������ �������� SrvPacketID */

  DECLARE tmpSrvPacketID bigint DEFAULT 0; /* �������� SrvPacketID ��� ������ ������������� ������ � ������� ����� ������, �� �������������� � ������������� */

  /* ������ ��� ������� �� ����� LogID */
  DECLARE CursorNewLogID CURSOR FOR
  SELECT
    LogID,
    SrvPacketID
  FROM datagpslost_ontmp;

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET NeedOptimize = 0;

  SELECT
    COALESCE(ConfirmedID, 0) INTO CurrentConfirmedID
  FROM mobitels
  WHERE Mobitel_ID = MobitelID;

  /* ����������� LogID � ����� ������ ������ c ������� ����� �������� */
  SELECT
    MIN(LogID) INTO MinLogID_n
  FROM datagpsbuffer_on64
  WHERE (Mobitel_ID = MobitelID) AND (LogID > CurrentConfirmedID);

  /* ������������ LogID � ����� ������ ������ c ������� ����� �������� */
  SELECT
    MAX(LogID) INTO MaxLogID_n
  FROM datagpsbuffer_on64
  WHERE Mobitel_ID = MobitelID;

  /* ������������ End_LogID � ����������� ���������� ������� ��������� */
  SELECT
    COALESCE(MAX(End_LogID), 0) INTO MaxEndLogID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;

  /* �������� ���� �� ������ ��� ������� */
  IF MinLogID_n IS NOT NULL THEN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF MinLogID_n < MaxEndLogID THEN
      /* ����  MinLogID_n < MaxEndLogID ������ ������ ������, 
       ������� �������� ������� ����� ���������. */

      /* ������� ��������� LogID ��� ���������. */
      SELECT
        COALESCE(Begin_LogID, MinLogID_n) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MinLogID_n > Begin_LogID)
      AND (MinLogID_n < End_LogID);

      /* ������� ��������� LogID ��� ���������. */
      SELECT
        COALESCE(End_LogID, MaxLogID_n) INTO FinishLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MaxLogID_n > Begin_LogID)
      AND (MaxLogID_n < End_LogID);

      /* ������ ������ ������ �� ������� ����������� ������� 
         (����� �������������, ���� �����������) */
      DELETE
        FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID >= StartLogIDSelect)
        AND (Begin_LogID <= FinishLogIDSelect);

      /* ���� ������ ������ LogID = 0 � ���� ������ ��� � DataGps, �����
         ���� �������� ������ ��������� ������ ��� �������. */
      IF (StartLogIDSelect = 0) AND (NOT EXISTS (SELECT
            1
          FROM datagps64
          WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect))) THEN

        INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
          VALUES (0, 0);
      END IF;
    ELSE
      /* ������ ����� ������. ���� �������� ��� ������� ������������� ������ - 
         ������ ������� */

      /* ������� ��������� LogID ��� ���������. */
      SELECT
        COALESCE(MAX(End_LogID), CurrentConfirmedID) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE Mobitel_ID = MobitelID;

      /* ������� ��������� LogID ��� ���������. */
      SELECT
        MAX(LogID) INTO FinishLogIDSelect
      FROM datagpsbuffer_on64
      WHERE Mobitel_ID = MobitelID;

      /* �������� �� datagps ��������������� �������� SrvPacketID  ��� ������
         ������������� ������ */
      SELECT
        COALESCE(SrvPacketID, 0) INTO tmpSrvPacketID
      FROM datagps64
      WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect);

      /* �������� � ������� datagpslost_ontmp64 ��� ������� ��������� 
         �������������� ������ ������ */
      INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
        VALUES (StartLogIDSelect, tmpSrvPacketID);
    END IF; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

    /* ���������� ��������� ������� ���������� ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
      SELECT
        LogID,
        SrvPacketID
      FROM datagps64
      WHERE (Mobitel_ID = MobitelID) AND (LogID >= StartLogIDSelect)
      AND (LogID <= FinishLogIDSelect)
      ORDER BY LogID;

    /* ������������ �������-������� � ������� ����������� ���������� */
    SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    WHILE DataExists = 1 DO
      IF FirstLogID = -9999999 THEN
        /* ������ �������� � ������ */
        SET FirstLogID = NewLogID;
        SET FirstSrvPacketID = NewSrvPacketID;
      ELSE
        /* �������� ����� �������*/
        SET SecondLogID = NewLogID;
        SET SecondSrvPacketID = NewSrvPacketID;

        IF (SecondLogID - FirstLogID) > 1 THEN
          /* ������ ������ */
          CALL OnInsertDatagpsLost(MobitelID, FirstLogID, SecondLogID,
          FirstSrvPacketID, SecondSrvPacketID);

          IF NeedOptimize = 0 THEN
            SET NeedOptimize = 1;
          END IF;
        END IF;
        /* ���������� ������� ������ �������� �� ������ �������
           ��� ������� � ��������� �������� � �������� ���������� �������� */
        SET FirstLogID = SecondLogID;
        SET FirstSrvPacketID = SecondSrvPacketID;
      END IF;
      FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    END WHILE;
    CLOSE CursorNewLogID;

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

    /* ���� ����������� ������� � ������� ��������� ��� - 
       ����� ConfirmedID ����� ����� ������������� LogID 
       ������� ���������. ����� - ����������� Begin_LogID
       �� ������� ��������� */
    SELECT
      MIN(Begin_LogID) INTO NewConfirmedID
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;

    IF NewConfirmedID IS NULL THEN
      SELECT
        MAX(LogID) INTO NewConfirmedID
      FROM datagps64
      WHERE Mobitel_ID = MobitelID;
    END IF;

    /* ������� ConfirmedID ���� ���� */
    UPDATE mobitels
    SET ConfirmedID = NewConfirmedID
    WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END IF; -- IF MaxEndLogID IS NOT NULL  
END;

DROP PROCEDURE IF EXISTS OnTransferBuffer64;
CREATE PROCEDURE OnTransferBuffer64()
  SQL SECURITY INVOKER
  COMMENT ''
BEGIN
  DECLARE MobitelIDInCursor int; /* ??????obitelID */
  DECLARE DataExists tinyint DEFAULT 1; /* */
  DECLARE NeedOptimize tinyint DEFAULT 0; /* ?? ????? ? */
  DECLARE NeedOptimizeParam tinyint;

  DECLARE TmpMaxUnixTime int; /* �??? ???? ? ??? Max UnixTime ??? online */

  /* */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
  SELECT DISTINCT
    (Mobitel_ID)
  FROM datagpsbuffer_on64;
  /* */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
  SELECT DISTINCT
    (Mobitel_ID)
  FROM datagpsbuffer_ontmp64;

  /* */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  /*DECLARE EXIT HANDLER FOR SQLEXCEPTION 
  BEGIN
    ROLLBACK;
    SELECT 'An error has occurred, operation rollbacked and the stored procedure was terminated';
  end;*/

  CALL delDupSrvPacket();
  CALL OnDeleteDuplicates64();
  /*CALL OnCorrectInfotrackLogId64();*/

  /* ??????, ?????? ?I ? ?? ?? ????? 
     ????????OnLostDataGPS264. */
  /*INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID)
    SELECT
      b.Mobitel_ID,
      b.LogID,
      b.SrvPacketID
    FROM datagpsbuffer_on64 b
      LEFT JOIN datagps64 d
        ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL GROUP BY d.SrvPacketID;*/
 DROP TEMPORARY TABLE IF EXISTS temp1;
  DROP TABLE IF EXISTS temp1;
  CREATE TEMPORARY TABLE temp1  SELECT
      b.Mobitel_ID,
      b.LogID,
      b.SrvPacketID
    FROM datagpsbuffer_on64 b
      LEFT JOIN datagps64 d
        ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL GROUP BY d.SrvPacketID;

INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID) SELECT * FROM temp1 GROUP BY SrvPacketID;

DROP TEMPORARY TABLE IF EXISTS temp1;
DROP TABLE IF EXISTS temp1;
  /* -------------------- ?????? Online ---------------------- */
  /* ??? ??? online ??????? datagpsbuffer_on64 */
  /* ????obitel_ID ?ogID */
  DELETE o
    FROM online64 o, datagpsbuffer_on64 dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);

  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* xxxx */
    SELECT
      COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online64 FORCE INDEX (IDX_Mobitelid64)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* �??????(???00)?????? online */
    INSERT INTO online64 (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, `Events`,
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS)
      SELECT
        Mobitel_ID,
        LogID,
        UnixTime,
        Latitude,
        Longitude,
        Acceleration,
        Direction,
        Speed,
        Valid,
        `Events`,
        Satellites,
        RssiGsm,
        SensorsSet,
        Sensors,
        Voltage,
        DGPS
      FROM (SELECT
          *
        FROM datagpsbuffer_on64
        WHERE (Mobitel_ID = MobitelIDInCursor) AND
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
        GROUP BY Mobitel_ID,
                 LogID
        ORDER BY UnixTime DESC
        LIMIT 100) AS T
      ORDER BY UnixTime ASC;

    /* */
    DELETE
      FROM online64
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < (SELECT
          COALESCE(MIN(UnixTime), 0)
        FROM (SELECT
            UnixTime
          FROM online64
          WHERE Mobitel_ID = MobitelIDInCursor
          ORDER BY UnixTime DESC
          LIMIT 100) T1));

    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */
  /* ???? ?? ... */
  UPDATE datagps64 d, datagpsbuffer_on64 b
  SET d.Latitude = b.Latitude,
      d.Longitude = b.Longitude,
      d.Acceleration = b.Acceleration,
      d.UnixTime = b.UnixTime,
      d.Speed = b.Speed,
      d.Direction = b.Direction,
      d.Valid = b.Valid,
      d.LogID = b.LogID,
      d.`Events` = b.`Events`,
      d.Satellites = b.Satellites,
      d.RssiGsm = b.RssiGsm,
      d.SensorsSet = b.SensorsSet,
      d.Sensors = b.Sensors,
      d.Voltage = b.Voltage,
      d.DGPS = b.DGPS,
      d.SrvPacketID = b.SrvPacketID
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  AND (d.SrvPacketID < b.SrvPacketID);

  /* ?????????? ??? */
  DELETE b
    FROM datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

    /* �??????*/ 
  INSERT INTO datagps64 (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
  Direction, Speed, Valid, `Events`,
  Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID)
    SELECT
      Mobitel_ID,
      LogID,
      UnixTime,
      Latitude,
      Longitude,
      Acceleration,
      Direction,
      Speed,
      Valid,
      `Events`,
      Satellites,
      RssiGsm,
      SensorsSet,
      Sensors,
      Voltage,
      DGPS,
      SrvPacketID
    FROM datagpsbuffer_on64
    GROUP BY Mobitel_ID,
             LogID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ------ ???????????????------- */
    UPDATE mobitels
    SET LastInsertTime = UNIX_TIMESTAMP(NOW())
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ------ ?????????????-------------- */
    CALL OnLostDataGPS64(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;

  /* ????? */
  TRUNCATE TABLE datagpsbuffer_on64;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ---- ???????????????? ???--- */
    CALL OnLostDataGPS264(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsUpdateSet;

  /* ???????????/
  TRUNCATE TABLE datagpsbuffer_ontmp64;

  /* {???? - ?? ????*/
  IF NeedOptimize = 1 THEN
    OPTIMIZE TABLE datagpslost_on;
  END IF;
END;

DROP INDEX IDX_MobitelidSrvpacketid64 ON datagpsbuffer_ontmp64;
ALTER TABLE datagpsbuffer_ontmp64 ADD INDEX IDX_MobitelidSrvpacketid64(Mobitel_ID, SrvPacketID);

DROP PROCEDURE IF EXISTS OnListMobitelConfig;
CREATE PROCEDURE OnListMobitelConfig()
  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT '������� �������� ���������� ������� ����� �������� � online ������'
BEGIN
  DECLARE MobitelIDInCursor INT;
  DECLARE DevIdShortInCursor CHAR(4);
  DECLARE DataExists TINYINT DEFAULT 1;

  /* ������ � ��������� ����������� ���������� */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS MobitelID, c.devIdShort AS DevIdShort
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID)
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND
      (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')
    ORDER BY m.Mobitel_ID;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* ��������� ������� �������� ��������� */
  CREATE TEMPORARY TABLE tmpMobitelsConfig (
    MobitelID INT NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL) ENGINE = MEMORY;

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor, DevIdShortInCursor;
  WHILE DataExists = 1 DO

    INSERT INTO tmpMobitelsConfig SELECT MobitelIDInCursor, DevIdShortInCursor,
      (SELECT COALESCE(MAX(SrvPacketID), 0)
       FROM datagps
       WHERE Mobitel_ID = MobitelIDInCursor) AS LastPacketID;

    FETCH CursorMobitels INTO MobitelIDInCursor, DevIdShortInCursor;
  END WHILE;
  CLOSE CursorMobitels;

  SELECT MobitelID, DevIdShort, LastPacketID
  FROM tmpMobitelsConfig;

  DROP TEMPORARY TABLE tmpMobitelsConfig;  
END;


DROP PROCEDURE IF EXISTS OnInsertDatagpsLost;
CREATE PROCEDURE OnInsertDatagpsLost(
  IN MobitelID INT, 
  IN BeginLogID INT, 
  IN EndLogID INT,
  IN BeginSrvPacketID BIGINT, 
  IN EndSrvPacketID BIGINT)

  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT '������� ������ � ������� datagpslost_on.'
BEGIN
  /* ���� ������������� ������ � ��������� Mobitel_ID � Begin_LogID
     � ������� datagpslost_on */
  DECLARE MobitelId_BeginLogID_Exists TINYINT DEFAULT 0; 
  /* ���� ������������� ������ � ��������� Mobitel_ID � Begin_SrvPacketID
     � ������� datagpslost_on  */
  DECLARE MobitelId_BeginSrvPacketID_Exists TINYINT DEFAULT 0;
  
  DECLARE RowBeginSrvPacketID BIGINT; 
  
  IF EXISTS(
    SELECT NULL
    FROM datagpslost_on
    WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID))
  THEN
    SET MobitelId_BeginLogID_Exists = 1;
  END IF;

  IF EXISTS(
    SELECT NULL
    FROM datagpslost_on
    WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID))
  THEN
    SET MobitelId_BeginSrvPacketID_Exists = 1;
  END IF;

  IF (MobitelId_BeginLogID_Exists = 0) AND (MobitelId_BeginSrvPacketID_Exists = 0)
  THEN
    /* ������� ����� ������ */
    INSERT INTO datagpslost_on(Mobitel_ID, Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID)
    VALUES (MobitelID, BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID);
  ELSE
    IF ((MobitelId_BeginLogID_Exists = 1) AND (MobitelId_BeginSrvPacketID_Exists = 1))
    THEN
      /* ���������� ������������ ������ */
      UPDATE datagpslost_on
      SET End_LogID = EndLogID, End_SrvPacketID = EndSrvPacketID
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);
    ELSE
      /* ���������� MobitelId_BeginLogID_Exists = 0, MobitelId_BeginSrvPacketID_Exists = 1
         ���������� - ��� �������������� ��������, ������� �� ������ ���������. */
      SELECT Begin_SrvPacketID
      INTO RowBeginSrvPacketID
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID)
      LIMIT 1;
      
      IF ((MobitelId_BeginLogID_Exists = 1) AND (MobitelId_BeginSrvPacketID_Exists = 0) AND
        (BeginSrvPacketID < RowBeginSrvPacketID))
      THEN
        /* � ��� ����� ��������, ���� �������� ������ ���� � �� �� ������ ��������� ��� 
           (LogID � ������� ���� � �����, � SrvPacketID ��������) */
        UPDATE datagpslost_on
        SET End_LogID = EndLogID, Begin_SrvPacketID = BeginSrvPacketID, End_SrvPacketID = EndSrvPacketID
        WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);
      END IF;
    END IF;
  END IF;
END;


DROP PROCEDURE IF EXISTS OnLostDataGPS;
CREATE PROCEDURE OnLostDataGPS(IN MobitelID INTEGER(11), OUT NeedOptimize TINYINT)
  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT '����� ��������� � online ������ ��������� ��������� � ���������� ������.'
BEGIN

  /********** DECLARE **********/
  
  DECLARE CurrentConfirmedID INT DEFAULT 0; /* ������� �������� ConfirmedID */
  DECLARE NewConfirmedID INT DEFAULT 0; /* ����� �������� ConfirmedID */
  DECLARE StartLogIDSelect INT DEFAULT 0; /* min LogID ��� ������� */
  DECLARE FinishLogIDSelect INT DEFAULT 0; /* max LogID ��� ������� */
  DECLARE MaxEndLogID INT DEFAULT 0; /* max End_LogID � ����������� ���������� ������� ��������� */
  
  DECLARE MinLogID_n INT DEFAULT 0; /* ����������� LogID ����� ConfirmedID � ��������� ����� ������ */
  DECLARE MaxLogID_n INT DEFAULT 0; /* ������������ LogID � ��������� ����� ������ */
  
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
    
  DECLARE NewLogID INT; /* �������� LogID � ������� CursorNewLogID */
  DECLARE NewSrvPacketID BIGINT; /* �������� SrvPacketID � ������� CursorNewLogID */
  DECLARE FirstLogID INT DEFAULT -9999999; /* ������ �������� LogID ��� ������� */
  DECLARE SecondLogID INT; /* ������ �������� LogID ��� ������� */
  DECLARE FirstSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
  DECLARE SecondSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
  
  DECLARE tmpSrvPacketID BIGINT DEFAULT 0; /* �������� SrvPacketID ��� ������ ������������� ������ � ������� ����� ������, �� �������������� � ������������� */  
  
  /* ������ ��� ������� �� ����� LogID */
  DECLARE CursorNewLogID CURSOR FOR
    SELECT LogID, SrvPacketID 
    FROM datagpslost_ontmp;
    
  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  /********** END DECLARE **********/

  SET NeedOptimize = 0;
 
  SELECT COALESCE(ConfirmedID, 0) INTO CurrentConfirmedID
  FROM mobitels
  WHERE Mobitel_ID = MobitelID;

  /* ����������� LogID � ����� ������ ������ c ������� ����� �������� */
  SELECT MIN(LogID) INTO MinLogID_n
  FROM datagpsbuffer_on
  WHERE (Mobitel_ID = MobitelID) AND (LogID > CurrentConfirmedID);
  
  /* ������������ LogID � ����� ������ ������ c ������� ����� �������� */
  SELECT MAX(LogID) INTO MaxLogID_n
  FROM datagpsbuffer_on 
  WHERE Mobitel_ID = MobitelID;  
  
  /* ������������ End_LogID � ����������� ���������� ������� ��������� */
  SELECT COALESCE(MAX(End_LogID), 0) INTO MaxEndLogID
  FROM datagpslost_on 
  WHERE Mobitel_ID = MobitelID;  
  
  /* �������� ���� �� ������ ��� ������� */
  IF MinLogID_n IS NOT NULL THEN
  
    /********** PREPARE **********/
    
    TRUNCATE TABLE datagpslost_ontmp;
  
    IF MinLogID_n < MaxEndLogID THEN
      /* ����  MinLogID_n < MaxEndLogID ������ ������ ������, 
       ������� �������� ������� ����� ���������. */
       
      /* ������� ��������� LogID ��� ���������. */
      SELECT COALESCE(Begin_LogID, MinLogID_n) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MinLogID_n > Begin_LogID) 
        AND (MinLogID_n < End_LogID);
      
      /* ������� ��������� LogID ��� ���������. */  
      SELECT COALESCE(End_LogID, MaxLogID_n) INTO FinishLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MaxLogID_n > Begin_LogID) 
        AND (MaxLogID_n < End_LogID);
       
      /* ������ ������ ������ �� ������� ����������� ������� 
         (����� �������������, ���� �����������) */
      DELETE FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID >= StartLogIDSelect)
        AND (Begin_LogID <= FinishLogIDSelect);
      
      /* ���� ������ ������ LogID = 0 � ���� ������ ��� � DataGps, �����
         ���� �������� ������ ��������� ������ ��� �������. */
      IF (StartLogIDSelect = 0) AND (NOT EXISTS
        (Select 1 
         FROM datagps 
         WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect))) THEN
      
        INSERT INTO datagpslost_ontmp(LogID, SrvPacketID) VALUES (0, 0);  
      END IF;       
    ELSE
      /* ������ ����� ������. ���� �������� ��� ������� ������������� ������ - 
         ������ ������� */
      
      /* ������� ��������� LogID ��� ���������. */
      SELECT COALESCE(MAX(End_LogID), CurrentConfirmedID) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE Mobitel_ID = MobitelID;   
         
      /* ������� ��������� LogID ��� ���������. */  
      SELECT MAX(LogID)  INTO FinishLogIDSelect
      FROM datagpsbuffer_on
      WHERE Mobitel_ID = MobitelID;
      
      /* �������� �� datagps ��������������� �������� SrvPacketID  ��� ������
         ������������� ������ */
      SELECT COALESCE(SrvPacketID, 0) INTO tmpSrvPacketID
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect);
      
      /* �������� � ������� datagpslost_ontmp ��� ������� ��������� 
         �������������� ������ ������ */
      INSERT INTO datagpslost_ontmp(LogID, SrvPacketID)
      VALUES (StartLogIDSelect, tmpSrvPacketID);
    END IF; -- IF MinLogID_n < MaxEndLogID   
    
    /********** END PREPARE **********/
    
    /**********  INSERT ROWS **********/
    
    /* ���������� ��������� ������� ���������� ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      SELECT LogID, SrvPacketID
      FROM datagps
      WHERE (Mobitel_ID = MobitelID) AND (LogID >= StartLogIDSelect)
        AND (LogID <= FinishLogIDSelect)
      ORDER BY LogID;
            
    /* ������������ �������-������� � ������� ����������� ���������� */
    SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    WHILE DataExists = 1 DO
      IF FirstLogID = -9999999 THEN
        /* ������ �������� � ������ */
        SET FirstLogID = NewLogID;
        SET FirstSrvPacketID = NewSrvPacketID;
      ELSE
        /* �������� ����� �������*/
        SET SecondLogID = NewLogID;
        SET SecondSrvPacketID = NewSrvPacketID;
        
        IF (SecondLogID - FirstLogID) > 1 THEN
          /* ������ ������ */
          CALL OnInsertDatagpsLost(MobitelID, FirstLogID, SecondLogID,  
            FirstSrvPacketID, SecondSrvPacketID);
          
          IF NeedOptimize = 0 THEN
            SET NeedOptimize = 1; 
          END IF;
        END IF;
        /* ���������� ������� ������ �������� �� ������ �������
           ��� ������� � ��������� �������� � �������� ���������� �������� */
        SET  FirstLogID = SecondLogID;
        SET FirstSrvPacketID = SecondSrvPacketID;
      END IF;  
      FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    END WHILE;
    CLOSE CursorNewLogID;
 
    /********** END INSERT ROWS **********/
               
    /********** UPDATE MOBITELS **********/
    
    /* ���� ����������� ������� � ������� ��������� ��� - 
       ����� ConfirmedID ����� ����� ������������� LogID 
       ������� ���������. ����� - ����������� Begin_LogID
       �� ������� ��������� */
    SELECT MIN(Begin_LogID) INTO NewConfirmedID
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;
       
    IF NewConfirmedID IS NULL THEN
      SELECT MAX(LogID) INTO NewConfirmedID
      FROM datagps
      WHERE Mobitel_ID = MobitelID;
    END IF;  
    
    /* ������� ConfirmedID ���� ���� */
    UPDATE mobitels
    SET ConfirmedID = NewConfirmedID
    WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);
    
    /********** END UPDATE MOBITELS **********/
        
    TRUNCATE TABLE datagpslost_ontmp;
  END IF; -- IF MaxEndLogID IS NOT NULL  
END;


DROP PROCEDURE IF EXISTS OnLostDataGPS2;
CREATE PROCEDURE OnLostDataGPS2(IN MobitelID INTEGER(11), OUT NeedOptimize TINYINT)
  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT '����������� ���������� ���������. ������ ����������� ������ ��������� ���������.'
BEGIN
    /********** DECLARE **********/
    
  DECLARE NewConfirmedID INT DEFAULT 0; /* ����� �������� ConfirmedID */
  DECLARE BeginLogID INT; /* Begin_LogID � ������� CursorLostRanges */
  DECLARE EndLogID INT; /* End_LogID � ������� CursorLostRanges */
  DECLARE BeginSrvPacketID BIGINT; /* Begin_SrvPacketID � ������� CursorLostRanges */
  DECLARE EndSrvPacketID BIGINT; /* End_SrvPacketID � ������� CursorLostRanges */
  DECLARE RowCountForAnalyze INT DEFAULT 0; /* ���������� ������� ��� ������� */
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
    
  /* ������ ��� ������� �� ���������� ��������� ��������� */
  DECLARE CursorLostRanges CURSOR FOR
    SELECT Begin_LogID, End_LogID, Begin_SrvPacketID, End_SrvPacketID 
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;    

  /* ������ ��� ������� �� ����� LogID */
  DECLARE CursorNewLogID CURSOR FOR
    SELECT LogID, SrvPacketID 
    FROM datagpslost_ontmp
    ORDER BY SrvPacketID;    
    
  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  /********** END DECLARE **********/

  SET NeedOptimize = 0;
  
  TRUNCATE TABLE datagpslost_ontmp;
  
  /* ���������� ��������� � ��������� ������� ���� ��� �������� */
  SET DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  WHILE DataExists = 1 DO
    /* ���������� ��������� ������� ������������ ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp  (LogID, SrvPacketID)
      SELECT DISTINCT LogID, SrvPacketID
      FROM datagpsbuffer_ontmp
      WHERE (Mobitel_ID = MobitelID) AND (SrvPacketID > BeginSrvPacketID)
        AND (SrvPacketID < EndSrvPacketID)
      ORDER BY SrvPacketID;
      
    /* ���� ���� ������ ������������ �������� */  
    SELECT COUNT(1) INTO RowCountForAnalyze
    FROM datagpslost_ontmp;  
    
    IF RowCountForAnalyze > 0 THEN  
      /* ��������� ������ � ��������� ������ ��� ������� */  
      INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
      VALUES (BeginLogID, BeginSrvPacketID), (EndLogID, EndSrvPacketID);
      
      /* ������� ������ �������� */
      DELETE FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);
       
      /**********  INSERT ROWS **********/             
      BEGIN
        DECLARE NewLogID INT; /* �������� LogID � ������� CursorNewLogID */
        DECLARE NewSrvPacketID BIGINT; /* �������� SrvPacketID � ������� CursorNewLogID */
        DECLARE FirstLogID INT DEFAULT -9999999; /* ������ �������� LogID ��� ������� */
        DECLARE SecondLogID INT; /* ������ �������� LogID ��� ������� */
        DECLARE FirstSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
        DECLARE SecondSrvPacketID BIGINT; /* ������ �������� SrvPacketID */
        
        /* ���������� ������������� ������ ��� FETCH ��������� ������ */
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
        
        /* ������������ �������-������� � ������� ����������� ���������� */
        SET DataExists = 1;
        OPEN CursorNewLogID;
        FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        WHILE DataExists = 1 DO
          IF FirstLogID = -9999999 THEN
            /* ������ �������� � ������ */
            SET FirstLogID = NewLogID;
            SET FirstSrvPacketID = NewSrvPacketID;
          ELSE
            /* �������� ����� �������*/
            SET SecondLogID = NewLogID;
            SET SecondSrvPacketID = NewSrvPacketID;
            
            IF (SecondSrvPacketID - FirstSrvPacketID) > 1 THEN
              /* ������ ������ */
              CALL OnInsertDatagpsLost(MobitelID, FirstLogID, SecondLogID,  
                FirstSrvPacketID, SecondSrvPacketID);

              IF NeedOptimize = 0 THEN
                SET NeedOptimize = 1; 
              END IF;
            END IF;
            /* ���������� ������� ������ �������� �� ������ �������
               ��� ������� � ��������� �������� � �������� ���������� �������� */
            SET FirstLogID = SecondLogID;
            SET FirstSrvPacketID = SecondSrvPacketID;
          END IF;  
          FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        END WHILE;
        CLOSE CursorNewLogID;
      END;
      /********** END INSERT ROWS **********/  

      TRUNCATE TABLE datagpslost_ontmp;
    END IF;
      
    FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  END WHILE;
  CLOSE CursorLostRanges;
  
  /********** UPDATE MOBITELS **********/
    
  /* ���� ����������� ������� � ������� ��������� ��� - 
     ����� ConfirmedID ����� ����� ������������� LogID 
     ������� ���������. ����� - ����������� Begin_LogID
     �� ������� ��������� */
  SELECT MIN(Begin_LogID) INTO NewConfirmedID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;
       
  IF NewConfirmedID IS NULL THEN
    SELECT MAX(LogID) INTO NewConfirmedID
    FROM datagps
    WHERE Mobitel_ID = MobitelID;
  END IF;  
    
  /* ������� ConfirmedID ���� ���� */
  UPDATE mobitels
  SET ConfirmedID = NewConfirmedID
  WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);
    
  /********** END UPDATE MOBITELS **********/
END;


DROP PROCEDURE IF EXISTS OnCorrectInfotrackLogId;
CREATE PROCEDURE OnCorrectInfotrackLogId()
  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT '������������� �������� LogId � InMobitelID � ������ ����������'
BEGIN
  DECLARE MobitelIDInCursor INT; /* ������� �������� MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
  DECLARE MaxLogId INT DEFAULT 0; /* ������������ LogId ��� ������� ������� */
  DECLARE SrvPacketIDInCursor BIGINT DEFAULT 0; /* ������� �������� ���������� ������ */
    
  /* ������ �� ���������� � ��������� ����� ������. 
     ��� ��������� ���������� � ����� I */
  DECLARE CursorInfoTrack CURSOR FOR
    SELECT DISTINCT buf.Mobitel_ID
    FROM datagpsbuffer_on buf 
      JOIN mobitels m ON (buf.mobitel_id = m.mobitel_id)
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID) 
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND 
      (c.devIdShort LIKE "I%") AND (buf.LogId = 0)
    ORDER BY 1;
    
  /* ������ �� ������ ��������� */
  DECLARE CursorInfoTrackUpdateSet CURSOR FOR
    SELECT SrvPacketID 
    FROM datagpsbuffer_on
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId = 0)
    ORDER BY UnixTime;  

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  
  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ������� ������������ LogId ��� ������� ��������� */
    SELECT COALESCE(MAX(LogID), 0) INTO	MaxLogId
    FROM DataGps
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ��������� LogId � InMobitelID � ������ ����� ������ �������� ��������� */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    WHILE DataExists = 1 DO
      SET MaxLogId = MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET LogId = MaxLogId, InMobitelID = MaxLogId
      WHERE SrvPacketID = SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    END WHILE;
    CLOSE CursorInfoTrackUpdateSet;
 
    SET DataExists = 1;
    FETCH CursorInfoTrack INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorInfoTrack;
END;


DROP PROCEDURE IF EXISTS OnDeleteDuplicates;
CREATE PROCEDURE OnDeleteDuplicates()
  SQL SECURITY INVOKER
  COMMENT '�������� ������������� ����� �� datagpsbuffer_on'
BEGIN
  /* ������� ��������� ����� �� ������� datagpsbuffer_on. ��������� ������
   �������� ���������� �������� � ����� Mobitel_Id + LogID. �� ���������
   ������������� ������� � ������� datagpsbuffer_on ��������� ������ ���� - 
   ��������� �������� �������������� ��������, �.�. � ������������ SrvPacketID. */
   
  /* ������� MobitelId � ������� CursorDuplicateGroups */
  DECLARE MobitelIdInCursor INT DEFAULT 0; 
  /* ������� LogID � ������� CursorDuplicateGroups */
  DECLARE LogIdInCursor INT DEFAULT 0; 
  /* Max SrvPacketId � ��������� ����� ������� ������ ���������� */
  DECLARE MaxSrvPacketId BIGINT DEFAULT 0; 
  DECLARE DataExists TINYINT DEFAULT 1;

  /* ������ ��� ������� �� ���� ������� ���������� */  
  DECLARE CursorDuplicateGroups CURSOR FOR
    SELECT Mobitel_Id, LogId 
    FROM datagpsbuffer_on
    GROUP BY Mobitel_Id, LogId 
    HAVING COUNT(LogId) > 1;
    
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  WHILE DataExists = 1 DO
    SELECT MAX(SrvPacketID) INTO MaxSrvPacketId
    FROM datagpsbuffer_on 
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor);
         
    DELETE FROM datagpsbuffer_on  
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor) 
      AND (SrvPacketID < MaxSrvPacketId);
        
    FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  END WHILE;
  CLOSE CursorDuplicateGroups; 
END;


DROP PROCEDURE IF EXISTS OnTransferBuffer;
CREATE PROCEDURE OnTransferBuffer()
  NOT DETERMINISTIC
  CONTAINS SQL
  SQL SECURITY INVOKER
  COMMENT '��������� GPS ������ �� �������� ������� � datagps'
BEGIN
  DECLARE MobitelIDInCursor INT; /* ������� �������� MobitelID */
  DECLARE DataExists TINYINT DEFAULT 1; /* ���� ������� ������ ����� ����� */
  DECLARE NeedOptimize TINYINT DEFAULT 0; /* ����� ����������� ��� ��� */
  DECLARE NeedOptimizeParam TINYINT;
  
  DECLARE TmpMaxUnixTime INT; /* ��������� ���������� ��� �������� Max UnixTime �� ������� online */

  /* ������ �� ���������� � ��������� ����� ������ */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_on;
  /* ������ �� ���������� � ��������� ��������� ������ */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
    SELECT DISTINCT(Mobitel_ID) FROM datagpsbuffer_ontmp;  

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  CALL OnDeleteDuplicates();
  CALL OnCorrectInfotrackLogId();
  
  /* ��������� ������, ������� ��� ���� � ��, ��� ������� ����� ������� ��������� 
     ������������ ���������� OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp (Mobitel_ID, LogID, SrvPacketID)
    SELECT b.Mobitel_ID, b.LogID, b.SrvPacketID
    FROM datagpsbuffer_on b LEFT JOIN datagps d ON
      (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL;
       
  /* -------------------- ���������� ������� Online ---------------------- */  
  /* �������� �� ������� online ����������� ����� ������� datagpsbuffer_on */
  /* �� ����� Mobitel_ID � LogID */
  DELETE o
  FROM `online` o, datagpsbuffer_on dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);
  
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ���������� Max �������� UnixTime � ������� online ��� ���������
       ���������. p.s. UnixTime � ������� online ������ �������� */
    SELECT COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online FORCE INDEX (IDX_Mobitelid)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ������� ����� ������� (�������� 100)������� ��������� � online */
    INSERT INTO `online` (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
      Direction, Speed, Valid, `Events`,
      Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
      Counter1, Counter2, Counter3, Counter4, whatIs
    FROM (
      SELECT *
      FROM datagpsbuffer_on
      WHERE (Mobitel_ID = MobitelIDInCursor)  AND 
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
      GROUP BY Mobitel_ID, LogID
      ORDER BY UnixTime DESC
      LIMIT 100) AS T
    ORDER BY UnixTime ASC;

    /* �������� ����� �� ������� ����������� ������ �� ���������������
       �������� �������� - �� �������� ���������� 100 �������� */
    DELETE FROM `online`
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < 
      (SELECT COALESCE(MIN(UnixTime), 0) 
       FROM (
         SELECT UnixTime
         FROM `online`
         WHERE Mobitel_ID = MobitelIDInCursor 
         ORDER BY UnixTime DESC
         LIMIT 100) T1)); 
  
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */ 
  
  /* �����-�� ����� ������ ... */
  UPDATE datagps d, datagpsbuffer_on b SET
    d.Latitude = b.Latitude,
    d.Longitude = b.Longitude,
    d.Altitude = b.Altitude,
    d.UnixTime = b.UnixTime,
    d.Speed = b.Speed,
    d.Direction = b.Direction,
    d.Valid = b.Valid,
    d.InMobitelID = b.LogID,
    d.Events = b.Events,
    d.Sensor1 = b.Sensor1,
    d.Sensor2 = b.Sensor2,
    d.Sensor3 = b.Sensor3,
    d.Sensor4 = b.Sensor4,
    d.Sensor5 = b.Sensor5,
    d.Sensor6 = b.Sensor6,
    d.Sensor7 = b.Sensor7,
    d.Sensor8 = b.Sensor8,
    d.whatIs = b.whatIs,
    d.Counter1 = b.Counter1,
    d.Counter2 = b.Counter2,
    d.Counter3 = b.Counter3,
    d.Counter4 = b.Counter4,
    d.SrvPacketID = b.SrvPacketID
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    AND (d.SrvPacketID < b.SrvPacketID);

  /* ������ ������ ������� ����������� � ������� */
  DELETE b
  FROM datagps d, datagpsbuffer_on b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

  /* ������� ����� �������*/
  INSERT INTO datagps (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, InMobitelID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs, SrvPacketID)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Altitude,
    Direction, Speed, Valid, `Events`, LogID,
    Sensor1, Sensor2, Sensor3, Sensor4, Sensor5, Sensor6, Sensor7, Sensor8,
    Counter1, Counter2, Counter3, Counter4, whatIs, SrvPacketID
  FROM datagpsbuffer_on
  GROUP BY Mobitel_ID, LogID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ------ ���������� ������� ��������� ������� ������� ------- */
    UPDATE mobitels SET LastInsertTime = UNIX_TIMESTAMP(NOW())
    WHERE Mobitel_ID = MobitelIDInCursor;
       
    /* ------ ���������� ������ ����������� ������� -------------- */
    CALL OnLostDataGPS(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;

  /* ������� ������ */
  TRUNCATE TABLE datagpsbuffer_on;
    
  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ---- ���������� ������ ����������� ������� �� ������� ��������� --- */
    CALL OnLostDataGPS2(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsUpdateSet;
  
  /* ������� ���������������� ������ */
  TRUNCATE TABLE datagpsbuffer_ontmp;

  /* ���� ��������� - ������� ����������� */
  IF NeedOptimize = 1 THEN
    OPTIMIZE TABLE datagpslost_on;
  END IF;            
END;

DROP PROCEDURE IF EXISTS OnDeleteLostRange;
CREATE PROCEDURE OnDeleteLostRange(IN MobitelID INTEGER(11), IN BeginSrvPacketID BIGINT)
  NOT DETERMINISTIC
  CONTAINS SQL                          
  SQL SECURITY INVOKER
  COMMENT '�������� ���������� ��������� ��������� �� datagpslost_on.'
BEGIN
  /* ���������� �������� �� ������, ��������������� ��� ��������,
     ������ � ������ ��������� ���������. ���� �������� - �����
     ������ �������� � �������� ConfirmedID */  
     
  DECLARE MinBeginSrvPacketID BIGINT; -- ����������� �������� Begin_SrvPacketID ��� ������� ���������
  DECLARE NewConfirmedID INT; -- ����� �������� ConfirmedID  

  /* ���������� ������������ �������� Begin_SrvPacketID */  
  SELECT MIN(Begin_SrvPacketID) INTO MinBeginSrvPacketID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;
  
  /* ���� ���� ������ � ��������� � ��������� - ��������� */
  IF MinBeginSrvPacketID IS NOT NULL THEN
    /* ���� ������� ������ ������ - �������� ����� �������� ConfirmedID */
    IF MinBeginSrvPacketID = BeginSrvPacketID  THEN
      SELECT End_LogID INTO NewConfirmedID
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID);
    END IF;
  
    /* �������� ������ */
    DELETE FROM  datagpslost_on
    WHERE (Mobitel_ID = MobitelID) AND (Begin_SrvPacketID = BeginSrvPacketID);
    
    /* ���� ������� ������ ������ - ������� ConfirmedID */  
    IF MinBeginSrvPacketID = BeginSrvPacketID  THEN
      UPDATE mobitels
      SET ConfirmedID = NewConfirmedID
      WHERE (Mobitel_ID = MobitelID) AND (NewConfirmedID > ConfirmedID);
    END IF;
  END IF;  
END;

DROP PROCEDURE IF EXISTS delDupSrvPacket;
CREATE PROCEDURE delDupSrvPacket()
  NOT DETERMINISTIC
  CONTAINS SQL                          
  SQL SECURITY INVOKER
  COMMENT '������� ��������� SrvPacketID �� ������� datagpsbuffer_on64'
BEGIN
DROP TEMPORARY TABLE IF EXISTS tmp64;
DROP TABLE IF EXISTS tmp64;

CREATE TEMPORARY TABLE tmp64 SELECT Mobitel_ID,
  Latitude,
  Longitude,
  Direction,
  Acceleration,
  UnixTime,
  Speed,
  Valid,
  Satellites,
  RssiGsm,
  `Events`,
  SensorsSet,
  Sensors,
  Voltage,
  DGPS,
  LogID,
  SrvPacketID
FROM datagpsbuffer_on64 d
GROUP BY d.SrvPacketID;

  TRUNCATE TABLE datagpsbuffer_on64;

  INSERT INTO datagpsbuffer_on64 SELECT Mobitel_ID,
  Latitude,
  Longitude,
  Direction,
  Acceleration,
  UnixTime,
  Speed,
  Valid,
  Satellites,
  RssiGsm,
  `Events`,
  SensorsSet,
  Sensors,
  Voltage,
  DGPS,
  LogID,
  SrvPacketID
FROM tmp64 d;

DROP TEMPORARY TABLE IF EXISTS tmp64;
DROP TABLE IF EXISTS tmp64;
END;

DROP PROCEDURE IF EXISTS OnListMobitelConfig64;
CREATE PROCEDURE OnListMobitelConfig64()
  NOT DETERMINISTIC
  CONTAINS SQL                          
  SQL SECURITY INVOKER
  COMMENT '������� �������� ���������� ������� ����� �������� � online ������ ��� datagps � datagps64'
BEGIN
  DECLARE MobitelIDInCursor INT;
  DECLARE Is64Cursor tinyint;
  DECLARE DevIdShortInCursor CHAR(4);
  DECLARE DataExists TINYINT DEFAULT 1;

  /* ������ � ��������� ����������� ���������� */
  DECLARE CursorMobitels CURSOR FOR
    SELECT m.Mobitel_ID AS MobitelID, m.Is64bitPackets AS Is64, c.devIdShort AS DevIdShort
    FROM mobitels m
      JOIN internalmobitelconfig c ON (c.ID = m.InternalMobitelConfig_ID)
    WHERE (c.InternalMobitelConfig_ID = (
      SELECT intConf.InternalMobitelConfig_ID
      FROM internalmobitelconfig intConf
      WHERE (intConf.ID = c.ID)
      ORDER BY intConf.InternalMobitelConfig_ID DESC
      LIMIT 1)) AND
      (c.devIdShort IS NOT NULL) AND (c.devIdShort <> _cp1251'')
    ORDER BY m.Mobitel_ID;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /* ��������� ������� �������� ��������� */
  CREATE TEMPORARY TABLE tmpMobitelsConfig (
    MobitelID INT NOT NULL,
    Is64 tinyint NOT NULL,
    DevIdShort CHAR(4) NOT NULL,
    LastPacketID BIGINT NOT NULL,
    LastPacketID64 BIGINT NOT NULL) ENGINE = MEMORY;

  OPEN CursorMobitels;
  FETCH CursorMobitels INTO MobitelIDInCursor, Is64Cursor, DevIdShortInCursor;
  WHILE DataExists = 1 DO

    INSERT INTO tmpMobitelsConfig SELECT MobitelIDInCursor, Is64Cursor, DevIdShortInCursor,
      (SELECT COALESCE(MAX(d.SrvPacketID), 0)
       FROM datagps d
       WHERE d.Mobitel_ID = MobitelIDInCursor) AS LastPacketID,
      (SELECT COALESCE(MAX(t.SrvPacketID), 0)
       FROM datagps64 t
       WHERE t.Mobitel_ID = MobitelIDInCursor) AS LastPacketID64;

    FETCH CursorMobitels INTO MobitelIDInCursor, Is64Cursor, DevIdShortInCursor;
  END WHILE;
  CLOSE CursorMobitels;

  SELECT MobitelID, Is64, DevIdShort, LastPacketID, LastPacketID64
  FROM tmpMobitelsConfig;

  DROP TEMPORARY TABLE tmpMobitelsConfig;
END;

ALTER TABLE datagpsbuffer_on MODIFY COLUMN SrvPacketID BIGINT NOT NULL DEFAULT '0';

ALTER TABLE datagpsbuffer_ontmp MODIFY COLUMN SrvPacketID BIGINT NOT NULL DEFAULT '0';

ALTER TABLE datagpslost_on MODIFY COLUMN Begin_SrvPacketID BIGINT NOT NULL DEFAULT '0';

ALTER TABLE datagpslost_on MODIFY COLUMN End_SrvPacketID BIGINT NOT NULL DEFAULT '0';

DROP PROCEDURE IF EXISTS tmpAlterDmTables;
CREATE PROCEDURE tmpAlterDmTables()
BEGIN
DROP TABLE IF EXISTS datagpsbuffer_on;
CREATE TABLE IF NOT EXISTS datagpsbuffer_on (
  Mobitel_ID int(11) NOT NULL default 0,
  Message_ID int(11) default 0,
  Latitude int(11) NOT NULL default 0,
  Longitude int(11) NOT NULL default 0,
  Altitude int(11) default NULL,
  UnixTime int(11) NOT NULL default 0,
  Speed smallint(6) NOT NULL default 0,
  Direction int(11) NOT NULL default 0,
  Valid tinyint(4) NOT NULL default 1,
  InMobitelID int(11) default NULL,
  Events int(10) unsigned default 0,
  Sensor1 tinyint(4) unsigned default 0,
  Sensor2 tinyint(4) unsigned default 0,
  Sensor3 tinyint(4) unsigned default 0,
  Sensor4 tinyint(4) unsigned default 0,
  Sensor5 tinyint(4) unsigned default 0,
  Sensor6 tinyint(4) unsigned default 0,
  Sensor7 tinyint(4) unsigned default 0,
  Sensor8 tinyint(4) unsigned default 0,
  LogID int(11) NOT NULL default 0,
  isShow tinyint(4) default 0,
  whatIs smallint(6) NOT NULL default 0,
  Counter1 int(11) NOT NULL default -1,
  Counter2 int(11) NOT NULL default -1,
  Counter3 int(11) NOT NULL default -1,
  Counter4 int(11) NOT NULL default -1,
  SrvPacketID BIGINT NOT NULL DEFAULT 0 COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������',
  INDEX IDX_MobitelidLogid USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid USING BTREE (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_SrvPacketID (SrvPacketID)
) ENGINE=MEMORY DEFAULT CHARSET=cp1251 COMMENT '����� ������ ������';
  END;


DROP TABLE IF EXISTS datagpsbuffer_ontmp;
CREATE TABLE IF NOT EXISTS datagpsbuffer_ontmp (
  Mobitel_ID INT(11) NOT NULL DEFAULT 0,
  LogID INT(11) NOT NULL DEFAULT 0,
  SrvPacketID BIGINT NOT NULL,
  INDEX IDX_MobitelidSrvpacketid USING BTREE (Mobitel_ID, SrvPacketID),
  INDEX IDX_Mobitelid USING BTREE (Mobitel_ID)
) ENGINE = MEMORY DEFAULT CHARSET=cp1251 COMMENT '��������� ��������������� ����� ������ ������';

DROP TABLE IF EXISTS online;
CREATE TABLE IF NOT EXISTS online (
  DataGps_ID INT(11) AUTO_INCREMENT,
  Mobitel_ID INT(11) DEFAULT 0,
  Message_ID INT(11) DEFAULT 0,
  Latitude INT(11) NOT NULL DEFAULT 0,
  Longitude INT(11) NOT NULL DEFAULT 0,
  Altitude INT(11) DEFAULT 0,
  UnixTime INT(11) NOT NULL DEFAULT 0,
  Speed SMALLINT(6) NOT NULL DEFAULT 0,
  Direction INT(11) NOT NULL DEFAULT 0,
  Valid TINYINT(4) NOT NULL DEFAULT 1,
  InMobitelID INT(11) DEFAULT 0,
  Events INT(10) UNSIGNED DEFAULT 0,
  Sensor1 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor2 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor3 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor4 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor5 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor6 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor7 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor8 TINYINT(4) UNSIGNED DEFAULT 0,
  LogID INT(11) DEFAULT 0,
  isShow TINYINT(4) DEFAULT 0,
  whatIs SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  Counter1 INT(11) NOT NULL DEFAULT -1,
  Counter2 INT(11) NOT NULL DEFAULT -1,
  Counter3 INT(11) NOT NULL DEFAULT -1,
  Counter4 INT(11) NOT NULL DEFAULT -1,
  PRIMARY KEY (DataGps_ID),
  INDEX IDX_MobitelidLogid USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtimeValid USING BTREE (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_MobitelidUnixtime USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_Mobitelid USING BTREE (Mobitel_ID)
) ENGINE = INNODB COMMENT '����������� ������';

DROP TABLE IF EXISTS datagpslost_on;
CREATE TABLE IF NOT EXISTS datagpslost_on (
  Mobitel_ID INT(11) NOT NULL DEFAULT 0,
  Begin_LogID INT(11) NOT NULL DEFAULT 0 COMMENT '�������� LogID ����� �������� ���������� ������',
  End_LogID INT(11) NOT NULL DEFAULT 0 COMMENT '�������� LogID ����� ������� ����������� ������',
  Begin_SrvPacketID BIGINT NOT NULL COMMENT '������������ ���������� ������ � ������� ������ ������ � Begin_LogID',
  End_SrvPacketID BIGINT NOT NULL COMMENT '������������ ���������� ������ � ������� ������ ������ � End_LogID',
  PRIMARY KEY PK_DatagpslostOn(Mobitel_ID, Begin_LogID),
  INDEX IDX_MobitelID (Mobitel_ID),
  UNIQUE INDEX IDX_MobitelID_BeginSrvPacketID (Mobitel_ID, Begin_SrvPacketID)
) ENGINE=InnoDB COMMENT='�������� � ������ DataGPS, ���������� �� Online �������';

DROP TABLE IF EXISTS datagpslost_ontmp;
CREATE TABLE IF NOT EXISTS datagpslost_ontmp (
  LogID INT(11) NOT NULL DEFAULT 0,
  SrvPacketID BIGINT NOT NULL
) ENGINE = MEMORY COMMENT = '������������ ������������� ���������� OnLostDataGPS';


DROP PROCEDURE IF EXISTS tmpAddColumns;
CREATE PROCEDURE tmpAddColumns()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '��������� ��������� ����������� ��������'
BEGIN
  DECLARE DbName VARCHAR(64); /* ������������ ������� �� */
  DECLARE ColumnExist TINYINT DEFAULT 0;

  SELECT DATABASE() INTO DbName;

  SELECT COUNT(1) INTO ColumnExist
  FROM information_schema.columns
  WHERE (TABLE_SCHEMA = DbName) AND
    (TABLE_NAME = "mobitels") AND (COLUMN_NAME = "ConfirmedID");

  IF ColumnExist = 0 THEN
    ALTER TABLE mobitels ADD COLUMN ConfirmedID int(11) NOT NULL DEFAULT 0
      COMMENT 'MIN �������� LogID ����� �������� ��������� ��� ��������� ��� ��� ���������� ���������';
  END IF;

  SELECT COUNT(1) INTO ColumnExist
  FROM information_schema.columns
  WHERE (TABLE_SCHEMA = DbName) AND
    (TABLE_NAME = "mobitels") AND (COLUMN_NAME = "LastInsertTime");

  IF ColumnExist = 0 THEN
    ALTER TABLE mobitels ADD COLUMN LastInsertTime INT(11) NOT NULL DEFAULT 0
      COMMENT 'UNIX_TIMESTAMP ����� ��������� ������� ������ � DataGPS';
  END IF;
END;

CALL tmpAddColumns;
DROP PROCEDURE IF EXISTS tmpAddColumns;


DROP PROCEDURE IF EXISTS tmpAlterDmTables;
CREATE PROCEDURE tmpAlterDmTables()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '��������� ��������� ����������� ������'
BEGIN
  IF EXISTS (
    SELECT NULL 
    FROM information_schema.TABLE_CONSTRAINTS
    WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND (TABLE_NAME = "datagpslost_on") 
      AND (CONSTRAINT_NAME = "IDX_MobitelID_BeginLogID"))
  THEN
    ALTER TABLE datagpslost_on DROP INDEX IDX_MobitelID_BeginLogID;
  END IF;

	IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "datagps64")
	THEN
	ALTER TABLE datagps64 MODIFY DGPS varchar(32);
	END IF;

	IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "online64")
	THEN
	ALTER TABLE online64 MODIFY DGPS varchar(32);
	END IF;
END;

CALL tmpAlterDmTables;
DROP PROCEDURE IF EXISTS tmpAlterDmTables;

DROP PROCEDURE IF EXISTS rcs_Vehicles_Team_Driver_Update;
CREATE PROCEDURE rcs_Vehicles_Team_Driver_Update()
BEGIN
       
    IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "OutLinkId")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `OutLinkId` VARCHAR(20)  NULL COMMENT '��� ������� ����';
	END IF;
	
    IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "team") AND
        (COLUMN_NAME = "OutLinkId")
	)
	THEN
		ALTER TABLE `team` ADD COLUMN `OutLinkId` VARCHAR(20)  NULL COMMENT '��� ������� ����';
		ALTER TABLE team MODIFY `Name` VARCHAR(50);
	END IF;	
	
    IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "OutLinkId")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `OutLinkId` VARCHAR(20)  NULL COMMENT '��� ������� ����';
	END IF;	
	
	CREATE TABLE IF NOT EXISTS driver_types(
	Id INT(11) NOT NULL AUTO_INCREMENT,
	TypeName VARCHAR(50) DEFAULT NULL COMMENT '������������� ��������',
	PRIMARY KEY (Id),
	UNIQUE INDEX id (Id)
	)
	ENGINE = INNODB
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci;
		
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "idType")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `idType` int  NULL default 0 COMMENT '��� ������������� ��������';
	END IF;	

	CREATE TABLE IF NOT EXISTS vehicle_category(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(50) NOT NULL COMMENT '��������� ������������� ��������',
  Tonnage DECIMAL(10, 3) NOT NULL DEFAULT 0.000 COMMENT '����������������,  ����',
  FuelNormMoving DOUBLE NOT NULL DEFAULT 0 COMMENT '����� ������� ������� � �������� �/100 ��',
  FuelNormParking DOUBLE NOT NULL DEFAULT 0 COMMENT '����� ������� ������� �� �������� �������� �/���',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id` int  NULL  COMMENT '��������� �����������';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id` FOREIGN KEY (Category_id)
		REFERENCES vehicle_category (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;	
	
CREATE TABLE IF NOT EXISTS vehicle_category2(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(255) NOT NULL COMMENT '���������2 ������������� ��������',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id2")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id2` int  NULL  COMMENT '���������2 ����������';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id2` FOREIGN KEY (Category_id2)
		REFERENCES vehicle_category2 (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "PcbVersionId")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `PcbVersionId` int(11) DEFAULT - 1 COMMENT '������ ���� ������� ����������� �� ������';
	END IF;
	CREATE TABLE IF NOT EXISTS vehicle_category3(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(255) NOT NULL COMMENT '���������3 ������������� ��������',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id3")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id3` int  NULL  COMMENT '���������3 ����������';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id3` FOREIGN KEY (Category_id3)
		REFERENCES vehicle_category3 (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;
	    CREATE TABLE IF NOT EXISTS vehicle_category4(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(255) NOT NULL COMMENT '���������4 ������������� ��������',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id4")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id4` int  NULL  COMMENT '���������4 ����������';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id4` FOREIGN KEY (Category_id4)
		REFERENCES vehicle_category4 (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata")	)
	THEN
CREATE TABLE pcbdata (
  Id int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '������',
  Typedata varchar(255) NOT NULL DEFAULT ' ' COMMENT '��� ������',
  koeffK double NOT NULL DEFAULT 0 COMMENT '����������� �',
  Startbit int(11) NOT NULL DEFAULT 0 COMMENT '��������� ��� ����',
  Lengthbit int(11) NOT NULL DEFAULT 0 COMMENT '����� ����',
  Comment varchar(255) DEFAULT ' ' COMMENT '����������',
  Idpcb int(11) NOT NULL DEFAULT 0 COMMENT '������ ����� �������',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 11
AVG_ROW_LENGTH = 2340
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = '�������� ����� ��������';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbversion")	)
	THEN
CREATE TABLE pcbversion (
  Id int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '������',
  Name varchar(50) NOT NULL DEFAULT '*' COMMENT '��� ����� �������',
  Comment varchar(255) DEFAULT ' ' COMMENT '�����������',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 18
AVG_ROW_LENGTH = 5461
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = '������������� ������ ��� ������ ���������� ����� ��������';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zone_category")	)
	THEN
CREATE TABLE zone_category (
  Id int(11) NOT NULL AUTO_INCREMENT,
  Name varchar(255) DEFAULT NULL COMMENT '��������� ����',
  PRIMARY KEY (Id))
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = '��������� ��� ���';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zone_category2")	)
	THEN
CREATE TABLE zone_category2 (
  Id int(11) NOT NULL AUTO_INCREMENT,
  Name varchar(255) DEFAULT NULL COMMENT '���������2 ����',
  PRIMARY KEY (Id))
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = '���������2 ��� ���';
END IF;
IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle_commentary")	)
	THEN
CREATE TABLE vehicle_commentary (
  Id int NOT NULL AUTO_INCREMENT,
  `Comment` varchar(255) DEFAULT NULL,
  Mobitel_id int NOT NULL,
  PRIMARY KEY (Id))
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = '����������� ��� ������';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "setting") AND
        (COLUMN_NAME = "TimeMinimMovingSize")
	)
	THEN
		ALTER TABLE `setting` ADD COLUMN `TimeMinimMovingSize` time NOT NULL DEFAULT '00:00:00' COMMENT '����������� ����� �������� � ������ �����';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbversion") AND
        (COLUMN_NAME = "MobitelId")
	)
	THEN
		ALTER TABLE `pcbversion` ADD COLUMN `MobitelId` int(11) DEFAULT - 1 COMMENT 'ID ������ �� ������� ��������� ��� ����� ��������';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "mobitels") AND
        (COLUMN_NAME = "Is64bitPackets")
	)
	THEN
		ALTER TABLE `mobitels` ADD COLUMN `Is64bitPackets` tinyint(1) NOT NULL DEFAULT 0 COMMENT '�������� ������ ���� 64 �������� ��������';
	END IF;

	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "mobitels") AND
        (COLUMN_NAME = "IsNotDrawDgps")
	)
	THEN
		ALTER TABLE `mobitels` ADD COLUMN `IsNotDrawDgps` tinyint(1) NOT NULL DEFAULT 1 COMMENT '���������� ��� ��� ������ DGPS �� �����';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zones") AND
        (COLUMN_NAME = "Category_id")
	)
	THEN
		ALTER TABLE `zones` ADD COLUMN `Category_id` int(11) DEFAULT NULL COMMENT '��������� ��� ����';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zones") AND
        (COLUMN_NAME = "Category_id2")
	)
	THEN
		ALTER TABLE `zones` ADD COLUMN `Category_id2` int(11) DEFAULT NULL COMMENT '���������2 ��� ����';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "numTelephone")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `numTelephone` varchar(32) DEFAULT '' COMMENT '������� �������� ��� �����';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "Department")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `Department` varchar(40) NULL COMMENT '����������� ��������';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "NameVal")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `NameVal` varchar(12) DEFAULT ' ' COMMENT 'Name meassurement value';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "NumAlg")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `NumAlg` int(11) DEFAULT 0 COMMENT 'Number of algorithm';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "sensors") AND
        (COLUMN_NAME = "B")
	)
	THEN
		ALTER TABLE `sensors` ADD COLUMN `B` double DEFAULT 0 COMMENT 'Shift coefficient';
	END IF;
IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "Shifter")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `Shifter` double DEFAULT 0 COMMENT 'Shift coefficient';
	END IF;


IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "sensors") AND
        (COLUMN_NAME = "S")
	)
	THEN
		ALTER TABLE `sensors` ADD COLUMN `S` tinyint(1) DEFAULT 0 COMMENT 'Sign arifmetic';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "S")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `S` tinyint(1) DEFAULT 0 COMMENT 'Sign arifmetic';
	END IF;



IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "datagpsbuffer_on64")
	THEN
	ALTER TABLE datagpsbuffer_on64 MODIFY DGPS varchar(32);
	END IF;
END;
CALL rcs_Vehicles_Team_Driver_Update();
DROP PROCEDURE IF EXISTS rcs_Vehicles_Team_Driver_Update;
/* End this file */