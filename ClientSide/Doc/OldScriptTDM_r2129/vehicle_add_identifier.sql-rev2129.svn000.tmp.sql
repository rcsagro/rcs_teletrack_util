IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'Identifier')
  ALTER TABLE vehicle ADD Identifier INT NULL DEFAULT (0);

  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'PcbVersionId')
  ALTER TABLE vehicle ADD PcbVersionId INT NULL DEFAULT (-1);
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'sensors'
                 AND COLUMN_NAME = 'B')
  ALTER TABLE sensors ADD B FLOAT  NOT NULL DEFAULT (0);
IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'sensors'
                 AND COLUMN_NAME = 'S')
  ALTER TABLE sensors ADD S  TINYINT  NOT NULL DEFAULT (0);

  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_plant'
                 AND COLUMN_NAME = 'TimeStart')
  ALTER TABLE agro_plant ADD TimeStart DATETIME NULL DEFAULT (NULL);

  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'agro_field'
                 AND COLUMN_NAME = 'IsOutOfDate')
  ALTER TABLE agro_field ADD IsOutOfDate BIT NOT NULL DEFAULT (0);

  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'pcbversion'
                 AND COLUMN_NAME = 'MobitelId')
  ALTER TABLE pcbversion ADD MobitelId INT NULL DEFAULT (-1);
IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'pcbdata'
                 AND COLUMN_NAME = 'NameVal')
  ALTER TABLE pcbdata ADD NameVal VARCHAR(32) NOT NULL DEFAULT (' ');
IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'pcbdata'
                 AND COLUMN_NAME = 'NumAlg')
  ALTER TABLE pcbdata ADD NumAlg INT NOT NULL DEFAULT (0);
  
  
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'PcbVersionId')
  ALTER TABLE vehicle ADD PcbVersionId INT NULL DEFAULT (-1);
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'Category_id3')
  ALTER TABLE vehicle ADD Category_id3 INT NULL DEFAULT (NULL);
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'vehicle'
                 AND COLUMN_NAME = 'Category_id4')
  ALTER TABLE vehicle ADD Category_id4 INT NULL DEFAULT (NULL);
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'setting'
                 AND COLUMN_NAME = 'TimeMinimMovingSize')
  ALTER TABLE setting ADD TimeMinimMovingSize TIME NOT NULL DEFAULT ('00:00:00');
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'setting'
                 AND COLUMN_NAME = 'MinTimeSwitch')
  ALTER TABLE setting ADD MinTimeSwitch INT NOT NULL DEFAULT (0);
  IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'driver'
                 AND COLUMN_NAME = 'numTelephone')
  ALTER TABLE driver ADD numTelephone VARCHAR(32) NOT NULL DEFAULT '';
IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'pcbdata'
                 AND COLUMN_NAME = 'S')
  ALTER TABLE pcbdata ADD S tinyint NOT NULL DEFAULT (0);
IF NOT EXISTS (SELECT *
               FROM
                 INFORMATION_SCHEMA.COLUMNS
               WHERE
                 TABLE_NAME = 'pcbdata'
                 AND COLUMN_NAME = 'Shifter')
  ALTER TABLE pcbdata ADD Shifter float NOT NULL DEFAULT (0);

  IF NOT EXISTS	(SELECT * FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = 'driver' AND
			      COLUMN_NAME = 'Department')
  ALTER TABLE driver ADD Department varchar(40) DEFAULT NULL;

    IF NOT EXISTS	(SELECT * FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = 'ntf_main' AND
			      COLUMN_NAME = 'Period')
  ALTER TABLE ntf_main ADD Period INT NOT NULL DEFAULT 0;



