﻿DROP PROCEDURE IF EXISTS rcs_Vehicles_Team_Driver_Update;
CREATE PROCEDURE rcs_Vehicles_Team_Driver_Update()
BEGIN
       
    IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "OutLinkId")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `OutLinkId` VARCHAR(20)  NULL COMMENT 'Код внешней базы';
	END IF;
	
    IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "team") AND
        (COLUMN_NAME = "OutLinkId")
	)
	THEN
		ALTER TABLE `team` ADD COLUMN `OutLinkId` VARCHAR(20)  NULL COMMENT 'Код внешней базы';
		ALTER TABLE team MODIFY `Name` VARCHAR(50);
	END IF;	
	
    IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "OutLinkId")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `OutLinkId` VARCHAR(20)  NULL COMMENT 'Код внешней базы';
	END IF;	
	
	CREATE TABLE IF NOT EXISTS driver_types(
	Id INT(11) NOT NULL AUTO_INCREMENT,
	TypeName VARCHAR(50) DEFAULT NULL COMMENT 'Специализация водителя',
	PRIMARY KEY (Id),
	UNIQUE INDEX id (Id)
	)
	ENGINE = INNODB
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci;
		
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "idType")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `idType` int  NULL default 0 COMMENT 'Код специализации водителя';
	END IF;	

	CREATE TABLE IF NOT EXISTS vehicle_category(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(50) NOT NULL COMMENT 'Категория транспортного средства',
  Tonnage DECIMAL(10, 3) NOT NULL DEFAULT 0.000 COMMENT 'Грузоподъемность,  тонн',
  FuelNormMoving DOUBLE NOT NULL DEFAULT 0 COMMENT 'норма расхода топлива в движении л/100 км',
  FuelNormParking DOUBLE NOT NULL DEFAULT 0 COMMENT 'норма расхода топлива на холостых оборотах л/час',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id` int  NULL  COMMENT 'Категория транспортая';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id` FOREIGN KEY (Category_id)
		REFERENCES vehicle_category (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;	
	
CREATE TABLE IF NOT EXISTS vehicle_category2(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(255) NOT NULL COMMENT 'Категория2 транспортного средства',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id2")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id2` int  NULL  COMMENT 'Категория2 транспорта';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id2` FOREIGN KEY (Category_id2)
		REFERENCES vehicle_category2 (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "PcbVersionId")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `PcbVersionId` int(11) DEFAULT - 1 COMMENT 'Версия плат датчика назначенная на машину';
	END IF;
	CREATE TABLE IF NOT EXISTS vehicle_category3(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(255) NOT NULL COMMENT 'Категория3 транспортного средства',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id3")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id3` int  NULL  COMMENT 'Категория3 транспорта';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id3` FOREIGN KEY (Category_id3)
		REFERENCES vehicle_category3 (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;
	    CREATE TABLE IF NOT EXISTS vehicle_category4(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(255) NOT NULL COMMENT 'Категория4 транспортного средства',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id4")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id4` int  NULL  COMMENT 'Категория4 транспорта';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id4` FOREIGN KEY (Category_id4)
		REFERENCES vehicle_category4 (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata")	)
	THEN
CREATE TABLE pcbdata (
  Id int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Индекс',
  Typedata varchar(255) NOT NULL DEFAULT ' ' COMMENT 'Тип данных',
  koeffK double NOT NULL DEFAULT 0 COMMENT 'Коеффициент К',
  Startbit int(11) NOT NULL DEFAULT 0 COMMENT 'Стартовый бит поля',
  Lengthbit int(11) NOT NULL DEFAULT 0 COMMENT 'Длина поля',
  Comment varchar(255) DEFAULT ' ' COMMENT 'Коментарий',
  Idpcb int(11) NOT NULL DEFAULT 0 COMMENT 'Индекс платы датчика',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 11
AVG_ROW_LENGTH = 2340
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = 'Описание платы датчиков';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbversion")	)
	THEN
CREATE TABLE pcbversion (
  Id int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Индекс',
  Name varchar(50) NOT NULL DEFAULT '*' COMMENT 'Имя платы датчика',
  Comment varchar(255) DEFAULT ' ' COMMENT 'Комментарий',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 18
AVG_ROW_LENGTH = 5461
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = 'Распределение данных для версии исполнения платы датчиков';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zone_category")	)
	THEN
CREATE TABLE zone_category (
  Id int(11) NOT NULL AUTO_INCREMENT,
  Name varchar(255) DEFAULT NULL COMMENT 'Категория зоны',
  PRIMARY KEY (Id))
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = 'Категории для зон';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zone_category2")	)
	THEN
CREATE TABLE zone_category2 (
  Id int(11) NOT NULL AUTO_INCREMENT,
  Name varchar(255) DEFAULT NULL COMMENT 'Категория2 зоны',
  PRIMARY KEY (Id))
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = 'Категории2 для зон';
END IF;
IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle_commentary")	)
	THEN
CREATE TABLE vehicle_commentary (
  Id int NOT NULL AUTO_INCREMENT,
  `Comment` varchar(255) DEFAULT NULL,
  Mobitel_id int NOT NULL,
  PRIMARY KEY (Id))
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = 'Комментарий для машины';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "setting") AND
        (COLUMN_NAME = "TimeMinimMovingSize")
	)
	THEN
		ALTER TABLE `setting` ADD COLUMN `TimeMinimMovingSize` time NOT NULL DEFAULT '00:00:00' COMMENT 'Минимальное время движения в начале смены';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbversion") AND
        (COLUMN_NAME = "MobitelId")
	)
	THEN
		ALTER TABLE `pcbversion` ADD COLUMN `MobitelId` int(11) DEFAULT - 1 COMMENT 'ID машины на которую назначена эта плата датчиков';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "mobitels") AND
        (COLUMN_NAME = "Is64bitPackets")
	)
	THEN
		ALTER TABLE `mobitels` ADD COLUMN `Is64bitPackets` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'передача данных идет 64 байтными пакетами';
	END IF;

	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "mobitels") AND
        (COLUMN_NAME = "IsNotDrawDgps")
	)
	THEN
		ALTER TABLE `mobitels` ADD COLUMN `IsNotDrawDgps` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'отображать или нет данные DGPS на карте';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zones") AND
        (COLUMN_NAME = "Category_id")
	)
	THEN
		ALTER TABLE `zones` ADD COLUMN `Category_id` int(11) DEFAULT NULL COMMENT 'Категория для зоны';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zones") AND
        (COLUMN_NAME = "Category_id2")
	)
	THEN
		ALTER TABLE `zones` ADD COLUMN `Category_id2` int(11) DEFAULT NULL COMMENT 'Категория2 для зоны';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "numTelephone")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `numTelephone` varchar(32) DEFAULT '' COMMENT 'Телефон водителя для связи';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "Department")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `Department` varchar(40) NULL COMMENT 'Организация водителя';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "NameVal")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `NameVal` varchar(12) DEFAULT ' ' COMMENT 'Name meassurement value';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "NumAlg")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `NumAlg` int(11) DEFAULT 0 COMMENT 'Number of algorithm';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "sensors") AND
        (COLUMN_NAME = "B")
	)
	THEN
		ALTER TABLE `sensors` ADD COLUMN `B` double DEFAULT 0 COMMENT 'Shift coefficient';
	END IF;
IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "Shifter")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `Shifter` double DEFAULT 0 COMMENT 'Shift coefficient';
	END IF;


IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "sensors") AND
        (COLUMN_NAME = "S")
	)
	THEN
		ALTER TABLE `sensors` ADD COLUMN `S` tinyint(1) DEFAULT 0 COMMENT 'Sign arifmetic';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "S")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `S` tinyint(1) DEFAULT 0 COMMENT 'Sign arifmetic';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "datagpslost_on") AND
        (COLUMN_NAME = "Id")
	)
	THEN
		ALTER TABLE `datagpslost_on` ADD COLUMN `Id` bigint(20) UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT;
	END IF;


IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "datagpsbuffer_on64")
	THEN
	ALTER TABLE datagpsbuffer_on64 MODIFY DGPS varchar(32);
	END IF;


END;
CALL rcs_Vehicles_Team_Driver_Update();
DROP PROCEDURE IF EXISTS rcs_Vehicles_Team_Driver_Update;
