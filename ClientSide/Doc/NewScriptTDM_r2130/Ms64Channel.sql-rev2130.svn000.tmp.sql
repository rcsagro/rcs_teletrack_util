﻿/****** Object:  Table [dbo].[datagps64]    Script Date: 16.07.2015 13:13:26 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagps64]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagps64](
	[DataGps_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NOT NULL,
	[Latitude] [int] NOT NULL,
	[Longitude] [int] NOT NULL,
	[Direction] [tinyint] NULL,
	[Acceleration] [tinyint] NULL,
	[UnixTime] [int] NOT NULL,
	[Speed] [smallint] NOT NULL,
	[Valid] [bit] NOT NULL,
	[Satellites] [smallint] NULL,
	[RssiGsm] [smallint] NULL,
	[Events] [int] NULL,
	[SensorsSet] [smallint] NULL,
	[Sensors] [varchar](50) NULL,
	[Voltage] [smallint] NULL,
	[DGPS] [varchar](24) NULL,
	[LogID] [int] NULL,
	[SrvPacketID] [bigint] NOT NULL,
 CONSTRAINT [PK_datagps64] PRIMARY KEY CLUSTERED 
(
	[DataGps_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
 
SET ANSI_PADDING OFF
 
 
/****** Object:  Index [IDX_MobitelIDLogID64]    Script Date: 20.10.2015 15:11:17 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps64]') AND name = N'IDX_MobitelIDLogID64')
CREATE NONCLUSTERED INDEX [IDX_MobitelIDLogID64] ON [dbo].[datagps64]
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [IDX_MobitelIDSrvPacketID64]    Script Date: 20.10.2015 15:11:17 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps64]') AND name = N'IDX_MobitelIDSrvPacketID64')
CREATE NONCLUSTERED INDEX [IDX_MobitelIDSrvPacketID64] ON [dbo].[datagps64]
(
	[Mobitel_ID] ASC,
	[SrvPacketID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [IDX_MobitelIDUnixTimeValid64]    Script Date: 20.10.2015 15:11:17 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[datagps64]') AND name = N'IDX_MobitelIDUnixTimeValid64')
CREATE NONCLUSTERED INDEX [IDX_MobitelIDUnixTimeValid64] ON [dbo].[datagps64]
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC,
	[Valid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

 
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'Mobitel_ID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Код Телетрека' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'Mobitel_ID'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'Latitude'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Широта' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'Latitude'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'Longitude'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Долгота' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'Longitude'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'Direction'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'градусы/1.5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'Direction'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'Acceleration'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Изменение скорости по GPS за последнюю секунду' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'Acceleration'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'Speed'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Сотен метров в час (0,1 км/ч)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'Speed'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'Satellites'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Количество спутников ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'Satellites'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'RssiGsm'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Уровень GSM сигнала' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'RssiGsm'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'Events'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Флаги событий' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'Events'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'SensorsSet'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Код набора датчиков' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'SensorsSet'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'Sensors'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Значения датчиков' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'Sensors'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'Voltage'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Напряжение питания текущего источника/1.5 от 0 до 38,2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'Voltage'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'DGPS'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Данные дифференциальной системы GPS - высота,широта,долгота' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'DGPS'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'LogID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Идентификатор записи в журнале Телетрека' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'LogID'
 
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'datagps64', N'COLUMN',N'SrvPacketID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID серверного пакета в состав которого входит эта запись' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'datagps64', @level2type=N'COLUMN',@level2name=N'SrvPacketID'

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_on64]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer_on64](
	[Mobitel_ID] [int] NOT NULL,
	[Latitude] [int] NOT NULL,
	[Longitude] [int] NOT NULL,
	[Direction] [tinyint] NULL,
	[Acceleration] [tinyint] NULL,
	[UnixTime] [int] NOT NULL,
	[Speed] [smallint] NULL,
	[Valid] [bit] NOT NULL,
	[Satellites] [smallint] NULL,
	[RssiGsm] [smallint] NULL,
	[Events] [int] NULL,
	[SensorsSet] [smallint] NOT NULL,
	[Sensors] [varchar](50) NULL,
	[Voltage] [smallint] NULL,
	[DGPS] [varchar](24) NULL,
	[LogID] [int] NULL,
	[SrvPacketID] [bigint] NULL
) ON [PRIMARY]

END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[datagpsbuffer_ontmp64]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[datagpsbuffer_ontmp64](
	[Mobitel_ID] [int] NOT NULL,
	[LogID] [int] NOT NULL,
	[SrvPacketID] [bigint] NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[datagpsbuffer_ontmp64]  WITH CHECK ADD  CONSTRAINT [FK_datagps64_mobitels_Mobitel_ID_2] FOREIGN KEY([Mobitel_ID])
REFERENCES [dbo].[mobitels] ([Mobitel_ID])
ON UPDATE CASCADE
ON DELETE CASCADE

ALTER TABLE [dbo].[datagpsbuffer_ontmp64] CHECK CONSTRAINT [FK_datagps64_mobitels_Mobitel_ID_2]

END;

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[online64]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[online64](
	[DataGps_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Mobitel_ID] [int] NOT NULL,
	[Latitude] [int] NOT NULL,
	[Longitude] [int] NOT NULL,
	[Direction] [tinyint] NULL,
	[Acceleration] [tinyint] NULL,
	[UnixTime] [int] NOT NULL,
	[Speed] [smallint] NOT NULL,
	[Valid] [bit] NOT NULL,
	[Satellites] [smallint] NULL,
	[RssiGsm] [smallint] NULL,
	[Events] [int] NULL,
	[SensorsSet] [smallint] NULL,
	[Sensors] [varchar](50) NULL,
	[Voltage] [smallint] NULL,
	[DGPS] [varchar](24) NULL,
	[LogID] [int] NULL,
	[SrvPacketID] [bigint] NULL,
CONSTRAINT [PK_online64] PRIMARY KEY CLUSTERED 
(
	[DataGps_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Index [IDX_Mobitelid64]    Script Date: 26.02.2016 14:55:18 ******/
CREATE NONCLUSTERED INDEX [IDX_Mobitelid64] ON [dbo].[online64]
(
	[Mobitel_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [IDX_MobitelidLogid64]    Script Date: 26.02.2016 14:55:18 ******/
CREATE NONCLUSTERED INDEX [IDX_MobitelidLogid64] ON [dbo].[online64]
(
	[Mobitel_ID] ASC,
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [IDX_MobitelidUnixtime64]    Script Date: 26.02.2016 14:55:18 ******/
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtime64] ON [dbo].[online64]
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [IDX_MobitelidUnixtimeValid64]    Script Date: 26.02.2016 14:55:18 ******/
CREATE NONCLUSTERED INDEX [IDX_MobitelidUnixtimeValid64] ON [dbo].[online64]
(
	[Mobitel_ID] ASC,
	[UnixTime] ASC,
	[Valid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

END;


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnCorrectInfotrackLogId64]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OnCorrectInfotrackLogId64]
AS
--SQL SECURITY INVOKER
BEGIN
  DECLARE @MobitelIDInCursor INT; /* Текущее значение MobitelID  */
  DECLARE @DataExists TINYINT; /* Флаг наличия данных после фетча */
  SET @DataExists = 1;
  DECLARE @MaxLogId INT; /* Максимальный LogId для данного дивайса*/
  SET @MaxLogId = 0;
  DECLARE @SrvPacketIDInCursor BIGINT; /* Текущее значение серверного пакета  */
  SET @SrvPacketIDInCursor = 0;

  /* Курсор по инфотрекам в множестве новых данных. 
     Все инфотреки начинаются с буквы I  */
  DECLARE CursorInfoTrack CURSOR LOCAL FOR
  SELECT DISTINCT buf.Mobitel_ID
  FROM
    datagpsbuffer_on64 buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE
    (c.InternalMobitelConfig_ID = (SELECT TOP 1 intConf.InternalMobitelConfig_ID
                                   FROM
                                     internalmobitelconfig intConf
                                   WHERE
                                     (intConf.ID = c.ID)
                                   ORDER BY
                                     intConf.InternalMobitelConfig_ID DESC))
    AND (c.devIdShort LIKE ''I%'')
    AND (buf.LogId = 0)
  ORDER BY
    1;

  /* ?????? ?? ?????? ????????? */
  DECLARE CursorInfoTrackUpdateSet CURSOR LOCAL FOR
  SELECT SrvPacketID
  FROM
    datagpsbuffer_on
  WHERE
    (Mobitel_ID = @MobitelIDInCursor)
    AND (LogId = 0)
  ORDER BY
    UnixTime;

  /*  Курсор по данным телетрека */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    /* Находим максимальный LogId для данного инфотрека */
    SET @MaxLogId = (SELECT coalesce(max(LogID), 0)
                     FROM
                       DataGps
                     WHERE
                       Mobitel_ID = @MobitelIDInCursor);

    /* Обновляем LogId и InMobitelID в наборе новых данных текущего инфотрека */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    WHILE @@fetch_status = 0
    BEGIN
      SET @MaxLogId = @MaxLogId + 1;

      UPDATE datagpsbuffer_on
      SET
        LogId = @MaxLogId, InMobitelID = @MaxLogId
      WHERE
        SrvPacketID = @SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO @SrvPacketIDInCursor;
    END;
    CLOSE CursorInfoTrackUpdateSet;
    --DEALLOCATE CursorInfoTrackUpdateSet;

    --SET DataExists = 1;
    FETCH CursorInfoTrack INTO @MobitelIDInCursor;
  END;
  CLOSE CursorInfoTrack;
--DEALLOCATE CursorInfoTrack;
END;'
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnDeleteDuplicates64]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OnDeleteDuplicates64]
AS
BEGIN
  /* Удаление повторяющихся строк из datagpsbuffer_on64 */

  /* Текущий MobitelId в курсоре CursorDuplicateGroups*/
  DECLARE @MobitelIdInCursor INT;
  SET @MobitelIdInCursor = 0;
  /* Текущий LogID в курсоре CursorDuplicateGroups  */
  DECLARE @LogIdInCursor INT;
  SET @LogIdInCursor = 0;
  /* Max SrvPacketId в множестве строк текущей группы дубликатов */
  DECLARE @MaxSrvPacketId BIGINT;
  SET @MaxSrvPacketId = 0;
  DECLARE @DataExists TINYINT;
  SET @DataExists = 1;

  /* Курсор для прохода по всем группам дубликатов */
  DECLARE CursorDuplicateGroups CURSOR LOCAL FOR
  SELECT Mobitel_Id
       , LogId
  FROM
    datagpsbuffer_on64
  GROUP BY
    Mobitel_Id
  , LogId
  HAVING
    count(LogId) > 1;

  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  --SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  WHILE @@fetch_status = 0
  BEGIN
    SELECT @MaxSrvPacketId = max(SrvPacketID)
    FROM
      datagpsbuffer_on64
    WHERE
      (Mobitel_ID = @MobitelIdInCursor)
      AND (LogId = @LogIdInCursor);
    DELETE
    FROM
      datagpsbuffer_on64
    WHERE
      (Mobitel_ID = @MobitelIdInCursor) AND
      (LogId = @LogIdInCursor) AND
      (SrvPacketID < @MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO @MobitelIdInCursor, @LogIdInCursor;
  END;
  CLOSE CursorDuplicateGroups;
--DEALLOCATE CursorDuplicateGroups;
END;'
END



IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnLostDataGPS264]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OnLostDataGPS264]
(
  @MobitelID    INTEGER,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
BEGIN
  /********** DECLARE **********/

  DECLARE @NewConfirmedID INT;
  SET @NewConfirmedID = 0; /* Новое значение ConfirmedID  */
  DECLARE @BeginLogID INT; /* Begin_LogID в курсоре CursorLostRanges */
  DECLARE @EndLogID INT; /* End_LogID в курсоре CursorLostRanges */
  DECLARE @BeginSrvPacketID BIGINT; /*  Begin_SrvPacketID в курсоре CursorLostRanges */
  DECLARE @EndSrvPacketID BIGINT; /* End_SrvPacketID в курсоре CursorLostRanges  */
  DECLARE @RowCountForAnalyze INT; /* Количество записей для анализа  */
  SET @RowCountForAnalyze = 0;
  --DECLARE @DataExists TINYINT; /* ???? ??????? ?????? ????? ????? */
  --SET @DataExists = 1;
  DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
  DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
  DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
  DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
  DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

  /* Курсор для прохода по диапазонам пропусков телетрека */
  DECLARE CursorLostRanges CURSOR LOCAL FOR
  SELECT Begin_LogID
       , End_LogID
       , Begin_SrvPacketID
       , End_SrvPacketID
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* Курсор для прохода по новым LogID */
  DECLARE CursorNewLogID2 CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp
  ORDER BY
    SrvPacketID;

  /* Обработчик отсутствующей записи при FETCH следующей записи*/
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* Перебираем диапазоны и сокращаем разрывы если это возможно  */
  --SET DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;

  WHILE @@fetch_status = 0
  BEGIN
    /* Заполнение временной таблицы дублирующими данными телетрека
       для анализа разрывов */
    INSERT INTO datagpslost_ontmp(LogID
                                , SrvPacketID)
    SELECT DISTINCT LogID
                  , SrvPacketID
    FROM
      datagpsbuffer_ontmp64
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (SrvPacketID > @BeginSrvPacketID)
      AND (SrvPacketID < @EndSrvPacketID)
    ORDER BY
      SrvPacketID;

     /* Если есть данные модифицируем диапазон */  
    SET @RowCountForAnalyze = (SELECT count(1)
                               FROM
                                 datagpslost_ontmp);

    IF @RowCountForAnalyze > 0
    BEGIN
      /* Добавляем первую и последнюю записи для анализа */ 
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@BeginLogID, @BeginSrvPacketID)
      , (@EndLogID, @EndSrvPacketID);

       /* Удаляем старый диапазон */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID = @BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        --DECLARE @NewLogID INT; /* ???????? LogID ? ??????? CursorNewLogID2 */
        --DECLARE @NewSrvPacketID BIGINT; /* ???????? SrvPacketID ? ??????? CursorNewLogID2 */
        --DECLARE @FirstLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --SET @FirstLogID = -9999999;
        --DECLARE @SecondLogID INT; /* ?????? ???????? LogID ??? ??????? */
        --DECLARE @FirstSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */
        --DECLARE @SecondSrvPacketID BIGINT; /* ?????? ???????? SrvPacketID */

        /* ?????????? ????????????? ?????? ??? FETCH ????????? ?????? */
        --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* Формирование команды-вставки в таблицу пропущенных диапазонов */
        --SET DataExists = 1;
        OPEN CursorNewLogID2;
        FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;

        WHILE @@fetch_status = 0
        BEGIN
          IF @FirstLogID = -9999999
          BEGIN
            /* Первое значение в списке */
            SET @FirstLogID = @NewLogID;
            SET @FirstSrvPacketID = @NewSrvPacketID;
          END
          ELSE
          BEGIN
            /* Основная ветка расчета*/
            SET @SecondLogID = @NewLogID;
            SET @SecondSrvPacketID = @NewSrvPacketID;

            IF (@SecondLogID - @FirstLogID) > 1 begin
          /* Найден разрыв */
          /* Из-за всяких комбинаций состояний LogId и SrvPacketId из-за особенностей работы телематического сервера
          и ТДМ вводим дополнительный анализ вставляемых записей, если запись не соответсвует указаным правилам, она 
          отбрасывается */
          IF (@SecondSrvPacketID > @FirstSrvPacketID) begin
            IF EXISTS(SELECT NULL FROM datagps64 d WHERE (d.Mobitel_ID = @MobitelID) AND ((d.LogID = @FirstLogID) AND (d.SrvPacketID = @FirstSrvPacketID))) begin
              IF EXISTS(SELECT NULL FROM datagps64 d WHERE (d.Mobitel_ID = @MobitelID) AND ((d.LogID = @SecondLogID) AND (d.SrvPacketID = @SecondSrvPacketID))) begin

                EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;
                IF @NeedOptimize = 0 begin SET @NeedOptimize = 1; END;
                
              END;
            END;
          END;
        END;
            /* Переместим текущее второе значение на первую позицию
               для участия в следующей итерации в качестве начального значения */
            SET @FirstLogID = @SecondLogID;
            SET @FirstSrvPacketID = @SecondSrvPacketID;
          END; -- ELSE  
          FETCH CursorNewLogID2 INTO @NewLogID, @NewSrvPacketID;
        END; -- WHILE;
        CLOSE CursorNewLogID2;
        DEALLOCATE CursorNewLogID2;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END;

    FETCH CursorLostRanges INTO @BeginLogID, @EndLogID, @BeginSrvPacketID, @EndSrvPacketID;
  END;
  CLOSE CursorLostRanges;
  --DEALLOCATE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* Если пропущенных записей у данного телетрека нет - 
     тогда ConfirmedID будет равен максимальному LogID 
     данного телетрека. Иначе - минимальный Begin_LogID
     из таблицы пропусков */
  SELECT @NewConfirmedID = min(Begin_LogID)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  IF @NewConfirmedID IS NULL
  BEGIN
    SET @NewConfirmedID = (SELECT max(LogID)
                           FROM
                             datagps64
                           WHERE
                             Mobitel_ID = @MobitelID);
  END;

  /* Обновим ConfirmedID если надо */
  UPDATE mobitels
  SET
    ConfirmedID = @NewConfirmedID
  WHERE
    (Mobitel_ID = @MobitelID) AND
    (ConfirmedID < @NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END;'
END



IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnLostDataGPS64]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OnLostDataGPS64]
(
  @MobitelID    INT,
  @NeedOptimize TINYINT OUT
)
AS
--SQL SECURITY INVOKER
BEGIN

  /********** DECLARE **********/

  DECLARE @CurrentConfirmedID INT; /* Текущее значение ConfirmedID  */
  SET @CurrentConfirmedID = 0;
  DECLARE @NewConfirmedID INT; /* Новое значение ConfirmedID */
  SET @NewConfirmedID = 0;
  DECLARE @StartLogIDSelect INT; /* min LogID для выборки */
  SET @StartLogIDSelect = 0;
  DECLARE @FinishLogIDSelect INT; /* max LogID для выборки */
  SET @FinishLogIDSelect = 0;
  DECLARE @MaxEndLogID INT; /* max End_LogID в пропущенных диапазонах данного телетрека */
  SET @MaxEndLogID = 0;

  DECLARE @MinLogID_n INT; /* Минимальный LogID после ConfirmedID в множестве новых данных */
  SET @MinLogID_n = 0;
  DECLARE @MaxLogID_n INT; /* Максимальный LogID в множестве новых данных */
  SET @MaxLogID_n = 0;

  DECLARE @DataExists TINYINT; /*  Флаг наличия данных после фетча */
  SET @DataExists = 1;

  DECLARE @NewLogID INT; /* Значение LogID в курсоре CursorNewLogID */
  DECLARE @NewSrvPacketID BIGINT; /*  Значение SrvPacketID в курсоре CursorNewLogID */
  DECLARE @FirstLogID INT; /* Первое значение LogID для анализа*/
  SET @FirstLogID = -9999999;
  DECLARE @SecondLogID INT; /* Второе значение LogID для анализа*/
  DECLARE @FirstSrvPacketID BIGINT; /* Первое значение SrvPacketID*/
  DECLARE @SecondSrvPacketID BIGINT; /* Второе значение SrvPacketID*/

  DECLARE @tmpSrvPacketID BIGINT; /* Значение SrvPacketID для первой искусственной записи в анализе новых данных, не пересекающихся с существующими*/
  SET @tmpSrvPacketID = 0;

  /* Курсор для прохода по новым LogID  */
  DECLARE CursorNewLogID CURSOR LOCAL FOR
  SELECT LogID
       , SrvPacketID
  FROM
    datagpslost_ontmp;

  /* Обработчик отсутствующей записи при FETCH следующей записи */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET @NeedOptimize = 0;

  SET @CurrentConfirmedID = (SELECT coalesce(ConfirmedID, 0)
                             FROM
                               mobitels
                             WHERE
                               Mobitel_ID = @MobitelID);

  /*  Минимальный LogID в новом наборе данных c которым будем работать */
  SELECT @MinLogID_n = min(LogID)
  FROM
    datagpsbuffer_on64
  WHERE
    (Mobitel_ID = @MobitelID)
    AND (LogID > @CurrentConfirmedID);

  /* Максимальный End_LogID в пропущенных диапазонах данного телетрека*/
  SET @MaxLogID_n = (SELECT max(LogID)
                     FROM
                       datagpsbuffer_on64
                     WHERE
                       Mobitel_ID = @MobitelID);

  /* Максимальный End_LogID в пропущенных диапазонах данного телетрека  */
  SELECT @MaxEndLogID = coalesce(max(End_LogID), 0)
  FROM
    datagpslost_on
  WHERE
    Mobitel_ID = @MobitelID;

  /* Проверим есть ли данные для анализа */
  IF @MinLogID_n IS NOT NULL
  BEGIN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF @MinLogID_n < @MaxEndLogID
    BEGIN
      /* Если  MinLogID_n < MaxEndLogID значит пришли данные, 
       которые возможно закроют часть пропусков. */
       
      /* Находим стартовый LogID для пересчета. */
      SET @StartLogIDSelect = (SELECT coalesce(Begin_LogID, @MinLogID_n)
                               FROM
                                 datagpslost_on
                               WHERE
                                 (Mobitel_ID = @MobitelID)
                                 AND (@MinLogID_n > Begin_LogID)
                                 AND (@MinLogID_n < End_LogID));

      /* Находим последний LogID для пересчета. */
      SET @FinishLogIDSelect = (SELECT coalesce(End_LogID, @MaxLogID_n)
                                FROM
                                  datagpslost_on
                                WHERE
                                  (Mobitel_ID = @MobitelID)
                                  AND (@MaxLogID_n > Begin_LogID)
                                  AND (@MaxLogID_n < End_LogID));

      /* Удалим старые записи из реестра пропущенных записей 
         (стали неактуальными, надо пересчитать) */
      DELETE
      FROM
        datagpslost_on
      WHERE
        (Mobitel_ID = @MobitelID) AND
        (Begin_LogID >= @StartLogIDSelect) AND
        (Begin_LogID <= @FinishLogIDSelect);

      /* Если первая запись LogID = 0 и этой записи нет в DataGps, тогда
         надо добавить первую фиктивную запись для анализа. */
      IF (@StartLogIDSelect = 0) AND (NOT EXISTS (SELECT 1
                                                  FROM
                                                    datagps64
                                                  WHERE
                                                    (Mobitel_ID = @MobitelID)
                                                    AND (LogID = @StartLogIDSelect)))
      BEGIN
        INSERT INTO datagpslost_ontmp(LogID
                                    , SrvPacketID)
        VALUES
          (0, 0);
      END;
    END;
    ELSE
    BEGIN

      /* Если  MinLogID_n < MaxEndLogID значит пришли данные, 
       которые возможно закроют часть пропусков. */
       
      /* Находим стартовый LogID для пересчета. */
      SELECT @StartLogIDSelect = coalesce(max(End_LogID), @CurrentConfirmedID)
      FROM
        datagpslost_on
      WHERE
        Mobitel_ID = @MobitelID;

      /* Находим последний LogID для пересчета. */  
      SELECT @FinishLogIDSelect = max(LogID)
      FROM
        datagpsbuffer_on64
      WHERE
        Mobitel_ID = @MobitelID;

      /* Удалим старые записи из реестра пропущенных записей 
         (стали неактуальными, надо пересчитать) */
      SELECT @tmpSrvPacketID = coalesce(SrvPacketID, 0)
      FROM
        datagps64
      WHERE
        (Mobitel_ID = @MobitelID)
        AND (LogID = @StartLogIDSelect);

      /* Помещаем в таблицу datagpslost_ontmp для анализа пропусков 
         дополнительную первую запись */
      INSERT INTO datagpslost_ontmp(LogID
                                  , SrvPacketID)
      VALUES
        (@StartLogIDSelect, @tmpSrvPacketID);
    END; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

    /* Заполнение временной таблицы последними данными телетрека
       для анализа разрывов */
    INSERT INTO datagpslost_ontmp(LogID, SrvPacketID)
    SELECT LogID
         , SrvPacketID
    FROM
      datagps64
    WHERE
      (Mobitel_ID = @MobitelID)
      AND (LogID >= @StartLogIDSelect)
      AND (LogID <= @FinishLogIDSelect)
    ORDER BY
      LogID;

     /* Формирование команды-вставки в таблицу пропущенных диапазонов */
    --SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    WHILE @@fetch_status = 0
    BEGIN
      IF @FirstLogID = -9999999
      BEGIN
        /* Первое значение в списке */
        SET @FirstLogID = @NewLogID;
        SET @FirstSrvPacketID = @NewSrvPacketID;
      END
      ELSE
      BEGIN
        /* Основная ветка расчета*/
        SET @SecondLogID = @NewLogID;
        SET @SecondSrvPacketID = @NewSrvPacketID;

        IF (@SecondLogID - @FirstLogID) > 1 begin
          /* Найден разрыв */
          /* Из-за всяких комбинаций состояний LogId и SrvPacketId из-за особенностей работы телематического сервера
          и ТДМ вводим дополнительный анализ вставляемых записей, если запись не соответсвует указаным правилам, она 
          отбрасывается */
          IF (@SecondSrvPacketID > @FirstSrvPacketID) begin
            IF EXISTS(SELECT NULL FROM datagps64 d WHERE (d.Mobitel_ID = @MobitelID) AND ((d.LogID = @FirstLogID) AND (d.SrvPacketID = @FirstSrvPacketID))) begin
              IF EXISTS(SELECT NULL FROM datagps64 d WHERE (d.Mobitel_ID = @MobitelID) AND ((d.LogID = @SecondLogID) AND (d.SrvPacketID = @SecondSrvPacketID))) begin

                EXEC OnInsertDatagpsLost @MobitelID, @FirstLogID, @SecondLogID, @FirstSrvPacketID, @SecondSrvPacketID;
                IF @NeedOptimize = 0 begin SET @NeedOptimize = 1; END;
                
              END;
            END;
          END;
        END;        /* Переместим текущее второе значение на первую позицию
           для участия в следующей итерации в качестве начального значения */
        SET @FirstLogID = @SecondLogID;
        SET @FirstSrvPacketID = @SecondSrvPacketID;
      END; -- IF
      FETCH CursorNewLogID INTO @NewLogID, @NewSrvPacketID;
    END; -- WHILE
    CLOSE CursorNewLogID;
    --DEALLOCATE CursorNewLogID; 

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

    /* Если пропущенных записей у данного телетрека нет - 
       тогда ConfirmedID будет равен максимальному LogID 
       данного телетрека. Иначе - минимальный Begin_LogID
       из таблицы пропусков */
    SET @NewConfirmedID = (SELECT min(Begin_LogID)
                           FROM
                             datagpslost_on
                           WHERE
                             Mobitel_ID = @MobitelID);

    IF @NewConfirmedID IS NULL
    BEGIN
      SET @NewConfirmedID = (SELECT max(LogID)
                             FROM
                               datagps64
                             WHERE
                               Mobitel_ID = @MobitelID);
    END;

    /* Обновим ConfirmedID если надо */
    UPDATE mobitels
    SET
      ConfirmedID = @NewConfirmedID
    WHERE
      (Mobitel_ID = @MobitelID) AND
      (ConfirmedID < @NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END; -- IF MaxEndLogID IS NOT NULL  
END;'
END

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnTransferBuffer64]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[OnTransferBuffer64]
AS
BEGIN
  DECLARE @MobitelIDInCursor INT; /*  MobitelID */
  --DECLARE @DataExists TINYINT; /*  */
  --SET @DataExists = 1;
  DECLARE @NeedOptimize TINYINT; /*  */
  SET @NeedOptimize = 0;
  DECLARE @NeedOptimizeParam TINYINT;
  DECLARE @TmpMaxUnixTime INT; /*  Max UnixTime  online */

  /*  */
  DECLARE CursorMobitelsInsertSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_on64;
  /*  */
  DECLARE CursorMobitelsUpdateSet CURSOR LOCAL FOR
  SELECT DISTINCT (Mobitel_ID)
  FROM
    datagpsbuffer_ontmp64;

  /*  FETCH  */
  --DECLARE CONTINUE HANDLER FOR NOT FOUND SET @DataExists = 0

  /*DECLARE @HANDLER INT*/

  EXEC OnDeleteDuplicates64
  --EXEC OnCorrectInfotrackLogId64

  /* OnLostDataGPS2. */
  INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID)
    SELECT b.Mobitel_ID, b.LogID, b.SrvPacketID
    FROM datagpsbuffer_on64 b LEFT JOIN datagps64 d ON
      (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL;
       

  /* --------------------  Online ---------------------- */
  /*  online  datagpsbuffer_on64 */
  /*  Mobitel_ID LogID */
   DELETE o
  FROM online64 o, datagpsbuffer_on64 dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);

  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /*  Max  UnixTime online 
       . p.s. UnixTime  online  */
    --INSERT INTO @TmpMaxUnixTime
    SET @TmpMaxUnixTime = (SELECT coalesce(max(UnixTime), 0)
                           FROM
                             online64 WITH (INDEX (IDX_Mobitelid64))
                           WHERE
                             Mobitel_ID = @MobitelIDInCursor);

    /*  100 online */
	      INSERT INTO online64 (
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude,Acceleration, 
      Direction, Speed, Valid, Events,
      Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS)
    SELECT
      Mobitel_ID, LogID, UnixTime, Latitude, Longitude,Acceleration, 
      Direction, Speed, Valid, Events,
      Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS
    FROM (
      SELECT TOP 100 *
      FROM datagpsbuffer_on64
      WHERE (Mobitel_ID = @MobitelIDInCursor)  AND 
        (UnixTime > @TmpMaxUnixTime) AND (Valid = 1)
      ORDER BY UnixTime DESC
      ) AS T
    ORDER BY UnixTime ASC;

    /*  100 */
    DELETE
    FROM
      online64
    WHERE
      (Mobitel_ID = @MobitelIDInCursor) AND
      (UnixTime < (SELECT coalesce(min(UnixTime), 0)
                   FROM
                     (SELECT TOP 100 UnixTime
                      FROM
                        online
                      WHERE
                        Mobitel_ID = @MobitelIDInCursor
                      ORDER BY
                        UnixTime DESC) T1));


    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */

  UPDATE datagps64 SET
    Latitude = b.Latitude,
    Longitude = b.Longitude,
    Acceleration = b.Acceleration,
    UnixTime = b.UnixTime,
    Speed = b.Speed,
    Direction = b.Direction,
    Valid = b.Valid,
    LogID = b.LogID,
    Events = b.Events,
    Satellites = b.Satellites,
    RssiGsm = b.RssiGsm,
    SensorsSet = b.SensorsSet,
    Sensors = b.Sensors,
    Voltage = b.Voltage,
    DGPS = b.DGPS,
    SrvPacketID = b.SrvPacketID
  FROM
    datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    AND (d.SrvPacketID < b.SrvPacketID);
  /*  */
  DELETE b
  FROM datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);
  /* */
    INSERT INTO datagps64 (
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, Events, 
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID)
  SELECT
    Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, Events, 
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID
  FROM datagpsbuffer_on64;
  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ------  ------- */
    UPDATE mobitels
    SET
      LastInsertTime = dbo.UNIX_TIMESTAMPS(getdate())
    WHERE
      Mobitel_ID = @MobitelIDInCursor;

    /* ------  -------------- */
    EXEC OnLostDataGPS64 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);*/

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsInsertSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsInsertSet;
  --DEALLOCATE CursorMobitelsInsertSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_on64;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  --SET @DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  --WHILE @DataExists = 1
  WHILE (@@fetch_status = 0)
  BEGIN
    /* ----  --- */
    EXEC OnLostDataGPS264 @MobitelIDInCursor, @NeedOptimizeParam OUT
    /* SET @NeedOptimize = CREATEST(@NeedOptimize, @NeedOptimizeParam); */

    IF @NeedOptimize > @NeedOptimizeParam
      SET @NeedOptimize = @NeedOptimize
    ELSE
      SET @NeedOptimize = @NeedOptimizeParam

    FETCH CursorMobitelsUpdateSet INTO @MobitelIDInCursor;
  END
  CLOSE CursorMobitelsUpdateSet;
  --DEALLOCATE CursorMobitelsUpdateSet;

  /*  */
  TRUNCATE TABLE datagpsbuffer_ontmp64;

  /*  */
  /*IF @NeedOptimize = 1
    ALTER INDEX ALL ON datagpslost_on REORGANIZE*/
END;'
END