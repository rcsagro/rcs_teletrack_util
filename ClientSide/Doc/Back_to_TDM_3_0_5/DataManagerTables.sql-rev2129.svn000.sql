DROP PROCEDURE IF EXISTS tmpAlterDmTables;
CREATE PROCEDURE tmpAlterDmTables()
BEGIN
DROP TABLE IF EXISTS datagpsbuffer_on;
CREATE TABLE IF NOT EXISTS datagpsbuffer_on (
  Mobitel_ID int(11) NOT NULL default 0,
  Message_ID int(11) default 0,
  Latitude int(11) NOT NULL default 0,
  Longitude int(11) NOT NULL default 0,
  Altitude int(11) default NULL,
  UnixTime int(11) NOT NULL default 0,
  Speed smallint(6) NOT NULL default 0,
  Direction int(11) NOT NULL default 0,
  Valid tinyint(4) NOT NULL default 1,
  InMobitelID int(11) default NULL,
  Events int(10) unsigned default 0,
  Sensor1 tinyint(4) unsigned default 0,
  Sensor2 tinyint(4) unsigned default 0,
  Sensor3 tinyint(4) unsigned default 0,
  Sensor4 tinyint(4) unsigned default 0,
  Sensor5 tinyint(4) unsigned default 0,
  Sensor6 tinyint(4) unsigned default 0,
  Sensor7 tinyint(4) unsigned default 0,
  Sensor8 tinyint(4) unsigned default 0,
  LogID int(11) NOT NULL default 0,
  isShow tinyint(4) default 0,
  whatIs smallint(6) NOT NULL default 0,
  Counter1 int(11) NOT NULL default -1,
  Counter2 int(11) NOT NULL default -1,
  Counter3 int(11) NOT NULL default -1,
  Counter4 int(11) NOT NULL default -1,
  SrvPacketID BIGINT NOT NULL DEFAULT 0 COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������',
  INDEX IDX_MobitelidLogid USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid USING BTREE (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_SrvPacketID (SrvPacketID)
) ENGINE=MEMORY DEFAULT CHARSET=cp1251 COMMENT '����� ������ ������';
  END;


DROP TABLE IF EXISTS datagpsbuffer_ontmp;
CREATE TABLE IF NOT EXISTS datagpsbuffer_ontmp (
  Mobitel_ID INT(11) NOT NULL DEFAULT 0,
  LogID INT(11) NOT NULL DEFAULT 0,
  SrvPacketID BIGINT NOT NULL,
  INDEX IDX_MobitelidSrvpacketid USING BTREE (Mobitel_ID, SrvPacketID),
  INDEX IDX_Mobitelid USING BTREE (Mobitel_ID)
) ENGINE = MEMORY DEFAULT CHARSET=cp1251 COMMENT '��������� ��������������� ����� ������ ������';

DROP TABLE IF EXISTS online;
CREATE TABLE IF NOT EXISTS online (
  DataGps_ID INT(11) AUTO_INCREMENT,
  Mobitel_ID INT(11) DEFAULT 0,
  Message_ID INT(11) DEFAULT 0,
  Latitude INT(11) NOT NULL DEFAULT 0,
  Longitude INT(11) NOT NULL DEFAULT 0,
  Altitude INT(11) DEFAULT 0,
  UnixTime INT(11) NOT NULL DEFAULT 0,
  Speed SMALLINT(6) NOT NULL DEFAULT 0,
  Direction INT(11) NOT NULL DEFAULT 0,
  Valid TINYINT(4) NOT NULL DEFAULT 1,
  InMobitelID INT(11) DEFAULT 0,
  Events INT(10) UNSIGNED DEFAULT 0,
  Sensor1 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor2 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor3 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor4 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor5 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor6 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor7 TINYINT(4) UNSIGNED DEFAULT 0,
  Sensor8 TINYINT(4) UNSIGNED DEFAULT 0,
  LogID INT(11) DEFAULT 0,
  isShow TINYINT(4) DEFAULT 0,
  whatIs SMALLINT(6) UNSIGNED NOT NULL DEFAULT 0,
  Counter1 INT(11) NOT NULL DEFAULT -1,
  Counter2 INT(11) NOT NULL DEFAULT -1,
  Counter3 INT(11) NOT NULL DEFAULT -1,
  Counter4 INT(11) NOT NULL DEFAULT -1,
  PRIMARY KEY (DataGps_ID),
  INDEX IDX_MobitelidLogid USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtimeValid USING BTREE (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_MobitelidUnixtime USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_Mobitelid USING BTREE (Mobitel_ID)
) ENGINE = INNODB COMMENT '����������� ������';

DROP TABLE IF EXISTS datagpslost_on;
CREATE TABLE IF NOT EXISTS datagpslost_on (
  Mobitel_ID INT(11) NOT NULL DEFAULT 0,
  Begin_LogID INT(11) NOT NULL DEFAULT 0 COMMENT '�������� LogID ����� �������� ���������� ������',
  End_LogID INT(11) NOT NULL DEFAULT 0 COMMENT '�������� LogID ����� ������� ����������� ������',
  Begin_SrvPacketID BIGINT NOT NULL COMMENT '������������ ���������� ������ � ������� ������ ������ � Begin_LogID',
  End_SrvPacketID BIGINT NOT NULL COMMENT '������������ ���������� ������ � ������� ������ ������ � End_LogID',
  PRIMARY KEY PK_DatagpslostOn(Mobitel_ID, Begin_LogID),
  INDEX IDX_MobitelID (Mobitel_ID),
  UNIQUE INDEX IDX_MobitelID_BeginSrvPacketID (Mobitel_ID, Begin_SrvPacketID)
) ENGINE=InnoDB COMMENT='�������� � ������ DataGPS, ���������� �� Online �������';

DROP TABLE IF EXISTS datagpslost_ontmp;
CREATE TABLE IF NOT EXISTS datagpslost_ontmp (
  LogID INT(11) NOT NULL DEFAULT 0,
  SrvPacketID BIGINT NOT NULL
) ENGINE = MEMORY COMMENT = '������������ ������������� ���������� OnLostDataGPS';


DROP PROCEDURE IF EXISTS tmpAddColumns;
CREATE PROCEDURE tmpAddColumns()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '��������� ��������� ����������� ��������'
BEGIN
  DECLARE DbName VARCHAR(64); /* ������������ ������� �� */
  DECLARE ColumnExist TINYINT DEFAULT 0;

  SELECT DATABASE() INTO DbName;

  SELECT COUNT(1) INTO ColumnExist
  FROM information_schema.columns
  WHERE (TABLE_SCHEMA = DbName) AND
    (TABLE_NAME = "mobitels") AND (COLUMN_NAME = "ConfirmedID");

  IF ColumnExist = 0 THEN
    ALTER TABLE mobitels ADD COLUMN ConfirmedID int(11) NOT NULL DEFAULT 0
      COMMENT 'MIN �������� LogID ����� �������� ��������� ��� ��������� ��� ��� ���������� ���������';
  END IF;

  SELECT COUNT(1) INTO ColumnExist
  FROM information_schema.columns
  WHERE (TABLE_SCHEMA = DbName) AND
    (TABLE_NAME = "mobitels") AND (COLUMN_NAME = "LastInsertTime");

  IF ColumnExist = 0 THEN
    ALTER TABLE mobitels ADD COLUMN LastInsertTime INT(11) NOT NULL DEFAULT 0
      COMMENT 'UNIX_TIMESTAMP ����� ��������� ������� ������ � DataGPS';
  END IF;
END;

CALL tmpAddColumns;
DROP PROCEDURE IF EXISTS tmpAddColumns;


DROP PROCEDURE IF EXISTS tmpAlterDmTables;
CREATE PROCEDURE tmpAlterDmTables()
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT '��������� ��������� ����������� ������'
BEGIN
  IF EXISTS (
    SELECT NULL 
    FROM information_schema.TABLE_CONSTRAINTS
    WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND (TABLE_NAME = "datagpslost_on") 
      AND (CONSTRAINT_NAME = "IDX_MobitelID_BeginLogID"))
  THEN
    ALTER TABLE datagpslost_on DROP INDEX IDX_MobitelID_BeginLogID;
  END IF;

	IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "datagps64")
	THEN
	ALTER TABLE datagps64 MODIFY DGPS varchar(32);
	END IF;

	IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "online64")
	THEN
	ALTER TABLE online64 MODIFY DGPS varchar(32);
	END IF;
END;

CALL tmpAlterDmTables;
DROP PROCEDURE IF EXISTS tmpAlterDmTables;