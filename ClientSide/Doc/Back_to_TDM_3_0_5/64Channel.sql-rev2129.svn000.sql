DROP TABLE IF EXISTS datagpsbuffer_on64;
CREATE TABLE IF NOT EXISTS datagpsbuffer_on64 (
  Mobitel_ID int(11) NOT NULL DEFAULT 0 COMMENT '��� ���������',
  Latitude int(11) NOT NULL DEFAULT 0 COMMENT '������',
  Longitude int(11) NOT NULL DEFAULT 0 COMMENT '�������',
  Direction tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������/1.5',
  Acceleration int(11) NOT NULL DEFAULT 0 COMMENT '��������� �������� �� GPS �� ��������� �������',
  UnixTime int(11) NOT NULL DEFAULT 0,
  Speed smallint(6) NOT NULL DEFAULT 0 COMMENT '����� ������ � ��� (0,1 ��/�)',
  Valid tinyint(1) NOT NULL DEFAULT 1,
  Satellites tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '���������� ��������� ',
  RssiGsm tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '������� GSM �������',
  Events int(11) NOT NULL DEFAULT 0 COMMENT '����� �������',
  SensorsSet tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '��� ������ ��������',
  Sensors varbinary(50) NOT NULL DEFAULT '0' COMMENT '�������� ��������',
  Voltage tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '���������� ������� �������� ���������/1.5 �� 0 �� 38,2',
  DGPS varchar(32) NOT NULL DEFAULT '0' COMMENT '������ ���������������� ������� GPS - ������,������,�������',
  LogID int(11) NOT NULL DEFAULT 0 COMMENT '������������� ������ � ������� ���������',
  SrvPacketID bigint(20) NOT NULL DEFAULT 0 COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������',
  INDEX IDX_MobitelidLogid_64 USING BTREE (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime_64 USING BTREE (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid_64 USING BTREE (Mobitel_ID, UnixTime, Valid),
  INDEX IDX_SrvPacketID_64 USING BTREE (SrvPacketID),
  CONSTRAINT FK_datagps64_mobitels_Mobitel_ID_1 FOREIGN KEY (Mobitel_ID)
  REFERENCES mobitels (Mobitel_ID) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

DROP TABLE IF EXISTS datagpsbuffer_ontmp64;
CREATE TABLE IF NOT EXISTS datagpsbuffer_ontmp64 (
  Mobitel_ID int(11) NOT NULL DEFAULT 0 COMMENT '��� ���������',
  LogID int(11) UNSIGNED DEFAULT 0 COMMENT '������������� ������ � ������� ���������',
  SrvPacketID bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������',
  INDEX IDX_Mobitelid64 (Mobitel_ID),
  INDEX IDX_MobitelidSrvpacketid64 (Mobitel_ID, SrvPacketID),
  CONSTRAINT FK_datagps64_mobitels_Mobitel_ID_2 FOREIGN KEY (Mobitel_ID)
  REFERENCES mobitels (Mobitel_ID) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

DROP TABLE IF EXISTS online64;
CREATE TABLE IF NOT EXISTS online64 (
  DataGps_ID int(11) NOT NULL AUTO_INCREMENT,
  Mobitel_ID int(11) NOT NULL DEFAULT 0 COMMENT '��� ���������',
  Latitude int(11) NOT NULL DEFAULT 0 COMMENT '������',
  Longitude int(11) NOT NULL DEFAULT 0 COMMENT '�������',
  Direction tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '�������/1.5',
  Acceleration int(11) DEFAULT 0 COMMENT '��������� �������� �� GPS �� ��������� �������',
  UnixTime int(11) NOT NULL DEFAULT 0,
  Speed smallint(6) NOT NULL DEFAULT 0 COMMENT '����� ������ � ��� (0,1 ��/�)',
  Valid tinyint(1) NOT NULL DEFAULT 1,
  Satellites tinyint(1) UNSIGNED DEFAULT 0 COMMENT '���������� ��������� ',
  RssiGsm tinyint(1) UNSIGNED DEFAULT 0 COMMENT '������� GSM �������',
  Events int(11) DEFAULT 0 COMMENT '����� �������',
  SensorsSet tinyint(1) DEFAULT 0 COMMENT '��� ������ ��������',
  Sensors varbinary(50) DEFAULT '0' COMMENT '�������� ��������',
  Voltage tinyint(4) UNSIGNED DEFAULT 0 COMMENT '���������� ������� �������� ���������/1.5 �� 0 �� 38,2',
  DGPS varbinary(32) DEFAULT '0' COMMENT '������ ���������������� ������� GPS - ������,������,�������',
  LogID int(11) DEFAULT 0 COMMENT '������������� ������ � ������� ���������',
  SrvPacketID bigint(20) NOT NULL DEFAULT 0 COMMENT 'ID ���������� ������ � ������ �������� ������ ��� ������',
  PRIMARY KEY (DataGps_ID),
  INDEX IDX_Mobitelid64 (Mobitel_ID),
  INDEX IDX_MobitelidLogid64 (Mobitel_ID, LogID),
  INDEX IDX_MobitelidUnixtime64 (Mobitel_ID, UnixTime),
  INDEX IDX_MobitelidUnixtimeValid64 (Mobitel_ID, UnixTime, Valid)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;


DROP PROCEDURE IF EXISTS OnCorrectInfotrackLogId64;
CREATE PROCEDURE OnCorrectInfotrackLogId64 ()
SQL SECURITY INVOKER
COMMENT '������������� �������� LogId � InMobitelID � ������ ����������'
BEGIN
  DECLARE MobitelIDInCursor int; /* ������� �������� MobitelID */
  DECLARE DataExists tinyint DEFAULT 1; /* ���� ������� ������ ����� ����� */
  DECLARE MaxLogId int DEFAULT 0; /* ������������ LogId ��� ������� ������� */
  DECLARE SrvPacketIDInCursor bigint DEFAULT 0; /* ������� �������� ���������� ������ */

  /* ������ �� ���������� � ��������� ����� ������. 
     ��� ��������� ���������� � ����� I */
  DECLARE CursorInfoTrack CURSOR FOR
  SELECT DISTINCT
    buf.Mobitel_ID
  FROM datagpsbuffer_on64 buf
    JOIN mobitels m
      ON (buf.mobitel_id = m.mobitel_id)
    JOIN internalmobitelconfig c
      ON (c.ID = m.InternalMobitelConfig_ID)
  WHERE (c.InternalMobitelConfig_ID = (SELECT
      intConf.InternalMobitelConfig_ID
    FROM internalmobitelconfig intConf
    WHERE (intConf.ID = c.ID)
    ORDER BY intConf.InternalMobitelConfig_ID DESC
    LIMIT 1)) AND
  (c.devIdShort LIKE "I%") AND (buf.LogId = 0)
  ORDER BY 1;

  /* ������ �� ������ ��������� */
  DECLARE CursorInfoTrackUpdateSet CURSOR FOR
  SELECT
    SrvPacketID
  FROM datagpsbuffer_on64
  WHERE (Mobitel_ID = MobitelIDInCursor) AND (LogId = 0)
  ORDER BY UnixTime;

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  OPEN CursorInfoTrack;
  FETCH CursorInfoTrack INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ������� ������������ LogId ��� ������� ��������� */
    SELECT
      COALESCE(MAX(LogID), 0) INTO MaxLogId
    FROM DataGps
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ��������� LogId � InMobitelID � ������ ����� ������ �������� ��������� */
    OPEN CursorInfoTrackUpdateSet;
    FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    WHILE DataExists = 1 DO
      SET MaxLogId = MaxLogId + 1;

      UPDATE datagpsbuffer_on64
      SET LogId = MaxLogId,
          InMobitelID = MaxLogId
      WHERE SrvPacketID = SrvPacketIDInCursor;

      FETCH CursorInfoTrackUpdateSet INTO SrvPacketIDInCursor;
    END WHILE;
    CLOSE CursorInfoTrackUpdateSet;

    SET DataExists = 1;
    FETCH CursorInfoTrack INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorInfoTrack;
END;

DROP PROCEDURE IF EXISTS OnDeleteDuplicates64;
CREATE PROCEDURE OnDeleteDuplicates64 ()
SQL SECURITY INVOKER
COMMENT '�������� ������������� ����� �� datagpsbuffer_on64'
BEGIN
  /* ������� ��������� ����� �� ������� datagpsbuffer_on64. ��������� ������
   �������� ���������� �������� � ����� Mobitel_Id + LogID. �� ���������
   ������������� ������� � ������� datagpsbuffer_on64 ��������� ������ ���� - 
   ��������� �������� �������������� ��������, �.�. � ������������ SrvPacketID. */

  /* ������� MobitelId � ������� CursorDuplicateGroups */
  DECLARE MobitelIdInCursor int DEFAULT 0;
  /* ������� LogID � ������� CursorDuplicateGroups */
  DECLARE LogIdInCursor int DEFAULT 0;
  /* Max SrvPacketId � ��������� ����� ������� ������ ���������� */
  DECLARE MaxSrvPacketId bigint DEFAULT 0;
  DECLARE DataExists tinyint DEFAULT 1;

  /* ������ ��� ������� �� ���� ������� ���������� */
  DECLARE CursorDuplicateGroups CURSOR FOR
  SELECT
    Mobitel_Id,
    LogId
  FROM datagpsbuffer_on64
  GROUP BY Mobitel_Id,
           LogId
  HAVING COUNT(LogId) > 1;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  SET DataExists = 1;
  OPEN CursorDuplicateGroups;
  FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  WHILE DataExists = 1 DO
    SELECT
      MAX(SrvPacketID) INTO MaxSrvPacketId
    FROM datagpsbuffer_on64
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor);

    DELETE
      FROM datagpsbuffer_on64
    WHERE (Mobitel_ID = MobitelIdInCursor) AND (LogId = LogIdInCursor)
      AND (SrvPacketID < MaxSrvPacketId);

    FETCH CursorDuplicateGroups INTO MobitelIdInCursor, LogIdInCursor;
  END WHILE;
  CLOSE CursorDuplicateGroups;
END;

DROP PROCEDURE IF EXISTS OnLostDataGPS264;
CREATE PROCEDURE OnLostDataGPS264 (IN MobitelID integer(11), OUT NeedOptimize tinyint)
SQL SECURITY INVOKER
COMMENT '����������� ���������� ���������. ������ ����������� ������ ��������� ���������.'
BEGIN
  /********** DECLARE **********/

  DECLARE NewConfirmedID int DEFAULT 0; /* ����� �������� ConfirmedID */
  DECLARE BeginLogID int; /* Begin_LogID � ������� CursorLostRanges */
  DECLARE EndLogID int; /* End_LogID � ������� CursorLostRanges */
  DECLARE BeginSrvPacketID bigint; /* Begin_SrvPacketID � ������� CursorLostRanges */
  DECLARE EndSrvPacketID bigint; /* End_SrvPacketID � ������� CursorLostRanges */
  DECLARE RowCountForAnalyze int DEFAULT 0; /* ���������� ������� ��� ������� */
  DECLARE DataExists tinyint DEFAULT 1; /* ���� ������� ������ ����� ����� */

  /* ������ ��� ������� �� ���������� ��������� ��������� */
  DECLARE CursorLostRanges CURSOR FOR
  SELECT
    Begin_LogID,
    End_LogID,
    Begin_SrvPacketID,
    End_SrvPacketID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;

  /* ������ ��� ������� �� ����� LogID */
  DECLARE CursorNewLogID CURSOR FOR
  SELECT
    LogID,
    SrvPacketID
  FROM datagpslost_ontmp
  ORDER BY SrvPacketID;

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET NeedOptimize = 0;

  TRUNCATE TABLE datagpslost_ontmp;

  /* ���������� ��������� � ��������� ������� ���� ��� �������� */
  SET DataExists = 1;
  OPEN CursorLostRanges;
  FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  WHILE DataExists = 1 DO
    /* ���������� ��������� ������� ������������ ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
      SELECT DISTINCT
        LogID,
        SrvPacketID
      FROM datagpsbuffer_ontmp64
      WHERE (Mobitel_ID = MobitelID) AND (SrvPacketID > BeginSrvPacketID)
      AND (SrvPacketID < EndSrvPacketID)
      ORDER BY SrvPacketID;

    /* ���� ���� ������ ������������ �������� */
    SELECT
      COUNT(1) INTO RowCountForAnalyze
    FROM datagpslost_ontmp;

    IF RowCountForAnalyze > 0 THEN
      /* ��������� ������ � ��������� ������ ��� ������� */
      INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
        VALUES (BeginLogID, BeginSrvPacketID), (EndLogID, EndSrvPacketID);

      /* ������� ������ �������� */
      DELETE
        FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID = BeginLogID);

      /**********  INSERT ROWS **********/
      BEGIN
        DECLARE NewLogID int; /* �������� LogID � ������� CursorNewLogID */
        DECLARE NewSrvPacketID bigint; /* �������� SrvPacketID � ������� CursorNewLogID */
        DECLARE FirstLogID int DEFAULT -9999999; /* ������ �������� LogID ��� ������� */
        DECLARE SecondLogID int; /* ������ �������� LogID ��� ������� */
        DECLARE FirstSrvPacketID bigint; /* ������ �������� SrvPacketID */
        DECLARE SecondSrvPacketID bigint; /* ������ �������� SrvPacketID */

        /* ���������� ������������� ������ ��� FETCH ��������� ������ */
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

        /* ������������ �������-������� � ������� ����������� ���������� */
        SET DataExists = 1;
        OPEN CursorNewLogID;
        FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        WHILE DataExists = 1 DO
          IF FirstLogID = -9999999 THEN
            /* ������ �������� � ������ */
            SET FirstLogID = NewLogID;
            SET FirstSrvPacketID = NewSrvPacketID;
          ELSE
            /* �������� ����� �������*/
            SET SecondLogID = NewLogID;
            SET SecondSrvPacketID = NewSrvPacketID;

            IF (SecondSrvPacketID - FirstSrvPacketID) > 1 THEN
              /* ������ ������ */
              CALL OnInsertDatagpsLost(MobitelID, FirstLogID, SecondLogID,
              FirstSrvPacketID, SecondSrvPacketID);

              IF NeedOptimize = 0 THEN
                SET NeedOptimize = 1;
              END IF;
            END IF;
            /* ���������� ������� ������ �������� �� ������ �������
               ��� ������� � ��������� �������� � �������� ���������� �������� */
            SET FirstLogID = SecondLogID;
            SET FirstSrvPacketID = SecondSrvPacketID;
          END IF;
          FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
        END WHILE;
        CLOSE CursorNewLogID;
      END;
      /********** END INSERT ROWS **********/

      TRUNCATE TABLE datagpslost_ontmp;
    END IF;

    FETCH CursorLostRanges INTO BeginLogID, EndLogID, BeginSrvPacketID, EndSrvPacketID;
  END WHILE;
  CLOSE CursorLostRanges;

  /********** UPDATE MOBITELS **********/

  /* ���� ����������� ������� � ������� ��������� ��� - 
     ����� ConfirmedID ����� ����� ������������� LogID 
     ������� ���������. ����� - ����������� Begin_LogID
     �� ������� ��������� */
  SELECT
    MIN(Begin_LogID) INTO NewConfirmedID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;

  IF NewConfirmedID IS NULL THEN
    SELECT
      MAX(LogID) INTO NewConfirmedID
    FROM datagps64
    WHERE Mobitel_ID = MobitelID;
  END IF;

  /* ������� ConfirmedID ���� ���� */
  UPDATE mobitels
  SET ConfirmedID = NewConfirmedID
  WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);

/********** END UPDATE MOBITELS **********/
END;

DROP PROCEDURE IF EXISTS OnLostDataGPS64;
CREATE PROCEDURE OnLostDataGPS64 (IN MobitelID integer(11), OUT NeedOptimize tinyint)
SQL SECURITY INVOKER
COMMENT '����� ��������� � online ������ ��������� ��������� � ���������� ������.'
BEGIN

  /********** DECLARE **********/

  DECLARE CurrentConfirmedID int DEFAULT 0; /* ������� �������� ConfirmedID */
  DECLARE NewConfirmedID int DEFAULT 0; /* ����� �������� ConfirmedID */
  DECLARE StartLogIDSelect int DEFAULT 0; /* min LogID ��� ������� */
  DECLARE FinishLogIDSelect int DEFAULT 0; /* max LogID ��� ������� */
  DECLARE MaxEndLogID int DEFAULT 0; /* max End_LogID � ����������� ���������� ������� ��������� */

  DECLARE MinLogID_n int DEFAULT 0; /* ����������� LogID ����� ConfirmedID � ��������� ����� ������ */
  DECLARE MaxLogID_n int DEFAULT 0; /* ������������ LogID � ��������� ����� ������ */

  DECLARE DataExists tinyint DEFAULT 1; /* ���� ������� ������ ����� ����� */

  DECLARE NewLogID int; /* �������� LogID � ������� CursorNewLogID */
  DECLARE NewSrvPacketID bigint; /* �������� SrvPacketID � ������� CursorNewLogID */
  DECLARE FirstLogID int DEFAULT -9999999; /* ������ �������� LogID ��� ������� */
  DECLARE SecondLogID int; /* ������ �������� LogID ��� ������� */
  DECLARE FirstSrvPacketID bigint; /* ������ �������� SrvPacketID */
  DECLARE SecondSrvPacketID bigint; /* ������ �������� SrvPacketID */

  DECLARE tmpSrvPacketID bigint DEFAULT 0; /* �������� SrvPacketID ��� ������ ������������� ������ � ������� ����� ������, �� �������������� � ������������� */

  /* ������ ��� ������� �� ����� LogID */
  DECLARE CursorNewLogID CURSOR FOR
  SELECT
    LogID,
    SrvPacketID
  FROM datagpslost_ontmp;

  /* ���������� ������������� ������ ��� FETCH ��������� ������ */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;

  /********** END DECLARE **********/

  SET NeedOptimize = 0;

  SELECT
    COALESCE(ConfirmedID, 0) INTO CurrentConfirmedID
  FROM mobitels
  WHERE Mobitel_ID = MobitelID;

  /* ����������� LogID � ����� ������ ������ c ������� ����� �������� */
  SELECT
    MIN(LogID) INTO MinLogID_n
  FROM datagpsbuffer_on64
  WHERE (Mobitel_ID = MobitelID) AND (LogID > CurrentConfirmedID);

  /* ������������ LogID � ����� ������ ������ c ������� ����� �������� */
  SELECT
    MAX(LogID) INTO MaxLogID_n
  FROM datagpsbuffer_on64
  WHERE Mobitel_ID = MobitelID;

  /* ������������ End_LogID � ����������� ���������� ������� ��������� */
  SELECT
    COALESCE(MAX(End_LogID), 0) INTO MaxEndLogID
  FROM datagpslost_on
  WHERE Mobitel_ID = MobitelID;

  /* �������� ���� �� ������ ��� ������� */
  IF MinLogID_n IS NOT NULL THEN

    /********** PREPARE **********/

    TRUNCATE TABLE datagpslost_ontmp;

    IF MinLogID_n < MaxEndLogID THEN
      /* ����  MinLogID_n < MaxEndLogID ������ ������ ������, 
       ������� �������� ������� ����� ���������. */

      /* ������� ��������� LogID ��� ���������. */
      SELECT
        COALESCE(Begin_LogID, MinLogID_n) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MinLogID_n > Begin_LogID)
      AND (MinLogID_n < End_LogID);

      /* ������� ��������� LogID ��� ���������. */
      SELECT
        COALESCE(End_LogID, MaxLogID_n) INTO FinishLogIDSelect
      FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (MaxLogID_n > Begin_LogID)
      AND (MaxLogID_n < End_LogID);

      /* ������ ������ ������ �� ������� ����������� ������� 
         (����� �������������, ���� �����������) */
      DELETE
        FROM datagpslost_on
      WHERE (Mobitel_ID = MobitelID) AND (Begin_LogID >= StartLogIDSelect)
        AND (Begin_LogID <= FinishLogIDSelect);

      /* ���� ������ ������ LogID = 0 � ���� ������ ��� � DataGps, �����
         ���� �������� ������ ��������� ������ ��� �������. */
      IF (StartLogIDSelect = 0) AND (NOT EXISTS (SELECT
            1
          FROM datagps64
          WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect))) THEN

        INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
          VALUES (0, 0);
      END IF;
    ELSE
      /* ������ ����� ������. ���� �������� ��� ������� ������������� ������ - 
         ������ ������� */

      /* ������� ��������� LogID ��� ���������. */
      SELECT
        COALESCE(MAX(End_LogID), CurrentConfirmedID) INTO StartLogIDSelect
      FROM datagpslost_on
      WHERE Mobitel_ID = MobitelID;

      /* ������� ��������� LogID ��� ���������. */
      SELECT
        MAX(LogID) INTO FinishLogIDSelect
      FROM datagpsbuffer_on64
      WHERE Mobitel_ID = MobitelID;

      /* �������� �� datagps ��������������� �������� SrvPacketID  ��� ������
         ������������� ������ */
      SELECT
        COALESCE(SrvPacketID, 0) INTO tmpSrvPacketID
      FROM datagps64
      WHERE (Mobitel_ID = MobitelID) AND (LogID = StartLogIDSelect);

      /* �������� � ������� datagpslost_ontmp64 ��� ������� ��������� 
         �������������� ������ ������ */
      INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
        VALUES (StartLogIDSelect, tmpSrvPacketID);
    END IF; -- IF MinLogID_n < MaxEndLogID   

    /********** END PREPARE **********/

    /**********  INSERT ROWS **********/

    /* ���������� ��������� ������� ���������� ������� ���������
       ��� ������� �������� */
    INSERT INTO datagpslost_ontmp (LogID, SrvPacketID)
      SELECT
        LogID,
        SrvPacketID
      FROM datagps64
      WHERE (Mobitel_ID = MobitelID) AND (LogID >= StartLogIDSelect)
      AND (LogID <= FinishLogIDSelect)
      ORDER BY LogID;

    /* ������������ �������-������� � ������� ����������� ���������� */
    SET DataExists = 1;
    OPEN CursorNewLogID;
    FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    WHILE DataExists = 1 DO
      IF FirstLogID = -9999999 THEN
        /* ������ �������� � ������ */
        SET FirstLogID = NewLogID;
        SET FirstSrvPacketID = NewSrvPacketID;
      ELSE
        /* �������� ����� �������*/
        SET SecondLogID = NewLogID;
        SET SecondSrvPacketID = NewSrvPacketID;

        IF (SecondLogID - FirstLogID) > 1 THEN
          /* ������ ������ */
          CALL OnInsertDatagpsLost(MobitelID, FirstLogID, SecondLogID,
          FirstSrvPacketID, SecondSrvPacketID);

          IF NeedOptimize = 0 THEN
            SET NeedOptimize = 1;
          END IF;
        END IF;
        /* ���������� ������� ������ �������� �� ������ �������
           ��� ������� � ��������� �������� � �������� ���������� �������� */
        SET FirstLogID = SecondLogID;
        SET FirstSrvPacketID = SecondSrvPacketID;
      END IF;
      FETCH CursorNewLogID INTO NewLogID, NewSrvPacketID;
    END WHILE;
    CLOSE CursorNewLogID;

    /********** END INSERT ROWS **********/

    /********** UPDATE MOBITELS **********/

    /* ���� ����������� ������� � ������� ��������� ��� - 
       ����� ConfirmedID ����� ����� ������������� LogID 
       ������� ���������. ����� - ����������� Begin_LogID
       �� ������� ��������� */
    SELECT
      MIN(Begin_LogID) INTO NewConfirmedID
    FROM datagpslost_on
    WHERE Mobitel_ID = MobitelID;

    IF NewConfirmedID IS NULL THEN
      SELECT
        MAX(LogID) INTO NewConfirmedID
      FROM datagps64
      WHERE Mobitel_ID = MobitelID;
    END IF;

    /* ������� ConfirmedID ���� ���� */
    UPDATE mobitels
    SET ConfirmedID = NewConfirmedID
    WHERE (Mobitel_ID = MobitelID) AND (ConfirmedID < NewConfirmedID);

    /********** END UPDATE MOBITELS **********/

    TRUNCATE TABLE datagpslost_ontmp;
  END IF; -- IF MaxEndLogID IS NOT NULL  
END;

DROP PROCEDURE IF EXISTS OnTransferBuffer64;
CREATE PROCEDURE OnTransferBuffer64()
  SQL SECURITY INVOKER
  COMMENT ''
BEGIN
  DECLARE MobitelIDInCursor int; /* ??????obitelID */
  DECLARE DataExists tinyint DEFAULT 1; /* */
  DECLARE NeedOptimize tinyint DEFAULT 0; /* ?? ????? ? */
  DECLARE NeedOptimizeParam tinyint;

  DECLARE TmpMaxUnixTime int; /* �??? ???? ? ??? Max UnixTime ??? online */

  /* */
  DECLARE CursorMobitelsInsertSet CURSOR FOR
  SELECT DISTINCT
    (Mobitel_ID)
  FROM datagpsbuffer_on64;
  /* */
  DECLARE CursorMobitelsUpdateSet CURSOR FOR
  SELECT DISTINCT
    (Mobitel_ID)
  FROM datagpsbuffer_ontmp64;

  /* */
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET DataExists = 0;
  /*DECLARE EXIT HANDLER FOR SQLEXCEPTION 
  BEGIN
    ROLLBACK;
    SELECT 'An error has occurred, operation rollbacked and the stored procedure was terminated';
  end;*/

  CALL delDupSrvPacket();
  CALL OnDeleteDuplicates64();
  /*CALL OnCorrectInfotrackLogId64();*/

  /* ??????, ?????? ?I ? ?? ?? ????? 
     ????????OnLostDataGPS264. */
  /*INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID)
    SELECT
      b.Mobitel_ID,
      b.LogID,
      b.SrvPacketID
    FROM datagpsbuffer_on64 b
      LEFT JOIN datagps64 d
        ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL GROUP BY d.SrvPacketID;*/
 DROP TEMPORARY TABLE IF EXISTS temp1;
  DROP TABLE IF EXISTS temp1;
  CREATE TEMPORARY TABLE temp1  SELECT
      b.Mobitel_ID,
      b.LogID,
      b.SrvPacketID
    FROM datagpsbuffer_on64 b
      LEFT JOIN datagps64 d
        ON (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
    WHERE d.LogID IS NOT NULL GROUP BY d.SrvPacketID;

INSERT INTO datagpsbuffer_ontmp64 (Mobitel_ID, LogID, SrvPacketID) SELECT * FROM temp1 GROUP BY SrvPacketID;

DROP TEMPORARY TABLE IF EXISTS temp1;
DROP TABLE IF EXISTS temp1;
  /* -------------------- ?????? Online ---------------------- */
  /* ??? ??? online ??????? datagpsbuffer_on64 */
  /* ????obitel_ID ?ogID */
  DELETE o
    FROM online64 o, datagpsbuffer_on64 dgb
  WHERE (o.Mobitel_ID = dgb.Mobitel_ID) AND (o.LogID = dgb.LogID);

  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* xxxx */
    SELECT
      COALESCE(MAX(UnixTime), 0) INTO TmpMaxUnixTime
    FROM online64 FORCE INDEX (IDX_Mobitelid64)
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* �??????(???00)?????? online */
    INSERT INTO online64 (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
    Direction, Speed, Valid, `Events`,
    Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS)
      SELECT
        Mobitel_ID,
        LogID,
        UnixTime,
        Latitude,
        Longitude,
        Acceleration,
        Direction,
        Speed,
        Valid,
        `Events`,
        Satellites,
        RssiGsm,
        SensorsSet,
        Sensors,
        Voltage,
        DGPS
      FROM (SELECT
          *
        FROM datagpsbuffer_on64
        WHERE (Mobitel_ID = MobitelIDInCursor) AND
        (UnixTime > TmpMaxUnixTime) AND (Valid = 1)
        GROUP BY Mobitel_ID,
                 LogID
        ORDER BY UnixTime DESC
        LIMIT 100) AS T
      ORDER BY UnixTime ASC;

    /* */
    DELETE
      FROM online64
    WHERE (Mobitel_ID = MobitelIDInCursor) AND (UnixTime < (SELECT
          COALESCE(MIN(UnixTime), 0)
        FROM (SELECT
            UnixTime
          FROM online64
          WHERE Mobitel_ID = MobitelIDInCursor
          ORDER BY UnixTime DESC
          LIMIT 100) T1));

    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;
  /* ---------------- Online ---------------- */
  /* ???? ?? ... */
  UPDATE datagps64 d, datagpsbuffer_on64 b
  SET d.Latitude = b.Latitude,
      d.Longitude = b.Longitude,
      d.Acceleration = b.Acceleration,
      d.UnixTime = b.UnixTime,
      d.Speed = b.Speed,
      d.Direction = b.Direction,
      d.Valid = b.Valid,
      d.LogID = b.LogID,
      d.`Events` = b.`Events`,
      d.Satellites = b.Satellites,
      d.RssiGsm = b.RssiGsm,
      d.SensorsSet = b.SensorsSet,
      d.Sensors = b.Sensors,
      d.Voltage = b.Voltage,
      d.DGPS = b.DGPS,
      d.SrvPacketID = b.SrvPacketID
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID)
  AND (d.SrvPacketID < b.SrvPacketID);

  /* ?????????? ??? */
  DELETE b
    FROM datagps64 d, datagpsbuffer_on64 b
  WHERE (d.Mobitel_ID = b.Mobitel_ID) AND (d.LogID = b.LogID);

    /* �??????*/ 
  INSERT INTO datagps64 (Mobitel_ID, LogID, UnixTime, Latitude, Longitude, Acceleration,
  Direction, Speed, Valid, `Events`,
  Satellites, RssiGsm, SensorsSet, Sensors, Voltage, DGPS, SrvPacketID)
    SELECT
      Mobitel_ID,
      LogID,
      UnixTime,
      Latitude,
      Longitude,
      Acceleration,
      Direction,
      Speed,
      Valid,
      `Events`,
      Satellites,
      RssiGsm,
      SensorsSet,
      Sensors,
      Voltage,
      DGPS,
      SrvPacketID
    FROM datagpsbuffer_on64
    GROUP BY Mobitel_ID,
             LogID;

  /* ------------------- FETCH Mobitels (INSERT SET) ----------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsInsertSet;
  FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ------ ???????????????------- */
    UPDATE mobitels
    SET LastInsertTime = UNIX_TIMESTAMP(NOW())
    WHERE Mobitel_ID = MobitelIDInCursor;

    /* ------ ?????????????-------------- */
    CALL OnLostDataGPS64(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsInsertSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsInsertSet;

  /* ????? */
  TRUNCATE TABLE datagpsbuffer_on64;

  /* ------------------- FETCH Mobitels (UPDATE SET) ---------------------- */
  SET DataExists = 1;
  OPEN CursorMobitelsUpdateSet;
  FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  WHILE DataExists = 1 DO
    /* ---- ???????????????? ???--- */
    CALL OnLostDataGPS264(MobitelIDInCursor, NeedOptimizeParam);
    SET NeedOptimize = GREATEST(NeedOptimize, NeedOptimizeParam);
    FETCH CursorMobitelsUpdateSet INTO MobitelIDInCursor;
  END WHILE;
  CLOSE CursorMobitelsUpdateSet;

  /* ???????????/
  TRUNCATE TABLE datagpsbuffer_ontmp64;

  /* {???? - ?? ????*/
  IF NeedOptimize = 1 THEN
    OPTIMIZE TABLE datagpslost_on;
  END IF;
END;

DROP INDEX IDX_MobitelidSrvpacketid64 ON datagpsbuffer_ontmp64;
ALTER TABLE datagpsbuffer_ontmp64 ADD INDEX IDX_MobitelidSrvpacketid64(Mobitel_ID, SrvPacketID);

