DROP PROCEDURE IF EXISTS rcs_Vehicles_Team_Driver_Update;
CREATE PROCEDURE rcs_Vehicles_Team_Driver_Update()
BEGIN
       
    IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "OutLinkId")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `OutLinkId` VARCHAR(20)  NULL COMMENT '��� ������� ����';
	END IF;
	
    IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "team") AND
        (COLUMN_NAME = "OutLinkId")
	)
	THEN
		ALTER TABLE `team` ADD COLUMN `OutLinkId` VARCHAR(20)  NULL COMMENT '��� ������� ����';
		ALTER TABLE team MODIFY `Name` VARCHAR(50);
	END IF;	
	
    IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "OutLinkId")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `OutLinkId` VARCHAR(20)  NULL COMMENT '��� ������� ����';
	END IF;	
	
	CREATE TABLE IF NOT EXISTS driver_types(
	Id INT(11) NOT NULL AUTO_INCREMENT,
	TypeName VARCHAR(50) DEFAULT NULL COMMENT '������������� ��������',
	PRIMARY KEY (Id),
	UNIQUE INDEX id (Id)
	)
	ENGINE = INNODB
	CHARACTER SET cp1251
	COLLATE cp1251_general_ci;
		
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "idType")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `idType` int  NULL default 0 COMMENT '��� ������������� ��������';
	END IF;	

	CREATE TABLE IF NOT EXISTS vehicle_category(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(50) NOT NULL COMMENT '��������� ������������� ��������',
  Tonnage DECIMAL(10, 3) NOT NULL DEFAULT 0.000 COMMENT '����������������,  ����',
  FuelNormMoving DOUBLE NOT NULL DEFAULT 0 COMMENT '����� ������� ������� � �������� �/100 ��',
  FuelNormParking DOUBLE NOT NULL DEFAULT 0 COMMENT '����� ������� ������� �� �������� �������� �/���',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id` int  NULL  COMMENT '��������� �����������';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id` FOREIGN KEY (Category_id)
		REFERENCES vehicle_category (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;	
	
CREATE TABLE IF NOT EXISTS vehicle_category2(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(255) NOT NULL COMMENT '���������2 ������������� ��������',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;

	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id2")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id2` int  NULL  COMMENT '���������2 ����������';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id2` FOREIGN KEY (Category_id2)
		REFERENCES vehicle_category2 (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "PcbVersionId")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `PcbVersionId` int(11) DEFAULT - 1 COMMENT '������ ���� ������� ����������� �� ������';
	END IF;
	CREATE TABLE IF NOT EXISTS vehicle_category3(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(255) NOT NULL COMMENT '���������3 ������������� ��������',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id3")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id3` int  NULL  COMMENT '���������3 ����������';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id3` FOREIGN KEY (Category_id3)
		REFERENCES vehicle_category3 (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;
	    CREATE TABLE IF NOT EXISTS vehicle_category4(
  Id INT(11) NOT NULL AUTO_INCREMENT,
  Name VARCHAR(255) NOT NULL COMMENT '���������4 ������������� ��������',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
CHARACTER SET cp1251
COLLATE cp1251_general_ci;
	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle") AND
        (COLUMN_NAME = "Category_id4")
	)
	THEN
		ALTER TABLE `vehicle` ADD COLUMN `Category_id4` int  NULL  COMMENT '���������4 ����������';
		ALTER TABLE `vehicle`
		ADD CONSTRAINT `FK_vehicle_vehicle_category_Id4` FOREIGN KEY (Category_id4)
		REFERENCES vehicle_category4 (Id) ON DELETE RESTRICT ON UPDATE RESTRICT;
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata")	)
	THEN
CREATE TABLE pcbdata (
  Id int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '������',
  Typedata varchar(255) NOT NULL DEFAULT ' ' COMMENT '��� ������',
  koeffK double NOT NULL DEFAULT 0 COMMENT '����������� �',
  Startbit int(11) NOT NULL DEFAULT 0 COMMENT '��������� ��� ����',
  Lengthbit int(11) NOT NULL DEFAULT 0 COMMENT '����� ����',
  Comment varchar(255) DEFAULT ' ' COMMENT '����������',
  Idpcb int(11) NOT NULL DEFAULT 0 COMMENT '������ ����� �������',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 11
AVG_ROW_LENGTH = 2340
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = '�������� ����� ��������';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbversion")	)
	THEN
CREATE TABLE pcbversion (
  Id int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '������',
  Name varchar(50) NOT NULL DEFAULT '*' COMMENT '��� ����� �������',
  Comment varchar(255) DEFAULT ' ' COMMENT '�����������',
  PRIMARY KEY (Id)
)
ENGINE = INNODB
AUTO_INCREMENT = 18
AVG_ROW_LENGTH = 5461
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = '������������� ������ ��� ������ ���������� ����� ��������';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zone_category")	)
	THEN
CREATE TABLE zone_category (
  Id int(11) NOT NULL AUTO_INCREMENT,
  Name varchar(255) DEFAULT NULL COMMENT '��������� ����',
  PRIMARY KEY (Id))
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = '��������� ��� ���';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zone_category2")	)
	THEN
CREATE TABLE zone_category2 (
  Id int(11) NOT NULL AUTO_INCREMENT,
  Name varchar(255) DEFAULT NULL COMMENT '���������2 ����',
  PRIMARY KEY (Id))
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = '���������2 ��� ���';
END IF;
IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "vehicle_commentary")	)
	THEN
CREATE TABLE vehicle_commentary (
  Id int NOT NULL AUTO_INCREMENT,
  `Comment` varchar(255) DEFAULT NULL,
  Mobitel_id int NOT NULL,
  PRIMARY KEY (Id))
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET cp1251
COLLATE cp1251_general_ci
COMMENT = '����������� ��� ������';
END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "setting") AND
        (COLUMN_NAME = "TimeMinimMovingSize")
	)
	THEN
		ALTER TABLE `setting` ADD COLUMN `TimeMinimMovingSize` time NOT NULL DEFAULT '00:00:00' COMMENT '����������� ����� �������� � ������ �����';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbversion") AND
        (COLUMN_NAME = "MobitelId")
	)
	THEN
		ALTER TABLE `pcbversion` ADD COLUMN `MobitelId` int(11) DEFAULT - 1 COMMENT 'ID ������ �� ������� ��������� ��� ����� ��������';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "mobitels") AND
        (COLUMN_NAME = "Is64bitPackets")
	)
	THEN
		ALTER TABLE `mobitels` ADD COLUMN `Is64bitPackets` tinyint(1) NOT NULL DEFAULT 0 COMMENT '�������� ������ ���� 64 �������� ��������';
	END IF;

	IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "mobitels") AND
        (COLUMN_NAME = "IsNotDrawDgps")
	)
	THEN
		ALTER TABLE `mobitels` ADD COLUMN `IsNotDrawDgps` tinyint(1) NOT NULL DEFAULT 1 COMMENT '���������� ��� ��� ������ DGPS �� �����';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zones") AND
        (COLUMN_NAME = "Category_id")
	)
	THEN
		ALTER TABLE `zones` ADD COLUMN `Category_id` int(11) DEFAULT NULL COMMENT '��������� ��� ����';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "zones") AND
        (COLUMN_NAME = "Category_id2")
	)
	THEN
		ALTER TABLE `zones` ADD COLUMN `Category_id2` int(11) DEFAULT NULL COMMENT '���������2 ��� ����';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "numTelephone")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `numTelephone` varchar(32) DEFAULT '' COMMENT '������� �������� ��� �����';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "driver") AND
        (COLUMN_NAME = "Department")
	)
	THEN
		ALTER TABLE `driver` ADD COLUMN `Department` varchar(40) NULL COMMENT '����������� ��������';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "NameVal")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `NameVal` varchar(12) DEFAULT ' ' COMMENT 'Name meassurement value';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "NumAlg")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `NumAlg` int(11) DEFAULT 0 COMMENT 'Number of algorithm';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "sensors") AND
        (COLUMN_NAME = "B")
	)
	THEN
		ALTER TABLE `sensors` ADD COLUMN `B` double DEFAULT 0 COMMENT 'Shift coefficient';
	END IF;
IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "Shifter")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `Shifter` double DEFAULT 0 COMMENT 'Shift coefficient';
	END IF;


IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "sensors") AND
        (COLUMN_NAME = "S")
	)
	THEN
		ALTER TABLE `sensors` ADD COLUMN `S` tinyint(1) DEFAULT 0 COMMENT 'Sign arifmetic';
	END IF;

IF NOT EXISTS
	(
		SELECT NULL FROM information_schema.columns
			WHERE (TABLE_SCHEMA = (SELECT DATABASE())) AND
				(TABLE_NAME = "pcbdata") AND
        (COLUMN_NAME = "S")
	)
	THEN
		ALTER TABLE `pcbdata` ADD COLUMN `S` tinyint(1) DEFAULT 0 COMMENT 'Sign arifmetic';
	END IF;



IF EXISTS(SELECT NULL FROM information_schema.columns
    WHERE TABLE_SCHEMA = (SELECT DATABASE()) AND TABLE_NAME = "datagpsbuffer_on64")
	THEN
	ALTER TABLE datagpsbuffer_on64 MODIFY DGPS varchar(32);
	END IF;
END;
CALL rcs_Vehicles_Team_Driver_Update();
DROP PROCEDURE IF EXISTS rcs_Vehicles_Team_Driver_Update;
