rem ECHO OFF

SET LicServerDir=%cd%\ClientSide\Licensing\LicServer\Protection\
SET TDataMngrDir=%cd%\ClientSide\TDataMngr\Protection\
SET TrackControlDir=%cd%\ClientSide\TrackControl\Protection\

SET x86Protector=Protect_x86.cmd
SET x64Protector=Protect_x64.cmd

IF EXIST "%programfiles(x86)%" (
  cd %LicServerDir%
  call %x64Protector%

  echo *****************************
  cd %TDataMngrDir%
  call %x64Protector%

  echo *****************************
  cd %TrackControlDir%
  call %x64Protector%
) ELSE (
  cd %LicServerDir%
  call %x86Protector%

  echo *****************************
  cd %TDataMngrDir%
  call %x86Protector%

  echo *****************************
  cd %TrackControlDir%
  call %x86Protector%
)

pause
