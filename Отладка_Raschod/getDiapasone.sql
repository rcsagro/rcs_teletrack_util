
IF OBJECT_ID ( 'dbo.getDiapasone', 'P' ) IS NOT NULL
    DROP PROCEDURE dbo.getDiapasone;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.getDiapasone
(
  @mobile int,
  @prevLogId int,
  @currLogId int
)
AS
BEGIN
  SELECT DataGps_ID AS DataGps_ID
       , Mobitel_ID AS Mobitel_ID
       , ((Latitude * 1.0000) / 600000) AS Lat
       , ((Longitude * 1.0000) / 600000) AS Lon
       , Altitude AS Altitude
       , dbo.FROM_UNIXTIME(UnixTime) AS time
       , (Speed * 1.852) AS speed
       , ((Direction * 360) / 255) AS direction
       , Valid AS Valid
       , (Sensor8 + (Sensor7 * 256) + (Sensor6 * 65536) + (cast(Sensor5 AS BIGINT) * 16777216) + (Sensor4 * 4294967296) + (Sensor3 * 1099511627776) + (Sensor2 * 281474976710656) + (Sensor1 * 72057594037927936)) AS sensor
       , Events AS Events
       , LogID AS LogID,
    SrvPacketID AS srvPack
  FROM
    datagps
  WHERE
    Mobitel_ID = @mobile
    AND (LogID BETWEEN @prevLogId AND @currLogId) ORDER BY UnixTime;
END
GO

IF OBJECT_ID ( 'dbo.getDiapasone64', 'P' ) IS NOT NULL
    DROP PROCEDURE dbo.getDiapasone64;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.getDiapasone64(
  @mobile INT,
  @prevLogId int,
  @currLogId int)
  AS
BEGIN
  SELECT  DataGps_ID, Mobitel_ID,
    ((Latitude * 1.000) / 10000000) AS Lat,
    ((Longitude * 1.000) / 10000000) AS Lng,
    Acceleration ,
    dbo.FROM_UNIXTIME(UnixTime) AS time ,
    Speed / 10 AS Speed,
    Valid,
    Sensors AS sensor,
  Events,
  LogID,
  Voltage,
  DGPS,
  Direction,
  Satellites,
  SensorsSet,
  RssiGsm,
  SrvPacketID AS srvPack FROM datagps64 d WHERE Mobitel_ID = @mobile AND
    (LogID BETWEEN @prevLogId AND @currLogId) ORDER BY UnixTime;
END
GO
